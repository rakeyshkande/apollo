<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:import href="validation.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="countryid" select="root/parameters/countryid"/>
<xsl:variable name="country" select="root/parameters/country"/>
<xsl:variable name="zipCode" select="root/parameters/zipCode"/>
<xsl:variable name="cartItemNumber" select="root/parameters/cartItemNumber"/>
<xsl:variable name="custom" select="root/parameters/custom"/>
<xsl:variable name="refreshServlet" select="root/parameters/refreshServlet"/>
<xsl:variable name="cancelItemShoppingServlet" select="root/parameters/cancelItemShoppingServlet"/>
<xsl:variable name="searchType" select="root/parameters/searchType"/>
<xsl:variable name="upsellCrumb" select="root/parameters/upsellCrumb"/>
<xsl:variable name="companyId" select="root/parameters/companyId"/>";
<xsl:variable name="imageSuffixSmall" select="root/parameters/imageSuffixSmall"/>";
<xsl:variable name="imageSuffixMedium" select="root/parameters/imageSuffixMedium"/>";
<xsl:variable name="imageSuffixLarge" select="root/parameters/imageSuffixLarge"/>";

<xsl:template match="/root">

<HTML>
<HEAD>
<TITLE> FTD - Product Detail </TITLE>
	<script language="javascript" src="../js/FormChek.js"/>
	<script language="javascript" src="../js/util.js"/>
	
	<SCRIPT language="javascript">
	
	var shipMethodFlorist = "<xsl:value-of select="/root/productList/products/product/@shipMethodFlorist"/>";
	var shipMethodCarrier = "<xsl:value-of select="/root/productList/products/product/@shipMethodCarrier"/>";
	var carrier = "<xsl:value-of select="/root/productList/products/product/@carrier"/>";
	var companyId = "<xsl:value-of select="$companyId"/>";

	var disableFlag = false;	
	var disableRefreshFlag = true;
	var validZip = false;
	var submitInd = "submit";
	var backupFlag = false;
	
	var upsellChecked = false;
	
	<![CDATA[
	

		function onKeyDown()
		{		
		   if (window.event.keyCode == 13)
			{
			    submitForm();				
			}
		}	
		
		function onKeyDownBack()
		{		
		   if (window.event.keyCode == 13)
			{
			    backUp();				
			}
		}	
		
		
		function EnterToAccept()
		{		
		   if (window.event.keyCode == 13)
			{
			    enableContinue();
			    refreshPage('zipChange');				
			}
		}	
		
	
		function backToShopping()
		{
		]]>
			var country = "<xsl:value-of select="/root/orderData/data[@name = 'country']/@value"/>";
			var zip = "<xsl:value-of select="/root/orderData/data[@name = 'zipCode']/@value"/>";
			var deliveryDate = "<xsl:value-of select="/root/orderData/data[@name = 'requestedDeliveryDate']/@value"/>";
			var deliveryDateDisplay = "<xsl:value-of select="/root/orderData/data[@name = 'requestedDeliveryDateDisplay']/@value"/>";
			var occasionDescription = "<xsl:value-of select="/root/orderData/data[@name = 'occasionDescription']/@value"/>";
			var city = "<xsl:value-of select="/root/orderData/data[@name = 'city']/@value"/>";
			var state = "<xsl:value-of select="/root/orderData/data[@name = 'state']/@value"/>";
			var occasion = "<xsl:value-of select="/root/orderData/data[@name = 'occasion']/@value"/>";
		<![CDATA[

			var url = "CategoryServlet" + "?sessionId=" + sessionId + 
						       "&persistentObjId=" + persistentObjId + 
						       "&countries=" + country +
						       "&zipCode=" + zip +
						       "&deliveryDates=" + deliveryDate +
						       "&deliveryDateDisplay=" + deliveryDateDisplay +
						       "&occasionDescription=" + occasionDescription +
						       "&city=" + city +
						       "&state=" + state +
						       "&occasions=" + occasion +
						       "&companyId=" + companyId;
			document.location = url;
		}
		
		function backUp()
		{
			window.history.back();
		}

	    /* Show an object */
	    function showObject(o,col,object,object1)
	    {
	        object.visibility = VISIBLE;
	        object1.visibility = VISIBLE;
	    }

	   /* Hide an object */
	    function hideObject(o,col,object,object1)
	    {
	        object.visibility = HIDDEN;
	        object1.visibility = HIDDEN;
	    }
	]]>
	</SCRIPT>


	<script language="javascript">
<![CDATA[

	function makeChecked(checkbox)
	{
		checkbox.checked='true';
	}
	
]]>	
	</script>
	
	
	<script language="javascript">
	
		var sessionId = "<xsl:value-of select="$sessionId"/>";
		var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
		var searchType = "<xsl:value-of select="$searchType"/>";
		var upsellCrumb = "<xsl:value-of select="$upsellCrumb"/>";
		var itemNumber = "<xsl:value-of select="$cartItemNumber"/>";
	
	</script>
	<script language="javascript">
	<![CDATA[
		
		function openUpdateWin()
		{
			var ie=document.all
			var dom=document.getElementById
			var ns4=document.layers

			cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
			updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
			scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
			updateWinV.top=scroll_top+document.body.clientHeight/2-100

			updateWinV.width=290
			updateWinV.height=100
			updateWinV.left=document.body.clientWidth/2 - 100
			updateWinV.visibility=(dom||ie)? "visible" : "show"
		}
	]]>
	</script>
	<SCRIPT language="javascript" >
	<![CDATA[

		function init()
  		{
  			disableFlag = false;
  			disableRefreshFlag = true;
  			]]>	
			
			<![CDATA[
  			//Populate the hidden occasion and countryType variables
  			]]>
  			document.forms[0].occasionId.value ="<xsl:value-of select="/root/orderData/data[@name = 'occasion']/@value"/>";																			 		
  			document.forms[0].countryType.value ="<xsl:value-of select="/root/orderData/data[@name = 'countryType']/@value"/>";
  			document.forms[0].searchType.value ="<xsl:value-of select="$searchType"/>";
  			
			<xsl:if test="/root/productList/products/product/@shipMethodFlorist = 'Y'">
				document.all.DELIVERY_DATE.value = "<xsl:value-of select="/root/orderData/data[@name ='requestedDeliveryDate']/@value"/>";
				
				
			</xsl:if>
			<!--End of if ShipMethodFlorist = Y -->
<!-- ***** -->			
<!-- ***** -->			
			
			
			// populate all other form fields
 			<xsl:for-each select="/root/item/ADD_ONS/ADD_ON">
				<xsl:choose>
					<xsl:when test="@type = '1'">			
						document.all.baloonCheckBox.checked = 'true';
						document.all.baloonQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@type = '2'">			
						document.all.bearCheckBox.checked = 'true';
						document.all.bearQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>				
				<xsl:choose>
					<xsl:when test="@type = '3'">			
						document.all.funeralBannerCheckBox.checked = 'true';
						document.all.funeralBannerQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>				
				<xsl:choose>
					<xsl:when test="@type = '4'">			
						document.all.cards.value = "<xsl:value-of select="/root/item/ADD_ONS/ADD_ON[@type = '4']/@id"/>";
					</xsl:when>
				</xsl:choose>								
				<xsl:choose>
					<xsl:when test="@type = '5'">			
						document.all.chocolateCheckBox.checked = 'true';
						document.all.chocolateQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>				
				
			</xsl:for-each>
			
			
			if(document.all.color1 != null) document.all.color1.value = "<xsl:value-of select="/root/item/DETAIL/@color1"/>";
			if(document.all.color2 != null) document.all.color2.value = "<xsl:value-of select="/root/item/DETAIL/@color2"/>";
			
			var priceValue = '';
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'standard']">
					priceValue = 'standard';
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'deluxe']">
					priceValue = 'deluxe';
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'premium']">
					priceValue = 'premium';
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'variable']">
					priceValue = 'variable';
				</xsl:when>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/orderData/data[@name='countryType']/@value = 'I'">
						<![CDATA[
							openJCPPopup();
						]]>
				</xsl:when>
				
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/productList/products/product/@jcpCategory != 'A'">
							<![CDATA[
								openJcpFoodItem();
							]]>
						</xsl:when>
					
						<xsl:otherwise>
							<xsl:if test="/root/pageData/data[@name='displaySpecGiftPopup']/@value = 'Y'">
								<![CDATA[
									//display the Yes/No Floral DIV Tag
									openNoFloral();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayNoProductPopup']/@value = 'Y'">
								<![CDATA[
									//display the No Products DIV Tag
									openNoProduct();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayFloristPopup']/@value = 'Y'">
								<![CDATA[
									//display the No Products DIV Tag
									openOnlyFloral();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayCodifiedSpecial']/@value = 'Y'">
								<![CDATA[
									// display the Codified Special DIV Tag			
									openCodifiedSpecial();
								]]>
							</xsl:if>							
							<xsl:if test="/root/pageData/data[@name='displayProductUnavailable']/@value = 'Y'">
								<![CDATA[
									// display the Product Unavailable DIV Tag
									openProductUnavailable();
								]]>
							</xsl:if>
						</xsl:otherwise>
					
					</xsl:choose>
					
				</xsl:otherwise>
			</xsl:choose>


			<xsl:if test="/root/extraInfo/subtypes/product">
				document.all.subCodeId.value = "<xsl:value-of select="/root/orderData/data[@name = 'subCodeSelected']/@value"/>";
			</xsl:if>
			if (document.all.variablePriceAmount != null) document.all.variablePriceAmount.value = "<xsl:value-of select="/root/item/DETAIL/@variablePrice"/>";
			
 			<![CDATA[ 
 			
 			// populate all other form fields
 			if(document.all.price != null)
 			{
				for (i=0; i<document.all.price.length; i++) 
				{
					if ( document.all.price[i].value == priceValue )
						document.all.price[i].checked = true;
				}
			}
 			
 			document.all.cartItemNumber.value = itemNumber; 			
 			
 			
 			
			//Display an alert when the page refreshes or reloads if the recipient is in Alaska or Hawaii
			]]>
		   	<xsl:if test="/root/orderData/data[@name='displaySpecialFee']/@value = 'Y'">
		   	<![CDATA[
		   		alertHIorAK();
		   	]]>
		   	</xsl:if>
		   	<![CDATA[
 			
   	    }
	
	function JCPPopChoice(inChoice)
	{
	/***  JC penney popup for international change
	*/
		if(inChoice == 'Y')
		{
			
		var url = "DNISChangeServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&source=productDetailJCP" + "&companyId=" + companyId;
	
			document.forms[0].source.value = 'productDetailJCP';
			document.forms[0].action = url;
			document.forms[0].method = "get";	
			document.forms[0].submit();	

		} else {
			document.forms[0].zip.value = '';
			refreshPage('zipChange');
			closeJCPPopup();
		}



	}
	function openJCPPopup()
	{

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		JCPPopV = (dom)?document.getElementById("JCPPop").style : ie? document.all.JCPPop : document.JCPPop
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		JCPPopV.top=scroll_top+document.body.clientHeight/2-100

		JCPPopV.width=290
		JCPPopV.height=100
		JCPPopV.left=document.body.clientWidth/2 - 50
		JCPPopV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJCPPopup()
	{
		JCPPopV.visibility = "hidden";
	}

	/****  initialize delivery method and date
	*/
	function initializeDeliveryDate(inType)
	{
]]>
		var inDate = "<xsl:value-of select="/root/orderData/data[@name = 'requestedDeliveryDate']/@value"/>";
<![CDATA[

		var dateExist = false;
		
		if ( inDate == "" )
		{
			dateExist = true;
		}
		else if ( inType == "Next Day Delivery" )
		{
			document.all.nextDayDate.value = inDate;
			if ((document.forms[0].nextDayDate.value != "") &&
				(document.forms[0].nextDayDate.value != " "))
			{
				dateExist = true;
			} 
		}
		else if ( inType == "Standard Delivery" )
		{
			document.all.standardDate.value = inDate;
			if ((document.forms[0].standardDate.value != "") &&
				(document.forms[0].standardDate.value != " "))
			{
				dateExist = true;
			} 
		}
		else if ( inType == "Two Day Delivery" )
		{
			document.all.twoDayDate.value = inDate;
			if ((document.forms[0].twoDayDate.value != "") &&
				(document.forms[0].twoDayDate.value != " "))
			{
				dateExist = true;
			}
		}
		else if ( inType == "Saturday Delivery" )
		{
			document.all.saturdayDate.value = inDate;
			if ((document.forms[0].saturdayDate.value != "") &&
				(document.forms[0].saturdayDate.value != " "))
			{
				dateExist = true;
			}
		}
		
		return dateExist;		
	}

	function populateDeliveryDate()
	{
		var radioGrp = document.forms[0].deliveryMethodx;
		
		if(radioGrp[0] != null)
		{
			for (var i=0; i < radioGrp.length; i++)
			{
				if(radioGrp[i].checked == true)
				{
					document.all.DELIVERY_SHIPPING_METHOD.value = radioGrp[i].value;
					getDeliveryDate(document.all.DELIVERY_SHIPPING_METHOD.value);
					
				}
			}			
		}
		else
		{
			if(document.all.deliveryMethodx.checked)
			{
				getDeliveryDate(document.all.deliveryMethodx.value);
		  		document.all.DELIVERY_SHIPPING_METHOD.value = document.all.deliveryMethodx.value;			
			}
		}
	}
		
	function getDeliveryDate(inMethod)
	{
		if(inMethod == "Next Day Delivery")
		{
			document.all.DELIVERY_DATE.value = document.all.nextDayDate.value;
		} else {
			if(inMethod == "Standard Delivery")
			{
				document.all.DELIVERY_DATE.value = document.all.standardDate.value;
			} else {
				if(inMethod == "Two Day Delivery")
				{
					document.all.DELIVERY_DATE.value = document.all.twoDayDate.value;
				} else {
					if(inMethod == "Saturday Delivery")
					{
						document.all.DELIVERY_DATE.value = document.all.saturdayDate.value;
					} 
				}
			}
		}
	}

		function deliveryPolicy()
		{
			var url_source = "../deliveryPolicy_floral.htm";

			if ( carrier == "Fed Ex" )
			{
				url_source = "../deliveryPolicy_dropship.htm";
			}
			else
			{
				url_source = "../deliveryPolicy_floral.htm";
			}

			var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
			window.showModalDialog(url_source,"delivery_policy", modal_dim);
		}

		
		function resetRadio( value )
		{
			var size = document.all.price.length;
			for (i=0; i<size; i++) {
				if ( document.all.price[i].value == "variable" )
					document.all.price[i].checked = true;
			}
		}
		
		function isValidUSDollar( dollar )
		{
			if ( dollar == null || dollar == "" )
				return false;

			first = dollar.indexOf(".");
		
			if ( first != -1 )
			{
				dollar_length = dollar.length;
			
				firstSub = dollar.substr( 0, first );
				secondSub = dollar.substr( first + 1, dollar_length - first );
				
				if ( ( dollar_length - first ) == 1 )
					return isInteger( firstSub );
				else				
					return ( isInteger( firstSub ) && isInteger( secondSub ) );
				
			} else {
				return isInteger( dollar );
			}
			
			return false;
		}
		
		
		function validateVariablePrice()
		{
			var field = document.all.variablePriceAmount;
			if ( field == null )
				return true;
				
			checked = isValidUSDollar ( field.value );		
		  	//Check to make sure the variablePriceAmount is valid US dollar
		  	
		  	// Check the variable price range
		  	if ( checked )
		  	{
		  		if ( parseFloat(field.value) < parseFloat(variablePriceMin) || parseFloat(field.value) > parseFloat(variablePriceMax ) )
		  		{
		  			invalidVariablePrice.style.visibility = "visible";
		  			message = "Varible price range: " + variablePriceMin + " - " + variablePriceMax;
		  			checked = false;
		  		} else {
]]>
			<xsl:if test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
<![CDATA[
		  	
		  			var decpos = (field.value).indexOf(".")
		  			if(decpos == "-1" || (decpos >= ((field.value).length - 2)))
		  			{
		  				invalidVariablePrice.style.visibility = "visible";
		  				checked = false;
		  				message = "Price must end with .99";
		  			} else {
		  				
		  				if((field.value).substr(decpos, 3) != ".99")
		  				{
		  					invalidVariablePrice.style.visibility = "visible";
		  					checked = false;
		  					message = "Price must end with .99";
		  				}
		  			}
]]>
			</xsl:if>
<![CDATA[

		  			
		  			invalidVariablePrice.style.visibility = "hidden";
		  		}
		  	
		  	}
		  	
		  	if( !checked )
		  	{
	   			field.focus();
		   		field.style.backgroundColor='pink'
		  	}
		    return checked;
		 }

		 function checkVariablePrice( value )
		 {
		
		 	 if (value != "" && document.all.variablePriceAmount != null ) {
		 	 	var v = document.all.variablePriceAmount.value= "";
		 	 	document.all.variablePriceAmount.style.backgroundColor = "white";
			  }
		 }


 		 function validateForm()
  	     {
  	      	  size = document.all.price.length;
  	      	  checked = true;
			  for (i=0; i<size; i++)
			  {
			  	if (document.forms[0].price[i].checked && document.forms[0].price[i].value == "variable" )
			  	{
			  		checked = validateVariablePrice();  					  		
		  		}
			  }

			  
]]>
			<xsl:if test="/root/extraInfo/subtypes/product">
<![CDATA[
				document.forms[0].subCodeId.className="TblText";
				if(document.forms[0].subCodeId.value == "")
				{
					document.forms[0].subCodeId.className="Error";
					document.forms[0].subCodeId.focus();
					checked = false;
				}

]]>
			</xsl:if>
			<xsl:if test="/root/productList/products/product/@color1 !=''">
<![CDATA[
				document.forms[0].color1.className="TblText";
				if(document.forms[0].color1.value == "")
				{
					document.forms[0].color1.className="Error";
					if(checked != false)
					{
						document.forms[0].color1.focus();
						checked = false;
					}
				}
							
]]>
			</xsl:if>
			<xsl:if test="/root/productList/products/product/@color2 !=''">
<![CDATA[
				document.forms[0].color2.className="TblText";
				if(document.forms[0].color2.value == "")
				{
					document.forms[0].color2.className="Error";
					if(checked != false)
					{
						document.forms[0].color2.focus();
						checked = false;
					}	
				}
				


]]>
			</xsl:if>
<![CDATA[

 			  return checked;
		 }

	 function submitForm()
	 {	
	
		if(backupFlag == true)
		{
			backToShopping();
			return;
		}
		else
		{
			form.submit();
		}	
			
	 }
		 
		 function viewLargeImage()
		 {
		 	var ie=document.all
			var dom=document.getElementById
			var ns4=document.layers
	
			cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
			largeImage = (dom)?document.getElementById("showLargeImage").style : ie? document.all.showLargeImage : document.showLargeImage
			scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
			largeImage.top=scroll_top+document.body.clientHeight/2 -200
	
			largeImage.width=340;
			largeImage.height=400;
			largeImage.left= 200;
			largeImage.visibility=(dom||ie)? "visible" : "show"

		 }
		 
		function closeLargeImage()
		{
			largeImage.visibility= "hidden";
		}
	
	function disableContinue()
	{
	/*** Change image of continue button to disabled and prevent submitting the page
	*/
	//but only if the country is domestic and the key stroke isn't a tab
	        if (window.event.keyCode != 9)	
		{
	]]>
		<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
			<xsl:if test="/root/productList/products/product/@status = 'A'">						
				<![CDATA[	
					disableFlag = true;
					document.images['continueButton'].src  = '../images/button_continue_disabled.gif';
				]]>
			</xsl:if>
		</xsl:if>
	<![CDATA[	
		}
	}
	
	function clickToAccept()
	{
		if(disableRefreshFlag == false)
		{
			enableContinue();
			refreshPage('zipChange');
		}
	}
	
	function enableContinue()
	{
	/*** Change image of continue button to enabled and allow submitting the page
	*/
	//but only if domestic
	]]>
		<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
			<xsl:if test="/root/productList/products/product/@status = 'A'">						
				<![CDATA[	
						disableFlag = false;
						document.images['continueButton'].src  = '../images/button_continue.gif';
				]]>
			</xsl:if>
		</xsl:if>
	<![CDATA[
	}
	function refreshPage()
	{
    /***
    *   This function submits the page to refresh delivery dates
    */    
    	//But first do client side zip validation    
        
    	//reset the backupFlag    
    	backupFlag = false;
    	
    	//reset the background color    	
	form.zip.style.backgroundColor='white';

	//basic zip validation
	var check = true
		
]]>
	<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
<![CDATA[			
		ziplen = document.all.zip.value.length;
		zip = stripWhitespace(document.all.zip.value);	        
		firstchar = zip.substring(0,1);	 
		
		//length of zip must be 0, 5 or 6 
		if ((ziplen != 5) ]]> &amp; <![CDATA[ (ziplen != 6) ]]> &amp; <![CDATA[ (ziplen != 0))
		    check = false;	        

		// Canada must start with a letter
		// if ((ziplen == 6) ]]> &amp; <![CDATA[ (!isLetter(firstchar)))
		//    check = false;

		// USA must start with a number
		// if ((ziplen == 5) ]]> &amp; <![CDATA[ (isLetter(firstchar)))
		//    check = false;		   
		
		//don't call the server if the length is 0
		if (zip == '')
		{
		    disableRefreshFlag = true;		    
		    disableClickToAccept();
		    check = true
		}
		  
		
		//validation error
		if (check == false)
		{
		   document.forms[0].zip.focus();
		   document.forms[0].zip.style.backgroundColor='pink';
		   alert("Invalid zip code")
		   disableContinue();		   
		   disableRefreshFlag = true;
		   return false;
		}
		
		
]]>
	</xsl:if>
<![CDATA[    
    
    	//only refresh the page if the disableRefresh flag is false (the button is not dimmed)
		if (disableRefreshFlag == false)
		{
			//server side zip validation
		    validateZip()
			
		    //evaluate the response from the server side zip validation
		    if (validZip == false)
		    {
		    	return false;
		    }	
		    
		    if ( shipMethodCarrier == "Y" )
	   		{
	   			populateDeliveryDate();
	   		}
		}
   	}
	
	function validateZip(){
	//This function is for validating the zip code
	//when the delivery date is being recalculated	
	
	//reset the background color
	form.zip.style.backgroundColor='white';	
	
	var check = true	
	
	//open the DIV tag
	openUpdateWin();					
	
	/***
     *   Check Recipient Zip Code if it exist
     */
 		document.forms[0].zip.className="TblText";
 		var bag = "-";
		var zpnws = stripCharsInBag(document.forms[0].zip.value, bag);
     		var zcnws = stripWhitespace(zpnws);

		//If the zip code doesn't exist or its made up of invalid characters then its invalid
		//so set the check variable to false, disable the continue button, and close the update window
		//if (zcnws.length == 0 || (zcnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(zcnws)))
		
		//if ((zcnws.length == 0) || (!isAlphanumeric(zcnws)))
		if ((!isAlphanumeric(zcnws)) ]]> &amp; <![CDATA[ (zcnws.length != 0))
    	{
    		document.forms[0].zip.style.backgroundColor='pink';    		
    		alert("Please correct marked fields");
   			document.forms[0].zip.focus();
    		check = false;
    		disableContinue();    		    		
    		closeUpdateWin();
    		return false;
    	}

///////////////////////////////////////////////////////////////////////////
//INITIATE SERVER SIDE VALIDATIION
///////////////////////////////////////////////////////////////////////////
	
	if(check == true)
	{
]]>
    	<xsl:if test="/root/orderData/data[@name = 'countryType']/@value != 'I'">
<![CDATA[				
			first5 = document.forms[0].zip.value;
			first5 = first5.substr(0,5);			
			addValidationEntry("ZIPCODE", first5);			
			callToServer();
]]>
		</xsl:if>
<![CDATA[
	}

	return check;

	}   //close function
	
	
	function deCode(strSelection)
	{
			strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
			strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

			return strSelection;
	}
	
	function onValidationNotAvailable()
	{
	closeUpdateWin();
	var x = confirm("Validation for this page was not fully completed. Would you still like to continue?");
		if(x == true)
		{
			if(submitInd == "submit")
			{
				occasionform.submit();
			} else
			{			 
				//do nothing
				return false;
			}
		}
	}

	function onValidationResponse(validation)
	{		
		var ret_zipCode = validation.ZIPCODE.value;		
	
		//get the return value from the server
		closeUpdateWin();
		
		//if OK then submit the page so the delivery dropdown is refreshed
		if(ret_zipCode == "OK")
		{			
			document.all.command.value = "updateZip";
			]]>
				form.action = "<xsl:value-of select="$refreshServlet"/>";
			<![CDATA[						
		   		if ( shipMethodCarrier == "Y" )
		   		{
		   			populateDeliveryDate();
		   		}
		   				 	
				form.submit();		
				enableContinue();				
		}

		//not OK, so set the background color, display alert,
		//disable the button, and set a variable
		else
		{			
			form.zip.style.backgroundColor='pink';			
			alert("Could not validate zip code");
			disableContinue();
			validZip = false;	
			return false;
		}

	}
	
	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";
	}
	
	function openCalendarLoadingDiv()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
	
		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("calendarLoading").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2-200
	
		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}
	
	function closeCalendarLoadingDiv()
	{
		calendarLoading.style.visibility = "hidden";
	}
	
	/*	**************************************************************************
			This group of functions is used to display/hide the Specialty Gift
			only available for zip code message box.
		**************************************************************************	*/
	function openNoFloral()
	{
		disableContinue();
		
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
	
		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noFloralV = (dom)?document.getElementById("noFloral").style : ie? document.all.noFloral : document.noFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noFloralV.top=scroll_top+document.body.clientHeight/2-100
	
		noFloralV.width=290
		noFloralV.height=100
		noFloralV.left=document.body.clientWidth/2 - 100
		noFloralV.visibility=(dom||ie)? "visible" : "show"
	}
	
	function closeNoFloral()
	{
		//enableContinue();
		disableContinue();
		noFloral.style.visibility = "hidden";
	}
	
	/*	**************************************************************************
			This group of functions is used to display/hide the No Product
			available for zip code message box
		**************************************************************************	*/
	function openNoProduct()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noProductV = (dom)?document.getElementById("noProduct").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noProductV.top=scroll_top+document.body.clientHeight/2-100

		noProductV.width=290
		noProductV.height=100
		noProductV.left=document.body.clientWidth/2 - 50
		noProductV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoProduct()
	{
		noProductV.visibility = "hidden";
		document.forms[0].zip.focus();
	}

	function noProduct()
	{
		closeNoProduct();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the Product Unavailable
			for zip code message box
		**************************************************************************	*/
	function openProductUnavailable()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		productUV = (dom)?document.getElementById("productUnavailable").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		productUV.top=scroll_top+document.body.clientHeight/2-100

		productUV.width=290
		productUV.height=100
		productUV.left=document.body.clientWidth/2 - 50
		productUV.visibility=(dom||ie)? "visible" : "show"
	}
	
	function openJcpFoodItem()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		productUV = (dom)?document.getElementById("jcpFood").style : ie? document.all.jcpFood : document.jcpFood
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		productUV.top=scroll_top+document.body.clientHeight/2-100

		productUV.width=290
		productUV.height=100
		productUV.left=document.body.clientWidth/2 - 50
		productUV.visibility=(dom||ie)? "visible" : "show"
	}
	
	function closeJcpFoodItem()
	{
		productUV.visibility = "hidden";
		document.forms[0].backButton.focus();
	}
	
	function closeProductUnavailable()
	{
		productUV.visibility = "hidden";
		document.forms[0].zip.focus();
	}

	function productUnavailable()
	{
		closeProductUnavailable();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the Floral Only
			for zip code message box
		**************************************************************************	*/
	function openOnlyFloral()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		onlyFloralV = (dom)?document.getElementById("onlyFloral").style : ie? document.all.onlyFloral : document.onlyFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		onlyFloralV.top=scroll_top+document.body.clientHeight/2-100

		onlyFloralV.width=290
		onlyFloralV.height=100
		onlyFloralV.left=document.body.clientWidth/2 - 50
		onlyFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeOnlyFloral()
	{
		onlyFloralV.visibility = "hidden";
		document.forms[0].zip.focus();
	}

	function onlyFloral()
	{
		closeOnlyFloral();
	}


	/*	**************************************************************************
			This group of functions is used to display/hide the Codified Special
			for zip code message box
		**************************************************************************	*/
	function openCodifiedSpecial()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		codifiedSpecialV = (dom)?document.getElementById("codifiedSpecial").style : ie? document.all.codifiedSpecial : document.codifiedSpecial
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		codifiedSpecialV.top=scroll_top+document.body.clientHeight/2-100

		codifiedSpecialV.width=290
		codifiedSpecialV.height=100
		codifiedSpecialV.left=document.body.clientWidth/2 - 50
		codifiedSpecialV.visibility=(dom||ie)? "visible" : "show"
				
		document.all.continueButton.src = "../images/button_back.gif";
		backupFlag = true;
		
	}

	function closeCodifiedSpecial()
	{
		codifiedSpecialV.visibility = "hidden";
		document.forms[0].zip.focus();
	}

	function codifiedSpecial()
	{
		closeCodifiedSpecial();
	}


function disableClickToAccept()
{
/*** Change image of click to accept button to disabled and prevent page refresh
*/
	//but only if its domestic
]]>
	<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
<![CDATA[
		disableRefreshFlag = true;
		document.all.clickToAcceptButton.src  = '../images/button_click_to_accept_disabled.gif';
]]>
	</xsl:if>
<![CDATA[
}

function enableClickToAccept()
{
/*** Change image of clicktoaccept button to enabled
*/
//but only if domestic and only if the key stroke isn't the tab
	if (window.event.keyCode != 9)
	{
	]]>
		<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
<![CDATA[
		disableRefreshFlag = false;
		document.all.clickToAcceptButton.src  = '../images/button_click_to_accept.gif';
]]>
		</xsl:if>
<![CDATA[
	}
}

	function setDeliveryDateDisplay(newValue)
	{
		//alert(newValue);
		document.all.deliveryDateDisplay.value = newValue;
		
		//alert(document.all.deliveryDateDisplay.value);
	}

function cancelItem()
    /***
    *   This function will call the cancel Item servlet
    */
	{
	]]>
		target = "<xsl:value-of select="$cancelItemShoppingServlet"/>";

<![CDATA[
var url = target + "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&fromPageName=ProductDetail" + "&zipCode=" + document.all.zip.value + "&companyId=" + companyId;
]]>
		<xsl:if test="/root/pageData/data[@name='cartItemNumber']/@value">
			var cartitem = "<xsl:value-of select="/root/pageData/data[@name='cartItemNumber']/@value"/>";
<![CDATA[
			url = url.concat("&itemCartNumber=" + cartitem);

]]>
		</xsl:if>
<![CDATA[

		document.location = url;

	}


]]>
	</SCRIPT>
	
	<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init()">
	<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
		<TR>
			<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
			<TD align="right"> <IMG src="../images/headerFAQ.gif" onclick="javascript:doFAQ();"/> </TD>
		</TR>
		<TR> <TD colspan="6" valign="center"> <HR> </HR> </TD> </TR>
	</TABLE>

	<FORM name="form" method="get" action="{$actionServlet}" >
		<input type="hidden" name="addWithSameRecpFlag" value="Y"/>
		<input type="hidden" name="deliveryDateDisplay" value=""/>
		<INPUT type="hidden" name="command" value="updateItem"/>
		<INPUT type="hidden" name="cartItemNumber" value=""/>
		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
		<INPUT type="hidden" name="searchType" value="{$searchType}"/>
		<INPUT type="hidden" name="upsellFlag" value="Y"/>
		
		<INPUT type="hidden" name="custom" value="{$custom}"/>
		<INPUT type="hidden" name="DELIVERY_DATE" value=""/>
		<INPUT type="hidden" name="DELIVERY_SHIPPING_METHOD" value=""/>
		<INPUT type="hidden" name="SELECTED_YEAR" value=""/>
		<INPUT type="hidden" name="SELECTED_MONTH" value=""/>
		<INPUT type="hidden" name="CHOSEN_DATE" value=""/>
		<INPUT type="hidden" name="occasionId" value=""/>
		<INPUT type="hidden" name="countryType" value=""/>
		<INPUT type="hidden" name="source" value=""/>
		<input type="hidden" name="companyId" value="{$companyId}"/>
		<INPUT type="hidden" name="test">
			<xsl:attribute name="value"><xsl:value-of select="/root/deliveryDates/deviveryDate[@type='nextday']/@date"/></xsl:attribute>
		</INPUT>
			
		<CENTER>
		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
	    	<TR>
			    <TD width="70%"> &nbsp; </TD>
				<TD valign="center" align="right" nowrap="">
					<xsl:choose>
						<xsl:when test="$cartItemNumber=''">
            				<A tabindex="-1" href="javascript:doShoppingCart();" STYLE="text-decoration:none">
			    			<IMG tabindex="-1" id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A>
			    		</xsl:when>
			    		<xsl:otherwise>
							<IMG tabindex="-1" id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/><A tabindex="-1"><font color="#C0C0C0">Shopping Cart</font></A>
						</xsl:otherwise>
					</xsl:choose>
        		</TD>
				<TD valign="center" align="right" nowrap="">
					<A href="javascript:doCancelOrder();" STYLE="text-decoration:none">Cancel Order</A>
				</TD>
			</TR>
	    	<TR>
			    <TD width="70%"><xsl:call-template name="customerHeader"/> &nbsp; </TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD colspan="4" class="tblheader">
					<xsl:variable name="occasionlbl" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
					<xsl:variable name="categories" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
					<xsl:variable name="productlist" select="/root/previousPages/previousPage[@name = 'Product List']/@value"/>

					<xsl:if test="$occasionlbl != ''">
						&nbsp;
						<a tabindex="-1" href="{$occasionlbl}" class="tblheader">Occasion</a>&nbsp;>
						<xsl:if test= "/root/orderData/data[@name='countryType']/@value = 'D'">
							<a tabindex="-1" href="{$categories}" class="tblheader">Categories</a>&nbsp;>
						</xsl:if>
						<xsl:if test= "$searchType != 'simpleproductsearch'">
							<a tabindex="-1" href="{$productlist}" class="tblheader">Product List</a>&nbsp;>
						</xsl:if>
						Upsell Detail
					</xsl:if>
				</TD>
			</TR>
		</TABLE>

		<!-- Main Table -->
		<xsl:choose>
		<xsl:when test="/root/upsellMaster/upsellDetail[@name = 'masterStatus']/@value = 'Y' ">
		
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
						<TR>
							<td>
								<SCRIPT language="JavaScript">
										// Exception display trigger variables
										var exceptionMessageTrigger = false;
										var selectedDate = new Date("<xsl:value-of select="/root/orderData/data[@name='requestedDeliveryDate']/@value"/>");
										var exceptionStart = new Date("<xsl:value-of select="/root/productList/products/product/@exceptionStartDate"/>");
										var exceptionEnd = new Date("<xsl:value-of select="/root/productList/products/product/@exceptionEndDate"/>");
		
										var pid = "<xsl:value-of select="/root/upsellMaster/upsellDetail[@name='masterId']/@value"/>";
										var imageSuffixSmall = '<xsl:value-of select="$imageSuffixSmall"/>';
		 											<![CDATA[
		 											document.write("&nbsp; &nbsp; <img  border=\"0\" ")
		 											document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
													document.write("if(imag != \'npi" + imageSuffixSmall + "\'){ ")
		
													document.write("this.src = \'/product_images/npi_1.gif\'}\" ")
													document.write("src=\"/product_images/" + pid + imageSuffixSmall + "\"> ")
										document.write("</img>")
										]]>
								</SCRIPT>
							</td>
							
							<TD colspan="3" valign="top">
								<SPAN class="header2">
									<script><![CDATA[
										document.write(deCode("]]> <xsl:value-of select="/root/upsellMaster/upsellDetail[@name='masterName']/@value"/> <![CDATA["));
									]]></script>
								&nbsp; &nbsp; #<xsl:value-of select="/root/upsellMaster/upsellDetail[@name='masterId']/@value"/> </SPAN>
								<br/>
								<SPAN class="description"><xsl:value-of select="/root/upsellMaster/upsellDetail[@name='masterDescription']/@value"  disable-output-escaping="yes"/></SPAN>
							
								<xsl:if test="/root/upsellMaster/upsellDetail[@name = 'noneAvailable']/@value = 'Y' or /root/upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'N'">
									<br/><br/><SPAN class="tblText"><font color="red">None of the products are currently available for this delivery location.</font></SPAN>
								</xsl:if>
							
							</TD>
						</TR>
						<tr>
							<td colspan="3">
							&nbsp; &nbsp;
							<A tabindex ="1">
								<xsl:attribute name="href">javascript:viewLargeImage();</xsl:attribute>View Larger Image</A>
							</td>
						</tr>
						
						<TR> <TD height="10" colspan="6"> </TD> </TR>
						
						<xsl:variable name="rewardType" select="/root/orderData/data[@name='rewardType']/@value"/>
						<xsl:variable name="finalSequence" select="count(/root/productList/products/product)"/>
						<xsl:variable name="finalProductName" select="/root/productList/products/product[@upsellSequence = $finalSequence]/@upsellProductName"/>
						
						<xsl:for-each select="/root/productList/products/product">
									<TR>
										<xsl:variable name="upsellSequence" select="@upsellSequence"/>
										<TD id="productRow_{$upsellSequence}" valign="top" colspan="4">
									
										<xsl:if test="@status = 'U' or @specialUnavailable = 'Y' or /root/upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'N'">
											<SCRIPT language="JavaScript">
											    var productSeq = "<xsl:value-of select="@upsellSequence"/>";
	 											<![CDATA[
												document.getElementById("productRow_" + productSeq).style.backgroundColor = '#CCCCCC';
												]]>
											</SCRIPT>
										</xsl:if>
										
										   <TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
											 						
											<TR> <TD height="1" colspan="6" class="tblheader"> </TD> </TR>
												<xsl:if test="/root/upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'Y' and @upsellSequence != $finalSequence and /root/upsellMaster/upsellDetail[@name='showScripting']/@value = 'Y' and $upsellCrumb = 'Y'">
													<xsl:if test="@status = 'U' or @specialUnavailable = 'Y'">
														<TR>
														<TD colspan="6" class="ScreenPrompt">
															<xsl:value-of select="/root/upsellExtraList/scripting/script[@fieldName='BASE_NOT_AVAIL_1']/@scriptText"/>
		 													<xsl:value-of select="@upsellProductName"/>
		 													<xsl:value-of select="/root/upsellExtraList/scripting/script[@fieldName='BASE_NOT_AVAIL_2']/@scriptText"/>
		 													<xsl:value-of select="$finalProductName"/>
		 													<xsl:value-of select="/root/upsellExtraList/scripting/script[@fieldName='BASE_NOT_AVAIL_3']/@scriptText"/>
		 												</TD>
		 												</TR>
	 												</xsl:if>
 												</xsl:if>
											<TR>
											
													<TD width="15%" class="tblText" align="left" valign="top">
													<TABLE border="0" cellpadding="0" cellspacing="0">
													<tr><td>
														<SCRIPT language="JavaScript">
														<xsl:choose>
														<xsl:when test="@status = 'U' or @specialUnavailable = 'Y' or /root/upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'N'">
				 												<![CDATA[
				 												document.write("<input type=\'radio\' class=\'tblText\' name=\'productid\' disabled>")
																]]>
														</xsl:when>
														<xsl:otherwise>
															var newpid = "<xsl:value-of select="@productID"/>";
															
																<![CDATA[
																if(upsellChecked == false) 
																{
				 													document.write("<input type=\'radio\' class=\'tblText\' name=\'productid\' value=\'" + newpid + "\' checked>")
																	upsellChecked = true;
																}
																else 
																{
																	document.write("<input type=\'radio\' class=\'tblText\' name=\'productid\' value=\'" + newpid + "\'>")
																}
																]]>
												
															
														</xsl:otherwise>
														</xsl:choose>
														</SCRIPT>
														
													</td><td>
														&nbsp; &nbsp;
														<xsl:choose>
															<xsl:when test="@standardDiscountPrice > 0 and @standardDiscountPrice != @standardPrice">
																<SPAN style="color='red'"><strike>$<xsl:value-of select="@standardPrice"/></strike> &nbsp; </SPAN>$<xsl:value-of select="@standardDiscountPrice"/>
															</xsl:when>
    														<xsl:otherwise>
																$<xsl:value-of select="@standardPrice"/>
															</xsl:otherwise>
														</xsl:choose>
														<xsl:if test="@standardRewardValue > 0">
															<xsl:if test="$rewardType != 'Percent' and $rewardType != 'Dollars'">
																 &nbsp; (<xsl:value-of select="@standardRewardValue"/>)
															</xsl:if>
														</xsl:if>
														 &nbsp; &nbsp;
													</td></tr>
													</TABLE>
													</TD>
											
											
												<TD width="85%" colspan="2" class="tblText">
													<SPAN class="label"> <xsl:value-of select="@upsellProductName" />&nbsp;&nbsp;&nbsp;&nbsp; #<xsl:value-of select="@productID"/> </SPAN> <BR> </BR>
													<xsl:value-of select="@longDescription"  disable-output-escaping="yes"/>
														<BR> </BR>
													<xsl:value-of select="@arrangementSize"/>
												</TD>
											</TR>

											<xsl:if test="@productType != 'SPEGFT' and @serviceCharge != ''">
												<TR>
												
												<td></td>
												
													<TD class="TblTextBold" align="left">
														Service Charge:
													</TD>
													<TD class="tblText" align="left"> $<xsl:value-of select="@serviceCharge"/> </TD>
												</TR>
    										</xsl:if>

											<TR>
											<td></td>
											
												<TD class="TblTextBold" align="left" valign="top">
													Delivery Method:
												</TD>

												<TD class="tblText">We can deliver this product as early as
													<xsl:value-of select="@deliveryDate"/>
													<xsl:if test="@firstDeliveryMethod = 'florist'">
												 		&nbsp;using an FTD Florist.
													</xsl:if>

    												<xsl:if test="@firstDeliveryMethod = 'carrier'">
    													&nbsp;using <xsl:value-of select="@carrier"/>.
    												</xsl:if>

    												<xsl:variable name="devtype1" select="@deliveryType1"/>

    												<xsl:if test="@deliveryType1 !='' and @deliveryType1 = /root/shippingMethods/shippingMethod/method[@description=$devtype1]/@description">
	    					
	    												<TR>
	    												<td></td>
	    												
	    													<TD class="tblText">
	    														<xsl:value-of select="@deliveryType1"/>
	   														</TD>
	    													<TD class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
																	$<xsl:value-of select="@deliveryPrice1"/>
	    														</xsl:if>
	    													</TD>
	    												</TR>
    												</xsl:if>

    												<xsl:variable name="devtype2" select="@deliveryType2"/>

    												<xsl:if test="@deliveryType2 !='' and @deliveryType2 = /root/shippingMethods/shippingMethod/method[@description=$devtype2]/@description">
			    										<TR>
			    										<td></td>
			    											<TD class="tblText">
			    												<xsl:value-of select="@deliveryType2"/>
		    												</TD>
			    											<TD class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
				    												$<xsl:value-of select="@deliveryPrice2"/>
	    														</xsl:if>
			    											</TD>
			    										</TR>
		    										</xsl:if>

    												<xsl:variable name="devtype3" select="@deliveryType3"/>

    												<xsl:if test="@deliveryType3 !='' and @deliveryType3 = /root/shippingMethods/shippingMethod/method[@description=$devtype3]/@description">
    													<TR>
    													<td></td>
    														<TD class="tblText">
    															<xsl:value-of select="@deliveryType3"/>
   															</TD>
    														<TD class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
	    															$<xsl:value-of select="@deliveryPrice3"/>
	    														</xsl:if>
    														</TD>
    													</TR>
    												</xsl:if>

    												<xsl:variable name="devtype4" select="@deliveryType4"/>

   													<xsl:if test="@deliveryType4 !='' and @deliveryType4 = /root/shippingMethods/shippingMethod/method[@description=$devtype4]/@description">
    													<TR>
    													<td></td>
    														<TD class="tblText">
    															<xsl:value-of select="@deliveryType4"/>
   															</TD>
    														<TD class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
	    															$<xsl:value-of select="@deliveryPrice4"/>
	    														</xsl:if>
    														</TD>
    													</TR>
		    										</xsl:if>

		    										<xsl:variable name="devtype5" select="@deliveryType5"/>

		    										<xsl:if test="@deliveryType5 !='' and @deliveryType5 = /root/shippingMethods/shippingMethod/method[@description=$devtype5]/@description">
			    										<TR>
			    										<td></td>
			    											<TD class="tblText">
			    												<xsl:value-of select="@deliveryType5"/>
			    												</TD>
			    											<TD class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
					    											$<xsl:value-of select="@deliveryPrice5"/>
	    														</xsl:if>
			    											</TD>
			    										</TR>
		    										</xsl:if>

		    										<xsl:variable name="devtype6" select="@deliveryType6"/>

		    										<xsl:if test="@deliveryType6 !='' and @deliveryType6 = /root/shippingMethods/shippingMethod/method[@description=$devtype6]/@description">
			    										<TR>
			    										<td></td>
			    											<TD class="tblText">
			    												<xsl:value-of select="@deliveryType6"/>
			    											</TD>
			    											<TD class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
				    												$<xsl:value-of select="@deliveryPrice6"/>
	    														</xsl:if>
			    											</TD>
			    										</TR>
		    										</xsl:if>
												</TD>
											</TR>

											<xsl:if test="@discountPercent > 0">
												<TR>
		             								<TD class="TblTextBold" align="left">
														Discount:	</TD>
													<TD class="tblText" align="left"> <xsl:value-of select="@discountPercent"/>% </TD>
												</TR>
    										</xsl:if>
    										
    										<xsl:if test="@status = 'U' or @specialUnavailable = 'Y'">
    											<xsl:if test="/root/upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'Y'">
													<SCRIPT language="JavaScript">
													    <![CDATA[
														document.write("<TR><td></td><TD colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for the selected delivery location.<![CDATA[</font></TD></TR>");
														]]>
													</SCRIPT>
												</xsl:if>
											</xsl:if>

											<xsl:choose>
												<xsl:when test="/root/pageData/data[@name='requestedDeliveryDate']/@value">

													<xsl:if test="@exceptionCode = 'U'">
														<SCRIPT language="JavaScript">
														<![CDATA[
															if(selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
																exceptionMessageTrigger = true;
				 												document.write("<TR><td></td><TD colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionEndDate"/><![CDATA[</font></TD></TR>");
															}
														]]>
														</SCRIPT>
													</xsl:if>

													<xsl:if test="@exceptionCode = 'A'">
														<SCRIPT language="JavaScript">
														<![CDATA[
															if(selectedDate < exceptionStart || selectedDate > exceptionEnd) {
																exceptionMessageTrigger = true;
				 												document.write("<TR><td></td><TD colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionEndDate"/><![CDATA[</font></TD></TR>");
															}
														]]>
														</SCRIPT>
													</xsl:if>

													<SCRIPT language="JavaScript">
													<![CDATA[
														if(exceptionMessageTrigger == true) {
															document.write("<TR><td></td><TD colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="@exceptionMessage"/><![CDATA[</font></TD></TR>");
														}
														else {
															document.write("<TR><td></td><TD colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="@exceptionMessage"/><![CDATA[</font></TD></TR>");
														}
													]]>
													</SCRIPT>

												</xsl:when>
												<xsl:otherwise>

													<xsl:if test="@exceptionCode = 'A'">
		    											<TR>
		    											    <td></td>
			   												<TD colspan="3" class="tblText" align="left"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionEndDate"/></font> </TD>
														</TR>
													</xsl:if>

													<xsl:if test="@exceptionCode = 'U'">
		    											<TR>
		    											  <td></td>
			   												<TD colspan="3" class="tblText" align="left"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionEndDate"/></font> </TD>
														</TR>
													</xsl:if>

													<TR>
													    <td></td>
			   											<TD colspan="3" class="tblText" align="left"><font color="red"><xsl:value-of select="@exceptionMessage"/></font></TD>
													</TR>

												</xsl:otherwise>
											</xsl:choose>


											<xsl:if test="@displaySpecialFee = 'Y'">
    											<TR>
	   												<TD colspan="2" class="tblText" align="left"> <font color="red">A $12.99 surcharge will  be added to deliver this item to Alaska or Hawaii</font> </TD>
												</TR>
											</xsl:if>

											<xsl:if test="@displayCableFee = 'Y'">
    											<TR>
	   												<TD colspan="2" class="tblText" align="left"> <font color="red">A $12.99 cable fee will  be added to deliver this item to an International country</font> </TD>
												</TR>
											</xsl:if>
											
											
										</TABLE>
									</TD>
								</TR>
							
						</xsl:for-each>
						
						<TR> <TD height="1" colspan="6" class="tblheader"> </TD> </TR>
						
						<TR> <TD height="10" colspan="6"> </TD> </TR>

						<TR>
							<TD colspan="6" align="right">
								<xsl:choose>
									<xsl:when test="/root/productList/products/product[@upsellSequence = '1']/@status = 'A'">		
										<IMG tabindex ="37" onkeydown="javascript:onKeyDown()" name="continueButton" src="../images/button_continue.gif" onclick="submitForm()" border="0"/> &nbsp;&nbsp;
									</xsl:when>
		   							<xsl:otherwise>
		   								<IMG tabindex ="37" onkeydown="javascript:onKeyDownBack()" name="backButton" src="../images/button_back.gif" onclick="backUp()" border="0"/> &nbsp;&nbsp;
		   							</xsl:otherwise>
								</xsl:choose>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<TD class="tblheader">&nbsp;
				
					<xsl:variable name="occasionlbl1" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>									
					<xsl:variable name="categories1" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>

					<xsl:variable name="productlist1" select="/root/previousPages/previousPage[@name = 'Product List']/@value"/>
					<!--<xsl:if test="$cartItemNumber=''">-->
					<xsl:if test="$occasionlbl1 != ''">
						&nbsp;
						<a tabindex="-1" href="{$occasionlbl1}" class="tblheader">Occasion</a>&nbsp;>
						<xsl:if test= "/root/orderData/data[@name='countryType']/@value = 'D'">
							<a tabindex="-1" href="{$categories1}" class="tblheader">Categories</a>&nbsp;>
						</xsl:if>
						<xsl:if test= "$searchType != 'simpleproductsearch'">					
							<a tabindex="-1" href="{$productlist1}" class="tblheader">Product List</a>&nbsp;>
						</xsl:if>
						Upsell Detail
					</xsl:if>
				</TD>
			</TR>
			
		</TABLE>
		</xsl:when>
		<xsl:otherwise>
		
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
						<TR><td>&nbsp;</td></TR>
						<TR><td><center>This product is unavailable.</center></td></TR>
						<TR><td>&nbsp;</td></TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		
		</xsl:otherwise>
		</xsl:choose>
				
		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD> &nbsp; <SPAN style="color:red"> *** - required field</SPAN> </TD>
     		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
		</TABLE>
		</CENTER>
	</FORM>	
		
	<DIV id="showLargeImage" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<center>
			<br></br>
			<br></br>
			<SCRIPT language="JavaScript">
			 var lpid = "<xsl:value-of select="/root/upsellMaster/upsellDetail[@name='masterId']/@value"/>";
			<![CDATA[
		 	document.write("<img ")
 			document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
			document.write("if(imag != \'npi_2.jpg\'){ ")
			document.write("this.src = \'../images/npi_2.jpg\'}\" ")
			document.write("src=\"../images/" + lpid + ".jpg\"> ")
			document.write("</img>")
			]]>
			</SCRIPT>
			<br></br>
			<br></br>
			<a href="javascript:closeLargeImage();">close</a>
		</center>
	</DIV>
	
<!-- beginning of zipcode lookup DIV -->
	
	<DIV id="zipCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
	<center>
		<DIV id="zipCodeLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
	</DIV>
	</center>

	<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD colspan="3" align="left">

					<font face="Verdana"><strong>Zip/Postal Code Lookup </strong></font>
	            </TD>

	        </TR>
	        
	        <TR>
	            <TD nowrap="true" width="50%" class="label" align="right"> City: </TD>
	            <TD width="50%">
	                <INPUT tabindex ="22" onkeydown="javascript:EnterToZipPopup()" class="TblText" type="text" name="cityInput" size="20" maxlength="50" value=""/>
	            </TD>
	        </TR>
	        
	        <TR>
	            <TD nowrap="true" width="50%" class="label" align="right"> State: </TD>
	            <TD width="50%">	  			
		  			<xsl:choose>
		  			<xsl:when test="/root/orderData/data[@name = 'country']/@value = 'US'">
		  				<SELECT tabindex ="23" onkeydown="javascript:EnterToZipPopup()" class="TblText" name="stateInput">
		  					<OPTION value="" ></OPTION>
							<xsl:for-each select="/root/extraInfo/states/state[@countryCode='']">
					  	      <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
							</xsl:for-each>
						</SELECT>		  			
		  			</xsl:when>		
		  			<xsl:when test="/root/orderData/data[@name = 'country']/@value = 'CA'">
		  				<SELECT tabindex ="23" onkeydown="javascript:EnterToZipPopup()" class="TblText" name="stateInput">
		  					<OPTION value="" ></OPTION>
							<xsl:for-each select="/root/extraInfo/states/state[@countryCode='CAN']">
					  	      <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
							</xsl:for-each>
						</SELECT>
		  			</xsl:when>
		  			<xsl:otherwise>
		                <INPUT tabindex ="23" onkeydown="javascript:EnterToZipPopup()" class="TblText" type="text" name="stateInput" size="20" maxlength="50" value=""/>	  			
					</xsl:otherwise>
		  			</xsl:choose>
	            </TD>
	        </TR>
			
			<TR>
				<TD nowrap="true" class="labelright"> Zip/Postal Code:</TD>
				<TD>
	                <INPUT tabindex ="24" onkeydown="javascript:EnterToZipPopup()" class="TblText" type="text" name="zipCodeInput" size="5" maxlength="10"/>
				</TD>
			</TR>
		    <TR>
				<TD colspan="3" align="right">
				<IMG tabindex ="25" onkeydown="javascript:EnterToZipPopup()" src="../images/button_search.gif" alt="Search" onclick="javascript:goSearchZipCode()"  />
		           	<IMG tabindex ="26" src="../images/button_close.gif" alt="Close screen" onclick="javascript:goCancelZipCode()" />
	         	</TD>
	        </TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	    </TABLE>
	</DIV>
<!-- end of zipcode lookup  DIV -->

<!--Begin No Floral DIV -->
<DIV id="noFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE>
				<TR>
					<TD align="center">Floral Items can no longer be delivered to this zip/postal code area.
						<BR></BR>
						<BR></BR>
					    Would you be interested in our specialty items?
			   		</TD>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:closeNoFloral()"></IMG>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:cancelItem()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>	
</TABLE>
</DIV>
<!--End No Floral DIV -->

<!--Begin No Product DIV -->
<DIV id="noProduct" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						No Items can be delivered to this Zip/Postal Code at this time.
						<BR></BR>
						<BR></BR>
					    Please enter a new Zip/Postal Code or select <U>Occasion</U>&nbsp; to start over.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noProduct()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Product DIV -->

<!--Begin Product Unavailable DIV -->
<DIV id="productUnavailable" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						This item cannot be delivered to this Zip/Postal Code at this time.
						<BR></BR>
						<BR></BR>
					    Please enter a new Zip/Postal Code or select another item to order.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Product Unavailable DIV -->

<!--Begin JCPenney Food DIV -->
<DIV id="jcpFood" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						We're sorry, food products are unavailable for purchase using a JCPenney credit card.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:closeJcpFoodItem()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End JCPenney Food DIV -->

<!--Begin Floral only DIV -->
<DIV id="onlyFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						Only Floral items can be delivered to this Zip/Postal Code.
						<BR></BR>
						<BR></BR>
					    Would you be interested in selecting a floral item?
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:onlyFloral()"></IMG>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:cancelItem()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Only Floral DIV -->

<!--Begin Codified Special DIV -->
<DIV id="codifiedSpecial" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						We're sorry the product you've selected is currently not available in the 
						zip code you entered.  However, the majority of our flowers can be delivered
						to this zip code.  
						<BR/><BR/>
						
						If this is an incorrect zip code, please re-enter the zip code
						below and "Click to Accept".  Otherwise, please click the "Back" button to
						return to shopping.
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"></IMG>						
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Codified Special DIV -->


<!--Begin JCP Popup DIV -->
<DIV id="JCPPop" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						At this time, JC Penney accepts orders going to or coming from the 50 United States
						only.  We can process your Order using any major credit card through FTD.com.
						<BR></BR>
						<BR></BR>
					    Would you like to proceed?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End JCPPop DIV -->


<!-- insert footer
	<xsl:call-template name="footer"/>	-->
		
<!--SERVER SIDE VALIDATION SECTION-->
<xsl:call-template name="validation"/>
	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... updating!</font> </p>
	</DIV>
	
	<DIV id="calendarLoading" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... loading!</font> </p>
	</DIV>
	
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
