<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="headerButtons.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="siteName" select="root/parameters/siteName"/>
<xsl:variable name="applicationContext" select="root/parameters/applicationContext"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE>
   FTD - Order Confirmation
</TITLE>
<SCRIPT language="JavaScript" src="../js/util.js"> </SCRIPT>
<SCRIPT type="text/javascript" language="JavaScript">
var siteName = '<xsl:value-of select="$siteName"/>';
var applicationContext = '<xsl:value-of select="$applicationContext"/>';
<![CDATA[
	function submitForm()
    	{
    		form.submit();
    	}

	document.oncontextmenu=stopIt;

    	function onKeyDown()
    	{
       		if (window.event.keyCode == 13)
    		{
    	    		submitForm();
    		}
    	}

	document.onkeydown = backKeyHandler;
]]>
</SCRIPT>

<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>
<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF">
<FORM name="form" method="get" action="http://{$siteName}{$actionServlet}">
<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
<CENTER>

<TABLE width="98%" cellspacing="0" cellpadding="0">
	<TR>
		<xsl:call-template name="header"/>
	</TR>
</TABLE>

<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
	<TR>
		<TD class="tblheader" colspan="8">
			Order Confirmation
		</TD>
	 </TR>
</TABLE>
        <!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD valign="center">
					<TABLE summary="" width="100%" border="0">
						<TR>
							<TD colspan="1" class="ScreenPrompt">
								<xsl:value-of select="/root/confirmationData/scripting/script[@fieldName='CONFIRM']/@scriptText"/>
							</TD>
						</TR>
						<TR>
							<TD colspan="1"><xsl:call-template name="customerHeader"/>&nbsp;</TD>
						</TR>
						<TR>
							<TD>
								<TABLE width="100%" height="100%">
									<TR></TR>
									<tr>
										<TD>Your confirmation number is...</TD>
									</tr>
									<TR>
										<TD width="33%" class="TblTextHeader">Confirmation #</TD>

										<xsl:if test="/root/orderData/data[@name = 'rewardType']/@value = 'Miles' or /root/orderData/data[@name = 'rewardType']/@value = 'Points'">
											<TD width="33%" class="TblTextHeader"><xsl:value-of select="root/orderData/data[@name = 'rewardType']/@value"/></TD>
										</xsl:if>

										<xsl:if test="/root/orderData/data[@name = 'giftCertificateAmount']/@value != ''">
											<TD width="33%" class="TblTextHeader">Gift Certificate</TD>
										</xsl:if>

										<TD width="33%" class="TblTextHeader">Total Price</TD>
									</TR>
									<TR>
										<TD align="center">
											<xsl:value-of select="substring(root/orderData/data[@name = 'confirmationNumber']/@value, 1,11 )"/>

										</TD>

										<xsl:if test="/root/orderData/data[@name = 'rewardType']/@value = 'Miles' or /root/orderData/data[@name = 'rewardType']/@value = 'Points'">
											<TD align="center">
												<xsl:value-of select="root/orderData/data[@name = 'rewardValue']/@value"/>
											</TD>
									   	</xsl:if>

									   	<xsl:if test="/root/orderData/data[@name = 'giftCertificateAmount']/@value != ''">
											<TD align="center"><xsl:value-of select="root/orderData/data[@name = 'giftCertificateAmount']/@value"/></TD>
										</xsl:if>

										<TD align="center">
											<xsl:value-of select="root/orderData/data[@name = 'totalPrice']/@value"/>
											<xsl:if test="/root/orderData/data[@name = 'country']/@value = 'CA'">
												&nbsp;(<xsl:value-of select="root/orderData/data[@name = 'canadianFunds']/@value"/>&nbsp;approximate Canadian)
											</xsl:if>
										</TD>
									</TR>
								</TABLE>
								<BR/>
								<BR/>
								<TABLE width="100%" height="100%">
									<TR>
										<TD width="25%" class="TblTextHeader">Product ID</TD>
										<TD width="25%" class="TblTextHeader">Product Name</TD>
										<TD width="25%" class="TblTextHeader">Recipient Name</TD>
										<TD width="25%" class="TblTextHeader">Delivery Date</TD>
									</TR>
									<xsl:for-each select="root/itemData/item">
									<TR>
										<TD align="center">
											<xsl:value-of select="@productId"/>
										</TD>
										<TD align="center">
											<xsl:value-of select="@productDescription"/>
										</TD>
										<TD align="center">
											<xsl:value-of select="@recipientName"/>
										</TD>
										<TD align="center">
											<xsl:value-of select="@date"/>
										</TD>
									</TR>
									</xsl:for-each>
								</TABLE>
								<BR/>
								<xsl:if test="/root/orderData/data[@name = 'countryType']/@value = 'I'">
									<TR>
										<TD class="instruction" colspan="3">
										   <xsl:value-of select="/root/confirmationData/scripting/script[@fieldName='CONF_INTL']/@scriptText"/>
										</TD>
									</TR>
								</xsl:if>
								<xsl:if test="/root/orderData/data[@name = 'countryType']/@value = 'D'">
									<TR>
										<TD class="instruction" colspan="3">
										   <xsl:value-of select="/root/confirmationData/scripting/script[@fieldName='CONF_DOM']/@scriptText"/>
										</TD>
									</TR>
								</xsl:if>
							</TD>
						<TR></TR>
						</TR>

						<xsl:if test="/root/confirmationData/scriptOverride/script[@fieldName = 'CASHBACK']">
							<tr><TD>
									<xsl:value-of select="/root/confirmationData/scriptOverride/script[@fieldName='CASHBACK']/@scriptText"/> $<xsl:value-of select="/root/orderData/data[@name='totalMonetaryDiscount']/@value"/> <xsl:value-of select="/root/confirmationData/scriptOverride/script[@fieldName='CASHBACK2']/@scriptText"/>
							</TD></tr>
						</xsl:if>

						<xsl:if test="/root/confirmationData/scriptOverride/script[@fieldName = 'TOTALSAVINGS']">
							<tr><TD>
									<xsl:value-of select="/root/confirmationData/scriptOverride/script[@fieldName='TOTALSAVINGS']/@scriptText"/> $<xsl:value-of select="/root/orderData/data[@name='totalMonetaryDiscount']/@value"/> <xsl:value-of select="/root/confirmationData/scriptOverride/script[@fieldName='TOTALSAVINGS2']/@scriptText"/>
							</TD></tr>
						</xsl:if>

						<xsl:choose>
						<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
							<TR>
							<TD colspan="3">
							<SPAN style="color:red"> Taxes have not been charged,
							yet there may be additional tax assessed at a later time by JC Penney </SPAN>
							</TD>
							</TR>
						</xsl:when>
						<xsl:otherwise>

						<tr><TD>
							<xsl:choose>
								<xsl:when test="root/confirmationData/scriptOverride/script[@fieldName='THANKYOU']">
									<xsl:value-of select="/root/confirmationData/scriptOverride/script[@fieldName='THANKYOU']/@scriptText"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="/root/confirmationData/scripting/script[@fieldName='THANKYOU']/@scriptText"/>
								</xsl:otherwise>
							</xsl:choose>
						</TD></tr>
						</xsl:otherwise>
						</xsl:choose>
						<TR>
							<TD align="right">
								<IMG tabindex="1" onkeydown="javascript:onKeyDown()" src="../images/button_continue.gif" width="74" height="17" onclick="submitForm()"/>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
	<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
	<TR>
		<TD class="tblheader" colspan="8">
			Order Confirmation
		</TD>
	 </TR>

    </TABLE>
	</TABLE>
	<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
		<TR><td>&nbsp;</td></TR>
		<TR>
			<xsl:call-template name="footer"/>
		</TR>
	</TABLE>
</CENTER>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>

