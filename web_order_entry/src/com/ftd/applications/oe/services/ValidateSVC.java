package com.ftd.applications.oe.services;

import com.ftd.applications.oe.common.AddressUtility;
import com.ftd.applications.oe.common.CCASType;
import com.ftd.applications.oe.common.CommandConstants;
import com.ftd.applications.oe.common.CreditCardApprovalUtility;
import com.ftd.applications.oe.common.DataConstants;
import com.ftd.applications.oe.common.FieldUtils;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.applications.oe.common.OEValidationErrorMessage;
import com.ftd.applications.oe.common.ValidateMembership;
import com.ftd.applications.oe.common.valobjs.AddressVO;
import com.ftd.applications.oe.common.valobjs.CreditCardVO;
import com.ftd.framework.businessservices.servicebusinessobjects.BusinessService;
import com.ftd.framework.common.utilities.FrameworkConstants;
import com.ftd.framework.common.utilities.LogManager;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.utilities.XMLEncoder;
import com.ftd.framework.common.utilities.XPathQuery;
import com.ftd.framework.common.valueobjects.FTDArguments;
import com.ftd.framework.common.valueobjects.FTDCommand;
import com.ftd.framework.common.valueobjects.FTDDataRequest;
import com.ftd.framework.common.valueobjects.FTDDataResponse;
import com.ftd.framework.common.valueobjects.FTDSystemVO;
import com.ftd.framework.dataaccessservices.GenericDataService;
import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.TooManyAttemptsException;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

public class ValidateSVC extends BusinessService
{
    public FTDSystemVO main(FTDCommand command, FTDArguments arguments)
    {
        FTDSystemVO validateResultVO = null;

        if((command.getCommand()).equals(CommandConstants.VALIDATE_PAGE)) {
            validateResultVO = validatePage(arguments);
        }

        return validateResultVO;
    }

    public  void initialize()
    {}

    public  String toString()
    {
        return null;
    }

    private FTDSystemVO validatePage(FTDArguments arguments)
    {
        FTDSystemVO systemVO = new FTDSystemVO();
        Iterator requestFields = arguments.keySet().iterator();
        String requestField = null;
        boolean validationOK = true;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");

        FTDSystemVO validationVO = null;
        String xpath = "root/validation";
        NodeList nl = null;
        XPathQuery q = null;
        Element validationLM = null;
        Element listElement = null;
        XMLEncoder encoder = null;
        NamedNodeMap validationAttrs = null;
        int x, y;

        while ( requestFields.hasNext() && validationOK )
        {
            requestField = (String) requestFields.next();

            try
            {
                if ( requestField.equals("DNIS") )
                {
                    validationVO = validateDnis(arguments);
                }
                else if ( requestField.equals("SOURCE_CODE") )
                {
                    validationVO = validateSourceCode(arguments);
                }
                else if ( requestField.equals("ZIPCODE") )
                {
                    validationVO = validateZipCode(arguments);
                }
                else if ( requestField.equals("GIFT_CERTIFICATE_ID") )
                {
                    validationVO = validateGiftCertificate(arguments);
                }
                else if ( requestField.equals("CREDIT_CARD_NUMBER") )
                {
                    validationVO = validateCreditCard(arguments);
                }
                else if ( requestField.equals("MEMBERSHIP_ID") )
                {
                    validationVO = validateMembershipId(arguments);
                }
                else if ( requestField.equals("QMS_ADDRESS1") )
                {
                    validationVO = validateQmsAddress(arguments);
                }
                else if ( requestField.equals("USER_FUNCTION") )
                {
                    validationVO = validateUserFunction(arguments);
                }
                else if ( requestField.equals("BILLING_INFO") )
                {
                    validationVO = validateBillingInfo(arguments);
                }
                else
                {
                    continue;
                }

                q = new XPathQuery();
                nl = q.query((XMLDocument)validationVO.getXML(), xpath);
                for ( x = 0; x < nl.getLength(); x++ )
                {
                    validationLM = (Element) nl.item(x);
                    listElement = xmlDocument.createElement("validation");
                    rootElement.appendChild(listElement);
                    validationAttrs = validationLM.getAttributes();
                    for ( y = 0; y < validationAttrs.getLength(); y++ )
                    {
                        listElement.setAttribute(validationAttrs.item(y).getNodeName(),
                                                validationAttrs.item(y).getNodeValue());
                    }
                }
            }
            catch ( Exception e )
            {
                validationOK = false;
            }
        }

        listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "VALIDATION");

        if ( validationOK )
        {
            listElement.setAttribute("result", "OK");
        }
        else
        {
            listElement.setAttribute("result", "ERROR");
        }

        XMLDocument document = XMLEncoder.createXMLDocument("validatePage");
        XMLEncoder.addSection(document, rootElement.getChildNodes());
        try
        {
            systemVO.setXML(document);
        }
        catch ( Exception e )
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
    }

    private FTDSystemVO validateDnis(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;
        FTDDataRequest dataRequest = new FTDDataRequest();

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "DNIS");

        try
        {
            dataRequest.addArgument("dnisId", (String) arguments.get("DNIS"));

            GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_DNIS);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            NodeList nl = null;
            String xpath = "validateDnis/dnisInformation/dnis";
            XPathQuery q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
            Element dnisLM = (Element) nl.item(0);

            /* If the DNIS was not found, or it is inactive. */
            if ( (dnisLM == null) || (! dnisLM.getAttribute("statusFlag").equals("A")) )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.DNIS_Invalid);
            }
            /* Otherwise if the DNIS is a customer service DNIS. */
            else if ( ! dnisLM.getAttribute("oeFlag").equals("Y") )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.DNIS_Customer_Service);
            }
            else
            {
                listElement.setAttribute("result", "OK");
            }
        }
        catch(Exception e)
        {
            listElement.setAttribute("result", OEValidationErrorMessage.DNIS_Invalid);
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateDnisPage");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private FTDSystemVO validateSourceCode(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;
        String      dnisType = null;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "SOURCE_CODE");

        try
        {
            dnisType = FieldUtils.translateNullToString((String) arguments.get("SOURCE_CODE_DNIS_TYPE"));

            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("sourceCode", (String) arguments.get("SOURCE_CODE"));

            GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_SOURCE_CODE);
            FTDDataResponse dataResponse = service.execute(dataRequest);

            NodeList nl = null;
            String xpath = "validateSourceCode/sourceCodeCheck/sourceCode";
            XPathQuery q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
            Element sourceCodeLM = (Element) nl.item(0);

            if ( sourceCodeLM == null )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Introduction_SourceCode);
            }
            else if ( sourceCodeLM.getAttribute("expiredFlag").equals("Y") )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Introduction_SourceCode_Expired);
            }
            else if ( ( dnisType.equals("JP") && ! sourceCodeLM.getAttribute("jcPenneyFlag").equals("Y") )
                    || ( ! dnisType.equals("JP") && sourceCodeLM.getAttribute("jcPenneyFlag").equals("Y") ) )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Introduction_SourceCode_DNIS);
            }
            // ** SFMB **
            else if((dnisType.equals("SF") && !sourceCodeLM.getAttribute("sfmbFlag").equals("Y")) || (!dnisType.equals("SF") && sourceCodeLM.getAttribute("sfmbFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            // ** Gift Site **
            else if((dnisType.equals("GS") && !sourceCodeLM.getAttribute("giftFlag").equals("Y")) || (!dnisType.equals("GS") && sourceCodeLM.getAttribute("giftFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            // ** High End Site **
            else if((dnisType.equals("HI") && !sourceCodeLM.getAttribute("highFlag").equals("Y")) || (!dnisType.equals("HI") && sourceCodeLM.getAttribute("highFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            // ** Gift Store Card **
            else if((dnisType.equals("SC") && !sourceCodeLM.getAttribute("gscFlag").equals("Y")) || (!dnisType.equals("SC") && sourceCodeLM.getAttribute("gscFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            // ** Flowers USA **
            else if((dnisType.equals("FS") && !sourceCodeLM.getAttribute("fusaFlag").equals("Y")) || (!dnisType.equals("FS") && sourceCodeLM.getAttribute("fusaFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            // ** Florist Direct **
            else if((dnisType.equals("FD") && !sourceCodeLM.getAttribute("fdirectFlag").equals("Y")) || (!dnisType.equals("FD") && sourceCodeLM.getAttribute("fdirectFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            // ** Florist.com **
            else if((dnisType.equals("FL") && !sourceCodeLM.getAttribute("floristcomFlag").equals("Y")) || (!dnisType.equals("FL") && sourceCodeLM.getAttribute("floristcomFlag").equals("Y")))
                listElement.setAttribute("result", "Invalid Source Code for DNIS");
            else
            {
                listElement.setAttribute("result", "OK");
            }
        }
        catch(Exception e)
        {
            listElement.setAttribute("result", "Invalid Source Code");
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateSourceCode");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private FTDSystemVO validateUserFunction(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;
        com.ftd.security.SecurityManager security = com.ftd.security.SecurityManager.getInstance();

        LogManager logger = this.getLogManager();

        String token = null;

        String[] roleFunctions = null;
        boolean functionFound = false;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "USER_FUNCTION");

        try
        {
            String userFunction = (String) arguments.get("USER_FUNCTION");
            String sessionId = (String) arguments.get("USER_FUNCTION_SESSION_ID");
            String userName = (String) arguments.get("USER_FUNCTION_USER_NAME");
            String userPassword = (String) arguments.get("USER_FUNCTION_USER_PASSWORD");

            ResourceManager rm = ResourceManager.getInstance();

            //get the resource and permission as needed by OSP
            String resource = null;
            String permission = null;
            if(userFunction != null)
            {
              if(userFunction.equals("NOCHARGEAPPRV"))
              {
                resource = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "security.nocharge.resource");
                permission = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "security.nocharge.permission");                
              }
              else
              {
                logger.error("Unknown function name:" + userFunction);
              }
            }
            else
            {
              logger.error("Null user function method.");
            }

            String context = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, GeneralConstants.SECURITY_CONTEXT_OPS);
            String unitID = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, GeneralConstants.SECURITY_UNIT_ID);

            //authenticate user
            if ( userName != null & userPassword != null )
            {
                // Get certificate by authenticating username / password

                try{
                  Object obj = security.authenticateIdentity(context,unitID,userName, userPassword);
                  if(obj != null){
                    token = (String)obj;
                  }
                }
                catch(AuthenticationException ae)
                {
                  logger.error("AuthenticationException");
                }
                catch(ExpiredIdentityException eie)
                {
                  logger.error("ExpiredIdentityException");                  
                }
                catch(ExpiredCredentialsException exe)
                {
                  logger.error("ExpiredCredentialsException");                  
                }
                catch(TooManyAttemptsException tmae)
                {
                  logger.error("TooManyAttemptsException");                  
                }
                catch(Exception e)
                {
                  logger.error(e);
                  e.printStackTrace();
                }
                
            }

            // Check the user's permitted functions if they authenticated
            if ( token != null && token.length() > 0)
            {
                //check if user has the authority to do this stuff

                try{
                  functionFound = security.assertPermission(context,token,resource,permission);
                }
                catch(Exception e)
                {
                  logger.error("Error during assert.");
                  e.printStackTrace();
                }                
                if(!functionFound)
                {
                  logger.error("User not authorized.  User Id=" + userName);
                  logger.error("Attempted to access resource:" + resource + " permission:" + permission);
                }

            }
            else
            {
              logger.error("Security token is null.  User not authenticated.  User Id=" + userName);
              logger.error("Attempted to access resource:" + resource + " permission:" + permission);
            }

            if ( functionFound )
            {
                listElement.setAttribute("result", "OK");
            }
            else
            {
                listElement.setAttribute("result", OEValidationErrorMessage.User_Function_Denied);
            }

            XMLDocument document = XMLEncoder.createXMLDocument("validateUserFunction");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            listElement.setAttribute("result", OEValidationErrorMessage.User_Function_Denied);
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateUserFunction");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private FTDSystemVO validateZipCode(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;
        boolean cityStateCheck = false;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "ZIPCODE");

        try
        {
            String city = (String) arguments.get("CITY");
            String state = (String) arguments.get("STATE");

            // Validate against the first 5 characters of the zipcode, UPPERCASED
            String cleanZip = ((String) arguments.get("ZIPCODE")).toUpperCase();
            if ( cleanZip.length() > 5 )
            {
                cleanZip = cleanZip.substring(0,5);
            }

            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("zipCode", cleanZip);
            dataRequest.addArgument("city", city);
            dataRequest.addArgument("state", state);

            if ( city != null || state != null )
            {
                cityStateCheck = true;
            }

            GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_ZIP_CODE);
            FTDDataResponse dataResponse = service.execute(dataRequest);

            NodeList nl = null;
            String xpath = "validateZipCode/zipCodeCheck/zipCode";
            XPathQuery q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
            Element zipCodeLM = (Element) nl.item(0);

            // Let the Canadians pass if LNL format
            if ( zipCodeLM == null && (!Character.isLetter(cleanZip.charAt(0))
                                        || !Character.isDigit(cleanZip.charAt(1))
                                        || !Character.isLetter(cleanZip.charAt(2))))
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Zipcode_Invalid);
            }
            else if ( cityStateCheck &&
                   (( city != null && ! city.equalsIgnoreCase(zipCodeLM.getAttribute("city"))) ||
                    ( state != null && ! state.equalsIgnoreCase(zipCodeLM.getAttribute("state")))))
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Zipcode_Match);
            }
            else
            {
                listElement.setAttribute("result", "OK");

                /* Zip code is valid, also determine whether zip is presently
                   blocked under GNADD. */
                if ( ( zipCodeLM.getAttribute("gnaddLevel").equals("0") ) &&
                     ( zipCodeLM.getAttribute("zipGnaddFlag").equals(GeneralConstants.YES) ))
                {
                    listElement.setAttribute("blocked", GeneralConstants.YES);
                }
                else
                {
                    listElement.setAttribute("blocked", GeneralConstants.NO);
                }
            }
        }
        catch(Exception e)
        {
            listElement.setAttribute("blocked", GeneralConstants.NO);
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateZipCode");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private FTDSystemVO validateGiftCertificate(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;
        double orderTotalAmount;
        double maxAmount;
        double certificateAmount = 0;
        double adjustedTotal = 0;
        double adjustmentAmount = 0;
        boolean validCertificate = false;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "GIFT_CERTIFICATE_ID");

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("giftCertificateId", (String) arguments.get("GIFT_CERTIFICATE_ID"));
            String companyId = (String) arguments.get("COMPANY_ID");
            // We only want to specify companyId in query if Gift Store Card.  All other gift
            // certificate values apply to all other companies so use null instead. 
            if (! GeneralConstants.COMPANY_ID_GSC.equals(companyId)) {  
              companyId = null;
            }
            dataRequest.addArgument("companyId", companyId);

            GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_GIFT_CERTIFICATE);
            FTDDataResponse dataResponse = service.execute(dataRequest);

            NodeList nl = null;
            String xpath = "validateGiftCertificate/giftCertificateCheck/giftCertificate";
            XPathQuery q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
            Element giftCertLM = (Element) nl.item(0);

            if ( giftCertLM == null )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Invalid);
            }
            else if ( giftCertLM.getAttribute("redemptionFlag").equals("Y") )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Redeemed);
            }
            else
            {
                java.util.Date expireDate = FieldUtils.formatStringToUtilDate(giftCertLM.getAttribute("expirationDate"));
                java.util.Date currentDate = FieldUtils.formatStringToUtilDate(FieldUtils.formatUtilDateToString(GregorianCalendar.getInstance().getTime()));
                if (( expireDate == null) || (currentDate.compareTo(expireDate) <= 0 ))
                {
                    // Get the order total amount and max gift certificate allowed amount
                    orderTotalAmount = Double.parseDouble((String) arguments.get("GIFT_ORDER_TOTAL_AMOUNT"));
                    maxAmount = Double.parseDouble((String) arguments.get("GIFT_MAX_AMOUNT"));

                    // Get the amount of the gift certificate
                    certificateAmount = Double.parseDouble(giftCertLM.getAttribute("certificateAmount"));

                    // If the amount is greater than the max amount allowed, apply the max amount,
                    // otherwise apply the certificate amount
                    adjustmentAmount = Math.min(certificateAmount, maxAmount);
                    adjustedTotal = orderTotalAmount - adjustmentAmount;

                    // If the adjusted order total is less than zero, make it zero
                    adjustedTotal = Math.max(adjustedTotal, 0);

                    // If a balance still remains and this is a Gift Store Card, then this is an error
                    if ((adjustedTotal > 0) && (GeneralConstants.COMPANY_ID_GSC.equals(companyId))) {  
                      listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Insufficient);
                    } else {
                      listElement.setAttribute("result", "OK");
                      validCertificate = true;
                    }
                }
                else
                {
                    listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Expired);
                }
            }

            if ( validCertificate )
            {
                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "GIFT_ADJUSTED_TOTAL");
                listElement.setAttribute("result", FieldUtils.formatDoubleNoRound(adjustedTotal));

                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "GIFT_CERTIFICATE_AMOUNT");
                listElement.setAttribute("result", FieldUtils.formatDoubleNoRound(certificateAmount));

                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "GIFT_DESCRIPTION");
                listElement.setAttribute("result", "Your gift certificate was applied in the amount of " +
                                            FieldUtils.formatBigDecimalAsCurrency(new BigDecimal(adjustmentAmount), 2));
            }
        }
        catch(Exception e)
        {
            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Invalid);
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateGiftCertificate");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private FTDSystemVO validateCreditCard(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = new FTDSystemVO();
        String      dnisType = null;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "CREDIT_CARD_NUMBER");

        try
        {
            LogManager lm = this.getLogManager();
            CreditCardApprovalUtility ccas = new CreditCardApprovalUtility();
            CreditCardVO inCreditCardVo = new CreditCardVO();
            CreditCardVO outCreditCardVo = inCreditCardVo;
            String creditCardNumber;
            String creditCardType;

            creditCardNumber = (String) arguments.get("CREDIT_CARD_NUMBER");
            creditCardType = (String) arguments.get("CREDIT_CARD_TYPE");
            String creditCardExpDate = (String) arguments.get("CREDIT_CARD_EXPIRATION");

            dnisType = FieldUtils.translateNullToString((String) arguments.get("CREDIT_CARD_DNIS_TYPE"));
            // Call CCAS if credit card number passes check digit routine
            if ( new ValidateMembership().checkCreditCard(creditCardType, creditCardNumber) )
            {
                Date curDate = new Date();
                Calendar expDate = Calendar.getInstance();
                if(dnisType.equals("JP"))
                {
                    expDate.set(Calendar.YEAR, 9999);
                } else
                {
                    if (creditCardType.equals("MS"))
                    {
                        expDate.set(Calendar.YEAR, 9999);
                    } else
                    {
                        if (creditCardExpDate != null)
                        {
                            int month = Integer.parseInt(creditCardExpDate.substring(2));
                            int year = Integer.parseInt(creditCardExpDate.substring(0, 2));
                            expDate.set(2, month - 1);
                            expDate.set(1, year + 2000);
                            expDate.set(5, 31);
                        }
                    }
                }
                // Check expiration date
                if(expDate.getTime().after(curDate))
                {
                    // For JCPenney DNIS type, check whether credit card validation is enabled
                    if ( dnisType.equals("JP") && ! doJcPenneyValidation() )
                    {
                        // If credit card validation not enabled, just return 'OK'
                        listElement.setAttribute("result", "OK");
                    }
                    //else if(Float.parseFloat((String) arguments.get("CREDIT_CARD_AMOUNT")) < 100)
                    // 6/1/07 Mike Kruger - Allow AMEX transactions under 25 cents to authorize
                    else if(Float.parseFloat((String) arguments.get("CREDIT_CARD_AMOUNT")) < GeneralConstants.CC_AUTH_THRESHOLD_PRICE
                            && !creditCardType.equals(GeneralConstants.CC_AMEX))
                    {
                        // Do not validate if less than $0.25 and the credit card is not AMEX
                        listElement.setAttribute("result", "OK");
                    }
                    else
                    {
                        inCreditCardVo.setCreditCardNumber((String) arguments.get("CREDIT_CARD_NUMBER"));
                        inCreditCardVo.setAddressLine((String) arguments.get("CREDIT_CARD_ADDRESS"));
                        inCreditCardVo.setExpirationDate((String) arguments.get("CREDIT_CARD_EXPIRATION"));
                        inCreditCardVo.setAmount((String) arguments.get("CREDIT_CARD_AMOUNT"));
                        inCreditCardVo.setZipCode((String) arguments.get("CREDIT_CARD_ZIP"));

                        // Call JCPenney credit card authorization for JCPenney DNIS type
                        if ( dnisType.equals("JP") )
                            outCreditCardVo = ccas.purchaseRequest(inCreditCardVo,CCASType.TYPE_JCP);
                        //8/1/03 ed mueller....added SFMB credit card processing.
                        else if(dnisType.equals("SF"))
                            outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_SFMB);
                        //11/4/03 tim peterson...added Gift Site processing
                        else if(dnisType.equals("GS")) 
                            outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_GIFT);
                        //02/19/04 ivan vojinovic...added High End processing
                        else if(dnisType.equals("HI")) 
                            outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_HIGH);
                        //09/29/2005 Tim Peterson...added Flowers USA
                        else if(dnisType.equals("FS"))  
                            outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_FUSA);
                        //09/29/2005 Tim Peterson...added Florist Direct
                        else if(dnisType.equals("FD")) 
                            outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_FDIRECT);
                        //09/29/2005 Tim Peterson...added Florist.COM
                        else if(dnisType.equals("FL"))   
                            outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_FLORIST);
                        else if(creditCardType.equals("MS"))
                            outCreditCardVo = ccas.aafesPurchaseRequest(inCreditCardVo);
                        else
                            outCreditCardVo = ccas.purchaseRequest(inCreditCardVo, CCASType.TYPE_FTD);

                        this.getLogManager().info("CREDIT CARD AUTHORIZATION STATUS: " + outCreditCardVo.getStatus());
                        this.getLogManager().info("CREDIT CARD ACTION CODE         : " + outCreditCardVo.getActionCode());

                        /* Status of S and action code one of '000','0NC','0NE','094'
                           indicates credit card approval. */
                        if (  outCreditCardVo.getStatus().equals("S") &&
                             (outCreditCardVo.getActionCode().equals("000") ||
                              outCreditCardVo.getActionCode().equals("0NC") ||
                              outCreditCardVo.getActionCode().equals("0NE") ||
                              outCreditCardVo.getActionCode().equals("094")) )
                        {
                            listElement.setAttribute("result", "OK");
                        }
                        /* Status other than 'S' indicates a system error with credit auth. */
                        else if ( !outCreditCardVo.getStatus().equals("S") && !creditCardType.equals("MS"))
                        {
                            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card_Error);
                        }
                        /* Action code '999' indicates decline. */
                        else if ( outCreditCardVo.getActionCode().equals("999") )
                        {
                            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card);
                        }
                        /* Action code of '800' or '500' indicates call card issuer. */
                        else if ( outCreditCardVo.getActionCode().equals("800") ||
                                outCreditCardVo.getActionCode().equals("500") )
                        {
                            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card_Pickup);
                        }
                        /* Action code '700' indicates invalid amount. */
                        else if ( outCreditCardVo.getActionCode().equals("700") )
                        {
                            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card_Amount);
                        }
                        /* Action code '600' indicates expired or invalid date. */
                        else if ( outCreditCardVo.getActionCode().equals("600") )
                        {
                            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card_Date);
                        }
                        // AAFES return codes
                        else if(outCreditCardVo.getStatus().equals("D"))
                        {
                            // AAFES CARD DECLINED
                            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card);
                        }
                        else if(outCreditCardVo.getStatus().equals("X"))
                        {
                            // AAFES CARD ERROR
                            listElement.setAttribute("result", "We currently are unable to receive authorization from AAFES on your Military Star Card due to technical difficulties.  Would you like to use a different credit card for payment?");
                        }
                        else if(outCreditCardVo.getStatus().equals("A"))
                        {
                            // AAFES CARD ACCEPTED
                            listElement.setAttribute("result", "OK");
                            // Set the verbiage to AP
                            outCreditCardVo.setVerbiage("AP");
                        }
                    }
                    listElement = xmlDocument.createElement("validation");
                    rootElement.appendChild(listElement);
                    listElement.setAttribute("object", "CREDIT_CARD_EXP");
                    listElement.setAttribute("result", "OK");
                }
                else
                {
                    // Card is expired
                    listElement.setAttribute("result", "OK");
                    listElement = xmlDocument.createElement("validation");
                    rootElement.appendChild(listElement);
                    listElement.setAttribute("object", "CREDIT_CARD_EXP");
                    listElement.setAttribute("result", "Invalid expiration date");
                }
            }
            else
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card_Invalid);
                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "CREDIT_CARD_EXP");
                listElement.setAttribute("result", "OK");
            }

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_APPROVAL_CODE");
            listElement.setAttribute("result", FieldUtils.translateNullToString(outCreditCardVo.getApprovalCode()));

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_AVS_RESULT");
            listElement.setAttribute("result", FieldUtils.translateNullToString(outCreditCardVo.getAVSIndicator()));

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_APPROVAL_AMOUNT");
            if ( outCreditCardVo.getApprovedAmount() == null )
            {
                listElement.setAttribute("result", "");
            }
            else
            {
                listElement.setAttribute("result",
                        FieldUtils.formatDoubleNoRound(
                        Double.parseDouble(outCreditCardVo.getApprovedAmount()) / 100 ));
            }

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_ACQ_REFERENCE_DATA");
            listElement.setAttribute("result", FieldUtils.translateNullToString(outCreditCardVo.getAcquirerReferenceData()));

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_APPROVAL_VERBIAGE");
            listElement.setAttribute("result", FieldUtils.translateNullToString(outCreditCardVo.getVerbiage()));

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_ACTION_CODE");
            listElement.setAttribute("result", FieldUtils.translateNullToString(outCreditCardVo.getActionCode()));
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Credit_Card_Fatal);
            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "CREDIT_CARD_EXP");
            listElement.setAttribute("result", "OK");
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateCreditCard");
            XMLEncoder.addSection(document, rootElement.getChildNodes());
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private boolean doJcPenneyValidation() throws Exception
    {
        FTDDataRequest dataRequest = new FTDDataRequest();
        GenericDataService service = this.getGenericDataService(DataConstants.SHOPPING_GET_GLOBAL_PARMS);
        FTDDataResponse dataResponse = service.execute(dataRequest);

        NodeList nl = null;
        String xpath = "globalParmsDataSet/globalParmsData/data";
        XPathQuery q = new XPathQuery();
        nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
        Element globalParmsLM = (Element) nl.item(0);

        if ( globalParmsLM.getAttribute("checkJcPenney").equals("Y") )
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    private FTDSystemVO validateMembershipId(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "MEMBERSHIP_ID");

        try
        {
            String membershipId = (String) arguments.get("MEMBERSHIP_ID");

            // Get the partner ID stripping off any trailing digits
            StringBuffer sb = new StringBuffer((String) arguments.get("PARTNER_ID"));
            while ( Character.isDigit(sb.charAt(sb.length()-1)) )
            {
                sb.setLength(sb.length()-1);
            }
            String partnerId = sb.toString();

            boolean validMembershipId = true;

            if ( partnerId.equals("AAA") )
            {
                // 11/17/02 JBozarth - Changed to check DB table
                validMembershipId = validateAAAMembershipId(membershipId);
            }
            else if ( partnerId.equals("AIRC") )
            {
                validMembershipId = new ValidateMembership().checkAirCanada(membershipId);
            }
            else if ( partnerId.equals("ALAK") )
            {
                validMembershipId = new ValidateMembership().checkAlaskaAir(membershipId);
            }
            else if ( partnerId.equals("AADV") )
            {
                validMembershipId = new ValidateMembership().checkAmericanAirlinesAdvantage(membershipId);
            }
            else if ( partnerId.equals("AMER") || partnerId.equals("AMER3"))
            {
                validMembershipId = new ValidateMembership().checkAmericaWest(membershipId);
            }
            else if ( partnerId.equals("ATA") )
            {
                validMembershipId = new ValidateMembership().checkATA(membershipId);
            }
            else if ( partnerId.equals("BAY") )
            {
                validMembershipId = new ValidateMembership().checkBaymont(membershipId);
            }
            else if ( partnerId.equals("BEST") )
            {
                validMembershipId = new ValidateMembership().checkBestWestern(membershipId);
            }
            else if ( partnerId.equals("CEND") )
            {
                validMembershipId = new ValidateMembership().checkCendantHotels(membershipId);
            }
            else if ( partnerId.equals("CONT") )
            {
                validMembershipId = new ValidateMembership().checkContinentalAirlines(membershipId);
            }
            else if ( partnerId.equals("DELT") )
            {
                validMembershipId = new ValidateMembership().checkDelta(membershipId);
            }
            else if ( partnerId.equals("GP") )
            {
                validMembershipId = new ValidateMembership().checkGoldPoints(membershipId);
            }
            else if ( partnerId.equals("HAWA") )
            {
                validMembershipId = new ValidateMembership().checkHawaiian(membershipId);
            }
            else if ( partnerId.equals("KORA") )
            {
                /* Per requirements, we will be accepting any id.  Method created
                 * for possible future changes.
                 */
                validMembershipId = true; //new ValidateMembership().checkKoreanAir(membershipId);
            }
            else if ( partnerId.equals("MIDW") )
            {
                validMembershipId = new ValidateMembership().checkMidwestExpress(membershipId);
            }
            else if ( partnerId.equals("NWES") )
            {
                validMembershipId = new ValidateMembership().checkNorthwest(membershipId);
            }
            else if ( partnerId.equals("SPRT") )
            {
                validMembershipId = new ValidateMembership().checkSprint(membershipId);
            }
            else if ( partnerId.equals("SWAIR") )
            {
                validMembershipId = new ValidateMembership().checkSouthwest(membershipId);
            }
            else if ( partnerId.equals("UNIT") )
            {
                validMembershipId = new ValidateMembership().checkUnited(membershipId);
            }
            else if ( partnerId.equals("USAIR") )
            {
                validMembershipId = new ValidateMembership().checkUsAir(membershipId);
            }
            else if ( partnerId.equals("VIRG") )
            {
                validMembershipId = new ValidateMembership().checkVirginAir(membershipId);
            }

            if ( validMembershipId )
            {
                listElement.setAttribute("result", "OK");
            }
            else
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Customer_ID);
            }
        }
        catch(Exception e)
        {
            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Customer_ID);
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateMembershipId");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private boolean validateAAAMembershipId(String membershipId) throws Exception
    {
        // Validate the id is the valid length
        membershipId = membershipId.trim();
        if (membershipId.length() != 6)
            return false;

        // Validate the id is numeric
        try{
            Integer.parseInt(membershipId);
        }catch(NumberFormatException e){
            return false;
        }

        // Check the AAA member ID against the database
        FTDDataRequest dataRequest = new FTDDataRequest();
        dataRequest.addArgument("aaaMemberId", membershipId);

        GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_AAA);
        FTDDataResponse dataResponse = service.execute(dataRequest);

        NodeList nl = null;
        String xpath = "validateAaa/aaaCheck/aaa";
        XPathQuery q = new XPathQuery();
        nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
        Element sourceCodeLM = (Element) nl.item(0);

        if ( sourceCodeLM == null )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
        
    private boolean validateCostCenterId(FTDArguments arguments, String a_partnerId, String a_sourceCode) throws Exception
    {
        StringBuffer costCenterId = new StringBuffer();

        for ( int i = 0; i < 10; i++ ) 
        {
            if ( (arguments.get("promptValue" + i) != null) && 
                 (!((String) arguments.get("promptValue" + i)).equals("")) ) 
            {
                costCenterId.append((String) arguments.get("promptValue" + i));
            }
            else 
            {
                break;
            }
        }

        FTDDataRequest dataRequest = new FTDDataRequest();
        dataRequest.addArgument("partnerId", a_partnerId);
        dataRequest.addArgument("sourceCodeId", a_sourceCode);
        dataRequest.addArgument("costCenterId", costCenterId.toString());

        GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_COST_CENTER);
        FTDDataResponse dataResponse = service.execute(dataRequest);

        NodeList nl = null;
        String xpath = "validateCostCenter/costCenterCheck/costCenter";
        XPathQuery q = new XPathQuery();
        nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
        Element costCenterElement = (Element) nl.item(0);

        if ( costCenterElement == null )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private FTDSystemVO validateBillingInfo(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "BILLING_INFO_ID");

        try
        { 
            if(arguments.get("PARTNER_ID") != null && ((String) arguments.get("PARTNER_ID")).length() > 0)
            {
                StringBuffer sb = new StringBuffer((String) arguments.get("PARTNER_ID"));
                while ( Character.isDigit(sb.charAt(sb.length()-1)) )
                {
                    sb.setLength(sb.length()-1);
                }
                String partnerId = sb.toString();

                boolean validBillingInfo = true;
            
                if (partnerId.equals(GeneralConstants.ADVO_CODE))
                {
                    validBillingInfo = validateCostCenterId(arguments, 
                                                                GeneralConstants.ADVO_CODE,
                                                                null);
                } else if (partnerId.equals(GeneralConstants.TARGET_CODE)) {
                    validBillingInfo = validateCostCenterId(arguments, 
                                                                GeneralConstants.TARGET_CODE,
                                                                (String) arguments.get("SOURCE_CODE_ID"));
                }

                if (validBillingInfo)
                {
                    listElement.setAttribute("result", "OK");
                }
                else
                {
                    listElement.setAttribute("result", OEValidationErrorMessage.Billing_Cost_Center);
                }
            }
            else
            {
                listElement.setAttribute("result", "OK");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            listElement.setAttribute("result", "OK");
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateBillingInfoId");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
    }

    private FTDSystemVO validateQmsAddress(FTDArguments arguments) throws Exception
    {
        FTDSystemVO systemVO = null;
        AddressVO qmsAddress = new AddressVO();
        AddressUtility qmsProcess = new AddressUtility();
        String matchCode = null;
        int matchError;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);

        try
        {
            /* 11/12 JBozarth.  Replace '#' with space in address lines. */
            qmsAddress.setFirmName((String) arguments.get("QMS_FIRM_NAME"));
            qmsAddress.setAddressLine1(((String) arguments.get("QMS_ADDRESS1")).replace('#',' '));
            qmsAddress.setAddressLine2(((String) arguments.get("QMS_ADDRESS2")).replace('#',' '));
            qmsAddress.setCity((String) arguments.get("QMS_CITY"));
            qmsAddress.setState((String) arguments.get("QMS_STATE"));
            qmsAddress.setZip10((String) arguments.get("QMS_ZIP"));
            qmsAddress.setDeliveryMethod((String) arguments.get("QMS_DELIVERY_METHOD"));

            qmsAddress = qmsProcess.processAddress(qmsAddress);

            listElement.setAttribute("object", "QMS_ADDRESS");

            String result = validateQmsFlorist(arguments);
            if(result.equals("error"))
            {
                listElement.setAttribute("result", "Florist does not exist in the database as a valid florist");
            }
            else
            {
                /* Most QMS errors are indicated by matchCode prefix 'E' */
                matchCode = qmsAddress.getMatchCode();
                if ( matchCode.startsWith("E") )
                {
                    matchError = Integer.parseInt(matchCode.substring(2,4));
                    if ( matchError == 0 || ( matchError >= 20 && matchError <= 26 ))
                    {
                        listElement.setAttribute("result", OEValidationErrorMessage.QMS_Address_Invalid);
                    }
                    else if ( matchError == 10 )
                    {
                        listElement.setAttribute("result", OEValidationErrorMessage.QMS_City_State_Invalid);
                    }
                    else if ( matchError == 11 )
                    {
                        listElement.setAttribute("result", OEValidationErrorMessage.QMS_Zip_Invalid);
                    }
                    else if ( matchError == 12 || matchError == 13 )
                    {
                        listElement.setAttribute("result", OEValidationErrorMessage.QMS_City_Invalid);
                    }
                    else
                    {
                        listElement.setAttribute("result", OEValidationErrorMessage.QMS_Error);
                    }
                }
                else if ( !matchCode.equals(GeneralConstants.QMS_MATCH_CODE_QMS_DOWN) &&
                           qmsAddress.getUSPSRangeRecordType().equals("H") &&
                            ( qmsAddress.getLowUnitNumber().equals("") ||
                              qmsAddress.getHighUnitNumber().equals("") ))
                {
                    listElement.setAttribute("result", OEValidationErrorMessage.QMS_Unit_Invalid);
                }
                else if ( matchCode.startsWith("S1") || matchCode.startsWith("S3") ||
                        matchCode.startsWith("S5") || matchCode.startsWith("S7") ||
                        matchCode.startsWith("S9") || matchCode.startsWith("SB") ||
                        matchCode.startsWith("SD") || matchCode.startsWith("SF") )
                {
                    listElement.setAttribute("result", OEValidationErrorMessage.QMS_Zip_Changed);
                }
                else if ( matchCode.equals("S00") || 
                          matchCode.equals("S80") || 
                          matchCode.equals(GeneralConstants.QMS_MATCH_CODE_QMS_DOWN) )
                {
                    listElement.setAttribute("result", "OK");
                }
                else
                {
                    listElement.setAttribute("result", OEValidationErrorMessage.QMS_Address_Changed);
                }
            }

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_MATCH_CODE");
            listElement.setAttribute("result", qmsAddress.getMatchCode());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_FIRM_NAME");
            listElement.setAttribute("result", qmsAddress.getFirmName());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_LATITUDE");
            listElement.setAttribute("result", qmsAddress.getLatitude());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_LONGITUDE");
            listElement.setAttribute("result", qmsAddress.getLongitude());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_RANGE_RECORD_TYPE");
            listElement.setAttribute("result", qmsAddress.getUSPSRangeRecordType());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_ADDRESS1");
            listElement.setAttribute("result", qmsAddress.getAddressLine1());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_ADDRESS2");
            listElement.setAttribute("result", qmsAddress.getAddressLine2());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_CITY");
            listElement.setAttribute("result", qmsAddress.getCity());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_STATE");
            listElement.setAttribute("result", qmsAddress.getState());

            listElement = xmlDocument.createElement("validation");
            rootElement.appendChild(listElement);
            listElement.setAttribute("object", "QMS_ZIP");
            listElement.setAttribute("result", qmsAddress.getZip10());

            XMLDocument document = XMLEncoder.createXMLDocument("validateQmsAddress");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        return systemVO;
    }

    private String validateQmsFlorist(FTDArguments arguments) throws Exception
    {
        FTDDataRequest dataRequest = new FTDDataRequest();
        String result = "OK";

        try
        {
            String floristId = (String) arguments.get("QMS_FLORIST");
            if( (floristId != null) && (!floristId.equals("")) )
            {
                dataRequest.addArgument("productId", (String) arguments.get("QMS_PRODUCT"));
                dataRequest.addArgument("floristId", floristId);

                GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_FLORIST);
                FTDDataResponse dataResponse = service.execute(dataRequest);

                NodeList nl = null;
                String xpath = "validateFlorist/floristInformation/florist";
                XPathQuery q = new XPathQuery();
                nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
                Element floristLM = (Element) nl.item(0);

                /* If the Florist was not found. */
                if (floristLM == null)
                {
                    result = "error";
                }
            }
        }
        catch(Exception e)
        {
            result = "error";
        }

        return result;
    }
}
