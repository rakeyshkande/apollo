package com.ftd.applications.oe.services;

import com.ftd.framework.businessservices.servicebusinessobjects.*;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.*;
import com.ftd.applications.oe.common.persistent.*;
import com.ftd.applications.oe.services.utilities.*;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.w3c.dom.*;
import java.math.*;

import java.sql.Connection;

import java.util.*;
import java.text.*;
import oracle.xml.parser.v2.*;

public class ShoppingSVC extends BusinessService
{
	public FTDSystemVO main(FTDCommand command, FTDArguments arguments)
	{
        FTDSystemVO shoppingVO = null;

        if((command.getCommand()).equals(CommandConstants.SHOPPING_SET_INTRODUCTION)) {
			shoppingVO = setIntroduction(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_UPDATE_OCCASION)) {
			shoppingVO = updateOccasion(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_SET_OCCASION)) {
            shoppingVO = setOccasion(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_ADD_ITEM)) {
            addItemToCart(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_ADD_CUSTOM_ITEM)) {
            addCustomItemToCart(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_ADD_DUPLICATE_ITEM)) {
            addDuplicateItemToCart(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_REMOVE_ITEM)) {
            removeItemFromCart(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_UPDATE_DELIVERY_INFO)) {
            updateDeliveryInfo(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_CANCEL_ORDER)) {
            shoppingVO = cancelOrder(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_GET_SHOPPING_CART)) {
            shoppingVO = getShoppingCart(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_LOAD_DELIVERY_INFO)) {
            shoppingVO = loadDeliveryInfo(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_LOAD_ITEM_FROM_CART)) {
            shoppingVO = getCartItemDetail(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_UPDATE_SOURCE_CODE)) {
            updateSourceCode(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_UPDATE_DNIS)) {
            updateDNIS(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_UPDATE_ITEM_INFO)) {
            shoppingVO = updateItemDetail(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_UPDATE_CUSTOM_ITEM_INFO)) {
            updateCustomItemDetail(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SHOPPING_CANCEL_ITEM)) {
            shoppingVO = setCancelItem(arguments);
        }
        else if((command.getCommand()).equals("SHOPPING_CANCEL_ITEM_TO_SHOPPING")) {
            shoppingVO = cancelItemToShopping(arguments);
        }
        else if((command.getCommand()).equals("SHOPPING_LOGOUT")) {
            logoutFromShopping(arguments);
        }
        else if((command.getCommand()).equals("SHOPPING_CLEAR_ORDER_CUSTOMER")) {
            clearOrderCustomer(arguments);
        }

        return shoppingVO;
	}

	public  void initialize()
	{}

	public  String toString()
	{
		return null;
	}

    private FTDSystemVO clearOrderCustomer(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            order.getSendToCustomer().setAddressOne("");
            order.getSendToCustomer().setAddressTwo("");
            order.getSendToCustomer().setCity("");
            order.getSendToCustomer().setCompanyName("");
            order.getSendToCustomer().setCompanyType("");
            order.getSendToCustomer().setCompanyInfo("");
            order.getSendToCustomer().setContactInfo("");
            order.getSendToCustomer().setHomePhone("");
            order.getSendToCustomer().setWorkPhone("");
            order.getSendToCustomer().setState("");
            order.getSendToCustomer().setZipCode("");

        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
    }

    private FTDSystemVO setIntroduction(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Remove expired breadcrumbs
            Iterator crumbIterator = order.getCrumbMap().keySet().iterator();
            while(crumbIterator.hasNext())
            {
                order.removeCrumbURL((String) crumbIterator.next());
            }

            order.setCrumbURL("Occasion", (String) arguments.get("url") + "&crumbFlag=Y");

            //used for retrieval when changing zip code on product detail and delievery pages
            order.setCrumbURL("occasionChangedZip", (String) arguments.get("url") + "&crumbFlag=Y");

            // Clear any previous order information
            if(arguments.get("crumbFlag") == null || ((String) arguments.get("crumbFlag")).equals("")) {
                order.setDeliveryDate(null);
                order.setDeliveryDateDisplay(null);
                order.setOccasion(null);
                order.getSendToCustomer().setZipCode(null);
                order.setYellowPagesCode(null);
                order.getSendToCustomer().setCountry("US");
            }

            // Set base info to order
            order.getBillToCustomer().setExistingFlag(Boolean.FALSE);

            if(arguments.get(ArgumentConstants.SOURCE_CODE) != null && !((String) arguments.get(ArgumentConstants.SOURCE_CODE)).equals("")) {
                order.setSourceCode((String) arguments.get(ArgumentConstants.SOURCE_CODE));
            }
            if(arguments.get(ArgumentConstants.CONTACT_FIRST_NAME) != null && !((String) arguments.get(ArgumentConstants.CONTACT_FIRST_NAME)).equals("")) {
                order.getBillToCustomer().setFirstName((String) arguments.get(ArgumentConstants.CONTACT_FIRST_NAME));
            }
            if(arguments.get(ArgumentConstants.CONTACT_LAST_NAME) != null && !((String) arguments.get(ArgumentConstants.CONTACT_LAST_NAME)).equals("")) {
                order.getBillToCustomer().setLastName((String) arguments.get(ArgumentConstants.CONTACT_LAST_NAME));
            }
            if(arguments.get(ArgumentConstants.CONTACT_HOME_PHONE) != null && !((String) arguments.get(ArgumentConstants.CONTACT_HOME_PHONE)).equals("")) {
                order.getBillToCustomer().setHomePhone((String) arguments.get(ArgumentConstants.CONTACT_HOME_PHONE));
            }
            if(arguments.get("customerId") != null && !((String) arguments.get("customerId")).equals("")) {
                order.getBillToCustomer().setHpCustomerId((String) arguments.get("customerId"));
            }
            if(arguments.get("yellowPagesCode") != null && !((String) arguments.get("yellowPagesCode")).equals("")) {
                order.setYellowPagesCode((String) arguments.get("yellowPagesCode"));
            }

            FTDDataRequest dataRequest = null;
            GenericDataService service = null;
            FTDDataResponse dataResponse = null;

            NodeList nl = null;
            String xpath = null;
            XPathQuery q = null;

            // If customer id is passed load all customer info
            if(order.getBillToCustomer().getHpCustomerId() != null && !order.getBillToCustomer().getHpCustomerId().equals("")) {
                dataRequest = new FTDDataRequest();

                dataRequest.addArgument("customerId",  order.getBillToCustomer().getHpCustomerId());
                dataRequest.addArgument("institutionName",  null);
                dataRequest.addArgument("address1",  null);
                dataRequest.addArgument("address2",  null);
                dataRequest.addArgument("inCity",  null);
                dataRequest.addArgument("inState",  null);
                dataRequest.addArgument("inZipCode",  null);
                dataRequest.addArgument("inPhone",  null);
                dataRequest.addArgument("dnisType", order.getScriptCode());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_CUSTOMER);
                dataResponse = service.executeRequest(dataRequest);

                xpath = "customerLookup/searchResults/searchResult";
                q = new XPathQuery();
                nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                if (nl.getLength() > 0)
                {
                    Element customerData = (Element) nl.item(0);

                    order.getBillToCustomer().setExistingFlag(Boolean.TRUE);
                    order.getBillToCustomer().setHpCustomerId(customerData.getAttribute("customerId"));
                    order.getBillToCustomer().setAddressOne(customerData.getAttribute("address1"));
                    order.getBillToCustomer().setAddressTwo(customerData.getAttribute("address2"));
                    order.getBillToCustomer().setZipCode(customerData.getAttribute("zipCode"));
                    order.getBillToCustomer().setCity(customerData.getAttribute("city"));
                    order.getBillToCustomer().setState(customerData.getAttribute("state"));
                    order.getBillToCustomer().setCountry(customerData.getAttribute("countryId"));
                    order.getBillToCustomer().setWorkPhone(customerData.getAttribute("workPhone"));
                    order.getBillToCustomer().setPhoneExtension(customerData.getAttribute("workPhoneExt"));
                    order.getBillToCustomer().setEmailAddress(customerData.getAttribute("email"));
                    order.getBillToCustomer().setSpecialPromotionsFlag(customerData.getAttribute("promoFlag"));
                }
            }

            // Set up header for customer information
            if(order.getBillToCustomer().getFirstName() != null && !order.getBillToCustomer().getFirstName().trim().equals("")) {
                order.setHeaderValue("customerFirstName", order.getBillToCustomer().getFirstName());
            }
            if(order.getBillToCustomer().getLastName() != null && !order.getBillToCustomer().getLastName().trim().equals("")) {
                order.setHeaderValue("customerLastName", order.getBillToCustomer().getLastName());
            }

            if(!order.getHeaderMap().containsKey("customerFirstName") && !order.getHeaderMap().containsKey("customerLastName")) {
                order.setHeaderValue("customerFirstName", "Unknown");
            }

            if(order.getBillToCustomer().isExisting()) {
                order.setHeaderValue("customerRegistered", GeneralConstants.YES);
            }
            else {
                order.setHeaderValue("registered", GeneralConstants.NO);
            }

            XMLDocument document = XMLEncoder.createXMLDocument("OCCASION PAGE VO");

            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            Date deliveryDate = order.getDeliveryDate();

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("sourcecode", order.getSourceCode());
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
            dataResponse = service.executeRequest(dataRequest);

            // Pull the source code detail
            xpath = "sourceCodeDataList/sourceCodeData/data";

            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                Element sourceData = null;
                String rewardType = null;

                for(int i=0; i < nl.getLength(); i++) {

                    sourceData = (Element) nl.item(i);
                    order.setMarketingGroup(sourceData.getAttribute("marketingGroup"));

                    if(sourceData.getAttribute("discountType").equals("P"))
                        rewardType = "Percent";
                    else if(sourceData.getAttribute("discountType").equals("D"))
                        rewardType = "Dollars";
                    else if(sourceData.getAttribute("discountType").equals("M"))
                        rewardType = "Miles";
                    else
                        this.getLogManager().error("Invalid reward type");

                    // Set the order with source code data
                    order.setRewardType(rewardType);
                    order.setDomesticServiceFee(new BigDecimal(sourceData.getAttribute("domesticServiceCharge")));
                    order.setInternationalServiceFee(new BigDecimal(sourceData.getAttribute("intlServiceCharge")));
                    order.setPaymentMethod(sourceData.getAttribute("validPayMethod"));
                    order.setPaymentMethodType(sourceData.getAttribute("paymentType"));
                    order.setPartnerId(sourceData.getAttribute("partnerId"));
                    order.setBillingPrompt(sourceData.getAttribute("billingInfoPrompt"));
                    order.setBillingInfoLogic(sourceData.getAttribute("billingInfoLogic"));
                    order.setSourceCodeDescription(sourceData.getAttribute("description"));
                    order.setPricingCode(sourceData.getAttribute("pricingCode"));
                    order.setSourceCodeType(sourceData.getAttribute("sourceType"));

                    if(sourceData.getAttribute("jcPenneyFlag") != null && sourceData.getAttribute("jcPenneyFlag").equals(GeneralConstants.YES)) {
                        order.setJcPenneyFlag(Boolean.TRUE);
                    }

                    if(order.getYellowPagesCode() != null && !order.getYellowPagesCode().trim().equals("")) {
                        order.setYellowPagesFlag(Boolean.TRUE);
                    }
                }
            }

            // Set up header for customer information
            order.setHeaderValue("sourceCodeSelect", order.getSourceCode());
            order.setHeaderValue("sourceCodeText", order.getSourceCodeDescription());

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("promotionID", order.getPartnerId());

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_PROMOTIONS);
            dataResponse = service.executeRequest(dataRequest);

            // Pull the promotion details
            Element promotionData = null;
            PromotionPO partnerPromotion = null;
            xpath = "promotionData/promotions/promotion";

            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if ( (nl.getLength() > 0) &&
                 (order.getPricingCode() != null) &&
                 (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
            {
              for ( int i=0; i < nl.getLength(); i++ )
              {
                    promotionData = (Element) nl.item(i);

                    partnerPromotion = new PromotionPO();
                    partnerPromotion.setPromotionId(promotionData.getAttribute("promotionId"));
                    partnerPromotion.setBasePoints(new Long(promotionData.getAttribute("basePoints")));
                    partnerPromotion.setVariablePoints(new BigDecimal(promotionData.getAttribute("unitPoints")));
                    partnerPromotion.setPointDriver(promotionData.getAttribute("unit"));
                    partnerPromotion.setLowPrice(new Double(promotionData.getAttribute("minPrice")));
                    partnerPromotion.setHighPrice(new Double(promotionData.getAttribute("maxPrice")));

                    order.setRewardType(promotionData.getAttribute("rewardType"));
                    order.addPromotion(partnerPromotion);
              }
            }

            // Load required default data for occasion page
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("zipCode", null);
            dataRequest.addArgument("dnisType", order.getScriptCode());

            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_OCCASION_DATA);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();
            XMLEncoder.addSection(document, xmlResponse.getChildNodes());

            // Encode the delivery dates to the XML (default unknown values)
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(this.getLogManager());
            OEDeliveryDateParm parms = new OEDeliveryDateParm();
            parms.setOrder(order);
            parms.setDeliverToday(Boolean.TRUE);
            //parms.setSaturdayDelivery(Boolean.TRUE);
            parms.setSundayDelivery(Boolean.TRUE);
            parms.setGlobalParms(getGlobalParms(GeneralConstants.XML_OCCASION_DATA, xmlResponse));
            parms.setFloralServiceCharge(order.getDomesticServiceFee());
            parms.setHolidayDates(getHolidayDates(order.getSendToCustomer().getCountry()));
            parms.setProductType(GeneralConstants.OE_PRODUCT_TYPE_FLORAL);
            parms.setShipMethodFlorist(true);
            parms.setShipMethodCarrier(false);
            parms.setProductSubType(GeneralConstants.OE_PRODUCT_SUBTYPE_NONE);
            parms.setZipTimeZone(GeneralConstants.OE_TIMEZONE_DEFAULT);
            parms.setZipCodeGNADDFlag(GeneralConstants.NO);
            parms.setZipCodeGotoFloristFlag("Y");

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter objecft
            deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_OCCASION_DATA, xmlResponse, parms);

            XMLDocument deliveryDates = deliveryDateUtil.getOrderDeliveryDates(parms);

            XMLEncoder.addSection(document, deliveryDates.getChildNodes());

            // set the default values for the Occasion page
            HashMap occasionData = new HashMap();
            occasionData.put("occasion", order.getOccasion());
            occasionData.put("country", order.getSendToCustomer().getCountry());
            occasionData.put("zip", order.getSendToCustomer().getZipCode());
            occasionData.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            occasionData.put("deliveryDaysOut", order.getDeliveryDaysOut());

            if ( order.getDeliveryDate() != null )
            {
                occasionData.put("deliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            else
            {
                occasionData.put("deliveryDate", "");
            }
            XMLEncoder.addSection(document, "selectedData", "data", occasionData);

            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
/*
        if(order.getPaymentMethodType() == null)
        {
            System.out.println("**** paymentMethodType = null");
        }
        else
        {
            System.out.println("**** paymentMethodType = " + order.getPaymentMethodType());
        }
*/
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO setCancelItem(FTDArguments arguments)
	{
        // This method is called from the delivery info page and product
        // detail page.  We only want to remove the cart item if it has
        // been added previously.

        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            String fromPage =(String) arguments.get("fromPageName");
            if(fromPage == null) fromPage = "";
            String cartItemNumber = (String) arguments.get("cartItemNumber");
            if(cartItemNumber == null) cartItemNumber = "";

            if(fromPage.equals("ProductDetail") && cartItemNumber.equals(""))
            {

            }
            else
            {

                removeItemFromCart(arguments);
            }

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Remove expired breadcrumbs
            Iterator crumbIterator = order.getCrumbMap().keySet().iterator();
            while(crumbIterator.hasNext())
            {
               order.removeCrumbURL((String) crumbIterator.next());
            }

            order.setCrumbURL("Occasion", (String) arguments.get("url"));

            // NEW CRUMB
            //order.clearBreadCrumbs();
            //order.addBreadCrumb("Occasion" + "|" + (String) arguments.get("url"));

            // Clear any previous order information
            order.setDeliveryDate(null);
            order.setDeliveryDateDisplay(null);
            order.setOccasion(null);
            order.getSendToCustomer().setZipCode(null);
            order.getSendToCustomer().setCountry((String) arguments.get("country"));

            // Set base info to order
            //order.setExistingCustomer(Boolean.FALSE);

            //order.setSourceCode((String) arguments.get(ArgumentConstants.SOURCE_CODE));
            //order.setBillToFirstName((String) arguments.get(ArgumentConstants.CONTACT_FIRST_NAME));
            //order.setBillToLastName((String) arguments.get(ArgumentConstants.CONTACT_LAST_NAME));
            //order.setBillToHomePhone((String) arguments.get(ArgumentConstants.CONTACT_HOME_PHONE));
            //order.setBillToId((String) arguments.get("customerId"));

            FTDDataRequest dataRequest = null;
            GenericDataService service = null;
            FTDDataResponse dataResponse = null;

            NodeList nl = null;
            String xpath = null;
            XPathQuery q = null;

            // If customer id is passed load all customer info
            if(order.getBillToCustomer().getHpCustomerId() != null && !order.getBillToCustomer().getHpCustomerId().equals("")) {
                dataRequest = new FTDDataRequest();

                dataRequest.addArgument("customerId",  order.getBillToCustomer().getHpCustomerId());
                dataRequest.addArgument("institutionName",  null);
                dataRequest.addArgument("address1",  null);
                dataRequest.addArgument("address2",  null);
                dataRequest.addArgument("inCity",  null);
                dataRequest.addArgument("inState",  null);
                dataRequest.addArgument("inZipCode",  null);
                dataRequest.addArgument("inPhone",  null);
                dataRequest.addArgument("dnisType", order.getScriptCode());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_CUSTOMER);
                dataResponse = service.executeRequest(dataRequest);

                xpath = "customerLookup/searchResults/searchResult";
                q = new XPathQuery();
                nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                if (nl.getLength() > 0)
                {
                    Element customerData = (Element) nl.item(0);

                    order.getBillToCustomer().setExistingFlag(Boolean.TRUE);
                    order.getBillToCustomer().setHpCustomerId(customerData.getAttribute("customerId"));
                    order.getBillToCustomer().setAddressOne(customerData.getAttribute("address1"));
                    order.getBillToCustomer().setAddressTwo(customerData.getAttribute("address2"));
                    order.getBillToCustomer().setZipCode(customerData.getAttribute("zipCode"));
                    order.getBillToCustomer().setCity(customerData.getAttribute("city"));
                    order.getBillToCustomer().setState(customerData.getAttribute("state"));
                    order.getBillToCustomer().setCountry(customerData.getAttribute("countryId"));
                    order.getBillToCustomer().setWorkPhone(customerData.getAttribute("workPhone"));
                    order.getBillToCustomer().setPhoneExtension(customerData.getAttribute("workPhoneExt"));
                    order.getBillToCustomer().setEmailAddress(customerData.getAttribute("email"));
                }
            }


            // Set up header for customer information
            if(order.getBillToCustomer().getFirstName() != null && !order.getBillToCustomer().getFirstName().trim().equals("")) {
                order.setHeaderValue("customerFirstName", order.getBillToCustomer().getFirstName());
            }
            if(order.getBillToCustomer().getLastName() != null && !order.getBillToCustomer().getLastName().trim().equals("")) {
                order.setHeaderValue("customerLastName", order.getBillToCustomer().getLastName());
            }

            if(!order.getHeaderMap().containsKey("customerFirstName") && !order.getHeaderMap().containsKey("customerLastName")) {
                order.setHeaderValue("customerFirstName", "Unknown");
            }

            if(order.getBillToCustomer().isExisting()) {
                order.setHeaderValue("customerRegistered", GeneralConstants.YES);
            }
            else {
                order.setHeaderValue("registered", GeneralConstants.NO);
            }


            XMLDocument document = XMLEncoder.createXMLDocument("OCCASION PAGE VO");

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("sourcecode", order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");

            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
            dataResponse = service.executeRequest(dataRequest);

            // Pull the source code detail
            xpath = "sourceCodeDataList/sourceCodeData/data";

            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
              Element sourceData = null;
              String rewardType = null;

              for(int i=0; i < nl.getLength(); i++)
              {
                sourceData = (Element) nl.item(i);
                order.setMarketingGroup(sourceData.getAttribute("marketingGroup"));

                if(sourceData.getAttribute("discountType").equals("P"))
                    rewardType = "Percent";
                else if(sourceData.getAttribute("discountType").equals("D"))
                    rewardType = "Dollars";
                else if(sourceData.getAttribute("discountType").equals("M"))
                    rewardType = "Miles";
                else
                    this.getLogManager().error("Invalid reward type");

                // Set the order with source code data
                order.setRewardType(rewardType);
                order.setDomesticServiceFee(new BigDecimal(sourceData.getAttribute("domesticServiceCharge")));
                order.setInternationalServiceFee(new BigDecimal(sourceData.getAttribute("intlServiceCharge")));
                order.setPaymentMethod(sourceData.getAttribute("validPayMethod"));
                order.setPaymentMethodType(sourceData.getAttribute("paymentType"));
                order.setPartnerId(sourceData.getAttribute("partnerId"));
                order.setBillingPrompt(sourceData.getAttribute("billingInfoPrompt"));
                order.setBillingInfoLogic(sourceData.getAttribute("billingInfoLogic"));
                order.setSourceCodeDescription(sourceData.getAttribute("description"));
                order.setSourceCodeType(sourceData.getAttribute("sourceType"));
              }
            }

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("promotionID", order.getPartnerId());

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_PROMOTIONS);
            dataResponse = service.executeRequest(dataRequest);

            // Pull the promotion details
            Element promotionData = null;
            PromotionPO partnerPromotion = null;
            xpath = "promotionData/promotions/promotion";

            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
              for(int i=0; i < nl.getLength(); i++)
              {
                    promotionData = (Element) nl.item(i);

                    partnerPromotion = new PromotionPO();
                    partnerPromotion.setBasePoints(new Long(promotionData.getAttribute("basePoints")));
                    partnerPromotion.setVariablePoints(new BigDecimal(promotionData.getAttribute("unitPoints")));
                    partnerPromotion.setPointDriver(promotionData.getAttribute("unit"));
                    partnerPromotion.setLowPrice(new Double(promotionData.getAttribute("minPrice")));
                    partnerPromotion.setHighPrice(new Double(promotionData.getAttribute("maxPrice")));

                    order.setRewardType(promotionData.getAttribute("rewardType"));
                    order.addPromotion(partnerPromotion);
              }
            }

            // Load required default data for occasion page
            dataRequest = new FTDDataRequest();

            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("zipCode", null);
            dataRequest.addArgument("dnisType", order.getScriptCode());

            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_OCCASION_DATA);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();
            XMLEncoder.addSection(document, xmlResponse.getChildNodes());

            // Encode the delivery dates to the XML (default to HST)
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(this.getLogManager());
            OEDeliveryDateParm parms = new OEDeliveryDateParm();

            parms.setOrder(order);
            parms.setDeliverToday(Boolean.TRUE);
            //parms.setSaturdayDelivery(Boolean.TRUE);
            parms.setSundayDelivery(Boolean.TRUE);
            parms.setGlobalParms(getGlobalParms(GeneralConstants.XML_OCCASION_DATA, xmlResponse));
            parms.setZipTimeZone(GeneralConstants.OE_TIMEZONE_DEFAULT);
            parms.setFloralServiceCharge(order.getDomesticServiceFee());
            parms.setHolidayDates(getHolidayDates(order.getSendToCustomer().getCountry()));
            parms.setProductType(GeneralConstants.OE_PRODUCT_TYPE_FLORAL);
            parms.setProductSubType("NONE");

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter objecft
            deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_OCCASION_DATA, xmlResponse, parms);

            XMLDocument deliveryDates = deliveryDateUtil.getOrderDeliveryDates(parms);

            XMLEncoder.addSection(document, deliveryDates.getChildNodes());
            //order.setDeliveryDates(deliveryDates);

            HashMap occasionData = new HashMap();
            occasionData.put("occasion", order.getOccasion());
            occasionData.put("country", order.getSendToCustomer().getCountry());
            occasionData.put("zip", order.getSendToCustomer().getZipCode());
            if(order.getDeliveryDate() != null)
            {
                occasionData.put("deliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            else
            {
                occasionData.put("deliveryDate", "");
            }
            XMLEncoder.addSection(document, "selectedData", "data", occasionData);

            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO updateOccasion(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Set variables for delivery date calcs
            String countryId = (String) arguments.get("countryCode");
            String isInternational = (String) arguments.get("flag");

            String zipCode = null;
            String updzip = (String) arguments.get("updzip");
            //determine whether to use zip code from the occasion url becasue breadcrumb is not dynamic
            if(updzip.equals("Y"))
            {
                zipCode = (String) arguments. get("zipcode");
            }
            else
            {

                zipCode = order.getSendToCustomer().getZipCode();
            }

            String deliveryDate = (String) arguments.get("deliveryDate");
            String deliveryDateDisplay = (String) arguments.get("deliveryDateDisplay");
            String maxDeliveryDaysOut = (String) arguments.get("maxDeliveryDaysOut");
            if (maxDeliveryDaysOut != null && maxDeliveryDaysOut.equals("true"))
            {
              order.setDeliveryDaysOut(ArgumentConstants.DELIVERY_DAYS_OUT_MAX);
            }

            // Zip Code or Country changed, reset popup flags
            if ( ((zipCode != null) &&
                  (order.getSendToCustomer().getZipCode() == null)) ||
                 ((countryId != null) &&
                  (order.getSendToCustomer().getCountry() == null)) )
            {
                order.setDisplayFloral(GeneralConstants.YES);
                order.setDisplayNoProduct(GeneralConstants.YES);
                order.setDisplaySpecialtyGift(GeneralConstants.YES);
            }
            else if ( ((zipCode != null) &&
                       (order.getSendToCustomer().getZipCode() != null)) ||
                      ((countryId != null) &&
                       (order.getSendToCustomer().getCountry() != null)) )
            {
                if ( (!zipCode.equals(order.getSendToCustomer().getZipCode()) ||
                     (!countryId.equals(order.getSendToCustomer().getCountry()))) )
                {
                    order.setDisplayFloral(GeneralConstants.YES);
                    order.setDisplayNoProduct(GeneralConstants.YES);
                    order.setDisplaySpecialtyGift(GeneralConstants.YES);
                }
            }

            // Set information for delivery date calcs
            order.setCrumbURL("Occasion", (String) arguments.get("url"));
            order.setOccasion((String) arguments.get("occasion"));
            order.getSendToCustomer().setZipCode(zipCode);

            if ( (deliveryDate != null) &&
                 (!deliveryDate.trim().equals("")) )
            {
                order.setDeliveryDate(FieldUtils.formatStringToUtilDate(deliveryDate));
                order.setDeliveryDateDisplay(deliveryDateDisplay);
            }


            // catches if delivery country was switched from international to domestic
            // and zip was not cleared
            if ( (isInternational.equals(GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC)) &&
                 (zipCode != null) &&
                 (zipCode.equals("N/A")) )
            {
                order.getSendToCustomer().setZipCode(null);
            }
            else
            {
                order.getSendToCustomer().setZipCode(zipCode);
            }

            // Load required data for occasion page
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("zipCode", order.getSendToCustomer().getZipCode());
            dataRequest.addArgument("dnisType", order.getScriptCode());

            GenericDataService service =  this.getGenericDataService(DataConstants.SHOPPING_GET_OCCASION_DATA);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);
            XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

            XMLDocument document = XMLEncoder.createXMLDocument("OCCASION PAGE VO");
            XMLEncoder.addSection(document, xmlResponse.getChildNodes());

            OEParameters globalParms = getGlobalParms(GeneralConstants.XML_OCCASION_DATA, xmlResponse);

            // Get the timezone from the xml for processing delivery dates
            String timeZone = null;
            String zipGnaddFlag = null;
            String zipFloralFlag = null;
            String zipGotoFloristFlag = null;
            String zipSundayFlag = null;
            String stateId = null;
            String countryCode = null;

            Element timezoneElement = null;
            NodeList nl = null;
            String xpath = "occasionData/zipCodeData/data";
            XPathQuery q = new XPathQuery();

            nl = q.query(xmlResponse, xpath);

            if ( nl.getLength() > 0 )
            {
                order.getSendToCustomer().setZipCode(zipCode);
                timezoneElement = (Element) nl.item(0);
                timeZone = timezoneElement.getAttribute("timeZone");

                zipGnaddFlag = timezoneElement.getAttribute("zipGnaddFlag").trim();
                zipFloralFlag = timezoneElement.getAttribute("zipFloralFlag").trim();
                zipGotoFloristFlag = timezoneElement.getAttribute("zipGotoFloristFlag").trim();
                zipSundayFlag = timezoneElement.getAttribute("zipSundayFlag").trim();
                stateId = timezoneElement.getAttribute("stateId");
                countryCode = timezoneElement.getAttribute("countryCode");

            }
            else {
                // No valid zip code was entered
                timeZone = GeneralConstants.OE_TIMEZONE_DEFAULT;
                zipGnaddFlag = GeneralConstants.NO;
                zipFloralFlag = GeneralConstants.YES;  // Assume floral delivery available
                zipGotoFloristFlag = GeneralConstants.YES;
                zipSundayFlag = GeneralConstants.YES;
                stateId = order.getSendToCustomer().getState();
                countryCode = countryId;
            }

            // Canadians have the LNL format
            if (  order.getSendToCustomer().getZipCode() != null
                  && order.getSendToCustomer().getZipCode().length() > 3
                  && Character.isLetter(order.getSendToCustomer().getZipCode().charAt(0))
                  && Character.isDigit(order.getSendToCustomer().getZipCode().charAt(1))
                  && Character.isLetter(order.getSendToCustomer().getZipCode().charAt(2)))
            {
                countryCode = GeneralConstants.COUNTRY_CODE_CANADA_CA;
            }

            // check to see if state is Puerto Rico or Virgin Islands and
            // country is not the same, so set it to same
            if ( (stateId != null) &&
                 (stateId.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                  stateId.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
                 (!stateId.equals(countryId) ||
                  !countryCode.equals(countryId)) )
            {
                countryId = stateId;
                countryCode = stateId;
            }

            // country is United States or Canada and country is not same, so set it same
            else if ( (countryCode != null) &&
                      (countryCode.equals(GeneralConstants.COUNTRY_CODE_US) ||
                       countryCode.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                       countryCode.equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN)) &&
                      (!countryCode.equals(countryId)) )
            {
                // account for 'CAN' as country code, incorrect, but possible
                if ( countryCode.length() > 2 )
                {
                    countryCode = countryCode.substring(0,2);
                }

                if ( !countryCode.equals(countryId) )
                {
                    countryId = countryCode;
                }
            }

            int delivery_date_type = -1;
            if ( isInternational.equals(GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC) )
            {
                // Canada is a domestic country unless Source Code is JC Penney
                if ( order.getScriptCode() != null && countryId != null &&
                     order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE) &&
                     (countryId.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                      countryId.equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN) ||
                      countryId.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                      countryId.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) )
                {
                    delivery_date_type = 0;
                }
                else
                {
                    delivery_date_type = 1;
                }
            }
            else if ( isInternational.equals(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL) )
            {
                if(countryId.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                    countryId.equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN) ||
                    countryId.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                    countryId.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS))
                {
                    delivery_date_type = 1;
                }
                else
                {
                    delivery_date_type = 0;
                }
            }
            else
            {
                // default to domestic
                this.getLogManager().error("Could not determine if international or domestic");
                delivery_date_type = 1;
            }

            switch ( delivery_date_type )
            {
                case 0:   // International
                    order.getSendToCustomer().setDomesticFlag(Boolean.FALSE);
                    timeZone = GeneralConstants.OE_TIMEZONE_CENTRAL;
                    zipGnaddFlag = GeneralConstants.NO;
                    zipGotoFloristFlag = GeneralConstants.NO;
                    zipSundayFlag = GeneralConstants.NO;
                    break;

                case 1:   // Domestic
                    order.getSendToCustomer().setDomesticFlag(Boolean.TRUE);
                    break;
            }

            // Encode the delivery dates to the XML (default to HST)
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(this.getLogManager());
            OEDeliveryDateParm parms = new OEDeliveryDateParm();

            parms.setOrder(order);
            parms.setZipTimeZone(timeZone);
            //parms.setSaturdayDelivery(Boolean.TRUE);
            parms.setGlobalParms(globalParms);
            parms.setZipCodeGNADDFlag(zipGnaddFlag);
            parms.setZipCodeFloralFlag(zipFloralFlag);
            parms.setZipCodeGotoFloristFlag(zipGotoFloristFlag);
            parms.setHolidayDates(getHolidayDates(countryId));
            parms.setProductType(GeneralConstants.OE_PRODUCT_TYPE_FLORAL);
            parms.setShipMethodFlorist(true);
            parms.setShipMethodCarrier(false);
            parms.setProductSubType(GeneralConstants.OE_PRODUCT_SUBTYPE_NONE);
//            parms.setCntryAddOnDays(Integer.parseInt(element.getAttribute("addonDays")));

/* CLF - codified product code */
            if ( (zipSundayFlag != null) &&
                 (zipSundayFlag.equals(GeneralConstants.YES)) )
            {
                parms.setSundayDelivery(Boolean.TRUE);
            }
            else
            {
                parms.setSundayDelivery(Boolean.FALSE);
            }
/* */

            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                parms.setDeliverToday(Boolean.TRUE);
                parms.setFloralServiceCharge(order.getDomesticServiceFee());
                parms.setDeliveryType(GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                parms.setDeliverToday(Boolean.FALSE);
                parms.setFloralServiceCharge(order.getInternationalServiceFee());
                parms.setDeliveryType(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter objecft
            deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_OCCASION_DATA, xmlResponse, parms);

            // retrieve the delivery dates
            XMLDocument deliveryDates = deliveryDateUtil.getOrderDeliveryDates(parms);

            XMLEncoder.addSection(document, deliveryDates.getChildNodes());
            //order.setDeliveryDates(deliveryDates);
            order.getSendToCustomer().setCountry(countryId);

            // country is Canada, Puerto Rico, Virgin Islands, or International
            // so can only deliver Floral products
            if ( countryId.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||       // Canada
                 countryId.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||      // Puerto Rico
                 countryId.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS) )    // Virgin Islands
            {

                // Zip code does not exist or is shut down,
                // so can not delivery any products
                if ( ((zipGnaddFlag.equals(GeneralConstants.YES) && globalParms.getGNADDLevel().equals("0"))
                       || zipFloralFlag.equals(GeneralConstants.NO)) &&
                     (order.getDisplayNoProduct().equals(GeneralConstants.YES)) )
                {
                    HashMap pageData = new HashMap();
                    pageData.put("displayNoProductPopup", GeneralConstants.YES);
                    XMLEncoder.addSection(document, "pageData", "data", pageData);

                    // set order flag so will not be displayed unless zip code changed later
                    order.setDisplayNoProduct(GeneralConstants.NO);
                    order.setDisplayFloral(GeneralConstants.YES);
                    order.setDisplaySpecialtyGift(GeneralConstants.YES);
                }
            }
            // zip code does not exist or is shut down
            // so can only delivery Carrier delivered products
            else if ( ((zipGnaddFlag.equals(GeneralConstants.YES) && globalParms.getGNADDLevel().equals("0"))
                       || zipFloralFlag.equals(GeneralConstants.NO)) &&
                     (order.getDisplaySpecialtyGift().equals(GeneralConstants.YES)) )
            {
                HashMap pageData = new HashMap();
                pageData.put("displaySpecGiftPopup", GeneralConstants.YES);
                XMLEncoder.addSection(document, "pageData", "data", pageData);

                order.setDisplayNoProduct(GeneralConstants.YES);
                order.setDisplayFloral(GeneralConstants.YES);
                order.setDisplaySpecialtyGift(GeneralConstants.NO);
            }

            HashMap occasionData = new HashMap();
            occasionData.put("occasion", order.getOccasion());
            occasionData.put("zip", order.getSendToCustomer().getZipCode());
            occasionData.put("country", order.getSendToCustomer().getCountry());
            occasionData.put("deliveryDaysOut", order.getDeliveryDaysOut());

            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                occasionData.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                occasionData.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }

            if ( order.getDeliveryDate() != null )
            {
                occasionData.put("deliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            else
            {
                occasionData.put("deliveryDate", "");
            }
            XMLEncoder.addSection(document, "selectedData", "data", occasionData);

            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());
            //XMLEncoder.addSection(shoppingUTIL.transformBreadCrumbsToXML(order.getBreadCrumbList()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
    }

    private FTDSystemVO setOccasion(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;
        String custOrderAllowed = "Y";

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            order.removeCrumbURL("Product List");
            order.setCrumbURL("NEW Search", (String) arguments.get("url"));

            // Update order with occasion info
            String internationalFlag = (String) arguments.get("flag");
            if ( (internationalFlag != null) && (internationalFlag.equals(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL)) )
            {
                order.getSendToCustomer().setDomesticFlag(Boolean.FALSE);
            }
            else
            {
                order.getSendToCustomer().setDomesticFlag(Boolean.TRUE);
            }

            // Get all params needed below.
            // If we are adding another item to the same recipient then use
            // the item already in the cart.  Otherwise get the params out
            // of the arguments
            String command = (String)arguments.get("command");
            String argCity = null;
            String argState = null;
            String argZip =  null;
            String argCountry =  null;
            String argOccasion =  null;
            String argDeliveryDate =  null;
            String argDeliveryDateDisplay =  null;
            String argRecipientId = null;
            String argFirstName = null;
            String argLastName = null;
            String argAddress1 = null;
            String argAddress2 = null;
            String argPhone = null;
            String argPhoneExt = null;
            String argCompanyName=null;
            String argCompanyType=null;
            String argCompanyInfo=null;

            ItemPO item = null;
            if(command != null && command.equals("addDifferentItem"))
            {
                String itemCartNumber = (String)arguments.get("itemCartNumber");
                item = order.getItem(new Integer(itemCartNumber));
                argCity = item.getSendToCustomer().getCity();
                argState = item.getSendToCustomer().getState();
                argZip = item.getSendToCustomer().getZipCode();
                argCountry = item.getSendToCustomer().getCountry();
                argOccasion = item.getOccasion();
                argDeliveryDate = FieldUtils.formatUtilDateToString(item.getDeliveryDate());
                argDeliveryDateDisplay = item.getDeliveryDateDisplay();
                argCompanyName = item.getSendToCustomer().getCompanyName();
                argCompanyType = item.getSendToCustomer().getCompanyType();
                argCompanyInfo = item.getSendToCustomer().getCompanyInfo();
            }
            else
            {
                argRecipientId = (String) arguments.get("recipientId");
                argFirstName = (String) arguments.get("firstName");
                argLastName = (String) arguments.get("lastName");
                argAddress1 = (String) arguments.get("address1");
                argAddress2 = (String) arguments.get("address2");
                argCity = (String) arguments.get(ArgumentConstants.CONTACT_CITY);
                argState = (String) arguments.get(ArgumentConstants.CONTACT_STATE);
                argZip = (String) arguments.get("zipcode");
                argCountry = (String) arguments.get("country");
                argOccasion = (String) arguments.get("occasion");
                argDeliveryDate = (String) arguments.get("deliveryDate");
                argDeliveryDateDisplay = (String) arguments.get("deliveryDateDisplay");
                argPhone = (String) arguments.get("phone");
                argPhoneExt = (String) arguments.get("phoneExt");
                argCompanyName = (String) arguments.get("bhfName");
                argCompanyType = (String) arguments.get("addressType");
                argCompanyInfo = (String) arguments.get("bhfInfo");
            }
            order.getSendToCustomer().setHpCustomerId(argRecipientId);
            order.getSendToCustomer().setFirstName(argFirstName);
            order.getSendToCustomer().setLastName(argLastName);
            order.getSendToCustomer().setAddressOne(argAddress1);
            order.getSendToCustomer().setAddressTwo(argAddress2);
            order.getSendToCustomer().setCity(argCity);
            order.getSendToCustomer().setState(argState);
            order.getSendToCustomer().setHomePhone(argPhone);
            order.getSendToCustomer().setPhoneExtension(argPhoneExt);
            order.getSendToCustomer().setCompanyName(argCompanyName);
            order.getSendToCustomer().setCompanyType(argCompanyType);
            order.getSendToCustomer().setCompanyInfo(argCompanyInfo);
            // clear zip code if order is domestic and no valid zip code was entered
            String zipCode = argZip;
            if ( order.getSendToCustomer().isDomesticFlag() && zipCode != null && zipCode.equals("N/A") )
            {
                order.getSendToCustomer().setZipCode(null);
            }
            else
            {
                order.getSendToCustomer().setZipCode(zipCode);
            }

            order.getSendToCustomer().setCountry(argCountry);
            order.setOccasion(argOccasion);

            // Load state from database for notification check later
            FTDDataRequest dataRequest = null;
            GenericDataService service =  null;
            FTDDataResponse dataResponse = null;

            if ( (order.getSendToCustomer().getZipCode() != null) &&
                 (!order.getSendToCustomer().getZipCode().trim().equals("")) )
            {
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("zipIn", order.getSendToCustomer().getZipCode());
                dataRequest.addArgument("cityIn", null);
                dataRequest.addArgument("stateIn", null);
                dataRequest.addArgument("dnisType", order.getScriptCode());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_ZIP_CODE_DETAIL);
                dataResponse = service.executeRequest(dataRequest);

                XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                Element zipCodeElement = null;
                NodeList nl = null;
                String xpath = "zipCodeLookup/searchResults/searchResult";
                XPathQuery q = new XPathQuery();

                nl = q.query(xmlResponse, xpath);

                if ( nl.getLength() > 0 )
                {
                    zipCodeElement = (Element) nl.item(0);
                    String stateId = zipCodeElement.getAttribute("state");
                    order.getSendToCustomer().setState(stateId);
                }
            }

            if ( (argDeliveryDate != null) &&
                 (!(argDeliveryDate).equals("")) )
            {
                String deliveryDate = argDeliveryDate;
                //deliveryDate = getFirstDate(deliveryDate);
                order.setDeliveryDate(FieldUtils.formatStringToUtilDate(deliveryDate));
                order.setDeliveryDateDisplay(argDeliveryDateDisplay);

                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("sourcecode", order.getSourceCode());
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, deliveryDate);
                this.getLogManager().debug(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS + " - " +
                    order.getSourceCode() + " " + deliveryDate);
                service = this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
                dataResponse = service.executeRequest(dataRequest);
                XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                StringWriter sw = new StringWriter();
                xmlResponse.print(new PrintWriter(sw));
                this.getLogManager().debug(sw.toString());

                NodeList nls3 = null;
                String xpath3 = "sourceCodeDataList/sourceCodeData/data";
                XPathQuery q3 = new XPathQuery();
                nls3 = q3.query(xmlResponse, xpath3);
                if ( nls3.getLength() > 0 )
                {
                    Element serviceFeeElement = (Element) nls3.item(0);
                    String domesticFee = serviceFeeElement.getAttribute("domesticServiceCharge");
                    //this.getLogManager().debug("domestic: " + domesticFee);
                    if (domesticFee != null && !(domesticFee.equals(""))) {
                        order.setDomesticServiceFee(new BigDecimal(domesticFee));
                    }
                    String internationalFee = serviceFeeElement.getAttribute("intlServiceCharge");
                    //this.getLogManager().debug("international: " + internationalFee);
                    if (internationalFee != null && !(internationalFee.equals(""))) {
                        order.setInternationalServiceFee(new BigDecimal(internationalFee));
                    }
                }
            }

            // Check if custom order product is available for this source code
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("custOrderPid", GeneralConstants.CUSTOM_ORDER_PID);
            dataRequest.addArgument("sourceCodeId", order.getSourceCode());
            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_CUSTOM_ORDER_AVAIL);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument xmlResp = (XMLDocument)dataResponse.getDataVO().getData();
            NodeList nls = null;
            String xpath2 = "customOrderAvail/availForSource/avail";
            XPathQuery q2 = new XPathQuery();
            nls = q2.query(xmlResp, xpath2);
            if ( nls.getLength() <= 0 )
            {
                custOrderAllowed = "N";
            }

            // Load Category Page for Domestic
            dataRequest = new FTDDataRequest();
            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
                dataRequest.addArgument("company", order.getCompanyId());

                // Category Index Args
                dataRequest.addArgument("liveFlag_1", GeneralConstants.YES);
                dataRequest.addArgument("occasionsFlag_1", GeneralConstants.NO);
                dataRequest.addArgument("productsFlag_1", GeneralConstants.YES);
                dataRequest.addArgument("dropshipFlag_1", GeneralConstants.NO);
                dataRequest.addArgument("countryID_1", null);
                dataRequest.addArgument("sourceCodeID_1", order.getSourceCode());
                // Occasions Index Args
                dataRequest.addArgument("liveFlag_2", GeneralConstants.YES);
                dataRequest.addArgument("occasionsFlag_2", GeneralConstants.YES);
                dataRequest.addArgument("productsFlag_2", GeneralConstants.NO);
                dataRequest.addArgument("dropshipFlag_2", GeneralConstants.NO);
                dataRequest.addArgument("countryID_2", null);
                dataRequest.addArgument("sourceCodeID_2", order.getSourceCode());
                // Sub Index Args
                dataRequest.addArgument("liveFlag_3", GeneralConstants.YES);
                dataRequest.addArgument("dropshipFlag_3", GeneralConstants.NO);
                dataRequest.addArgument("countryID_3", null);
                dataRequest.addArgument("sourceCodeID_3", order.getSourceCode());
                service =  this.getGenericDataService(DataConstants.SHOPPING_GET_INDEX_PAGE);
                dataResponse = service.executeRequest(dataRequest);

                // Encode the Category to the XML
                XMLDocument document = XMLEncoder.createXMLDocument("SHOPPING PAGE VO");
                XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

                XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
                XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());
                HashMap pageDataMap = new HashMap();
                pageDataMap.put("custOrderAllowed", custOrderAllowed);
                XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

                systemVO.setXML(document);
            }
            else
            {
                // Super Index Args
                dataRequest.addArgument("liveFlag", GeneralConstants.YES);
                dataRequest.addArgument("occasionsFlag", GeneralConstants.NO);
                dataRequest.addArgument("productsFlag", GeneralConstants.NO);
                dataRequest.addArgument("dropshipFlag", GeneralConstants.NO);
                dataRequest.addArgument("countryID", order.getSendToCustomer().getCountry());
                dataRequest.addArgument("sourceCodeID", order.getSourceCode());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_SUPER_INDEX);
                dataResponse = service.executeRequest(dataRequest);
                XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                // Pull the super index details for the selected country
                Element superIndexData = null;
                String  indexId = null;
                String xpath = "superIndexData/superIndexes/index";

                XPathQuery q = new XPathQuery();
                NodeList nl = q.query(xmlResponse, xpath);

                if (nl.getLength() > 0)
                {
                  superIndexData = (Element) nl.item(0);
                  indexId = superIndexData.getAttribute("indexId");
                }

                try {
                    SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

                    XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT LIST VO");
                    String pageNumber = (String) arguments.get("pageNumber");

                    // Initial search hit loads all products matching the query for all pages
                    if ( pageNumber == null || (pageNumber).equals("init" ) )
                    {
                        order.setCrumbURL("Product List", (String) arguments.get("url"));

/*
                        // Get products by index args
                        dataRequest = new FTDDataRequest();
                        dataRequest.addArgument(ArgumentConstants.SEARCH_INDEX_ID, indexId);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                        dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, null);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, null);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, "I");
                        dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                        dataRequest.addArgument("scriptCode", order.getScriptCode());

                        // NEEDS TO BE CLEANED UP
                        OEParameters oeParmsFull = getGlobalParms();
                        order.setGlobalParameters(oeParmsFull);

                        // build the end date for the delivery date range
                        OEParameters oeParms = getGlobalParms(GeneralConstants.XML_SUPER_INDEX_DATA, xmlResponse);

                        Calendar deliveryEndDate = Calendar.getInstance();
                        deliveryEndDate.add(Calendar.DATE, oeParms.getDeliveryDaysOut());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_END_DATE, dateFormat.format(deliveryEndDate.getTime()));

                        service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);
                        dataResponse = service.executeRequest(dataRequest);

                        XMLDocument searchResultsIndex = (XMLDocument)dataResponse.getDataVO().getData();
*/

                        // NEEDS TO BE CLEANED UP
                        OEParameters oeParmsFull = getGlobalParms();
                        order.setGlobalParameters(oeParmsFull);

                        // build the end date for the delivery date range
                        OEParameters oeParms = getGlobalParms(GeneralConstants.XML_SUPER_INDEX_DATA, xmlResponse);

                        Calendar deliveryEndDate = Calendar.getInstance();
                        deliveryEndDate.add(Calendar.DATE, DeliveryDateUTIL.getDeliveryDaysOut(order, oeParms));//oeParms.getDeliveryDaysOut());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                        String endDateStr = dateFormat.format(deliveryEndDate.getTime());

                        // set arguments
                        Map searchArgs = new HashMap();
                        searchArgs.put(ArgumentConstants.SEARCH_INDEX_ID, indexId);
                        searchArgs.put(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                        searchArgs.put(ArgumentConstants.SEARCH_ZIP_CODE, null);
                        searchArgs.put(ArgumentConstants.SEARCH_PRICE_POINT_ID, null);
                        searchArgs.put(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, "I");
                        searchArgs.put(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                        searchArgs.put("scriptCode", order.getScriptCode());
                        searchArgs.put(ArgumentConstants.SEARCH_DELIVERY_END_DATE, endDateStr);

                        // perform search
                        XMLDocument searchResultsIndex = searchUTIL.preformProductListSearch(searchArgs, DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);

                        // save search criteria
                        String searchCriteria = searchUTIL.getSearchCriteriaString(searchArgs);
                        order.setSearchCriteria(searchCriteria);

                        SearchUTIL.paginate(searchResultsIndex,1, GeneralConstants.PRODUCTS_PER_PAGE);

                        order.setSearchResults(searchResultsIndex);

                        XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, "1", arguments, order );

                        searchResultsPerPage = searchUTIL.sortProductList("sortDisplayOrder", searchResultsPerPage);

                        SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, GeneralConstants.PRODUCTS_PER_PAGE);

                        Element data = (Element) searchResultsIndex.selectNodes("//pageData/data[@name='totalPages']").item(0);
                        order.setSearchPageCount(new Integer(data.getAttribute("value")));

                        order.setSearchCurrentPage(new Integer(1));

                        // Encode products to xml for product list
                        XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage, order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
                    }
                    else
                    {
                        XMLDocument searchResultsIndex = order.getSearchResults();

                        // If search results are null then perform another search.
                        // This could happen if the session was loaded from the database and it did not already
                        // exist in the app server cache
                        if(searchResultsIndex == null)
                        {
                            Map searchCiteria = SearchUTIL.getSearchCriteriaMap(order.getSearchCriteria());
                            searchResultsIndex = SearchUTIL.preformProductListSearch(searchCiteria, DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);
                            SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                            order.setSearchResults(searchResultsIndex);
                        }

                        // if we are sorting by price then we need to paginate the sorted list and save it
                        // to the order
                        if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                        {
                            searchResultsIndex = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsIndex);
                            SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                            order.setSearchResults(searchResultsIndex);
                        }

                        // Get the product details for the current page
                        XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, pageNumber, arguments, order );

                        // if we are not sorting by anything then the default sort is by
                        // the order in the product index
                        if(arguments.get("sortType") == null || ((String) arguments.get("sortType")).equals(""))
                        {
                            searchResultsPerPage = searchUTIL.sortProductList("sortDisplayOrder", searchResultsPerPage);
                        }
                        // else sort by whatever type is passed in the arguments
                        else if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                        {
                            searchResultsPerPage = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsPerPage);
                        }

                        int iCurrentPage = Integer.parseInt(pageNumber);
                        SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, GeneralConstants.PRODUCTS_PER_PAGE);

                        // Encode products to xml for product list
                        order.setSearchCurrentPage(new Integer(pageNumber));
                        XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage, order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
                    }

                    // Load price points from database
                    dataRequest = new FTDDataRequest();
                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRICE_POINTS);
                    dataResponse = service.executeRequest(dataRequest);
                    XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

                    // Encode page data for product list
                    HashMap pageDataMap = new HashMap();
                    pageDataMap.put("totalPages", order.getSearchPageCount().toString());
                    pageDataMap.put("currentPage", order.getSearchCurrentPage().toString());
                    pageDataMap.put("rewardType", order.getRewardType());
                    pageDataMap.put("indexId", indexId);
                    pageDataMap.put("custOrderAllowed", custOrderAllowed);

                    XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

                    XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
                    XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

                    systemVO.setXML(document);

                } catch(Exception e) {
                    this.getLogManager().error(e.toString(), e);
                }
            }

        } catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO cancelItemToShopping(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            //String fromPage =(String) arguments.get("fromPageName");
            //if(fromPage == null) fromPage = "";
            String cartItemNumber = (String) arguments.get("cartItemNumber");
            if(cartItemNumber == null) cartItemNumber = "";

            if(cartItemNumber.equals(""))
            {

            }
            else
            {

                removeItemFromCart(arguments);
            }

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            order.removeCrumbURL("Product List");
            order.setCrumbURL("NEW Search", (String) arguments.get("url"));

/*
JMP Not needed?
            // Update order with occasion info
            String internationalFlag = (String) arguments.get("flag");
            if ( (internationalFlag != null) && (internationalFlag.equals("I")) )
            {
                order.setOrderDomesticFlag(Boolean.FALSE);
            }
            else
            {
                order.setOrderDomesticFlag(Boolean.TRUE);
            }

            order.setOrderDeliveryCity((String) arguments.get(ArgumentConstants.CONTACT_CITY));
            order.setOrderDeliveryState((String) arguments.get(ArgumentConstants.CONTACT_STATE));
*/
            // clear zip code if order is domestic and no valid zip code was entered
            String zipCode = (String) arguments.get("zipCode");
            if ( order.getSendToCustomer().isDomesticFlag() && zipCode != null && zipCode.equals("N/A") )
            {
                order.getSendToCustomer().setZipCode(null);
            }
            else
            {
                order.getSendToCustomer().setZipCode(zipCode);
            }
/*
JMP Not needed?
            order.setOrderDeliveryCountry((String) arguments.get("country"));
            order.setOrderOccasion((String) arguments.get("occasion"));
*/
            // Load state from database for notification check later
            FTDDataRequest dataRequest = null;
            GenericDataService service =  null;
            FTDDataResponse dataResponse = null;

            if ( (order.getSendToCustomer().getZipCode() != null) &&
                 (!order.getSendToCustomer().getZipCode().trim().equals("")) )
            {
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("zipIn", order.getSendToCustomer().getZipCode());
                dataRequest.addArgument("cityIn", null);
                dataRequest.addArgument("stateIn", null);

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_ZIP_CODE_DETAIL);
                dataResponse = service.executeRequest(dataRequest);

                XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                Element zipCodeElement = null;
                NodeList nl = null;
                String xpath = "zipCodeLookup/searchResults/searchResult";
                XPathQuery q = new XPathQuery();

                nl = q.query(xmlResponse, xpath);

                if ( nl.getLength() > 0 )
                {
                    zipCodeElement = (Element) nl.item(0);
                    String stateId = zipCodeElement.getAttribute("state");
//                    String countryCode = zipCodeElement.getAttribute("countryCode");

                    order.getSendToCustomer().setState(stateId);
/*
                    // Puerto Rico or Virgin Islands zip code was entered
                    // without selecting them as country.
                    if ( stateId.equals("PR") || stateId.equals("VI") )
                    {
                        order.setOrderDeliveryCountry(stateId);
                    }
                    // Canadian zip code was entered and country is not Canada
                    else if ( (countryCode != null) &&
                              (countryCode.equals("CAN")) &&
                              (!countryCode .equals(order.getOrderDeliveryCountry())) )
                    {
                        order.setOrderDeliveryCountry(countryCode);
                    }
*/
                }
            }

/*
JMP Not needed?
            if ( (arguments.get("deliveryDate") != null) &&
                 (!((String)arguments.get("deliveryDate")).equals("")) )
            {
                order.setOrderDeliveryDate(FieldUtils.formatStringToUtilDate((String) arguments.get("deliveryDate")));
            }
*/
           dataRequest = new FTDDataRequest();

            // Load Category Page for Domestic
            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                // NEW CRUMB
                //order.clearBreadCrumbs(1);
                //order.addBreadCrumb((String) arguments.get("occasionDescription") + "|" + (String) arguments.get("url"));

                dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
                dataRequest.addArgument("company", order.getCompanyId());

                // Category Index Args
                dataRequest.addArgument("liveFlag_1", GeneralConstants.YES);
                dataRequest.addArgument("occasionsFlag_1", GeneralConstants.NO);
                dataRequest.addArgument("productsFlag_1", GeneralConstants.YES);
                dataRequest.addArgument("dropshipFlag_1", GeneralConstants.NO);
                dataRequest.addArgument("countryID_1", null);
                dataRequest.addArgument("sourceCodeID_1", order.getSourceCode());
                // Occasions Index Args
                dataRequest.addArgument("liveFlag_2", GeneralConstants.YES);
                dataRequest.addArgument("occasionsFlag_2", GeneralConstants.YES);
                dataRequest.addArgument("productsFlag_2", GeneralConstants.NO);
                dataRequest.addArgument("dropshipFlag_2", GeneralConstants.NO);
                dataRequest.addArgument("countryID_2", null);
                dataRequest.addArgument("sourceCodeID_2", order.getSourceCode());
                // Sub Index Args
                dataRequest.addArgument("liveFlag_3", GeneralConstants.YES);
                dataRequest.addArgument("dropshipFlag_3", GeneralConstants.NO);
                dataRequest.addArgument("countryID_3", null);
                dataRequest.addArgument("sourceCodeID_3", order.getSourceCode());
                service =  this.getGenericDataService(DataConstants.SHOPPING_GET_INDEX_PAGE);
                dataResponse = service.executeRequest(dataRequest);

                // Encode the Category to the XML
                XMLDocument document = XMLEncoder.createXMLDocument("SHOPPING PAGE VO");
                XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());
                HashMap occasionMap = new HashMap();
                occasionMap.put("occasionId", order.getOccasion());
                XMLEncoder.addSection(document, "orderOccasion", "orderOccasion", occasionMap);
                XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
                XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

                //XMLEncoder.addSection(shoppingUTIL.transformBreadCrumbsToXML(order.getBreadCrumbList()).getChildNodes());


                systemVO.setXML(document);
            }
            else
            {
                // NEW CRUMB
                //order.clearBreadCrumbs(1);
                //order.addBreadCrumb(order.getOrderDeliveryCountry() + "|" + (String) arguments.get("url"));

                // Super Index Args
                dataRequest.addArgument("liveFlag", GeneralConstants.YES);
                dataRequest.addArgument("occasionsFlag", GeneralConstants.NO);
                dataRequest.addArgument("productsFlag", GeneralConstants.NO);
                dataRequest.addArgument("dropshipFlag", GeneralConstants.NO);
                dataRequest.addArgument("countryID", order.getSendToCustomer().getCountry());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_SUPER_INDEX);
                dataResponse = service.executeRequest(dataRequest);
                XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                // Pull the super index details for the selected country
                Element superIndexData = null;
                String  indexId = null;
                String xpath = "superIndexData/superIndexes/index";

                XPathQuery q = new XPathQuery();
                NodeList nl = q.query(xmlResponse, xpath);

                if (nl.getLength() > 0)
                {
                  superIndexData = (Element) nl.item(0);
                  indexId = superIndexData.getAttribute("indexId");
                }

                try {
                    SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

                    XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT LIST VO");
                    String pageNumber = (String) arguments.get("pageNumber");

                    // Initial search hit loads all products matching the query for all pages
                    if ( pageNumber == null || (pageNumber).equals("init" ) )
                    {
                        order.setCrumbURL("Product List", (String) arguments.get("url"));

                        // Get products by index args
                        dataRequest = new FTDDataRequest();
                        dataRequest.addArgument(ArgumentConstants.SEARCH_INDEX_ID, indexId);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                        dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, null);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, null);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, "I");
                        dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                        dataRequest.addArgument("scriptCode", order.getScriptCode());

                        // NEEDS TO BE CLEANED UP
                        OEParameters oeParmsFull = getGlobalParms();
                        order.setGlobalParameters(oeParmsFull);

                        // build the end date for the delivery date range
                        OEParameters oeParms = getGlobalParms(GeneralConstants.XML_SUPER_INDEX_DATA, xmlResponse);

                        Calendar deliveryEndDate = Calendar.getInstance();
                        deliveryEndDate.add(Calendar.DATE, DeliveryDateUTIL.getDeliveryDaysOut(order, oeParms));//oeParms.getDeliveryDaysOut());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_END_DATE, dateFormat.format(deliveryEndDate.getTime()));

                        service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);
                        dataResponse = service.executeRequest(dataRequest);
                        order.setSearchResults((XMLDocument)dataResponse.getDataVO().getData());

                        nl = null;
                        xpath = "productList/products/product";
                        q = new XPathQuery();
                        nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                        int pageCount = nl.getLength() / GeneralConstants.PRODUCTS_PER_PAGE;
                        if(nl.getLength() != (pageCount * GeneralConstants.PRODUCTS_PER_PAGE))
                            pageCount++;

                        order.setSearchPageCount(new Integer(pageCount));
                        order.setSearchCurrentPage(new Integer(1));

                        // Encode products to xml for product list
                        XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, order.getSearchResults(), order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
                    }
                    else
                    {
                        // Encode products to xml for product list
                        order.setSearchCurrentPage(new Integer(pageNumber));
                        XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, order.getSearchResults(), order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
                    }

                    // Load price points from database
                    dataRequest = new FTDDataRequest();
                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRICE_POINTS);
                    dataResponse = service.executeRequest(dataRequest);
                    XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

                    // Encode page data for product list
                    HashMap pageDataMap = new HashMap();
                    pageDataMap.put("totalPages", order.getSearchPageCount().toString());
                    pageDataMap.put("currentPage", order.getSearchCurrentPage().toString());
                    pageDataMap.put("rewardType", order.getRewardType());
                    pageDataMap.put("indexId", indexId);

                    XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

                    XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
                    XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());
                    //XMLEncoder.addSection(shoppingUTIL.transformBreadCrumbsToXML(order.getBreadCrumbList()).getChildNodes());

                    systemVO.setXML(document);

                } catch(Exception e) {
                    this.getLogManager().error(e.toString(), e);
                }
            }

        } catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private void removeItemFromCart(FTDArguments arguments)
	{
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            if ( (arguments.get("itemCartNumber") != null) &&
                 (((String)arguments.get("itemCartNumber")).length() > 0) )
            {
                if ( ((String) arguments.get("itemCartNumber")).equals("all") )
                {
                    Iterator itemIterator = order.getAllItems().keySet().iterator();
                    while(itemIterator.hasNext())
                    {
                         itemIterator.next();
                         itemIterator.remove();
                    }
                }
                else
                {
                    try
                    {
                        Integer itemCartNumber = new Integer((String) arguments.get("itemCartNumber"));
                        order.removeItem(itemCartNumber);
                    }
                    catch(Exception e) {
                        this.getLogManager().error("Could not remove item from cart.");
                    }
                }
            }
            else
            {
                // remove the current item
                try
                {
                    order.removeItem(new Integer(order.getCurrentCartNumber()));
                }
                catch(Exception e)
                {
                    this.getLogManager().error("Could not remove current item from cart.");
                }
            }
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
	}

    private FTDSystemVO getShoppingCart(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            ShoppingUTIL shoppingUtil = new ShoppingUTIL(this.getLogManager());
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
// *
            // Clean all breadcrumbs in order and clear out major object values for minimal storage
            // Remove expired breadcrumbs
            Iterator crumbIterator = order.getCrumbMap().keySet().iterator();
            while(crumbIterator.hasNext())
            {
               order.removeCrumbURL((String) crumbIterator.next());
            }

            order.setSearchResults(null);

            XMLDocument document = XMLEncoder.createXMLDocument("SHOPPING_CART");

            // Clear any invalid incomplete items from cart
            LinkedList invalidItemList = order.getAllInvalidItems();
            if(invalidItemList != null && invalidItemList.size() > 0) {
                Integer itemKey = null;
                Iterator invalidItemIterator = invalidItemList.iterator();
                while(invalidItemIterator.hasNext()) {
                    itemKey = (Integer) invalidItemIterator.next();
                    order.getAllItems().remove(itemKey);
                }
            }

            // Get global parameters and set them in the order
            order.setGlobalParameters(searchUTIL.getGlobalParms());

            // Build and encode cart
            XMLEncoder.addSection(document, shoppingUtil.transformCartToXML(order).getChildNodes());
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            systemVO.setXML(document);

        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO cancelOrder(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Set status to cancelled
            order.setOrderStatus(GeneralConstants.CANCEL);

            // If order contains items, persist as a non-order
            if(order.getAllItems() != null && order.getAllItems().size() > 0) {

                // Set order confirmation number
                //emueller, 2/18/04 - this line used to append "/#" to the master order number,
                //this was removed for defect 378
                order.setConfirmationNumber("M" + new Long(OEOrderUTIL.getUniqueNumber()).toString());

               //do not assign detail item numbers - emueller , 2/17/04
               // Iterator itemIterator = order.getAllItems().values().iterator();
               // while(itemIterator.hasNext()) {
               //     item = (ItemPO) itemIterator.next();
               //     item.setDetailItemNumber("C" + new Long(OEOrderUTIL.getUniqueNumber()).toString());
               // }

                // Set the processing time and transaction date
                long totalProcessingTime = System.currentTimeMillis() - order.getProcessingTime().longValue();
                order.setProcessingTime(new Long(totalProcessingTime));
                order.setTransactionDate(new Date(System.currentTimeMillis()));

                this.getLogManager().info("Saving cancelled order to scrub");

                ShoppingUTIL shoppingUTIL = new ShoppingUTIL(this.getLogManager());
                shoppingUTIL.cancelOrder(order);

                this.getLogManager().info("Non-Order is complete.  Total time:" + new BigDecimal(order.getProcessingTime().longValue() / 1000).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + " seconds");
            }

            // JMP 12/23/03 - Removed because we now need to save the order in the
            // session table for scrub
            // Clear database of all data no longer needed
            //order.emptyMapStorage();
            //order.getPromotionList().clear();
            //order.setSendToCustomer(null);
        }
        catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private void addItemToCart(FTDArguments arguments)
	{
        OrderPO order = null;
        BigDecimal feeWithFuelSurcharge;
        Connection connection = null;

        try {
            // Get fuel surcharge amount
            connection = DataManager.getInstance().getConnection();
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            ShoppingUTIL shoppingUtil = new ShoppingUTIL(this.getLogManager());
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            // Get Global parameters
            order.setGlobalParameters(searchUTIL.getGlobalParms());

            // Pull item from database for processing
            FTDDataRequest dataRequest = new FTDDataRequest();
            String productId = (String) arguments.get(ArgumentConstants.SEARCH_PRODUCT_ID);

            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, productId);
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }
            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            // reset the product exclusion display flags
            //order.setDisplayFloral(GeneralConstants.YES);
            //order.setDisplayNoProduct(GeneralConstants.YES);
            //order.setDisplaySpecialtyGift(GeneralConstants.YES);

/*  *****   */
            Element productElement = searchUTIL.pageBuilder(false, false, (XMLDocument)dataResponse.getDataVO().getData(), order, 1, null, null);
 /*  *****   */

            // Create instance of item for creation
            ItemPO item = new ItemPO();

            NodeList nodeList = productElement.getElementsByTagName("product");
            Element productDetailElement = (Element) nodeList.item(0);

            // Set item values
            item.setItemStatus(GeneralConstants.IN_PROCESS);
            item.setCustomItemFlag(false);
            item.setProductId(productDetailElement.getAttribute("productID"));
            item.setItemName(productDetailElement.getAttribute("productName"));
            item.setNovatorId(productDetailElement.getAttribute("novatorID"));
            item.setNovatorName(productDetailElement.getAttribute("novatorName"));
            item.setProductType(productDetailElement.getAttribute("productType"));
            item.setItemSKU(productDetailElement.getAttribute("vendorSKU"));
            item.setColorOne((String) arguments.get("COLOR1"));
            item.setColorTwo((String) arguments.get("COLOR2"));
            item.setExceptionCode(productDetailElement.getAttribute("exceptionCode"));
            item.setExceptionStartDate(productDetailElement.getAttribute("exceptionStartDate"));
            item.setExceptionEndDate(productDetailElement.getAttribute("exceptionEndDate"));
            item.setEgiftFlag((String) productDetailElement.getAttribute("egift"));
            item.setCodifiedSpecial((String) productDetailElement.getAttribute("codifiedSpecial"));

            // Default the product substitution to No unless the box is checked
            String substAuth = (String) arguments.get("secondChoiceAuth");
            if(substAuth == null)
            {
                substAuth = "N";
            }
            else if(substAuth.equals("true") || substAuth.equals("on"))
            {
                substAuth = "Y";
            }
            else
            {
                substAuth = "N";
            }
            item.setSubstitutionAuth(substAuth);

            //item.setDeliveryDates(order.getDeliveryDates());
            item.setOccasion(order.getOccasion());

            item.getSendToCustomer().setZipCode(order.getSendToCustomer().getZipCode());

            if ( (order.getSendToCustomer().getCountry() != null) &&
                 (!order.getSendToCustomer().getCountry().trim().equals("")) )
            {
                item.getSendToCustomer().setCountry(order.getSendToCustomer().getCountry());
            }

            item.setDeliveryDate(order.getDeliveryDate());
            item.setDeliveryDateDisplay(order.getDeliveryDateDisplay());

            if ( (arguments.get("subCodeId") != null) &&
                 (!((String) arguments.get("subCodeId")).trim().equals("")) )
            {
                item.setSubCodeSelected(Boolean.TRUE);
                item.setSubCodeId(((String) arguments.get("subCodeId")).trim());

                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("subCodeId", item.getSubCodeId());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_SUB_CODE);
                dataResponse = service.executeRequest(dataRequest);
                XMLDocument subCodeList = (XMLDocument)dataResponse.getDataVO().getData();

                Element subCodeElement = null;
                NodeList nl = null;
                String xpath = "subCodeList/subCode/data";
                XPathQuery q = new XPathQuery();

                nl = q.query(subCodeList, xpath);

                if (nl.getLength() > 0)
                {
                    subCodeElement = (Element) nl.item(0);
                    item.setSubCodeDescription(subCodeElement.getAttribute("subcodeDesc"));

                    try {
                        item.setSubCodePrice(new BigDecimal(subCodeElement.getAttribute("subcodePrice").trim()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                    } catch(Exception e) {
                        this.getLogManager().info("Subcode did not have a valid price associated with it.");

                        // Set the subcode price to the items regular price if subcode price does not exist
                        item.setSubCodePrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                    }

                    item.setSubCodeReferenceNumber(subCodeElement.getAttribute("subcodeRefNumber"));
                }
            }

            //String floristDelivered = (String) productDetailElement.getAttribute("shipMethodFlorist");
            String carrierDelivered = productDetailElement.getAttribute("shipMethodCarrier");
            item.setShippingMethod("");

            if (carrierDelivered.equals("Y"))
            {
                item.setShippingCarrier("COMMON_CARRIER");

                if(item.getProductType() != null && (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                    item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC))) {
                    if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null && order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) {
                        feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getGlobalParameters().getFreshCutSrvcCharge());
                        item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    }
                    else {
                        if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                        }
                        else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                        }
                    }

                    //Set the service fee for saturday fresh cuts
               		if( (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                         item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) ) &&
                    	order.getSendToCustomer().isDomesticFlag() && order.getDeliveryDate()!=null )
                    {
                    	Calendar cal = Calendar.getInstance();
        				cal.setTime(order.getDeliveryDate());
        				if( cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY  )
        				{
        					item.setExtraShippingFee(order.getGlobalParameters().getFreshCutSatCharge());
                    	}
                        else
                        {
                            item.setExtraShippingFee(new BigDecimal(0.00));
                        }
                    }
                    else
                    {
                        item.setExtraShippingFee(new BigDecimal(0.00));
                    }
                }
                else
                {
                    HashMap shippingMethods = searchUTIL.getProductShippingMethods(productDetailElement.getAttribute("productID"), productDetailElement.getAttribute("standardPrice"), productDetailElement.getAttribute("shippingKey"));
                    ShippingMethod shipMethod = (ShippingMethod)shippingMethods.get(item.getShippingMethod());
                    if(shipMethod != null)
                    {
                        try
                        {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(shipMethod.getDeliveryCharge());
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                        }
                        catch(Exception e) {
                            this.getLogManager().error("Product #" + item.getProductId() + " has shipping inconsistencies.  Could not find a valid delivery price for " + item.getShippingMethod() + " shipping.");
                        }
                    }
                }
            }
            else
            {
                item.setShippingCarrier("FLORIST");
                item.setShippingMethod("florist");
                if ( (order.getSendToCustomer().isDomesticFlag()) &&
                     (order.getDomesticServiceFee() != null) )
                {
                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
                else if ( (!order.getSendToCustomer().isDomesticFlag()) &&
                          (order.getInternationalServiceFee() != null) )
                {
                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
            }

            // If ship cost was never set put it to zero
            if(item.getShippingCost() == null) {
                item.setShippingCost(new BigDecimal(0));
            }

            if(order.getSendToCustomer().isDomesticFlag())
                item.getSendToCustomer().setCountryType(GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            else
                item.getSendToCustomer().setCountryType(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);

            // Calculate and set pricing for added item
            if(item.isSubCodeSelected())
            	item.setPriceType("standard");
            else
            	item.setPriceType((String) arguments.get("PRICE"));

			//remove code above
			//IF this is a sub code, then set the standard price and discounts to those of the subcode
            if(item.getPriceType().equals("variable")) {
                try {
                    item.setRegularPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    item.setDiscountedPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has problems processing variable price.");
                }
            }
            else if(item.isSubCodeSelected()) {
                try {
                    item.setRegularPrice(item.getSubCodePrice());
                    item.setDiscountedPrice(item.getSubCodePrice());
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has problems processing sub-code price.");
                }
            }
            else {
                try {
                    item.setRegularPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                    item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.");
                }
            }

            if(order.getRewardType() != null && !order.getRewardType().equals("")) {
                try {
                    if(!item.getPriceType().equals("variable") && !item.isSubCodeSelected()) {
                        item.setDiscountAmount(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "RewardValue")));
                        item.setDiscountDescription(order.getRewardType());

                        if(order.getRewardType().equals("Percent") || order.getRewardType().equals("Dollars")) {
                            if(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice") != null) {
                                item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice")));
                            }
                        }
                        else if(order.getRewardType().equals("Miles") || order.getRewardType().equals("Points")) {
                            order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + item.getDiscountAmount().longValue()));
                        }
                    }
                    else {
                        if(arguments.get("VARIABLE_PRICE_AMT") != null && !((String)arguments.get("VARIABLE_PRICE_AMT")).equals("")) {
                            String discountAmount = "0";

                            dataRequest = new FTDDataRequest();
                            dataRequest.addArgument("inPriceHeaderId", order.getPricingCode());
                            dataRequest.addArgument("inItemPrice", (String)arguments.get("VARIABLE_PRICE_AMT"));

                            // Determine if its a partner or standard discount
                            if ( (order.getPartnerId() != null) &&
                                 (!order.getPartnerId().equals("")) &&
                                 (order.getPricingCode() != null) &&
                                 (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
                            {
                                item.setDiscountAmount(new BigDecimal(searchUTIL.retrievePromotionValue(order.getPromotionList(), item.getRegularPrice().setScale(0, BigDecimal.ROUND_UP).doubleValue())));
                            }
                            else
                            {
                                service =  this.getGenericDataService(DataConstants.SEARCH_GET_DISCOUNT_AMOUNT);
                                dataResponse = service.executeRequest(dataRequest);
                                XMLDocument discountAmountList = (XMLDocument)dataResponse.getDataVO().getData();

                                Element discountAmountElement = null;
                                NodeList nl = null;
                                String xpath = "discountAmountList/discountAmount/data";
                                XPathQuery q = new XPathQuery();

                                nl = q.query(discountAmountList, xpath);

                                if (nl != null && nl.getLength() > 0)
                                {
                                    discountAmountElement = (Element) nl.item(0);
                                    discountAmount = discountAmountElement.getAttribute("discountAmount");
                                }
                                else
                                {
                                    this.getLogManager().error("Could not find discount amount for the variable price.");
                                }
                                item.setDiscountAmount(new BigDecimal(discountAmount));
                            }

                            item.setDiscountDescription(order.getRewardType());

                            if(order.getRewardType().equals("Percent")) {
                                item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal((item.getRegularPrice().doubleValue() * (Double.parseDouble(discountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                            }
                            else if(order.getRewardType().equals("Dollars")) {
                                item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal(discountAmount).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                            }
                            else if(order.getRewardType().equals("Miles") || order.getRewardType().trim().equals("Points")) {
                                order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + item.getDiscountAmount().longValue()));

                            }
                        }
                    }
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.", e);
                }
            }

            // Get list of possible add ons for the occasion
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("occasionIn", order.getOccasion());

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_ADDON_LIST);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument addOnList = (XMLDocument)dataResponse.getDataVO().getData();

            // Set the selected add ons to the item
            AddOnPO addOn = null;

            try {
                if(arguments.get("balloon") != null && !((String)arguments.get("balloon")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("balloon"), new Integer((String) arguments.get("balloonQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("bear") != null && !((String)arguments.get("bear")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("bear"), new Integer((String) arguments.get("bearQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("chocolate") != null && !((String)arguments.get("chocolate")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("chocolate"), new Integer((String) arguments.get("chocolateQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("banner") != null && !((String)arguments.get("banner")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("banner"), new Integer((String) arguments.get("bannerQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("card") != null && !((String)arguments.get("card")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("card"), new Integer(1), addOnList);
                    item.addAddOn(addOn);
                }
            }
            catch(Exception e) {
                this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has problems with addons...", e);
            }

            // Add item to order
            order.addItem(item);

            if(item.isSubCodeSelected()) {
                this.updateSourceCode(order, arguments);
            }
        }
        catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        	this.returnPersistentServiceObject(order);
        }
	}


    private void addCustomItemToCart(FTDArguments arguments)
	{
        Connection connection = null;
        OrderPO order = null;
        BigDecimal feeWithFuelSurcharge;

        try {
            // Get fuel surcharge amount
            connection = DataManager.getInstance().getConnection();
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            order.setDisplayProductUnavailable("Y");

            // Pull item from database for processing
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("productId", GeneralConstants.CUSTOM_ORDER_PID);
            dataRequest.addArgument("sourceCode", order.getSourceCode());
            dataRequest.addArgument("zipCode", order.getSendToCustomer().getZipCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());

            if ( order.getSendToCustomer().isDomesticFlag() )
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            else
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);

            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            Element productElement = searchUTIL.pageBuilder(false, false, (XMLDocument)dataResponse.getDataVO().getData(), order, 1, null, null);

            // Create instance of item for creation
            ItemPO item = new ItemPO();

            NodeList nodeList = productElement.getElementsByTagName("product");
            Element productDetailElement = (Element) nodeList.item(0);

            // Set item values
            item.setItemStatus(GeneralConstants.IN_PROCESS);
            item.setCustomItemFlag(true);
            item.setProductId(productDetailElement.getAttribute("productID"));
            item.setItemName(productDetailElement.getAttribute("productName"));
            item.setNovatorId(productDetailElement.getAttribute("novatorID"));
            item.setNovatorName(productDetailElement.getAttribute("novatorName"));
            item.setItemSKU(productDetailElement.getAttribute("vendorSKU"));
            item.setPriceType((String) arguments.get("price"));
            item.setEgiftFlag((String) productDetailElement.getAttribute("egift"));

            // Set comments for custom item
            item.setItemComments(FieldUtils.replaceAll((String) arguments.get("comments"), GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT));
            item.setFloristComments(FieldUtils.replaceAll((String) arguments.get("desc"), GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT));

            item.setOccasion(order.getOccasion());
            //item.setDeliveryDates(order.getDeliveryDates());
            item.getSendToCustomer().setZipCode(order.getSendToCustomer().getZipCode());

            if(order.getSendToCustomer().getCountry() != null && !order.getSendToCustomer().getCountry().trim().equals("")) {
                item.getSendToCustomer().setCountry(order.getSendToCustomer().getCountry());
            }

            if(order.getDeliveryDate() != null) {
                item.setDeliveryDate(order.getDeliveryDate());
                item.setDeliveryDateDisplay(order.getDeliveryDateDisplay());
            }

            // Set shipping charges and method
            item.setShippingMethod("florist");

            if(order.getSendToCustomer().isDomesticFlag() && order.getDomesticServiceFee() != null) {
                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
            }
            else if(!order.getSendToCustomer().isDomesticFlag() && order.getInternationalServiceFee() != null) {
                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
            }

            // If ship cost was never set put it to zero
            if(item.getShippingCost() == null) {
                item.setShippingCost(new BigDecimal(0));
            }

            if(order.getSendToCustomer().isDomesticFlag())
                item.getSendToCustomer().setCountryType("D");
            else
                item.getSendToCustomer().setCountryType("I");

            // Calculate and set pricing for added item
            /*if(!item.getPriceType().equals("variable")) {
                try {
                    item.setItemRegularPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                    item.setItemDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.");
                }
            }
            else if(arguments.get("variablePriceAmount") != null && !((String)arguments.get("variablePriceAmount")).equals("")) {
                try {
                    item.setItemRegularPrice(new BigDecimal((String) arguments.get("variablePriceAmount")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    item.setItemDiscountedPrice(new BigDecimal((String) arguments.get("variablePriceAmount")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has problems processing variable price.");
                }
            }*/


            // Calculate and set pricing for added item
            item.setPriceType((String) arguments.get("PRICE"));

            if(item.getPriceType().equals("variable")) {
                try {
                    item.setRegularPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    item.setDiscountedPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has problems processing variable price.");
                }
            }
            else if(item.isSubCodeSelected()) {
                try {
                    item.setRegularPrice(item.getSubCodePrice());
                    item.setDiscountedPrice(item.getSubCodePrice());
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has problems processing sub-code price.");
                }
            }
            else {
                try {
                    item.setRegularPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                    item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.");
                }
            }

            if(order.getRewardType() != null && !order.getRewardType().equals("")) {
                try {
                    if(!item.getPriceType().equals("variable") && !item.isSubCodeSelected()) {
                        item.setDiscountAmount(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "RewardValue")));
                        item.setDiscountDescription(order.getRewardType());

                        if(order.getRewardType().equals("Percent") || order.getRewardType().equals("Dollars")) {
                            if(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice") != null) {
                                item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice")));
                            }
                        }
                        else if(order.getRewardType().equals("Miles") || order.getRewardType().equals("Points")) {
                            order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + item.getDiscountAmount().longValue()));
                        }
                    }
                    else {
                        if(arguments.get("VARIABLE_PRICE_AMT") != null && !((String)arguments.get("VARIABLE_PRICE_AMT")).equals("")) {
                            String discountAmount = "0";

                            dataRequest = new FTDDataRequest();
                            dataRequest.addArgument("inPriceHeaderId", order.getPricingCode());
                            dataRequest.addArgument("inItemPrice", (String)arguments.get("VARIABLE_PRICE_AMT"));

                            // Determine if its a partner or standard discount
                            if ( (order.getPartnerId() != null) &&
                                 (!order.getPartnerId().equals("")) &&
                                 (order.getPricingCode() != null) &&
                                 (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
                            {
                                item.setDiscountAmount(new BigDecimal(searchUTIL.retrievePromotionValue(order.getPromotionList(), item.getRegularPrice().setScale(0, BigDecimal.ROUND_UP).doubleValue())));
                            }
                            else
                            {
                                service =  this.getGenericDataService(DataConstants.SEARCH_GET_DISCOUNT_AMOUNT);
                                dataResponse = service.executeRequest(dataRequest);
                                XMLDocument discountAmountList = (XMLDocument)dataResponse.getDataVO().getData();

                                Element discountAmountElement = null;
                                NodeList nl = null;
                                String xpath = "discountAmountList/discountAmount/data";
                                XPathQuery q = new XPathQuery();

                                nl = q.query(discountAmountList, xpath);

                                if (nl != null && nl.getLength() > 0)
                                {
                                    discountAmountElement = (Element) nl.item(0);
                                    discountAmount = discountAmountElement.getAttribute("discountAmount");
                                }
                                else
                                {
                                    this.getLogManager().error("Could not find discount amount for the variable price.");
                                }
                                item.setDiscountAmount(new BigDecimal(discountAmount));
                            }

                            item.setDiscountDescription(order.getRewardType());

                            if(order.getRewardType().equals("Percent")) {
                                item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal((item.getRegularPrice().doubleValue() * (Double.parseDouble(discountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                            }
                            else if(order.getRewardType().equals("Dollars")) {
                                item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal(discountAmount).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                            }
                            else if(order.getRewardType().equals("Miles") || order.getRewardType().trim().equals("Points")) {
                                order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + item.getDiscountAmount().longValue()));

                            }
                        }
                    }
                }
                catch(Exception e) {
                    this.getLogManager().error("ERROR:  Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.", e);
                }
            }


            order.addItem(item);

        }
        catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        }
	}

    private FTDSystemVO loadDeliveryInfo(FTDArguments arguments)
	{
        Date timerDate = new Date();
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
            DeliveryDateUTIL deliveryDateUTIL = new DeliveryDateUTIL(this.getLogManager());
            ItemPO item = null;
            String zipCode = null;
            String domesticIntlFlag = null;
            String countryId = null;
            String state = null;
            String city = null;
            String itemCartNumber = (String)arguments.get("cartItemNumber");

            // Check if its an edit or new item
            if ( (itemCartNumber != null) &&
                 (!itemCartNumber.equals("")) )
            {
                item = order.getItem(new Integer((String)arguments.get("cartItemNumber")));

                // info from item
                zipCode = item.getSendToCustomer().getZipCode();
                countryId = item.getSendToCustomer().getCountry();
                state = item.getSendToCustomer().getState();
                city = item.getSendToCustomer().getCity();
            }
            else
            {
                item = order.getItem(new Integer(order.getCurrentCartNumber()));

                // info from order
                zipCode = order.getSendToCustomer().getZipCode();
                countryId = order.getSendToCustomer().getCountry();
                state = order.getSendToCustomer().getState();
                city = order.getSendToCustomer().getCity();
            }
            domesticIntlFlag = item.getSendToCustomer().getCountryType();

            ShoppingUTIL shoppingUtil = new ShoppingUTIL(this.getLogManager());

            order.setCrumbURL("Delivery", (String) arguments.get("url"));
            String maxDeliveryDaysOut = (String) arguments.get("maxDeliveryDaysOut");
            if (maxDeliveryDaysOut != null &&  maxDeliveryDaysOut.equals("true"))
            {
              order.setDeliveryDaysOut(ArgumentConstants.DELIVERY_DAYS_OUT_MAX);
            }

            XMLDocument document = XMLEncoder.createXMLDocument("DELIVERY INFO PAGE VO");

            // Load lookups for delivery page
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, zipCode);
            dataRequest.addArgument("dnisType", order.getScriptCode());

            // Params for Product lookup
            dataRequest.addArgument("productId", item.getProductId());
            dataRequest.addArgument("sourceCode", order.getSourceCode());
            dataRequest.addArgument("domesticIntlFlag", domesticIntlFlag);
            dataRequest.addArgument("zipCode", zipCode);
            dataRequest.addArgument("countryId", countryId);

            dataRequest.addArgument("customerId", null);
            dataRequest.addArgument("institutionName", null);
            dataRequest.addArgument("address1",  null);
            dataRequest.addArgument("address2",  null);
            dataRequest.addArgument("inCity",  null);
            dataRequest.addArgument("inState",  null);
            dataRequest.addArgument("inZipCode",  null);
            dataRequest.addArgument("inPhone",  null);

            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SHOPPING_GET_DELIVERY_INFO_DATA);
            FTDDataResponse deliveryDataResponse = service.executeRequest(dataRequest);

            XMLEncoder.addSection(document, ((XMLDocument)deliveryDataResponse.getDataVO().getData()).getChildNodes());

            // Was the recipient lookup used?
            String recipientId = order.getSendToCustomer().getHpCustomerId();
            if( recipientId != null && recipientId.length() > 0 )
            {
                item.getSendToCustomer().setFirstName(order.getSendToCustomer().getFirstName());
                item.getSendToCustomer().setLastName(order.getSendToCustomer().getLastName());
                item.getSendToCustomer().setAddressOne(order.getSendToCustomer().getAddressOne());
                item.getSendToCustomer().setAddressTwo(order.getSendToCustomer().getAddressTwo());
                item.getSendToCustomer().setCity(order.getSendToCustomer().getCity());
                item.getSendToCustomer().setState(order.getSendToCustomer().getState());
                item.getSendToCustomer().setPhoneExtension(order.getSendToCustomer().getPhoneExtension());
                item.getSendToCustomer().setCompanyName(order.getSendToCustomer().getCompanyName());
                item.getSendToCustomer().setCompanyInfo(order.getSendToCustomer().getCompanyInfo());
                item.getSendToCustomer().setCompanyType(order.getSendToCustomer().getCompanyType());
                item.setInstitutionInfo(order.getSendToCustomer().getCompanyInfo());
                item.setInstitutionName(order.getSendToCustomer().getCompanyName());
                item.setInstitutionType(order.getSendToCustomer().getCompanyType());

            }


            // Init display variables
            String displayInstitution = GeneralConstants.YES;
            String displayInstitutionLookUp = GeneralConstants.YES;
            String displayRoomNumber = GeneralConstants.YES;
            String displayWorkingHours = GeneralConstants.YES;
            String displayReleaseCheckbox = GeneralConstants.YES;
            String displayDeliveryDate = GeneralConstants.YES;
            String displaySpecialFee = GeneralConstants.NO;

            // set company type in two places based on occasion
            if ( item.getInstitutionType() == null || item.getInstitutionType().equalsIgnoreCase(""))
            {
                if ( order.getOccasion().equals(GeneralConstants.OE_OCCASION_FUNERAL) )
                {
                    item.setInstitutionType("F");
                    item.getSendToCustomer().setCompanyType("F");
                }
                else
                {
                    item.setInstitutionType("R");
                    item.getSendToCustomer().setCompanyType("R");
                }
            }

            // Set page flags based on shipping type
            if ( item.getInstitutionType().equals("B") )
            {
                displayInstitutionLookUp = GeneralConstants.NO;
                displayReleaseCheckbox = GeneralConstants.NO;
            }
            else if ( item.getInstitutionType().equals("R") || item.getInstitutionType().equals("O") )
            {
                displayInstitution = GeneralConstants.NO;
                displayInstitutionLookUp = GeneralConstants.NO;
                displayRoomNumber = GeneralConstants.NO;
                displayWorkingHours = GeneralConstants.NO;
                displayReleaseCheckbox = GeneralConstants.NO;
            }
            else if ( item.getInstitutionType().equals("H") ||  item.getInstitutionType().equals(GeneralConstants.NO) )
            {
                displayWorkingHours = GeneralConstants.NO;
            }

            if ( (item.getProductType() != null) &&
                 (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                  item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)) ) {
                HashMap delayDataMap = new HashMap();
                delayDataMap.put("H", GeneralConstants.YES);
                delayDataMap.put("N", GeneralConstants.YES);
                delayDataMap.put("F", GeneralConstants.YES);

                XMLEncoder.addSection(document, "delayMessage", "location", delayDataMap);
            }

            // Show delivery types if not same day gift or florist delivered
            if(!item.getShippingMethod().equalsIgnoreCase("florist") ||
                (item.getProductType() != null &&
                 (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) ||
                  item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG)))) {
                displayDeliveryDate = GeneralConstants.NO;
            }

            // Load state from zip code
            if ( (zipCode != null) &&
                 (!zipCode.trim().equals("")) )
            {
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("zipIn", zipCode);
                dataRequest.addArgument("cityIn", null);
                dataRequest.addArgument("stateIn", null);
                dataRequest.addArgument("dnisType", order.getScriptCode());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_ZIP_CODE_DETAIL);
                FTDDataResponse dataResponse = service.executeRequest(dataRequest);

                XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                Element zipCodeElement = null;
                NodeList nl = null;
                String xpath = "zipCodeLookup/searchResults/searchResult";
                XPathQuery q = new XPathQuery();

                nl = q.query(xmlResponse, xpath);
                if (nl.getLength() > 0)
                {
                    zipCodeElement = (Element) nl.item(0);
                    item.getSendToCustomer().setZipCode(zipCodeElement.getAttribute("zipCode"));
                    item.getSendToCustomer().setState(zipCodeElement.getAttribute("state"));

                    if ( nl.getLength() == 1 )
                    {
                        item.getSendToCustomer().setCity(zipCodeElement.getAttribute("city"));
                    }
                    else
                    {
                        item.getSendToCustomer().setCity(city);
                    }
                }
            }

            if ( (state != null) &&
                 (state.equals("AK") ||
                  state.equals("HI")) )
            {
                if ( (item.getProductType() != null) &&
                     (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) )
                {
                    displaySpecialFee = GeneralConstants.YES;
                }
            }

            // Get the product details

            FTDDataResponse dataResponse = null;
            XPathQuery q = null;
            NodeList nl = null;
            String xpath = null;

            // get product information
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, item.getProductId());
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, zipCode);
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, countryId);
            if ( domesticIntlFlag == null || domesticIntlFlag.equals("D") )
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }
            deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            FTDDataAccessManager dam = new FTDDataAccessManager();
            service =  dam.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument productList = (XMLDocument)dataResponse.getDataVO().getData();

            this.getLogManager().debug("LOAD_DELIVERY_INFO before date clac " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");

            List deliveryDates = searchUTIL.getItemDeliveryDates(productList, item.getProductId(),
                                                item.getSubCodeId(), order, false);

            // Do not allow freshcuts to be shipped to Canada
            if(
                (   countryId != null && countryId.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ) ||
                (   (state != null && ((state.equals(GeneralConstants.STATE_CODE_PUERTO_RICO))
                    || state.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS))) ) // if country is canada or state == Puerto_Rico or Virgin islands
              )
            {

                if ( (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                      item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) )
                {
                    order.setDisplayProductUnavailable("Y");
                }
            }

            // Set required delivery date parameter fields
            OEDeliveryDateParm delParms = new OEDeliveryDateParm();
            delParms.setProductId(item.getProductId());
            delParms.setProductType(item.getProductType());
            delParms.setOrder(order);

/*
            // get floral service charge
            String xpath2 = "productList/products/product[@productID = '" + item.getProductId() + "']";
            XPathQuery q2 = new XPathQuery();
            NodeList nl2 = q2.query(productList, xpath2);
            String serviceCharge = "";
            Element element = null;
            if (nl2.getLength() > 0)
            {
                element = (Element)nl2.item(0);
                serviceCharge = element.getAttribute("serviceCharge");
                if(serviceCharge != null)
                {
                    delParms.setSameDayGiftCharge(new BigDecimal(serviceCharge));
                }
            }
*/
            XMLDocument deliveryDatesXML = deliveryDateUTIL.getXMLEncodedDeliveryDates(deliveryDates, delParms);
            deliveryDatesXML =  shoppingUtil.formatDeliveryDatesXML(deliveryDatesXML);

            this.getLogManager().debug("LOAD_DELIVERY_INFO after date clac " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");

            // Build data for page displays
            HashMap pageDataMap = new HashMap();
            pageDataMap.put("cartItemNumber", item.getCartNumber().toString());
            pageDataMap.put("displayInstitution", displayInstitution);
            pageDataMap.put("displayInstitutionLookUp", displayInstitutionLookUp);
            pageDataMap.put("displayRoomNumber", displayRoomNumber);
            pageDataMap.put("displayWorkingHours", displayWorkingHours);
            pageDataMap.put("displayReleaseCheckbox", displayReleaseCheckbox);
            pageDataMap.put("displayDeliveryDate", displayDeliveryDate);
            pageDataMap.put("displaySpecialFee", displaySpecialFee);

            // Show any error popups that are needed
            pageDataMap.put("displayFloristPopup", order.getDisplayFloral());
            pageDataMap.put("displayNoProductPopup", order.getDisplayNoProduct());
            pageDataMap.put("displaySpecGiftPopup", order.getDisplaySpecialtyGift());
            pageDataMap.put("displayNoCodifiedFloristHasCommonCarrier", order.getDisplayNoCodifiedFloristHasCommonCarrier());
            pageDataMap.put("displayNoFloristHasCommonCarrier", order.getDisplayNoFloristHasCommonCarrier());
            pageDataMap.put("displayNoCodifiedFloristHasTwoDayDeliveryOnly", order.getDisplayCodifiedFloristHasTwoDayDeliveryOnly());

            //Put the earliest date out in first avail node
            pageDataMap.put("firstAvailableDate", order.getFirstAvailableDate());

            if(item.getProductType()!= null &&
               (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
            {
                String deliveryMethod = (String)arguments.get("deliveryMethod");
                if(deliveryMethod == null) deliveryMethod = "";
                if(arguments.get("source") == null || !arguments.get("source").equals("zipChange"))
                {
                    // If zip code has not changed then do not show Two Day delivery DIV
                    pageDataMap.put("displayNoCodifiedFloristHasTwoDayDeliveryOnly", "N");
                }
                else if(arguments.get("source") != null && arguments.get("source").equals("zipChange"))
                {
                    String displayCodifiedFloristHasTwoDayDeliveryOnly = order.getDisplayCodifiedFloristHasTwoDayDeliveryOnly();
                    // If code has changed
                    if( deliveryMethod.equals("Florist Delivery") && displayCodifiedFloristHasTwoDayDeliveryOnly != null &&
                       displayCodifiedFloristHasTwoDayDeliveryOnly.equals(GeneralConstants.YES))
                    {
                        // Show the florist has been selected but one is not available so use
                        // Two Day delivery instead DIV
                        pageDataMap.put("displayFloristSelectedButNotAvailable", GeneralConstants.YES);
                        pageDataMap.put("displayNoCodifiedFloristHasTwoDayDeliveryOnly", GeneralConstants.NO);
                    }
                    if(!deliveryMethod.equals(GeneralConstants.DELIVERY_NEXT_DAY) && displayCodifiedFloristHasTwoDayDeliveryOnly != null &&
                       displayCodifiedFloristHasTwoDayDeliveryOnly.equals(GeneralConstants.YES))
                    {
                        // Do not show the displayNoCodifiedFloristHasTwoDayDeliveryOnly DIV because Next Day was not
                        // selected
                        pageDataMap.put("displayNoCodifiedFloristHasTwoDayDeliveryOnly", GeneralConstants.NO);
                    }
                }
            }

            // indicates whether product is codified special and should be displayed
            pageDataMap.put("displayCodifiedSpecial", getDisplayCodifiedSpecial("deliveryData" ,deliveryDataResponse, item));
            if(!((String)pageDataMap.get("displayCodifiedSpecial")).equals("Y"))
            {
                pageDataMap.put("displayProductUnavailable", order.getDisplayProductUnavailable());
            }

            // Check if it is an upsell
            if(arguments.get("upsellFlag") != null && ((String)arguments.get("upsellFlag")).equals("Y"))
            {
                String firstAvailableDate = null;
                String zipGnaddFlag = null;
                Element upsellElement = null;
                xpath = "shippingData/deliveryDates/deliveryDate";
                q = new XPathQuery();

                // Pull the date for check against base date
                nl = q.query(deliveryDatesXML, xpath);
                if (nl.getLength() > 0)
                {
                    upsellElement = (Element) nl.item(0);
                    firstAvailableDate = upsellElement.getAttribute("date");
                }

                // Pull the gnadd flag for check if gnadd is on for the zip
                xpath = "deliveryData/zipCodeData/data";
                nl = q.query((XMLDocument)deliveryDataResponse.getDataVO().getData(), xpath);

                if (nl.getLength() > 0)
                {
                    upsellElement = (Element) nl.item(0);
                    zipGnaddFlag = upsellElement.getAttribute("zipGnaddFlag").trim();
                }

                // Check all conditions to see if base product is avail before gnadd
                if(order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_CARRIER) != null
                    && order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_CARRIER).equals("Y")
                    && order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_ID) != null
                    && !order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_ID).equals(item.getProductId())
                    && order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE) != null
                    && firstAvailableDate != null
                    && zipGnaddFlag != null
                    && zipGnaddFlag.equals("Y"))
                {
                    if (order.getSendToCustomer().isDomesticFlag()
                          && countryId != null
                          && !countryId.equals("CA")
                          && !countryId.equals("CAN")
                          && !countryId.equals("PR")
                          && !countryId.equals("VI"))
                    {
                        Date upsellBaseDate = FieldUtils.formatStringToUtilDate(order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE));
                        Date firstAvailDate = FieldUtils.formatStringToUtilDate(firstAvailableDate);

                        if(upsellBaseDate.before(firstAvailDate))
                        {
                            pageDataMap.put("displayUpsellGnaddSpecial", "Y");
                            pageDataMap.put("upsellBaseId", order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_ID));
                            pageDataMap.put("upsellBaseName", order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_NAME));
                            pageDataMap.put("upsellBaseDeliveryDate", order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE));
                        }
                    }
                }
            }

            // if the order phone number is filled in then set it on the item.
            // The order phone number is set on the occasion page
            String homePhone = order.getSendToCustomer().getHomePhone();
            if((homePhone != null && homePhone.length() > 0) && (itemCartNumber != null && itemCartNumber.length() < 1))
            {
                item.getSendToCustomer().setHomePhone(order.getSendToCustomer().getHomePhone());
            }

            // Build data for product detail
            HashMap productDetailMap = new HashMap();
            productDetailMap.put("novatorName", item.getNovatorName());
            productDetailMap.put("productId", item.getProductId());
            productDetailMap.put("selectedCountryOnlyItem", GeneralConstants.NO);
            productDetailMap.put("egift", item.getEgiftFlag());

            // Build Order data
            HashMap orderDataMap = new HashMap();
            // Added so we can go back to the shopping page
            if ( order.getDeliveryDateDisplay() != null )
            {
                orderDataMap.put("requestedDeliveryDateDisplay", order.getDeliveryDateDisplay());
            }
            orderDataMap.put("occasionDescription", searchUTIL.getDescriptionForCode(order.getOccasion(), SearchUTIL.GET_OCCASION_DESCRIPTION));
            orderDataMap.put("occasion", order.getOccasion());

            // set the default values for the Occasion page
            HashMap occasionData = new HashMap();
            occasionData.put("occasion", order.getOccasion());
            occasionData.put("country", countryId);
            occasionData.put("zip", zipCode);
            occasionData.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            occasionData.put("deliveryDaysOut", order.getDeliveryDaysOut());

            if ( order.getDeliveryDate() != null )
            {
                occasionData.put("deliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            else
            {
                occasionData.put("deliveryDate", "");
            }

            this.getLogManager().debug("LOAD_DELIVERY_INFO before page encode " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");
            // Encode delivery page
            XMLEncoder.addSection(document, shoppingUtil.transformRecipientToXML(item).getChildNodes());
            XMLEncoder.addSection(document, shoppingUtil.transformDeliveryInfoToXML(item).getChildNodes());
            XMLEncoder.addSection(document, deliveryDatesXML.getChildNodes());
            XMLEncoder.addSection(document, "productData", "data", productDetailMap);
            XMLEncoder.addSection(document, "pageData", "data", pageDataMap);
            XMLEncoder.addSection(document, "orderData", "data", orderDataMap);
            XMLEncoder.addSection(document, "selectedData", "data", occasionData);
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());

            //set flag to prevent zip code from occasion bread crumb from being used
            String ocUrl = order.getCrumbURL("Occasion");
            if(ocUrl != null)
            {
                //used to prevent flag from being concatenated more than once
                ocUrl = order.getCrumbURL("occasionChangedZip");
                ocUrl = ocUrl.concat("&updzip=N");
                order.removeCrumbURL("Occasion");
                order.setCrumbURL("Occasion", ocUrl);
            }

            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            // output the product exception information which is used to
            // notify the user when the product is available
            HashMap zipCodeMap = new HashMap();
            zipCodeMap.put("exceptionCode", item.getExceptionCode());
            zipCodeMap.put("exceptionStartDate", item.getExceptionStartDate());
            zipCodeMap.put("exceptionEndDate", item.getExceptionEndDate());
            XMLEncoder.addSection(document, "exceptionData", "data", zipCodeMap);

            // For Same Day Gift products only show US and Canada countries and their states
            if(item.getProductType() != null &&
               (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) ||
                item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG)))
            {
                XPathQuery query = new XPathQuery();
                // Remove all countries except US and Canada
                NodeList countryNodes = query.query(document, "root/deliveryData/countries/country");
                Element countryNode = null;
                for(int i = 0; i < countryNodes.getLength(); i++)
                {
                    countryNode = (Element)countryNodes.item(i);
                    if(countryNode.getAttribute("countryId").indexOf("US") < 0 &&
                       countryNode.getAttribute("countryId").indexOf("CA") < 0)
                    {
                        countryNode.getParentNode().removeChild(countryNode);
                    }
                }

/*
                // Loop through all delivery dates to see if this is a florist or
                // carrier delivered item

                NodeList deliveryDateNodes = query.query(document, "root/shippingData/deliveryDates/deliveryDate");
                Element dateNode = null;
                boolean floristDelivered = false;
                for(int i = 0; i < deliveryDateNodes.getLength(); i++)
                {
                    dateNode = (Element)deliveryDateNodes.item(i);
                    if(dateNode.getAttribute("types").indexOf("FL:") > -1)
                    {
                        // This is a florist delivered item
                        floristDelivered = true;
                        break;
                    }
                }
*/
                boolean floristDelivered = false;
                List shipMethods = searchUTIL.getDeliveryMethodsFromDateList(deliveryDates);
                if(shipMethods.contains("FL"))
                {
                    floristDelivered = true;
                }

                if(floristDelivered)
                {
                    // If the item is florist delivered then show all 50 states, VI and PR
                }
                else
                {
                    // If the item is carrier delivered only then show only 48 states
                    NodeList stateNodes = query.query(document, "root/deliveryData/states/state");
                    Element stateNode = null;
                    for(int i = 0; i < stateNodes.getLength(); i++)
                    {
                        stateNode = (Element)stateNodes.item(i);
                        if(stateNode.getAttribute("countryCode").indexOf("CAN") > -1 ||
                           stateNode.getAttribute("stateMasterId").indexOf("PR") > -1 ||
                           stateNode.getAttribute("stateMasterId").indexOf("VI") > -1)
                        {
                            stateNode.getParentNode().removeChild(stateNode);
                        }
                    }
                }
            }

            systemVO.setXML(document);
            this.getLogManager().debug("LOAD_DELIVERY_INFO end " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private void updateDeliveryInfo(FTDArguments arguments)
	{
        OrderPO order = null;
        BigDecimal feeWithFuelSurcharge;
        Connection connection = null;

        try
        {
            // Get fuel surcharge amount
            connection = DataManager.getInstance().getConnection();
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
            // Get Global parameters
            order.setGlobalParameters(searchUTIL.getGlobalParms());

            ItemPO item = null;
            FTDDataRequest dataRequest = null;
            FTDDataResponse dataResponse = null;
            GenericDataService service =  null;
            String xpath = null;
            NodeList nl = null;
            XPathQuery q = null;

            // Check if its an edit or new item
            if ( (arguments.get("cartItemNumber") != null) &&
                 (!((String)arguments.get("cartItemNumber")).equals("")) )
            {
                item = order.getItem(new Integer((String)arguments.get("cartItemNumber")));
            }
            else
            {
                item = order.getItem(new Integer(order.getCurrentCartNumber()));
            }

            // Set all delivery info to item

            /*  ***********************************************************
                    This section checks to see if the country does not
                    match the country associated with the entered zip code
                    and changes it if it does not
                *********************************************************** */
            String zipCode = (String) arguments.get("zipCode");
            String countryCode = (String) arguments.get("country");
            String countryId = item.getSendToCustomer().getCountry();

            // Zip Code or Country changed, reset popup flags
            if ( (zipCode != null) &&
                 (item.getSendToCustomer().getZipCode() == null) )
            {
                order.setDisplayFloral(GeneralConstants.YES);
                order.setDisplayNoProduct(GeneralConstants.YES);
                order.setDisplaySpecialtyGift(GeneralConstants.YES);
            }
            else if ( (zipCode != null) &&
                      (item.getSendToCustomer().getZipCode() != null) &&
                      (!zipCode.equals(item.getSendToCustomer().getZipCode())) )
            {
                order.setDisplayFloral(GeneralConstants.YES);
                order.setDisplayNoProduct(GeneralConstants.YES);
                order.setDisplaySpecialtyGift(GeneralConstants.YES);
            }

            /*  *************************************************************
                    This section retrievs the global parameter and the
                    selected zip code values
                *************************************************************   */
            String zipGnaddFlag = null;
            String zipFloralFlag = null;
            String zipGotoFloristFlag = null;
            String stateId = item.getSendToCustomer().getState();

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("zipCode", zipCode);

            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_TIMEZONE_DATA);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

            OEParameters globalParms = getGlobalParms("timezoneDataSet", xmlResponse);

            // Get the timezone from the xml for processing delivery dates
            Element timezoneElement = null;
            xpath = "timezoneDataSet/timezoneData/data";
            q = new XPathQuery();

            nl = q.query(xmlResponse, xpath);
            if ( nl.getLength() > 0 )
            {
                timezoneElement = (Element) nl.item(0);

                zipGnaddFlag = timezoneElement.getAttribute("zipGnaddFlag").trim();
                zipFloralFlag = timezoneElement.getAttribute("zipFloralFlag").trim();
                zipGotoFloristFlag = timezoneElement.getAttribute("zipGotoFloristFlag").trim();
                stateId = timezoneElement.getAttribute("stateId");
                countryCode = timezoneElement.getAttribute("countryCode");
            }
            else
            {
                zipGnaddFlag = GeneralConstants.NO;
                zipFloralFlag = GeneralConstants.YES;  // Assume floral delivery avail for zip
                zipGotoFloristFlag = GeneralConstants.YES;
            }

            // Canadians have the LNL format
            if (  zipCode != null
                  && zipCode.length() > 3
                  && Character.isLetter(zipCode.charAt(0))
                  && Character.isDigit(zipCode.charAt(1))
                  && Character.isLetter(zipCode.charAt(2)))
            {
                countryCode = GeneralConstants.COUNTRY_CODE_CANADA_CA;
            }

            // check to see if state is Puerto Rico or Virgin Islands and
            // country is not the same, so set it to same
            if ( (stateId != null) &&
                 (stateId.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                  stateId.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
                 (!stateId.equals(countryId) ||
                  !countryCode.equals(countryId)) )
            {
                countryId = stateId;
                countryCode = stateId;
            }

            // country is United States or Canada and country is not same, so set it same
            else if ( (countryCode != null) &&
                      (countryCode.equals(GeneralConstants.COUNTRY_CODE_US) ||
                       countryCode.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                       countryCode.equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN)) &&
                      (!countryCode.equals(countryId)) )
            {
                // account for 'CAN' as country code, incorrect, but possible
                if ( countryCode.length() > 2 )
                {
                    countryCode = countryCode.substring(0,2);
                }

                if ( !countryCode.equals(countryId) )
                {
                    countryId = countryCode;
                }
            }

            /*  *************************************************************
                This section checks to see if the country is Canada, Puerto
                Rico or the Virgin Islands and displays the approprate
                message if the product is of the incorrect type.
                *************************************************************   */
            // country is Canada, Puerto Rico, Virgin Islands, or International
            if ( countryId.equals("CA") ||       // Canada
                 countryId.equals("PR") ||       // Puerto Rico
                 countryId.equals("VI") )        // Virgin Islands
            {
                // Zip code does not exist or is shut down,
                // so can not delivery any products
                if ( (zipGnaddFlag.equals(GeneralConstants.YES) &&
                      globalParms.getGNADDLevel().equals("0")) ||
                     zipFloralFlag.equals(GeneralConstants.NO) )
                {
                    // only display if has not been displayed before
                    if ( order.getDisplayNoProduct().equals(GeneralConstants.YES) )
                    {
                        // set order flag so will not be displayed unless zip code changed later
                        order.setDisplayNoProduct(GeneralConstants.NO);
                        order.setDisplayFloral(GeneralConstants.YES);
                        order.setDisplaySpecialtyGift(GeneralConstants.YES);
                    }
                }
                // can only delivery floral products
                else if ( order.getDisplayFloral().equals(GeneralConstants.YES) &&
                          !item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                {
                    // set order flag so will not be displayed unless zip code changed later
                    order.setDisplayNoProduct(GeneralConstants.YES);
                    order.setDisplayFloral(GeneralConstants.NO);
                    order.setDisplaySpecialtyGift(GeneralConstants.YES);
                }

            }
            // zip code does not exist or is shut down
            // so can only delivery Carrier delivered products
            else if ( (zipGnaddFlag.equals(GeneralConstants.YES) &&
                       globalParms.getGNADDLevel().equals("0")) ||
                      zipFloralFlag.equals(GeneralConstants.NO) )
            {
                // only display if has not been displayed before
                if ( order.getDisplaySpecialtyGift().equals(GeneralConstants.YES) &&
                     item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                {
                    order.setDisplayNoProduct(GeneralConstants.YES);
                    order.setDisplayFloral(GeneralConstants.YES);
                    order.setDisplaySpecialtyGift(GeneralConstants.NO);
                }
            }

            //  update the order information for later use
            order.getSendToCustomer().setCountry(countryId);
            order.getSendToCustomer().setState(stateId);
            order.getSendToCustomer().setZipCode(zipCode);

            // populate the Item to the current values
            item.getSendToCustomer().setHpCustomerId(new Long(OEOrderUTIL.getUniqueNumber()).toString());
            item.getSendToCustomer().setFirstName((String) arguments.get("recipientFirstName"));
            item.getSendToCustomer().setLastName((String) arguments.get("recipientLastName"));

            if(arguments.get("recipientAddress1") != null && ((String) arguments.get("recipientAddress1")).length() > 30) {
                int spaceIndex = ((String) arguments.get("recipientAddress1")).substring(0, 30).lastIndexOf(" ");
                item.getSendToCustomer().setAddressOne(((String) arguments.get("recipientAddress1")).substring(0, spaceIndex));
                item.getSendToCustomer().setAddressTwo(((String) arguments.get("recipientAddress1")).substring(spaceIndex + 1, ((String) arguments.get("recipientAddress1")).length()));

                if(arguments.get("recipientAddress2") != null && ((String) arguments.get("recipientAddress2")).trim().length() > 0) {
                    item.getSendToCustomer().setAddressTwo(item.getSendToCustomer().getAddressTwo() + " " + (String) arguments.get("recipientAddress2"));
                }
            }
            else {
                item.getSendToCustomer().setAddressOne((String) arguments.get("recipientAddress1"));
                item.getSendToCustomer().setAddressTwo((String) arguments.get("recipientAddress2"));
            }

            order.getSendToCustomer().setCity((String) arguments.get("city"));
            item.getSendToCustomer().setCity((String) arguments.get("city"));
            item.getSendToCustomer().setState(stateId);

            if(zipCode != null && zipCode.length() >= 5) {
                item.getSendToCustomer().setZipCode(zipCode.substring(0, 5));
            }

            item.getSendToCustomer().setCountry(countryId);
            item.getSendToCustomer().setCountryType((String) arguments.get("countryType"));
            item.getSendToCustomer().setHomePhone((String) arguments.get("recipientPhone"));
            item.getSendToCustomer().setPhoneExtension((String) arguments.get("recipientPhoneExt"));

            // JMP 9/24/03
            // Set the order send to phone too
            order.getSendToCustomer().setHomePhone((String) arguments.get("recipientPhone"));
            order.getSendToCustomer().setPhoneExtension((String) arguments.get("recipientPhoneExt"));
            // Set delivery information in all appropriate places
            item.setInstitutionType((String) arguments.get("deliveryLocation"));
            order.getSendToCustomer().setCompanyType((String) arguments.get("deliveryLocation"));
            item.getSendToCustomer().setCompanyType((String) arguments.get("deliveryLocation"));
            item.setInstitutionName((String) arguments.get("institution"));
            order.getSendToCustomer().setCompanyName((String) arguments.get("institution"));
            item.getSendToCustomer().setCompanyName((String) arguments.get("institution"));
            item.setInstitutionInfo((String) arguments.get("roomNumber"));
            order.getSendToCustomer().setCompanyInfo((String) arguments.get("roomNumber"));
            item.getSendToCustomer().setCompanyInfo((String) arguments.get("roomNumber"));
            item.setShipTimeFrom((String) arguments.get("fromWorkHours"));
            item.setShipTimeTo((String) arguments.get("toWorkHours"));
            String giftMessage = FieldUtils.replaceAll((String) arguments.get("giftMessage"),
                                 GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT);
            item.setCardMessage(FieldUtils.replaceAll(giftMessage, GeneralConstants.TILDE_CHAR,
                                GeneralConstants.TILDE_CHAR_REPLACEMENT));
            item.setCardSignature(FieldUtils.replaceAll((String) arguments.get("signature"),
                                  GeneralConstants.TILDE_CHAR, GeneralConstants.TILDE_CHAR_REPLACEMENT));
            item.setReleaseSenderNameFlag((String) arguments.get("releaseSendersName"));
            item.setItemComments(FieldUtils.replaceAll((String) arguments.get("orderComments"), GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT));
            String floristComments = FieldUtils.replaceAll((String) arguments.get("floristComments"),
                                     GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT);
            item.setFloristComments(FieldUtils.replaceAll(floristComments, GeneralConstants.TILDE_CHAR,
                                    GeneralConstants.TILDE_CHAR_REPLACEMENT));
            item.setFloristCode((String) arguments.get("floristCode"));
            item.setLastMinuteGiftFlag((String) arguments.get("lastMinuteCheckBox"));
            item.setLastMinuteGiftEmail((String) arguments.get("lastMinuteEmailAddress"));

            if(arguments.get("QMSAddress1") != null && ((String) arguments.get("QMSAddress1")).length() > 30) {
                int spaceIndex = ((String) arguments.get("QMSAddress1")).substring(0, 30).lastIndexOf(" ");
                item.getSendToCustomer().setQmsAddressOne(((String) arguments.get("QMSAddress1")).substring(0, spaceIndex));
                item.getSendToCustomer().setQmsAddressTwo(((String) arguments.get("QMSAddress1")).substring(spaceIndex + 1, ((String) arguments.get("QMSAddress1")).length()));

                if(arguments.get("QMSAddress2") != null && ((String) arguments.get("QMSAddress2")).trim().length() > 0) {
                    item.getSendToCustomer().setQmsAddressTwo(item.getSendToCustomer().getQmsAddressTwo() + " " + (String) arguments.get("QMSAddress2"));
                }
            }
            else {
                item.getSendToCustomer().setQmsAddressOne((String) arguments.get("QMSAddress1"));
                item.getSendToCustomer().setQmsAddressTwo((String) arguments.get("QMSAddress2"));
            }

            item.getSendToCustomer().setQmsCity((String) arguments.get("QMSCity"));
            item.getSendToCustomer().setQmsState((String) arguments.get("QMSState"));

            if(arguments.get("QMSZipCode") != null && ((String) arguments.get("QMSZipCode")).length() >= 5) {
                item.getSendToCustomer().setQmsZipCode(((String) arguments.get("QMSZipCode")).substring(0, 5));
            }

            item.getSendToCustomer().setQmsFirmName((String) arguments.get("QMSFirmName"));
            item.getSendToCustomer().setQmsLatitude((String) arguments.get("QMSLatitude"));
            item.getSendToCustomer().setQmsLongitude((String) arguments.get("QMSLongitude"));
            item.getSendToCustomer().setQmsResultCode((String) arguments.get("QMSMatchCode"));
            item.getSendToCustomer().setQmsUSPSRangeRecordType((String) arguments.get("QMSRangeRecordType"));
            item.getSendToCustomer().setQmsOverrideFlag((String) arguments.get("QMSOverrideFlag"));

            if(item.getSendToCustomer().getCountry() != null
                && (item.getSendToCustomer().getCountry().equals(GeneralConstants.COUNTRY_CODE_CANADA_CA)
                || item.getSendToCustomer().getCountry().equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN)))
            {

                if(order.getScriptCode() != null && order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE))
                {
                    item.getSendToCustomer().setCountryType(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                }
                else
                {
                    item.getSendToCustomer().setCountryType(GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
                }
            }

            if(item.getSendToCustomer().getState() != null
                && (item.getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_PUERTO_RICO)
                || item.getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)))
            {
                if(order.getScriptCode() != null && order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE))
                {
                    item.getSendToCustomer().setCountryType(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                }
                else
                {
                    item.getSendToCustomer().setCountryType(GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
                }
            }

            if ( (arguments.get("deliveryDate") != null) &&
                 (!arguments.get("deliveryDate").equals("")) ) {
                String deliveryDateS = (String) arguments.get("deliveryDate");
                Date deliveryDateD = FieldUtils.formatStringToUtilDate(deliveryDateS);
                item.setDeliveryDate(deliveryDateD);
                String deliveryDateDisplay = (String) arguments.get("deliveryDateDisplay");
                if (deliveryDateDisplay == null || deliveryDateDisplay.equals(""))
                {
                  deliveryDateDisplay = FieldUtils.formatUtilDateToString(deliveryDateD);
                }
                item.setDeliveryDateDisplay(deliveryDateDisplay);

                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("sourcecode", order.getSourceCode());
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, deliveryDateS);
                this.getLogManager().debug(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS + " - " +
                    order.getSourceCode() + " " + deliveryDateS);
                service = this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
                dataResponse = service.executeRequest(dataRequest);
                xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                StringWriter sw = new StringWriter();
                xmlResponse.print(new PrintWriter(sw));
                this.getLogManager().debug(sw.toString());

                NodeList nls3 = null;
                String xpath3 = "sourceCodeDataList/sourceCodeData/data";
                XPathQuery q3 = new XPathQuery();
                nls3 = q3.query(xmlResponse, xpath3);
                if ( nls3.getLength() > 0 )
                {
                    Element serviceFeeElement = (Element) nls3.item(0);
                    String domesticFee = serviceFeeElement.getAttribute("domesticServiceCharge");
                    //this.getLogManager().debug("domestic: " + domesticFee);
                    if (domesticFee != null && !(domesticFee.equals(""))) {
                        order.setDomesticServiceFee(new BigDecimal(domesticFee));
                    }
                    String internationalFee = serviceFeeElement.getAttribute("intlServiceCharge");
                    //this.getLogManager().debug("international: " + internationalFee);
                    if (internationalFee != null && !(internationalFee.equals(""))) {
                        order.setInternationalServiceFee(new BigDecimal(internationalFee));
                    }
                }
            }

            /*  *************************************************************
                    Retrieve the shipping methods for this product
                *************************************************************   */
            String deliveryMethod = (String) arguments.get("deliveryMethod");
            if ( (deliveryMethod != null) &&
                 (!deliveryMethod.trim().equals("")) )
            {
                String deliveryMethodId = "";

                // If drop ship then find the corresponding shipping method id
                //if ( !deliveryMethod.equals("Florist Delivery") )
                if( !deliveryMethod.equals("florist") )
                {
                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("shipMethodDescription", deliveryMethod);

                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_SHIPPING_METHOD_ID);
                    dataResponse = service.executeRequest(dataRequest);

                    xpath = "shippingMethodList/shippingMethod/id";
                    q = new XPathQuery();
                    nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                    if ( nl.getLength() > 0 )
                    {
                        Element shipMethodData = (Element) nl.item(0);
                        deliveryMethodId = shipMethodData.getAttribute("shipMethodId");
                    }
                    // For SDG the SD code does not exist in the database so set it here.
                    else if(item.getProductType() != null && (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                            item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                    {
                        deliveryMethodId = GeneralConstants.DELIVERY_SAME_DAY_CODE;
                    }

                    try
                    {
                        if( item.getProductType() != null && (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                            item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC))) {
                            if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null &&
                                order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) {
                                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getGlobalParameters().getFreshCutSrvcCharge());
                                item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                            }
                            else {
                                if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) {
                                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                                }
                                else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) {
                                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                                }
                            }
                        }
                        else {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(new BigDecimal((String) arguments.get("deliveryCost")));
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));

                        }

                        if ( deliveryMethod.equals("Florist Delivery") )
                        {
                            item.setExtraShippingFee(new BigDecimal(0.00));
                        }
                        else if(item.getProductType() != null &&
                            (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                             item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) ) {
                        	//Set the service fee for saturday fresh cuts
		               		if( order.getSendToCustomer().isDomesticFlag() )
		                    {
		                    	Calendar cal = Calendar.getInstance();
		        				cal.setTime(item.getDeliveryDate());
		        				if( cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY  )
		        				{
		        					item.setExtraShippingFee(order.getGlobalParameters().getFreshCutSatCharge());
		                    	}
		                    	else {
		                    		item.setExtraShippingFee(new BigDecimal(0.00));
		                    	}
		                    }
		                    else {
		                    	item.setExtraShippingFee(new BigDecimal(0.00));
		                    }
                        }
                        else
                        {
                            item.setExtraShippingFee(new BigDecimal(0.00));
                        }
                    }
                    catch (Exception e)
                    {
                        this.getLogManager().error("Item # " + item.getProductId());
                        this.getLogManager().error("Problems setting shipping cost from delivery information page.");
                    }
                } else {
                    if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) {
                        feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                        item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    }
                    else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) {
                        feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                        item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    }
                }

                item.setShippingMethodId(deliveryMethodId);
                item.setShippingMethod(deliveryMethod);
            }

            //Initialize tax rate to zero
            item.setTax(new BigDecimal(0));

            if ( (item.getSendToCustomer().getCountryType() != null) &&
                 (item.getSendToCustomer().getCountryType().equals("D")) &&
                 (item.getSendToCustomer().getState() != null) &&
                 (!item.getSendToCustomer().getState().equals("")) &&
                 (order.getScriptCode() != null) &&
                 (!order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE)) )
            {
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("inStateCode", item.getSendToCustomer().getState());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_STATE_DETAIL);
                dataResponse = service.executeRequest(dataRequest);

                xpath = "stateList/state/data";
                q = new XPathQuery();
                nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                if ( nl.getLength() > 0 )
                {
                    Element stateData = (Element) nl.item(0);
                    try
                    {
                        if(stateData.getAttribute("taxRate") != null && !stateData.getAttribute("taxRate").trim().equals("")) {
                            item.setTax(new BigDecimal(Double.parseDouble(stateData.getAttribute("taxRate")) / 100));
                        }
                    }
                    catch(Exception e)
                    {
                        this.getLogManager().error("Problems calculating taxes for item #" + item.getProductId() + " with the state of " + item.getSendToCustomer().getState());
                    }
                }
            }

            // Commit item to cart
            if ( arguments.get("submitt") != null )
            {
                try
                {
                    // Save or update recipient in database
                    dataRequest = new FTDDataRequest();

                    dataRequest.addArgument("customerIn", item.getSendToCustomer().getHpCustomerId());
                    dataRequest.addArgument("firstNameIn", item.getSendToCustomer().getFirstName());
                    dataRequest.addArgument("lastNameIn", item.getSendToCustomer().getLastName());
                    dataRequest.addArgument("addressTypeIn", item.getInstitutionType());

                    if(item.getSendToCustomer().getQmsOverrideFlag() != null && item.getSendToCustomer().getQmsOverrideFlag().equals("N")) {
                        dataRequest.addArgument("address1In", item.getSendToCustomer().getQmsAddressOne());
                        dataRequest.addArgument("address2In", item.getSendToCustomer().getQmsAddressTwo());
                        dataRequest.addArgument("cityIn", item.getSendToCustomer().getQmsCity());
                        dataRequest.addArgument("stateIn", item.getSendToCustomer().getQmsState());
                        dataRequest.addArgument("zipcodeIn", item.getSendToCustomer().getQmsZipCode());
                    }
                    else {
                        dataRequest.addArgument("address1In", item.getSendToCustomer().getAddressOne());
                        dataRequest.addArgument("address2In", item.getSendToCustomer().getAddressTwo());
                        dataRequest.addArgument("cityIn", item.getSendToCustomer().getCity());
                        dataRequest.addArgument("stateIn", item.getSendToCustomer().getState());
                        dataRequest.addArgument("zipcodeIn", item.getSendToCustomer().getZipCode());
                    }

                    dataRequest.addArgument("countyIn", "");
                    dataRequest.addArgument("countryIdIn", item.getSendToCustomer().getCountry());
                    dataRequest.addArgument("homePhoneIn", "");
                    dataRequest.addArgument("workPhoneIn", item.getSendToCustomer().getHomePhone());
                    dataRequest.addArgument("workPhoneExtIn", item.getSendToCustomer().getPhoneExtension());
                    dataRequest.addArgument("faxNumberIn", "");
                    dataRequest.addArgument("emailIn", item.getLastMinuteGiftEmail());
                    dataRequest.addArgument("bfhNameIn", item.getInstitutionName());
                    dataRequest.addArgument("bfhInfoIn", item.getInstitutionInfo());
                    dataRequest.addArgument("promoFlag", "");
                    if(order.getCsrUserName() != null && !order.getCsrUserName().equals("")) {
                        dataRequest.addArgument("lastUpdateUserIn", order.getCsrUserName());
                    }
                    else {
                        dataRequest.addArgument("lastUpdateUserIn", "NA");
                    }

                    service =  this.getGenericDataService(DataConstants.SHOPPING_INSERT_CUSTOMER);
                    dataResponse = service.executeRequest(dataRequest);

                    //Clear out the recipient search data from the occasion screen
                    order.getSendToCustomer().setHpCustomerId("");
                }
                catch(Exception e)
                {
                    this.getLogManager().error("Problems occured while trying to insert or update customer: " + order.getGUID(), e);
                }

                // Set the status to complete
                item.setItemStatus(GeneralConstants.COMPLETE);
            }
        }
        catch (Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        }
	}

    private FTDSystemVO updateItemDetail(FTDArguments arguments)
	{

        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;
        BigDecimal feeWithFuelSurcharge;
        Connection connection = null;

        try
        {
            // Get fuel surcharge amount
            connection = DataManager.getInstance().getConnection();
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            ShoppingUTIL shoppingUtil = new ShoppingUTIL(this.getLogManager());
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            // Get Global paramaters
            order.setGlobalParameters(searchUTIL.getGlobalParms());

            SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
            OEParameters globalParms = new OEParameters();
            ItemPO item = null;

            String zipCode = (String) arguments.get(ArgumentConstants.CONTACT_ZIP);

            // Get item requested
            String cartItemNumber = (String) arguments.get("cartItemNumber");

            if ( (cartItemNumber != null) &&
                 (!cartItemNumber.equals("")) )
            {
                item = order.getItem(new Integer(cartItemNumber));

                // Zip Code or Country changed, reset popup flags
                if ( (zipCode != null) &&
                     (item.getSendToCustomer().getZipCode() != null) )
                {
                    if ( !zipCode.equals(item.getSendToCustomer().getZipCode()) )
                    {
                        order.setDisplayFloral(GeneralConstants.YES);
                        order.setDisplayNoProduct(GeneralConstants.YES);
                        order.setDisplaySpecialtyGift(GeneralConstants.YES);
                    }
                }
            }
            else
            {
                item = new ItemPO();

                // Zip Code or Country changed, reset popup flags
                if ( (zipCode != null) &&
                     (item.getSendToCustomer().getZipCode() == null) )
                {
                    order.setDisplayFloral(GeneralConstants.YES);
                    order.setDisplayNoProduct(GeneralConstants.YES);
                    order.setDisplaySpecialtyGift(GeneralConstants.YES);
                }
                else if ( (zipCode != null) &&
                          (item.getSendToCustomer().getZipCode() != null) &&
                          (!zipCode.equals(item.getSendToCustomer().getZipCode())) )
                {
                    order.setDisplayFloral(GeneralConstants.YES);
                    order.setDisplayNoProduct(GeneralConstants.YES);
                    order.setDisplaySpecialtyGift(GeneralConstants.YES);
                }

                item.setProductId((String) arguments.get("productId"));
                item.getSendToCustomer().setZipCode(zipCode);
                order.getSendToCustomer().setZipCode(zipCode);
            }

            // Pull item from database for processing
            FTDDataRequest dataRequest = new FTDDataRequest();

            // If the product ID is different then we are changin products for
            // the same recipient
            if(!item.getProductId().equals((String) arguments.get("productId")))
            {
                item.setProductId((String) arguments.get("productId"));
            }
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, item.getProductId());
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, item.getSendToCustomer().getZipCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, item.getSendToCustomer().getCountry());

            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }
            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            FTDDataResponse detailDataResponse = service.executeRequest(dataRequest);
            XMLDocument productList = (XMLDocument)detailDataResponse.getDataVO().getData();

            /*  ***********************************************************
                    This section retrieves the global parameters from
                    the retrieved product information
                *********************************************************** */
            Element element = null;
            NodeList nl = null;
            String xpath = null;
            XPathQuery q = null;

            xpath = "productList/globalParmsData/data";
            q = new XPathQuery();
            nl = q.query(productList, xpath);

            if ( nl.getLength() > 0 )
            {
                element = (Element) nl.item(0);

                //String days = element.getAttribute("deliveryDaysOut");
                //globalParms.setDeliveryDaysOut(Integer.parseInt(element.getAttribute("deliveryDaysOut")));
                globalParms.setDeliveryDaysOutMAX(Integer.parseInt(element.getAttribute("deliveryDaysOutMAX")));
                globalParms.setDeliveryDaysOutMIN(Integer.parseInt(element.getAttribute("deliveryDaysOutMIN")));
                globalParms.setAllowSubstitution(element.getAttribute("allowSubstitution"));
                globalParms.setExoticCutoff(element.getAttribute("exoticCutoff"));
                globalParms.setFreshCutCutoff(element.getAttribute("freshcutsCutoff"));
                globalParms.setFridayCutoff(element.getAttribute("fridayCutoff"));
                globalParms.setGNADDDate(sdfInput.parse(element.getAttribute("gnaddDate")));
                globalParms.setGNADDLevel(element.getAttribute("gnaddLevel"));
                globalParms.setIntlAddOnDays(Integer.parseInt(element.getAttribute("intlOverrideDays")));
                globalParms.setIntlCutoff(element.getAttribute("internationalCutoff"));
                globalParms.setMondayCutoff(element.getAttribute("mondayCutoff"));
                globalParms.setSaturdayCutoff(element.getAttribute("saturdayCutoff"));
                globalParms.setSpecialtyGiftCutoff(element.getAttribute("specialtyGiftCutoff"));
                globalParms.setSundayCutoff(element.getAttribute("sundayCutoff"));
                globalParms.setThursdayCutoff(element.getAttribute("thursdayCutoff"));
                globalParms.setTuesdayCutoff(element.getAttribute("tuesdayCutoff"));
                globalParms.setWednesdayCutoff(element.getAttribute("wednesdayCutoff"));
                globalParms.setFreshCutSrvcChargeTrigger(element.getAttribute("freshCutsSvcChargeTrigger"));
                if(element.getAttribute("freshCutsSvcCharge") != null && !element.getAttribute("freshCutsSvcCharge").equals("")) {
                    globalParms.setFreshCutSrvcCharge(new BigDecimal(element.getAttribute("freshCutsSvcCharge")));
                }
                if(element.getAttribute("specialSvcCharge") != null && !element.getAttribute("specialSvcCharge").equals("")) {
                    globalParms.setSpecialSrvcCharge(new BigDecimal(element.getAttribute("specialSvcCharge")));
                }
                if(element.getAttribute("freshCutsSatCharge") != null && !element.getAttribute("freshCutsSatCharge").equals("")) {
                    globalParms.setFreshCutSatCharge(new BigDecimal(element.getAttribute("freshCutsSatCharge")));
                }
            }

            /*  ***********************************************************
                    This section checks to see if the country does not
                    match the country associated with the entered zip code
                    and changes it if it does not
                *********************************************************** */
            String timeZone = null;
            String zipGnaddFlag = null;
            String zipFloralFlag = null;
            String zipGotoFloristFlag = null;
            String zipSundayFlag = null;
            String stateId = null;
            String countryCode = null;
            String countryId = item.getSendToCustomer().getCountry();

            xpath = "productList/zipCodeData/data";
            q = new XPathQuery();
            nl = q.query(productList, xpath);

            // valid zip code entered
            if ( nl.getLength() > 0 )
            {
                element = (Element) nl.item(0);

                timeZone = element.getAttribute("timeZone");
                zipGnaddFlag = element.getAttribute("zipGnaddFlag").trim();
                zipFloralFlag = element.getAttribute("zipFloralFlag").trim();
                zipGotoFloristFlag = element.getAttribute("zipGotoFloristFlag").trim();
                zipSundayFlag = element.getAttribute("zipSundayFlag").trim();
                stateId = element.getAttribute("stateId");
                countryCode = element.getAttribute("countryCode");
            }
            else {
                // No valid zip code was entered
                timeZone = GeneralConstants.OE_TIMEZONE_DEFAULT;
                zipGnaddFlag = GeneralConstants.NO;
                zipFloralFlag = GeneralConstants.YES;  // Assume floral delivery avail for zip
                zipGotoFloristFlag = GeneralConstants.YES;
                zipSundayFlag = GeneralConstants.YES;
                stateId = item.getSendToCustomer().getState();
                countryCode = item.getSendToCustomer().getCountry();
            }

            // check to see if state is Puerto Rico or Virgin Islands and
            // country is not the same, so set it to same
            if ( (stateId != null) &&
                 (stateId.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                  stateId.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
                 (!stateId.equals(countryId) ||
                  !countryCode.equals(countryId)) )
            {
                countryId = stateId;
                countryCode = stateId;
            }

            // country is United States or Canada and country is not same, so set it same
            else if ( (countryCode != null) &&
                      (countryCode.equals(GeneralConstants.COUNTRY_CODE_US) ||
                       countryCode.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                       countryCode.equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN)) &&
                      (!countryCode.equals(countryId)) )
            {
                // account for 'CAN' as country code, incorrect, but possible
                if ( countryCode.length() > 2 )
                {
                    countryCode = countryCode.substring(0,2);
                }

                if ( !countryCode.equals(countryId) )
                {
                    countryId = countryCode;
                }
            }

            item.getSendToCustomer().setState(stateId);
            item.getSendToCustomer().setCountry(countryId);
            order.getSendToCustomer().setState(stateId);
            order.getSendToCustomer().setCountry(countryId);

            /* retrieve page level data (ex. delivery dates) and build the xml element */
            Element productElement = searchUTIL.pageBuilder(false, true, productList, order, 1, null, null);


            // Set item information to most recent data and page changes
            NodeList nodeList = productElement.getElementsByTagName("product");
            Element productDetailElement = (Element) nodeList.item(0);

            // Set item values
            item.setCustomItemFlag(false);
            item.setProductId(productDetailElement.getAttribute("productID"));
            item.setItemName(productDetailElement.getAttribute("productName"));
            item.setNovatorId(productDetailElement.getAttribute("novatorID"));
            item.setNovatorName(productDetailElement.getAttribute("novatorName"));
            item.setProductType(productDetailElement.getAttribute("productType"));
            item.setItemSKU(productDetailElement.getAttribute("vendorSKU"));
            item.setExceptionCode(productDetailElement.getAttribute("exceptionCode"));
            item.setExceptionStartDate(productDetailElement.getAttribute("exceptionStartDate"));
            item.setExceptionEndDate(productDetailElement.getAttribute("exceptionEndDate"));

            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                order.setDomesticServiceFee(new BigDecimal(productDetailElement.getAttribute("serviceCharge").trim()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            }
            else
            {
                order.setInternationalServiceFee(new BigDecimal(productDetailElement.getAttribute("serviceCharge").trim()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            }
            //this.getLogManager().debug("order.getDomesticServiceFee: " + order.getDomesticServiceFee());
            //this.getLogManager().debug("order.getInternationalServiceFee: " + order.getInternationalServiceFee());

            // Default the product substitution to No unless the box is checked
            String substAuth = (String) arguments.get("secondChoiceAuth");
            if(substAuth == null)
            {
                substAuth = "N";
            }
            else if(substAuth.equals("true") || substAuth.equals("on"))
            {
                substAuth = "Y";
            }
            else
            {
                substAuth = "N";
            }
            item.setSubstitutionAuth(substAuth);


            //item.setDeliveryDates(order.getDeliveryDates());
            item.setOccasion(order.getOccasion());
            item.setColorOne((String) arguments.get("COLOR1"));
            item.setColorTwo((String) arguments.get("COLOR2"));

            if(arguments.get(ArgumentConstants.DELIVERY_DATE) != null && !((String) arguments.get(ArgumentConstants.DELIVERY_DATE)).equals("")) {
                item.setDeliveryDate(FieldUtils.formatStringToUtilDate((String) arguments.get(ArgumentConstants.DELIVERY_DATE)));
                item.setDeliveryDateDisplay((String) arguments.get("deliveryDateDisplay"));
            }

            if(arguments.get("subCodeId") != null && !((String) arguments.get("subCodeId")).trim().equals("")) {
                item.setSubCodeSelected(Boolean.TRUE);
                item.setSubCodeId(((String) arguments.get("subCodeId")).trim());

                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("subCodeId", item.getSubCodeId());

                service =  this.getGenericDataService(DataConstants.SEARCH_GET_SUB_CODE);
                FTDDataResponse dataResponse = service.executeRequest(dataRequest);
                XMLDocument subCodeList = (XMLDocument)dataResponse.getDataVO().getData();

                Element subCodeElement = null;
                xpath = "subCodeList/subCode/data";
                q = new XPathQuery();

                nl = q.query(subCodeList, xpath);

                if (nl.getLength() > 0)
                {
                    subCodeElement = (Element) nl.item(0);
                    item.setSubCodeDescription(subCodeElement.getAttribute("subcodeDesc"));

                    try {
                        item.setSubCodePrice(new BigDecimal(subCodeElement.getAttribute("subcodePrice").trim()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    } catch(Exception e) {
                        this.getLogManager().error("Subcode did not have a valid price associated with it.");
                    }

                    item.setSubCodeReferenceNumber(subCodeElement.getAttribute("subcodeRefNumber"));
                }
            }
            else {
                item.setSubCodeSelected(Boolean.FALSE);
                item.setSubCodeDescription(null);
                item.setSubCodeId(null);
                item.setSubCodePrice(null);
                item.setSubCodeReferenceNumber(null);
            }

            String carrierDelivered = productDetailElement.getAttribute("shipMethodCarrier");

            if (carrierDelivered.equals("Y"))
            {
                item.setShippingCarrier("COMMON_CARRIER");

                if(item.getProductType() != null && (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                    item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                {
                    // Reset the shipping method just in case we are switching from
                    // a floral to fresh cut product
                    item.setShippingMethod("");
                    if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null && order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) {
                        feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getGlobalParameters().getFreshCutSrvcCharge());
                        item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    }
                    else {
                        if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                        }
                        else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                        }
                    }

                    //Set the service fee for saturday fresh cuts
               		if( (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                         item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) &&
                    	order.getSendToCustomer().isDomesticFlag() && item.getDeliveryDate()!=null )
                    {
                    	Calendar cal = Calendar.getInstance();
        				cal.setTime(item.getDeliveryDate());
        				if( cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY  )
        				{
        					item.setExtraShippingFee(order.getGlobalParameters().getFreshCutSatCharge());
                    	}
                        else
                        {
                            item.setExtraShippingFee(new BigDecimal(0.00));
                        }
                    }
                    else
                    {
                        item.setExtraShippingFee(new BigDecimal(0.00));
                    }
                }
                else
                {
                    HashMap shippingMethods = searchUTIL.getProductShippingMethods(productDetailElement.getAttribute("productID"), productDetailElement.getAttribute("standardPrice"), productDetailElement.getAttribute("shippingKey"));
                    ShippingMethod shipMethod = (ShippingMethod)shippingMethods.get(item.getShippingMethod());
                    if(shipMethod != null)
                    {
                        try
                        {
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(shipMethod.getDeliveryCharge());
                            item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                        }
                        catch(Exception e) {
                            this.getLogManager().error("Product #" + item.getProductId() + " has shipping inconsistencies.  Could not find a valid delivery price for " + item.getShippingMethod() + " shipping.");
                        }
                    }
                    else
                    {
                        // Could not find this shipping method so set it to blank.
                        item.setShippingMethod("");
                    }
                }
            }
            else
            {
                item.setShippingCarrier("FLORIST");
                item.setShippingMethod("florist");
                if(order.getSendToCustomer().isDomesticFlag() && order.getDomesticServiceFee() != null) {
                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
                else if(!order.getSendToCustomer().isDomesticFlag() && order.getInternationalServiceFee() != null) {
                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
            }

            // If ship cost was never set put it to zero
            if(item.getShippingCost() == null) {
                item.setShippingCost(new BigDecimal(0));
            }

            // Calculate and set pricing for added item
            if(item.isSubCodeSelected())
            	item.setPriceType("standard");
            else
            	item.setPriceType((String) arguments.get("PRICE"));

            if(!item.getPriceType().equals("variable")) {
                try {
                    if(item.isSubCodeSelected()) {
                        item.setRegularPrice(item.getSubCodePrice());
                        item.setDiscountedPrice(item.getSubCodePrice());
                    }
                    else
                    {
                        item.setRegularPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                        item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                    }
                }
                catch(Exception e) {
                    this.getLogManager().error("Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.");
                }

                if(order.getRewardType() != null && !order.getRewardType().equals("")) {
                    try {
                        item.setDiscountAmount(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "RewardValue")));
                        item.setDiscountDescription(order.getRewardType());

                        if(order.getRewardType().equals("Percent") || order.getRewardType().equals("Dollars")) {

                            if(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice") != null) {
                                item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice")));
                            }
                        }
                    }
                    catch(Exception e) {
                        this.getLogManager().error("Product #" + item.getProductId() + " has discount inconsistencies.");
                    }
                }
            }
            else if(arguments.get("VARIABLE_PRICE_AMT") != null && !((String)arguments.get("VARIABLE_PRICE_AMT")).equals("")) {
                try {
                    String discountAmount = "0";

                    item.setRegularPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    item.setDiscountedPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("inPriceHeaderId", order.getPricingCode());
                    dataRequest.addArgument("inItemPrice", (String)arguments.get("VARIABLE_PRICE_AMT"));

                    // Determine if its a partner or standard discount
                    if ( (order.getPartnerId() != null) &&
                       (!order.getPartnerId().equals("")) &&
                       (order.getPricingCode() != null) &&
                       (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
                    {
                        item.setDiscountAmount(new BigDecimal(searchUTIL.retrievePromotionValue(order.getPromotionList(), item.getRegularPrice().setScale(0, BigDecimal.ROUND_UP).doubleValue())));
                    }
                    else
                    {
                        service =  this.getGenericDataService(DataConstants.SEARCH_GET_DISCOUNT_AMOUNT);
                        FTDDataResponse dataResponse = service.executeRequest(dataRequest);
                        XMLDocument discountAmountList = (XMLDocument)dataResponse.getDataVO().getData();

                        Element discountAmountElement = null;
                        xpath = "discountAmountList/discountAmount/data";
                        q = new XPathQuery();

                        nl = q.query(discountAmountList, xpath);

                        if (nl != null && nl.getLength() > 0)
                        {
                            discountAmountElement = (Element) nl.item(0);
                            discountAmount = discountAmountElement.getAttribute("discountAmount");
                        }
                        else
                        {
                            this.getLogManager().error("Could not find discount amount for the variable price.");
                        }
                            item.setDiscountAmount(new BigDecimal(discountAmount));
                        }

                        item.setDiscountDescription(order.getRewardType());

                        if(order.getRewardType().equals("Percent")) {
                            item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal((item.getRegularPrice().doubleValue() * (Double.parseDouble(discountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                        }
                        else if(order.getRewardType().equals("Dollars")) {
                            item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal(discountAmount).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                        }
                        else if(order.getRewardType().equals("Miles") || order.getRewardType().trim().equals("Points")) {
                            order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + item.getDiscountAmount().longValue()));

                        }
                }
                catch(Exception e) {
                    this.getLogManager().error("Product #" + item.getProductId() + " has problems processing variable price.");
                }
            }

            // Get list of possible add ons for the occasion
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("occasionIn", order.getOccasion());

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_ADDON_LIST);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);
            XMLDocument addOnList = (XMLDocument)dataResponse.getDataVO().getData();

            // Remove all previous add ons
            item.getAddOnList().clear();

            // Set the selected add ons to the item
            AddOnPO addOn = null;

            try {
                if(arguments.get("balloon") != null && !((String)arguments.get("balloon")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("balloon"), new Integer((String) arguments.get("balloonQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("bear") != null && !((String)arguments.get("bear")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("bear"), new Integer((String) arguments.get("bearQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("chocolate") != null && !((String)arguments.get("chocolate")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("chocolate"), new Integer((String) arguments.get("chocolateQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("banner") != null && !((String)arguments.get("banner")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("banner"), new Integer((String) arguments.get("bannerQty")), addOnList);
                    item.addAddOn(addOn);
                }
                if(arguments.get("card") != null && !((String)arguments.get("card")).equals("")) {
                    addOn = shoppingUtil.setAddOnToItem((String) arguments.get("card"), new Integer(1), addOnList);
                    item.addAddOn(addOn);
                }
            }
            catch(Exception e) {
                this.getLogManager().error("Product #" + item.getProductId() + " has problems with addons: " + e.getMessage());
            }

            // Determine if its an edit or refresh
            if(cartItemNumber != null && !cartItemNumber.equals("")) {
                order.setItem(new Integer(cartItemNumber), item);
            }
            else {
                // Zip code refresh
                ShoppingUTIL shoppingUTIL = new ShoppingUTIL(this.getLogManager());

                XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT DETAIL PAGE VO");
                XMLEncoder.addSection(document, productElement.getChildNodes());
                XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("occasionIn", order.getOccasion());
                dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
                dataRequest.addArgument("company", order.getCompanyId());
                dataRequest.addArgument("productId", item.getProductId());
                service =  this.getGenericDataService(DataConstants.SEARCH_GET_EXTRA_PRODUCT_DETAIL);
                dataResponse = service.executeRequest(dataRequest);

                XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());
                XMLEncoder.addSection(document, shoppingUTIL.transformItemDetailXML(item).getChildNodes());

                // Load state from for notification check
                String displaySpecialFee = GeneralConstants.NO;

                if(item.getSendToCustomer().getZipCode() != null && !item.getSendToCustomer().getZipCode().trim().equals("")) {
                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("zipIn", item.getSendToCustomer().getZipCode());
                    dataRequest.addArgument("cityIn", null);
                    dataRequest.addArgument("stateIn", null);
                    dataRequest.addArgument("dnisType", order.getScriptCode());

                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_ZIP_CODE_DETAIL);
                    dataResponse = service.executeRequest(dataRequest);

                    XMLDocument xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                    Element zipCodeElement = null;
                    xpath = "zipCodeLookup/searchResults/searchResult";
                    q = new XPathQuery();

                    nl = q.query(xmlResponse, xpath);

                    if (nl.getLength() > 0)
                    {
                        zipCodeElement = (Element) nl.item(0);
                        item.getSendToCustomer().setState(zipCodeElement.getAttribute("state"));

                        if ( item.getSendToCustomer().getState() != null &&
                             (item.getSendToCustomer().getState().equals("AK") ||
                              item.getSendToCustomer().getState().equals("HI")) )
                        {
                            if ( item.getProductType() != null &&
                                 (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)))
                            {
                                displaySpecialFee = GeneralConstants.YES;
                            }
                        }
                    }
                }

                HashMap orderDataMap = new HashMap();
                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    if( order.getSendToCustomer().getCountry() != null &&
                        item.getSendToCustomer().getState() != null &&
                        order.getScriptCode() != null &&
                        order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE) &&
                        (order.getSendToCustomer().getCountry().equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                         order.getSendToCustomer().getCountry().equals(GeneralConstants.COUNTRY_CODE_CANADA_CAN) ||
                         item.getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                         item.getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) )
                    {
                        orderDataMap.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                    }
                    else {
                        orderDataMap.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
                    }
                }
                else
                {
                    orderDataMap.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                }

                orderDataMap.put("displaySpecialFee", displaySpecialFee);
                orderDataMap.put("zipCode", item.getSendToCustomer().getZipCode());
                orderDataMap.put("occasion", item.getOccasion());
                orderDataMap.put("subCodeSelected", item.getSubCodeId());
                orderDataMap.put("country", order.getSendToCustomer().getCountry());

                // Added so we can go back to the shopping page
                if ( item.getDeliveryDateDisplay() != null )
                {
                    orderDataMap.put("requestedDeliveryDateDisplay", item.getDeliveryDateDisplay());
                }
                orderDataMap.put("occasionDescription", searchUTIL.getDescriptionForCode(order.getOccasion(), SearchUTIL.GET_OCCASION_DESCRIPTION));
                orderDataMap.put("city", item.getSendToCustomer().getCity());
                orderDataMap.put("state", item.getSendToCustomer().getState());
                // End shopping page params

                if ( item.getDeliveryDate() != null )
                {
                    orderDataMap.put("requestedDeliveryDate", FieldUtils.formatUtilDateToString(item.getDeliveryDate()));
                }
                XMLEncoder.addSection(document, "orderData", "data", orderDataMap);

                XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());

                //set flag to prevent zip code from occasion bread crumb from being used
                String ocUrl = order.getCrumbURL("Occasion");
                if(ocUrl != null)
                {
                    //used to prevent flag from being concatenated more than once
                    ocUrl = order.getCrumbURL("occasionChangedZip");
                    ocUrl = ocUrl.concat("&updzip=N");
                    order.removeCrumbURL("Occasion");
                    order.setCrumbURL("Occasion", ocUrl);
                }

                XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

                /*  **********************************************************
                        These flags are set in SearchUtil.pageBuilder()
                    **********************************************************  */
                HashMap pageData = new HashMap();

                // indicates whether product is codified special and should be displayed
                pageData.put("displayCodifiedSpecial", getDisplayCodifiedSpecial("productList" ,detailDataResponse, item));

                if ( order.getDisplayProductUnavailable().equals(GeneralConstants.YES) )
                {
                    if(!((String)pageData.get("displayCodifiedSpecial")).equals("Y"))
                    {
                        pageData.put("displayProductUnavailable", order.getDisplayProductUnavailable());
                    }
                }
                // country is Canada, Puerto Rico, Virgin Islands, or International
                else if ( countryId != null &&
                         (countryId.equals("CA") ||       // Canada
                          countryId.equals("PR") ||       // Puerto Rico
                          countryId.equals("VI") ))        // Virgin Islands
                {
                    // Zip code does not exist or is shut down,
                    // so can not delivery any products
                    if ( (zipGnaddFlag.equals(GeneralConstants.YES) &&
                          globalParms.getGNADDLevel().equals("0")) ||
                         zipFloralFlag.equals(GeneralConstants.NO) )
                    {
                        // only display if has not been displayed before
                        if ( order.getDisplayNoProduct().equals(GeneralConstants.YES) )
                        {
                            pageData.put("displayNoProductPopup", GeneralConstants.YES);

                            // set order flag so will not be displayed unless zip code changed later
                            order.setDisplayNoProduct(GeneralConstants.NO);
                            order.setDisplayFloral(GeneralConstants.YES);
                            order.setDisplaySpecialtyGift(GeneralConstants.YES);
                        }
                    }
                    // can only delivery floral products
                    else if ( order.getDisplayFloral().equals(GeneralConstants.YES) &&
                              !item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        pageData.put("displayFloristPopup", GeneralConstants.YES);

                        // set order flag so will not be displayed unless zip code changed later
                        order.setDisplayNoProduct(GeneralConstants.YES);
                        order.setDisplayFloral(GeneralConstants.NO);
                        order.setDisplaySpecialtyGift(GeneralConstants.YES);
                    }
                }
                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                else if ( (zipGnaddFlag.equals(GeneralConstants.YES) &&
                           globalParms.getGNADDLevel().equals("0")) ||
                          zipFloralFlag.equals(GeneralConstants.NO) )
                {
                    // only display if has not been displayed before
                    if ( order.getDisplaySpecialtyGift().equals(GeneralConstants.YES) &&
                         item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        pageData.put("displaySpecGiftPopup", GeneralConstants.YES);

                        order.setDisplayNoProduct(GeneralConstants.YES);
                        order.setDisplayFloral(GeneralConstants.YES);
                        order.setDisplaySpecialtyGift(GeneralConstants.NO);
                    }
                }

                if(arguments.get("upsellFlag") != null && ((String)arguments.get("upsellFlag")).equals("Y"))
                {
                    if(order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_CARRIER) != null
                        && order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_CARRIER).equals("Y")
                        && order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_ID) != null
                        && !order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_ID).equals(item.getProductId())
                        && order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE) != null
                        && productDetailElement.getAttribute("deliveryDate") != null
                        && productDetailElement.getAttribute("zipGnaddFlag") != null
                        && productDetailElement.getAttribute("zipGnaddFlag").trim().equals("Y"))
                    {
                        if (order.getSendToCustomer().isDomesticFlag()
                              && countryId != null
                              && !countryId.equals("CA")
                              && !countryId.equals("CAN")
                              && !countryId.equals("PR")
                              && !countryId.equals("VI"))
                        {
                            Date upsellBaseDate = FieldUtils.formatStringToUtilDate(order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE));
                            Date firstAvailDate = FieldUtils.formatStringToUtilDate(productDetailElement.getAttribute("deliveryDate"));

                            if(upsellBaseDate.before(firstAvailDate))
                            {
                                pageData.put("displayUpsellGnaddSpecial", "Y");
                                pageData.put("upsellBaseId", order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_ID));
                                pageData.put("upsellBaseName", order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_NAME));
                                pageData.put("upsellBaseDeliveryDate", order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE));
                            }
                        }
                    }
                }

                // get substitution preference
                pageData.put("secondChoiceAuth", item.getSubstitutionAuth());
                pageData.put("allowSubstitution", globalParms.getAllowSubstitution());

                XMLEncoder.addSection(document, "pageData", "data", pageData);

                systemVO.setXML(document);
            }

            if(item.isSubCodeSelected()) {
                this.updateSourceCode(order, arguments);
            }
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        }
        return systemVO;
	}

    private void updateCustomItemDetail(FTDArguments arguments)
	{
        OrderPO order = null;
        BigDecimal feeWithFuelSurcharge;
        Connection connection = null;

        try
        {
            // Get fuel surcharge amount
            connection = DataManager.getInstance().getConnection();
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            ItemPO item = null;
            NodeList nl = null;
            String xpath = null;
            XPathQuery q = null;

            // Get item requested
            String cartItemNumber = (String) arguments.get("cartItemNumber");
            try {
                item = order.getItem(new Integer(cartItemNumber));
            }
            catch(Exception e) {
                this.getLogManager().error("Invalid cart item number:" + cartItemNumber);
            }

            // Pull item from database for processing
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, GeneralConstants.CUSTOM_ORDER_PID);
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, item.getSendToCustomer().getCountryType());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, item.getSendToCustomer().getZipCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            Element productElement = searchUTIL.pageBuilder(false, true, (XMLDocument)dataResponse.getDataVO().getData(), order, 1, null, null);

            // Create instance of item for creation

            NodeList nodeList = productElement.getElementsByTagName("product");
            Element productDetailElement = (Element) nodeList.item(0);

            // Set item values
            item.setCustomItemFlag(true);
            item.setProductId(GeneralConstants.CUSTOM_ORDER_PID);
            item.setItemName(productDetailElement.getAttribute("productName"));
            item.setNovatorName(productDetailElement.getAttribute("novatorName"));
            item.setItemSKU(productDetailElement.getAttribute("vendorSKU"));

            String price = (String) arguments.get("price");
            if(price == null)  // Try to get the price again if we are
            {                  // adding a different item to the same recipient.
                price = (String) arguments.get("PRICE");
            	if( price==null || price.length()==0 ) {
            		price = "standard";
            	}
            }
            item.setPriceType(price);

            // JMP 4/7/03
            // reset the custom item info when updating to a different item
            // with the same recipient
            item.setShippingMethod("florist");
            item.setProductType(null);
            item.setShippingCarrier(null);
            item.setNovatorId(GeneralConstants.CUSTOM_ORDER_PID);
            if(order.getSendToCustomer().isDomesticFlag() && order.getDomesticServiceFee() != null)
            {
                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
            }
            else if(!order.getSendToCustomer().isDomesticFlag() && order.getInternationalServiceFee() != null)
            {
                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
            }
            // If ship cost was never set put it to zero
            if(item.getShippingCost() == null)
            {
                item.setShippingCost(new BigDecimal(0));
            }
            // end same recipient code

            // Set comments for custom item
            item.setItemComments(FieldUtils.replaceAll((String) arguments.get("comments"), GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT));
            item.setFloristComments(FieldUtils.replaceAll((String) arguments.get("desc"), GeneralConstants.LINE_SEPARATOR, GeneralConstants.LINE_SEPARATOR_REPLACEMENT));

            // If ship cost was never set put it to zero
            if(item.getShippingCost() == null) {
                item.setShippingCost(new BigDecimal(0));
            }

            // Calculate and set pricing for added item
            if(!item.getPriceType().equals("variable")) {
                try {
                    item.setRegularPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                    item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "Price")));
                }
                catch(Exception e) {
                    this.getLogManager().error("Product #" + item.getProductId() + " has pricing inconsistencies.  Price type " + item.getPriceType() + " was selected but not found in the product details.");
                }

                if(order.getRewardType() != null && !order.getRewardType().equals("")) {
                    try {
                        item.setDiscountAmount(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "RewardValue")));
                        item.setDiscountDescription(order.getRewardType());

                        if(order.getRewardType().equals("Percent") || order.getRewardType().equals("Dollars")) {

                            if(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice") != null) {
                                item.setDiscountedPrice(new BigDecimal(productDetailElement.getAttribute(item.getPriceType() + "DiscountPrice")));
                            }
                        }
                    }
                    catch(Exception e) {
                        this.getLogManager().error("Product #" + item.getProductId() + " has discount inconsistencies.");
                    }
                }
            }
            else if(arguments.get("VARIABLE_PRICE_AMT") != null && !((String)arguments.get("VARIABLE_PRICE_AMT")).equals("")) {
                try {
                    String discountAmount = "0";

                    item.setRegularPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    item.setDiscountedPrice(new BigDecimal((String) arguments.get("VARIABLE_PRICE_AMT")).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("inPriceHeaderId", order.getPricingCode());
                    dataRequest.addArgument("inItemPrice", (String)arguments.get("VARIABLE_PRICE_AMT"));

                    // Determine if its a partner or standard discount
                    if ( (order.getPartnerId() != null) &&
                       (!order.getPartnerId().equals("")) &&
                       (order.getPricingCode() != null) &&
                       (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
                    {
                        item.setDiscountAmount(new BigDecimal(searchUTIL.retrievePromotionValue(order.getPromotionList(), item.getRegularPrice().setScale(0, BigDecimal.ROUND_UP).doubleValue())));
                    }
                    else
                    {
                        service =  this.getGenericDataService(DataConstants.SEARCH_GET_DISCOUNT_AMOUNT);
                        dataResponse = service.executeRequest(dataRequest);
                        XMLDocument discountAmountList = (XMLDocument)dataResponse.getDataVO().getData();

                        Element discountAmountElement = null;
                        xpath = "discountAmountList/discountAmount/data";
                        q = new XPathQuery();

                        nl = q.query(discountAmountList, xpath);

                        if (nl != null && nl.getLength() > 0)
                        {
                            discountAmountElement = (Element) nl.item(0);
                            discountAmount = discountAmountElement.getAttribute("discountAmount");
                        }
                        else
                        {
                            this.getLogManager().error("Could not find discount amount for the variable price.");
                        }
                            item.setDiscountAmount(new BigDecimal(discountAmount));
                        }

                        item.setDiscountDescription(order.getRewardType());

                        if(order.getRewardType().equals("Percent")) {
                            item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal((item.getRegularPrice().doubleValue() * (Double.parseDouble(discountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                        }
                        else if(order.getRewardType().equals("Dollars")) {
                            item.setDiscountedPrice(new BigDecimal(item.getRegularPrice().doubleValue() - new BigDecimal(discountAmount).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));

                        }
                        else if(order.getRewardType().equals("Miles") || order.getRewardType().trim().equals("Points")) {
                            order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + item.getDiscountAmount().longValue()));

                        }
                }
                catch(Exception e) {
                    this.getLogManager().error("Product #" + item.getProductId() + " has problems processing variable price.");
                }
            }

            // Set item to order
            order.setItem(new Integer(cartItemNumber), item);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        }
	}

    private FTDSystemVO getCartItemDetail(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Get item requested
            ItemPO item = null;
            String cartItemNumber = (String) arguments.get("cartItemNumber");
            try {
                item = order.getItem(new Integer(cartItemNumber));
            }
            catch(Exception e) {
                this.getLogManager().error("Invalid cart item number:" + cartItemNumber);
            }

            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
            ShoppingUTIL shoppingUTIL = new ShoppingUTIL(this.getLogManager());
            OEParameters oeGlobalParms = searchUTIL.getGlobalParms();

            // order.setCrumbURL("Product Detail", (String) arguments.get("url"));
            String argMonth = (String) arguments.get(ArgumentConstants.CALENDAR_MONTH);
            String argYear = (String) arguments.get(ArgumentConstants.CALENDAR_YEAR);

            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, item.getProductId());
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, item.getSendToCustomer().getZipCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, item.getSendToCustomer().getCountryType());
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, item.getSendToCustomer().getCountry());
            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            FTDDataResponse dataDetailResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT DETAIL PAGE VO");

            XMLEncoder.addSection(document, searchUTIL.pageBuilder(false, false, (XMLDocument)dataDetailResponse.getDataVO().getData(), order, 1, argMonth, argYear).getChildNodes());

            Element element = null;
            String xpath = "root/productList/products/product";
            XPathQuery q = new XPathQuery();

            NodeList nl = q.query(document, xpath);
            if (nl.getLength() > 0)
            {
                element = (Element) nl.item(0);
                if( item.getProductType()!=null &&
                    (item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                     item.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) &&
                    order.getSendToCustomer().isDomesticFlag() )
                {
                    element.setAttribute("extraShippingFee", String.valueOf(oeGlobalParms.getFreshCutSatCharge()));
                }
                else
                {
                    element.setAttribute("extraShippingFee","");
                }
            }

            // If we are loading the item detail from the delivery info breadcrumb
            // then we need to show the breadcrumbs on the page
            String showBreadCrumbs = (String) arguments.get("showBreadCrumbs");
            if(showBreadCrumbs != null && showBreadCrumbs.equals(GeneralConstants.YES))
            {
                XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());
            }
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("occasionIn", order.getOccasion());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, item.getProductId());
            service =  this.getGenericDataService(DataConstants.SEARCH_GET_EXTRA_PRODUCT_DETAIL);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());
            XMLEncoder.addSection(document, shoppingUTIL.transformItemDetailXML(item).getChildNodes());

            HashMap orderDataMap = new HashMap();
            orderDataMap.put("zipCode", item.getSendToCustomer().getZipCode());
            orderDataMap.put("occasion", item.getOccasion());
            orderDataMap.put("subCodeSelected", item.getSubCodeId());
            orderDataMap.put("country", item.getSendToCustomer().getCountry());
            orderDataMap.put("countryType", item.getSendToCustomer().getCountryType());
            orderDataMap.put("rewardType", item.getDiscountDescription());
            if(item.getDeliveryDate() != null) {
                orderDataMap.put("requestedDeliveryDate", FieldUtils.formatUtilDateToString(item.getDeliveryDate()));
            }

            // Added so we can go back to the shopping page
            if ( item.getDeliveryDateDisplay() != null )
            {
                orderDataMap.put("requestedDeliveryDateDisplay", item.getDeliveryDateDisplay());
            }
            orderDataMap.put("occasionDescription", searchUTIL.getDescriptionForCode(order.getOccasion(), SearchUTIL.GET_OCCASION_DESCRIPTION));
            orderDataMap.put("city", item.getSendToCustomer().getCity());
            orderDataMap.put("state", item.getSendToCustomer().getState());
            // End shopping page params

            XMLEncoder.addSection(document, "orderData", "data", orderDataMap);

            // Build data for page displays
            HashMap pageDataMap = new HashMap();
            // check values of flags that were set in updateDeliveryInfo() function
            if ( order.getDisplayFloral().equals(GeneralConstants.NO) )
            {
                pageDataMap.put("displayFloristPopup", GeneralConstants.YES);
            }
            else if ( order.getDisplayNoProduct().equals(GeneralConstants.NO) )
            {
                pageDataMap.put("displayNoProductPopup", GeneralConstants.YES);
            }
            else if ( order.getDisplaySpecialtyGift().equals(GeneralConstants.NO) )
            {
                pageDataMap.put("displaySpecGiftPopup", GeneralConstants.YES);
            }

            // indicates whether product is codified special and should be displayed
            pageDataMap.put("displayCodifiedSpecial", getDisplayCodifiedSpecial("productList" ,dataDetailResponse, item));
            if(!((String)pageDataMap.get("displayCodifiedSpecial")).equals("Y"))
            {
                pageDataMap.put("displayProductUnavailable", order.getDisplayProductUnavailable());
            }

            // get substitution preference
            pageDataMap.put("secondChoiceAuth", item.getSubstitutionAuth());
            pageDataMap.put("allowSubstitution", oeGlobalParms.getAllowSubstitution());

            XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private void addDuplicateItemToCart(FTDArguments arguments)
	{
        OrderPO order = null;

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            ItemPO item = null;

            try {
                item = order.getItem(new Integer((String)arguments.get("cartItemNumber")));
            } catch(Exception e) {
                this.getLogManager().error("Cart item number " + (String)arguments.get("cartItemNumber") + " is invalid.");
            }

            ItemPO itemClone = (ItemPO) item.clone();
            order.addItem(itemClone);

            CustomerPO customerClone = (CustomerPO) item.getSendToCustomer().clone();
            itemClone.setSendToCustomer(customerClone);

            LinkedList addOnListClone = (LinkedList) item.getAddOnList().clone();
            itemClone.setAddOnList(addOnListClone);

            if(item.getAddOnList() != null && item.getAddOnList().size() > 0)
            {
                AddOnPO addOnClone = null;
                itemClone.getAddOnList().clear();
                Iterator addOnIterator = item.getAddOnList().iterator();
                while(addOnIterator.hasNext())
                {
                    addOnClone = (AddOnPO) ((AddOnPO) addOnIterator.next()).clone();
                    itemClone.addAddOn(addOnClone);
                }
            }


            /*AddOnPO addOnClone = null;
            HashSet newAddOnSet = new HashSet();
            Set oldAddOnSet = item.getAddOnSet();


            if(oldAddOnSet != null && oldAddOnSet.size() > 0)
            {
                Iterator oldAddOnIterator = oldAddOnSet.iterator();
                while(oldAddOnIterator.hasNext())
                {
                    addOnClone = (AddOnPO) ((AddOnPO) oldAddOnIterator.next()).clone();
                    newAddOnSet.add(addOnClone);
                }
            }

            itemClone.setAddOnSet(newAddOnSet);*/

        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
	}

    private void updateDNIS(FTDArguments arguments)
	{
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Get default DNIS for the company
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("inCompanyId", order.getCompanyId());

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_COMPANY_BY_ID);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            NodeList nl = null;
            String xpath = "companyList/company/data";
            XPathQuery q = new XPathQuery();

            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            try {
                Element companyElement = (Element) nl.item(0);
                order.setDnisCode(new Integer(companyElement.getAttribute("dnisId")));
                order.setSourceCode(companyElement.getAttribute("sourceCode"));
            }
            catch(Exception e) {
                this.getLogManager().error("Problems occured during DNIS change while pulling default DNIS from company.");
            }

            // Get DNIS information
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("dnisId", order.getDnisCode().toString());

            service =  this.getGenericDataService(DataConstants.VALIDATE_DNIS);
            dataResponse = service.executeRequest(dataRequest);

            nl = null;
            xpath = "validateDnis/dnisInformation/dnis";
            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            try {
                Element dnisElement = (Element) nl.item(0);

                order.setCompanyName(dnisElement.getAttribute("companyName"));
                order.setScriptCode(dnisElement.getAttribute("scriptCode"));
                order.setHeaderValue("dnisType", order.getScriptCode());
            }
            catch(Exception e) {
                this.getLogManager().error("Problems occured during DNIS change while setting up new DNIS data to order.");
            }

            this.updateSourceCode(order, arguments);

        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
	}

    private void updateSourceCode(FTDArguments arguments)
	{
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            this.updateSourceCode(order, arguments);

        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
	}

    private void updateSourceCode(OrderPO order, FTDArguments arguments) throws Exception
    {
        BigDecimal feeWithFuelSurcharge;
        Connection connection = null;
        BigDecimal fuelSurchargeAmt = null;

        // Get fuel surcharge amount
        try {
            connection = DataManager.getInstance().getConnection();
            fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));
        } finally {
            if(connection != null) {
                connection.close();
            }
        }

        if(arguments.get("sourceCode") != null && !((String) arguments.get("sourceCode")).trim().equals("")) {
            order.setSourceCode((String) arguments.get("sourceCode"));
        }

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date deliveryDate = order.getDeliveryDate();

        FTDDataRequest dataRequest = new FTDDataRequest();
        dataRequest.addArgument("sourcecode", order.getSourceCode());
        if (deliveryDate == null) {
            dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
            this.getLogManager().debug("updateSourceCode: " +
                order.getSourceCode() + " null delivery date");
        } else {
            dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
            this.getLogManager().debug("updateSourceCode: " +
                order.getSourceCode() + " " + df.format(deliveryDate));
        }

        GenericDataService service =  this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
        FTDDataResponse dataResponse = service.executeRequest(dataRequest);

        // Pull the source code detail
        String xpath = "sourceCodeDataList/sourceCodeData/data";

        XPathQuery q = new XPathQuery();
        NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

        if (nl.getLength() > 0)
        {
            Element sourceData = (Element) nl.item(0);
            order.setMarketingGroup(sourceData.getAttribute("marketingGroup"));

            String rewardType = "";
            if(sourceData.getAttribute("discountType").equals("P"))
                rewardType = "Percent";
            else if(sourceData.getAttribute("discountType").equals("D"))
                rewardType = "Dollars";
            else if(sourceData.getAttribute("discountType").equals("M"))
                rewardType = "Miles";

            // Set the order with source code data
            order.setRewardType(rewardType);
            order.setDomesticServiceFee(new BigDecimal(sourceData.getAttribute("domesticServiceCharge")));
            order.setInternationalServiceFee(new BigDecimal(sourceData.getAttribute("intlServiceCharge")));
            order.setPaymentMethod(sourceData.getAttribute("validPayMethod"));
            order.setPaymentMethodType(sourceData.getAttribute("paymentType"));
            order.setPartnerId(sourceData.getAttribute("partnerId"));
            order.setBillingPrompt(sourceData.getAttribute("billingInfoPrompt"));
            order.setBillingInfoLogic(sourceData.getAttribute("billingInfoLogic"));
            order.setSourceCodeDescription(sourceData.getAttribute("description"));
            order.setPricingCode(sourceData.getAttribute("pricingCode"));
            order.setSourceCodeDescription(sourceData.getAttribute("description"));
            order.setSourceCodeType(sourceData.getAttribute("sourceType"));

            if ( (sourceData.getAttribute("jcPenneyFlag") != null) &&
                 (sourceData.getAttribute("jcPenneyFlag").equals(GeneralConstants.YES)) )
            {
                order.setJcPenneyFlag(Boolean.TRUE);
            }
        }

        // Set up header for customer information
        order.setHeaderValue("sourceCodeSelect", order.getSourceCode());
        order.setHeaderValue("sourceCodeText", order.getSourceCodeDescription());

        dataRequest = new FTDDataRequest();
        dataRequest.addArgument("promotionID", order.getPartnerId());

        service =  this.getGenericDataService(DataConstants.SEARCH_GET_PROMOTIONS);
        dataResponse = service.executeRequest(dataRequest);

        // Clear previous promotions from order
        order.getPromotionList().clear();

        // Pull the promotion details
        Element promotionData = null;
        PromotionPO partnerPromotion = null;
        xpath = "promotionData/promotions/promotion";

        q = new XPathQuery();
        nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

        if ( (nl.getLength() > 0) &&
             (order.getPricingCode() != null) &&
             (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
        {
            for ( int i=0; i < nl.getLength(); i++ )
            {
                promotionData = (Element) nl.item(i);

                partnerPromotion = new PromotionPO();
                partnerPromotion.setPromotionId(promotionData.getAttribute("promotionId"));
                partnerPromotion.setBasePoints(new Long(promotionData.getAttribute("basePoints")));
                partnerPromotion.setVariablePoints(new BigDecimal(promotionData.getAttribute("unitPoints")));
                partnerPromotion.setPointDriver(promotionData.getAttribute("unit"));
                partnerPromotion.setLowPrice(new Double(promotionData.getAttribute("minPrice")));
                partnerPromotion.setHighPrice(new Double(promotionData.getAttribute("maxPrice")));

                order.setRewardType(promotionData.getAttribute("rewardType"));
                order.addPromotion(partnerPromotion);
            }
        }

        ItemPO item = null;
        Element productElement = null;
        Element discountAmountElement = null;
        Element stateData = null;
        XMLDocument discountAmountList = null;
        String discountAmount = null;
        double currentPrice;
        SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

        Collection itemCollection = order.getAllItems().values();
        Iterator itemIterator = itemCollection.iterator();

        while(itemIterator.hasNext())
        {
            item = (ItemPO) itemIterator.next();

            item.setDiscountDescription(order.getRewardType());
            item.setDiscountedPrice(item.getRegularPrice());

            deliveryDate = item.getDeliveryDate();

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("sourcecode", order.getSourceCode());
            if (deliveryDate == null) {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
                this.getLogManager().debug("updateSourceCode: " +
                    order.getSourceCode() + " null delivery date");
            } else {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
                this.getLogManager().debug("updateSourceCode: " +
                    order.getSourceCode() + " " + df.format(deliveryDate));
            }

            service =  this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
            dataResponse = service.executeRequest(dataRequest);

            // Pull the source code detail
            xpath = "sourceCodeDataList/sourceCodeData/data";

            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                Element sourceData = (Element) nl.item(0);
                order.setDomesticServiceFee(new BigDecimal(sourceData.getAttribute("domesticServiceCharge")));
                order.setInternationalServiceFee(new BigDecimal(sourceData.getAttribute("intlServiceCharge")));
            }

            this.getLogManager().debug("Carrier: " + item.getShippingCarrier() + " " +
                item.getSendToCustomer().getCountryType() + " " +
                df.format(item.getDeliveryDate()));

            if( item.getShippingCarrier() != null ) {
                if (item.getSendToCustomer().getCountryType() != null && order.getDomesticServiceFee() != null && item.getSendToCustomer().getCountryType().equals("D")) {
                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
                else if (item.getSendToCustomer().getCountryType() != null && order.getInternationalServiceFee() != null && item.getSendToCustomer().getCountryType().equals("I")) {
                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                    item.setShippingCost(feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN));
                }
            }

            // Determine if its a partner or standard discount
            if ( (order.getPartnerId() != null) &&
                 (!order.getPartnerId().equals("")) &&
                 (order.getPricingCode() != null) &&
                 (order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE)) )
            {
                item.setDiscountAmount(new BigDecimal(searchUTIL.retrievePromotionValue(order.getPromotionList(), item.getRegularPrice().setScale(0, BigDecimal.ROUND_UP).doubleValue())));
            }
            else {

                if(order.getRewardType() != null)
                {
                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("pagename", "");
                    dataRequest.addArgument("company", order.getCompanyId());
                    dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, item.getProductId());
                    dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                    dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, item.getSendToCustomer().getZipCode());
                    dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, item.getSendToCustomer().getCountry());

                   if ( order.getSendToCustomer().isDomesticFlag() )
                    {
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
                    }
                    else
                    {
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                    }
                    deliveryDate = item.getDeliveryDate();
                    if (deliveryDate == null) {
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
                    } else {
                        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
                    }

                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
                    dataResponse = service.executeRequest(dataRequest);

                    // Pull the source code detail
                    xpath = "productList/products/product";
                    q = new XPathQuery();
                    nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                    if (nl.getLength() > 0) {
                        productElement = (Element) nl.item(0);
                    }

                    if(item.getPriceType().equals("variable")) {
                        discountAmount = "0";

                        dataRequest = new FTDDataRequest();
                        dataRequest.addArgument("inPriceHeaderId", order.getPricingCode());
                        dataRequest.addArgument("inItemPrice", item.getRegularPrice().toString());
                        service =  this.getGenericDataService(DataConstants.SEARCH_GET_DISCOUNT_AMOUNT);
                        dataResponse = service.executeRequest(dataRequest);
                        discountAmountList = (XMLDocument)dataResponse.getDataVO().getData();

                        nl = null;
                        xpath = "discountAmountList/discountAmount/data";
                        q = new XPathQuery();

                        nl = q.query(discountAmountList, xpath);

                        if (nl != null && nl.getLength() > 0)
                        {
                            discountAmountElement = (Element) nl.item(0);
                            item.setDiscountAmount(new BigDecimal(discountAmountElement.getAttribute("discountAmount")));
                        }
                        else
                        {
                            this.getLogManager().error("Could not find discount amount for the variable price.");
                        }
                    }
                    else {
                        try {
                            item.setDiscountAmount(new BigDecimal(productElement.getAttribute(item.getPriceType() + "DiscountAmt")));
                        }
                        catch(Exception e) {
                            item.setDiscountAmount(new BigDecimal(0));
                        }
                    }

                    if(item.getDiscountAmount().doubleValue() > 0) {
                        // Discount type of dollars off
                        if(order.getRewardType().equals("Dollars")) {
                            currentPrice = item.getRegularPrice().doubleValue();
                            currentPrice = currentPrice - item.getDiscountAmount().doubleValue();
                            item.setDiscountedPrice(new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_DOWN));
                        }

                        // Discount type of percent off
                        else if(order.getRewardType().equals("Percent")) {
                            currentPrice = item.getRegularPrice().doubleValue();
                            currentPrice = currentPrice - new BigDecimal((currentPrice * (item.getDiscountAmount().doubleValue() / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                            item.setDiscountedPrice(new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_DOWN));
                        }
                    }
                }
            }

            // Change to a DNIS needs to update taxes
            if(arguments.get("dnis") != null) {
                item.setTax(new BigDecimal(0));

                if ( (item.getSendToCustomer().getCountryType() != null) &&
                     (item.getSendToCustomer().getCountryType().equals("D")) &&
                     (item.getSendToCustomer().getState() != null) &&
                     (!item.getSendToCustomer().getState().equals("")) &&
                     (order.getScriptCode() != null) &&
                     (!order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE)) )
                {
                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("inStateCode", item.getSendToCustomer().getState());

                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_STATE_DETAIL);
                    dataResponse = service.executeRequest(dataRequest);

                    xpath = "stateList/state/data";
                    q = new XPathQuery();
                    nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                    if ( nl.getLength() > 0 )
                    {
                        stateData = (Element) nl.item(0);
                        try
                        {
                            if(stateData.getAttribute("taxRate") != null && !stateData.getAttribute("taxRate").trim().equals("")) {
                                item.setTax(new BigDecimal(Double.parseDouble(stateData.getAttribute("taxRate")) / 100));
                            }
                        }
                        catch(Exception e)
                        {
                            this.getLogManager().error("Problems calculating taxes for item #" + item.getProductId() + " with the state of " + item.getSendToCustomer().getState());
                        }
                    }
                }
            }
        }
    }

    private void logoutFromShopping(FTDArguments arguments)
	{
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            order.setOrderStatus(GeneralConstants.ABANDONED);

            // Clear database of all data no longer needed
            order.emptyMapStorage();
            order.getPromotionList().clear();
            order.setSendToCustomer(null);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally
		{
        	this.returnPersistentServiceObject(order);
        }
	}

    /**
     *
     * @return OEParameters object that contains the values of the global settings
     */
    private OEParameters getGlobalParms(String dataType, XMLDocument data)
    {
        OEParameters oeGlobalParms = new OEParameters();

        try
        {
            Element globalData = null;
            XPathQuery q = new XPathQuery();
            String xpath = dataType + "/globalParmsData/data";
            NodeList nl = q.query(data, xpath);

            if (nl.getLength() > 0)
            {
                SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");

                globalData = (Element) nl.item(0);

                //oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(globalData.getAttribute("deliveryDaysOut")));
                oeGlobalParms.setDeliveryDaysOutMAX(Integer.parseInt(globalData.getAttribute("deliveryDaysOutMAX")));
                oeGlobalParms.setDeliveryDaysOutMIN(Integer.parseInt(globalData.getAttribute("deliveryDaysOutMIN")));
                oeGlobalParms.setAllowSubstitution(globalData.getAttribute("allowSubstitution"));
                oeGlobalParms.setExoticCutoff(globalData.getAttribute("exoticCutoff"));
                oeGlobalParms.setFreshCutCutoff(globalData.getAttribute("freshcutsCutoff"));
                oeGlobalParms.setFridayCutoff(globalData.getAttribute("fridayCutoff"));
                oeGlobalParms.setGNADDDate(sdfInput.parse(globalData.getAttribute("gnaddDate")));
                oeGlobalParms.setGNADDLevel(globalData.getAttribute("gnaddLevel"));
                oeGlobalParms.setIntlAddOnDays(Integer.parseInt(globalData.getAttribute("intlOverrideDays")));
                oeGlobalParms.setIntlCutoff(globalData.getAttribute("internationalCutoff"));
                oeGlobalParms.setMondayCutoff(globalData.getAttribute("mondayCutoff"));
                oeGlobalParms.setSaturdayCutoff(globalData.getAttribute("saturdayCutoff"));
                oeGlobalParms.setSpecialtyGiftCutoff(globalData.getAttribute("specialtyGiftCutoff"));
                oeGlobalParms.setSundayCutoff(globalData.getAttribute("sundayCutoff"));
                oeGlobalParms.setThursdayCutoff(globalData.getAttribute("thursdayCutoff"));
                oeGlobalParms.setTuesdayCutoff(globalData.getAttribute("tuesdayCutoff"));
                oeGlobalParms.setWednesdayCutoff(globalData.getAttribute("wednesdayCutoff"));
                oeGlobalParms.setFreshCutSrvcChargeTrigger(globalData.getAttribute("freshCutsSvcChargeTrigger"));
                if(globalData.getAttribute("freshCutsSvcCharge") != null && !globalData.getAttribute("freshCutsSvcCharge").equals("")) {
                    oeGlobalParms.setFreshCutSrvcCharge(new BigDecimal(globalData.getAttribute("freshCutsSvcCharge")));
                }
                if(globalData.getAttribute("specialSvcCharge") != null && !globalData.getAttribute("specialSvcCharge").equals("")) {
                    oeGlobalParms.setSpecialSrvcCharge(new BigDecimal(globalData.getAttribute("specialSvcCharge")));
                }
                if(globalData.getAttribute("freshCutsSatCharge") != null && !globalData.getAttribute("freshCutsSatCharge").equals("")) {
                    oeGlobalParms.setFreshCutSatCharge(new BigDecimal(globalData.getAttribute("freshCutsSatCharge")));
                }
            }
        }
        catch (Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return oeGlobalParms;
    }

    private OEParameters getGlobalParms()
    {
        OEParameters oeGlobalParms = new OEParameters();
        FTDDataRequest dataRequest = new FTDDataRequest();

        try
        {
            // retrieve the Global Parameter information
            GenericDataService service =  this.getGenericDataService(DataConstants.SHOPPING_GET_GLOBAL_PARMS);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            Element globalData = null;
            String xpath = "globalParmsDataSet/globalParmsData/data";

            XPathQuery q = new XPathQuery();
            NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");

                globalData = (Element) nl.item(0);

                //oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(globalData.getAttribute("deliveryDaysOut")));
                oeGlobalParms.setDeliveryDaysOutMAX(Integer.parseInt(globalData.getAttribute("deliveryDaysOutMAX")));
                oeGlobalParms.setDeliveryDaysOutMIN(Integer.parseInt(globalData.getAttribute("deliveryDaysOutMIN")));
                oeGlobalParms.setAllowSubstitution(globalData.getAttribute("allowSubstitution"));
                oeGlobalParms.setExoticCutoff(globalData.getAttribute("exoticCutoff"));
                oeGlobalParms.setFreshCutCutoff(globalData.getAttribute("freshcutsCutoff"));
                oeGlobalParms.setFridayCutoff(globalData.getAttribute("fridayCutoff"));
                oeGlobalParms.setGNADDDate(sdfInput.parse(globalData.getAttribute("gnaddDate")));
                oeGlobalParms.setGNADDLevel(globalData.getAttribute("gnaddLevel"));
                oeGlobalParms.setIntlAddOnDays(Integer.parseInt(globalData.getAttribute("intlOverrideDays")));
                oeGlobalParms.setIntlCutoff(globalData.getAttribute("internationalCutoff"));
                oeGlobalParms.setMondayCutoff(globalData.getAttribute("mondayCutoff"));
                oeGlobalParms.setSaturdayCutoff(globalData.getAttribute("saturdayCutoff"));
                oeGlobalParms.setSpecialtyGiftCutoff(globalData.getAttribute("specialtyGiftCutoff"));
                oeGlobalParms.setSundayCutoff(globalData.getAttribute("sundayCutoff"));
                oeGlobalParms.setThursdayCutoff(globalData.getAttribute("thursdayCutoff"));
                oeGlobalParms.setTuesdayCutoff(globalData.getAttribute("tuesdayCutoff"));
                oeGlobalParms.setWednesdayCutoff(globalData.getAttribute("wednesdayCutoff"));
                oeGlobalParms.setLatestCutoff(globalData.getAttribute("latestCutoff"));
                oeGlobalParms.setFreshCutSrvcChargeTrigger(globalData.getAttribute("freshCutsSvcChargeTrigger"));
                oeGlobalParms.setDispatchOrderFlag(globalData.getAttribute("dispatchOrderFlag"));
                if(globalData.getAttribute("freshCutsSvcCharge") != null && !globalData.getAttribute("freshCutsSvcCharge").equals("")) {
                    oeGlobalParms.setFreshCutSrvcCharge(new BigDecimal(globalData.getAttribute("freshCutsSvcCharge")));
                }
                if(globalData.getAttribute("specialSvcCharge") != null && !globalData.getAttribute("specialSvcCharge").equals("")) {
                    oeGlobalParms.setSpecialSrvcCharge(new BigDecimal(globalData.getAttribute("specialSvcCharge")));
                }
                if(globalData.getAttribute("freshCutsSatCharge") != null && !globalData.getAttribute("freshCutsSatCharge").equals("")) {
                    oeGlobalParms.setFreshCutSatCharge(new BigDecimal(globalData.getAttribute("freshCutsSatCharge")));
                }
                if(globalData.getAttribute("fuelSurchargeAmt") != null && !globalData.getAttribute("fuelSurchargeAmt").equals("")) {
                    oeGlobalParms.setFuelSurchargeAmt(new BigDecimal(globalData.getAttribute("fuelSurchargeAmt")));
                }

            }
        }
        catch (Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return oeGlobalParms;
    }

    /**
     *
     * @param countryId String parameter that holds the country id to retrieve
     *                  the holiday dates for
     * @return HashMap containing the retrieved dates and their associated
     *                  information (text, deliverable flag)
     */
    private HashMap getHolidayDates(String countryId)
    {
        HashMap holidayMap = new HashMap();
        FTDDataRequest dataRequest = new FTDDataRequest();

        try
        {
            // retrieve the Global Parameter information
            GenericDataService service =  this.getGenericDataService(DataConstants.SHOPPING_GET_HOLIDAY_DATES);
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, countryId);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            Element element = null;
            String xpath = "holidayDates/holidayDate/date";

            XPathQuery q = new XPathQuery();
            NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                for ( int i = 0; i < nl.getLength(); i++ )
                {
                    element = (Element) nl.item(i);

                    OEDeliveryDate holidayDate = new OEDeliveryDate();
                    holidayDate.setDeliveryDate(element.getAttribute("holidayDate"));
                    holidayDate.setHolidayText(element.getAttribute("holidayDescription"));
                    holidayDate.setShippingAllowed(element.getAttribute("shippingAllowed"));
                    holidayDate.setDeliverableFlag(element.getAttribute("deliverableFlag"));

                    holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
                }
            }
        }
        catch (Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return holidayMap;
    }

    private String getDisplayCodifiedSpecial(String elementName, FTDDataResponse dataResponse, ItemPO item) throws Exception
    {
        // retrieve the product special codified flag
        String xpath = elementName + "/products/product";
        XPathQuery q = new XPathQuery();
        NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
        Element product = null;
        String codifiedSpecialFlag = "N";
        String deliverableFlag = "N";
        if(nl.getLength() > 0)
        {
            product = (Element) nl.item(0);
            codifiedSpecialFlag = product.getAttribute("codifiedSpecial");
            if(codifiedSpecialFlag == null) codifiedSpecialFlag = "N";

            deliverableFlag = product.getAttribute("codifiedDeliverable");
            if(deliverableFlag == null) deliverableFlag = "N";
        }
        else if(item.getCodifiedSpecial() != null)
        {
             codifiedSpecialFlag = item.getCodifiedSpecial();
        }

        // retrieve the florist information
        //xpath = elementName +  "/floristListData/data";
        //q = new XPathQuery();
        //NodeList floristNodelist = q.query(dataResponse.getDataVO().getData().toString(), xpath);
        String ret = "N";

        // check for at least one florist that has this product available
        if(codifiedSpecialFlag.equals("Y") && (deliverableFlag.equals("N")) /*&& oeGlobalParms.getGNADDLevel().equals("0")*/)
        {
            ret = "Y";
        }

        return ret;
    }
}
