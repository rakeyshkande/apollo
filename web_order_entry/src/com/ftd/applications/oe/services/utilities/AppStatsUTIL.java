package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.common.utilities.LogManager;

import com.ftd.applications.oe.admin.util.ConnectionPool;

import java.text.SimpleDateFormat;
import java.sql.*; 

public class AppStatsUTIL extends SuperUTIL 
{
    public AppStatsUTIL(LogManager lm)
    {
        super(lm);

    }

    public String getNumberUsersSince(java.util.Date dateSince)
    {
        String ret = "";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ConnectionPool pool = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            // parse date
            String strDate = sdf.format(dateSince);

            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select count(*) from ftd_apps.security_cert where last_update_date > to_date('" + strDate + "', 'YYYY-MM-DD HH24:MI:SS')");
            while(resultSet.next())
            {
                ret = resultSet.getString(1);
            }

        }
        catch(Exception e)
        {
            lm.error(e);
        }
        finally
        {
            try
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }

                if(statement != null)
                {
                      statement.close();  
                }
            
                if(connection != null)
                {
                    pool.free(connection);
                }
            }
            catch(Exception e)
            {
                lm.error(e);
            }
        }

        return ret;
    }

    public String getNumberOrders(java.util.Date dateSince)
    {
        String ret = "";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ConnectionPool pool = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            // parse date
            String strDate = sdf.format(dateSince);

            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select count(*) from ftd_apps.order_blob_archive where order_timestamp > GET_TIME_IN_MILLIS('" + strDate + "') AND STATUS = 'HP_ARCHIVE'");
            while(resultSet.next())
            {
                ret = resultSet.getString(1);
            }

        }
        catch(Exception e)
        {
            lm.error(e);
        }
        finally
        {
            try
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }

                if(statement != null)
                {
                      statement.close();  
                }
            
                if(connection != null)
                {
                    pool.free(connection);
                }
            }
            catch(Exception e)
            {
                lm.error(e);
            }
        }

        return ret;
    }

    public String getAverageCallTimeSince(java.util.Date dateSince)
    {
        String ret = "";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ConnectionPool pool = null;
        int total = 0;
        int count = 0;
        int average = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            // parse date
            String strDate = sdf.format(dateSince);

            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select CALL_TIME from orders where last_update_date > TO_DATE('" + strDate + "', 'YYYY-MM-DD HH24:MI:SS')");
            while(resultSet.next())
            {
                count++;
                total += resultSet.getInt(1);
            }

            average = total / count;
            ret = String.valueOf(average);
        }
        catch(Exception e)
        {
            lm.error(e);
        }
        finally
        {
            try
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }

                if(statement != null)
                {
                      statement.close();  
                }
            
                if(connection != null)
                {
                    pool.free(connection);
                }
            }
            catch(Exception e)
            {
                lm.error(e);
            }
        }

        return ret;
    }

    public String getOpenDBCursors()
    {
        String ret = "";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ConnectionPool pool = null;
        try
        {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            statement = connection.createStatement();
            //resultSet = statement.executeQuery("select CALL_TIME from orders where last_update_date > TO_DATE('" + strDate + "', 'YYYY-MM-DD HH24:MI:SS')");
            //while(resultSet.next())
            //{
            //    ret = resultSet.getString(1);
            //}
        }
        catch(Exception e)
        {
            lm.error(e);
        }
        finally
        {
            try
            {
                if(resultSet != null)
                {
                    resultSet.close();
                }

                if(statement != null)
                {
                      statement.close();  
                }
            
                if(connection != null)
                {
                    pool.free(connection);
                }
            }
            catch(Exception e)
            {
                lm.error(e);
            }
        }

        return ret;
    }    
}