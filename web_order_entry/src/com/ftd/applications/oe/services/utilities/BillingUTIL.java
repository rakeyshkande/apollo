package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.dataaccessservices.DataManager;
import com.ftd.applications.oe.common.persistent.*;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.*;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.vo.MessageToken;
import java.sql.Connection;
import com.ftd.osp.utilities.systemmessenger.*;
import org.xml.sax.*;
import java.sql.*;
import javax.xml.parsers.*;
import java.io.*;
import com.ftd.osp.utilities.systemmessenger.exception.*;
import com.ftd.osp.utilities.order.vo.*;
import java.util.*;
import com.ftd.applications.oe.common.DomainObjectConverter;
import com.ftd.osp.ordervalidator.util.*;

import javax.transaction.UserTransaction;
import javax.naming.InitialContext;


public class BillingUTIL extends SuperUTIL
{
    private static final String DISPATCH_STATUS = "ES1005";
    
    public BillingUTIL(com.ftd.framework.common.utilities.LogManager lm)
    {
        super(lm);
    }
            
    public void dispatchOrder(OrderPO order, boolean useTransaction) throws SAXException, SQLException, 
                ParserConfigurationException, IOException, SystemMessengerException
    {
        Connection connection = null;
        InitialContext context = null;
        UserTransaction userTransaction = null;

        try
        {

            //new debugging for prod problem, 3/15/04 -bmunter
            if(order != null)
            {
                lm.debug("BEFORE CONVERSION:" + order.getGUID());

                ItemPO detailVO = order.getItem(new Integer(1));
                if(detailVO != null)
                {
                    CustomerPO cust = detailVO.getSendToCustomer();
                    if(cust !=null)
                    {
                        if(cust.getAddressOne() != null)
                        {
                            lm.debug("ADDRESS BEFORE CONVERSION");
                            lm.debug(cust.getAddressOne());
                        }
                        else
                        {
                            lm.debug("Address One is null.");
                        }
                    }
                    else
                    {
                        lm.debug("Recip is null.");
                    }
                }
                else 
                {
                    lm.debug("OE Product not found.");
                }
            }
        
            // Convert the order
            DomainObjectConverter doc = new DomainObjectConverter();
            OrderVO dOrder = doc.convertToDomain(order);
            lm.info("Order " + dOrder.getGUID()  + " converted...");

            this.logOrder(dOrder, "AFTER CONVERSION");

            context = new InitialContext();
            if (useTransaction) {
              /******************************************************************************/
              userTransaction = (UserTransaction)  context.lookup("java:comp/UserTransaction");
              // Start the transaction with the begin method
              userTransaction.begin();
              lm.info("**************BEGIN USER TRANSACTION**************");
            }
            
            // Get a database connection
            connection = DataManager.getInstance().getConnection();
            
            // Massage Order
            DataMassager dm = new DataMassager();
            dOrder = dm.massageOrderData(dOrder, connection);
            lm.info("DataMassager done...");

            this.logOrder(dOrder, "AFTER MASSAGER");

            //Order is going to Scrub only...does not need to be Fixed - emueller 2/17/04
            // Default the order values not present
            //FRPDataFixer fixer = new FRPDataFixer(connection);
            //dOrder = fixer.fixOrder(dOrder);
            //lm.info("FRPDataFixer done...");

            // Set order status
            setStatusOrderValid(dOrder);
            lm.info("Order " + dOrder.getGUID()  + " status set...");

            // Save the order to FRP
            //FRPMapperDAO dao = new FRPMapperDAO(connection);
            //boolean saved = dao.mapOrderToDB(dOrder);

             this.logOrder(dOrder, "BEFORE SCRUB DAO");

            // Save to scrub schema
            ScrubMapperDAO scrubMapper = new ScrubMapperDAO(connection);
            scrubMapper.mapOrderToDB(dOrder);            

            this.logOrder(dOrder, "AFTER SCRUB DAO");

            lm.info("Order " + dOrder.getGUID()  + " saved to Scrub...");
            
            // Dispatch the order
            MessageToken token = null;

            // Dispatch each Item
            Iterator it = dOrder.getOrderDetail().iterator();
            OrderDetailsVO item = null;
            Dispatcher dispatcher = Dispatcher.getInstance();
            while(it.hasNext())
            {
                item = (OrderDetailsVO)it.next();
                token = new MessageToken();
                token.setMessage(item.getGuid() + "/" + item.getLineNumber());
                token.setStatus(DISPATCH_STATUS);
                dispatcher.dispatch(context, token);
                lm.info("Item " + item.getExternalOrderNumber()  + " dispatched...");
            }

            this.logOrder(dOrder, "DONE WITH EVERYTHING");
            
            if (useTransaction) {
                //Assuming everything went well, commit the transaction.
                userTransaction.commit();
                lm.info("**************END USER TRANSACTION**************");
                /******************************************************************************/
            }
        }
        catch(Exception e)
        {


            lm.error("Could not dispatch order");
            lm.error(e);
        
            SystemMessengerVO vo = new SystemMessengerVO();
            vo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            vo.setType("SYSTEM");
            vo.setSource("WEBOE");
            String error = "Problem dispatching order " + order.getGUID();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            error = error + " " + sw.toString() ;
            vo.setMessage(error);
            SystemMessenger.getInstance().send(vo,connection);
            
            if (useTransaction) {
              // rollback the user transaction
              rollback(userTransaction); 
            } else {
              // we are not using a transaction since we are running
              // in a container managed transaction (WEbOE Dispatcher use of this code in weboe.jar
              // so the container must know about this exception: re-throw it!
              throw new SystemMessengerException(e.getMessage());
            }
        }
        finally
        {
            if(connection != null)
            {
                connection.close();
            }
        }
    }

  /**
   * Rollback the User Transaction
   */
  private void rollback(UserTransaction userTransaction)
  {
      try  
      {
          if (userTransaction != null)  
          {
            // rollback the user transaction
            userTransaction.rollback();

            lm.debug("User transaction rolled back");
          }
      } 
      catch (Exception ex)  
      {
          lm.error(ex);
      } 
  }


    /**
     * This method sets the status of order valid
     * valid
     * @author Jeff Penney
     *
    */
    private void setStatusOrderValid(OrderVO order)
    {
        // Set order status to Valid
        order.setStatus(String.valueOf(OrderStatus.VALID));
        setBuyerStatus(order, String.valueOf(OrderStatus.VALID));

        // Set Item status
        List items = order.getOrderDetail();
        MessageToken mt = null;
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            item.setStatus(String.valueOf(OrderStatus.VALID_ITEM));
            setRecipientStatus(item, String.valueOf(OrderStatus.VALID_ITEM));
        }    
    }

    public void setStatusOrderAbandoned(OrderVO order)
    {
        // Set order status to Valid
        order.setStatus(String.valueOf(OrderStatus.ABANDONED_HEADER));
        setBuyerStatus(order, String.valueOf(OrderStatus.ABANDONED_HEADER));

        // Set Item status
        List items = order.getOrderDetail();
        MessageToken mt = null;
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            item.setStatus(String.valueOf(OrderStatus.ABANDONED_ITEM));
            setRecipientStatus(item, String.valueOf(OrderStatus.ABANDONED_ITEM));
        }    
    }

    private void setBuyerStatus(OrderVO order, String status)
    {
        BuyerVO buyer = null;
        if(order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer != null)
            {
                buyer.setStatus(status);
            }
        }
    }

    private void setRecipientStatus(OrderDetailsVO item, String status)
    {
        RecipientsVO recipient = null;

        if(item.getRecipients().size() > 0)
        {
            recipient = (RecipientsVO)item.getRecipients().get(0);

            if(recipient != null)
            {
                recipient.setStatus(status);
            }
        }
    }    

    private void logOrder(OrderVO dOrder, String desc)
    {
        if(dOrder != null)
        {
            lm.debug(desc + " " + dOrder.getGUID());
            List detailList = dOrder.getOrderDetail();
            if(detailList != null && detailList.size() > 0)
            {
                OrderDetailsVO detailVO = (OrderDetailsVO)detailList.get(0);

                List recipList = detailVO.getRecipients();
                if(recipList != null && recipList.size() > 0)
                {
                    RecipientsVO recip = (RecipientsVO)recipList.get(0);
                    if(recip != null)
                    {
                        List recipientAddressesList = recip.getRecipientAddresses();
                        if(recipientAddressesList != null && recipientAddressesList.size() > 0)
                        {
                            RecipientAddressesVO recipientAddresses = (RecipientAddressesVO) recipientAddressesList.get(0);
                            if(recipientAddresses.getAddressLine1() != null)
                            {
                                lm.debug(desc + " Address:" + recipientAddresses.getAddressLine1());
                            }
                            else
                            {
                                lm.debug(desc + " " + "Address One is null.");
                            }
                        }  
                    }
                }
                else
                {
                    lm.debug(desc + " " + "Recip is null.");
                }  
            }     
            else
            {
                lm.debug(desc + " " + "No items in this order.");
            }
        }
    }

}

