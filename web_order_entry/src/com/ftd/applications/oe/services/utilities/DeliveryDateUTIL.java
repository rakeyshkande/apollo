package com.ftd.applications.oe.services.utilities;

import com.ftd.applications.oe.common.ArgumentConstants;
import com.ftd.applications.oe.common.DataConstants;
import com.ftd.applications.oe.common.FTDUtil;
import com.ftd.applications.oe.common.FieldUtils;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.applications.oe.common.OEDeliveryDate;
import com.ftd.applications.oe.common.OEDeliveryDateParm;
import com.ftd.applications.oe.common.OEParameters;
import com.ftd.applications.oe.common.ShippingMethod;
import com.ftd.applications.oe.common.persistent.OrderPO;
import com.ftd.applications.oe.common.valobjs.ProductVO;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.utilities.LogManager;
import com.ftd.framework.common.utilities.XPathQuery;
import com.ftd.framework.common.valueobjects.FTDDataRequest;
import com.ftd.framework.common.valueobjects.FTDDataResponse;
import com.ftd.framework.dataaccessservices.FTDDataAccessManager;
import com.ftd.framework.dataaccessservices.GenericDataService;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;

import java.math.BigDecimal;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.sql.Connection;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class DeliveryDateUTIL extends SuperUTIL
{
    // Following variables are used for keeping track of last zipcode 
    // cutoff lookup (to avoid repeated DB lookups).
    private String lastCutoffZip    = "";  
    private String lastCutoffProdId = "";  
    private String lastCutoffTime   = "";
    private Boolean weArePastCutoff = null;
    
    public DeliveryDateUTIL(LogManager lm)
    {
        super(lm);
    }

    public static int getDeliveryDaysOut(OrderPO orderPO, OEParameters oeParams) 
    {
      int ret = oeParams.getDeliveryDaysOutMAX();
      if (orderPO != null && oeParams != null && orderPO.getDeliveryDaysOut().equals(ArgumentConstants.DELIVERY_DAYS_OUT_MIN)) 
      {
        ret = oeParams.getDeliveryDaysOutMIN();
      } 
      return ret;
    }
    /**
     * 
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return String XML string representing delivery date information
     */

    public XMLDocument getOrderDeliveryDates(OEDeliveryDateParm parms) throws FTDApplicationException
    {
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderPO order = parms.getOrder();

        // Domestic order, set time zone of calendar
        if ( order.getSendToCustomer().isDomesticFlag() )
        {
            timeZone = getTimeZone(parms); 
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        int maxDays = getDeliveryDaysOut(order, parms.getGlobalParms()) + 1;

        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays);
        return getXMLEncodedDeliveryDates(deliveryDates, parms);
    }

    /**
     * 
     * @param OEDeliveryDateParm object encompassing all information used in creating delivery dates
     * @return String representation of delivery date value object in XML
     */

    public XMLDocument getCalendarDeliveryDates(OEDeliveryDateParm parms) throws FTDApplicationException
    {
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderPO order = parms.getOrder();

        // Domestic order, set time zone of calendar
        if ( order.getSendToCustomer().isDomesticFlag() )
        {
            timeZone = getTimeZone(parms); 
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        int calMonth = calendar.get(Calendar.MONTH);
        if ( (parms.getCalendarMonth() != null) && 
             (!parms.getCalendarMonth().trim().equals("")) )
        {
            calMonth = getCalendarMonth(parms.getCalendarMonth());
        }
        
        int calYear = calendar.get(Calendar.YEAR);
        if ( (parms.getCalendarYear() != null) && 
             (!parms.getCalendarYear().trim().equals("")) )
        {
            calYear = Integer.parseInt(parms.getCalendarYear());
        }
        calendar.set(calYear, calMonth, 1);
        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//        ArrayList deliveryDates = getDeliveryDates(timeZone, calendar, parms, maxDays);
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays);
        
        return getXMLEncodedCalendarDates(timeZone, deliveryDates, parms.getProductId());
    }

    /**
     * 
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return String XML string representing delivery date information
     */
    public OEDeliveryDate getFirstAvailableDate(OEDeliveryDateParm parms) throws FTDApplicationException
    {
        TimeZone timeZone = null;
        Calendar calendar = Calendar.getInstance();
        OrderPO order = parms.getOrder();

        // Domestic order, set time zone of calendar
        if (  order.getSendToCustomer().isDomesticFlag() )
        {
            timeZone = getTimeZone(parms); 
            calendar.setTimeZone(timeZone);
        }

        // set the number of days to process plus one
        int maxDays = getDeliveryDaysOut(order, parms.getGlobalParms()) + 1;
        ArrayList deliveryDates = getProductDeliveryDates(true, calendar, parms, maxDays);
        // Loop through delivery dates looking for the first available one.
        // If the first available date is in a range then calculate all the dates
        // in that range excluding dates that are not deliverable
        OEDeliveryDate deliveryDate = null;
        OEDeliveryDate rangeDate = null;
        StringBuffer dateRangeDisplayBuffer = new StringBuffer();
        boolean inRange = false;
        boolean firstRangeDate = true;
        for (int i = 0; i < deliveryDates.size(); i++) 
        {
            deliveryDate = (OEDeliveryDate) deliveryDates.get(i);

            if(deliveryDate.getDeliverableFlag().equals("Y") && deliveryDate.isInDateRange())
            {
                if(firstRangeDate)
                {
                    rangeDate = deliveryDate;
                    firstRangeDate = false;
                }
                inRange = true;                
                dateRangeDisplayBuffer.append(deliveryDate.getDayOfWeek());
                dateRangeDisplayBuffer.append(" ");
                dateRangeDisplayBuffer.append(deliveryDate.getDisplayDate());
                dateRangeDisplayBuffer.append(" or ");       
            }
            else if(!deliveryDate.isInDateRange() && inRange)
            {
                inRange = false;
                deliveryDate = rangeDate;
                dateRangeDisplayBuffer.delete((dateRangeDisplayBuffer.length()-4), dateRangeDisplayBuffer.length());
                deliveryDate.setDisplayDate(dateRangeDisplayBuffer.toString());
            }

            if(!inRange && deliveryDate.getDeliverableFlag().equals("Y"))
            {
                break;
            }
        }

        return deliveryDate;
    }

    /**
     * 
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return ArrayList list of OEDeliveryDate objects
     */
    public ArrayList getOrderProductDeliveryDates(OEDeliveryDateParm parms) throws FTDApplicationException
    {        
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderPO order = parms.getOrder();

        // Domestic order, set time zone of calendar
        if ( order.getSendToCustomer().isDomesticFlag() )
        {
            timeZone = getTimeZone(parms); 
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        // set the number of days to process plus one
        int maxDays = getDeliveryDaysOut(order, parms.getGlobalParms()) + 1;
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays);
        
        return deliveryDates;      
    }

    /**
     * 
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return XML string representing delivery date values
     */
    public XMLDocument getOrderProductDeliveryDatesXML(OEDeliveryDateParm parms) throws FTDApplicationException
    {                
        ArrayList deliveryDates = getOrderProductDeliveryDates(parms);
        
        return getXMLEncodedDeliveryDates(deliveryDates, parms);
    }

    /**
     * 
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return ArrayList list of OEDeliveryDate objects
     */
    public XMLDocument getCalendarProductDeliveryDates(OEDeliveryDateParm parms) throws FTDApplicationException
    {
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderPO order = parms.getOrder();

        // Domestic order, set time zone of calendar
        if ( order.getSendToCustomer().isDomesticFlag() )
        {
            timeZone = getTimeZone(parms); 
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        // convert passed month and year to valid calendar date
        int calMonth = calendar.get(Calendar.MONTH);
        if ( (parms.getCalendarMonth() != null) && 
             (!parms.getCalendarMonth().trim().equals("")) )
        {
            calMonth = getCalendarMonth(parms.getCalendarMonth());
        }
        
        int calYear = calendar.get(Calendar.YEAR);
        if ( (parms.getCalendarYear() != null)  && 
             (!parms.getCalendarYear().trim().equals("")) )
        {
            calYear = Integer.parseInt(parms.getCalendarYear());
        }

        calendar.set(calYear, calMonth, 1);
        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays);  

        return getXMLEncodedCalendarDates(timeZone, deliveryDates, parms.getProductId());
//        return deliveryDates;
     }

    /**
     * 
     * @param boolean flag to stop processing once first available delivery date is identified
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @param Calendar calendar object that is used to derive delivery dates
     * @param int number of days to retrieve
     * @return ArrayList list of OEDeliveryDate objects
     */

    private ArrayList getProductDeliveryDates(boolean firstAvailable, 
                                                Calendar calendar,
                                                    OEDeliveryDateParm delParms,
                                                            int maxDays) throws FTDApplicationException
    {
    
        Date timerDate = new Date();
        ArrayList dateList = new ArrayList();
        OEDeliveryDate deliveryVO = null;
        Calendar currentDay = null;
        String currentDateStr = null;
        Date currentDate = null;
        int currentDayOfWeek;
        int month;
        long dateDiff;
        int rangeKey = 0;
        boolean isDeliverable;
        boolean firstAvailDate = true;
        ArrayList shippingMethods = null;
        StringBuffer dateRangeStringBuffer = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        String productType = delParms.getProductType();
        String productSubType = delParms.getProductSubType();
        Calendar today = Calendar.getInstance(calendar.getTimeZone());

        //TDP: 01/18/2004
        //We need the real cutoff times that are in the database in order to
        //calculate valid delivery dates, so, if the object only has the
        //default data in it, then hit the database
        if( delParms!=null )
        {
            OEParameters oeparms = delParms.getGlobalParms();
            if( oeparms==null || oeparms.isDefaulted() ) 
            {
                SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
                delParms.setGlobalParms(searchUTIL.getGlobalParms());
            }
        }
        
        // Find delivery methods for each date
        for ( int i=0; i < maxDays; i++ )
        {
            isDeliverable = true;
            currentDay = (Calendar) calendar.clone();
            currentDay.add(Calendar.DATE, i);

            month = currentDay.get(Calendar.MONTH) + 1;
            
            // do not use the getTime() method of the calendar object as it returns the date & time
            // converted to the timezone of the machine that it is running on. For example,
            // input E.S.T timezone zip code, getTime() will return in C.S.T if that is where server is.
            currentDateStr = zeroPad(month) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);
            currentDayOfWeek = currentDay.get(Calendar.DAY_OF_WEEK);
            
            // Set today's date
            deliveryVO = new OEDeliveryDate();
            deliveryVO.setDeliveryDate(currentDateStr);
            deliveryVO.setDayOfWeek(getDayOfWeek(currentDayOfWeek));

            // check florist delivery for current date if the product is florist
            // delivered or if we have not product selected
            if(delParms.isShipMethodFlorist() || (delParms.getProductId() != null && delParms.getProductId().length() == 0))
            {
                deliveryVO = floristDeliverableDate(deliveryVO, delParms, calendar, currentDay);
            }

            // Only check common carrier if not type floral
            if(!productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL))
            {
                // check common carrier delivery for current date
                deliveryVO = carrierDeliverableDate(deliveryVO, delParms, calendar, currentDay);
            }

            // If no shipping methods then the date is not deliverable
            if(deliveryVO.getShippingMethods().size() < 1)
            {
                isDeliverable = false;
            }
            
            // check to see if date is a Holiday date
            if (isDeliverable && delParms.getHolidayDates() != null )
            {
                OEDeliveryDate holidayDate = (OEDeliveryDate) delParms.getHolidayDates().get(deliveryVO.getDeliveryDate());

                if ( holidayDate != null )
                {
                    deliveryVO.setHolidayText(holidayDate.getHolidayText());
                    if ( holidayDate.getDeliverableFlag().equals("N") )
                    {
                        isDeliverable = false;
                    }
                }
            }

            // Exception range is for unavailable dates and current date is in range
            if ( (isDeliverable) && 
                 (delParms.getExceptionCode() != null) && 
                 (delParms.getExceptionCode().equals("U")) )
            {
                // skip date if is in exception range (includes both ends of range)
                if ( (getDateDiff(delParms.getExceptionFrom(), currentDay.getTime()) > -1)  && 
                     (getDateDiff(delParms.getExceptionTo(), currentDay.getTime()) < 1) )
                {
                    isDeliverable = false;
                }
            }

            // Exception range is for available dates and current date is in range
            if ( (isDeliverable) && 
                 (delParms.getExceptionCode() != null) && 
                 (delParms.getExceptionCode().equals("A")) )
            {
                // skip date if not in exception range (includes both ends of range)
                if ( (getDateDiff(delParms.getExceptionFrom(), currentDay.getTime()) < 0)  || 
                     (getDateDiff(delParms.getExceptionTo(), currentDay.getTime()) > 0) )
                {
                    isDeliverable = false;
                }
            }

            // get the number of days between the today and process date to
            // use for comparisons.
            dateDiff = getDateDiff(today.getTime(), currentDay.getTime());

            // all dates before current date and after last allowed date out are not deliverable
            if ( (dateDiff < 0) || (dateDiff > (maxDays - 1)))//delParms.getGlobalParms().getDeliveryDaysOutMAX()) )
            {
                isDeliverable = false;
            }

            boolean inRange = false;
            // Check for date ranging
            if(//isDeliverable && 
               (delParms.isShipMethodFlorist() || (delParms.getProductId() != null && delParms.getProductId().length() == 0))  && 
               delParms.isDeliveryRangeFlag().booleanValue())
            {
                // Check to see if currentDay is in date range
                HashMap dateRangeFromMap = delParms.getDeliveryRangeFrom();
                HashMap dateRangeToMap = delParms.getDeliveryRangeTo();                
                int y = 0;
                Integer integer = null;
                
                for (y = 0; y < dateRangeFromMap.size(); y++ )
                {
                    Date dateRangeFrom = null;
                    Date dateRangeTo = null;
                    Date myCurrentDate = null;
                    try
                    {
                        dateRangeFrom = sdf.parse((String)dateRangeFromMap.get(String.valueOf(y)));
                        dateRangeTo = sdf.parse((String)dateRangeToMap.get(String.valueOf(y)));
                        myCurrentDate = sdf.parse(currentDateStr);
                    }
                    catch(Exception e)
                    {
                        lm.error(e.toString(), e);
                        continue;
                    }

                    if ( (getDateDiff(dateRangeFrom, myCurrentDate) > -1) &&
                         (getDateDiff(dateRangeTo, myCurrentDate) < 1) )
                    {
                        inRange = true;
                        break;
                    }
                }
                
                if(inRange)
                {
/*                
                    // Append to date range
                    dateRangeStringBuffer.append(this.getDayOfWeek(currentDayOfWeek));
                    dateRangeStringBuffer.append(" ");
                    dateRangeStringBuffer.append(currentDateStr);
                    dateRangeStringBuffer.append(" or ");
                    rangeKey = y;
*/
                    deliveryVO.setInDateRange(true);
                    deliveryVO.setHolidayText((String)delParms.getDeliveryRangeText().get(String.valueOf(y)));
//                    continue;
                }
                else
                {
                    inRange = false;
                }
/*
                else if(dateRangeStringBuffer.length() > 0)
                {
                    // Output date range and current date
                    dateRangeStringBuffer.delete((dateRangeStringBuffer.length()-4), dateRangeStringBuffer.length());
                    OEDeliveryDate deliveryRangeVO = new OEDeliveryDate();
                    deliveryRangeVO.setHolidayText((String)delParms.getDeliveryRangeText().get(String.valueOf(rangeKey)));
                    deliveryRangeVO.setDeliverableFlag("Y");
                    deliveryRangeVO.setDeliveryDate(dateRangeStringBuffer.substring(4, 14));
                    deliveryRangeVO.setDisplayDate(dateRangeStringBuffer.toString());
                    ShippingMethod shippingMethod = new ShippingMethod();
                    shippingMethod.setDescription(GeneralConstants.DELIVERY_FLORIST);
                    shippingMethod.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
                    shippingMethod.setDeliveryCharge(delParms.getFloralServiceCharge());
                    deliveryRangeVO.getShippingMethods().add(shippingMethod);
                    dateList.add(deliveryRangeVO);
                    dateRangeStringBuffer.setLength(0);
                    rangeKey = 0;

                    if(firstAvailable)
                    {
                        break;
                    }
                }
                else
                {
                    // output current date only
                }
*/                
            }
/*
            // Catch the case when a product is not deliverable the day after a date range 
            else if( !isDeliverable && dateRangeStringBuffer.length() > 0 &&  delParms.isShipMethodFlorist() && delParms.isDeliveryRangeFlag().booleanValue())
            {
                // Output date range and current date
                dateRangeStringBuffer.delete((dateRangeStringBuffer.length()-4), dateRangeStringBuffer.length());
                OEDeliveryDate deliveryRangeVO = new OEDeliveryDate();
                deliveryRangeVO.setHolidayText((String)delParms.getDeliveryRangeText().get(String.valueOf(rangeKey)));
                deliveryRangeVO.setDeliverableFlag("Y");
                deliveryRangeVO.setDeliveryDate(dateRangeStringBuffer.substring(4, 14));
                deliveryRangeVO.setDisplayDate(dateRangeStringBuffer.toString());
                ShippingMethod shippingMethod = new ShippingMethod();
                shippingMethod.setDescription(GeneralConstants.DELIVERY_FLORIST);
                shippingMethod.setDeliveryCharge(delParms.getFloralServiceCharge());
                deliveryRangeVO.getShippingMethods().add(shippingMethod);
                dateList.add(deliveryRangeVO);
                dateRangeStringBuffer.setLength(0);
                rangeKey = 0;
            }
*/
            deliveryVO.setDisplayDate(deliveryVO.getDeliveryDate());

            if(isDeliverable)
            {
                //is the delivery date a delivery exclusion day
                HashSet eDOW = delParms.getExcludedDeliveryDays();
                if( eDOW!=null ) {
                    String sDOW = deliveryVO.getDayOfWeek();
                    if( sDOW.equals("Sun") && eDOW.contains(new Integer(Calendar.SUNDAY)) )
                    {
                        isDeliverable=false;    
                    }
                    else if( sDOW.equals("Mon") && eDOW.contains(new Integer(Calendar.MONDAY)) )
                    {
                        isDeliverable=false; 
                    }
                    else if( sDOW.equals("Tue") && eDOW.contains(new Integer(Calendar.TUESDAY)) )
                    {
                        isDeliverable=false; 
                    }
                    else if( sDOW.equals("Wed") && eDOW.contains(new Integer(Calendar.WEDNESDAY)) )
                    {
                        isDeliverable=false; 
                    }
                    else if( sDOW.equals("Thu") && eDOW.contains(new Integer(Calendar.THURSDAY)) )
                    {
                        isDeliverable=false; 
                    }
                    else if( sDOW.equals("Fri") && eDOW.contains(new Integer(Calendar.FRIDAY)) )
                    {
                        isDeliverable=false; 
                    }
                    else if( sDOW.equals("Sat") && eDOW.contains(new Integer(Calendar.SATURDAY)) )
                    {
                        isDeliverable=false; 
                    }
                }
            }

            if(isDeliverable)
            {
                deliveryVO.setDeliverableFlag("Y");
                if(firstAvailDate)
                {
                    // set the first deliverable date in the order for later processing
                    delParms.getOrder().setFirstAvailableDate(deliveryVO.getDeliveryDate());
                    firstAvailDate = false;
                }
            }
            else
            {
                deliveryVO.setDeliverableFlag("N");
            }
            
            dateList.add(deliveryVO);

            // If only need first available date, break loop
            if (firstAvailable && isDeliverable && !inRange)
            {
                break;
            }
        }

        lm.info("Delivery date calculation took " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");
        return dateList;
    }

/*     
    private ArrayList getProductDeliveryDates(boolean firstAvailable, 
                                                Calendar calendar,
                                                    OEDeliveryDateParm delParms,
                                                            int maxDays) 
    {    
        Date timerDate = new Date();
        ArrayList dateList = new ArrayList();
        OEParameters oeParms = delParms.getGlobalParms();
        OEOrder order = delParms.getOrder();
        HashMap holidayDates = null;
        HashMap shipMethods = new HashMap(); // In case the ship methods are null
        
        Calendar today = Calendar.getInstance(calendar.getTimeZone());
        Calendar intlGNADDDay = (Calendar) today.clone();

        boolean domesticOrder = order.isOrderDomesticFlag();
        boolean nextDayAvailable = false;
        boolean firstSaturdayAvailable = true;
        boolean floralProduct = false;
        boolean saturdayDelivery = false;
        boolean sundayDelivery = false;
        boolean skipFirstAvailable = false;
        boolean gnaddActive = false;

        // Flag indicating that the product is codified and should show
        // when GNADD is turned on.
        String codifiedSpecialFlag = delParms.getCodifiedSpecialFlag();
        if(codifiedSpecialFlag == null) codifiedSpecialFlag = "N";
        
        long dateDiff = 0;      // date difference value
        long nextDay = 0;       // date difference value for next-day shipping
        long twoDay = 0;        // date difference value for two-day shipping
        BigDecimal noCharge = new BigDecimal("0");

        String productType = delParms.getProductType();
        String productSubType = delParms.getProductSubType();
        String orderState = order.getOrderDeliveryState();
        
        // skip first available date if drop ship product and state is Alaska(AK) or Hawaii(HI)
        // as they only support 2-day delivery
        if ( (productType != null) && (orderState != null) )
        {
             if ( (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) &&
                  (orderState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
                   orderState.equals(GeneralConstants.STATE_CODE_HAWAII)) )
            {
                skipFirstAvailable = true;
            }
        }
        
        // set product type flag for later processing
        if ( (productType != null) &&
             (productSubType != null) &&
             (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL)) && 
             (!productSubType.equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC)) )
        {
            floralProduct = true;

            // check to see if domestic floral GNADD is in effect
            if ( domesticOrder )
            {
                gnaddActive = getGnaddSetting(delParms);
            }
            // international floral orders cannot be delivered on Sunday
            else
            {
                sundayDelivery = false;
            }
        }

        // set Saturday delivery flag
        if ( (delParms.isSaturdayDelivery() != null) &&
             (delParms.isSaturdayDelivery().booleanValue()) )
        {
            saturdayDelivery = true;
        }
                     
        // set Sunday delivery flag for Domestic orders
        if ( (delParms.isSundayDelivery() != null) &&
             (delParms.isSundayDelivery().booleanValue()) &&
              domesticOrder )
        {
            sundayDelivery = true;
        }
                     
        // retrieve the holiday dates for the selected country
        if ( delParms.getHolidayDates() != null ) 
        {
            holidayDates = delParms.getHolidayDates();
        }

        // retrieve the valid ship methods for the product
        if ( delParms.getShipMethods() != null ) 
        {
            shipMethods = delParms.getShipMethods();
        }

        // set GNADD date for country if not domestic order
        if ( !domesticOrder ) 
        {
            // use whichever is farther out
            if ( oeParms.getIntlAddOnDays() > delParms.getCntryAddOnDays() )
            {
                intlGNADDDay.add(Calendar.DATE, oeParms.getIntlAddOnDays());
            }
            else
            {
                intlGNADDDay.add(Calendar.DATE, delParms.getCntryAddOnDays());
            }
        }
        
        StringBuffer dateRangeStringBuffer = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        int rangeKey = 0;
        for ( int i=0; i < maxDays; i++ )
        {
            SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");
            boolean isDeliverable = true;
            boolean shipBlocked = false;
            int currentDayOfWeek = 0;

            Calendar currentDay = (Calendar) calendar.clone();
            currentDay.add(Calendar.DATE, i);
            
            int month = currentDay.get(Calendar.MONTH);
            int dayOfWeek = currentDay.get(Calendar.DAY_OF_WEEK);
            month++;

            // do not use the getTime() method of the calendar object as it returns the date & time
            // converted to the timezone of the machine that it is running on. For example,
            // input E.S.T timezone zip code, getTime() will return in C.S.T if that is where server is.
            String currentDate = zeroPad(month) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);
            currentDayOfWeek = currentDay.get(Calendar.DAY_OF_WEEK);
            
            // set the default delivery date values
            OEDeliveryDate deliveryVO = new OEDeliveryDate();
            deliveryVO.setHolidayText("");
            deliveryVO.setDeliverableFlag("Y");
            deliveryVO.setDeliveryDate(currentDate);

            deliveryVO.setDayOfWeek(getDayOfWeek(dayOfWeek));
            deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_STANDARD);

            order.setDeliverToday(Boolean.TRUE);
            
            // check to see if GNADD is active for the passed zip code
            long gnaddDiff = getDateDiff(currentDay.getTime(), oeParms.getGNADDDate());
            if ( gnaddActive &&
                 gnaddDiff > 0 )
            {
                    isDeliverable = false;
            }

            // Check for International GNADD date
            if ( isDeliverable && 
                 !domesticOrder &&
                 currentDay.before(intlGNADDDay) )
            {
                isDeliverable = false;
            }
            
            // check if vendor has blocked product from delivery on current day
            if ( isDeliverable && 
                 (delParms.isVendorNoDeliverFlag() != null) && 
                 (delParms.isVendorNoDeliverFlag().booleanValue()) )
            {
                HashMap noDeliverFromMap = delParms.getVendorNoDeliverFrom();
                HashMap noDeliverToMap = delParms.getVendorNoDeliverTo();
                
                for ( int y = 0; y < noDeliverFromMap.size(); y++ )
                {
                    Date noDeliverFrom = (Date) noDeliverFromMap.get(String.valueOf(y));
                    Date noDeliverTo = (Date) noDeliverToMap.get(String.valueOf(y));
                    
                    if ( (getDateDiff(noDeliverFrom, currentDay.getTime()) > -1) &&
                         (getDateDiff(noDeliverTo, currentDay.getTime()) < 1) )
                    {
                        isDeliverable = false;
                    }
                }
            }
            
            // check to see if date is a Holiday date and date is deliverable
            if ( isDeliverable && holidayDates != null )
            {
                OEDeliveryDate holidayDate = (OEDeliveryDate) holidayDates.get(deliveryVO.getDeliveryDate());

                if ( holidayDate != null )
                {
                    deliveryVO.setHolidayText(holidayDate.getHolidayText());
                    if ( holidayDate.getDeliverableFlag().equals("N") )
                    {
                        isDeliverable = false;
                    }
                }
            }

            // skip if date is Sunday and Sunday delivery is not allowed
            if ( isDeliverable && 
                 (currentDayOfWeek == Calendar.SUNDAY) &&
                 (!sundayDelivery) &&
                 // JMP 04/28/03
                 // Added as a quick fix to allow Sunday delivery
                 // for Mother's Day 2003 for all florists even if the zip
                 // code does not normally allow Sunday delivery
                 !currentDate.equals("05/11/2003"))
            {
                isDeliverable = false;
            }
            
            // skip if date is Saturday and Saturday delivery is not allowed
            if ( isDeliverable && 
                 (currentDayOfWeek == Calendar.SATURDAY) &&
                 (!saturdayDelivery) )
            {
                isDeliverable = false;
            }
            
            // Exception range is for unavailable dates and current date is in range
            if ( (isDeliverable) && 
                 (delParms.getExceptionCode() != null) && 
                 (delParms.getExceptionCode().equals("U")) )
            {
                // skip date if is in exception range (includes both ends of range)
                if ( (getDateDiff(delParms.getExceptionFrom(), currentDay.getTime()) > -1)  && 
                     (getDateDiff(delParms.getExceptionTo(), currentDay.getTime()) < 1) )
                {
                    isDeliverable = false;
                }
            }

            // Exception range is for available dates and current date is in range
            if ( (isDeliverable) && 
                 (delParms.getExceptionCode() != null) && 
                 (delParms.getExceptionCode().equals("A")) )
            {
                // skip date if not in exception range (includes both ends of range)
                if ( (getDateDiff(delParms.getExceptionFrom(), currentDay.getTime()) < 0)  || 
                     (getDateDiff(delParms.getExceptionTo(), currentDay.getTime()) > 0) )
                {
                    isDeliverable = false;
                }
            }

            // check if vendor has blocked product from shipping on current day
            if ( (delParms.isVendorNoShipFlag() != null) && 
                 (delParms.isVendorNoShipFlag().booleanValue()) )
            {
                HashMap noShipFromMap = delParms.getVendorNoShipFrom();
                HashMap noShipToMap = delParms.getVendorNoShipTo();
                
                for ( int y = 0; y < noShipFromMap.size(); y++ )
                {
                    Date noShipFrom = (Date) noShipFromMap.get(String.valueOf(y));
                    Date noShipTo = (Date) noShipToMap.get(String.valueOf(y));
                    
                    if ( (getDateDiff(noShipFrom, currentDay.getTime()) > -1) &&
                         (getDateDiff(noShipTo, currentDay.getTime()) < 1) )
                    {
                        shipBlocked = true;
                    }
                }
            }
            
            // check if product cannot be shipped on a current day
            if ( delParms.getNoShipDays() != null )
            {
                // skip if is in list of non-ship days
                if ( delParms.getNoShipDays().contains(new Integer(currentDayOfWeek)) )
                {
                    shipBlocked = true;
                }
            }
            
            // get the number of days between the today and process date to
            // use for comparisons.
            dateDiff = getDateDiff(today.getTime(), currentDay.getTime());

            // all dates before current date and after last allowed date out are not deliverable
            if ( (dateDiff < 0) || (dateDiff > oeParms.getDeliveryDaysOut()) )
            {
                isDeliverable = false;
            }
            // current date
            else if ( dateDiff == 0 )
            {
                // format the current and cutoff times for comparison
                int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));
                int cutoffTime = getCutoffTime(domesticOrder, delParms, oeParms, currentDay);

                if ( (currentTime < cutoffTime) )
                {                    
                    // Make sure product supports same day delivery (floral only)
                    if ( isDeliverable && delParms.isDeliverToday().booleanValue() )
                    {
                        nextDayAvailable = true;
                    }
                    else 
                    {
                        isDeliverable = false;
                        nextDayAvailable = true;
                        order.setDeliverToday(Boolean.FALSE);
                    }

                    // first Saturday is unavailable if order date is Friday 
                    // and shipping is blocked for Friday
                    if ( currentDayOfWeek == Calendar.FRIDAY &&
                         shipBlocked )
                    {
                        firstSaturdayAvailable = false;
                        nextDayAvailable = false;
                    }
                }
                // Funeral Occasion - override same day delivery to available
                else if ( (floralProduct) && 
                          (domesticOrder) &&
                          (order.getOrderOccasion() != null) && 
                          (order.getOrderOccasion().equals(GeneralConstants.OE_OCCASION_FUNERAL)) )
                {
                    isDeliverable = true;
                    nextDayAvailable = true;
                }
                else
                {
                    // domestic floral orders after cutoff are delivered next day
                    if ( floralProduct && domesticOrder )
                    {
                        nextDayAvailable = true;
                    }
                    // international floral order after cutoff are delivered next day + 1
                    else
                    {
                        nextDayAvailable = false;
                    }

                    isDeliverable = false;
                    order.setDeliverToday(Boolean.FALSE);

                    // first Saturday is unavailable if order date is Friday
                    if ( (!floralProduct) && (currentDayOfWeek == Calendar.FRIDAY) )
                    {
                        firstSaturdayAvailable = false;
                    }
                }

                if ( floralProduct )
                {
                    nextDay = (nextDayAvailable == true ? 1 : 2);
                    twoDay = (nextDayAvailable == true ? 2 : 3);
                }
                else 
                {
                    deliveryVO.setDeliveryCharge(delParms.getStandardDeliveryCharge());

                    switch ( currentDayOfWeek )
                    {
                        case Calendar.WEDNESDAY:
                            nextDay = (nextDayAvailable == true ? 1 : 2);
                            twoDay = (nextDayAvailable == true ? 2 : 5);
                            break;
                        case Calendar.THURSDAY:
                            nextDay = (nextDayAvailable == true ? 1 : 4);
                            twoDay = (nextDayAvailable == true ? 4 : 5);
                            break;
                        case Calendar.FRIDAY:
                            nextDay = (nextDayAvailable == true ? 3 : 4);
                            twoDay = (nextDayAvailable == true ? 4 : 5);
                            break;
                        case Calendar.SATURDAY:
                            nextDay = 3;
                            twoDay = 4;
                            break;
                        case Calendar.SUNDAY:
                            nextDay = 2;
                            twoDay = 3;
                            break;
                        default:
                            nextDay = (nextDayAvailable == true ? 1 : 2);
                            twoDay = (nextDayAvailable == true ? 2 : 3);
                            break;
                    }

                    // current day is not valid for shipping and next day
                    // is valid for delivery, move next day and 2nd day
                    // back 1 day
                    if ( shipBlocked && 
                         nextDayAvailable &&
                         (currentDayOfWeek != Calendar.SATURDAY) &&
                         (currentDayOfWeek != Calendar.SUNDAY) )
                    {
                        nextDay++;
                        twoDay++;
                    }
                }
            }
            else if ( (dateDiff < nextDay) )
            {
                if ( isDeliverable &&
                     currentDayOfWeek == Calendar.SATURDAY &&
                     firstSaturdayAvailable  &&
                     !floralProduct )
                {
                    deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_SATURDAY);
                }
                else
                {
                    // current day is not a ship day, so push out the next and 
                    // 2nd day delivery
                    if ( (shipBlocked) &&
                         (currentDayOfWeek != Calendar.SATURDAY) &&
                         (currentDayOfWeek != Calendar.SUNDAY) )
                    {
                        nextDay++;
                        twoDay++;

                        // cancel first Saturday if Friday is not a ship day
                        if ( currentDayOfWeek == Calendar.FRIDAY )
                        {
                            firstSaturdayAvailable = false;
                        }
                    }

                    // current date is not deliverable as is before "Next Day" date
                    if(!floralProduct)
                        isDeliverable = false;
                }
            }
            else if ( dateDiff == nextDay )
            {
                // day not deliverable, move next day to next available day
                if ( !isDeliverable )
                {
                    switch ( currentDayOfWeek )
                    {
                        case Calendar.THURSDAY:
                            nextDay++;      // Friday
                            twoDay += 3;    // Monday
                            break;
                        case Calendar.FRIDAY:
                            nextDay += 3;   // Monday
                            twoDay++;       // Tuesday
                            break;
                        default:
                            nextDay++;      // move 1 day
                            twoDay++;       // move 1 day
                            break;
                    }
                }
                // Check for Sunday delivery
                else if ( currentDayOfWeek == Calendar.SUNDAY &&
                          !sundayDelivery )
                {
                    nextDay++;
                    twoDay++;
                }
                // date is deliverable
                else
                {
                    // Next Day delivery is not available to Alaska & Hawaii
                    if ( (productType != null) && (orderState != null) &&
                         (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                          productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)) &&
                         (orderState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
                          orderState.equals(GeneralConstants.STATE_CODE_HAWAII)) )
                    {
                        isDeliverable = false;
                    }
                    else if ( !floralProduct )
                    {
                        deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_NEXT_DAY);
                    }
                }
            }
            else if ( dateDiff == twoDay )
            {
                if ( isDeliverable )
                {
                    if ( !floralProduct )
                    {
                        deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_TWO_DAY);

                        // first available drop ship date for Alaska & Hawaii
                        skipFirstAvailable = false;
                    }
                }
                // date unavailable, move 2nd day to next available day
                else
                {
                    if ( !floralProduct )
                    {
                        switch ( currentDayOfWeek )
                        {
                            case Calendar.FRIDAY:
                                twoDay+=3;       // Monday
                                break;
                            default:
                                twoDay++;       // move 1 day
                                break;
                        }
                    }
                }
            }
            else if ( (currentDayOfWeek == Calendar.SATURDAY) &&
                     (saturdayDelivery) )
            {
                if ( isDeliverable && !floralProduct )
                {
                    deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_SATURDAY);
                }
            }

//            else if ( (currentDayOfWeek == Calendar.SUNDAY) &&
//                      (sundayDelivery) &&
//                      (isDeliverable) &&
//                      (!floralProduct) )
//            {
//                deliveryVO.setDeliveryCharge(delParms.getStandardDeliveryCharge());
//            }
//            else
//            {
//                if ( isDeliverable && !floralProduct )
//                {
//                    deliveryVO.setDeliveryCharge(delParms.getStandardDeliveryCharge());
//                }
//            }

            // date is not available for delivery
            if ( !isDeliverable )
            {
                deliveryVO.setDeliverableFlag("N");
                deliveryVO.setDeliveryCharge(new BigDecimal("0"));
                deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_STANDARD);
            }
            // date is available for delivery
            else
            {
                // floral product, use floral service charge
                if ( floralProduct )
                {
                    deliveryVO.setDeliveryCharge(delParms.getFloralServiceCharge());
                }
                else if ( delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) )
                {
                    deliveryVO.setShippingMethod(GeneralConstants.DELIVERY_NEXT_DAY);
                    deliveryVO.setDeliveryCharge(delParms.getFloralServiceCharge());
                }
                // drop ship product
                else
                {
                    // JMP 01/14/03 
                    // Temp debug code to catch a troublesome product
                    if(delParms.getProductId() != null)
                    {
                        this.lm.debug("The product ID is: " + delParms.getProductId());
                    }
                    // get shipping charge for shipping method
                    BigDecimal shipCharge =  (BigDecimal) shipMethods.get(deliveryVO.getShippingMethod());
                    String shipMethod = deliveryVO.getShippingMethod();
                    
                    // shipping method not valid for product
                    if ( shipCharge == null )
                    {
                        // Standard delivery not valid, see if Two Day delivery valid
                        if ( shipMethod.equals(GeneralConstants.DELIVERY_STANDARD) &&
                             shipMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY) )
                        {
                            shipMethod = GeneralConstants.DELIVERY_TWO_DAY;
                            shipCharge = (BigDecimal) shipMethods.get(GeneralConstants.DELIVERY_TWO_DAY);
                        }
                        // Standard delivery not valid, see if Next Day delivery valid
                        else if ( shipMethod.equals(GeneralConstants.DELIVERY_STANDARD) &&
                             shipMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY) )
                        {
                            shipMethod = GeneralConstants.DELIVERY_NEXT_DAY;
                            shipCharge = (BigDecimal) shipMethods.get(GeneralConstants.DELIVERY_NEXT_DAY);
                        }
                        // Two Day delivery not valid, see if Next Day delivery valid
                        else if ( shipMethod.equals(GeneralConstants.DELIVERY_TWO_DAY) &&
                             shipMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY) )
                        {
                            shipMethod = GeneralConstants.DELIVERY_NEXT_DAY;
                            shipCharge = (BigDecimal) shipMethods.get(GeneralConstants.DELIVERY_NEXT_DAY);
                        }
                    }

                    // shipping method changed, set new value
                    if ( !shipMethod.equals(deliveryVO.getShippingMethod()) )
                    {
                        deliveryVO.setShippingMethod(shipMethod);
                    }
                    deliveryVO.setDeliveryCharge(shipCharge);
                }
            }
            
            // Check for date ranging
            if(isDeliverable && floralProduct && delParms.isDeliveryRangeFlag().booleanValue())
            {
                // Check to see if currentDay is in date range
                HashMap dateRangeFromMap = delParms.getDeliveryRangeFrom();
                HashMap dateRangeToMap = delParms.getDeliveryRangeTo();
                boolean inRange = false;
                int y = 0;
                Integer integer = null;
                
                for (y = 0; y < dateRangeFromMap.size(); y++ )
                {
                    Date dateRangeFrom = null;
                    Date dateRangeTo = null;
                    Date myCurrentDate = null;
                    try
                    {
                        dateRangeFrom = sdf.parse((String)dateRangeFromMap.get(String.valueOf(y)));
                        dateRangeTo = sdf.parse((String)dateRangeToMap.get(String.valueOf(y)));
                        myCurrentDate = sdf.parse(currentDate);
                    }
                    catch(Exception e)
                    {
                        lm.error(e.toString(), e);
                        continue;
                    }

                    if ( (getDateDiff(dateRangeFrom, myCurrentDate) > -1) &&
                         (getDateDiff(dateRangeTo, myCurrentDate) < 1) )
                    {
                        inRange = true;
                        break;
                    }
                }
                
                if(inRange)
                {
                    // Append to date range
                    dateRangeStringBuffer.append(this.getDayOfWeek(dayOfWeek));
                    dateRangeStringBuffer.append(" ");
                    dateRangeStringBuffer.append(currentDate);
                    dateRangeStringBuffer.append(" or ");
                    rangeKey = y;
                    continue;
                }
                else if(dateRangeStringBuffer.length() > 0)
                {
                    // Output date range and current date
                    dateRangeStringBuffer.delete((dateRangeStringBuffer.length()-4), dateRangeStringBuffer.length());
                    OEDeliveryDate deliveryRangeVO = new OEDeliveryDate();
                    deliveryRangeVO.setHolidayText((String)delParms.getDeliveryRangeText().get(String.valueOf(rangeKey)));
                    deliveryRangeVO.setDeliverableFlag("Y");
                    deliveryRangeVO.setDeliveryDate(dateRangeStringBuffer.substring(4, 14));
                    deliveryRangeVO.setDisplayDate(dateRangeStringBuffer.toString());
                    deliveryRangeVO.setShippingMethod(GeneralConstants.DELIVERY_STANDARD);
                    deliveryRangeVO.setDeliveryCharge(delParms.getFloralServiceCharge());
                    dateList.add(deliveryRangeVO);
                    dateRangeStringBuffer.setLength(0);
                    rangeKey = 0;

                    if(firstAvailable && !skipFirstAvailable)
                    {
                        break;
                    }
                }
                else
                {
                    // output current date only
                }                
            }
            // Catch the case when a product is not deliverable the day after a date range 
            else if( !isDeliverable && dateRangeStringBuffer.length() > 0 &&  floralProduct && delParms.isDeliveryRangeFlag().booleanValue())
            {
                // Output date range and current date
                dateRangeStringBuffer.delete((dateRangeStringBuffer.length()-4), dateRangeStringBuffer.length());
                OEDeliveryDate deliveryRangeVO = new OEDeliveryDate();
                deliveryRangeVO.setHolidayText((String)delParms.getDeliveryRangeText().get(String.valueOf(rangeKey)));
                deliveryRangeVO.setDeliverableFlag("Y");
                deliveryRangeVO.setDeliveryDate(dateRangeStringBuffer.substring(4, 14));
                deliveryRangeVO.setDisplayDate(dateRangeStringBuffer.toString());
                deliveryRangeVO.setShippingMethod(GeneralConstants.DELIVERY_STANDARD);
                deliveryRangeVO.setDeliveryCharge(delParms.getFloralServiceCharge());
                dateList.add(deliveryRangeVO);
                dateRangeStringBuffer.setLength(0);
                rangeKey = 0;
            }

            deliveryVO.setDisplayDate(deliveryVO.getDeliveryDate());
            
            dateList.add(deliveryVO);

            // If only need first available date, break loop
            if ( (isDeliverable) && 
                 (firstAvailable) &&
                 (!skipFirstAvailable) )
            {
                break;
            }
        }

        // Added JMP 01/12/03
        // If no dates are available then set the product unavailable flag
        boolean deliverable = false;
        for(int i = 0; i < dateList.size(); i++)
        {
            if(((OEDeliveryDate)dateList.get(i)).getDeliverableFlag().equalsIgnoreCase("Y"))
            {
                deliverable = true;
                break;
            }
        }
        if(!deliverable)
        {
            order.setDisplayProductUnavailable(GeneralConstants.YES);
        }

        lm.info("Delivery date calculation took " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");   
        return dateList;
    }
  */ 
    
    /**
     * 
     * @param ArrayList list of OEDeliveryDate objects
     * @return String delivery date information formatted in an XML string
     */
    public XMLDocument getXMLEncodedDeliveryDates(List dates, OEDeliveryDateParm delParms)
    {    
        XMLDocument document = new XMLDocument();
        Element root = (Element) document.createElement("shippingData");
        document.appendChild(root);
        
        Element deliveryDates = (Element) document.createElement("deliveryDates");
        root.appendChild(deliveryDates);
        
        Element entry = null;
        Element entryRange = null;
        OEDeliveryDate currentDate = null;
        OEDeliveryDate rangeDate = null;
        HashMap shippingMethods = new HashMap();
        StringBuffer dateRangeDisplayBuffer = new StringBuffer();
        boolean inRange = false;
        boolean firstRange = true;
        Connection connection = null;
        BigDecimal fuelSurchargeAmt = null;
        
        try {
            connection = DataManager.getInstance().getConnection();
            fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));
        } catch (Exception e) { 
            // Just ignore
        } finally {
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        }
        
        for(int i = 0; i < dates.size(); i++)
        {
            currentDate = (OEDeliveryDate) dates.get(i);
            if ( currentDate.getDeliverableFlag().equals("Y") )
            {
                entry = (Element) (document.createElement("deliveryDate"));
                entry.setAttribute("date", currentDate.getDeliveryDate());
                entry.setAttribute("dayOfWeek", currentDate.getDayOfWeek());
                // Loop through the delivery methods and concat them                                    
                StringBuffer sbTypes = new StringBuffer();
                ShippingMethod shippingMethod = null;
                for (int d = 0; d < currentDate.getShippingMethods().size(); d++) 
                {
                    shippingMethod = (ShippingMethod)currentDate.getShippingMethods().get(d);
                    sbTypes.append(shippingMethod.getCode()).append(":");
                    shippingMethods.put(shippingMethod.getCode(), shippingMethod);
                }                                    
                entry.setAttribute("types", sbTypes.toString());

                // Pull out date ranges for florist deliverable dates
                // This will add duplicate dates to the list with different delivery 
                // methods if the product is both florist and carrier delivered
                if(currentDate.isInDateRange())
                {
                    inRange = true;
                    if(firstRange)
                    {
                        // Store the first date in the range
                        rangeDate = currentDate;
                    }

                    // remove the florist ship method from the element (but only
                    // if it's there in the first place)
                    String types = entry.getAttribute("types");
                    if ((types != null) && (types.indexOf("FL:") >= 0)) {
                        dateRangeDisplayBuffer.append(currentDate.getDayOfWeek());
                        dateRangeDisplayBuffer.append(" ");
                        dateRangeDisplayBuffer.append(currentDate.getDisplayDate());
                        dateRangeDisplayBuffer.append(" or ");
                        firstRange = false;
                        // Notice we leave firstRange alone otherwise (so next date will
                        // be first if necessary)
                    }
                    types = FieldUtils.replaceAll(types, "FL:", "");
                    entry.setAttribute("types", types);
                }
                else if(inRange)
                {
                    // Done with range, so add to xml.  
                    // Note that if firstRange is still true, then there were no florist ship methods
                    // in element, so there's nothing to add.
                    if (firstRange == false) {
                        entryRange = (Element) (document.createElement("deliveryDate"));
                        dateRangeDisplayBuffer.delete((dateRangeDisplayBuffer.length()-4), dateRangeDisplayBuffer.length());
                        entryRange.setAttribute("displayDate", dateRangeDisplayBuffer.toString());
                        entryRange.setAttribute("date",rangeDate.getDeliveryDate());
                        entryRange.setAttribute("dayOfWeek", "");
                        entryRange.setAttribute("types", "FL:");
                        entryRange.setAttribute("index", ("index" + i));
                        deliveryDates.appendChild(entryRange);
                        dateRangeDisplayBuffer = new StringBuffer();
                    }
                    inRange = false;
                    firstRange = true;
                    rangeDate = null;                        
                }
                entry.setAttribute("displayDate", currentDate.getDisplayDate());
                entry.setAttribute("index", ("index" + i));

                // Do not append the date if no delivery methods exist
                if(entry.getAttribute("types").length() != 0)
                {
                    deliveryDates.appendChild(entry);
                }
            }
        }

        OrderPO order = delParms.getOrder();
        String stateId = order.getSendToCustomer().getState();

        if ( shippingMethods.size() > 0 )
        {
            Element methodsElement = document.createElement("shippingMethods");
            root.appendChild(methodsElement);

            Element methodElement = document.createElement("shippingMethod");
            methodsElement.appendChild(methodElement);

            // only show Two Day delivery for Hawaii & Alaska                    
            // Two Day delivery is only available delivery option for carrier delivered products
            // going to Alaska or Hawaii
            if  ( (stateId != null) && 
                  (stateId.equals(GeneralConstants.STATE_CODE_ALASKA) ||
                   stateId.equals(GeneralConstants.STATE_CODE_HAWAII)) &&
                   delParms.getProductType() != null &&
                  (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                   delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)) )
            {
                // Product support 2-day delivery, so output shipping methods
                if ( delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) &&
                     shippingMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY_CODE) )
                {
                    BigDecimal cost = (BigDecimal) ((ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_TWO_DAY_CODE)).getDeliveryCharge();
                
                    entry = (Element) document.createElement("method");
                    entry.setAttribute("description", GeneralConstants.DELIVERY_TWO_DAY);
                    entry.setAttribute("code", GeneralConstants.DELIVERY_TWO_DAY_CODE);
                    entry.setAttribute("cost", cost.toString()); 
                    cost = cost.add(fuelSurchargeAmt);
                    entry.setAttribute("costWithSurcharge", cost.toString()); 
                
                    methodElement.appendChild(entry);
                }
                // Fresh Cut (next-day only) or product that does not support 2-day delivery
                // so cannot be delivered to Alaska or Hawaii
                else
                {
                    order.setDisplayProductUnavailable(GeneralConstants.YES);
                }
            }
            else
            {
                // JMP - 8/22/03
                // SDG Project rules
                // For SDG products with more than just ND delivery show both florist
                // carrier delivery methods
                if(delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) &&
                   (shippingMethods.containsKey(GeneralConstants.DELIVERY_STANDARD_CODE) ||
                    shippingMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY_CODE) ||
                    shippingMethods.containsKey(GeneralConstants.DELIVERY_SATURDAY_CODE)))
                {
                    // do nothing
                }                    
                // For SDFC and SDG products with ND delivery only show florist
                // if possible. If not then show ND delivery
                else if((delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) ||
                   delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG)) &&
                   (shippingMethods.containsKey(GeneralConstants.DELIVERY_FLORIST_CODE)))
                {
                    // Remove ND delivery
                    shippingMethods.remove(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
                }                    
            
                Collection methodCollection = shippingMethods.keySet();
                Iterator iter = methodCollection.iterator();

                while ( iter.hasNext() )
                {
                    String code = (String) iter.next();
                    
                    BigDecimal cost = (BigDecimal) ((ShippingMethod)shippingMethods.get(code)).getDeliveryCharge();
                    String description = ((ShippingMethod)shippingMethods.get(code)).getDescription();
                
                    entry = (Element) document.createElement("method");
                    
                    // fresh cut prducts use Floral service charge instead of delivery charge
                    if (delParms.getProductType() != null && (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||  delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                    {
                        entry.setAttribute("cost", "");
                        entry.setAttribute("costWithSurcharge", ""); 
                    }
                    else
                    {
                        if(cost == null)
                        {
                            entry.setAttribute("cost", "");                        
                            entry.setAttribute("costWithSurcharge", ""); 
                        }
                        else
                        {
                            //if(delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) && delParms.getSameDayGiftCharge() != null)
                            //{
                                //entry.setAttribute("cost", delParms.getSameDayGiftCharge().toString());               
                            //}
                            //else
                            //{
                                entry.setAttribute("cost", cost.toString()); 
                                cost = cost.add(fuelSurchargeAmt);
                                entry.setAttribute("costWithSurcharge", cost.toString()); 
                            //}
                        }                        
                    }
                    entry.setAttribute("description", description);
                    entry.setAttribute("code", code);                
                    methodElement.appendChild(entry);
                }
            }

        }
        // AK & HI rules
        else if(shippingMethods.size() == 0 && ((stateId != null) && 
                  (stateId.equals(GeneralConstants.STATE_CODE_ALASKA) ||
                   stateId.equals(GeneralConstants.STATE_CODE_HAWAII))) &&
                   (delParms.getProductType() != null &&
                  (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                   delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))))
        {
            order.setDisplayProductUnavailable(GeneralConstants.YES);
        }
        
        return document;
    }

    /**
     * 
     * @param TimeZone object representing recipients time zone
     * @param ArrayList List of OEDeliveryDate objects
     * @return String representing XML for delivery dates
     */
    public XMLDocument getXMLEncodedCalendarDates(TimeZone timeZone, ArrayList dates, String productId)
    {    
        XMLDocument document = new XMLDocument();
 
        Element calendarDatesElement = (Element) document.createElement("calendarDates");
        Element calendarElement = null;
        Element dateElement = null;
        Element yearElement = null;
        StringBuffer sb = new StringBuffer();

        document.appendChild(calendarDatesElement);

        ArrayList yearList = new ArrayList();
        
        if ( (productId != null) &&
             (!productId.trim().equals("")) )
        {
            Element productElement = (Element) document.createElement("product");
            calendarDatesElement.appendChild(productElement);

            Element dataElement = (Element) document.createElement("data");
            dataElement.setAttribute("productId", productId);
            productElement.appendChild(dataElement);
        }
  
        // get Calendar for current date
        Calendar currentDay = null;
        // Domestic delivery
        if ( timeZone != null )
        {
            currentDay = Calendar.getInstance(timeZone);
        }
        // International delivery
        else
        {
            currentDay = Calendar.getInstance();
        }

        int month = currentDay.get(Calendar.MONTH);
        month++;

        String todayDate = zeroPad(month) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);

        // output Calendar and Day nodes
        for ( int i=0; i < dates.size(); i++ )
        {
            OEDeliveryDate deliveryVO = (OEDeliveryDate) dates.get(i);
                
            // parse delivery date into Month, Day, and Year
            String stringDate = deliveryVO.getDeliveryDate();              

            String dateDay = stringDate.substring(3,5); 
            String dateMonth = stringDate.substring(0,2);
            String dateYear = stringDate.substring(6,10);
                
            // first time through, output Calendar root element
            if ( i ==0 )
            {
                calendarElement = (Element) document.createElement("calendar");
                calendarElement.setAttribute("month", String.valueOf(Integer.parseInt(dateMonth)));
                calendarElement.setAttribute("year", dateYear);
                calendarDatesElement.appendChild(calendarElement); 
            }
/* 
            // Check for a date range and parse it out into individual dates
            if(deliveryVO.getDisplayDate().length() > 10)
            {
                String displayDateString = FieldUtils.replaceAll(deliveryVO.getDisplayDate(), "or", ":");
                StringTokenizer tokenizer = new StringTokenizer(displayDateString, ":", false);                
                while(tokenizer.hasMoreTokens())
            {
                    OEDeliveryDate deliveryRangeVO = new OEDeliveryDate();                    
                    deliveryRangeVO.setDeliverableFlag(deliveryVO.getDeliverableFlag());
                    deliveryRangeVO.setDeliveryDate(deliveryVO.getDeliveryDate());
                    sb.append(tokenizer.nextToken());
                    deliveryRangeVO.setDayOfWeek(sb.substring(0, 4).trim());
                    sb.delete(0, 4);            
                    deliveryRangeVO.setDisplayDate(sb.toString().trim());
                    deliveryRangeVO.setHolidayText(deliveryVO.getHolidayText());
                    deliveryRangeVO.setShippingMethods(deliveryVO.getShippingMethods());
                    dateDay = deliveryRangeVO.getDisplayDate().substring(3,5); 
                    dateElement = this.makeCalendarDateElement(deliveryRangeVO, document, dateDay, todayDate, true);
                    calendarElement.appendChild(dateElement);
                    sb.setLength(0);                    
            }
                continue;
            }
?            */
            dateElement = this.makeCalendarDateElement(deliveryVO, document, dateDay, todayDate, deliveryVO.isInDateRange());                         
            calendarElement.appendChild(dateElement);
        }
 
        // output Years node
        int year = currentDay.get(Calendar.YEAR);
        for (int i=0; i < 2; i++)
        {
            yearList.add( String.valueOf(year++) );
        }
 
        // convert array to space delimited string
        StringBuffer years = new StringBuffer();
        for ( int i=0; i < yearList.size(); i++ ) 
        {
            years.append( (i > 0 ? " " : "") + yearList.get(i));
        }
                
        yearElement = (Element) document.createElement("years");        
        yearElement.setAttribute("value", years.toString());
        calendarDatesElement.appendChild(yearElement);       
 /*
        StringWriter stringWriter = new StringWriter();
        try
        {
            document.print(new PrintWriter(stringWriter));
        }
        catch(Exception e)
        {
            //log message
            lm.error("Can not convert XML to String");
        }
 
        return stringWriter.toString();
*/
        return document;
    }

    private Element makeCalendarDateElement(OEDeliveryDate deliveryVO, XMLDocument document, String dateDay, String todayDate, boolean dateRange)
    {
        String stringDate = deliveryVO.getDisplayDate();
        // output delivery date information each time
        Element dateElement = (Element) document.createElement("day");
        dateElement.setAttribute("value", String.valueOf(Integer.parseInt(dateDay)));            
        dateElement.setAttribute("dayOfWeek", deliveryVO.getDayOfWeek());
        dateElement.setAttribute("isDeliverable", deliveryVO.getDeliverableFlag());

        // Loop through the delivery methods and concat them                                    
        StringBuffer sbTypes = new StringBuffer();
        ShippingMethod shippingMethod = null;
        BigDecimal lowestCharge = new BigDecimal("999.99");
        for (int d = 0; d < deliveryVO.getShippingMethods().size(); d++) 
        {
            shippingMethod = (ShippingMethod)deliveryVO.getShippingMethods().get(d);
            sbTypes.append(shippingMethod.getCode()).append(":");
    
            if(shippingMethod.getDeliveryCharge().compareTo(lowestCharge) == -1)
            {
                lowestCharge = shippingMethod.getDeliveryCharge();
            }
        }                                    
        dateElement.setAttribute("types", sbTypes.toString());
        if(dateRange)
        {
            dateElement.setAttribute("range", "Y");
        }
        else
        {
            dateElement.setAttribute("range", "N");
        }

        if ( lowestCharge.compareTo(new BigDecimal("999.99")) != 0 )
        {
            dateElement.setAttribute("charge", lowestCharge.toString());
        }
        else
        {
            dateElement.setAttribute("charge", "0.00");
        }
            
        if ( stringDate.equals(todayDate) ) 
        {
            dateElement.setAttribute("currentDay", "Y");
        }
 
        // output Holiday Text and Date identifier if exists
        if ( !deliveryVO.getHolidayText().trim().equals("") )
        {
            dateElement.setAttribute("holiday", deliveryVO.getHolidayText());
        }
        
        return dateElement;
    }

   /**
     * 
     * @see #aaa
     * @param int numeric representation of Time Zone
     * @return TimeZone object based on intput zip time zone
     */
    private TimeZone getTimeZone(OEDeliveryDateParm parms)
    {
        Calendar calendar = Calendar.getInstance();        
        TimeZone tz = calendar.getTimeZone();
        TimeZone timeZone = null;

        // cutoff time for all Specialty Gift & Fresh Cut products is Central Time
        if ( (parms.getProductType() != null) && 
             (parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
              parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) )
        {
            timeZone = tz.getTimeZone("America/Chicago");
        }
        else
        {
            switch ( Integer.parseInt(parms.getZipTimeZone()) )
            {
                case 0:     // international
                    timeZone = tz.getTimeZone("America/Chicago");
                    break;
                case 1:     // eastern
                    timeZone = tz.getTimeZone("America/New_York");
                    break;
                case 2:     // central
                    timeZone = tz.getTimeZone("America/Chicago");
                    break;
                case 3:     // mountain
                    timeZone = tz.getTimeZone("America/Denver");
                    break;
                case 4:     // pacific
                    timeZone = tz.getTimeZone("America/Los_Angeles");
                    break;
                case 5:     // hawaiian (alaska, hawaii)
                    timeZone = tz.getTimeZone("HST");
                    break;
                case 6:     // atlantic (puerto rico , virgin islands)
                    timeZone = tz.getTimeZone("America/Puerto_Rico");
                    break;
            }
        }
        return timeZone;
    }

    /**
     * 
     * @param String number representation of month
     * @return int Calendar object numeric representation of month
     */
    private int getCalendarMonth(String argMonth)
    {
        int calMonth = 0;
        
        switch (Integer.parseInt(argMonth))
        {
            case 1:
                calMonth = Calendar.JANUARY;
                break;
            case 2:
                calMonth = Calendar.FEBRUARY;
                break;
            case 3:
                calMonth = Calendar.MARCH;
                break;
            case 4:
                calMonth = Calendar.APRIL;
                break;
            case 5:
                calMonth = Calendar.MAY;
                break;
            case 6:
                calMonth = Calendar.JUNE;
                break;
            case 7:
                calMonth = Calendar.JULY;
                break;
            case 8:
                calMonth = Calendar.AUGUST;
                break;
            case 9:
                calMonth = Calendar.SEPTEMBER;
                break;
            case 10:
                calMonth = Calendar.OCTOBER;
                break;
            case 11:
                calMonth = Calendar.NOVEMBER;
                break;
            case 12:
                calMonth = Calendar.DECEMBER;
                break;
        }
        
        return calMonth;
    }

    /**
     * 
     * @param Date first date to compare
     & @param Date second date to compare
     * @return Returns a long value showing the number of days between the 2 dates
     */
     private long getDateDiff (Date inFromDate, Date inToDate)
     {
        long diff = 0;

        // convert dates to same time of day (00:00:00) for better comparison as
        // number of milliseconds between dates can be less than 24 hours
        // which causes function to return 0 as difference
        // ex. 11/06/2002 23:59 & 11/07/2002 23:58 returns 0 as less than 24 
        // hours as elapsed
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String fromDate = sdf.format(inFromDate);
            String toDate = sdf.format(inToDate);

            Date date1 = sdf.parse(fromDate);
            Date date2 = sdf.parse(toDate);
        
            diff = date2.getTime() - date1.getTime();
        }
        catch (Exception e)
        {
            diff = inToDate.getTime() - inFromDate.getTime();
        }
        float tmpFlt = ((float)diff / (float)(1000 * 60 * 60 * 24));
        String tmpStr = String.valueOf(tmpFlt);
        BigDecimal bd = new BigDecimal(tmpStr);
        int ret = bd.setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
        return ret;
     }

    /**
     * This function checks to see if the Global Next Available Delivery Date (GNADD)
     * should be used.
     * @param OEDeliveryDateParm 
     * @return boolean Flag showing whether GNADD is active for the product zip code
     */
     private boolean getGnaddSetting(OEDeliveryDateParm delParms)
     {
        OEParameters oeParms = delParms.getGlobalParms();
        boolean gnaddActive = false;
        
        // global GNADD flag is active for all products and florists
        if ( oeParms.getGNADDLevel().equals("3") )
        {
            gnaddActive = true;
        }
        // GNADD level 2 and no GoTo Florists covering zip code, GNADD active
        else if ( oeParms.getGNADDLevel().equals("2") &&
                  delParms.getZipCodeGotoFloristFlag().equals("N") )
        {
            gnaddActive = true;
        }
        // GNADD level 1, no florists available in zip code, GNADD active
        else if ( (oeParms.getGNADDLevel().equals("1")) &&
                  (delParms.getZipCodeGNADDFlag().equals("Y")) )
        {
            gnaddActive = true;
        }
        
        return gnaddActive;
     }



    /**
     * This function returns the cutoff time for the order based on the global settings
     * @param boolean flag denoting whether the order is domestic or not
     * @param OEDeliveryDateParm global order entry parameters that hold the cutoff times
     * @param Calendar calendar object for the current day being processed 
     * @param boolean flag showing whether GNADD is active for the product zip code
     * @param boolean flag denoting whether order should be checked for zipcode cutoff
     */
     private int getCutoffTime(boolean domesticOrder, OEDeliveryDateParm delParms, OEParameters oeParms, 
                               Calendar cutoffDay, boolean forceSpeGft, boolean checkZipCutoff)
     {
        String cutoffTime = null;
        FTDDataRequest dataRequest = null;
        GenericDataService service = null;
        FTDDataResponse dataResponse = null;
        FTDDataAccessManager dam = null;

        if ( domesticOrder )
        {
            // Specialty Gift and Exotic cutoff times always take precedence
            //
            if ( delParms.getProductSubType() != null &&
                      delParms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC) )
            {
                cutoffTime = oeParms.getExoticCutoff();
            }
            else if ( delParms.getProductType() != null && 
                      delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT))
            {
                cutoffTime = oeParms.getSpecialtyGiftCutoff();
            }

            // If none of special cases above, then (if applicable) get cutoff time based on 
            // delivery zipcode or codified product cutoff for florist delivered products.  
            // If not applicable, then fall back to global cutoff times based on day of week (or
            // certain product type cutoffs).  Note special handling for Sat and Sun with 
            // respect to zipcode cutoff.
            //
            else
            {
                String zipCutoffTime    = null;
                int    zipCutoffTimeInt = -1;
                int    cutoffTimeInt    = -1;
                String curCutoffProdId = delParms.getProductId();
                String curCutoffZip    = delParms.getOrder().getSendToCustomer().getZipCode();

                // For florist delivered products, get zipcode cutoff time (if available)
                //
                if (checkZipCutoff) {
                    if (curCutoffZip == null) {
                        zipCutoffTime = null;            // Don't even bother when no zip code specified
                    } else if (lastCutoffZip.equals(curCutoffZip) && lastCutoffProdId.equals(curCutoffProdId)) {
                        zipCutoffTime = lastCutoffTime;  // Same as last time so just re-use
                    } else {
                        dataRequest = new FTDDataRequest();
                        dam = new FTDDataAccessManager();

                        dataRequest.addArgument("inZip",    curCutoffZip);
                        dataRequest.addArgument("inProdId", curCutoffProdId);

                        service =  dam.getGenericDataService(DataConstants.SEARCH_GET_ZIPCODE_CUTOFF);
                        try {
                            dataResponse = service.executeRequest(dataRequest);

                            String xpath = "zipcodeCutoff/zipCutoff/cutoffTime";
                            XPathQuery q = new XPathQuery();
                            NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
                            if (nl.getLength() > 0) {
                                Element cutoffData = (Element) nl.item(0);
                                zipCutoffTime = cutoffData.getAttribute("ZIPCUTOFF"); 
                                if ((zipCutoffTime != null) && (zipCutoffTime.length() == 0)) {
                                    zipCutoffTime = null;                                  
                                }
                            }
                            lastCutoffProdId = curCutoffProdId;
                            lastCutoffZip    = curCutoffZip;
                            lastCutoffTime   = zipCutoffTime;
                        } catch(Exception e) {
                            lm.error("getCutoffTime: " + e.toString(), e);
                        }
                    
                    }
                    if (zipCutoffTime != null) {
                        try {
                            zipCutoffTimeInt = Integer.parseInt(zipCutoffTime);
                        } catch (NumberFormatException nfe) {
                            lm.error("getCutoffTime: Zip cutoff is non-numeric", nfe);
                            zipCutoffTimeInt = -1;
                        }
                    }
                } // End if (checkZipCutoff...

                
                int dayOfWeek = cutoffDay.get(Calendar.DAY_OF_WEEK);

                // Use zipcode cutoff
                //
                if (zipCutoffTimeInt > -1) {
                    if ((dayOfWeek != Calendar.SUNDAY) && (dayOfWeek != Calendar.SATURDAY)) {
                        cutoffTime = zipCutoffTime;
                        //lm.debug("getCutoffTime: using zip cutoff (zip,prodId,time): " + 
                        //         curCutoffZip + "," + curCutoffProdId + "," + zipCutoffTime); 
                    } else {
                    
                        // For weekends, zipcode cutoff is ignored unless it specifies an
                        // earlier cutoff time. 
                        //
                        switch ( dayOfWeek ) {
                            case Calendar.SUNDAY:
                                cutoffTime = oeParms.getSundayCutoff();
                                break;
                            case Calendar.SATURDAY:
                                cutoffTime = oeParms.getSaturdayCutoff();
                                break;
                        }
                        try {
                            cutoffTimeInt = Integer.parseInt(cutoffTime);
                            if (zipCutoffTimeInt < cutoffTimeInt) {
                              cutoffTime = zipCutoffTime;
                              //lm.debug("getCutoffTime: using weekend zip cutoff (zip,prodId,time): " + 
                              //         curCutoffZip + "," + curCutoffProdId + "," + zipCutoffTime); 
                            }
                        } catch (NumberFormatException nfe) {
                            lm.error("getCutoffTime: Sat or Sun cutoff is non-numeric", nfe);
                        }
                    }

                // Otherwise, use global cutoff
                //
                } else {
                    switch ( dayOfWeek )
                    {
                        case Calendar.SUNDAY:
                            cutoffTime = oeParms.getSundayCutoff();
                            break;
                        case Calendar.MONDAY:
                            cutoffTime = oeParms.getMondayCutoff();
                            break;
                        case Calendar.TUESDAY:
                            cutoffTime = oeParms.getTuesdayCutoff();
                            break;
                        case Calendar.WEDNESDAY:
                            cutoffTime = oeParms.getWednesdayCutoff();
                            break;
                        case Calendar.THURSDAY:
                            cutoffTime = oeParms.getThursdayCutoff();
                            break;
                        case Calendar.FRIDAY:
                            cutoffTime = oeParms.getFridayCutoff();
                             break;
                        case Calendar.SATURDAY:
                            cutoffTime = oeParms.getSaturdayCutoff();
                            break;
                    }

                    // Other special cases (take precedence over day-of-week cutoff)
                    //
                    if ( delParms.getProductType() != null && 
                        delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) )
                    {
                        cutoffTime = oeParms.getFreshCutCutoff();
                    }
                    else if ((delParms.getProductType() != null && 
                             (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                             delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) && forceSpeGft))
                    {
                        cutoffTime = oeParms.getSpecialtyGiftCutoff();
                    }
                                        
                    //lm.debug("getCutoffTime: using global cutoff time (zip,prodId,time): " + 
                    //         curCutoffZip + "," + curCutoffProdId + "," + cutoffTime); 
                } // end else if (zipCutoffTimeInt > -1)...

                String latestCutOffTimeParameter = oeParms.getLatestCutoff();
                if(useLatestCutoffTimeParameter(latestCutOffTimeParameter, cutoffTime)) 
                {
                  cutoffTime = latestCutOffTimeParameter;                  
                }

            }
        } // end if (domesticOrder)...
        else
        {
            cutoffTime = oeParms.getIntlCutoff();
        }
        return Integer.parseInt(cutoffTime);
     }



    /**
     * 
     * This function left pads values that are less than 10 with zeros
     * @param int integer value that is to be left padded with 0's
      * @return string result of the padded value
     */
    private String zeroPad(int i)
    {
        String val = Integer.toString(i);
        return i < 10 ? "0" + val : val;
    }

    private String getDayOfWeek(int day)
    {
        String dayOfWeek = null;
        
        switch ( day )
        {
            case 1:
                dayOfWeek = "Sun";
                break;
            case 2:
                dayOfWeek = "Mon";
                break;
            case 3:
                dayOfWeek = "Tue";
                break;
            case 4:
                dayOfWeek = "Wed";
                break;
            case 5:
                dayOfWeek = "Thu";
                break;
            case 6:
                dayOfWeek = "Fri";
                break;
            case 7:
                dayOfWeek = "Sat";
                break;
        }

        return dayOfWeek;
    }

    /**
     * 
     * @return OEParameters object that contains the values of the global settings
     */
    public void getDeliveryRanges(String dataType, XMLDocument data, OEDeliveryDateParm parms)
    {
        HashMap deliveryRangeFrom = new HashMap();
        HashMap deliveryRangeTo = new HashMap();
        HashMap deliveryRangeText = new HashMap();

        try
        {
            Element element = null;
            XPathQuery q = new XPathQuery();
            String xpath = dataType + "/deliveryRangeData/data";
            NodeList nl = q.query(data, xpath);
           
            if (nl.getLength() > 0)
            {
                for ( int y = 0; y < nl.getLength(); y++ )
                {
                    element = (Element) nl.item(y);
                
                    deliveryRangeFrom.put(String.valueOf(y), element.getAttribute("startDate"));
                    deliveryRangeTo.put(String.valueOf(y), element.getAttribute("endDate"));
                    deliveryRangeText.put(String.valueOf(y), element.getAttribute("instructionText"));
                }
            }
        }
        catch (Exception e)
        {
            lm.error(e.toString(), e);
        }

        if ( deliveryRangeFrom.size() > 0 )
        {
            parms.setDeliveryRangeFlag(Boolean.TRUE);
            parms.setDeliveryRangeFrom(deliveryRangeFrom);
            parms.setDeliveryRangeTo(deliveryRangeTo);
            parms.setDeliveryRangeText(deliveryRangeText);
        }
        else
        {
            parms.setDeliveryRangeFlag(Boolean.FALSE);
        }
    }

    private OEDeliveryDate floristDeliverableDate(OEDeliveryDate dateVO, OEDeliveryDateParm delParms, Calendar calendar, Calendar currentDay) throws FTDApplicationException
    {
        boolean isDeliverable = true;
        boolean gnaddActive = false;
        OrderPO order = delParms.getOrder();
        boolean domesticOrder = order.getSendToCustomer().isDomesticFlag();
        Calendar today = Calendar.getInstance(calendar.getTimeZone());
        Calendar intlGNADDDay = (Calendar) today.clone();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date date = null;
        try
        {
            date = sdf.parse(dateVO.getDeliveryDate());
        }
        catch(Exception e)
        {
            lm.error(e.toString(), e);
            return null;        
        }        

        int addonDays = 0;

        // Check for codified products.  Do not allow delivery if the product 
        // is codified and not deliverable
        if(delParms.getCodifiedProduct() != null && delParms.getCodifiedProduct().equals("Y") &&
           delParms.getCodifiedSpecialFlag() != null && !delParms.getCodifiedSpecialFlag().equals("Y") &&
           delParms.getCodifiedAvailable() != null && delParms.getCodifiedAvailable().equals("N"))
        {
            isDeliverable = false;
        }


        
        // set GNADD date for country if not domestic order
        if ( !domesticOrder ) 
        {
            // use whichever is farther out
            if ( delParms.getGlobalParms().getIntlAddOnDays() > delParms.getCntryAddOnDays() )
            {
                addonDays = delParms.getGlobalParms().getIntlAddOnDays();
            }
            else
            {
                addonDays = delParms.getCntryAddOnDays();
            }
            intlGNADDDay.add(Calendar.DATE, addonDays);
        }

        // if the product is exotic then add one day to shipping
        if(delParms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC))
        {
            addonDays += 1;
        }

        // check to see if domestic floral GNADD is in effect
        if ( domesticOrder )
        {
            gnaddActive = getGnaddSetting(delParms);            
        }                    

        // check to see if GNADD is active for the passed zip code
        Date gnaddDate = delParms.getGlobalParms().getGNADDDate();
        if(gnaddDate != null)
        {
            long gnaddDiff = getDateDiff(date, gnaddDate);
            if ( gnaddActive && gnaddDiff > 0 )
            {
                    isDeliverable = false;
            }
        }


        // Set the times equal and just compare the dates
        intlGNADDDay.set(Calendar.MINUTE, currentDay.get(Calendar.MINUTE));
        intlGNADDDay.set(Calendar.SECOND, currentDay.get(Calendar.SECOND));
        intlGNADDDay.set(Calendar.MILLISECOND, currentDay.get(Calendar.MILLISECOND));
        
        // Check for International GNADD date
        if ( isDeliverable && 
             !domesticOrder &&
             (currentDay.before(intlGNADDDay)) )
        {
            isDeliverable = false;
        }       

        // Check for days that are not deliverable
        if (isDeliverable && (currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY))
        {
            // International floral orders do not allow Sunday delivery
            if(!domesticOrder || (delParms.isSundayDelivery() == Boolean.FALSE && !deliverOnSundayDueToRange(delParms, currentDay, domesticOrder, calendar)))
            {
                isDeliverable = false;
            }
        }

        if(isDeliverable)
        {
            if(domesticOrder && !delParms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC))
            {
                // For domestic orders
                // get the number of days between the today and process date to
                // use for comparisons.
                long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());
                if ( dateDiff == 0 )
                {
                    // format the current and cutoff times for comparison
                    int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));
                    int cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, false, true);

                    if ( (currentTime < cutoffTime) )
                    {                    
                        weArePastCutoff = Boolean.FALSE;
                        // Make sure product supports same day delivery (floral only)
                        if ( isDeliverable && !delParms.isDeliverToday().booleanValue() )
                        {
                            isDeliverable = false;
                            order.setDeliverTodayFlag(Boolean.FALSE);
                        }
                    }
                    else
                    {
                        weArePastCutoff = Boolean.TRUE;
                        // Past cutoff so cannot deliver today
                        isDeliverable = false;
                    }

                    // Funeral Occasion - override same day delivery to available
                    if ((order.getOccasion() != null) && 
                        (order.getOccasion().equals(GeneralConstants.OE_OCCASION_FUNERAL)) )
                    {
                        isDeliverable = true;
                        order.setDeliverTodayFlag(Boolean.TRUE);
                    }
                }
            }
            else
            {
                // For international and exotic floral orders we check for cutoff on current day + addon days
                // Get the number of days between the today and process date to
                // use for comparisons.
                long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());
                if ( dateDiff == addonDays )
                {
                    // format the current and cutoff times for comparison
                    int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));
                    int cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, false, false);

                    if(currentTime < cutoffTime) 
                    {                    
                        // Can deliver this day
                    }
                    else
                    {
                        // Past cutoff so cannot deliver today
                        isDeliverable = false;
                    }
                }
                else if(dateDiff == 0)
                {
                    // Cannot delivery today because same day is not available
                    // to exotic
                    isDeliverable = false;
                }
            }
        }

        // Check if floral delivery unavailable for zip code
        if ( domesticOrder )
        {
          if (delParms.getZipCodeFloralFlag().equals("N")) {
              isDeliverable = false;
          }
        }

        if(isDeliverable)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_FLORIST);
            shippingMethod.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
            shippingMethod.setDeliveryCharge(delParms.getFloralServiceCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        
        return dateVO;
    }

    private OEDeliveryDate carrierDeliverableDate(OEDeliveryDate dateVO, OEDeliveryDateParm delParms, Calendar calendar, Calendar currentDay)
    {
        boolean isDeliverable = true;
        boolean domesticOrder = delParms.getOrder().getSendToCustomer().isDomesticFlag();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar today = Calendar.getInstance(calendar.getTimeZone());
        boolean standardAvailable = true;
        boolean secondDayAvailable = true;
        boolean nextDayAvailable = true;
        boolean saturdayAvailable = true;
        HashMap shipMethods = null;
        int standardTransitTime = 3; // Days        
        int secondDayTransitTime = 2; // Days
        int nextDayTransitTime = 1; // Day
        int saturdayTransitTime = 2; // Day

        // JMP - 8/22/03 - We need to be able to show carrier delivery according to
        // page 3 of the spec
        //Check for codified products.  Do not allow carrier delivery if the product 
        // is codified, available from a florist and GNADD is not active
        //if(delParms.getCodifiedProduct() != null && delParms.getCodifiedProduct().equals("Y") &&
        //   delParms.getCodifiedSpecialFlag() != null && !delParms.getCodifiedSpecialFlag().equals("Y") &&
        //   delParms.getCodifiedAvailable() != null && !delParms.getCodifiedAvailable().equals("N") &&
        //   delParms.isShipMethodFlorist() && (getGnaddSetting(delParms) == false))
        //{
        //    isDeliverable = false;
        //}

        // Make currentDay Calendar have a timezone of CST
        currentDay.setTimeZone(currentDay.getTimeZone().getTimeZone("America/Chicago"));

        // check if vendor has blocked product from delivery on current day
        if ( isDeliverable && 
             (delParms.isVendorNoDeliverFlag() != null) && 
             (delParms.isVendorNoDeliverFlag().booleanValue()) )
        {
            HashMap noDeliverFromMap = delParms.getVendorNoDeliverFrom();
            HashMap noDeliverToMap = delParms.getVendorNoDeliverTo();
                
            for ( int y = 0; y < noDeliverFromMap.size(); y++ )
            {
                Date noDeliverFrom = (Date) noDeliverFromMap.get(String.valueOf(y));
                Date noDeliverTo = (Date) noDeliverToMap.get(String.valueOf(y));
                    
                if ( (getDateDiff(noDeliverFrom, currentDay.getTime()) > -1) &&
                     (getDateDiff(noDeliverTo, currentDay.getTime()) < 1) )
                {
                    isDeliverable = false;
                }
            }
        }

        // skip if date is Sunday
        if (isDeliverable && 
            (currentDay.get(Calendar.DAY_OF_WEEK)== Calendar.SUNDAY))
        {
            isDeliverable = false;
        }

        // Get product ship methods
        if ( delParms.getShipMethods() != null ) 
        {
            shipMethods = delParms.getShipMethods();
        }  
        
        // Check for shipping restrictions
        if(isDeliverable)
        {
            /*
             *  retreive the product information,
             *  and then check for the SDS ship method
             */
            ProductVO prodVO = null;
            try
            {
              prodVO = retrieveProductInformation(delParms.getProductId()) ; 
            }
            catch (Exception e)
            {
              lm.error("DeliveryDateUtil - carrierDeliverableDate() - Product Information cannot be retrieved.");
              lm.error(e); 
            }
  
            Calendar standardShipDate = getShipDate(GeneralConstants.DELIVERY_STANDARD_CODE, today, currentDay, delParms,standardTransitTime);
            Calendar secondDayShipDate = getShipDate(GeneralConstants.DELIVERY_TWO_DAY_CODE, today, currentDay, delParms, secondDayTransitTime);
            Calendar nextDayShipDate = getShipDate(GeneralConstants.DELIVERY_NEXT_DAY_CODE, today, currentDay, delParms, nextDayTransitTime);

            // Check to make sure each method's ship day is greater than right now
            // format the current and cutoff times for comparison
            int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));
            int cutoffTime = -1; 
            try
            {
              if (prodVO != null &&
                  prodVO.getShippingSystem() != null && !prodVO.getShippingSystem().equalsIgnoreCase("") && 
                  prodVO.getShippingSystem().equalsIgnoreCase("SDS"))
              {

                String sCutoffTime = retrieveVendorCutoffInfo(
                                          delParms.getProductSubCodeId() != null && 
                                          !delParms.getProductSubCodeId().equalsIgnoreCase("")?
                                          delParms.getProductSubCodeId():
                                          delParms.getProductId()) ; 

                if (sCutoffTime != null && !sCutoffTime.equalsIgnoreCase(""))
                  cutoffTime = Integer.valueOf(sCutoffTime).intValue(); 
                else 
                {
                  throw new Exception("The cutoff time is empty");
                }
              }
              else
              {
                cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, true,false);
              }
            }
            catch(Exception e)
            {
              lm.error("DeliveryDateUtil - carrierDeliverableDate() - Cutoff time could not be determined.");
              lm.error(e);
            }

            // Alaska and Hawaii must use two day shipping 
            String orderState = delParms.getOrder().getSendToCustomer().getState();
            long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());

            //System.out.println(currentDay.getTime().toString());
            if(orderState != null && (orderState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
               orderState.equals(GeneralConstants.STATE_CODE_HAWAII)))
            {
               saturdayTransitTime = secondDayTransitTime;
            }
            // If Saturday is closer than 2 days the do Next Day Delivery
            else if(dateDiff == 1 )
            {
                saturdayTransitTime = nextDayTransitTime;
            }
            //Saturday is always "Next Day Delivery" for freshcuts/sameday freshcuts
            else if(dateDiff >0 && ( 
                delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) )
            {
                saturdayTransitTime = nextDayTransitTime;
            }
            else if( dateDiff==2 &&
                delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) &&
                currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && currentTime > cutoffTime )
            {
                saturdayTransitTime = nextDayTransitTime;
            }

            //if (orderState != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) &&
            //             !orderState.equals(GeneralConstants.STATE_CODE_ALASKA) &&
            //            !orderState.equals(GeneralConstants.STATE_CODE_HAWAII ) ) 
            //{
            //    if (currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ) 
            //    {
            //        if( dateDiff > 1 )
            //        {
            //            if( delParms.isTwoDayShipFreshCuts() )
            //                saturdayTransitTime = secondDayTransitTime;
            //            else if(dateDiff == 1) 
            //                saturdayTransitTime = nextDayTransitTime; 
            //        }
            //    } 
            //}
            
            Calendar saturdayShipDate = getShipDate(GeneralConstants.DELIVERY_SATURDAY_CODE, today, currentDay, delParms, saturdayTransitTime);   

            dateDiff = getDateDiff(today.getTime(), standardShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                standardAvailable = false;
            }

            dateDiff = getDateDiff(today.getTime(), secondDayShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                secondDayAvailable = false;
            }

            dateDiff = getDateDiff(today.getTime(), nextDayShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                nextDayAvailable = false;
            }

            dateDiff = getDateDiff(today.getTime(), saturdayShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                saturdayAvailable = false;
            } 

            //Cannot get a two day delivery of fresh cuts on monday or tuesday
            if ( delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) &&
                  (currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY||currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY ))
            {
                secondDayAvailable = false;
            }

            // If the difference between the ship date and the current date is 
            // more than one day for Next Day delivery, do not allow the delivery 
            // of this product on the current date (since we're assuming product 
            // cannot be in transit more than one day).
            // Exception is friday or saturday ship for monday delivery.
            // Note this was changed (Jan 05) - it used to apply to cases where
            // Next Day was the ONLY delivery method - now it applies to cases
            // where there may be other delivery methods in addition to Next Day.
            dateDiff = getDateDiff(nextDayShipDate.getTime(), currentDay.getTime());
            if(shipMethods != null && shipMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY) &&
               !((nextDayShipDate.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY ||
                  nextDayShipDate.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY) && 
                    currentDay.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) &&
               dateDiff > 1 )
            {
                nextDayAvailable = false;    
            }
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_STANDARD))
        {
            standardAvailable = false;
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY))
        {
            secondDayAvailable = false;
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY))
        {
            nextDayAvailable = false;
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_SATURDAY))
        {
            saturdayAvailable = false;
        }        

        // if the delivery state is AK or HI then only 2nd day delivery is available
        if(delParms.getOrder().getSendToCustomer().getState() != null &&
           (delParms.getOrder().getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_ALASKA) ||
           delParms.getOrder().getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_HAWAII)))
        {
            standardAvailable = false;
            nextDayAvailable = false;
            saturdayAvailable = false;
        }

        // if all are false then not deliverable by carrier
        if(!standardAvailable && !secondDayAvailable && !nextDayAvailable && !saturdayAvailable)
        {
            isDeliverable = false;
        }
        
        if(isDeliverable && standardAvailable && currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_STANDARD);
            shippingMethod.setCode(GeneralConstants.DELIVERY_STANDARD_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_STANDARD)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }

        if(isDeliverable && secondDayAvailable && currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_TWO_DAY);
            shippingMethod.setCode(GeneralConstants.DELIVERY_TWO_DAY_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_TWO_DAY)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }

        if(isDeliverable && nextDayAvailable && currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_NEXT_DAY);
            shippingMethod.setCode(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_NEXT_DAY)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        
        if(isDeliverable && saturdayAvailable && currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_SATURDAY);
            shippingMethod.setCode(GeneralConstants.DELIVERY_SATURDAY_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_SATURDAY)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        
        return dateVO;
    }

    /*
     * Gets the ship day for a delivery method
     */
     private Calendar getShipDate(String shipMethod, Calendar today, Calendar currentDay, OEDeliveryDateParm delParms, int minTransitDays)
     {
        Calendar retDate = null;
        Calendar calTmp = (Calendar)currentDay.clone();
        //int standardTransitTime = 3; // Days        
        //int secondDayTransitTime = 2; // Days
        //int nextDayTransitTime = 1; // Day
        //int saturdayTransitTime = 2; // Day
        //int weekendRollBack = 0;        
        String currentDateStr = zeroPad(currentDay.get(Calendar.MONTH) + 1) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);
        OEDeliveryDate holidayDate = null;

        // check to see if date is a Holiday date
        if (delParms.getHolidayDates() != null )
        {
            holidayDate = (OEDeliveryDate) delParms.getHolidayDates().get(currentDateStr);
        }

        // This section moves the ship date back the specified number of 
        // transit days.
        if(minTransitDays > 0)
        {
            rollCalendarBack(calTmp);
            // decrement transit days if current date is not a weekend or holiday 
            if((holidayDate == null || (holidayDate != null && holidayDate.getDeliverableFlag().equals("Y"))) && 
               (currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY || 
                shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE)) && 
               currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
            {
                minTransitDays--;
            }
            calTmp = getShipDate(shipMethod, today, calTmp, delParms, minTransitDays);
        }

        // Once we have moved back the minimum transit days then check for
        // no ship days and vendor no ship dates.
        if ( (delParms.isVendorNoShipFlag() != null) && 
             (delParms.isVendorNoShipFlag().booleanValue()) )
        {
            HashMap noShipFromMap = delParms.getVendorNoShipFrom();
            HashMap noShipToMap = delParms.getVendorNoShipTo();
            for ( int y = 0; y < noShipFromMap.size(); y++ )
            {
                Date noShipFrom = (Date) noShipFromMap.get(String.valueOf(y));
                Date noShipTo = (Date) noShipToMap.get(String.valueOf(y));
                    
                if ( (getDateDiff(noShipFrom, calTmp.getTime()) > -1) &&
                     (getDateDiff(noShipTo, calTmp.getTime()) < 1) )
                {
                    rollCalendarBack(calTmp);
                    calTmp = getShipDate(shipMethod, today, calTmp, delParms, minTransitDays);
                }
            }
        }

        // check if product cannot be shipped for delivery current day
        if ( delParms.getNoShipDays() != null )
        {
            //Ignore the Friday noship day for Saturday deliveries
            if( shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE) && 
                calTmp.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY )
            {
                lm.debug("Overriding Friday \"No Ship Day\" for Saturday delivery");
            }
            else if ( delParms.getNoShipDays().contains(new Integer(calTmp.get(Calendar.DAY_OF_WEEK))))
            {
                rollCalendarBack(calTmp);
                calTmp = getShipDate(shipMethod, today, calTmp, delParms, minTransitDays);
            }
        }

        // Check for shipping on holidays
        if(holidayDate != null && holidayDate.getShippingAllowed().equals("N"))
        {
            rollCalendarBack(calTmp);
            calTmp = getShipDate(shipMethod, today, calTmp, delParms, minTransitDays);                            
        }
/*
        if(shipMethod.equals(GeneralConstants.DELIVERY_STANDARD_CODE))
        {
            for(int i = 0; i < standardTransitTime; i++)
            {
                rollCalendarBack(calTmp);
            }

            // if the ship date is Mon, Tue or Wed then we have to roll back two
            // more days because of no weekend shipping
            if(currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY ||
               currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY ||
               currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
            {
                weekendRollBack = 2;
            }

            for(int i = 0; i < weekendRollBack; i++)
            {
                rollCalendarBack(calTmp);
            }                        
        }
        else if(shipMethod.equals(GeneralConstants.DELIVERY_TWO_DAY_CODE))
        {
            for(int i = 0; i < secondDayTransitTime; i++)
            {
                rollCalendarBack(calTmp);
            }
            
            // if the ship date is Mon or Tue then we have to roll back two
            // more days because of no weekend shipping
            if(currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY ||
               currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
            {
                weekendRollBack = 2;
            }

            for(int i = 0; i < weekendRollBack; i++)
            {
                rollCalendarBack(calTmp);
            }                                    
        }
        else if(shipMethod.equals(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
        {
            for(int i = 0; i < nextDayTransitTime; i++)
            {
                rollCalendarBack(calTmp);
            }

            // if the ship date is Mon then we have to roll back two
            // more days because of no weekend shipping
            if(currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
            {
                weekendRollBack = 2;
            }

            for(int i = 0; i < weekendRollBack; i++)
            {
                rollCalendarBack(calTmp);
            }              
        }
        else if(shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE))
        {
            // Alaska and Hawaii must use two day shipping 
            String orderState = delParms.getOrder().getSendToCustomer().getState();
            long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());
            
            if(orderState != null && (orderState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
               orderState.equals(GeneralConstants.STATE_CODE_HAWAII)))
            {
               saturdayTransitTime = secondDayTransitTime;
            }
            // If Saturday is closer than 2 days the do Next Day Delivery
            else if(dateDiff == 1)
            {
                saturdayTransitTime = nextDayTransitTime;
            }
            
            for(int i = 0; i < saturdayTransitTime; i++)
            {
                rollCalendarBack(calTmp);
            }

            // if the ship date is Mon or Tue then we have to roll back two
            // more days because of no weekend shipping
            if((currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY ||
               currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) &&
               saturdayTransitTime == secondDayTransitTime)
            {
                weekendRollBack = 2;
            }
            else if(currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY && 
                    saturdayTransitTime == nextDayTransitTime)
            {
                weekendRollBack = 2;
            }

            for(int i = 0; i < weekendRollBack; i++)
            {
                rollCalendarBack(calTmp);
            }              
        }

*/
         return calTmp;
     }

    private void rollCalendarBack(Calendar calendar)
    {
        if((calendar.get(Calendar.MONTH) - 1) < 0 && (calendar.get(Calendar.DATE) - 1) < 1)
        {
            // roll back the year too
            calendar.roll(Calendar.YEAR, false);
        }
        
        if((calendar.get(Calendar.DATE) - 1) < 1)
        {
            // roll back the month too
            calendar.roll(Calendar.MONTH, false);
        }
        
        calendar.roll(Calendar.DATE, false);     
    }
/*
    public static void main(String[] args)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 6);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.YEAR, 2003);
        System.out.println(calendar.getTime().toString());
        for(int i = 0; i < 5; i++)
        {
            DeliveryDateUTIL.rollCalendarBack(calendar);
            System.out.println(calendar.getTime().toString());
        }
    }
*/

    private void printCalendar(Calendar calendar)
    {
/*    
        System.out.println("ERA: " + calendar.get(Calendar.ERA));
        System.out.println("YEAR: " + calendar.get(Calendar.YEAR));
        System.out.println("MONTH: " + calendar.get(Calendar.MONTH));
        System.out.println("WEEK_OF_YEAR: " + calendar.get(Calendar.WEEK_OF_YEAR));
        System.out.println("WEEK_OF_MONTH: " + calendar.get(Calendar.WEEK_OF_MONTH));
        System.out.println("DATE: " + calendar.get(Calendar.DATE));
        System.out.println("DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
        System.out.println("DAY_OF_YEAR: " + calendar.get(Calendar.DAY_OF_YEAR));
        System.out.println("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
        System.out.println("DAY_OF_WEEK_IN_MONTH: "
                        + calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));
        System.out.println("AM_PM: " + calendar.get(Calendar.AM_PM));
        System.out.println("HOUR: " + calendar.get(Calendar.HOUR));
        System.out.println("HOUR_OF_DAY: " + calendar.get(Calendar.HOUR_OF_DAY));
        System.out.println("MINUTE: " + calendar.get(Calendar.MINUTE));
        System.out.println("SECOND: " + calendar.get(Calendar.SECOND));
        System.out.println("MILLISECOND: " + calendar.get(Calendar.MILLISECOND));
        System.out.println("ZONE_OFFSET: "
                        + (calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000)));
        System.out.println("DST_OFFSET: "
                        + (calendar.get(Calendar.DST_OFFSET)/(60*60*1000)));
*/

        //System.out.println((calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DATE) + "/" +
        //                   calendar.get(Calendar.YEAR) + " " + calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + 
        //                   ":" + calendar.get(Calendar.SECOND) + " " + calendar.get(Calendar.AM_PM));

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:SS");
        sdf.setCalendar(calendar);
        String str = sdf.format(calendar.getTime());
        System.out.println(str);
    }

    private boolean deliverOnSundayDueToRange(OEDeliveryDateParm delParms, Calendar currentDay, boolean domesticOrder, Calendar calendar) throws FTDApplicationException
    {
        // validate input
        if (currentDay == null || delParms == null || delParms.isDeliveryRangeFlag() == null ||
          delParms.getDeliveryRangeFrom() == null || delParms.getDeliveryRangeTo() == null ||
          !delParms.isDeliveryRangeFlag().booleanValue()
          || !(currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) 
         {
          lm.error("Invalid Input to DeliveryDateUTIL.deliverOnSundayDueToRange()");
           return false;
         }
         
        boolean deliverOnSundayDueToRange = false;
        HashMap deliveryRangeFromHM = delParms.getDeliveryRangeFrom();
        HashMap deliveryRangeToHM = delParms.getDeliveryRangeTo();
        // the assumption is that 'to' and 'from' have the same keys 
        // (just as they do in getDeliveryRanges() of this same class)
        Iterator rangeIterator = deliveryRangeFromHM.keySet().iterator();
        while(rangeIterator != null && rangeIterator.hasNext()) {
          // get the 'to' and 'from' values
          String itemKey = (String) rangeIterator.next();
          String deliveryRangeFromS = (String) deliveryRangeFromHM.get(itemKey);
          String deliveryRangeToS = (String) deliveryRangeToHM.get(itemKey);
          if (isThisDateWithinTheDeliveryDateRange(deliveryRangeFromS, deliveryRangeToS, currentDay))
          {
             deliverOnSundayDueToRange = true;
             Calendar today = Calendar.getInstance(calendar.getTimeZone());
             if(isTodaySaturdayBeforeThisSunday(today, currentDay)){
               if(isPastTheCutoff(delParms, currentDay, domesticOrder)) 
               {
                   deliverOnSundayDueToRange = false;
               }
             }
          }
        } 
        return deliverOnSundayDueToRange;
    }

    private boolean isThisDateWithinTheDeliveryDateRange(String deliveryRangeFromS, 
      String deliveryRangeToS, Calendar currentDay) throws FTDApplicationException
    {
      //validate input
      if (deliveryRangeFromS == null || deliveryRangeToS == null || currentDay == null) 
      {
        lm.error("Invalid Input to DeliveryDateUTIL.isThisDateWithinTheDeliveryDateRange()");
        return false;
      }
      boolean itIs = false;
      Calendar deliveryRangeFromC = Calendar.getInstance();
      Calendar deliveryRangeToC = Calendar.getInstance();
      try {
        deliveryRangeFromC.setTime(FTDUtil.formatStringToUtilDate(deliveryRangeFromS));
        deliveryRangeToC.setTime(FTDUtil.formatStringToUtilDate(deliveryRangeToS));
      } catch (FTDApplicationException pe) 
      {
        lm.error(pe.getMessage());
        pe.printStackTrace();
        throw pe;
      }
      deliveryRangeToC.add(Calendar.DATE,1);
      if (currentDay.after(deliveryRangeFromC) && currentDay.before(deliveryRangeToC)) 
      {
        itIs = true;
      }
        
      return itIs;
    }
    
    private boolean isTodaySaturdayBeforeThisSunday(Calendar today, Calendar currentDay) 
    {
      //validate input
      if (today == null || currentDay == null) 
      {
        lm.error("Invalid Input to DeliveryDateUTIL.isTodaySaturdayBeforeThisSunday()");
        return false;
      }
      
       boolean itIs = false;
       if(today.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ){
         Date todayD = new Date(today.getTimeInMillis());
         Date currentDayD = new Date(currentDay.getTimeInMillis());
         if (getDateDiff(todayD, currentDayD) == 1) {
            itIs = true;
         }
       }
       return itIs;
    }
    
    private boolean isPastTheCutoff(OEDeliveryDateParm delParms, Calendar currentDay, boolean domesticOrder)
    {
      //validate input
      if (delParms == null || currentDay == null) 
      {
        lm.error("Invalid Input to DeliveryDateUTIL.isPastTheCutoff()");
        return false;
      }
      boolean itsPast = false;
      if (weArePastCutoff == null) {
        // format the current and cutoff times for comparison
        int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));
        int cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, false, true);
  
        if ( !(currentTime < cutoffTime) )
        {
            itsPast = true;
        }
      } 
      else 
      {
        itsPast = weArePastCutoff.booleanValue();  
      }
      return itsPast;
    }
    
    private boolean useLatestCutoffTimeParameter(String latestCutOffTimeParameter, String cutOffTime) 
    {
      boolean useuseLatestCutoff = false;
      if (latestCutOffTimeParameter != null && !latestCutOffTimeParameter.equals("")){
        int latestCutoff = Integer.parseInt(latestCutOffTimeParameter);
        if(cutOffTime != null && !cutOffTime.equals("")) {
          int cutOff = Integer.parseInt(cutOffTime);
          if(latestCutoff < cutOff) {
            useuseLatestCutoff = true;          
          }
        } else {
          useuseLatestCutoff = true;
        }
      }
      return useuseLatestCutoff; 
    }



    /*
     * This method retrieves the product information
     */
    public ProductVO retrieveProductInformation(String productId) throws Exception
    {
      ProductVO product = new ProductVO();
      FTDDataRequest dataRequest = new FTDDataRequest();
      GenericDataService service = null;
      FTDDataResponse dataResponse = new FTDDataResponse();
      FTDDataAccessManager dam = new FTDDataAccessManager();

      dataRequest.addArgument("productId", productId);

      service =  dam.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_BY_ID);

      /* execute the store prodcedure */
      dataResponse = service.executeRequest(dataRequest);

      String xpath = "productSearch/productSearchResults/productInfo";
      XPathQuery q = new XPathQuery();
      //Define the format
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
      NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
      if (nl.getLength() > 0) 
      {
        Element productData = (Element) nl.item(0);
        
        //set the product vo
        product.setProductId(productData.getAttribute("productId"));
        product.setNovatorId(productData.getAttribute("novatorId"));
        product.setProductName(productData.getAttribute("productName"));
        product.setNovatorName(productData.getAttribute("novatorName"));
        product.setStatus(productData.getAttribute("status"));
        product.setDeliveryType(productData.getAttribute("deliveryType"));
        product.setCategory(productData.getAttribute("category"));
        product.setProductType(productData.getAttribute("productType"));
        product.setProductSubType(productData.getAttribute("productSubType"));
        product.setColorSizeFlag(productData.getAttribute("colorSizeFlag"));
        product.setStandardPrice( productData.getAttribute("standardPrice")!= null?
                                  new Double((String)productData.getAttribute("standardPrice")).doubleValue():
                                  0
                                );
        product.setDeluxePrice( productData.getAttribute("deluxePrice")!= null?
                                new Double((String)productData.getAttribute("deluxePrice")).doubleValue():
                                0
                              );
        product.setPremiumPrice(  productData.getAttribute("premiumPrice")!= null?
                                  new Double((String)productData.getAttribute("premiumPrice")).doubleValue():
                                  0
                               );
        product.setPreferredPricePoint( productData.getAttribute("preferredPricePoint")!=null?
                                        new Double((String)productData.getAttribute("preferredPricePoint")).doubleValue():
                                        0
                                      );
        product.setVariablePriceMax ( productData.getAttribute("variablePriceMax")!= null?
                                      new Double((String)productData.getAttribute("variablePriceMax")).doubleValue():
                                      0
                                    );
        product.setShortDescription(productData.getAttribute("shortDescription"));
        product.setLongDescription(productData.getAttribute("longDescription"));
        product.setFloristReferenceNumber(productData.getAttribute("floristReferenceNumber"));
        product.setMercuryDescription(productData.getAttribute("mercuryDescription"));
        product.setItemComments(productData.getAttribute("itemsComments"));
        product.setAddOnBalloonsFlag(productData.getAttribute("addOnBallonsFlag"));
        product.setAddOnBearsFlag(productData.getAttribute("addOnBearsFlag"));
        product.setAddOnCardsFlag(productData.getAttribute("addOnCardsFlag"));
        product.setAddOnFuneralFlag(productData.getAttribute("addOnFuneralFlag"));
        product.setCodifiedFlag(productData.getAttribute("codifiedFlag"));
        product.setExceptionCode(productData.getAttribute("codifiedFlag"));
        
        String exceptionStartDate = (String)productData.getAttribute("exceptionStartDate");        
        String exceptionEndDate = (String)productData.getAttribute("exceptionEndDate");
        lm.debug("exceptionStartDate: "+exceptionStartDate);
        lm.debug("exceptionEndDate: "+exceptionEndDate);
        if  ( exceptionStartDate!= null && !exceptionStartDate.equalsIgnoreCase("") )
        {
            //Defect 2579
            product.setExceptionStartDate (sdf.parse(exceptionStartDate));
        }
        
        if  ( exceptionEndDate!= null && !exceptionEndDate.equalsIgnoreCase("") )
        {
            //Defect 2579
            product.setExceptionEndDate(sdf.parse(exceptionEndDate));
        }
        
        product.setExceptionMessage(productData.getAttribute("exceptionMessage"));
        product.setSecondChoiceCode(productData.getAttribute("secondChoiceCode"));
        product.setHolidaySecondChoiceCode(productData.getAttribute("holidaySecondChoiceCode"));
        product.setDropshipCode(productData.getAttribute("dropshipCode"));
        product.setDiscountAllowedFlag(productData.getAttribute("discountAllowedFlag"));
        product.setDeliveryIncludedFlag(productData.getAttribute("discountAllowedFlag"));
        product.setTaxFlag(productData.getAttribute("taxFlag"));
        product.setServiceFeeFlag(productData.getAttribute("serviceFeeFlag"));
        product.setExoticFlag(productData.getAttribute("exoticFlag"));
        product.setEgiftFlag(productData.getAttribute("egiftFlag"));
        product.setCountryId(productData.getAttribute("countryId"));
        product.setArrangementSize(productData.getAttribute("arrangementSize"));
        product.setArrangementColors(productData.getAttribute("arrangementColors"));
        product.setDominantFlowers(productData.getAttribute("dominantFlowers"));
        product.setSearchPriority(productData.getAttribute("searchPriority"));
        product.setRecipe(productData.getAttribute("recipe"));
        product.setSubcodeFlag(productData.getAttribute("subcodeFlag"));
        product.setDimWeight(productData.getAttribute("dimWeight"));
        product.setNextDayUpgradeFlag(productData.getAttribute("nextDayUpgradeFlag"));
        product.setCorporateSite(productData.getAttribute("corporateSite"));
        product.setUnspscCode(productData.getAttribute("unspscCode"));
        product.setPriceRank1(productData.getAttribute("priceRank1"));
        product.setPriceRank2(productData.getAttribute("priceRank2"));
        product.setPriceRank3(productData.getAttribute("priceRank3"));
        product.setShipMethodCarrier(productData.getAttribute("shipMethodCarrier"));
        product.setShipMethodFlorist(productData.getAttribute("shipMethodFlorist"));
        product.setShippingKey(productData.getAttribute("shippingKey"));
        product.setVariablePriceFlag(productData.getAttribute("variablePriceFlag"));
        product.setHolidaySku(productData.getAttribute("holidaySku"));
        product.setHolidayPrice ( productData.getAttribute("holidayPrice")!=null?
                                  new Double((String)productData.getAttribute("holidayPrice")).doubleValue():
                                  0
                                );
        product.setCatalogFlag(productData.getAttribute("catalogFlag"));
        product.setHolidayDeluxePrice ( productData.getAttribute("holidayDeluxePrice")!= null?
                                        new Double((String)productData.getAttribute("holidayDeluxePrice")).doubleValue():
                                        0
                                      );
        product.setHolidayPremiumPrice( productData.getAttribute("holidayPremiumPrice")!= null?
                                        new Double((String)productData.getAttribute("holidayPremiumPrice")).doubleValue():
                                        0
                                      );
        if  ( productData.getAttribute("holidayStartDate")!= null && 
              !((String)productData.getAttribute("holidayStartDate")).equalsIgnoreCase("")
            )
        {
          product.setHolidayStartDate(sdf.parse(productData.getAttribute("holidayStartDate")));
        }


        if  ( productData.getAttribute("holidayEndDate")!= null && 
              !((String)productData.getAttribute("holidayEndDate")).equalsIgnoreCase("")
            )
        {
          product.setHolidayEndDate(sdf.parse(productData.getAttribute("holidayEndDate")));
        }

        product.setHoldUntilAvailable(productData.getAttribute("holdUntilAvailable"));
        product.setMondayDeliveryFreshcut(productData.getAttribute("mondayDeliveryFreshcut"));
        product.setTwoDaySatFreshcut(productData.getAttribute("twoDayShipSatFreshcut"));
        product.setShippingSystem(productData.getAttribute("shippingSystem"));

      }

      return product;

    }

    /*
     * This method checks if we are currenly past the vendor cutoff time.
     * This is not the same as the SPEGFT and FRECUT cutoff times that OE uses.
     */
    public String retrieveVendorCutoffInfo(String productId) throws Exception
    {
      String cutoff = null;
            
      FTDDataRequest dataRequest = new FTDDataRequest();
      GenericDataService service = null;
      FTDDataResponse dataResponse = new FTDDataResponse();
      FTDDataAccessManager dam = new FTDDataAccessManager();

      dataRequest.addArgument("productId", productId);
      service =  dam.getGenericDataService(DataConstants.SEARCH_GET_MAX_VENDOR_CUTOFF);

      /* execute the store prodcedure */
      dataResponse = service.executeRequest(dataRequest);

      String xpath = "maxVendorCutoffSearch/maxVendorCutoffSearchResults/maxVendorCutoffInfo";
      XPathQuery q = new XPathQuery();
      NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
      if (nl.getLength() > 0) 
      {
        Element cutofftData = (Element) nl.item(0);
        cutoff = cutofftData.getAttribute("cutoff");
      }
      
      return cutoff;
    }
}
