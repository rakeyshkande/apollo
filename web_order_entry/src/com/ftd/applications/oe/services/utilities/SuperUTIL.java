package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.common.utilities.LogManager;

public class SuperUTIL 
{
    protected LogManager lm;
    public SuperUTIL(LogManager logManager)
    {
        lm = logManager;
    }

    public LogManager getLogManager() 
    {
        return lm;
    }
}