package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.common.utilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.util.*;

public class KeywordSearch extends SuperUTIL
{
    private final static String WEBOE_CONFIG_CONTEXT = "WEBOE_CONFIG_CONTEXT";
    
    public KeywordSearch(LogManager lm)
    {
        super(lm);
    }

    public String send(String keywords, String companyId)
    {
        Date timerDate = new Date();
		lm.debug("starting KeywordSearch");

        String ret = "";
        try
	    {
            ConfigurationUtil rm = ConfigurationUtil.getInstance();        
    		StringBuffer message = encodeXml(keywords, companyId);
            lm.debug(message.toString());
            String serverIP = rm.getFrpGlobalParm(WEBOE_CONFIG_CONTEXT, "keyword.server.ip");
            String serverPort = rm.getFrpGlobalParm(WEBOE_CONFIG_CONTEXT, "keyword.server.port");
			SocketClient sc = new SocketClient(lm, serverIP, Integer.parseInt(serverPort));
			sc.setDebug(true);
			sc.open();
			ret = sc.send(message.toString());
			sc.close();
		}
		catch(Exception e)
		{
			lm.error(e.getMessage(), e);
		}

        long timeInMs = (System.currentTimeMillis() - timerDate.getTime());
        if(timeInMs > 1000)
        {
            lm.info("Novator Keyword search took " + timeInMs + " ms");            
        }
/*
        //Test data (get fresh data from the log4joutput.txt file)
        ret  = "<?xml version=\"1.0\"?><!-- filename = keywordSearchResults.xml --><keywordSearch><products><product productId=\"GC09\"/><product productId=\"GC10\"/><product productId=\"GC11\"/><product productId=\"GC12\"/><product productId=\"CT20\"/><product productId=\"0BGG\"/><product productId=\"0BGB\"/><product productId=\"X058\"/><product productId=\"GC01\"/><product productId=\"2850\"/><product productId=\"X604\"/><product productId=\"2571\"/>";
        ret += "<product productId=\"X212\"/><product productId=\"BB24\"/><product productId=\"X018\"/><product productId=\"P120\"/><product productId=\"B508\"/><product productId=\"6103\"/><product productId=\"X073\"/><product productId=\"X417\"/><product productId=\"COLG\"/><product productId=\"2103\"/><product productId=\"GC06\"/><product productId=\"W027\"/><product productId=\"GC07\"/><product productId=\"XX32\"/><product productId=\"2586\"/>";
        ret += "<product productId=\"2101\"/><product productId=\"T125\"/><product productId=\"H014\"/><product productId=\"B15-3502\"/><product productId=\"2408\"/><product productId=\"X202\"/><product productId=\"X211\"/><product productId=\"C37-2945\"/><product productId=\"A216\"/><product productId=\"JS10\"/><product productId=\"JB10\"/><product productId=\"XX-6050\"/><product productId=\"2534\"/><product productId=\"B511\"/><product productId=\"X346\"/>";
        ret += "<product productId=\"X315\"/><product productId=\"2225\"/><product productId=\"X345\"/><product productId=\"GC04\"/><product productId=\"T342\"/><product productId=\"C193\"/><product productId=\"X151\"/><product productId=\"MLB2\"/><product productId=\"FFF2\"/><product productId=\"X41\"/><product productId=\"T032\"/><product productId=\"BB22\"/><product productId=\"X807\"/><product productId=\"F205\"/><product productId=\"X605\"/>";
        ret += "<product productId=\"BB19\"/><product productId=\"X419\"/><product productId=\"BB27\"/><product productId=\"C13-3068\"/><product productId=\"2259\"/><product productId=\"2215\"/><product productId=\"2435\"/><product productId=\"FM00\"/><product productId=\"2434\"/><product productId=\"X342\"/><product productId=\"CT21\"/><product productId=\"2506\"/><product productId=\"6101\"/><product productId=\"X641\"/><product productId=\"EO-6036\"/>";
        ret += "<product productId=\"2433\"/><product productId=\"X045\"/><product productId=\"2049\"/><product productId=\"EO-6034\"/><product productId=\"X343\"/><product productId=\"6X39\"/><product productId=\"X344\"/><product productId=\"FFCC\"/><product productId=\"C40-3536\"/><product productId=\"X064\"/><product productId=\"E028\"/><product productId=\"D5-821P\"/><product productId=\"EO-6037\"/><product productId=\"FFMY\"/><product productId=\"B500\"/>";
        ret += "<product productId=\"EO-6033\"/><product productId=\"EO-6035\"/><product productId=\"P635\"/><product productId=\"6100\"/><product productId=\"6099\"/><product productId=\"P245\"/><product productId=\"2432\"/><product productId=\"2001\"/><product productId=\"2431\"/></products></keywordSearch>CON";
*/
        return ret;
    }

	private StringBuffer encodeXml(String keywords, String companyId)
	{
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\"?>");
		xml.append("<keywordSearch>");

        // ** SFMB **
        if(companyId != null && companyId.equals("SFMB"))
        {
            ResourceManager rm = ResourceManager.getInstance();
        
            xml.append("<markcode>");
            xml.append(rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "keyword.sfmb.markcode"));
            xml.append("</markcode>");
        }
        // ** Gift Site **
        else if(companyId != null && companyId.equals("GIFT"))
        {
            ResourceManager rm = ResourceManager.getInstance();
        
            xml.append("<markcode>");
            xml.append(rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "keyword.gift.markcode"));
            xml.append("</markcode>");
        }
        // ** High End Site **
        else if(companyId != null && companyId.equals("HIGH"))
        {
            ResourceManager rm = ResourceManager.getInstance();
        
            xml.append("<markcode>");
            xml.append(rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "keyword.high.markcode"));
            xml.append("</markcode>");
        }
        // ** FUSA **
        else if(companyId != null && companyId.equals("FUSA"))
        {
            ResourceManager rm = ResourceManager.getInstance();
        
            xml.append("<markcode>");
            xml.append(rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "keyword.fusa.markcode"));
            xml.append("</markcode>");
        }
        // ** FDIRECT **
        else if(companyId != null && companyId.equals("FDIRECT"))
        {
            ResourceManager rm = ResourceManager.getInstance();
        
            xml.append("<markcode>");
            xml.append(rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "keyword.fdirect.markcode"));
            xml.append("</markcode>");
        }
        // ** FLORIST **
        else if(companyId != null && companyId.equals("FLORIST"))
        {
            ResourceManager rm = ResourceManager.getInstance();
        
            xml.append("<markcode>");
            xml.append(rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "keyword.florist.markcode"));
            xml.append("</markcode>");
        }
        
		xml.append("<keywords>");
		StringTokenizer s = new StringTokenizer(keywords);
		int count = 0;
		while(s.hasMoreTokens())
		{
			xml.append("<keyword value=\"");
			String word = s.nextToken();
			xml.append(word);
			xml.append("\"/>");
		}
		xml.append("</keywords>");
		xml.append("</keywordSearch>");
		return xml;
    }
}
