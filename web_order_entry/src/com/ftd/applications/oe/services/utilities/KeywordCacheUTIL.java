package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.common.utilities.*;

import java.util.*;

public class KeywordCacheUTIL extends SuperUTIL
{
    private static HashMap mapCache;
    private static KeywordCacheUTIL keywordCacheUtil;
    private static int maxCacheSizeInt;
    
    private KeywordCacheUTIL(LogManager lm)
    {
        super(lm);
        ResourceManager rm = ResourceManager.getInstance();
        String resetTime = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "KeywordRefresh");
        if(resetTime == null) resetTime = "180";
        String maxCacheSize = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "KeywordMaxCache");
        if(maxCacheSize == null) maxCacheSize = "100";
        maxCacheSizeInt = Integer.parseInt(maxCacheSize);
        mapCache = new HashMap(maxCacheSizeInt);
        // convert to milliseconds
        long timeInMillis = (Long.parseLong(resetTime) * 60 * 1000);
        KeywordCacheResetTask task = new KeywordCacheResetTask(this, lm);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, new Date(), timeInMillis);
    }

    public static synchronized KeywordCacheUTIL getInstance(LogManager lm)
    {
        if(keywordCacheUtil == null)
        {
            keywordCacheUtil = new KeywordCacheUTIL(lm);
        }

        return keywordCacheUtil;
    }

    public synchronized String getProducts(String keywords)
    {
        String products = (String)mapCache.get(keywords);
        
        return products;
    }

    public synchronized void setProducts(String keywords, String products)
    {
        if(mapCache.size() + 1 > maxCacheSizeInt)
        {
            mapCache.clear();
        }
        
        mapCache.put(keywords, products);
    }   

    public synchronized void clearCache()
    {
        mapCache.clear();
    }
}

class KeywordCacheResetTask extends TimerTask
{
    KeywordCacheUTIL keywordUtil;
    LogManager lm;
    
    public KeywordCacheResetTask(KeywordCacheUTIL util, LogManager lm)
    {
        keywordUtil = util;
        this.lm = lm;
    }

    public void run()
    {
        keywordUtil.clearCache();
        lm.info("---- Keyword Cache Reset");
    }
}