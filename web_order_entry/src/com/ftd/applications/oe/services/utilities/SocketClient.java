package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.common.utilities.LogManager;

import java.net.*;
import java.io.*;

public class SocketClient extends SuperUTIL
{
    private Socket socket;
    private String serverName;
    private int portNumber;
    private OutputStreamWriter writer;
    private BufferedReader reader;
    private boolean debug = false;


    private static final char DELIMITER = '\n';

    public void setDebug( boolean debug )
    {
        this.debug = debug;
    }
    public SocketClient( LogManager lm, String serverName, int portNumber) throws IOException
    {
        super(lm);
        this.serverName = serverName;
        this.portNumber = portNumber;
    }

    public void open() throws IOException
    {
        socket = new Socket( serverName, portNumber );
        writer = new OutputStreamWriter( socket.getOutputStream() );
        reader = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
    }

    public String send( String message ) throws IOException
    {
        char[] chars = message.toCharArray();
        writer.write( chars, 0, chars.length );
        writer.write( DELIMITER );

        if ( lm != null && debug )
            lm.debug( "Sent: " + message );
        writer.flush();


        StringBuffer sb = new StringBuffer();
        int i;
        while ( (i=reader.read()) != -1 ) {
            if ( ( (char)i )!= DELIMITER )
                sb.append( (char)i );
            else
                break;
        }

        if ( lm != null && debug )
            lm.debug( "Received: " + sb.toString() );
        return sb.toString();

    }

    public void close() throws IOException
    {
        if ( writer != null)
            writer.close();
        if ( reader != null )
            reader.close();
        if ( socket != null )
            socket.close();
    }
}