package com.ftd.applications.oe.services.utilities;

import com.ftd.applications.oe.services.*;
import com.ftd.framework.businessservices.servicebusinessobjects.*;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.*;
import com.ftd.applications.oe.common.persistent.*;
import com.ftd.applications.oe.services.utilities.*;
import org.w3c.dom.*;
import oracle.xml.parser.v2.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.math.*;

import java.rmi.server.UID;

public class OEOrderUTIL 
{

/**
  * A unitity method to generate a unique 10-digit number
  * @return long a 10-digit unique number
  */
    public static long getUniqueNumber()
    {
        long seq = 0;

        try{

        FTDDataRequest dataRequest = new FTDDataRequest();

        ShoppingSVC shoppingSVC = new ShoppingSVC();
        GenericDataService service = shoppingSVC.getGenericDataService("search service::get sequence");

        FTDDataResponse dataResponse = service.executeRequest(dataRequest);

        XMLDocument document = (XMLDocument)dataResponse.getDataVO().getData();
        String xpath = "OE_SEQUENCE_UTIL/SEQUENCES/SEQUENCE";
        XPathQuery q = new XPathQuery();
        NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

        XMLElement seqElement = (XMLElement) nl.item(0);     

        seqElement.print(System.out);
        String valueString = seqElement.getAttribute("NEXTVAL");        

        seq = Long.parseLong(valueString); 


        }
        catch(Exception e)
        {
          e.printStackTrace();          
        }

        //UID uid = new UID();
        //long hashCode = Math.abs( uid.hashCode() );        
        //return  (hashCode < (long)1000000000) ? ( hashCode + (long)1000000000 ) : hashCode;

        return seq;

    

    }
    
}