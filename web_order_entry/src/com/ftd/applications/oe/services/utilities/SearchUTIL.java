package com.ftd.applications.oe.services.utilities;

import com.ftd.framework.common.utilities.*;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.valueobjects.*;

import com.ftd.applications.oe.common.*;
import com.ftd.applications.oe.common.persistent.*;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;

import oracle.xml.parser.v2.*;
import org.w3c.dom.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.math.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.DOMSource;

import java.sql.Connection;

public class SearchUTIL extends SuperUTIL
{
    public static final int GET_OCCASION_DESCRIPTION = 0;
    public static final int GET_COUNTRY_DESCRIPTION = 1;

    public SearchUTIL(LogManager lm)
    {
        super(lm);
    }

    public Map getHolidays(XMLDocument doc) throws Exception
    {
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
        Map holidayMap = new HashMap();
        XPathQuery q = new XPathQuery();
        String xpath = "productList/holidayDates/data";
        NodeList nl = q.query(doc, xpath);
        Date holidayUtilDate;
        Date currentDate = new Date();
        Element element = null;
        OEDeliveryDate holidayDate = null;
        
        if ( nl.getLength() > 0 )
        {
            for ( int i = 0; i < nl.getLength(); i++ )
            {
                element = (Element) nl.item(i);

                holidayDate = new OEDeliveryDate();
                holidayDate.setDeliveryDate(element.getAttribute("holidayDate"));
                holidayDate.setHolidayText(element.getAttribute("holidayDescription"));
                holidayDate.setDeliverableFlag(element.getAttribute("deliverableFlag"));
                holidayDate.setShippingAllowed(element.getAttribute("shippingAllowed"));
                holidayUtilDate = sdfInput.parse(holidayDate.getDeliveryDate());
                holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
            }
        }
        return holidayMap;
    }

    /**
     * This method builds the XML for a list of products or for one product.  
     * This is used from the product list page and the product detail page. 
     * @param isList Tells if the productList parameter is actually a list or just
     * one product
     * @param loadDeliveryDates Switch to turn off loading of delivery dates
     * @param productList An XMLDocument of product details
     * @param order The current order object
     * @param displayCount The number of products to display per page
     * @param argMonth The month
     * @param argYear The year 

     * @return This method builds an Element object for the product list and 
     * product detail pages.
     * 
     * @author Jeff Penney
     *
     */
    public Element pageBuilder(boolean isList, boolean loadDeliveryDates, XMLDocument productList, OrderPO order, int displayCount, String argMonth, String argYear)
    {
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Connection connection = null;

        try {
            // Get fuel surcharge amount
            connection = DataManager.getInstance().getConnection();
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(connection));
            
            OEParameters oeGlobalParms = new OEParameters();
            HashMap shipMethodsMap = new HashMap();

            // Create initial node structure
            Element listElement = xmlDocument.createElement("productList");
            Element productsElement = xmlDocument.createElement("products");
            xmlDocument.appendChild(rootElement);
            rootElement.appendChild(listElement);
            listElement.appendChild(productsElement);

            // Storage for delivery dates if product detail
            Element deliveryDatesListElement = null;
            Element shipMethodsElement = null;
            Element shipMethodElement = null;
            Element calendarListElement = null;
            Element yearListElement = null;

            // Pull out elements for the page
            Element element = null;
            NodeList nl = null;
            String xpath = null;
            XPathQuery q = new XPathQuery();

            // retrieve the Global Parameter information
            oeGlobalParms = this.getGlobalParms();
            
            // retrieve the product information
            xpath = "productList/products/product";
            nl = q.query(productList, xpath);

            int pageSplitter = 0;
            order.setSearchRecordCnt(nl.getLength());

            if (nl.getLength() > 0)
            {
                double currentPrice;
                double currentDiscountAmount;
                BigDecimal feeWithFuelSurcharge;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;
                String stateId = null;
                String productType = null;

                for(int i=pageSplitter; i < (pageSplitter + displayCount) && i < nl.getLength(); i++)
                {            
                    element = (Element) nl.item(i);
                    Date firstArrivalDate = null;

                    // Fuel surcharge is added to serviceCharge here in case it doesn't get set in any conditionals below
                    String serviceChargeAttr = element.getAttribute("serviceCharge");
                    if ((serviceChargeAttr != null) && (!serviceChargeAttr.equals(""))) {
                        feeWithFuelSurcharge = fuelSurchargeAmt.add(new BigDecimal(serviceChargeAttr));
                        element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                    }

                    // set Florist delivery information
                    if ( element.getAttribute("shipMethodFlorist") != null &&
                         element.getAttribute("shipMethodFlorist").equals("Y"))
                    {
                        if(element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) || element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) 
                        {
                            if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null && order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) 
                            {
                                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getGlobalParameters().getFreshCutSrvcCharge());
                                element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            }
                            else 
                            {
                                if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) 
                                {
                                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                                    element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                                else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) 
                                {
                                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                                    element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }
                        }
                    }

                    // Check delivery type for carrier ship methods
                    if ( element.getAttribute("shipMethodCarrier") != null &&
                         element.getAttribute("shipMethodCarrier").equals("Y") )
                    {
                        HashMap shipMethods = new HashMap();

                        if(element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) || element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) 
                        {
                            if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null && order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) 
                            {
                                feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getGlobalParameters().getFreshCutSrvcCharge());
                                element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            }
                            else 
                            {
                                if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) 
                                {
                                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getDomesticServiceFee());
                                    element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                                else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) 
                                {
                                    feeWithFuelSurcharge = fuelSurchargeAmt.add(order.getInternationalServiceFee());
                                    element.setAttribute("serviceCharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }
                        }

                        // Get the shipping methods for this product
                        HashMap shippingMethods = this.getProductShippingMethods(element.getAttribute("productID"), element.getAttribute("standardPrice"), element.getAttribute("shippingKey"));

                        // Set the delivery types and prices in the product XML Node
                        int count = 1;
                        ShippingMethod method = null;
                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_STANDARD))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_STANDARD);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_STANDARD);
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryPrice" + count), feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }

                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_TWO_DAY);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_TWO_DAY);
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryPrice" + count), feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }

                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_NEXT_DAY);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_NEXT_DAY);
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryPrice" + count), feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }

                        element.setAttribute("extraShippingFee","");
                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_SATURDAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_SATURDAY);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_SATURDAY);
                            feeWithFuelSurcharge = fuelSurchargeAmt.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryPrice" + count), feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                            //Set the service fee for saturday fresh cuts
                            if( (element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                                 element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) )&& 
                                order.getSendToCustomer().isDomesticFlag() )
                            {
                                element.setAttribute("extraShippingFee", String.valueOf(oeGlobalParms.getFreshCutSatCharge()));
                            }
                        }
                    }
                    // Determine if there is a special fee assoc with state and product type
                    order.setSpecialFeeFlag("N");
                    if ( (order.getSendToCustomer().getState() != null) &&
                         (order.getSendToCustomer().getState().equals("AK") ||
                          order.getSendToCustomer().getState().equals("HI")) )
                    {
                        if ( (element.getAttribute("productType") != null) &&
                             (element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)))
                        {
                            element.setAttribute("displaySpecialFee", "Y");
                            order.setSpecialFeeFlag("Y");
                        }
                    }

                    String codifiedAvailable = element.getAttribute("codifiedAvailable");
                    String codifiedProduct = element.getAttribute("codifiedProduct");
                    String codifiedSpecial = element.getAttribute("codifiedSpecial");
                    String codifiedDeliverable = element.getAttribute("codifiedDeliverable");
                    
                    // Set unavail flag if the status is U or its an unavailable codified product
                    if ( // product is unavaiable if status is "U"
                         (element.getAttribute("status") != null &&
                          element.getAttribute("status").equals("U")) ||

                        // product is unavaiable if it is a regular codified product
                        // and it is set as unavailable for this zip code in the
                        // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                         (codifiedAvailable != null &&
                          codifiedAvailable.equals(GeneralConstants.NO) &&
                          (element.getAttribute("shipMethodCarrier") != null &&
                           !element.getAttribute("shipMethodCarrier").equals("Y"))) ||

                        // product is unavaiable if it is a regular codified product 
                        // and GNADD is at level 3
                         (codifiedProduct != null &&
                          codifiedProduct.equals(GeneralConstants.YES) &&
                          oeGlobalParms.getGNADDLevel().equals("3"))
                       )
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.YES);
                    }
                    else
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.NO);
                    }

                    // Special Codified products should be deliverable if GNADD is on.
                    if (element.getAttribute("status") != null &&
                        !element.getAttribute("status").equals("U") &&
                        codifiedSpecial != null &&
                        codifiedSpecial.equals(GeneralConstants.YES) &&
                        codifiedDeliverable != null &&
                        (!codifiedDeliverable.equals("N") &&
                         !codifiedDeliverable.equals("D"))
                       )
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.NO);
                    }
            
                    // Do not allow freshcuts and specialty gift to be shipped to Canada
                    String country = null;
                    String state = null;
                    if(order.getSendToCustomer() != null)
                    {
                        country = order.getSendToCustomer().getCountry();
                        state = order.getSendToCustomer().getState();
                        
                        if(((country != null && country.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA)) ||
                            (state != null && (state.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) || state.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)))) && 
                            (element.getAttribute("productType") != null) &&
                            ((element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) || element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT))))
                        {
                            order.setDisplayProductUnavailable("Y");
                        }
                    }
                    
                    if(loadDeliveryDates) 
                    {
                        // retreive first available delivery date information for product list
                        if ( isList )
                        {
                            OEDeliveryDate firstAvailableDate = null;
                            List dates = this.getItemDeliveryDates(productList, element.getAttribute("productID"), order, true);                                                        
                            firstAvailableDate = (OEDeliveryDate)dates.get(0);

                            // Determine which carrier to show (florist or fed ex)
                            for (int ii = 0; ii < firstAvailableDate.getShippingMethods().size(); ii++) 
                            {
                                ShippingMethod shipMethod = (ShippingMethod)firstAvailableDate.getShippingMethods().get(ii);
                                if(shipMethod.getCode().equals(GeneralConstants.DELIVERY_FLORIST_CODE))
                                {                                        
                                    element.setAttribute("firstDeliveryMethod", "florist");

                                    // If this is SDG and a florist can deliver then remove all other shipping methods
                                    if(element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                                       element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC))
                                    {
                                        element.setAttribute("deliveryType1", "");
                                        element.setAttribute("deliveryType2", "");
                                        element.setAttribute("deliveryType3", "");
                                        element.setAttribute("deliveryType4", "");
                                        element.setAttribute("deliveryType5", "");
                                    }
                                    break;
                                }
                                else
                                {
                                    element.setAttribute("firstDeliveryMethod", "carrier");
                                }
                            }
                            
                            element.setAttribute("deliveryDate", firstAvailableDate.getDisplayDate());
                        }
                        // retrieve delivery date information for selected product
                        else
                        {
                            List dateList = getItemDeliveryDates(productList, element.getAttribute("productID"), order, false);

                            deliveryDatesListElement = xmlDocument.createElement("deliveryDates");
                            Element deliveryDateElement = null;
                            boolean firstDate = true;

                            for(int z = 0; z < dateList.size(); z++) {
                                deliveryDateElement = xmlDocument.createElement("deliveryDate");

                                OEDeliveryDate deliveryDate = (OEDeliveryDate) dateList.get(z);
                                if ( deliveryDate.getDeliverableFlag().equals("Y") )
                                {
                                    if (firstDate) {
                                        element.setAttribute("deliveryDate", deliveryDate.getDeliveryDate());
                                        firstDate = false;
                                    }

                                    deliveryDateElement.setAttribute("date", deliveryDate.getDeliveryDate());
                                    deliveryDateElement.setAttribute("dayOfWeek", deliveryDate.getDayOfWeek());

                                    // Loop through the delivery methods and concat them
                                    StringBuffer sbTypes = new StringBuffer();
                                    ShippingMethod shippingMethod = null;
                                    for (int d = 0; d < deliveryDate.getShippingMethods().size(); d++)
                                    {
                                        shippingMethod = (ShippingMethod)deliveryDate.getShippingMethods().get(d);
                                        sbTypes.append(shippingMethod.getCode()).append(":");
                                        shipMethodsMap.put(shippingMethod.getCode(), shippingMethod.getDeliveryCharge());
                                    }
                                    deliveryDateElement.setAttribute("types", sbTypes.toString());

                                    deliveryDateElement.setAttribute("displayDate", deliveryDate.getDisplayDate());
                                    deliveryDatesListElement.appendChild(deliveryDateElement);
                                }
                            }

                            if ( element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                                 element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) )
                            {
                                if ( shipMethodsMap.size() > 0 )
                                {
                                    shipMethodElement = xmlDocument.createElement("shippingMethod");

                                    Collection methodCollection = shipMethodsMap.keySet();
                                    Iterator iter = methodCollection.iterator();

                                    while ( iter.hasNext() )
                                    {
                                        String description = (String) iter.next();
                                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(description);

                                        Element entry = (Element) xmlDocument.createElement("method");
                                        entry.setAttribute("description", description);
                                        entry.setAttribute("cost", cost.toString());

                                        shipMethodElement.appendChild(entry);
                                    }
                                }
                            }
                        }
                    }

                    // Convert prices to XX.XX format
                    String price = null;
                    price = element.getAttribute("standardPrice");                  
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("standardPrice", price);
                    }

                    price = element.getAttribute("deluxePrice");                 
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("deluxePrice", price);
                    }
                    price = element.getAttribute("premiumPrice");                    
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("premiumPrice", price);
                    }

                    // Calculate discounts for product
                    standardDiscountAmount = element.getAttribute("standardDiscountAmt");
                    deluxeDiscountAmount = element.getAttribute("deluxeDiscountAmt");
                    premiumDiscountAmount = element.getAttribute("premiumDiscountAmt");

                    if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                        element.setAttribute("standardRewardValue", standardDiscountAmount);
                    }

                    if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                        element.setAttribute("deluxeRewardValue", deluxeDiscountAmount);
                    }

                    if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                        element.setAttribute("premiumRewardValue", premiumDiscountAmount);
                    }

                    // Determine if its a partner or standard discount
                    if(order.getPricingCode() != null && order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE) && order.getPartnerId() != null && !order.getPartnerId().equals("")) {

                      if(!order.getRewardType().equals("TotSavings")) {

                        if(element.getAttribute("standardPrice") != null && !element.getAttribute("standardPrice").equals("")) {
                            currentPrice = new BigDecimal(element.getAttribute("standardPrice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("standardRewardValue", retrievePromotionValue(order.getPromotionList(), currentPrice));
                        }
                        if(element.getAttribute("deluxePrice") != null && !element.getAttribute("deluxePrice").equals("")) {
                            currentPrice =  new BigDecimal(element.getAttribute("deluxePrice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("deluxeRewardValue", retrievePromotionValue(order.getPromotionList(), currentPrice));
                        }
                        if(element.getAttribute("premiumPrice") != null && !element.getAttribute("premiumPrice").equals("")) {
                            currentPrice = new BigDecimal(element.getAttribute("premiumPrice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("premiumRewardValue", retrievePromotionValue(order.getPromotionList(), currentPrice));
                        }
                      }
                    }

                    else {

                        if(order.getRewardType() != null)
                        {
                            // Discount type of dollars off
                            if(order.getRewardType().equals("Dollars")) {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardPrice"));
                                    currentPrice = currentPrice - Double.parseDouble(standardDiscountAmount);
                                    element.setAttribute("standardDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxePrice"));
                                    currentPrice = currentPrice - Double.parseDouble(deluxeDiscountAmount);
                                    element.setAttribute("deluxeDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumPrice"));
                                    currentPrice = currentPrice - Double.parseDouble((premiumDiscountAmount));
                                    element.setAttribute("premiumDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }

                            // Discount type of percent off
                            else if(order.getRewardType().equals("Percent")) {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardPrice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(standardDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("standardDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxePrice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(deluxeDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("deluxeDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumPrice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(premiumDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("premiumDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }
                        }
                    }

                    productsElement.appendChild(xmlDocument.importNode(element, true));

                    // Do only if product detail (only one cycle in loop)
                    if ( !isList && loadDeliveryDates )
                    {
                        rootElement.appendChild(deliveryDatesListElement);

                        XMLDocument deliveryDateDocument = new XMLDocument();
                        Element deliveryDateElement = deliveryDateDocument.createElement("deliveryDateList");
                        deliveryDateElement.appendChild(deliveryDateDocument.importNode(deliveryDatesListElement, true));
                        deliveryDateDocument.appendChild(deliveryDateElement);
                    }

                    // get state id for later processing
                    stateId = element.getAttribute("stateId");
                    productType = element.getAttribute("productType");
                } // end of For...Loop

                // only output for Product List
                Element shipMethods = xmlDocument.createElement("shippingMethods");

                // If zip code state is Alaska (AK) or Hawaii (HI) output valid
                // delivery types for state for presentation layer to use
                if  ( (stateId != null) &&
                      (stateId.equals("AK") ||
                       stateId.equals("HI")) )
                {
                    // if 2-day delivery is not one of the ship methods, then do not show any methods
                    // and set the product as undeliverable
                    if ( isList ||
                         shipMethodsMap.containsKey(GeneralConstants.DELIVERY_TWO_DAY) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(GeneralConstants.DELIVERY_TWO_DAY);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = xmlDocument.createElement("shippingMethod");
                        Element entry = (Element) xmlDocument.createElement("method");
                        entry.setAttribute("description", GeneralConstants.DELIVERY_TWO_DAY);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if ( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(GeneralConstants.DELIVERY_STANDARD);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = xmlDocument.createElement("shippingMethod");
                        Element entry = (Element) xmlDocument.createElement("method");
                        entry.setAttribute("description", GeneralConstants.DELIVERY_STANDARD);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if(productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))
                    // If the state is AK or HI and this is a freshcut then
                    // delivery is not available unless to day is available
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.YES);
                    }
                }
                // product list, show all delivery methods
                else if ( isList )
                {
                    // retrieve the Global Parameter information
                    xpath = "productList/shippingMethods/data";
                    //q = new XPathQuery();
                    nl = q.query(productList, xpath);

                    if ( nl.getLength() > 0 )
                    {
                        shipMethodElement = xmlDocument.createElement("shippingMethod");

                        for (int z = 0; z < nl.getLength(); z++)
                        {
                            element = (Element) nl.item(z);

                            Element entry = (Element) xmlDocument.createElement("method");
                            entry.setAttribute("description", element.getAttribute("description"));
                            entry.setAttribute("cost", element.getAttribute("cost"));
                            shipMethodElement.appendChild(entry);
                        }
                    }
                }
                // product detail / delivery information, only show available delivery methods
                if ( shipMethodElement != null )
                {
                    shipMethods.appendChild(shipMethodElement);
                }
                rootElement.appendChild(shipMethods);
            }
            else 
            {
                lm.info("No results were returned to page through");
            }
        }
        catch(Exception e) 
        {
            lm.error(e.toString(), e);
        }
        finally {
            if(connection != null) {
              try {connection.close();} catch (Exception z) {}
            }
        }

        return rootElement;
    }
    
/*
    public Element pageBuilder(boolean isList, boolean loadDeliveryDates, XMLDocument productList, OrderPO order, int displayCount, String argMonth, String argYear)
    {
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");

        try {
            SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
            OEParameters oeGlobalParms = new OEParameters();
            HashMap holidayMap = new HashMap();
            HashMap shipMethodsMap = new HashMap();

            // Create initial node structure
            Element listElement = xmlDocument.createElement("productList");
            Element productsElement = xmlDocument.createElement("products");
            xmlDocument.appendChild(rootElement);
            rootElement.appendChild(listElement);
            listElement.appendChild(productsElement);

            // Storage for delivery dates if product detail
            Element deliveryDatesListElement = null;
            Element shipMethodsElement = null;
            Element shipMethodElement = null;
            Element calendarListElement = null;
            Element yearListElement = null;

            // Pull out elements for the page
            Element element = null;
            NodeList nl = null;
            String xpath = null;
            XPathQuery q = null;

            // retrieve the Global Parameter information
            xpath = "productList/globalParmsData/data";
            q = new XPathQuery();
            nl = q.query(productList, xpath);

            if ( nl.getLength() > 0 )
            {
                element = (Element) nl.item(0);

                String days = element.getAttribute("deliveryDaysOut");
                oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(element.getAttribute("deliveryDaysOut")));
                oeGlobalParms.setExoticCutoff(element.getAttribute("exoticCutoff"));
                oeGlobalParms.setFreshCutCutoff(element.getAttribute("freshcutsCutoff"));
                oeGlobalParms.setFridayCutoff(element.getAttribute("fridayCutoff"));
                oeGlobalParms.setGNADDDate(sdfInput.parse(element.getAttribute("gnaddDate")));
                oeGlobalParms.setGNADDLevel(element.getAttribute("gnaddLevel"));
                oeGlobalParms.setIntlAddOnDays(Integer.parseInt(element.getAttribute("intlOverrideDays")));
                oeGlobalParms.setIntlCutoff(element.getAttribute("internationalCutoff"));
                oeGlobalParms.setMondayCutoff(element.getAttribute("mondayCutoff"));
                oeGlobalParms.setSaturdayCutoff(element.getAttribute("saturdayCutoff"));
                oeGlobalParms.setSpecialtyGiftCutoff(element.getAttribute("specialtyGiftCutoff"));
                oeGlobalParms.setSundayCutoff(element.getAttribute("sundayCutoff"));
                oeGlobalParms.setThursdayCutoff(element.getAttribute("thursdayCutoff"));
                oeGlobalParms.setTuesdayCutoff(element.getAttribute("tuesdayCutoff"));
                oeGlobalParms.setWednesdayCutoff(element.getAttribute("wednesdayCutoff"));
            }

            // retrieve the Holiday Date information
            xpath = "productList/holidayDates/data";
            nl = q.query(productList, xpath);
            Date holidayUtilDate;
            Date currentDate = new Date();
            if ( nl.getLength() > 0 )
            {
                for ( int i = 0; i < nl.getLength(); i++ )
                {
                    element = (Element) nl.item(i);

                    OEDeliveryDate holidayDate = new OEDeliveryDate();
                    holidayDate.setDeliveryDate(element.getAttribute("holidayDate"));
                    holidayDate.setHolidayText(element.getAttribute("holidayDescription"));
                    holidayDate.setDeliverableFlag(element.getAttribute("deliverableFlag"));
                    holidayDate.setShippingAllowed(element.getAttribute("shippingAllowed"));
                    holidayUtilDate = sdfInput.parse(holidayDate.getDeliveryDate());
                    // Only add the holiday date if it is greater than or equal to today - 30 days
                    // this will safely account for holidays in the past month
                    //if(holidayUtilDate.getTime() >= (currentDate.getTime() - (30 * 24 * 60 * 60 * 1000)))
                    //{
                        holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
                    //}
                }
            }

            // retrieve the product information
            xpath = "productList/products/product";
            nl = q.query(productList, xpath);

            int pageSplitter = 0;

            order.setSearchRecordCnt(nl.getLength());


            // retrieve the delivery range information from the XML and put into
            // a temporary delivery date parameter object so the values
            // can be set for each product below without re-searching the
            // the complete XML document for date ranges
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(lm);
            OEDeliveryDateParm deliveryDateRangeParms = new OEDeliveryDateParm();
            if(loadDeliveryDates)
            {
                deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_PRODUCT_LIST, productList, deliveryDateRangeParms);
            }

            if (nl.getLength() > 0)
            {
                double currentPrice;
                double currentDiscountAmount;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;
                String stateId = null;
                String productType = null;

                for(int i=pageSplitter; i < (pageSplitter + displayCount) && i < nl.getLength(); i++)
                {            
                    element = (Element) nl.item(i);
                    OEDeliveryDateParm deliveryDateParm = new OEDeliveryDateParm();
                    deliveryDateParm.setOrder(order);

                    deliveryDateParm.setProductType(element.getAttribute("productType"));
                    deliveryDateParm.setProductSubType(element.getAttribute("productSubType"));
                    deliveryDateParm.setDeliveryType(element.getAttribute("deliveryType"));
                    deliveryDateParm.setProductId(element.getAttribute("productID"));
                    deliveryDateParm.setDeliverToday(Boolean.TRUE);
                    deliveryDateParm.setSundayDelivery(Boolean.TRUE);

                    deliveryDateParm.setCodifiedSpecialFlag(element.getAttribute("codifiedSpecial"));

                    // Exotic floral items cannot be delivered same day
                    if ( element.getAttribute("productSubType").equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC) ||
                         element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                         element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                         element.getAttribute("deliveryType").equals(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL) )
                    {
                        deliveryDateParm.setDeliverToday(Boolean.FALSE);
                    }

                    // set the exception dates for product
                    deliveryDateParm.setExceptionCode(element.getAttribute("exceptionCode"));
                    try {
                        deliveryDateParm.setExceptionFrom(sdfInput.parse(element.getAttribute("exceptionStartDate")));
                        deliveryDateParm.setExceptionTo(sdfInput.parse(element.getAttribute("exceptionEndDate")));
                    }
                    catch (Exception e) {
                    }

                    // set the vendor no delivery dates for product
                    HashMap vendorNoDeliverFrom = new HashMap();
                    HashMap vendorNoDeliverTo = new HashMap();
                    for ( int y=1; y < 7; y++ )
                    {
                        String vendorFrom = element.getAttribute("vendorDelivBlockStart" + y);

                        if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                        {
                            vendorNoDeliverFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorDelivBlockStart" + y)));
                            vendorNoDeliverTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorDelivBlockEnd" + y)));
                        }
                    }
                    if ( vendorNoDeliverFrom.size() > 0 )
                    {
                        deliveryDateParm.setVendorNoDeliverFlag(Boolean.TRUE);
                        deliveryDateParm.setVendorNoDeliverFrom(vendorNoDeliverFrom);
                        deliveryDateParm.setVendorNoDeliverTo(vendorNoDeliverTo);
                    }
                    else
                    {
                        deliveryDateParm.setVendorNoDeliverFlag(Boolean.FALSE);
                    }

                    // set the vendor no ship dates for product
                    HashMap vendorNoShipFrom = new HashMap();
                    HashMap vendorNoShipTo = new HashMap();
                    for ( int y=1; y < 7; y++ )
                    {
                        String vendorFrom = element.getAttribute("vendorShipBlockStart" + y);

                        if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                        {
                            vendorNoShipFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorShipBlockStart" + y)));
                            vendorNoShipTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorShipBlockEnd" + y)));
                        }
                    }
                    if ( vendorNoShipFrom.size() > 0 )
                    {
                        deliveryDateParm.setVendorNoShipFlag(Boolean.TRUE);
                        deliveryDateParm.setVendorNoShipFrom(vendorNoShipFrom);
                        deliveryDateParm.setVendorNoShipTo(vendorNoShipTo);
                    }
                    else
                    {
                        deliveryDateParm.setVendorNoShipFlag(Boolean.FALSE);
                    }

                    Date firstArrivalDate = null;

                    // set Florist delivery information
                    if ( element.getAttribute("shipMethodFlorist") != null &&
                         element.getAttribute("shipMethodFlorist").equals("Y"))
                    {
                        //deliveryDateParm.setSaturdayDelivery(Boolean.TRUE);
                        deliveryDateParm.setShipMethodFlorist(true);

                        if(element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)) {
                            if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null && order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) {
                                element.setAttribute("serviceCharge", order.getGlobalParameters().getFreshCutSrvcCharge().toString());
                            }
                            else {
                                if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) {
                                    element.setAttribute("serviceCharge", order.getDomesticServiceFee().toString());
                                }
                                else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) {
                                    element.setAttribute("serviceCharge", order.getInternationalServiceFee().toString());
                                }
                            }
                        }

                        if ( element.getAttribute("serviceCharge") != null &&
                              !element.getAttribute("serviceCharge").trim().equals("") )
                        {
                            deliveryDateParm.setFloralServiceCharge(new BigDecimal(element.getAttribute("serviceCharge")));
                        }
                    }

                    // Check delivery type for carrier ship methods
                    if ( element.getAttribute("shipMethodCarrier") != null &&
                         element.getAttribute("shipMethodCarrier").equals("Y") )
                    {
                        //deliveryDateParm.setSaturdayDelivery(Boolean.FALSE);
                        deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                        deliveryDateParm.setShipMethodCarrier(true);

                        HashMap shipMethods = new HashMap();

                        if(element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)) {
                            if(order.getGlobalParameters().getFreshCutSrvcChargeTrigger() != null && order.getGlobalParameters().getFreshCutSrvcChargeTrigger().equals(GeneralConstants.YES)) {
                                element.setAttribute("serviceCharge", order.getGlobalParameters().getFreshCutSrvcCharge().toString());
                            }
                            else {
                                if ( (order.getSendToCustomer().isDomesticFlag()) && (order.getDomesticServiceFee() != null) ) {
                                    element.setAttribute("serviceCharge", order.getDomesticServiceFee().toString());
                                }
                                else if ( (!order.getSendToCustomer().isDomesticFlag()) && (order.getInternationalServiceFee() != null) ) {
                                    element.setAttribute("serviceCharge", order.getInternationalServiceFee().toString());
                                }
                            }
                        }

                        if ( element.getAttribute("serviceCharge") != null &&
                              !element.getAttribute("serviceCharge").trim().equals("") )
                        {
                            deliveryDateParm.setFloralServiceCharge(new BigDecimal(element.getAttribute("serviceCharge")));
                        }

                        // Get the shipping methods for this product
                        HashMap shippingMethods = this.getProductShippingMethods(element.getAttribute("productID"), element.getAttribute("standardPrice"), element.getAttribute("shippingKey"));

                        deliveryDateParm.setShipMethods(shippingMethods);

                        // Set the delivery types and prices in the product XML Node
                        int count = 1;
                        ShippingMethod method = null;
                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_STANDARD))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_STANDARD);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_STANDARD);
                            element.setAttribute(("deliveryPrice" + count), method.getDeliveryCharge().toString());
                            count++;
                        }

                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_TWO_DAY);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_TWO_DAY);
                            element.setAttribute(("deliveryPrice" + count), method.getDeliveryCharge().toString());
                            count++;
                        }

                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_NEXT_DAY);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_NEXT_DAY);
                            element.setAttribute(("deliveryPrice" + count), method.getDeliveryCharge().toString());
                            count++;
                        }

                        if(shippingMethods.containsKey(GeneralConstants.DELIVERY_SATURDAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_SATURDAY);
                            element.setAttribute(("deliveryType" + count), GeneralConstants.DELIVERY_SATURDAY);
                            element.setAttribute(("deliveryPrice" + count), method.getDeliveryCharge().toString());
                            count++;
                        }


                        // build values of No Ship Days for product
                        HashSet noShipDays = new HashSet();
                        if ( element.getAttribute("sundayFlag") != null &&
                             element.getAttribute("sundayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.SUNDAY));
                        }
                        if ( element.getAttribute("mondayFlag") != null &&
                             element.getAttribute("mondayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.MONDAY));
                        }
                        if ( element.getAttribute("tuesdayFlag") != null &&
                             element.getAttribute("tuesdayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.TUESDAY));
                        }
                        if ( element.getAttribute("wednesdayFlag") != null &&
                             element.getAttribute("wednesdayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.WEDNESDAY));
                        }
                        if ( element.getAttribute("thursdayFlag") != null &&
                             element.getAttribute("thursdayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.THURSDAY));
                        }
                        if ( element.getAttribute("fridayFlag") != null &&
                             element.getAttribute("fridayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.FRIDAY));
                        }
                        if ( element.getAttribute("saturdayFlag") != null &&
                             element.getAttribute("saturdayFlag").equals("N") )
                        {
                            noShipDays.add(new Integer(Calendar.SATURDAY));
                        }

                        deliveryDateParm.setNoShipDays(noShipDays);
                    }

                    deliveryDateParm.setGlobalParms(oeGlobalParms);
                    deliveryDateParm.setHolidayDates(holidayMap);
                    deliveryDateParm.setZipCodeGNADDFlag(element.getAttribute("zipGnaddFlag"));
                    deliveryDateParm.setZipCodeGotoFloristFlag(element.getAttribute("zipGotoFloristFlag"));
                    deliveryDateParm.setZipTimeZone(element.getAttribute("timeZone"));

                    // zip code does support Sunday delivery
                    if ( element.getAttribute("zipSundayFlag") != null &&
                         element.getAttribute("zipSundayFlag").equals("Y") )
                    {
// CLF - codified product code 
                        // codified products follow different rules under normal processing
                        if ( element.getAttribute("codifiedProduct") != null &&
                             element.getAttribute("codifiedProduct").equals("Y") &&
                             oeGlobalParms.getGNADDLevel().equals("0") )
                        {
                            // Valid valued are: N-codified; NA-not codified/not available
                            //                   Y-codified (No Sunday); S-codified (Sunday)
                            if ( element.getAttribute("codifiedAvailable") != null &&
                                 element.getAttribute("codifiedAvailable").equals("S") )
                            {
                                deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                            }
                            else
                            {
                                deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                            }
                        }
//
                    }
                    // zip code does NOT support Sunday delivery
                    else
                    {
                       deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                    }

                    // Determine if there is a special fee assoc with state and product type
                    order.setSpecialFeeFlag("N");
                    if ( (order.getSendToCustomer().getState() != null) &&
                         (order.getSendToCustomer().getState().equals("AK") ||
                          order.getSendToCustomer().getState().equals("HI")) )
                    {
                        if ( (element.getAttribute("productType") != null) &&
                             (element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                              element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                              element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) )
                        {
                            element.setAttribute("displaySpecialFee", "Y");
                            order.setSpecialFeeFlag("Y");
                        }
                    }

                    String codifiedAvailable = element.getAttribute("codifiedAvailable");
                    String codifiedProduct = element.getAttribute("codifiedProduct");
                    String codifiedSpecial = element.getAttribute("codifiedSpecial");
                    String codifiedDeliverable = element.getAttribute("codifiedDeliverable");

                    // set codified flags in delParms
                    deliveryDateParm.setCodifiedProduct(codifiedProduct);
                    deliveryDateParm.setCodifiedAvailable(codifiedAvailable);
                    
                    // Set unavail flag if the status is U or its an unavailable codified product
                    if ( // product is unavaiable if status is "U"
                         (element.getAttribute("status") != null &&
                          element.getAttribute("status").equals("U")) ||

                        // product is unavaiable if it is a regular codified product
                        // and it is set as unavailable for this zip code in the
                        // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                         (codifiedAvailable != null &&
                          codifiedAvailable.equals(GeneralConstants.NO) &&
                          !deliveryDateParm.isShipMethodCarrier()) ||

                        // product is unavaiable if it is a regular codified product 
                        // and GNADD is at level 3
                         (codifiedProduct != null &&
                          codifiedProduct.equals(GeneralConstants.YES) &&
                          oeGlobalParms.getGNADDLevel().equals("3"))
                       )
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.YES);
                    }
                    else
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.NO);
                    }

                    // Special Codified products should be deliverable if GNADD is on.
                    if (element.getAttribute("status") != null &&
                        !element.getAttribute("status").equals("U") &&
                        codifiedSpecial != null &&
                        codifiedSpecial.equals(GeneralConstants.YES) &&
                        codifiedDeliverable != null &&
                        (!codifiedDeliverable.equals("N") &&
                         !codifiedDeliverable.equals("D"))
                       )
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.NO);
                    }

                    // This method will check all the same day gift rules and
                    // set the availability flags on the order
                    checkForSDGAvailable(order, element);

                    if(loadDeliveryDates)
                    {
                        // Set date range parameters from the temp deliveryDateRangeParms
                        // object
                        deliveryDateParm.setDeliveryRangeFlag(deliveryDateRangeParms.isDeliveryRangeFlag());
                        deliveryDateParm.setDeliveryRangeFrom(deliveryDateRangeParms.getDeliveryRangeFrom());
                        deliveryDateParm.setDeliveryRangeTo(deliveryDateRangeParms.getDeliveryRangeTo());
                        deliveryDateParm.setDeliveryRangeText(deliveryDateRangeParms.getDeliveryRangeText());
                    }

//  **************************************************************************
//    **************************************************************************
//            This ends the setup section and starts the processing section.
//    **************************************************************************
//    **************************************************************************  

                    if(loadDeliveryDates) {
                        // retreive first available delivery date information for product list
                        if ( isList )
                        {
                            OEDeliveryDate firstAvailableDate = null;
                            firstAvailableDate = deliveryDateUtil.getFirstAvailableDate(deliveryDateParm);

                            // Determine which carrier to show (florist or fed ex)
                            for (int ii = 0; ii < firstAvailableDate.getShippingMethods().size(); ii++) 
                            {
                                ShippingMethod shipMethod = (ShippingMethod)firstAvailableDate.getShippingMethods().get(ii);
                                if(shipMethod.getCode().equals(GeneralConstants.DELIVERY_FLORIST_CODE))
                                {                                        
                                    element.setAttribute("firstDeliveryMethod", "florist");

                                    // If this is SDG and a florist can deliver then remove all other shipping methods
                                    if(element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                                       element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC))
                                    {
                                        element.setAttribute("deliveryType1", "");
                                        element.setAttribute("deliveryType2", "");
                                        element.setAttribute("deliveryType3", "");
                                        element.setAttribute("deliveryType4", "");
                                        element.setAttribute("deliveryType5", "");
                                    }
                                    break;
                                }
                                else
                                {
                                    element.setAttribute("firstDeliveryMethod", "carrier");
                                }
                            }
                            
                            element.setAttribute("deliveryDate", firstAvailableDate.getDisplayDate());
                        }
                        // retrieve delivery date information for selected product
                        else
                        {
                            ArrayList dateList = deliveryDateUtil.getOrderProductDeliveryDates(deliveryDateParm);

                            deliveryDatesListElement = xmlDocument.createElement("deliveryDates");
                            Element deliveryDateElement = null;
                            boolean firstDate = true;

                            for(int z = 0; z < dateList.size(); z++) {
                                deliveryDateElement = xmlDocument.createElement("deliveryDate");

                                OEDeliveryDate deliveryDate = (OEDeliveryDate) dateList.get(z);
                                if ( deliveryDate.getDeliverableFlag().equals("Y") )
                                {
                                    if (firstDate) {
                                        element.setAttribute("deliveryDate", deliveryDate.getDeliveryDate());
                                        firstDate = false;
                                    }

                                    deliveryDateElement.setAttribute("date", deliveryDate.getDeliveryDate());
                                    deliveryDateElement.setAttribute("dayOfWeek", deliveryDate.getDayOfWeek());

                                    // Loop through the delivery methods and concat them
                                    StringBuffer sbTypes = new StringBuffer();
                                    ShippingMethod shippingMethod = null;
                                    for (int d = 0; d < deliveryDate.getShippingMethods().size(); d++)
                                    {
                                        shippingMethod = (ShippingMethod)deliveryDate.getShippingMethods().get(d);
                                        sbTypes.append(shippingMethod.getCode()).append(":");
                                        shipMethodsMap.put(shippingMethod.getCode(), shippingMethod.getDeliveryCharge());
                                    }
                                    deliveryDateElement.setAttribute("types", sbTypes.toString());

                                    deliveryDateElement.setAttribute("displayDate", deliveryDate.getDisplayDate());
                                    deliveryDatesListElement.appendChild(deliveryDateElement);
                                }
                            }

                            if ( element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                                 element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) )
                            {
                                if ( shipMethodsMap.size() > 0 )
                                {
                                    shipMethodElement = xmlDocument.createElement("shippingMethod");

                                    Collection methodCollection = shipMethodsMap.keySet();
                                    Iterator iter = methodCollection.iterator();

                                    while ( iter.hasNext() )
                                    {
                                        String description = (String) iter.next();
                                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(description);

                                        Element entry = (Element) xmlDocument.createElement("method");
                                        entry.setAttribute("description", description);
                                        entry.setAttribute("cost", cost.toString());

                                        shipMethodElement.appendChild(entry);
                                    }
                                }
                            }
                        }
                    }

                    // Convert prices to XX.XX format
                    String price = null;
                    price = element.getAttribute("standardPrice");                  
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("standardPrice", price);
                    }

                    price = element.getAttribute("deluxePrice");                 
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("deluxePrice", price);
                    }
                    price = element.getAttribute("premiumPrice");                    
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("premiumPrice", price);
                    }

                    // Calculate discounts for product
                    standardDiscountAmount = element.getAttribute("standardDiscountAmt");
                    deluxeDiscountAmount = element.getAttribute("deluxeDiscountAmt");
                    premiumDiscountAmount = element.getAttribute("premiumDiscountAmt");

                    if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                        element.setAttribute("standardRewardValue", standardDiscountAmount);
                    }

                    if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                        element.setAttribute("deluxeRewardValue", deluxeDiscountAmount);
                    }

                    if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                        element.setAttribute("premiumRewardValue", premiumDiscountAmount);
                    }

                    // Determine if its a partner or standard discount
                    if(order.getPricingCode() != null && order.getPricingCode().equals(GeneralConstants.PARTNER_PRICE_CODE) && order.getPartnerId() != null && !order.getPartnerId().equals("")) {

                        if(element.getAttribute("standardPrice") != null && !element.getAttribute("standardPrice").equals("")) {
                            currentPrice = new BigDecimal(element.getAttribute("standardPrice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("standardRewardValue", retrievePromotionValue(order.getPromotionList(), currentPrice));
                        }
                        if(element.getAttribute("deluxePrice") != null && !element.getAttribute("deluxePrice").equals("")) {
                            currentPrice =  new BigDecimal(element.getAttribute("deluxePrice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("deluxeRewardValue", retrievePromotionValue(order.getPromotionList(), currentPrice));
                        }
                        if(element.getAttribute("premiumPrice") != null && !element.getAttribute("premiumPrice").equals("")) {
                            currentPrice = new BigDecimal(element.getAttribute("premiumPrice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("premiumRewardValue", retrievePromotionValue(order.getPromotionList(), currentPrice));
                        }
                    }

                    else {

                        if(order.getRewardType() != null)
                        {
                            // Discount type of dollars off
                            if(order.getRewardType().equals("Dollars")) {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardPrice"));
                                    currentPrice = currentPrice - Double.parseDouble(standardDiscountAmount);
                                    element.setAttribute("standardDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxePrice"));
                                    currentPrice = currentPrice - Double.parseDouble(deluxeDiscountAmount);
                                    element.setAttribute("deluxeDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumPrice"));
                                    currentPrice = currentPrice - Double.parseDouble((premiumDiscountAmount));
                                    element.setAttribute("premiumDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }

                            // Discount type of percent off
                            else if(order.getRewardType().equals("Percent")) {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardPrice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(standardDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("standardDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxePrice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(deluxeDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("deluxeDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumPrice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(premiumDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("premiumDiscountPrice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }
                        }
                    }

                    productsElement.appendChild(xmlDocument.importNode(element, true));

                    // Do only if product detail (only one cycle in loop)
                    if ( !isList && loadDeliveryDates )
                    {
                        rootElement.appendChild(deliveryDatesListElement);

                        XMLDocument deliveryDateDocument = new XMLDocument();
                        Element deliveryDateElement = deliveryDateDocument.createElement("deliveryDateList");
                        deliveryDateElement.appendChild(deliveryDateDocument.importNode(deliveryDatesListElement, true));
                        deliveryDateDocument.appendChild(deliveryDateElement);
                    }

                    // get state id for later processing
                    stateId = element.getAttribute("stateId");
                    productType = element.getAttribute("productType");
                } // end of For...Loop

                // only output for Product List
                Element shipMethods = xmlDocument.createElement("shippingMethods");

                // If zip code state is Alaska (AK) or Hawaii (HI) output valid
                // delivery types for state for presentation layer to use
                if  ( (stateId != null) &&
                      (stateId.equals("AK") ||
                       stateId.equals("HI")) )
                {
                    // if 2-day delivery is not one of the ship methods, then do not show any methods
                    // and set the product as undeliverable
                    if ( isList ||
                         shipMethodsMap.containsKey(GeneralConstants.DELIVERY_TWO_DAY) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(GeneralConstants.DELIVERY_TWO_DAY);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = xmlDocument.createElement("shippingMethod");
                        Element entry = (Element) xmlDocument.createElement("method");
                        entry.setAttribute("description", GeneralConstants.DELIVERY_TWO_DAY);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if ( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(GeneralConstants.DELIVERY_STANDARD);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = xmlDocument.createElement("shippingMethod");
                        Element entry = (Element) xmlDocument.createElement("method");
                        entry.setAttribute("description", GeneralConstants.DELIVERY_STANDARD);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if(productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))
                    // If the state is AK or HI and this is a freshcut then
                    // delivery is not available unless to day is available
                    {
                        order.setDisplayProductUnavailable(GeneralConstants.YES);
                    }
                }
                // product list, show all delivery methods
                else if ( isList )
                {
                    // retrieve the Global Parameter information
                    xpath = "productList/shippingMethods/data";
                    //q = new XPathQuery();
                    nl = q.query(productList, xpath);

                    if ( nl.getLength() > 0 )
                    {
                        shipMethodElement = xmlDocument.createElement("shippingMethod");

                        for (int z = 0; z < nl.getLength(); z++)
                        {
                            element = (Element) nl.item(z);

                            Element entry = (Element) xmlDocument.createElement("method");
                            entry.setAttribute("description", element.getAttribute("description"));
                            entry.setAttribute("cost", element.getAttribute("cost"));
                            shipMethodElement.appendChild(entry);
                        }
                    }
                }
                // product detail / delivery information, only show available delivery methods
                if ( shipMethodElement != null )
                {
                    shipMethods.appendChild(shipMethodElement);
                }
                rootElement.appendChild(shipMethods);
            }
            else 
            {
                lm.info("No results were returned to page through");
            }
        }
        catch(Exception e) 
        {
            lm.error(e.toString(), e);
        }

        return rootElement;
    }
*/
    private boolean isAvailableForShipping(int currentDayOfWeek, Element productElement) {

        String availableFlag = "N";

        switch (currentDayOfWeek)
        {
            case 1:     // sunday
                availableFlag = productElement.getAttribute("sundayFlag");
                break;
            case 2:     // monday
                availableFlag = productElement.getAttribute("mondayFlag");
                break;
            case 3:     // tuesday
                availableFlag = productElement.getAttribute("tuesdayFlag");
                break;
            case 4:     // wednesday
                availableFlag = productElement.getAttribute("wednesdayFlag");
                break;
            case 5:     // thursday
                availableFlag = productElement.getAttribute("thursdayFlag");
                break;
            case 6:     // friday
                availableFlag = productElement.getAttribute("fridayFlag");
                break;
            case 7:     // saturday
                availableFlag = productElement.getAttribute("saturdayFlag");
                break;
        }

        if(availableFlag.equals("Y"))
            return true;
        else
            return false;
    }

    public Element convertLookupAnchors(XMLDocument sourceLookUpXML) {

        NodeList nl = null;
        String anchor = null;
        LinkedList anchorList = new LinkedList();
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = rootElement = xmlDocument.createElement("root");

        try {
            String xpath = "sourceCodeLookup/searchResults/searchResult";
            XPathQuery q = new XPathQuery();
            nl = q.query(sourceLookUpXML, xpath);

            if (nl.getLength() > 0)
            {
                Element element = null;

                for(int i = 0; i < nl.getLength(); i++) {
                    element = (Element) nl.item(i);
                    anchor = element.getAttribute("anchor");

                    if(!anchorList.contains(anchor)) {
                        anchorList.add(anchor);
                    }
                }

                Collections.sort(anchorList);

                Element anchorListElement = xmlDocument.createElement("anchors");
                rootElement.appendChild(anchorListElement);
                Element anchorElement = null;

                Iterator anchorIterator = anchorList.iterator();
                while(anchorIterator.hasNext()) {
                    anchorElement = xmlDocument.createElement("anchor");
                    anchorElement.setAttribute("value", (String) anchorIterator.next());
                    anchorListElement.appendChild(anchorElement);
                }
            }
        }
        catch(Exception e) {
            lm.error(e.toString(), e);
        }

        return rootElement;
    }

    public String retrievePromotionValue(List promotionList, double price) {
        PromotionPO partnerPromotion = null;
        String promotionValue = null;

        try {
            Iterator promotionIterator = promotionList.iterator();
            while(promotionIterator.hasNext()) {
                partnerPromotion = (PromotionPO) promotionIterator.next();
                if(price >= partnerPromotion.getLowPrice().doubleValue() && price < partnerPromotion.getHighPrice().doubleValue()) {
                    if(partnerPromotion.getPointDriver().equals("DOL")) {
                        promotionValue = new BigDecimal((partnerPromotion.getVariablePoints().doubleValue() * price) + partnerPromotion.getBasePoints().longValue()).toString();
                    }
                    else if(partnerPromotion.getPointDriver().equals("ITM")) {
                        promotionValue = new BigDecimal((partnerPromotion.getVariablePoints().doubleValue()) + partnerPromotion.getBasePoints().longValue()).toString();
                    }
                    else if(partnerPromotion.getPointDriver().equals("PER")) {
                        promotionValue = new BigDecimal((partnerPromotion.getVariablePoints().doubleValue() * price)).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();
                    }

                    break;
                }
            }
        }
        catch(Exception e){
            lm.error("Missing data to calculate correct partner promotion.");
        }

        return promotionValue;
    }

    public String getDescriptionForCode(String code, int searchType) throws Exception
    {
        String ret = null;

        switch(searchType)
        {
            case GET_OCCASION_DESCRIPTION:
                ret = getOccasionDescription(code);
                break;
            case GET_COUNTRY_DESCRIPTION:
                ret = getCountryDescription(code);
                break;
            default:
                break;
        }

        return ret;
    }

    private String getOccasionDescription(String occasionId) throws Exception
    {
        String ret = "";
        FTDDataRequest dataRequest = new FTDDataRequest();
        GenericDataService service = null;
        FTDDataResponse dataResponse = null;
        FTDDataAccessManager dam = new FTDDataAccessManager();

        dataRequest.addArgument("occasionId",  occasionId);

        service =  dam.getGenericDataService(DataConstants.SEARCH_GET_OCCASION_DESC);
        dataResponse = service.executeRequest(dataRequest);

        String xpath = "occassionLookup/searchResults/searchResult";
        XPathQuery q = new XPathQuery();
        NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

        if (nl.getLength() > 0)
        {
            Element occasionData = (Element) nl.item(0);
            ret = occasionData.getAttribute("description");
        }

        return ret;
    }

    public HashMap getProductShippingMethods(String productId, String standardPrice, String shippingKeyId) throws Exception
    {
        HashMap ret = new HashMap();
        FTDDataRequest dataRequest = new FTDDataRequest();
        GenericDataService service = null;
        FTDDataResponse dataResponse = null;
        FTDDataAccessManager dam = new FTDDataAccessManager();

        dataRequest.addArgument("productId",  productId);
        dataRequest.addArgument("standardPrice",  standardPrice);
        dataRequest.addArgument("shippingKeyId",  shippingKeyId);

        service =  dam.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_SHIPPING_METHODS);
        dataResponse = service.executeRequest(dataRequest);

        String xpath = "productShippingMethods/shipMethods/shipMethod";
        XPathQuery q = new XPathQuery();
        NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

        Element shipMethodElement = null;
        ShippingMethod shipMethod = null;
        for(int i = 0; i < nl.getLength(); i++)
        {
            shipMethod = new ShippingMethod();
            shipMethodElement = (Element) nl.item(i);
            shipMethod.setCode(shipMethodElement.getAttribute("shipMethodCode"));
            shipMethod.setDescription(shipMethodElement.getAttribute("shipMethodDesc"));
            shipMethod.setDeliveryCharge(new BigDecimal(shipMethodElement.getAttribute("shipCost")));

            ret.put(shipMethod.getDescription(), shipMethod);
        }

        return ret;
    }

    private String getCountryDescription(String code) throws Exception
    {
        String ret = "";
        FTDDataRequest dataRequest = new FTDDataRequest();
        GenericDataService service = null;
        FTDDataResponse dataResponse = null;

        dataRequest.addArgument("code",  code);

        FTDDataAccessManager dam = new FTDDataAccessManager();
        service =  dam.getGenericDataService(DataConstants.SEARCH_GET_COUNTRY_DESC);

        dataResponse = service.executeRequest(dataRequest);

        String xpath = "countryLookup/searchResults/searchResult";
        XPathQuery q = new XPathQuery();
        NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

        if (nl.getLength() > 0)
        {
            Element occasionData = (Element) nl.item(0);
            ret = occasionData.getAttribute("countryName");
        }

        return ret;
    }

    public static void rebuildSearchResultsIndex(XMLDocument searchResultsIndex,
                                               XMLDocument searchResultsPerPage,
                                               int currentPage,
                                               int pageSize) throws Exception{

      // assumes that the index is pre-paginated
      NodeList nodesOnIndex =
          searchResultsIndex.selectNodes("//product[@page='"+ currentPage +"']");
      int nodeCountOnIndex = nodesOnIndex.getLength();

      // assumes the search results have not been paginated
      NodeList nodesOnsearchResultsPerPage =
          searchResultsPerPage.selectNodes("//product");
      int nodeCountOnsearchResultsPerPage = nodesOnsearchResultsPerPage.getLength();

      // check to see if any products have been made unavailable
        if (nodeCountOnIndex > nodeCountOnsearchResultsPerPage)  
        {
            // remove nodes from the search results index that are missing in the search results
            Element nodeOnIndex = null;
            String productID = null;
            Element productsNodeOnsearchResultsPerPage = (Element)(searchResultsPerPage.selectNodes("//products").item(0));
            NodeList productNodeOnsearchResultsPerPageNL = null;
            Element productNodeOnSearchResultsPerPage = null;
            Element importedNode = null;

            for (int i = 0; i < nodeCountOnIndex; i++)  
            {
                nodeOnIndex = (Element) nodesOnIndex.item(i);

                if (nodeOnIndex != null)  
                {
                    // get the product id from the index
                    productID = nodeOnIndex.getAttribute("productID");

                    // check to see if the product id exists on search results
                    productNodeOnsearchResultsPerPageNL =
                        searchResultsPerPage.selectNodes("//product[@productID='"+ productID +"']");

                
                    // if the product id on the index cannot be found on the search results
                    if (productNodeOnsearchResultsPerPageNL.getLength() == 0)  
                    {
                        nodeOnIndex.getParentNode().removeChild(nodeOnIndex);
                    }
                  // mark the product as unavailable from the search results index products node
//                  nodeOnIndex.setAttribute("status", "unavailable");
//                  // add the missing nodes on the search results, with the status of unavailable
//                  if (productsNodeOnsearchResultsPerPage != null)  {
//                      importedNode = (Element)searchResultsPerPage.importNode(nodeOnIndex, true);
//                      productsNodeOnsearchResultsPerPage.appendChild(importedNode);
//                  }
//              }
//              else {
//                  productNodeOnSearchResultsPerPage = (Element) productNodeOnsearchResultsPerPageNL.item(0);
//                  productNodeOnSearchResultsPerPage.setAttribute("status", "available");
//              }
                }
            }
        }

      // insert the pagination node on the search results DOM
      //paginate(searchResultsPerPage, currentPage, pageSize);
        Element pageData = (Element) searchResultsIndex.selectNodes("//pageData").item(0);
        if (pageData != null)  {
            Element importedNode = (Element) searchResultsPerPage.importNode(pageData, true);
            searchResultsPerPage.getDocumentElement().appendChild(importedNode);
        }
        else {
            System.out.println("Element 'pageData' not found");
        }
    }


  /**
   *
   * @return total page count
   */
  public static void paginate(XMLDocument doc,  int currentPage, int pageSize) throws Exception{

      NodeList nodeList =   doc.selectNodes("//product");
      int page = 1, totalPages = 1; // start at 1
      int counter = 0;

      Element node = null;

      for (int i = 0; i < nodeList.getLength(); i++,counter++)  {
          // control pagination
          if (counter >= pageSize)  {
              page++; // increment the page number
              totalPages = page;
              counter = 0; // reset counter
          }

          node = (Element) nodeList.item(i);
          // rewrite the page attribute
          node.setAttribute("page", String.valueOf(page));
          // set the status attribute to available
          node.setAttribute("status", "available");

      }

      // check if the current page is greater that the total page count
      // reset it to the last page
      if (currentPage > totalPages)  {
          currentPage = totalPages;
      }

      // set up pagination nodes on the search results
      Element pageData = (Element) doc.selectNodes("//pageData").item(0);
      Element data = null;

      if (pageData == null)  {
          pageData = doc.createElement("pageData");
          // total pages
          data = (Element) doc.createElement("data");
          data.setAttribute("name", "totalPages" );
          data.setAttribute("value", String.valueOf(totalPages) );
          pageData.appendChild(data);

          // current page
          data = (Element) doc.createElement("data");
          data.setAttribute("name", "currentPage" );
          data.setAttribute("value", String.valueOf(currentPage) );
          pageData.appendChild(data);

          // append the page data node to the root node
          doc.getDocumentElement().appendChild(pageData);
      }
      else {
          // total pages
          data = (Element) doc.selectNodes("//pageData/data[@name='totalPages']").item(0);
          data.setAttribute("value", String.valueOf(totalPages) );
          // current page
          data = (Element) doc.selectNodes("//pageData/data[@name='currentPage']").item(0);
          data.setAttribute("value", String.valueOf(currentPage) );
      }
  }

    public XMLDocument getProductsByIDs(XMLDocument searchResultsIndex, String strCurrentPage, FTDArguments arguments, OrderPO order ) throws Exception
    {
        //Get the list of products for the current page\
        NodeList nodeList=null;
        String strProductId;
        String strPage;

        Element node = null;


        nodeList=searchResultsIndex.selectNodes("//product");
        StringBuffer sb = new StringBuffer();
        HashMap productOrderMap = new HashMap();
        String productOrder = null;
        String productId = null;
        for (int i = 0; i < nodeList.getLength(); i++ )
        {
            // control pagination
            node = (Element) nodeList.item(i);
            strProductId = node.getAttribute("novatorID");
            productId = node.getAttribute("productID");
            strPage = node.getAttribute("page");
            productOrder = node.getAttribute("displayOrder");
            productOrderMap.put(productId, productOrder);

            if( strProductId!=null && strPage!=null && strPage.compareTo(strCurrentPage)==0 )
            {
                sb.append("'");
                sb.append(strProductId);
                sb.append("'");
                sb.append(",");
            }
        }

        if(sb.length() > 0)
        {
            sb.deleteCharAt(sb.length()-1);
        }
        else
        {
            sb.append("''");
        }

        //Get the product detail for the list of products
        FTDDataRequest dataRequest = new FTDDataRequest();
        dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID_LIST, sb.toString());
        String pricePointId = (String)arguments.get("pricePointId");
        if(pricePointId != null && pricePointId.length() == 0)
        {
            pricePointId = null;
        }
        dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, pricePointId);
        dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());

        if ( order.getSendToCustomer().isDomesticFlag() )
        {
            dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            dataRequest.addArgument(ArgumentConstants.SEARCH_IN_ZIP_CODE, order.getSendToCustomer().getZipCode());
        }
        else
        {
            dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            dataRequest.addArgument(ArgumentConstants.SEARCH_IN_ZIP_CODE, null);
        }



        OEParameters oeParms = getGlobalParms();
        order.setGlobalParameters(oeParms);
        Calendar deliveryEndDate = Calendar.getInstance();
        deliveryEndDate.add(Calendar.DATE, DeliveryDateUTIL.getDeliveryDaysOut(order, oeParms));// oeParms.getDeliveryDaysOut());
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_END, dateFormat.format(deliveryEndDate.getTime()));
        dataRequest.addArgument(ArgumentConstants.SEARCH_IN_COUNTRY_ID, order.getSendToCustomer().getCountry());
        dataRequest.addArgument(ArgumentConstants.SEARCH_SCRIPT_CODE, order.getScriptCode());

        Date deliveryDate = order.getDeliveryDate();
        if (deliveryDate == null) {
            dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
        } else {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
        }

        FTDDataAccessManager dam = new FTDDataAccessManager();
        GenericDataService service =  dam.getGenericDataService(DataConstants.SEARCH_GET_PRODUCTS_BY_ID);
        FTDDataResponse dataResponse = service.executeRequest(dataRequest);
        XMLDocument searchResults = (XMLDocument)dataResponse.getDataVO().getData();

        nodeList=searchResults.selectNodes("//product");
        Element element = null;
        productId = null;
        for(int i = 0; i < nodeList.getLength(); i++)
        {
            element = (Element)nodeList.item(i);
            productId = element.getAttribute("productID");
            element.setAttribute("displayOrder", (String)productOrderMap.get(productId));
        }

        return searchResults;
    }

    public OEParameters getGlobalParms()
    {
        OEParameters oeGlobalParms = new OEParameters();
        FTDDataRequest dataRequest = new FTDDataRequest();

        try
        {
            // retrieve the Global Parameter information
            FTDDataAccessManager dam = new FTDDataAccessManager();
            GenericDataService service =  dam.getGenericDataService(DataConstants.SHOPPING_GET_GLOBAL_PARMS);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            Element globalData = null;
            String xpath = "globalParmsDataSet/globalParmsData/data";

            XPathQuery q = new XPathQuery();
            NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");

                globalData = (Element) nl.item(0);

                //oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(globalData.getAttribute("deliveryDaysOut")));
                oeGlobalParms.setDeliveryDaysOutMAX(Integer.parseInt(globalData.getAttribute("deliveryDaysOutMAX")));
                oeGlobalParms.setDeliveryDaysOutMIN(Integer.parseInt(globalData.getAttribute("deliveryDaysOutMIN")));
                oeGlobalParms.setAllowSubstitution(globalData.getAttribute("allowSubstitution"));
                oeGlobalParms.setExoticCutoff(globalData.getAttribute("exoticCutoff"));
                oeGlobalParms.setFreshCutCutoff(globalData.getAttribute("freshcutsCutoff"));
                oeGlobalParms.setFridayCutoff(globalData.getAttribute("fridayCutoff"));
                oeGlobalParms.setGNADDDate(sdfInput.parse(globalData.getAttribute("gnaddDate")));
                oeGlobalParms.setGNADDLevel(globalData.getAttribute("gnaddLevel"));
                oeGlobalParms.setIntlAddOnDays(Integer.parseInt(globalData.getAttribute("intlOverrideDays")));
                oeGlobalParms.setIntlCutoff(globalData.getAttribute("internationalCutoff"));
                oeGlobalParms.setMondayCutoff(globalData.getAttribute("mondayCutoff"));
                oeGlobalParms.setSaturdayCutoff(globalData.getAttribute("saturdayCutoff"));
                oeGlobalParms.setSpecialtyGiftCutoff(globalData.getAttribute("specialtyGiftCutoff"));
                oeGlobalParms.setSundayCutoff(globalData.getAttribute("sundayCutoff"));
                oeGlobalParms.setThursdayCutoff(globalData.getAttribute("thursdayCutoff"));
                oeGlobalParms.setTuesdayCutoff(globalData.getAttribute("tuesdayCutoff"));
                oeGlobalParms.setWednesdayCutoff(globalData.getAttribute("wednesdayCutoff"));
                oeGlobalParms.setLatestCutoff(globalData.getAttribute("latestCutoff"));
                oeGlobalParms.setFreshCutSrvcChargeTrigger(globalData.getAttribute("freshCutsSvcChargeTrigger"));
                oeGlobalParms.setDispatchOrderFlag(globalData.getAttribute("dispatchOrderFlag"));
                if(globalData.getAttribute("freshCutsSvcCharge") != null && !globalData.getAttribute("freshCutsSvcCharge").equals("")) {
                    oeGlobalParms.setFreshCutSrvcCharge(new BigDecimal(globalData.getAttribute("freshCutsSvcCharge")));
                }
                if(globalData.getAttribute("specialSvcCharge") != null && !globalData.getAttribute("specialSvcCharge").equals("")) {
                    oeGlobalParms.setSpecialSrvcCharge(new BigDecimal(globalData.getAttribute("specialSvcCharge")));
                }
                if(globalData.getAttribute("freshCutsSatCharge") != null && !globalData.getAttribute("freshCutsSatCharge").equals("")) {
                    oeGlobalParms.setFreshCutSatCharge(new BigDecimal(globalData.getAttribute("freshCutsSatCharge")));
                }
                if(globalData.getAttribute("fuelSurchargeAmt") != null && !globalData.getAttribute("fuelSurchargeAmt").equals("")) {
                    oeGlobalParms.setFuelSurchargeAmt(new BigDecimal(globalData.getAttribute("fuelSurchargeAmt")));
                }

                oeGlobalParms.setDefaulted(false);
            }
        }
        catch (Exception e)
        {
            lm.error(e.toString(), e);
        }

        return oeGlobalParms;
    }

    public XMLDocument sortProductList(String sortType, XMLDocument xml) {
        XMLDocument result = null;

        try {

            ResourceManager rm = ResourceManager.getInstance();
            String xslLocation = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "xsl.location");

            TransformerFactory factory = TransformerFactory.newInstance();
            String sortSheet = xslLocation + sortType + ".xsl";

            Templates pss = factory.newTemplates(new StreamSource(new File(sortSheet)));
            Transformer transformer = pss.newTransformer();

            StringWriter sortOutput = new StringWriter();
            transformer.transform(new DOMSource(xml), new StreamResult(sortOutput));
            result = XPathQuery.createDocument(sortOutput.toString());

            //XMLDocument outDoc = new XMLDocument();
            //transformer.transform(new DOMSource(xml), new StreamResult(outDoc.getSystemId()));
            //result = outDoc;
        }
        catch (Exception e) {
            lm.error(e.toString(), e);
        }

        return result;
    }

    /**
     * Performs a product list search with the paramers passed
     * @param arguments Contains any arguments that should be used int he search
     * @param queryName Contains the data constant referring to a search procedure 
     * @exception Exception if a SQL Exception occures
     * @author Jeff Penney
     *
     */
    public static XMLDocument preformProductListSearch(Map arguments, String queryName) throws Exception
    {
        XMLDocument ret = null;

        FTDDataRequest dataRequest = new FTDDataRequest();

        // All searches
        dataRequest.addArgument(ArgumentConstants.SEARCH_INDEX_ID, (String)arguments.get(ArgumentConstants.SEARCH_INDEX_ID));
        dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, (String)arguments.get(ArgumentConstants.SEARCH_SOURCE_CODE));
        dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, (String)arguments.get(ArgumentConstants.SEARCH_ZIP_CODE));
        dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String)arguments.get(ArgumentConstants.SEARCH_PRICE_POINT_ID));
        dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, (String)arguments.get(ArgumentConstants.SEARCH_COUNTRY_ID));
        dataRequest.addArgument("scriptCode", (String)arguments.get("scriptCode"));
        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_END_DATE, (String)arguments.get(ArgumentConstants.SEARCH_DELIVERY_END_DATE));
        dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, (String)arguments.get(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG));
        dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_DATE, (String)arguments.get(ArgumentConstants.SEARCH_DELIVERY_DATE));
        
        // Advanced Search
        dataRequest.addArgument("inOccasionId", (String)arguments.get("inOccasionId"));
        dataRequest.addArgument("inCategory", (String)arguments.get("inCategory"));
        dataRequest.addArgument("inRecipient", (String)arguments.get("inRecipient"));
        dataRequest.addArgument("inColor", (String)arguments.get("inColor"));
        dataRequest.addArgument("inName", (String)arguments.get("inName"));

        // Keyword Search
        dataRequest.addArgument("searchKeywordList", (String)arguments.get("searchKeywordList"));

        FTDDataAccessManager dam = new FTDDataAccessManager();
        GenericDataService service = dam.getGenericDataService(queryName);
        FTDDataResponse dataResponse = service.executeRequest(dataRequest);

        ret = (XMLDocument)dataResponse.getDataVO().getData();

        return ret;
    }

    /**
     * This method tokenizes a Map into a String
     * @param inMap A Map of search criteria that will be converted to a String
     * @author Jeff Penney
     *
     */
    public static String getSearchCriteriaString(Map inMap)
    {
        StringBuffer ret = new StringBuffer();

        Set keySet = inMap.keySet();
        Iterator it = keySet.iterator();
        String key = null;
        String value = null;
        while(it.hasNext())
        {
            key = (String)it.next();
            value = (String)inMap.get(key);
            if(value == null)
            {
                value = "null";
            }

            ret.append(key).append(":").append(value).append(" ");
        }

        return ret.toString();
    }

    /**
     * This method tokenizes a String into a Map
     * @param inString A String of search criteria that will be converted to a Map
     * @author Jeff Penney
     *
     */
    public static Map getSearchCriteriaMap(String inString)
    {
        Map ret = new HashMap();
        StringTokenizer tokenizer1 = new StringTokenizer(inString, " ");
        String token = null;
        StringTokenizer tokenizer2 = null;
        String key = null;
        String value = null;
        
        while(tokenizer1.hasMoreTokens())
        {
            token = tokenizer1.nextToken();

            tokenizer2 = new StringTokenizer(token, ":");
            key = tokenizer2.nextToken();
            value = tokenizer2.nextToken();
            if(value.equals("null"))
            {
                value = null;
            }
            ret.put(key, value);
        }
        return ret;
    }
    
    public void checkForSDGAvailable(OEDeliveryDateParm delParms, List shipMethods)
    {
        String codifiedAvailable = delParms.getCodifiedAvailable();
        if(codifiedAvailable == null) codifiedAvailable = "";
        String codifiedProduct = delParms.getCodifiedProduct();
        if(codifiedProduct == null) codifiedProduct = "";
        String codifiedSpecial = delParms.getCodifiedSpecialFlag();
        if(codifiedSpecial == null) codifiedSpecial = "";
        String codifiedDeliverable = delParms.getCodifiedDeliverable();
        if(codifiedDeliverable == null) codifiedDeliverable = "";
        
        if(codifiedProduct.equals(GeneralConstants.YES) && 
           (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
            delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))            
        {
            // if the delivery country is Canada, the Virgin Islands or
            // Puerto Rico and thier is no codified florist then set the
            // displayProductUnavailable flag
            if(codifiedAvailable.equals(GeneralConstants.NO) &&
               (delParms.getOrder().getSendToCustomer().getCountry().equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
               delParms.getOrder().getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
               delParms.getOrder().getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
               codifiedDeliverable.equals("N"))
            {
                delParms.getOrder().setDisplayProductUnavailable(GeneralConstants.YES);
                delParms.getOrder().setDisplayNoProduct(GeneralConstants.NO);
            }
            // if the delivery country is Canada, the Virgin Islands or
            // Puerto Rico and thier is no florist coverage then set the
            // displayNoProduct flag            
            else if(codifiedAvailable.equals(GeneralConstants.NO) &&
                    (delParms.getOrder().getSendToCustomer().getCountry().equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                     delParms.getOrder().getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                     delParms.getOrder().getSendToCustomer().getState().equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
                     codifiedDeliverable.equals("D"))
            {
                delParms.getOrder().setDisplayNoProduct(GeneralConstants.YES);
                delParms.getOrder().setDisplayProductUnavailable(GeneralConstants.NO);
            }
            // This case is when the product only has two day delivery (ex. AK, HI)
            else if(codifiedAvailable.equals(GeneralConstants.NO) && shipMethods.size() == 1 && 
                    shipMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE))
            {
                delParms.getOrder().setDisplayCodifiedFloristHasTwoDayDeliveryOnly(GeneralConstants.YES);
            }            
            // if the zip code can still take orders but just not for this product
            else if(codifiedAvailable.equals(GeneralConstants.NO) &&
                    codifiedDeliverable.equals("N") && shipMethods.size() > 0)
            {
                delParms.getOrder().setDisplayNoCodifiedFloristHasCommonCarrier(GeneralConstants.YES);
                delParms.getOrder().setDisplayNoProduct(GeneralConstants.NO);
            }
            // if the zip code is shut down
            else if(codifiedAvailable.equals(GeneralConstants.NO) &&
                    codifiedDeliverable.equals("D") && shipMethods.size() > 0)
            {
                delParms.getOrder().setDisplayNoFloristHasCommonCarrier(GeneralConstants.YES);
                delParms.getOrder().setDisplayNoProduct(GeneralConstants.NO);
            }
            // No florist and no carrier delivery methods
            else if(codifiedAvailable.equals(GeneralConstants.NO) && shipMethods.size() == 0)
            {
                delParms.getOrder().setDisplayProductUnavailable(GeneralConstants.YES);
            }
            else
            {
                delParms.getOrder().setDisplayNoProduct("N");
                delParms.getOrder().setDisplayNoCodifiedFloristHasCommonCarrier("N");
                delParms.getOrder().setDisplayNoFloristHasCommonCarrier("N");
            }
        }
    }

    /**
     * This is an overloaded method which will pass a null value for the product sub code. 
     * @param document The XML document containing product information
     * @param productId The product ID that the delivery dates will be calculated for 
     * @param order The current order object 
     * @param getFirstAvailableDate Determines if all delivery dates should be calculated or just the first available

     * @return A List of Delivery date objects
     * @author Ali Lakhani
     *
     */
    public List getItemDeliveryDates(XMLDocument document, String productId,  OrderPO order, boolean getFirstAvailableDate)
    {
      return getItemDeliveryDates(document, productId, null, order, getFirstAvailableDate);
    }

    /**
     * This method get the Delivery dates for a line item in an order
     * @param document The XML document containing product information
     * @param productId The product ID that the delivery dates will be calculated for 
     * @param order The current order object 
     * @param getFirstAvailableDate Determines if all delivery dates should be calculated or just the first available

     * @return A List of Delivery date objects
     * @author Jeff Penney
     *
     */
    public List getItemDeliveryDates(XMLDocument document, String productId,  
                            String productSubCodeId, OrderPO order, boolean getFirstAvailableDate)
    {
        List deliveryDates = null;
        try
        {
            SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(lm);
            SearchUTIL searchUTIL = new SearchUTIL(lm);
            OEDeliveryDateParm deliveryDateParm = new OEDeliveryDateParm();
            Map holidayMap = null;
            String zipCode = order.getSendToCustomer().getZipCode();
            String zipSundayFlag = null;

            order.setDisplayFloral(GeneralConstants.NO);
            order.setDisplayNoCodifiedFloristHasCommonCarrier(GeneralConstants.NO);
            order.setDisplayNoFloristHasCommonCarrier(GeneralConstants.NO);
            order.setDisplayNoProduct(GeneralConstants.NO);
            order.setDisplayProductUnavailable(GeneralConstants.NO);
            order.setDisplaySpecialtyGift(GeneralConstants.NO);
            order.setDisplayCodifiedFloristHasTwoDayDeliveryOnly(GeneralConstants.NO);

            // parse global parameters from the returned XML
            OEParameters oeGlobalParms = getGlobalParms();

            // retrieve the holiday date information for the country
            holidayMap = getHolidays(document);
            
            // retrieve the product information
            String xpath = "productList/products/product[@productID = '" + productId + "']";

            XPathQuery q = new XPathQuery();
            NodeList nl = q.query(document, xpath);
            boolean floralServiceChargeSet = false;
            if (nl.getLength() > 0)
            {
                double currentPrice;
                double currentDiscountAmount;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;
                String cntryAddOnDays = null;

                Element element = (Element) nl.item(0);

                deliveryDateParm.setOrder(order);
                deliveryDateParm.setGlobalParms(oeGlobalParms);
                deliveryDateParm.setDeliverToday(Boolean.TRUE);
                deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                deliveryDateParm.setProductType(element.getAttribute("productType"));
                deliveryDateParm.setProductSubType(element.getAttribute("productSubType"));
                deliveryDateParm.setProductId(element.getAttribute("productID"));
                deliveryDateParm.setProductSubCodeId(productSubCodeId);

                deliveryDateParm.setMondayDeliveryFreshCuts(FTDUtil.convertStringToBoolean(element.getAttribute("mondayDelFCFlag")));
                deliveryDateParm.setTwoDayShipFreshCuts(FTDUtil.convertStringToBoolean(element.getAttribute("twoDaySatFCFlag")));
                cntryAddOnDays = element.getAttribute("addonDays");

                if ( (cntryAddOnDays != null) &&
                     (!cntryAddOnDays.trim().equals("")) )
                {
                    deliveryDateParm.setCntryAddOnDays(Integer.parseInt(cntryAddOnDays));
                }
                else
                {
                    deliveryDateParm.setCntryAddOnDays(0);
                }

                // Use zip code information if exists
                if ( zipCode != null && !zipCode.equals("") )
                {
                    deliveryDateParm.setZipTimeZone(element.getAttribute("timeZone"));
                    deliveryDateParm.setZipCodeGNADDFlag(element.getAttribute("zipGnaddFlag").trim());
                    deliveryDateParm.setZipCodeFloralFlag(element.getAttribute("zipFloralFlag").trim());
                    deliveryDateParm.setZipCodeGotoFloristFlag(element.getAttribute("zipGotoFloristFlag").trim());
                    zipSundayFlag = element.getAttribute("zipSundayFlag").trim();
                }
                // zip code information does not exist, use default values
                else {
                    deliveryDateParm.setZipTimeZone(GeneralConstants.OE_TIMEZONE_DEFAULT);
                    deliveryDateParm.setZipCodeGNADDFlag(GeneralConstants.NO);
                    deliveryDateParm.setZipCodeGotoFloristFlag(GeneralConstants.YES);
                    zipSundayFlag = GeneralConstants.YES;
                }

                // zip code does support Sunday delivery
                if ( zipSundayFlag != null &&
                     zipSundayFlag.equals(GeneralConstants.YES) )
                {
                    // codified products follow different rules under normal processing
                    if ( element.getAttribute("codifiedProduct") != null &&
                         element.getAttribute("codifiedProduct").equals(GeneralConstants.YES) &&
                         oeGlobalParms.getGNADDLevel().equals("0") )
                    {
                        // Valid valued are: N-codified; NA-not codified/not available
                        //                   Y-codified (No Sunday); S-codified (Sunday)
                        if ( element.getAttribute("codifiedAvailable") != null &&
                             element.getAttribute("codifiedAvailable").equals("S") )
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                        }
                        else
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                        }
                    }
                }
                // zip code does NOT support Sunday delivery
                else
                {
                   deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                }

                // Exotic floral items cannot be delivered same day
                if ( element.getAttribute("productSubType").equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC) ||
                     element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                     element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                     element.getAttribute("deliveryType").equals(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL) )
                {
                    deliveryDateParm.setDeliverToday(Boolean.FALSE);
                }

                // set the exception dates for product
                deliveryDateParm.setExceptionCode(element.getAttribute("exceptionCode"));
                try {
                    deliveryDateParm.setExceptionFrom(sdfInput.parse(element.getAttribute("exceptionStartDate")));
                    deliveryDateParm.setExceptionTo(sdfInput.parse(element.getAttribute("exceptionEndDate")));
                }
                catch (Exception e) {
                }

                // set the vendor no delivery dates for product
                HashMap vendorNoDeliverFrom = new HashMap();
                HashMap vendorNoDeliverTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendorDelivBlockStart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                    {
                        vendorNoDeliverFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorDelivBlockStart" + y)));
                        vendorNoDeliverTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorDelivBlockEnd" + y)));
                    }
                }

                if ( vendorNoDeliverFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoDeliverFrom(vendorNoDeliverFrom);
                    deliveryDateParm.setVendorNoDeliverTo(vendorNoDeliverTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.FALSE);
                }

                // set the vendor no ship dates for product
                HashMap vendorNoShipFrom = new HashMap();
                HashMap vendorNoShipTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendorShipBlockStart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                    {
                        vendorNoShipFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorShipBlockStart" + y)));
                        vendorNoShipTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorShipBlockEnd" + y)));
                    }
                }

                if ( vendorNoShipFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoShipFrom(vendorNoShipFrom);
                    deliveryDateParm.setVendorNoShipTo(vendorNoShipTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.FALSE);
                }

                Date firstArrivalDate = null;

                // set Florist delivery information
                if ( element.getAttribute("shipMethodFlorist") != null &&
                     element.getAttribute("shipMethodFlorist").equals(GeneralConstants.YES) )
                {
                    deliveryDateParm.setShipMethodFlorist(true);

                    if ( element.getAttribute("serviceCharge") != null &&
                          !element.getAttribute("serviceCharge").trim().equals("") )
                    {   
                            floralServiceChargeSet = true;
                            deliveryDateParm.setFloralServiceCharge(new BigDecimal(element.getAttribute("serviceCharge")));
                    }
                }

                // Check delivery type for carrier ship methods
                if ( element.getAttribute("shipMethodCarrier") != null &&
                     element.getAttribute("shipMethodCarrier").equals(GeneralConstants.YES) )
                {
                    // Only set Sunday Flag to false if this is not a same day gift product
                    if(!deliveryDateParm.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) &&
                       !deliveryDateParm.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC))
                    {
                        deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                    }
                    deliveryDateParm.setShipMethodCarrier(true);
                    // Get the shipping methods for this product
                    HashMap shippingMethods = searchUTIL.getProductShippingMethods(element.getAttribute("productID"), element.getAttribute("standardPrice"), element.getAttribute("shippingKey"));
                    deliveryDateParm.setShipMethods(shippingMethods);

                    // build values of No Ship Days for product
                    HashSet noShipDays = new HashSet();
                    if ( element.getAttribute("sundayFlag") != null &&
                         element.getAttribute("sundayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.SUNDAY));
                    }
                    if ( element.getAttribute("mondayFlag") != null &&
                         element.getAttribute("mondayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.MONDAY));
                    }
                    if ( element.getAttribute("tuesdayFlag") != null &&
                         element.getAttribute("tuesdayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.TUESDAY));
                    }
                    if ( element.getAttribute("wednesdayFlag") != null &&
                         element.getAttribute("wednesdayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.WEDNESDAY));
                    }
                    if ( element.getAttribute("thursdayFlag") != null &&
                         element.getAttribute("thursdayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.THURSDAY));
                    }
                    if ( element.getAttribute("fridayFlag") != null &&
                         element.getAttribute("fridayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.FRIDAY));
                    }
                    if ( element.getAttribute("saturdayFlag") != null &&
                         element.getAttribute("saturdayFlag").equals(GeneralConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.SATURDAY));
                    }
                    deliveryDateParm.setNoShipDays(noShipDays);

                    // build values of Delivery Exclusion Days for product
                    HashSet excludedDeliveryDays = new HashSet();
                    if ( element.getAttribute("exSundayFlag") != null &&
                         element.getAttribute("exSundayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.SUNDAY));
                    }
                    if ( element.getAttribute("exMondayFlag") != null &&
                         element.getAttribute("exMondayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.MONDAY));
                    }
                    if ( element.getAttribute("exTuesdayFlag") != null &&
                         element.getAttribute("exTuesdayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.TUESDAY));
                    }
                    if ( element.getAttribute("exWednesdayFlag") != null &&
                         element.getAttribute("exWednesdayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.WEDNESDAY));
                    }
                    if ( element.getAttribute("exThursdayFlag") != null &&
                         element.getAttribute("exThursdayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.THURSDAY));
                    }
                    if ( element.getAttribute("exFridayFlag") != null &&
                         element.getAttribute("exFridayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.FRIDAY));
                    }
                    if ( element.getAttribute("exSaturdayFlag") != null &&
                         element.getAttribute("exSaturdayFlag").equals(GeneralConstants.YES) )
                    {
                        excludedDeliveryDays.add(new Integer(Calendar.SATURDAY));
                    }
                    deliveryDateParm.setExcludedDeliveryDays(excludedDeliveryDays);
                }

                // if the product is not available or is codified and not available in the zip code
                // set flag on order for later processing
                String codifiedAvailable = element.getAttribute("codifiedAvailable");
                String codifiedSpecialFlag = element.getAttribute("codifiedSpecial");
                String deliverableFlag = element.getAttribute("codifiedDeliverable");
                String codifiedProduct = element.getAttribute("codifiedProduct");
                String status = element.getAttribute("status");

                // set codified flags in delParms
                deliveryDateParm.setCodifiedProduct(codifiedProduct);
                deliveryDateParm.setCodifiedAvailable(codifiedAvailable);
                deliveryDateParm.setCodifiedSpecialFlag(codifiedSpecialFlag);
                deliveryDateParm.setCodifiedDeliverable(deliverableFlag);

                if (
                    // product is unavaiable if status is "U"
                    (status != null &&
                      status.equals("U")) ||

                    // product is unavaiable if it is a regular codified product
                    // and it is set as unavailable for this zip code in the
                    // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                     (codifiedAvailable != null &&
                      codifiedAvailable.equals(GeneralConstants.NO) &&
                      (codifiedSpecialFlag != null &&
                       codifiedSpecialFlag.equals(GeneralConstants.NO)) &&
                       !deliveryDateParm.isShipMethodCarrier()) ||

                    // product is unavaiable if it is a regular codified product 
                    // and GNADD is at level 3
                     (codifiedProduct != null &&
                      codifiedProduct.equals(GeneralConstants.YES) &&
                      codifiedSpecialFlag != null &&
                      codifiedSpecialFlag.equals(GeneralConstants.NO) &&
                      oeGlobalParms.getGNADDLevel().equals("3"))
                    )
                {
                    order.setDisplayProductUnavailable(GeneralConstants.YES);
                }
                else
                {
                    order.setDisplayProductUnavailable(GeneralConstants.NO);
                }


                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                if (order.getSendToCustomer().isDomesticFlag() && 
                    ( (deliveryDateParm.getZipCodeGNADDFlag().equals("Y") &&
                       deliveryDateParm.getGlobalParms().getGNADDLevel().equals("0")) ||
                     deliveryDateParm.getZipCodeFloralFlag().equals("N") )
                     )
                {
                    // only display if has not been displayed before
                    if ( deliveryDateParm.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        order.setDisplaySpecialtyGift("Y");
                    }
                }
                
            }

            if(!floralServiceChargeSet)
            {
                if(order.getSendToCustomer().isDomesticFlag())
                {
                    deliveryDateParm.setFloralServiceCharge(order.getDomesticServiceFee());
                }
                else
                {
                    deliveryDateParm.setFloralServiceCharge(order.getInternationalServiceFee());
                }
            }
            deliveryDateParm.setOrder(order);
            deliveryDateParm.setGlobalParms(oeGlobalParms);
            deliveryDateParm.setHolidayDates((HashMap)holidayMap);

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter object
            deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_PRODUCT_LIST, document, deliveryDateParm);

            if(getFirstAvailableDate)
            {
                deliveryDates = new ArrayList();
                deliveryDates.add(deliveryDateUtil.getFirstAvailableDate(deliveryDateParm));
            }
            else
            {
                deliveryDates = deliveryDateUtil.getOrderProductDeliveryDates(deliveryDateParm);
            }

            // This method will check all the same day gift rules and
            // set the availability flags on the order
            List shipMethods = getDeliveryMethodsFromDateList(deliveryDates);
            checkForSDGAvailable(deliveryDateParm, shipMethods);            
        }
        catch(Exception e)
        {
            lm.error(e.toString(), e);
        }

        return deliveryDates;
    }

    public List getDeliveryMethodsFromDateList(List dates)
    {
        boolean foundFlorist = false;
        boolean foundGround = false;
        boolean foundTwoDay = false;
        boolean foundNextDay = false;
        boolean foundSaturday = false;
        List shippingMethods = new ArrayList();
        ShippingMethod floristShipping = new ShippingMethod();
        floristShipping.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
        ShippingMethod standardShipping = new ShippingMethod();
        standardShipping.setCode(GeneralConstants.DELIVERY_STANDARD_CODE);
        ShippingMethod twoDayShipping = new ShippingMethod();
        twoDayShipping.setCode(GeneralConstants.DELIVERY_TWO_DAY_CODE);
        ShippingMethod nextDayShipping = new ShippingMethod();
        nextDayShipping.setCode(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
        ShippingMethod saturdayShipping = new ShippingMethod();
        saturdayShipping.setCode(GeneralConstants.DELIVERY_SATURDAY_CODE);
        
        for (int i = 0; i < dates.size(); i++) 
        {
            OEDeliveryDate date = (OEDeliveryDate)dates.get(i);
            if(date.getShippingMethods().contains(floristShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_FLORIST_CODE))
            {
                foundFlorist = true;
                shippingMethods.add(GeneralConstants.DELIVERY_FLORIST_CODE);
            }
            else if(date.getShippingMethods().contains(standardShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_STANDARD_CODE))
            {
                foundGround = true;
                shippingMethods.add(GeneralConstants.DELIVERY_STANDARD_CODE);
            }
            else if(date.getShippingMethods().contains(twoDayShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE))
            {
                foundTwoDay = true;
                shippingMethods.add(GeneralConstants.DELIVERY_TWO_DAY_CODE);
            }
            else if(date.getShippingMethods().contains(nextDayShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
            {
                foundNextDay = true;
                shippingMethods.add(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
            }
            else if(date.getShippingMethods().contains(saturdayShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_SATURDAY_CODE))
            {
                foundSaturday = true;
                shippingMethods.add(GeneralConstants.DELIVERY_SATURDAY_CODE);
            }
            
            if(foundFlorist && foundGround && foundTwoDay && foundNextDay &&
               foundSaturday)
            {
                break;
            }
        }
        return shippingMethods;
    } 
}

