package com.ftd.applications.oe.services.utilities;

import java.net.*;
import java.io.*;

public class KeywordProxyThread  extends Thread
{
    private Socket socket = null;
    private int timeout;
    private int debugLevel;

    public KeywordProxyThread(Socket socket, int timeoutIn, int debug)
    {
		super("KeywordProxyThread");
		this.socket = socket;
		timeout = timeoutIn;
        debugLevel = debug;
    }

    public void run()
    {
        String inputLine = "";
        String novatorIp = "10.5.0.60";
        int port = 1035;
		try
		{
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			String outputLine = "";

			socket.setSoTimeout(timeout);

            inputLine = in.readLine();

            System.out.println("Request from OE recieved...\r\n" + inputLine);

			SocketClient sc = new SocketClient(null,novatorIp, port);
			sc.setDebug(true);
            System.out.println("Opening socket to Novator " + novatorIp + " " + port);
			sc.open();
            System.out.println("Sending XML to Novator");
			outputLine = sc.send(inputLine);
            System.out.println("Recieved products:");
            System.out.println(outputLine);
			sc.close();

            System.out.println("Returned products to internal host");
			out.println(outputLine);

			out.close();
			in.close();
			socket.close();
            System.out.println("Ending thread...");
		}
		catch (IOException e)
		{
	    	e.printStackTrace();
		}
    }
}