package com.ftd.applications.oe.services.utilities;

import java.io.*;
import java.net.*;

public class KeywordProxy extends Thread
{
	private ServerSocket serverSocket;
	public boolean listening;
	private int port;
	private int timeout;
    private int debugLevel;

	public KeywordProxy(int listenPort, int waitTimeout, int level)
	{
        serverSocket = null;
	    listening = true;
	    port = listenPort;
	    timeout = waitTimeout;
        debugLevel = level;
	}

	public void run()
	{
		try
		{
			serverSocket = new ServerSocket(port);
		}
		catch (IOException e)
		{
			System.err.println("Could not listen on port: " + port + ".");
			System.exit(-1);
		}

		while (listening)
		{
			try
			{
                System.out.println("Waiting for a connection...");
				new KeywordProxyThread(serverSocket.accept(), timeout, debugLevel).start();
                if(debugLevel > 0)
                {
                    System.out.println("Connection accepted...");
                }
			}
			catch(IOException ioe)
			{
				ioe.printStackTrace();
			}
		}

		try
		{
			System.out.println("Closing the ServerSocket");
			serverSocket.close();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
        int port = 7777;
        int level = 1;

        if(args.length > 0)
        {
            port = Integer.parseInt(args[0]);
            level = Integer.parseInt(args[1]);
        }
		KeywordProxy listener = new KeywordProxy(port, 60000, level);
		listener.start();
	}
}