package com.ftd.applications.oe.services.utilities;

import com.ftd.applications.oe.common.DomainObjectConverter;
import com.ftd.applications.oe.common.FieldUtils;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.applications.oe.common.ShipMethodSortDescription;
import com.ftd.applications.oe.common.ShippingMethod;
import com.ftd.applications.oe.common.persistent.AddOnPO;
import com.ftd.applications.oe.common.persistent.CustomerPO;
import com.ftd.applications.oe.common.persistent.ItemPO;
import com.ftd.applications.oe.common.persistent.OrderPO;
import com.ftd.framework.common.utilities.LogManager;
import com.ftd.framework.common.utilities.XPathQuery;
import com.ftd.framework.dataaccessservices.DataManager;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


public class ShoppingUTIL extends SuperUTIL
{
    public ShoppingUTIL(LogManager lm)
    {
        super(lm);
    }

    public Element transformCartToXML(OrderPO order) {
        XMLDocument xmlDocument = new XMLDocument();
        Element cartElement = xmlDocument.createElement("SHOPPING_CART");

        try {
            double itemTotalPrice;
            
            order.setTotalOrderPrice(new BigDecimal(0));
            order.setTotalRewardAmount(new Long(0));
            order.setTaxAmount(new BigDecimal(0));

            SearchUTIL searchUTIL = null;
            ItemPO currentItem = null;

            Element orderElement = null;
            Element itemElement = null;
            Element generalElement = null;
            Element deliveryElement = null;
            Element addOnElement = null;

            orderElement = xmlDocument.createElement("ORDER");
            orderElement.setAttribute("lastOrderAction", order.getLastOrderAction());
            orderElement.setAttribute("customerName", order.getBillToCustomer().getFirstName() + " " + order.getBillToCustomer().getLastName());
            orderElement.setAttribute("sourceCode", order.getSourceCode());
            orderElement.setAttribute("sourceCodeDescription", order.getSourceCodeDescription());
            cartElement.appendChild(orderElement);
            
            Collection itemCollection = order.getAllItems().values();
            Iterator itemIterator = itemCollection.iterator();
            while(itemIterator.hasNext()) 
            {
                itemTotalPrice = 0;
            
                try {
                    currentItem = (ItemPO) itemIterator.next();

                    // Build Item Node
                    itemElement = xmlDocument.createElement("ITEM");
                    itemElement.setAttribute("name", currentItem.getNovatorName());
                    String shipCarrier = currentItem.getShippingCarrier();

                    if(shipCarrier != null && shipCarrier.equalsIgnoreCase("FLORIST")) {                              
                        itemElement.setAttribute("shipMethodFlorist", "Y");
                    }
                    else {                
                        itemElement.setAttribute("shipMethodFlorist", "N");
                    }
                    
                    if(shipCarrier != null && shipCarrier.equalsIgnoreCase("COMMON_CARRIER")) {                  
                        itemElement.setAttribute("shipMethodCarrier", "Y");
                    }
                    else {               
                        itemElement.setAttribute("shipMethodCarrier", "N");
                    }

                    itemElement.setAttribute("number", currentItem.getProductId());
                    itemElement.setAttribute("novatorId", currentItem.getNovatorId());
                    itemElement.setAttribute("sku", currentItem.getItemSKU());

                    if(currentItem.isSubCodeSelected()) {
                        itemElement.setAttribute("subCodeId", currentItem.getSubCodeId());
                        itemElement.setAttribute("subCodeDescription", currentItem.getSubCodeDescription());
                    }

                    try {
                        itemElement.setAttribute("cart_item_number", currentItem.getCartNumber().toString());
                        if(currentItem.isCustomItem()) {
                            itemElement.setAttribute("custom", "Y");
                        }
                        else {
                            itemElement.setAttribute("custom", "N");
                        }
                    }
                    catch(Exception e) {
                        lm.error("Cart item #" + currentItem.getProductId() + " is missing detail data");
                    }

                    // Build Detail Node
                    generalElement = xmlDocument.createElement("DETAIL");
                    generalElement.setAttribute("color1", currentItem.getColorOne());
                    generalElement.setAttribute("color2", currentItem.getColorTwo());
                    itemElement.appendChild(generalElement);

                    // Build Delivery Node
                    deliveryElement = xmlDocument.createElement("DELIVERY");
                    deliveryElement.setAttribute("delivery_date", FieldUtils.formatUtilDateToString(currentItem.getDeliveryDate()));
                    String deliveryDateDisplay = currentItem.getDeliveryDateDisplay();
                    // the 'greater then 10 thing' here means 'we have a delivery date range'
                    // in that case we cannot get a correct 'display date with day of the week string'
                    // since the delivery date itself does not reflect the range (which is a display only thing)
                    if (deliveryDateDisplay != null && deliveryDateDisplay.length() > 10) 
                    {
                      deliveryElement.setAttribute("deliveryDateDisplay", deliveryDateDisplay);  
                    } 
                    else
                    {
                      deliveryElement.setAttribute("deliveryDateDisplay", FieldUtils.formatUtilDateToStringWithDOW(currentItem.getDeliveryDate()));
                    }

                    // Temp hardcoding of ship to types
                    String shipToType = null;
                    if(currentItem.getInstitutionType() != null) {
                        if(currentItem.getInstitutionType().equals("R"))
                            shipToType = "Residential";
                        else if(currentItem.getInstitutionType().equals("B"))
                            shipToType = "Business";
                        else if(currentItem.getInstitutionType().equals("F"))
                            shipToType = "Funeral Home";
                        else if(currentItem.getInstitutionType().equals("H"))
                            shipToType = "Hospital";
                        else if(currentItem.getInstitutionType().equals("N"))
                            shipToType = "Nursing Home";
                        else if(currentItem.getInstitutionType().equals("O"))
                            shipToType = "Other";
                    }
                
                    deliveryElement.setAttribute("location_type", shipToType);
                    deliveryElement.setAttribute("location_name", currentItem.getInstitutionName());

                    if(currentItem.getShippingMethod() != null 
                        && !currentItem.getShippingMethod().equals("") 
                        && !currentItem.getShippingMethod().equals("florist")) {

                        if(currentItem.getProductType() != null && 
                          (currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                          currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) ) {
                            deliveryElement.setAttribute("shipping_type", GeneralConstants.DELIVERY_SERVICE_CHARGE);
                        }
                        else {
                            deliveryElement.setAttribute("shipping_type", currentItem.getShippingMethod());
                        }
                    }
                    else {
                        deliveryElement.setAttribute("shipping_type", GeneralConstants.DELIVERY_SERVICE_CHARGE);
                    }

                    if(currentItem.getShippingCost() != null) {
                        deliveryElement.setAttribute("shipping_amount", currentItem.getShippingCost().toString());

                        if(currentItem.getProductType() != null && currentItem.getSendToCustomer().getState() != null) {
                            if((currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                                currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                                currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) &&
                                !currentItem.getShippingMethodId().equals(GeneralConstants.DELIVERY_SAME_DAY_CODE) &&
                               (currentItem.getSendToCustomer().getState().equals("AK") || currentItem.getSendToCustomer().getState().equals("HI"))) {

                                currentItem.setExtraShippingFee(order.getGlobalParameters().getSpecialSrvcCharge());
                                deliveryElement.setAttribute("shipping_amount", currentItem.getShippingCost().add(currentItem.getExtraShippingFee()).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                itemTotalPrice += currentItem.getExtraShippingFee().doubleValue();
                            }
                            else if(currentItem.getProductType() != null && 
                                (currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)||
                                 currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) ) 
                            {
                                deliveryElement.setAttribute("shipping_amount", currentItem.getShippingCost().add(currentItem.getExtraShippingFee()).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                itemTotalPrice += currentItem.getExtraShippingFee().doubleValue();
                            }
                        }
                        
                        
                    }
                    
                    deliveryElement.setAttribute("hours_from", currentItem.getShipTimeFrom());
                    deliveryElement.setAttribute("hours_to", currentItem.getShipTimeTo());
                    deliveryElement.setAttribute("deliveryLocationExtra", currentItem.getInstitutionInfo());

                    // Build Recipient Node
                    generalElement = xmlDocument.createElement("RECIPIENT");
                    generalElement.setAttribute("firstName", currentItem.getSendToCustomer().getFirstName());
                    generalElement.setAttribute("lastName", currentItem.getSendToCustomer().getLastName());
                    generalElement.setAttribute("customerId", currentItem.getSendToCustomer().getCustomerId());
                    generalElement.setAttribute("institutionName", currentItem.getInstitutionName());
                    generalElement.setAttribute("institutionInfo", currentItem.getInstitutionInfo());
                    generalElement.setAttribute("institutionType", currentItem.getInstitutionType());


                    if(currentItem.getSendToCustomer().getQmsOverrideFlag() != null && currentItem.getSendToCustomer().getQmsOverrideFlag().equals("N")) {        
                        generalElement.setAttribute("address1", currentItem.getSendToCustomer().getQmsAddressOne());
                        generalElement.setAttribute("address2", currentItem.getSendToCustomer().getQmsAddressTwo());
                        generalElement.setAttribute("city", currentItem.getSendToCustomer().getQmsCity());
                        generalElement.setAttribute("state", currentItem.getSendToCustomer().getQmsState());
                        generalElement.setAttribute("zip", currentItem.getSendToCustomer().getQmsZipCode());
                    }
                    else {
                        generalElement.setAttribute("address1", currentItem.getSendToCustomer().getAddressOne());
                        generalElement.setAttribute("address2", currentItem.getSendToCustomer().getAddressTwo());
                        generalElement.setAttribute("city", currentItem.getSendToCustomer().getCity());
                        generalElement.setAttribute("state", currentItem.getSendToCustomer().getState());
                        generalElement.setAttribute("zip", currentItem.getSendToCustomer().getZipCode());
                    }               
   
                    generalElement.setAttribute("country", currentItem.getSendToCustomer().getCountry());
                    generalElement.setAttribute("countryName", new SearchUTIL(lm).getDescriptionForCode(currentItem.getSendToCustomer().getCountry(), SearchUTIL.GET_COUNTRY_DESCRIPTION));
                    generalElement.setAttribute("countryType", currentItem.getSendToCustomer().getCountryType());
                    if(currentItem.getSendToCustomer().getHomePhone() != null) {
                        generalElement.setAttribute("phone", FieldUtils.formatUSPhone(currentItem.getSendToCustomer().getHomePhone().toString()));
                    }
                    deliveryElement.appendChild(generalElement);
                    itemElement.appendChild(deliveryElement);

                    // Build Add On Node
                    addOnElement = xmlDocument.createElement("ADD_ONS");

                    double addOnTotalPrice = 0;
                    AddOnPO addOn = null;
                    BigDecimal addOnPrice = null;
                    List addOnList = currentItem.getAddOnList();
                    if(addOnList != null && !addOnList.isEmpty()) {
                        Iterator addOnIterator = addOnList.iterator();

                        // Generate all add ons for item
                        while(addOnIterator.hasNext()) {
                            try {
                                generalElement = xmlDocument.createElement("ADD_ON");
                                addOn = (AddOnPO) addOnIterator.next();

                                addOnPrice = addOn.getAddOnPrice().multiply(new BigDecimal(addOn.getAddOnQty().toString()));

                                generalElement.setAttribute("type", addOn.getAddOnType());
                                generalElement.setAttribute("price", addOnPrice.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                generalElement.setAttribute("quantity", addOn.getAddOnQty().toString());
                                generalElement.setAttribute("description", addOn.getAddOnDescription());

                                // Add total add on price to item total price
                                addOnTotalPrice += addOnPrice.doubleValue(); 
                            
                                addOnElement.appendChild(generalElement);
                            }
                            catch(Exception e) {
                                lm.error("Problems occured while building add-ons for item #" + currentItem.getProductId() + " in shopping cart");
                            }
                        }
                    }

                    itemElement.appendChild(addOnElement);
                    
                    // Calculate detail prices
                    try {
                        itemTotalPrice += currentItem.getDiscountedPrice().doubleValue();

                        // Add the addon price to the item total
                        currentItem.setAddOnTotalPrice(new BigDecimal(addOnTotalPrice));
                        itemTotalPrice += addOnTotalPrice;

                        // Set the item tax
                        itemTotalPrice += currentItem.getShippingCost().doubleValue(); 
                        currentItem.setTaxAmount(currentItem.getTax().multiply(new BigDecimal(itemTotalPrice)).setScale(2, BigDecimal.ROUND_DOWN));

                        // Add to the total order tax
                        order.setTaxAmount(order.getTaxAmount().add(currentItem.getTaxAmount()));

                        // Add the item tax to the total item price
                        itemTotalPrice += currentItem.getTaxAmount().doubleValue(); 
                    }
                    catch(Exception e) {
                        lm.error("Could not calculate detail prices for item #" + currentItem.getProductId() + " in shopping cart");
                    }

                    if(order.getRewardType().equals("TotSavings")) {
                      searchUTIL = new SearchUTIL(lm);
                      currentItem.setDiscountAmount(new BigDecimal(searchUTIL.retrievePromotionValue(order.getPromotionList(), itemTotalPrice)));
                      currentItem.setDiscountDescription(order.getRewardType());
                    }

                    // Build Payment Node
                    generalElement = xmlDocument.createElement("PAYMENT");
                    generalElement.setAttribute("item_price", currentItem.getDiscountedPrice().toString());
                    generalElement.setAttribute("item_regular_price", currentItem.getRegularPrice().toString());

                    if(currentItem.getDiscountDescription() != null && currentItem.getDiscountAmount() != null) {
                        generalElement.setAttribute("discount_description", currentItem.getDiscountDescription());

                        if(currentItem.getDiscountDescription() != null && currentItem.getDiscountDescription().equals(GeneralConstants.CASH_BACK)) {
                            searchUTIL = new SearchUTIL(lm);
                            generalElement.setAttribute("discount_amount", searchUTIL.retrievePromotionValue(order.getPromotionList(), currentItem.getRegularPrice().doubleValue() + currentItem.getAddOnTotalPrice().doubleValue()));
                        }
                        else {
                            generalElement.setAttribute("discount_amount", currentItem.getDiscountAmount().toString());
                        }

                        if(currentItem.getDiscountDescription().equals(GeneralConstants.MILES) || currentItem.getDiscountDescription().equals(GeneralConstants.POINTS) 
                         || currentItem.getDiscountDescription().equals("TotSavings")) {
                            order.setTotalRewardAmount(new Long(order.getTotalRewardAmount().longValue() + currentItem.getDiscountAmount().longValue()));
                        } 
                    }
                        
                    if(currentItem.getServiceFee() != null)
                    {
                        BigDecimal serviceFee = currentItem.getServiceFee();
                        if( currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                            (currentItem.getShippingCarrier()!=null && 
                             currentItem.getShippingCarrier().equals("COMMON_CARRIER") &&
                             currentItem.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) )
                            serviceFee.add(currentItem.getExtraShippingFee());
                        
                        generalElement.setAttribute("service_fee", serviceFee.toString());
                    }
                    if(currentItem.getTaxAmount() != null)
                        generalElement.setAttribute("tax", currentItem.getTaxAmount().toString());
            
                    itemElement.appendChild(generalElement);

                    // Build Comment Node
                    generalElement = xmlDocument.createElement("COMMENT");
                    generalElement.setAttribute("item", currentItem.getItemComments());
                    generalElement.setAttribute("florist", currentItem.getFloristComments());

                    itemElement.appendChild(generalElement);

                    // Add entire item node to cart structure
                    currentItem.setItemTotalPrice(new BigDecimal(itemTotalPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                    itemElement.setAttribute("total_price", currentItem.getItemTotalPrice().toString());
                    
                    order.setTotalOrderPrice(order.getTotalOrderPrice().add(new BigDecimal(itemTotalPrice)));
                        
                    orderElement.appendChild(itemElement);
                
                }
                catch(Exception e) {
                    lm.error("Problems occured while building item #" + currentItem.getProductId() + " for shopping cart");
                }
            }

            // Set order total
            order.setTotalOrderPrice(order.getTotalOrderPrice().setScale(2, BigDecimal.ROUND_HALF_DOWN));
            orderElement.setAttribute("totalOrderPrice", order.getTotalOrderPrice().toString());
            orderElement.setAttribute("totalItems", Integer.toString(order.getAllItems().size()));
            

        } catch(Exception e) {
                lm.error(e.toString(), e);
        }

        return cartElement;
    }


    public Element transformItemDetailXML(ItemPO currentItem) 
    {
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element itemElement = xmlDocument.createElement("item");
        rootElement.appendChild(itemElement);
  
        try {
            Element generalElement = null;

            // Build Detail Node
            generalElement = xmlDocument.createElement("DETAIL");
            generalElement.setAttribute("price", currentItem.getPriceType());
            if(currentItem.getPriceType() != null && currentItem.getPriceType().equals("variable")) {
                generalElement.setAttribute("variablePrice", currentItem.getRegularPrice().toString());
            }
            generalElement.setAttribute("color1", currentItem.getColorOne());
            generalElement.setAttribute("color2", currentItem.getColorTwo());
            itemElement.appendChild(generalElement);

            if(currentItem.isCustomItem()) {
                generalElement = xmlDocument.createElement("COMMENT");
                generalElement.setAttribute("item", currentItem.getItemComments());
                generalElement.setAttribute("florist", currentItem.getFloristComments());
                itemElement.appendChild(generalElement);
            }

            // Build Add On Node
            Element addOnElement = xmlDocument.createElement("ADD_ONS");
                
            AddOnPO addOn = null;
            List addOnList = currentItem.getAddOnList();
            if(addOnList != null && !addOnList.isEmpty()) {
                Iterator addOnIterator = addOnList.iterator();

                // Generate all add ons for item
                while(addOnIterator.hasNext()) {
                    try {
                        generalElement = xmlDocument.createElement("ADD_ON");
                        addOn = (AddOnPO) addOnIterator.next();

                        generalElement.setAttribute("id", addOn.getAddOnId());
                        generalElement.setAttribute("type", addOn.getAddOnType());
                        generalElement.setAttribute("price", addOn.getAddOnPrice().toString());
                        generalElement.setAttribute("quantity", addOn.getAddOnQty().toString());
                        generalElement.setAttribute("description", addOn.getAddOnDescription());
                        addOnElement.appendChild(generalElement);

                    }
                    catch(Exception e) {
                        lm.error("Problems occured while building add-ons for item #" + currentItem.getProductId() + " in get cart item detail");
                    }
                }
            }
            itemElement.appendChild(addOnElement);

            // Build Delivery Node
            generalElement = xmlDocument.createElement("DELIVERY");
            generalElement.setAttribute("deliveryType", currentItem.getShippingMethod());
            if(currentItem.getDeliveryDate() != null) {
                generalElement.setAttribute("deliveryDate", FieldUtils.formatUtilDateToString(currentItem.getDeliveryDate()));
            }
            itemElement.appendChild(generalElement);
        
        }
        catch(Exception e) {
            lm.error(e.toString(), e);
        }
        
        return rootElement; 
    }
    

    public Element transformRecipientToXML(ItemPO currentItem) 
    {
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element recipientElement = xmlDocument.createElement("recipient");
        rootElement.appendChild(recipientElement);

        // Build Recipient Node
        try {
            recipientElement.setAttribute("firstName", currentItem.getSendToCustomer().getFirstName());
            recipientElement.setAttribute("lastName", currentItem.getSendToCustomer().getLastName());
            recipientElement.setAttribute("customerId", currentItem.getSendToCustomer().getCustomerId());
            recipientElement.setAttribute("institutionName", currentItem.getInstitutionName());
            recipientElement.setAttribute("institutionInfo", currentItem.getInstitutionInfo());
            recipientElement.setAttribute("institutionType", currentItem.getInstitutionType());
            
            if(currentItem.getSendToCustomer().getQmsOverrideFlag() != null && currentItem.getSendToCustomer().getQmsOverrideFlag().equals("N")) {        
                recipientElement.setAttribute("address1", currentItem.getSendToCustomer().getQmsAddressOne());
                recipientElement.setAttribute("address2", currentItem.getSendToCustomer().getQmsAddressTwo());
                recipientElement.setAttribute("city", currentItem.getSendToCustomer().getQmsCity());
                recipientElement.setAttribute("state", currentItem.getSendToCustomer().getQmsState());
                recipientElement.setAttribute("zip", currentItem.getSendToCustomer().getQmsZipCode());
            }
            else {
                recipientElement.setAttribute("address1", currentItem.getSendToCustomer().getAddressOne());
                recipientElement.setAttribute("address2", currentItem.getSendToCustomer().getAddressTwo());
                recipientElement.setAttribute("city", currentItem.getSendToCustomer().getCity());
                recipientElement.setAttribute("state", currentItem.getSendToCustomer().getState());
                recipientElement.setAttribute("zip", currentItem.getSendToCustomer().getZipCode());
            }

            recipientElement.setAttribute("country", currentItem.getSendToCustomer().getCountry());
            recipientElement.setAttribute("countryType", currentItem.getSendToCustomer().getCountryType());
            recipientElement.setAttribute("lastMinuteCheckBox", currentItem.getLastMinuteGiftFlag());
            recipientElement.setAttribute("lastMinuteEmailAddress", currentItem.getLastMinuteGiftEmail());
            recipientElement.setAttribute("phoneExt", currentItem.getSendToCustomer().getPhoneExtension());
            if(currentItem.getSendToCustomer().getHomePhone() != null) {
                recipientElement.setAttribute("phone", FieldUtils.formatUSPhone(currentItem.getSendToCustomer().getHomePhone().toString()));
            }        
        }
        catch(Exception e) {
            lm.error(e.toString(), e);
        }
        
        return rootElement; 
    }

    public Element transformCustomerToXML(OrderPO currentOrder) 
    {
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element customerElement = xmlDocument.createElement("customer");
        rootElement.appendChild(customerElement);

        // Build Customer Node
        try {
            customerElement.setAttribute("firstName", currentOrder.getBillToCustomer().getFirstName());
            customerElement.setAttribute("lastName", currentOrder.getBillToCustomer().getLastName());
            customerElement.setAttribute("address1", currentOrder.getBillToCustomer().getAddressOne());
            customerElement.setAttribute("address2", currentOrder.getBillToCustomer().getAddressTwo());
            customerElement.setAttribute("city", currentOrder.getBillToCustomer().getCity());
            customerElement.setAttribute("state", currentOrder.getBillToCustomer().getState());
            customerElement.setAttribute("country", currentOrder.getBillToCustomer().getCountry());
            customerElement.setAttribute("countryType", currentOrder.getBillToCustomer().getCountryType());
            customerElement.setAttribute("zip", currentOrder.getBillToCustomer().getZipCode());
            if(currentOrder.getBillToCustomer().getHomePhone() != null) {
                customerElement.setAttribute("homePhone", FieldUtils.formatUSPhone(currentOrder.getBillToCustomer().getHomePhone().toString()));
            }
            if(currentOrder.getBillToCustomer().getWorkPhone() != null) {
                customerElement.setAttribute("workPhone", FieldUtils.formatUSPhone(currentOrder.getBillToCustomer().getWorkPhone().toString()));
            }
            customerElement.setAttribute("phoneExt", currentOrder.getBillToCustomer().getPhoneExtension());
            customerElement.setAttribute("business", currentOrder.getBillToCustomer().getCompanyName());
            customerElement.setAttribute("email", currentOrder.getBillToCustomer().getEmailAddress());
            customerElement.setAttribute("contactInfo", currentOrder.getBillToCustomer().getContactInfo());
            customerElement.setAttribute("special", currentOrder.getSpecialPromotionFlag());
            
            customerElement.setAttribute("memberId", currentOrder.getBillToCustomer().getMembershipId());
            customerElement.setAttribute("memberFirstName", currentOrder.getBillToCustomer().getMembershipFirstName());
            customerElement.setAttribute("memberLastName", currentOrder.getBillToCustomer().getMembershipLastName());
            
        }
        catch(Exception e) {
            lm.error(e.toString(), e);
        }
        
        return rootElement; 
    }


    public Element transformBillingInfoToXML(OrderPO currentOrder) 
    {
        SimpleDateFormat sdfInput = null;
        Element generalElement = null;
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element billingElement = xmlDocument.createElement("billing");
        Element priceElement = xmlDocument.createElement("pricing");
        rootElement.appendChild(billingElement);
        rootElement.appendChild(priceElement);

        // Build Billing Node
        try {
            Calendar currentTime = Calendar.getInstance();
        
            billingElement.setAttribute("currentYear", new Integer(currentTime.get(Calendar.YEAR)).toString());
            billingElement.setAttribute("creditType", currentOrder.getCreditCardType());
            billingElement.setAttribute("creditNumber", currentOrder.getCreditCardNumber());
            billingElement.setAttribute("creditExpMonth", currentOrder.getCreditCardExpireMonth());
            billingElement.setAttribute("creditExpYear", currentOrder.getCreditCardExpireYear());
            billingElement.setAttribute("invoiceNumber", currentOrder.getInvoiceNumber());
            billingElement.setAttribute("giftCertId", currentOrder.getGiftCertificateId());
            if(currentOrder.getGiftCertificateAmount() != null) {
                billingElement.setAttribute("giftCertAmount", currentOrder.getGiftCertificateAmount().toString());
            }
            
        }
        catch(Exception e) {
            lm.error(e.toString(), e);
        }
        
        return rootElement; 
    }


    public Element transformDeliveryInfoToXML(ItemPO currentItem) 
    {
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element deliveryElement = xmlDocument.createElement("deliveryDetail");
        rootElement.appendChild(deliveryElement);

        // Build Delivery Detail Node
        try {
            if(currentItem.getDeliveryDate() != null) {
                deliveryElement.setAttribute("requestedDeliveryDate", FieldUtils.formatUtilDateToString(currentItem.getDeliveryDate()));
            }
            String institutionType = null;
            String institutionName = null;
            String institutionInfo = null;
            // put customer information into institution information
            institutionType = currentItem.getSendToCustomer().getCompanyType();
            currentItem.setInstitutionType(institutionType);
            institutionName = currentItem.getSendToCustomer().getCompanyName();
            currentItem.setInstitutionName(institutionName);
            institutionInfo = currentItem.getSendToCustomer().getCompanyInfo();
            currentItem.setInstitutionInfo(institutionInfo);
            deliveryElement.setAttribute("deliveryLocation", currentItem.getInstitutionType());
            deliveryElement.setAttribute("deliveryLocationName", currentItem.getInstitutionName());
            deliveryElement.setAttribute("deliveryLocationExtra", currentItem.getInstitutionInfo());
            deliveryElement.setAttribute("deliveryMethod", currentItem.getShippingMethod());
            deliveryElement.setAttribute("fromWorkHours", currentItem.getShipTimeFrom());
            deliveryElement.setAttribute("toWorkHours", currentItem.getShipTimeTo());
            deliveryElement.setAttribute("giftMessage", currentItem.getCardMessage());
            deliveryElement.setAttribute("signature", currentItem.getCardSignature());
            deliveryElement.setAttribute("releaseSendersName", currentItem.getReleaseSenderNameFlag());
            deliveryElement.setAttribute("orderComments", currentItem.getItemComments());
            deliveryElement.setAttribute("floristComments", currentItem.getFloristComments());
            deliveryElement.setAttribute("floristCode", currentItem.getFloristCode());
        }
        catch(Exception e) {
            lm.error(e.toString(), e);
        }
        
        return rootElement; 
    }
    

    public AddOnPO setAddOnToItem(String addOnId, Integer quantity, XMLDocument addOnList) throws Exception
    {
        AddOnPO addOn = new AddOnPO();
        String xpath = "addOnList/addOns/addOn";
        XPathQuery q = new XPathQuery();

        try {
            NodeList nl = q.query(addOnList, xpath);
            if (nl.getLength() > 0)
            {
                for(int i=0; i < nl.getLength(); i++)
                {            
                    Element addOnElement = (Element) nl.item(i);
                    if(addOnElement.getAttribute("addonId").equals(addOnId)) {
                        try {
                            addOn.setAddOnId(addOnId);
                            addOn.setAddOnType(addOnElement.getAttribute("addonType"));
                            addOn.setAddOnPrice(new BigDecimal(addOnElement.getAttribute("price")).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                            addOn.setAddOnDescription(addOnElement.getAttribute("description"));
                            addOn.setAddOnQty(quantity);

                            break;
                        }
                        catch(Exception e) {
                            throw new Exception("ERROR:  Add on " + addOnId + " has bad data.");
                        }
                    }
                }
            }   
            else {
                lm.error("No addons were found for this product.");
            }
            
        } catch(Exception e) {
            lm.error(e.toString(), e);
        }

        return addOn;
    }


    public Element transformBreadCrumbsToXML(ArrayList crumbList) 
    {
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element crumbListElement = xmlDocument.createElement("breadCrumbList");
        Element crumbElement = null;
        
        rootElement.appendChild(crumbListElement);

        // Build Bread Crumb Node   
        String crumbValue;
        
        for(int i = 0; i < crumbList.size(); i++) {
            try {
                crumbValue = (String) crumbList.get(i);
           
                crumbElement = xmlDocument.createElement("breadCrumb");
                crumbElement.setAttribute("name", crumbValue.substring(0, crumbValue.indexOf("|")));
                crumbElement.setAttribute("value", crumbValue.substring((crumbValue.indexOf("|") + 1), crumbValue.length()));

                crumbListElement.appendChild(crumbElement);
        
            }
            catch(Exception e) {
                // Exception is fine
            }
        }
        
        return rootElement; 
    }
    
    public void cancelOrder(OrderPO order) throws SAXException, SQLException, 
                ParserConfigurationException, IOException, SystemMessengerException
    {
        Connection connection = null;

        try
        {
            // Convert the order
            DomainObjectConverter doc = new DomainObjectConverter();
            OrderVO dOrder = doc.convertToDomain(order);
            lm.info("Order " + dOrder.getGUID()  + " converted...");

            // Get a database connection
            connection = DataManager.getInstance().getConnection();
            connection.setAutoCommit(false);
                        
            // Set order status
            BillingUTIL billingUTIL = new BillingUTIL(lm);
            billingUTIL.setStatusOrderAbandoned(dOrder);
            lm.info("Order " + dOrder.getGUID()  + " status set...");

            // Save the order to FRP
            ScrubMapperDAO dao = new ScrubMapperDAO(connection);
            boolean saved = dao.mapOrderToDB(dOrder);

            // Dispatch the order
            if(saved)
            {                
                connection.commit();
                lm.info("Order " + dOrder.getGUID()  + " saved to Scrub...");                
            }
        }
        catch(Exception e)
        {

            lm.error("Could not save cancelled order to scrub");
            lm.error(e);
            System.out.println("PRINTING STACK TRACE FOR ERROR:");
            e.printStackTrace();
        
            SystemMessengerVO vo = new SystemMessengerVO();
            vo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            vo.setType("SYSTEM");
            vo.setSource("WEBOE");
            String error = "Problem saving cancelled order to scrub " + order.getGUID();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            error = error + " " + sw.toString() ;
            vo.setMessage(error);
            SystemMessenger.getInstance().send(vo,connection);

            if(connection != null)
            {
                connection.rollback();
            }

        }
        finally
        {
            if(connection != null)
            {
                connection.setAutoCommit(true);
                connection.close();
            }
        }
    }

  
 /*******************************************************************************************
  *		formatDeliveryDates(DeliveryDates) - This method is used to format the delivery dates. 
  *********************************************************************************************
  *
  *	Input Format
  *	------------
  *		<shippingData>
  *			<deliveryDates>
  *				<deliveryDate date="09/30/2006" dayOfWeek="Sat" types="FL:SA:" displayDate="09/30/2006" index="index3"/>
  *				<deliveryDate date="10/02/2006" dayOfWeek="Mon" types="FL:GR:2D:ND:" displayDate="10/02/2006" index="index5"/>
  *			</deliveryDates>
  *				<shippingMethods>
  *				<shippingMethod>
  *					<method cost="35.99" description="Saturday Delivery" code="SA"/>
  *					<method cost="9.98" description="Standard Delivery" code="GR"/>
  *					<method cost="20.99" description="Next Day Delivery" code="ND"/>
  *					<method cost="20.99" description="Florist Delivery" code="FL"/>
  *					<method cost="16.99" description="Two Day Delivery" code="2D"/>
  *				</shippingMethod>
  *			</shippingMethods>
  *			</shippingData>
  *
  *
  *	Output Format
  *	-------------
  *		<shippingData>
  *			<shippingMethods>
  *				<shippingMethod>
  *					<method cost="20.99" description="Next Day Delivery" code="ND"/>
  *					<method cost="16.99" description="Two Day Delivery" code="2D"/>
  *					<method cost="9.98" description="Standard Delivery" code="GR"/>
  *					<method cost="35.99" description="Saturday Delivery" code="SA"/>
  *					<method cost="20.99" description="Florist Delivery" code="FL"/>
  *				</shippingMethod>
  *			</shippingMethods>
  *			<deliveryDates>
  *				<deliveryDate date="09/30/2006" dayOfWeek="Sat" types="FL:SA:" displayDate="09/30/2006" index="index3"/>
  *				<deliveryDate date="10/02/2006" dayOfWeek="Mon" types="FL:GR:2D:ND:" displayDate="10/02/2006" index="index5"/>
  *			</deliveryDates>
  *		</shippingData>
  *
  *********************************************************************************************/

  public XMLDocument formatDeliveryDatesXML(XMLDocument unfShippingData) throws Exception
  {
    XMLDocument document = new XMLDocument();
    Element shippingData = (Element) document.createElement("shippingData");
    document.appendChild(shippingData);

    Element shippingMethods = (Element) document.createElement("shippingMethods");
    shippingData.appendChild(shippingMethods);

    Element inputElement, outputElement, newElement;
    String xpath;
    NodeList nl;

    //Process the deliveryDates
    xpath = "//deliveryDates";
    nl = unfShippingData.selectNodes(xpath);
    for(int i = 0; i < nl.getLength(); i++)
    {
      document.getDocumentElement().appendChild(document.importNode(nl.item(i), true));
    }


    //Process the shippingMethods
    xpath = "//method";
    nl = unfShippingData.selectNodes(xpath);

    if ( nl.getLength() > 0 )
    {
      ShippingMethod[] methods = new ShippingMethod[nl.getLength()]; 
      //create the main element
      outputElement = (Element)document.createElement("shippingMethod");
      String cost = "";
      String costWithSurcharge = "";
      String description = "";
      String code = "";
      for ( int i = 0; i < nl.getLength(); i++ )
      {
        //retrieve the <method ... > 
        inputElement = (Element) nl.item(i);
      
        cost = inputElement.getAttribute("cost");
        costWithSurcharge = inputElement.getAttribute("costWithSurcharge");
        description = inputElement.getAttribute("description");
        code = inputElement.getAttribute("code");

        methods[i] = new ShippingMethod(); 
        methods[i].setSDeliveryCharge(cost);
        methods[i].setSDeliveryChargeWithSurcharge(costWithSurcharge);
        methods[i].setDescription(description);
        methods[i].setCode(code);

      }

      Arrays.sort(methods, new ShipMethodSortDescription()); 

      for (int i = 0 ; i < nl.getLength(); i++)
      {
        ShippingMethod method = methods[i]; 
        //create the <method ...>
        newElement = (Element)document.createElement("method");
        newElement.setAttribute("cost", method.getSDeliveryCharge());
        newElement.setAttribute("costWithSurcharge", method.getSDeliveryChargeWithSurcharge());
        newElement.setAttribute("description", method.getDescription());
        newElement.setAttribute("code", method.getCode());
        outputElement.appendChild(newElement);
      }

      shippingMethods.appendChild(outputElement);
    }

    return document;
  }
 
}
