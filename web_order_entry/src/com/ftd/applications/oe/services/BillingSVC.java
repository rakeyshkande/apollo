package com.ftd.applications.oe.services;

import com.ftd.applications.oe.common.CommandConstants;
import com.ftd.applications.oe.common.DataConstants;
import com.ftd.applications.oe.common.FieldUtils;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.applications.oe.common.OEDeliveryDateParm;
import com.ftd.applications.oe.common.OEParameters;
import com.ftd.applications.oe.common.persistent.ItemPO;
import com.ftd.applications.oe.common.persistent.OrderPO;
import com.ftd.applications.oe.services.utilities.BillingUTIL;
import com.ftd.applications.oe.services.utilities.DeliveryDateUTIL;
import com.ftd.applications.oe.services.utilities.OEOrderUTIL;
import com.ftd.applications.oe.services.utilities.SearchUTIL;
import com.ftd.applications.oe.services.utilities.ShoppingUTIL;

import com.ftd.framework.businessservices.servicebusinessobjects.BusinessService;
import com.ftd.framework.common.utilities.XMLEncoder;
import com.ftd.framework.common.utilities.XPathQuery;
import com.ftd.framework.common.valueobjects.FTDArguments;
import com.ftd.framework.common.valueobjects.FTDCommand;
import com.ftd.framework.common.valueobjects.FTDDataRequest;
import com.ftd.framework.common.valueobjects.FTDDataResponse;
import com.ftd.framework.common.valueobjects.FTDSystemVO;
import com.ftd.framework.dataaccessservices.GenericDataService;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;


public class BillingSVC extends BusinessService {
  public FTDSystemVO main(FTDCommand command, FTDArguments arguments) {
    FTDSystemVO billingVO = null;

    if ((command.getCommand()).equals(CommandConstants.BILLING_LOAD_BILLING_INFO)) {
      billingVO = loadBillingInfo(arguments);
    } else if ((command.getCommand()).equals(CommandConstants.BILLING_LOAD_CREDIT_CARD_INFO)) {
      billingVO = loadCreditCardInfo(arguments);
    } else if ((command.getCommand()).equals(CommandConstants.BILLING_UPDATE_BILLING_INFO)) {
      updateBillingInfo(arguments);
    } else if ((command.getCommand()).equals(CommandConstants.BILLING_UPDATE_CREDIT_CARD_INFO)) {
      updateCreditCardInfo(arguments);
    } else if ((command.getCommand()).equals(CommandConstants.BILLING_LOAD_CONFIRMATION_INFO)) {
      billingVO = loadConfirmationInfo(arguments);
    } else if ((command.getCommand()).equals(CommandConstants.BILLING_COMPLETE_ORDER_ITEMS)) {
      completeOrder(arguments);
    } else if ((command.getCommand()).equals(CommandConstants.BILLING_COMMIT_ORDER)) {
      commitOrder(arguments);
    }

    return billingVO;
  }

  public void initialize() {
  }

  public String toString() {
    return null;
  }

  private FTDSystemVO loadBillingInfo(FTDArguments arguments) {
    FTDSystemVO systemVO = new FTDSystemVO();
    OrderPO order = null;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      ShoppingUTIL shoppingUtil = new ShoppingUTIL(this.getLogManager());

      FTDDataRequest dataRequest = new FTDDataRequest();
      dataRequest.addArgument("company", order.getCompanyId());
      dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
      dataRequest.addArgument("paymentType", GeneralConstants.CREDIT_CARD_PAYMENT_TYPE);

      if ((order.getScriptCode() != null) && order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE)) {
        dataRequest.addArgument("jcPenneyFlag", GeneralConstants.YES);
      } else {
        dataRequest.addArgument("jcPenneyFlag", GeneralConstants.NO);
      }

      if ((order.getPartnerId() != null) && order.getPartnerId().equals(GeneralConstants.DISCOVER_CODE)) {
        dataRequest.addArgument("discoverFlag", GeneralConstants.YES);
      } else {
        dataRequest.addArgument("discoverFlag", GeneralConstants.NO);
      }

      if (order.getMarketingGroup().equals("AAFES")) {
        dataRequest.addArgument("aafesFlag", GeneralConstants.YES);
      } else {
        dataRequest.addArgument("aafesFlag", GeneralConstants.NO);
      }

      dataRequest.addArgument("sourceCode_1", order.getSourceCode());
      dataRequest.addArgument("prompt_1", "CERTIFICATE#");

      dataRequest.addArgument("sourceCode_2", order.getSourceCode());
      dataRequest.addArgument("prompt_2", null);

      dataRequest.addArgument("partnerId", order.getPartnerId());

      dataRequest.addArgument("dnisType", order.getScriptCode());

      GenericDataService service = this.getGenericDataService(DataConstants.SHOPPING_GET_BILLING_INFO_DATA);
      FTDDataResponse dataResponse = service.execute(dataRequest);

      XMLDocument document = XMLEncoder.createXMLDocument("BILLING INFO PAGE VO");
      XMLEncoder.addSection(document, ((XMLDocument) dataResponse.getDataVO().getData()).getChildNodes());

      // Init display variables
      String displaySpecialPromotionCheckbox = "Y";
      String displayMembershipId = "N";
      String displayGiftCertificate = "N";
      String displayCreditCard = "N";
      String displayInvoice = "N";
      String paymentMethod = "";

      // Get source code info
      FTDDataRequest dataRequest2 = new FTDDataRequest();
      dataRequest2.addArgument("sourcecode", order.getSourceCode());
      dataRequest2.addArgument("deliveryDate", "");

      GenericDataService service2 = this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
      FTDDataResponse dataResponse2 = service2.executeRequest(dataRequest2);

      // Pull the source code detail
      String xpath2 = "sourceCodeDataList/sourceCodeData/data";
      XPathQuery q2 = new XPathQuery();
      NodeList nl2 = q2.query((XMLDocument) dataResponse2.getDataVO().getData(), xpath2);

      if (nl2.getLength() > 0) {
        Element sourceData = null;
        String membershipDataRequired = null;

        for (int i = 0; i < nl2.getLength(); i++) {
          sourceData = (Element) nl2.item(i);

          // Set the order with source code data
          order.setPaymentMethodType(sourceData.getAttribute("paymentType"));
          membershipDataRequired = sourceData.getAttribute("membershipDataRequired");
          if (membershipDataRequired != null && membershipDataRequired.equals("Y")) 
          {
            displayMembershipId = "Y";
          }
        }
      }

      if (order.getPaymentMethodType() != null) {
        if (!order.getPaymentMethodType().equals("I")) {
          displayCreditCard = "Y";
        } else {
          displayInvoice = "Y";
        }
      }

      if ((order.getBillingPrompt() != null) && order.getBillingPrompt().equals("CERTIFICATE#")) {
        displayGiftCertificate = "Y";
      }

      if (order.getBillToCustomer().isExisting() && (order.getSpecialPromotionFlag() != null) &&
          !order.getSpecialPromotionFlag().equals("")) {
        displaySpecialPromotionCheckbox = "N";
      }

      // Build data for page displays
      HashMap pageDataMap = new HashMap();

      if ((order.getPartnerId() != null) && !order.getPartnerId().equals("") &&
          (order.getPartnerId().equals("EBAY") || order.getPartnerId().equals("EBAY1") || order.getPartnerId().equals("EBAY2") ||
          order.getPartnerId().equals("HILT4") || order.getPartnerId().equals("HILT5"))) {
        pageDataMap.put("membershipIdLength", "64");
        pageDataMap.put("membershipIdNOTRequired", "Y");
      }

      pageDataMap.put("cartItemNumber", new Integer(order.getCurrentCartNumber()).toString());
      pageDataMap.put("displayMembershipId", displayMembershipId);
      pageDataMap.put("partnerId", order.getPartnerId());
      pageDataMap.put("displayCreditCard", displayCreditCard);
      pageDataMap.put("displaySpecialPromotionCheckbox", displaySpecialPromotionCheckbox);
      pageDataMap.put("paymentMethod", paymentMethod);
      pageDataMap.put("orderTotalPrice", FieldUtils.formatDoubleNoRound(order.getTotalOrderPrice().doubleValue()));
      pageDataMap.put("sourceCodeId", order.getSourceCode());
      pageDataMap.put("approvalVerbiage", order.getApprovalVerbiage());

      // This is a kludge - should be cleaned up.  Since companyId (in order) originates from
      // DNIS script_id, it will be FTD for Gift Store Card (since GSC uses FTD origins).  Since
      // gift certificate validation (and eventually HP) rely on companyId being set to GSC
      // appropriately, we set it here manually.  Since we know only GSC uses script codes of SC, 
      // we can use that as trigger.
      if ("SC".equals(order.getScriptCode())) {
        order.setCompanyId(GeneralConstants.COMPANY_ID_GSC);
      }

      pageDataMap.put("companyId", order.getCompanyId());

      String prompt = "";
      HashMap promptMap = order.getPromptMap();

      if ((promptMap != null) && (promptMap.size() > 0)) {
        for (int i = 0; i < promptMap.size(); i++) {
          prompt = (String) promptMap.get(GeneralConstants.MAP_TYPE_PROMPT + i);
          pageDataMap.put(prompt.substring(0, prompt.indexOf(":")), prompt.substring(prompt.indexOf(":") + 1, prompt.length()));
        }
      }

      // Encode billing page
      XMLEncoder.addSection(document, shoppingUtil.transformCustomerToXML(order).getChildNodes());

      //XMLEncoder.addSection(document, shoppingUtil.transformBillingInfoToXML(order).getChildNodes());
      XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

      XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
      XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

      systemVO.setXML(document);
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);
    } finally {
      this.returnPersistentServiceObject(order);
    }

    return systemVO;
  }

  private FTDSystemVO loadCreditCardInfo(FTDArguments arguments) {
    FTDSystemVO systemVO = new FTDSystemVO();
    OrderPO order = null;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      ShoppingUTIL shoppingUtil = new ShoppingUTIL(this.getLogManager());

      FTDDataRequest dataRequest = new FTDDataRequest();
      dataRequest.addArgument("company", order.getCompanyId());
      dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
      dataRequest.addArgument("paymentType", GeneralConstants.CREDIT_CARD_PAYMENT_TYPE);

      if ((order.getScriptCode() != null) && order.getScriptCode().equals(GeneralConstants.JC_PENNEY_CODE)) {
        dataRequest.addArgument("jcPenneyFlag", GeneralConstants.YES);
      } else {
        dataRequest.addArgument("jcPenneyFlag", GeneralConstants.NO);
      }

      if ((order.getPartnerId() != null) && order.getPartnerId().equals(GeneralConstants.DISCOVER_CODE)) {
        dataRequest.addArgument("discoverFlag", GeneralConstants.YES);
      } else {
        dataRequest.addArgument("discoverFlag", GeneralConstants.NO);
      }

      if (order.getMarketingGroup().equals("AAFES")) {
        dataRequest.addArgument("aafesFlag", GeneralConstants.YES);
      } else {
        dataRequest.addArgument("aafesFlag", GeneralConstants.NO);
      }

      dataRequest.addArgument("sourceCode_1", order.getSourceCode());
      dataRequest.addArgument("prompt_1", "CERTIFICATE#");

      dataRequest.addArgument("sourceCode_2", order.getSourceCode());
      dataRequest.addArgument("prompt_2", null);

      dataRequest.addArgument("partnerId", order.getPartnerId());

      dataRequest.addArgument("dnisType", order.getScriptCode());

      GenericDataService service = this.getGenericDataService(DataConstants.SHOPPING_GET_CREDIT_CARD_DATA);
      FTDDataResponse dataResponse = service.execute(dataRequest);

      XMLDocument document = XMLEncoder.createXMLDocument("CREDIT CARD INFO PAGE VO");
      XMLEncoder.addSection(document, ((XMLDocument) dataResponse.getDataVO().getData()).getChildNodes());

      // Init display variables
      String displaySpecialPromotionCheckbox = "Y";
      String displayMembershipId = "N";
      String displayGiftCertificate = "N";
      String displayCreditCard = "N";
      String displayInvoice = "N";
      String paymentMethod = "";

      // Set display based on order and source code values
      if ((order.getPartnerId() != null) && !order.getPartnerId().equals("") &&
          !order.getPartnerId().equals(GeneralConstants.DISCOVER_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.UPROMISE_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.UPROMISE1_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.CORP15_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.CORP20_CODE) && !order.getPartnerId().equals(GeneralConstants.AMEX_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.AMEX1_CODE) && !order.getPartnerId().equals(GeneralConstants.DINE_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.DINE1_CODE) && !order.getPartnerId().equals(GeneralConstants.LOWES_CODE) &&
          !order.getPartnerId().equals(GeneralConstants.ADVO_CODE)) {
        displayMembershipId = "Y";
      }

      // Get source code info
      FTDDataRequest dataRequest2 = new FTDDataRequest();
      dataRequest2.addArgument("sourcecode", order.getSourceCode());
      dataRequest2.addArgument("deliveryDate", "");

      GenericDataService service2 = this.getGenericDataService(DataConstants.SHOPPING_GET_SOURCE_CODE_DETAILS);
      FTDDataResponse dataResponse2 = service2.executeRequest(dataRequest2);

      // Pull the source code detail
      String xpath2 = "sourceCodeDataList/sourceCodeData/data";
      XPathQuery q2 = new XPathQuery();
      NodeList nl2 = q2.query((XMLDocument) dataResponse2.getDataVO().getData(), xpath2);

      if (nl2.getLength() > 0) {
        Element sourceData = null;

        for (int i = 0; i < nl2.getLength(); i++) {
          sourceData = (Element) nl2.item(i);

          // Set the order with source code data
          order.setPaymentMethodType(sourceData.getAttribute("paymentType"));
        }
      }

      if (order.getPaymentMethodType() != null) {
        if (!order.getPaymentMethodType().equals("I")) {
          displayCreditCard = "Y";
        } else {
          displayInvoice = "Y";
        }
      }

      if ((order.getBillingPrompt() != null) && order.getBillingPrompt().equals("CERTIFICATE#")) {
        displayGiftCertificate = "Y";
      }

      if (order.getBillToCustomer().isExisting() && (order.getSpecialPromotionFlag() != null) &&
          !order.getSpecialPromotionFlag().equals("")) {
        displaySpecialPromotionCheckbox = "N";
      }

      double adjustedTotal = this.getAdjustedTotal(order);


      // Build data for page displays
      HashMap pageDataMap = new HashMap();

      if ((order.getPartnerId() != null) && !order.getPartnerId().equals("") &&
          (order.getPartnerId().equals("EBAY") || order.getPartnerId().equals("EBAY1") || order.getPartnerId().equals("EBAY2") ||
          order.getPartnerId().equals("HILT4") || order.getPartnerId().equals("HILT5"))) {
        pageDataMap.put("membershipIdLength", "64");
        pageDataMap.put("membershipIdNOTRequired", "Y");
      }

      pageDataMap.put("cartItemNumber", new Integer(order.getCurrentCartNumber()).toString());
      pageDataMap.put("displayMembershipId", displayMembershipId);
      pageDataMap.put("partnerId", order.getPartnerId());
      pageDataMap.put("displayCreditCard", displayCreditCard);
      pageDataMap.put("displaySpecialPromotionCheckbox", displaySpecialPromotionCheckbox);
      pageDataMap.put("paymentMethod", paymentMethod);
      pageDataMap.put("orderTotalPrice", FieldUtils.formatDoubleNoRound(order.getTotalOrderPrice().doubleValue()));
      pageDataMap.put("adjustedPrice", FieldUtils.formatDoubleNoRound(adjustedTotal));
      pageDataMap.put("sourceCodeId", order.getSourceCode());
      pageDataMap.put("approvalVerbiage", order.getApprovalVerbiage());

      // This is a kludge - should be cleaned up.  Since companyId (in order) originates from
      // DNIS script_id, it will be FTD for Gift Store Card (since GSC uses FTD origins).  Since
      // gift certificate validation (and eventually HP) rely on companyId being set to GSC
      // appropriately, we set it here manually.  Since we know only GSC uses script codes of SC, 
      // we can use that as trigger.
      if ("SC".equals(order.getScriptCode())) {
        order.setCompanyId(GeneralConstants.COMPANY_ID_GSC);
      }

      pageDataMap.put("companyId", order.getCompanyId());

      String prompt = "";
      HashMap promptMap = order.getPromptMap();

      if ((promptMap != null) && (promptMap.size() > 0)) {
        for (int i = 0; i < promptMap.size(); i++) {
          prompt = (String) promptMap.get(GeneralConstants.MAP_TYPE_PROMPT + i);
          pageDataMap.put(prompt.substring(0, prompt.indexOf(":")), prompt.substring(prompt.indexOf(":") + 1, prompt.length()));
        }
      }

      // Encode billing page
      XMLEncoder.addSection(document, shoppingUtil.transformCustomerToXML(order).getChildNodes());
      XMLEncoder.addSection(document, shoppingUtil.transformBillingInfoToXML(order).getChildNodes());
      XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

      XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
      XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

      systemVO.setXML(document);
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);
    } finally {
      this.returnPersistentServiceObject(order);
    }

    return systemVO;
  }

  private void completeOrder(FTDArguments arguments) {
    OrderPO order = null;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      ItemPO item = null;

      this.getLogManager().info("Finalizing all items to be sent for processing.");

      // Set the processing time
      long totalProcessingTime = System.currentTimeMillis() - order.getProcessingTime().longValue();
      order.setProcessingTime(new Long(Math.round(totalProcessingTime / 1000)));

      // Set order confirmation number
      order.setConfirmationNumber("M" + new Long(OEOrderUTIL.getUniqueNumber()).toString() + "/" + order.getAllItems().size());
      this.getLogManager().info("M-Number: " + order.getConfirmationNumber() + " was generated for GUID: " +
        (String) arguments.get("_PERSISTENT_OBJECT_"));

      // Finalize item details
      FTDDataRequest dataRequest = null;
      GenericDataService service = null;
      FTDDataResponse dataResponse = null;

      Element sequenceElement = null;
      String xpath = null;
      XPathQuery q = null;
      NodeList nl = null;
      Calendar deliveryCalendar = null;

      String deliveryDate = null;
      HashMap fromMap = null;
      HashMap toMap = null;
      String productType = null;
      Set keys = null;
      Iterator it = null;
      String key = null;
      Date secondDeliveryDate = null;

      SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
      DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(this.getLogManager());
      SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
      BillingUTIL billingUtil = new BillingUTIL(this.getLogManager());
      OEDeliveryDateParm deliveryDateParm = new OEDeliveryDateParm();
      dataRequest = new FTDDataRequest();
      service = this.getGenericDataService(DataConstants.SEARCH_GET_DELIVERY_DATE_RANGES);
      dataResponse = service.executeRequest(dataRequest);

      XMLDocument dateRangeList = (XMLDocument) dataResponse.getDataVO().getData();
      deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_DATE_RANGE_DATA, dateRangeList, deliveryDateParm);

      Iterator itemIterator = order.getAllItems().values().iterator();

      while (itemIterator.hasNext()) {
        item = (ItemPO) itemIterator.next();

        // Pull the sequential ID for all items
        if ((item.getDetailItemNumber() == null) || (item.getDetailItemNumber().length() < 1)) {
          dataRequest = new FTDDataRequest();
          service = this.getGenericDataService(DataConstants.SHOPPING_GET_SEQUENTIAL_ID);
          dataResponse = service.execute(dataRequest);

          xpath = "orderSequenceId/sequenceId/value";

          q = new XPathQuery();
          nl = q.query((XMLDocument) dataResponse.getDataVO().getData(), xpath);

          if (nl.getLength() > 0) {
            sequenceElement = (Element) nl.item(0);
            item.setDetailItemNumber("C" + sequenceElement.getAttribute("orderSequence"));

            this.getLogManager().info("C-Number: " + "C" + sequenceElement.getAttribute("orderSequence") +
              " found and assigned to GUID: " + (String) arguments.get("_PERSISTENT_OBJECT_"));
          } else {
            this.getLogManager().error("C-Number was not found in the database for GUID: " +
              (String) arguments.get("_PERSISTENT_OBJECT_"));
          }
        } else {
          this.getLogManager().info("C-Number: " + item.getDetailItemNumber() +
            " was previously pulled (User most likely hit the back button and resubmitted).  A new number was not requested for GUID: " +
            (String) arguments.get("_PERSISTENT_OBJECT_"));
        }

        // Set sunday delivery flag
        deliveryCalendar = Calendar.getInstance();
        deliveryCalendar.setTime(item.getDeliveryDate());

        if (deliveryCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
          item.setSundayDeliveryFlag("Y");
        }

        // Set each item second delivery date if within a date range.                
        deliveryDate = sdfInput.format(item.getDeliveryDate());
        fromMap = deliveryDateParm.getDeliveryRangeFrom();
        toMap = deliveryDateParm.getDeliveryRangeTo();
        productType = item.getProductType();

        String shippingMethod = item.getShippingMethod();

        if (shippingMethod == null) {
          shippingMethod = "";
        }

        if ((productType != null) &&
            (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) ||
            (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
            (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) && shippingMethod.equals("Florist Delivery")))) &&
            deliveryDateParm.isDeliveryRangeFlag().booleanValue() && fromMap.containsValue(deliveryDate)) {
          keys = fromMap.keySet();
          it = keys.iterator();

          while (it.hasNext()) {
            key = (String) it.next();

            if (fromMap.get(key).equals(deliveryDate)) {
              secondDeliveryDate = sdfInput.parse((String) toMap.get(key));
              item.setSecondDeliveryDate(secondDeliveryDate);
            }
          }
        }
      }

      // Save or update customer in database
      dataRequest = new FTDDataRequest();

      try {
        dataRequest.addArgument("customerIn", order.getBillToCustomer().getHpCustomerId());
        dataRequest.addArgument("firstNameIn", order.getBillToCustomer().getFirstName());
        dataRequest.addArgument("lastNameIn", order.getBillToCustomer().getLastName());
        dataRequest.addArgument("addressTypeIn", "");
        dataRequest.addArgument("address1In", order.getBillToCustomer().getAddressOne());
        dataRequest.addArgument("address2In", order.getBillToCustomer().getAddressTwo());
        dataRequest.addArgument("cityIn", order.getBillToCustomer().getCity());
        dataRequest.addArgument("stateIn", order.getBillToCustomer().getState());
        dataRequest.addArgument("zipcodeIn", order.getBillToCustomer().getZipCode());
        dataRequest.addArgument("countyIn", "");
        dataRequest.addArgument("countryIdIn", order.getBillToCustomer().getCountry());
        dataRequest.addArgument("homePhoneIn", order.getBillToCustomer().getHomePhone());
        dataRequest.addArgument("workPhoneIn", order.getBillToCustomer().getWorkPhone());
        dataRequest.addArgument("workPhoneExtIn", order.getBillToCustomer().getPhoneExtension());
        dataRequest.addArgument("faxNumberIn", "");
        dataRequest.addArgument("emailIn", order.getBillToCustomer().getEmailAddress());
        dataRequest.addArgument("bfhNameIn", order.getBillToCustomer().getCompanyName());
        dataRequest.addArgument("bfhInfoIn", order.getBillToCustomer().getContactInfo());

        if (order.getSpecialPromotionFlag() != null) {
          dataRequest.addArgument("promoFlag", order.getSpecialPromotionFlag());
        } else {
          dataRequest.addArgument("promoFlag", "N");
        }

        if ((order.getCsrUserName() != null) && !order.getCsrUserName().equals("")) {
          dataRequest.addArgument("lastUpdateUserIn", order.getCsrUserName());
        } else {
          dataRequest.addArgument("lastUpdateUserIn", "NA");
        }

        service = this.getGenericDataService(DataConstants.SHOPPING_INSERT_CUSTOMER);
        dataResponse = service.execute(dataRequest);
      } catch (Exception e) {
        this.getLogManager().error("Problems occured while trying to insert or update customer: " +
          order.getBillToCustomer().getFirstName() + " " + order.getBillToCustomer().getLastName(), e);
      }

      // Set the newsletter flag so the HP can see if this bill to customer wants to recieve
      // special promotions
      if ((order.getSpecialPromotionFlag() != null) && order.getSpecialPromotionFlag().equals("Y")) {
        order.setNewsletterFlag(true);
      }

      // Translate yellow page code data
      if (order.isYellowPagesFlag()) {
        boolean switchSourceCode = false;

        try {
          dataRequest = new FTDDataRequest();
          dataRequest.addArgument("dnisId", order.getDnisCode().toString());
          service = this.getGenericDataService(DataConstants.VALIDATE_DNIS);
          dataResponse = service.execute(dataRequest);

          xpath = "validateDnis/dnisInformation/dnis";
          q = new XPathQuery();
          nl = q.query((XMLDocument) dataResponse.getDataVO().getData(), xpath);

          if (nl.getLength() > 0) {
            Element dnisElement = (Element) nl.item(0);

            if ((order.getSourceCode() != null) && order.getSourceCode().equals(dnisElement.getAttribute("defaultSourceCode"))) {
              switchSourceCode = true;
            }
          }

          if (switchSourceCode) {
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("yellowPagesCode", order.getYellowPagesCode());
            service = this.getGenericDataService(DataConstants.SEARCH_GET_SOURCE_BY_YELLOW);
            dataResponse = service.execute(dataRequest);

            xpath = "sourceCodeList/souceCode/data";
            q = new XPathQuery();
            nl = q.query((XMLDocument) dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0) {
              Element sourceCodeElement = (Element) nl.item(0);
              order.setSourceCode(sourceCodeElement.getAttribute("sourceCode"));
            }
          }
        } catch (Exception e) {
          this.getLogManager().error("Problems occured while trying to translate yellow page data.", e);
        }
      }

      // Retrieve the credit card ID for the HP
      if ((order.getPaymentMethodType() == null) || !order.getPaymentMethodType().equals("I")) {
        dataRequest = new FTDDataRequest();
        dataRequest.addArgument("inPaymentMethodId", order.getCreditCardType());
        service = this.getGenericDataService(DataConstants.SEARCH_GET_PAYMENT_METHOD_BY_ID);
        dataResponse = service.execute(dataRequest);

        xpath = "paymentMethodList/paymentMethod/data";
        q = new XPathQuery();
        nl = q.query((XMLDocument) dataResponse.getDataVO().getData(), xpath);

        if (nl.getLength() > 0) {
          Element paymentMethodElement = (Element) nl.item(0);
          order.setCreditCardId(paymentMethodElement.getAttribute("cardId"));
        }
      }

      // Set the transaction date and order status to complete
      order.setTransactionDate(new Date(System.currentTimeMillis()));
      order.setOrderStatus(GeneralConstants.DONE);

      OEParameters globalParms = searchUTIL.getGlobalParms();

      if ((globalParms.getDispatchOrderFlag() != null) && globalParms.getDispatchOrderFlag().equalsIgnoreCase("Y")) {
        this.getLogManager().debug("Calling BillingUTIL.dispatchOrder()...");

        // Dispatch the order
        billingUtil.dispatchOrder(order, true);
      }
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);
    } finally {
      this.returnPersistentServiceObject(order);
    }
  }

  private void updateBillingInfo(FTDArguments arguments) {
    OrderPO order = null;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      // this check is specificlly for 're-instating' orders in WebOE, which
      // in general should not allow for any change in order data.
      // Since a duplicate credit card authorization is prevented in XSL, here
      // we must specifically prevent overwriting cc auth info with the resulting NULLS from the XSL
      if ((order.getApprovalVerbiage() == null) || !order.getApprovalVerbiage().equals("AP")) {
        // Set order with new billing information
        order.getBillToCustomer().setHpCustomerId(new Long(OEOrderUTIL.getUniqueNumber()).toString());
        order.getBillToCustomer().setFirstName((String) arguments.get("billingFirstName"));
        order.getBillToCustomer().setLastName((String) arguments.get("billingLastName"));
        order.getBillToCustomer().setAddressOne((String) arguments.get("billingAddress1"));
        order.getBillToCustomer().setAddressTwo((String) arguments.get("billingAddress2"));
        order.getBillToCustomer().setCity((String) arguments.get("billingCity"));
        String billingCountry = (String) arguments.get("billingCountry");
        String billingState = "";
        if (billingCountry.equalsIgnoreCase("US") || billingCountry.equalsIgnoreCase("CA"))
        {
          billingState = (String) arguments.get("billingStateSelect");
        }
        else
        {
          billingState = (String) arguments.get("billingStateText");
        }
        order.getBillToCustomer().setState(billingState);
        order.getBillToCustomer().setCountry((String) arguments.get("billingCountry"));
        order.getBillToCustomer().setCountryType((String) arguments.get("countryType"));
        order.getBillToCustomer().setZipCode((String) arguments.get("billingZipCode"));
        order.getBillToCustomer().setHomePhone((String) arguments.get("homePhone"));
        order.getBillToCustomer().setWorkPhone((String) arguments.get("altPhone"));
        order.getBillToCustomer().setPhoneExtension((String) arguments.get("extension"));
        order.getBillToCustomer().setCompanyName((String) arguments.get("businessName"));
        order.getBillToCustomer().setEmailAddress((String) arguments.get("eMail"));
        order.getBillToCustomer().setContactInfo((String) arguments.get("contactInformation"));

        String specialPromo = (String) arguments.get("specialPromotions");

        // Quick fix for 3/12/04
        if ((specialPromo != null) && (specialPromo.equals("true") || specialPromo.equals("on") || specialPromo.equals("Y"))) {
          specialPromo = "Y";
        }
        // Quick fix for 3/12/04
        else {
          specialPromo = "N";
        }

        order.setSpecialPromotionFlag(specialPromo);

        /********************************************/
        // Special Promotion Flag is now being set  on the customent object
        order.getBillToCustomer().setSpecialPromotionsFlag(specialPromo);

        /********************************************/

        // membership information not supplied, so use default values
        if ((arguments.get("skipMemberInfo") != null)) {
          order.getBillToCustomer().setMembershipId(GeneralConstants.DEFAULT_MEMBER_ID);
        } else {
          order.getBillToCustomer().setMembershipId((String) arguments.get("memberId"));
        }

        order.getBillToCustomer().setMembershipFirstName((String) arguments.get("memberFirstName"));
        order.getBillToCustomer().setMembershipLastName((String) arguments.get("memberLastName"));
        order.setInvoiceNumber((String) arguments.get("invoiceNumber"));
        order.setGiftCertificateId((String) arguments.get("giftCertId"));

        if ((arguments.get("giftCertificateAmount") != null) && (!((String) arguments.get("giftCertificateAmount")).equals(""))) {
          order.setGiftCertificateAmount(new BigDecimal((String) arguments.get("giftCertificateAmount")).setScale(2,
              BigDecimal.ROUND_HALF_DOWN));
        }

        // Set up header for customer information
        if ((order.getBillToCustomer().getFirstName() != null) && (!order.getBillToCustomer().getFirstName().trim().equals(""))) {
          order.setHeaderValue("customerFirstName", order.getBillToCustomer().getFirstName());
        }

        if ((order.getBillToCustomer().getLastName() != null) && (!order.getBillToCustomer().getLastName().trim().equals(""))) {
          order.setHeaderValue("customerLastName", order.getBillToCustomer().getLastName());
        }

        if (!order.getHeaderMap().containsKey("customerFirstName") && !order.getHeaderMap().containsKey("customerLastName")) {
          order.setHeaderValue("customerFirstName", "Unknown");
        }
      } //if (!order.getApprovalVerbiage().equals("AP")) 
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);
    } finally {
      this.returnPersistentServiceObject(order);
    }
  }

  private void updateCreditCardInfo(FTDArguments arguments) {
    OrderPO order = null;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      // this check is specificlly for 're-instating' orders in WebOE, which
      // in general should not allow for any change in order data.
      // Since a duplicate credit card authorization is prevented in XSL, here
      // we must specifically prevent overwriting cc auth info with the resulting NULLS from the XSL
      if ((order.getApprovalVerbiage() == null) || !order.getApprovalVerbiage().equals("AP")) {
        // Set order with new billing information
        
        // if the order has been paid by a gift certificate, then set CC type to 'NC'
        if (this.getAdjustedTotal(order) <= 0) {
          order.setCreditCardType("NC");
        } else {
          String ccType = (String) arguments.get("creditCardType");

          if (ccType == null) {
            ccType = "";
          }

          // if this is an AAFES card then use only a few fields            
          if (ccType.equals("MS")) // AAFES
           {
            order.setCreditCardTicketNumber((String) arguments.get("ccACQReferenceData"));
            order.setCreditCardAuthCode((String) arguments.get("ccActionCode"));
            order.setApprovalCode((String) arguments.get("ccActionCode"));
            order.setApprovalVerbiage((String) arguments.get("ccApprovalVerbiage"));
          } else {
            // verbal credit card authorization provided, so default values for other information
            String verbalAuthorization = (String) arguments.get("verbalAuthorization");

            if ((verbalAuthorization != null) && (verbalAuthorization.trim().length() > 0)) {
              order.setApprovalCode((String) arguments.get("verbalAuthorization"));
              order.setApprovalVerbiage(GeneralConstants.VERBAL_APPROVAL_VERBAGE);
              order.setApprovalActionCode(GeneralConstants.VERBAL_APPROVAL_ACTION_CODE);
            } else {
              order.setApprovalCode((String) arguments.get("ccApprovalCode"));
              order.setApprovalVerbiage((String) arguments.get("ccApprovalVerbiage"));
              order.setApprovalActionCode((String) arguments.get("ccActionCode"));
            }

            order.setAcqReferenceData((String) arguments.get("ccACQReferenceData"));
            order.setCreditCardTicketNumber("");
            order.setCreditCardAuthCode("");
          }

          order.setAvsResult((String) arguments.get("ccAVSResult"));
          order.setApprovalAmount((String) arguments.get("ccApprovalAmount"));
          order.setCreditCardType(ccType);
          order.setCreditCardNumber((String) arguments.get("creditCardNumber"));
          order.setCreditCardExpireMonth((String) arguments.get("creditCardExpirationMonth"));
          order.setCreditCardExpireYear((String) arguments.get("creditCardExpirationYear"));
          order.setNcApprovalId((String) arguments.get("noChargeMgrId"));
        }
        
        // Clear previous prompting for billing page
        if ((order.getPromptMap() != null) && (order.getPromptMap().size() > 0)) {
          order.clearPrompts();
        }

        // Update prompting for billing page
        for (int i = 0; i < 10; i++) {
          if ((arguments.get("promptName" + i) != null) && (!((String) arguments.get("promptName" + i)).equals(""))) {
            order.addPrompt(GeneralConstants.MAP_TYPE_PROMPT + i,
              (String) arguments.get("promptName" + i) + ":" + (String) arguments.get("promptValue" + i));
          } else {
            break;
          }
        }
      }
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);
    } finally {
      this.returnPersistentServiceObject(order);
    }
  }

  private FTDSystemVO loadConfirmationInfo(FTDArguments arguments) {
    FTDSystemVO systemVO = new FTDSystemVO();
    OrderPO order = null;
    StringBuffer sb = new StringBuffer();

    double certificateAmount = 0;
    double adjustedTotal = 0;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      FTDDataRequest dataRequest = new FTDDataRequest();
      dataRequest.addArgument("company", order.getCompanyId());
      dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
      dataRequest.addArgument("partnerId", order.getPartnerId());

      // ** SFMB **
      dataRequest.addArgument("dnisId", order.getDnisCode().toString());

      GenericDataService service = this.getGenericDataService(DataConstants.SHOPPING_GET_CONFIRMATION_DATA);
      service = this.getGenericDataService(DataConstants.SHOPPING_GET_CONFIRMATION_DATA);

      FTDDataResponse dataResponse = service.execute(dataRequest);
      dataResponse = service.execute(dataRequest);

      XMLDocument xmlDocument = XMLEncoder.createXMLDocument("ORDER CONFIRMATION PAGE VO");
      XMLEncoder.addSection(xmlDocument, ((XMLDocument) dataResponse.getDataVO().getData()).getChildNodes());

      String confirmationNumber = order.getConfirmationNumber();

      if ((order.getAllItems() != null) && (order.getAllItems().size() == 1)) {
        ItemPO item = (ItemPO) order.getAllItems().values().toArray()[0];
        confirmationNumber = item.getDetailItemNumber();
      }

      // Build the page data for needed fields
      HashMap pageDataMap = new HashMap();

      pageDataMap.put("confirmationNumber", confirmationNumber);

      pageDataMap.put("countryType", order.getBillToCustomer().getCountryType());
      pageDataMap.put("rewardType", order.getRewardType());

      if ((order.getRewardType() != null) && (order.getRewardType().equals("Miles") || order.getRewardType().equals("Points"))) {
        pageDataMap.put("rewardValue", order.getTotalRewardAmount().toString());
      }

      pageDataMap.put("country", order.getBillToCustomer().getCountry());

      //determine if gift certificate applied
      // Get the order total amount
      adjustedTotal = this.getAdjustedTotal(order);

      pageDataMap.put("totalPrice", FieldUtils.formatDoubleNoRound(adjustedTotal));

      if (order.getGiftCertificateAmount() != null) {
        certificateAmount = order.getGiftCertificateAmount().doubleValue();
        pageDataMap.put("giftCertificateAmount", FieldUtils.formatDoubleNoRound(certificateAmount));
        pageDataMap.put("giftCertificateAdjustedTotal", FieldUtils.formatDoubleNoRound(adjustedTotal));
      } else {
        pageDataMap.put("giftCertificateAmount", "");
        pageDataMap.put("giftCertificateAdjustedTotal", "");
      }

      //determine approximate canadian funds for total amount charged to credit card
      if (order.getBillToCustomer().getCountry().equals("CA")) {
        dataRequest = new FTDDataRequest();
        service = this.getGenericDataService(DataConstants.SHOPPING_GET_GLOBAL_PARMS);
        dataResponse = service.execute(dataRequest);

        NodeList nl = null;
        String xpath = "globalParmsDataSet/globalParmsData/data";
        XPathQuery q = new XPathQuery();
        nl = q.query((XMLDocument) dataResponse.getDataVO().getData(), xpath);

        Element globalParmsLM = (Element) nl.item(0);

        String s = "0";
        s = globalParmsLM.getAttribute("canadianExchangeRate");

        BigDecimal convertToCanadian = new BigDecimal(s);
        BigDecimal convertTotalAmount = new BigDecimal(adjustedTotal);
        convertToCanadian = convertTotalAmount.multiply(convertToCanadian);
        pageDataMap.put("canadianFunds", convertToCanadian.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
      }

      // Get item delivery date info for display
      XMLDocument document = new XMLDocument();
      Element root = document.createElement("itemData");
      document.appendChild(root);

      BigDecimal totalMonetaryDiscount = new BigDecimal(0);
      Element entry = null;
      HashMap items = order.getAllItems();

      for (int i = 0; i < items.size(); i++) {
        String date = "";
        ItemPO item = (ItemPO) order.getAllItems().values().toArray()[i];

        if (item.getDiscountAmount() != null) {
          totalMonetaryDiscount = totalMonetaryDiscount.add(item.getDiscountAmount());
        }

        if ((item.getDeliveryDateDisplay() != null) && (item.getDeliveryDateDisplay().length() > 0)) {
          String dates = item.getDeliveryDateDisplay();
          dates = FieldUtils.replaceAll(dates, "Mon", "");
          dates = FieldUtils.replaceAll(dates, "Tue", "");
          dates = FieldUtils.replaceAll(dates, "Wed", "");
          dates = FieldUtils.replaceAll(dates, "Thu", "");
          dates = FieldUtils.replaceAll(dates, "Fri", "");
          dates = FieldUtils.replaceAll(dates, "Sat", "");
          dates = FieldUtils.replaceAll(dates, "Sun", "");
          date = dates.trim();
        } else {
          date = FieldUtils.formatUtilDateToString(item.getDeliveryDate());
        }

        entry = document.createElement("item");
        entry.setAttribute("productId", item.getProductId());
        entry.setAttribute("productDescription", item.getItemName());
        sb.append(item.getSendToCustomer().getFirstName()).append(" ").append(item.getSendToCustomer().getLastName());
        entry.setAttribute("recipientName", sb.toString());
        sb.setLength(0);
        entry.setAttribute("date", date);
        root.appendChild(entry);
      }

      /* JMP - 6/6/03
       * commented out because we can just get the nodes and append them
       * the docuemnt directly
      StringWriter stringWriter = new StringWriter();
      try
      {
          document.print(new PrintWriter(stringWriter));
      }
      catch(Exception e)
      {
          //log message
          this.getLogManager().error("Can not convert XML to String", e);
      }

      XMLEncoder.addSection(stringWriter.toString());
      */
      XMLEncoder.addSection(xmlDocument, document.getChildNodes());
      pageDataMap.put("totalMonetaryDiscount", totalMonetaryDiscount.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());

      XMLEncoder.addSection(xmlDocument, "orderData", "data", pageDataMap);
      XMLEncoder.addSection(xmlDocument, "pageHeader", "headerDetail", order.getHeaderMap());
      XMLEncoder.addSection(xmlDocument, "previousPages", "previousPage", order.getCrumbMap());

      systemVO.setXML(xmlDocument);

      // Clear database of all data no longer needed
      order.emptyMapStorage();
      order.getPromotionList().clear();
      order.setSendToCustomer(null);
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);
    } finally {
      this.returnPersistentServiceObject(order);
    }

    return systemVO;
  }

  private void commitOrder(FTDArguments arguments) {
    OrderPO order = null;

    try {
      order = (OrderPO) this.getPersistentServiceObject((String) arguments.get("_PERSISTENT_OBJECT_"), OrderPO.class);

      // this.getLogManager().info("Sending order to RMI Server for processing");
      // BillingUTIL billingUTIL = new BillingUTIL(this.getLogManager());
      // billingUTIL.commitOrder((String)arguments.get("_PERSISTENT_OBJECT_"));
      this.getLogManager().info("Order is complete.  Total time: " + order.getProcessingTime() + " seconds");
    } catch (Exception e) {
      this.getLogManager().error(e.toString(), e);

      // order.setOrderStatus(GeneralConstants.BLOB_STATUS_ERR_ADDON_TRANS);
    } finally {
      this.returnPersistentServiceObject(order);
    }
  }

  private double getAdjustedTotal(OrderPO order) {
    double orderTotalAmount = order.getTotalOrderPrice().doubleValue();
    double certificateAmount = 0;

    if (order.getGiftCertificateAmount() != null) {
      // Get the amount of the gift certificate
      certificateAmount = order.getGiftCertificateAmount().doubleValue();
    }

    // apply the certificate amount
    double adjustedTotal = orderTotalAmount - certificateAmount;

    // If the adjusted order total is less than zero, make it zero
    adjustedTotal = Math.max(adjustedTotal, 0);

    return adjustedTotal;
  }
}
