package com.ftd.applications.oe.services;

import com.ftd.framework.businessservices.servicebusinessobjects.*;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.framework.security.*;

import com.ftd.applications.oe.common.*;
import com.ftd.applications.oe.services.utilities.AppStatsUTIL;
import com.ftd.applications.oe.admin.util.*;
import oracle.xml.parser.v2.*;

import org.w3c.dom.*;

import java.util.*;

public class AdminSVC extends BusinessService
{    
	public FTDSystemVO main(FTDCommand command, FTDArguments arguments)
	{
        FTDSystemVO adminVO = null;
        
        if((command.getCommand()).equals("GET_USER")) {
			adminVO = getUser(arguments);
        }
        else if((command.getCommand()).equals("UPDATE_USER")) {
            adminVO = updateUser(arguments);
        }
        else if((command.getCommand()).equals("GET_ADMIN")) {
            adminVO = getAdmin(arguments);
        }
        else if((command.getCommand()).equals("GET_APP_STATS")) {
            adminVO = getAppStats(arguments);
        }        
        
        return adminVO;
	}

	public  void initialize()
	{}	
    
	public  String toString()
	{
		return null;	
	}

    private FTDSystemVO getAppStats(FTDArguments arguments)
    {
        FTDSystemVO systemVO = new FTDSystemVO();
        OEAdminParams.CONNECTION_POOL_DATABASE_URL = "jdbc:oracle:thin:@ithaca.ftdi.com:1521:ZEUS1";
        //OEAdminParams.CONNECTION_POOL_DATABASE_USERNAME = "ftd_apps";
        //OEAdminParams.CONNECTION_POOL_DATABASE_PASSWORD = "athens";

        //OEAdminParams.CONNECTION_POOL_DATABASE_URL = "jdbc:oracle:thin:@delphi.ftdi.com:1521:DEV1";
        OEAdminParams.CONNECTION_POOL_DATABASE_USERNAME = "ftd_apps";
        OEAdminParams.CONNECTION_POOL_DATABASE_PASSWORD = "athens";
        try 
        {
            XMLDocument document = XMLEncoder.createXMLDocument("APP STATS PAGE VO");
            HashMap pageDataMap = new HashMap();
            AppStatsUTIL statsUTIL = new AppStatsUTIL(this.getLogManager());
            Date currentDate = new Date();
            
            // get current users in the last 15 minutes
            String currentUsers = "";
            Calendar startToday = Calendar.getInstance();            
            startToday.setTime(currentDate);
            startToday.set(Calendar.MINUTE, (startToday.get(Calendar.MINUTE) - 15));            
            currentUsers = statsUTIL.getNumberUsersSince(startToday.getTime());
            pageDataMap.put("currentUsers", currentUsers);
            
            // get orders today since 7:00 AM CST
            String ordersToday = "";
            startToday = Calendar.getInstance();            
            startToday.setTime(currentDate);
            startToday.set(Calendar.HOUR, 7);
            startToday.set(Calendar.MINUTE, 1);
            startToday.set(Calendar.AM_PM, Calendar.AM);
            ordersToday = statsUTIL.getNumberOrders(startToday.getTime());
            pageDataMap.put("ordersToday", ordersToday);
            
            // get orders last 24 hours
            String ordersLast24 = "";
            // subtract 24 hours
            long minus24 = (currentDate.getTime() - (1000 * 60 * 60 * 24));
            ordersLast24 = statsUTIL.getNumberOrders(new Date(minus24));
            pageDataMap.put("ordersLast24", ordersLast24);
            
            // get orders last 48 hours
            String ordersLast48 = "";
            // subtract 48 hours
            long minus48 = (currentDate.getTime() - (1000 * 60 * 60 * 48));
            ordersLast48 = statsUTIL.getNumberOrders(new Date(minus48));
            pageDataMap.put("ordersLast48", ordersLast48);
            
            // get average call time over last 24 hours
            String averageCallTime = "";
            averageCallTime = statsUTIL.getAverageCallTimeSince(new Date(minus24));
            pageDataMap.put("averageCallTime", averageCallTime);

            // get open database cursors
            String openDBCursors = "";
            openDBCursors = statsUTIL.getOpenDBCursors();
            pageDataMap.put("openDBCursors", openDBCursors);
            
            // get cached objects on both servers
            String cachedObjects = "";

            // get sparta
            pageDataMap.put("cachedObjectsSparta", cachedObjects);

            // get olympia
            pageDataMap.put("cachedObjectsOlympia", cachedObjects);

            // output the current date and time
            pageDataMap.put("runDate", currentDate.toString());
            
            XMLEncoder.addSection(document,"pageData", "data", pageDataMap);
            systemVO.setXML(document);
        }
        catch(Exception e) 
        {
            this.getLogManager().error(e.toString(), e);
        }
        
        return systemVO;
    }    

    private FTDSystemVO getAdmin(FTDArguments arguments)
    {
        FTDSystemVO systemVO = new FTDSystemVO();
        FTDSecurityCertificate cert = null;
        
        try 
        {
            // Get session ID
            String sessionId = (String)arguments.get("sessionId");
            
            // Get the user's role        
            com.ftd.framework.security.SecurityManager security = com.ftd.framework.security.SecurityManager.getInstance();

            cert = security.authenticateSession(sessionId);

            FTDRole role = cert.getRole();

            HashMap functionMap = new HashMap();

            // Set flags in XML representing what will be shown in page
            if(role.getFunction().contains("USERMAIN"))
            {
                functionMap.put("userMaintFlag", "Y");                
            }

            if(role.getFunction().contains("PDBMAINT"))
            {
                functionMap.put("pdbMaintFlag", "Y");                
            }

            if(role.getFunction().contains("LISTUSERMAINT"))
            {
                functionMap.put("listUserMaintFlag", "Y");                
            }

            if(role.getFunction().contains("EDITDELUSERMAINT"))
            {
                functionMap.put("editUserMaintFlag", "Y");                
            }
            
        
            XMLDocument document = XMLEncoder.createXMLDocument("ADMIN PAGE VO");
            XMLEncoder.addSection(document, "functionData", "data", functionMap);

            systemVO.setXML(document);            
            
        }
        catch(Exception e) 
        {
            this.getLogManager().error(e.toString(), e);
        }
        
        return systemVO;
    }

    private FTDSystemVO getUser(FTDArguments arguments)
    {
        FTDSystemVO systemVO = new FTDSystemVO();

        try 
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("username", (String) arguments.get("username"));

            GenericDataService service =  this.getGenericDataService(DataConstants.ADMIN_GET_USER);
            FTDDataResponse dataResponse = service.execute(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("USER ADMIN PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e) 
        {
            this.getLogManager().error(e.toString(), e);
        }
        
        return systemVO;
    }

    private FTDSystemVO updateUser(FTDArguments arguments)
    {
        FTDSystemVO systemVO = new FTDSystemVO();
        try 
        {            
            com.ftd.framework.security.SecurityManager security = com.ftd.framework.security.SecurityManager.getInstance();
            if(arguments.get("batchUpdate") != null)
            {
                // we are doing a batch update
                String batchStr = (String)arguments.get("batchUpdate");
                StringTokenizer lineTokenizer = new StringTokenizer(batchStr, "\r\n", false);
                String userCC = null; 
                while(lineTokenizer.hasMoreTokens())
                {                    
                    String line = lineTokenizer.nextToken();
                    StringTokenizer stringTokenizer = new StringTokenizer(line, ",", false);
                    int counter = 0;
                    while(stringTokenizer.hasMoreTokens())
                    {
                        String str = stringTokenizer.nextToken();
                        switch(counter)
                        {
                            case 0:
                                arguments.put(SecurityConstants.USERPROFILE_USER_ID, str.trim());
                                break;
                            case 1:
                                arguments.put(SecurityConstants.USERPROFILE_CURRENT_PASSWORD, str.trim());
                                break;
                            case 2:
                                arguments.put(SecurityConstants.USERPROFILE_FIRST_NAME, str.trim());
                                break;
                            case 3:
                                arguments.put(SecurityConstants.USERPROFILE_LAST_NAME, str.trim());
                                break;
                            case 4:
                                userCC = str.trim();
                                arguments.put(SecurityConstants.USERPROFILE_CALL_CENTER_ID, userCC);                                
                                break;
                            case 5:
                                arguments.put(SecurityConstants.USERPROFILE_ROLE_ID, str.trim());
                                break;
                            case 6:
                                arguments.put(SecurityConstants.USERPROFILE_ACTIVE_FLAG, str.trim());
                                break;
                                
                        }
                        counter++;
                    }

                    String adminId = (String) arguments.get("updateUserId");

                    // Check to make sure the admin user can modify this user (are they in the same callcenter)
                    FTDDataRequest dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("username", adminId);
                    GenericDataService service =  this.getGenericDataService(DataConstants.ADMIN_GET_USER);
                    FTDDataResponse dataResponse = service.executeRequest(dataRequest);
                    NodeList nl = null;
                    Element element = null;
                    String xpath = "user/userInfo/data";
                    XPathQuery q = new XPathQuery();

                    nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                    element = (Element) nl.item(0);
                    String adminCC = element.getAttribute("callCenterId");
                    
                    //if(adminCC.equals(userCC))
                    //{
                        arguments.put(SecurityConstants.USERPROFILE_LAST_UPDATE_USER, adminId);                    
                        arguments.put("encryptPassword", "true");
                        this.getLogManager().debug("Updating user " + (String)arguments.get(SecurityConstants.USERPROFILE_USER_ID));
                        security.updateUser(arguments);
                    //}
                    //else
                    //{
                    //    throw new Exception("Must be in same call center as user");                        
                    //}
                }
            }
            else
            {
                String userCC = (String) arguments.get("callCenterId");
                // Update one user
                arguments.put(SecurityConstants.USERPROFILE_USER_ID, (String) arguments.get("username"));
                arguments.put(SecurityConstants.USERPROFILE_CURRENT_PASSWORD, (String) arguments.get("password"));
                arguments.put(SecurityConstants.USERPROFILE_ACTIVE_FLAG, (String) arguments.get("activeFlag"));
                arguments.put(SecurityConstants.USERPROFILE_CALL_CENTER_ID, userCC);
                arguments.put(SecurityConstants.USERPROFILE_FIRST_NAME, (String) arguments.get("firstName"));
                arguments.put(SecurityConstants.USERPROFILE_LAST_NAME, (String) arguments.get("lastName"));
                arguments.put(SecurityConstants.USERPROFILE_ROLE_ID, (String) arguments.get("roleId"));
                arguments.put(SecurityConstants.USERPROFILE_LAST_UPDATE_USER, (String) arguments.get("updateUserId"));

                if(arguments.get("updatePassword") != null && arguments.get("updatePassword").equals("true"))
                {
                    arguments.put("encryptPassword", "true");
                }
                else
                {
                    arguments.put("encryptPassword", "false");
                }
    //            security.updateUserPassword(arguments);

                String adminId = (String) arguments.get("updateUserId");

                // Check to make sure the admin user can modify this user (are they in the same callcenter)
                FTDDataRequest dataRequest = new FTDDataRequest();
                dataRequest.addArgument("username", adminId);
                GenericDataService service =  this.getGenericDataService(DataConstants.ADMIN_GET_USER);
                FTDDataResponse dataResponse = service.executeRequest(dataRequest);
                NodeList nl = null;
                Element element = null;
                String xpath = "user/userInfo/data";
                XPathQuery q = new XPathQuery();

                nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                element = (Element) nl.item(0);
                String adminCC = element.getAttribute("callCenterId");
                    
                //if(adminCC.equals(userCC))
                //{
                    security.updateUser(arguments);
                    this.getLogManager().debug("Updating user " + (String)arguments.get(SecurityConstants.USERPROFILE_USER_ID));
                //}
                //else
                //{
                //    throw new Exception("Must be in same call center as user");
                //}
            }

            systemVO = this.getUser(arguments);
        }
        catch(Exception e) 
        {
            XMLDocument document = XMLEncoder.createXMLDocument("USER ADMIN PAGE VO");

            String result = "<result data=\"" + e.getMessage() + "\" />";
            try
            {
                //XMLEncoder.addSection(document, result);
                //arguments.clear();
                //XMLEncoder.addSection(getUser(arguments).getXML());
                systemVO.setXML(document);
            }
            catch(Exception ex)
            {
            }            

            this.getLogManager().error(e.toString(), e);
        }
        
        return systemVO;
    }
    
    

}