package com.ftd.applications.oe.services;

import com.ftd.framework.businessservices.servicebusinessobjects.*;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.*;
import com.ftd.applications.oe.common.persistent.*;
import com.ftd.applications.oe.services.utilities.*;
import org.w3c.dom.*;
import oracle.xml.parser.v2.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.math.*;

public class SearchSVC extends BusinessService
{
    public FTDSystemVO main(FTDCommand command, FTDArguments arguments)
	{
        FTDSystemVO searchResultVO = null;

        if((command.getCommand()).equals(CommandConstants.SEARCH_GET_INTRODUCTION)) {
			searchResultVO = getIntroductionData(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_GET_DNIS_SCRIPT)) {
            searchResultVO = getDnisData(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_PRODUCT_BY_ID)) {
            searchResultVO = getProductByID(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_UPDATE_PRODUCT_DATES)) {
            searchResultVO = updateProductDates(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_PRODUCTS_BY_CATEGORY)) {
            searchResultVO = getProductsByCategory(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_CUSTOMER_LOOKUP)) {
            searchResultVO = getCustomer(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_INSTITUTION_LOOKUP)) {
            searchResultVO = getInstitution(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_PRODUCTS_BY_VALUE)) {
            searchResultVO = getProductsBySearch(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_PRODUCTS_BY_KEYWORD)) {
            searchResultVO = getProductsByKeyword(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_SOURCE_CODE_LOOKUP)) {
            searchResultVO = getSourceCodeByValue(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_GET_ZIP_CODE)) {
            searchResultVO = getZipCode(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_GET_FLORIST)) {
            searchResultVO = getFlorist(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_GREETING_CARD_LOOKUP)) {
            searchResultVO = getGreetingCards(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_DELIVERY_POLICY_LOOKUP)) {
            searchResultVO = getDeliveryPolicy(arguments);
        }
        else if((command.getCommand()).equals(CommandConstants.SEARCH_GET_CALENDAR_LOOKUP)) {
            searchResultVO = getCalendar(arguments);
        }

        return searchResultVO;
	}

	public  void initialize()
	{}

	public  String toString()
	{
		return null;
	}

    private FTDSystemVO getDnisData(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            order.setOrderStatus(GeneralConstants.IN_PROCESS);

            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("pagename", (String) arguments.get("PAGE_NAME"));
            dataRequest.addArgument("company", (String) arguments.get("COMPANY"));

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_DNIS_SCRIPT);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("DNIS PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO getIntroductionData(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            // Set the DNIS Number to the Order
            order.setDnisCode(new Integer((String) arguments.get(ArgumentConstants.DNIS)));

            // ** SFMB **
            order.setLastOrderAction((String)arguments.get("callCenterId"));
            
            order.setCompanyId((String) arguments.get(ArgumentConstants.CALL_CENTER_ID));
            order.setCsrFirstName((String) arguments.get(ArgumentConstants.CSR_FIRST_NAME));
            order.setCsrLastName((String) arguments.get(ArgumentConstants.CSR_LAST_NAME));
            order.setCsrUserName((String) arguments.get(ArgumentConstants.USER_ID));

            // Set the initial start time of the order
            order.setProcessingTime(new Long(System.currentTimeMillis()));

            // Get company associated with DNIS
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("dnisId", (String) arguments.get(ArgumentConstants.DNIS));

            GenericDataService service =  this.getGenericDataService(DataConstants.VALIDATE_DNIS);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            NodeList nl = null;
            Element dnisElement = null;
            String xpath = "validateDnis/dnisInformation/dnis";
            XPathQuery q = new XPathQuery();

            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            try {
                dnisElement = (Element) nl.item(0);                
                order.setScriptCode(dnisElement.getAttribute("scriptCode"));
                order.setHeaderValue("dnisType", order.getScriptCode());

                // ** SFMB **
                order.setCompanyId(dnisElement.getAttribute("scriptId"));
                order.setCompanyName(dnisElement.getAttribute("scriptDescription"));
                if(order.getCompanyId().equals("SFMB"))
                {
                    order.setLastOrderAction("SFMBP");
                } 
                else if(order.getCompanyId().equals("GIFT"))
                {
                    order.setLastOrderAction("GIFTP");
                }                               
                else if(order.getCompanyId().equals("HIGH"))
                {
                    order.setLastOrderAction("BLOMP");
                }                               
                else if(order.getCompanyId().equals("FUSA"))
                {
                    order.setLastOrderAction("FUSAP");
                }                               
                else if(order.getCompanyId().equals("FDIRECT"))
                {
                    order.setLastOrderAction("FDIRECTP");
                }                               
                else if(order.getCompanyId().equals("FLORIST"))
                {
                    order.setLastOrderAction("FLORISTP");
                }                               
            }
            catch(Exception e) {
                this.getLogManager().error("No company was found for this DNIS.");
            }

            // Load required data for introduction page
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("pagename", (String) arguments.get(ArgumentConstants.PAGE_NAME));
            dataRequest.addArgument("displayExpired", (String) arguments.get(ArgumentConstants.DISPLAY_EXPIRED));
            dataRequest.addArgument("dnisId", (String) arguments.get(ArgumentConstants.DNIS));

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_INTRODUCTION_DATA);
            dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("INTRODUCTION PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO getProductByID(FTDArguments arguments)
	{
  
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;
        String productType = null;
        Element element = null;
        NodeList nl = null;
        String xpath = null;
        XPathQuery q = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
            HashMap pageData = new HashMap();

            boolean simpleSearch = false;
            String productId = (String) arguments.get("productId");

            // Check to see if Custom Item Product ID
            if ( productId != null &&
                 productId.equals("6611") )
            {
                order.setCrumbURL("Custom Item", (String) arguments.get("url") + "&cartItemNumber=" + (order.getCurrentCartNumber() + 1) + "&updateFlag=Y");
            }
            else
            {
                order.setCrumbURL("Product Detail", (String) arguments.get("url") + "&cartItemNumber=" + (order.getCurrentCartNumber() + 1) + "&updateFlag=Y");

                if ( arguments.get("search") != null &&
                     ((String) arguments.get("search")).equals("simpleproductsearch") )
                {
                    simpleSearch = true;
                }
            }

            OEParameters oeParms = searchUTIL.getGlobalParms();
            order.setGlobalParameters(oeParms);

            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, productId);
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());

            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }
            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument("deliveryDate", "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument("deliveryDate", df.format(deliveryDate));
            }

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT DETAIL PAGE VO");
            Element productDetailElementList = searchUTIL.pageBuilder(false, false, (XMLDocument)dataResponse.getDataVO().getData(), order, 1, null, null);
            XMLEncoder.addSection(document, productDetailElementList.getChildNodes());
            XMLDocument productList = (XMLDocument)dataResponse.getDataVO().getData();
                        
            if ( simpleSearch )
            {
                String countryId = null;
                String zipGnaddFlag = null;
                String zipFloralFlag = null;
                String gnaddLevel = null;

                nl = productDetailElementList.getElementsByTagName("product");
                Element productDetailElement = (Element) nl.item(0);
                if ( productDetailElement != null )
                {
                    productId = productDetailElement.getAttribute("productID");
                }

                /*  **********************************************************
                        This section set the flags to display the product
                        exclusion popup windows. These popups will always
                        be shown as the user specifically searched for the
                        product.
                    **********************************************************  */
                countryId = order.getSendToCustomer().getCountry();

                // retrieve the Global Parameter information
                xpath = "productList/globalParmsData/data";

                q = new XPathQuery();
                nl = q.query(productList, xpath);

                if ( nl.getLength() > 0 )
                {
                    element = (Element) nl.item(0);
                    gnaddLevel = element.getAttribute("gnaddLevel");
                }
                else
                {
                    gnaddLevel = "0";
                }

                // retrieve the product information
                xpath = "productList/products/product";
                //q = new XPathQuery();
                nl = q.query(productList, xpath);

                if (nl.getLength() > 0)
                {
                    element = (Element) nl.item(0);
                    zipGnaddFlag = element.getAttribute("zipGnaddFlag").trim();
                    zipFloralFlag = element.getAttribute("zipFloralFlag").trim();
                    productType = element.getAttribute("productType");
                }
                else
                {
                    zipGnaddFlag = "N";
                    zipFloralFlag = "Y";
                }

                // country is Canada, Puerto Rico, Virgin Islands, or International
                if ( countryId != null &&
                     (countryId.equals("CA") ||       // Canada
                     countryId.equals("PR") ||       // Puerto Rico
                     countryId.equals("VI") ))        // Virgin Islands
                {
                    // Zip code does not exist or is shut down,
                    // so can not delivery any products
                    if ( (zipGnaddFlag.equals("Y") && gnaddLevel.equals("0")) 
                         || zipFloralFlag.equals("N") )
                    {
                        // only display if has not been displayed before
                         pageData.put("displayNoProductPopup", "Y");

                            // set order flag so will not be displayed unless zip code changed later
                            //order.setDisplayNoProduct("N");
                            //order.setDisplayFloral("Y");
                            //order.setDisplaySpecialtyGift("Y");
                    }
                    // can only delivery floral products
                    else if ( productType != null && !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) &&
                             (!productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) &&
                              !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                    {
                        pageData.put("displayFloristPopup", "Y");

                        // set order flag so will not be displayed unless zip code changed later
                        //order.setDisplayNoProduct("Y");
                        //order.setDisplayFloral("N");
                        //order.setDisplaySpecialtyGift("Y");
                    }
                }
                
                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                else if ( (zipGnaddFlag.equals("Y") && gnaddLevel.equals("0"))
                          || zipFloralFlag.equals("N") )
                {
                    // only display if has not been displayed before
                    if ( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        pageData.put("displaySpecGiftPopup", "Y");

                        //order.setDisplayNoProduct("Y");
                        //order.setDisplayFloral("Y");
                        //order.setDisplaySpecialtyGift("N");
                    }
                }

                // Logic for same day gift pop-ups
                if(productType != null && 
                   (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                    productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                {
                    pageData.put("displayNoCodifiedFloristHasCommonCarrier", order.getDisplayNoCodifiedFloristHasCommonCarrier());
                    pageData.put("displayNoFloristHasCommonCarrier", order.getDisplayNoFloristHasCommonCarrier());
                    pageData.put("displayProductUnavailable", order.getDisplayProductUnavailable());
                    //pageData.put("displayNoProductPopup", order.getDisplayNoProduct());
                }                
            }
            else  
            {
                // retrieve the product information
                xpath = "productList/products/product";
                q = new XPathQuery();
                nl = q.query(productList, xpath);

                if (nl.getLength() > 0)
                {
                    element = (Element) nl.item(0);
                    productType = element.getAttribute("productType");
                }
            }

            // indicates whether product is codified special and should be displayed
            //String displayCodified = getDisplayCodifiedSpecial("productList", (XMLDocument)dataResponse.getDataVO().getData());
            //pageData.put("displayCodifiedSpecial",displayCodified.equals("Y")? "Y":"N" );
            //if( !displayCodified.equals("Y") && !displayCodified.equals("NA") )
            //{
            //    pageData.put("displayProductUnavailable", order.getDisplayProductUnavailable());
            //}

            // indicates whether product is codified special and should be displayed
            pageData.put("displayCodifiedSpecial", getDisplayCodifiedSpecial("productList", (XMLDocument)dataResponse.getDataVO().getData()));
            if(!((String)pageData.get("displayCodifiedSpecial")).equals("Y"))
            {
                pageData.put("displayProductUnavailable", order.getDisplayProductUnavailable());
            }

            if(productType != null && 
                !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) &&
                order.getDeliveryDate()!=null )
            {
                xpath = "productList/products/product";
                nl = q.query(productList, xpath);
                if (nl.getLength() > 0)
                {
                    element = (Element) nl.item(0);
                    boolean bBadDay = false;
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(order.getDeliveryDate());

                    //No fresh cut deliveries on monday
                    //ignore this for this release
                    //if( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)  
                    //    && cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY ) 
                    //{
                    //    String mondayDelivery = element.getAttribute("mondayDelFCFlag");
                    //    if( mondayDelivery!=null && !FTDUtil.convertStringToBoolean(mondayDelivery) )
                    //    {
                    //        bBadDay = true;
                    //        pageData.put("displayProductUnavailableStatus", "noMonday");
                    //    }
                    //}

                    //Excluded day
                    /*else*/ if ( !bBadDay && order.getDeliveryDate() != null )
                    {
                        String sExcludeDay;
                        boolean bExSunday = false;
                        boolean bExMonday = false;
                        boolean bExTuesday = false;
                        boolean bExWednesday = false;
                        boolean bExThursday = false;
                        boolean bExFriday = false;
                        boolean bExSaturday = false;
                        
                        sExcludeDay = element.getAttribute("exSundayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExSunday=true;
                        }

                        sExcludeDay = element.getAttribute("exMondayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExMonday=true;
                        }
                        
                        sExcludeDay = element.getAttribute("exTuesdayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExTuesday=true;
                        }
                        
                        sExcludeDay = element.getAttribute("exWednesdayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExWednesday=true;
                        }
                        
                        sExcludeDay = element.getAttribute("exThursdayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExThursday=true;
                        }
                        
                        sExcludeDay = element.getAttribute("exFridayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExFriday=true;
                        }
                        
                        sExcludeDay = element.getAttribute("exSaturdayFlag");
                        if( sExcludeDay!=null && FTDUtil.convertStringToBoolean(sExcludeDay) )
                        {
                            bExSaturday=true;
                        }

                        if( bExSunday && bExMonday && bExTuesday && bExWednesday &&
                            bExThursday && bExFriday && bExSaturday )
                        {
                            bBadDay=true;
                            pageData.put("displayProductUnavailableStatus", "exAlways");      
                        }
                        else
                        {
                            switch( cal.get(Calendar.DAY_OF_WEEK) )
                            {
                                case Calendar.MONDAY:
                                if( bExMonday )
                                {
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exMonday");
                                }
                                break;

                                case Calendar.TUESDAY:
                                if( bExTuesday )
                                {
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exTuesday");
                                }
                                break;

                                case Calendar.WEDNESDAY:
                                if( bExWednesday )
                                {
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exWednesday");
                                }
                                break;

                                case Calendar.THURSDAY:
                                if( bExThursday )
                                {
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exThursday");
                                }
                                break;

                                case Calendar.FRIDAY:
                                if( bExFriday )
                                {
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exFriday");
                                }
                                break;

                                case Calendar.SATURDAY:
                                if( bExSaturday )
                                {
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exSaturday");
                                }
                                break;

                                default:    //includes sunday
                                    bBadDay=true;
                                    pageData.put("displayProductUnavailableStatus", "exSunday");
                                break;
                            }   
                        }                        
                    }

                    if( bBadDay ) 
                    {
                        pageData.put("displayProductUnavailable", "Y");
                    }
                }
            }                  
            // if order is placed on last date of an available exception period, 
            // after order is past cutuff, mark the displayProductUnavailable as 'Y'
            String exceptionCodeStr = element.getAttribute("exceptionCode");
            String exceptionEndDateStr = element.getAttribute("exceptionEndDate");
            
            if (exceptionCodeStr!= null && exceptionCodeStr.equalsIgnoreCase("A")
                  && exceptionEndDateStr != null && !exceptionEndDateStr.equalsIgnoreCase("")
                  && productType != null && !productType.equalsIgnoreCase("")
            )
            {

                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                // get the exeception end date and set the time to zero
                Calendar exceptionEndDate = Calendar.getInstance();
                exceptionEndDate.setTime(sdf.parse(exceptionEndDateStr));
                exceptionEndDate.set(Calendar.HOUR_OF_DAY, 0);
                exceptionEndDate.set(Calendar.MINUTE, 0);
                exceptionEndDate.set(Calendar.SECOND, 0);
                exceptionEndDate.set(Calendar.MILLISECOND, 0);
                // get the day before the exeception end date
                Calendar dayBeforeExceptionEndDate = (Calendar) exceptionEndDate.clone();
                dayBeforeExceptionEndDate.add(Calendar.DATE, -1);
                // get today and set the time to 00:00:00
                Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);
                
                 // if today equals the day before the exception end days
                // see if it's before or after cutoff
                if (today.equals(dayBeforeExceptionEndDate)){
                    // vendor product // 
                     if (productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)
                         || productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)
                     ){
                        if (this.afterVendorCutoff(service, productType)){
                            pageData.put("displayProductUnavailable", "Y");
                        }
                    }
                }else if (today.equals(exceptionEndDate)){
                    // if floral or SDFC or SDG
                    if (productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FLORAL)
                        || productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDFC) 
                        || productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDG)
                    ){
                        if (this.afterFloristCutoff(service, productId, order.getSendToCustomer().getZipCode())){
                            pageData.put("displayProductUnavailable", "Y");
                        }
                    }else if (productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)
                         || productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)){
                        pageData.put("displayProductUnavailable", "Y");
                    }
                }
                else if (today.after(exceptionEndDate)){
                    pageData.put("displayProductUnavailable", "Y");
                }
            }//end of exception data
            //set order.setDisplayProductUnavailable() and 
            //pageData.put("displayProductUnavailable", order.getDisplayProductUnavailable());
            pageData.put("allowSubstitution", oeParms.getAllowSubstitution());
            XMLEncoder.addSection(document, "pageData", "data", pageData);

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("occasionIn", order.getOccasion());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("productId", productId);
            service =  this.getGenericDataService(DataConstants.SEARCH_GET_EXTRA_PRODUCT_DETAIL);
            dataResponse = service.executeRequest(dataRequest);

            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            //For some reason the DB is now returning addon prices as 5.0 rather then 5.
            //This function cleans up the price.
            cleanUp(document);

            HashMap orderDataMap = new HashMap();
            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                orderDataMap.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                orderDataMap.put("countryType", GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }

            if ( order.getSearchRecordCnt() > 0 )
            {
                orderDataMap.put("displaySpecialFee", order.getSpecialFeeFlag());
            }
            orderDataMap.put("zipCode", order.getSendToCustomer().getZipCode());
            orderDataMap.put("occasion", order.getOccasion());
            orderDataMap.put("country", order.getSendToCustomer().getCountry());
            orderDataMap.put("rewardType", order.getRewardType());

            // Added so we can go back to the shopping page
            if ( order.getDeliveryDateDisplay() != null )
            {
                orderDataMap.put("requestedDeliveryDateDisplay", order.getDeliveryDateDisplay());
            }
            orderDataMap.put("occasionDescription", searchUTIL.getDescriptionForCode(order.getOccasion(), SearchUTIL.GET_OCCASION_DESCRIPTION));
            orderDataMap.put("city", order.getSendToCustomer().getCity());
            orderDataMap.put("state", order.getSendToCustomer().getState());
            // End shopping page params

            if(arguments.get("soonerOverride") != null && ((String) arguments.get("soonerOverride")).equals("Y"))
            {
                Date overrideDate = FieldUtils.formatStringToUtilDate((String) order.getUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE));
                order.setDeliveryDate(overrideDate);
            }

            if ( order.getDeliveryDate() != null )
            {
                orderDataMap.put("requestedDeliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            XMLEncoder.addSection(document, "orderData", "data", orderDataMap);
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            //If no products were returned, check if it is an upsell product
            xpath = "productList/products/product";
            q = new XPathQuery();
            nl = q.query(productList, xpath);
            if (nl.getLength() == 0)
            {
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("upsellMasterId", productId);
                service =  this.getGenericDataService(DataConstants.SEARCH_GET_UPSELL_DETAIL);
                dataResponse = service.executeRequest(dataRequest);

                xpath = "upsellDetailList/upsellDetail/upsell";
                nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                if(nl.getLength() > 0) {
                    HashMap upsellMasterData = new HashMap();

                    Iterator upsellIterator = order.getUpsellMap().keySet().iterator();
                    while(upsellIterator.hasNext())
                    {
                        order.getUpsellMap().remove((String) upsellIterator.next());
                    }

                    String upsellProductId = null;
                    String upsellProductName = null;
                    Element upsellElement = null;
                    String skipUpsell = "N";
                    String noneAvailable = "Y";
                    String baseAvailable = "Y";
                    String showScripting = "N";
                    int upsellSequence = 0;

                    document = XMLEncoder.createXMLDocument("UPSELL PAGE VO");
                    order.setSearchCurrentPage(new Integer(1));
                    arguments.put(ArgumentConstants.UPSELL_EXISTS, "Y");

                    for(int i=0; i < nl.getLength(); i++) {
                        upsellElement = (Element) nl.item(i);
                        upsellProductId = upsellElement.getAttribute("upsellDetailId");
                        upsellProductName = upsellElement.getAttribute("upsellDetailName");
                        upsellProductName = upsellProductName.replaceAll("\\<.*?>","");

                        dataRequest = new FTDDataRequest();
                        dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
                        dataRequest.addArgument("company", order.getCompanyId());
                        dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, upsellProductId);
                        dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                        dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
                        dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());

                        if ( order.getSendToCustomer().isDomesticFlag() )
                        {
                            dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
                        }
                        else
                        {
                            dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                        }
                        deliveryDate = order.getDeliveryDate();
                        if (deliveryDate == null) {
                            dataRequest.addArgument("deliveryDate", "");
                        } else {
                            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                            dataRequest.addArgument("deliveryDate", df.format(deliveryDate));
                        }

                        service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
                        dataResponse = service.executeRequest(dataRequest);

                        productDetailElementList = searchUTIL.pageBuilder(true, true, (XMLDocument)dataResponse.getDataVO().getData(), order, 1, null, null);

                        Element productDetailElement = (Element) productDetailElementList.getElementsByTagName("product").item(0);
                        if (productDetailElement == null) {
                          continue;
                        } else {
                          upsellSequence++;
                        }
                        productDetailElement.setAttribute("upsellSequence", new Integer(upsellSequence).toString());
                        productDetailElement.setAttribute("upsellProductName", upsellProductName);

                        createUpsellDetail(productDetailElement, (XMLDocument)dataResponse.getDataVO().getData(), order);

                        // Check if upsell page should be skipped
                        if(productDetailElement.getAttribute("upsellSequence") != null && productDetailElement.getAttribute("upsellSequence").equals("1"))
                        {
                            upsellMasterData.put("masterId", upsellElement.getAttribute("upsellMasterId"));
                            upsellMasterData.put("masterName", upsellElement.getAttribute("upsellMasterName"));
                            upsellMasterData.put("masterDescription", upsellElement.getAttribute("upsellMasterDescription"));
                            upsellMasterData.put("masterStatus", upsellElement.getAttribute("upsellMasterStatus"));

                            order.addUpsellDetail(ArgumentConstants.UPSELL_BASE_ID, upsellElement.getAttribute("upsellDetailId"));
                            order.addUpsellDetail(ArgumentConstants.UPSELL_BASE_DATE, productDetailElement.getAttribute("deliveryDate"));
                            order.addUpsellDetail(ArgumentConstants.UPSELL_BASE_CARRIER, productDetailElement.getAttribute("shipMethodCarrier"));
                            order.addUpsellDetail(ArgumentConstants.UPSELL_BASE_NAME, productDetailElement.getAttribute("novatorName"));

                            if((productDetailElement.getAttribute("status") != null
                               && productDetailElement.getAttribute("status").equals("U"))
                               || (productDetailElement.getAttribute("specialUnavailable") != null
                               && productDetailElement.getAttribute("specialUnavailable").equals("Y")))
                            {
                                skipUpsell = "N";
                                baseAvailable = "N";
                            }
                            else
                            {
                                skipUpsell = upsellProductId;
                                noneAvailable = "N";
                            }
                        }
                        else
                        {
                            if(productDetailElement.getAttribute("status") != null
                               && productDetailElement.getAttribute("status").equals("A")
                               && productDetailElement.getAttribute("specialUnavailable") != null
                               && productDetailElement.getAttribute("specialUnavailable").equals("N"))
                            {
                                skipUpsell = "N";
                                noneAvailable = "N";

                                // If the last product is available show scripting
                                if(i+1 == nl.getLength()) {
                                    showScripting = "Y";
                                }
                            }
                        }

                        XMLEncoder.addSection(document, productDetailElementList.getChildNodes());
                    }

                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("pagename", "upsell");
                    dataRequest.addArgument("company", order.getCompanyId());
                    service =  this.getGenericDataService(DataConstants.SEARCH_GET_UPSELL_EXTRA);
                    dataResponse = service.executeRequest(dataRequest);

                    if(skipUpsell.equals("N"))
                    {
                        order.removeCrumbURL("Product Detail");
                        order.setCrumbURL("Upsell Detail", (String) arguments.get("url"));
                    }

                    arguments.put(ArgumentConstants.UPSELL_SKIP, skipUpsell);
                    upsellMasterData.put("skipUpsell", skipUpsell);
                    upsellMasterData.put("noneAvailable", noneAvailable);
                    upsellMasterData.put("baseAvailable", baseAvailable);
                    upsellMasterData.put("showScripting", showScripting);

                    XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());
                    XMLEncoder.addSection(document, "orderData", "data", orderDataMap);
                    XMLEncoder.addSection(document, "upsellMaster", "upsellDetail", upsellMasterData);
                    XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
                    XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());
                }
            }
            //document.print(System.out);
            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO getProductsByCategory(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);

            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT LIST VO");
            String pageNumber = (String) arguments.get("pageNumber");

            // Initial search hit loads all products matching the query for all pages
            if ( (pageNumber == null) || (pageNumber.equals("init")) )
            {
                order.setCrumbURL("Product List", (String) arguments.get("url"));

                String categoryName = "";
                String parentCategoryName = "";
                String indexId = (String) arguments.get("indexId");
                FTDDataRequest dataRequest = new FTDDataRequest();
                GenericDataService service = null;
                FTDDataResponse dataResponse = null;

                dataRequest.addArgument(ArgumentConstants.SEARCH_INDEX_ID, indexId);
                service =  this.getGenericDataService(DataConstants.SEARCH_GET_INDEX_DETAILS);
                dataResponse = service.executeRequest(dataRequest);
                String xpath = "indexList/indexDetail/data";
                XPathQuery q = new XPathQuery();
                Element indexElement = null;
                NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

                if (nl.getLength() > 0)
                {
                    indexElement = (Element) nl.item(0);
                    categoryName = indexElement.getAttribute("indexName");
                    if(indexElement.getAttribute("parentIndexName") != null && !indexElement.getAttribute("parentIndexName").equals("")) {
                        parentCategoryName = indexElement.getAttribute("parentIndexName");
                        categoryName = " (" + categoryName + ")";
                    }
                }

/*
                // Get products by index args
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument(ArgumentConstants.SEARCH_INDEX_ID, indexId);
                dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                String orderDeliveryZipCode = order.getSendToCustomer().getZipCode();
                if(orderDeliveryZipCode.equals("N/A"))
                {
                    orderDeliveryZipCode = "";
                }
                dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, orderDeliveryZipCode);
                dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String) arguments.get("pricePointId"));
                dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                dataRequest.addArgument("scriptCode", order.getScriptCode());

                // build the end date for the delivery date range

                OEParameters oeParms = searchUTIL.getGlobalParms();
                order.setGlobalParameters(oeParms);
                Calendar deliveryEndDate = Calendar.getInstance();
                deliveryEndDate.add(Calendar.DATE, oeParms.getDeliveryDaysOut());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_END_DATE, dateFormat.format(deliveryEndDate.getTime()));

                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
                }
                else
                {
                    dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
                }

                //System.out.println(dataRequest.getArguments().toString());
                service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);
                dataResponse = service.executeRequest(dataRequest);

                XMLDocument searchResultsIndex = (XMLDocument)dataResponse.getDataVO().getData();
*/

                // NEEDS TO BE CLEANED UP
                OEParameters oeParms = searchUTIL.getGlobalParms();
                order.setGlobalParameters(oeParms);
                Calendar deliveryEndDate = Calendar.getInstance();
                deliveryEndDate.add(Calendar.DATE, DeliveryDateUTIL.getDeliveryDaysOut(order, oeParms));//oeParms.getDeliveryDaysOut());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                String endDateStr = dateFormat.format(deliveryEndDate.getTime());

                String orderDeliveryZipCode = order.getSendToCustomer().getZipCode();
                if(orderDeliveryZipCode.equals("N/A"))
                {
                    orderDeliveryZipCode = "";
                }

                String intDomFlag = null;
                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    intDomFlag = GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC;
                }
                else
                {
                    intDomFlag = GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL;
                }
                        
                // set arguments
                Map searchArgs = new HashMap();
                searchArgs.put(ArgumentConstants.SEARCH_INDEX_ID, indexId);
                searchArgs.put(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                searchArgs.put(ArgumentConstants.SEARCH_ZIP_CODE, orderDeliveryZipCode);
                searchArgs.put(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String) arguments.get("pricePointId"));
                searchArgs.put(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, intDomFlag);
                searchArgs.put(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                searchArgs.put("scriptCode", order.getScriptCode());
                searchArgs.put(ArgumentConstants.SEARCH_DELIVERY_END_DATE, endDateStr);
                        
                // perform search
                XMLDocument searchResultsIndex = SearchUTIL.preformProductListSearch(searchArgs, DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);
                        
                // save search criteria
                String searchCriteria = searchUTIL.getSearchCriteriaString(searchArgs);
                order.setSearchCriteria(searchCriteria);
                
                SearchUTIL.paginate(searchResultsIndex,1, GeneralConstants.PRODUCTS_PER_PAGE);
                order.setSearchResults(searchResultsIndex);

                XMLDocument searchResultsPerPage= searchUTIL.getProductsByIDs(searchResultsIndex, "1", arguments, order );

                searchResultsPerPage = searchUTIL.sortProductList("sortDisplayOrder", searchResultsPerPage);

                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, GeneralConstants.PRODUCTS_PER_PAGE);

                Element data = (Element) searchResultsIndex.selectNodes("//pageData/data[@name='totalPages']").item(0);
                order.setSearchPageCount(new Integer(data.getAttribute("value")));

                // current page
                order.setSearchCurrentPage(new Integer(1));

                // Encode products to xml for product list
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage, order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
            }
            else
            {
                XMLDocument searchResultsIndex = order.getSearchResults();

                // If search results are null then perform another search.
                // This could happen if the session was loaded from the database and it did not already
                // exist in the app server cache
                if(searchResultsIndex == null)
                {
                    Map searchCiteria = SearchUTIL.getSearchCriteriaMap(order.getSearchCriteria());
                    searchResultsIndex = SearchUTIL.preformProductListSearch(searchCiteria, DataConstants.SEARCH_GET_PRODUCTS_BY_INDEX);
                    SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                    order.setSearchResults(searchResultsIndex);
                }

                // if we are sorting by price then we need to paginate the sorted list and save it
                // to the order
                if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                {

                    searchResultsIndex = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsIndex);
                    SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                    order.setSearchResults(searchResultsIndex);
                }

                // Get the product details for the current page
                XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, pageNumber, arguments, order );

                // if we are not sorting by anything then the default sort is by
                // the order in the product index
                if(arguments.get("sortType") == null || ((String) arguments.get("sortType")).equals(""))
                {
                    searchResultsPerPage = searchUTIL.sortProductList("sortDisplayOrder", searchResultsPerPage);
                }
                // else sort by whatever type is passed in the arguments
                else if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                {
                    searchResultsPerPage = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsPerPage);
                }

                int iCurrentPage = Integer.parseInt(pageNumber);
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, GeneralConstants.PRODUCTS_PER_PAGE);

                // Encode products to xml for product list
                order.setSearchCurrentPage(new Integer(pageNumber));
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage , order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
            }


            // Load price points from database
            FTDDataRequest dataRequest = new FTDDataRequest();
            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRICE_POINTS);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            // Encode previously loaded price points
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            // Encode page data for product list
            HashMap pageDataMap = new HashMap();
            pageDataMap.put("totalPages", order.getSearchPageCount().toString());
            pageDataMap.put("currentPage", order.getSearchCurrentPage().toString());
            pageDataMap.put("rewardType", order.getRewardType());

            if(order.getDeliveryDate() != null) {
                pageDataMap.put("requestedDeliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }

            XMLEncoder.addSection(document, "pageData", "data", pageDataMap);
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            systemVO.setXML(document);

        } catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO getProductsBySearch(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try {

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT LIST VO");
            String pageNumber = (String) arguments.get("pageNumber");

            // Initial search hit loads all products matching the query for all pages
            if(pageNumber == null || (pageNumber).equals("init"))
            {
                order.setCrumbURL("Product List", (String) arguments.get("url"));

/*
                // Get products by index args
                FTDDataRequest dataRequest = new FTDDataRequest();

                if ( arguments.get("advSearchType") != null && ((String)arguments.get("advSearchType")).equals("searchby") )
                {
                    dataRequest.addArgument("inOccasionId", (String) arguments.get("occasion"));
                    dataRequest.addArgument("inCategory", (String) arguments.get("category"));
                    dataRequest.addArgument("inRecipient", (String) arguments.get("recipient"));
                    dataRequest.addArgument("inColor", null);
                    dataRequest.addArgument("inName", null);
                }
                else if ( arguments.get("advSearchType") != null && ((String)arguments.get("advSearchType")).equals("flower") )
                {
                    dataRequest.addArgument("inColor", (String) arguments.get("color"));
                    dataRequest.addArgument("inName", (String) arguments.get("name"));
                    dataRequest.addArgument("inOccasionId", null);
                    dataRequest.addArgument("inCategory", null);
                    dataRequest.addArgument("inRecipient", null);
                }
                else
                {
                    dataRequest.addArgument("inOccasionId", (String) arguments.get("occasion"));
                    dataRequest.addArgument("inCategory", (String) arguments.get("category"));
                    dataRequest.addArgument("inRecipient", (String) arguments.get("recipient"));
                    dataRequest.addArgument("inColor", (String) arguments.get("color"));
                    dataRequest.addArgument("inName", (String) arguments.get("name"));
                }

                dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
                dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String) arguments.get("pricePointId"));
                dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                dataRequest.addArgument("scriptCode", order.getScriptCode());

                // build the end date for the delivery date range
                OEParameters oeParms = searchUTIL.getGlobalParms();
                order.setGlobalParameters(oeParms);
                Calendar deliveryEndDate = Calendar.getInstance();
                deliveryEndDate.add(Calendar.DATE, oeParms.getDeliveryDaysOut());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument(ArgumentConstants.SEARCH_DELIVERY_END_DATE, dateFormat.format(deliveryEndDate.getTime()));

                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, "D");
                }
                else
                {
                    dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, "I");
                }

                GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCTS_BY_SEARCH);

                FTDDataResponse dataResponse = service.executeRequest(dataRequest);

                XMLDocument searchResultsIndex = (XMLDocument)dataResponse.getDataVO().getData();
*/
                String occasionId = null;
                String category = null;
                String recipient = null;
                String color = null;
                String name = null;
                
                if ( arguments.get("advSearchType") != null && ((String)arguments.get("advSearchType")).equals("searchby") )
                {
                    occasionId = (String) arguments.get("occasion");
                    category = (String) arguments.get("category");
                    recipient = (String) arguments.get("recipient");
                    color = null;
                    name = null;
                }
                else if ( arguments.get("advSearchType") != null && ((String)arguments.get("advSearchType")).equals("flower") )
                {
                    color = (String) arguments.get("color");
                    name = (String) arguments.get("name");
                    occasionId = null;
                    category = null;
                    recipient = null;
                }
                else
                {
                    occasionId = (String) arguments.get("occasion");
                    category = (String) arguments.get("category");
                    recipient = (String) arguments.get("recipient");
                    color = (String) arguments.get("color");
                    name = (String) arguments.get("name");
                }

                // build the end date for the delivery date range
                OEParameters oeParms = searchUTIL.getGlobalParms();
                order.setGlobalParameters(oeParms);
                Calendar deliveryEndDate = Calendar.getInstance();
                deliveryEndDate.add(Calendar.DATE, DeliveryDateUTIL.getDeliveryDaysOut(order, oeParms));//oeParms.getDeliveryDaysOut());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                String endDateStr = dateFormat.format(deliveryEndDate.getTime());

                String inDomFlag = null;
                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    inDomFlag =  "D";
                }
                else
                {
                    inDomFlag =  "I";
                }                

                // set arguments
                Map searchArgs = new HashMap();
                searchArgs.put("inOccasionId", occasionId);
                searchArgs.put("inCategory", category);
                searchArgs.put("inRecipient", recipient);
                searchArgs.put("inColor", color);
                searchArgs.put("inName", name);
                
                searchArgs.put(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                searchArgs.put(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
                searchArgs.put(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String) arguments.get("pricePointId"));
                searchArgs.put(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, inDomFlag);
                searchArgs.put(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                searchArgs.put("scriptCode", order.getScriptCode());
                searchArgs.put(ArgumentConstants.SEARCH_DELIVERY_END_DATE, endDateStr);
                Date deliveryDate = order.getDeliveryDate();
                if (deliveryDate == null) {
                    searchArgs.put(ArgumentConstants.SEARCH_DELIVERY_DATE, "");
                } else {
                    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                    searchArgs.put(ArgumentConstants.SEARCH_DELIVERY_DATE, df.format(deliveryDate));
                }
                        
                // perform search
                XMLDocument searchResultsIndex = searchUTIL.preformProductListSearch(searchArgs, DataConstants.SEARCH_GET_PRODUCTS_BY_SEARCH);
                        
                // save search criteria
                String searchCriteria = searchUTIL.getSearchCriteriaString(searchArgs);
                order.setSearchCriteria(searchCriteria);


                SearchUTIL.paginate(searchResultsIndex,1, GeneralConstants.PRODUCTS_PER_PAGE);
                order.setSearchResults(searchResultsIndex);

                XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, "1", arguments, order );
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, GeneralConstants.PRODUCTS_PER_PAGE);

                Element data = (Element) searchResultsIndex.selectNodes("//pageData/data[@name='totalPages']").item(0);
                order.setSearchPageCount(new Integer(data.getAttribute("value")));

                // current page
                order.setSearchCurrentPage(new Integer(1));

                // Encode products to xml for product list
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage, order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
            }
            else
            {
                XMLDocument searchResultsIndex = order.getSearchResults();
                // If search results are null then perform another search.
                // This could happen if the session was loaded from the database and it did not already
                // exist in the app server cache
                if(searchResultsIndex == null)
                {
                    Map searchCiteria = SearchUTIL.getSearchCriteriaMap(order.getSearchCriteria());
                    searchResultsIndex = SearchUTIL.preformProductListSearch(searchCiteria, DataConstants.SEARCH_GET_PRODUCTS_BY_SEARCH);
                    SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                    order.setSearchResults(searchResultsIndex);                    
                }                

                // if we are sorting by price then we need to paginate the sorted list and save it
                // to the order
                if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                {

                    searchResultsIndex = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsIndex);
                    SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                    order.setSearchResults(searchResultsIndex);
                }

                // Get the product details for the current page
                XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, pageNumber, arguments, order );

                // sort by whatever type is passed in the arguments
                if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                {
                    searchResultsPerPage = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsPerPage);
                }

                int iCurrentPage = Integer.parseInt(pageNumber);
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, GeneralConstants.PRODUCTS_PER_PAGE);

                // Encode products to xml for product list
                order.setSearchCurrentPage(new Integer(pageNumber));
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage , order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
            }


            // Load price points from database
            FTDDataRequest dataRequest = new FTDDataRequest();
            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRICE_POINTS);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            // Encode page data for product list
            HashMap pageDataMap = new HashMap();
            pageDataMap.put("totalPages", order.getSearchPageCount().toString());
            pageDataMap.put("currentPage", order.getSearchCurrentPage().toString());
            pageDataMap.put("rewardType", order.getRewardType());
            if(order.getDeliveryDate() != null) {
                pageDataMap.put("requestedDeliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            systemVO.setXML(document);
        } catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO getProductsByKeyword(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT LIST VO");
            String pageNumber = (String) arguments.get("pageNumber");

            // Initial search hit loads all products matching the query for all pages
            if(pageNumber == null || (pageNumber).equals("init") || pageNumber.equals(""))
            {
                order.setCrumbURL("Product List", (String) arguments.get("url"));

                // Get products by index args
                String searchResults  = null;
                StringBuffer sb = new StringBuffer();

                KeywordSearch kws = new KeywordSearch(this.getLogManager());
                String strQuery = (String) arguments.get("searchKeyword");
                searchResults = kws.send(strQuery, order.getCompanyId());
                
                if(searchResults.indexOf("CON") != -1)
                {
                    searchResults = searchResults.substring(0, searchResults.length() - 3);
                    this.getLogManager().debug(searchResults);
                    // Parse product IDs
                    XMLDocument domDoc = XPathQuery.createDocument(searchResults);

                    NodeList nl = null;
                    XPathQuery q = new XPathQuery();
                    String xpath = "keywordSearch/products/product";
                    nl = q.query(domDoc, xpath);
                    if(nl.getLength() > 0)
                    {
                        // We have products
                        for (int i = 0; i < nl.getLength() && i < 255; i++)
                        {
                            Element node = (Element)nl.item(i);
                            sb.append("'");
                            sb.append(node.getAttribute("productId"));
                            sb.append("'");
                            sb.append(",");
                        }

                        // Remove the last comma
                        if(sb.length() > 0)
                        {
                            sb.deleteCharAt(sb.length()-1);
                        }
                    }
                    else
                    {
                        // We do not have products
                        searchResults = "";
                        sb = new StringBuffer("''");
                    }
                }
                else
                {
                    searchResults = "";
                    sb = new StringBuffer("''");
                }

/*                
                dataRequest = new FTDDataRequest();
                dataRequest.addArgument("searchKeywordList", sb.toString());
                dataRequest.addArgument(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String) arguments.get("pricePointId"));
                dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
                dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                dataRequest.addArgument("scriptCode", order.getScriptCode());

                // build the end date for the delivery date range
                OEParameters oeParms = searchUTIL.getGlobalParms();
                order.setGlobalParameters(oeParms);
                Calendar deliveryEndDate = Calendar.getInstance();
                deliveryEndDate.add(Calendar.DATE, oeParms.getDeliveryDaysOut());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument("inDeliveryEndDate", dateFormat.format(deliveryEndDate.getTime()));

                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    dataRequest.addArgument("domesticIntlFlag", "D");
                }
                else
                {
                    dataRequest.addArgument("domesticIntlFlag", "I");
                }

                try
                {
                    Set mapKeys = dataRequest.getArguments().keySet();
                    Iterator it = mapKeys.iterator();
                    while(it.hasNext())
                    {
                        String key = (String)it.next();
                        String value = (String)dataRequest.getArguments().get(key);
                        if(value == null) value = "null";
                        this.getLogManager().debug("Keyword Search Param: " + key + " = " + value);
                    }
                }
                catch(Exception ex)
                {
                    this.getLogManager().error(ex);
                }


                service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCTS_BY_KEYWORD);
                dataResponse = service.executeRequest(dataRequest);

                XMLDocument searchResultsIndex = (XMLDocument)dataResponse.getDataVO().getData();
*/


                // build the end date for the delivery date range
                OEParameters oeParms = searchUTIL.getGlobalParms();
                order.setGlobalParameters(oeParms);
                Calendar deliveryEndDate = Calendar.getInstance();
                deliveryEndDate.add(Calendar.DATE, DeliveryDateUTIL.getDeliveryDaysOut(order, oeParms));//oeParms.getDeliveryDaysOut());
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                String endDateStr = dateFormat.format(deliveryEndDate.getTime());

                String inDomFlag = null;
                if ( order.getSendToCustomer().isDomesticFlag() )
                {
                    inDomFlag = "D";
                }
                else
                {
                    inDomFlag = "I";
                }

                // set arguments
                Map searchArgs = new HashMap();
                searchArgs.put("searchKeywordList", sb.toString());
                searchArgs.put(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
                searchArgs.put(ArgumentConstants.SEARCH_ZIP_CODE, order.getSendToCustomer().getZipCode());
                searchArgs.put(ArgumentConstants.SEARCH_PRICE_POINT_ID, (String) arguments.get("pricePointId"));
                searchArgs.put(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, inDomFlag);
                searchArgs.put(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
                searchArgs.put("scriptCode", order.getScriptCode());
                searchArgs.put(ArgumentConstants.SEARCH_DELIVERY_END_DATE, endDateStr);

                try
                {
                    Set mapKeys = searchArgs.keySet();
                    Iterator it = mapKeys.iterator();
                    while(it.hasNext())
                    {
                        String key = (String)it.next();
                        String value = (String)searchArgs.get(key);
                        if(value == null) value = "null";
                        this.getLogManager().debug("Keyword Search Param: " + key + " = " + value);
                    }
                }
                catch(Exception ex)
                {
                    this.getLogManager().error(ex);
                }                
                        
                // perform search
                XMLDocument searchResultsIndex = searchUTIL.preformProductListSearch(searchArgs, DataConstants.SEARCH_GET_PRODUCTS_BY_KEYWORD);
                        
                // save search criteria
                String searchCriteria = searchUTIL.getSearchCriteriaString(searchArgs);
                order.setSearchCriteria(searchCriteria);        
                
                SearchUTIL.paginate(searchResultsIndex,1, GeneralConstants.PRODUCTS_PER_PAGE);
                order.setSearchResults(searchResultsIndex);

                XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, "1", arguments, order );
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, GeneralConstants.PRODUCTS_PER_PAGE);

                Element data = (Element) searchResultsIndex.selectNodes("//pageData/data[@name='totalPages']").item(0);
                order.setSearchPageCount(new Integer(data.getAttribute("value")));
                order.setSearchCurrentPage(new Integer(1));

                // Encode products to xml for product list
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage, order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
            }
            else
            {
                XMLDocument searchResultsIndex = order.getSearchResults();
                // If search results are null then perform another search.
                // This could happen if the session was loaded from the database and it did not already
                // exist in the app server cache
                if(searchResultsIndex == null)
                {
                    Map searchCiteria = SearchUTIL.getSearchCriteriaMap(order.getSearchCriteria());
                    searchResultsIndex = SearchUTIL.preformProductListSearch(searchCiteria, DataConstants.SEARCH_GET_PRODUCTS_BY_KEYWORD);
                    SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                    order.setSearchResults(searchResultsIndex);                    
                }                

                // if we are sorting by price then we need to paginate the sorted list and save it
                // to the order
                if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                {

                    searchResultsIndex = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsIndex);
                    SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), GeneralConstants.PRODUCTS_PER_PAGE);
                    order.setSearchResults(searchResultsIndex);
                }

                // Get the product details for the current page
                XMLDocument searchResultsPerPage = searchUTIL.getProductsByIDs(searchResultsIndex, pageNumber, arguments, order );

                // sort by whatever type is passed in the arguments
                if(arguments.get("sortType") != null && !((String) arguments.get("sortType")).equals(""))
                {
                    searchResultsPerPage = searchUTIL.sortProductList((String) arguments.get("sortType"), searchResultsPerPage);
                }

                int iCurrentPage = Integer.parseInt(pageNumber);
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, GeneralConstants.PRODUCTS_PER_PAGE);

                // Encode products to xml for product list
                order.setSearchCurrentPage(new Integer(pageNumber));
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(true, true, searchResultsPerPage , order, GeneralConstants.PRODUCTS_PER_PAGE, null, null).getChildNodes());
            }


            // Load price points from database
            FTDDataRequest dataRequest = new FTDDataRequest();
            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRICE_POINTS);
            FTDDataResponse  dataResponse = service.executeRequest(dataRequest);
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            // Encode page data for product list
            HashMap pageDataMap = new HashMap();
            pageDataMap.put("totalPages", order.getSearchPageCount().toString());
            pageDataMap.put("currentPage", order.getSearchCurrentPage().toString());
            pageDataMap.put("rewardType", order.getRewardType());
            if(order.getDeliveryDate() != null) {
                pageDataMap.put("requestedDeliveryDate", FieldUtils.formatUtilDateToString(order.getDeliveryDate()));
            }
            XMLEncoder.addSection(document, "pageData", "data", pageDataMap);

            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());
            //XMLEncoder.addSection(shoppingUTIL.transformBreadCrumbsToXML(order.getBreadCrumbList()).getChildNodes());

            systemVO.setXML(document);

        } catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    private FTDSystemVO getCustomer(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            String searchConst;
/*
            String searchString = (String)arguments.get("institutionName");
            if(searchString != null && searchString.length() > 0)
            {
                //lookup the Institution
                dataRequest.addArgument("institutionName", (String) arguments.get("institutionName"));
                searchConst = DataConstants.SEARCH_GET_INSTITUTION;
            }
            else
            {
*/
                // lookup the customer
                dataRequest.addArgument("customerId", (String) arguments.get("customerId"));
                dataRequest.addArgument("institutionName", (String) arguments.get("institutionName"));
                dataRequest.addArgument("address1", (String) arguments.get("address1"));
                dataRequest.addArgument("address2", (String) arguments.get("address2"));
                dataRequest.addArgument("inCity", (String) arguments.get("inCity"));
                dataRequest.addArgument("inState", (String) arguments.get("inState"));
                dataRequest.addArgument("inZipCode", (String) arguments.get("inZipCode"));
                dataRequest.addArgument("inPhone", (String) arguments.get("inPhone"));
                dataRequest.addArgument("dnisType", null);
                searchConst = DataConstants.SEARCH_GET_CUSTOMER;
//            }


            GenericDataService service =  this.getGenericDataService(searchConst);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("CUSTOMER LOOKUP PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
	}

    private FTDSystemVO getInstitution(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            String searchConst;

            String name = (String) arguments.get("institutionName");
            if(name == null) name = "";
            String address1 = (String) arguments.get("address1");
            if(address1 == null) address1 = "";
            String inCity = (String) arguments.get("inCity");
            if(inCity == null) inCity = "";
            String inState = (String) arguments.get("inState");
            if(inState == null) inState = "";
            String inZipCode = (String) arguments.get("inZipCode");
            if(inZipCode == null) inZipCode = "";
            String inPhone = (String) arguments.get("inPhone");
            if(inPhone == null) inPhone = "";

            // lookup the Institution
            dataRequest.addArgument("institutionName", name);
            dataRequest.addArgument("address", address1);
            dataRequest.addArgument("city", inCity);
            dataRequest.addArgument("state", inState);
            dataRequest.addArgument("zip", inZipCode);
            dataRequest.addArgument("phoneNumber", inPhone);
            dataRequest.addArgument("instType", (String) arguments.get("instType"));
            dataRequest.addArgument("dnisType", null);
            searchConst = DataConstants.SEARCH_GET_INSTITUTION;

            GenericDataService service =  this.getGenericDataService(searchConst);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("CUSTOMER LOOKUP PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
	}

    private FTDSystemVO getSourceCodeByValue(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("searchValue", ((String) arguments.get("searchValue")).toUpperCase());
            dataRequest.addArgument("displayExpired", "Y");
            dataRequest.addArgument("dnisType", (String) arguments.get("dnisType"));

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_SOURCE_BY_VALUE);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("SOURCE CODE LOOKUP PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
            XMLEncoder.addSection(document, (searchUTIL.convertLookupAnchors((XMLDocument)dataResponse.getDataVO().getData())).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
	}

    private FTDSystemVO updateProductDates(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try {
            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            order.getSendToCustomer().setZipCode((String) arguments.get("zipcode"));
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            String argMonth = (String) arguments.get(ArgumentConstants.CALENDAR_MONTH);
            String argYear = (String) arguments.get(ArgumentConstants.CALENDAR_YEAR);

            // Calculate new delivery dates based on new zip code
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("zipCode", order.getSendToCustomer().getZipCode());

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_TIMEZONE_FOR_ZIP);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);
            XMLDocument xmlResponse =(XMLDocument)dataResponse.getDataVO().getData();

            // Get the timezone from the xml for processing delivery dates
            String timeZone = null;

            if(order.getSendToCustomer().isDomesticFlag()) {
                Node node = null;
                NodeList nl = null;
                String xpath = "timezoneDataSet/timezoneData/data/@timeZone";
                XPathQuery q = new XPathQuery();

                nl = q.query(xmlResponse, xpath);

                if (nl.getLength() > 0)
                {
                  for(int i=0; i < nl.getLength(); i++)
                  {
                    node = nl.item(i);
                    timeZone = node.getNodeValue();
                  }
                }
                else {
                    // No valid zip code was entered
                    timeZone = GeneralConstants.OE_TIMEZONE_DEFAULT;
                }
            }

            // Encode the delivery dates to the XML (default to HST)
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(this.getLogManager());
            OEDeliveryDateParm parms = new OEDeliveryDateParm();
            parms.setZipTimeZone(timeZone);
            parms.setOrder(order);
            parms.setFloralServiceCharge(order.getDomesticServiceFee());

            XMLDocument deliveryDates = deliveryDateUtil.getOrderDeliveryDates(parms);
            XMLDocument calendarDates = deliveryDateUtil.getCalendarDeliveryDates(parms);

            //order.setDeliveryDates(deliveryDates);

            // Rebuild the product detail page
            order.setCrumbURL("Product Detail", (String) arguments.get("url"));

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("company", order.getCompanyId());
            dataRequest.addArgument("productId", (String) arguments.get("productId"));
            dataRequest.addArgument("sourceCode", order.getSourceCode());
            dataRequest.addArgument("zipCode", order.getSendToCustomer().getZipCode());
            if(order.getSendToCustomer().isDomesticFlag())
                dataRequest.addArgument("domesticIntlFlag", "D");
            else
                dataRequest.addArgument("domesticIntlFlag", "I");

            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument("deliveryDate", "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument("deliveryDate", df.format(deliveryDate));
            }

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("PRODUCT DETAIL PAGE VO");

            XMLEncoder.addSection(document, deliveryDates.getChildNodes());
            XMLEncoder.addSection(document, calendarDates.getChildNodes());

            XMLEncoder.addSection(document, searchUTIL.pageBuilder(false, true, (XMLDocument)dataResponse.getDataVO().getData(), order, 1, argMonth, argYear).getChildNodes());

            // value set in searchUTIL.pageBuilder
            HashMap pageData = new HashMap();
            // indicates whether product is codified special and should be displayed
            pageData.put("displayCodifiedSpecial", getDisplayCodifiedSpecial("productList", (XMLDocument)dataResponse.getDataVO().getData()));
            if(!((String)pageData.get("displayCodifiedSpecial")).equals("Y"))
            {
                pageData.put("displayProductUnavailable", order.getDisplayProductUnavailable());
            }

            XMLEncoder.addSection(document, "pageData", "data", pageData);

            dataRequest = new FTDDataRequest();
            dataRequest.addArgument("occasionIn", order.getOccasion());
            dataRequest.addArgument("pagename", (String) arguments.get("pagename"));
            dataRequest.addArgument("company", order.getCompanyId());
            service =  this.getGenericDataService(DataConstants.SEARCH_GET_ADDON_LIST);
            dataResponse = service.executeRequest(dataRequest);

            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            XMLEncoder.addSection(document, "pageHeader", "headerDetail", order.getHeaderMap());
            XMLEncoder.addSection(document, "previousPages", "previousPage", order.getCrumbMap());

            systemVO.setXML(document);

        } catch(Exception e) {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
    }

    private FTDSystemVO getZipCode(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            String zip = (String) arguments.get("zip");
            String dnisType = (String) arguments.get("dnisType");
            if(dnisType == null) dnisType = "";
            dataRequest.addArgument("dnisType", dnisType);
            String searchConst;
            if(zip != null && zip.length() > 0)
            {
                dataRequest.addArgument("zipIn", ((String) arguments.get("zip")));
                dataRequest.addArgument("cityIn", "");
                dataRequest.addArgument("stateIn", "");
                searchConst = DataConstants.SEARCH_GET_ZIP_CODE;
            }
            else
            {
                dataRequest.addArgument("zipIn", "");
                dataRequest.addArgument("cityIn", ((String) arguments.get("city")).toUpperCase());
                dataRequest.addArgument("stateIn", ((String) arguments.get("state")).toUpperCase());

                searchConst = DataConstants.SEARCH_GET_ZIP_CODE;
            }

            GenericDataService service =  this.getGenericDataService(searchConst);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("ZIP CODE LOOKUP PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
	}

    private FTDSystemVO getFlorist(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("floristName", (String) arguments.get("floristName"));
            dataRequest.addArgument("address1", (String) arguments.get("address"));
            dataRequest.addArgument("inCity", (String) arguments.get("inCity"));
            dataRequest.addArgument("inState", (String) arguments.get("inState"));
            dataRequest.addArgument("inZipCode", (String) arguments.get("inZipCode"));
            dataRequest.addArgument("inPhone", (String) arguments.get("inPhone"));
            dataRequest.addArgument("productId", (String) arguments.get("productId"));
            dataRequest.addArgument("dnisType", null);

            GenericDataService service =  this.getGenericDataService(DataConstants.SEARCH_GET_FLORIST);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("FLORIST LOOKUP PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
	}

    private FTDSystemVO getGreetingCards(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            String dataCommand = "";
            String occasion = (String) arguments.get("occasion");
            String cardId = (String) arguments.get("cardId");

            // if we have all three arguments then search for the specific
            // card plus all other cards for this occasion.
            if(occasion != null && occasion.length() > 0 && cardId != null && cardId.length() > 0)
            {
                dataRequest.addArgument("type", "4");
                dataRequest.addArgument("occasion", occasion);
                dataRequest.addArgument("cardId", cardId);
                dataCommand = DataConstants.SEARCH_GET_GREETING_CARDS2;
            }
            else
            {
                dataRequest.addArgument("type", "4");
                dataRequest.addArgument("occasion", occasion);
                if(cardId != null)
                {
                    cardId = cardId.toUpperCase();
                }
                dataRequest.addArgument("cardId", cardId);
                dataCommand = DataConstants.SEARCH_GET_GREETING_CARDS;
            }

            GenericDataService service =  this.getGenericDataService(dataCommand);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            XMLDocument document = XMLEncoder.createXMLDocument("GREETING CARD LOOKUP PAGE VO");
            XMLEncoder.addSection(document, ((XMLDocument)dataResponse.getDataVO().getData()).getChildNodes());

            systemVO.setXML(document);
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return systemVO;
	}

    private FTDSystemVO getDeliveryPolicy(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        XMLDocument document = new XMLDocument();

        String category = (String)arguments.get("category");

        Element root = document.createElement("root");

        Element deliveryPolicy = document.createElement("deliveryPolicy");
        Element deliveryFlag = document.createElement("deliveryFlag");
        deliveryFlag.setAttribute("value", category);

        deliveryPolicy.appendChild(deliveryFlag);

        root.appendChild(deliveryPolicy);

        document.appendChild(root);

        StringWriter stringWriter = new StringWriter();
        try
        {
            document.print(new PrintWriter(stringWriter));
        }
        catch(Exception e)
        {
            this.getLogManager().error("Can not convert XML to String");
        }

        systemVO.setXML(stringWriter.toString());

        return systemVO;
	}

    private FTDSystemVO getCalendar(FTDArguments arguments)
	{
        FTDSystemVO systemVO = new FTDSystemVO();
        OrderPO order = null;

        try
        {
            SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
            OEParameters oeGlobalParms = new OEParameters();
            HashMap holidayMap = new HashMap();
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL(this.getLogManager());
            SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());

            order = (OrderPO) this.getPersistentServiceObject((String)arguments.get(ArgumentConstants.PERSISTENT_OBJECT), OrderPO.class);
            String zipCode = order.getSendToCustomer().getZipCode();

            String productId = (String) arguments.get("productId");
            String month = (String) arguments.get("month");
            String year = (String) arguments.get("year");

            // create delivery date parameter object and set default values
            OEDeliveryDateParm deliveryDateParm = new OEDeliveryDateParm();
            deliveryDateParm.setDeliverToday(Boolean.TRUE);
            //deliveryDateParm.setSaturdayDelivery(Boolean.TRUE);
            deliveryDateParm.setSundayDelivery(Boolean.TRUE);
            deliveryDateParm.setProductId(productId);
            deliveryDateParm.setOrder(order);
            deliveryDateParm.setProductType(GeneralConstants.OE_PRODUCT_TYPE_FLORAL);
            deliveryDateParm.setProductSubType(GeneralConstants.OE_PRODUCT_SUBTYPE_NONE);
            deliveryDateParm.setCalendarMonth(month);
            deliveryDateParm.setCalendarYear(year);

            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                deliveryDateParm.setFloralServiceCharge(order.getDomesticServiceFee());
            }
            else
            {
                deliveryDateParm.setDeliverToday(Boolean.FALSE);
                deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                deliveryDateParm.setFloralServiceCharge(order.getInternationalServiceFee());
            }

            GenericDataService service = null;
            FTDDataRequest dataRequest = null;
            FTDDataResponse dataResponse = null;
            XMLDocument xmlResponse = null;
            XPathQuery q = null;
            NodeList nl = null;
            String xpath = null;

            // get product information
            dataRequest = new FTDDataRequest();
            dataRequest.addArgument(ArgumentConstants.SEARCH_PRODUCT_ID, productId);
            dataRequest.addArgument(ArgumentConstants.SEARCH_SOURCE_CODE, order.getSourceCode());
            dataRequest.addArgument(ArgumentConstants.SEARCH_ZIP_CODE, zipCode);
            if ( order.getSendToCustomer().isDomesticFlag() )
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC);
            }
            else
            {
                dataRequest.addArgument(ArgumentConstants.SEARCH_DOMESTIC_INTL_FLAG, GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL);
            }
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, order.getSendToCustomer().getCountry());
            Date deliveryDate = order.getDeliveryDate();
            if (deliveryDate == null) {
                dataRequest.addArgument("deliveryDate", "");
            } else {
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                dataRequest.addArgument("deliveryDate", df.format(deliveryDate));
            }

            service =  this.getGenericDataService(DataConstants.SEARCH_GET_PRODUCT_DETAIL);
            dataResponse = service.executeRequest(dataRequest);
            XMLDocument productList = (XMLDocument)dataResponse.getDataVO().getData();

            // retrieve the product information
            xpath = "productList/products/product";
            q = new XPathQuery();
            nl = q.query(productList, xpath);

            if (nl.getLength() > 0)
            {
                Element element = (Element) nl.item(0);

                deliveryDateParm.setProductType(element.getAttribute("productType"));
                deliveryDateParm.setProductSubType(element.getAttribute("productSubType"));

                // Exotic floral items cannot be delivered same day
                if ( element.getAttribute("productSubType").equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC) ||
                     element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                     element.getAttribute("productType").equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                     element.getAttribute("deliveryType").equals(GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL) )
                {
                    deliveryDateParm.setDeliverToday(Boolean.FALSE);
                }

                // set the exception dates for product
                deliveryDateParm.setExceptionCode(element.getAttribute("exceptionCode"));
                try {
                    deliveryDateParm.setExceptionFrom(sdfInput.parse(element.getAttribute("exceptionStartDate")));
                    deliveryDateParm.setExceptionTo(sdfInput.parse(element.getAttribute("exceptionEndDate")));
                }
                catch (Exception e) {
                }

                // set the vendor no delivery dates for product
                HashMap vendorNoDeliverFrom = new HashMap();
                HashMap vendorNoDeliverTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendorDelivBlockStart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                    {
                        vendorNoDeliverFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorDelivBlockStart" + y)));
                        vendorNoDeliverTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorDelivBlockEnd" + y)));
                    }
                }
                if ( vendorNoDeliverFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoDeliverFrom(vendorNoDeliverFrom);
                    deliveryDateParm.setVendorNoDeliverTo(vendorNoDeliverTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.FALSE);
                }

                // set the vendor no ship dates for product
                HashMap vendorNoShipFrom = new HashMap();
                HashMap vendorNoShipTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendorShipBlockStart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                    {
                        vendorNoShipFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorShipBlockStart" + y)));
                        vendorNoShipTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorShipBlockEnd" + y)));
                    }
                }
                if ( vendorNoShipFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoShipFrom(vendorNoShipFrom);
                    deliveryDateParm.setVendorNoShipTo(vendorNoShipTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.FALSE);
                }

                // set Florist delivery information
                if ( element.getAttribute("shipMethodFlorist") != null &&
                     element.getAttribute("shipMethodFlorist").equals("Y") )
                {

                   //deliveryDateParm.setSaturdayDelivery(Boolean.TRUE);
                   deliveryDateParm.setShipMethodFlorist(true);

                    if ( element.getAttribute("serviceCharge") != null &&
                          !element.getAttribute("serviceCharge").trim().equals("") )
                    {
                        deliveryDateParm.setFloralServiceCharge(new BigDecimal(element.getAttribute("serviceCharge")));
                    }
                }

                // Check delivery type for carrier ship methods
                if ( element.getAttribute("shipMethodCarrier") != null &&
                     element.getAttribute("shipMethodCarrier").equals("Y") )
                {
                    //deliveryDateParm.setSaturdayDelivery(Boolean.FALSE);
                    deliveryDateParm.setSundayDelivery(Boolean.FALSE);

                    deliveryDateParm.setShipMethodCarrier(true);
                    // Get the shipping methods for this product
                    HashMap shippingMethods = searchUTIL.getProductShippingMethods(element.getAttribute("productID"), element.getAttribute("standardPrice"), element.getAttribute("shippingKey"));
                    deliveryDateParm.setShipMethods(shippingMethods);

                    // build values of No Ship Days for product
                    HashSet noShipDays = new HashSet();
                    if ( element.getAttribute("sundayFlag") != null &&
                         element.getAttribute("sundayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.SUNDAY));
                    }
                    if ( element.getAttribute("mondayFlag") != null &&
                         element.getAttribute("mondayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.MONDAY));
                    }
                    if ( element.getAttribute("tuesdayFlag") != null &&
                         element.getAttribute("tuesdayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.TUESDAY));
                    }
                    if ( element.getAttribute("wednesdayFlag") != null &&
                         element.getAttribute("wednesdayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.WEDNESDAY));
                    }
                    if ( element.getAttribute("thursdayFlag") != null &&
                         element.getAttribute("thursdayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.THURSDAY));
                    }
                    if ( element.getAttribute("fridayFlag") != null &&
                         element.getAttribute("fridayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.FRIDAY));
                    }
                    if ( element.getAttribute("saturdayFlag") != null &&
                         element.getAttribute("saturdayFlag").equals("N") )
                    {
                        noShipDays.add(new Integer(Calendar.SATURDAY));
                    }
                    deliveryDateParm.setNoShipDays(noShipDays);
                }

                deliveryDateParm.setZipCodeGNADDFlag(element.getAttribute("zipGnaddFlag").trim());
                deliveryDateParm.setZipCodeFloralFlag(element.getAttribute("zipFloralFlag").trim());
                deliveryDateParm.setZipCodeGotoFloristFlag(element.getAttribute("zipGotoFloristFlag").trim());
                deliveryDateParm.setZipTimeZone(element.getAttribute("timeZone"));

                // zip code does support Sunday delivery
                if ( element.getAttribute("zipSundayFlag") != null &&
                     element.getAttribute("zipSundayFlag").equals("Y") )
                {
/* CLF - codified products code */
                    // codified products follow different rules under normal processing
                    if ( element.getAttribute("codifiedProduct") != null &&
                         element.getAttribute("codifiedProduct").equals("Y") &&
                         oeGlobalParms.getGNADDLevel().equals("0") )
                    {
                        // Valid valued are: N-codified; NA-not codified/not available
                        //                   Y-codified (No Sunday); S-codified (Sunday)
                        if ( element.getAttribute("codifiedAvailable") != null &&
                             element.getAttribute("codifiedAvailable").equals("S") )
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                        }
                        else
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                        }
                    }
/* */
                }
                // zip code does NOT support Sunday delivery
                else
                {
                   deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                }
            }

            // retrieve the Global Parameter information
            xpath = "productList/globalParmsData/data";
            //q = new XPathQuery();
            nl = q.query(productList, xpath);

            if ( nl.getLength() > 0 )
            {
                Element element = (Element) nl.item(0);

                //String days = element.getAttribute("deliveryDaysOut");
                //oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(element.getAttribute("deliveryDaysOut")));
                oeGlobalParms.setDeliveryDaysOutMAX(Integer.parseInt(element.getAttribute("deliveryDaysOutMAX")));
                oeGlobalParms.setDeliveryDaysOutMIN(Integer.parseInt(element.getAttribute("deliveryDaysOutMIN")));
                oeGlobalParms.setAllowSubstitution(element.getAttribute("allowSubstitution"));
                oeGlobalParms.setExoticCutoff(element.getAttribute("exoticCutoff"));
                oeGlobalParms.setFreshCutCutoff(element.getAttribute("freshcutsCutoff"));
                oeGlobalParms.setFridayCutoff(element.getAttribute("fridayCutoff"));
                oeGlobalParms.setGNADDDate(sdfInput.parse(element.getAttribute("gnaddDate")));
                oeGlobalParms.setGNADDLevel(element.getAttribute("gnaddLevel"));
                oeGlobalParms.setIntlAddOnDays(Integer.parseInt(element.getAttribute("intlOverrideDays")));
                oeGlobalParms.setIntlCutoff(element.getAttribute("internationalCutoff"));
                oeGlobalParms.setMondayCutoff(element.getAttribute("mondayCutoff"));
                oeGlobalParms.setSaturdayCutoff(element.getAttribute("saturdayCutoff"));
                oeGlobalParms.setSpecialtyGiftCutoff(element.getAttribute("specialtyGiftCutoff"));
                oeGlobalParms.setSundayCutoff(element.getAttribute("sundayCutoff"));
                oeGlobalParms.setThursdayCutoff(element.getAttribute("thursdayCutoff"));
                oeGlobalParms.setTuesdayCutoff(element.getAttribute("tuesdayCutoff"));
                oeGlobalParms.setWednesdayCutoff(element.getAttribute("wednesdayCutoff"));
                oeGlobalParms.setFreshCutSrvcChargeTrigger(element.getAttribute("freshCutsSvcChargeTrigger"));
                if(element.getAttribute("freshCutsSvcCharge") != null && !element.getAttribute("freshCutsSvcCharge").equals("")) {
                    oeGlobalParms.setFreshCutSrvcCharge(new BigDecimal(element.getAttribute("freshCutsSvcCharge")));
                }
                if(element.getAttribute("specialSvcCharge") != null && !element.getAttribute("specialSvcCharge").equals("")) {
                    oeGlobalParms.setSpecialSrvcCharge(new BigDecimal(element.getAttribute("specialSvcCharge")));
                }
                if(element.getAttribute("freshCutsSatCharge") != null && !element.getAttribute("freshCutsSatCharge").equals("")) {
                    oeGlobalParms.setFreshCutSatCharge(new BigDecimal(element.getAttribute("freshCutsSatCharge")));
                }
                if(element.getAttribute("fuelSurchargeAmt") != null && !element.getAttribute("fuelSurchargeAmt").equals("")) {
                    oeGlobalParms.setFuelSurchargeAmt(new BigDecimal(element.getAttribute("fuelSurchargeAmt")));
                }
            }

            // retrieve the holiday date information for the country
            xpath = "productList/holidayDates/data";
            q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                for ( int i = 0; i < nl.getLength(); i++ )
                {
                    Element element = (Element) nl.item(i);

                    OEDeliveryDate holidayDate = new OEDeliveryDate();
                    holidayDate.setDeliveryDate(element.getAttribute("holidayDate"));
                    holidayDate.setHolidayText(element.getAttribute("holidayDescription"));
                    holidayDate.setShippingAllowed(element.getAttribute("shippingAllowed"));
                    holidayDate.setDeliverableFlag(element.getAttribute("deliverableFlag"));

                    holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
                }
            }

            deliveryDateParm.setGlobalParms(oeGlobalParms);
            deliveryDateParm.setHolidayDates(holidayMap);

            // do not need to retrieve zip information separately if have product
            if ( productId == null || productId.equals("") )
            {
                // get zip information if zip provided
                if ( zipCode != null && !zipCode.equals("") )
                {
                    dataRequest = new FTDDataRequest();
                    dataRequest.addArgument("zipCode", zipCode);

                    service =  this.getGenericDataService(DataConstants.SHOPPING_GET_TIMEZONE_DATA);
                    dataResponse = service.executeRequest(dataRequest);
                    xmlResponse = (XMLDocument)dataResponse.getDataVO().getData();

                    // Get the timezone from the xml for processing delivery dates
                    Element timezoneElement = null;
                    xpath = "timezoneDataSet/timezoneData/data";

                    nl = q.query(xmlResponse, xpath);
                    if ( nl.getLength() > 0 )
                    {
                        timezoneElement = (Element) nl.item(0);
                        deliveryDateParm.setZipTimeZone(timezoneElement.getAttribute("timeZone"));
                        deliveryDateParm.setZipCodeGNADDFlag(timezoneElement.getAttribute("zipGnaddFlag").trim());
                        deliveryDateParm.setZipCodeFloralFlag(timezoneElement.getAttribute("zipFloralFlag").trim());
                        deliveryDateParm.setZipCodeGotoFloristFlag(timezoneElement.getAttribute("zipGotoFloristFlag").trim());

                        if ( (timezoneElement.getAttribute("zipSundayFlag") != null) &&
                             (timezoneElement.getAttribute("zipSundayFlag").equals("Y")) )
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                        }
                        else
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                        }
                    }
                }
                // No Zipcode exists to check against
                else {
                    deliveryDateParm.setZipTimeZone(GeneralConstants.OE_TIMEZONE_DEFAULT);
                    deliveryDateParm.setZipCodeGNADDFlag("N");
                    deliveryDateParm.setZipCodeGotoFloristFlag("Y");
                }
            }

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter objecft
            deliveryDateUtil.getDeliveryRanges(GeneralConstants.XML_PRODUCT_LIST, productList, deliveryDateParm);

            XMLDocument calendarDates = null;
            if ( productId == null || productId.equals("") )
            {
                calendarDates = deliveryDateUtil.getCalendarDeliveryDates(deliveryDateParm);
            }
            else
            {
                calendarDates = deliveryDateUtil.getCalendarProductDeliveryDates(deliveryDateParm);
            }

            XMLDocument document = XMLEncoder.createXMLDocument("CALENDAR PAGE VO");
            XMLEncoder.addSection(document, calendarDates.getChildNodes());

            systemVO.setXML(document);

        }
        catch(Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }
        finally 
		{
        	this.returnPersistentServiceObject(order);
        }
        return systemVO;
	}

    /**
     *
     * @param countryId String parameter that holds the country id to retrieve
     *                  the holiday dates for
     * @return HashMap containing the retrieved dates and their associated
     *                  information (text, deliverable flag)
     */
    private HashMap getHolidayDates(String countryId)
    {
        HashMap holidayMap = new HashMap();
        FTDDataRequest dataRequest = new FTDDataRequest();

        try
        {
            // retrieve the Global Parameter information
            GenericDataService service =  this.getGenericDataService(DataConstants.SHOPPING_GET_HOLIDAY_DATES);
            dataRequest.addArgument(ArgumentConstants.SEARCH_COUNTRY_ID, countryId);
            FTDDataResponse dataResponse = service.executeRequest(dataRequest);

            Element element = null;
            String xpath = "holidayDates/holidayDate/date";

            XPathQuery q = new XPathQuery();
            NodeList nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);

            if (nl.getLength() > 0)
            {
                for ( int i = 0; i < nl.getLength(); i++ )
                {
                    element = (Element) nl.item(i);

                    OEDeliveryDate holidayDate = new OEDeliveryDate();
                    holidayDate.setDeliveryDate(element.getAttribute("holidayDate"));
                    holidayDate.setHolidayText(element.getAttribute("holidayDescription"));
                    holidayDate.setShippingAllowed(element.getAttribute("shippingAllowed"));
                    holidayDate.setDeliverableFlag(element.getAttribute("deliverableFlag"));

                    holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
                }
            }
        }
        catch (Exception e)
        {
            this.getLogManager().error(e.toString(), e);
        }

        return holidayMap;
    }

    private String getDisplayCodifiedSpecial(String elementName, XMLDocument xmlResponse) throws Exception
    {

        // Get global params
        SearchUTIL searchUTIL = new SearchUTIL(this.getLogManager());
        OEParameters oeGlobalParms = searchUTIL.getGlobalParms();

        // retrieve the product special codified flag
        String xpath = "productList/products/product";
        XPathQuery q = new XPathQuery();
        NodeList nl = q.query(xmlResponse, xpath);
        Element product = null;
        String codifiedSpecialFlag = "N";
        String deliverableFlag = "N";
        if(nl.getLength() > 0)
        {
            product = (Element) nl.item(0);
            codifiedSpecialFlag = product.getAttribute("codifiedSpecial");
            if(codifiedSpecialFlag == null) codifiedSpecialFlag = "N";

            deliverableFlag = product.getAttribute("codifiedDeliverable");
            if(deliverableFlag == null) deliverableFlag = "N";
        }

        // retrieve the florist information
        //xpath = "productList/floristListData/data";
        //q = new XPathQuery();
        //NodeList floristNodelist = q.query(dataResponse.getDataVO().getData().toString(), xpath);
        String ret = "N";

        // check for at least one florist that has this product available
        if(codifiedSpecialFlag.equals("Y") && (deliverableFlag.equals("N")) /*&& oeGlobalParms.getGNADDLevel().equals("0")*/)
        {
            ret = "Y";
        }

        return ret;
    }

    private void createUpsellDetail(Element productDetailElement, XMLDocument productList, OrderPO order) throws Exception {
        String countryId = null;
        String zipGnaddFlag = null;
        String zipFloralFlag = null;
        String productType = null;
        String gnaddLevel = null;

        Element element = null;
        NodeList nl = null;
        String xpath = null;
        XPathQuery q = null;
        countryId = order.getSendToCustomer().getCountry();

        productDetailElement.setAttribute("specialUnavailable","N");

        xpath = "productList/globalParmsData/data";
        q = new XPathQuery();
        nl = q.query(productList, xpath);

        if ( nl.getLength() > 0 )
        {
            element = (Element) nl.item(0);
            gnaddLevel = element.getAttribute("gnaddLevel");
        }
        else
        {
            gnaddLevel = "0";
        }

        xpath = "productList/products/product";
        //q = new XPathQuery();
        nl = q.query(productList, xpath);

        if (nl.getLength() > 0)
        {
            element = (Element) nl.item(0);
            zipGnaddFlag = element.getAttribute("zipGnaddFlag").trim();
            zipFloralFlag = element.getAttribute("zipFloralFlag").trim();
            productType = element.getAttribute("productType");
        }
        else
        {
            zipGnaddFlag = "N";
            zipFloralFlag = "Y";
        }

        if ( countryId != null &&
             (countryId.equals("CA") ||       // Canada
             countryId.equals("PR") ||        // Puerto Rico
             countryId.equals("VI") ))        // Virgin Islands
        {
            if ( (zipGnaddFlag.equals("Y") && gnaddLevel.equals("0")) 
                 || zipFloralFlag.equals("N"))
            {
                productDetailElement.setAttribute("specialUnavailable","Y");
            }
             else if ( productType != null && !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
            {
                productDetailElement.setAttribute("specialUnavailable","Y");
            }
        }
        else if ( (zipGnaddFlag.equals("Y") && gnaddLevel.equals("0")) 
                  || zipFloralFlag.equals("N"))
        {
            if ( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
            {
                productDetailElement.setAttribute("specialUnavailable","Y");
            }
        }

        if(getDisplayCodifiedSpecial("productList", productList).equals("Y")) {
            productDetailElement.setAttribute("specialUnavailable","Y");
        }

        if(order.getDisplayProductUnavailable() != null && order.getDisplayProductUnavailable().equals("Y"))
        {
            productDetailElement.setAttribute("specialUnavailable","Y");
        }

        if (order.getSendToCustomer().getState() != null && (order.getSendToCustomer().getState().equals("AK") || order.getSendToCustomer().getState().equals("HI")))
        {
            if(productType != null && productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))
            {
                productDetailElement.setAttribute("specialUnavailable","Y");
            }
        }
    }


    private static String formatAddonDollars(String amt)
    {
      String formatted = amt;

      int index = amt.indexOf(".");
      if(index >= 0)
      {
        String remaining = amt.substring(index+1,amt.length());
        int fraction = Integer.parseInt(remaining);

        //if fraction does not contain a value, then return the whole number
        if(fraction == 0)
        {          
          formatted = (new Double(amt)).intValue() + "";
        }
        
      }
      

      return formatted;
    }

    /* clean up xml data...make it look pretty*/
   private void cleanUp(XMLDocument doc) throws Exception
   {

  
      //for each order in the document
      Element data = null;
      XPathQuery q = new XPathQuery();
      NodeList nl = q.query(doc, "//addOn");

      for (int i = 0; i < nl.getLength() ; i++)
      {

          //format the date
          data = (Element) nl.item(i);
          String value = data.getAttribute("price");
          data.setAttribute("price",formatAddonDollars(value));


      }                        
   }
   
   /**
     * Returns true if the current time is after the florist cutoff based 
     * on product id and zip code.  Defaults to false.
     * @param productId
     * @param zipCode
     * @return boolean
     */
    private boolean afterFloristCutoff(GenericDataService service, String productId, String zipCode){
        boolean afterFloristCutoff = false;
        try {
            FTDDataRequest zipCodeDataRequest = new FTDDataRequest();
            zipCodeDataRequest = new FTDDataRequest();
            zipCodeDataRequest.addArgument("inZip", zipCode);
            zipCodeDataRequest.addArgument("inProdId", productId);
            service =  this.getGenericDataService(DataConstants.SEARCH_GET_ZIPCODE_CUTOFF);
            FTDDataResponse zipCodeDataResponse = service.executeRequest(zipCodeDataRequest);
            NodeList nl = null;
            Element cutoffTimeElement = null;
            String xpath = "zipcodeCutoff/zipCutoff/cutoffTime";
            XPathQuery q = new XPathQuery();
            nl = q.query((XMLDocument)zipCodeDataResponse.getDataVO().getData(), xpath);
            cutoffTimeElement = (Element) nl.item(0); 
            String floristCutoff = cutoffTimeElement.getAttribute("ZIPCUTOFF");
            if (floristCutoff != null && !floristCutoff.equalsIgnoreCase("")){
                Calendar cutoff = Calendar.getInstance();
                int hour = new Integer(floristCutoff.substring(0, 2)).intValue();
                cutoff.set(Calendar.HOUR_OF_DAY, hour);
                int minute = new Integer(floristCutoff.substring(2));
                cutoff.set(Calendar.MINUTE, minute);
                cutoff.set(Calendar.SECOND, 0);
                cutoff.set(Calendar.MILLISECOND, 0);
                Calendar today = Calendar.getInstance();
                if (today.after(cutoff)){
                    afterFloristCutoff = true;
                }
            }
        }catch(Exception e){
            this.getLogManager().error(e.toString(), e);
            e.printStackTrace();
        }
        return afterFloristCutoff;
    }

    /**
      * Returns true if the current time is after the vendor cutoff based 
      * on the string passed in in the format HHMM.  The WebOE vendor cutoff is 
      * defined in FRP.GLOBAL_PARMS for speciality gifts and fresh cuts.
      * Defaults to false.
      * @param service
      * @param productType
      * @return boolean
      */
     private boolean afterVendorCutoff(GenericDataService service, String productType){
         boolean afterVendorCutoff = false;
         try {
             FTDDataRequest dataRequest = new FTDDataRequest();
             dataRequest = new FTDDataRequest();
             service =  this.getGenericDataService(DataConstants.SHOPPING_GET_GLOBAL_PARMS);
             FTDDataResponse dataResponse = service.executeRequest(dataRequest);
             NodeList nl = null;
             Element cutoffTimeElement = null;
             String xpath = "globalParmsDataSet/globalParmsData/data";
             XPathQuery q = new XPathQuery();
             nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
             cutoffTimeElement = (Element) nl.item(0); 
             String vendorCutoff = null;
             if (productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)){
                 vendorCutoff = cutoffTimeElement.getAttribute("freshcutsCutoff");
             }else if (productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)){
                 vendorCutoff = cutoffTimeElement.getAttribute("specialtyGiftCutoff");
             }
             if (vendorCutoff != null && !vendorCutoff.equalsIgnoreCase("")){
                 Calendar cutoff = Calendar.getInstance();
                 int hour = new Integer(vendorCutoff.substring(0, 2)).intValue();
                 cutoff.set(Calendar.HOUR_OF_DAY, hour);
                 int minute = new Integer(vendorCutoff.substring(2));
                 cutoff.set(Calendar.MINUTE, minute);
                 cutoff.set(Calendar.SECOND, 0);
                 cutoff.set(Calendar.MILLISECOND, 0);
                 Calendar today = Calendar.getInstance();
                 if (today.after(cutoff)){
                     afterVendorCutoff = true;
                 }
             }
         }catch(Exception e){
             this.getLogManager().error(e.toString(), e);
         }
        return afterVendorCutoff;
     }
}

