package com.ftd.applications.oe.admin.presentation;

import com.ftd.framework.common.valueobjects.*;

import com.ftd.applications.oe.presentation.Util;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.security.cache.vo.UserInfo;

public class UserAdminServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        response.setHeader("Cache-Control", "no-cache");
        String function = request.getParameter("functionName");
        if(function == null) function = "";

        String userId = userInfo.getUserID();        


        //String userId = "JPENNEY";

        if(function.equals("updateUser"))
        {   
            handleUpdateUser(request, response, userId, sessionId);
        }
        else if(function.equals("getUser"))
        {   
            handleGetUser(request, response, sessionId);
        }
        else
        {
            handleGetUser(request, response, sessionId);
            
//            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
//            if (dispatcher != null)
//                dispatcher.forward(request, response);
        }        
    }

    private void handleGetUser(HttpServletRequest request, HttpServletResponse response, String sessionId) throws ServletException, IOException
    {
        Util util = new Util();
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.NONE);

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
          
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            if(value != null && value.length() > 0)
            {
                ftdRequest.addArgument(param, value);
            }
        }
      
        ftdRequest.setServiceDescription("ADMIN SERVICE");
        ftdRequest.setCommand("GET_USER");

        FTDServiceResponse ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/user.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/UserAdminServlet"));
            params.put("maintServlet", (request.getContextPath() + "/servlet/AdminServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("user.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }    
    }

    private void handleUpdateUser(HttpServletRequest request, HttpServletResponse response, String userId, String sessionId) throws ServletException, IOException
    {
        Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            if(value != null && value.length() > 0)
            {
                ftdRequest.addArgument(param, value);
            }
        }

        ftdRequest.addArgument("updateUserId", userId);
        ftdRequest.setPersistence(FTDServiceRequest.NONE);
      
        ftdRequest.setServiceDescription("ADMIN SERVICE");
        ftdRequest.setCommand("UPDATE_USER");

        FTDServiceResponse ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/user.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/UserAdminServlet"));
            params.put("maintServlet", (request.getContextPath() + "/servlet/AdminServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("user.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }   
    }

}
