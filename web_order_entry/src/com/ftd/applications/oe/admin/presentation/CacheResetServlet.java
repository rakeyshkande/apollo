package com.ftd.applications.oe.admin.presentation;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.PrintWriter;
import java.io.IOException;

import com.ftd.framework.dataaccessservices.DataManager;
import com.ftd.applications.oe.common.*;
import com.ftd.applications.oe.presentation.Util;

public class CacheResetServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
        resetCache();
        
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>CacheResetServlet</title></head>");
        out.println("<body>");
        out.println("<p>The Order cache has been reset.</p>");
        out.println("<p><form><input type='submit' value='Reset Cache'/></form></p>");
        out.println("</body></html>");
        out.close();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    private void resetCache()
    {
        DataManager.getInstance().getSession().initializeIdentityMaps();
    }
}