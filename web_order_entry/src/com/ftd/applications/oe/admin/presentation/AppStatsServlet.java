package com.ftd.applications.oe.admin.presentation;

import com.ftd.framework.common.valueobjects.*;

import com.ftd.applications.oe.presentation.Util;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import java.text.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;

public class AppStatsServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
        Util util = new Util();
        response.setHeader("Cache-Control", "no-cache");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.NONE);

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";

        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            if(value != null && value.length() > 0)
            {
                ftdRequest.addArgument(param, value);
            }
        }

        ftdRequest.setServiceDescription("ADMIN SERVICE");
        ftdRequest.setCommand("GET_APP_STATS");

        FTDServiceResponse ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/appstats.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            //params.put("sessionId", sessionid);

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                Date nowDate = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss_SS");
                String strDate = sdf.format(nowDate);
                util.writeXMLtoFile(("appstats_" + strDate + ".xml"), xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }

}
