package com.ftd.applications.oe.admin.util;

import java.sql.*;

public class ConnectionHelper
{
    private static ConnectionHelper instance;

    private ConnectionHelper()
    {
    }

    public static ConnectionHelper getInstance()
    {
        if ( instance == null )
        {
           synchronized( ConnectionHelper.class )
           {
                if ( instance == null )
                {
                    instance = new ConnectionHelper();
                }
            }
        }
        return instance;
     }

/**
 * try to create Connection throught runtime config, if failed, use default.
 */
      public Connection getConnection() throws Exception
      {

        try {
            String database_ = System.getProperty("database");
            String user_ = System.getProperty("username");
            String password_ = System.getProperty("password");
            String driver_ = System.getProperty("jdbcDriver");

            Class.forName(driver_);
            return DriverManager.getConnection(database_, user_, password_);
        } catch ( Exception e )
        {
            System.out.println( "Cannot find runtime configuration, uses default database" );
            return getDumbConnection();
        }
       }

       private Connection getDumbConnection() throws Exception
       {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
          //  String database_ = "jdbc:oracle:thin:@delphi.ftdi.com:1521:DEV1";
          //  String user_ = "ftd_apps";
            String database_ = "jdbc:oracle:thin:@ithaca.ftdi.com:1521:ZEUS1";
            String user_ = "ftd_apps_uat";
            String password_ = "athens";

            Class.forName(driver_);
            return DriverManager.getConnection(database_, user_, password_);
       }
}