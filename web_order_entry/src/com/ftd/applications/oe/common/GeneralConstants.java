package com.ftd.applications.oe.common;

import java.math.BigDecimal;

public interface GeneralConstants 
{
    public static final String YES = "Y";
    public static final String NO = "N";

    public static final String LINE_SEPARATOR = "\r\n";
    public static final String LINE_SEPARATOR_REPLACEMENT = " ";
    public static final String TILDE_CHAR = "~";
    public static final String TILDE_CHAR_REPLACEMENT = "-";
    
    public static final String CUSTOMER_TYPE_ITEM_SEND_TO = "ITEM_SEND_TO";
    public static final String CUSTOMER_TYPE_ORDER_SEND_TO = "ORDER_SEND_TO";
    public static final String CUSTOMER_TYPE_BILL_TO = "BILL_TO";
    
    public static final String POPUP_ID_KEY = "POPUP_ID";
    public static final String LOOKUP_SOURCE_CODE_KEY = "LOOKUP_SOURCE_CODE";
    public static final String LOOKUP_ZIP_CODE_KEY = "LOOKUP_ZIP_CODE";
    public static final String LOOKUP_FLORIST_KEY = "LOOKUP_FLORIST";
    public static final String LOOKUP_GREETING_CARDS_KEY = "LOOKUP_GREETING_CARDS";
    public static final String LOOKUP_GREETING_CARD_KEY = "LOOKUP_GREETING_CARD";
    public static final String LOOKUP_CUSTOMER_KEY = "LOOKUP_CUSTOMER";
    public static final String LOOKUP_RECIPIENT_KEY = "LOOKUP_RECIPIENT";
    public static final String DELIVERY_POLICIES_KEY = "DELIVERY_POLICIES";
    public static final String LOOKUP_RECIPE_KEY = "LOOKUP_RECIPE";
    public static final String LOOKUP_CARE_INSTRUCTIONS_KEY = "LOOKUP_CARE_INSTRUCTIONS";
    public static final String DISPLAY_CALENDAR_KEY = "DISPLAY_CALENDAR";
    
    public static final String SOURCE_CODE_SEARCH_KEY = "sourceCodeInput";
    public static final String PHONE_NUMBER_SEARCH_KEY = "phoneInput";
    public static final String CUSTOMER_ID_SEARCH_KEY = "customerIdInput";
    public static final String INSTITUTION_SEARCH_KEY = "institutionInput";
    public static final String ADDRESS1_SEARCH_KEY = "addressInput";
    public static final String ADDRESS2_SEARCH_KEY = "address2Input";
    public static final String ZIP_CODE_SEARCH_KEY = "zipCodeInput";
    public static final String CITY_SEARCH_KEY = "cityInput";
    public static final String STATE_SEARCH_KEY = "stateInput";
    public static final String OCCASION_SEARCH_KEY = "occasionInput";
    public static final String CATEGORY_SEARCH_KEY = "categoryInput";
    public static final String GREETING_CARD_SEARCH_KEY = "cardIdInput";
    public static final String MONTH_SEARCH_KEY = "month";
    public static final String YEAR_SEARCH_KEY = "year";
    public static final String COUNTRY_TYPE_SEARCH_KEY = "countryTypeInput";
    public static final String INST_TYPE_SEARCH_KEY = "institutionTypeInput";

    public static final String MAP_TYPE_HEADER = "HEADER";
    public static final String MAP_TYPE_CRUMB = "CRUMB";
    public static final String MAP_TYPE_UPSELL = "UPSELL";
    public static final String MAP_TYPE_PROMPT = "PROMPT"; 
    public static final String MAP_TYPE_SEARCH_CRITERIA = "SRCH_CRIT";

    /* Order Blob Status */
    public static final String BLOB_STATUS_ERR_ADDON_TRANS = "ERR_ADDON_TRANS";

    /* Source Code and DNIS Constants */
    public final static String JC_PENNEY_CODE = "JP";
    public final static String PARTNER_PRICE_CODE = "ZZ";
    public final static String DISCOVER_CODE = "DISC1";
    public final static String CORP15_CODE = "CORP15";
    public final static String CORP20_CODE = "CORP20";
    public final static String ADVO_CODE = "ADVO";
    public final static String TARGET_CODE = "TARGET";
    public final static String UPROMISE_CODE = "UPROM";
    public final static String UPROMISE1_CODE = "UPROM1";
    public final static String AMEX_CODE = "AMEX";
    public final static String AMEX1_CODE = "AMEX1";
    public final static String DINE_CODE = "DINE";
    public final static String DINE1_CODE = "DINE1";
    public final static String LOWES_CODE = "LOWES";
    
    /* QMS Constants */
    public final static String QMS_CONFIG_REALM_KEY = "ftd.pdb.application.configuration";
    public final static String QMS_SERVER_IP_AND_PORT = "qms.server.ip.port";
    public final static String QMS_TRANSPORT = "qms.transport";
    public final static String QMS_GEOSTAN_PATHS = "qms.geostan.paths";
    public final static String QMS_INPUT_FIELD_LIST = "qms.input.field.list";
    public final static String QMS_OUTPUT_FIELD_LIST = "qms.output.field.list";
    public final static String QMS_INPUT_MODE_VALUE = "qms.input.mode";
    public final static String QMS_KEEP_MULTIMATCH_VALUE = "qms.keep.multimatch";
    public final static String QMS_BUFFER_RADIUS_VALUE = "qms.buffer.radius";
    public final static String QMS_ACTIVE = "qms.active";

    public final static String QMS_FIRM_NAME = "firmname";
    public final static String QMS_ADDRESS_LINE = "addressline";
    public final static String QMS_LAST_LINE = "lastline";
    public final static String QMS_CITY = "city";
    public final static String QMS_STATE = "state";
    public final static String QMS_ZIP_10 = "ZIP10";
    public final static String QMS_LONGITUDE = "longitude";
    public final static String QMS_LATITUDE = "latitude";
    public final static String QMS_USPS_RANGE_RECORD_TYPE = "uspsrangerecordtype";
    public final static String QMS_LOW_END_HOUSE_NUMBER = "lowendhousenumber";
    public final static String QMS_HIGH_END_HOUSE_NUMBER = "highendhousenumber";
    public final static String QMS_LOW_UNIT_NUMBER = "lowunitnumber";
    public final static String QMS_HIGH_UNIT_NUMBER = "highunitnumber";
    public final static String QMS_HOUSE_NUMBER = "HouseNumber";
    public final static String QMS_UNIT_NUMBER = "UnitNumber";
    public final static String QMS_MATCH_CODE = "match_code";

    public final static String QMS_INIT_LIST = "INIT_LIST";
    public final static String QMS_INPUTFIELDLIST = "INPUTFIELDLIST";
    public final static String QMS_OUTPUTFIELDLIST = "OUTPUTFIELDLIST";
    public final static String QMS_INPUT_MODE = "Input_Mode";
    public final static String QMS_KEEP_MULTIMATCH = "Keep_multimatch";
    public final static String QMS_BUFFER_RADIUS = "BUFFER RADIUS";

    public final static String QMS_MATCH_CODE_ILLEGAL_ARGUMENT_EXCEPTION = "Q001";
    public final static String QMS_MATCH_CODE_IO_EXCEPTION = "Q002";
    public final static String QMS_MATCH_CODE_ADDRESSBROKER_EXCEPTION = "Q003";
    public final static String QMS_MATCH_CODE_SYSTEM_ERROR = "Q004";
    public final static String QMS_MATCH_CODE_QMS_DOWN = "Q005";

    /* CCAS Constants */
    public final static String OE_CCAS_PURCHASE_REQUEST = "PR";
    public final static String OE_CCAS_JC_PURCHASE_REQUEST = "JP";
    public final static String OE_COMMON_UTILITIES_CATEGORY_NAME = "com.ftd.pdb";
    public final static String CCAS_CONFIG_REALM_KEY = "ftd.pdb.application.configuration";
    public final static String CCAS_SERVER_IP = "ccas.server.ip";
    public final static String CCAS_PORT_NUMBER = "ccas.port";
    public final static String CCAS_JC_SERVER_IP = "ccas.jc.server.ip";
    public final static String CCAS_JC_PORT_NUMBER = "ccas.jc.port";
    public final static int OE_UNKNOWN_HOST_EXCEPTION = 304;
    public final static int OE_IO_EXCEPTION = 306;

    /* Status Constants */
    public final static String DONE = "DONE";
    public final static String CANCEL = "CANCEL";
    public final static String COMPLETE = "COMPLETE";
    public final static String ABANDONED = "ABANDONED";
    public final static String IN_PROCESS = "INPROC";

    /* Delivery Constants */    

    /* defect 895 - shipping system */
    public final static String DELIVERY_NEXT_DAY = "Next Day Delivery";
    public final static String DELIVERY_TWO_DAY = "Two Day Delivery";
    public final static String DELIVERY_STANDARD = "Standard Delivery";
    public final static String DELIVERY_SATURDAY = "Saturday Delivery";
    /* end defect 895 */
    
    public final static String DELIVERY_FLORIST = "Florist Delivery";
    public final static String DELIVERY_NEXT_DAY_CODE = "ND";
    public final static String DELIVERY_TWO_DAY_CODE = "2D";
    public final static String DELIVERY_STANDARD_CODE = "GR";
    public final static String DELIVERY_SATURDAY_CODE = "SA";
    public final static String DELIVERY_FLORIST_CODE = "FL";
    public final static String DELIVERY_SAME_DAY_CODE = "SD";
    
    public final static String DELIVERY_SERVICE_CHARGE = "Service Charge";

    /* Occasion Constants */
    public final static String OE_OCCASION_FUNERAL = "1";

    /* Product Constants */
    public final static int PRODUCTS_PER_PAGE = 9;

    /* Time Zone Constants */
    public final static String OE_TIMEZONE_INTERNATIONAL = "0";
    public final static String OE_TIMEZONE_EASTERN = "1";
    public final static String OE_TIMEZONE_CENTRAL = "2";
    public final static String OE_TIMEZONE_MOUNTAIN = "3";
    public final static String OE_TIMEZONE_PACIFIC = "4";
    public final static String OE_TIMEZONE_HAWAII = "5";
    public final static String OE_TIMEZONE_ALASKA = "5";
    public final static String OE_TIMEZONE_DEFAULT = "5";

    /* Product Type & Sub-Type constants */
    public final static String OE_PRODUCT_TYPE_FLORAL = "FLORAL";
    public final static String OE_PRODUCT_TYPE_FRECUT = "FRECUT";
    public final static String OE_PRODUCT_TYPE_SPEGFT = "SPEGFT";
    public final static String OE_PRODUCT_TYPE_SDG = "SDG";
    public final static String OE_PRODUCT_TYPE_SDFC = "SDFC";
    
    public final static String OE_PRODUCT_SUBTYPE_EXOTIC = "EXOTIC";
    public final static String OE_PRODUCT_SUBTYPE_NONE = "NONE";

    /* Product delivery type constants */
    public final static String OE_DELIVERY_TYPE_DOMESTIC = "D";
    public final static String OE_DELIVERY_TYPE_INTERNATIONAL = "I";

    /* Payment and Pricing Constants */
    public final static String CREDIT_CARD_PAYMENT_TYPE = "C";
    public final static BigDecimal DISCOUNT_ROUNDER = new BigDecimal(".99");
    public final static String CASH_BACK = "Cash Back";
    public final static String POINTS = "Points";
    public final static String MILES = "Miles";

    /* Country Code Constants */
    public final static String COUNTRY_CODE_US = "US";
    public final static String COUNTRY_CODE_CANADA_CA = "CA";
    public final static String COUNTRY_CODE_CANADA_CAN = "CAN";
    public final static String STATE_CODE_VIRGIN_ISLANDS = "VI";
    public final static String STATE_CODE_PUERTO_RICO = "PR";
    public final static String STATE_CODE_ALASKA = "AK";
    public final static String STATE_CODE_HAWAII = "HI";
    public final static String STATE_CODE_ARIZONA = "AZ";

    /* Function constants */
    public final static String FUNCTION_GENERALMAINT = "GENERALMAINT";

    /* Billing Info constants */
    public final static String DEFAULT_MEMBER_ID = "000000000";
    public final static String VERBAL_APPROVAL_VERBAGE = "AP";
    public final static String VERBAL_APPROVAL_ACTION_CODE = "000";    
    
    /* Global Parm processing constants */
    public final static String XML_OCCASION_DATA = "occasionData";
    public final static String XML_SUPER_INDEX_DATA = "superIndexData";
    public final static String XML_DELIVERY_DATA = "deliveryData";
    public final static String XML_PRODUCT_LIST = "productList";
    public final static String XML_DATE_RANGE_DATA = "dateRangeList";

   //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    public final static String SECURITY_TOKEN_NAME_OPS = "security.token.name.ops";
    public final static String SECURITY_TOKEN_NAME_WEBOE = "security.token.name.weboe";
    public final static String SECURITY_CONTEXT_OPS = "security.context.ops";
    public final static String SECURITY_UNIT_ID = "security.unit.id";
    public final static String SECURITY_ERROR_PAGE = "security.error.page";
    public final static String SECURITY_LOGIN_PAGE = "security.login.page";

    /* Company ID constants */
    public final static String COMPANY_ID_GSC = "GSC";  // Company used for Gift Store Card

    // Image Name Suffix 
    public final static String IMAGE_NAME_SUFFIX = "image.name.suffix.size.";    

    // Custom order product ID
    public final static String CUSTOM_ORDER_PID = "6611";
    
    // CC auth threshold (amounts less than this value will not be auth'ed)
    public final static float CC_AUTH_THRESHOLD_PRICE = 0.25f;
    
    //CC type constants
    public final static String CC_AMEX = "AX";
    
    // Global Parms Context for WEBOE
    public final static String WEBOE_CONFIG_CONTEXT = "WEBOE_CONFIG";
    
    // Standard, Deluxe, Premium parameters
    public final static String FTDAPPS_CONFIG_CONTEXT = "FTDAPPS_PARMS";
    public final static String STANDARD_LABEL = "FLORAL_LABEL_STANDARD";
    public final static String DELUXE_LABEL = "FLORAL_LABEL_DELUXE";
    public final static String PREMIUM_LABEL = "FLORAL_LABEL_PREMIUM";
}
