package com.ftd.applications.oe.common;

/*
 * The purpose of this class is to provide a pseudo-enumeration of the different
 * credit card processing that is available
 */
public class CCASType 
{
  public static final int TYPE_FTD_INT = 0;
	public static final int TYPE_JCP_INT = 1;
	public static final int TYPE_SFMB_INT = 2;
	public static final int TYPE_GIFT_INT = 3;
	public static final int TYPE_HIGH_INT = 4;
  public static final int TYPE_FUSA_INT = 5;
  public static final int TYPE_FDIRECT_INT = 6;
  public static final int TYPE_FLORIST_INT = 7;
  public static final CCASType TYPE_FTD = new CCASType(TYPE_FTD_INT,"ccas.auth.default.server","ccas.auth.default.port");
  public static final CCASType TYPE_JCP = new CCASType(TYPE_JCP_INT,"ccas.auth.jcpenney.server","ccas.auth.jcpenney.port");
  public static final CCASType TYPE_SFMB = new CCASType(TYPE_SFMB_INT,"ccas.auth.sfmb.server","ccas.auth.sfmb.port");
  public static final CCASType TYPE_GIFT = new CCASType(TYPE_GIFT_INT,"ccas.auth.gift.server","ccas.auth.gift.port");
  public static final CCASType TYPE_HIGH = new CCASType(TYPE_HIGH_INT,"ccas.auth.high.server","ccas.auth.high.port");
  public static final CCASType TYPE_FUSA = new CCASType(TYPE_FUSA_INT,"ccas.auth.fusa.server","ccas.auth.fusa.port");
  public static final CCASType TYPE_FDIRECT = new CCASType(TYPE_FDIRECT_INT,"ccas.auth.fdirect.server","ccas.auth.fdirect.port");
  public static final CCASType TYPE_FLORIST = new CCASType(TYPE_FLORIST_INT,"ccas.auth.florist.server","ccas.auth.florist.port");
  
  private int type;
  private String serverConfigName;
  private String portConfigName;
  
  /*
   * Private constructor.
   */
  private CCASType(int type, String serverConfigName, String portConfigName)
  {
    this.type = type;
    this.serverConfigName=serverConfigName;
    this.portConfigName=portConfigName;
  }
  
  /*
   * Accessor for the private member variable "type"
   */
  public int getType() 
  {
    return type;
  }
  
  /*
   * Accessor for the private member variable "serverConfigName"
   */
  public String getServerConfigName() 
  {
    return serverConfigName;
  }
  
  /*
   * Accessor for the private member variable "getPortConfigName"
   */
  public String getPortConfigName() 
  {
    return portConfigName;
  }
}