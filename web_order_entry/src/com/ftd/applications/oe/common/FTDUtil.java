package com.ftd.applications.oe.common;


/**
 * Performs miscellaneous utility functionality such as data type conversions 
 & and formatting
 * Creation date: (08/13/2002)
 * @author: Chris Fagalde
 */

import com.ftd.framework.common.exceptions.*;

import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.security.MessageDigest;

public class FTDUtil {

    /**
     * Util constructor comment.
     */
    private FTDUtil() {
        super();
    }
    
    /**
     * This method is used to addDays to the current date.
     * Creation date: (4/4/02 1:37:05 PM)
     * It takes one parameter, an integer, which specifies how many days
     * should be added to the current date
     * The function returns a date that is the equivalent of the
     * current date + number of months passed to the function
     */
    public static Date addDays(int numberOfDays) {

        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.DATE,numberOfDays);
	
        return calendar.getTime();
		
    }

    /**
     * This method is used to add Days to the date passed in as a parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     * 
     * The function returns a date that is the equivalent of the
     * date + number of days passed to the function
     */
    public static Date addDays(Date date, int numberOfDays) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE,numberOfDays);
        return calendar.getTime();
		
    }

    /**
     * This method is used to add Hours to the curent date passed in as a 
     * parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     * 
     * The function returns a date that is the equivalent of the
     * date + number of hours passed to the function
     */
    public static Date addHours(int numberOfHours) {

        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.HOUR,numberOfHours);
        return calendar.getTime();
		
    }

    /**
     * This method is used to add Hours to the date passed in as a parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     * 
     * The function returns a date that is the equivalent of the
     * date + number of hours passed to the function
     */
    public static Date addHours(Date date, int numberOfHours) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR,numberOfHours);
        return calendar.getTime();
		
    }

    /**
     * This method is used to add Minutes to the date passed in as a parameter.
     * Creation date: (4/4/02 1:37:05 PM)
     * 
     * The function returns a date that is the equivalent of the
     * date + number of minutes passed to the function
     */
    public static Date addMinutes(Date date, int numberOfMinutes) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE,numberOfMinutes);
        return calendar.getTime();
		
    }

    /**
     * Name: addMonths
     * Creation date: (4/4/02 1:37:05 PM)
     *
     * This function is used to add months to the date that is passed in
     * as a parameter.
     *
     * Parameters: Date originalDate
     *             int the number of months to add to the original date
     *
     * Return: Date which will represent the date passed in plus the 
     *         number of months that was also passed in.
     */
    public static Date addMonths(Date originalDate, int numberOfMonths) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(originalDate);
        calendar.add(Calendar.MONTH,numberOfMonths);
	
        return calendar.getTime();	
	
    }
    
    /**
     * Converts boolean value to a String value
     * Creation date: (3/13/02 12:54:09 PM)
     * @return java.lang.String
     */
    public static String convertBooleanToString(boolean bFlag) {
        String flag = String.valueOf(bFlag).toUpperCase();
            if(flag.equals("TRUE"))
                flag = "Y";
            else
                flag = "N";	
        return flag;
    }
    
	public static boolean convertStringToBoolean(String sFlag) {
		boolean flag = false;
		if (sFlag != null && (sFlag.toUpperCase()).equals("Y"))
			flag = true;
		return flag;
	}
    
    /**
     * Encrypts text
     * Creation date: (5/24/02 3:29:04 PM)
     * @return java.lang.String
     * @param password java.lang.String
     */
    public static String encryptText(String text) throws FTDApplicationException {
        String encryptedText = null;
        try {
	
            // Create a Message Digest from a Factory method
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] msg = text.getBytes();
	
            // Update the message digest with some more bytes
            // This can be performed multiple times before creating the hash
            md.update(msg);

            // Create the digest from the message
            byte[] aMessageDigest = md.digest();

            encryptedText = new String(aMessageDigest);
        }
        catch (Exception e) 
        {    
            String msg[] = new String[1];
            msg[0] = e.getMessage();
            
            throw new FTDApplicationException(999, msg);
        }
        
        return encryptedText;
    }
 
	/**
	 * Description: Takes a SQL Date in the format yyyy-mm-dd and formats it
	 *              as MM/dd/yyyy (which is the format we show in the screens).
	 * Creation date:  11/16/01
	 * @author:		   Gasior
	 * @param:         SQLDate  
	 * @return: 	   String
	 * Revisions:      Date          By           Desc
	 * 
	 * 
	 * @return String
	 */
	public static String formatSQLDateToString(java.sql.Date sqlDate)
	throws FTDApplicationException 
	{
		String strDate = "";
		java.util.Date utilDate = null;
	
		if (sqlDate != null) {
			strDate = sqlDate.toString();
			SimpleDateFormat dfIn = new SimpleDateFormat ("yyyy-MM-dd");	
			SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");
	
			try {
				utilDate = dfIn.parse(strDate);
			} catch (ParseException pe) {
				throw new FTDApplicationException(1,pe);
			}
			
			strDate = dfOut.format(utilDate);
		}
			
		return strDate;
	}

/**
 * Description: Takes a SQL Date in the format yyyy-mm-dd hh:mm:ss and formats it
 *              as MM/dd/yyyy hh:mm:ss (which is the format we show in the screens).
 * Creation date:  12/04/01
 * @author:		   Fagalde
 * @param:         SQLDate  
 * @return: 	   String
 * Revisions:      Date          By           Desc
 * 
 * 
 * @return String
 */
public static String formatSQLTimestampToString(java.sql.Timestamp sqlDate)
	throws FTDApplicationException 
{
	String strDate = "";
	java.util.Date utilDate = null;

	if (sqlDate != null) {
		strDate = sqlDate.toString();
		SimpleDateFormat dfIn = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.S");	
		SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");

		try {
			utilDate = dfIn.parse(strDate);
		} catch (ParseException pe) {
			throw new FTDApplicationException (1,pe);
		}
		
		strDate = dfOut.format(utilDate);
	}
		
	return strDate;
}

    /**
     * Description: Takes in a string Date, checks for valid formatting
     * 				in the form of mm/dd/yyyy and converts it
     * 				to a SQL date of yyyy-mm-dd.
     * Creation date:  11/16/01
     * @author:		   Gasior
     * @param:         String 
     * @return: 
     * Revisions:      Date          By           Desc
     * 
     * 
     * @return java.sql.Date
     */
    public static java.sql.Date formatStringToSQLDate(String strDate)
        throws FTDApplicationException {

        java.sql.Date sqlDate = null;

        if ((strDate != null) && (!strDate.trim().equals(""))) {
            try {
                if (strDate.length() < 10) {
                   throw new FTDApplicationException(1);
                }
                SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" ); 
                SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" ); 
		
                java.util.Date date = sdfInput.parse( strDate ); 
                String outDateString = sdfOutput.format( date ); 
	    
                // now that we have no errors, use the string to make a SQL date
                sqlDate = sqlDate.valueOf(outDateString);
			
                } catch (Exception e) {
                   throw new FTDApplicationException(1, e);
            }
        }
	
        return sqlDate;
    }

/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				in the form of mm/dd/yyyy hh:mm:ss and converts it
 * 				to a SQL date of yyyy-mm-dd hh:mm:ss.
 * Creation date: 	12/04/01
 * @author:			Fagalde
 * @param:         	String 
 * @return: 
 * Revisions:      Date          By           Desc
 * 
 * 
 * @return java.sql.Date
 */
public static java.sql.Timestamp formatStringToSQLTimestamp(String strDate)
    throws FTDApplicationException {

    java.sql.Timestamp sqlDate = null;

    if ((strDate != null) && (!strDate.trim().equals(""))) {
	    try {
		    if (strDate.length() < 19) {
			   throw new FTDApplicationException(1);
		    }
			SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy hh:mm:ss aa" ); 
		    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss.S" ); 
		
		    java.util.Date date = sdfInput.parse( strDate ); 
		    String outDateString = sdfOutput.format( date ); 
	    
			// now that we have no errors, use the string to make a SQL date
			sqlDate = sqlDate.valueOf(outDateString);
			
			} catch (Exception e) {
	     	   throw new FTDApplicationException(1,e);
	    }
    }
	
	return sqlDate;
}   

    /**
    *  Changes non-XML characters to escaped XML characters
    */
    public static String convertToXMLCharacters(String in)
    {
        if(in != null) 
        { 
            StringBuffer buf = new StringBuffer(in.length() + 8); 
            char c; 
            for(int i = 0; i < in.length(); i++) 
            {
                c = in.charAt(i); 
                switch(c) 
                { 
                    case '<': 
                        buf.append("&lt;"); 
                        break; 
                    case '>': 
                        buf.append("&gt;"); 
                        break; 
                    case '&': 
                        buf.append("&amp;"); 
                        break; 
                    case '\"': 
                        buf.append("\""); 
                        break; 
                    case '\'': 
                        buf.append("'"); 
                        break; 
                    default: 
                        buf.append(c); 
                        break; 
                } 
            } 
            return buf.toString(); 
        } 
        else 
        { 
            return new String(""); 
        } 
    }
    
    /**
     * Description: Takes in a string Date, checks for valid formatting
     * 				and converts it to a Util date of yyyy-mm-dd.
     * Creation date: 	03/22/2002
     * @author:			Fagalde
     * @param:         	String 
     * @return: 
     * Revisions:      	Date          	By           	Desc
     *					----------		-------------	------------------------------------
     * 					05/07/2002		Chris Fagalde	added muliple date formats
     * 
     * @return java.sql.Date
     */
    public static java.util.Date formatStringToUtilDate(String strDate)
        throws FTDApplicationException {

        java.util.Date utilDate = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

        if ((strDate != null) && (!strDate.trim().equals(""))) {
            try {

                // set input date format
                dateLength = strDate.length();
                if ( dateLength > 10) {
                    inDateFormat = "yyyy-MM-dd hh:mm:ss";
                } else {
                    firstSep = strDate.indexOf("/");
                    lastSep = strDate.lastIndexOf("/");

                    switch ( dateLength ) {
                        case 10:
                            inDateFormat = "MM/dd/yyyy";
                            break;
                        case 9:
                            if ( firstSep == 1 ) {
                                inDateFormat = "M/dd/yyyy";
                            } else {
                                inDateFormat = "MM/d/yyyy";
                            }
                            break;
                        case 8:
                            if ( firstSep == 1 ) {
                                inDateFormat = "M/d/yyyy";
                            } else {
                                inDateFormat = "MM/dd/yy";
                            }
                            break;
                        case 7:
                            if ( firstSep == 1 ) {
                                inDateFormat = "M/dd/yy";
                            } else {
                                inDateFormat = "MM/d/yy";
                            }
                            break;
                        case 6:
                            inDateFormat = "M/d/yy";
                            break;
                        default:
                            break;
                    }
                }
                SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat ); 
                SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" ); 
		
                java.util.Date date = sdfInput.parse(strDate); 
                String outDateString = sdfOutput.format(date); 
	    
                // now that we have no errors, use the string to make a Util date
                utilDate = sdfOutput.parse(outDateString);
			
                } catch (Exception e) {
                   throw new FTDApplicationException(1,e);
            }
        }
	
        return utilDate;
    }
}
