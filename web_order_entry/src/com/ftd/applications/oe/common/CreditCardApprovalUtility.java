package com.ftd.applications.oe.common;

import com.ftd.applications.oe.common.valobjs.CreditCardVO;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.applications.oe.common.CCASType;

public class CreditCardApprovalUtility 
/**
*  This Class will be used to interface with the NDS Credit Card System
* @author Mike McCormack
*/
{
    public static CreditCardVO purchaseRequest(CreditCardVO creditCardVo, CCASType ccasType) throws Exception
	/**
     * Process a Purchase Request for a Credit Card purchase
     *
     * @param CreditCardVo A VO that represents Credit Card information
     *                      required attributes that need to be entered
     *                          creditCardNumber
     *                          addressLine1
     *                          expirationDate (YYMM)
     *                          zipCode (9 digit)
     *                          amount (Amount of purchase max length 8)
     * 
     * 
     * @return CreditCardVO A VO that represents Credit Card Information
     *                      Attributes to be populated
     *                          approvedAmount
     *                          approvalCode
     *                          status
     *                          actionCode
     *                          verbiage
     *                          AVSIndicator
     *                          batchNumber
     *                          dateStamp
     *                          timeStamp
     *                          acquirerReferenceData
     *                          userData
     *
     * @exception Exception if any error occurs
     */
    {
    	CCAS ccas = CCAS.getInstance();
        creditCardVo.setTransactionType(GeneralConstants.OE_CCAS_PURCHASE_REQUEST);

        creditCardVo = ccas.processCreditCard(creditCardVo, ccasType);

        return creditCardVo;
    }

//    public static CreditCardVO jCPurchaseRequest(CreditCardVO creditCardVo) throws Exception
//	/**
//     * Process a JC Penney Purchase Request for a Credit Card purchase
//     *
//     * @param CreditCardVo A VO that represents Credit Card information
//     *                      required attributes that need to be entered
//     *                          creditCardNumber
//     *                          addressLine1
//     *                          expirationDate (YYMM)
//     *                          zipCode (9 digit)
//     *                          amount (Amount of purchase max length 8)
//     * 
//     * 
//     * @return CreditCardVO A VO that represents Credit Card Information
//     *                      Attributes to be populated
//     *                          approvedAmount
//     *                          approvalCode
//     *                          status
//     *                          actionCode
//     *                          verbiage
//     *                          AVSIndicator
//     *                          batchNumber
//     *                          dateStamp
//     *                          timeStamp
//     *                          acquirerReferenceData
//     *                          userData
//     *
//     * @exception Exception if any error occurs
//     */
//    {
//    	CCAS ccas = CCAS.getInstance();
//        creditCardVo.setTransactionType(GeneralConstants.OE_CCAS_JC_PURCHASE_REQUEST);
//
//        creditCardVo = ccas.processJCCreditCard(creditCardVo);
//
//        return creditCardVo;
//    }

//    public static CreditCardVO SFMBPurchaseRequest(CreditCardVO creditCardVo) throws Exception
//	/**
//     * Process a SFMB Purchase Request for a Credit Card purchase
//     *
//     * @param CreditCardVo A VO that represents Credit Card information
//     *                      required attributes that need to be entered
//     *                          creditCardNumber
//     *                          addressLine1
//     *                          expirationDate (YYMM)
//     *                          zipCode (9 digit)
//     *                          amount (Amount of purchase max length 8)
//     * 
//     * 
//     * @return CreditCardVO A VO that represents Credit Card Information
//     *                      Attributes to be populated
//     *                          approvedAmount
//     *                          approvalCode
//     *                          status
//     *                          actionCode
//     *                          verbiage
//     *                          AVSIndicator
//     *                          batchNumber
//     *                          dateStamp
//     *                          timeStamp
//     *                          acquirerReferenceData
//     *                          userData
//     *
//     * @exception Exception if any error occurs
//     */
//    {
//    {
//        CCAS ccas = CCAS.getInstance();
//        creditCardVo.setTransactionType(GeneralConstants.OE_CCAS_PURCHASE_REQUEST);
//
//        creditCardVo = ccas.processSFMBCreditCard(creditCardVo);
//
//        return creditCardVo;
//    }
//
//    }

//    public static CreditCardVO GiftPurchaseRequest(CreditCardVO creditCardVo) throws Exception
//	/**
//     * Process a Gift Site Purchase Request for a Credit Card purchase
//     *
//     * @param CreditCardVo A VO that represents Credit Card information
//     *                      required attributes that need to be entered
//     *                          creditCardNumber
//     *                          addressLine1
//     *                          expirationDate (YYMM)
//     *                          zipCode (9 digit)
//     *                          amount (Amount of purchase max length 8)
//     * 
//     * 
//     * @return CreditCardVO A VO that represents Credit Card Information
//     *                      Attributes to be populated
//     *                          approvedAmount
//     *                          approvalCode
//     *                          status
//     *                          actionCode
//     *                          verbiage
//     *                          AVSIndicator
//     *                          batchNumber
//     *                          dateStamp
//     *                          timeStamp
//     *                          acquirerReferenceData
//     *                          userData
//     *
//     * @exception Exception if any error occurs
//     */
//    {
//    	CCAS ccas = CCAS.getInstance();
//        creditCardVo.setTransactionType(GeneralConstants.OE_CCAS_PURCHASE_REQUEST);
//
//        creditCardVo = ccas.processGiftCreditCard(creditCardVo);
//
//        return creditCardVo;
//    }

//    public static CreditCardVO HighEndPurchaseRequest(CreditCardVO creditCardVo) throws Exception
//	/**
//     * Process a High End Site Purchase Request for a Credit Card purchase
//     *
//     * @param CreditCardVo A VO that represents Credit Card information
//     *                      required attributes that need to be entered
//     *                          creditCardNumber
//     *                          addressLine1
//     *                          expirationDate (YYMM)
//     *                          zipCode (9 digit)
//     *                          amount (Amount of purchase max length 8)
//     * 
//     * 
//     * @return CreditCardVO A VO that represents Credit Card Information
//     *                      Attributes to be populated
//     *                          approvedAmount
//     *                          approvalCode
//     *                          status
//     *                          actionCode
//     *                          verbiage
//     *                          AVSIndicator
//     *                          batchNumber
//     *                          dateStamp
//     *                          timeStamp
//     *                          acquirerReferenceData
//     *                          userData
//     *
//     * @exception Exception if any error occurs
//     */
//    {
//    	CCAS ccas = CCAS.getInstance();
//        creditCardVo.setTransactionType(GeneralConstants.OE_CCAS_PURCHASE_REQUEST);
//
//        creditCardVo = ccas.processHighEndCreditCard(creditCardVo);
//
//        return creditCardVo;
//    }

    public static CreditCardVO aafesPurchaseRequest(CreditCardVO creditCardVo) throws Exception
	/**
     * Process a AAFES Purchase Request for a Credit Card purchase
     *
     * @param CreditCardVo A VO that represents Credit Card information
     *                      
     * 
     * 
     * @return CreditCardVO A VO that represents Credit Card Information
 
     *
     * @exception Exception if any error occurs
     */
    {
        creditCardVo.setTransactionType("A");

        creditCardVo = AafesAuthUtil.processCreditCard(creditCardVo);

        return creditCardVo;
    }
}
