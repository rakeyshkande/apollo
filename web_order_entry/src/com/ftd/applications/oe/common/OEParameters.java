package com.ftd.applications.oe.common;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

public class OEParameters implements Serializable
{
    private String GNADDLevel = "0";
    private Date GNADDDate = null;
    private int intlAddOnDays = 0;
    private String intlCutoff = "1200";
    private String mondayCutoff = "1400";
    private String tuesdayCutoff = "1400";
    private String wednesdayCutoff = "1400";
    private String thursdayCutoff = "1400";
    private String fridayCutoff = "1400";
    private String saturdayCutoff = "1400";
    private String sundayCutoff  = "1200";
    private String specialtyGiftCutoff = "1200";
    private String exoticCutoff = "1200";
    private String freshCutCutoff = "1200";
    private String latestCutoff = "2359";
    private BigDecimal freshCutSrvcCharge = new BigDecimal(0);
    private BigDecimal specialSrvcCharge = new BigDecimal(0);
    private String freshCutSrvcChargeTrigger = "Y";
    private BigDecimal canadianExchangeRate = new BigDecimal(0);
    private String dispatchOrderFlag;
    private BigDecimal freshCutSatCharge = new BigDecimal(0);
    private boolean defaulted = true;
    private int deliveryDaysOutMAX;
    private int deliveryDaysOutMIN;
    private String allowSubstitution;
    private BigDecimal fuelSurchargeAmt = new BigDecimal(0); 

    public String getLatestCutoff()
    {
        return latestCutoff;
    }

    public void setLatestCutoff(String newLatestCutoff)
    {
        latestCutoff = newLatestCutoff;
    }

    public String getAllowSubstitution()
    {
        return allowSubstitution;
    }

    public void setAllowSubstitution(String newAllowSubstitution)
    {
        allowSubstitution = newAllowSubstitution;
    }
    
    public String getGNADDLevel()
    {
        return GNADDLevel;
    }

    public void setGNADDLevel(String newGNADDLevel)
    {
        GNADDLevel = newGNADDLevel;
    }

    public Date getGNADDDate()
    {
        return GNADDDate;
    }

    public void setGNADDDate(Date newGNADDDate)
    {
        GNADDDate = newGNADDDate;
    }

    public int getIntlAddOnDays()
    {
        return intlAddOnDays;
    }

    public void setIntlAddOnDays(int newIntlAddOnDays)
    {
        intlAddOnDays = newIntlAddOnDays;
    }

    public String getMondayCutoff()
    {
        return mondayCutoff;
    }

    public void setMondayCutoff(String newMondayCutoff)
    {
        mondayCutoff = newMondayCutoff;
    }

    public String getTuesdayCutoff()
    {
        return tuesdayCutoff;
    }

    public void setTuesdayCutoff(String newTuesdayCutoff)
    {
        tuesdayCutoff = newTuesdayCutoff;
    }

    public String getWednesdayCutoff()
    {
        return wednesdayCutoff;
    }

    public void setWednesdayCutoff(String newWednesdayCutoff)
    {
        wednesdayCutoff = newWednesdayCutoff;
    }

    public String getThursdayCutoff()
    {
        return thursdayCutoff;
    }

    public void setThursdayCutoff(String newThursdayCutoff)
    {
        thursdayCutoff = newThursdayCutoff;
    }

    public String getFridayCutoff()
    {
        return fridayCutoff;
    }

    public void setFridayCutoff(String newFridayCutoff)
    {
        fridayCutoff = newFridayCutoff;
    }

    public String getSaturdayCutoff()
    {
        return saturdayCutoff;
    }

    public void setSaturdayCutoff(String newSaturdayCutoff)
    {
        saturdayCutoff = newSaturdayCutoff;
    }

    public String getSundayCutoff()
    {
        return sundayCutoff;
    }

    public void setSundayCutoff(String newSundayCutoff)
    {
        sundayCutoff = newSundayCutoff;
    }

    public int getDeliveryDaysOutMIN()
    {
        return deliveryDaysOutMIN;
    }

    public void setDeliveryDaysOutMIN(int newDeliveryDaysOutMIN)
    {
        deliveryDaysOutMIN = newDeliveryDaysOutMIN;
    }

    public int getDeliveryDaysOutMAX()
    {
        return deliveryDaysOutMAX;
    }

    public void setDeliveryDaysOutMAX(int newDeliveryDaysOutMAX)
    {
        deliveryDaysOutMAX = newDeliveryDaysOutMAX;
    }
    
    public String getIntlCutoff()
    {
        return intlCutoff;
    }

    public void setIntlCutoff(String newIntlCutoff)
    {
        intlCutoff = newIntlCutoff;
    }

    public String getSpecialtyGiftCutoff()
    {
        return specialtyGiftCutoff;
    }

    public void setSpecialtyGiftCutoff(String newSpecialtyGiftCutoff)
    {
        specialtyGiftCutoff = newSpecialtyGiftCutoff;
    }

    public String getExoticCutoff()
    {
        return exoticCutoff;
    }

    public void setExoticCutoff(String newExoticCutoff)
    {
        exoticCutoff = newExoticCutoff;
    }

    public String getFreshCutCutoff()
    {
        return freshCutCutoff;
    }

    public void setFreshCutCutoff(String newFreshCutCutoff)
    {
        freshCutCutoff = newFreshCutCutoff;
    }

    public BigDecimal getFreshCutSrvcCharge()
    {
        return freshCutSrvcCharge;
    }

    public void setFreshCutSrvcCharge(BigDecimal newFreshCutSrvcCharge)
    {
        freshCutSrvcCharge = newFreshCutSrvcCharge;
    }

    public BigDecimal getSpecialSrvcCharge()
    {
        return specialSrvcCharge;
    }

    public void setSpecialSrvcCharge(BigDecimal newSpecialSrvcCharge)
    {
        specialSrvcCharge = newSpecialSrvcCharge;
    }

    public String getFreshCutSrvcChargeTrigger()
    {
        return freshCutSrvcChargeTrigger;
    }

    public void setFreshCutSrvcChargeTrigger(String newFreshCutSrvcChargeTrigger)
    {
        freshCutSrvcChargeTrigger = newFreshCutSrvcChargeTrigger;
    }

    public void setCanadianExchangeRate(BigDecimal newCanadianExchangeRate)
    {
        canadianExchangeRate = newCanadianExchangeRate;
    }

    public BigDecimal getCanadianExchangeRate()
    {
        return canadianExchangeRate;
    }

    public String getDispatchOrderFlag()
    {
        return dispatchOrderFlag;
    }

    public void setDispatchOrderFlag(String newDispatchOrderFlag)
    {
        dispatchOrderFlag = newDispatchOrderFlag;
    }

    public void setFreshCutSatCharge(BigDecimal newFreshCutSatCharge)
    {
        freshCutSatCharge = newFreshCutSatCharge;
    }

    public BigDecimal getFreshCutSatCharge()
    {
        return freshCutSatCharge;
    }

    public void setDefaulted( boolean newDefaulted )
    {
        defaulted = newDefaulted;
    }

    public boolean isDefaulted()
    {
        return defaulted;
    }

    public void setFuelSurchargeAmt(BigDecimal newFuelSurchargeAmt) {
        this.fuelSurchargeAmt = newFuelSurchargeAmt;
    }

    public BigDecimal getFuelSurchargeAmt() {
        return fuelSurchargeAmt;
    }
}
