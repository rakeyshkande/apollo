package com.ftd.applications.oe.common;

import java.math.BigDecimal;
import java.io.Serializable;

public class OEAddOn implements Serializable, Cloneable
{
    private BigDecimal addOnPrice;
    private String addOnDescription;
    private String addOnId;
    private Integer addOnQty;
    private String addOnType;

    public OEAddOn()
    {
    }

    public BigDecimal getAddOnPrice()
    {
        return addOnPrice;
    }

    public void setAddOnPrice(BigDecimal newAddOnPrice)
    {
        addOnPrice = newAddOnPrice;
    }

    public String getAddOnDescription()
    {
        return addOnDescription;
    }

    public void setAddOnDescription(String newAddOnDescription)
    {
        addOnDescription = newAddOnDescription;
    }

    public String getAddOnId()
    {
        return addOnId;
    }

    public void setAddOnId(String newAddOnId)
    {
        addOnId = newAddOnId;
    }

    public Integer getAddOnQty()
    {
        return addOnQty;
    }

    public void setAddOnQty(Integer newAddOnQty)
    {
        addOnQty = newAddOnQty;
    }

    public String getAddOnType()
    {
        return addOnType;
    }

    public void setAddOnType(String newAddOnType)
    {
        addOnType = newAddOnType;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}