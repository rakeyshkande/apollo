package com.ftd.applications.oe.common;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Checks words for misspellings and suggest properly spelled words
 *
 * The dictionary comes from a small, medium, or large file-based database. The small
 * file will be loaded at initialization stage. Medium and large file will be lazily-loaded if necessary
 *
 * Two ways to provide file names at run-time. One uses -D parameter, and the other uses a Properties 
 * in the class constructor. If both ways are used, the -D parameter one will take the precedence.
 *
 * @author William Sandner
 * @version %I, %G
 * @since JDK1.3.1
 */
public class SpellCheckImpl implements ISpellCheck
{
    private TreeSet dictionary;
    private boolean loadMediumDictionary;
    private boolean loadLargeDictionary;
    private int maxSuggestions;
    private Properties props;
    private HashSet htmlTags;

/**
  * Default constructor, pre-load the small dictionary file into memory, throw exception
  * to the caller
  */
    public SpellCheckImpl() throws IOException
    {
        this( new Properties() );
    }

/**
  * Constructor, get the file names in the Properties
  * If file names are also provided at run-time, override the programmatical file name
  * @param Properties props, list of file names
  */
    public SpellCheckImpl( Properties props ) throws IOException
    {
        dictionary = new TreeSet();
        maxSuggestions = 10;
        this.props = props;

        if ( System.getProperty( ISpellCheck.SMALL_DICTIONARY_FILE_NAME ) != null )
            props.put( ISpellCheck.SMALL_DICTIONARY_FILE_NAME, 
                System.getProperty( ISpellCheck.SMALL_DICTIONARY_FILE_NAME ) );

        if ( System.getProperty( ISpellCheck.MEDIUM_DICTIONARY_FILE_NAME ) != null )
            props.put( ISpellCheck.MEDIUM_DICTIONARY_FILE_NAME, 
                System.getProperty( ISpellCheck.MEDIUM_DICTIONARY_FILE_NAME ) );

                
        if ( System.getProperty( ISpellCheck.LARGE_DICTIONARY_FILE_NAME ) != null )
            props.put( ISpellCheck.LARGE_DICTIONARY_FILE_NAME, 
                System.getProperty( ISpellCheck.LARGE_DICTIONARY_FILE_NAME ) );

        String smallDictionaryFile = props.getProperty( ISpellCheck.SMALL_DICTIONARY_FILE_NAME );
       
        loadDictionary( smallDictionaryFile );

        htmlTags = new HashSet();
        htmlTags.add( "<br>" );
    }
    /**
     * Set the maximum suggestions for a given mis-spelled word.
     */
    public void setMaximumSuggestion ( int maxSuggestions ) 
    {
        this.maxSuggestions = maxSuggestions;
    }
    
/**
  * load the words from the file-based dictionary and store in <B>dictionary</B>
  * @param  String dictFile the name of the dictionary file
  */
    private void loadDictionary( String dictFile ) throws IOException
    {
        URL url = this.getClass().getClassLoader().getResource( dictFile );
        BufferedReader in = new BufferedReader(new FileReader( new File(url.getFile())));
        String s;
        while( (s=in.readLine()) != null)
            dictionary.add( s.trim().toLowerCase() );
        in.close();   
    }

/**
  * It completely ignores words with anything except letters, dashes, and apostrophes
  * @param String word to be checked
  * @return String the processed word 
  */
    private String prepForSpellCheck( String word ) 
    {
        if ( word == null )
            return "";

        StringBuffer sb = new StringBuffer();
        String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'-";

        for ( int i=0; i<word.length(); i++ )
        {
            char c = word.charAt( i );
            if ( validChars.indexOf( (int) c) != -1 )
                sb.append( c );
        }
        return sb.toString();
    }

    /**
    * Check to see if the word in in the dictionary
    * First check the pre-loaded small_dictionary
    * if failed, then load the medium_dictionary
    * if failed, then load the large_dictionary
    * return false if failed again
    *
    * @param word a word to spell check
    * @return true if the word is in dictionary, false if not
    */
    public boolean spellCheck(String word) throws IOException {

        if ( word == null )
            return false;

        if ( htmlTags.contains( word ) )
            return true;

        word = prepForSpellCheck( word );

        word = word.trim().toLowerCase();

        if ( word.length() == 0 )
            return true;

        if ( dictionary.contains( word ) )
            return true;

        if ( !loadMediumDictionary )
        {
            loadMediumDictionary = true;            
            String mediumDictionaryFile = props.getProperty( ISpellCheck.MEDIUM_DICTIONARY_FILE_NAME );
            if ( mediumDictionaryFile != null ) 
            {
                loadDictionary( mediumDictionaryFile );
                if ( dictionary.contains( word ) )
                    return true;
            }
        }

        if ( !loadLargeDictionary )
        {
            loadLargeDictionary = true;
            String largeDictionaryFile = props.getProperty( ISpellCheck.LARGE_DICTIONARY_FILE_NAME );
            if ( largeDictionaryFile != null )
            {
                loadDictionary( largeDictionaryFile );
                if ( dictionary.contains( word ) )
                    return true;
            }
        }

        return false;
    }

/**
  * The first letter of the soundex code is the first letter of the word (line 91). 
  * The rest of the soundex code consists of numbers. 
  * For each letter of the word after the first letter, append a number to the code
  * based on the rules. Note that vowels are ignored and several letters result in the same number.
  * Only append the number if it's not a repeat of the last number added to the soundex code 
  *
  * @param String word 
  * @return String the soundex of word
  */
    private String soundex( String word ) 
    {
        if ( word == null )
            return null;
        word = word.toUpperCase();    
        
        String result = "";
        
        if ( word.length() > 0 )
            result += word.charAt( 0 );
                
        char c;
        String code = "";
        
        for (int i = 1; i < word.length(); i++) {
            c = word.charAt( i );
            
            if( c == 'B' || c == 'P' ) 
                code = "1";
            else if( c == 'F' || c == 'V') 
                code = "2";
            else if( c == 'C' || c == 'K' || c == 'S') 
                code = "3";
            else if( c == 'J' || c == 'G') 
                code = "4";
            else if( c == 'Q' || c == 'X' || c =='Z') 
                code = "5";
            else if( c == 'D' || c == 'T') 
                code = "6";
            else if( c == 'L') 
                code = "7";
            else if( c == 'M' || c == 'N' ) 
                code = "8";
            else if( c == 'R' ) 
                code = "9";
            else
                code = "";             

            if( !result.substring( result.length() -1 ).equals( code ) )
              result += code;
        }
        return result;
    }

/**
  * Calculate the word similarity index
  * @param String strWord
  * @param String similarWord
  * @return Float the value to evaluate the similarity
  */
    private float wordSimilarity(String strWord, String similarWord) 
    {
        strWord = strWord.toLowerCase();
        similarWord = similarWord.toLowerCase();
        
        int wordLen = strWord.length();
        int similarWordLen = similarWord.length();
        int maxBonus = 3;
        int perfectValue = wordLen + wordLen + maxBonus;
        
        int similarityValue;
     
        similarityValue = maxBonus - Math.abs(wordLen - similarWordLen);

        int minLength = Math.min( wordLen, similarWordLen );

        for (int i = 0; i < minLength; i++) 
        {
            if ( strWord.charAt( i ) == similarWord.charAt( i ) )   
                similarityValue += 1;    // forward match 

            if ( strWord.charAt( strWord.length() - i -1 ) == similarWord.charAt( similarWord.length() - i -1 ) )
                similarityValue += 1;   // backward match
            
        }

        return ((float)similarityValue / (float)perfectValue);
    }

/** Find the suggested word set
  * @param String original word
  * @return Set a set of suggestion words
  */

    public List suggest ( String strWord )
    {
        if ( strWord == null || strWord.length() == 0 )
            return null;
            
        TreeSet result = new TreeSet();
        String soundexWord = this.soundex( strWord );

        strWord = this.prepForSpellCheck( strWord ).toLowerCase();

        if ( strWord == null || strWord.length() == 0 )
            return null;

        char startChar = strWord.charAt( 0 );
        char endChar = (char) (startChar + 1);
        SortedSet searchRange = dictionary.subSet( (new Character(startChar)).toString(), (new Character(endChar)).toString() );

        Iterator iter = searchRange.iterator();
        while (iter.hasNext() )
        {
            String tempWord = (String)iter.next();
            if ( soundex(tempWord).equals( soundexWord ) ) 
            {
                result.add( new SuggestWord( tempWord, wordSimilarity( strWord, tempWord ) ) );
            }
        }

        if ( result.isEmpty() )
            return null;
            
        int suggestionNumber = Math.min( result.size(), this.maxSuggestions );
        int index = 0;
        
        ArrayList suggested = new ArrayList();
        Iterator iter1 = result.iterator();

        while ( iter1.hasNext() )
        {
            suggested.add( ((SuggestWord)iter1.next()).word );
            if ( ++index >= suggestionNumber )
                break;
        }
        
        return suggested;
    }

    class SuggestWord implements Comparable 
    {
        public float similarity;
        public String word;

        public SuggestWord( String word, float similarity )
        {
            this.word = word;
            this.similarity = similarity;            
        }

        public int compareTo( Object other )
        {
            if ( other instanceof SuggestWord )
            {
                SuggestWord second = (SuggestWord)other;
                if ( this.similarity == second.similarity )
                    return this.word.compareTo( second.word );
                else
                    return (this.similarity > second.similarity) ? -1 : 1;
            }

            return this.toString().compareTo( other.toString() );
        }
        
        public boolean equals( Object other )
        {
            if ( other instanceof SuggestWord) 
            {
                SuggestWord otherWord = (SuggestWord)other;
                return ( this.word.equals(otherWord.word) &&
                         this.similarity == otherWord.similarity );
            }

            return false;
        }
    }
}
