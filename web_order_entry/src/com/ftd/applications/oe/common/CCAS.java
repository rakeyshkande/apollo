package com.ftd.applications.oe.common;

import java.io.*;
import java.net.*;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.HttpsURLConnection;
import com.ftd.applications.oe.common.valobjs.CreditCardVO;
import com.ftd.framework.common.utilities.*;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import com.ftd.applications.oe.common.CCASType;
import com.ftd.osp.utilities.ConfigurationUtil;

/**
 * CCAS
 * This Class sends and receives socket messages.
 */
public class CCAS 
{
    private final static String CCAS_CONFIG_CONTEXT = "CCAS_CONFIG";
	/**
	 * The unique instance of this class.
	 */
	private static CCAS instance = new CCAS();
	/**
	 * Socket pools
	 */
	private GenericObjectPool ftdSocketPool;
	private GenericObjectPool giftSocketPool;
	private GenericObjectPool highSocketPool;
	private GenericObjectPool sfmbSocketPool;
	private GenericObjectPool jcpSocketPool;
  private GenericObjectPool fusaSocketPool; 
  private GenericObjectPool fdirectSocketPool; 
  private GenericObjectPool floristSocketPool;

	private CCAS() {
		
	}

	/**
	 * Return the unique instance of this class.
	 *
	 * @return the unique instance of this class
	 */
	public static CCAS getInstance() {
		return instance;
	}

    public CreditCardVO processCreditCard(CreditCardVO creditCardVo, CCASType type) throws Exception
    /**
    *  This method will take in a Credit Card number in a CreditCardVo object.
    * @param CreditCardVO - Value Object that represents Credit Card Information
    * @param CCASType - Indicates what type of credit card processing is to be performed
    * @return CreditCardVO - Value Object that represents Credit Card information
    */
    {
//        LogManager lm = new LogManager(GeneralConstants.PDB_COMMON_UTILITIES_CATEGORY_NAME);
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String sendPacket = null;
        String receivePacket = null;
        String creditCardXML = null;

        // Determine whether authorization is disabled using framework config
        String ccasOn = cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.auth.active");
        if ( ccasOn.equals("N") || ccasOn.equals("n") )
        {
            throw new Exception("Credit card authorization disabled");
        }

        sendPacket = buildPacket(creditCardVo);
        
//        System.out.println("Send Packet: " + sendPacket);
//        System.out.println("template   : |--------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|");
        if(sendPacket != null)
        {
            //receivePacket = send(sendPacket, port, server);
        	  receivePacket = send(sendPacket, type); //Pass through the ccas type
            creditCardVo = parsePacket(receivePacket, creditCardVo);
//            System.out.println("return Pckt: " + receivePacket);
        }

        return creditCardVo;
    }

    private String send( String transmitData, CCASType type ) throws Exception {
    	
    	SSLSocket socket = null;
    	PrintWriter out;
    	BufferedReader in;
    	String response = "";
    	GenericObjectPool pool = null;
    	SocketParms params = null;
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
    	String serverName = cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.auth.high.server");
    	
    	try {
          switch (type.getType()) {
            case CCASType.TYPE_FTD_INT:
	    			pool = getFTDPool();
	    			break;
            case CCASType.TYPE_GIFT_INT:
	    			pool = getGiftPool();
	    			break;
	    		case CCASType.TYPE_HIGH_INT:
	    			pool = getHighPool();
	    			break;
            case CCASType.TYPE_FUSA_INT:
              pool = getFusaPool();
              break;
            case CCASType.TYPE_FDIRECT_INT:
              pool = getFdirectPool();
              break;
            case CCASType.TYPE_FLORIST_INT:
              pool = getFloristPool();
              break;
            case CCASType.TYPE_JCP_INT:
              pool = getJCPPool();
              break;
            case CCASType.TYPE_SFMB_INT:
	    			pool = getSfmbPool();
	    			break;
	    		default:
	    			throw new Exception("Unknown company type in CCAS.send");
	    	}
	    	
	    	params  = (SocketParms)pool.borrowObject();

            String testSSLAuth = cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ssl.auth.cert.check");
            if (testSSLAuth.equals("OFF"))
            {
                SSLUtilities.trustAllHostnames();
                SSLUtilities.trustAllHttpsCertificates();
            }

            javax.net.SocketFactory factory;
            factory = HttpsURLConnection.getDefaultSSLSocketFactory();
            socket = (SSLSocket) factory.createSocket(params.getHostname(),params.getPort());
            socket.setSoTimeout(params.getTimeout());
	    socket.startHandshake();

	    	
	    	try 
	        {
	    		out = new PrintWriter(socket.getOutputStream(), true);
	    		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	        }
	        catch (UnknownHostException e)
	        {            
	            String[] args = {serverName};
	//            lm.error(GeneralConstants.OE_UNKNOWN_HOST_EXCEPTION, args);
	            throw new Exception("CCAS unknown host " + serverName);
	        }
	        catch (java.net.ConnectException e)
	        {
	        	e.printStackTrace();
	            String[] args = {serverName};
	//            lm.error(GeneralConstants.OE_IO_EXCEPTION, args);
	            throw new Exception("CCAS error connecting to " + serverName + ".  " + e.getMessage());   
	        }
	        catch (IOException e)
	        {
	        	e.printStackTrace();
	            String[] args = {serverName};
	//            lm.error(GeneralConstants.OE_IO_EXCEPTION, args);
	            throw new Exception("CCAS IO exception sending to " + serverName + ".  " + e.getMessage());   
	        }
	
	        out.println(transmitData);
	        
	       try
	       {
	           StringBuffer sb = new StringBuffer();
	           int i;
	           char[] buffer = new char[5120];
	           in.read( buffer, 0, buffer.length );
	           response = (new String( buffer ) ).substring(0,110);
	           
	 		}
			catch(IOException ioe)
			{
	           String[] args = {serverName};
	//           lm.error(GeneralConstants.OE_IO_EXCEPTION, args);
	           throw new Exception("CCAS IO exception reading from " + serverName);   
			}
    	} finally {
    		if( socket!=null ) {
    			try {
    	   			if ( !socket.isInputShutdown() ) {
    	   				socket.getInputStream().close();
    	   			}
       			} catch (Exception e) {
       				//ignore it
       			}
       			
       			try {
    	   			if ( !socket.isOutputShutdown() ) {
    	   				socket.getOutputStream().close();
    	   			}
       			} catch (Exception e) {
       				//ignore it
       			}
       			
       			try {
    	   			if ( socket.isConnected() ) {
    	   				socket.close();
    	   			}
       			} catch (Exception e) {
       				//ignore it
       			}
    		}
    		
    		if( pool!=null && params !=null )
    			pool.returnObject(params);
    	}
    	return response;
    }
    
    private String buildPacket(CreditCardVO creditVo) throws Exception
  /**
   * Build packet to be sent over socket.
   *
   * @param CreditCardVO - credit Card value object containing credit data
   * @return String - Send Packet
   */
    {
        String newPacket = null;
        String blanks = "                         ";
        String zeroes = "000000000";
//        LogManager lm = new LogManager(GeneralConstants.OE_COMMON_UTILITIES_CATEGORY_NAME);
        
        newPacket = creditVo.getTransactionType();
        newPacket = newPacket.concat("0081");
        String tempCCNumber = creditVo.getCreditCardNumber();
        if (tempCCNumber.length() > 24)
        {
//            lm.error(GeneralConstants.OE_INVALID_CREDIT_CARD_NUMBER, null);
            throw new Exception("CCAS build packet invalid credit card number");
        }
        if (tempCCNumber.length() < 24)
        {
            newPacket = newPacket.concat(blanks.substring(0,24 - tempCCNumber.length()));
        }
        newPacket = newPacket.concat(tempCCNumber);

        String tempExpDate = creditVo.getExpirationDate();
        if(tempExpDate != null && tempExpDate.length() == 4)
        {
            newPacket = newPacket.concat(tempExpDate);
        } else {
//            lm.error(GeneralConstants.OE_INVALID_CREDIT_CARD_EXP_DATE, null);
            throw new Exception("CCAS build packet invalid credit card expiration");
        }
                    
        String tempAmount = String.valueOf((int)(Float.parseFloat(creditVo.getAmount()) * 100));
        if (tempAmount.length() > 8)
        {
//            lm.error(GeneralConstants.OE_INVALID_CREDIT_CARD_AMOUNT, null);
            throw new Exception("CCAS build packet invalid credit card amount");
        }
        if (tempAmount.length() < 8)
        {
            newPacket = newPacket.concat(zeroes.substring(0,8 - tempAmount.length()));
        }
        newPacket = newPacket.concat(tempAmount);
                    
        String tempZip = creditVo.getZipCode();
        // remove dash from zip code
        if (tempZip.length() == 10 && tempZip.indexOf("-") != -1) 
        {
          tempZip = tempZip.substring(0,5) + tempZip.substring(6,10);
        }
        if (tempZip.length() > 9)
        {
//            lm.error(GeneralConstants.OE_INVALID_CREDIT_CARD_ZIPCODE, null);
            throw new Exception("CCAS build packet invalid credit card zip");
        }
        newPacket = newPacket.concat(tempZip);
        if (tempZip.length() < 9)
        {
            newPacket = newPacket.concat(zeroes.substring(0,9 - tempZip.length()));
        }
                    
        String tempAddress = creditVo.getAddressLine();
        if (tempAddress.length() > 24)
        {
           newPacket = newPacket.concat(tempAddress.substring(0,24));
        } else {
            newPacket = newPacket.concat(tempAddress);
            if (tempAddress.length() < 24)
            {
                newPacket = newPacket.concat(blanks.substring(0,24 - tempAddress.length()));
            }
        }
        
        String csc = creditVo.getCscValue();
        newPacket = newPacket.concat(csc==null ? "" : csc);
        newPacket = newPacket.concat(blanks.substring(0,10-(csc==null ? 0 : csc.length())));
                    
        newPacket = newPacket.concat("O/E*12");
        return newPacket;

    }
    
    private CreditCardVO parsePacket(String inPacket, CreditCardVO creditCardVo) 
  /**
   * Build packet to be sent over socket.
   *
   * @param String - received Packet from CCAS
   * @return CreditCardVO - credit Card value object containing new credit data
   */
    {    
        creditCardVo.setTransactionType(inPacket.substring(0,2));
        creditCardVo.setStatus(inPacket.substring(6,7));
        creditCardVo.setActionCode((inPacket.substring(7,10)).trim());
        creditCardVo.setVerbiage((inPacket.substring(10,30)).trim());
        creditCardVo.setApprovalCode((inPacket.substring(30,36)).trim());
        creditCardVo.setDateStamp((inPacket.substring(36,42)).trim());
        creditCardVo.setTimeStamp((inPacket.substring(42,48)).trim());
        creditCardVo.setAVSIndicator((inPacket.substring(48,49)).trim());
        creditCardVo.setBatchNumber((inPacket.substring(49,53)).trim());
        creditCardVo.setItemNumber((inPacket.substring(53,57)).trim());
        creditCardVo.setApprovedAmount((inPacket.substring(57,65)).trim());
        creditCardVo.setAcquirerReferenceData((inPacket.substring(65,100)).trim());
        creditCardVo.setUserData((inPacket.substring(100,110)).trim());
 

        return creditCardVo;

    } 
    
    private GenericObjectPool getFTDPool() throws Exception {
    	if( ftdSocketPool==null ) {
    		ftdSocketPool=setupPool(ftdSocketPool,CCASType.TYPE_FTD);
    	}
    	
    	return ftdSocketPool;
    }
    
    
    
    private GenericObjectPool getGiftPool() throws Exception {
    	if( giftSocketPool==null ) {
    		giftSocketPool=setupPool(giftSocketPool,CCASType.TYPE_GIFT);
    	}
    	
    	return giftSocketPool;
    }
    
    
    private GenericObjectPool getHighPool() throws Exception {
    	if( highSocketPool==null ) {
    		highSocketPool=setupPool(highSocketPool,CCASType.TYPE_HIGH);
    	}
    	
    	return highSocketPool;
    }

    private GenericObjectPool getSfmbPool() throws Exception {
    	if( sfmbSocketPool==null ) {
    		sfmbSocketPool=setupPool(sfmbSocketPool,CCASType.TYPE_SFMB);
    	}
    	
    	return sfmbSocketPool;
    }
   
    private GenericObjectPool getJCPPool() throws Exception {
    	if( jcpSocketPool==null ) {
    		jcpSocketPool=setupPool(jcpSocketPool,CCASType.TYPE_JCP);
    	}
    	
    	return jcpSocketPool;
    }

    private GenericObjectPool getFusaPool() throws Exception {
    	if( fusaSocketPool==null ) {
    		fusaSocketPool=setupPool(fusaSocketPool,CCASType.TYPE_FUSA);
            }
    	
    	return fusaSocketPool;
    	}
    
    private GenericObjectPool getFdirectPool() throws Exception {
    	if( fdirectSocketPool==null ) {
    		fdirectSocketPool=setupPool(fdirectSocketPool,CCASType.TYPE_FDIRECT);
    }
    
        return fdirectSocketPool;
    	}
    	
    private GenericObjectPool getFloristPool() throws Exception {
    	if( floristSocketPool==null ) {
    		floristSocketPool=setupPool(fdirectSocketPool,CCASType.TYPE_FLORIST);
    }
    
        return floristSocketPool;
    }

  /**
   * @param pool The pool that needs to be configured 
   * @param ccasType String CCASType for the pool that needs to be configured.  
   * this value is used to get the properties from the config file.
   * @return The pool that is configured for the passed ccas type
   * @throws Exception 
   */
  private GenericObjectPool setupPool(GenericObjectPool pool, CCASType ccasType) throws Exception
  {
    if (pool == null && ccasType != null)
    {
      synchronized(ccasType) {
          ConfigurationUtil cu = ConfigurationUtil.getInstance();
          int port;
          String server = cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, ccasType.getServerConfigName());
          try
          {
            port = Integer.parseInt(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, ccasType.getPortConfigName()));
            pool = new GenericObjectPool(new SocketFactory(server, port, getAuthTimeout()));
            setPoolOptions(pool);
            }
          catch (NumberFormatException e)
          {
                throw new Exception("Unable to parse port number for credit card auth");
            }
    	}
    }
      
      return pool;
  }
    
    private void setPoolOptions(GenericObjectPool pool) throws Exception
    {
    	if( pool!=null ) {
	    	pool.setMaxActive(getMaxActiveConnections());
	    	pool.setMaxIdle(getMaxIdleConnections());
	    	pool.setMaxWait(getMaxConnectionTimeout());
	    	pool.setMinIdle(getMinIdleConnections());
	    	pool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    	}
    }

	/**
	 * Determines the number of milliseconds to wait for a resonse from the 
	 * credit card server once a connection has been established
	 * 
	 * @return int The number of milliseconds to wait 
	 */
	public int getAuthTimeout() throws Exception
    {
		
	    ConfigurationUtil cu = ConfigurationUtil.getInstance();
        int timeout;
        try
        {
            timeout = Integer.parseInt(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.auth.timeout"));
        }
        catch(Exception e)
        {                
            timeout = 6000; // default to 6 seconds if the config file 
            // contains a bad number.
        }
		return timeout;
	}

	/**
	 * Determines the maximum number of concurrent connections allowed 
	 * 
	 * @return int The maximum number concurrent connections allowed 
	 */
	public int getMaxActiveConnections() throws Exception
    {
		
	    ConfigurationUtil cu = ConfigurationUtil.getInstance();
        int count;
        try
        {
        	count = Integer.parseInt(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.max.active.connections"));
        }
        catch(Exception e)
        {                
        	count = 9; // default to 9 connections
        }
		return count;
	}

	/**
	 * Determines the maximum number of idle connections allowed 
	 * 
	 * @return int The maximum number idle connections allowed 
	 */
	public int getMaxIdleConnections() throws Exception
    {
		
	    ConfigurationUtil cu = ConfigurationUtil.getInstance();
        int count;
        try
        {
        	count = Integer.parseInt(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.max.idle.connections"));
        }
        catch(Exception e)
        {                
        	count = 9; // default to 9 connections
        }
		return count;
	}

	/**
	 * Determines the maximum number of idle connections allowed 
	 * 
	 * @return int The maximum number idle connections allowed 
	 */
	public int getMinIdleConnections() throws Exception
    {
		
	    ConfigurationUtil cu = ConfigurationUtil.getInstance();
        int count;
        try
        {
        	count = Integer.parseInt(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.min.idle.connections"));
        }
        catch(Exception e)
        {                
        	count = 9; // default to 9 connections
        }
		return count;
	}

	/**
	 * Determines the number of milliseconds to wait for a connection 
	 * from the pool
	 * 
	 * @return int The number of milliseconds to wait 
	 */
	public int getMaxConnectionTimeout() throws Exception
    {
		
	    ConfigurationUtil cu = ConfigurationUtil.getInstance();
        int timeout;
        try
        {
            timeout = Integer.parseInt(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "ccas.connect.timeout"));
        }
        catch(Exception e)
        {                
            timeout = 6000; // default to 6 seconds if the config file 
            // contains a bad number.
        }
		return timeout;
	}
    
   class SocketFactory implements PoolableObjectFactory {
   		String serverName;
   		int port;
   		int timeout;
   		
   		public SocketFactory(String serverName, int port, int timeout) {
   			super();
   			this.serverName = serverName;
   			this.port = port;
   			this.timeout = timeout;
   		}
 
   		public Object makeObject() { 
   			return new SocketParms(serverName,port,timeout);
   		} 

   	    public void activateObject(Object obj) {
   	    }
        
   		public void passivateObject(Object obj) { 
   		} 
        
   		public void destroyObject(Object obj) { 
   		}
   		
   	    public boolean validateObject(Object obj) { 
   	    	return true;
   	    }
   }

	/*
	 * SocketParms
	 * Pooled parameters for connecting to the credit card server
	 */
	class SocketParms {
		private String hostname;
		private int port;
		private int timeout;
		
		public SocketParms(String hostname, int port, int timeout) {
			this.hostname=hostname;
			this.port=port;
			this.timeout=timeout;
		}
		
		public String getHostname() {
			return hostname;
		}
		
		public int getPort() {
			return port;
		}
		
		public int getTimeout() {
			return timeout;
		}
	}
}
