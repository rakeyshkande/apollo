package com.ftd.applications.oe.common;

import java.io.*;
import java.net.*;
//import java.security.Security;
import com.ftd.applications.oe.common.valobjs.CreditCardVO;
import com.ftd.framework.common.utilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;


public class AafesAuthUtil 
{
    private final static String CCAS_CONFIG_CONTEXT = "CCAS_CONFIG";
	private static PrintWriter out;
	private static BufferedReader in;
        private static Logger logger = new Logger("com.ftd.applications.oe.common.AafesAuthUtil");
	
    /**
    * Constructor for the AafesAuthUtil
    *
    */
    public AafesAuthUtil()
    {
		out = null;
        in = null;
	}
    
    public static CreditCardVO processCreditCard(CreditCardVO creditCardVo) throws Exception
    /**
    *  This method will take in a Credit Card number in a CreditCardVo object.
    * @param CreditCardVO - Value Object that represents Credit Card Information
    * @return CreditCardVO - CreditCardVO object that represents verification information
    */
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String sendPacket = null;
        String receivePacket = null;
       
        // Determine whether authorization is disabled using framework config
        String aafesOn = cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "aafes.auth.active");
        if ( aafesOn.equals("N") || aafesOn.equals("n") )
        {
            throw new Exception("AAFES Credit card authorization disabled");
        }

        sendPacket = buildPacket(creditCardVo);
        
        if(sendPacket != null)
        {
            receivePacket = send(sendPacket);
        }

        CreditCardVO outVO = (CreditCardVO)ObjectCloner.deepCopy(creditCardVo);        
        outVO.setStatus(receivePacket.substring(0, 1));        
        outVO.setActionCode(receivePacket.substring(2, 8).trim());        
        outVO.setAcquirerReferenceData(receivePacket.substring(9, 23).trim());
        
        try {
          outVO.setVerbiage(receivePacket.substring(24, receivePacket.length()-1));
        } catch (StringIndexOutOfBoundsException sioobe) 
        {
          outVO.setVerbiage("");
        }
    
        
        return outVO;
    }

	public static String send(String transmitData) throws Exception
  /**
   * Send data.
   *
   * @param java.lang.String - packet to be sent
   * @return String - Response Packet
   */
	{
        String upsResponse = null;  
        ConfigurationUtil cu = ConfigurationUtil.getInstance();    
        try
        {
            //Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
      
            //use url
            //System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");
            URL url = new URL(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "aafes.auth.default.server"));
                    
            // Send data
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            // Setup HTTP POST parameters
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
           
            // POST data
            OutputStream out = connection.getOutputStream();
            out.write(transmitData.getBytes());
            out.close();  

            // get data from URL connection
            upsResponse = readURLConnection(connection);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e);
            throw new Exception("Unable to retrieve credit card authorization");
        }

        return upsResponse;
    }
    
    private static String buildPacket(CreditCardVO creditVo) throws Exception
  /**
   * Build packet to be sent.
   *
   * @param CreditCardVO - credit Card value object containing credit data
   * @return String - Send Packet
   */
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();  
        String newPacket = null;
        
        //Transaction Type
        newPacket = "type=";
        newPacket = newPacket.concat(creditVo.getTransactionType());
        newPacket = newPacket.concat("&");
      
        //Military Star Charge Card Number
        newPacket = newPacket.concat("ccnumber=");        
        newPacket = newPacket.concat(creditVo.getCreditCardNumber());
        newPacket = newPacket.concat("&");

        //Amount
        newPacket = newPacket.concat("amount=");
        // changes for Issue 1875. Add 2 cents to the amount. Ganesh M
        String amount = String.valueOf((Float.parseFloat(creditVo.getAmount()) * 100) + 2);
        amount = amount.substring(0, amount.indexOf("."));
        newPacket = newPacket.concat(amount);
        newPacket = newPacket.concat("&");
                    
        //Facility Number    
        newPacket = newPacket.concat("facnbr=");    
        newPacket = newPacket.concat(cu.getFrpGlobalParm(CCAS_CONFIG_CONTEXT, "aafes.auth.facnbr"));

        return newPacket;
    }
    
    private static String readURLConnection(URLConnection uc) throws Exception
    {
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = null;

        try
        {
            reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String line = null;
            int letter = 0;
            while ((letter = reader.read()) != -1)
            {
                buffer.append((char) letter);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e);
            throw new Exception("Unable to read credit card authorization retrieved");
        }
        finally
        {
            try
            {
                reader.close();
            }
            catch (IOException io)
            {
                throw new Exception("Unable to close URLReader");
            }
        }
        return buffer.toString();
    }
}