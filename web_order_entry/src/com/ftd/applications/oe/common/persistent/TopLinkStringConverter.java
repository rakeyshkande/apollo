package com.ftd.applications.oe.common.persistent;

public class TopLinkStringConverter  {
    private static final String TOPLINK_EMPTY_STRING = "^";
    private static final String JAVA_EMPTY_STRING = "";

  /**
   * Converts a java empty string to the toplink empty string
   * This is a workaround to fix the null vs empty string issue with TopLink
   *
   * @param javaEmptyString a Java empty string
   * @return the toplink empty string
   */
    public static String convertToTopLinkEmptyString(String
javaEmptyString){
      if ( (javaEmptyString != null) &&
(javaEmptyString.trim().equals(JAVA_EMPTY_STRING)) )  {
          return TOPLINK_EMPTY_STRING;
      } else {
        return javaEmptyString;
      }
    }

  /**
   * Converts the toplink empty string to a Java empty string
   * This is a workaround to fix the null vs empty string issue with TopLink
   *
   * @param toplinkEmptyString the toplink empty string
   * @return the Java empty string
   */
    public static String convertToJavaEmptyString(String
toplinkEmptyString){
      if ( (toplinkEmptyString != null) &&
(toplinkEmptyString.trim().equals(TOPLINK_EMPTY_STRING)) )  {
          return JAVA_EMPTY_STRING;
      } else {
        return toplinkEmptyString;
      }
    }

}

