package com.ftd.applications.oe.common.persistent;

import java.util.*;
import oracle.xml.parser.v2.*;

import com.ftd.applications.oe.common.OEParameters;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.applications.oe.common.ArgumentConstants;
import com.ftd.applications.oe.services.utilities.OEOrderUTIL;
import com.ftd.framework.common.valueobjects.FTDPersistentObject;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.List;

public class OrderPO extends FTDPersistentObject implements Cloneable
{
    private String sourceCode;
    private Integer dnisCode;
    private int currentCartNumber;
    private String occasion;
    private String csrUserName;
    private String csrFirstName;
    private String csrLastName;
    private Long processingTime;
    private String companyName;
    private String scriptCode;
    private String companyId;
    private Date deliveryDate;
    private String deliveryDateDisplay;
    private String yellowPagesCode;
    private String marketingGroup;
    private BigDecimal domesticServiceFee;
    private BigDecimal internationalServiceFee;
    private String paymentMethod;
    private String paymentMethodType;
    private String rewardType;
    private String partnerId;
    private String billingPrompt;
    private String billingInfoLogic;
    private String sourceCodeDescription;
    private String pricingCode;
    private String sourceCodeType;
    private Boolean jcPenneyFlag = Boolean.FALSE;
    private Boolean yellowPagesFlag = Boolean.FALSE;
    private Boolean deliverTodayFlag;
    private String displayProductUnavailable;
    private String displayFloral = GeneralConstants.YES;
    private String displaySpecialtyGift = GeneralConstants.YES;
    private String displayNoProduct = GeneralConstants.YES;
    private Integer searchPageCount;
    private Integer searchCurrentPage;
    private int searchRecordCnt;
    private String specialFeeFlag;
    private BigDecimal totalOrderPrice = new BigDecimal(0);
    private Long totalRewardAmount = new Long(0);
    private String lastOrderAction;
    private BigDecimal taxAmount;
    private String confirmationNumber;
    private Date transactionDate;
    private String orderStatus;
    private String specialPromotionFlag;
    private String creditCardType;
    private String creditCardNumber;
    private String creditCardExpireMonth;
    private String creditCardExpireYear;
    private String invoiceNumber;
    private String giftCertificateId;
    private BigDecimal giftCertificateAmount;
    private Boolean newsletterFlag;
    private String creditCardId;
    private String creditCardTicketNumber;
    private String creditCardAuthCode;
    private String approvalCode;
    private String approvalVerbiage;
    private String approvalActionCode;
    private String avsResult;
    private String approvalAmount;
    private String acqReferenceData;
    private String deliveryDaysOut = ArgumentConstants.DELIVERY_DAYS_OUT_MIN;
    private String ncApprovalId;
    
    private OEParameters globalParameters;
    private XMLDocument searchResults;

    private static final String SEARCH_CRITERIA_KEY = "searchCrit"; 
    
    /**
     * @association <com.ftd.applications.oe.common.persistent.orderBillTo> com.ftd.applications.oe.common.persistent.CustomerPO
     */
    private CustomerPO billToCustomer;
    
    /**
     * @association <com.ftd.applications.oe.common.persistent.orderSendTo> com.ftd.applications.oe.common.persistent.CustomerPO
     */
    private CustomerPO sendToCustomer;

    /**
     * @association <com.ftd.applications.oe.common.persistent.orderItem> com.ftd.applications.oe.common.persistent.ItemPO
     */
    private Map itemMap = new HashMap();
    
    /**
     * @association <com.ftd.applications.oe.common.persistent.orderGeneric> com.ftd.applications.oe.common.persistent.MapStorePO
     */
    private Map genericMap = new HashMap();
    
    /**
     * @association <com.ftd.applications.oe.common.persistent.orderPromotion> com.ftd.applications.oe.common.persistent.PromotionPO
     */
    private List promotionList = new LinkedList();
    private String displayNoFloristHasCommonCarrier;
    private String displayNoCodifiedFloristHasCommonCarrier;
    private String firstAvailableDate;
    private String displayCodifiedFloristHasTwoDayDeliveryOnly;
    private String searchCriteria;
    

    public OrderPO()
    {
    }

    public String getSourceCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( sourceCode);
    }

    public void setSourceCode(String newSourceCode)
    {
        sourceCode = TopLinkStringConverter.convertToTopLinkEmptyString( newSourceCode);
    }

    public Integer getDnisCode()
    {
        return dnisCode;
    }

    public void setDnisCode(Integer newDnisCode)
    {
        dnisCode = newDnisCode;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDeliveryDate(Date newDeliveryDate)
    {
        deliveryDate = newDeliveryDate;
    }

    public int getCurrentCartNumber()
    {
        return currentCartNumber;
    }

    public void setCurrentCartNumber(int newCurrentCartNumber)
    {
        currentCartNumber = newCurrentCartNumber;
    }

    public void setItem(Integer cartItemNumber, ItemPO newItem)
    {
        itemMap.put(cartItemNumber, newItem);
    }

    public ItemPO getItem(Integer cartItemNumber)
    {
        return (ItemPO) itemMap.get(cartItemNumber);
    }

    public void addItem(ItemPO item) 
    {
        currentCartNumber++;
        item.setCartNumber(new Integer(currentCartNumber));
        item.setOrder(this);
        
        itemMap.put(new Integer(currentCartNumber), item);
    }

    public void removeItem(Integer cartItemNumber)
    {
        itemMap.remove(cartItemNumber);
    }

    public HashMap getAllItems()
	{
		return (HashMap) itemMap;
	}

    public LinkedList getAllInvalidItems()
    {
        LinkedList invalidItemList = new LinkedList();

        if(itemMap != null && itemMap.size() > 0) {
            ItemPO item = null;
            Integer itemKey = null;
            Iterator itemIterator = itemMap.keySet().iterator();

            while(itemIterator.hasNext()) {
                itemKey = (Integer) itemIterator.next();
                item = (ItemPO) itemMap.get(itemKey);
                if(item.getItemStatus().equals(GeneralConstants.IN_PROCESS)) {
                    invalidItemList.add(itemKey);
                }
            }
        }

        return invalidItemList;
    }

    public String getOccasion()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( occasion);
    }

    public void setOccasion(String newOccasion)
    {
        occasion = TopLinkStringConverter.convertToTopLinkEmptyString( newOccasion);
    }

    public String getCsrUserName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( csrUserName);
    }

    public void setCsrUserName(String newCsrUserName)
    {
        csrUserName = TopLinkStringConverter.convertToTopLinkEmptyString( newCsrUserName);
    }

    public String getCsrFirstName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( csrFirstName);
    }

    public void setCsrFirstName(String newCsrFirstName)
    {
        csrFirstName = TopLinkStringConverter.convertToTopLinkEmptyString( newCsrFirstName);
    }

    public String getCsrLastName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( csrLastName);
    }

    public void setCsrLastName(String newCsrLastName)
    {
        csrLastName = TopLinkStringConverter.convertToTopLinkEmptyString( newCsrLastName);
    }

    public Long getProcessingTime()
    {
        return processingTime;
    }

    public void setProcessingTime(Long newProcessingTime)
    {
        processingTime = newProcessingTime;
    }

    public String getCompanyName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( companyName);
    }

    public void setCompanyName(String newCompanyName)
    {
        companyName = TopLinkStringConverter.convertToTopLinkEmptyString( newCompanyName);
    }

    public String getScriptCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( scriptCode);
    }

    public void setScriptCode(String newScriptCode)
    {
        scriptCode = TopLinkStringConverter.convertToTopLinkEmptyString( newScriptCode);
    }

    public HashMap getGenericMap()
    {
        return (HashMap) genericMap;
    }

    public void setHeaderValue(String headerNameIn, String headerValueIn)
    {
        String headerName = TopLinkStringConverter.convertToTopLinkEmptyString(headerNameIn);
        String headerValue =  TopLinkStringConverter.convertToTopLinkEmptyString(headerValueIn);
        if(genericMap.containsKey(headerName))
        {
            ((MapStorePO) genericMap.get(headerName)).setMapValue(headerValue);
        }
        else
        {
            MapStorePO mapStorePO = new MapStorePO(this, new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.MAP_TYPE_HEADER, headerName, headerValue);
            genericMap.put(headerName, mapStorePO);
        }
    }

    public HashMap getHeaderMap() {
        HashMap tempMap = new HashMap();
        MapStorePO mapStorePO = null;

        Iterator mapValueIterator = genericMap.values().iterator();
        while(mapValueIterator.hasNext())
        {
            mapStorePO = (MapStorePO) mapValueIterator.next();
            if(mapStorePO.getMapType().equals(GeneralConstants.MAP_TYPE_HEADER))
            {
                tempMap.put(mapStorePO.getMapKey(), mapStorePO.getMapValue());
            }
        }
        
        return tempMap;
    }

    public String getCrumbURL(String crumbNameIn)
    {
        String crumbName = TopLinkStringConverter.convertToTopLinkEmptyString(crumbNameIn);
        String crumbURL = null;
        if(genericMap.containsKey(crumbName))
        {
            crumbURL = ((MapStorePO) genericMap.get(crumbName)).getMapValue();
        }

        return TopLinkStringConverter.convertToJavaEmptyString( crumbURL);
    }

    public void setCrumbURL(String crumbNameIn, String crumbUrlIn)
    {
        String crumbName = TopLinkStringConverter.convertToTopLinkEmptyString(crumbNameIn);
        String crumbUrl =  TopLinkStringConverter.convertToTopLinkEmptyString(crumbUrlIn);
        if(genericMap.containsKey(crumbName))
        {
            ((MapStorePO) genericMap.get(crumbName)).setMapValue(crumbUrl);
        }
        else
        {
            MapStorePO mapStorePO = new MapStorePO(this, new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.MAP_TYPE_CRUMB, crumbName, crumbUrl);
            genericMap.put(crumbName, mapStorePO);
        }
    }

    public void removeCrumbURL(String crumbNameIn)
    {
        String crumbName = TopLinkStringConverter.convertToTopLinkEmptyString(crumbNameIn);
        if(genericMap.containsKey(crumbName)) 
        {
            genericMap.remove(crumbName);
        }
    }

    public HashMap getCrumbMap() 
    {
        HashMap tempMap = new HashMap();
        MapStorePO mapStorePO = null;

        Iterator mapValueIterator = genericMap.values().iterator();
        while(mapValueIterator.hasNext())
        {
            mapStorePO = (MapStorePO) mapValueIterator.next();
            if(mapStorePO.getMapType().equals(GeneralConstants.MAP_TYPE_CRUMB))
            {
                tempMap.put(mapStorePO.getMapKey(), mapStorePO.getMapValue());
            }
        }
        
        return tempMap;
    }

    public HashMap getUpsellMap()
    {
        HashMap tempMap = new HashMap();
        MapStorePO mapStorePO = null;

        Iterator mapValueIterator = genericMap.values().iterator();
        while(mapValueIterator.hasNext())
        {
            mapStorePO = (MapStorePO) mapValueIterator.next();
            if(mapStorePO.getMapType().equals(GeneralConstants.MAP_TYPE_UPSELL))
            {
                tempMap.put(mapStorePO.getMapKey(), mapStorePO.getMapValue());
            }
        }
        
        return tempMap;
    }

    public String getUpsellDetail(String descriptionIn)
    {
        String description = TopLinkStringConverter.convertToTopLinkEmptyString(descriptionIn);
        String upsellValue = null;
        if(genericMap.containsKey(description))
        {
            upsellValue = ((MapStorePO) genericMap.get(description)).getMapValue();
        }

        return TopLinkStringConverter.convertToJavaEmptyString( upsellValue);
    }

    public void addUpsellDetail(String descriptionIn, String valueIn)
    {
        String description = TopLinkStringConverter.convertToTopLinkEmptyString(descriptionIn);
        String value = TopLinkStringConverter.convertToTopLinkEmptyString(valueIn);
        if(genericMap.containsKey(description))
        {
            ((MapStorePO) genericMap.get(description)).setMapValue(value);
        }
        else
        {
            MapStorePO mapStorePO = new MapStorePO(this, new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.MAP_TYPE_UPSELL, description, value);
            genericMap.put(description, mapStorePO);
        }
    }

    public void removeUpsellDetail(String upsellDetailIn)
    {
        String upsellDetail = TopLinkStringConverter.convertToTopLinkEmptyString(upsellDetailIn);
        if(genericMap.containsKey(upsellDetail)) 
        {
            genericMap.remove(upsellDetail);
        }
    }

    public void addPrompt(String promptKeyIn, String promptValueIn)
    {
        String promptKey = TopLinkStringConverter.convertToTopLinkEmptyString(promptKeyIn);
        String promptValue = TopLinkStringConverter.convertToTopLinkEmptyString(promptValueIn);
        if(genericMap.containsKey(promptKey))
        {
            ((MapStorePO) genericMap.get(promptKey)).setMapValue(promptValue);
        }
        else
        {
            MapStorePO mapStorePO = new MapStorePO(this, new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.MAP_TYPE_PROMPT, promptKey, promptValue);
            genericMap.put(promptKey, mapStorePO);
        }
    }

    public void clearPrompts()
    {
        MapStorePO mapStorePO = null;
        LinkedList removePromptList = new LinkedList();
        
        Iterator mapValueIterator = genericMap.values().iterator();
        while(mapValueIterator.hasNext())
        {
            mapStorePO = (MapStorePO) mapValueIterator.next();
            if(mapStorePO.getMapType().equals(GeneralConstants.MAP_TYPE_PROMPT))
            {
                removePromptList.add(mapStorePO.getMapKey());
            }
        }

        Iterator removePromptIterator = removePromptList.iterator();
        while(removePromptIterator.hasNext())
        {
            genericMap.remove((String) removePromptIterator.next());
        }
    }

    public String getPrompt(String promptKeyIn)
    {
        String promptKey = TopLinkStringConverter.convertToTopLinkEmptyString(promptKeyIn);
        String promptValue = null;
        if(genericMap.containsKey(promptKey))
        {
            promptValue = ((MapStorePO) genericMap.get(promptKey)).getMapValue();
        }

        return TopLinkStringConverter.convertToJavaEmptyString( promptValue);
    }

    public HashMap getPromptMap() {
        HashMap tempMap = new HashMap();
        MapStorePO mapStorePO = null;

        Iterator mapValueIterator = genericMap.values().iterator();
        while(mapValueIterator.hasNext())
        {
            mapStorePO = (MapStorePO) mapValueIterator.next();
            if(mapStorePO.getMapType().equals(GeneralConstants.MAP_TYPE_PROMPT))
            {
                tempMap.put(mapStorePO.getMapKey(), mapStorePO.getMapValue());
            }
        }
        
        return tempMap;
    }

    public void emptyMapStorage()
    {
        MapStorePO mapStorePO = null;
        LinkedList removeMapList = new LinkedList();
        
        Iterator mapValueIterator = genericMap.values().iterator();
        while(mapValueIterator.hasNext())
        {
            mapStorePO = (MapStorePO) mapValueIterator.next();
            if(!mapStorePO.getMapType().equals(GeneralConstants.MAP_TYPE_PROMPT))
            {
                removeMapList.add(mapStorePO.getMapKey());
            }
        }

        Iterator removeMapIterator = removeMapList.iterator();
        while(removeMapIterator.hasNext())
        {
            genericMap.remove((String) removeMapIterator.next());
        }
    }

    public String getCompanyId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( companyId);
    }

    public void setCompanyId(String newCompanyId)
    {
        companyId = TopLinkStringConverter.convertToTopLinkEmptyString( newCompanyId);
    }

    public CustomerPO getBillToCustomer()
    {
        if(billToCustomer == null)
        {
            billToCustomer = new CustomerPO(new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.CUSTOMER_TYPE_BILL_TO); 
        }
        
        return billToCustomer;
    }

    public void setBillToCustomer(CustomerPO newBillToCustomer)
    {
        billToCustomer = newBillToCustomer;
    }

    public CustomerPO getSendToCustomer()
    {
        if(sendToCustomer == null)
        {
            sendToCustomer = new CustomerPO(new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.CUSTOMER_TYPE_ORDER_SEND_TO); 
        }
        
        return sendToCustomer;
    }

    public void setSendToCustomer(CustomerPO newSendToCustomer)
    {
        sendToCustomer = newSendToCustomer;
    }

    public void addPromotion(PromotionPO promotion) 
    {
        promotion.setOrder(this);
        promotion.setPersistenceId(new Long(OEOrderUTIL.getUniqueNumber()).toString());
        promotionList.add(promotion);
    }

    public LinkedList getPromotionList()
    {
        return (LinkedList) promotionList;
    }
    
    public String getYellowPagesCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( yellowPagesCode);
    }

    public void setYellowPagesCode(String newYellowPagesCode)
    {
        yellowPagesCode = TopLinkStringConverter.convertToTopLinkEmptyString( newYellowPagesCode);
    }

    public String getDeliveryDateDisplay()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( deliveryDateDisplay);
    }

    public void setDeliveryDateDisplay(String newDeliveryDateDisplay)
    {
        deliveryDateDisplay = TopLinkStringConverter.convertToTopLinkEmptyString( newDeliveryDateDisplay);
    }

    public String getMarketingGroup()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( marketingGroup);
    }

    public void setMarketingGroup(String newMarketingGroup)
    {
        marketingGroup = TopLinkStringConverter.convertToTopLinkEmptyString( newMarketingGroup);
    }

    public BigDecimal getDomesticServiceFee()
    {
        return domesticServiceFee;
    }

    public void setDomesticServiceFee(BigDecimal newDomesticServiceFee)
    {
        domesticServiceFee = newDomesticServiceFee;
    }

    public BigDecimal getInternationalServiceFee()
    {
        return internationalServiceFee;
    }

    public void setInternationalServiceFee(BigDecimal newInternationalServiceFee)
    {
        internationalServiceFee = newInternationalServiceFee;
    }

    public String getPaymentMethod()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( paymentMethod);
    }

    public void setPaymentMethod(String newPaymentMethod)
    {
        paymentMethod = TopLinkStringConverter.convertToTopLinkEmptyString( newPaymentMethod);
    }

    public String getPaymentMethodType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( paymentMethodType);
    }

    public void setPaymentMethodType(String newPaymentMethodType)
    {
        paymentMethodType = TopLinkStringConverter.convertToTopLinkEmptyString( this.trim(newPaymentMethodType));
    }

    public String getRewardType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( rewardType);
    }

    public void setRewardType(String newRewardType)
    {
        rewardType = TopLinkStringConverter.convertToTopLinkEmptyString( newRewardType);
    }

    public String getPartnerId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( partnerId);
    }

    public void setPartnerId(String newPartnerId)
    {
        partnerId = TopLinkStringConverter.convertToTopLinkEmptyString( newPartnerId);
    }

    public String getBillingPrompt()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( billingPrompt);
    }

    public void setBillingPrompt(String newBillingPrompt)
    {
        billingPrompt = TopLinkStringConverter.convertToTopLinkEmptyString( newBillingPrompt);
    }

    public String getSourceCodeDescription()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( sourceCodeDescription);
    }

    public void setSourceCodeDescription(String newSourceCodeDescription)
    {
        sourceCodeDescription = TopLinkStringConverter.convertToTopLinkEmptyString( newSourceCodeDescription);
    }

    public String getPricingCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( pricingCode);
    }

    public void setPricingCode(String newPricingCode)
    {
        pricingCode = TopLinkStringConverter.convertToTopLinkEmptyString( newPricingCode);
    }

    public String getSourceCodeType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( sourceCodeType);
    }

    public void setSourceCodeType(String newSourceCodeType)
    {
        sourceCodeType = TopLinkStringConverter.convertToTopLinkEmptyString( newSourceCodeType);
    }

    public Boolean isJcPenneyFlag()
    {
        return jcPenneyFlag;
    }

    public Boolean getJcPenneyFlag()
    {
        return jcPenneyFlag;
    }

    public void setJcPenneyFlag(Boolean newJcPenneyFlag)
    {
        jcPenneyFlag = newJcPenneyFlag;
    }

    public boolean isYellowPagesFlag()
    {
        return yellowPagesFlag.booleanValue();
    }

    public Boolean getYellowPagesFlag()
    {
        return yellowPagesFlag;
    }

    public void setYellowPagesFlag(Boolean newYellowPagesFlag)
    {
        yellowPagesFlag = newYellowPagesFlag;
    }

    public Boolean isDeliverTodayFlag()
    {
        return deliverTodayFlag;
    }

    public void setDeliverTodayFlag(Boolean newDeliverTodayFlag)
    {
        deliverTodayFlag = newDeliverTodayFlag;
    }

    public String getDisplayProductUnavailable()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displayProductUnavailable);
    }

    public void setDisplayProductUnavailable(String newDisplayProductUnavailable)
    {
        displayProductUnavailable = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplayProductUnavailable);
    }

    public String getDisplayFloral()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displayFloral);
    }

    public void setDisplayFloral(String newDisplayFloral)
    {
        displayFloral = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplayFloral);
    }

    public String getDisplaySpecialtyGift()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displaySpecialtyGift);
    }

    public void setDisplaySpecialtyGift(String newDisplaySpecialtyGift)
    {
        displaySpecialtyGift = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplaySpecialtyGift);
    }

    public String getDisplayNoProduct()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displayNoProduct);
    }

    public void setDisplayNoProduct(String newDisplayNoProduct)
    {
        displayNoProduct = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplayNoProduct);
    }

    public Integer getSearchPageCount()
    {
        return searchPageCount;
    }

    public void setSearchPageCount(Integer newSearchPageCount)
    {
        searchPageCount = newSearchPageCount;
    }

    public Integer getSearchCurrentPage()
    {
        return searchCurrentPage;
    }

    public void setSearchCurrentPage(Integer newSearchCurrentPage)
    {
        searchCurrentPage = newSearchCurrentPage;
    }

    public OEParameters getGlobalParameters()
    {
        return globalParameters;
    }

    public void setGlobalParameters(OEParameters newGlobalParameters)
    {
        globalParameters = newGlobalParameters;
    }

    public XMLDocument getSearchResults() throws Exception
    {
        return searchResults;
    }

    public void setSearchResults(XMLDocument newSearchResults) throws Exception
    {
        searchResults = newSearchResults;
    }

    public String getBillingInfoLogic()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( billingInfoLogic);
    }

    public void setBillingInfoLogic(String newBillingInfoLogic)
    {
        billingInfoLogic = TopLinkStringConverter.convertToTopLinkEmptyString( newBillingInfoLogic);
    }

    public int getSearchRecordCnt()
    {
        return searchRecordCnt;
    }

    public void setSearchRecordCnt(int newSearchRecordCnt)
    {
        searchRecordCnt = newSearchRecordCnt;
    }

    public String getSpecialFeeFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( specialFeeFlag);
    }

    public void setSpecialFeeFlag(String newSpecialFeeFlag)
    {
        specialFeeFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newSpecialFeeFlag);
    }

    public BigDecimal getTotalOrderPrice()
    {
        return totalOrderPrice;
    }

    public void setTotalOrderPrice(BigDecimal newTotalOrderPrice)
    {
        totalOrderPrice = newTotalOrderPrice;
    }

    public Long getTotalRewardAmount()
    {
        return totalRewardAmount;
    }

    public void setTotalRewardAmount(Long newTotalRewardAmount)
    {
        totalRewardAmount = newTotalRewardAmount;
    }

    public String getLastOrderAction()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( lastOrderAction);
    }

    public void setLastOrderAction(String newLastOrderAction)
    {
        lastOrderAction = TopLinkStringConverter.convertToTopLinkEmptyString( newLastOrderAction);
    }

    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal newTaxAmount)
    {
        taxAmount = newTaxAmount;
    }

    public String getConfirmationNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( confirmationNumber);
    }

    public void setConfirmationNumber(String newConfirmationNumber)
    {
        confirmationNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newConfirmationNumber);
    }

    public Date getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(Date newTransactionDate)
    {
        transactionDate = newTransactionDate;
    }

    public String getOrderStatus()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( orderStatus);
    }

    public void setOrderStatus(String newOrderStatus)
    {
        orderStatus = TopLinkStringConverter.convertToTopLinkEmptyString( newOrderStatus);
    }

    public void mergeNonPersistent(oracle.toplink.publicinterface.DescriptorEvent event) 
    {
        try
        {
            OrderPO cacheObject = (OrderPO)event.getObject();
            OrderPO uowObject = (OrderPO)event.getOriginalObject();
            
            cacheObject.setSearchResults(uowObject.getSearchResults());
            cacheObject.setGlobalParameters(uowObject.getGlobalParameters());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getSpecialPromotionFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( specialPromotionFlag);
    }

    public void setSpecialPromotionFlag(String newSpecialPromotionFlag)
    {
        specialPromotionFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newSpecialPromotionFlag);
    }

    public String getCreditCardType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardType);
    }

    public void setCreditCardType(String newCreditCardType)
    {
        creditCardType = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardType);
    }

    public String getCreditCardNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardNumber);
    }

    public void setCreditCardNumber(String newCreditCardNumber)
    {
        creditCardNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardNumber);
    }

    public String getCreditCardExpireMonth()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardExpireMonth);
    }

    public void setCreditCardExpireMonth(String newCreditCardExpireMonth)
    {
        creditCardExpireMonth = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardExpireMonth);
    }

    public String getCreditCardExpireYear()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardExpireYear);
    }

    public void setCreditCardExpireYear(String newCreditCardExpireYear)
    {
        creditCardExpireYear = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardExpireYear);
    }

    public String getInvoiceNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( invoiceNumber);
    }

    public void setInvoiceNumber(String newInvoiceNumber)
    {
        invoiceNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newInvoiceNumber);
    }

    public String getGiftCertificateId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( giftCertificateId);
    }

    public void setGiftCertificateId(String newGiftCertificateId)
    {
        giftCertificateId = TopLinkStringConverter.convertToTopLinkEmptyString( newGiftCertificateId);
    }

    public BigDecimal getGiftCertificateAmount()
    {
        return giftCertificateAmount;
    }

    public void setGiftCertificateAmount(BigDecimal newGiftCertificateAmount)
    {
        giftCertificateAmount = newGiftCertificateAmount;
    }

    public Boolean isNewsletterFlag()
    {
        return newsletterFlag;
    }

    public void setNewsletterFlag(Boolean newNewsletterFlag)
    {
        newsletterFlag = newNewsletterFlag;
    }

    public String getCreditCardId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardId);
    }

    public void setCreditCardId(String newCreditCardId)
    {
        creditCardId = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardId);
    }

    public String getCreditCardTicketNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardTicketNumber);
    }

    public void setCreditCardTicketNumber(String newCreditCardTicketNumber)
    {
        creditCardTicketNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardTicketNumber);
    }

    public String getCreditCardAuthCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( creditCardAuthCode);
    }

    public void setCreditCardAuthCode(String newCreditCardAuthCode)
    {
        creditCardAuthCode = TopLinkStringConverter.convertToTopLinkEmptyString( newCreditCardAuthCode);
    }

    public String getApprovalCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( approvalCode);
    }

    public void setApprovalCode(String newApprovalCode)
    {
        approvalCode = TopLinkStringConverter.convertToTopLinkEmptyString( newApprovalCode);
    }

    public String getApprovalVerbiage()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( approvalVerbiage);
    }

    public void setApprovalVerbiage(String newApprovalVerbiage)
    {
        approvalVerbiage = TopLinkStringConverter.convertToTopLinkEmptyString( newApprovalVerbiage);
    }

    public String getApprovalActionCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( approvalActionCode);
    }

    public void setApprovalActionCode(String newApprovalActionCode)
    {
        approvalActionCode = TopLinkStringConverter.convertToTopLinkEmptyString( newApprovalActionCode);
    }

    public String getAvsResult()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( avsResult);
    }

    public void setAvsResult(String newAvsResult)
    {
        avsResult = TopLinkStringConverter.convertToTopLinkEmptyString( newAvsResult);
    }

    public String getApprovalAmount()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( approvalAmount);
    }

    public void setApprovalAmount(String newApprovalAmount)
    {
        approvalAmount = TopLinkStringConverter.convertToTopLinkEmptyString( newApprovalAmount);
    }

    public String getAcqReferenceData()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( acqReferenceData);
    }

    public void setAcqReferenceData(String newAcqReferenceData)
    {
        acqReferenceData = TopLinkStringConverter.convertToTopLinkEmptyString( newAcqReferenceData);
    }

    public String getDisplayNoFloristHasCommonCarrier()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displayNoFloristHasCommonCarrier);
    }

    public void setDisplayNoFloristHasCommonCarrier(String newDisplayNoFloristHasCommonCarrier)
    {
        displayNoFloristHasCommonCarrier = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplayNoFloristHasCommonCarrier);
    }

    public String getDisplayNoCodifiedFloristHasCommonCarrier()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displayNoCodifiedFloristHasCommonCarrier);
    }

    public void setDisplayNoCodifiedFloristHasCommonCarrier(String newDisplayNoCodifiedFloristHasCommonCarrier)
    {
        displayNoCodifiedFloristHasCommonCarrier = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplayNoCodifiedFloristHasCommonCarrier);
    }

    public String getFirstAvailableDate()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( firstAvailableDate);
    }

    public void setFirstAvailableDate(String newFirstAvailableDate)
    {
        firstAvailableDate = TopLinkStringConverter.convertToTopLinkEmptyString( newFirstAvailableDate);
    }

    public String getDisplayCodifiedFloristHasTwoDayDeliveryOnly()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( displayCodifiedFloristHasTwoDayDeliveryOnly);
    }

    public void setDisplayCodifiedFloristHasTwoDayDeliveryOnly(String newDisplayCodifiedFloristHasTwoDayDeliveryOnly)
    {
        displayCodifiedFloristHasTwoDayDeliveryOnly = TopLinkStringConverter.convertToTopLinkEmptyString( newDisplayCodifiedFloristHasTwoDayDeliveryOnly);
    }

    public String getNcApprovalId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( ncApprovalId);
    }

    public void setNcApprovalId(String newNcApprovalId)
    {
        ncApprovalId = TopLinkStringConverter.convertToTopLinkEmptyString( newNcApprovalId);
    }

    private String trim(String str)
    {
        String ret = "";
        if(str != null)
        {
            ret = str.trim();
        }

        return ret;
    }

    public String getSearchCriteria()
    {
        String value = null;
        if(genericMap.containsKey(SEARCH_CRITERIA_KEY))
        {
            value = ((MapStorePO) genericMap.get(SEARCH_CRITERIA_KEY)).getMapValue();
        }

        return TopLinkStringConverter.convertToJavaEmptyString( value);        
    }

    public void setSearchCriteria(String newSearchCriteriaIn)
    {
        String newSearchCriteria = TopLinkStringConverter.convertToTopLinkEmptyString(newSearchCriteriaIn);
        if(genericMap.containsKey(SEARCH_CRITERIA_KEY))
        {
            ((MapStorePO) genericMap.get(SEARCH_CRITERIA_KEY)).setMapValue(newSearchCriteria);
        }
        else
        {
            MapStorePO mapStorePO = new MapStorePO(this, new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.MAP_TYPE_SEARCH_CRITERIA, SEARCH_CRITERIA_KEY, newSearchCriteria);
            genericMap.put(SEARCH_CRITERIA_KEY, mapStorePO);
        }        
    }

    public String getDeliveryDaysOut()
    {
         return deliveryDaysOut;
    }
 
    public void setDeliveryDaysOut(String newDeliveryDaysOut)
    {
        deliveryDaysOut = newDeliveryDaysOut;
    }

}