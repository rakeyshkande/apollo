package com.ftd.applications.oe.common.persistent;

import java.math.BigDecimal;

public class PromotionPO 
{
    private OrderPO order;
    private String persistenceId;
    private String promotionId;
    private Long basePoints;
    private BigDecimal variablePoints;
    private Double lowPrice;
    private Double highPrice;
    private String pointDriver;

    public PromotionPO()
    {
    }

    public OrderPO getOrder()
    {
        return order;
    }

    public void setOrder(OrderPO newOrder)
    {
        order = newOrder;
    }

    public Long getBasePoints()
    {
        return basePoints;
    }

    public void setBasePoints(Long newBasePoints)
    {
        basePoints = newBasePoints;
    }

    public BigDecimal getVariablePoints()
    {
        return variablePoints;
    }

    public void setVariablePoints(BigDecimal newVariablePoints)
    {
        variablePoints = newVariablePoints;
    }

    public Double getLowPrice()
    {
        return lowPrice;
    }

    public void setLowPrice(Double newLowPrice)
    {
        lowPrice = newLowPrice;
    }

    public Double getHighPrice()
    {
        return highPrice;
    }

    public void setHighPrice(Double newHighPrice)
    {
        highPrice = newHighPrice;
    }

    public String getPointDriver()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(pointDriver);
    }

    public void setPointDriver(String newPointDriver)
    {
        pointDriver = TopLinkStringConverter.convertToTopLinkEmptyString(newPointDriver);
    }

    public String getPromotionId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(promotionId);
    }

    public void setPromotionId(String newPromotionId)
    {
        promotionId = TopLinkStringConverter.convertToTopLinkEmptyString(newPromotionId);
    }

    public String getPersistenceId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(persistenceId);
    }

    public void setPersistenceId(String newPersistenceId)
    {
        persistenceId = TopLinkStringConverter.convertToTopLinkEmptyString(newPersistenceId);
    }
}