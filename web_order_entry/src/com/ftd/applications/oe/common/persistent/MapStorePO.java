package com.ftd.applications.oe.common.persistent;

public class MapStorePO
{
    private OrderPO order;
    private String persistenceId;
    private String mapType;
    private String mapKey;
    private String mapValue;
    private String orderId;
   
    public MapStorePO()
    {
    }

    public MapStorePO(OrderPO newOrder, String newPersistenceId, String newMapType, String newMapKey, String newMapValue)
    {
        order = newOrder;
        orderId = newOrder.getGUID();
        persistenceId = TopLinkStringConverter.convertToTopLinkEmptyString(newPersistenceId);
        mapType = TopLinkStringConverter.convertToTopLinkEmptyString(newMapType);
        mapKey = TopLinkStringConverter.convertToTopLinkEmptyString(newMapKey);
        mapValue = TopLinkStringConverter.convertToTopLinkEmptyString(newMapValue);
    }

    public OrderPO getOrder()
    {
        return order;
    }

    public void setOrder(OrderPO newOrder)
    {
        order = newOrder;
    }

    public String getPersistenceId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(persistenceId);
    }

    public void setPersistenceId(String newPersistenceId)
    {
        persistenceId = TopLinkStringConverter.convertToTopLinkEmptyString(newPersistenceId);
    }

    public String getMapKey()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(mapKey);
    }

    public void setMapKey(String newMapKey)
    {
        mapKey = TopLinkStringConverter.convertToTopLinkEmptyString(newMapKey);
    }

    public String getMapValue()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(mapValue);
    }

    public void setMapValue(String newMapValue)
    {
        mapValue = TopLinkStringConverter.convertToTopLinkEmptyString(newMapValue);
    }

    public String getMapType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(mapType);
    }

    public void setMapType(String newMapType)
    {
        mapType = TopLinkStringConverter.convertToTopLinkEmptyString(newMapType);
    }

}