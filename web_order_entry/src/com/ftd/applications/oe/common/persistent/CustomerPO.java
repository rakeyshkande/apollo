package com.ftd.applications.oe.common.persistent;

public class CustomerPO implements Cloneable
{
    private String customerId;
    private String hpCustomerId;
    private String customerType;
    private String firstName;
    private String lastName;
    private String addressOne;
    private String addressTwo;
    private String city;
    private String state;
    private String zipCode;
    private String homePhone;
    private String workPhone;
    private String phoneExtension;
    private String faxNumber;
    private String emailAddress;
    private String country;
    private String companyName;
    private String countryType;
    private String contactInfo;
    private String membershipId;
    private String membershipFirstName;
    private String membershipLastName;
    private String specialPromotionsFlag;
    private Boolean existingFlag = Boolean.FALSE;
    private Boolean domesticFlag = Boolean.TRUE;
    private String qmsAddressOne;
    private String qmsAddressTwo;
    private String qmsCity;
    private String qmsState;
    private String qmsZipCode;
    private String qmsOverrideFlag;
    private String qmsFirmName;
    private String qmsLatitude;
    private String qmsLongitude;
    private String qmsResultCode;
    private String qmsUSPSRangeRecordType;
    private String companyInfo;
    private String companyType;
    

    public CustomerPO()
    {
    }

    public CustomerPO(String newCustomerId, String newCustomerType)
    {
        customerId = TopLinkStringConverter.convertToTopLinkEmptyString( newCustomerId);
        customerType = TopLinkStringConverter.convertToTopLinkEmptyString( newCustomerType);
    }

    public String getFirstName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( firstName);
    }

    public void setFirstName(String newFirstName)
    {
        firstName = TopLinkStringConverter.convertToTopLinkEmptyString( newFirstName);
    }

    public String getLastName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( lastName);
    }

    public void setLastName(String newLastName)
    {
        lastName = TopLinkStringConverter.convertToTopLinkEmptyString( newLastName);
    }

    public String getAddressOne()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( addressOne);
    }

    public void setAddressOne(String newAddressOne)
    {
        addressOne = TopLinkStringConverter.convertToTopLinkEmptyString( newAddressOne);
    }

    public String getAddressTwo()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( addressTwo);
    }

    public void setAddressTwo(String newAddressTwo)
    {
        addressTwo = TopLinkStringConverter.convertToTopLinkEmptyString( newAddressTwo);
    }

    public String getCity()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( city);
    }

    public void setCity(String newCity)
    {
        city = TopLinkStringConverter.convertToTopLinkEmptyString( newCity);
    }

    public String getState()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( state);
    }

    public void setState(String newState)
    {
        state = TopLinkStringConverter.convertToTopLinkEmptyString( newState);
    }

    public String getZipCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( zipCode);
    }

    public void setZipCode(String newZipCode)
    {
        zipCode = TopLinkStringConverter.convertToTopLinkEmptyString( newZipCode);
    }

    public String getHomePhone()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( homePhone);
    }

    public void setHomePhone(String newHomePhone)
    {
        homePhone = TopLinkStringConverter.convertToTopLinkEmptyString( newHomePhone);
    }

    public String getWorkPhone()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( workPhone);
    }

    public void setWorkPhone(String newWorkPhone)
    {
        workPhone = TopLinkStringConverter.convertToTopLinkEmptyString( newWorkPhone);
    }

    public String getPhoneExtension()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( phoneExtension);
    }

    public void setPhoneExtension(String newPhoneExtension)
    {
        phoneExtension = TopLinkStringConverter.convertToTopLinkEmptyString( newPhoneExtension);
    }

    public String getFaxNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( faxNumber);
    }

    public void setFaxNumber(String newFaxNumber)
    {
        faxNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newFaxNumber);
    }

    public String getEmailAddress()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( emailAddress);
    }

    public void setEmailAddress(String newEmailAddress)
    {
        emailAddress = TopLinkStringConverter.convertToTopLinkEmptyString( newEmailAddress);
    }

    public String getCountry()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( country);
    }

    public void setCountry(String newCountry)
    {
        country = TopLinkStringConverter.convertToTopLinkEmptyString( newCountry);
    }

    public String getCompanyName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( companyName);
    }

    public void setCompanyName(String newCompanyName)
    {
        companyName = TopLinkStringConverter.convertToTopLinkEmptyString( newCompanyName);
    }

    public String getCountryType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( countryType);
    }

    public void setCountryType(String newCountryType)
    {
        countryType = TopLinkStringConverter.convertToTopLinkEmptyString( newCountryType);
    }

    public String getContactInfo()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( contactInfo);
    }

    public void setContactInfo(String newContactInfo)
    {
        contactInfo = TopLinkStringConverter.convertToTopLinkEmptyString( newContactInfo);
    }

    public String getMembershipId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( membershipId);
    }

    public void setMembershipId(String newMembershipId)
    {
        membershipId = TopLinkStringConverter.convertToTopLinkEmptyString( newMembershipId);
    }

    public String getMembershipFirstName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( membershipFirstName);
    }

    public void setMembershipFirstName(String newMembershipFirstName)
    {
        membershipFirstName = TopLinkStringConverter.convertToTopLinkEmptyString( newMembershipFirstName);
    }

    public String getMembershipLastName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( membershipLastName);
    }

    public void setMembershipLastName(String newMembershipLastName)
    {
        membershipLastName = TopLinkStringConverter.convertToTopLinkEmptyString( newMembershipLastName);
    }

    public String getCustomerId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( customerId);
    }

    public void setCustomerId(String newCustomerId)
    {
        customerId = TopLinkStringConverter.convertToTopLinkEmptyString( newCustomerId);
    }

    public String getCustomerType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( customerType);
    }

    public void setCustomerType(String newCustomerType)
    {
        customerType = TopLinkStringConverter.convertToTopLinkEmptyString( newCustomerType);
    }

    public boolean isExisting()
    {
        return existingFlag.booleanValue();
    }

    public Boolean getExistingFlag()
    {
        return existingFlag;
    }

    public void setExistingFlag(Boolean newExistingFlag)
    {
        existingFlag = newExistingFlag;
    }

    public String getSpecialPromotionsFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( specialPromotionsFlag);
    }

    public void setSpecialPromotionsFlag(String newSpecialPromotionsFlag)
    {
        specialPromotionsFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newSpecialPromotionsFlag);
    }

    public String getHpCustomerId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( hpCustomerId);
    }

    public void setHpCustomerId(String newHpCustomerId)
    {
        hpCustomerId = TopLinkStringConverter.convertToTopLinkEmptyString( newHpCustomerId);
    }

    public boolean isDomesticFlag()
    {
        return domesticFlag.booleanValue();
    }

    public Boolean getDomesticFlag()
    {
        return domesticFlag;
    }

    public void setDomesticFlag(Boolean newDomesticFlag)
    {
        domesticFlag = newDomesticFlag;
    }    

    public String getQmsAddressOne()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsAddressOne);
    }

    public void setQmsAddressOne(String newQmsAddressOne)
    {
        qmsAddressOne = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsAddressOne);
    }

    public String getQmsAddressTwo()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsAddressTwo);
    }

    public void setQmsAddressTwo(String newQmsAddressTwo)
    {
        qmsAddressTwo = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsAddressTwo);
    }

    public String getQmsCity()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsCity);
    }

    public void setQmsCity(String newQmsCity)
    {
        qmsCity = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsCity);
    }

    public String getQmsState()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsState);
    }

    public void setQmsState(String newQmsState)
    {
        qmsState = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsState);
    }

    public String getQmsZipCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsZipCode);
    }

    public void setQmsZipCode(String newQmsZipCode)
    {
        qmsZipCode = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsZipCode);
    }

    public String getQmsOverrideFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsOverrideFlag);
    }

    public void setQmsOverrideFlag(String newQmsOverrideFlag)
    {
        qmsOverrideFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsOverrideFlag);
    }

    public String getQmsFirmName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsFirmName);
    }

    public void setQmsFirmName(String newQmsFirmName)
    {
        qmsFirmName = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsFirmName);
    }

    public String getQmsLatitude()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsLatitude);
    }

    public void setQmsLatitude(String newQmsLatitude)
    {
        qmsLatitude = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsLatitude);
    }

    public String getQmsLongitude()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsLongitude);
    }

    public void setQmsLongitude(String newQmsLongitude)
    {
        qmsLongitude = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsLongitude);
    }

    public String getQmsResultCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsResultCode);
    }

    public void setQmsResultCode(String newQmsResultCode)
    {
        qmsResultCode = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsResultCode);
    }

    public String getQmsUSPSRangeRecordType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( qmsUSPSRangeRecordType);
    }

    public void setQmsUSPSRangeRecordType(String newQmsUSPSRangeRecordType)
    {
        qmsUSPSRangeRecordType = TopLinkStringConverter.convertToTopLinkEmptyString( newQmsUSPSRangeRecordType);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void setCompanyInfo(String companyInfo) {
        this.companyInfo = TopLinkStringConverter.convertToTopLinkEmptyString(companyInfo);
    }

    public String getCompanyInfo() {
        return TopLinkStringConverter.convertToJavaEmptyString(companyInfo);
    }

    public void setCompanyType(String companyType) {
        this.companyType = TopLinkStringConverter.convertToTopLinkEmptyString(companyType);
    }

    public String getCompanyType() {
        return TopLinkStringConverter.convertToJavaEmptyString(companyType);
    }

}
