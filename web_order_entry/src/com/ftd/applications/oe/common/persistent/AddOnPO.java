package com.ftd.applications.oe.common.persistent;

import java.math.BigDecimal;

public class AddOnPO implements Cloneable
{
    private ItemPO item;
    private String persistenceId;
    private BigDecimal addOnPrice;
    private String addOnDescription;
    private String addOnId;
    private Integer addOnQty;
    private String addOnType;

    public AddOnPO()
    {
    }
    
    public ItemPO getItem()
    {
        return item;
    }

    public void setItem(ItemPO newItem)
    {
        item = newItem;
    }

    public BigDecimal getAddOnPrice()
    {
        return addOnPrice;
    }

    public void setAddOnPrice(BigDecimal newAddOnPrice)
    {
        addOnPrice = newAddOnPrice;
    }

    public String getAddOnDescription()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(addOnDescription);
    }

    public void setAddOnDescription(String newAddOnDescription)
    {
        addOnDescription = TopLinkStringConverter.convertToTopLinkEmptyString(newAddOnDescription);
    }

    public String getAddOnId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(addOnId);
    }

    public void setAddOnId(String newAddOnId)
    {
        addOnId = TopLinkStringConverter.convertToTopLinkEmptyString(newAddOnId);
    }

    public Integer getAddOnQty()
    {
        return addOnQty;
    }

    public void setAddOnQty(Integer newAddOnQty)
    {
        addOnQty = newAddOnQty;
    }

    public String getAddOnType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(addOnType);
    }

    public void setAddOnType(String newAddOnType)
    {
        addOnType = TopLinkStringConverter.convertToTopLinkEmptyString(newAddOnType);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getPersistenceId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString(persistenceId);
    }

    public void setPersistenceId(String newPersistenceId)
    {
        persistenceId = TopLinkStringConverter.convertToTopLinkEmptyString(newPersistenceId);
    }
}