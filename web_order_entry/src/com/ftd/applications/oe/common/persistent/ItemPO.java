package com.ftd.applications.oe.common.persistent;

import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.framework.common.valueobjects.FTDPersistentObject;
import com.ftd.applications.oe.services.utilities.OEOrderUTIL;
import java.math.BigDecimal;
import java.util.*;
import java.util.Date;
import java.util.List;

public class ItemPO extends FTDPersistentObject implements Cloneable
{
    private OrderPO order;
    private String orderId;
    private Integer cartNumber;
    private String productId;
    private String itemName;
    private String colorOne;
    private String colorTwo;
    private String itemStatus;
    private String institutionType;
    private String institutionName;
    private String institutionInfo;
    private String occasion;
    private BigDecimal regularPrice;
    private BigDecimal discountedPrice;
    private BigDecimal tax;
    private BigDecimal itemTotalPrice;
    private BigDecimal addOnTotalPrice;
    private Boolean customItemFlag = Boolean.FALSE;
    private String deliveryDateDisplay;
    private Date deliveryDate;
    private String novatorId;
    private String novatorName;
    private String productType;
    private String itemSKU;
    private String exceptionCode;
    private String exceptionStartDate;
    private String exceptionEndDate;
    private String egiftFlag;
    private String codifiedSpecial;
    private String substitutionAuth;
    private Boolean subCodeSelected = Boolean.FALSE;
    private String subCodeId;
    private String subCodeDescription;
    private String priceType;
    private BigDecimal subCodePrice;
    private String subCodeReferenceNumber;
    private String shippingMethod;
    private BigDecimal shippingCost;
    private String shippingCarrier;
    private BigDecimal discountAmount;
    private String discountDescription;
    private String itemComments;
    private String floristComments;
    private String lastMinuteGiftFlag;
    private String lastMinuteGiftEmail;
    private String shipTimeFrom;
    private String shipTimeTo;
    private String cardMessage;
    private String cardSignature;
    private String releaseSenderNameFlag;
    private String floristCode;
    private String shippingMethodId;
    private BigDecimal extraShippingFee;
    private BigDecimal taxAmount;
    private BigDecimal serviceFee;
    private String detailItemNumber;
    private String sundayDeliveryFlag;
    private Date secondDeliveryDate;

    /**
     * @association <com.ftd.applications.oe.common.persistent.itemSendTo> com.ftd.applications.oe.common.persistent.CustomerPO
     */
    private CustomerPO sendToCustomer;

    /**
     * @association <com.ftd.applications.oe.common.persistent.itemAddOn> com.ftd.applications.oe.common.persistent.AddOnPO
     */
    private List addOnList = new LinkedList();
    
    
    public ItemPO()
    {
    }

    public OrderPO getOrder()
    {
        return order;
    }

    public void setOrder(OrderPO newOrder)
    {
        order = newOrder;
        orderId = newOrder.getGUID();
    }

    public String getProductId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( productId);
    }

    public void setProductId(String newProductId)
    {
        productId = TopLinkStringConverter.convertToTopLinkEmptyString( newProductId);
    }

    public Integer getCartNumber()
    {
        return cartNumber;
    }

    public void setCartNumber(Integer newCartNumber)
    {
        cartNumber = newCartNumber;
    }

    public String getItemName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( itemName);
    }

    public void setItemName(String newItemName)
    {
        itemName = TopLinkStringConverter.convertToTopLinkEmptyString( newItemName);
    }

    public String getColorOne()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( colorOne);
    }

    public void setColorOne(String newColorOne)
    {
        colorOne = TopLinkStringConverter.convertToTopLinkEmptyString( newColorOne);
    }

    public String getColorTwo()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( colorTwo);
    }

    public void setColorTwo(String newColorTwo)
    {
        colorTwo = TopLinkStringConverter.convertToTopLinkEmptyString( newColorTwo);
    }

    public CustomerPO getSendToCustomer()
    {
        if(sendToCustomer == null)
        {
            sendToCustomer = new CustomerPO(new Long(OEOrderUTIL.getUniqueNumber()).toString(), GeneralConstants.CUSTOMER_TYPE_ITEM_SEND_TO); 
        }
        
        return sendToCustomer;
    }

    public void setSendToCustomer(CustomerPO newSendToCustomer)
    {
        newSendToCustomer.setCustomerId(new Long(OEOrderUTIL.getUniqueNumber()).toString());
        sendToCustomer = newSendToCustomer;
    }

    public String getItemStatus()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( itemStatus);
    }

    public void setItemStatus(String newItemStatus)
    {
        itemStatus = TopLinkStringConverter.convertToTopLinkEmptyString( newItemStatus);
    }

    public String getInstitutionType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( institutionType);
    }

    public void setInstitutionType(String newInstitutionType)
    {
        institutionType = TopLinkStringConverter.convertToTopLinkEmptyString( newInstitutionType);
    }

    public String getInstitutionName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( institutionName);
    }

    public void setInstitutionName(String newInstitutionName)
    {
        institutionName = TopLinkStringConverter.convertToTopLinkEmptyString( newInstitutionName);
    }

    public String getInstitutionInfo()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( institutionInfo);
    }

    public void setInstitutionInfo(String newInstitutionInfo)
    {
        institutionInfo = TopLinkStringConverter.convertToTopLinkEmptyString( newInstitutionInfo);
    }

    public String getOccasion()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( occasion);
    }

    public void setOccasion(String newOccasion)
    {
        occasion = TopLinkStringConverter.convertToTopLinkEmptyString( newOccasion);
    }

    public BigDecimal getRegularPrice()
    {
        return regularPrice;
    }

    public void setRegularPrice(BigDecimal newRegularPrice)
    {
        regularPrice = newRegularPrice;
    }

    public BigDecimal getDiscountedPrice()
    {
        return discountedPrice;
    }

    public void setDiscountedPrice(BigDecimal newDiscountedPrice)
    {
        discountedPrice = newDiscountedPrice;
    }

    public BigDecimal getTax()
    {
        return tax;
    }

    public void setTax(BigDecimal newTax)
    {
        tax = newTax;
    }

    public BigDecimal getItemTotalPrice()
    {
        return itemTotalPrice;
    }

    public void setItemTotalPrice(BigDecimal newItemTotalPrice)
    {
        itemTotalPrice = newItemTotalPrice;
    }

    public BigDecimal getAddOnTotalPrice()
    {
        return addOnTotalPrice;
    }

    public void setAddOnTotalPrice(BigDecimal newAddOnTotalPrice)
    {
        addOnTotalPrice = newAddOnTotalPrice;
    }

    public Boolean getCustomItemFlag()
    {
        return customItemFlag;
    }

    public void setCustomItemFlag(Boolean newCustomItemFlag)
    {
        customItemFlag = newCustomItemFlag;
    }

    public boolean isCustomItem()
    {
        return customItemFlag.booleanValue();
    }

    public String getDeliveryDateDisplay()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( deliveryDateDisplay);
    }

    public void setDeliveryDateDisplay(String newDeliveryDateDisplay)
    {
        deliveryDateDisplay = TopLinkStringConverter.convertToTopLinkEmptyString( newDeliveryDateDisplay);
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDeliveryDate(Date newDeliveryDate)
    {
        deliveryDate = newDeliveryDate;
    }





    public String getNovatorId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( novatorId);
    }

    public void setNovatorId(String newNovatorId)
    {
        novatorId = TopLinkStringConverter.convertToTopLinkEmptyString( newNovatorId);
    }

    public String getNovatorName()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( novatorName);
    }

    public void setNovatorName(String newNovatorName)
    {
        novatorName = TopLinkStringConverter.convertToTopLinkEmptyString( newNovatorName);
    }

    public String getProductType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( productType);
    }

    public void setProductType(String newProductType)
    {
        productType = TopLinkStringConverter.convertToTopLinkEmptyString( newProductType);
    }

    public String getItemSKU()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( itemSKU);
    }

    public void setItemSKU(String newItemSKU)
    {
        itemSKU = TopLinkStringConverter.convertToTopLinkEmptyString( newItemSKU);
    }

    public String getExceptionCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( exceptionCode);
    }

    public void setExceptionCode(String newExceptionCode)
    {
        exceptionCode = TopLinkStringConverter.convertToTopLinkEmptyString( newExceptionCode);
    }

    public String getExceptionStartDate()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( exceptionStartDate);
    }

    public void setExceptionStartDate(String newExceptionStartDate)
    {
        exceptionStartDate = TopLinkStringConverter.convertToTopLinkEmptyString( newExceptionStartDate);
    }

    public String getExceptionEndDate()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( exceptionEndDate);
    }

    public void setExceptionEndDate(String newExceptionEndDate)
    {
        exceptionEndDate = TopLinkStringConverter.convertToTopLinkEmptyString( newExceptionEndDate);
    }

    public String getEgiftFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( egiftFlag);
    }

    public void setEgiftFlag(String newEgiftFlag)
    {
        egiftFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newEgiftFlag);
    }

    public String getCodifiedSpecial()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( codifiedSpecial);
    }

    public void setCodifiedSpecial(String newCodifiedSpecial)
    {
        codifiedSpecial = TopLinkStringConverter.convertToTopLinkEmptyString( newCodifiedSpecial);
    }

    public String getSubstitutionAuth()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( substitutionAuth);
    }

    public void setSubstitutionAuth(String newSubstitutionAuth)
    {
        substitutionAuth = TopLinkStringConverter.convertToTopLinkEmptyString( newSubstitutionAuth);
    }

    public boolean isSubCodeSelected()
    {
        return subCodeSelected.booleanValue();
    }

    public Boolean getSubCodeSelected()
    {
        return subCodeSelected;
    }

    public void setSubCodeSelected(Boolean newSubCodeSelected)
    {
        subCodeSelected = newSubCodeSelected;
    }

    public String getSubCodeId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( subCodeId);
    }

    public void setSubCodeId(String newSubCodeId)
    {
        subCodeId = TopLinkStringConverter.convertToTopLinkEmptyString( newSubCodeId);
    }

    public String getSubCodeDescription()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( subCodeDescription);
    }

    public void setSubCodeDescription(String newSubCodeDescription)
    {
        subCodeDescription = TopLinkStringConverter.convertToTopLinkEmptyString( newSubCodeDescription);
    }

    public String getPriceType()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( priceType);
    }

    public void setPriceType(String newPriceType)
    {
        priceType = TopLinkStringConverter.convertToTopLinkEmptyString( newPriceType);
    }

    public BigDecimal getSubCodePrice()
    {
        return subCodePrice;
    }

    public void setSubCodePrice(BigDecimal newSubCodePrice)
    {
        subCodePrice = newSubCodePrice;
    }

    public String getSubCodeReferenceNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( subCodeReferenceNumber);
    }

    public void setSubCodeReferenceNumber(String newSubCodeReferenceNumber)
    {
        subCodeReferenceNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newSubCodeReferenceNumber);
    }

    public String getShippingMethod()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( shippingMethod);
    }

    public void setShippingMethod(String newShippingMethod)
    {
        shippingMethod = TopLinkStringConverter.convertToTopLinkEmptyString( newShippingMethod);
    }

    public BigDecimal getShippingCost()
    {
        return shippingCost;
    }

    public void setShippingCost(BigDecimal newShippingCost)
    {
        shippingCost = newShippingCost;
    }

    public String getShippingCarrier()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( shippingCarrier);
    }

    public void setShippingCarrier(String newShippingCarrier)
    {
        shippingCarrier = TopLinkStringConverter.convertToTopLinkEmptyString( newShippingCarrier);
    }

    public BigDecimal getDiscountAmount()
    {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal newDiscountAmount)
    {
        discountAmount = newDiscountAmount;
    }

    public String getDiscountDescription()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( discountDescription);
    }

    public void setDiscountDescription(String newDiscountDescription)
    {
        discountDescription = TopLinkStringConverter.convertToTopLinkEmptyString( newDiscountDescription);
    }

    public LinkedList getAddOnList() {
        return (LinkedList) addOnList;
    }

    public void setAddOnList(LinkedList newAddOnList) 
    {
        addOnList = newAddOnList;
    }

    public void addAddOn(AddOnPO addOn) 
    {
        addOn.setItem(this);
        addOn.setPersistenceId(new Long(OEOrderUTIL.getUniqueNumber()).toString());
        addOnList.add(addOn);
    }

    public String getItemComments()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( itemComments);
    }

    public void setItemComments(String newItemComments)
    {
        itemComments = TopLinkStringConverter.convertToTopLinkEmptyString( newItemComments);
    }

    public String getFloristComments()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( floristComments);
    }

    public void setFloristComments(String newFloristComments)
    {
        floristComments = TopLinkStringConverter.convertToTopLinkEmptyString( newFloristComments);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getLastMinuteGiftFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( lastMinuteGiftFlag);
    }

    public void setLastMinuteGiftFlag(String newLastMinuteGiftFlag)
    {
        lastMinuteGiftFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newLastMinuteGiftFlag);
    }

    public String getLastMinuteGiftEmail()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( lastMinuteGiftEmail);
    }

    public void setLastMinuteGiftEmail(String newLastMinuteGiftEmail)
    {
        lastMinuteGiftEmail = TopLinkStringConverter.convertToTopLinkEmptyString( newLastMinuteGiftEmail);
    }

    public String getShipTimeFrom()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( shipTimeFrom);
    }

    public void setShipTimeFrom(String newShipTimeFrom)
    {
        shipTimeFrom = TopLinkStringConverter.convertToTopLinkEmptyString( newShipTimeFrom);
    }

    public String getShipTimeTo()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( shipTimeTo);
    }

    public void setShipTimeTo(String newShipTimeTo)
    {
        shipTimeTo = TopLinkStringConverter.convertToTopLinkEmptyString( newShipTimeTo);
    }

    public String getCardMessage()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( cardMessage);
    }

    public void setCardMessage(String newCardMessage)
    {
        cardMessage = TopLinkStringConverter.convertToTopLinkEmptyString( newCardMessage);
    }

    public String getCardSignature()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( cardSignature);
    }

    public void setCardSignature(String newCardSignature)
    {
        cardSignature = TopLinkStringConverter.convertToTopLinkEmptyString( newCardSignature);
    }

    public String getReleaseSenderNameFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( releaseSenderNameFlag);
    }

    public void setReleaseSenderNameFlag(String newReleaseSenderNameFlag)
    {
        releaseSenderNameFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newReleaseSenderNameFlag);
    }

    public String getFloristCode()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( floristCode);
    }

    public void setFloristCode(String newFloristCode)
    {
        floristCode = TopLinkStringConverter.convertToTopLinkEmptyString( newFloristCode);
    }

    public String getShippingMethodId()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( shippingMethodId);
    }

    public void setShippingMethodId(String newShippingMethodId)
    {
        shippingMethodId = TopLinkStringConverter.convertToTopLinkEmptyString( newShippingMethodId);
    }

    public BigDecimal getExtraShippingFee()
    {
        return extraShippingFee;
    }

    public void setExtraShippingFee(BigDecimal newExtraShippingFee)
    {
        extraShippingFee = newExtraShippingFee;
    }

    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal newTaxAmount)
    {
        taxAmount = newTaxAmount;
    }

    public BigDecimal getServiceFee()
    {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal newServiceFee)
    {
        serviceFee = newServiceFee;
    }

    public String getDetailItemNumber()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( detailItemNumber);
    }

    public void setDetailItemNumber(String newDetailItemNumber)
    {
        detailItemNumber = TopLinkStringConverter.convertToTopLinkEmptyString( newDetailItemNumber);
    }

    public String getSundayDeliveryFlag()
    {
        return TopLinkStringConverter.convertToJavaEmptyString( sundayDeliveryFlag);
    }

    public void setSundayDeliveryFlag(String newSundayDeliveryFlag)
    {
        sundayDeliveryFlag = TopLinkStringConverter.convertToTopLinkEmptyString( newSundayDeliveryFlag);
    }

    public Date getSecondDeliveryDate()
    {
        return secondDeliveryDate;
    }

    public void setSecondDeliveryDate(Date newSecondDeliveryDate)
    {
        secondDeliveryDate = newSecondDeliveryDate;
    }
}