package com.ftd.applications.oe.common;

public interface CommandConstants 
{
    /* Shopping Constants */
    public final static String SHOPPING_SET_INTRODUCTION = "SHOPPING_SET_INTRODUCTION";
    public final static String SHOPPING_CANCEL_ITEM = "SHOPPING_CANCEL_ITEM";
    public final static String SHOPPING_UPDATE_OCCASION = "SHOPPING_UPDATE_OCCASION";
    public final static String SHOPPING_SET_DNIS = "SHOPPING_SET_DNIS";
    public final static String SHOPPING_SET_OCCASION = "SHOPPING_SET_OCCASION";
    public final static String SHOPPING_ADD_ITEM = "SHOPPING_ADD_ITEM";
    public final static String SHOPPING_ADD_CUSTOM_ITEM = "SHOPPING_ADD_CUSTOM_ITEM";
    public final static String SHOPPING_ADD_DUPLICATE_ITEM = "SHOPPING_ADD_DUPLICATE_ITEM";
    public final static String SHOPPING_REMOVE_ITEM = "SHOPPING_REMOVE_ITEM";
    public final static String SHOPPING_SET_ITEM_DELIVERY_INFO = "SHOPPING_SET_ITEM_DELIVERY_INFO";
    public final static String SHOPPING_SET_DELIVERY_LOCATION_TYPE = "SHOPPING_SET_DELIVERY_LOCATION_TYPE";
    public final static String SHOPPING_CANCEL_ORDER = "SHOPPING_CANCEL_ORDER";
    public final static String SHOPPING_GET_SHOPPING_CART = "SHOPPING_GET_SHOPPING_CART";
    public final static String SHOPPING_LOAD_DELIVERY_INFO = "SHOPPING_LOAD_DELIVERY_INFO";
    public final static String SHOPPING_UPDATE_DELIVERY_INFO = "SHOPPING_UPDATE_DELIVERY_INFO";
    public final static String SHOPPING_UPDATE_BILLING_INFO = "SHOPPING_UPDATE_BILLING_INFO";
    public final static String SHOPPING_LOAD_BILLING_INFO = "SHOPPING_LOAD_BILLING_INFO";
    public final static String SHOPPING_LOAD_CONFIRMATION_INFO = "SHOPPING_LOAD_CONFIRMATION_INFO";
    public final static String SHOPPING_LOAD_ITEM_FROM_CART = "SHOPPING_LOAD_ITEM_FROM_CART";
    public final static String SHOPPING_UPDATE_SOURCE_CODE = "SHOPPING_UPDATE_SOURCE_CODE";
    public final static String SHOPPING_UPDATE_ITEM_INFO = "SHOPPING_UPDATE_ITEM_INFO";
    public final static String SHOPPING_UPDATE_CUSTOM_ITEM_INFO = "SHOPPING_UPDATE_CUSTOM_ITEM_INFO";
    public final static String SHOPPING_UPDATE_DNIS = "SHOPPING_UPDATE_DNIS";

    /* Billing Constants */
    public final static String BILLING_UPDATE_BILLING_INFO = "BILLING_UPDATE_BILLING_INFO";
    public final static String BILLING_UPDATE_CREDIT_CARD_INFO = "UPDATE_CREDIT_CARD_INFO";
    public final static String BILLING_LOAD_CREDIT_CARD_INFO = "BILLING_LOAD_CREDIT_CARD_INFO";
    public final static String BILLING_LOAD_BILLING_INFO = "BILLING_LOAD_BILLING_INFO";
    public final static String BILLING_LOAD_CONFIRMATION_INFO = "BILLING_LOAD_CONFIRMATION_INFO";
    public final static String BILLING_COMPLETE_ORDER_ITEMS = "BILLING_COMPLETE_ORDER_ITEMS";
    public final static String BILLING_COMMIT_ORDER = "BILLING_COMMIT_ORDER";
    public final static String BILLING_COMPLETE_ORDER = "BILLING_COMPLETE_ORDER";
    
    /* Search Constants */
    public final static String SEARCH_GET_INTRODUCTION = "SEARCH_GET_INTRODUCTION";
    public final static String SEARCH_GET_OCCASION = "SEARCH_GET_OCCASION";
    public final static String SEARCH_CUSTOMER_LOOKUP = "SEARCH_CUSTOMER_LOOKUP";
    public final static String SEARCH_INSTITUTION_LOOKUP = "SEARCH_INSTITUTION_LOOKUP";
    public final static String SEARCH_SOURCE_CODE_LOOKUP = "SEARCH_SOURCE_CODE_LOOKUP";
    public final static String SEARCH_GET_DNIS_SCRIPT = "SEARCH_GET_DNIS_SCRIPT";
    public final static String SEARCH_PRODUCT_BY_ID = "SEARCH_PRODUCT_BY_ID";
    public final static String SEARCH_PRODUCTS_BY_PRICE = "SEARCH_PRODUCTS_BY_PRICE";
    public final static String SEARCH_PRODUCTS_BY_CATEGORY = "SEARCH_PRODUCTS_BY_CATEGORY";
    public final static String SEARCH_PRODUCTS_BY_VALUE = "SEARCH_PRODUCTS_BY_VALUE";
    public final static String SEARCH_PRODUCTS_BY_KEYWORD = "SEARCH_PRODUCTS_BY_KEYWORD";
    public final static String SEARCH_SUB_CATEGORY_LOOKUP = "SEARCH_SUB_CATEGORY_LOOKUP";
    public final static String SEARCH_GET_USER_SEARCH = "SEARCH_GET_USER_SEARCH";
    public final static String SEARCH_GET_ROLE_SEARCH = "SEARCH_GET_ROLE_SEARCH";
    public final static String SEARCH_GET_CALENDAR_LOOKUP = "SEARCH_GET_CALENDAR_LOOKUP";
    public final static String SEARCH_UPDATE_PRODUCT_DATES = "SEARCH_UPDATE_PRODUCT_DATES";
    public final static String SEARCH_GET_ZIP_CODE = "SEARCH_GET_ZIP_CODE";
    public final static String SEARCH_GET_FLORIST = "SEARCH_GET_FLORIST";
    public final static String SEARCH_GREETING_CARD_LOOKUP = "SEARCH_GREETING_CARD_LOOKUP";
    public final static String SEARCH_DELIVERY_POLICY_LOOKUP = "SEARCH_DELIVERY_POLICY_LOOKUP";
    
    /* Admin Constants */
    public final static String ADMIN_UPDATE_USER = "ADMIN_UPDATE_USER";
    public final static String ADMIN_DELETE_USER = "ADMIN_DELETE_USER";
    public final static String ADMIN_UPDATE_ROLE = "ADMIN_UPDATE_ROLE";
    public final static String ADMIN_DELETE_ROLE = "ADMIN_DELETE_ROLE";
    
    /* Validation Constants */
    public final static String VALIDATE_PAGE = "VALIDATE_PAGE";
}