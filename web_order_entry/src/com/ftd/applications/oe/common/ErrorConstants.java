package com.ftd.applications.oe.common;

public class ErrorConstants 
{
    public final static String PROCESS_OK = "0";
    public final static String ACCOUNT_DELETED = "151";
    public final static String INVALID_PASSWORD = "155";
    public final static String ACCOUNT_INACTIVE = "158";
}