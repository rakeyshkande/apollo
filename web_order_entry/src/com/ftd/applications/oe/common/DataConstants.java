package com.ftd.applications.oe.common;

public interface DataConstants
{
    public final static String SHOPPING_GET_OCCASION_DATA = "shopping service::get occasion data";
    public final static String SHOPPING_GET_SOURCE_CODE_DETAILS = "shopping service::get source code details";
    public final static String SHOPPING_GET_INDEX_PAGE = "shopping service::get index page";
    public final static String SHOPPING_GET_DELIVERY_INFO_DATA = "shopping service::get delivery info data";
    public final static String SHOPPING_GET_BILLING_INFO_DATA = "shopping service::get billing info data";
    public final static String SHOPPING_GET_CREDIT_CARD_DATA = "shopping service::get credit card data";
    public final static String SHOPPING_GET_CONFIRMATION_DATA = "shopping service::get order confirmation data";
    public final static String SHOPPING_GET_TIMEZONE_DATA = "shopping service::get timezone data";
    public final static String SHOPPING_GET_GLOBAL_PARMS = "shopping service::get global parms";
    public final static String SHOPPING_GET_HOLIDAY_DATES = "shopping service::get holiday dates";
    public final static String SHOPPING_GET_SEQUENTIAL_ID = "shopping service::get sequential id";
    public final static String SHOPPING_INSERT_CUSTOMER = "shopping service::insert customer";
    public final static String SHOPPING_UPDATE_ORDER_BLOB_STATUS = "shopping service::update order blob status";
    public final static String SHOPPING_GET_CUSTOM_ORDER_AVAIL = "shopping service::get custom order availability";

    public final static String SEARCH_GET_DISCOUNT_AMOUNT = "search service::get discount amount";
    public final static String SEARCH_GET_PAYMENT_METHOD_BY_ID = "search service::get payment method by id";
    public final static String SEARCH_GET_COMPANY_BY_ID = "search service::get company by id";
    public final static String SEARCH_GET_SOURCE_BY_YELLOW = "search service::get source by yellow page code";
    public final static String SEARCH_GET_INDEX_DETAILS = "search service::get index details";
    public final static String SEARCH_GET_SUB_CODE = "search service::get sub code";
    public final static String SEARCH_GET_STATE_DETAIL = "search service::get state detail";
    public final static String SEARCH_GET_SHIPPING_METHOD_ID = "search service::get shipping method id";
    public final static String SEARCH_GET_PROMOTIONS = "search service::get promotion data";
    public final static String SEARCH_GET_PRODUCTS_BY_INDEX = "search service::get products by index";
    public final static String SEARCH_GET_PRODUCTS_BY_SEARCH = "search service::get products by search";
    public final static String SEARCH_GET_PRODUCTS_BY_KEYWORD = "search service::get products by keyword";
    public final static String SEARCH_GET_DNIS_SCRIPT = "search service::get dnis script";
    public final static String SEARCH_GET_ZIP_CODE_DETAIL = "search service::get zip code detail";
    public final static String SEARCH_GET_ZIP_CODE = "search service::get zip code";
    public final static String SEARCH_GET_FLORIST = "search service::get florist";
    public final static String SEARCH_GET_GREETING_CARDS = "search service::get cards";
    public final static String SEARCH_GET_GREETING_CARDS2 = "search service::get all cards";
    public final static String SEARCH_GET_INTRODUCTION_DATA = "search service::get introduction data";
    public final static String SEARCH_GET_SUB_CATEGORY_SCRIPT = "search service::get subindex page";
    public final static String SEARCH_GET_PRICE_POINTS = "search service::get price points";
    public final static String SEARCH_GET_USER_SEARCH = "search service::get user search data";
    public final static String SEARCH_GET_PRODUCT_DETAIL = "search service::get product detail";
    public final static String SEARCH_GET_CUSTOMER = "search service::get customer";
    public final static String SEARCH_GET_INSTITUTION = "search service::get institution";
    public final static String SEARCH_GET_SOURCE_BY_VALUE = "search service::get source by value";
    public final static String SEARCH_GET_HOLIDAY_DATES = "search service::get calendar holiday dates";
    public final static String SEARCH_GET_ADDON_LIST = "search service::get addon list";
    public final static String SEARCH_GET_EXTRA_PRODUCT_DETAIL = "search service::get extra product detail";
    public final static String SEARCH_GET_TIMEZONE_FOR_ZIP = "search service::get timezone for zip";
    public final static String SEARCH_GET_SUPER_INDEX = "search service::get super index";
    public final static String SEARCH_GET_DELIVERY_DATE_RANGES = "search service::get delivery date ranges";
    public final static String SEARCH_GET_OCCASION_DESC = "search service::get occasion description";
    public final static String SEARCH_GET_COUNTRY_DESC = "search service::get country description";
    public final static String SEARCH_GET_UPSELL_DETAIL = "search service::get upsell detail";
    public final static String SEARCH_GET_UPSELL_EXTRA = "search service::get upsell extra";
    public final static String SEARCH_GET_PRODUCT_BY_ID = "search service::get product by id";
    public final static String SEARCH_GET_PRODUCTS_BY_ID = "search service:: get products by id";
    public final static String SEARCH_GET_PRODUCT_SHIPPING_METHODS = "search service::get product shipping methods";
    public final static String SEARCH_GET_ZIPCODE_CUTOFF = "search service::get zipcode cutoff";
    public final static String SEARCH_GET_MAX_VENDOR_CUTOFF = "search service::get max vendor cutoff";

    public final static String VALIDATE_DNIS = "validate service::validate dnis";
    public final static String VALIDATE_SOURCE_CODE = "validate service::validate source code";
    public final static String VALIDATE_ZIP_CODE = "validate service::validate zip";
    public final static String VALIDATE_GIFT_CERTIFICATE = "validate service::validate gift certificate";
    public final static String VALIDATE_AAA = "validate service::validate aaa";
    public final static String VALIDATE_BILLING = "validate service::validate billing info";
    public final static String VALIDATE_FLORIST = "validate service::validate florist";
    public final static String VALIDATE_COST_CENTER= "validate service::validate cost center";

    public final static String ADMIN_GET_USER = "admin service::get user";
    public final static String ADMIN_INSERT_APPLICATION_COUNTER = "admin service::insert app counter";
}