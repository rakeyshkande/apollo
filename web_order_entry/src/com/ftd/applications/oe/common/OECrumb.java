package com.ftd.applications.oe.common;

import java.io.Serializable;

public class OECrumb implements Serializable
{
    private String crumbName;
    private String url;

    public OECrumb(String newCrumbName, String newUrl) {
        crumbName = newCrumbName;
        url = newUrl;
    }    

    public String getCrumbName()
    {
        return crumbName;
    }

    public void setCrumbName(String newCrumbName)
    {
        crumbName = newCrumbName;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String newUrl)
    {
        url = newUrl;
    }



}