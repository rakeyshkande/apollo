package com.ftd.applications.oe.common;

public class OEValidationErrorMessage 
{
    public static final String Login_UserName = "Invalid User Name";
    public static final String Login_Password = "Invalid Password";
    public static final String Login_Password_Expiry = "Password has expired";

    public static final String DNIS_Invalid = "DNIS does not exist in the database as a valid DNIS";
    public static final String DNIS_Customer_Service = "DNIS is a customer service DNIS";

    public static final String Introduction_SourceCode = "Invalid Source Code";
    public static final String Introduction_SourceCode_Expired = "Source code has expired";
    public static final String Introduction_SourceCode_DNIS = "Invalid Source Code for DNIS";

    public static final String Occasion_Zipcode = "Zip Code Invalid, entered postal code is not valid";
    public static final String Occasion_Delivery_Country = "No valid delivery dates for country";
    public static final String Occasion_Delivery_Zipcode = "No valid delivery dates for zip code";

    public static final String Shopping_Item_Number = "	Item number not found";
    public static final String Shopping_Keyword = "Keyword not found";
    public static final String Shopping_Products = "No Products found matching search criteria";

    public static final String Subcategory_Item = "Item number not found";
    public static final String Subcategory_Keyword = "Keyword not found";
    public static final String Subcategory_Products = "No Products found matching search criteria";

    public static final String Advanced_Search_Products = "No Products found matching search criteria";

    public static final String Product_List_Price = "No Products found for requested price filter";

    public static final String Product_Detail_Zipcode = "Delivery Date not valid for entered Zip Code";
    public static final String Product_Detail_Price = "Entered variable price cannot be greater than maximum price";

    public static final String Custom_Order_Zipcode = "Delivery Date not valid for entered Zip Code";
    public static final String Custom_Order_Price = "Entered variable price cannot be greater than maximum price";

    public static final String Delivery_Spelling_Gift = "Possible spelling errors were found in the Gift Message";
    public static final String Delivery_Word_Gift = "Inappropriate words were found in the Gift Message";
    public static final String Delivery_Word_Comments = "Inappropriate words were found in the Order Comments";

    public static final String Shopping_Cart_Checkout = "There must be at least one (1) item in the cart in order to checkout";
    public static final String Shopping_Cart_Cancel = "Canceling an order with no items in the cart will just abandon the order";

    public static final String Billing_Credit_Card = "Could not authorize credit card";
    public static final String Billing_Credit_Card_Invalid = "Invalid credit card number";
    public static final String Billing_Credit_Card_Error = "Error occurred during credit card authorization";
    public static final String Billing_Credit_Card_Fatal = "Credit card validation system error";
    public static final String Billing_Credit_Card_Pickup = "Credit card not active or please call card issuer";
    public static final String Billing_Credit_Card_Amount = "Credit card invalid charge amount";
    public static final String Billing_Credit_Card_Date = "Credit card expired or invalid expiration date";
    public static final String Billing_Customer_ID = "Customer member ID could not be verified";
    public static final String Billing_Zipcode_Invalid = "Invalid zip code";
    public static final String Billing_Zipcode_Match = "Zip Code does not match specified City or State";
    public static final String Billing_Certificate_Invalid = "Gift certificate is invalid";
    public static final String Billing_Certificate_Expired = "Gift certificate has expired";
    public static final String Billing_Certificate_Redeemed = "Gift certificate has already been redeemed";
    public static final String Billing_Certificate_Insufficient = "Order amount exceeds the $55.00 gift card value";
    public static final String Billing_Invoice = "Invoice number is invalid";
    public static final String Billing_Cost_Center = "We're sorry, but the cost center combination you have entered is not valid.  Please check your records and enter the cost center values again";

    public static final String QMS_Address_Invalid = "Address could not be validated";
    public static final String QMS_City_State_Invalid = "City/state or zip code could not be validated";
    public static final String QMS_Zip_Invalid = "Zip code could not be validated";
    public static final String QMS_Zip_Changed = "Zip code has been changed";
    public static final String QMS_City_Invalid = "City could not be validated";
    public static final String QMS_Unit_Invalid = "Address valid but unit number not in range";
    public static final String QMS_Error = "Address system error has occurred";
    public static final String QMS_Address_Changed = "Address has been standardized";

    public static final String User_Function_Denied = "User has been denied access to this function";

    public static final String Confirmation = "none";

    public static final String Lookup_City_Record = "no records found for entered search criteria";

    public static final String Lookup_SourceCode = "no records found for entered search criteria";

    public static final String Lookup_Customer = "no records found for entered search criteria";
    
    public static final String Lookup_Greeting_Card_List = "no records found for entered search criteria";

    public static final String Lookup_Greeting_Card = "no records found for entered search criteria";

    public static final String Lookup_Florist = "no records found for entered search criteria";

    public static final String Calendar_Record = "no records found for entered search criteria";

    public static final String Calendar_Date = "selected date is invalid for specified zip code";

}