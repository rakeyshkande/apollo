package com.ftd.applications.oe.common;

import java.util.HashSet;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import com.ftd.applications.oe.common.persistent.*;

public class OEDeliveryDateParm implements Serializable
{
    private int deliveryTypeDriver = 0;
    private int cntryAddOnDays;
    private OrderPO order = null;
    private OEParameters globalParms= null;

    private Date exceptionFrom = null;
    private Date exceptionTo = null;

    private String calendarMonth = null;
    private String calendarYear = null;
    private String productType = null;
    private String productSubType = null;
    private String zipCodeGNADDFlag = null;
    // Assume floral delivery available for zip
    private String zipCodeFloralFlag = "Y";
    private String codifiedSpecialFlag = null;
    private String zipCodeGotoFloristFlag = null;
    private String deliveryType= null;
    private String exceptionCode= null;
    private String productId= null;
    private String productSubCodeId = null; 
    private String zipTimeZone= null;

    private Boolean deliverToday = Boolean.TRUE;
    //private Boolean saturdayDelivery = Boolean.TRUE;
    private Boolean sundayDelivery = Boolean.TRUE;
    private Boolean vendorNoDeliverFlag = Boolean.FALSE;
    private Boolean vendorNoShipFlag = Boolean.FALSE;
    private Boolean deliveryRangeFlag = Boolean.FALSE;

    private BigDecimal floralServiceCharge = null;
    private BigDecimal nextDayDeliveryCharge = null;
    private BigDecimal twoDayDeliveryCharge = null;
    private BigDecimal standardDeliveryCharge = null;
    private BigDecimal saturdayDeliveryCharge = null;
    private BigDecimal sundayDeliveryCharge = null;

    private HashSet noShipDays = null;
    private HashSet excludedDeliveryDays = null;
    private HashMap vendorNoDeliverFrom= null;
    private HashMap vendorNoShipFrom= null;
    private HashMap vendorNoShipTo= null;
    private HashMap vendorNoDeliverTo= null;
    private HashMap holidayDates= null;
    private HashMap shipMethods= null;
    private HashMap deliveryRangeFrom= null;
    private HashMap deliveryRangeTo= null;
    private HashMap deliveryRangeText= null;
    private BigDecimal sameDayGiftCharge;
    private boolean shipMethodFlorist;
    private boolean shipMethodCarrier;
    private String codifiedAvailable;
    private String codifiedProduct;
    private String codifiedDeliverable;
    private boolean twoDayShipFreshCuts = false;
    private boolean mondayDeliveryFreshCuts = false;

    public String getCalendarMonth()
    {
        return calendarMonth;
    }

    public void setCalendarMonth(String newCalendarMonth)
    {
        calendarMonth = newCalendarMonth;
    }

    public String getCalendarYear()
    {
        return calendarYear;
    }

    public void setCalendarYear(String newCalendarYear)
    {
        calendarYear = newCalendarYear;
    }

    public OrderPO getOrder()
    {
        return order;
    }

    public void setOrder(OrderPO newOrder)
    {
        order = newOrder;
    }

    public Boolean isDeliverToday()
    {
        return deliverToday;
    }

    public void setDeliverToday(Boolean newDeliverToday)
    {
        deliverToday = newDeliverToday;
    }
/*
    public Boolean isSaturdayDelivery()
    {
        return saturdayDelivery;
    }

    public void setSaturdayDelivery(Boolean newSaturdayDelivery)
    {
        saturdayDelivery = newSaturdayDelivery;
    }
*/
    public Boolean isSundayDelivery()
    {
        return sundayDelivery;
    }

    public void setSundayDelivery(Boolean newSundayDelivery)
    {
        sundayDelivery = newSundayDelivery;
    }

    public HashSet getNoShipDays()
    {
        return noShipDays;
    }

    public void setNoShipDays(HashSet newNoShipDays)
    {
        noShipDays = newNoShipDays;
    }

    public HashSet getExcludedDeliveryDays()
    {
        return excludedDeliveryDays;
    }

    public void setExcludedDeliveryDays(HashSet newExcludedDeliveryDays)
    {
        excludedDeliveryDays = newExcludedDeliveryDays;
    }

    public int getDeliveryTypeDriver()
    {
        return deliveryTypeDriver;
    }

    public void setDeliveryTypeDriver(int newDeliveryTypeDriver)
    {
        deliveryTypeDriver = newDeliveryTypeDriver;
    }



    public Date getExceptionFrom()
    {
        return exceptionFrom;
    }

    public void setExceptionFrom(Date newExceptionFrom)
    {
        exceptionFrom = newExceptionFrom;
    }

    public Date getExceptionTo()
    {
        return exceptionTo;
    }

    public void setExceptionTo(Date newExceptionTo)
    {
        exceptionTo = newExceptionTo;
    }

    public BigDecimal getFloralServiceCharge()
    {
        return floralServiceCharge;
    }

    public void setFloralServiceCharge(BigDecimal newFloralServiceCharge)
    {
        floralServiceCharge = newFloralServiceCharge;
    }

    public BigDecimal getNextDayDeliveryCharge()
    {
        return nextDayDeliveryCharge;
    }

    public void setNextDayDeliveryCharge(BigDecimal newNextDayDeliveryCharge)
    {
        nextDayDeliveryCharge = newNextDayDeliveryCharge;
    }

    public BigDecimal getTwoDayDeliveryCharge()
    {
        return twoDayDeliveryCharge;
    }

    public void setTwoDayDeliveryCharge(BigDecimal newTwoDayDeliveryCharge)
    {
        twoDayDeliveryCharge = newTwoDayDeliveryCharge;
    }

    public BigDecimal getStandardDeliveryCharge()
    {
        return standardDeliveryCharge;
    }

    public void setStandardDeliveryCharge(BigDecimal newStandardDeliveryCharge)
    {
        standardDeliveryCharge = newStandardDeliveryCharge;
    }

    public BigDecimal getSaturdayDeliveryCharge()
    {
        return saturdayDeliveryCharge;
    }

    public void setSaturdayDeliveryCharge(BigDecimal newSaturdayDeliveryCharge)
    {
        saturdayDeliveryCharge = newSaturdayDeliveryCharge;
    }

    public BigDecimal getSundayDeliveryCharge()
    {
        return sundayDeliveryCharge;
    }

    public void setSundayDeliveryCharge(BigDecimal newSundayDeliveryCharge)
    {
        sundayDeliveryCharge = newSundayDeliveryCharge;
    }

    public String getProductType()
    {
        return productType;
    }

    public void setProductType(String newProductType)
    {
        productType = newProductType;
    }

    public String getProductSubType()
    {
        return productSubType;
    }

    public void setProductSubType(String newProductSubType)
    {
        productSubType = newProductSubType;
    }



    public String getZipCodeGNADDFlag()
    {
        return zipCodeGNADDFlag;
    }

    public void setZipCodeGNADDFlag(String newZipCodeGNADDFlag)
    {
        zipCodeGNADDFlag = newZipCodeGNADDFlag;
    }

    public String getZipCodeFloralFlag()
    {
        return zipCodeFloralFlag;
    }

    public void setZipCodeFloralFlag(String newZipCodeFloralFlag)
    {
        // This should be set to "Y" if floral delivery available, i.e., entry
        // exists in CSZ_AVAIL for zipcode.  Set to "N" otherwise.
        zipCodeFloralFlag = newZipCodeFloralFlag;
    }

    public String getZipCodeGotoFloristFlag()
    {
        return zipCodeGotoFloristFlag;
    }

    public void setZipCodeGotoFloristFlag(String newZipCodeGotoFloristFlag)
    {
        zipCodeGotoFloristFlag = newZipCodeGotoFloristFlag;
    }



    public int getCntryAddOnDays()
    {
        return cntryAddOnDays;
    }

    public void setCntryAddOnDays(int newCntryAddOnDays)
    {
        cntryAddOnDays = newCntryAddOnDays;
    }

    public Boolean isVendorNoDeliverFlag()
    {
        return vendorNoDeliverFlag;
    }

    public void setVendorNoDeliverFlag(Boolean newVendorNoDeliverFlag)
    {
        vendorNoDeliverFlag = newVendorNoDeliverFlag;
    }





    public Boolean isVendorNoShipFlag()
    {
        return vendorNoShipFlag;
    }

    public void setVendorNoShipFlag(Boolean newVendorNoShipFlag)
    {
        vendorNoShipFlag = newVendorNoShipFlag;
    }





    public String getZipTimeZone()
    {
        return zipTimeZone;
    }

    public void setZipTimeZone(String newZipTimeZone)
    {
        zipTimeZone = newZipTimeZone;
    }

    public HashMap getVendorNoDeliverFrom()
    {
        return vendorNoDeliverFrom;
    }

    public void setVendorNoDeliverFrom(HashMap newVendorNoDeliverFrom)
    {
        vendorNoDeliverFrom = newVendorNoDeliverFrom;
    }



    public HashMap getVendorNoShipFrom()
    {
        return vendorNoShipFrom;
    }

    public void setVendorNoShipFrom(HashMap newVendorNoShipFrom)
    {
        vendorNoShipFrom = newVendorNoShipFrom;
    }

    public HashMap getVendorNoShipTo()
    {
        return vendorNoShipTo;
    }

    public void setVendorNoShipTo(HashMap newVendorNoShipTo)
    {
        vendorNoShipTo = newVendorNoShipTo;
    }

    public HashMap getVendorNoDeliverTo()
    {
        return vendorNoDeliverTo;
    }

    public void setVendorNoDeliverTo(HashMap newVendorNoDeliverTo)
    {
        vendorNoDeliverTo = newVendorNoDeliverTo;
    }

    public OEParameters getGlobalParms()
    {
        return globalParms;
    }

    public void setGlobalParms(OEParameters newGlobalParms)
    {
        globalParms = newGlobalParms;
    }

    public String getDeliveryType()
    {
        return deliveryType;
    }

    public void setDeliveryType(String newDeliveryType)
    {
        deliveryType = newDeliveryType;
    }

    public String getExceptionCode()
    {
        return exceptionCode;
    }

    public void setExceptionCode(String newExceptionCode)
    {
        exceptionCode = newExceptionCode;
    }

    public HashMap getHolidayDates()
    {
        return holidayDates;
    }

    public void setHolidayDates(HashMap newHolidayDates)
    {
        holidayDates = newHolidayDates;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }

    public HashMap getShipMethods()
    {
        return shipMethods;
    }

    public void setShipMethods(HashMap newShipMethods)
    {
        shipMethods = newShipMethods;
    }

    public Boolean isDeliveryRangeFlag()
    {
        return deliveryRangeFlag;
    }

    public void setDeliveryRangeFlag(Boolean newDeliveryRangeFlag)
    {
        deliveryRangeFlag = newDeliveryRangeFlag;
    }

    public HashMap getDeliveryRangeFrom()
    {
        return deliveryRangeFrom;
    }

    public void setDeliveryRangeFrom(HashMap newDeliveryRangeFrom)
    {
        deliveryRangeFrom = newDeliveryRangeFrom;
    }

    public HashMap getDeliveryRangeTo()
    {
        return deliveryRangeTo;
    }

    public void setDeliveryRangeTo(HashMap newDeliveryRangeTo)
    {
        deliveryRangeTo = newDeliveryRangeTo;
    }

    public HashMap getDeliveryRangeText()
    {
        return deliveryRangeText;
    }

    public void setDeliveryRangeText(HashMap newDeliveryRangeText)
    {
        deliveryRangeText = newDeliveryRangeText;
    }

    public String getCodifiedSpecialFlag()
    {
        return codifiedSpecialFlag;
    }

    public void setCodifiedSpecialFlag(String newCodifiedSpecialFlag)
    {
        codifiedSpecialFlag = newCodifiedSpecialFlag;
    }

    public BigDecimal getSameDayGiftCharge()
    {
        return sameDayGiftCharge;
    }

    public void setSameDayGiftCharge(BigDecimal newSameDayGiftCharge)
    {
        sameDayGiftCharge = newSameDayGiftCharge;
    }

    public boolean isShipMethodFlorist()
    {
        return shipMethodFlorist;
    }

    public void setShipMethodFlorist(boolean newShipMethodFlorist)
    {
        shipMethodFlorist = newShipMethodFlorist;
    }

    public boolean isShipMethodCarrier()
    {
        return shipMethodCarrier;
    }

    public void setShipMethodCarrier(boolean newShipMethodCarrier)
    {
        shipMethodCarrier = newShipMethodCarrier;
    }

    public String getCodifiedAvailable()
    {
        return codifiedAvailable;
    }

    public void setCodifiedAvailable(String newCodifiedAvailable)
    {
        codifiedAvailable = newCodifiedAvailable;
    }

    public String getCodifiedProduct()
    {
        return codifiedProduct;
    }

    public void setCodifiedProduct(String newCodifiedProduct)
    {
        codifiedProduct = newCodifiedProduct;
    }

    public String getCodifiedDeliverable()
    {
        return codifiedDeliverable;
    }

    public void setCodifiedDeliverable(String newCodifiedDeliverable)
    {
        codifiedDeliverable = newCodifiedDeliverable;
    }
    
    public boolean isTwoDayShipFreshCuts()
    {
        return twoDayShipFreshCuts;
    }

    public void setTwoDayShipFreshCuts(boolean newTwoDayShipFreshCuts)
    {
        twoDayShipFreshCuts = newTwoDayShipFreshCuts;
    }

    public boolean isMondayDeliveryFreshCuts()
    {
        return mondayDeliveryFreshCuts;
    }

    public void setMondayDeliveryFreshCuts(boolean newMondayDeliveryFreshCuts)
    {
        mondayDeliveryFreshCuts = newMondayDeliveryFreshCuts;
    }

  public void setProductSubCodeId(String productSubCodeId)
  {
    this.productSubCodeId = productSubCodeId;
  }

  public String getProductSubCodeId()
  {
    return productSubCodeId;
  }
}
