/*
 * Created on Apr 28, 2004
 *
 * Copyright 2004 FTD, Inc.
 */
package com.ftd.applications.oe.common.test;

import com.ftd.applications.oe.common.CCASType;
import com.ftd.applications.oe.common.valobjs.CreditCardVO;
import com.ftd.applications.oe.common.CreditCardApprovalUtility;

/**
 * @author tpeterson
 *
 * Stress tester for the com.ftd.applications.oe.common.CCAS class
 */
public class CCASTester {
	/**
	 * Counter to make the authorization unique
	 */
	private static int counter=0;
	private static int threadCounter=0;

	public static void main(String[] args) {
        new CCASTester().runTest();
	}
	/**
	 * Runs the CCAS stress tester
	 */
	public void runTest() {
		for( int idx=0; idx<200; idx++ ) {
			new Approval().start();
		}
		return;
	}

	/**
	 * Get the count
	 * 
	 * @return int 
	 */
	protected synchronized static int getCount() {
		return counter++;
	}

	/**
	 * Increment the count
	 * 
	 * @return int 
	 */
	protected synchronized static int incrementThreadCount() {
		return ++threadCounter;
	}

	/**
	 * Decrement the count
	 * 
	 * @return int 
	 */
	protected synchronized static int decrementThreadCount() {
		return --threadCounter;
	}

	class Approval extends Thread {
		
		/**
		 * Entry point into the thread
		 * Create a CreditCardVO and get the authorization
		 */
		public void run() {
			int threadCnt = CCASTester.getCount();
			System.out.println(String.valueOf(CCASTester.incrementThreadCount())+" active threads.");
			
			for( int idx=0; idx<1; idx++ ) {
	            CreditCardVO inCreditCardVo = new CreditCardVO();
	            CreditCardVO outCreditCardVo = inCreditCardVo;
				inCreditCardVo.setCreditCardNumber("4003000123456781");
	            inCreditCardVo.setAddressLine(String.valueOf(idx)+" Woodcreek Drive");
	            inCreditCardVo.setExpirationDate("0908");
	            StringBuffer sb = new StringBuffer();
	            sb.append(String.valueOf(threadCnt));
	            sb.append(String.valueOf(idx));
	            sb.append(".99");
	            inCreditCardVo.setAmount(sb.toString());
	            inCreditCardVo.setZipCode("60515");
	            
	            try {
	            	outCreditCardVo = CreditCardApprovalUtility.purchaseRequest(inCreditCardVo, CCASType.TYPE_FTD);
	            	
	            	sb = new StringBuffer();
	            	sb.append("Thread: ");
	            	sb.append(String.valueOf(threadCnt));
	            	sb.append("\tAuth Status: ");
	            	sb.append(outCreditCardVo.getStatus());
	            	sb.append("\tAction Code: ");
	            	sb.append(outCreditCardVo.getActionCode());
	            	sb.append("\tApproval: ");
	            	sb.append(outCreditCardVo.getApprovalCode());
	            	sb.append("\tVerbiage: ");
	            	sb.append(outCreditCardVo.getVerbiage());
	     
	            	System.out.println(sb.toString());
	            	
	            	try {
	    				sleep(5000);
	    			} catch (Exception e) {
	    				//
	    			}
	            	
	            } catch (Exception e) {
	            	e.printStackTrace();
	            }
			}
			
			//System.out.println("***** Thread " + String.valueOf(threadCnt) + " finished *****");
			//System.out.println(String.valueOf(CCASTester.decrementThreadCount())+" active threads.");
			return;
		}
	}
}
