package com.ftd.applications.oe.common;

import org.w3c.dom.Document;
import java.io.PrintWriter;
import java.io.StringWriter;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.utilities.order.OrderXAO;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.applications.oe.common.persistent.*;

import java.util.*;
import java.text.*;
import java.math.*;

public class DomainObjectConverter
{
    private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    private static SimpleDateFormat orderDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    
    public DomainObjectConverter()
    {
    }

    public OrderVO convertToDomain(OrderPO oeOrder)
    {
        OrderVO order = new OrderVO();
        List payments = new ArrayList();
        List creditCards = new ArrayList();
        List items = new ArrayList();
        List recipients = new ArrayList();
        List recipAddresses = new ArrayList();
        List recipPhones = new ArrayList();
        List addons = new ArrayList();
        List qmsAddresses = new ArrayList();
        List buyers = new ArrayList();
        List buyerAddresses = new ArrayList();
        List buyerPhones = new ArrayList();
        List buyerEmails = new ArrayList();
        List cobrands = new ArrayList();
        List memberships = new ArrayList();
        List contactInformation = new ArrayList();

        OrderDetailsVO item = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        RecipientPhonesVO recipPhone = null;
        BuyerVO buyer = null;
        BuyerPhonesVO buyerPhone = null;
        BuyerEmailsVO buyerEmail = null;
        BuyerAddressesVO buyerAddress = null;
        AddOnsVO addon = null;
        CoBrandVO cobrand = null;
        QmsAddressesVO qmsAddress = null;
        MembershipsVO membership = null;
        OrderContactInfoVO contactInfo = null;

        // Order Header
        order.setDnisCode(oeOrder.getDnisCode().toString());
        order.setMasterOrderNumber(oeOrder.getConfirmationNumber());
        order.setSourceCode(oeOrder.getSourceCode());
        order.setOrderOrigin(oeOrder.getLastOrderAction());
        order.setCsrId(oeOrder.getCsrUserName());
        order.setGUID(oeOrder.getGUID());
        order.setCallTime(oeOrder.getProcessingTime().toString());
        order.setOrderDate(orderDateFormat.format(new Date()));

        order.setOrderTotal(oeOrder.getTotalOrderPrice().toString());
        order.setProductsTotal(Integer.toString(oeOrder.getAllItems().size()));        

        // Customer
        CustomerPO oeCustomer = oeOrder.getBillToCustomer();
        if(oeCustomer != null)
        {
            buyer = new BuyerVO();
            buyer.setAutoHold(null);
            buyer.setBestCustomer(null);
            buyer.setFirstName(oeCustomer.getFirstName());
            buyer.setLastName(oeCustomer.getLastName());
            buyer.setCustomerId(oeCustomer.getCustomerId());

            // Buyer Phones
            buyerPhone = new BuyerPhonesVO();
            buyerPhone.setExtension(null);
            buyerPhone.setPhoneNumber(oeCustomer.getHomePhone());
            buyerPhone.setPhoneType("HOME");
            buyerPhones.add(buyerPhone);

//Per PhaseIII QA Defect 1787 the alt phone in WebOE will not longer
//map to the buyers work phone number.  Instead it will map to the
//alt contact number.
//            buyerPhone = new BuyerPhonesVO();
//            buyerPhone.setExtension(oeCustomer.getPhoneExtension());
//            buyerPhone.setPhoneNumber(oeCustomer.getWorkPhone());
//            buyerPhone.setPhoneType("WORK");
//            buyerPhones.add(buyerPhone);

            // Buyer Email
            buyerEmail = new BuyerEmailsVO();
            buyerEmail.setEmail(oeCustomer.getEmailAddress());
            buyerEmail.setNewsletter(oeCustomer.getSpecialPromotionsFlag());
            buyerEmail.setPrimary("Y");
            buyerEmails.add(buyerEmail);

            // Buyer Address
            buyerAddress = new BuyerAddressesVO();
            buyerAddress.setAddressEtc(oeCustomer.getCompanyName());
            buyerAddress.setAddressLine1(oeCustomer.getAddressOne());
            buyerAddress.setAddressLine2(oeCustomer.getAddressTwo());
            buyerAddress.setAddressType("Other");
            buyerAddress.setCity(oeCustomer.getCity());
            buyerAddress.setCountry(oeCustomer.getCountry());
            buyerAddress.setCounty(null);
            buyerAddress.setPostalCode(oeCustomer.getZipCode());
            buyerAddress.setStateProv(oeCustomer.getState());
            buyerAddresses.add(buyerAddress);            

            buyer.setBuyerPhones(buyerPhones);
            buyer.setBuyerEmails(buyerEmails);
            buyer.setBuyerAddresses(buyerAddresses);

            buyers.add(buyer);

            // Memberships
            membership = new MembershipsVO();
            membership.setFirstName(oeCustomer.getMembershipFirstName());
            membership.setLastName(oeCustomer.getMembershipLastName());
            membership.setMembershipIdNumber(oeCustomer.getMembershipId());
            membership.setMembershipType(oeOrder.getPartnerId());
            memberships.add(membership);

            // Contact info
            contactInfo = new OrderContactInfoVO();
            contactInfo.setOrderGuid(order.getGUID());
            contactInfo.setEmail(null);
            contactInfo.setExt(oeCustomer.getPhoneExtension());
            contactInfo.setFirstName(null);
            contactInfo.setLastName(oeCustomer.getContactInfo());
            contactInfo.setPhone(oeCustomer.getWorkPhone());

            contactInformation.add(contactInfo);
        }

        // Items
        Map oeItems = oeOrder.getAllItems();
        Iterator it = oeItems.keySet().iterator();
        ItemPO oeItem = null;
        String deliveryDateStr = null;
        Iterator addonIt = null;
        CustomerPO oeRecipient = null;
        int count = 1;
        while(it.hasNext())
        {
            oeItem = (ItemPO)oeItems.get(it.next());

            item = new OrderDetailsVO();
            item.setLineNumber(Integer.toString(count));
            item.setGuid(order.getGUID());
            count++;
            item.setCardMessage(oeItem.getCardMessage());
            item.setCardSignature(oeItem.getCardSignature());
            item.setColorFirstChoice(oeItem.getColorOne());
            item.setColorSecondChoice(oeItem.getColorTwo());
            if(oeItem.getDeliveryDate() != null)
            {
                deliveryDateStr = sdf.format(oeItem.getDeliveryDate());
            }
            item.setDeliveryDate(deliveryDateStr);
            deliveryDateStr = null;
            if(oeItem.getSecondDeliveryDate() != null)
            {
                deliveryDateStr = sdf.format(oeItem.getSecondDeliveryDate());
            }
            item.setDeliveryDateRangeEnd(deliveryDateStr);
            item.setExternalOrderNumber(oeItem.getDetailItemNumber());
            item.setFloristNumber(oeItem.getFloristCode());
            item.setLastMinuteGiftEmail(oeItem.getLastMinuteGiftEmail());
            item.setLastMinuteGiftSignature(null);
            item.setOccassionId(oeItem.getOccasion());
            item.setProductId(oeItem.getProductId());
            item.setSourceCode(order.getSourceCode());
            item.setItemOfTheWeekFlag("N");
            item.setProductSubCodeId(oeItem.getSubCodeId());
            item.setProductsAmount(oeItem.getRegularPrice().toString());
            item.setQuantity("1");
            item.setShipMethod(oeItem.getShippingMethodId());
            item.setSubstituteAcknowledgement(oeItem.getSubstitutionAuth());
            item.setOrderComments(oeItem.getItemComments());

            if(oeItem.getFloristComments() == null){
              oeItem.setFloristComments("");
            }
            if(oeItem.getShipTimeFrom() == null){
              oeItem.setShipTimeFrom("");
            }
            if(oeItem.getShipTimeTo() == null){
              oeItem.setShipTimeTo("");
            }
            if(oeItem.getInstitutionInfo() == null){             
              oeItem.setInstitutionInfo("");
            }

            String specialInstructions = oeItem.getFloristComments();
            if(oeItem.getShipTimeFrom() != null && oeItem.getShipTimeFrom().length() > 0)
            {
                specialInstructions = specialInstructions + " " + oeItem.getShipTimeFrom() + " to " + oeItem.getShipTimeTo();
            }
            if(oeItem.getInstitutionInfo() != null && oeItem.getInstitutionInfo().length() > 0)
            {
                specialInstructions = specialInstructions + " Room Number/Ward:" + oeItem.getInstitutionInfo();
            }
            item.setSpecialInstructions(specialInstructions);
            item.setSenderInfoRelease(oeItem.getReleaseSenderNameFlag());

            // Add ons
            addons = new ArrayList();
            List oeAddons = oeItem.getAddOnList();
            AddOnPO oeAddon = null;
            addonIt = oeAddons.iterator();
            while(addonIt.hasNext())
            {
                oeAddon = (AddOnPO)addonIt.next();
                addon = new AddOnsVO();
                addon.setAddOnCode(oeAddon.getAddOnId());
                addon.setAddOnQuantity(oeAddon.getAddOnQty().toString());
                addon.setAddOnType(oeAddon.getAddOnType());
                addons.add(addon);
            }

            //Recipient
            oeRecipient = oeItem.getSendToCustomer();
            if(oeRecipient != null)
            {
                recipients = new ArrayList();
                recipient = new RecipientsVO();
                recipient.setAutoHold(null);
                recipient.setFirstName(oeRecipient.getFirstName());
                recipient.setLastName(oeRecipient.getLastName());
                recipient.setCustomerId(oeRecipient.getCustomerId());

                // Recipient address
                recipAddresses = new ArrayList();
                recipAddress = new RecipientAddressesVO();
                recipAddress.setAddressLine1(oeRecipient.getAddressOne());
                recipAddress.setAddressLine2(oeRecipient.getAddressTwo());
                String instType = oeItem.getInstitutionType();
                if(instType == null) instType = "";
                if(instType.equals("H"))
                {
                    recipAddress.setAddressType("HOSPITAL");
                }
                else if(instType.equals("F"))
                {
                    recipAddress.setAddressType("FUNERAL HOME");
                }
                else if(instType.equals("N"))
                {
                    recipAddress.setAddressType("NURSING HOME");
                }
                else if(instType.equals("B"))
                {
                    recipAddress.setAddressType("BUSINESS");
                }
                else if(instType.equals("O"))
                {
                    recipAddress.setAddressType("OTHER");
                }
                else
                {
                    recipAddress.setAddressType("HOME");
                }

                recipAddress.setCity(oeRecipient.getCity());
                recipAddress.setCountry(oeRecipient.getCountry());
                recipAddress.setCounty(null);
                recipAddress.setInfo(oeItem.getInstitutionInfo());
                recipAddress.setName(oeItem.getInstitutionName());  
                if(oeRecipient.getCountryType() == null || oeRecipient.getCountryType().equalsIgnoreCase("D"))
                {
                    recipAddress.setInternational("N");
                }
                else
                {
                    recipAddress.setInternational("Y");
                }
                recipAddress.setName(oeItem.getInstitutionName());
                recipAddress.setPostalCode(oeRecipient.getZipCode());
                recipAddress.setStateProvince(oeRecipient.getState());
                recipAddress.setGeofindMatchCode(oeRecipient.getQmsUSPSRangeRecordType());
                recipAddress.setLatitude(oeRecipient.getQmsLatitude());
                recipAddress.setLongitude(oeRecipient.getQmsLongitude());
                recipAddresses.add(recipAddress);

                // Recipient phones
                recipPhones = new ArrayList();
                recipPhone = new RecipientPhonesVO();
                recipPhone.setExtension(oeRecipient.getPhoneExtension());
                recipPhone.setPhoneNumber(oeRecipient.getWorkPhone());
                recipPhone.setPhoneType("WORK");
                recipPhones.add(recipPhone);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setExtension(null);
                recipPhone.setPhoneNumber(oeRecipient.getHomePhone());
                recipPhone.setPhoneType("HOME");
                recipPhones.add(recipPhone);

                recipient.setRecipientAddresses(recipAddresses);
                recipient.setRecipientPhones(recipPhones);
                recipients.add(recipient);

                // QMS Address
                qmsAddresses = new ArrayList();
                qmsAddress = new QmsAddressesVO();
                qmsAddress.setAddressLine1(oeRecipient.getQmsAddressOne());
                qmsAddress.setAddressLine2(oeRecipient.getQmsAddressTwo());
                qmsAddress.setCity(oeRecipient.getQmsCity());
                qmsAddress.setCountry(null);
                qmsAddress.setCounty(null);
                qmsAddress.setFirmName(oeRecipient.getQmsFirmName());
                qmsAddress.setGeofindMatchCode(oeRecipient.getQmsResultCode());
                qmsAddress.setLatitude(oeRecipient.getQmsLatitude());
                qmsAddress.setLongitude(oeRecipient.getQmsLongitude());
                qmsAddress.setOverrideFlag(oeRecipient.getQmsOverrideFlag());
                qmsAddress.setPostalCode(oeRecipient.getQmsZipCode());
                qmsAddress.setRangeRecordType(oeRecipient.getQmsUSPSRangeRecordType());
                qmsAddress.setStateProvince(oeRecipient.getQmsState());
            }

            item.setAddOns(addons);
            item.setQmsAddresses(qmsAddresses);
            item.setRecipients(recipients);

            items.add(item);
        }

        // Co brand info
        Map oeCoBrands = oeOrder.getPromptMap();
        Iterator coBrandIt = oeCoBrands.keySet().iterator();
        String coBrandName = null;
        String coBrandValue = null;
        while(coBrandIt.hasNext())
        {
            coBrandName = (String)coBrandIt.next();
            coBrandValue = (String)oeCoBrands.get(coBrandName);

            if(coBrandValue != null){
                int pos = coBrandValue.indexOf(":");
                if(pos > 0 ){
                    String code = coBrandValue.substring(0,pos);
                    String value = coBrandValue.substring(pos+1,coBrandValue.length());

                    cobrand = new CoBrandVO();
                    cobrand.setInfoData(value);
                    cobrand.setInfoName(code);
                    cobrands.add(cobrand);
                }//end : found
            }//end cobrand value not null
        }
        
        /**
         * Added membership information as part of co-brand information. This
         * was done on account of the ScrubMapperDAO, which requires membership
         * information to be sent as a part of co-brand information
         */
        if ( (oeCustomer.getMembershipFirstName() != null)
              && ( ! oeCustomer.getMembershipFirstName().equals("")))
        {
              cobrand = new CoBrandVO();
              cobrand.setInfoName("FNAME");
              cobrand.setInfoData(oeCustomer.getMembershipFirstName());
              cobrands.add(cobrand);           
        }
        if ( (oeCustomer.getMembershipLastName() != null)
              && ( ! oeCustomer.getMembershipLastName().equals("")))
        {
              cobrand = new CoBrandVO();
              cobrand.setInfoName("LNAME");
              cobrand.setInfoData(oeCustomer.getMembershipLastName());
              cobrands.add(cobrand);           
        }
        if ( (oeCustomer.getMembershipId() != null)
              && ( ! oeCustomer.getMembershipId().equals("")))
        {
              cobrand = new CoBrandVO();
              cobrand.setInfoName(oeOrder.getPartnerId());
              cobrand.setInfoData(oeCustomer.getMembershipId());
              cobrands.add(cobrand);           
        }

        // Payments
        // Get credit card payment
        payment = new PaymentsVO();
        payment.setAafesTicket(oeOrder.getCreditCardTicketNumber());
        payment.setAcqReferenceNumber(oeOrder.getAcqReferenceData());
        payment.setAuthNumber(oeOrder.getApprovalCode());
        payment.setAuthResult(oeOrder.getApprovalVerbiage());
        payment.setAvsCode(oeOrder.getAvsResult());
        payment.setPaymentMethodType("C");
        payment.setInvoiceNumber(oeOrder.getInvoiceNumber()); 
        payment.setNcApprovalCode(oeOrder.getNcApprovalId());
        
        //Apollo Production defect #216
        if( oeOrder.getPaymentMethodType() !=null && oeOrder.getPaymentMethodType().equals("I") )
        {
            payment.setPaymentMethodType("I");
            payment.setPaymentsType("IN");
        }
        else
        {
            payment.setPaymentsType(oeOrder.getCreditCardType());            
        }


        // Credit cards
        creditCard = new CreditCardsVO();
        String ccExpire = "";
        if(oeOrder.getCreditCardExpireMonth() != null && oeOrder.getCreditCardExpireYear() != null && oeOrder.getCreditCardExpireYear().length() > 1)
        {
            ccExpire = oeOrder.getCreditCardExpireMonth() + "/" + oeOrder.getCreditCardExpireYear().substring(2);
        }

        creditCard.setCCExpiration(ccExpire);
        creditCard.setCCNumber(oeOrder.getCreditCardNumber());
        //if credit card number is null, set it to blank
        if(creditCard.getCCNumber() == null)
        {
          creditCard.setCCNumber("");
        }
        creditCard.setCCType(oeOrder.getCreditCardType());


        
        creditCards.add(creditCard);

        payment.setCreditCards(creditCards);
        payments.add(payment);

        // Get gift certificate if present
        if(oeOrder.getGiftCertificateId() != null && oeOrder.getGiftCertificateId().length() > 0)
        {
            payment = new PaymentsVO();
            BigDecimal giftCertAmount = oeOrder.getGiftCertificateAmount();
            if(giftCertAmount == null)
            {
                giftCertAmount = new BigDecimal("0");
            }
            

            //check if the gift certificate covers the entire order price
            int compareResult = oeOrder.getTotalOrderPrice().compareTo(giftCertAmount);
            if(compareResult == 0 || compareResult < 0)
            {
            
                //If the total amount of the gift card is equal to or greater then amount on order
                //then remove all other payment types from order.
                //This was put in for phaseIII defect #1492
                payments = new ArrayList();
                
                //set the amount of the gift certificate equal to the order total
                //This is for phaseIII defect #1554
                payment.setAmount(oeOrder.getTotalOrderPrice().toString());
            }
            else
            {
                //Set the amount of the gift certificate
                payment.setAmount(giftCertAmount.toString());
            }
            
            payment.setGiftCertificateId(oeOrder.getGiftCertificateId());
            payment.setPaymentsType("GC");
            payment.setPaymentMethodType("G");
            payments.add(payment);
        }

        order.setBuyer(buyers);
        order.setCoBrand(cobrands);
        order.setMemberships(memberships);
        order.setOrderContactInfo(contactInformation);
        order.setOrderDetail(items);
        order.setPayments(payments);


        return order;
    }



    public static void main(String[] args)
    {
        DomainObjectConverter domainObjectConverter = new DomainObjectConverter();
    }
}
