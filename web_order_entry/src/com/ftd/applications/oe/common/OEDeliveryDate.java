package com.ftd.applications.oe.common;

import java.io.Serializable;
import java.util.ArrayList;

public class OEDeliveryDate implements Serializable
{
    private String deliveryDate;
    private String holidayText;
    private String deliverableFlag;
    private String shippingAllowed;
    private String dayOfWeek;
    private ArrayList shippingMethods;  // Array of ShippingMethod objects 
    private String displayDate;
    private boolean inDateRange;

    public OEDeliveryDate()
    {
        deliveryDate = "";
        holidayText = "";
        deliverableFlag = "N";
        dayOfWeek = "";
        shippingMethods = new ArrayList();
        inDateRange = false;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDeliveryDate(String newDeliveryDate)
    {
        deliveryDate = newDeliveryDate;
    }

    public String getHolidayText()
    {
        return holidayText;
    }

    public void setHolidayText(String newHolidayText)
    {
        holidayText = newHolidayText;
    }

    public String getDeliverableFlag()
    {
        return deliverableFlag;
    }

    public void setDeliverableFlag(String newDeliverableFlag)
    {
        deliverableFlag = newDeliverableFlag;
    } 

    public String getDayOfWeek()
    {
        return dayOfWeek;
    }

    public void setDayOfWeek(String newDayOfWeek)
    {
        dayOfWeek = newDayOfWeek;
    }

/*
    public String getShippingMethod()
    {
        return shippingMethod;
    }

    public void setShippingMethod(String newShippingMethod)
    {
        shippingMethod = newShippingMethod;
    }
*/
/*
    public BigDecimal getDeliveryCharge()
    {
        return deliveryCharge;
    }

    public void setDeliveryCharge(BigDecimal newDeliveryCharge)
    {
        deliveryCharge = newDeliveryCharge;
    }
*/
    public String getDisplayDate()
    {
        return displayDate;
    }

    public void setDisplayDate(String newDisplayDate)
    {
        displayDate = newDisplayDate;
    }

    public ArrayList getShippingMethods()
    {
        return shippingMethods;
    }

    public void setShippingMethods(ArrayList newShippingMethods)
    {
        shippingMethods = newShippingMethods;
    }

    public String getShippingAllowed()
    {
        return shippingAllowed;
    }

    public void setShippingAllowed(String newShippingAllowed)
    {
        shippingAllowed = newShippingAllowed;
    }

    public boolean isInDateRange()
    {
        return inDateRange;
    }

    public void setInDateRange(boolean newInDateRange)
    {
        inDateRange = newInDateRange;
    }



}