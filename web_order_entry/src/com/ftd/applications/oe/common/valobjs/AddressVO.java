package com.ftd.applications.oe.common.valobjs;

/**
 * AddressVO contains address information.
 * 
 * @author 	Mike McCormack
 */
public class AddressVO
{

    private String firmName;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zip10;
    private String latitude;
    private String longitude;
    private String USPSRangeRecordType;
    private String lowEndHouseNumber;
    private String highEndHouseNumber;
    private String lowUnitNumber;
    private String highUnitNumber;
    private String unitNumber;
    private String houseNumber;
    private String matchCode;
    private String deliveryMethod;
    

/** 
 * Get firmName
 *
 * @returns string : firmName of address
 */
    public String getFirmName()
    {
        return firmName;
    }

/** 
 * Set firmName
 *
 * @param string : firmName of address
 */
    public void setFirmName(String newFirmName)
    {
        firmName = newFirmName;
    }
/** 
 * Get addressLine1
 *
 * @returns string : addressLine1 of address
 */
    public String getAddressLine1()
    {
        return addressLine1;
    }

/** 
 * Set addressLine1
 *
 * @param string : addressLine1 of address
 */
    public void setAddressLine1(String newAddressLine1)
    {
        addressLine1 = newAddressLine1;
    }
/** 
 * Get addressLine2
 *
 * @returns string : addressLine2 of address
 */
    public String getAddressLine2()
    {
        return addressLine2;
    }

/** 
 * Set addressLine2
 *
 * @param string : addressLine2 of address
 */
    public void setAddressLine2(String newAddressLine2)
    {
        addressLine2 = newAddressLine2;
    }



/** 
 * Get city
 *
 * @returns string : city of address
 */
    public String getCity()
    {
        return city;
    }

/** 
 * Set city
 *
 * @param string : city of address
 */
    public void setCity(String newCity)
    {
        city = newCity;
    }
/** 
 * Get state
 *
 * @returns string : state of address
 */
    public String getState()
    {
        return state;
    }

/** 
 * Set state
 *
 * @param string : state of address
 */
    public void setState(String newState)
    {
        state = newState;
    }
/** 
 * Get zip10
 *
 * @returns string : zip10 of address
 */
    public String getZip10()
    {
        return zip10;
    }

/** 
 * Set zip10
 *
 * @param string : zip10 of address
 */
    public void setZip10(String newZip10)
    {
        zip10 = newZip10;
    }
/** 
 * Get latitude
 *
 * @returns string : latitude of address
 */
    public String getLatitude()
    {
        return latitude;
    }

/** 
 * Set latitude
 *
 * @param string : latitude of address
 */
    public void setLatitude(String newLatitude)
    {
        latitude = newLatitude;
    }
/** 
 * Get longitude
 *
 * @returns string : longitude of address
 */
    public String getLongitude()
    {
        return longitude;
    }

/** 
 * Set longitude
 *
 * @param string : longitude of address
 */
    public void setLongitude(String newLongitude)
    {
        longitude = newLongitude;
    }
/** 
 * Get USPSRangeRecordType
 *
 * @returns string : USPSRangeRecordType of address
 */
    public String getUSPSRangeRecordType()
    {
        return USPSRangeRecordType;
    }

/** 
 * Set USPSRangeRecordType
 *
 * @param string : USPSRangeRecordType of address
 */
    public void setUSPSRangeRecordType(String newUSPSRangeRecordType)
    {
        USPSRangeRecordType = newUSPSRangeRecordType;
    }
/** 
 * Get lowEndHouseNumber
 *
 * @returns string : lowEndHouseNumber of address
 */
    public String getLowEndHouseNumber()
    {
        return lowEndHouseNumber;
    }

/** 
 * Set lowEndHouseNumber
 *
 * @param string : lowEndHouseNumber of address
 */
    public void setLowEndHouseNumber(String newLowEndHouseNumber)
    {
        lowEndHouseNumber = newLowEndHouseNumber;
    }
/** 
 * Get highEndHouseNumber
 *
 * @returns string : highEndHouseNumber of address
 */
    public String getHighEndHouseNumber()
    {
        return highEndHouseNumber;
    }

/** 
 * Set highEndHouseNumber
 *
 * @param string : highEndHouseNumber of address
 */
    public void setHighEndHouseNumber(String newHighEndHouseNumber)
    {
        highEndHouseNumber = newHighEndHouseNumber;
    }

/** 
 * Get lowUnitNumber
 *
 * @returns string : lowUnitNumber of address
 */
    public String getLowUnitNumber()
    {
        return lowUnitNumber;
    }

/** 
 * Set lowUnitNumber
 *
 * @param string : lowUnitNumber of address
 */
    public void setLowUnitNumber(String newLowUnitNumber)
    {
        lowUnitNumber = newLowUnitNumber;
    }
/** 
 * Get highUnitNumber
 *
 * @returns string : highUnitNumber of address
 */
    public String getHighUnitNumber()
    {
        return highUnitNumber;
    }

/** 
 * Set highUnitNumber
 *
 * @param string : highUnitNumber of address
 */
    public void setHighUnitNumber(String newHighUnitNumber)
    {
        highUnitNumber = newHighUnitNumber;
    }
/** 
 * Get houseNumber
 *
 * @returns string : houseNumber of address
 */
    public String getHouseNumber()
    {
        return houseNumber;
    }

/** 
 * Set houseNumber
 *
 * @param string : houseNumber of address
 */
    public void setHouseNumber(String newHouseNumber)
    {
        houseNumber = newHouseNumber;
    }
/** 
 * Get unitNumber
 *
 * @returns string : unitNumber of address
 */
    public String getUnitNumber()
    {
        return unitNumber;
    }

/** 
 * Set unitNumber
 *
 * @param string : unitNumber of address
 */
    public void setUnitNumber(String newUnitNumber)
    {
        unitNumber = newUnitNumber;
    }


/** 
 * Get matchCode
 *
 * @returns string : matchCode of address
 */
    public String getMatchCode()
    {
        return matchCode;
    }

/** 
 * Set matchCode
 *
 * @param string : matchCode of address
 */
    public void setMatchCode(String newMatchCode)
    {
        matchCode = newMatchCode;
    }


/** 
 * Get deliveryMethod
 *
 * @returns string : deliveryMethod of address
 */
    public String getDeliveryMethod()
    {
        return deliveryMethod;
    }

/** 
 * Set deliveryMethode
 *
 * @param string : deliveryMethod of address
 */
    public void setDeliveryMethod(String newDeliveryMethod)
    {
        deliveryMethod = newDeliveryMethod;
    }

    
}