package com.ftd.applications.oe.common;

import java.util.Comparator;

public class ShipMethodSortDescription implements Comparator
{
  public int compare(Object origMethod, Object newMethod) 
  {
    String origDescription = ((ShippingMethod) origMethod).getDescription().toUpperCase();
    String newDescription = ((ShippingMethod) newMethod).getDescription().toUpperCase();

    return origDescription.compareTo(newDescription);

  }
}
