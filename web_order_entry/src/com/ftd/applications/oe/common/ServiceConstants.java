package com.ftd.applications.oe.common;

public class ServiceConstants 
{
    public final static String ADMIN_SERVICE = "Admin Service";
    public final static String SEARCH_SERVICE = "Search Service";
    public final static String SHOPPING_SERVICE = "Shopping Service";
    public final static String BILLING_SERVICE = "Billing Service";
}