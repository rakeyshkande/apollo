package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class DeliveryUpdateServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId =  request.getParameter("persistentObjId");

        String searchType = request.getParameter("searchType");
        if(searchType == null) searchType = "";

        String upsellFlag = request.getParameter("upsellFlag");
        if(upsellFlag == null) upsellFlag = "";
        
        String cartItemNumber = request.getParameter("cartItemNumber");
        if(cartItemNumber == null) cartItemNumber = "";

        String source = request.getParameter("source");
        if(source == null) source = "";

        String deliveryMethod = request.getParameter("deliveryMethod");
        if(deliveryMethod == null) deliveryMethod = "";

        String maxDeliveryDaysOut = request.getParameter("MAXDeliveryDaysOut");
        if(maxDeliveryDaysOut == null) maxDeliveryDaysOut = "false";
        
    
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

         Enumeration enum1 = request.getParameterNames();
         String param = "";
        String value = "";
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            ftdRequest.addArgument(param, value);
        }
     //ftdRequest.addArgument("countryType", "D");
     ftdRequest.setServiceDescription("SHOPPING SERVICE");
     ftdRequest.setCommand("SHOPPING_UPDATE_DELIVERY_INFO");

      FTDServiceResponse ftdResponse = null;
    
      try
      {     
         ftdResponse = util.executeService(ftdRequest);
      }
      catch(Exception e)
      {
            util.getLogManager().error("Exception in executing serlvet.Specific: " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response); 
      }    

      try
      {
      
         ConfigurationUtil cu = ConfigurationUtil.getInstance();

         // Get the user's call center URL
         StringBuffer callCenterUrlKey = new StringBuffer();
         callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
         String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
         if(callCenterUrl == null)
         {
             // get default url
             callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
         }
        
        String url = callCenterUrl;
        url = url + request.getServletPath() + "?" + request.getQueryString();

          ftdRequest = new FTDServiceRequest();
          ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
          ftdRequest.setPersistenceID(persistentObjId);
          
          ftdRequest.addArgument("company", "FTDP");
          ftdRequest.addArgument("pagename", "DeliveryInfo");
          ftdRequest.addArgument("url", url);
          ftdRequest.addArgument("upsellFlag", upsellFlag);
          ftdRequest.addArgument("cartItemNumber", cartItemNumber);
          ftdRequest.addArgument("source", source);
          ftdRequest.addArgument("deliveryMethod", deliveryMethod);
          ftdRequest.addArgument("maxDeliveryDaysOut", maxDeliveryDaysOut);
    
          ftdRequest.setServiceDescription("SHOPPING SERVICE");
          ftdRequest.setCommand("SHOPPING_LOAD_DELIVERY_INFO");
    
          if(request.getParameter("submitt") == null)
          {      
              ftdResponse  = util.executeService(ftdRequest);
              XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
              String xslFile = "/xsl/delivery_info.xsl";
              xslFile = getServletContext().getRealPath(xslFile);
              HashMap params = new HashMap();
    
              params.put("persistentObjId", ftdResponse.getPersistanceID());
              params.put("sessionId", sessionId);
              params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryUpdateServlet"));
              params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
              params.put("breadCrumbType", "D");
              params.put("searchType", searchType);
              params.put("upsellFlag", upsellFlag);
    
              String focusElement = request.getParameter("focusElement");
              if(focusElement == null) focusElement = "";
              params.put("focusElement", focusElement);
    
                util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
                if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
                {
                    util.writeXMLtoFile("deliveryUpdate.xml", xml);
                }
          }
          else
          {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/ShoppingCartServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId);
                if (dispatcher != null)
                    dispatcher.forward(request, response);      
          }     
      }
      catch(Exception e)
      {
          util.getLogManager().error("get execute service " + e.toString(), e);
          RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
          if (dispatcher != null)
              dispatcher.forward(request, response);  
      }
  }

}  
