package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class DeliveryServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);

        String persistentObjId = "";
        //String zip = "";
        String productId = "";
        //String deliveryDate = "";
        //String deliveryDateDisplay = "";
        String deliveryShippingMethod = "";
        String carrier = "";
        String price = "";
        String variablePrice = "";
        String color1 = "";
        String color2 = "";
        String baloon = "";
        String baloonQty = "";
        String bear = "";
        String bearQty = "";  
        String chocolate = "";
        String chocolateQty = "";  
        String banner = "";
        String bannerQty = "";
        String card = "";
        String subCodeId = "";
        String searchType = "";
        String upsellFlag = "";

        persistentObjId =  request.getParameter("persistentObjId");
        if(persistentObjId == null) persistentObjId = "";
    
        //zip = request.getParameter("zip");
        //if(zip == null) zip = "";
    
        productId = request.getParameter("productId");
        if(productId  == null) productId = "";
    
        //deliveryDate = request.getParameter("DELIVERY_DATE");
        //if(deliveryDate == null) deliveryDate = "";
    
        //deliveryDateDisplay = request.getParameter("deliveryDateDisplay");
        //if(deliveryDateDisplay == null) deliveryDateDisplay = "";

        deliveryShippingMethod = request.getParameter("DELIVERY_SHIPPING_METHOD");
        if(deliveryShippingMethod == null) deliveryShippingMethod = "";
    
        carrier = request.getParameter("carrier");
        if(carrier == null) carrier = "";
    
        price = request.getParameter("price");    
        if(price == null) price = "";

        variablePrice = request.getParameter("variablePriceAmount");    
        if(variablePrice == null) variablePrice = "";

        color1 = request.getParameter("color1");    
        if(color1 == null) color1 = "";

        color2 = request.getParameter("color2");    
        if(color2 == null) color2 = "";

        baloon = request.getParameter("baloonCheckBox");   
        if(baloon == null) baloon = "";

        baloonQty = request.getParameter("baloonQty");    
        if(baloonQty == null) baloonQty = "";

        bear = request.getParameter("bearCheckBox");    
        if(bear == null) bear = "";

        bearQty = request.getParameter("bearQty");    
        if(bearQty == null) bearQty = "";

        chocolate = request.getParameter("chocolateCheckBox");    
        if(chocolate == null) chocolate = "";

        chocolateQty = request.getParameter("chocolateQty");
        if(chocolateQty == null) chocolateQty = "";

        banner = request.getParameter("funeralBannerCheckBox");
        if(banner == null) banner = "";

        bannerQty = request.getParameter("funeralBannerQty");
        if(bannerQty == null) bannerQty = "";

        card = request.getParameter("cards");
        if(card == null) card = "";

        subCodeId = request.getParameter("subCodeId");
        if(subCodeId == null) subCodeId = "";

        searchType = request.getParameter("searchType");
        if(searchType == null) searchType = "";

        upsellFlag = request.getParameter("upsellFlag");
        if(upsellFlag == null) upsellFlag = "";

        String cartItemNumber = request.getParameter("cartItemNumber");
        if(cartItemNumber == null) cartItemNumber = "";

        String secondChoiceAuth = request.getParameter("secondChoiceAuth");
        if(secondChoiceAuth == null) secondChoiceAuth = "";

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("pagename", "Shopping");
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("productId", productId);
        //ftdRequest.addArgument("CONTACT_ZIP", zip);
        //ftdRequest.addArgument("DELIVERY_DATE", deliveryDate);
        //ftdRequest.addArgument("deliveryDateDisplay", deliveryDateDisplay);
        ftdRequest.addArgument("DELIVERY_SHIPPING_METHOD", deliveryShippingMethod);
        ftdRequest.addArgument("CARRIER", carrier);
        ftdRequest.addArgument("PRICE", price);
        ftdRequest.addArgument("VARIABLE_PRICE_AMT", variablePrice);
        ftdRequest.addArgument("COLOR1", color1);
        ftdRequest.addArgument("COLOR2", color2); 
        ftdRequest.addArgument("balloon", baloon);
        ftdRequest.addArgument("balloonQty", baloonQty);
        ftdRequest.addArgument("bear", bear);
        ftdRequest.addArgument("bearQty", bearQty);
        ftdRequest.addArgument("chocolate", chocolate);
        ftdRequest.addArgument("chocolateQty", chocolateQty);
        ftdRequest.addArgument("banner", banner);
        ftdRequest.addArgument("bannerQty", bannerQty);
        ftdRequest.addArgument("card", card);
        ftdRequest.addArgument("subCodeId", subCodeId);
        ftdRequest.addArgument("upsellFlag", upsellFlag);
        ftdRequest.addArgument("secondChoiceAuth", secondChoiceAuth);

        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        
        if(cartItemNumber.length() > 0)
        {
            ftdRequest.setCommand("SHOPPING_UPDATE_ITEM_INFO");
            ftdRequest.addArgument("cartItemNumber", cartItemNumber);
        }
        else
        {
          ftdRequest.setCommand("SHOPPING_ADD_ITEM");
        }

        FTDServiceResponse ftdResponse = null;
    
        try
        {     
          ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error("Exception in executing serlvet.Specific: " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response); 
        }    

        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
        String url = callCenterUrl;
        url = url + request.getServletPath() + "?" + request.getQueryString();

        ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
      
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "DeliveryInfo");
        ftdRequest.addArgument("url", url);
        ftdRequest.addArgument("upsellFlag", upsellFlag);
        ftdRequest.addArgument("cartItemNumber", cartItemNumber);

        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_LOAD_DELIVERY_INFO");
      
          ftdResponse  = util.executeService(ftdRequest);
          XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
          String xslFile = "/xsl/delivery_info.xsl";
          xslFile = getServletContext().getRealPath(xslFile);
          HashMap params = new HashMap();

          params.put("persistentObjId", ftdResponse.getPersistanceID());
          params.put("sessionId", sessionId);
          params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryUpdateServlet"));
          params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
          params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
          params.put("searchType", searchType);
          params.put("upsellFlag", upsellFlag);
        
          params.put("breadCrumbType", "D");

          util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
          if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
          {
              util.writeXMLtoFile("delivery.xml", xml);
          }
        }
        catch(Exception e)
        {
              util.getLogManager().error("get execute service " + e.toString(), e);
              RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
              if (dispatcher != null)
                  dispatcher.forward(request, response);  
        }
    }

}  