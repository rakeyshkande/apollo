package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.GeneralConstants;
import oracle.xml.parser.v2.*;

public class AdminServlet extends HttpServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
    
        Util util = new Util();
        util.recordTask(request, response);

        response.setHeader("Cache-Control", "no-cache");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.NONE);
      
        ftdRequest.addArgument("sessionId", sessionId);
    
        ftdRequest.setServiceDescription("ADMIN SERVICE");
        ftdRequest.setCommand("GET_ADMIN");

        try
        {
            FTDServiceResponse ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/adminhome.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/DNISServlet"));
            params.put("userAdminServlet", (request.getContextPath() + "/servlet/UserAdminServlet"));
            ResourceManager rm = ResourceManager.getInstance();
            String pdbURL = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "pdb.url");
            params.put("pdbServlet", pdbURL);
			
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, true);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("adminhome.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

}