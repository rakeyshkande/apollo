package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.security.cache.vo.UserInfo;

public class IntroductionServlet extends HttpServlet
{

  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
    
        Util util = new Util();
        util.recordTask(request, response);
        String sourcetext = "";
        String homephone = "";
        String dnis = "";
        String city = "";
        String address = "";
        String state = "";
        String zip = "";
        String yellowPageBookCode = "";
        String firstName = "";
        String lastName = "";
        String sourceText = "";
        String sourceSelect = "";

        String persistentObjId = "";
        String reqsource = request.getParameter("reqsource");
        dnis = request.getParameter("dnis");

        if(reqsource.equals("search"))
        {
            sourcetext = request.getParameter("sourceText");
            if(sourcetext == null) sourcetext = "";
            homephone = request.getParameter("phone");
            if(homephone == null) homephone = "";
            city = request.getParameter("city");
            if(city == null) city = "";
            address = request.getParameter("address");
            if(address == null) address = "";
            state = request.getParameter("state");
            if(state == null) state = "";
            zip = request.getParameter("zip");
            if(zip == null) zip = "";
            yellowPageBookCode = request.getParameter("yellowPageBookCode");
            if(yellowPageBookCode == null) yellowPageBookCode = "";
            firstName = request.getParameter("firstName");
            if(firstName == null) firstName = "";
            lastName = request.getParameter("lastName");
            if(lastName == null) lastName = "";
            sourceSelect =  request.getParameter("sourceSelect");
            if(sourceSelect == null) sourceSelect = "";
        } 

        String userFirstName = userInfo.getFirstName();
        if(userFirstName == null) userFirstName = "";
        String userLastName = userInfo.getLastName();
        if(userLastName == null) userLastName = "";
        String callCenterId = userInfo.getCallCenter();
        if(callCenterId == null) callCenterId = "";
        String userId = userInfo.getUserID();
        if(userId == null) userId = "";

        response.setHeader("Cache-Control", "no-cache");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        if (reqsource.equals("csrMenu")) {
            ftdRequest.setPersistence(FTDServiceRequest.CREATE_OBJECT);
        } else {
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            persistentObjId = request.getParameter("persistentObjId");
            ftdRequest.setPersistenceID(persistentObjId);
        }
        ftdRequest.addArgument("dnis", dnis);
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "Introduction");
        ftdRequest.addArgument("displayExpired", "Y");
        ftdRequest.addArgument("userFirstName", userFirstName);
        ftdRequest.addArgument("userLastName", userLastName);
        ftdRequest.addArgument("callCenterId", callCenterId);
        ftdRequest.addArgument("userId", userId);
        ftdRequest.setServiceDescription("SEARCH SERVICE");
        ftdRequest.setCommand("SEARCH_GET_INTRODUCTION");
        FTDServiceResponse ftdResponse = null;
        String error = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/introduction.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/OccasionServlet"));
            params.put("searchServlet", (request.getContextPath() + "/servlet/SearchServlet"));
            params.put("yellowPageBookCode", yellowPageBookCode);
            params.put("sourceSelect", "");
            params.put("sourceText", sourcetext);
            params.put("homePhone", homephone);
            params.put("firstName", firstName);
            params.put("lastName", lastName);
            params.put("city", city);
            params.put("address", address);
            params.put("state", state);
            params.put("zip", zip);
            params.put("userFirstName", userFirstName);
            params.put("userLastName", userLastName);
            params.put("sourceSelect", sourceSelect);

                util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("introduction.xml", xml);
            }
    }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

}
