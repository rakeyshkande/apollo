package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;

public class ShoppingCartBillingServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
        Util util = new Util();
        util.recordTask(request, response);

        String persistentObjId = request.getParameter("persistentObjId");

        response.setHeader("Cache-Control", "no-cache");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
     	Enumeration enum1 = request.getParameterNames();
     	String param = "";
     	String value = "";
     	while(enum1.hasMoreElements())
     	{
     	   param = (String) enum1.nextElement();
     	   value = request.getParameter(param);
     	   ftdRequest.addArgument(param, value);
     	}

        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "Billing");

        ftdRequest.setServiceDescription("BILLING SERVICE");
        ftdRequest.setCommand("BILLING_UPDATE_BILLING_INFO");

        FTDServiceResponse ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);

			ftdRequest.setServiceDescription("SHOPPING SERVICE");
        	ftdRequest.setCommand("SHOPPING_GET_SHOPPING_CART");
            ftdResponse  = util.executeService(ftdRequest);

            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();

            String xslFile = "/xsl/shoppingCart.xsl";
            xslFile = getServletContext().getRealPath(xslFile);

            HashMap params = new HashMap();
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));
            util.determineCompanyIdAndSetImageSuffixParams(params, xml);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("shoppingCart.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("this is where it hapenned ... get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }

}
