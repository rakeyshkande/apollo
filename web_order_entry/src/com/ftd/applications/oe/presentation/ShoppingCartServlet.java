package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.applications.oe.common.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.util.ServletHelper;

public class ShoppingCartServlet extends HttpServlet 
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId = request.getParameter("persistentObjId");
        String command = request.getParameter("command");
        String custom = request.getParameter("custom");
        if(custom == null) custom = "";
        if(command == null) command = "";
        
        if(command.equals("remove"))
        {
            handleRemoveItem(request, response);
        }
        else if(command.equals("changeSourceCode"))
        {
            handleChangeSourceCode(request, response);
        }
        else if(command.equals("addSameItem"))
        {
            handleAddSameItem(request, response);
        }
        else if(command.equals("updateItem") && custom.equals("N"))
        {
            handleUpdateItem(request, response);
        }
        else if(command.equals("updateItem") && custom.equals("Y"))
        {
            handleUpdateCustomItem(request, response);
        }

        FTDServiceResponse ftdResponse = null;

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_GET_SHOPPING_CART");

        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/shoppingCart.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();

            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            //params.put("actionServlet", (request.getContextPath() + "/servlet/XSLTestServlet"));
            params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));
            
            // Defect 1240 enhancement
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String siteName = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.name");
            String sslPort = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.sslport");

            params.put("siteNameSsl", ServletHelper.switchServerPort(siteName, sslPort));
            params.put("applicationContext", request.getContextPath());
      
            util.determineCompanyIdAndSetImageSuffixParams(params, xml);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("shoppingcart.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);            
        }        
    }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId = request.getParameter("persistentObjId");
        String command = request.getParameter("command");
        String custom = request.getParameter("custom");
        if(custom == null) custom = "";
        if(command == null) command = "";
        

        if(command.equals("remove"))
        {
            handleRemoveItem(request, response);
        }
        else if(command.equals("changeSourceCode"))
        {
            handleChangeSourceCode(request, response);
        }
        else if(command.equals("addSameItem"))
        {
            handleAddSameItem(request, response);
        }
        else if(command.equals("updateItem") && custom.equals("N"))
        {
            handleUpdateItem(request, response);
        }
        else if(command.equals("updateItem") && custom.equals("Y"))
        {
            handleUpdateCustomItem(request, response);
        }
        
        FTDServiceResponse ftdResponse = null;

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_GET_SHOPPING_CART");

        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/shoppingCart.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();

            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            //params.put("actionServlet", (request.getContextPath() + "/servlet/XSLTestServlet"));
            params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));
            util.determineCompanyIdAndSetImageSuffixParams(params, xml);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("shoppingcart.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }        
    }

    private void handleRemoveItem(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        String itemNumber = request.getParameter("itemCartNumber");
        if(itemNumber == null) itemNumber = "";

        FTDServiceResponse ftdResponse = null;

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand(CommandConstants.SHOPPING_REMOVE_ITEM);
        ftdRequest.addArgument("itemCartNumber", itemNumber);

        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
           util.getLogManager().error("get execute service" + e.toString(), e);  
        }        
    }

    private void handleAddSameItem(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        String itemNumber = request.getParameter("itemCartNumber");
        if(itemNumber == null) itemNumber = "";

        FTDServiceResponse ftdResponse = null;

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand(CommandConstants.SHOPPING_ADD_DUPLICATE_ITEM);
        ftdRequest.addArgument("cartItemNumber", itemNumber);

        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);  
        }        
    }

    private void handleUpdateCustomItem(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String persistentObjId =  request.getParameter("persistentObjId");
      
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";

        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param); 
            ftdRequest.addArgument(param, value);
        }

        FTDServiceResponse ftdResponse = null;

        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_CUSTOM_ITEM_INFO");
        
        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);  
        }        
    }
    private void handleUpdateItem(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        String itemNumber = request.getParameter("cartItemNumber");
        if(itemNumber == null) itemNumber = "";

        String zip = "";
        String productId = "";
        String deliveryDate = "";
        String deliveryDateDisplay = "";
        String deliveryShippingMethod = "";
        String carrier = "";
        String price = "";
        String variablePrice = "";
        String color1 = "";
        String color2 = "";
        String baloon = "";
        String baloonQty = "";
        String bear = "";
        String bearQty = "";  
        String chocolate = "";
        String chocolateQty = "";  
        String banner = "";
        String bannerQty = "";
        String card = "";
        String subCodeId = "";

        persistentObjId =  request.getParameter("persistentObjId");
        if(persistentObjId == null) persistentObjId = "";
    
        zip = request.getParameter("zip");
        if(zip == null) zip = "";
    
        productId = request.getParameter("productId");
        if(productId  == null) productId = "";
    
        deliveryDate = request.getParameter("DELIVERY_DATE");
        if(deliveryDate == null) deliveryDate = "";
    
        deliveryDateDisplay = request.getParameter("deliveryDateDisplay");
        if(deliveryDateDisplay == null) deliveryDateDisplay = "";
        
        deliveryShippingMethod = request.getParameter("DELIVERY_SHIPPING_METHOD");
        if(deliveryShippingMethod == null) deliveryShippingMethod = "";
    
        carrier = request.getParameter("carrier");
        if(carrier == null) carrier = "";
    
        price = request.getParameter("price");    
        if(price == null) price = "";

        variablePrice = request.getParameter("variablePriceAmount");    
        if(variablePrice == null) variablePrice = "";

        color1 = request.getParameter("color1");    
        if(color1 == null) color1 = "";

        color2 = request.getParameter("color2");    
        if(color2 == null) color2 = "";

        baloon = request.getParameter("baloonCheckBox");   
        if(baloon == null) baloon = "";

        baloonQty = request.getParameter("baloonQty");    
        if(baloonQty == null) baloonQty = "";

        bear = request.getParameter("bearCheckBox");    
        if(bear == null) bear = "";

        bearQty = request.getParameter("bearQty");    
        if(bearQty == null) bearQty = "";

        chocolate = request.getParameter("chocolateCheckBox");    
        if(chocolate == null) chocolate = "";

        chocolateQty = request.getParameter("chocolateQty");
        if(chocolateQty == null) chocolateQty = "";

        banner = request.getParameter("funeralBannerCheckBox");
        if(banner == null) banner = "";

        bannerQty = request.getParameter("funeralBannerQty");
        if(bannerQty == null) bannerQty = "";

        card = request.getParameter("cards");
        if(card == null) card = "";

        subCodeId = request.getParameter("subCodeId");
        if(subCodeId == null) subCodeId = "";

        String secondChoiceAuth = request.getParameter("secondChoiceAuth");
        if(secondChoiceAuth == null) secondChoiceAuth = "";        

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("pagename", "Shopping");
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("productId", productId);
        ftdRequest.addArgument("CONTACT_ZIP", zip);
        ftdRequest.addArgument("DELIVERY_DATE", deliveryDate);
        ftdRequest.addArgument("deliveryDateDisplay", deliveryDateDisplay);
        ftdRequest.addArgument("DELIVERY_SHIPPING_METHOD", deliveryShippingMethod);
        ftdRequest.addArgument("CARRIER", carrier);
        ftdRequest.addArgument("PRICE", price);
        ftdRequest.addArgument("VARIABLE_PRICE_AMT", variablePrice);
        ftdRequest.addArgument("COLOR1", color1);
        ftdRequest.addArgument("COLOR2", color2); 
        ftdRequest.addArgument("balloon", baloon);
        ftdRequest.addArgument("balloonQty", baloonQty);
        ftdRequest.addArgument("bear", bear);
        ftdRequest.addArgument("bearQty", bearQty);
        ftdRequest.addArgument("chocolate", chocolate);
        ftdRequest.addArgument("chocolateQty", chocolateQty);
        ftdRequest.addArgument("banner", banner);
        ftdRequest.addArgument("bannerQty", bannerQty);
        ftdRequest.addArgument("card", card);
        ftdRequest.addArgument("subCodeId", subCodeId);
        ftdRequest.addArgument("cartItemNumber", itemNumber);
        ftdRequest.addArgument("secondChoiceAuth", secondChoiceAuth);
        
        ftdRequest.setServiceDescription("SHOPPING SERVICE");        

        FTDServiceResponse ftdResponse = null;

        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_ITEM_INFO");
        
        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);  
        }        
    }

    private void handleChangeSourceCode(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        String sourceCode = request.getParameter("sourceCode");
        if(sourceCode == null) sourceCode = "";

        FTDServiceResponse ftdResponse = null;

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand(CommandConstants.SHOPPING_UPDATE_SOURCE_CODE);
        ftdRequest.addArgument("sourceCode", sourceCode);

        try
        {        
            ftdResponse  = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);  
        }        
    }

}
