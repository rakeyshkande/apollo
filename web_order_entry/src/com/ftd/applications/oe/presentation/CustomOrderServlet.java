package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class CustomOrderServlet extends HttpServlet 
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId =  request.getParameter("persistentObjId");
        
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
            String cartItemNumber = request.getParameter("cartItemNumber");
            if(cartItemNumber == null) cartItemNumber = "";    
            String itemCartNumber = request.getParameter("itemCartNumber");
            if(itemCartNumber == null) itemCartNumber = "";
            
            // if we are coming from the delivery info breadcrumb
            // then redirect to the EditProductDetailServlet
            if(cartItemNumber.length() > 0)
            {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/EditItemDetailServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&command=getDetail&showBreadCrumbs=Y&custom=Y&itemCartNumber=" + cartItemNumber);
                if (dispatcher != null)
                    dispatcher.forward(request, response);
                    
                return;
            }      
    
          FTDServiceRequest ftdRequest = new FTDServiceRequest();
          ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
          ftdRequest.setPersistenceID(persistentObjId);
    
          ftdRequest.addArgument("pagename", "CustomOrder");
          ftdRequest.addArgument("url", url);
          ftdRequest.addArgument("company", "FDTP");
          ftdRequest.addArgument("productId", "6611");
          
          ftdRequest.setServiceDescription("SEARCH SERVICE");
          ftdRequest.setCommand("SEARCH_PRODUCT_BY_ID");
    
          FTDServiceResponse ftdResponse = null;

        ftdResponse  = util.executeService(ftdRequest);
        XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
        String xslFile = "/xsl/customOrder.xsl";
        xslFile = getServletContext().getRealPath(xslFile);
        HashMap params = new HashMap();

        params.put("persistentObjId", ftdResponse.getPersistanceID());
        params.put("sessionId", sessionId);
        params.put("cartItemNumber", itemCartNumber);
        
        params.put("actionServlet", (request.getContextPath() + "/servlet/CustomOrderServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("confirmation.xml", xml);
            }
      }
      catch(Exception e)
      {
          util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
      }  
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        String persistentObjId =  request.getParameter("persistentObjId");
      

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
    
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";

        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);  
            ftdRequest.addArgument(param, value);
        }

        String cartItemNumber = request.getParameter("cartItemNumber");
        if(cartItemNumber == null) cartItemNumber = "";
      ftdRequest.setServiceDescription("SHOPPING SERVICE");
        if(cartItemNumber.length() > 0)
        {
            ftdRequest.setCommand("SHOPPING_UPDATE_CUSTOM_ITEM_INFO");
        }
        else
        {
      ftdRequest.setCommand("SHOPPING_ADD_CUSTOM_ITEM");
        }    
 
      try
      {     
         util.executeService(ftdRequest);
      }
      catch(Exception e)
      {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
                
            return;
      }    


        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
          ftdRequest = new FTDServiceRequest();
          ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
          ftdRequest.setPersistenceID(persistentObjId);
          
          ftdRequest.addArgument("company", "FTDP");
          ftdRequest.addArgument("pagename", "DeliveryInfo");
          ftdRequest.addArgument("url", url);
          ftdRequest.addArgument("cartItemNumber", cartItemNumber);
    
          ftdRequest.setServiceDescription("SHOPPING SERVICE");
          ftdRequest.setCommand("SHOPPING_LOAD_DELIVERY_INFO");
    
          FTDServiceResponse ftdResponse = null;
      
        ftdResponse  = util.executeService(ftdRequest);
        XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
        String xslFile = "/xsl/delivery_info.xsl";
        xslFile = getServletContext().getRealPath(xslFile);
        HashMap params = new HashMap();

        params.put("persistentObjId", ftdResponse.getPersistanceID());
        params.put("sessionId", sessionId);
        params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryUpdateServlet"));
        
        params.put("breadCrumbType", "Custom");

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("deliveryinfo.xml", xml);
            }
      }
      catch(Exception e)
      {
            util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
      } 
    }

}  



  
