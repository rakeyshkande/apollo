package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class EditItemDetailServlet extends HttpServlet 
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);

        String command = request.getParameter("command");
        String custom = request.getParameter("custom");
        if(custom == null) custom = "";

        if(command == null) command = "";
        
        if(command.equals("getDetail")  && custom.equals("N"))
        {
            handleGetDetail(request, response, userInfo, sessionId);
        }
        else if(command.equals("getDetail")  && custom.equals("Y"))
        {
            handleGetCustomDetail(request, response, userInfo, sessionId);
        }        
        
    }

    private void handleGetDetail(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws ServletException, IOException
    {
        Util util = new Util();
        String persistentObjId =request.getParameter("persistentObjId");
        String itemCartNumber = request.getParameter("itemCartNumber");
        String custom = request.getParameter("custom");
        String upsellFlag = request.getParameter("upsellFlag");
        if(upsellFlag == null) upsellFlag = "";
        String showBreadCrumbs = request.getParameter("showBreadCrumbs");
        if(showBreadCrumbs == null) showBreadCrumbs = "";
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            if(itemCartNumber == null) itemCartNumber = "";
            if(custom == null) custom = "";
        
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("pagename", "ProductDTLFloral");
            ftdRequest.addArgument("url", url);
            ftdRequest.addArgument("showBreadCrumbs", showBreadCrumbs);
            ftdRequest.addArgument("cartItemNumber", itemCartNumber);
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_LOAD_ITEM_FROM_CART");
    
            FTDServiceResponse ftdResponse = null;
            String error = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
    
            String xslFile = "/xsl/productDetail.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("cartItemNumber", itemCartNumber);
            params.put("custom", custom); 
            params.put("upsellFlag", upsellFlag);  
            util.setImageSuffixParams(params, request.getParameter("companyId"));

            // if we are showing breadcrumbs then the actionServlet should be DeliveryServlet
            if(showBreadCrumbs.length() > 0)
            {
                params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));//"/servlet/XSLTestServlet"));
            }
            else
            {
            params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));//"/servlet/XSLTestServlet"));
            }

            params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
            params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
            params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("productDetail.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            try
            {
                if (dispatcher != null)
                    dispatcher.forward(request, response);
            }
            catch(IOException ioe)
            {
                util.getLogManager().error(ioe.toString());
            }
        }
    }

    private void handleGetCustomDetail(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws ServletException, IOException
    {
        Util util = new Util();
        String persistentObjId =request.getParameter("persistentObjId");
        String itemCartNumber = request.getParameter("itemCartNumber");
        String custom = request.getParameter("custom");
        String showBreadCrumbs = request.getParameter("showBreadCrumbs");
        if(showBreadCrumbs == null) showBreadCrumbs = "";        
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            if(itemCartNumber == null) itemCartNumber = "";
            if(custom == null) custom = "";
        
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("pagename", "ProductDTLFloral");
            ftdRequest.addArgument("url", url);
            ftdRequest.addArgument("showBreadCrumbs", showBreadCrumbs);
            ftdRequest.addArgument("cartItemNumber", itemCartNumber);
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_LOAD_ITEM_FROM_CART");
    
            FTDServiceResponse ftdResponse = null;
            String error = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
       
            String xslFile = "/xsl/customOrder.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("cartItemNumber", itemCartNumber);
            params.put("custom", custom);
            // if we are showing breadcrumbs then the actionServlet should be DeliveryServlet
            if(showBreadCrumbs.length() > 0)
            {
                params.put("actionServlet", (request.getContextPath() + "/servlet/CustomOrderServlet"));//"/servlet/XSLTestServlet"));
            }
            else
            {
            params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));//"/servlet/XSLTestServlet"));
            }
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("productDetail.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            try
            {
                if (dispatcher != null)
                    dispatcher.forward(request, response);
            }
            catch(IOException ioe)
            {
                util.getLogManager().error(ioe.toString());
            }
        }
    }

}