package com.ftd.applications.oe.presentation;

import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.framework.common.valueobjects.FTDServiceRequest;
import com.ftd.framework.common.valueobjects.FTDServiceResponse;
import com.ftd.framework.common.utilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.util.ServletHelper;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.xml.parser.v2.XMLDocument;


public class BillingServlet extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  ServletConfig config;

  public void init(ServletConfig config) throws ServletException {
    super.init(config);

    this.config = config;
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);

    if (sessionId == null) {
      return;
    }

    Util util = new Util();
    util.recordTask(request, response);

    String persistentObjId = request.getParameter("persistentObjId");

    response.setHeader("Cache-Control", "no-cache");

    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.addArgument("company", "FTDP");
    ftdRequest.addArgument("pagename", "Billing");

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_LOAD_BILLING_INFO");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);

      XMLDocument xml = (XMLDocument) ftdResponse.getSystemValueObject().getXML();

      String xslFile = "/xsl/billing_payment.xsl";
      xslFile = getServletContext().getRealPath(xslFile);

      HashMap params = new HashMap();
      params.put("persistentObjId", persistentObjId);
      params.put("sessionId", sessionId);
      params.put("actionServlet", (request.getContextPath() + "/servlet/CreditCardServlet"));
      
      // Defect 1240 enhancement
      ConfigurationUtil cu = ConfigurationUtil.getInstance();
      String siteName = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.name");
      String sslPort = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.sslport");
      
      params.put("siteName", siteName);
      params.put("siteNameSsl", ServletHelper.switchServerPort(siteName, sslPort));
      params.put("applicationContext", request.getContextPath());
            
      util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, true);

      if (config.getInitParameter("WriteToXML").equalsIgnoreCase("true")) {
        util.writeXMLtoFile("billing.xml", xml);
      }
    } catch (Throwable e) {
      e.printStackTrace();
      util.getLogManager().error("this is where it hapenned ... get execute service " + e.toString(), e);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }
    }
  }
}
