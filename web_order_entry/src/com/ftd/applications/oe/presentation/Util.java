package com.ftd.applications.oe.presentation;

import java.io.*;

import java.util.*;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import oracle.xml.parser.v2.*;

import com.ftd.framework.common.utilities.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.businessservices.serviceprovider.*;

import javax.servlet.http.*;
import javax.servlet.*;

import org.w3c.dom.*;

import com.ftd.security.SecurityManager;
import com.ftd.security.util.ServletHelper;
import com.ftd.applications.oe.common.*;
import com.ftd.security.cache.vo.UserInfo;

public class Util
{
    private final static boolean DEBUG = false;
    private final static boolean RECORD_TASKS = false;
    private final static boolean CLIENT_SIDE_TRANSFORM = false;
    private final static String PATH = "/orderentryxmls/";
    /**
     * changed
     */
    private static HashMap cache = new HashMap();

    private LogManager lm = null;
    private LogManager lm2 = null;

    public Util()
    {
        lm = new LogManager("com.ftd.oe");
        lm2 = new LogManager("com.ftd.applications.oe.presentation.Util");
    }

    public LogManager getLogManager()
    {
        return lm;
    }

    public LogManager getLogManager2()
    {
        return lm2;
    }

    public void transform(XMLDocument source, String style, String clear, HttpServletRequest request, HttpServletResponse response, HashMap parameters) throws Exception
    {
        transform(source, style, clear, request, response, parameters, true);
    }

    public void transform(XMLDocument source, String style, String clear, HttpServletRequest request, HttpServletResponse response, HashMap parameters, boolean transformOnClient) throws Exception
    {
        if (clear!=null && clear.equals("yes"))
        {
            clearCache();
        }

        try
        {
            apply(style, source, request, response, parameters, transformOnClient);
        }
        catch (TransformerException err)
        {
            lm.error(err.getMessage());
        }

    }

    /**
     * changed
     */
    private void apply(String style, XMLDocument doc,  HttpServletRequest req, HttpServletResponse res, HashMap params, boolean transformOnClient) throws TransformerException, java.io.IOException {

        ServletOutputStream out = res.getOutputStream();
        try
        {
            if (doc==null) 
            {
                    lm.error("********** DOC PASSED INTO Util.apply IS NULL ***********");
            }
            
            if (out==null) 
            {
                    lm.error("********** OUT PASSED INTO Util.apply IS NULL ************");
            }
            
            //Transformer transformer = pss.newTransformer();
            //Transformer transformer = tryCache(style);
            //Properties details = pss.getOutputProperties();
            //res.setContentType("text/html");

            if(params != null)
            {
            /*
              String paramName = null;
              for(Iterator keyIterator = (params.keySet()).iterator(); keyIterator.hasNext();)
              {
                paramName = (String)keyIterator.next();
                transformer.setParameter(paramName, (String)(params.get(paramName)));
              }
            */
                doc = insertParameters(doc, params);
            }
            //transformer.transform(new StreamSource(new StringReader(source)), new StreamResult(out));
            //transformer.transform(new DOMSource(doc), new StreamResult(out));

            String userAgent = req.getHeader("USER-AGENT");
            StringTokenizer st = new StringTokenizer(userAgent, ";");
            boolean clientSideTransformation = false ;
            //while (st.hasMoreTokens())
            //{
            //    if (st.nextToken().trim().equalsIgnoreCase("MSIE 6.0"))
            //    {
            //        //System.out.println("The browser version is MSIE 6.0");
            //        clientSideTransformation = true;
            //        break;
            //    }
            //}

            if (clientSideTransformation && CLIENT_SIDE_TRANSFORM && transformOnClient)
            {
                //System.out.println("Performing client side transformation");
                // client side transformation
                String xslName = null;
                String fileSep = System.getProperty("file.separator");
                st = new StringTokenizer(style, fileSep);
                while (st.hasMoreTokens())
                {
                    xslName = st.nextToken();
                }

                StringBuffer xslLocation = new StringBuffer();
                xslLocation.append("http://");
                xslLocation.append(req.getServerName());
                xslLocation.append(":");
                xslLocation.append(req.getServerPort());
                xslLocation.append(req.getContextPath());
                xslLocation.append("/xsl/");
                xslLocation.append(xslName);

                ProcessingInstruction pi =
                    doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + xslLocation.toString() + "\"");
                Node importedNode = doc.importNode( pi, true);
                doc.insertBefore(importedNode, doc.getDocumentElement());
                res.setContentType("text/xml");
                //doc.print(res.getWriter());
                doc.print(out);
            }
            else
            {
                // server side transformation
                //System.out.println("Performing server side transformation");
                res.setContentType("text/html");
                Transformer transformer = tryCache(style);

                //String docStr;
                //StringWriter stw = new StringWriter();
                //PrintWriter pw = new PrintWriter(stw);

                //doc.print(pw);

                //docStr = stw.toString();
                
                transformer.transform(new DOMSource(doc), new StreamResult(out));
                //transformer.transform(new StreamSource(new StringReader(docStr)), new StreamResult(out));
            }
        }
        catch (Exception err)
        {
            out.println(err.getMessage());
            lm.error(err.toString(), err);
        }

    }

    /**
     * changed, note the method is not synchronized, instead it uses a sync code block
     */
    private  Transformer tryCache(String url) throws TransformerException, Exception
    {
/*
        Templates x = (Templates)cache.get(url);
        if (x==null)
        {
            TransformerFactory factory = TransformerFactory.newInstance();
            x = factory.newTemplates(new StreamSource(new File(url)));
            cache.put(url, x);
        }
        return x;
*/
        // Search the cache for the templates entry
        TemplatesCacheEntry templatesCacheEntry = (TemplatesCacheEntry) cache.get(url);
        File file = null;
        // If entry found
        if (templatesCacheEntry != null)
        {
          // Check timestamp of modification
          if (templatesCacheEntry.lastModified
            < templatesCacheEntry.templatesFile.lastModified())
            templatesCacheEntry = null;
        }
        // If no templatesEntry is found or this entry was obsolete
        if (templatesCacheEntry == null)
        {
          file = new File(url);
          lm.debug("Loading transformation [" + file.getAbsolutePath() + "].");
          // If this file does not exists, throw the exception
          if (!file.exists())
          {
            throw new Exception(
              "Requested transformation ["
              + file.getAbsolutePath()
              + "] does not exist.");
          }

          TransformerFactory factory = TransformerFactory.newInstance();


          // Create new cache entry
          templatesCacheEntry =
            new TemplatesCacheEntry(factory.newTemplates(new StreamSource(file)), file);

          synchronized(this){
            // Save this entry to the cache
            cache.put(file.getAbsolutePath(), templatesCacheEntry);
          }
        }
        else
        {
          lm.debug("Using cached transformation [" + url + "].");
        }

        return templatesCacheEntry.templates.newTransformer();

    }

    /**
     * changed
     */
    private synchronized void clearCache()
    {
        cache.clear();
    }



    public  void writeXMLtoFile(String strFileName, XMLDocument xmlDoc) throws Exception
    {
        if (DEBUG)
        {
            try
            {
                // Check for home directory
                String homeDir = System.getProperty("user.home");

                // make sure orderentryxmls exists
                // If not create it
                File file = new File(homeDir + PATH);
                if(!file.exists())
                {
                    file.mkdir();
                }

                String resultXML = null;
                StringWriter s = new StringWriter();
                xmlDoc.print(new PrintWriter(s));
                resultXML = s.toString();
                FileWriter fv = new FileWriter(homeDir + PATH + strFileName);
                fv.write(resultXML);
                fv.close();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }


    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    public static String authenticate(HttpServletRequest request, HttpServletResponse response, String securityTokenNameParam) throws ServletException, IOException
    {
      boolean securityTokenValid = false;
      //get securuty token from request object
      ResourceManager rm = ResourceManager.getInstance();

      String securityTokenName = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, securityTokenNameParam);
      String securityToken = request.getParameter(securityTokenName);
      String securitySwitch =  rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "security.switch");

      if(securitySwitch!= null && securitySwitch.equalsIgnoreCase("off"))
      {
        return "SECURITYOFF";
      }
      
      //check if security token is valid or not
      try {
        SecurityManager securityManager = SecurityManager.getInstance();
        String context = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, GeneralConstants.SECURITY_CONTEXT_OPS);
        String unitID = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, GeneralConstants.SECURITY_UNIT_ID);
        if(securityToken != null 
        && !securityToken.trim().equals("") 
        && securityManager.authenticateSecurityToken(context, unitID, securityToken)) {
          securityTokenValid = true;
        }
      } catch(Exception e) {
        e.printStackTrace();
      }

        String tokenParam = "?securitytoken=" + securityToken;

      //if security token is not valid ...
      if (!securityTokenValid) {
        //forward to the error page
        // String errorPage = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, GeneralConstants.SECURITY_ERROR_PAGE);
        // errorPage = errorPage + tokenParam;
        // response.sendRedirect(errorPage);
        
        // Defect 907: PCI compliance. Forward to the login page.
        ServletHelper.redirectToLogin(request, response, securityToken);
        
        //returning null signals that the calling servlet should return immediately (beacuse we forwarded to error page)
        securityToken = null;
      }

      //return the security token to the calling servlet repost it 
      return securityToken;
    }
    
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    public static UserInfo getUserInfo(String securityToken) 
    {
      UserInfo userInfo = null;
      try {
        SecurityManager securityManager = SecurityManager.getInstance();
        userInfo = securityManager.getUserInfo(securityToken);
      } catch(Exception e) {
        e.printStackTrace();
      }
      return userInfo;
    }
    
    public FTDServiceResponse executeService(FTDServiceRequest request) throws Exception
	  {
        ServiceManager serviceManager = ServiceManager.getInstance();
        long start = System.currentTimeMillis();
		    FTDServiceResponse response = null;
		    IService service = null;

		try
		{
			service = serviceManager.getService(request.getServiceDescription());
            if(service == null)
            {
                lm.error("Cant get handle on service");
            }
            else
            {
                lm.info("Executing " + service.getClass().getName() + " of type" + service.getDescription());
                lm.info("Command was: " + request.getCommand().getCommand());
                response = service.executeCommand(request);

                try
                {
                    // If the response has a null value then try to dump out the parameters
                    // and command for debugging the error
                    if(response.getSystemValueObject() == null || response.getSystemValueObject().getXML() == null)
                    {
                        lm.info("The service returned a null FTDSystem VO");
                        lm.info("Command was: " + request.getCommand().getCommand());
                        if (lm2.isDebugEnabled()) {
                        FTDArguments args = request.getArguments();
                        Set keys = args.keySet();
                        Iterator it = keys.iterator();
                        while(it.hasNext())
                        {
                            String key = (String)it.next();
                            // JP Puzon 12-28-2005
                            // IF condition added for PCI Compliance.
                            if (!key.equalsIgnoreCase("creditCardNumber")) {
                            lm2.debug(key + " = " + args.get(key));
                            }
                        }
                        }
                    }
                }
                catch(Exception ex)
                {
                    lm.error("Could not print all arguments from the Command: " + request.getCommand().getCommand());
                }
            }
		}
		catch(Exception e)
		{
			service.unlock();
			throw new Exception("Unable to execute request : "+ e.toString());
		}
		long end = System.currentTimeMillis();

        lm.info("Total server processing time is: " + (end - start));
		return response;
	}

    public void recordTask(HttpServletRequest req, HttpServletResponse res)
    {
        if(RECORD_TASKS == true)
        {
            try
            {
                StringBuffer task = new StringBuffer();
                String clearMods = "true";
                if(!req.getParameterMap().containsKey("sessionId"))
                {
                    clearMods = "false";
                }
                String sep = System.getProperty("line.separator");
                task.append("<task name=\"").append(req.getServletPath()).append("\" order=\"\" class=\"com.ftd.applications.jmpmeter.tasks.JMPMeterHTTPRequestTask\" clearMods=\"");
                task.append(clearMods).append("\">");
                task.append(sep);
                Enumeration names = req.getParameterNames();
                while(names.hasMoreElements())
                {
                    String name = (String)names.nextElement();
                    String value = req.getParameter(name);

                    if(!name.equalsIgnoreCase("sessionId") && !name.equalsIgnoreCase("persistentobjId"))
                    {
                        task.append("<parameter name=\"");
                        task.append(name);
                        task.append("\" value=\"");
                        task.append(value);
                        task.append("\"/>");
                        task.append(sep);
                    }
                }

                task.append("<property name=\"serverIp\" value=\"").append(req.getServerName()).append("\"/>");
                task.append(sep);
                task.append("<property name=\"serverPort\" value=\"").append(req.getServerPort()).append("\"/>");
                task.append(sep);
                task.append("<property name=\"protocol\" value=\"").append(req.getProtocol()).append("\"/>");
                task.append(sep);
                task.append("<property name=\"path\" value=\"").append(req.getContextPath()).append(req.getServletPath()).append("\"/>");
                task.append(sep);
                task.append("<property name=\"method\" value=\"").append(req.getMethod()).append("\"/>");
                task.append(sep);
                task.append("</task>");
                task.append(sep);
                task.append(sep);
                FileWriter fv = new FileWriter("/temp/taskFile.txt", true);
                fv.write(task.toString());
                fv.close();
            }
            catch (Exception e)
            {
                System.out.println("Could not record task " + e.toString());
            }
        }
    }
  /**
   * Private class to hold templates cache entry.
   */
  private class TemplatesCacheEntry
  {
    /** When was the cached entry last modified. */
    private long lastModified;

    /** Cached templates object. */
    private Templates templates;

    /** Templates file object. */
    private File templatesFile;

    /**
     * Constructs a new cache entry.
     * @param templates templates to cache.
     * @param templatesFile file, from which this transformer was loaded.
     */
    private TemplatesCacheEntry(final Templates templates, final File templatesFile)
    {
      this.templates = templates;
      this.templatesFile = templatesFile;
      this.lastModified = templatesFile.lastModified();
    }
  }

  /**
   * Inserts the specified parameters into the XML document
   *
   * @param doc the XML document
   * @param parameters the collection of parameters
   * @return the updated XML document
   */
    private XMLDocument insertParameters(XMLDocument doc, Map parameters){
        if (doc == null)  {
            return doc;
        }
        if (parameters == null)  {
            return doc;
        }

        Node parametersNode = null, parameterNode = null;
        String key = null, value = null;
        try  {
          parametersNode = doc.createElement("parameters");

          for (Iterator i = parameters.keySet().iterator(); i.hasNext() ;)  {
            key = (String)i.next();
            value = (String) parameters.get(key);
            parameterNode = doc.createElement(key);
            parameterNode.appendChild( doc.createTextNode(value) );
            // append to the parameters node
            parametersNode.appendChild(parameterNode);
          }
          // append the parameters node to the root node
          doc.getDocumentElement().appendChild(parametersNode);

        } catch (Exception ex)  {
          ex.printStackTrace();
        } finally  {

        }

    return doc;

  }

  public void determineCompanyIdAndSetImageSuffixParams(HashMap params, XMLDocument xml)
  {  
    try {
      NodeList nodeList = xml.getElementsByTagName("ORDER");
      Element orderElement = (Element) nodeList.item(0);
      String companyId = orderElement.getAttribute("lastOrderAction");
      setImageSuffixParams(params, companyId);
      } catch(Exception e) {
        getLogManager().error(e.toString(), e);
     }
  }

    public void setImageSuffixParams(HashMap params, String companyId)
    {
      try {
        if (companyId == null) {companyId = "";}
        params.put("companyId", companyId);
        params.put("imageSuffixSmall", getImageSuffix(companyId, "1"));
        params.put("imageSuffixMedium", getImageSuffix(companyId, "2"));
        params.put("imageSuffixLarge", getImageSuffix(companyId, "3"));      
        params.put("addonImageSuffixSmall", getImageSuffix("ADDON", "1"));
        params.put("addonImageSuffixMedium", getImageSuffix("ADDON", "2"));
        params.put("addonImageSuffixLarge", getImageSuffix("ADDON", "3"));      
      } catch(Exception e) {
        getLogManager().error(e.toString(), e);
     }
    }
    
    public String getImageSuffix(String companyId, String size) 
    {
      String imageSuffix  = "";
      String propertyName = "";
      try {
        if (companyId == null || companyId.equals("")) 
        {
          companyId = "FTDP";
        }
        
        ResourceManager rm = ResourceManager.getInstance();
        propertyName = GeneralConstants.IMAGE_NAME_SUFFIX + size + "." + companyId;
        imageSuffix = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, propertyName);
      } catch(Exception e) {
        getLogManager().error("Attempting to get property:" + propertyName);
        getLogManager().error(e.toString(), e);
     }
    
      return imageSuffix;
    }
}
