package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class ProductDetailServlet extends HttpServlet 
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        response.setHeader("Cache-Control", "no-cache");
        Util util = new Util();
        util.recordTask(request, response);
        String command = request.getParameter("command");
        if(command == null) command = "";


        if(command.equals("updateZip"))
        {
            handleUpdateDeliveryDates(request, response, userInfo, sessionId);
        }
        else
        {
            handleDefaultAction(request, response, userInfo, sessionId);
        }
    }

    private void handleUpdateDeliveryDates(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws IOException, ServletException  
    {
        Util util = new Util();
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
		String persistentObjId =request.getParameter("persistentObjId");
        String productid = request.getParameter("productid");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
        String url = callCenterUrl;
        url = url + request.getServletPath() + "?" + request.getQueryString();

        String zip = "";
        String productId = "";
        String deliveryDate = "";
        String deliveryShippingMethod = "";
        String carrier = "";
        String price = "";
        String variablePrice = "";
        String color1 = "";
        String color2 = "";
        String baloon = "";
        String baloonQty = "";
        String bear = "";
        String bearQty = "";  
        String chocolate = "";
        String chocolateQty = "";  
        String banner = "";
        String bannerQty = "";
        String card = "";
        String subCodeId = "";

        persistentObjId =  request.getParameter("persistentObjId");
        if(persistentObjId == null) persistentObjId = "";
    
        zip = request.getParameter("zip");
        if(zip == null) zip = "";
    
        productId = request.getParameter("productId");
        if(productId  == null) productId = "";
    
        deliveryDate = request.getParameter("DELIVERY_DATE");
        if(deliveryDate == null) deliveryDate = "";
    
        deliveryShippingMethod = request.getParameter("DELIVERY_SHIPPING_METHOD");
        if(deliveryShippingMethod == null) deliveryShippingMethod = "";
        
        carrier = request.getParameter("carrier");
        if(carrier == null) carrier = "";
    
        price = request.getParameter("price");    
        if(price == null) price = "";

        variablePrice = request.getParameter("variablePriceAmount");    
        if(variablePrice == null) variablePrice = "";

        color1 = request.getParameter("color1");    
        if(color1 == null) color1 = "";

        color2 = request.getParameter("color2");    
        if(color2 == null) color2 = "";

        baloon = request.getParameter("baloonCheckBox");   
        if(baloon == null) baloon = "";

        baloonQty = request.getParameter("baloonQty");    
        if(baloonQty == null) baloonQty = "";

        bear = request.getParameter("bearCheckBox");    
        if(bear == null) bear = "";

        bearQty = request.getParameter("bearQty");    
        if(bearQty == null) bearQty = "";

        chocolate = request.getParameter("chocolateCheckBox");    
        if(chocolate == null) chocolate = "";

        chocolateQty = request.getParameter("chocolateQty");
        if(chocolateQty == null) chocolateQty = "";

        banner = request.getParameter("funeralBannerCheckBox");
        if(banner == null) banner = "";

        bannerQty = request.getParameter("funeralBannerQty");
        if(bannerQty == null) bannerQty = "";

        card = request.getParameter("cards");
        if(card == null) card = "";

        subCodeId = request.getParameter("subCodeId");
        if(subCodeId == null) subCodeId = "";

        String searchType = request.getParameter("searchType");
        if(searchType == null) searchType = "";     

        String upsellFlag = request.getParameter("upsellFlag");
        if(upsellFlag == null) searchType = ""; 

        String secondChoiceAuth = request.getParameter("secondChoiceAuth");
        if(secondChoiceAuth == null) secondChoiceAuth = "";

        ftdRequest.addArgument("pagename", "Shopping");
        ftdRequest.addArgument("company", "FTDP");
      ftdRequest.addArgument("productId", productId);
      ftdRequest.addArgument("CONTACT_ZIP", zip);
      ftdRequest.addArgument("DELIVERY_DATE", deliveryDate);
      ftdRequest.addArgument("DELIVERY_SHIPPING_METHOD", deliveryShippingMethod);
      ftdRequest.addArgument("CARRIER", carrier);
      ftdRequest.addArgument("PRICE", price);
      ftdRequest.addArgument("VARIABLE_PRICE_AMT", variablePrice);
      ftdRequest.addArgument("COLOR1", color1);
      ftdRequest.addArgument("COLOR2", color2); 
      ftdRequest.addArgument("balloon", baloon);
      ftdRequest.addArgument("balloonQty", baloonQty);
      ftdRequest.addArgument("bear", bear);
      ftdRequest.addArgument("bearQty", bearQty);
      ftdRequest.addArgument("chocolate", chocolate);
      ftdRequest.addArgument("chocolateQty", chocolateQty);
      ftdRequest.addArgument("banner", banner);
      ftdRequest.addArgument("bannerQty", bannerQty);
      ftdRequest.addArgument("card", card);
      ftdRequest.addArgument("subCodeId", subCodeId);
      ftdRequest.addArgument("upsellFlag", upsellFlag);
      ftdRequest.addArgument("secondChoiceAuth", secondChoiceAuth);
    
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
      
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "ProductDTLFloral");
        ftdRequest.addArgument("url", url);
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_ITEM_INFO");

        FTDServiceResponse ftdResponse = null;
        String error = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/productDetail.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();

            String focusElement = request.getParameter("focusElement");
            if(focusElement == null) focusElement = "";
            params.put("focusElement", focusElement);
            
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("searchType", searchType);           
            params.put("upsellFlag", upsellFlag);
            params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));
            params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
            params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
            params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));

            params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
            params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
            params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

            util.setImageSuffixParams(params, request.getParameter("companyId"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("productDetail.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);
          
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }
    }

    private void handleDefaultAction(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws IOException, ServletException
    {
        Util util = new Util();
        String searchType = request.getParameter("searchType");
        if(searchType == null) searchType = "";    
        String upsellFlag = request.getParameter("upsellFlag");
        if(upsellFlag == null) upsellFlag = "";    
        String persistentObjId =request.getParameter("persistentObjId");
        String productid = request.getParameter("productid");
        if (productid == null) {
          // This kludge is to get around the productid vs productId mess
          productid = request.getParameter("productId");
        }
        String cartItemNumber = request.getParameter("cartItemNumber");
        if(cartItemNumber == null) cartItemNumber = "";
        String addWithSameRecpFlag = request.getParameter("addWithSameRecpFlag");
        if(addWithSameRecpFlag == null) addWithSameRecpFlag = "";
        String upsellCrumb = request.getParameter("upsellCrumb");
        if(upsellCrumb == null) upsellCrumb = "";
        String soonerOverride = request.getParameter("soonerOverride");
        if(soonerOverride == null) soonerOverride = "";
        
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
        
            // if we are coming from the delivery info breadcrumb
            // then redirect to the EditProductDetailServlet
            if(cartItemNumber.length() > 0 && !addWithSameRecpFlag.equals("Y"))
            {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/EditItemDetailServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&command=getDetail&showBreadCrumbs=Y&custom=N&itemCartNumber=" + cartItemNumber);
                if (dispatcher != null)
                    dispatcher.forward(request, response);
            
                return;
            }
        
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("pagename", "ProductDTLFloral");
            ftdRequest.addArgument("url", url);
            ftdRequest.addArgument("productId", productid);
            ftdRequest.addArgument("soonerOverride", soonerOverride);
            ftdRequest.setServiceDescription("SEARCH SERVICE");
            ftdRequest.setCommand("SEARCH_PRODUCT_BY_ID");
    
            FTDServiceResponse ftdResponse = null;
            String error = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();

            if(ftdRequest.getArguments() != null && ftdRequest.getArguments().containsKey(ArgumentConstants.UPSELL_EXISTS)) 
            {
                String xslFile = "/xsl/upsell.xsl";

                if(ftdRequest.getArguments().containsKey(ArgumentConstants.UPSELL_SKIP) 
                    && !((String) ftdRequest.getArguments().get(ArgumentConstants.UPSELL_SKIP)).equals("N")
                    && !upsellCrumb.equals("Y"))
                {
                    ftdRequest.addArgument("productId", (String) ftdRequest.getArguments().get(ArgumentConstants.UPSELL_SKIP));
                    ftdResponse  = util.executeService(ftdRequest);
                    xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
                    
                    xslFile = "/xsl/productDetail.xsl";
                }

                xslFile = getServletContext().getRealPath(xslFile);
                HashMap params = new HashMap();
                params.put("persistentObjId", ftdResponse.getPersistanceID());
                params.put("sessionId", sessionId);
                params.put("cartItemNumber", cartItemNumber);
                params.put("actionServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));//"/servlet/XSLTestServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("searchType", searchType);  
                params.put("upsellCrumb", upsellCrumb);
                params.put("soonerOverride", soonerOverride);

                util.setImageSuffixParams(params, request.getParameter("companyId"));
                util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
                if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
                {
                    util.writeXMLtoFile("productdetail.xml", xml);
                }
            }
            else 
            {
                String xslFile = "/xsl/productDetail.xsl";
                xslFile = getServletContext().getRealPath(xslFile);
                HashMap params = new HashMap();
                params.put("persistentObjId", ftdResponse.getPersistanceID());
                params.put("sessionId", sessionId);
                params.put("cartItemNumber", cartItemNumber);
                params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));//"/servlet/XSLTestServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("searchType", searchType);  
                params.put("upsellFlag", upsellFlag);  
                params.put("soonerOverride", soonerOverride);

                params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
                params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
                params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

                util.setImageSuffixParams(params, request.getParameter("companyId"));
                util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
                if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
                {
                    util.writeXMLtoFile("productdetail.xml", xml);
                }
            }
            
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }
    }

}