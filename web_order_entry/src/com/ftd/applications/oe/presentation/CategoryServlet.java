package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class CategoryServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    ServletConfig config;
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
      
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId = request.getParameter("persistentObjId");    
        String country = request.getParameter("countries");
        if(country == null) country = "";
        String zipCode = request.getParameter("zipCode");
        if(zipCode == null) zipCode = "";
        String deliveryDate = request.getParameter("deliveryDates");
        if(deliveryDate == null) deliveryDate = "";
        String deliveryDateDisplay = request.getParameter("deliveryDateDisplay");
        if(deliveryDateDisplay == null) deliveryDateDisplay = "";
        String occasion = request.getParameter("occasions");
        if(occasion == null) occasion = "";
        String occasionDescription = request.getParameter("occasionDescription");
        if(occasionDescription == null) occasionDescription = "";
        String city = request.getParameter("city");
        if(city == null) city = "";
        String state = request.getParameter("state");
        if(state == null) state = "";
        String command = request.getParameter("command");
        if(command == null) command = "";
        String itemCartNumber = request.getParameter("itemCartNumber");
        if(itemCartNumber == null) itemCartNumber = "";
        String recipientId = request.getParameter("recipientId");
        if(recipientId == null) recipientId = "";
        String recipientFirstName = request.getParameter("recipientFirstName");
        if(recipientFirstName == null) recipientFirstName = "";
        String recipientLastName = request.getParameter("recipientLastName");
        if(recipientLastName == null) recipientLastName = "";
        String recipientAddress1 = request.getParameter("recipientAddress1");
        if(recipientAddress1 == null) recipientAddress1 = "";
        String recipientAddress2 = request.getParameter("recipientAddress2");
        if(recipientAddress2 == null) recipientAddress2 = "";
        String recipientPhone = request.getParameter("recipientPhone");
        if(recipientPhone == null) recipientPhone = "";
        String recipientPhoneExt = request.getParameter("recipientPhoneExt");
        if(recipientPhoneExt == null) recipientPhoneExt = "";
        String recipientAddressType = request.getParameter("recipientAddressType");
        if(recipientAddressType == null) recipientAddressType = "";
        String recipientBhfInfo = request.getParameter("recipientBhfInfo");
        if(recipientBhfInfo == null) recipientBhfInfo = "";
        String recipientBhfName = request.getParameter("recipientBhfName");
        if(recipientBhfName == null) recipientBhfName = "";

        try
        {
    
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
    
            // Get the user's call center URL
            StringBuffer callCenterUrlKey = new StringBuffer();
            callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
            String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
            if(callCenterUrl == null)
            {
                // get default url
                callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
            }
            
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
     
            response.setHeader("Cache-Control", "no-cache");
        
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
    
            ftdRequest.addArgument("flag", "D");
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("occasion", occasion );
            ftdRequest.addArgument("country", country);
            ftdRequest.addArgument("zipcode", zipCode);
            ftdRequest.addArgument("pagename", "Shopping");
            ftdRequest.addArgument("deliveryDate", deliveryDate);
            ftdRequest.addArgument("deliveryDateDisplay", deliveryDateDisplay);
            ftdRequest.addArgument("occasionDescription", occasionDescription);
            ftdRequest.addArgument("url", url);
            ftdRequest.addArgument("CONTACT_CITY", city);
            ftdRequest.addArgument("CONTACT_STATE", state);
            ftdRequest.addArgument("command", command);
            ftdRequest.addArgument("itemCartNumber", itemCartNumber);
    
            //Wipe out old search results in there is nothing in the recipient phone
            if( recipientPhone.length()==0 && recipientId.length()>0 )
              recipientId="";
    
            //Did a recipient search        
            ftdRequest.addArgument("recipientId", recipientId);
            ftdRequest.addArgument("phone", recipientPhone);
            if( recipientId.length()>0 ) 
            {
              ftdRequest.addArgument("recipientId", recipientId);
              ftdRequest.addArgument("firstName", recipientFirstName);
              ftdRequest.addArgument("lastName", recipientLastName);
              ftdRequest.addArgument("address1", recipientAddress1);
              ftdRequest.addArgument("address2", recipientAddress2); 
              ftdRequest.addArgument("phoneExt",recipientPhoneExt);
              ftdRequest.addArgument("bhfName", recipientBhfName);
              ftdRequest.addArgument("addressType", recipientAddressType); 
              ftdRequest.addArgument("bhfInfo",recipientBhfInfo);
            }
            
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_SET_OCCASION");
    
            FTDServiceResponse ftdResponse = null;
            String error = null;
    
                ftdResponse  = util.executeService(ftdRequest);
                XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
                String xslFile = "/xsl/shopping.xsl";
                xslFile = getServletContext().getRealPath(xslFile);
                HashMap params = new HashMap();
          
          params.put("persistentObjId", persistentObjId);
          params.put("sessionId", sessionId);
          params.put("actionServlet", (request.getContextPath() + "/servlet/OccasionServlet"));
          params.put("occasion", occasionDescription);
          params.put("occasionDescription", occasionDescription);     
          params.put("itemCartNumber", itemCartNumber);
          params.put("productListServlet",  (request.getContextPath() + "/servlet/ProductListServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("shopping.xml", xml);
            }
        }
    catch(Exception e)
    {
        util.getLogManager().error(e.toString(), e);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
        if (dispatcher != null)
            dispatcher.forward(request, response);
        return;
    }
}

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

}  
