package com.ftd.applications.oe.presentation;


import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.framework.common.utilities.FrameworkConstants;
import com.ftd.framework.common.utilities.ResourceManager;

import com.ftd.framework.common.valueobjects.FTDServiceRequest;
import com.ftd.framework.common.valueobjects.FTDServiceResponse;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import oracle.xml.parser.v2.XMLDocument;

public class DNISServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    ServletConfig config;
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
    
        Util util = new Util();
        util.recordTask(request, response);
            
        response.setHeader("Cache-Control", "no-cache");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.CREATE_OBJECT);
        ftdRequest.addArgument("COMPANY", "FTD");
        ftdRequest.addArgument("PAGE_NAME", "DNIS");
        ftdRequest.setServiceDescription("SEARCH SERVICE");
        ftdRequest.setCommand("SEARCH_GET_DNIS_SCRIPT");

        FTDServiceResponse ftdResponse = null;
        String error = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/homepage.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);            
            params.put("actionServlet", (request.getContextPath() + "/servlet/IntroductionServlet"));

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            ResourceManager rm = ResourceManager.getInstance();
            
            //get menu page from property file
            String menuPage  = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "security.menu.page");
            String adminAction = null;

            //First get admin action and put into session (if one was found in request)
            adminAction = request.getParameter("adminAction");
            if(adminAction !=null && adminAction.length() >0){       
                request.getSession().setAttribute("adminAction",adminAction);
            }

            //Second use the adminAction in the request to pass to the page
            adminAction = (String)request.getSession().getAttribute("adminAction");
            if(adminAction == null || adminAction.length() <= 0)
            {
              //admin action was not found..get default action from property file
              adminAction = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "security.action.default");              
            }

            //this is used to aid debugging            
            if(menuPage == null)
            {
              menuPage = "<null menu page>";
            }

            String securityContext = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, GeneralConstants.SECURITY_CONTEXT_OPS);        
            if(securityContext == null)
            {
              securityContext = "<null context>";
            }

            params.put("context",securityContext);
            params.put("menupage",menuPage);



            //ResourceManager rm = ResourceManager.getInstance();
            //String pdbURL = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "pdb.url");

            
            params.put("pdbServlet", (request.getContextPath() + "/servlet/AdminServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("dnis.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

}