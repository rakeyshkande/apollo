package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class OccasionServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    ServletConfig config;
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        response.setHeader("Cache-Control", "no-cache");
        Util util = new Util();
        util.recordTask(request, response);


        String source = request.getParameter("source");
        try
        {
            if(source.equals("introduction"))
            {
                handleIntroductionRequest(request, response, userInfo, sessionId);
            }
            else if(source.equals("occasion"))
            {
                handleOccasionRequest(request, response, userInfo, sessionId);
            }
            else
            {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
                if (dispatcher != null)
                    dispatcher.forward(request, response);
            }
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }
    
    private void handleIntroductionRequest(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
    
        String sourceCode = null;
        String selbytype = request.getParameter("selection");

        if(selbytype.equals("bysourcecode"))
        {
            sourceCode = request.getParameter("sourceText");
        }
        else
        {
            sourceCode = request.getParameter("sourceSelect");  
        }
        String phoneNumber = request.getParameter("homePhone");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String persistentObjId = request.getParameter("persistentObjId");
        String customerId = request.getParameter("customerId");
        String crumbFlag = request.getParameter("crumbFlag");
        String yellowPagesCode = request.getParameter("yellowPageBookCode");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
        
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
            ftdRequest.addArgument("SOURCE_CODE", sourceCode);
            ftdRequest.addArgument("CONTACT_HOME_PHONE", phoneNumber);
            ftdRequest.addArgument("CONTACT_FIRST_NAME", firstName);
            ftdRequest.addArgument("CONTACT_LAST_NAME", lastName);
            ftdRequest.addArgument("customerId", customerId);
            ftdRequest.addArgument("yellowPagesCode", yellowPagesCode);
            ftdRequest.addArgument("crumbFlag", crumbFlag);
            ftdRequest.addArgument("CUSTOMER_EXISTS_FLAG", "false");
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("pagename", "PrelimOrderInfo");
            ftdRequest.addArgument("url", url);
          
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_SET_INTRODUCTION");
    
            FTDServiceResponse ftdResponse = null;
            String error = null;

            ftdResponse  = util.executeService(ftdRequest);
        
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/occasion.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/CategoryServlet"));
            params.put("country", "United States");
            params.put("zipCode", "");
            params.put("occasion", "");
            params.put("deliverydate", "");  
            params.put("refreshTarget", (request.getContextPath() + "/servlet/OccasionServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("occasion.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }      
    }

    private void handleOccasionRequest(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId =request.getParameter("persistentObjId");
        String countryid=request.getParameter("country");
        String city = request.getParameter("city");
        if(city == null) city = "";
        String state = request.getParameter("state");
        if(state == null) state = "";
        String zip = request.getParameter("zip");
        String delivery = request.getParameter("deliverydate");
        String deliveryDisplay = request.getParameter("deliveryDateDisplay");
        if(deliveryDisplay == null) deliveryDisplay = "";
        String occasion = request.getParameter("occasion");
        String flag = request.getParameter("countryType");
        String country = request.getParameter("country");
        String focusElement = request.getParameter("focusElement");
        if(focusElement == null) focusElement = "";
        String updzip = request.getParameter("updzip");
        if(updzip == null) updzip = "Y";
        String maxDeliveryDaysOut = request.getParameter("MAXDeliveryDaysOut");
        if(maxDeliveryDaysOut == null) maxDeliveryDaysOut = "false";

        // Recipient info
        String recipientId = request.getParameter("recipientId");
        
        if(recipientId == null) recipientId = "";
        String recipientFirstName = request.getParameter("recipientFirstName");
        if(recipientFirstName == null) recipientFirstName = "";
        String recipientLastName = request.getParameter("recipientLastName");
        if(recipientLastName == null) recipientLastName = "";
        String recipientAddress1 = request.getParameter("recipientAddress1");
        if(recipientAddress1 == null) recipientAddress1 = "";
        String recipientAddress2 = request.getParameter("recipientAddress2");
        if(recipientAddress2 == null) recipientAddress2 = "";
        String recipientPhone = request.getParameter("recipientPhone");
        if(recipientPhone == null) recipientPhone = "";
        String recipientPhoneExt = request.getParameter("recipientPhoneExt");
        if(recipientPhoneExt == null) recipientPhoneExt = "";
        String recipientAddressType = request.getParameter("recipientAddressType");
        if(recipientAddressType == null) recipientAddressType = "";
        String recipientBhfInfo = request.getParameter("recipientBhfInfo");
        if(recipientBhfInfo == null) recipientBhfInfo = "";
        String recipientBhfName = request.getParameter("recipientBhfName");
        if(recipientBhfName == null) recipientBhfName = "";
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("occasion", occasion);
            ftdRequest.addArgument("flag", flag);
            ftdRequest.addArgument("zipcode", zip);
            ftdRequest.addArgument("pagename", "PrelimOrderInfo");
            ftdRequest.addArgument("deliveryDate", delivery);
            ftdRequest.addArgument("deliveryDateDisplay", deliveryDisplay);
            ftdRequest.addArgument("countryCode", countryid);
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("url", url);
            ftdRequest.addArgument("updzip", updzip);
            ftdRequest.addArgument("maxDeliveryDaysOut", maxDeliveryDaysOut);
          
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_UPDATE_OCCASION");
    
            FTDServiceResponse ftdResponse = null;
            String error = null;
    
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/occasion.xsl";
            xslFile = getServletContext().getRealPath(xslFile);

            HashMap params = new HashMap();
        
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/CategoryServlet"));
            params.put("country", country);
            params.put("city", city);
            params.put("state", state);
            params.put("zipCode", zip);
            params.put("countryid", countryid);
            params.put("occasion", occasion);
            params.put("deliverydate", "");
            params.put("focusElement", focusElement);
            params.put("recipientId", recipientId);
            params.put("recipientPhone", recipientPhone);
            params.put("recipientPhoneExt", recipientPhoneExt);
            params.put("recipientFirstName", recipientFirstName);
            params.put("recipientLastName", recipientLastName);
            params.put("recipientAddress1", recipientAddress1);
            params.put("recipientAddress2", recipientAddress2);
            params.put("recipientAddressType", recipientAddressType);
            params.put("recipientBhfInfo", recipientBhfInfo);
            params.put("recipientBhfName", recipientBhfName);
            params.put("refreshTarget", (request.getContextPath() + "/servlet/OccasionServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("occasion1.xml", xml);
            }

      }
      catch(Exception e)
      {
        util.getLogManager().error(e.toString(), e);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
        if (dispatcher != null)
            dispatcher.forward(request, response);  
      }    
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

} 