package com.ftd.applications.oe.presentation;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.applications.oe.common.*;

public class AddItemServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
    
        Util util = new Util();
		String persistentObjId =request.getParameter("persistentObjId");
        String actionServlet = request.getParameter("actionServlet");
        if(actionServlet == null) actionServlet = "";
        String productid = request.getParameter("productid");
        if(productid == null) productid = "";

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_CLEAR_ORDER_CUSTOMER");
        FTDServiceResponse ftdResponse = null;
        
        try
        {
            ftdResponse  = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
            {
                dispatcher.forward(request, response);
            }
        } 
        
    
        RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/" + actionServlet + "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&productid=" + productid);
        if (dispatcher != null)
        {
                dispatcher.forward(request, response);    
        }
    }
}