package com.ftd.applications.oe.presentation;

import com.ftd.applications.oe.common.ArgumentConstants;
import com.ftd.applications.oe.common.CommandConstants;
import com.ftd.framework.common.valueobjects.FTDServiceRequest;
import com.ftd.framework.common.valueobjects.FTDServiceResponse;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.xml.parser.v2.XMLDocument;

public class ValidationServlet extends HttpServlet 
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        processValidationRequest( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        processValidationRequest( request, response );
    }

    private void processValidationRequest( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        Util util = new Util();
        util.recordTask(request, response);
        Enumeration requestFields = request.getParameterNames();

        PrintWriter pw = new PrintWriter(response.getOutputStream());

        pw.println("<HTML><BODY>");
        pw.println("<SCRIPT>");
        pw.println("document.write('<INPUT TYPE=\"HIDDEN\" NAME=\"FINISHEDLOADING\" VALUE=\"YES\"/>');");
        pw.println("</SCRIPT>");
        pw.println("Hello world!");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.NONE);
        
        String requestField = "";
        while (requestFields.hasMoreElements())
        {
            requestField = (String) requestFields.nextElement();          
            ftdRequest.addArgument(requestField, request.getParameter(requestField));

            // JP Puzon 12-28-2005
            // IF condition added for PCI Compliance.
            if (util.getLogManager2().isDebugEnabled() && !requestField.equalsIgnoreCase(ArgumentConstants.CREDIT_CARD_NUMBER)) {
            util.getLogManager2().debug("|" + requestField + "|    |" + request.getParameter(requestField) + "|");   
            }
        }
         
        try
        {      
            ftdRequest.setCommand(CommandConstants.VALIDATE_PAGE);
            ftdRequest.setServiceDescription("VALIDATION SERVICE");
            
           
            FTDServiceResponse ftdResponse = util.executeService(ftdRequest);

            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/validationiframe.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            
            HashMap params = new HashMap();
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("validation.xml", xml);
            }  
        }
        catch ( Exception e )
        {
            util.getLogManager().error("get execute service" + e.toString(), e);
        }
    }
}