package com.ftd.applications.oe.presentation;

import com.ftd.applications.oe.common.GeneralConstants;
import com.ftd.framework.common.valueobjects.FTDServiceRequest;
import com.ftd.framework.common.valueobjects.FTDServiceResponse;
import com.ftd.framework.common.utilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.util.ServletHelper;

import java.io.IOException;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.xml.parser.v2.XMLDocument;


public class CreditCardServlet extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  ServletConfig config;

  public void init(ServletConfig config) throws ServletException {
    super.init(config);

    this.config = config;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);

    if (sessionId == null) {
      return;
    }

    Util util = new Util();
    util.recordTask(request, response);

    String forwardToAction = request.getParameter("forwardToAction");
    String persistentObjId = request.getParameter("persistentObjId");
    response.setHeader("Cache-Control", "no-cache");
    this.executeUpdateBillingInfo(request, response, util, persistentObjId);

    if (forwardToAction.equalsIgnoreCase("SHOPPING_CART")) {
      this.executeForwardtoShoppingCart(request, response, util, persistentObjId, sessionId);
    } else {
      this.executeForwardtoCreditCard(request, response, util, persistentObjId, sessionId);
    }
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
  }

  private void executeUpdateBillingInfo(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    Enumeration enum1 = request.getParameterNames();
    String param = "";
    String value = "";

    while (enum1.hasMoreElements()) {
      param = (String) enum1.nextElement();
      value = request.getParameter(param);
      ftdRequest.addArgument(param, value);
    }

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_UPDATE_BILLING_INFO");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);
    } catch (Exception e) {
      e.printStackTrace();

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }

      return;
    }
  }

  private void executeForwardtoCreditCard(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId, String sessionId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.addArgument("company", "FTDP");
    ftdRequest.addArgument("pagename", "Billing");

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_LOAD_CREDIT_CARD_INFO");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);

      XMLDocument xml = (XMLDocument) ftdResponse.getSystemValueObject().getXML();

      String xslFile = "/xsl/creditCardPayment.xsl";
      xslFile = getServletContext().getRealPath(xslFile);

      HashMap params = new HashMap();
      params.put("persistentObjId", persistentObjId);
      params.put("sessionId", sessionId);
      params.put("actionServlet", (request.getContextPath() + "/servlet/ConfirmationServlet"));
      
      // Defect 1240 enhancement
      ConfigurationUtil cu = ConfigurationUtil.getInstance();
      String siteName = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.name");
      String sslPort = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.sslport");
      
      params.put("siteName", siteName);
      params.put("siteNameSsl", ServletHelper.switchServerPort(siteName, sslPort));
      params.put("applicationContext", request.getContextPath());

      util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, true);

      if (config.getInitParameter("WriteToXML").equalsIgnoreCase("true")) {
        util.writeXMLtoFile("creditCardPayment.xml", xml);
      }
    } catch (Exception e) {
      e.printStackTrace();
      util.getLogManager().error("this is where it hapenned ... get execute service " + e.toString(), e);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }
    }
  }

  private void executeForwardtoShoppingCart(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId, String sessionId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    Enumeration enum1 = request.getParameterNames();
    String param = "";
    String value = "";

    while (enum1.hasMoreElements()) {
      param = (String) enum1.nextElement();
      value = request.getParameter(param);
      ftdRequest.addArgument(param, value);
    }

    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.addArgument("company", "FTDP");
    ftdRequest.addArgument("pagename", "Billing");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdRequest.setServiceDescription("SHOPPING SERVICE");
      ftdRequest.setCommand("SHOPPING_GET_SHOPPING_CART");
      ftdResponse = util.executeService(ftdRequest);

      XMLDocument xml = (XMLDocument) ftdResponse.getSystemValueObject().getXML();

      String xslFile = "/xsl/shoppingCart.xsl";
      xslFile = getServletContext().getRealPath(xslFile);

      HashMap params = new HashMap();
      params.put("persistentObjId", persistentObjId);
      params.put("sessionId", sessionId);
      params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));
      
      util.determineCompanyIdAndSetImageSuffixParams(params, xml);
      util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);

      if (config.getInitParameter("WriteToXML").equalsIgnoreCase("true")) {
        util.writeXMLtoFile("shoppingCart.xml", xml);
      }
    } catch (Exception e) {
      util.getLogManager().error("this is where it hapenned ... get execute service " + e.toString(), e);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }
    }
  }
}
