package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class DNISChangeServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        response.setHeader("Cache-Control", "no-cache");
        Util util = new Util();
        util.recordTask(request, response);
   
        String source = request.getParameter("source");
        
        try
        {
            if(source.equals("occasionJCP"))
            {
                handleOccasionJCP(request, response, userInfo, sessionId);
            }
            else if(source.equals("productDetailJCP"))
            {
                handleProductDetailJCP(request, response, userInfo, sessionId);
            }
             else if(source.equals("deliveryJCP"))
            {
                handleDeliveryJCP(request, response, userInfo, sessionId);
            }
              else if(source.equals("billingJCP"))
            {
                handleBillingJCP(request, response, sessionId);
            }
      
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }
    }

    private void handleOccasionJCP(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId =  request.getParameter("persistentObjId");

        // Update DNIS and source code
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("dnis", "6551");
      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_DNIS");

        FTDServiceResponse ftdResponse = null;
    
        try
        {     
            ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }  

        // Reload occasion page with new values
        String countryid=request.getParameter("country"); 
        String zip = request.getParameter("zip");
		String delivery = request.getParameter("deliverydate");
        String occasion = request.getParameter("occasion");
        String flag = request.getParameter("countryType");
        String country = request.getParameter("country");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("occasion", occasion);
            ftdRequest.addArgument("flag", flag);
            ftdRequest.addArgument("zipcode", zip);
            ftdRequest.addArgument("pagename", "PrelimOrderInfo");
            ftdRequest.addArgument("deliveryDate", delivery);
            ftdRequest.addArgument("countryCode", countryid);
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("url", url);
          
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_UPDATE_OCCASION");
    
            ftdResponse = null;
            String error = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/occasion.xsl";
            xslFile = getServletContext().getRealPath(xslFile);

            HashMap params = new HashMap();
        
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/CategoryServlet"));
            params.put("country", country);
            params.put("zipCode", zip);
            params.put("countryid", countryid);
            params.put("occasion", occasion);
            params.put("deliverydate", "");
            params.put("refreshTarget", (request.getContextPath() + "/servlet/OccasionServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("occasion1.xml", xml);
            }
            
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }     
    }

    private void handleProductDetailJCP(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        
        // Update DNIS and source code
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("dnis", "6551");
      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_DNIS");

        FTDServiceResponse ftdResponse = null;
    
        try
        {     
            ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }  

        // Update and reload the product detail
        ftdRequest = new FTDServiceRequest();
		persistentObjId =request.getParameter("persistentObjId");
        String productid = request.getParameter("productid");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            String zip = "";
            String productId = "";
            String deliveryDate = "";
            String deliveryShippingMethod = "";
            String carrier = "";
            String price = "";
            String variablePrice = "";
            String color1 = "";
            String color2 = "";
            String baloon = "";
            String baloonQty = "";
            String bear = "";
            String bearQty = "";  
            String chocolate = "";
            String chocolateQty = "";  
            String banner = "";
            String bannerQty = "";
            String card = "";
            String subCodeId = "";
    
            persistentObjId =  request.getParameter("persistentObjId");
            if(persistentObjId == null) persistentObjId = "";
        
            zip = request.getParameter("zip");
            if(zip == null) zip = "";
        
            productId = request.getParameter("productId");
            if(productId  == null) productId = "";
        
            deliveryDate = request.getParameter("DELIVERY_DATE");
            if(deliveryDate == null) deliveryDate = "";
        
            deliveryShippingMethod = request.getParameter("DELIVERY_SHIPPING_METHOD");
            if(deliveryShippingMethod == null) deliveryShippingMethod = "";
            
            carrier = request.getParameter("carrier");
            if(carrier == null) carrier = "";
        
            price = request.getParameter("price");    
            if(price == null) price = "";
    
            variablePrice = request.getParameter("variablePriceAmount");    
            if(variablePrice == null) variablePrice = "";
    
            color1 = request.getParameter("color1");    
            if(color1 == null) color1 = "";
    
            color2 = request.getParameter("color2");    
            if(color2 == null) color2 = "";
    
            baloon = request.getParameter("baloonCheckBox");   
            if(baloon == null) baloon = "";
    
            baloonQty = request.getParameter("baloonQty");    
            if(baloonQty == null) baloonQty = "";
    
            bear = request.getParameter("bearCheckBox");    
            if(bear == null) bear = "";
    
            bearQty = request.getParameter("bearQty");    
            if(bearQty == null) bearQty = "";
    
            chocolate = request.getParameter("chocolateCheckBox");    
            if(chocolate == null) chocolate = "";
    
            chocolateQty = request.getParameter("chocolateQty");
            if(chocolateQty == null) chocolateQty = "";
    
            banner = request.getParameter("funeralBannerCheckBox");
            if(banner == null) banner = "";
    
            bannerQty = request.getParameter("funeralBannerQty");
            if(bannerQty == null) bannerQty = "";
    
            card = request.getParameter("cards");
            if(card == null) card = "";
    
            subCodeId = request.getParameter("subCodeId");
            if(subCodeId == null) subCodeId = "";
    
            ftdRequest.addArgument("pagename", "Shopping");
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("productId", productId);
            ftdRequest.addArgument("CONTACT_ZIP", zip);
            ftdRequest.addArgument("DELIVERY_DATE", deliveryDate);
            ftdRequest.addArgument("DELIVERY_SHIPPING_METHOD", deliveryShippingMethod);
            ftdRequest.addArgument("CARRIER", carrier);
            ftdRequest.addArgument("PRICE", price);
            ftdRequest.addArgument("VARIABLE_PRICE_AMT", variablePrice);
            ftdRequest.addArgument("COLOR1", color1);
            ftdRequest.addArgument("COLOR2", color2); 
            ftdRequest.addArgument("balloon", baloon);
            ftdRequest.addArgument("balloonQty", baloonQty);
            ftdRequest.addArgument("bear", bear);
            ftdRequest.addArgument("bearQty", bearQty);
            ftdRequest.addArgument("chocolate", chocolate);
            ftdRequest.addArgument("chocolateQty", chocolateQty);
            ftdRequest.addArgument("banner", banner);
            ftdRequest.addArgument("bannerQty", bannerQty);
            ftdRequest.addArgument("card", card);
            ftdRequest.addArgument("subCodeId", subCodeId);
        
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("pagename", "ProductDTLFloral");
            ftdRequest.addArgument("url", url);
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_UPDATE_ITEM_INFO");
    
            ftdResponse = null;
            String error = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/productDetail.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));
            params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
            params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
            params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));

            params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
            params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
            params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

            // JCP is FTDP
            util.setImageSuffixParams(params, "FTDP");
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("productDetail.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service " + e.toString(), e);
          
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }
    }

    private void handleDeliveryJCP(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId =  request.getParameter("persistentObjId");

        // Update DNIS and source code
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("dnis", "6551");
      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_DNIS");

        FTDServiceResponse ftdResponse = null;
    
        try
        {     
            ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }  


        // Update and refresh the delivery information page
        ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            ftdRequest.addArgument(param, value);
        }
        
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_DELIVERY_INFO");

        try
        {     
            ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            util.getLogManager().error("Exception in executing serlvet.Specific: " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }    

        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
          
            ftdRequest.addArgument("company", "FTDP");
            ftdRequest.addArgument("pagename", "DeliveryInfo");
            ftdRequest.addArgument("url", url);
    
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setCommand("SHOPPING_LOAD_DELIVERY_INFO");

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/delivery_info.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();

            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryUpdateServlet"));
            params.put("breadCrumbType", "D");

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("deliveryUpdate.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error("get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }
    }

    private void handleBillingJCP(HttpServletRequest request, HttpServletResponse response, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        
        // Update DNIS and source code
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        ftdRequest.addArgument("dnis", "6551");
      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_UPDATE_DNIS");

        FTDServiceResponse ftdResponse = null;
    
        try
        {     
            ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }  

        // Update and refesh billing page
        ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);  
            ftdRequest.addArgument(param, value);
        }
      
        ftdRequest.setServiceDescription("BILLING SERVICE");
        ftdRequest.setCommand("BILLING_UPDATE_BILLING_INFO");

        try
        {     
            ftdResponse = util.executeService(ftdRequest);
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }  


        ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
    
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "Billing");

        ftdRequest.setServiceDescription("BILLING SERVICE");
        ftdRequest.setCommand("BILLING_LOAD_BILLING_INFO");
   
        try
        {
            ftdResponse  = util.executeService(ftdRequest);     
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();

            String xslFile = "/xsl/billing_payment.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            
            HashMap params = new HashMap();
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/ConfirmationServlet"));
            
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("billing.xml", xml);
            }
        }
        catch(Exception e)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);

            return;
        }
    }

}
