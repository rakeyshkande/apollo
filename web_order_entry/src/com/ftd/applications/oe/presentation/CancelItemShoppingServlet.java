package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class CancelItemShoppingServlet extends HttpServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
    
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId = request.getParameter("persistentObjId");
        String occasionDescription = request.getParameter("occasionDescription");
        String itemCartNumber = request.getParameter("itemCartNumber");
        if(itemCartNumber == null) itemCartNumber = "";
        String zipCode = request.getParameter("zipCode");
        if(zipCode == null) zipCode = "";        
        //String fromPageName = request.getParameter("fromPageName");
        //if(fromPageName == null) fromPageName = "";

         try
         {
        
            ConfigurationUtil cu = ConfigurationUtil.getInstance();

            // Get the user's call center URL
            StringBuffer callCenterUrlKey = new StringBuffer();
            callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
            String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
            if(callCenterUrl == null)
            {
                // get default url
                callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
            }
            
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
    
            response.setHeader("Cache-Control", "no-cache");
    
            FTDServiceResponse ftdResponse = null;

            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setServiceDescription("SHOPPING SERVICE");
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
            ftdRequest.addArgument("flag", "D");
            ftdRequest.addArgument("company", "FTDP");
//            ftdRequest.addArgument("occasion", occasion );
//            ftdRequest.addArgument("country", country);
            ftdRequest.addArgument("zipCode", zipCode);
            ftdRequest.addArgument("pagename", "Shopping");
//            ftdRequest.addArgument("deliveryDate", deliveryDate);
//            ftdRequest.addArgument("occasionDescription", occasionDescription);
            ftdRequest.addArgument("url", url);
            ftdRequest.setCommand("SHOPPING_CANCEL_ITEM_TO_SHOPPING");
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/shopping.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/OccasionServlet"));
            //params.put("occasion", occasionDescription);
            //params.put("occasionDescription", occasionDescription);      
            params.put("productListServlet",  (request.getContextPath() + "/servlet/ProductListServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, true);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("shopping.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }
    }

}
