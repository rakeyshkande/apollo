package com.ftd.applications.oe.presentation;


import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import oracle.xml.parser.v2.*;
import org.w3c.dom.*;
import com.ftd.applications.oe.common.*;

public class LoginServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }
    
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
       //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
       doPost(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_OPS);
    if(sessionId == null) return;
 
    Util util = new Util();
    util.recordTask(request, response);

    try
    {
            String xslFile = "/xsl/loginsuccessful.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            XMLDocument document = new XMLDocument();
            Element root = (Element) document.createElement("root");
            root.setAttribute("type", "login");
            document.appendChild(root);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/DNISServlet"));
            
            util.transform(document, xslFile, config.getInitParameter("ClearCache"), request, response, params);
    }
    catch(Exception ex)
    {
        util.getLogManager().error(("this is where it happened ... get execute service " + ex.toString()), ex);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/loginerror.htm");
        if (dispatcher != null)
            dispatcher.forward(request, response);
    }    
  }
}