package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.FieldUtils;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class ProductListServlet extends HttpServlet 
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId = request.getParameter("persistentObjId");
        
        String categoryindex = request.getParameter("categoryindex");
        String pageNumber = request.getParameter("pagenumber");
        String sortType = request.getParameter("sortType");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
            String pricePointId = request.getParameter("pricepointid");
            String occasionDescription = request.getParameter("occasionDescription");
    
            String itemCartNumber = request.getParameter("itemCartNumber");
            if(itemCartNumber == null) itemCartNumber = "";
            
            String keywords = request.getParameter("keywords");
            if(occasionDescription == null) occasionDescription = "";
          
            if(keywords == null) keywords = "";
        
            // Advanced search params
            String search = request.getParameter("search");
            if(search == null) search = "";
            String name = request.getParameter("name");
            if(name == null) name = "";
            String color = request.getParameter("color");
            if(color == null) color = "";
            String recipient = request.getParameter("recipient");
            if(recipient == null) recipient = "";
            if(categoryindex == null) categoryindex = "";
            if(sortType == null) sortType = "";
            String advSearchType = request.getParameter("advSearchType");
            if(advSearchType == null) advSearchType = "";
            // End advanced search params
           
            response.setHeader("Cache-Control", "no-cache");
        
            FTDServiceRequest ftdRequest = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
    
            if(sortType.length() > 0 ) ftdRequest.addArgument("sortType", sortType);
    
            if(search.equals("advancedproductsearch"))
            {
                if(name.length() > 0 ) ftdRequest.addArgument("name", name);
                if(color.length() > 0 ) ftdRequest.addArgument("color", color);
                if(recipient.length() > 0 ) ftdRequest.addArgument("recipient", recipient);
                if(categoryindex.length() > 0 ) ftdRequest.addArgument("categoryindex", categoryindex);
                if(occasionDescription.length() > 0 ) ftdRequest.addArgument("occasion", occasionDescription);            
                if(pricePointId != null && pricePointId.length() > 0 ) ftdRequest.addArgument("pricePointId", pricePointId);
                ftdRequest.addArgument("advSearchType", advSearchType);            
                ftdRequest.addArgument("pageNumber", pageNumber);
                ftdRequest.addArgument("url", url);
                ftdRequest.setCommand("SEARCH_PRODUCTS_BY_VALUE");
            }
            else if(search.equals("category"))
            {
                ftdRequest.addArgument("indexId", categoryindex);
                ftdRequest.addArgument("pricePointId", pricePointId);
                ftdRequest.addArgument("pageNumber", pageNumber);
                ftdRequest.addArgument("url", url);
                ftdRequest.setCommand("SEARCH_PRODUCTS_BY_CATEGORY");            
            }
            else if(search.equals("keywordsearch"))
            {
                // if we have keywords then always do a keyword search    
                ftdRequest.addArgument("searchKeyword", keywords);
                ftdRequest.addArgument("pageNumber", pageNumber);
                ftdRequest.addArgument("pricePointId", pricePointId);
                ftdRequest.addArgument("url", url);
               
                ftdRequest.setCommand("SEARCH_PRODUCTS_BY_KEYWORD");            
            }
    
            ftdRequest.setServiceDescription("SEARCH SERVICE");
      
            FTDServiceResponse ftdResponse = null;
            String error = null;
            if(categoryindex == null) categoryindex = "";

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/productListIndex.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("intlflag", "D");
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("sessionId", sessionId);
            params.put("indexid", categoryindex);
            params.put("occasion", occasionDescription);
            params.put("name", name);
            params.put("color", color);
            params.put("recipient", recipient);
            params.put("search", search);
            params.put("itemCartNumber", itemCartNumber);
            util.setImageSuffixParams(params, request.getParameter("companyId"));
    
            params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
            params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
            params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

            // Escape any apostophies
            if(keywords == null) keywords = "";
            keywords = FieldUtils.replaceAll(keywords, "'", "&apos;");            
      
            params.put("keywords", keywords);
            params.put("sortType", sortType);
      
            params.put("productListServlet", (request.getContextPath() + "/servlet/ProductListServlet"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("productList.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }

}