package com.ftd.applications.oe.presentation;

import com.ftd.framework.common.valueobjects.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.ftd.applications.oe.common.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;

public class LogoutServlet extends HttpServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
        Util util = new Util();
        util.recordTask(request, response);

        String persistentObjId = request.getParameter("persistentObjId");
        
        // Remove the persistant object        
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.REMOVE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_LOGOUT");
   
        FTDServiceResponse ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);     
        }
        catch(Exception e)
        {
            util.getLogManager().error("this is where it hapenned ... get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }
        
        // Log the user out
        com.ftd.framework.security.SecurityManager security = com.ftd.framework.security.SecurityManager.getInstance();
        try
        {
            security.logout(sessionId);
            // remove the default session
            request.getSession().invalidate();
            //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String loginPage  = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, GeneralConstants.SECURITY_LOGIN_PAGE);
            response.sendRedirect(loginPage);
        }
        catch(Throwable ex)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }

    }
}