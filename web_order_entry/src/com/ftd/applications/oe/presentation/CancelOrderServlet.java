package com.ftd.applications.oe.presentation;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.applications.oe.common.*;

public class CancelOrderServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      
        Util util = new Util();
        util.recordTask(request, response);
    
        String persistentObjId = request.getParameter("persistentObjId");
    
        response.setHeader("Cache-Control", "no-cache");

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_CANCEL_ORDER");
   
        FTDServiceResponse ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);     
        }
        catch(Exception e)
        {
            util.getLogManager().error("this is where it hapenned ... get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }

        ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.REMOVE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("EMPTY_REMOVAL");
   
        ftdResponse = null;

        try
        {
            ftdResponse  = util.executeService(ftdRequest);     
        }
        catch(Exception e)
        {
            util.getLogManager().error("this is where it hapenned ... get execute service" + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
            return;
        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/DNISServlet?sessionId=" + sessionId);
        if (dispatcher != null)
            dispatcher.forward(request, response);
    }

}  