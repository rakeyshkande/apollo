package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;

public class CancelItemServlet extends HttpServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    if(sessionId == null) return;
    
        Util util = new Util();
        util.recordTask(request, response);
        String persistentObjId = request.getParameter("persistentObjId");
        String country = request.getParameter("country");
        if(country == null) country = "";
        String itemCartNumber = request.getParameter("itemCartNumber");
        if(itemCartNumber == null) itemCartNumber = "";
        String fromPageName = request.getParameter("fromPageName");
        if(fromPageName == null) fromPageName = "";

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();


    response.setHeader("Cache-Control", "no-cache");

    FTDServiceResponse ftdResponse = null;
        
    try
    {
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.addArgument("CUSTOMER_EXISTS_FLAG", "false");
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "PrelimOrderInfo");
        ftdRequest.addArgument("country", country);
        ftdRequest.addArgument("itemCartNumber", itemCartNumber);
        ftdRequest.addArgument("fromPageName", fromPageName);
        ftdRequest.setCommand("SHOPPING_CANCEL_ITEM");
        ftdResponse  = util.executeService(ftdRequest);
        XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
        String xslFile = "/xsl/occasion.xsl";
        xslFile = getServletContext().getRealPath(xslFile);
        
        params.put("actionServlet", (request.getContextPath() + "/servlet/CategoryServlet"));
        params.put("zipCode", "");
        params.put("occasion", "");
        params.put("deliverydate", "");
        params.put("persistentObjId", persistentObjId);
        params.put("sessionId", sessionId);
        params.put("refreshTarget", (request.getContextPath() + "/servlet/OccasionServlet"));

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, true);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("occasion.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);
        }
    }

}
