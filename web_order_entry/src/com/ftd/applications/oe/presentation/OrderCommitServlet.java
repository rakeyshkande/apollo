package com.ftd.applications.oe.presentation;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.applications.oe.common.*;

public class OrderCommitServlet extends HttpServlet
{
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
        Util util = new Util();
        util.recordTask(request, response);

    String persistentObjId = request.getParameter("persistentObjId");

    response.setHeader("Cache-Control", "no-cache");

/*
	JMP 4/4/03
	Commented out because this was causing the persistent object to be
	added to the addon server stack twice.


    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.REMOVE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.setServiceDescription("BILLING SERVICE");
    try
    {
        ftdRequest.setCommand("EMPTY_COMMAND");

        util.executeService(ftdRequest);
    }
    catch(Exception e)
    {
        util.getLogManager().error(e.toString(), e);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
        if (dispatcher != null)
            dispatcher.forward(request, response);
    }
*/
    RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/DNISServlet?sessionId=" + sessionId);
    if (dispatcher != null)
        dispatcher.forward(request, response);
  }

}