package com.ftd.applications.oe.presentation;

import com.ftd.applications.oe.common.GeneralConstants;

import com.ftd.framework.common.valueobjects.FTDServiceRequest;
import com.ftd.framework.common.valueobjects.FTDServiceResponse;
import com.ftd.framework.common.utilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.util.ServletHelper;

import oracle.xml.parser.v2.XMLDocument;

import java.io.IOException;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ConfirmationServlet extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  private ServletConfig config;

  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    this.config = config;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
    String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
    String forwardToAction = request.getParameter("forwardToAction");

    if (sessionId == null) {
      return;
    }

    Util util = new Util();
    util.recordTask(request, response);

    String persistentObjId = request.getParameter("persistentObjId");

    this.executeUpdateCreditCardInfo(request, response, util, persistentObjId);

    /* if the user hit the back button, we return to the biling screen.
     * Otherwise, we go to the confirmation page */
    if (forwardToAction.equalsIgnoreCase("BILLING_SCREEN")) {
      this.executeForwardtoBillingScreen(request, response, util, persistentObjId, sessionId);
    } else if (forwardToAction.equalsIgnoreCase("SHOPPING_CART")) {
      this.executeForwardtoShoppingCart(request, response, util, persistentObjId, sessionId);
    } else {
      this.executeCompleteOrderItems(request, response, util, persistentObjId);
      this.executeCommitOrder(request, response, util, persistentObjId);
      this.executeForwardtoConfirmationScreen(request, response, util, persistentObjId, sessionId);
    }
  }

  private void executeForwardtoBillingScreen(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId, String sessionId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.addArgument("company", "FTDP");
    ftdRequest.addArgument("pagename", "Billing");

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_LOAD_BILLING_INFO");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);

      XMLDocument xml = (XMLDocument) ftdResponse.getSystemValueObject().getXML();

      String xslFile = "/xsl/billing_payment.xsl";
      xslFile = getServletContext().getRealPath(xslFile);

      HashMap params = new HashMap();
      params.put("persistentObjId", persistentObjId);
      params.put("sessionId", sessionId);
      params.put("actionServlet", (request.getContextPath() + "/servlet/CreditCardServlet"));
      
      // Defect 1240 enhancement
      ConfigurationUtil cu = ConfigurationUtil.getInstance();
      String siteName = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.name");
      String sslPort = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.sslport");
      
      params.put("siteName", siteName);
      params.put("siteNameSsl", ServletHelper.switchServerPort(siteName, sslPort));
      params.put("applicationContext", request.getContextPath());
      
      util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, true);

      if (config.getInitParameter("WriteToXML").equalsIgnoreCase("true")) {
        util.writeXMLtoFile("billing.xml", xml);
      }
    } catch (Throwable e) {
      e.printStackTrace();
      util.getLogManager().error("this is where it hapenned ... get execute service " + e.toString(), e);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }
    }
  }

  private void executeUpdateCreditCardInfo(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    Enumeration enum1 = request.getParameterNames();
    String param = "";
    String value = "";

    while (enum1.hasMoreElements()) {
      param = (String) enum1.nextElement();
      value = request.getParameter(param);
      ftdRequest.addArgument(param, value);
    }

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("UPDATE_CREDIT_CARD_INFO");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);
    } catch (Exception e) {
      e.printStackTrace();

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }

      return;
    }
  }

  private void executeForwardtoConfirmationScreen(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId, String sessionId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.addArgument("company", "FTDP");
    ftdRequest.addArgument("pagename", "Confirmation");

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_LOAD_CONFIRMATION_INFO");

    try {
      FTDServiceResponse ftdResponse = util.executeService(ftdRequest);
      XMLDocument xml = (XMLDocument) ftdResponse.getSystemValueObject().getXML();
      String xslFile = "/xsl/confirmOrder.xsl";
      xslFile = getServletContext().getRealPath(xslFile);

      HashMap params = new HashMap();

      params.put("persistentObjId", ftdResponse.getPersistanceID());
      params.put("sessionId", sessionId);
      params.put("actionServlet", (request.getContextPath() + "/servlet/OrderCommitServlet"));
      
      // Defect 1240 enhancement
       ConfigurationUtil cu = ConfigurationUtil.getInstance();
       String siteName = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.site.name");
      
      params.put("siteName", siteName);
      params.put("applicationContext", request.getContextPath());

      util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);

      if (config.getInitParameter("WriteToXML").equalsIgnoreCase("true")) {
        util.writeXMLtoFile("confirmation.xml", xml);
      }
    } catch (Exception e) {
      e.printStackTrace();
      util.getLogManager().error("get execute service " + e.toString(), e);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }

      return;
    }
  }

  private void executeCompleteOrderItems(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_COMPLETE_ORDER_ITEMS");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);
    } catch (Exception e) {
      e.printStackTrace();

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }

      return;
    }
  }

  private void executeForwardtoShoppingCart(HttpServletRequest request, HttpServletResponse response, Util util,
    String persistentObjId, String sessionId) throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    Enumeration enum1 = request.getParameterNames();
    String param = "";
    String value = "";

    while (enum1.hasMoreElements()) {
      param = (String) enum1.nextElement();
      value = request.getParameter(param);
      ftdRequest.addArgument(param, value);
    }

    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.addArgument("company", "FTDP");
    ftdRequest.addArgument("pagename", "Billing");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdRequest.setServiceDescription("SHOPPING SERVICE");
      ftdRequest.setCommand("SHOPPING_GET_SHOPPING_CART");
      ftdResponse = util.executeService(ftdRequest);

      XMLDocument xml = (XMLDocument) ftdResponse.getSystemValueObject().getXML();

      String xslFile = "/xsl/shoppingCart.xsl";
      xslFile = getServletContext().getRealPath(xslFile);

      HashMap params = new HashMap();
      params.put("persistentObjId", persistentObjId);
      params.put("sessionId", sessionId);
      params.put("actionServlet", (request.getContextPath() + "/servlet/ShoppingCartServlet"));
            
      util.determineCompanyIdAndSetImageSuffixParams(params, xml);
      util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);

      if (config.getInitParameter("WriteToXML").equalsIgnoreCase("true")) {
        util.writeXMLtoFile("shoppingCart.xml", xml);
      }
    } catch (Exception e) {
      util.getLogManager().error("this is where it hapenned ... get execute service " + e.toString(), e);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }
    }
  }

  private void executeCommitOrder(HttpServletRequest request, HttpServletResponse response, Util util, String persistentObjId)
    throws ServletException, IOException {
    FTDServiceRequest ftdRequest = new FTDServiceRequest();
    ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
    ftdRequest.setPersistenceID(persistentObjId);

    ftdRequest.setServiceDescription("BILLING SERVICE");
    ftdRequest.setCommand("BILLING_COMMIT_ORDER");

    FTDServiceResponse ftdResponse = null;

    try {
      ftdResponse = util.executeService(ftdRequest);
    } catch (Exception e) {
      e.printStackTrace();

      RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");

      if (dispatcher != null) {
        dispatcher.forward(request, response);
      }

      return;
    }
  }
}
