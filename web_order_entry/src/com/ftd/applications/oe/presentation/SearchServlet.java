package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.applications.oe.common.FieldUtils;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;

import oracle.xml.parser.v2.*;

import com.ftd.security.cache.vo.UserInfo;

public class SearchServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);
        String searchtype = request.getParameter("search");

        try
        {
            if(searchtype.equals("customer"))
            {
                handleCustomerSearch(request, response, sessionId);
            }
            if(searchtype.equals("sourcescode"))
            {
                handleSourceCodeSearch(request, response);
            }
            if(searchtype.equals("advancedproductsearch"))
            {
                handleAdvancedProductSearch(request, response, userInfo, sessionId);
            }
            if(searchtype.equals("simpleproductsearch"))
            {
                handleSimpleProductSearch(request, response, userInfo, sessionId);
            }
            if(searchtype.equals("keywordsearch"))
            {
                handleKeywordSearch(request, response, userInfo, sessionId);
            }
        
        }
        catch(Exception e)
        {
            util.getLogManager().error("system error " + e.toString(), e);
        }
    }
    
    private void  handleAdvancedProductSearch(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        String itemCartNumber = request.getParameter("itemCartNumber");
        if(itemCartNumber == null) itemCartNumber = "";
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
          
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            if(value != null && value.length() > 0)
            {
                ftdRequest.addArgument(param, value);
                params.put(param, value);
            }
        } 
        String pricePointId = request.getParameter("pricePointId");
        if(pricePointId == null) pricePointId = "";

        String price = request.getParameter("price");
        if(price == null) price = "";

        if(pricePointId.equals(""))
        {
            ftdRequest.addArgument("pricePointId", price);
        }
        
        response.setHeader("Cache-Control", "no-cache");
    
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SEARCH SERVICE");
        ftdRequest.setCommand("SEARCH_PRODUCTS_BY_VALUE");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
            ftdRequest.addArgument("url", url);
            FTDServiceResponse ftdResponse = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/productListIndex.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);    
            params.put("itemCartNumber", itemCartNumber);
            params.put("productListServlet", (request.getContextPath() + "/servlet/ProductListServlet"));
            params.put("intlflag", "D");

            params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
            params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
            params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

            util.setImageSuffixParams(params, request.getParameter("companyId"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("advancedSearchProductList.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
        }               
    }

    private void handleSimpleProductSearch(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        String cartItemNumber = request.getParameter("cartItemNumber");
        if(cartItemNumber == null) cartItemNumber = "";

        String itemCartNumber = request.getParameter("itemCartNumber");
        if(itemCartNumber == null) itemCartNumber = "";

        String upsellFlag = request.getParameter("upsellFlag");
        if(upsellFlag == null) upsellFlag = "";
        
        String upsellCrumb = request.getParameter("upsellCrumb");
        if(upsellCrumb == null) upsellCrumb = "";

        String soonerOverride = request.getParameter("soonerOverride");
        if(soonerOverride == null) soonerOverride = "";

        FTDServiceRequest ftdRequest = new FTDServiceRequest();
          
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);  
            ftdRequest.addArgument(param, value);
        } 

        // if we are coming from the delivery info breadcrumb
        // then redirect to the EditProductDetailServlet
        if(cartItemNumber.length() > 0)
        {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/servlet/EditItemDetailServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&command=getDetail&showBreadCrumbs=Y&custom=N&itemCartNumber=" + cartItemNumber);
            if (dispatcher != null)
                dispatcher.forward(request, response);        
            return;
        }

        response.setHeader("Cache-Control", "no-cache");

        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SEARCH SERVICE");
        ftdRequest.setCommand("SEARCH_PRODUCT_BY_ID");
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "ProductDTLFloral");
        ftdRequest.addArgument("soonerOverride", soonerOverride);
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
            ftdRequest.addArgument("url", url);
            FTDServiceResponse ftdResponse = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();

            if(ftdRequest.getArguments() != null && ftdRequest.getArguments().containsKey(ArgumentConstants.UPSELL_EXISTS)) 
            {
                String xslFile = "/xsl/upsell.xsl";
                String actionServlet = "/servlet/ProductDetailServlet";

                if(ftdRequest.getArguments().containsKey(ArgumentConstants.UPSELL_SKIP) 
                    && !((String) ftdRequest.getArguments().get(ArgumentConstants.UPSELL_SKIP)).equals("N")
                    && !upsellCrumb.equals("Y"))
                {
                    ftdRequest.addArgument("productId", (String) ftdRequest.getArguments().get(ArgumentConstants.UPSELL_SKIP));
                    ftdResponse  = util.executeService(ftdRequest);
                    xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
                    
                    xslFile = "/xsl/productDetail.xsl";
                    actionServlet = "/servlet/DeliveryServlet";
                }

                xslFile = getServletContext().getRealPath(xslFile);
                HashMap params = new HashMap();
      
                params.put("persistentObjId", persistentObjId);
                params.put("sessionId", sessionId);
                params.put("searchType", "simpleproductsearch");
                params.put("cartItemNumber", itemCartNumber);
                params.put("productListServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("actionServlet", (request.getContextPath() + actionServlet));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("upsellCrumb", upsellCrumb);
                params.put("soonerOverride", soonerOverride);

                util.setImageSuffixParams(params, request.getParameter("companyId"));
                util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
                if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
                {
                    util.writeXMLtoFile("simpleSearchProductList.xml", xml);
                }
            }
            else
            {
                String xslFile = "/xsl/productDetail.xsl";
                xslFile = getServletContext().getRealPath(xslFile);
                HashMap params = new HashMap();
      
                params.put("persistentObjId", persistentObjId);
                params.put("sessionId", sessionId);
                params.put("upsellFlag", upsellFlag);
                params.put("soonerOverride", soonerOverride);
                params.put("searchType", "simpleproductsearch");
                params.put("cartItemNumber", itemCartNumber);
                params.put("productListServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));

                params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
                params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
                params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

                util.setImageSuffixParams(params, request.getParameter("companyId"));
                util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
                if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
                {
                    util.writeXMLtoFile("simpleSearchProductList.xml", xml);
                }
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);          
        }               
    }

    private void handleKeywordSearch(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo, String sessionId) throws Exception
    {
        Util util = new Util();
        String persistentObjId = request.getParameter("persistentObjId");
        String itemCartNumber = request.getParameter("itemCartNumber");
        if(itemCartNumber == null) itemCartNumber = "";

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
          
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);  
            ftdRequest.addArgument(param, value);           
        } 

        response.setHeader("Cache-Control", "no-cache");
        
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
        ftdRequest.setServiceDescription("SEARCH SERVICE");
        ftdRequest.setCommand("SEARCH_PRODUCTS_BY_KEYWORD");
        try
        {
        
           ConfigurationUtil cu = ConfigurationUtil.getInstance();

           // Get the user's call center URL
           StringBuffer callCenterUrlKey = new StringBuffer();
           callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
           String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
           if(callCenterUrl == null)
           {
               // get default url
               callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
           }
        
            String url = callCenterUrl;
            url = url + request.getServletPath() + "?" + request.getQueryString();
            ftdRequest.addArgument("url", url);
            String searchKeyword = request.getParameter("searchKeyword");
            if(searchKeyword == null) searchKeyword = "";
            FTDServiceResponse ftdResponse = null;

            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/productListIndex.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
      
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);

            // Escape any apostophies
            if(searchKeyword == null) searchKeyword = "";
            searchKeyword = FieldUtils.replaceAll(searchKeyword, "'", "&apos;");            
            
            params.put("keywords", searchKeyword);
            params.put("search", "keywordsearch");
            params.put("itemCartNumber", itemCartNumber);

            // We can hard code this because only domistic orders can do a keyword search.
            params.put("intlflag", "D");
            
            params.put("productListServlet", (request.getContextPath() + "/servlet/ProductListServlet"));

            params.put("standardLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.STANDARD_LABEL));
            params.put("deluxeLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.DELUXE_LABEL));
            params.put("premiumLabel", cu.getFrpGlobalParm(GeneralConstants.FTDAPPS_CONFIG_CONTEXT, GeneralConstants.PREMIUM_LABEL));

            util.setImageSuffixParams(params, request.getParameter("companyId"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("keywordSearchProductList.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);            
        }          
    }
  
    private void handleSourceCodeSearch(HttpServletRequest request, HttpServletResponse response)
    {
    }

    private void handleCustomerSearch(HttpServletRequest request, HttpServletResponse response, String sessionId) throws Exception
    {
        Util util = new Util();
        String state = request.getParameter("state");
        if(state == null) state = "";
        String zip = request.getParameter("zip");
        if(zip == null) zip = "";
        String persistentObjId = request.getParameter("persistentObjId");
        if(persistentObjId == null) persistentObjId = "";
        String dnis = request.getParameter("dnis");
        if(dnis == null) dnis = "";
        String city = request.getParameter("city");
        if(city == null) city = "";    
        String homePhone = request.getParameter("homePhone");
        if(homePhone == null) homePhone = "";
        String yellowPageBookCode = request.getParameter("yellowPageBookCode");
        if(yellowPageBookCode == null) yellowPageBookCode = "";
        String address = request.getParameter("address");
        if(address == null) address = "";
        String selection = request.getParameter("selection");
        if(selection == null) selection = "";
        String firstName = request.getParameter("firstName"); 
        if(firstName == null) firstName = "";
        String lastName = request.getParameter("lastName");
        if(lastName == null) lastName = "";
        String sourceText = request.getParameter("sourceText");
        if(sourceText == null) sourceText = "";
        String sourceSelect = request.getParameter("sourceSelect");
        if(sourceSelect == null) sourceSelect = "";

        response.setHeader("Cache-Control", "no-cache");
    
          FTDServiceRequest ftdRequest = new FTDServiceRequest();
          ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
          ftdRequest.setPersistenceID(persistentObjId);
          ftdRequest.addArgument("phone", homePhone);
          ftdRequest.setServiceDescription("SEARCH SERVICE");
          ftdRequest.setCommand("SEARCH_CUSTOMER_LOOKUP");

          FTDServiceResponse ftdResponse = null;
          String error = null;

          try
          {
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            String xslFile = "/xsl/customerLookup.xsl";
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();      
            params.put("persistentObjId", persistentObjId);
            params.put("sessionId", sessionId);
            params.put("actionServlet", (request.getContextPath() + "/servlet/IntroductionServlet"));
            params.put("phone", homePhone);
            params.put("state", state);
            params.put("zip", zip);
            params.put("dnis", dnis);
            params.put("city", city);
            params.put("yellowPageBookCode", yellowPageBookCode);
            params.put("address", address);
            params.put("selection", selection);
            params.put("firstName", firstName);
            params.put("lastName",lastName);
            params.put("sourceText", sourceText);
            params.put("sourceSelect", sourceSelect);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("customerLookup.xml", xml);
            }
          }
          catch(Exception e)
          {
            util.getLogManager().error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);            
          }
    }
    
}  
