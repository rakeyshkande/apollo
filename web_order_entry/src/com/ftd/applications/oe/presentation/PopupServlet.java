package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.applications.oe.common.*;
import oracle.xml.parser.v2.*;

public class PopupServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/xml; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setHeader("Cache-Control", "no-cache");
        Util util = new Util();
        util.recordTask(request, response);
        // setup the correct FTDRequest parameters
        String popupId = request.getParameter(GeneralConstants.POPUP_ID_KEY);

        FTDServiceRequest ftdRequest = new FTDServiceRequest();

        if(popupId.equals(GeneralConstants.LOOKUP_SOURCE_CODE_KEY))
        {
            lookupSourceCode(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_ZIP_CODE_KEY))
        {
            lookupZipCode(request, response);
        }
        else if(popupId.equals(GeneralConstants.DISPLAY_CALENDAR_KEY))
        {
            displayCalendar(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_FLORIST_KEY))
        {
            lookupFlorist(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_GREETING_CARDS_KEY))
        {
            lookupGreetingCards(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_CUSTOMER_KEY))
        {
            lookupCustomer(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_RECIPIENT_KEY))
        {
            lookupRecipient(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_RECIPE_KEY))
        {
            lookupRecipe(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_CARE_INSTRUCTIONS_KEY))
        {
            lookupCareInstructions(request, response);
        }
        else if(popupId.equals(GeneralConstants.LOOKUP_GREETING_CARD_KEY))
        {
            lookupGreetingCard(request, response);
        }
        else
        {
            response.setContentType(CONTENT_TYPE);
            PrintWriter out = response.getWriter();
            out.println("<?xml version=\"1.0\"?>");
            out.println("<ERROR>UNKNOWN POPUP ID</ERROR>");
            out.close();
            return;
        }
    }

    private void lookupSourceCode(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String searchString = request.getParameter(GeneralConstants.SOURCE_CODE_SEARCH_KEY);
        String dnisType = request.getParameter("dnisType");
        if(dnisType == null) dnisType = "";

        if(searchString == null)
        {
            searchString = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);
            ftdRequest.setCommand(CommandConstants.SEARCH_SOURCE_CODE_LOOKUP);
            ftdRequest.addArgument("searchValue", searchString);
            ftdRequest.addArgument("dnisType", dnisType);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/sourceCodeLookup.xsl";
            String xmlFile = "sourceCode.xml";
            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("dnisType", dnisType);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile(xmlFile, xml);
            }        
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void displayCalendar(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        Util util = new Util();
        String month = request.getParameter(GeneralConstants.MONTH_SEARCH_KEY);
        String year = request.getParameter(GeneralConstants.YEAR_SEARCH_KEY);
        String productId = request.getParameter("productId");
        if(productId == null) productId = "";

        if(month == null)
        {
            month = "";
        }

        if(year == null)
        {
            year = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            String persistentObjId = request.getParameter("persistentObjId");
            ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
            ftdRequest.setPersistenceID(persistentObjId);
            ftdRequest.setCommand(CommandConstants.SEARCH_GET_CALENDAR_LOOKUP);
            ftdRequest.setServiceDescription("SEARCH SERVICE");
            ftdRequest.addArgument("month", month);
            ftdRequest.addArgument("year", year);
            ftdRequest.addArgument("productId", productId);
            String xslFile = "/xsl/calendar.xsl";
            String xmlFile = "calendar.xml";
            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("persistentObjId", ftdResponse.getPersistanceID());
            params.put("actionServlet", (request.getContextPath() + "/servlet/PopupServlet"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile(xmlFile, xml);
            }        
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupGreetingCard(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }

        String cardId = request.getParameter(GeneralConstants.GREETING_CARD_SEARCH_KEY);
        String occasion = request.getParameter(GeneralConstants.OCCASION_SEARCH_KEY);
        if(cardId == null)
        {
            cardId = "";
        }

        if(occasion == null)
        {
            occasion = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);
            ftdRequest.setCommand(CommandConstants.SEARCH_GREETING_CARD_LOOKUP);
            ftdRequest.addArgument("cardId", cardId);
            ftdRequest.addArgument("occasion", occasion);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/greetingDetail.xsl";
            String xmlFile = "card.xml";

            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
            util.setImageSuffixParams(params, request.getParameter("companyId"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile(xmlFile, xml);
            }        
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupZipCode(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        String searchCity = request.getParameter(GeneralConstants.CITY_SEARCH_KEY);
        String searchState = request.getParameter(GeneralConstants.STATE_SEARCH_KEY);
        String searchZip = request.getParameter(GeneralConstants.ZIP_CODE_SEARCH_KEY);
        String countryType = request.getParameter(GeneralConstants.COUNTRY_TYPE_SEARCH_KEY);

        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }

        if(countryType == null)
        {
            countryType = "";
        }

        if(searchCity == null)
        {
            searchCity = "";
        }

        if(searchState == null)
        {
            searchState = "";
        }

        if(searchZip == null)
        {
            searchZip = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);
            ftdRequest.setCommand(CommandConstants.SEARCH_GET_ZIP_CODE);
            ftdRequest.addArgument("city", searchCity);
            ftdRequest.addArgument("state", searchState);
            ftdRequest.addArgument("zip", searchZip);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/cityLookup.xsl";
            String xmlFile = "zipCode.xml";
            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
            params.put("countryType", countryType);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile(xmlFile, xml);
            }        
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupFlorist(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }

        String instName = request.getParameter(GeneralConstants.INSTITUTION_SEARCH_KEY);
        if(instName == null) instName = "";
        instName = FieldUtils.replaceAll(instName, "'", "&apos;");
        params.put(GeneralConstants.INSTITUTION_SEARCH_KEY, instName);

        String phoneNumber = request.getParameter(GeneralConstants.PHONE_NUMBER_SEARCH_KEY);
        String countryType = request.getParameter(GeneralConstants.COUNTRY_TYPE_SEARCH_KEY);

        if(countryType == null)
        {
            countryType = "";
        }

        if(phoneNumber == null)
        {
            phoneNumber = "";
        }

        String institution = request.getParameter(GeneralConstants.INSTITUTION_SEARCH_KEY);
        if(institution != null)
        {
            institution = institution.toUpperCase();
        }
        else
        {
            institution = "";
        }

        String address1 = request.getParameter(GeneralConstants.ADDRESS1_SEARCH_KEY);
        if(address1 == null)
        {
            address1 = "";
        }

        String city = request.getParameter(GeneralConstants.CITY_SEARCH_KEY);
        if(city != null)
        {
            city = city.toUpperCase();
        }
        else
        {
            city = "";
        }

        String state = request.getParameter(GeneralConstants.STATE_SEARCH_KEY);
        if(state != null)
        {
            state = state.toUpperCase();
        }
        else
        {
            state = "";
        }

        String zipCode = request.getParameter(GeneralConstants.ZIP_CODE_SEARCH_KEY);
        if(zipCode == null)
        {
            zipCode = "";
        }

        String productId = request.getParameter("productId");
        if(productId == null)
        {
            productId = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);
            ftdRequest.setCommand(CommandConstants.SEARCH_GET_FLORIST);
            ftdRequest.addArgument("inPhone", phoneNumber);
            ftdRequest.addArgument("floristName", institution);
            ftdRequest.addArgument("address1", address1);
            ftdRequest.addArgument("inCity", city);
            ftdRequest.addArgument("inState", state);
            ftdRequest.addArgument("inZipCode", zipCode);
            ftdRequest.addArgument("productId", productId);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/floristLookup.xsl";
            String xmlFile = "florist.xml";

            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
            params.put("countryType", countryType);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile(xmlFile, xml);
            }        
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupGreetingCards(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }
        String occasion = request.getParameter(GeneralConstants.OCCASION_SEARCH_KEY);
        if(occasion == null)
        {
            occasion = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);
            ftdRequest.setCommand(CommandConstants.SEARCH_GREETING_CARD_LOOKUP);
            ftdRequest.addArgument("occasion", occasion);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/greetingLookup.xsl";
            String xmlFile = "cards.xml";

            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
             util.setImageSuffixParams(params, request.getParameter("companyId"));
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("cards.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupCustomer(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }

        String phoneNumber = request.getParameter(GeneralConstants.PHONE_NUMBER_SEARCH_KEY);
        String institution = request.getParameter(GeneralConstants.INSTITUTION_SEARCH_KEY);
        String customerId = request.getParameter(GeneralConstants.CUSTOMER_ID_SEARCH_KEY);
        String address1 = request.getParameter(GeneralConstants.ADDRESS1_SEARCH_KEY);
        String address2 = request.getParameter(GeneralConstants.ADDRESS2_SEARCH_KEY);
        String city = request.getParameter(GeneralConstants.CITY_SEARCH_KEY);
        String state = request.getParameter(GeneralConstants.STATE_SEARCH_KEY);
        String zipCode = request.getParameter(GeneralConstants.ZIP_CODE_SEARCH_KEY);
        String countryType = request.getParameter(GeneralConstants.COUNTRY_TYPE_SEARCH_KEY);

        if(countryType == null)
        {
            countryType = "";
        }

        if(phoneNumber == null)
        {
            phoneNumber = "";
        }

        if(institution == null)
        {
            institution = "";
        }

        if(customerId == null)
        {
            customerId = "";
        }

        if(address1 ==  null)
        {
            address1 = "";
        }

        if(address2 == null)
        {
            address2 = "";
        }

        if(city != null)
        {
            city = city.toUpperCase();
        }
        else
        {
            city = "";
        }

        if(state != null)
        {
            state = state.toUpperCase();
        }
        else
        {
            state = "";
        }

        if(zipCode == null)
        {
            zipCode = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);
            ftdRequest.setCommand(CommandConstants.SEARCH_CUSTOMER_LOOKUP);
            ftdRequest.addArgument("inPhone", phoneNumber);
            ftdRequest.addArgument("customerId", customerId);
            ftdRequest.addArgument("institutionName", institution);
            ftdRequest.addArgument("address1", address1);
            ftdRequest.addArgument("address2", address2);
            ftdRequest.addArgument("inCity", city);
            ftdRequest.addArgument("inState", state);
            ftdRequest.addArgument("inZipCode", zipCode);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/customerLookup.xsl";
            String xmlFile = "customer.xml";

            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
            xslFile = getServletContext().getRealPath(xslFile);
            params.put("countryType", countryType);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("customer.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupRecipient(HttpServletRequest request, HttpServletResponse response)
    {
        Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }

        String recip = request.getParameter(GeneralConstants.INSTITUTION_SEARCH_KEY);
        if(recip == null) recip = "";
        recip = FieldUtils.replaceAll(recip, "'", "&apos;");
        params.put(GeneralConstants.INSTITUTION_SEARCH_KEY, recip);

        
        String phoneNumber = request.getParameter(GeneralConstants.PHONE_NUMBER_SEARCH_KEY);
        String institution = request.getParameter(GeneralConstants.INSTITUTION_SEARCH_KEY);
        String customerId = request.getParameter(GeneralConstants.CUSTOMER_ID_SEARCH_KEY);
        String address1 = request.getParameter(GeneralConstants.ADDRESS1_SEARCH_KEY);
        String address2 = request.getParameter(GeneralConstants.ADDRESS2_SEARCH_KEY);
        String city = request.getParameter(GeneralConstants.CITY_SEARCH_KEY);
        String state = request.getParameter(GeneralConstants.STATE_SEARCH_KEY);
        String zipCode = request.getParameter(GeneralConstants.ZIP_CODE_SEARCH_KEY);
        String countryType = request.getParameter(GeneralConstants.COUNTRY_TYPE_SEARCH_KEY);
        String instType = request.getParameter(GeneralConstants.INST_TYPE_SEARCH_KEY);

        if(countryType == null)
        {
            countryType = "";
        }

        if(instType == null)
        {
            instType = "";
        }

        if(phoneNumber == null)
        {
            phoneNumber = "";
        }

        if(institution == null)
        {
            institution = "";
        }

        if(customerId == null)
        {
            customerId = "";
        }

        if(address1 ==  null)
        {
            address1 = "";
        }

        if(address2 == null)
        {
            address2 = "";
        }

        if(city != null)
        {
            city = city.toUpperCase();
        }
        else
        {
            city = "";
        }

        if(state != null)
        {
            state = state.toUpperCase();
        }
        else
        {
            state = "";
        }

        if(zipCode == null)
        {
            zipCode = "";
        }

        try
        {
            FTDServiceRequest ftdRequest  = new FTDServiceRequest();
            ftdRequest.setPersistence(FTDServiceRequest.NONE);

            if(instType.equals("H") || instType.equals("F") || instType.equals("N"))
            {
                ftdRequest.setCommand(CommandConstants.SEARCH_INSTITUTION_LOOKUP);
            }
            else
            {
                ftdRequest.setCommand(CommandConstants.SEARCH_CUSTOMER_LOOKUP);
            }

            ftdRequest.addArgument("inPhone", phoneNumber);
            ftdRequest.addArgument("customerId", customerId);
            ftdRequest.addArgument("institutionName", institution);
            ftdRequest.addArgument("address1", address1);
            ftdRequest.addArgument("address2", address2);
            ftdRequest.addArgument("inCity", city);
            ftdRequest.addArgument("inState", state);
            ftdRequest.addArgument("inZipCode", zipCode);
            ftdRequest.addArgument("instType", instType);
            ftdRequest.addArgument("countryId", countryType);
            ftdRequest.setServiceDescription("SEARCH SERVICE");

            String xslFile = "/xsl/recipientLookup.xsl";
            String xmlFile = "recipient.xml";

            FTDServiceResponse ftdResponse = null;
            ftdResponse  = util.executeService(ftdRequest);
            XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();

            xslFile = getServletContext().getRealPath(xslFile);

            params.put("countryType", countryType);
            params.put("institutionType", instType);
            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params, false);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("recipient.xml", xml);
            }
        }
        catch(Exception e)
        {
            util.getLogManager().error(e.toString(), e);
        }
    }

    private void lookupRecipe(HttpServletRequest request, HttpServletResponse response)
    {
    }

    private void lookupCareInstructions(HttpServletRequest request, HttpServletResponse response)
    {
    }

}
