package com.ftd.applications.oe.presentation;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import com.ftd.applications.oe.common.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.cache.vo.UserInfo;

public class EditDeliveryServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private ServletConfig config;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //ixv 11/26/2003 DEV_PHASE_II Security Retrofit
      String sessionId = Util.authenticate(request, response, GeneralConstants.SECURITY_TOKEN_NAME_WEBOE);
      if(sessionId == null) return;
      UserInfo userInfo = Util.getUserInfo(sessionId);
      if(userInfo == null) return;
        Util util = new Util();
        util.recordTask(request, response);

        String persistentObjId = "";
        String cartItemNumber = "";
    
        persistentObjId =  request.getParameter("persistentObjId");
        if(persistentObjId == null) persistentObjId = "";

        cartItemNumber = request.getParameter("cartItemNumber");
        if(cartItemNumber  == null) cartItemNumber = "";
   
        FTDServiceRequest ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);

        FTDServiceResponse ftdResponse = null;
    
      try
      {
      
         ConfigurationUtil cu = ConfigurationUtil.getInstance();

         // Get the user's call center URL
         StringBuffer callCenterUrlKey = new StringBuffer();
         callCenterUrlKey.append("application.url.").append(userInfo.getCallCenter().toLowerCase());
         String callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, callCenterUrlKey.toString());
         if(callCenterUrl == null)
         {
             // get default url
             callCenterUrl = cu.getFrpGlobalParm(GeneralConstants.WEBOE_CONFIG_CONTEXT, "application.url.default");
         }
        
        String url = callCenterUrl;
        url = url + request.getServletPath() + "?" + request.getQueryString();

        ftdRequest = new FTDServiceRequest();
        ftdRequest.setPersistence(FTDServiceRequest.USE_OBJECT);
        ftdRequest.setPersistenceID(persistentObjId);
      
        ftdRequest.addArgument("company", "FTDP");
        ftdRequest.addArgument("pagename", "DeliveryInfo");
        ftdRequest.addArgument("url", url);
        ftdRequest.addArgument("cartItemNumber", cartItemNumber);      
      
        ftdRequest.setServiceDescription("SHOPPING SERVICE");
        ftdRequest.setCommand("SHOPPING_LOAD_DELIVERY_INFO");
      
        ftdResponse  = util.executeService(ftdRequest);
        XMLDocument xml = (XMLDocument)ftdResponse.getSystemValueObject().getXML();
        String xslFile = "/xsl/delivery_info.xsl";
        xslFile = getServletContext().getRealPath(xslFile);
        HashMap params = new HashMap();

        params.put("persistentObjId", ftdResponse.getPersistanceID());
        params.put("sessionId", sessionId);
        params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryUpdateServlet"));
        params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
        params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
        params.put("breadCrumbType", "D");

            util.transform(xml, xslFile, config.getInitParameter("ClearCache"), request, response, params);
            if(config.getInitParameter("WriteToXML").equalsIgnoreCase("true"))
            {
                util.writeXMLtoFile("editDelivery.xml", xml);
            }
      }
      catch(Exception e)
      {
            util.getLogManager().error("get execute service " + e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if (dispatcher != null)
                dispatcher.forward(request, response);  
      }
  }

}  