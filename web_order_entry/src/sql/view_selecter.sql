set pagesize 10000 linesize 132 trimspool on long 32000
spool &&schema_owner._views
column column_name format a30
select view_name, text
from   dba_views
where  owner = upper('&&schema_owner')
order by 1
/
spool off
exit
