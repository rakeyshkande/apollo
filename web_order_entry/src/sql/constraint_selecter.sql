set pagesize 10000 linesize 132 trimspool on
column constraint_name form a30
column table_name form a28
column r_owner form a12
colu r_constraint_name form a26
colu column_name form a26
spool &&schema_owner._constraints
select c.constraint_type, c.constraint_name, c.table_name, c.r_owner, c.r_constraint_name, cc.column_name
from   dba_constraints c, dba_cons_columns cc
where  c.owner = upper('&&schema_owner')
and    c.owner = cc.owner
and    c.constraint_name = cc.constraint_name
and    c.constraint_type in ('P','U','R')
order by 2, cc.position
/
spool off
exit
