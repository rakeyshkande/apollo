column grantee format a24
column owner format a24
column privilege format a24
set pagesize 50000 linesize 132 trimspool on
spool grants
select grantee, owner, table_name, privilege, grantable
from   dba_tab_privs
order by 1, 2, 3, 4
/
spool off
exit
