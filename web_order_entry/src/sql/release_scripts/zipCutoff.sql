ALTER  TABLE ftd_apps.CSZ_AVAIL ADD (
  LATEST_CUTOFF                  VARCHAR2(4)) ;

ALTER  TABLE ftd_apps.CSZ_PRODUCTS ADD (
  PRODUCT_CUTOFF                 VARCHAR2(4)) ;

@$CVS_BASE/OEAPP/plsql/oe_get_zipcode_cutoff.fnc
@$CVS_BASE/OEAPP/plsql/oel_update_csz_zip_code.prc
@$CVS_BASE/OEAPP/plsql/oel_update_csz_products.prc

commit;

exit
