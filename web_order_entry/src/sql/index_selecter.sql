set pagesize 10000 linesize 132 trimspool on
spool &&schema_owner._indexes
column column_name format a30
select i.table_owner, i.table_name, i.index_name, c.column_name
from   dba_indexes i, dba_ind_columns c
where  i.owner = upper('&&schema_owner')
and    i.owner = c.index_owner
and    i.index_name = c.index_name
order by 1, 2, 3
/
spool off
exit
