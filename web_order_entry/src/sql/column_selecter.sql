set linesize 132 long 10 pagesize 10000 trimspool on
COLUMN data_type FORMAT a8
spool &&schema_owner._columns
select table_name, column_name, data_type, data_length, data_precision, data_scale, nullable, data_default
from   dba_tab_columns
where  owner = upper('&&schema_owner')
order by 1, 2
/
spool off
exit
