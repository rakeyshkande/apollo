package com.ftd.decisionresult.vo;

import java.util.List;


/**
 * CallDispositionVO extends BaseDecisionResultVO
 * This VO represents the base call disposition data.
 * @author - Vinay Shivaswamy
 * @date - 2/15/2011
 * @version - 1.0 for CD-4.8.0
 */
public class DecisionResultVO extends BaseDecisionResultVO{
    
    //Call Disposition Id a sequence Id
    private long resultId;
    
    //Decision Config Id - unique Id of disposition
    private long decisionConfigId;
    
    //Current Level being displayed or to be displayed
    private int currentLevel;
    
    /*
     *  COMPLETE,
     *  INPROGRESS,
     *  DECLINE,
     *  VIEW
     */
    private DecisionResultStatus status;
    
    //List of CallDispositionDetailVO
    private List<DecisionResultDetailVO> details = null;
    
    //List of CallDispositionEntityVO
    private List<DecisionResultEntityVO> entities = null;
    
    /*
     * Default no-arg constructor
     */
    public DecisionResultVO() {
    }

    public void setResultId(long resultId) {
        this.resultId = resultId;
    }

    public long getResultId() {
        return resultId;
    }
   
    public void setDecisionConfigId(long decisionConfigId) {
        this.decisionConfigId = decisionConfigId;
    }

    public long getDecisionConfigId() {
        return decisionConfigId;
    }

    public void setStatus(DecisionResultStatus status) {
        this.status = status;
    }

    public DecisionResultStatus getStatus() {
        return status;
    }

    public void setDetails(List<DecisionResultDetailVO> details) {
        this.details = details;
    }

    public List<DecisionResultDetailVO> getDetails() {
        return details;
    }


    public void setEntities(List<DecisionResultEntityVO> entities) {
        this.entities = entities;
    }

    public List<DecisionResultEntityVO> getEntities() {
        return entities;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

}
