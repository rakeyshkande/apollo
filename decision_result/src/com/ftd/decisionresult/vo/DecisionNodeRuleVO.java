package com.ftd.decisionresult.vo;

/**
 * Represents DCSN_FX_CONFIG_VALUE_RULES
 * @author Vinay Shivaswamy
 * @date 2/16/2011
 * @version 1.0 for CD - 4.8.0
 */
public class DecisionNodeRuleVO extends BaseDecisionResultVO{
  private long valueId;
  private long decisionConfigId;
  private String ruleCode;


  public void setValueId(long valueId){
    this.valueId = valueId;
  }

  public long getValueId(){
    return valueId;
  }

  public void setDecisionConfigId(long decisionConfigId){
    this.decisionConfigId = decisionConfigId;
  }

  public long getDecisionConfigId(){
    return decisionConfigId;
  }

  public void setRuleCode(String ruleCode){
    this.ruleCode = ruleCode;
  }

  public String getRuleCode(){
    return ruleCode;
  }
}
