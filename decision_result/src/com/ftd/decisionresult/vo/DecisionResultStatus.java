package com.ftd.decisionresult.vo;

import java.io.Serializable;


/**
 * CallDispositionStatus represents the possible call disposition status types.
 * @author - Vinay Shivaswamy
 * @date - 2/15/2011
 * @version - 1.0 for CD-4.8.0
 */
public enum DecisionResultStatus implements Serializable{
    COMPLETE,
    INPROGRESS,
    DECLINE,
    VIEW;
}
