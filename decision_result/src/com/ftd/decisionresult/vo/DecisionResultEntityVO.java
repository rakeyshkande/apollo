package com.ftd.decisionresult.vo;

/**
 * CallDispositionEntityVO extends BaseDecisionResultVO 
 * This VO represents the call disposition entity data.
 * @author - Vinay Shivaswamy
 * @date - 2/15/2011
 * @version - 1.0 for CD-4.8.0
 */
public class DecisionResultEntityVO extends BaseDecisionResultVO {
    
    //Call Disposition Entity Id
    private long resultEntityId;
    
    //Call Disposition Id
    private long resultId;
    
    //Name could be CLEAN.ORDER_DETAIL.ORDER_DETAIL_ID
    //OR CLEAN.CUSTOMER.CUSTOMER_ID
    private String entityName;
    
    //Entity Value
    private long entityValue;
    
    /*
     * Default no-arg constructor
     */
    public DecisionResultEntityVO() {
    }

    public void setResultEntityId(long resultEntityId) {
        this.resultEntityId = resultEntityId;
    }

    public long getResultEntityId() {
        return resultEntityId;
    }

    public void setResultId(long resultId) {
        this.resultId = resultId;
    }

    public long getResultId() {
        return resultId;
    }
    
    public void setEntityValue(long entityValue) {
        this.entityValue = entityValue;
    }

    public long getEntityValue() {
        return entityValue;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    
}
