package com.ftd.decisionresult.vo;


/**
 * CallDispositionDetailVO extends BaseDecisionResultVO 
 * This VO represents the call disposition detail data.
 * @author - Vinay Shivaswamy
 * @date - 2/15/2011
 * @version - 1.0 for CD-4.8.0
 */
public class DecisionResultDetailVO extends BaseDecisionResultVO implements Comparable{
    
    //call disposition detail Id
    private long resultDetailId;
    
    //call Disposition Id
    private long resultId;
    
    //Selected value Id
    private long valueId;
    
    //Selected level value's parent value Id
    private long parentValueId; 
    
    //Name of the value selected
    private String name;
    
    //Comment for the selected value
    private String commentText;
    
    //Script text for the selected level to display in the next level
    private String scriptText;
    
    //Current selected level
    private int level;
    
    /*
     * Default no-arg constructor
     */
    public DecisionResultDetailVO() {
    }

    public void setResultDetailId(long resultDetailId) {
        this.resultDetailId = resultDetailId;
    }

    public long getResultDetailId() {
        return resultDetailId;
    }

    public void setResultId(long resultId) {
        this.resultId = resultId;
    }

    public long getResultId() {
        return resultId;
    }
    
    public void setValueId(long valueId) {
        this.valueId = valueId;
    }

    public long getValueId() {
        return valueId;
    }

    public void setParentValueId(long parentValueId) {
        this.parentValueId = parentValueId;
    }

    public long getParentValueId() {
        return parentValueId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getCommentText() {
        return commentText;
    }
   
    public void setScriptText(String scriptText) {
        this.scriptText = scriptText;
    }

    public String getScriptText() {
        return scriptText;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }


    public int compareTo(Object obj) {
        DecisionResultDetailVO detail = (DecisionResultDetailVO)obj;
        if (this.level < detail.level)
            return -1;
        else if (this.level > detail.level) 
            return 1;
        else
            return 0;
    }
}
