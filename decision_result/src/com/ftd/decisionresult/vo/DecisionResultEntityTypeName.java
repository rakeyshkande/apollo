package com.ftd.decisionresult.vo;

import java.io.Serializable;


/**
 * CallDispositionEntityTypeName represents the possible call disposition entity types.
 * @author - Vinay Shivaswamy
 * @date - 2/15/2011
 * @version - 1.0 for CD-4.8.0
 */
public enum DecisionResultEntityTypeName implements Serializable{
   ORDER_DETAIL_ID,
   CUSTOMER_ID;
}
