package com.ftd.decisionresult.vo;

import java.io.Serializable;

import java.util.Date;


/**
  * BaseCallDispositionVO is the base VO for all call disposition related VOs.
  * @author - Vinay Shivaswamy
  * @date - 2/15/2011
  * @version - 1.0 for CD-4.8.0
  */
public class BaseDecisionResultVO implements Serializable{
    
    //User Id that created
    private String createdBy;
    
    //Created on date
    private Date createdOn;
    
    //User that updated
    private String updateBy;
    
    //Updated on date
    private Date updatedOn;

    /*
     * Default no-arg constructor
     */
    public BaseDecisionResultVO() {
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }
}
