package com.ftd.decisionresult.vo;

import java.io.Serializable;

import java.util.List;


/**
 * CallDispositionStateVO represents all the required state information to be used in UI.
 * @author - Vinay Shivaswamy
 * @date - 2/16/2011
 * @version - 1.0 for CD-4.8.0
 */
public class DecisionResultStateVO implements Serializable{
    
    //current Level Parent node
    private DecisionNodeVO parentNode;
    
    //dispoEnds tells if it is the last step
    private boolean dispoEnds;
    
    //Next button to be displayed or not
    private boolean next;
    
    //Back button to be displayed or not 
    private boolean back;
    
    //Submit button to be displayed or not
    private boolean submit;
    
    //Decline button to be displayed or not
    private boolean decline;
    
    //CallDispositionVO
    private DecisionResultVO decisionResult;
    
    //available entities
    private String availableEntities;
    
    //List of EntityDetailVO
    private List<EntityDetailVO> entityDetails;
    
    //List of EntityDetailVO
    private List<DecisionNodeVO> childNodes;
    
    //tells whether the disposition data is available or not
    private boolean dispositionAvailable = true;
    
    /*
     * Default no-arg constructor
     */
    public DecisionResultStateVO() {
    }
    
    public void setDispoEnds(boolean dispoEnds) {
        this.dispoEnds = dispoEnds;
    }

    public boolean isDispoEnds() {
        return dispoEnds;
    }
    
    public void setNext(boolean next) {
        this.next = next;
    }

    public boolean isNext() {
        return next;
    }

    public void setBack(boolean back) {
        this.back = back;
    }

    public boolean isBack() {
        return back;
    }

    public void setSubmit(boolean submit) {
        this.submit = submit;
    }

    public boolean isSubmit() {
        return submit;
    }

    public void setDecline(boolean decline) {
        this.decline = decline;
    }

    public boolean isDecline() {
        return decline;
    }

    public void setParentNode(DecisionNodeVO parentNode) {
        this.parentNode = parentNode;
    }

    public DecisionNodeVO getParentNode() {
        return parentNode;
    }
   
    public void setChildNodes(List<DecisionNodeVO> childNodes) {
        this.childNodes = childNodes;
    }

    public List<DecisionNodeVO> getChildNodes() {
        return childNodes;
    }

  public void setEntityDetails(List<EntityDetailVO> entityDetails){
    this.entityDetails = entityDetails;
  }

  public List<EntityDetailVO> getEntityDetails()
  {
    return entityDetails;
  }

    public void setAvailableEntities(String availableEntities) {
        this.availableEntities = availableEntities;
    }

    public String getAvailableEntities() {
        return availableEntities;
    }

    public void setDecisionResult(DecisionResultVO decisionResult) {
        this.decisionResult = decisionResult;
    }

    public DecisionResultVO getDecisionResult() {
        return decisionResult;
    }

    public void setDispositionAvailable(boolean dispositionAvailable) {
        this.dispositionAvailable = dispositionAvailable;
    }

    public boolean isDispositionAvailable() {
        return dispositionAvailable;
    }
}
