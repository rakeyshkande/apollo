package com.ftd.decisionresult.vo;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents DCSN_FX_CONFIG_VALUE
 * @author Vinay Shivaswamy
 * @date 2/16/2011
 * @version 1.0 for CD - 4.8.0
 */
public class DecisionNodeVO extends BaseDecisionResultVO{
  private long valueId;
  private long decisionConfigId;
  private String name;
  private boolean active = true;
  private String scriptText;
  private boolean allowComments = false;
  private long groupOrder;
  private boolean eligibleToValidate;
  
  //Tells the level in the current disposition
  private int level;
  
  private List<DecisionNodeRuleVO> rules = new ArrayList<DecisionNodeRuleVO>();
    
  //Tells whether this node has any more children
  private boolean hasChildNodes = false;
  
  //Tells whether the node is selected or not
  private boolean selected;
  
  // This automatically tracks parentNode
  long parentValueId = -1; 

  // This shows the comment for the node selected
  private String commentText;

  
  /*
   * default constructor
   */
  public DecisionNodeVO(){
  }
  
  public DecisionNodeVO(long id, String name){
    this.valueId = id;
    this.name = name;
  }

  public void setName(String name){
    this.name = name;
  }

  public String getName(){
    return name;
  }
  
  public void setActive(boolean active){
    this.active = active;
  }

  public boolean isActive(){
    return active;
  }

  public void setScriptText(String scriptText){
    this.scriptText = scriptText;
  }

  public String getScriptText(){
    return scriptText;
  }

  public void setAllowComments(boolean allowComments){
    this.allowComments = allowComments;
  }

  public boolean isAllowComments(){
    return allowComments;
  }

 
  public void setLevel(int level){
    this.level = level;
  }

  public int getLevel(){
    return level;
  }


  public void setRules(List<DecisionNodeRuleVO> rules){
    this.rules = rules;
  }

  public List<DecisionNodeRuleVO> getRules(){
    return rules;
  }

  public void setParentValueId(long parentValueId){
    this.parentValueId = parentValueId;
  }

  public long getParentValueId(){
    return parentValueId;
  }

  public void setValueId(long valueId){
    this.valueId = valueId;
  }

  public long getValueId(){
    return valueId;
  }


  public void setDecisionConfigId(long decisionConfigId){
    this.decisionConfigId = decisionConfigId;
  }

  public long getDecisionConfigId(){
    return decisionConfigId;
  }

    
  public void setGroupOrder(long groupOrder){
    this.groupOrder = groupOrder;
  }

  public long getGroupOrder(){
    return groupOrder;
  }

    public void setHasChildNodes(boolean hasChildNodes) {
        this.hasChildNodes = hasChildNodes;
    }

    public boolean hasChildNodes() {
        return hasChildNodes;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getCommentText() {
        return commentText;
    }
   
    
    /**
     * This method returns true if active flag is true and rules size > 0
     * @return boolean
     */
    public boolean isEligibleToValidate() {
        boolean eligible = false;
        
        if(rules != null && rules.size()>0)
            eligible = true;
        
        return eligible;
    }
    
    
}
