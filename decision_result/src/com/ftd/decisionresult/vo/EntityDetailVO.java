package com.ftd.decisionresult.vo;

import com.ftd.decisionresult.constant.DecisionResultConstants;

import java.text.SimpleDateFormat;

import java.util.Date;

/**
 * EntityDetailVO extends BaseDecisionResultVO
 * This VO represents all the required details of the entity to be used to validate.
 * @author - Vinay Shivaswamy
 * @date - 2/16/2011
 * @version - 1.0 for CD-4.8.0
 */
public class EntityDetailVO extends BaseDecisionResultVO {
    //static variables for status
    private static String REQUIRED_TRUE="TRUE";
    private static String REQUIRED_FALSE="FALSE";
    private static String SUBMITTED="SUBMITTED";
    private static String DECLINED="DECLINED";
    private static String NONE="NONE";
    
    //EntityId - could be 
    private long entityId;
    
    //External Order Number 
    private String externalOrderNumber;
    
    //EntityId - could be 
    private String entityName;
    
    //First name of recipient
    private String firstName;
    
    //Last name of recipient
    private String lastName;
    
    //Order date
    private String orderDate;
    
    //True if order is international delivery
    private boolean internationalDelivery;
    
    //True if order delivered by florist
    private boolean floristDelivered;
    
    //True if order delivered by vendor
    private boolean vendorDelivered;
    
    //True if order has vase add-on
    private boolean vaseAddon;
    
    //True if order has non vase add-on
    private boolean nonVaseAddon;
    
    //Recipient Name - concatenated first and last name
    private String recipientName;
    
    //tells whether this entity is selected in the UI
    private boolean selected;
    
    //tells whether this entity is required or not
    private boolean required;
    
    //tells submission status for this entity
    private String status;
    
    /*
     * Default no-arg constructor
     */
    public EntityDetailVO() {
    }


    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

//    public String getFirstName() {
//        return firstName;
//    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public String getLastName() {
//        return lastName;
//    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
    
    public String getOrderDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        return formatter.format(orderDate);
    }
    
    public void setInternationalDelivery(boolean internationalDelivery) {
        this.internationalDelivery = internationalDelivery;
    }

    public boolean isInternationalDelivery() {
        return internationalDelivery;
    }

    public void setFloristDelivered(boolean floristDelivered) {
        this.floristDelivered = floristDelivered;
    }

    public boolean isFloristDelivered() {
        return floristDelivered;
    }

    public void setVendorDelivered(boolean vendorDelivered) {
        this.vendorDelivered = vendorDelivered;
    }

    public boolean isVendorDelivered() {
        return vendorDelivered;
    }

    public void setVaseAddon(boolean vaseAddon) {
        this.vaseAddon = vaseAddon;
    }

    public boolean isVaseAddon() {
        return vaseAddon;
    }

    public void setNonVaseAddon(boolean nonVaseAddon) {
        this.nonVaseAddon = nonVaseAddon;
    }

    public boolean isNonVaseAddon() {
        return nonVaseAddon;
    }

//    public void setRecipientName(String recipientName) {
//        this.recipientName = recipientName;
//    }
    
    /**
     * Returns the Name of the recipient
     * @return First Name intital and last name
     */
    public String getRecipientName() {
        return this.firstName.charAt(1) + " " + this.lastName;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
    
    public void setRequired(String required) {
        if(required.equalsIgnoreCase(REQUIRED_TRUE))
            this.required = true;
        else if(required.equalsIgnoreCase(REQUIRED_FALSE))
            this.required = false;
    }
    
    public boolean isRequired() {
        return required;
    }

    public void setStatus(String status) {
        if(status.equalsIgnoreCase(SUBMITTED))
            this.status = SUBMITTED;
        else if(status.equalsIgnoreCase(DECLINED))
            this.status=DECLINED;
        else if(status.equalsIgnoreCase("")|| status==null || status.equalsIgnoreCase(NONE))
            this.status=NONE; 
    }

    public String getStatus() {
        return status;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }
    
    public void setExternalOrderNumber(String externalOrderNumber) {
        this.externalOrderNumber = externalOrderNumber;
    }

    public String getExternalOrderNumber() {
        return externalOrderNumber;
    }
    
    /**
     * This method returns true if atleast one rule passes for this entity
     * @param DecisionNodeRuleVO ruleVO
     * @return boolean
     */
    public boolean validate(DecisionNodeRuleVO ruleVO){
        boolean valid = false;
        if(ruleVO.getRuleCode() != null){ 
            if(ruleVO.getRuleCode().equalsIgnoreCase(DecisionResultConstants.CD_FLORIST_DELIVERED)){
                if(floristDelivered)
                    valid=true;
            }
            else if(ruleVO.getRuleCode().equalsIgnoreCase(DecisionResultConstants.CD_VENDOR_DELIVERED)){
                if(vendorDelivered)
                    valid=true;
            }
            else if(ruleVO.getRuleCode().equalsIgnoreCase(DecisionResultConstants.CD_INT_DELIVERY)){
                if(internationalDelivery)
                    valid=true;
            }
            else if(ruleVO.getRuleCode().equalsIgnoreCase(DecisionResultConstants.CD_VASE_ADDON)){
                if(vaseAddon)
                    valid=true;
            }
            else if(ruleVO.getRuleCode().equalsIgnoreCase(DecisionResultConstants.CD_NON_VASE_ADDON)){
                if(nonVaseAddon)
                    valid=true;
            }
        }
        
        return valid;
    }
    
    /**
     * @override hashcode method
     * @return int hashcode
     */
    public int hashCode()   {               
        int prime = 31;
        prime = prime + (int)entityId;
        prime = prime+ (entityName==null?0:entityName.hashCode()); 
        return prime;
    }
    
    /**
     * @override equals method
     * @param obj
     * @return blloean
     */
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
       
        EntityDetailVO detailVO = (EntityDetailVO)obj;
        if(detailVO.entityId <= 0){
            return false;
        }
        else if(detailVO.entityName == null && entityName != null){
            return false;
        }
        else if(detailVO.entityId != entityId){
            return false;
        }
        else if(!detailVO.entityName.equals(entityName)){
            return false;
        }
                                
        return true;
    }

    
}
