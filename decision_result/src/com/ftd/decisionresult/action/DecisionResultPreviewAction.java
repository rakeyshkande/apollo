package com.ftd.decisionresult.action;

import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class DecisionResultPreviewAction
  extends DispatchAction
{
  private Logger logger = new Logger(DecisionResultPreviewAction.class.getName());

  public DecisionResultPreviewAction()
  {
  
  }
  public ActionForward previewDraft(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                             HttpServletResponse response)
  throws IOException, ServletException
  {
    logger.info("Entered Preview Draft");
    HashMap params = getHttpRequestParameters(request);
    request.setAttribute(DecisionResultConstants.REQ_DECISION_TYPE_CODE, params.get(DecisionResultConstants.REQ_DECISION_TYPE_CODE));
    request.setAttribute(DecisionResultConstants.REQ_DECISION_CONFIG_ID, params.get(DecisionResultConstants.REQ_DECISION_CONFIG_ID));
    request.setAttribute(DecisionResultConstants.REQ_DECISION_STATUS, DecisionResultConstants.DCSN_STATUS_DRAFT);
    request.setAttribute(DecisionResultConstants.REQ_DISPOSITION_STATUS, DecisionResultConstants.DISPOSITION_STATUS_VIEW);
    
    ActionForward forward = mapping.findForward("SUCCESS");
    return forward;
  
  }
  
  public ActionForward previewPublish(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                             HttpServletResponse response)
  throws IOException, ServletException
  {
    logger.info("Entered Preview Publish");
     HashMap params = getHttpRequestParameters(request);
    
    request.setAttribute(DecisionResultConstants.REQ_DECISION_CONFIG_ID, "");
    request.setAttribute(DecisionResultConstants.REQ_DECISION_STATUS, DecisionResultConstants.DCSN_STATUS_PUBLISHED);
    request.setAttribute(DecisionResultConstants.REQ_DISPOSITION_STATUS, DecisionResultConstants.DISPOSITION_STATUS_VIEW);
    request.setAttribute(DecisionResultConstants.REQ_DECISION_TYPE_CODE, params.get(DecisionResultConstants.REQ_DECISION_TYPE_CODE));
    
    ActionForward forward = mapping.findForward("SUCCESS");
    return forward;
  }
  
  
      /**
     * Method to retrieve all the parameters out of request and load into HM
     * @param   HttpServletRequest request
     * @return  HashMap
     */
    protected HashMap getHttpRequestParameters(HttpServletRequest request){
        HashMap result = new HashMap();
        for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); ){
            String key = (String) i.nextElement();
            String value = request.getParameter(key);
            logger.info("Preview Key:value = "+key+":"+value);
            if ((value != null) && (value.trim().length() > 0)){
                result.put(key, value.trim());
            }
        }
    
      return result;
    }
  
}
