package com.ftd.decisionresult.action;

import com.ftd.decisionresult.bo.DecisionResultBO;
import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.decisionresult.util.DBUtil;
import com.ftd.decisionresult.vo.DecisionResultStateVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

import java.io.File;
import java.io.Writer;

import java.sql.Connection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.w3c.dom.Document;


/**
 * CallDispositionAction is the action to handle the request from call disposition widget
 * @author Vinay Shivaswamy
 * @date 2/14/2011
 * @version - 1.0 for CD-4.8.0
 */
public class DecisionResultAction extends DispatchAction{
    
    Logger logger = new Logger(DecisionResultAction.class.getName());
     
    
    public DecisionResultAction() {
    }
    
    /**
     * Method to load the data into the widget
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     * @throws Exception
     */
    public ActionForward load(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception{
        String userId = null;
        Connection connection = null;
        DecisionResultBO decisionResultBO = null;
        DecisionResultStateVO stateVO = null;      
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date date = new Date();        
        try{
            HashMap params = (HashMap)getHttpRequestParameters(request);
            if(params.size()>0)
                connection = DBUtil.getInstance().getConnection();
            
            if(connection == null){
                logger.error("There was a problem getting connection to the database.");
                throw new Exception("There was a problem getting connection to the database.");
            }
            
            String actionType = (String)params.get(DecisionResultConstants.REQ_ACTION_TYPE);
            String securityToken = (String)params.get(DecisionResultConstants.SECURITY_TOKEN);
            
            //UI handles the null with error message
            if(securityToken == null){
                logger.error("There was a problem getting security token.");
                return null;
            }
            
            userId = (String)getUserId(securityToken);
            
            if(actionType == null){
                logger.error("There was a problem getting action type.");
                return null;
            }
            
            logger.info("Inside load, the action : "+actionType +" initiated on: " + dateFormat.format(date));
            
            /*
             * Based on the actionType call corresponding BO methods and get the stateVO
             * Each action instance will create its own instance of BO
             */
            if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_LOAD)){
                decisionResultBO = new DecisionResultBO(connection);    
                stateVO = (DecisionResultStateVO)decisionResultBO.doLoad(params);
            }
            else if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_NEXT)){
                decisionResultBO = new DecisionResultBO(connection);    
                stateVO = (DecisionResultStateVO)decisionResultBO.doNext(params);
            }
            else if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_BACK)){
                decisionResultBO = new DecisionResultBO(connection);    
                stateVO = (DecisionResultStateVO)decisionResultBO.doBack(params);
            }
            else if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_SUBMIT)){
                decisionResultBO = new DecisionResultBO(connection);    
                stateVO = (DecisionResultStateVO)decisionResultBO.doSubmit(params);
            }
            else if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_DECLINE)){
                decisionResultBO = new DecisionResultBO(connection);    
                stateVO = (DecisionResultStateVO)decisionResultBO.doDecline(params);
            }
            else
                return null;
                    
            //convert the stateVO into json and write to response
            if(stateVO != null){
                XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
                    public HierarchicalStreamWriter createWriter(Writer writer) {
                        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
                    }
                });
                
                logger.info("Inside load, the action : "+actionType +" completed on: " + dateFormat.format(date));
                response.getOutputStream().write( xstream.toXML(stateVO).getBytes());
            }
            
        }
        catch (Exception e) {     
            logger.error("Error occurred during the disposition started by CSR : "+userId +" on: " + dateFormat.format(date)+" Error Message: "+ e.getMessage());
            logger.error(e);
            throw e;  
        }
        finally{
            DBUtil.closeConnection(connection);
        }
        return null;
    }
        
    /**
     * Method to retrieve all the parameters out of request and load into HM
     * @param   HttpServletRequest request
     * @return  HashMap
     */
    protected HashMap getHttpRequestParameters(HttpServletRequest request){
        HashMap result = new HashMap();
        for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); ){
            String key = (String) i.nextElement();
            String value = request.getParameter(key);
            if ((value != null) && (value.trim().length() > 0)){
                result.put(key, value.trim());
            }
        }
    
      return result;
    }
    
    /*
     * Below are the test purpose methods that will be discarded later 
     */
    
    public ActionForward loadXSL(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
        logger.info("Entered loadXSL method.");
        String xslName = "index1";
        Document responseDocument = (Document)DOMUtil.getDocument();
        
        File xslFile = getXSL(xslName, mapping);
                //Transform the XSL File
                    ActionForward forward = mapping.findForward(xslName);
                    String xslFilePathAndName = forward.getPath(); //get real file name
    
                    // Change to client side transform
                    TraxUtil.getInstance().getInstance().transform(request, 
                                                                   response, 
                                                                   responseDocument, 
                                                                   xslFile, 
                                                                   xslFilePathAndName, 
                                                                   (HashMap)request.getAttribute("parameters"));
        return null;                                                               
    }
    
    private File getXSL(String xslName, ActionMapping mapping) {
        File xslFile = null;
        String xslFilePathAndName = null;
        ActionForward forward = mapping.findForward(xslName);
        xslFilePathAndName = forward.getPath(); //get real file name
        xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }       
    
    /**
     * Method to retrieve the user id based of securityToken from request.
     * @param securityToken
     * @return String userID
     * @throws Exception
     */
    private String getUserId(String securityToken)throws Exception {
        String userId = null;
        
        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) 
            userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
        
        //uncomment this code to get the userId in local testing
//        if(userId == null || userId.equals(""))
//          userId="Anonymous";
        
        return userId;
    }
}
