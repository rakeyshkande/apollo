package com.ftd.decisionresult.filter;

import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


/**
 * Filter to handle the parameters needed for widget to work
 * between page to page requests of main application.
 * @author Vinay Shivaswamy
 * @date 3/8/2011
 * @version 1.0 for Call disposiiton 4.8.0
 */
public class DecisionResultDataFilter  implements Filter{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.decisionResult.filter.DecisionResultDataFilter");

    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }

    public void destroy(){
        _filterConfig = null;
    }

    /**
     * Get and set all the parameters out of request
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try{
            HashMap parameters = new HashMap();

            if (request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS) != null)
            parameters = (HashMap) request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS);
            
            populateDecisionResultParameters(request, parameters);
            
            if (parameters.get(DecisionResultConstants.DRF_RESET_FLAG) != null && parameters.get(DecisionResultConstants.DRF_RESET_FLAG).toString().equalsIgnoreCase("Y"))
            resetDecisionResultParameters(parameters);
            
            request.setAttribute(DecisionResultConstants.CONS_APP_PARAMETERS, parameters);
            chain.doFilter(request, response);

        }
        catch (Exception ex){
            logger.error("Error in doFilter(): "+ex);
            try{
                redirectToError(response);
            }
            catch (Exception e){
                logger.error(e);
            }
        }

  }

    /**
     * Method to load the parameters from request
     * @param request
     * @param HashMap params
     */
    private void populateDecisionResultParameters(ServletRequest request, HashMap parameters){
        parameters.put(DecisionResultConstants.DRF_AVAILABLE_ENTITIES,              ((request.getParameter(DecisionResultConstants.DRF_AVAILABLE_ENTITIES)) !=null              ?request.getParameter(DecisionResultConstants.DRF_AVAILABLE_ENTITIES).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_CURRENT_LEVEL,                   ((request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL)) !=null                   ?request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_CURRENT_LEVEL_PARENT_VALUE_ID,   ((request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL_PARENT_VALUE_ID)) !=null   ?request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL_PARENT_VALUE_ID).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_CURRENT_LEVEL_SELECTED_COMMENT,  ((request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL_SELECTED_COMMENT)) !=null  ?request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL_SELECTED_COMMENT).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_CURRENT_LEVEL_SELECTED_VALUE_ID, ((request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL_SELECTED_VALUE_ID)) !=null ?request.getParameter(DecisionResultConstants.DRF_CURRENT_LEVEL_SELECTED_VALUE_ID).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_DECISION_CONFIG_ID,              ((request.getParameter(DecisionResultConstants.DRF_DECISION_CONFIG_ID)) !=null              ?request.getParameter(DecisionResultConstants.DRF_DECISION_CONFIG_ID).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_DECISION_STATUS,                 ((request.getParameter(DecisionResultConstants.DRF_DECISION_STATUS)) !=null                 ?request.getParameter(DecisionResultConstants.DRF_DECISION_STATUS).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_DECISION_TYPE_CODE,              ((request.getParameter(DecisionResultConstants.DRF_DECISION_TYPE_CODE)) !=null              ?request.getParameter(DecisionResultConstants.DRF_DECISION_TYPE_CODE).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_DISPOSITION_STATUS,              ((request.getParameter(DecisionResultConstants.DRF_DISPOSITION_STATUS)) !=null              ?request.getParameter(DecisionResultConstants.DRF_DISPOSITION_STATUS).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_DNIS_ID,                         ((request.getParameter(DecisionResultConstants.DRF_DNIS_ID)) !=null                         ?request.getParameter(DecisionResultConstants.DRF_DNIS_ID).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_OPEN_FLAG,                       ((request.getParameter(DecisionResultConstants.DRF_OPEN_FLAG)) !=null                       ?request.getParameter(DecisionResultConstants.DRF_OPEN_FLAG).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_PREVIOUS_LEVEL_VALUE_ID,         ((request.getParameter(DecisionResultConstants.DRF_PREVIOUS_LEVEL_VALUE_ID)) !=null         ?request.getParameter(DecisionResultConstants.DRF_PREVIOUS_LEVEL_VALUE_ID).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_RESET_FLAG,                      ((request.getParameter(DecisionResultConstants.DRF_RESET_FLAG)) !=null                      ?request.getParameter(DecisionResultConstants.DRF_RESET_FLAG).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_RESULT_ID,                       ((request.getParameter(DecisionResultConstants.DRF_RESULT_ID)) !=null                       ?request.getParameter(DecisionResultConstants.DRF_RESULT_ID).toString().trim()                  :""));
        parameters.put(DecisionResultConstants.DRF_SELECTED_ENTITIES,               ((request.getParameter(DecisionResultConstants.DRF_SELECTED_ENTITIES)) !=null               ?request.getParameter(DecisionResultConstants.DRF_SELECTED_ENTITIES).toString().trim()                  :""));
    }

    /**
     * Method to resets the parameters from request
     * @param request
     * @param HashMap params
     */
    private void resetDecisionResultParameters(HashMap parameters){
        parameters.put(DecisionResultConstants.DRF_AVAILABLE_ENTITIES,              "");
        parameters.put(DecisionResultConstants.DRF_DECISION_CONFIG_ID,              "");
        parameters.put(DecisionResultConstants.DRF_OPEN_FLAG,                       "");
        parameters.put(DecisionResultConstants.DRF_RESET_FLAG,                      "");
        parameters.put(DecisionResultConstants.DRF_RESULT_ID,                       "");
        parameters.put(DecisionResultConstants.DRF_SELECTED_ENTITIES,               "");
    }


    /**
     * Redirect csr to the error page.
     *
     * @param ServletResponse
     *
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private void redirectToError(ServletResponse response) throws Exception {
    //((HttpServletResponse) response).sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT, ERROR_PAGE));
    }


    /**
     * Static method to only add order to Entity List if it does not already exist
     *
     * @param request
     * @param orderDetailId
     */
    public static void updateEntity(ServletRequest request, String orderDetailIdStr) throws Exception {
        String entityListStr = null;
        HashMap<String, String> parameters = new HashMap();
    
        if (request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS) != null)
          parameters = (HashMap) request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS);
    
        // Check if passed order already exists within entity list 
        //
        entityListStr = parameters.get(DecisionResultConstants.DRF_AVAILABLE_ENTITIES);
        if ((entityListStr == null) || (entityListStr.indexOf(orderDetailIdStr) < 0))
        {
          // Order does not already exist so add it (with default values)
          if (entityListStr == null)
          {
            entityListStr = "";
          }
          else if ((entityListStr != null) && (entityListStr.trim().length() > 0))
          {
            // ...but entity list exists, so need entity delimeter before we append
            entityListStr += DecisionResultConstants.ENTITY_DELIMITER;
          }
          entityListStr += DecisionResultConstants.ENTITY_ORDER_DETAIL_ID + DecisionResultConstants.ENTITY_ELEMENT_DELIMITER + orderDetailIdStr + 
                           DecisionResultConstants.ENTITY_ELEMENT_DELIMITER + 
                           DecisionResultConstants.NOT_REQUIRED + 
                           DecisionResultConstants.ENTITY_ELEMENT_DELIMITER + 
                           DecisionResultConstants.ENTITY_STATUS_DEFAULT;
    
          parameters.put(DecisionResultConstants.DRF_AVAILABLE_ENTITIES, entityListStr);
          request.setAttribute(DecisionResultConstants.CONS_APP_PARAMETERS, parameters);
        }

    }


    /**
     * Static method to update or add order to Entity List.
     *
     * @param request
     * @param orderDetailId
     * @param requiredFlag
     */
    public static void updateEntity(ServletRequest request, String orderDetailIdStr, boolean requiredFlag) throws Exception {
        int eStart = 0;
        int eEnd = 0;
        String prologue = "";
        String epilogue = "";
        String entityListStr = null;
        String[] entityElements = null;
        String curEntity = null;
        String newEntity = null;
        String entityStatus = DecisionResultConstants.ENTITY_STATUS_DEFAULT;
        HashMap<String, String> parameters = null;
    
        // Get DCSN parameters (if any)
        //
        if (request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS) != null)
          parameters = (HashMap) request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS);
    
    
        // Check if passed entity already exists within entity list and extract current status
        //
        entityListStr = parameters.get(DecisionResultConstants.DRF_AVAILABLE_ENTITIES);
        if ((entityListStr != null) && (entityListStr.trim().length() > 0)){
          eStart = entityListStr.indexOf(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID + DecisionResultConstants.ENTITY_ELEMENT_DELIMITER + orderDetailIdStr);
          if (eStart >= 0){
            // Match was found
            eEnd = entityListStr.indexOf(DecisionResultConstants.ENTITY_DELIMITER, eStart);
            if (eEnd >= 0){
              curEntity = entityListStr.substring(eStart, eEnd);
            }
            else{
              curEntity = entityListStr.substring(eStart);
            }
            entityElements = curEntity.split(DecisionResultConstants.ENTITY_ELEMENT_DELIMITER);
            entityStatus = entityElements[DecisionResultConstants.ENTITY_ELEMENT_INDEX_STATUS];
            if (eStart > 0){
              prologue = entityListStr.substring(0, eStart);
            }
            if (eEnd > 0){
              epilogue = entityListStr.substring(eEnd);
            }
          }
          else{
            // No match found
            prologue = entityListStr + DecisionResultConstants.ENTITY_DELIMITER;
          }
        }
    
        // Build delimited string for passed entity
        newEntity = DecisionResultConstants.ENTITY_ORDER_DETAIL_ID + DecisionResultConstants.ENTITY_ELEMENT_DELIMITER + orderDetailIdStr +
                    DecisionResultConstants.ENTITY_ELEMENT_DELIMITER +
                    (requiredFlag? DecisionResultConstants.REQUIRED: DecisionResultConstants.NOT_REQUIRED) +
                    DecisionResultConstants.ENTITY_ELEMENT_DELIMITER + entityStatus;
    
        // Re-construct entity list string with updated entity then put back in DSCN parameters
        entityListStr = prologue + newEntity + epilogue;
        parameters.put(DecisionResultConstants.DRF_AVAILABLE_ENTITIES, entityListStr);
        request.setAttribute(DecisionResultConstants.CONS_APP_PARAMETERS, parameters);
    }

    /**
     * Static method to allow updates of parameter data
     */
    public static void updateParameter(ServletRequest request, String name, String value) throws Exception {
        HashMap parameters = null;
        
        if (request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS) != null){
          parameters = (HashMap)request.getAttribute(DecisionResultConstants.CONS_APP_PARAMETERS);
        }
        else{
          parameters  = new HashMap();
        }
        
        parameters.put(name, value);
        request.setAttribute(DecisionResultConstants.CONS_APP_PARAMETERS, parameters);
        }
}
