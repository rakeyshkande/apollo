package com.ftd.decisionresult.dao;

import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.decisionresult.vo.DecisionNodeRuleVO;
import com.ftd.decisionresult.vo.DecisionResultDetailVO;
import com.ftd.decisionresult.vo.DecisionResultEntityTypeName;
import com.ftd.decisionresult.vo.DecisionResultEntityVO;
import com.ftd.decisionresult.vo.DecisionResultStatus;
import com.ftd.decisionresult.vo.DecisionResultVO;
import com.ftd.decisionresult.vo.DecisionNodeVO;
import com.ftd.decisionresult.vo.EntityDetailVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * CallDispositionDAO provides DAO methods access the data related to call disposition
 * @author Vinay Shivaswamy
 * @date 2/14/2011
 * @version - 1.0 for CD-4.8.0
 */
public class DecisionResultDAO{
    Logger log = new Logger(DecisionResultDAO.class.getName());
    private Connection dbConnection = null;
    
    /**
     * @param Connection conn
     */
    public DecisionResultDAO(Connection conn){
        super();
        dbConnection = conn;
    }
    
    /**
     * This method retrieves the decision_config_id for the given decision_type_code and status
     * @param String decisionTypeCode 
     * @param String statusCode 
     * @return long decisonConfigId
     * @throws Exception
     */
    public long getConfigIdByTypeCode(String decisionTypeCode, String statusCode) throws Exception{
        Map inputs = new HashMap();
        Map outputs = null;
        
        inputs.put("IN_DECISION_TYPE_CODE", decisionTypeCode);
        inputs.put("IN_STATUS_CODE", statusCode);
        
        //Craete a data request and set the connection, statement Id, input parama 
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(dbConnection);
        dataRequest.setInputParams(inputs);
        dataRequest.setStatementID(DecisionResultConstants.GET_DECISION_CONFIG_ID);
        
        //execute the statement and process outputs
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        BigDecimal decisonConfigId = (BigDecimal)dataAccessUtil.execute(dataRequest);
        if(decisonConfigId == null)
            decisonConfigId = new BigDecimal(0);
//            throw new Exception("Decision config id is null.");

        return decisonConfigId.longValue();    
        
    }
    
    /**
     * This method retruns the DecisionResultVO for the specified resultId and decisionConfigId
     * @param long resultId
     * @param long decisionConfigId
     * @return DecisionResultVO result
     * @throws Exception
     */
    public DecisionResultVO getDecisionResultByResultId(long resultId, long decisionConfigId) throws Exception{
        Map inputs = new HashMap();
        Map outputs = null;
        
        inputs.put("IN_RESULT_ID", resultId);
        //inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
                
        //Craete a data request and set the connection, statement Id, input parama 
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(dbConnection);
        dataRequest.setInputParams(inputs);
        dataRequest.setStatementID(DecisionResultConstants.GET_RESULT_BY_ID);
        
        //execute the statement and process outputs
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        //Check if the result is null
        if(result ==  null || result.getRowCount()==0)
            throw new Exception("Couldn't retrieve DecisionResult for the resultId "+ " "+resultId);
        
        DecisionResultVO resultVO = new DecisionResultVO();
        
        while(result.next()){
            resultVO.setResultId(resultId);
            
            if (result.getObject("RESULT_ID") != null)
              resultVO.setResultId(result.getLong("RESULT_ID"));
            
            if (result.getObject("DECISION_CONFIG_ID") != null)
              resultVO.setDecisionConfigId(result.getLong("DECISION_CONFIG_ID"));
              
            if (result.getObject("CURRENT_LEVEL") != null)
               resultVO.setCurrentLevel(result.getInt("CURRENT_LEVEL"));
            
            if (result.getObject("STATUS") != null)
                if(result.getObject("STATUS").equals(DecisionResultStatus.COMPLETE))
                    resultVO.setStatus(DecisionResultStatus.COMPLETE);
                else if(result.getObject("STATUS").equals(DecisionResultStatus.INPROGRESS))
                    resultVO.setStatus(DecisionResultStatus.INPROGRESS);
                else if(result.getObject("STATUS").equals(DecisionResultStatus.VIEW))
                    resultVO.setStatus(DecisionResultStatus.VIEW);
                else if(result.getObject("STATUS").equals(DecisionResultStatus.DECLINE))
                    resultVO.setStatus(DecisionResultStatus.DECLINE);
               
            if (result.getObject("CREATED_ON") != null)
                resultVO.setCreatedOn(result.getDate("CREATED_ON"));
                
            if (result.getObject("UPDATED_ON") != null)
                resultVO.setUpdatedOn(result.getDate("UPDATED_ON"));
                
            if (result.getObject("CREATED_BY") != null)
                resultVO.setCreatedBy(result.getString("CREATED_BY"));
            
            if (result.getObject("UPDATED_BY") != null)
                resultVO.setUpdateBy(result.getString("UPDATED_BY"));
            
        }
        
        return resultVO;
    }
    
    /**
     * This method will retrieve and return the maximum level of disposition available for the given decision_config_id.
     * @param long decisionConfigId
     * @return int maxLevel
     * @throws Exception
     */
    public int getMaxLevelByConfigId (long decisionConfigId) throws Exception{
        Map inputs = new HashMap();
        Map outputs = null;
        
        inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
        
        //Craete a data request and set the connection, statement Id, input parama 
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(dbConnection);
        dataRequest.setInputParams(inputs);
        dataRequest.setStatementID(DecisionResultConstants.GET_DSCN_CONFIG_MAX_LEVEL);
        
        //execute the statement and process outputs
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        BigDecimal maxLevel = (BigDecimal)dataAccessUtil.execute(dataRequest);
        if(maxLevel == null)
             throw new Exception("No Max Levels are found for the DecisonConfigId"+" "+decisionConfigId);
        
        return maxLevel.intValue();
        
    }
    
    /**
     * This method will insert the disposition data into the DECISION_RESULT table and returns the result Id.
     * @param DecisionResultVO result
     * @return long resultId
     * @throws Exception
     */
    public Long insertDecisionResult(DecisionResultVO result) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_DECISION_CONFIG_ID",result.getDecisionConfigId());
        inputs.put("IN_STATUS",result.getStatus().name());
        inputs.put("IN_CURRENT_LEVEL",result.getCurrentLevel());
        inputs.put("IN_USER_ID",result.getCreatedBy());
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.INSERT_DECISION_RESULT);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(DecisionResultConstants.OUT_STATUS);
        if (status.equals("N")){
            String message = (String) outputs.get(DecisionResultConstants.OUT_MESSAGE);
            log.error(message);
            throw new Exception(message);
        }
        
        BigDecimal resultId = (BigDecimal)outputs.get("OUT_DCSN_FX_RESULT_ID");
        if(resultId == null)
            throw new Exception("ResultId is null after insert into Decison Result.");
            
        return resultId.longValue();
        
    }
    
    /**
     * This method will update the disposition level or status in the DECISION_RESULT table.
     * @param DecisionResultVO result
     * @return boolean status
     * @throws Exception
     */
    public boolean updateDecisionResult(DecisionResultVO result) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",result.getResultId());
        if(result.getStatus() != null)
            inputs.put("IN_STATUS",result.getStatus().name());
        inputs.put("IN_CURRENT_LEVEL",result.getCurrentLevel());
        inputs.put("IN_USER_ID",result.getUpdateBy());
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.UPDATE_DECISION_RESULT);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(DecisionResultConstants.OUT_STATUS);
        if (status.equals("N")){
            String message = (String) outputs.get(DecisionResultConstants.OUT_MESSAGE);
            log.error(message);
            throw new Exception(message);
        }
    
        return true;
    }
    
    
    /**
     * Method to delete and insert records into DCSN_FX_RESULT_ENTITIES table. 
     * This will do the destructive load i.e. delete if any and insert new entities for any resultId.
     * This is to avaiod making multiple calls to database in the absence of batch functionality.
     * @param long resultId
     * @param String entityTypeName (Ex:ORDER_DETAIL_ID, CUSTOMER_ID etc.)
     * @param String entityIds (Comma separated list Ex: 123,124,125,126)
     * @param String userId
     * @return long callDispositionId
     * @throws Exception
     */
    public boolean saveDecisionResultEntities(long resultId, String selectedEntityIds, String entityTypeName, String userId) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        inputs.put("IN_ENTITY_IDS",selectedEntityIds);
        inputs.put("IN_ENTITY_TYPE_NAME",entityTypeName);
        inputs.put("IN_USER_ID",userId);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.SAVE_DECISION_RESULT_ENTITIES);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if (status.equals("N")){
            String message = (String) outputs.get("OUT_MESSAGE");
            log.error(message);
            throw new Exception(message);
        }
    
        return true;
    }
    
    /**
     * Method to retrieve records from DCS_FX_RESULT_ENTITIES table. 
     * @param long resultId
     * @return List<DecisionResultEntityVO>
     * @throws Exception
     */
    public List<DecisionResultEntityVO> getDecisionResultEntities(long resultId) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.GET_DECISION_RESULT_ENTITIES);
        dataRequest.setInputParams(inputs);
        
        // execute the store prodcedure
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet results = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        //Check if results is null or of size 0
        if(results == null || results.getRowCount() == 0)
            return null;
            
        List<DecisionResultEntityVO> entities = new ArrayList<DecisionResultEntityVO>(); 
        while(results.next()){
            DecisionResultEntityVO entity=new DecisionResultEntityVO();
            entity.setResultId(resultId);
            
            if (results.getObject("RESULT_ENTITY_ID") != null)
              entity.setResultEntityId(results.getLong("RESULT_ENTITY_ID"));
            
            if (results.getObject("CALL_ENTITY_NAME") != null){
                if(results.getString("CALL_ENTITY_NAME").equals(DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_ORDER))
                    entity.setEntityName(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID);
                else if(results.getString("CALL_ENTITY_NAME").equals(DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_CUSTOMER))
                    entity.setEntityName(DecisionResultConstants.ENTITY_CUSTOMER_ID);
            }
                          
            if (results.getObject("CALL_ENTITY_VALUE") != null)
               entity.setEntityValue(results.getLong("CALL_ENTITY_VALUE"));
                
            entities.add(entity);
        }
        return entities;
    }
    
    /**
     * Method to insert or update DCSN_FX_RESULT_DETAILS table based on the resultId.
     * @param DecisionResultDetailVO detail
     * @return long decisionResultDetailId
     * @throws Exception
     */
    public long saveDecisionResultDetail(DecisionResultDetailVO detail) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",detail.getResultId());
        inputs.put("IN_VALUE_ID",detail.getValueId());
        inputs.put("IN_NAME",detail.getName());
        inputs.put("IN_PARENT_VALUE_ID",detail.getParentValueId());
        inputs.put("IN_COMMENT_TEXT", detail.getCommentText());
        inputs.put("IN_SCRIPT_TEXT", detail.getScriptText());
        inputs.put("IN_USER_ID",detail.getCreatedBy());
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.SAVE_DECISION_RESULT_DETAIL);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if (status.equals("N")){
            String message = (String) outputs.get("OUT_MESSAGE");
            log.error(message);
            throw new Exception(message);
        }
        
        BigDecimal resultDetailId = (BigDecimal)outputs.get("OUT_RESULT_DETAIL_ID");
        if(resultDetailId == null)
            throw new Exception("resultDetailId is null after saving Decison Result Detail for the resultId "+ detail.getResultId());
            
        return resultDetailId.longValue();
    }
    
    /**
     * Method to retrieve records from DCSN_FX_RESULT_DETAILS table based on resultId. 
     * @param long resultId
     * @return List<DecisionResultDetailVO>
     * @throws Exception
     */
    public List<DecisionResultDetailVO> getDecisionResultDetails(long resultId) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.GET_DECISION_RESULT_DETAILS);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        //Get the cachedResultSet and return null if rowCount is 0
        CachedResultSet results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if(results == null || results.getRowCount()==0)
            throw new Exception("No DecisionResultDetail VOs found for the resultId "+resultId);
            
        List<DecisionResultDetailVO> details = new ArrayList<DecisionResultDetailVO>(); 
        while(results.next()){
            DecisionResultDetailVO detail=new DecisionResultDetailVO();
            detail.setResultId(resultId);
            
            if (results.getObject("RESULT_DETAIL_ID") != null)
              detail.setResultDetailId(results.getLong("RESULT_DETAIL_ID"));
            
            if (results.getObject("VALUE_ID") != null)
              detail.setValueId(results.getLong("VALUE_ID"));
                          
            if (results.getObject("PARENT_VALUE_ID") != null)
                  detail.setParentValueId(results.getLong("PARENT_VALUE_ID"));;
            
            if (results.getObject("NAME") != null)
                detail.setName(results.getString("NAME"));
            
            if (results.getObject("LEVEL") != null)
                detail.setLevel(results.getInt("LEVEL"));
            
            if (results.getObject("COMMENT_TEXT") != null)
                detail.setCommentText(results.getString("COMMENT_TEXT"));
            
            if (results.getObject("SCRIPT_TEXT") != null)
                detail.setScriptText(results.getString("SCRIPT_TEXT"));
                
            details.add(detail);
        }
        return details;
    }
    
    
    /**
     * This method retrieves the DecisionResultDetail for the given result Id and ValueId
     * @param resultId
     * @param valueId
     * @return DecisionResultDetailVO
     * @throws Exception
     */
    public DecisionResultDetailVO getDecisionResultDetailByValueId(long resultId, long valueId, String nodeType) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        inputs.put("IN_VALUE_ID",valueId);
        inputs.put("IN_NODE_TYPE",nodeType);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.GET_RESULT_DETAIL_BY_VALUE_ID);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        //Get the cachedResultSet and return null if rowCount is 0
        CachedResultSet results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if(results == null || results.getRowCount()==0)
            return null;
            
        DecisionResultDetailVO detail = null;
        while(results.next()){
            detail=new DecisionResultDetailVO();
            detail.setResultId(resultId);
            
            if (results.getObject("RESULT_DETAIL_ID") != null)
              detail.setResultDetailId(results.getLong("RESULT_DETAIL_ID"));
            
            if (results.getObject("VALUE_ID") != null)
              detail.setValueId(results.getLong("VALUE_ID"));
                          
            if (results.getObject("PARENT_VALUE_ID") != null)
                  detail.setParentValueId(results.getLong("PARENT_VALUE_ID"));;
            
            if (results.getObject("NAME") != null)
                detail.setName(results.getString("NAME"));
            
            if (results.getObject("LEVEL") != null)
                detail.setLevel(results.getInt("LEVEL"));
            
            if (results.getObject("COMMENT_TEXT") != null)
                detail.setCommentText(results.getString("COMMENT_TEXT"));
            
            if (results.getObject("SCRIPT_TEXT") != null)
                detail.setScriptText(results.getString("SCRIPT_TEXT"));
        }

        return detail;
    }
    
    /**
     * Method to delete record from DCSN_FX_RESULT_DETAILS table based on the resultId and valueId.
     * @param long resultId
     * @param long valueId
     * @return boolean
     * @throws Exception
     */
    public boolean deleteDecisionResultDetail(long resultId, long valueId) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        inputs.put("IN_VALUE_ID",valueId);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.DELETE_DECISION_RESULT_DETAIL);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if (status.equals("N")){
            String message = (String) outputs.get("OUT_MESSAGE");
            log.error(message);
            throw new Exception(message);
        }
    
        return true;
    }
    
    /**
     * Method to delete previously selected records from DCSN_FX_RESULT_DETAILS table based on the resultId and parentValueId.
     * @param long resultId
     * @param long parentValueId
     * @return boolean
     * @throws Exception
     */
    public boolean deletePreviousResultDetails(long resultId, long parentValueId) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        inputs.put("IN_PARENT_VALUE_ID",parentValueId);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.DELETE_PREVIOUS_RESULT_DETAILS);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if (status.equals("N")){
            String message = (String) outputs.get("OUT_MESSAGE");
            log.error(message);
            throw new Exception(message);
        }
    
        return true;
    }
    
    /**
     * Method to insert records from DCSN_FX_RESULT_DETAILS into CLEAN.COMMENTS table based on the resultId.
     * Procedure will retrieve all the entities for the specified call disposition resultId and
     * retrieve detail records and inserts into comments for each entity Id
     * @param long resultId
     * @param long dnisId
     * @param String decisionTypeCode
     * @param String userId
     * @return boolean status
     * @throws Exception
     */
    public boolean saveResultEntityComments(long resultId, String decisionTypeCode, long dnisId, String userId) throws Exception{
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        //setup stored procedure input parameters 
        Map inputs = new HashMap();
        inputs.put("IN_RESULT_ID",resultId);
        inputs.put("IN_DNIS_ID",dnisId);
        inputs.put("IN_DECISION_TYPE_CODE",decisionTypeCode);
        inputs.put("IN_USER_ID",userId);
        
        // build DataRequest object 
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID(DecisionResultConstants.SAVE_RESULT_ENTITY_COMMENTS);
        dataRequest.setInputParams(inputs);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if (status.equals("N")){
            String message = (String) outputs.get("OUT_MESSAGE");
            log.error(message);
            throw new Exception(message);
        }
    
        return true;
    }
    
    /**
         * This method will query DCSN_FX_CONFIG_VALUE and DCSN_FX_CONFIG_VALUE_RULES tables based on config Id and level and parentValueId. 
         * If level is null then it gets all the nodes and corresponding rules.
         * @param long decisionConfigId
         * @param long level
         * @param long parentValueId
         * @return Lis<DecisionNodeVO> nodes 
         */
        public List<DecisionNodeVO> getNodesAndRulesByLevel (long decisionConfigId, int level, long parentValueId) throws Exception{
            Map inputs = new HashMap();
            Map outputs = null;
            
            inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
            inputs.put("IN_LEVEL", level);
            inputs.put("IN_PARENT_VALUE_ID", parentValueId);
            
            //Craete a data request and set the connection, statement Id, input parama 
            DataRequest dataRequest = new DataRequest();
            dataRequest.reset();
            dataRequest.setConnection(dbConnection);
            dataRequest.setInputParams(inputs);
            dataRequest.setStatementID(DecisionResultConstants.GET_DCSN_NODES_RULES_BY_LEVEL);
            
            //execute the statement and process outputs
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map) dataAccessUtil.execute(dataRequest);
            
            //Define a list for DecisionNodes to be returned
            List<DecisionNodeVO> nodes = new ArrayList<DecisionNodeVO>(); 
            
            CachedResultSet rsNodes = (CachedResultSet) outputs.get("OUT_NODES");
            CachedResultSet rsRules = (CachedResultSet) outputs.get("OUT_RULES");
            if(rsNodes.getRowCount()==0)
                return null;
            
            while(rsNodes.next()){
                DecisionNodeVO nodeVO = new DecisionNodeVO();
                
                if (rsNodes.getObject("DECISION_CONFIG_ID") != null)
                  nodeVO.setDecisionConfigId(rsNodes.getLong("DECISION_CONFIG_ID"));
                
                if (rsNodes.getObject("VALUE_ID") != null)
                  nodeVO.setValueId(rsNodes.getLong("VALUE_ID"));
                
                if (rsNodes.getObject("PARENT_VALUE_ID") != null)
                  nodeVO.setParentValueId(rsNodes.getLong("PARENT_VALUE_ID"));
                  
                if (rsNodes.getObject("LEVEL") != null)
                   nodeVO.setLevel(rsNodes.getInt("LEVEL"));
                
                if (rsNodes.getObject("GROUP_ORDER") != null)
                   nodeVO.setGroupOrder(rsNodes.getLong("GROUP_ORDER"));
                 
                if (rsNodes.getObject("NAME") != null)
                   nodeVO.setName(rsNodes.getString("NAME"));
                
                if (rsNodes.getObject("SCRIPT_TEXT") != null)
                   nodeVO.setScriptText(rsNodes.getString("SCRIPT_TEXT"));
                   
                if (rsNodes.getObject("ALLOW_COMMENTS_FLAG") != null)
                   nodeVO.setAllowComments((rsNodes.getString("ALLOW_COMMENTS_FLAG").equalsIgnoreCase("Y")?true:false));   
                
                if (rsNodes.getObject("IS_ACTIVE_FLAG") != null)
                   nodeVO.setActive((rsNodes.getString("IS_ACTIVE_FLAG").equalsIgnoreCase("Y")?true:false));
                
                if (rsNodes.getObject("ROW_COUNT") != null)
                    if(rsNodes.getInt("ROW_COUNT")>0)
                        nodeVO.setHasChildNodes(true);
                    
                nodes.add(nodeVO);
            }
            
            //Load the rules into the nodes
            if(rsRules.getRowCount()!=0){
                while(rsRules.next()){
                    for(DecisionNodeVO nodeVO:nodes){
                        List<DecisionNodeRuleVO> rules = nodeVO.getRules();
                        if(rsRules.getLong("VALUE_ID") == nodeVO.getValueId()){
                            DecisionNodeRuleVO ruleVO = new DecisionNodeRuleVO();
                            ruleVO.setValueId(rsRules.getLong("VALUE_ID"));
                            ruleVO.setDecisionConfigId(nodeVO.getDecisionConfigId());
                            ruleVO.setRuleCode(rsRules.getString("RULE_CODE"));
                            rules.add(ruleVO);
                        }
                    }
                }
                
            }
            
            return nodes;
        }
    
       
    /**
     * This method retrieves the parent level DecisionNodeVO based on configId and parentValueId as valueId in the where predicate
     * @param decisionConfigId
     * @param parentValueId
     * @return DecisionNodeVO
     * @throws Exception
     */
    public DecisionNodeVO getParentNode(long decisionConfigId, long parentValueId) throws Exception{
        Map inputs = new HashMap();
        Map outputs = null;
        
        inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
        inputs.put("IN_VALUE_ID", parentValueId);
        
        //Craete a data request and set the connection, statement Id, input parama 
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(dbConnection);
        dataRequest.setInputParams(inputs);
        dataRequest.setStatementID(DecisionResultConstants.GET_DCSN_NODE);
        
        //execute the statement and process outputs
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);
        
        CachedResultSet crsNode = (CachedResultSet) outputs.get("OUT_NODE"); 
        CachedResultSet crsRule = (CachedResultSet) outputs.get("OUT_RULE"); 
        if(crsNode == null || crsNode.getRowCount()==0)
            throw new Exception("Couldn't retrieve the node for the given decisionConfigId "+decisionConfigId+" and valueId "+parentValueId);
        
        DecisionNodeVO node = new DecisionNodeVO();
        
        while(crsNode.next()){
            if (crsNode.getObject("DECISION_CONFIG_ID") != null)
              node.setDecisionConfigId(crsNode.getLong("DECISION_CONFIG_ID"));
            
            if (crsNode.getObject("VALUE_ID") != null)
              node.setValueId(crsNode.getLong("VALUE_ID"));
            
            if (crsNode.getObject("PARENT_VALUE_ID") != null)
              node.setParentValueId(crsNode.getLong("PARENT_VALUE_ID"));
              
            if (crsNode.getObject("LEVEL") != null)
               node.setLevel(crsNode.getInt("LEVEL"));
            
            if (crsNode.getObject("GROUP_ORDER") != null)
               node.setGroupOrder(crsNode.getLong("GROUP_ORDER"));
             
            if (crsNode.getObject("NAME") != null)
               node.setName(crsNode.getString("NAME"));
            
            if (crsNode.getObject("SCRIPT_TEXT") != null)
               node.setScriptText(crsNode.getString("SCRIPT_TEXT"));
               
            if (crsNode.getObject("ALLOW_COMMENTS_FLAG") != null)
               node.setAllowComments((crsNode.getString("ALLOW_COMMENTS_FLAG").equalsIgnoreCase("Y")?true:false));   
            
            if (crsNode.getObject("IS_ACTIVE_FLAG") != null)
               node.setActive((crsNode.getString("IS_ACTIVE_FLAG").equalsIgnoreCase("Y")?true:false));
            
        }
        
        if(crsRule.getRowCount()!=0){
            List<DecisionNodeRuleVO> rules = new ArrayList<DecisionNodeRuleVO>();
            while(crsRule.next()){
                DecisionNodeRuleVO ruleVO = new DecisionNodeRuleVO();
                ruleVO.setValueId(crsRule.getLong("VALUE_ID"));
                ruleVO.setDecisionConfigId(node.getDecisionConfigId());
                ruleVO.setRuleCode(crsRule.getString("RULE_CODE"));
                rules.add(ruleVO);
            }
            node.setRules(rules);
        }
        
        return node;
    }
    
    
    
    /**
     * This method retrieves all the details available for the entity Id so that app can use it to 
     * validate against dispo level values to display or not and also to check if call dispo is mandatory or not. 
     * The EntityDetails in this case usually order details for the given comma separated order Ids
     * @param String entityIds
     * @return List<EntityDetailVO>
     * @throws Exception
     */
    public List<EntityDetailVO> getEntityTypeOrderDetails(String entityIds ) throws Exception{
        Map inputs = new HashMap();
        Map outputs = null;
        
        inputs.put("IN_ENTITY_IDS", entityIds);
        
        //Craete a data request and set the connection, statement Id, input parama 
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(dbConnection);
        dataRequest.setInputParams(inputs);
        dataRequest.setStatementID(DecisionResultConstants.GET_ORDER_INFO_BY_ORD_DET_IDS);
        
        //execute the statement and process outputs
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet results = (CachedResultSet) dataAccessUtil.execute(dataRequest); 
        if(results.getRowCount()==0)
            return null;
        
        //Define a list for DecisionNodes to be returned
        List<EntityDetailVO> entityDetails = new ArrayList<EntityDetailVO>(); 
        
        while(results.next()){
            EntityDetailVO entityDetailVO = new EntityDetailVO();
            //since this method handles only the type order_detail_id 
            entityDetailVO.setEntityName(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID);
            
            if (results.getObject("ORDER_DETAIL_ID") != null)
                entityDetailVO.setEntityId(results.getLong("ORDER_DETAIL_ID"));
            if (results.getObject("EXTERNAL_ORDER_NUMBER") != null)
                entityDetailVO.setExternalOrderNumber(results.getString("EXTERNAL_ORDER_NUMBER"));
            if (results.getObject("FIRST_NAME") != null)
                entityDetailVO.setFirstName(results.getString("FIRST_NAME"));
            if (results.getObject("LAST_NAME") != null)
                entityDetailVO.setLastName(results.getString("LAST_NAME"));
            if (results.getObject("ORDER_DATE_STR") != null)
                entityDetailVO.setOrderDate(results.getString("ORDER_DATE_STR"));   
            if (results.getObject("DELIVERY_TYPE") != null){
                if(results.getString("DELIVERY_TYPE").equalsIgnoreCase(DecisionResultConstants.INTERNATIONAL_DELIVERY))
                    entityDetailVO.setInternationalDelivery(true);
                else 
                    entityDetailVO.setInternationalDelivery(false);
            }
            
            if (results.getObject("FLORIST_OR_VENDOR") != null){
                if(results.getString("FLORIST_OR_VENDOR").equalsIgnoreCase(DecisionResultConstants.FLORIST_DELIVERED))
                    entityDetailVO.setFloristDelivered(true);
                else if(results.getString("FLORIST_OR_VENDOR").equalsIgnoreCase(DecisionResultConstants.VENDOR_DELIVERED))
                    entityDetailVO.setVendorDelivered(true);
            }
            
            if (results.getObject("VASE_ADDON") != null){
                if(results.getString("VASE_ADDON").equalsIgnoreCase(DecisionResultConstants.TRUE_STATE))
                    entityDetailVO.setVaseAddon(true);
                else 
                    entityDetailVO.setVaseAddon(false);
            }
            
            if (results.getObject("NON_VASE_ADDON") != null){
                if(results.getString("NON_VASE_ADDON").equalsIgnoreCase(DecisionResultConstants.TRUE_STATE))
                    entityDetailVO.setNonVaseAddon(true);
                else 
                    entityDetailVO.setNonVaseAddon(false);
            }
            
            entityDetails.add(entityDetailVO);
            
        }
        
        return entityDetails;
    }
    
}
