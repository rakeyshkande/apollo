package com.ftd.decisionresult.bo;

import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.decisionresult.dao.DecisionResultDAO;
import com.ftd.decisionresult.vo.DecisionNodeRuleVO;
import com.ftd.decisionresult.vo.DecisionNodeVO;
import com.ftd.decisionresult.vo.DecisionResultDetailVO;
import com.ftd.decisionresult.vo.DecisionResultEntityVO;
import com.ftd.decisionresult.vo.DecisionResultStateVO;
import com.ftd.decisionresult.vo.DecisionResultStatus;
import com.ftd.decisionresult.vo.DecisionResultVO;
import com.ftd.decisionresult.vo.EntityDetailVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.sql.Connection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * CallDispositionBO provides BO methods for business logic and access to the DecisionResultDAO
 * 5 Main public methods to drive the navigation in the widget
 *  -doLoad, doNext, doBack, doSubmit, doDecline
 *  -All these methods
 *      .throws Exception and action class treats it as runtime exception
 *      .returns null for stateVO if validtion failures under acceptable conditions
 * @author Vinay Shivaswamy
 * @date 2/14/2011
 * @version -   1.0 for Call Disposition -4.8.0
 *              1.1 doLoad() released
 *              1.2 doNext() released - navigation (using next) works without any order validation
 *              1.3 doBack() released
 *              1.4 doSubmit() released
 *              1.5 doDecline() released
 *              1.6 Fixes for submit() and decline()
 *              1.6.1 Bug fixes
 *              1.6.2/3/4/5 Bug fixes
 *              1.7 Major validation fix for defect 9032 that affected next and back
 */
public class DecisionResultBO{
    Logger logger = new Logger(DecisionResultBO.class.getName());
    private Connection dbConnection = null;
    private DecisionResultDAO decisionResultDAO = null;
    
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    
    //Variables declaration 
     DecisionResultStateVO stateVO = null;
     DecisionResultVO resultVO = null;
     HashMap entityMap = null;
     String availableEntitiesString = null, selectedEntitiesString = null;
     List<EntityDetailVO> entities = null, orderEntities = null, customerEntities = null;
     List<EntityDetailVO> orderDetailIdList = null, customerDetailIdList = null;
     List<DecisionResultEntityVO> existingEntities = null, updatedEntities = null;
     List<DecisionResultDetailVO> details = null;
     List<DecisionNodeVO> childNodes, displayNodes = null;
     int maxLevel=0;
     int minLevel= DecisionResultConstants.DISPO_MIN_LEVEL;
     int currentLevel=0, previousLevel = 0;
     long currentSelectedValueId = 0, previousSelectedValueId = 0, currentParentValueId = 0;
     boolean next = false, back = false, submit = false, decline = false, dispoEnds = false;
     
     long resultId = 0, decisionConfigId = 0;
     
    /**
     * constructor
     * @param conn
     */
    public DecisionResultBO(Connection conn){
        super();
        dbConnection = conn;
        decisionResultDAO = new DecisionResultDAO(dbConnection);
    }
    
    /**
     * This method will handles all the logic to load the widget
     * @param decisionTypeCode
     * @param HashMap params
     * @return DecisionResultStateVO
     * @throws Exception
     */
    public DecisionResultStateVO doLoad(HashMap params) throws Exception{
       
        
        String userId = (String)getUserId((String)params.get(DecisionResultConstants.SECURITY_TOKEN));
        String dispositionStatus = (String)params.get(DecisionResultConstants.REQ_DISPOSITION_STATUS);
        String decisionResultId = (String)params.get(DecisionResultConstants.REQ_RESULT_ID); 
        String configId = (String)params.get(DecisionResultConstants.REQ_DECISION_CONFIG_ID);
        
        /*
         * Request comes from either COM or Admin, if it is from Admin then decisionConfigId is expected to drive the widget for the first time.
         * Retrieve the decisionConfigId from request, if null.
         * Retrieve the dispositionStatus from request, if null then,
         * Use DCSN_STATUS_PUBLISH and CALL_DECISION_TYPE_CODE
         * Retrieve the DecisionConfigId by calling BO.getDecisionConfigId() by passing the CALL_DISPOSITION_TYPE_CODE and DCSN_STATUS_PUBLISHED
         */
        
        if(configId==null){
            String decisionStatus = (String)params.get(DecisionResultConstants.REQ_DECISION_STATUS);
            String decisionTypeCode = (String)params.get(DecisionResultConstants.REQ_DECISION_TYPE_CODE);
            if(decisionTypeCode == null)
                decisionTypeCode = DecisionResultConstants.CALL_DISPOSITION_TYPE_CODE;
            if(decisionStatus == null || decisionStatus.equalsIgnoreCase(DecisionResultConstants.DCSN_STATUS_PUBLISHED))
                decisionStatus = DecisionResultConstants.DCSN_STATUS_PUBLISHED;
            else if(decisionStatus.equalsIgnoreCase(DecisionResultConstants.DCSN_STATUS_DRAFT))
                decisionStatus = DecisionResultConstants.DCSN_STATUS_DRAFT;
            
            decisionConfigId = getDecisionConfigId(decisionTypeCode, decisionStatus);
        }
        else
            decisionConfigId = Long.parseLong(configId);
        
        //if decisionConfigId == null then return the stateVO with dispositionAvailable flag to false.
        if(decisionConfigId == 0){
            stateVO = new DecisionResultStateVO();
            stateVO.setDispositionAvailable(false);
            return stateVO;
        }
        
        //Now we have decisionConfigId so lets get the max levels of disposition available for this Id.
        maxLevel = getMaxLevelByConfig(decisionConfigId);
        
        /*
         * Retrieve the dispositionResultId from the request 
         * If dispositionResultId is null then start new disposition
         *  Retrieve the dispositionResultId by calling insertCallDisposition(userId, decisionConfigId) that inserts a record and returns dispositionResultId.
         *  Set the currentLevel= DISPO_MIN_LEVEL;
         *  Set the status VIEW if dispoStatus=�view� else INPROGRESS
         * Else if dispositionResultId is not null
         *  Retrieve the current DecisionResultVO by calling BO.getCurrentLevelByDispositionId(dispositionResultId)
         *  Retrieve currentLevelParentValueId from the request, if null then 
         *      Use currentLevelParentValueId=null since it should be start.
         */
        if(decisionResultId == null){
            //start the new disposition
            minLevel = DecisionResultConstants.DISPO_MIN_LEVEL;
            resultVO = new DecisionResultVO();
            resultVO.setDecisionConfigId(decisionConfigId);
            resultVO.setCreatedBy(userId);
            resultVO.setCurrentLevel(minLevel);
            
            if(dispositionStatus != null && dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW))
                resultVO.setStatus(DecisionResultStatus.VIEW);
            else
                resultVO.setStatus(DecisionResultStatus.INPROGRESS);
            
            //Now insert the decision result    
            resultId = insertDecisionResult(resultVO);
            resultVO.setResultId(resultId);
        }
        else{
            //retrieve the decisionResultVO based on resultId
            resultId = Long.parseLong(decisionResultId);
            resultVO = getDecisionResultByResultId(resultId,decisionConfigId);
        }
        
        /*
         * Get the current level from request
         * If the level from request is null then use the level from resultVO i.e minLevel
         */
        String level = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL);
        if(level == null)
            currentLevel = resultVO.getCurrentLevel();
        else 
            currentLevel = Integer.parseInt(level);
        
        /*
         * Get the parent valueNode based on ParentLevel ValueId
         * Use this valueNode as a parentNode in stateVo for UI to use it to display script
         * If the parentValueId is null then it is assumed to be be start of disposition so go with 0 value
         * SP will replace the 0 with null as parentValueID
         */
        DecisionNodeVO parentNode = null;
        
        String parentValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_PARENT_VALUE_ID);
        if(parentValueId != null)
            currentParentValueId = new Long(parentValueId).longValue();
            
        parentNode = getParentNode(decisionConfigId,currentParentValueId);
        
        //If the parentNode is null then return the stateVO with dispositionAvailable flag to false.
        if(parentNode == null){
            stateVO = new DecisionResultStateVO();
            stateVO.setDispositionAvailable(false);
            return stateVO;
        }
            
        /*
         * Get the child nodes based on the current level & parentNode ValueId 
         * If the childNodes are not found then return the stateVO with dispositionAvailable flag to false.
         */
        childNodes = getNodesAndRulesByLevel(decisionConfigId,currentLevel,parentNode.getValueId());
        if(childNodes == null || childNodes.size()==0){
            stateVO = new DecisionResultStateVO();
            stateVO.setDispositionAvailable(false);
            return stateVO;
        }
        
        /*
         * Get the current selected value and comment
         * Set it to the child node
         */
        String selectedValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_VALUE_ID);
        String selectedComment = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_COMMENT);
        if(selectedValueId != null){
            currentSelectedValueId = new Long(selectedValueId).longValue();
            setSelectedNode(childNodes, currentSelectedValueId, selectedComment);
        }
        
        /*
         * if dispositionStatus == view only do this
         */
        if(dispositionStatus != null && dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)) {
            
            //Add all the childnodes
            displayNodes = new ArrayList(); 
            displayNodes.addAll(childNodes);
            
            //Get the list<DecisionResultDetailVO> of all selected values for this disposition
            details = getDecisionResultDetails(resultId);
            //set the details into resultVO
            resultVO.setDetails(details);
            
            //Now set the next, back,etc flags.
            if(currentLevel<maxLevel && currentLevel>minLevel){
                next = true;
                back = true;
            }
            else if(currentLevel==minLevel){
                next = true;
            }
            else if(currentLevel==maxLevel){
                back = true;
                //dispoEnds = true;
                submit = true;
                
            }
            
            //Load the stateVO 
            stateVO = (DecisionResultStateVO)buildStateVO(null,null,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
            
        } //End of else dispositionStatus != view
        else{
            /*  
             * Handle entities only when dispositionStatus != �view� 
             * EntityMap will have following key/value,
                availableEntitiesString - string representation of available entities from UI
                selectedEntitiesString - string representation of selected entities from UI
                orderDetailIdList - List<EntityDetailVO> of available order type entities only from availableEntitiesString
                customerIdList - List<EntityDetailVO> of available customer type entities only from availableEntitiesString
                entities - List<EntityDetailVO> of all available entities with details from availableEntitiesString
                orderEntities - List<EntityDetailVO> of all available order type entities with details from availableEntitiesString
                customerEntities - List<EntityDetailVO> of all available customer type entities with details from availableEntitiesString
                existingEntityList - List<DecisionResultEntityVO> of all existing entities for teh resultId
             */
             entityMap = manageEntities(params, String.valueOf(resultId));
             
            //preliminary check to see if the validation is required
            boolean validationRequired = false;
            //Get the dispositionRequired flag to set submit or decline
            boolean dispositionRequired = false;
            
            //available entities
            String availableEntities = (String)params.get(DecisionResultConstants.REQ_AVAILABLE_ENTITIES);
            
            //Now save the entities
            selectedEntitiesString = (String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING);
            if(selectedEntitiesString != null && selectedEntitiesString.length()>0)
                saveDecisionResultEntities(resultId,selectedEntitiesString, DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_ORDER, userId);
             
            //get the exisitng list of entities
            existingEntities = (List<DecisionResultEntityVO>)entityMap.get(DecisionResultConstants.ENTITY_EXISTING_LIST);
            
            //Get the entities out of entityMap
            entities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ALL_DETAILS_LIST);
            
            //do the preliminary check to see if the validation required or not
            validationRequired = validationRequiredCheck((String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING), existingEntities);
            //do the disposition required check to set the submit or decline flags
            dispositionRequired = isDispsotionRequired(entities);
            
            /*
             * If validationRequired is true then validate the child nodes to get displayNodes
             * !! NOTE: Only order information is considered for any business rule validation for now, so this is not future proof
             */
            if(validationRequired){
                orderEntities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ORDER_DETAILS_LIST);
                displayNodes = validateNodes(childNodes, orderEntities);
                if(displayNodes==null || displayNodes.size()==0){
                    //If this is the start of the disposition and no other previous level to validate return the stateVO with dispositionAvailable flag to false.
                    if(parentNode.getParentValueId() == -1){
                        stateVO = new DecisionResultStateVO();
                        stateVO.setDispositionAvailable(false);
                        return stateVO;
                    }
                        
                    //displayNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel-1, parentNode.getParentValueId());
                    //set the selected node for this list of nodes
                    //setSelectedNode(displayNodes, currentSelectedValueId);
                    
                    //get the new parent node
                    parentNode = getParentNode(decisionConfigId, parentNode.getParentValueId());
                    
                    //Mark the end of disposition
                    dispoEnds = true;
                    //if(dispositionRequired)
                        submit=true;
                    //else
                    //    decline = true;
                }
                
            }
            else{
                displayNodes = new ArrayList(); 
                displayNodes.addAll(childNodes);
            }
            
            /*
             * Get the list<DecisionResultDetailVO> of all selected values of the disposition
             */
            details = getDecisionResultDetails(resultId);
            //set the details into resultVO
            resultVO.setDetails(details);
            
            
            updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
            resultVO.setEntities(updatedEntities);
            
            //Now set the next, back,etc flags.
            if(currentLevel<maxLevel && currentLevel>minLevel){
                next = true;
                back = true;
            }
            else if(currentLevel==minLevel){
                next = true;
            }
            else if(currentLevel==maxLevel){
                back = true;
                dispoEnds = true;
                //if(dispositionRequired)
                    submit = true;
                //else
                //    decline = true;
            }
            
            //Load the stateVO
            stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
            
        }//End of else dispositionStatus != view
        
        //Ha! finally returning stateVO
        return stateVO;
    }
    
    /**
     * This method will handles all the logic when the next action performed in the widget
     * @param HashMap params
     * @return DecisionResultStateVO
     * @throws Exception
     */
    public DecisionResultStateVO doNext(HashMap params) throws Exception{
        DecisionResultStateVO stateVO = null;
        /*
         * Retrieve the decisionConfigId from request. If null throw runtime Exception.
         * Retrieve the callDispositionId from request. If null throw runtime Exception.
         * Retrieve the currentLevel from request, tells the current step
         * Retrieve the currentParentValueId from request, tells the level with script used.
         * Retrieve the currentSelectedValueId from request, tells the selections.
         * Retrieve the currentSelectedName from request, tells the selections.
         * Retrieve the currentComment from request.
         * Retrieve the isDispoEnds from request. True, if this is the last step of disposition.
         * Retrieve the actionType=next, submit or decline from request.
         * Retrieve the isDispoRequired from request. True, if required.
         * Retrieve the dnisId from request. 
         * 
         * Get the maxLevel by calling loadMaxLevelByConfigId(configId).
         */
        String dispositionStatus = (String)params.get(DecisionResultConstants.REQ_DISPOSITION_STATUS);
        String decisionResultId = (String)params.get(DecisionResultConstants.REQ_RESULT_ID); 
            if(decisionResultId == null)
                throw new Exception("The resultId is null for this disposition event.");
        String configId = (String)params.get(DecisionResultConstants.REQ_DECISION_CONFIG_ID);
            if(configId == null)
                throw new Exception("The configId is null for this disposition event.");
        
        String level = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL);
        String parentValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_PARENT_VALUE_ID); 
        if(parentValueId == null)
            throw new Exception("The parent Value Id is null for this disposition event.");
            
        String selectedValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_VALUE_ID);
        if(selectedValueId == null)
            throw new Exception("The selected Value Id is null for this disposition event.");
            
        String selectedComment = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_COMMENT);
        String dnisId = (String)params.get(DecisionResultConstants.REQ_DNIS_ID);
        
        //available entities
        String availableEntities = (String)params.get(DecisionResultConstants.REQ_AVAILABLE_ENTITIES);
        
        long resultId = Long.parseLong(decisionResultId);
        currentSelectedValueId = Long.parseLong(selectedValueId);
        currentParentValueId = Long.parseLong(parentValueId);
        decisionConfigId = Long.parseLong(configId);
        
        //Get the decisionResultVO for the param resultId
        resultVO = getDecisionResultByResultId(resultId,resultId);
         
        //Get the userId by calling getUserId(securityToken)
        String userId = (String)getUserId((String)params.get(DecisionResultConstants.SECURITY_TOKEN));
        
        //Now we have decisionConfigId so lets get the max levels of disposition available for this Id.
        maxLevel = getMaxLevelByConfig(Long.parseLong(configId));
        
        //get the parentValueNode
        DecisionNodeVO parentNode = getParentNode(decisionConfigId,currentParentValueId);
        if(parentNode == null)
           throw new Exception("The ValueNode can not be found for the parent ValueID passed in: "+currentParentValueId);
           
        //Get the currentselecetd node
        DecisionNodeVO currentSelectedNode = getNode(decisionConfigId, currentSelectedValueId);
        if(currentSelectedNode == null)
            throw new Exception("Error while getting the ValueNode for the current selected valueId."+currentSelectedValueId);
         
        /*
         * Get the previous selected result detail
         * if previous detail is not same as currentDetail
         *      delete subsequent selections
         */
         DecisionResultDetailVO previousDetailVO = getDecisionResultDetailByValueId(resultId, currentParentValueId, DecisionResultConstants.PARENT_VALUE_ID);
         if(previousDetailVO != null){
             if(previousDetailVO.getValueId() != currentSelectedValueId){
                 deletePreviousResultDetail(resultId, currentParentValueId);
             }
         }

       /*
        * Build and save the DecisionResultDetailVO using selection value/comment etc. 
        * saveDecisionResultDetail(DecisionResultDetailVO) would update if parentValueId exists.
        */ 
        DecisionResultDetailVO detailVO = new DecisionResultDetailVO();
        detailVO.setResultId(Long.parseLong(decisionResultId));
        detailVO.setValueId(currentSelectedNode.getValueId());
        detailVO.setParentValueId(currentSelectedNode.getParentValueId());
        detailVO.setName(currentSelectedNode.getName());
        detailVO.setCommentText(selectedComment);
        detailVO.setScriptText(currentSelectedNode.getScriptText());
        detailVO.setCreatedBy(userId);
        detailVO.setUpdateBy(userId);
        
        long resultDetailId = saveDecisionResultDetail(detailVO);
        detailVO.setResultDetailId(resultDetailId);
        
        /*
         * validationRequired is a preliminary check to see if the validation is required
         * dispositionRequired is flag to set submit or decline
         */
         boolean validationRequired = false; 
         boolean dispositionRequired = false;
         boolean entitiesSelected = false;
         
        /* Handle entities only when dispositionStatus != �view� 
         * EntityMap will have following key/value,
            availableEntitiesString - string representation of available entities from UI
            selectedEntitiesString - string representation of selected entities from UI
            orderDetailIdList - List<EntityDetailVO> of available order type entities only from availableEntitiesString
            customerIdList - List<EntityDetailVO> of available customer type entities only from availableEntitiesString
            entities - List<EntityDetailVO> of all available entities with details from availableEntitiesString
            orderEntities - List<EntityDetailVO> of all available order type entities with details from availableEntitiesString
            customerEntities - List<EntityDetailVO> of all available customer type entities with details from availableEntitiesString
            existingEntityList - List<DecisionResultEntityVO> of all existing entities for teh resultId
         */
        if(dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)) {
            entityMap = manageEntities(params, decisionResultId);
            //Now save the selectedEntities
            if(entityMap != null){
                selectedEntitiesString = (String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING);
                if(selectedEntitiesString != null && selectedEntitiesString.length()>0){
                    saveDecisionResultEntities(resultId,selectedEntitiesString, DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_ORDER, userId);
                    entitiesSelected = true;   
                }
             
                //Get the entities out of entityMap
                entities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ALL_DETAILS_LIST);
                
                //get the exisitng list of entities
                existingEntities = (List<DecisionResultEntityVO>)entityMap.get(DecisionResultConstants.ENTITY_EXISTING_LIST);
                
                //preliminary check to see if the validation is required
                validationRequired = validationRequiredCheck((String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING), existingEntities);
                
                //Get the dispositionRequired flag to set submit or decline
                dispositionRequired = isDispsotionRequired(entities);
            }
        }
        
        
        //Get the current level from request
        if(level == null)
            currentLevel = resultVO.getCurrentLevel();
        else 
            currentLevel = Integer.parseInt(level);
        
        
        List<DecisionNodeVO> childNodes = null;
        int nextLevel=currentLevel;
        
        if(currentLevel<maxLevel){
            nextLevel++;
            
            //Get the List<DecisionNodeVO> of child nodes by calling getNodes(nextLevel, configId, selectedCurrentValueId).
            childNodes = getNodesAndRulesByLevel(decisionConfigId, nextLevel, currentSelectedValueId);
             
            if(dispositionStatus != null && dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)){
                /*
                 * If there are no child Nodes for the selected valueNode then return the
                 * same level nodes with selected node set and submit flag set.
                 */
                if(childNodes == null || childNodes.size()==0){
                    childNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentParentValueId);
                    //If the childnodes are null here at the root then return stateVO with dispositionAvailable = false
                     if(parentNode.getParentValueId() == -1){
                        if(childNodes == null || childNodes.size()==0){
                            stateVO = new DecisionResultStateVO();
                            stateVO.setDispositionAvailable(false);
                            return stateVO;
                        }
                     }
                     
                     //if childnodes are not null then set the selected node
                     if(childNodes != null && childNodes.size()>0){
                         setSelectedNode(childNodes, detailVO.getValueId());
                         currentLevel = maxLevel;
                     }
                     
                     //set the next, back flag also here
                     if(currentLevel<maxLevel && currentLevel>minLevel){
                         next = true;
                         back = true;
                     } 
                     else if(currentLevel==maxLevel){
                         dispoEnds = true;
                         back = true;
                         submit=true;
                         
                     }
                     else if(currentLevel==minLevel){
                         next = true;
                     }
                 
                }
                else{
                    parentNode = getParentNode(decisionConfigId, currentSelectedValueId);
                    resultVO.setCurrentLevel(nextLevel);
                    updateDecisionResult(resultVO);
                    //set the next, back flag also here
                    if(nextLevel<maxLevel && nextLevel>minLevel){
                        next = true;
                        back = true;
                    } 
                    else if(nextLevel==maxLevel){
                        dispoEnds = true;
                        back = true;
                        submit=true;
                        
                    }
                    else if(nextLevel==minLevel){
                        next = true;
                    }
                    //assign the nextLevel to currentLevel
                    currentLevel = nextLevel;
                }
                
                //assign the         
                displayNodes = new ArrayList(); 
                displayNodes.addAll(childNodes);
                
                /*
                 * Get the list<DecisionResultDetailVO> of all selected values of the disposition
                 */
                details = getDecisionResultDetails(resultId);
                
                DecisionResultDetailVO selectedDetailVO = null;
                for (int i = 0; i < details.size(); i++){
                  selectedDetailVO = (DecisionResultDetailVO) details.get(i);
                  if (selectedDetailVO.getLevel() == currentLevel)
                    break;
                }
                
                if(selectedDetailVO != null)
                    setSelectedNode(displayNodes, selectedDetailVO.getValueId(), selectedDetailVO.getCommentText());
                    
                //set the details and updatedEntities into resultVO
                resultVO.setDetails(details);
                
                //Load the stateVO 
                stateVO = (DecisionResultStateVO)buildStateVO(null,null,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                return stateVO;
            }
            else if(dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW) || validationRequired) {
                orderEntities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ORDER_DETAILS_LIST);
                    
                /*
                 * Changes due to Defect 9032 states - validate nodes from step 1 instead of current step and work backword.
                 */
                List<DecisionResultDetailVO> details = getDecisionResultDetails(resultId); 
                boolean allPreviousNodesValid = validatePreviousSelections(details, orderEntities, decisionConfigId, resultId);
                 
                //Now get the valid previous selection if allPreviousNodesValid == false
                if(!allPreviousNodesValid){
                    DecisionResultDetailVO currentValidDetail = null;
                    details = getDecisionResultDetails(resultId);
                    //reverse the order of detail based on level
                    Collections.reverse(details);
                    if(details != null && !details.isEmpty())
                        currentValidDetail = details.iterator().next();
                    
                    //simply return null, since this is an error condition
                    if(currentValidDetail == null)
                        return null;
                        
                    //get the childnodes for the currentValidDetail node
                    currentLevel = currentValidDetail.getLevel()+1;
                    childNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentValidDetail.getValueId());
                    //validate the childnodes
                    if(validationRequired)
                        displayNodes = validateNodes(childNodes,orderEntities);
                    else{
                        displayNodes = new ArrayList(); 
                        displayNodes.addAll(childNodes);
                    }
                        
                    if(displayNodes == null || displayNodes.size()==0){
                        //set the parent node for this detail using parentValueId
                        currentLevel = currentValidDetail.getLevel();
                        parentNode = getParentNode(decisionConfigId,currentValidDetail.getParentValueId());
                        childNodes = getNodesAndRulesByLevel(decisionConfigId,currentLevel,currentValidDetail.getParentValueId());
                        displayNodes = validateNodes(childNodes,orderEntities);
                        //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                        if(parentNode.getParentValueId() == -1){
                            if(displayNodes == null || displayNodes.size()==0){
                                stateVO = new DecisionResultStateVO();
                                stateVO.setDispositionAvailable(false);
                                return stateVO;
                            }
                        }
                        
                        //set the childnodes flag to false
                         for (DecisionNodeVO node: displayNodes){
                           if (node.getValueId() == currentValidDetail.getValueId())
                                 node.setHasChildNodes(false);
                         }
                        
                    }
                    else{
                        parentNode = getParentNode(decisionConfigId,currentValidDetail.getValueId());
                    }
                    
                    //set the Selected Node
                    setSelectedNode(displayNodes, currentValidDetail.getValueId(), currentValidDetail.getCommentText());
                                         
                    //set the next, back flag also here
                    if(currentLevel<maxLevel && currentLevel>minLevel){
                        next = true;
                        back = true;
                    } 
                    else if(currentLevel==maxLevel){
                        dispoEnds = true;
                        back = true;
                        //if(dispositionRequired || !entitiesSelected)
                            submit=true;
                        //else
                        //    decline = true;
                    }
                    else if(currentLevel==minLevel){
                        next = true;
                    }
                     
                    //Update the current level into resultVO
                    resultVO.setCurrentLevel(currentLevel);
                    updateDecisionResult(resultVO);
                      
                    //Update the details into resultVO
                    Collections.reverse(details);
                    resultVO.setDetails(details);
                      
                    //Update the entities into resultVO
                    updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
                    resultVO.setEntities(updatedEntities);
                         
                    //Load the stateVO
                    stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                    return stateVO;
                    
                }
                else{
                    /*
                     * allPreviousNodesValid = true means continue with the current selected node
                     */
                 
                    //Validate the current selected node (detailVO)
                    boolean isCurrentSelectedNodeEligible = true;
                    if(validationRequired)
                        isCurrentSelectedNodeEligible = validateNode(detailVO,orderEntities,decisionConfigId);
                        
                    if(isCurrentSelectedNodeEligible){
                        if(childNodes == null || childNodes.size()==0){
                            //get the childnodes for current level
                            displayNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentParentValueId);
                            if(validationRequired)
                                displayNodes = validateNodes(displayNodes, orderEntities);
                                
                            //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                             if(parentNode.getParentValueId() == -1){
                                if(displayNodes == null || displayNodes.size()==0){
                                    stateVO = new DecisionResultStateVO();
                                    stateVO.setDispositionAvailable(false);
                                    return stateVO;
                                }
                             }
                            if(displayNodes != null){
                                //set the selected node for this list of nodes
                                setSelectedNode(displayNodes, currentSelectedValueId, selectedComment);
                                currentLevel = maxLevel;    
                            }                        
                            //get the new parent node
                            //parentNode = getParentNode(decisionConfigId, parentNode.getParentValueId());
                            
                             if(currentLevel<maxLevel && currentLevel>minLevel){
                                 next = true;
                                 back = true;
                             } 
                             else if(currentLevel==maxLevel){
                                //Mark the end of disposition
                                dispoEnds = true;
                                if(parentNode.getParentValueId() != -1)
                                    back = true;
                                /*
                                 * submit = true, either disposition is required OR no entities selected.
                                 * So there are selected entities with disposition required status or none selected
                                 * The new logic is to display submit all the time where as COM triggers the decline button.
                                 */
                                //if(dispositionRequired || !entitiesSelected)
                                    submit=true;
                                //else
                                //    decline = true;
                            }
                             else if(currentLevel==minLevel){
                                 next = true;
                             }
                            
                             /*
                              * Get the list<DecisionResultDetailVO> of all selected values of the disposition
                              * Get the List<DecisionResultEntityVO> of all selected entities of teh disposition
                              */
                             details = getDecisionResultDetails(resultId);
                             updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
                             //set the details and updatedEntities into resultVO
                             resultVO.setDetails(details);
                             resultVO.setEntities(updatedEntities);
                            
                            //Load the stateVO
                            stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                            return stateVO;
                            
                        }//End of childNodes == null
                        else{
                            //Else if childNodes != null, validate if required
                            if(validationRequired){
                                displayNodes = validateNodes(childNodes,orderEntities);
                            }
                            else{
                                displayNodes = new ArrayList(); 
                                displayNodes.addAll(childNodes);
                            }
                            //Now check displayNodes
                            if(displayNodes == null || displayNodes.size()==0){
                                displayNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentParentValueId);
                                displayNodes = validateNodes(displayNodes,orderEntities);
                                
                                //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                                 if(parentNode.getParentValueId() == -1){
                                    if(displayNodes == null || displayNodes.size()==0){
                                        stateVO = new DecisionResultStateVO();
                                        stateVO.setDispositionAvailable(false);
                                        return stateVO;
                                    }
                                 }
                                 
                                //set the selected node for this list of nodes
                                setSelectedNode(displayNodes, currentSelectedValueId);
                                
                                //set the childnodes flag to false
                                 for (DecisionNodeVO node: displayNodes){
                                   if (node.getValueId() == currentSelectedValueId)
                                         node.setHasChildNodes(false);
                                 }
                                 
                                //get the new parent node
                                //parentNode = getParentNode(decisionConfigId, parentNode.getParentValueId());
                                
                                //Mark the end of disposition
                                dispoEnds = true;
                                
                                if(currentLevel > minLevel)
                                    back = true;    
                                //set the currentLevel to maxLevel
                                //currentLevel = maxLevel;
                                
                                //if(dispositionRequired || !entitiesSelected)
                                    submit=true;
                                //else
                                //    decline = true;
                                
                                /*
                                 * Get the list<DecisionResultDetailVO> of all selected values of the disposition
                                 * Get the List<DecisionResultEntityVO> of all selected entities of teh disposition
                                 */
                                details = getDecisionResultDetails(resultId);
                                updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
                                //set the details and updatedEntities into resultVO
                                resultVO.setDetails(details);
                                resultVO.setEntities(updatedEntities);
                                
                                DecisionResultDetailVO selectedDetailVO = null;
                                for (int i = 0; i < details.size(); i++){
                                  selectedDetailVO = (DecisionResultDetailVO) details.get(i);
                                  if (selectedDetailVO.getLevel() == currentLevel)
                                    break;
                                }
                                
                                if(selectedDetailVO != null)
                                    setSelectedNode(displayNodes, selectedDetailVO.getValueId(), selectedDetailVO.getCommentText());
                                    
                                //Load the stateVO
                                stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                                return stateVO;
                                
                            }
                            else{
                                //set the currentLevel to nextLevel
                                //currentLevel=nextLevel;
                                resultVO.setCurrentLevel(nextLevel);
                                updateDecisionResult(resultVO);
                                
                                //assign teh current selected node as parent node.
                                parentNode = currentSelectedNode;
                                //set the next, back flag also here
                                if(nextLevel<maxLevel && nextLevel>minLevel){
                                    next = true;
                                    back = true;
                                } 
                                else if(nextLevel==maxLevel){
                                    back = true;
                                    dispoEnds = true;
                                    //if(dispositionRequired || !entitiesSelected)
                                        submit=true;
                                    //else
                                    //    decline = true;
                                }
                                else if(nextLevel==minLevel){
                                    next = true;
                                }
                                
                                /*
                                 * Get the list<DecisionResultDetailVO> of all selected values of the disposition
                                 * Get the List<DecisionResultEntityVO> of all selected entities of teh disposition
                                 */
                                details = getDecisionResultDetails(resultId);
                                DecisionResultDetailVO selectedDetailVO = null;
                                for (int i = 0; i < details.size(); i++){
                                  selectedDetailVO = (DecisionResultDetailVO) details.get(i);
                                  if (selectedDetailVO.getLevel() == nextLevel)
                                    break;
                                }
                                
                                if(selectedDetailVO != null)
                                    setSelectedNode(displayNodes, selectedDetailVO.getValueId(), selectedDetailVO.getCommentText());
                                
                                updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
                                //set the details and updatedEntities into resultVO
                                resultVO.setDetails(details);
                                resultVO.setEntities(updatedEntities);
                                
                                //Load the stateVO
                                stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                                return stateVO;
                            }
                        }//End of Else childNodes !=null
                        
                    }//End of if for isCurrentSelectedNodeEligible
                    else{
                        // Else if currentNode is not eligible
                        //delete the current node
                        deleteDecisionResultDetail(resultId, currentSelectedValueId);
                        
                        //get the List of resultDetails ordered by level
                        details = getDecisionResultDetails(resultId);
                        
                        //reverse the order of detail based on level
                        Collections.reverse(details);
                        
                        //loop thru the list and validate each one
                        for(DecisionResultDetailVO detail:details){
                            //make sure to start with level 2 onwards, since level 1 is root.
                            if(detail.getLevel()>=1){
                                if(!validateNode(detail,orderEntities,decisionConfigId)){
                                    deleteDecisionResultDetail(resultId, detail.getValueId());
                                    continue;
                                }
                                else{
                                    //get the current level and the child nodes for this detail
                                    currentLevel = detail.getLevel()+1;
                                    childNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, detail.getValueId());
                                    displayNodes = validateNodes(childNodes,orderEntities);
                                    if(displayNodes == null || displayNodes.size()==0){
                                        //set the parent node for this detail using parentValueId
                                        currentLevel = detail.getLevel();
                                        parentNode = getParentNode(decisionConfigId,detail.getParentValueId());
                                        displayNodes = getNodesAndRulesByLevel(decisionConfigId,currentLevel,detail.getParentValueId());
                                        
                                        //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                                        if(parentNode.getParentValueId() == -1){
                                            if(displayNodes == null || displayNodes.size()==0){
                                                stateVO = new DecisionResultStateVO();
                                                stateVO.setDispositionAvailable(false);
                                                return stateVO;
                                            }
                                        }
                                         
                                        //set the selected node
                                        //setSelectedNode(displayNodes, detail.getValueId(), detail.getCommentText());
                                        break;
                                    }
                                    else{
                                        //set the selected node
                                        //setSelectedNode(displayNodes, detail.getValueId(), detail.getCommentText());
                                        parentNode = getParentNode(decisionConfigId,detail.getValueId());
                                        break;
                                    }
                                }
                            }                            
                        }
                        
                        //set the next, back flag also here
                        if(currentLevel<maxLevel && currentLevel>minLevel){
                            next = true;
                            back = true;
                        } 
                        else if(currentLevel==maxLevel){
                            dispoEnds = true;
                            back = true;
                            //if(dispositionRequired || !entitiesSelected)
                                submit=true;
                            //else
                            //    decline = true;
                        }
                        else if(currentLevel==minLevel){
                            next = true;
                        }
                        
                        
                        //Update the current level into resultVO
                        resultVO.setCurrentLevel(currentLevel);
                        updateDecisionResult(resultVO);
                        
                        //Update the details into resultVO
                        Collections.reverse(details);
                        resultVO.setDetails(details);
                        
                        //Update the entities into resultVO
                        updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
                        resultVO.setEntities(updatedEntities);
                        
                        //set the previously selected node if any.                        
                        DecisionResultDetailVO selectedDetailVO = null;
                        for (int i = 0; i < details.size(); i++){
                          selectedDetailVO = (DecisionResultDetailVO) details.get(i);
                          if (selectedDetailVO.getLevel() == currentLevel)
                            break;
                        }
                        
                        if(selectedDetailVO != null)
                            setSelectedNode(displayNodes, selectedDetailVO.getValueId(), selectedDetailVO.getCommentText());
                            
                        //Load the stateVO
                        stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                        return stateVO;
                        
                    }//End of else for isCurrentSelectedNodeEligible
                }//End of allPreviousNodesValid = true 
            }//End of else if(dispositionStatus != DecisionResultConstants.DISPOSITION_STATUS_VIEW 
        }
        else{
            //Oops!, Error, something wrong with current level
            return null;
        }
        
        //Glad to return stateVO
        return stateVO;
    }
    
    /**
     * This method will handles all the logic when the previous or breadcrumb is clicked in the widget
     * @param HashMap params
     * @return DecisionResultStateVO
     * @throws Exception
     */
    public DecisionResultStateVO doBack(HashMap params) throws Exception{
        DecisionResultDAO decisionResultDAO = new DecisionResultDAO(dbConnection);
        DecisionResultStateVO stateVO = new DecisionResultStateVO();
        
        /*
         * Retrieve the decisionConfigId from request. If null throw runtime Exception.
         * Retrieve the callDispositionId from request. If null throw runtime Exception.
         * Retrieve the currentLevel from request, tells the current step
         * Retrieve the currentParentValueId from request, tells the level with script used.
         * Retrieve the currentSelectedValueId from request, tells the selections.
         * Retrieve the currentSelectedName from request, tells the selections.
         * Retrieve the currentComment from request.
         * Retrieve the isDispoEnds from request. True, if this is the last step of disposition.
         * Retrieve the actionType=next, submit or decline from request.
         * Retrieve the isDispoRequired from request. True, if required.
         * Retrieve the dnisId from request.
         */
        String dispositionStatus = (String) params.get(DecisionResultConstants.REQ_DISPOSITION_STATUS);
        
        String decisionResultId = (String) params.get(DecisionResultConstants.REQ_RESULT_ID);
        if (decisionResultId == null)
            throw new Exception("The resultId is null for this disposition event.");
        
        String configId = (String) params.get(DecisionResultConstants.REQ_DECISION_CONFIG_ID);
        if (configId == null)
            throw new Exception("The configId is null for this disposition event.");

        String parentValueId = (String) params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_PARENT_VALUE_ID);
        if(parentValueId == null)
            throw new Exception("The parent Value Id is null for this disposition event.");
                    
        String previousValueId = (String) params.get(DecisionResultConstants.REQ_PREVIOUS_LEVEL_VALUE_ID);
        if(previousValueId == null)
            throw new Exception("The previous Value Id is null for this disposition event.");
        
        String cLevel = (String) params.get(DecisionResultConstants.REQ_CURRENT_LEVEL);
        String selectedValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_VALUE_ID);
        String selectedComment = (String) params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_COMMENT);
        String pLevel = (String) params.get(DecisionResultConstants.REQ_PREVIOUS_LEVEL);
        String dnisId = (String)params.get(DecisionResultConstants.REQ_DNIS_ID);
        
        //available entities
        String availableEntities = (String)params.get(DecisionResultConstants.REQ_AVAILABLE_ENTITIES);
        
        long resultId = Long.parseLong(decisionResultId);
        decisionConfigId = Long.parseLong(configId);
        currentParentValueId = Long.parseLong(parentValueId);
        previousSelectedValueId = Long.parseLong(previousValueId);
    
        //Get the decisionResultVO for the param resultId
        resultVO = getDecisionResultByResultId(resultId, resultId);

        //Get the userId by calling getUserId(securityToken)
        String userId = (String) getUserId((String) params.get(DecisionResultConstants.SECURITY_TOKEN));
        
        //Now we have decisionConfigId so lets get the max levels of disposition available for this Id.
        maxLevel = getMaxLevelByConfig(Long.parseLong(configId));
        
        /*
         * If there is any selection made in the current level save it now
         * Get the currentselecetd node
         */
        DecisionNodeVO currentSelectedNode = null;
        DecisionResultDetailVO currentDetailVO = null;
        
        if(selectedValueId != null){
            currentSelectedValueId = Long.parseLong(selectedValueId);
            currentSelectedNode = getNode(decisionConfigId, currentSelectedValueId);
            if(currentSelectedNode == null)
                throw new Exception("Error while getting the ValueNode for the current selected valueId."+currentSelectedValueId);
             
            /*
             * Build and save the DecisionResultDetailVO using selection value/comment etc. 
             * saveDecisionResultDetail(DecisionResultDetailVO) would update if parentValueId exists.
             */ 
            currentDetailVO = new DecisionResultDetailVO();
            currentDetailVO.setResultId(Long.parseLong(decisionResultId));
            currentDetailVO.setValueId(currentSelectedNode.getValueId());
            currentDetailVO.setParentValueId(currentSelectedNode.getParentValueId());
            currentDetailVO.setName(currentSelectedNode.getName());
            currentDetailVO.setCommentText(selectedComment);
            currentDetailVO.setScriptText(currentSelectedNode.getScriptText());
            currentDetailVO.setCreatedBy(userId);
            currentDetailVO.setUpdateBy(userId);
            
            long resultDetailId = saveDecisionResultDetail(currentDetailVO);
            currentDetailVO.setResultDetailId(resultDetailId);
        }
                
        //get the parentValueNode
        DecisionNodeVO parentNode = getParentNode(decisionConfigId, currentParentValueId);
        if (parentNode == null)
            throw new Exception("The ValueNode can not be found for the parent ValueID passed in: " + currentParentValueId);

        /*
         * Get the previousSelectedNode & previousParentNode
         * Using previousValueId get previousSelectedNode
         * Then use previousSelectedNode's parentValueId to get the previousParentNode
         */
        DecisionNodeVO previousSelectedNode = getNode(decisionConfigId, previousSelectedValueId);
        if(previousSelectedNode == null)
            throw new Exception("The ValueNode can not be found for the previous ValueID passed in: "+previousSelectedValueId);
       
        /*
         * Get the list<DecisionResultDetailVO> of all selected values of the disposition
         */
        details = getDecisionResultDetails(resultId);

        //set the details and updatedEntities into resultVO
        resultVO.setDetails(details);

        //Get the current level from request
        if (cLevel == null)
          currentLevel = resultVO.getCurrentLevel();
        else
          currentLevel = Integer.parseInt(cLevel);

        /*
         * Get the previous level if available 
         * Since the currentLevel will be one level ahead all time
         * so increment the previous level
         */
        if (pLevel != null){
          previousLevel = Integer.parseInt(pLevel);
          previousLevel++;
        }
        
        int nextLevel = previousLevel;

        //Loop through previously selected node Details and get the detailVO for the previousLevel 
        DecisionResultDetailVO previousDetailVO = null;
        for (int i = 0; i < details.size(); i++){
          previousDetailVO = (DecisionResultDetailVO) details.get(i);
          if (previousDetailVO.getLevel() == previousLevel)
            break;
        }
        
        if(previousDetailVO == null)
            throw new Exception("The previous result detail is not found for previous level: "+previousLevel);
        
        /*
         * validationRequired is a preliminary check to see if the validation is required
         * dispositionRequired is flag to set submit or decline
         */
        boolean validationRequired = false;
        boolean dispositionRequired = false;
        boolean entitiesSelected = false;
        
        if (dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)){
          entityMap = manageEntities(params, decisionResultId);
          //Now save the selectedEntities
          if (entityMap != null){
              if(selectedEntitiesString != null && selectedEntitiesString.length()>0){
                  saveDecisionResultEntities(resultId,selectedEntitiesString, DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_ORDER, userId);
                  entitiesSelected = true;   
              }

            //Get the entities out of entityMap
            entities = (List<EntityDetailVO>) entityMap.get(DecisionResultConstants.ENTITY_SEL_ALL_DETAILS_LIST);

            //get the exisitng list of entities
            existingEntities = (List<DecisionResultEntityVO>) entityMap.get(DecisionResultConstants.ENTITY_EXISTING_LIST);

            //preliminary check to see if the validation is required
            validationRequired = validationRequiredCheck((String) entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING), existingEntities);

            //Get the dispositionRequired flag to set submit or decline
            dispositionRequired = isDispsotionRequired(entities);
          }
        }


        List<DecisionNodeVO> childNodes = null;
        if (currentLevel > minLevel){
            childNodes = getNodesAndRulesByLevel(decisionConfigId, nextLevel, previousSelectedValueId);
            setSelectedNode(childNodes, previousDetailVO.getValueId(), previousDetailVO.getCommentText());
            /*
             * If the disposition status is view
             */
            if (dispositionStatus != null && dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)){
                if (childNodes == null || childNodes.size() == 0){
                    childNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentParentValueId);
                    //If the childnodes are null here at the root then return stateVO with dispositionAvailable = false
                    if(parentNode.getParentValueId() == -1){
                        if(childNodes == null || childNodes.size()==0){
                            stateVO = new DecisionResultStateVO();
                            stateVO.setDispositionAvailable(false);
                            return stateVO;
                        }
                    }
                     
                    setSelectedNode(childNodes, previousDetailVO.getValueId());
                    if (childNodes != null && childNodes.size() > 0)
                        currentLevel = maxLevel;
        
                    resultVO.setCurrentLevel(currentLevel);
                    updateDecisionResult(resultVO);
        
                    //set the next, back flag also here
                    if (currentLevel < maxLevel && currentLevel > minLevel){
                        next = true;
                        back = true;
                    }
                    else if (currentLevel == maxLevel){
                        dispoEnds = true;
                        back = true;
                        submit = true;
                    }
                    else if (currentLevel == minLevel){
                        next = true;
                    }
                }
                else{
                  parentNode = getParentNode(decisionConfigId, previousSelectedValueId);
                  resultVO.setCurrentLevel(nextLevel);
                  updateDecisionResult(resultVO);
                  //set the next, back flag also here
                  if (nextLevel < maxLevel && nextLevel > minLevel)
                  {
                    next = true;
                    back = true;
                  }
                  else if (nextLevel == maxLevel)
                  {
                    dispoEnds = true;
                    back = true;
                    submit = true;
    
                  }
                  else if (nextLevel == minLevel)
                  {
                    next = true;
                  }
    
                }
    
                //assign the         
                displayNodes = new ArrayList();
                displayNodes.addAll(childNodes);
    
                //Load the stateVO 
                stateVO = buildStateVO(null, null,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                return stateVO;
              }
              else if (dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW) || validationRequired){
                orderEntities = (List<EntityDetailVO>) entityMap.get(DecisionResultConstants.ENTITY_SEL_ORDER_DETAILS_LIST);
                
                
                List<DecisionResultDetailVO> details = getDecisionResultDetails(resultId); 
                boolean allPreviousNodesValid = validatePreviousSelections(details, orderEntities, decisionConfigId, resultId);
                 
                //Now get the valid previous selection if allPreviousNodesValid == false
                if(!allPreviousNodesValid){
                    DecisionResultDetailVO currentValidDetail = null;
                    details = getDecisionResultDetails(resultId);
                    //reverse the order of detail based on level
                    Collections.reverse(details);
                    if(details != null && !details.isEmpty())
                        currentValidDetail = details.iterator().next();
                    
                    //simply return null, since this is an error condition
                    if(currentValidDetail == null)
                        return null;
                        
                    //get the childnodes for the currentValidDetail node
                    currentLevel = currentValidDetail.getLevel()+1;
                    childNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentValidDetail.getValueId());
                    //validate the childnodes
                    if(validationRequired)
                        displayNodes = validateNodes(childNodes,orderEntities);
                    else{
                        displayNodes = new ArrayList(); 
                        displayNodes.addAll(childNodes);
                    }
                        
                    if(displayNodes == null || displayNodes.size()==0){
                        //set the parent node for this detail using parentValueId
                        currentLevel = currentValidDetail.getLevel();
                        parentNode = getParentNode(decisionConfigId,currentValidDetail.getParentValueId());
                        childNodes = getNodesAndRulesByLevel(decisionConfigId,currentLevel,currentValidDetail.getParentValueId());
                        displayNodes = validateNodes(childNodes,orderEntities);
                        
                        //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                        if(parentNode.getParentValueId() == -1){
                            if(displayNodes == null || displayNodes.size()==0){
                                stateVO = new DecisionResultStateVO();
                                stateVO.setDispositionAvailable(false);
                                return stateVO;
                            }
                        }
                        
                        //set the childnodes flag to false
                         for (DecisionNodeVO node: displayNodes){
                           if (node.getValueId() == currentValidDetail.getValueId())
                                 node.setHasChildNodes(false);
                         }
                        
                    }
                    else{
                        parentNode = getParentNode(decisionConfigId,currentValidDetail.getValueId());
                    }
                    
                    //set the Selected Node
                    setSelectedNode(displayNodes, currentValidDetail.getValueId(), currentValidDetail.getCommentText());
                    
                    //set the next, back flag also here
                    if(currentLevel<maxLevel && currentLevel>minLevel){
                          next = true;
                          back = true;
                    } 
                    else if(currentLevel==maxLevel){
                          dispoEnds = true;
                          back = true;
                          //if(dispositionRequired || !entitiesSelected)
                              submit=true;
                          //else
                          //    decline = true;
                    }
                    else if(currentLevel==minLevel){
                          next = true;
                    }
                    
                    //Update the current level into resultVO
                    resultVO.setCurrentLevel(currentLevel);
                    updateDecisionResult(resultVO);
                    
                    //Update the details into resultVO
                    Collections.reverse(details);
                    resultVO.setDetails(details);
                    
                    //Update the entities into resultVO
                    updatedEntities = getDecisionResultEntities(resultVO.getResultId()); 
                    resultVO.setEntities(updatedEntities);
                         
                     //Load the stateVO
                     stateVO = (DecisionResultStateVO)buildStateVO(entities,availableEntities,next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                     return stateVO;
                    
                }
                else{
                    /*
                     * allPreviousNodesValid = true means continue with the current selected node
                     */
                    
                    //Validate the previous selected node (detailVO)
                     boolean isPreviousSelectedNodeEligible = true;
                     if(validationRequired)
                        isPreviousSelectedNodeEligible = validateNode(previousDetailVO, orderEntities, decisionConfigId);
                        
                    if (isPreviousSelectedNodeEligible){
                        if (childNodes == null || childNodes.size() == 0){
                            displayNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentParentValueId);
                            if(validationRequired)
                                displayNodes = validateNodes(displayNodes, orderEntities);
                            
                            //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                            if(parentNode.getParentValueId() == -1){
                                if(displayNodes == null || displayNodes.size()==0){
                                    stateVO = new DecisionResultStateVO();
                                    stateVO.setDispositionAvailable(false);
                                    return stateVO;
                                }
                            }
                            //set the selected node for this list of nodes
                            if(previousDetailVO != null)
                                setSelectedNode(displayNodes, previousDetailVO.getValueId(), previousDetailVO.getCommentText());
        
                            //Mark the end of disposition
                            dispoEnds = true;
                            currentLevel = maxLevel;
            
                            if (currentLevel == maxLevel){
                                if(parentNode.getParentValueId() != -1)
                                    back = true;
            
                                if(dispositionRequired || !entitiesSelected)
                                    submit = true;
                                else
                                    decline = true;
                            }
        
                            //Get and set the updatedEntities into resultVO
                            updatedEntities = getDecisionResultEntities(resultVO.getResultId());
                            resultVO.setEntities(updatedEntities);
                            
                            //Load the stateVO
                            stateVO = buildStateVO(entities,availableEntities, next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                            return stateVO;
        
                      } //End of childNodes == null
                      else{
                        //Else if childNodes != null, validate if required
                        if (validationRequired){
                          displayNodes = validateNodes(childNodes, orderEntities);
                        }
                        else{
                          displayNodes = new ArrayList();
                          displayNodes.addAll(childNodes);
                        }
                        //Now check displayNodes
                        if (displayNodes == null || displayNodes.size() == 0){
                            displayNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, currentParentValueId);
                            
                            //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                            if(parentNode.getParentValueId() == -1){
                                if(displayNodes == null || displayNodes.size()==0){
                                    stateVO = new DecisionResultStateVO();
                                    stateVO.setDispositionAvailable(false);
                                    return stateVO;
                                }
                            }
                             
                            //set the selected node for this list of nodes
                            setSelectedNode(displayNodes, previousSelectedValueId);
        
                            //Mark the end of disposition
                            dispoEnds = true;
        
                            if (currentLevel > minLevel)
                                back = true;
                            //set the currentLevel to maxLevel
                            currentLevel = maxLevel;
        
                            if (dispositionRequired || !entitiesSelected)
                                submit = true;
                            else
                                decline = true;
        
                            //Get and set the updatedEntities into resultVO
                            updatedEntities = getDecisionResultEntities(resultVO.getResultId());
                            resultVO.setEntities(updatedEntities);
        
                            //Load the stateVO
                            stateVO = buildStateVO(entities, availableEntities, next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                            return stateVO;
        
                        }
                        else{
                          //set the currentLevel to nextLevel
                          currentLevel = nextLevel;
                          resultVO.setCurrentLevel(currentLevel);
                          updateDecisionResult(resultVO);
        
                          //assign teh current selected node as parent node.
                          parentNode = previousSelectedNode;
                          //set the next, back flag also here
                          if (nextLevel < maxLevel && nextLevel > minLevel){
                            next = true;
                            back = true;
                          }
                          else if (nextLevel == maxLevel){
                            back = true;
                            dispoEnds = true;
                            if (dispositionRequired || !entitiesSelected)
                              submit = true;
                            else
                              decline = true;
                          }
                          else if (nextLevel == minLevel){
                            next = true;
                          }
        
                          //Get and set the updatedEntities into resultVO
                          updatedEntities = getDecisionResultEntities(resultVO.getResultId());
                          resultVO.setEntities(updatedEntities);
        
                          //Load the stateVO
                          stateVO = (DecisionResultStateVO) buildStateVO(entities, availableEntities, next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                          return stateVO;
                        }
                      } //End of Else childNodes !=null
        
                    } //End of if for isCurrentSelectedNodeEligible
                    else{
                        // Else if previousNode is not eligible, delete all the subsequent nodes
                        deletePreviousResultDetail(resultId, previousDetailVO.getParentValueId());
                        
                        //get the List of resultDetails ordered by level
                        details = getDecisionResultDetails(resultId);
        
                        //reverse the order of detail based on level
                        Collections.reverse(details);
                                       
                      //loop thru the list and validate each one
                      for (DecisionResultDetailVO detail: details){
                        //make sure to start with level 2 onwards, since level 1 is root.
                        if (detail.getLevel() >= 1){
                            if (!validateNode(detail, orderEntities, decisionConfigId)){
                                deleteDecisionResultDetail(resultId, detail.getValueId());
                                continue;
                            }
                            else{
                                //get the current level and the child nodes for this detail
                                currentLevel = detail.getLevel() + 1;
                                childNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, detail.getValueId());
                                displayNodes = validateNodes(childNodes, orderEntities);
                                if (displayNodes == null || displayNodes.size() == 0){
                                    //set the parent node for this detail using parentValueId
                                    currentLevel = detail.getLevel();
                                    parentNode = getParentNode(decisionConfigId, detail.getParentValueId());
                                    displayNodes = getNodesAndRulesByLevel(decisionConfigId, currentLevel, detail.getParentValueId());
                                    
                                    //If the displayNodes are null here at the root then return stateVO with dispositionAvailable = false
                                    if(parentNode.getParentValueId() == -1){
                                        if(displayNodes == null || displayNodes.size()==0){
                                            stateVO = new DecisionResultStateVO();
                                            stateVO.setDispositionAvailable(false);
                                            return stateVO;
                                        }
                                    }
                                    
                                    break;
                                }
                                else{
                                    parentNode = getParentNode(decisionConfigId, detail.getValueId());
                                    break;
                                }
                            }
                        }
                      }
    
                      //set the next, back flag also here
                      if (currentLevel < maxLevel && currentLevel > minLevel){
                        next = true;
                        back = true;
                      }
                      else if (currentLevel == maxLevel){
                        dispoEnds = true;
                        back = true;
                        if (dispositionRequired || !entitiesSelected)
                          submit = true;
                        else
                          decline = true;
                      }
                      else if (currentLevel == minLevel){
                        next = true;
                      }
                    
                      //update the currentLevel in to result
                      resultVO.setCurrentLevel(currentLevel);
                      updateDecisionResult(resultVO);
                      
                      //update the details into resultVO
                      Collections.reverse(details);
                      resultVO.setDetails(details);
                      
                      //update the Entities into resultVO
                      updatedEntities = getDecisionResultEntities(resultVO.getResultId());
                      resultVO.setEntities(updatedEntities);
                        
                      //Load the stateVO
                      stateVO = buildStateVO(entities, availableEntities, next, back, submit, decline, dispoEnds, displayNodes, parentNode, resultVO);
                      return stateVO;
        
                    } //End of else for isCurrentSelectedNodeEligible
                }//End of else allPreviousNodesValid
            } //End of else if(dispositionStatus != DecisionResultConstants.DISPOSITION_STATUS_VIEW 
        }
        else
        {
          //Oops!, Error, something wrong with current level
          return null;
        }

            
        return stateVO;
    }
    
    /**
     * This method will handles all the logic when the disposition is submitted in the widget
     * @param HashMap params
     * @return DecisionResultStateVO
     * @throws Exception
     */
    public DecisionResultStateVO doSubmit(HashMap params) throws Exception{
        DecisionResultStateVO stateVO = null;
        /*
         * Retrieve the decisionConfigId from request. If null throw runtime Exception.
         * Retrieve the callDispositionId from request. If null throw runtime Exception.
         * Retrieve the currentLevel from request, tells the current step
         * Retrieve the currentParentValueId from request, tells the level with script used.
         * Retrieve the currentSelectedValueId from request, tells the selections.
         * Retrieve the currentSelectedName from request, tells the selections.
         * Retrieve the currentComment from request.
         * Retrieve the isDispoEnds from request. True, if this is the last step of disposition.
         * Retrieve the actionType=next, submit or decline from request.
         * Retrieve the isDispoRequired from request. True, if required.
         * Retrieve the dnisId from request. 
         * Retrieve the previousValueId from request. 
         * Get the maxLevel by calling loadMaxLevelByConfigId(configId).
         */
        String dispositionStatus = (String)params.get(DecisionResultConstants.REQ_DISPOSITION_STATUS);
        String decisionTypeCode = (String)params.get(DecisionResultConstants.REQ_DECISION_TYPE_CODE);
        if(decisionTypeCode == null)
            decisionTypeCode = DecisionResultConstants.CALL_DISPOSITION_TYPE_CODE;
            
        String actionType = (String)params.get(DecisionResultConstants.REQ_ACTION_TYPE);
        if(actionType == null)
            throw new Exception("The action type is null for this disposition event.");
            
        String decisionResultId = (String)params.get(DecisionResultConstants.REQ_RESULT_ID); 
        if(decisionResultId == null)
            throw new Exception("The resultId is null for this disposition event.");
        
        String configId = (String)params.get(DecisionResultConstants.REQ_DECISION_CONFIG_ID);
        if(configId == null)
            throw new Exception("The configId is null for this disposition event.");
        
        String parentValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_PARENT_VALUE_ID); 
        if(parentValueId == null)
            throw new Exception("The parent Value Id is null for this disposition event.");
            
        String selectedValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_VALUE_ID);
        if(selectedValueId == null)
            throw new Exception("The selected Value Id is null for this disposition event.");
            
        String selectedComment = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_COMMENT);
            
        String level = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL);
        String availableEntities = (String)params.get(DecisionResultConstants.REQ_AVAILABLE_ENTITIES);
        
        long resultId = Long.parseLong(decisionResultId);
        currentSelectedValueId = Long.parseLong(selectedValueId);
        currentParentValueId = Long.parseLong(parentValueId);
        decisionConfigId = Long.parseLong(configId);
         
        //Get the decisionResultVO for the param resultId
        resultVO = getDecisionResultByResultId(resultId,resultId);
        if(resultVO == null)
            throw new Exception("The Decision Result can not be recovered for this disposition resultId: "+resultId);
        
        //define a new resultVO object    
        DecisionResultVO updatedResultVO = null;
        
        
        //Get the current level from request or resultVO
        if(level == null)
            currentLevel = resultVO.getCurrentLevel();
        else 
            currentLevel = Integer.parseInt(level);
        
        
        //Get the userId by calling getUserId(securityToken)
        String userId = (String)getUserId((String)params.get(DecisionResultConstants.SECURITY_TOKEN));
        
        //Now we have decisionConfigId so lets get the max levels of disposition available for this Id.
        maxLevel = getMaxLevelByConfig(decisionConfigId);
        
        //get the parentValueNode
        DecisionNodeVO parentNode = getParentNode(decisionConfigId,currentParentValueId);
        if(parentNode == null)
           throw new Exception("The ValueNode can not be found for the parent ValueID passed in: "+currentParentValueId);
        
        //Get the currentselecetd node
        DecisionNodeVO currentSelectedNode = getNode(decisionConfigId, currentSelectedValueId);
        if(currentSelectedNode == null)
            throw new Exception("Error while getting the ValueNode for the current selected valueId."+currentSelectedValueId);
            
        int nextLevel = currentLevel;
        //Get the child nodes for the currentSelectedValueId.
        childNodes = getNodesAndRulesByLevel(decisionConfigId, ++nextLevel, currentSelectedValueId);
        
        /*
         * Get the DetailVO based on current selected value Id
         * If there is no entry then create an entry
         */
        DecisionResultDetailVO detailVO = getDecisionResultDetailByValueId(resultId, currentSelectedValueId, DecisionResultConstants.VALUE_ID);
        if(detailVO == null){
            detailVO = new DecisionResultDetailVO();
            detailVO.setResultId(resultId);
            detailVO.setValueId(currentSelectedNode.getValueId());
            detailVO.setParentValueId(currentSelectedNode.getParentValueId());
            detailVO.setName(currentSelectedNode.getName());
            detailVO.setCommentText(selectedComment);
            detailVO.setScriptText(currentSelectedNode.getScriptText());
            detailVO.setCreatedBy(userId);
            detailVO.setUpdateBy(userId);
            
            long resultDetailId = saveDecisionResultDetail(detailVO);
            detailVO.setResultDetailId(resultDetailId);
        }
        
       
            
        /*
         * validationRequired is a preliminary check to see if the validation is required
         * dispositionRequired is flag to set submit or decline
         * entitiesSelected will be true if any entities selected 
         */
         boolean validationRequired = false; 
         boolean dispositionRequired = false;
         boolean entitiesSelected = false;
         
        if(dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)) {
            entityMap = manageEntities(params, decisionResultId);
            //Now save the selectedEntities
            if(entityMap != null){
                selectedEntitiesString = (String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING);
                if(selectedEntitiesString != null && selectedEntitiesString.length()>0){
                    saveDecisionResultEntities(resultId,selectedEntitiesString, DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_ORDER, userId);
                    entitiesSelected = true;   
                }
             
                //Get the entities out of entityMap
                entities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ALL_DETAILS_LIST);
                
                //get the exisitng list of entities
                existingEntities = (List<DecisionResultEntityVO>)entityMap.get(DecisionResultConstants.ENTITY_EXISTING_LIST);
                
                //preliminary check to see if the validation is required
                validationRequired = validationRequiredCheck((String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING), existingEntities);
                
                //Get the dispositionRequired flag to set submit or decline
                dispositionRequired = isDispsotionRequired(entities);
                
                //Get the order related entities with selected based on selectedEntities
                orderEntities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ORDER_DETAILS_LIST);
                
            }
        }
        
        //if there are valid childnodes then just let doNext() handle it.
        if(childNodes != null){
            displayNodes = validateNodes(childNodes, orderEntities); 
            if(displayNodes != null && displayNodes.size() > 0)
                return doNext(params);
        }
        
        if(dispositionStatus != null && dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)){
            //update the status on current decison result
            resultVO.setCurrentLevel(currentLevel); 
            if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_SUBMIT))
                resultVO.setStatus(DecisionResultStatus.COMPLETE);
            
            resultVO.setUpdateBy(userId);
            //in the view mode do not actually update the status to complete in the table 
            //boolean resultUpdateStatus = updateDecisionResult(resultVO);
            //if(!resultUpdateStatus)
            //    throw new Exception("Error while while updating the decision result for resultId."+resultId);
            
            //update the status on the updated resultVO to return
            updatedResultVO = new DecisionResultVO();
            updatedResultVO.setStatus(resultVO.getStatus());
            
            //since it is view mode, simply clear out state accept resultVO.
            stateVO = buildStateVO(null,null,false,false,false,false,true,null,null,updatedResultVO);
            
            return stateVO;
        
        }
        else if(dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW) || validationRequired) {
            
            //Lets get the DNIS id now if it is null, throw an exception
            //Changed since DNIS is is not mandatory field in comments table.
            String dnisId = (String)params.get(DecisionResultConstants.REQ_DNIS_ID);
            //if(dnisId == null)
            //    throw new Exception("The DNIS Id is null for this disposition event.");
             long l_dnisId = 0;
             if(dnisId != null)
                 l_dnisId = Long.parseLong(dnisId);
            
            //Now lets validate the current DetailVO 
            boolean isCurrentSelectedNodeEligible = true;
            if(validationRequired)
                isCurrentSelectedNodeEligible = validateNode(detailVO,orderEntities,decisionConfigId);
            
            if(isCurrentSelectedNodeEligible){
                /*
                 * - update the result status - make status generic for submit
                 *     - insert the order comments
                 *     - update the status on entities and set it to available entities.
                 *     - clear out most of the stateVO
                 *     - return
                 */
                resultVO.setCurrentLevel(currentLevel); 
                if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_SUBMIT))
                    resultVO.setStatus(DecisionResultStatus.COMPLETE);
                
                resultVO.setUpdateBy(userId);
                boolean resultUpdateStatus = updateDecisionResult(resultVO);
                if(!resultUpdateStatus)
                    throw new Exception("Error while while updating the decision result for resultId."+resultId);
                
                //update the status on the updated resultVO to return
                updatedResultVO = new DecisionResultVO();
                updatedResultVO.setStatus(resultVO.getStatus());
                
                //now insert the order comments
                saveResultEntityComments(resultId,decisionTypeCode,l_dnisId,userId);
                
                //Build the stateVO
                stateVO = new DecisionResultStateVO();
                //update the status on entites
                if(entities != null){
                    entities = updateEntityStatus(entities, actionType);
                    stateVO.setAvailableEntities(parseEntitiesToString(entities));    
                }
                else{
                     stateVO.setEntityDetails(null);
                     stateVO.setAvailableEntities(availableEntities);
                 }
                 
                 stateVO.setNext(false);
                 stateVO.setBack(false);
                 stateVO.setSubmit(false);
                 stateVO.setDecline(false);
                 stateVO.setChildNodes(null);
                 stateVO.setDispoEnds(true);
                 stateVO.setParentNode(null);
                 stateVO.setDecisionResult(updatedResultVO);
                //stateVO = buildStateVO(entities,false,false,false,false,false,null,null,updatedResultVO);
                
            }//End of if isCurrentSelectedNodeEligible
            else{
                //if the current selected node is invalid then let doNext() handle it.
                stateVO = doNext(params);
                
            } //End of else !isCurrentSelectedNodeEligible
        }
        
        logger.info("Call Disposition submitted successfully by CSR : "+userId +" on: "+dateFormat.format(new Date()));
        return stateVO;
    }
    
    /**
     * This method will handles all the logic when the disposition is decline in the widget
     * @param HashMap params
     * @return DecisionResultStateVO
     * @throws Exception
     */
    public DecisionResultStateVO doDecline(HashMap params) throws Exception{
        DecisionResultStateVO stateVO = null;
        /*
         * Retrieve the decisionConfigId from request. If null throw runtime Exception.
         * Retrieve the callDispositionId from request. If null throw runtime Exception.
         * Retrieve the currentLevel from request, tells the current step
         * Retrieve the currentParentValueId from request, tells the level with script used.
         * Retrieve the currentSelectedValueId from request, tells the selections.
         * Retrieve the currentSelectedName from request, tells the selections.
         * Retrieve the currentComment from request.
         * Retrieve the isDispoEnds from request. True, if this is the last step of disposition.
         * Retrieve the actionType=next, submit or decline from request.
         * Retrieve the isDispoRequired from request. True, if required.
         * Retrieve the dnisId from request. 
         * Retrieve the previousValueId from request. 
         * Get the maxLevel by calling loadMaxLevelByConfigId(configId).
         */
        String dispositionStatus = (String)params.get(DecisionResultConstants.REQ_DISPOSITION_STATUS);
        String decisionTypeCode = (String)params.get(DecisionResultConstants.REQ_DECISION_TYPE_CODE);
        if(decisionTypeCode == null)
            decisionTypeCode = DecisionResultConstants.CALL_DISPOSITION_TYPE_CODE;
            
        String actionType = (String)params.get(DecisionResultConstants.REQ_ACTION_TYPE);
        if(actionType == null)
            throw new Exception("The action type is null for this disposition event.");
            
        String decisionResultId = (String)params.get(DecisionResultConstants.REQ_RESULT_ID); 
        if(decisionResultId == null)
            throw new Exception("The resultId is null for this disposition event.");
        
        String configId = (String)params.get(DecisionResultConstants.REQ_DECISION_CONFIG_ID);
        if(configId == null)
            throw new Exception("The configId is null for this disposition event.");
        /*
         * The parent node and the selected node are not necessary to decline
         */
        String parentValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_PARENT_VALUE_ID); 
        String selectedValueId = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_VALUE_ID);
        
        String availableEntities = (String)params.get(DecisionResultConstants.REQ_AVAILABLE_ENTITIES);
        
        String selectedComment = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL_SELECTED_COMMENT);
            
        String level = (String)params.get(DecisionResultConstants.REQ_CURRENT_LEVEL);
        
        long resultId = Long.parseLong(decisionResultId);
        decisionConfigId = Long.parseLong(configId);
         
        //Get the decisionResultVO for the param resultId
        resultVO = getDecisionResultByResultId(resultId,resultId);
        if(resultVO == null)
            throw new Exception("The Decision Result can not be recovered for this disposition resultId: "+resultId);
        
        //define a new resultVO object    
        DecisionResultVO updatedResultVO = null;
        
        
        //Get the current level from request or resultVO
        if(level == null)
            currentLevel = resultVO.getCurrentLevel();
        else 
            currentLevel = Integer.parseInt(level);
        
        
        //Get the userId by calling getUserId(securityToken)
        String userId = (String)getUserId((String)params.get(DecisionResultConstants.SECURITY_TOKEN));
        
        //Now we have decisionConfigId so lets get the max levels of disposition available for this Id.
        maxLevel = getMaxLevelByConfig(decisionConfigId);
        
        //get the parentValueNode
        if(parentValueId != null){
            currentParentValueId = Long.parseLong(parentValueId);
            DecisionNodeVO parentNode = getParentNode(decisionConfigId,currentParentValueId);
            if(parentNode == null)
               throw new Exception("The ValueNode can not be found for the parent ValueID passed in: "+currentParentValueId);
        }
        
        int nextLevel = currentLevel;
        //Get the child nodes for the currentSelectedValueId.
        childNodes = getNodesAndRulesByLevel(decisionConfigId, ++nextLevel, currentSelectedValueId);
        
        //if there are childnodes then just let doNext() handle it.
        if(childNodes != null)
            return doNext(params);
        
        /*
         * Get the DetailVO based on current selected value Id
         * If there is no entry then create an entry
         */
         DecisionResultDetailVO detailVO = null;
        if(selectedValueId != null){
            currentSelectedValueId = Long.parseLong(selectedValueId);
            
            //Get the currentselecetd node
            DecisionNodeVO currentSelectedNode = getNode(decisionConfigId, currentSelectedValueId);
            if(currentSelectedNode == null)
                throw new Exception("Error while getting the ValueNode for the current selected valueId."+currentSelectedValueId);
        
            detailVO = getDecisionResultDetailByValueId(resultId, currentSelectedValueId, DecisionResultConstants.VALUE_ID);
        
            if(detailVO == null){
                detailVO = new DecisionResultDetailVO();
                detailVO.setResultId(resultId);
                detailVO.setValueId(currentSelectedNode.getValueId());
                detailVO.setParentValueId(currentSelectedNode.getParentValueId());
                detailVO.setName(currentSelectedNode.getName());
                detailVO.setCommentText(selectedComment);
                detailVO.setScriptText(currentSelectedNode.getScriptText());
                detailVO.setCreatedBy(userId);
                detailVO.setUpdateBy(userId);
                
                long resultDetailId = saveDecisionResultDetail(detailVO);
                detailVO.setResultDetailId(resultDetailId);
            }
        }
        if(dispositionStatus != null && dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW)){
            //update the status on current decison result
            resultVO.setCurrentLevel(currentLevel); 
            if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_DECLINE))
                    resultVO.setStatus(DecisionResultStatus.DECLINE);
            
            resultVO.setUpdateBy(userId);
            //in the view mode do not actually update the status to complete in the table 
            //boolean resultUpdateStatus = updateDecisionResult(resultVO);
            //if(!resultUpdateStatus)
            //    throw new Exception("Error while while updating the decision result for resultId."+resultId);
            
            //update the status on the updated resultVO to return
            updatedResultVO = new DecisionResultVO();
            updatedResultVO.setStatus(resultVO.getStatus());
            
            //since it is view mode, simply clear out state accept resultVO.
            stateVO = buildStateVO(null,null,false,false,false,false,true,null,null,updatedResultVO);
            
        }
        else if(dispositionStatus == null || !dispositionStatus.equalsIgnoreCase(DecisionResultConstants.DISPOSITION_STATUS_VIEW) ) {
            //Lets get the DNIS id now if it is null throw exception
            //Changed since DNIS is is not mandatory field in comments table.
            String dnisId = (String)params.get(DecisionResultConstants.REQ_DNIS_ID);
            //if(dnisId == null)
            //    throw new Exception("The DNIS Id is null for this disposition event.");
            long l_dnisId = 0;
            if(dnisId != null)
                l_dnisId = Long.parseLong(dnisId);
            
            
            /*
             * Handle the entities here
             * 
             */
            entityMap = manageEntities(params, decisionResultId);
            
            //Now save the selectedEntities
            if(entityMap != null){
                selectedEntitiesString = (String)entityMap.get(DecisionResultConstants.ENTITY_SELECTED_STRING);
                if(selectedEntitiesString != null && selectedEntitiesString.length()>0){
                    saveDecisionResultEntities(resultId,selectedEntitiesString, DecisionResultConstants.RESULT_ENTITY_CALL_TYPE_NAME_ORDER, userId);
                    //entitiesSelected = true;   
                }
             
                //Get the entities out of entityMap
                entities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ALL_DETAILS_LIST);
                //get the exisitng list of entities
                existingEntities = (List<DecisionResultEntityVO>)entityMap.get(DecisionResultConstants.ENTITY_EXISTING_LIST);
                
                //Get the order related entities with selected based on selectedEntities
                orderEntities = (List<EntityDetailVO>)entityMap.get(DecisionResultConstants.ENTITY_SEL_ORDER_DETAILS_LIST);
            }
            
            /*
             * - update the result status - make status generic for decline
             *     - insert the order comments
             *     - update the status on entities and set it to available entities.
             *     - clear out most of the stateVO
             *     - return
             */
            resultVO.setCurrentLevel(currentLevel); 
            if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_DECLINE))
                resultVO.setStatus(DecisionResultStatus.DECLINE);
            
            resultVO.setUpdateBy(userId);
            boolean resultUpdateStatus = updateDecisionResult(resultVO);
            if(!resultUpdateStatus)
                throw new Exception("Error while while updating the decision result for resultId."+resultId);
            
            //update the status on the updated resultVO to return
            updatedResultVO = new DecisionResultVO();
            updatedResultVO.setStatus(resultVO.getStatus());
            
            //now insert the order comments
            //When declined, don't save order comments - defect 9042
            //saveResultEntityComments(resultId,decisionTypeCode,l_dnisId,userId);
            
            //Build the stateVO
            stateVO = new DecisionResultStateVO();
            //update the status on entites
            if(entities != null){
                entities = updateEntityStatus(entities, actionType);
                stateVO.setAvailableEntities(parseEntitiesToString(entities));    
            }
            else{
                 stateVO.setEntityDetails(null);
                 stateVO.setAvailableEntities(availableEntities);
             }
             
             stateVO.setNext(false);
             stateVO.setBack(false);
             stateVO.setSubmit(false);
             stateVO.setDecline(false);
             stateVO.setChildNodes(null);
             stateVO.setDispoEnds(true);
             stateVO.setParentNode(null);
             stateVO.setDecisionResult(updatedResultVO);
            
        }
        
        logger.info("Call Disposition declined successfully by CSR : "+userId +" on: "+dateFormat.format(new Date()));
        return stateVO;
    }
    
    /**
     * This method handles the entity related logic with respect to different entity types 
     * Loads the details and takes care of selected entities
     * Retrieve the availableEntities(list of name:value pair of entities like order Id or customer Id with values order_detail_id:121,order_detail_id:122,customer_id:909 etc.) from request.
     * Retrieve the selectedEntityList (list of selected order Id) from request that are selected.
     * Get the List of orderDetailIds by calling BO.parseEntities(availableEntities, type.ORDER_DETAIL_ID).
     * Get the List of customerIds by calling BO.parseEntities(availableEntities, type.CUSTOMER_ID).
     * Get the List<EntityDetailVO> entityDetails by calling the BO.loadEntityTypeOrderDetails(orderDetailIds, selectedEntities).
     * !!!NOTE: selected entities are assumed to be only order type for now w.r.t to COM so this is not future proof
     * @param params HashMap
     * @return HashMap entityMap
     * @throws Exception
     */
    private HashMap manageEntities(HashMap params, String decisionResultId) throws Exception{
        String availableEntitiesString = (String)params.get(DecisionResultConstants.REQ_AVAILABLE_ENTITIES);
        String selectedEntitiesString = (String)params.get(DecisionResultConstants.REQ_SELECTED_ENTITIES);
        HashMap entityMap = new HashMap();
        entityMap.put(DecisionResultConstants.ENTITY_AVAILABLE_STRING, availableEntitiesString);
        entityMap.put(DecisionResultConstants.ENTITY_SELECTED_STRING, selectedEntitiesString);
        String actionType = (String)params.get(DecisionResultConstants.REQ_ACTION_TYPE);
        List<DecisionResultEntityVO> existingEntities = null;
        List<EntityDetailVO> entities = null, orderEntities = null, customerEntities = null;
        
        if(availableEntitiesString != null && availableEntitiesString.length()>0){
            /*
            * Load the separate list of orderDetailIds and cutomerDetailIds
            * Just beacuse our DAO and stored procs are built to support one entity type at a time.
            * Due to the fact that business rules are different for customer type vs order type
            */
            List<EntityDetailVO> orderDetailIdList = parseEntitiesToList(availableEntitiesString,DecisionResultConstants.ENTITY_ORDER_DETAIL_ID);
            List<EntityDetailVO> customerIdList = parseEntitiesToList(availableEntitiesString,DecisionResultConstants.ENTITY_CUSTOMER_ID);
            
            //load the list into HM
            entityMap.put(DecisionResultConstants.ENTITY_AVAIL_ORDER_LIST, orderDetailIdList);
            entityMap.put(DecisionResultConstants.ENTITY_AVAIL_CUSTOMER_LIST, customerIdList);
            
            /*
             * If the list size==1 then auto select it i.e only for the first time during the load
             */
            if(actionType != null && actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_LOAD)){
                if(orderDetailIdList != null && orderDetailIdList.size()==1){
                    if(selectedEntitiesString == null){
                        EntityDetailVO orderDetail = (EntityDetailVO) orderDetailIdList.get(0);
                        if(orderDetail != null)
                            selectedEntitiesString = String.valueOf(orderDetail.getEntityId());
                            if(selectedEntitiesString != null)
                                entityMap.put(DecisionResultConstants.ENTITY_SELECTED_STRING, selectedEntitiesString);
                    }
                }
            }
            /*
             * Lets rule this out for now in COM. But eventually if an application wishes to plug in widget 
             * using customer should be OK with the code below but needs to be tuned later.
             */
            //Get the EntityDetailVO list based of orderDetailIds and customerDetailIds and set selected=true
            if(orderDetailIdList != null && orderDetailIdList.size()>0)
                orderEntities = getEntityTypeOrderDetails(orderDetailIdList,selectedEntitiesString);
            if(customerIdList != null && customerIdList.size()>0)
                customerEntities = getEntityTypeCustomerDetails(customerIdList,selectedEntitiesString);
            
            entities = new ArrayList();
            //put the order and customer list back together
            if(orderEntities != null && orderEntities.size()>0)
                entities.addAll(orderEntities);
            if(customerEntities != null && customerEntities.size()>0)
                entities.addAll(customerEntities);
            
            entityMap.put(DecisionResultConstants.ENTITY_SEL_ORDER_DETAILS_LIST,orderEntities);
            entityMap.put(DecisionResultConstants.ENTITY_SEL_CUSTOMER_DETAILS_LIST,customerEntities);
            entityMap.put(DecisionResultConstants.ENTITY_SEL_ALL_DETAILS_LIST, entities);
            
        }
        
        //Load the existing entities if resultId is not null
        if(selectedEntitiesString != null && selectedEntitiesString.length()>0){
            if(decisionResultId != null)
                existingEntities = getDecisionResultEntities(Long.parseLong(decisionResultId));       
        }
        
        //Put the existingEntities into the enityMap
        entityMap.put(DecisionResultConstants.ENTITY_EXISTING_LIST,existingEntities);
        
        return entityMap;
    }
    
    /**
     * Method to build the stateVO to return
     * @param entities
     * @param orderEntities
     * @param customerEntities
     * @param next
     * @param back
     * @param submit
     * @param decline
     * @param dispoEnds
     * @param displayNodes
     * @param parentNode
     * @param resultVO
     * @return
     */
    private DecisionResultStateVO buildStateVO(List<EntityDetailVO> entities, String availableEntities, boolean next, boolean back, boolean submit, boolean decline, boolean dispoEnds, 
                                                List<DecisionNodeVO> displayNodes, DecisionNodeVO parentNode, DecisionResultVO resultVO){
        stateVO = new DecisionResultStateVO();
        if(entities == null){
            stateVO.setEntityDetails(null);
            stateVO.setAvailableEntities(availableEntities);
        }
        else{
            stateVO.setEntityDetails(entities);
            stateVO.setAvailableEntities(parseEntitiesToString(entities));    
        }
        
        stateVO.setNext(next);
        stateVO.setBack(back);
        stateVO.setSubmit(submit);
        stateVO.setDecline(decline);
        stateVO.setChildNodes(displayNodes);
        stateVO.setDispoEnds(dispoEnds);
        stateVO.setParentNode(parentNode);
        stateVO.setDecisionResult(resultVO);
        
        return stateVO;
        
    }
    
    /**
     * This method checks if the disposition required or not
     * This flag will consider only selected entities just to set submit/decline flag
     * If (Selected & required & status != submitted) then submit=true Else decline=true
     * @param entities
     * @return boolean
     */
    private boolean isDispsotionRequired(List<EntityDetailVO> entities){
        if(entities == null || entities.size()==0)
            return false;
        
        boolean dispositionRequired = false;
        
        for(EntityDetailVO detail:entities){
            if(detail.isSelected())
                if(detail.isRequired())
                    if(!detail.getStatus().equalsIgnoreCase(DecisionResultConstants.ENTITY_STATUS_SUBMITTED))
                        dispositionRequired = true;
        }
        
        return dispositionRequired;
    }
    
    /**
     * This method will update the status for the selected entities as  SUBMITTED/DECLINED
     * If (Selected) then set status != submitted/declined/none
     * @param List<EntityDetailVO> entities
     * @param String actionType
     * @return List<EntityDetailVO> entities
     */
    private List<EntityDetailVO> updateEntityStatus(List<EntityDetailVO> entities, String actionType){
        if(entities == null || entities.size()==0)
            return null;
        if(actionType == null)
            return null;
            
        for(EntityDetailVO entity:entities){
            if(entity.isSelected()){
                if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_SUBMIT))
                    entity.setStatus(DecisionResultConstants.ENTITY_STATUS_SUBMITTED);
                else if(actionType.equalsIgnoreCase(DecisionResultConstants.ACTION_TYPE_DECLINE))
                    entity.setStatus(DecisionResultConstants.ENTITY_STATUS_DECLINED);
                else
                    entity.setStatus(DecisionResultConstants.ENTITY_STATUS_NONE);
            }
        }
        
        return entities;
    }
    
    /**
     * This method sets the selected flag for the node that is being selected
     * @param List<DecisionNodeVO> nodes
     * @param long currentSelectedValueId
     * @return List<DecisionNodeVO> nodes
     */
    private List<DecisionNodeVO> setSelectedNode(List<DecisionNodeVO> nodes, long currentSelectedValueId){
        if(nodes == null || nodes.size()==0)
            return null;
        for(DecisionNodeVO node:nodes){
            if(node.getValueId() == currentSelectedValueId)
                node.setSelected(true);
        }
        
        return nodes;
    }
    
    /**
     * This method sets the selected flag for the node that is being selected and the comment text
     * @param List<DecisionNodeVO> nodes
     * @param long currentSelectedValueId
     * @return List<DecisionNodeVO> nodes
     */
      private List<DecisionNodeVO> setSelectedNode(List<DecisionNodeVO> nodes, long currentSelectedValueId, String comment){
        if (nodes == null || nodes.size() == 0)
          return null;

        for (DecisionNodeVO node: nodes){
          if (node.getValueId() == currentSelectedValueId){
            node.setSelected(true);
            node.setCommentText(comment);
          }
        }

        return nodes;
      }
      
    /**
     * This method checks if validation required or not.
     * If validation is required then validateNodes() should be called. 
     * @param selectedEntities
     * @param existingEntities
     * @return
     */
    private boolean validationRequiredCheck(String selectedEntities, List<DecisionResultEntityVO> existingEntities){
        boolean required = true;
        
        //if there are no selected entities then no validation is required
        if(selectedEntities == null || selectedEntities.equals(""))
            return false;
        
        //if there are no saved entities then validation is required
         if(selectedEntities != null)
            if(existingEntities == null || existingEntities.size()==0)
                return true;
        
        /*
         * Loop through existingEntities and check if atleast one entity exist from selectedEntities. 
         * If one entity is common then return false since no validation is required
         * This code has been commented since this skips the validation for the subsequent node selections.
         */
        /*
        String temp[] = selectedEntities.split(",");
        List<String> sEntities = Arrays.asList(temp);
        
        for(DecisionResultEntityVO entityVO:existingEntities){
            if(sEntities.contains(String.valueOf(entityVO.getEntityValue())))
                return false;
            else 
                required = true;
        }
        */
        return required;
    }
    
    /**
     * This method checks if any entities selected vs existing is different
     * If the selected entities are different from existing then validation required.
     * If validation is required then validateNodes() will be called. 
     * @param selectedEntities
     * @param existingEntities
     * @return boolean
     */
    private boolean isEntitySelectionChanged(String selectedEntities, List<DecisionResultEntityVO> existingEntities){
        boolean required = true;
        
        //if there are no selected entities but there were entities selected earlier
        if(existingEntities != null && existingEntities.size()>0)
            if(selectedEntities == null || selectedEntities.equals(""))
                return true;
        
        //if there are no saved entities but now there are new selections 
         if(selectedEntities != null && !selectedEntities.equals(""))
            if(existingEntities == null || existingEntities.size()==0)
                return true;
        
        //parse the selected entities into List
        String temp[] = selectedEntities.split(",");
        List<String> sEntities = Arrays.asList(temp);
        //If the size of the selected vs existing doesn't match return
        if(sEntities.size() != existingEntities.size())
            return true;
        
        /*
         * Loop through existingEntities and check if atleast one entity is different from selectedEntities. 
         * If one entity is different then return true 
         */
        for(DecisionResultEntityVO entityVO:existingEntities){
            if(!sEntities.contains(String.valueOf(entityVO.getEntityValue())))
                return false;
            else 
                required = true;
        }
        
        return required;
    }
    
    /**
     * Method to validate each child node's business rules against the selected entity list
     * NOTE! existingEntities need not be check in this method as validationRequired will take care of the 
     * preliminary check.
     * @param List<DecisionNodeVO> nodes
     * @param List<EntityDetailVO> entities
     * @return List<DecisionNodeVO>
     */
    private List<DecisionNodeVO> validateNodes(List<DecisionNodeVO> nodes, List<EntityDetailVO> selectedEntities){
        List<DecisionNodeVO> resultNodes = new ArrayList();
        
        //if the nodes are null return null
        if(nodes == null || nodes.size()==0)
            return null;
        
        /*
         * Load the temp list of nodes that are eligible to validate
         * Temp has nodes that has active flag is true and has rules
         */
        List<DecisionNodeVO> activeNodes = new ArrayList();
        for(DecisionNodeVO node:nodes){
            if(node.isActive())
               activeNodes.add(node);
        }
        
        //if the selectedEntities is null then simply return the eligible nodes as valid
        if(selectedEntities == null || selectedEntities.size()==0){
            resultNodes.addAll(activeNodes);    
            return resultNodes;
        }
        
        /*
         * validate the nodes in temp list and return the validated nodes
         */
        if(activeNodes != null && activeNodes.size()>0){
            for(DecisionNodeVO nodeVO:activeNodes){
                if(nodeVO.isEligibleToValidate()){
                    if(validateNode(nodeVO, selectedEntities))
                        resultNodes.add(nodeVO);
                }
                else
                    resultNodes.add(nodeVO);
            }
        }
            
        return resultNodes;
    }
    
    /**
     * Method to validate current selected node's business rules against the selected entity list
     * If this method returns true then no need to validate the previously selected nodes against any entity changes.
     * If this method returns false then that node will be deleted from previous selection of nodes.
     * @param List<DecisionNodeVO> nodes
     * @param List<EntityDetailVO> entities
     * @return boolean
     */
    private boolean validateNode(DecisionNodeVO node, List<EntityDetailVO> selectedEntities){
        boolean valid = true;
        
        //Return false if the node is null
        if(node == null)
            return false;
            
        //If the node is not active and no rules attached then return false
        if(!node.isEligibleToValidate())
            return false;
        
        //if the selectedEntities is null then simply return the nodes as valid
        if(selectedEntities == null || selectedEntities.isEmpty())
            return true;
        
        List<DecisionNodeRuleVO> rules = node.getRules();
        //Loop through the entities and if one rule passes then node is valid.
        validated:
        for(EntityDetailVO detailVO:selectedEntities){
            if(detailVO.isSelected()){
                for(DecisionNodeRuleVO ruleVO:rules){
                    if(detailVO.validate(ruleVO)){
                        valid = true;
                        break validated;
                    }
                    else{
                        valid = false;
                        continue;
                    }
                }
            }
        }
        
        return valid;
    }
    
    /**
     * Method to validate current selected node's business rules against the selected entity list
     * This method gets the node based on the selected detailVO
     * @param DecisionResultDetailVO node
     * @param List<EntityDetailVO> selectedEntities
     * @param long decisionConfigId
     * @return boolean
     */
    private boolean validateNode(DecisionResultDetailVO detailVO, List<EntityDetailVO> selectedEntities, long decisionConfigId)throws Exception{
        boolean valid = false;
        
        //if the selectedEntities is null then simply return the nodes as valid
        if(selectedEntities == null || selectedEntities.isEmpty())
            return true;
        
        if(detailVO != null){
            DecisionNodeVO node = getNode(decisionConfigId, detailVO.getValueId());
            if(node != null){
                if(node.isEligibleToValidate())
                    valid = validateNode(node, selectedEntities);
                else valid = true;
            }
        }
        
        return valid;
    }
    
    /**
     * This method validates all the previous selections and delete all the subsequent invalid selections.
     * @param List<DecisionResultDetailVO> details
     * @param List<EntityDetailVO> orderEntities
     * @param long decisionConfigId
     * @param long resultId
     * @return boolean
     * @throws Exception
     */
    private boolean validatePreviousSelections(List<DecisionResultDetailVO> details, List<EntityDetailVO> orderEntities, long decisionConfigId, long resultId)throws Exception{
        boolean allPreviousNodesValid = true;
        
        if(details == null)
            return allPreviousNodesValid;
        
        //loop thru the list and validate each one
        for(DecisionResultDetailVO detail:details){
            //start with level 2 onwards, delete all the invalid selections.
            if(detail.getLevel()>1){
                if(!validateNode(detail,orderEntities,decisionConfigId)){
                    deletePreviousResultDetail(resultId, detail.getParentValueId());
                    allPreviousNodesValid = false;
                    continue;
                }
                
            }
        }
        
        return allPreviousNodesValid;
    }
    
    /**
     * This method will retrieve the call disposition Id for the specified decision type code and status
     * @param decisionTypeCode
     * @param statusCode
     * @return
     * @throws Exception
     */
    private long getDecisionConfigId(String decisionTypeCode, String statusCode) throws Exception{
        return decisionResultDAO.getConfigIdByTypeCode(decisionTypeCode, statusCode);
    }
    
    /**
     * This method will retrieve the maximum level available for the given config Id.
     * @param decisionConfigId
     * @return int maxLevel
     * @throws Exception
     */
    private int  getMaxLevelByConfig (long decisionConfigId) throws Exception{
        return decisionResultDAO.getMaxLevelByConfigId(decisionConfigId);
    }
    
    /**
     * This method will retrieve the DecisionResultVO for the given resultId
     * @param resultId long
     * @param decisionConfigId long
     * @return int DecisionResultVO
     * @throws Exception
     */
    private DecisionResultVO  getDecisionResultByResultId(long resultId, long decisionConfigId) throws Exception{
        return decisionResultDAO.getDecisionResultByResultId(resultId, decisionConfigId);
    }
    
    /**
     * This method will insert the new disposition record into dcsn_fx_results table
     * @param DecisionResultVO result
     * @return long resultId
     * @throws Exception
     */
    private long insertDecisionResult(int level, DecisionResultStatus status, String userId) throws Exception{
        DecisionResultVO resultVO = new DecisionResultVO();
        resultVO.setCreatedBy(userId);
        resultVO.setCurrentLevel(level);
        resultVO.setStatus(status);
        return insertDecisionResult(resultVO);
    }
    
    /**
     * This method will insert the new disposition record into dcsn_fx_results table
     * Also the stored procedure will insert the root node into teh detail table.
     * @param DecisionResultVO result
     * @return long resultId
     * @throws Exception
     */
    private long insertDecisionResult(DecisionResultVO result) throws Exception{
        return decisionResultDAO.insertDecisionResult(result);
    }
    
    /**
     * This method will update the disposition record in dcsn_fx_results table for the given result_id
     * @param DecisionResultVO result
     * @return boolean status
     * @throws Exception
     */
    private boolean updateDecisionResult(DecisionResultVO result) throws Exception{
        return decisionResultDAO.updateDecisionResult(result);
    }
    
    /**
     * This method will retrieve the nodes and rule available for the specified level, configId and parentValueId
     * @param long resultId
     * @param entityTypeName
     * @param selectedEntityIds
     * @param userId
     * @return
     * @throws Exception
     */
    private boolean saveDecisionResultEntities(long resultId, String selectedEntityIds, String entityTypeName, String userId) throws Exception{
        return decisionResultDAO.saveDecisionResultEntities(resultId, selectedEntityIds, entityTypeName, userId);
    }
    
    /**
     * This method will retrieve all the available entities for the specified callDispositionId
     * @param long resultId
     * @return List<DecisionResultEntityVO
     * @throws Exception
     */
    private List<DecisionResultEntityVO> getDecisionResultEntities(long resultId) throws Exception{
        return decisionResultDAO.getDecisionResultEntities(resultId);
    }
    
    /**
     * This method to save DecisionResultDetail object
     * @param DecisionResultDetailVO detail
     * @return long resultDetailId
     * @throws Exception
     */
    private long saveDecisionResultDetail(DecisionResultDetailVO detail) throws Exception{
        return decisionResultDAO.saveDecisionResultDetail(detail);
    }
    
    /**
     * This method will retrieve all the available disposition details for the specified resultId
     * @param resultId
     * @return List<DecisionResultDetailVO
     * @throws Exception
     */
    private List<DecisionResultDetailVO> getDecisionResultDetails(long resultId) throws Exception{
        return decisionResultDAO.getDecisionResultDetails(resultId);
    }
    
    /**
     * This method retrieves the disposition detail for the specified resultId and valueId
     * @param resultId
     * @param valueId
     * @return DecisionResultDetailVO
     * @throws Exception
     */
    private DecisionResultDetailVO getDecisionResultDetailByValueId(long resultId, long valueId, String nodeType) throws Exception{
        return decisionResultDAO.getDecisionResultDetailByValueId(resultId, valueId, nodeType);
    }   
    
    /**
     * Method to insert disposition details in to COMMENTS table based on the resultId
     * @param long resultId
     * @param String decisinTypeCode
     * @param long dnsiId
     * @param String userId
     * @return boolean status
     * @throws Exception
     */
    private boolean saveResultEntityComments(long resultId, String decisinTypeCode, long dnsiId, String userId) throws Exception{
        return decisionResultDAO.saveResultEntityComments(resultId, decisinTypeCode, dnsiId, userId);
    }
    
    /**
     * Method to delete record from DCSN_FX_RESULT_DETAILS table based on the resultId
     * @param long resultId
     * @param long currentSelectedValueId
     * @return boolean status
     * @throws Exception
     */
    private boolean deleteDecisionResultDetail(long resultId, long currentSelectedValueId) throws Exception{
        return decisionResultDAO.deleteDecisionResultDetail(resultId, currentSelectedValueId);
    }
    
    /**
     * Method to delete previously selected records from DCSN_FX_RESULT_DETAILS table based on the resultId and parentValueId
     * @param long resultId
     * @param long parentValueId
     * @return boolean status
     * @throws Exception
     */
    private boolean deletePreviousResultDetail(long resultId, long parentValueId) throws Exception{
        return decisionResultDAO.deletePreviousResultDetails(resultId, parentValueId);
    }
    
    /**
     * This method will retrieve the details for each entityId of type order, 
     * Builds and returns the list of entityDetailVO that has selected, required and submit/decline status set.
     * @param entityIds
     * @return List<EntityDetailsVO>
     * @throws Exception
     */
    private List<EntityDetailVO> getEntityTypeOrderDetails(List<EntityDetailVO> entities, String selectedEntities)throws Exception{
        if(entities == null || entities.size()==0)
            return null;
            
        StringBuffer entityIds = new StringBuffer();
        for(int i=0;i<entities.size();i++){
            if(i==0)
                entityIds.append(entities.get(i).getEntityId());
            else{
                entityIds.append(","); 
                entityIds.append(entities.get(i).getEntityId());
            }
        }
        
        List<EntityDetailVO> entityDetails = (List<EntityDetailVO>)decisionResultDAO.getEntityTypeOrderDetails(entityIds.toString()); 
        if(entityDetails == null || entityDetails.size()==0)
            return null;
        
        //loop through entities and entitydetails and set the required and status flag.
        for(EntityDetailVO detailVO:entityDetails){
            for(EntityDetailVO available:entities){
                if(detailVO.getEntityId() == available.getEntityId()){
                    detailVO.setRequired(available.isRequired());
                    detailVO.setStatus(available.getStatus());
                }
            }
        }
        
        /*
         * set the selected flag for each entity detail VO
         * Only if the there are any selected entities. 
         */
        if(selectedEntities != null && selectedEntities.length()>0){
            String temp[] = selectedEntities.split(",");
            if(entityDetails!=null&&entityDetails.size()>0){
                for(EntityDetailVO entity:entityDetails){
                    for(int i=0; i<temp.length;i++){
                        if(entity.getEntityId() == Long.parseLong(temp[i])){
                            entity.setSelected(true);
                        }
                    }
                }
            }
        }
        return entityDetails;
    }
    
   
    /**
     * This method will retrieve the details for each entityId of type customer, 
     * Builds and returns the list of entityDetailVO
     * @param entities
     * @param selectedEntities
     * @return
     * @throws Exception
     */
    private List<EntityDetailVO> getEntityTypeCustomerDetails(List<EntityDetailVO> entities, String selectedEntities)throws Exception{
        //Left blank for future implementation
        return null;
    }
    
    /**
     * This method will retrieve the nodes and rules available for the specified level, configId and parentValueId
     * @param decisionConfigId
     * @param level
     * @param valueId
     * @return
     * @throws Exception
     */
    private List<DecisionNodeVO> getNodesAndRulesByLevel(long decisionConfigId, int level, long valueId)throws Exception{
        return decisionResultDAO.getNodesAndRulesByLevel(decisionConfigId, level, valueId);
    }
        
    /**
     * This method will retrieve the parent node for the specified configId and parentValueId
     * @param decisionConfigId
     * @param valueId
     * @return DecisionNodeVO
     * @throws Exception
     */
    private DecisionNodeVO getParentNode(long decisionConfigId, long valueId)throws Exception{
        return getNode(decisionConfigId, valueId);
    }
    
    /**
     * Method will use getParentNode to get the node
     * @param decisionConfigId
     * @param valueId
     * @return
     * @throws Exception
     */
    private DecisionNodeVO getNode(long decisionConfigId, long valueId)throws Exception{
        return decisionResultDAO.getParentNode(decisionConfigId, valueId);
    }
    
    /**
     * This method parses the availableEntities string example
     * ORDER_DETAIL_ID:100001:TRUE:NONE,ORDER_DETAIL_ID:100002:TRUE:SUBMITTED,CUSTOMER_ID:200001:FALSE:NONE
     * @param availableEntities
     * @param entityType
     * @return String of comma separated values of entityType
     */
    private String parseEntities(String availableEntities, String entityType){
        //DecisionResultConstants.ENTITY_CUSTOMER_ID
        List<String> orderEntities = new ArrayList();
        List<String> customerEntities = new ArrayList();
        if(availableEntities.length()==0)
            return null;
            
        String temp[] = availableEntities.split(",");
        for(int i=0;i<temp.length;i++){
            String s1[]= temp[i].split(":");
            for(int j=0;j<s1.length;j++){
                if(s1[j].equalsIgnoreCase(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID)){
                    orderEntities.add(s1[++j]);
                }
                else if(s1[j].equalsIgnoreCase(DecisionResultConstants.ENTITY_CUSTOMER_ID)){
                    customerEntities.add(s1[++j]);
                }
            }
        }
        
        if(entityType.equalsIgnoreCase(DecisionResultConstants.ENTITY_CUSTOMER_ID))
            return customerEntities.toString();
        else if(entityType.equalsIgnoreCase(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID))
            return orderEntities.toString();
        else 
            return null;
    }
    
    /**
     * This method parses the availableEntities string into a List
     * Example:  ORDER_DETAIL_ID:100001:TRUE:NONE,ORDER_DETAIL_ID:100002:TRUE:SUBMITTED,CUSTOMER_ID:200001:FALSE:NONE
     * @param availableEntities
     * @param entityType
     * @return List<EntityDetailVO> entities.
     */
    private List<EntityDetailVO> parseEntitiesToList(String availableEntities, String entityType){
        //DecisionResultConstants.ENTITY_CUSTOMER_ID
        List<EntityDetailVO> orderEntities = new ArrayList();
        List<EntityDetailVO> customerEntities = new ArrayList();
        if(availableEntities.length()==0)
            return null;
            
        String temp[] = availableEntities.split(",");
        for(int i=0;i<temp.length;i++){
            String s1[]= temp[i].split(":");
            for(int j=0;j<s1.length;j++){
                if(s1[j].equalsIgnoreCase(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID)){
                    EntityDetailVO detailVO = new EntityDetailVO();
                    detailVO.setEntityName(s1[j]);
                    detailVO.setEntityId(Long.parseLong(s1[++j]));
                    detailVO.setRequired(s1[++j]);
                    detailVO.setStatus(s1[++j]);
                    orderEntities.add(detailVO);
                }
                else if(s1[j].equalsIgnoreCase(DecisionResultConstants.ENTITY_CUSTOMER_ID)){
                    EntityDetailVO detailVO = new EntityDetailVO();
                    detailVO.setEntityName(s1[j]);
                    detailVO.setEntityId(Long.parseLong(s1[++j]));
                    detailVO.setRequired(s1[++j]);
                    detailVO.setStatus(s1[++j]);
                    customerEntities.add(detailVO);
                }
            }
        }
        
        if(entityType.equalsIgnoreCase(DecisionResultConstants.ENTITY_CUSTOMER_ID))
            return customerEntities;
        else if(entityType.equalsIgnoreCase(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID))
            return orderEntities;
        else 
            return null;
    }
    
    /**
     * This method parses List<EntityDetailVO> back to the String availableEntities with the status update
     * example: ORDER_DETAIL_ID:100001:TRUE:NONE,ORDER_DETAIL_ID:100002:TRUE:SUBMITTED,CUSTOMER_ID:200001:FALSE:NONE
     * @param String availableEntities
     * @param List<EntityDetailVO> entities
     * @return String.
     */
    private String parseEntitiesToString(List<EntityDetailVO> orderEntities, List<EntityDetailVO> customerEntities){
        StringBuffer availableEntities = new StringBuffer(); 
        
        if(orderEntities != null && orderEntities.size()>0){
            for(EntityDetailVO detail:orderEntities){
                availableEntities.append(detail.getEntityName()).append(":").append(detail.getEntityId()).append(":")
                            .append(detail.isRequired()==true?"TRUE":"FALSE").append(":").append(detail.getStatus()).append(",");       
            }
        }
        if(customerEntities != null && customerEntities.size()>0){
            for(EntityDetailVO detail:customerEntities){
                availableEntities.append(detail.getEntityName()).append(":").append(detail.getEntityId()).append(":")
                            .append(detail.isRequired()==true?"TRUE":"FALSE").append(":").append(detail.getStatus()).append(",");       
            }
        }
        
        availableEntities.deleteCharAt(availableEntities.length()-1);
        
        return availableEntities.toString();
    }
    
   
     /**
      * This method parses the String availableEntities against List<EntityDetailVO> to update the status 
      * example: ORDER_DETAIL_ID:100001:TRUE:NONE,ORDER_DETAIL_ID:100002:TRUE:SUBMITTED,CUSTOMER_ID:200001:FALSE:NONE
      * @param List<EntityDetailVO> entities
      * @return String.
      */
    private String parseEntitiesToString(List<EntityDetailVO> allEntities){
        //Return null if the allEntities is null or size is 0
        if(allEntities == null || allEntities.size()==0)
            return null;
            
        StringBuffer availableEntities = new StringBuffer(); 
        
        if(allEntities != null && allEntities.size()>0){
            for(EntityDetailVO detail:allEntities){
                availableEntities.append(detail.getEntityName()).append(":").append(detail.getEntityId()).append(":")
                            .append(detail.isRequired()==true?"TRUE":"FALSE").append(":").append(detail.getStatus()).append(",");       
            }
        }
        
        availableEntities.deleteCharAt(availableEntities.length()-1);
        
        return availableEntities.toString();
    }

    /**
     * This method parses the availableEntities string and if there is only one order then it autochecks it
     * ORDER_DETAIL_ID:100001:TRUE:NONE,ORDER_DETAIL_ID:100002:TRUE:SUBMITTED,CUSTOMER_ID:200001:FALSE:NONE
     * @param availableEntities
     * @param entityType
     * @return String of comma separated values of entityType
     */
    private String autoCheckEntities(String availableEntities, String entityType){
        //DecisionResultConstants.ENTITY_CUSTOMER_ID
        List<String> orderEntities = new ArrayList();
        List<String> customerEntities = new ArrayList();
        if(availableEntities.length()==0)
            return null;
            
        String temp[] = availableEntities.split(",");
        for(int i=0;i<temp.length;i++){
            String s1[]= temp[i].split(":");
            for(int j=0;j<s1.length;j++){
                if(s1[j].equalsIgnoreCase(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID)){
                    orderEntities.add(s1[++j]);
                }
                else if(s1[j].equalsIgnoreCase(DecisionResultConstants.ENTITY_CUSTOMER_ID)){
                    customerEntities.add(s1[++j]);
                }
            }
        }
        
        if(entityType.equalsIgnoreCase(DecisionResultConstants.ENTITY_CUSTOMER_ID))
            return customerEntities.toString();
        else if(entityType.equalsIgnoreCase(DecisionResultConstants.ENTITY_ORDER_DETAIL_ID))
            return orderEntities.toString();
        else 
            return null;
    }
    
    /**
     * Method to retrieve the user id based of securityToken from request.
     * @param securityToken
     * @return String userID
     * @throws Exception
     */
    public String getUserId(String securityToken)throws Exception {
        String userId = null;
        
        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
            userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
        }
       
         //uncomment this code to get the userId in local testing
//        if(userId == null || userId.equals(""))
//            userId="Anonymous";
        
        return userId;
    }
}
