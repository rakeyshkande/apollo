package com.ftd.decisionresult.constant;

/**
 * Represents DCSN_FX_CONFIG_VALUE
 * @author Vinay Shivaswamy
 * @date 2/16/2011
 * @version 1.0 for CD - 4.8.0
 */
public abstract class DecisionResultConstants{
 
    
    /*********************************************************************************************
        Constants for stored procs
    *********************************************************************************************/
    
    public static final String GET_DECISION_CONFIG_ID           = "GET_DECISION_CONFIG_ID";
    public static final String GET_RESULT_BY_ID                 = "GET_RESULT_BY_ID";
    public static final String GET_DSCN_CONFIG_MAX_LEVEL        = "GET_DSCN_CONFIG_MAX_LEVEL";
    public static final String GET_DCSN_NODES_RULES_BY_LEVEL    = "GET_DCSN_NODES_RULES_BY_LEVEL";
    public static final String GET_DCSN_NODE                    = "GET_DCSN_NODE";
    public static final String INSERT_DECISION_RESULT           = "INSERT_DECISION_RESULT";
    public static final String UPDATE_DECISION_RESULT           = "UPDATE_DECISION_RESULT";
    public static final String GET_DECISION_RESULT_ENTITIES     = "GET_DECISION_RESULT_ENTITIES";
    public static final String SAVE_DECISION_RESULT_ENTITIES    = "SAVE_DECISION_RESULT_ENTITIES";
    public static final String GET_RESULT_DETAIL_BY_VALUE_ID    = "GET_RESULT_DETAIL_BY_VALUE_ID";
    public static final String GET_DECISION_RESULT_DETAILS      = "GET_DECISION_RESULT_DETAILS";
    public static final String SAVE_DECISION_RESULT_DETAIL      = "SAVE_DECISION_RESULT_DETAIL";
    public static final String DELETE_DECISION_RESULT_DETAIL    = "DELETE_DECISION_RESULT_DETAIL";
    public static final String DELETE_PREVIOUS_RESULT_DETAILS   = "DELETE_PREVIOUS_RESULT_DETAILS";
    public static final String SAVE_RESULT_ENTITY_COMMENTS      = "SAVE_RESULT_ENTITY_COMMENTS";
    public static final String GET_ORDER_INFO_BY_ORD_DET_IDS    = "GET_ORDER_INFO_BY_ORD_DET_IDS";
    
    public static final String OUT_STATUS   = "OUT_STATUS";
    public static final String OUT_MESSAGE  = "OUT_MESSAGE";
        
    public static final String RESULT_ENTITY_CALL_TYPE_NAME_CUSTOMER    = "CLEAN.CUSTOMER.CUSTOMER_ID";
    public static final String RESULT_ENTITY_CALL_TYPE_NAME_ORDER       = "CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID";
    
    public static final String INTERNATIONAL_DELIVERY   = "I";
    public static final String FLORIST_DELIVERED        = "F";
    public static final String VENDOR_DELIVERED         = "V";
    public static final String TRUE_STATE               = "Y";
    public static final String FALSE_STATE              = "N";
        
    /*********************************************************************************************
        Request param constants
    *********************************************************************************************/
    //filter will build widget parameters and set it on request/response 
    public static final String CONS_APP_PARAMETERS                  = "parameters";   // This must match COM's constant (of same name)
    public static final String WIDGET_PARAMETERS                    = "widgetParameters";
    
    //The widget parameters and some are stored as hidden fields
    public static final String REQ_ACTION_TYPE                      = "dr_actionType"; //could be load/next/back/submit/decline
    public static final String REQ_DECISION_CONFIG_ID               = "dr_decisionConfigId";
    public static final String REQ_RESULT_ID                        = "dr_resultId";
    public static final String REQ_DECISION_TYPE_CODE               = "dr_decisionTypeCode"; //for now "callDisposition"
    public static final String REQ_DECISION_STATUS                  = "dr_decisionStatus"; //could be draft/publish
    public static final String REQ_DISPOSITION_STATUS               = "dr_dispositionStatus"; //could be view/null
    public static final String REQ_AVAILABLE_ENTITIES               = "dr_availableEntities";
    public static final String REQ_SELECTED_ENTITIES                = "dr_selectedEntities"; //comma separtaed ids that are selected in UI
    public static final String REQ_CURRENT_LEVEL                    = "dr_currentLevel";
    public static final String REQ_CURRENT_LEVEL_PARENT_VALUE_ID    = "dr_currentLevelParentValueId";
    public static final String REQ_CURRENT_LEVEL_SELECTED_VALUE_ID  = "dr_currentLevelSelectedValueId";
    public static final String REQ_CURRENT_LEVEL_SELECTED_COMMENT   = "dr_currentLevelComment";
    public static final String REQ_PREVIOUS_LEVEL_VALUE_ID          = "dr_previousValueId";
    public static final String REQ_PREVIOUS_LEVEL                   = "dr_previousLevel";
    public static final String REQ_DNIS_ID                          = "dr_dnisId";
    
    /*********************************************************************************************
        Application constants
    *********************************************************************************************/
    
    public static final int DISPO_MIN_LEVEL                 = 2;
    public static final String ENTITY_CUSTOMER_ID           = "CUSTOMER_ID";
    public static final String ENTITY_ORDER_DETAIL_ID       = "ORDER_DETAIL_ID";
    public static final String CALL_DISPOSITION_TYPE_CODE   = "calldisposition";
    public static final String DCSN_STATUS_PUBLISHED        = "PUBLISH";
    public static final String DCSN_STATUS_DRAFT            = "DRAFT";
    public static final String DISPOSITION_STATUS_VIEW      = "view";
    public static final String MAX_LEVEL                    = "MAX_LEVEL";
    public static final String MIN_LEVEL                    = "MIN_LEVEL";
    public static final String ACTION_TYPE_LOAD             = "load";
    public static final String ACTION_TYPE_NEXT             = "next";
    public static final String ACTION_TYPE_BACK             = "back";
    public static final String ACTION_TYPE_SUBMIT           = "submit";
    public static final String ACTION_TYPE_DECLINE          = "decline";
    public static final String CSR_ID                       = "CUSTOMER_ID";
    public static final String VALUE_ID                     = "VALUE_ID";
    public static final String PARENT_VALUE_ID              = "PARENT_VALUE_ID";
    
    //Entity related constants
    public static final String ENTITY_AVAILABLE_STRING          = "availableEntitiesString";
    public static final String ENTITY_SELECTED_STRING           = "selectedEntitiesString";
    public static final String ENTITY_AVAIL_ORDER_LIST          = "orderDetailIdList";
    public static final String ENTITY_AVAIL_CUSTOMER_LIST       = "customerIdList";
    public static final String ENTITY_SEL_ALL_DETAILS_LIST      = "entities";
    public static final String ENTITY_SEL_ORDER_DETAILS_LIST    = "orderEntities";
    public static final String ENTITY_SEL_CUSTOMER_DETAILS_LIST = "customerEntities";
    public static final String ENTITY_EXISTING_LIST             = "existingEntityList";
    public static final String ENTITY_STATUS_NONE               = "NONE";
    public static final String ENTITY_STATUS_SUBMITTED          = "SUBMITTED";
    public static final String ENTITY_STATUS_DECLINED           = "DECLINED";
    public static final String ENTITY_DELIMITER                              = ",";
    public static final String ENTITY_ELEMENT_DELIMITER                      = ":";
    public static final int    ENTITY_ELEMENT_INDEX_ORDER_ID                 = 1;
    public static final int    ENTITY_ELEMENT_INDEX_REQUIRED_FLAG            = 2;
    public static final int    ENTITY_ELEMENT_INDEX_STATUS                   = 3;
    public static final String NOT_REQUIRED                                  = "false";
    public static final String REQUIRED                                      = "true";
    public static final String ENTITY_STATUS_DEFAULT                         = "none";

    
    //Widget filter data set/reset status for flag
    public static final String WIDGET_PARAMETERS_FLAG_SET       = "set";
    public static final String WIDGET_PARAMETERS_FLAG_RESET     = "reset";
    
    //validation rules related
    public static final String CD_FLORIST_DELIVERED = "cd.prodfloristdlvrd";
    public static final String CD_VENDOR_DELIVERED  = "cd.prodvendordlvrd";
    public static final String CD_INT_DELIVERY      = "cd.prodinternational";
    public static final String CD_VASE_ADDON        = "cd.prodhasvaseaddon";
    public static final String CD_NON_VASE_ADDON    = "cd.prodhasnonvaseaddon";
    public static final String CD_SHOW_NO_REFUND    = "cd.shownotrefund";
    
    /*********************************************************************************************
        Security parameters
    *********************************************************************************************/
    public static final String CONTEXT              = "context";
    public static final String SECURITY_TOKEN       = "securitytoken";
    public static final String SECURITY_IS_ON       = "SECURITY_IS_ON";
    public static final String ADMIN_ACTION         = "adminAction";
     
    /*********************************************************************************************
        Config flie locations
    *********************************************************************************************/
    public static final String CONFIG_FILE                        = "decisionresult_config.xml";
    public static final String SECURITY_FILE                      = "security-config.xml";
    public static final String DATASOURCE_NAME                    = "application.db.datasource";
    public static final String DECISIONFRAMEWORK_CONFIG_CONTEXT   = "decisionresult_config";
    public static final String FTD_APPS_CONTEXT                   = "FTDAPPS_PARMS";

    /*********************************************************************************************
        Decision Result Filter constants
    *********************************************************************************************/
    public static final String    DRF_AVAILABLE_ENTITIES                            = "dr_availableEntities";
    public static final String    DRF_CURRENT_LEVEL                                 = "dr_currentLevel";
    public static final String    DRF_CURRENT_LEVEL_PARENT_VALUE_ID                 = "dr_currentLevelParentValueId";
    public static final String    DRF_CURRENT_LEVEL_SELECTED_COMMENT                = "dr_currentLevelComment";
    public static final String    DRF_CURRENT_LEVEL_SELECTED_VALUE_ID               = "dr_currentLevelSelectedValueId";
    public static final String    DRF_DECISION_CONFIG_ID                            = "dr_decisionConfigId";
    public static final String    DRF_DECISION_STATUS                               = "dr_decisionStatus"; //could be draft/publish
    public static final String    DRF_DECISION_TYPE_CODE                            = "dr_decisionTypeCode"; //for now "callDisposition"
    public static final String    DRF_DISPOSITION_STATUS                            = "dr_dispositionStatus"; //could be view/null
    public static final String    DRF_DNIS_ID                                       = "dr_dnisId";
    public static final String    DRF_OPEN_FLAG                                     = "dr_openFlag";
    public static final String    DRF_PREVIOUS_LEVEL_VALUE_ID                       = "dr_previousValueId";
    public static final String    DRF_RESET_FLAG                                    = "dr_resetFlag";
    public static final String    DRF_RESULT_ID                                     = "dr_resultId";
    public static final String    DRF_SELECTED_ENTITIES                             = "dr_selectedEntities"; //comma separtaed ids that are selected in UI
   
    
     
}
