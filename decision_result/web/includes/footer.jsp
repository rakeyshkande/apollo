<script type="text/javascript" src="js/copyright.js"></script>
<div class="footer">
  <table border="0" width="90%">
    <tbody>
      <tr>
        <td width="200px">
          <span id="actionButtonProcessing" style="vertical-align:middle;visibility:hidden">
            <img 
              alt="Processing..."
              title="Processing..."
              src="images/pleasewait.gif"
              height="16px"
              width="200px"/>        
          </span>
        </td>
        	<td class="disclaimer"></td><script>showCopyright();</script>        
        <td width="200px">&nbsp;</td>
      </tr>
    </tbody>
  </table>
</div>
</body> 
</html>