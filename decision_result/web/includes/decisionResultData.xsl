<?xml version="1.0" encoding="windows-1252" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="availableEntities"><xsl:value-of select="key('security','dr_availableEntities')/value" /></xsl:param>
  <xsl:param name="currentLevel"><xsl:value-of select="key('security','dr_currentLevel')/value" /></xsl:param>  
  <xsl:param name="currentLevelParentValueId"><xsl:value-of select="key('security','dr_currentLevelParentValueId')/value" /></xsl:param>  
  <xsl:param name="currentLevelComment"><xsl:value-of select="key('security','dr_currentLevelComment')/value" /></xsl:param>
  
  <xsl:param name="currentLevelSelectedValueId"><xsl:value-of select="key('security','dr_currentLevelSelectedValueId')/value" /></xsl:param>  
  
  <xsl:param name="decisionConfigId"><xsl:value-of select="key('security','dr_decisionConfigId')/value" /></xsl:param>
  
  <xsl:param name="decisionStatus"><xsl:value-of select="key('security','dr_decisionStatus')/value" /></xsl:param>  
  
  <xsl:param name="decisionTypeCode"><xsl:value-of select="key('security','dr_decisionTypeCode')/value" /></xsl:param>  
  
  <xsl:param name="dispositionStatus"><xsl:value-of select="key('security','dr_dispositionStatus')/value" /></xsl:param>  
  
  <xsl:param name="dnisId"><xsl:value-of select="key('security','dr_dnisId')/value" /></xsl:param>  
  
  
  <xsl:param name="openFlag"><xsl:value-of select="key('security','dr_openFlag')/value" /></xsl:param>  
  <xsl:param name="previousValueId"><xsl:value-of select="key('security','dr_previousValueId')/value" /></xsl:param>  
  <xsl:param name="resetFlag"><xsl:value-of select="key('security','dr_resetFlag')/value" /></xsl:param>  
  <xsl:param name="resultId"><xsl:value-of select="key('security','dr_resultId')/value" /></xsl:param>  
  <xsl:param name="selectedEntities"><xsl:value-of select="key('security','dr_selectedEntities')/value" /></xsl:param>
  <xsl:param name="previousLevel"><xsl:value-of select="key('security','dr_previousLevel')/value" /></xsl:param>
  
  
  <xsl:output method="html" indent="yes"/>
  
  <xsl:template name="decisionResultData">
    
    
    
    <input type="hidden" id="dr_availableEntities"           name="dr_availableEntities"           value="{$availableEntities}"/>
    <input type="hidden" id="dr_currentLevel"                name="dr_currentLevel"                value="{$currentLevel}"/>
    <input type="hidden" id="dr_currentLevelParentValueId"   name="dr_currentLevelParentValueId"   value="{$currentLevelParentValueId}"/>
    <input type="hidden" id="dr_currentLevelComment"         name="dr_currentLevelComment"         value="{$currentLevelComment}"/>
    <input type="hidden" id="dr_currentLevelSelectedValueId" name="dr_currentLevelSelectedValueId" value="{$currentLevelSelectedValueId}"/>
    <input type="hidden" id="dr_decisionConfigId"            name="dr_decisionConfigId"            value="{$decisionConfigId}"/>
    <input type="hidden" id="dr_decisionStatus"              name="dr_decisionStatus"              value="{$decisionStatus}"/>
    <input type="hidden" id="dr_decisionTypeCode"            name="dr_decisionTypeCode"            value="{$decisionTypeCode}"/>
    <input type="hidden" id="dr_dispositionStatus"           name="dr_dispositionStatus"           value="{$dispositionStatus}"/>
    <input type="hidden" id="dr_dnisId"                      name="dr_dnisId"                      value="{$dnisId}"/>
    <input type="hidden" id="dr_openFlag"                    name="dr_openFlag"                    value="{$openFlag}"/>
    <input type="hidden" id="dr_previousValueId"             name="dr_previousValueId"             value="{$previousValueId}"/>
    <input type="hidden" id="dr_resetFlag"                   name="dr_resetFlag"                   value="{$resetFlag}"/>
    <input type="hidden" id="dr_resultId"                    name="dr_resultId"                    value="{$resultId}"/>
    <input type="hidden" id="dr_selectedEntities"            name="dr_selectedEntities"            value="{$selectedEntities}"/>
    <input type="hidden" id="dr_previousLevel"               name="dr_previousLevel"               value="{$previousLevel}"/>
   
    
    
  </xsl:template>
</xsl:stylesheet>



  