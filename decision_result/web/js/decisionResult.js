var widgetMessageFlag = null;
var widgetErrorFlag = "N";
var optionalMessage = "Would you like to submit a call disposition?";
var mandatoryMessage = "A call disposition must be submitted for the order number(s) in red.";
var callDispDirty = false;
var pageRefreshed = true;
var v_allowComments = true;
var v_submitFlag = false;
var v_origSelectedEntities = "";
var v_selectedHasChild = "";
var v_childNodes;
var v_entityDetails;
var v_selectedExtOrdNumbers = "";
$(document).ready(function() {
 if($("#dr_openFlag").val() == "Y"){
    loadCallDisp();
 }

});

/*
 * Determines if all orders in Entity List have been submitted or don't require submission
 */
function dcsnExitAllowed() {
    var retVal = true;
    var entityList = document.getElementById('dr_availableEntities');
    
    // Entity List is of format: order_detail_id:<id>:<true||false>:<submitted||declined||none>[, ...]
    // where 'true' would indicate diposition is required and 'none' would mean not submitted
    //
    if ((entityList != null) && (entityList.value.toLowerCase().indexOf(':true:none') >= 0)) {
        retVal = false;
    }
    // If widget encountered error it will set error flag.  We always allow exit if so.
    if ((widgetErrorFlag != null) && (widgetErrorFlag == 'Y')) {
        retVal = true;  
    }
    return retVal;
}

/*
 * Determines if any optional items in Entity List were either submitted or declined
 */
function dcsnWereAnySubmittedOrDeclined() {
    var retVal = true;
    var entityList = document.getElementById('dr_availableEntities');

    // Entity List is of format: order_detail_id:<id>:<true||false>:<submitted||declined||none>[, ...]
    // where 'false' would mean not required and 'none' would mean not submitted
    //
    if (entityList != null) {
      var entityListTrim = entityList.value.replace(/^\s+/,'').replace(/\s+$/,'');

      if ((entityListTrim.length > 0) &&
          (entityListTrim.toLowerCase().indexOf('false') >= 0) && 
          (entityListTrim.toLowerCase().indexOf('false:submitted') < 0) && 
          (entityListTrim.toLowerCase().indexOf('false:declined')  < 0)) 
      {
        retVal = false;
      }
    }
    // If widget encountered error it will set error flag.  We always allow exit if so.
    if ((widgetErrorFlag != null) && (widgetErrorFlag == 'Y')) {
        retVal = true;  
    }
    return retVal;
}

/*
 * Sets status to declined for all optional orders in Entity List
 */
function dcsnDeclineAllOptional() {
    var entityList = document.getElementById('dr_availableEntities');
    if (entityList != null) {
      var entityListStr = entityList.value.toLowerCase().replace(/false:none/g,'false:declined');
      entityList.value = entityListStr;
    }
}

/*
 * Clears Call Disposition parameters
 */
function dcsnClearParams() {

    // Sets flag to signal data filter to clear Call Disposition params.
    //
    var resetFlag = document.getElementById('dr_resetFlag');
    if (resetFlag != null) {
        resetFlag.value = 'Y';
    }
}


function showWidget(d){
  //jQuery("#"+d).corner();
  $("#"+d).animate({ height: 'show', opacity: 'show' }, 'slow');
  $("#dr_openFlag").val("Y");
  
}

function hideWidget(d){
  $("#"+d).animate({ height: 'hide', opacity: 'hide' }, 'slow');
  $("#dr_openFlag").val("N");
  
}


//function ShowHide(d){  
//    //jQuery("#"+d).corner();
//    jQuery("#"+d).animate({"height": "toggle"}, { duration: 500 });
//    //This will toggle openFlag hidden variable when t
//    $("#openFlag").val()? $("#openFlag").val(false):$("#openFlag").val(true);
// }  

//jQuery(function() {
//        jQuery( "#dispDiv" ).draggable().resizable();
//});

function drag(d){  
  $("#"+d).draggable();
  
}

function resize(d){  
  //$("#"+d).resizable();
  
}

function changeLevel1(){  
    callDispDirty = true;
    alert("callDispDirty value set to : "+callDispDirty);
}  

//This method adds parameters as key/value pairs to the params arraylist
function addParam(params, name, value){
  if(!name || !value) {
    return;
  }
  params.push(name+'='+escape(value));
}
//This method will security parameters to the params arraylist.
function addSecurityParams(params){
    
    if( $("#securitytoken").val() != undefined ) {
        addParam(params, "securitytoken",$("#securitytoken").val());
    }else{
        addParam(params, "securitytoken","testToken");
    }
    
    if( $("#applicationcontext").val() != undefined ) {
        addParam(params, "applicationcontext",$("#applicationcontext").val());
    }else{
        addParam(params, "applicationcontext","testAppContext");
    }
    
    if( $("#context").val() != undefined ) {
        addParam(params, "context",$("#context").val());
    }else{
        addParam(params, "context","testContext");
    }
}

//This method helps to build a query string from parameter arraylist
function buildQueryString(params){
    var urlString = "";
    var idx;
    
    for( idx=0; idx<params.length; idx++ ) {
      urlString+="&"+params[idx];
    }
    return urlString;
}

/*This method is used to submit ajax request. 
  On success it will call callback function otherwise it will call error function.
*/
function SubmitAjax(url, message, successFunc, errorFunc) {
   /* $("#dispDiv").block({ 
                message: '<h3 style="color: #000000"><img src="/decisionresult/js/busy.gif" />Processing....</h3>', 
                css: { border: '1px solid #decf98', width: '400px', height: '600px'} 
      });*/
    hideWidgetContent();
    $("#processDiv").show();
    $("#errorDiv").hide();
    
    $.ajax({
        type:'POST',
        url:url,
        data:message,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success:successFunc,
        error:errorFunc
        });
  
//    if(http.readyState == 4){
//      alert('test4');
//      document.getElementById('wait').style.visibility = "hidden";
//    }
//    else{
//    alert('else');
//      document.getElementById('wait').style.visibility = "visible";
//    }
};

function viewCallDisposition(){
    
    $("#dispDiv").show();
    $("#minButton").hide()
    var url = "/decisionresult/decisionresult.do?method=load";
    
    var params = new Array();
    addSecurityParams(params);
    addParam(params, "dr_actionType", "load");


    
    fillParamsArray(params);
    
    url = url+buildQueryString(params);
    //alert(url);
    SubmitAjax(url, null, processCallback, handleError);
    

}


function loadCallDispMandatory() {
    widgetMessageFlag = 'M';
    loadCallDisp();
}


function loadCallDispOptional() {
    widgetMessageFlag = 'O';
    loadCallDisp();
}


function toggleCallDisp() {
    if($("#dr_openFlag").val() == "Y"){
        hideWidget('dispDiv');
    } else {
        loadCallDisp();
    }
}


/*
This method is called to load call disposition widget.

*/
function loadCallDisp(){
    if(!pageRefreshed){
      showWidget('dispDiv');
      drag('dispDiv');
      resize('dispDiv');
      $("#dispDiv").css('overflow', 'auto');
      showDeclineOnOptional();
      return;
    }
    

    
    
    pageRefreshed = false;
    showWidget('dispDiv');
    var url = "/decisionresult/decisionresult.do?method=load";
    
    var params = new Array();
    addSecurityParams(params);
    addParam(params, "dr_actionType", "load");
    addParam(params, "dr_decisionTypeCode", "calldisposition");
    addParam(params, "dr_decisionStatus", "PUBLISH");
    addParam(params, "dr_dispositionStatus", "");
    addParam(params, "dr_currentLevel", "");
    addParam(params, "dr_currentLevelParentValueId", "");
    addParam(params, "dr_currentLevelSelectedValueId", "");
    addParam(params, "dr_currentLevelComment", "");
    addParam(params, "dr_previousValueId", "");
    fillParamsArray(params);
    //addParam(params, "dr_availableEntities", "order_detail_id:1691289:false:none");
    //addParam(params, "dr_selectedEntities", "1691262");
    //addParam(params, "dr_availableEntities", "order_detail_id:1691262:false:none,order_detail_id:1691280:false:none,order_detail_id:1691289:false:none,order_detail_id:1690205:false:none,order_detail_id:1671934:true:none");
    //addParam(params, "dr_availableEntities", "order_detail_id:1691262:false:none,order_detail_id:1691280:false:none,order_detail_id:1691289:false:none,order_detail_id:1690205:false:none");
    //addParam(params, "dr_availableEntities", "");
    

    url = url+buildQueryString(params);
    //alert("Swamy Comment : "+url);
    SubmitAjax(url, null, processCallback, handleError);
    
    drag('dispDiv');
    
    
    
}




function displayEntityDetails(data){
  $('#entityTbl').empty();
  if(data.entityDetails != null && data.entityDetails.length > 0){
    var selEntities = "";
    var selExtOrdNums = "";
    $('#entityDiv').show();
    var table = $('#entityTbl');
	  table.append('<tr></tr>');
    var tr = $('tr:last', table);
    tr.append('<td>Order Numbers(s) : </td>');
    table.append('<tr></tr>');
    var tr = $('tr:last', table);
    
    tr.append('<td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;<u>Order#</u></td>');
    tr.append('<td><u>Recipient</u></td>');
    tr.append('<td><u>Order Date</u></td>');
    v_entityDetails = data.entityDetails;
    $.each(data.entityDetails, function(i, entityDetails){
      var chkbox;
      table.append('<tr id="tr'+entityDetails.entityId+'"></tr>');
      tr = $('tr:last', table);
      if(entityDetails != null && data.entityDetails.length ==1){
        selEntities = selEntities + entityDetails.entityId + ",";
        selExtOrdNums = selExtOrdNums + entityDetails.externalOrderNumber + ",";
        chkbox = '<input class="dr_input" type="checkbox" id="'+entityDetails.entityId+'" value="'+entityDetails.entityId+'" name="'+entityDetails.entityId+'"  disabled = "true" checked />' ;
        
      }else{
        if(entityDetails.selected){
          selEntities = selEntities + entityDetails.entityId + ",";
          selExtOrdNums = selExtOrdNums + entityDetails.externalOrderNumber + ",";
          chkbox = '<input class="dr_input" type="checkbox" id="'+entityDetails.entityId+'" value="'+entityDetails.entityId+'" name="'+entityDetails.entityId+'" checked/>' ;
          
        }else{
          chkbox = '<input class="dr_input" type="checkbox" id="'+entityDetails.entityId+'" value="'+entityDetails.entityId+'" name="'+entityDetails.entityId+'" />' ; 
        }
      }
      //tr.append('<td width="2px">'+chkbox+'</td>');
      var txt = "";
      if(entityDetails.externalOrderNumber != null && entityDetails.externalOrderNumber.length > 0)
        tr.append('<td>'+chkbox+'&#160;'+padSpaces(entityDetails.externalOrderNumber, 20)+'</td>');
        
      //if first name is not null and take first initial of the first name.
      if(entityDetails.firstName != null && entityDetails.firstName.length > 0)
        txt =  txt + entityDetails.firstName.substring(0,1)+". ";
      else
        txt =  txt + "N/A ";
                 
      if(entityDetails.lastName != null && entityDetails.lastName.length > 0)
        txt =  txt + padSpaces(entityDetails.lastName, 25)+" ";
      else
        txt =  txt + padSpaces("N/A", 25)+" ";
      
      tr.append('<td>'+txt+'</td>');
      
      if(entityDetails.orderDate != null && entityDetails.orderDate.length > 0)
        tr.append('<td>'+entityDetails.orderDate+'</td>');
      else
        tr.append('<td>N/A</td>');
        
      
      
      if(entityDetails.selected){
          tr.removeClass();
          tr.addClass('entitySel');
      }else if(data.entityDetails.length == 1){
        tr.removeClass();
        tr.addClass('entitySel');
      }else{
        tr.removeClass();
        tr.addClass('entityNormal');
      }
     
//      if(entityDetails.required)
//        tr.append('<td>'+ chkbox+'<span style="color:red">'+txt+'</span></td>');
//      else
        //tr.append('<td>'+ chkbox+'<span  id="sp'+entityDetails.entityId+'" style="color:black">'+txt+'</span></td>');
      
//      tr.append('<td>

      
      
      

  });
    if(selEntities.length > 0){
      $("#dr_selectedEntities").val(selEntities.substring(0,selEntities.length-1));
      //This variable will preserve original selection which is used to validate entity selection to show next/submit.
      v_origSelectedEntities = selEntities.substring(0,selEntities.length-1);
    }else{
      $("#dr_selectedEntities").val("");
      v_origSelectedEntities = "";
    }
   if(selExtOrdNums.length > 0)
    v_selectedExtOrdNumbers = selExtOrdNums.substring(0,selExtOrdNums.length-1);
    
    
  }
  
 

}


function displayChildNodes(data){
  v_selectedHasChild = "";
  if(data.childNodes != null){
    $('#childNodeTbl').empty();
    var radio;
    $('#dr_currentLevelSelectedValueId').val("");
    $('#commentsTA').val("");
    $('#dr_currentLevelComment').val("");
    v_childNodes = data.childNodes;
    var table = $('#childNodeTbl');
	  
    $.each(data.childNodes, function(i, childNodes){
      table.append('<tr id="tr'+childNodes.valueId+'"></tr>');
      var tr = $('tr:last', table);
      if(childNodes.selected){
        radio = '<input class="dr_input" type="radio" id="'+childNodes.valueId+'" value="'+childNodes.valueId+'" name="childNodes" checked/>' ;
        $('#dr_currentLevelSelectedValueId').val(childNodes.valueId);
        if(childNodes.hasChildNodes)
          v_selectedHasChild = "T";
        else
          v_selectedHasChild = "F";
        
        if(childNodes.commentText != null && childNodes.commentText != undefined){
          $('#commentsTA').val(childNodes.commentText);
          $('#dr_currentLevelComment').val(childNodes.commentText);
        }else{
          $('#commentsTA').val("");
          $('#dr_currentLevelComment').val("");
        }
        tr.css("background-color","#D1BC63");
          
      }else{
        radio = '<input class="dr_input" type="radio" id="'+childNodes.valueId+'" value="'+childNodes.valueId+'" name="childNodes" />' ;
        tr.css("background-color","white");
      }
     
      
      tr.append('<td>'+ radio+' '+childNodes.name+'</td>');
      
//      $('#childNodeDiv').append(radio);
//      $('#childNodeDiv').append(document.createTextNode(childNodes.name));
//      $('#childNodeDiv').append(document.createElement('br'));
      
//      $("#"+childNodes.valueId).click(function() {
//        if(!data.parentNode.allowComments){
//          $("#next").trigger('click');
//        }
//
//      });
      
            
    });  
  }
}

function processViewCallback(data){
//$("#widgetContent").unblock();
//$("#dispDiv").unblock();
 $("#processDiv").hide();
 displayWidgetContent();
 
  if(data != null && !data.dispositionAvailable){
    handleDispNoFound();
    return;
  }  

  if(data != null){
    assignHiddenFields(data);
    $('#breadcrumbDiv').empty();
    if(data.parentNode.valueId != undefined && data.parentNode.valueId > 0){
      $('#breadcrumbDiv').show();
      if(data.decisionResult != null && data.decisionResult.details != null){
        $.each(data.decisionResult.details, function(i, details){
          if(i == 0){
            $('#breadcrumbDiv').append(document.createTextNode(details.name));
            
          }else{
            $('#breadcrumbDiv').append(document.createTextNode(" > "));
            $('<a/>').attr("href", "javascript:getPreviousSelectedNode("+details.valueId+");").appendTo("#breadcrumbDiv").text(details.name);
            
          }
          
        });
      } 
      displayEntityDetails(data);          
    }
    displayChildNodes(data);
    
    if(data.parentNode != null){
      if(data.parentNode.level != undefined)
        $('#stepLbl').text("Step "+ data.parentNode.level.toString()+" :");
      else
        $('#stepLbl').text("0");
      
      if(data.parentNode.scriptText != undefined)
        $('#questLbl').text(" "+data.parentNode.scriptText);
      else
        $('#questLbl').text("");
    }
    
    
    (data.parentNode.allowComments) ? $('#commentsDiv').show() : $('#commentsDiv').hide();
      
    (data.next && data.parentNode.allowComments) ? $('#nxtBtnDiv').show() : $('#nxtBtnDiv').hide();
    
    (data.back) ? $('#preBtnDiv').show() : $('#preBtnDiv').hide();
    
    if(data.submit){
      $('#subBtnDiv').show();
      v_submitFlag = true;
    }else{
      $('#subBtnDiv').hide();
      v_submitFlag = false;
    }
    (data.submit) ? $('#subBtnDiv').show() : $('#subBtnDiv').hide();
    
    (data.decline) ? $('#decBtnDiv').show() : $('#decBtnDiv').hide();
    
    

  }else{
    $("#errorDiv").text("Error occured");
    pageRefreshed = true;
    $("#errorDiv").show();
  }
  

}

function processCallback(data){
  if(data != null && !data.dispositionAvailable){
    handleDispNoFound();
    return;
  }
  if(data != null && data.decisionResult != null && data.decisionResult.status == "COMPLETE"){
    processSubmitCallback(data);
    return;
  }else if(data != null && data.decisionResult != null && data.decisionResult.status == "DECLINE"){
    processDeclineCallback(data);
    return;
  }
  //$("#dispDiv").unblock();
  $("#processDiv").hide();
  displayWidgetContent();
  if(data != null){
    $('#widgetContent').show();
    assignHiddenFields(data);
    $('#breadcrumbDiv').empty();
    if(data.parentNode != null && data.parentNode.valueId != undefined && data.parentNode.valueId > 0){
      if(data.decisionResult != null && data.decisionResult.details != null){
        $('#breadcrumbDiv').show();
        $.each(data.decisionResult.details, function(i, details){
          if(i < data.parentNode.level){        
            if(i == data.parentNode.level - 1){
              $('#breadcrumbDiv').append(document.createTextNode(details.name));
              
            }else{
              $('<a/>').attr("href", "javascript:getPreviousSelectedNode("+details.valueId+","+details.level+");").appendTo("#breadcrumbDiv").text(details.name);
              $('#breadcrumbDiv').append(document.createTextNode(" > "));
              
            }
          }
          
        });
      }
      displayEntityDetails(data);          
    }
    displayChildNodes(data);
    
    if(data.parentNode != null){
      if(!data.parentNode.allowComments){
        v_allowComments = false;
      }else{
        v_allowComments = true;
      }
      
      $("#dr_previousValueId").val(data.parentNode.parentValueId);
      if(data.parentNode.level != undefined){
        $("#dr_previousLevel").val(data.parentNode.level - 1);
        $('#stepLbl').text("Step "+ data.parentNode.level.toString()+" :");
      }else{
        $('#stepLbl').text("0");
      }
      if(data.parentNode.scriptText != undefined)
        $('#questLbl').text(" "+data.parentNode.scriptText);
      else
        $('#questLbl').text("");
        
      (data.parentNode.allowComments) ? $('#commentsDiv').show() : $('#commentsDiv').hide();  
    }
    
          
    (data.next && data.parentNode.allowComments) ? $('#nxtBtnDiv').show() : $('#nxtBtnDiv').hide();
    
    (data.back) ? $('#preBtnDiv').show() : $('#preBtnDiv').hide();
    
    //(data.submit) ? $('#subBtnDiv').show() : $('#subBtnDiv').hide();
    if(data.submit){

      $('#subBtnDiv').show();
      v_submitFlag = true;
    }else{
      $('#subBtnDiv').hide();
      v_submitFlag = false;



    }
    
    (data.decline) ? $('#decBtnDiv').show() : $('#decBtnDiv').hide();
    //submit-next button display
    $('#childNodeDiv input:radio:checked').each(function(){
        if($("#childNodeDiv input:radio:checked").val() != undefined){
          var selChild = $("#childNodeDiv input:radio:checked").val();
          $.each(v_childNodes, function(i, ansNode){
            if(ansNode.valueId ==  selChild){
              if(ansNode.hasChildNodes){
                $('#subBtnDiv').hide();
                $('#nxtBtnDiv').show();
              }else{
                $('#subBtnDiv').show();
                $('#nxtBtnDiv').hide();
              }
              //alert("selected has children : "+ v_selectedHasChild);
            }
          }); 
        }
    });
     widgetErrorFlag = "N";
     showDeclineOnOptional();
  //if json returns null go to else
  }else{
    //$("#dispDiv").unblock();
    $("#processDiv").hide();
    displayWidgetContent();
    $('#errorDiv').append(document.createElement('br'));
    $('#errorDiv').append(document.createTextNode("An error occurred for the following selected order number(s): ")); 
    
    if(v_selectedExtOrdNumbers != null && v_selectedExtOrdNumbers != undefined && v_selectedExtOrdNumbers != "")
    {
      $('#errorDiv').append(document.createTextNode(v_selectedExtOrdNumbers));
    }
    $('#errorDiv').append(document.createElement('br'));
    $('#errorDiv').append(document.createTextNode("To restart your disposition, refresh the current page or navigate to a new page."));
    $('#errorDiv').append(document.createElement('br'));
    resetDataFields();
    widgetErrorFlag = "Y";
    $('#breadcrumbDiv').empty();
    $('#widgetContent').hide();
    $("#errorDiv").show();
  }
  
  $("#dispDiv").css('overflow', 'auto');
  
}

function getPreviousSelectedNode(valueId, previousLevel){
  $("#dr_previousValueId").val(valueId);
  $("#dr_previousLevel").val(previousLevel);
  var url = "/decisionresult/decisionresult.do?method=load";
  var params = new Array();
  addParam(params, "dr_actionType", "back");
  addSecurityParams(params);
    
  fillParamsArray(params);
  url = url+buildQueryString(params);
  SubmitAjax(url, null, processCallback, handleError);  
}

function handleError(){
  //$("#dispDiv").unblock();
  $("#processDiv").hide();
  displayWidgetContent();
  $('#errorDiv').append(document.createElement('br'));
  $('#errorDiv').append(document.createTextNode("An error occurred for the following selected order number(s): ")); 
  
  if(v_selectedExtOrdNumbers != null && v_selectedExtOrdNumbers != undefined && v_selectedExtOrdNumbers != "")
  {
    $('#errorDiv').append(document.createTextNode(v_selectedExtOrdNumbers));
  }
  $('#errorDiv').append(document.createElement('br'));
  $('#errorDiv').append(document.createTextNode("To restart your disposition, refresh the current page or navigate to a new page."));
  
  $('#errorDiv').append(document.createElement('br'));
  resetDataFields();
  widgetErrorFlag = "Y";
  $('#breadcrumbDiv').empty();
  $('#widgetContent').hide();
  $("#errorDiv").show();
}



function imposeMaxLength(Event, Object, maxLen)
{
  return (Object.value.length < maxLen)||(Event.keyCode == 8 || Event.keyCode==46)
}

function checkMaxLength(object, maxLen){
  var str = object.value+window.clipboardData.getData("Text");
  if(str.length <= maxLen){
    return true;
  }else{
    event.returnValue = false;
    object.value = str.substring(0,maxLen);
    return;
  }
}


$(function(){
  $('#childNodeDiv').click(function(){
    
   
    //change background color to white for those not checked
    $('#childNodeDiv input:radio:not(:checked)').each(function(){
        $("#tr"+$(this).attr('value')).css("background-color","white");
    });
    if($("#childNodeDiv input:radio:checked").val() != undefined){
      $("#tr"+$("#childNodeDiv input:radio:checked").val()).css("background-color","#D1BC63");
      $('#dr_currentLevelSelectedValueId').val($("#childNodeDiv input:radio:checked").val());
            
    }
    if(v_childNodes != null  && v_childNodes.length > 0){
      $.each(v_childNodes, function(i, ansNode){
        if(ansNode.valueId ==  $('#dr_currentLevelSelectedValueId').val()){
          if(ansNode.hasChildNodes)
            v_selectedHasChild = "T";
          else
            v_selectedHasChild = "F";
          //alert("selected has children : "+ v_selectedHasChild);
        }
      });
    }
    //alert("v_allowComments : "+v_allowComments);
    if($("#childNodeDiv input:radio:checked").val() != undefined){
      if(!v_allowComments && !v_submitFlag && v_selectedHasChild == "T"){
        $("#next").trigger('click');
        return;
      }
    }
    //show next button if submit is true and user change selected attached value
//    if(v_submitFlag){
//      $('#subBtnDiv').hide();
//      $('#decBtnDiv').hide();  
//      $('#nxtBtnDiv').show();
//    }
      if($("#childNodeDiv input:radio:checked").val() != undefined){
        if(v_origSelectedEntities == $("#dr_selectedEntities").val() && v_selectedHasChild == "T"){
          $('#nxtBtnDiv').show();
          $('#subBtnDiv').hide();
        }else if(v_origSelectedEntities == $("#dr_selectedEntities").val() && v_selectedHasChild == "F"){
          $('#nxtBtnDiv').hide();
          $('#subBtnDiv').show();
        }else if(v_origSelectedEntities != $("#dr_selectedEntities").val()){
          $('#nxtBtnDiv').show();
          $('#subBtnDiv').hide();
        }
      }
    
  });
});

$(function(){
  $('#entityDiv').click(function(){
    var selEntities = "";
    $('#entityDiv input:checkbox:checked').each(function(){
      if( $('#entityDiv input:checkbox:checked').val() != undefined){
         selEntities = selEntities + $(this).attr('value')+"," ;
         $("#tr"+$(this).attr('value')).css("background-color","#D1BC63");
      }
    });
    if(selEntities != "")
      $("#dr_selectedEntities").val(selEntities.substring(0,selEntities.length-1));
    else
      $("#dr_selectedEntities").val("");
    //change background color to white for those not checked
    $('#entityDiv input:checkbox:not(:checked)').each(function(){
        $("#tr"+$(this).attr('value')).css("background-color","white");
    });
    //show next button if submit is true and user change selected orders.
    //var v_origSelectedEntities = "";
    //var v_selectedHasChild = false;
    //alert("original : "+v_origSelectedEntities);
    //alert("altred : "+$("#dr_selectedEntities").val());
   if(v_origSelectedEntities != $("#dr_selectedEntities").val()){      
      $('#nxtBtnDiv').show();
      $('#subBtnDiv').hide();
          
    }else{
        if(v_allowComments && v_selectedHasChild == "T"){
          $('#nxtBtnDiv').show();
          $('#subBtnDiv').hide();
        }else if(v_allowComments && v_selectedHasChild == "F"){
          $('#nxtBtnDiv').hide();
          $('#subBtnDiv').show();
        }else if(!v_allowComments && v_selectedHasChild == "F"){
          $('#nxtBtnDiv').hide();
          $('#subBtnDiv').show();
        }else if(!v_allowComments && v_selectedHasChild == "T"){
          $('#nxtBtnDiv').hide();
          $('#subBtnDiv').hide();
        }
      
//      else if(v_origSelectedEntities != $("#dr_selectedEntities").val()){
//        $('#nxtBtnDiv').show();
//        $('#subBtnDiv').hide();
//      }
    }
    //commented because json response will never will submit true becuase its front end task
    //to show whether its next or submit based on haschildflag
//    if(v_submitFlag){
//      $('#subBtnDiv').hide();
//      $('#decBtnDiv').hide();  
//      $('#nxtBtnDiv').show();
//    }
  });
});




$(function(){
  $('#next').click(function(){
    var url = "/decisionresult/decisionresult.do?method=load";
    var params = new Array();
    addParam(params, "dr_actionType", "next");
    addSecurityParams(params);
    
    fillParamsArray(params);
    
    if($("#dr_currentLevelSelectedValueId").val() == null || 
    $("#dr_currentLevelSelectedValueId").val() == undefined ||
    $("#dr_currentLevelSelectedValueId").val() == ""){
      $('#widgetMsgDiv').empty();
      $('#widgetMsgDiv').append(document.createElement('br'));
      if(widgetMessageFlag == 'O'){
        $('#widgetMsgDiv').append(document.createTextNode(optionalMessage)); 
        $('#widgetMsgDiv').append(document.createElement('br'));
      }else if(widgetMessageFlag == 'M'){
        $('#widgetMsgDiv').append(document.createTextNode(mandatoryMessage)); 
        $('#widgetMsgDiv').append(document.createElement('br'));
      }
      $('#widgetMsgDiv').append(document.createTextNode("Must select a value.")); 
      $("#widgetMsgDiv").show();
      
      return;
    }
    url = url+buildQueryString(params);
    //alert("next url : "+url);
    SubmitAjax(url, null, processCallback, handleError);

  });

});


$(function(){
  $('#previous').click(function(){
    var url = "/decisionresult/decisionresult.do?method=load";
    var params = new Array();
    addParam(params, "dr_actionType", "back");
    addSecurityParams(params);
    
    fillParamsArray(params);
    url = url+buildQueryString(params);
    SubmitAjax(url, null, processCallback, handleError);

  });

});

$(function(){
  $('#submitButton').click(function(){
    var url = "/decisionresult/decisionresult.do?method=load";
    var params = new Array();
    if($("#dr_selectedEntities").val() == null || 
      $("#dr_selectedEntities").val() == undefined || $("#dr_selectedEntities").val().length == 0){
      $('#widgetMsgDiv').empty();
      showDeclineOnOptional();
      $('#widgetMsgDiv').append(document.createElement('br'));
      $("#widgetMsgDiv").append(document.createTextNode("Must select an order number(s)."));
      $("#widgetMsgDiv").show();
      return;
    }
    addParam(params, "dr_actionType", "submit");
    addSecurityParams(params);
    
    fillParamsArray(params);
    url = url+buildQueryString(params);
    SubmitAjax(url, null, processCallback, handleError);

  });

});

$(function(){
  $('#decline').click(function(){
    var url = "/decisionresult/decisionresult.do?method=load";
    var params = new Array();
    addParam(params, "dr_actionType", "decline");
    addSecurityParams(params);
    
    fillParamsArray(params);
    url = url+buildQueryString(params);
    SubmitAjax(url, null, processDeclineCallback, handleError);

  });

});


function processSubmitCallback(data){
  //$("#widgetContent").unblock();
  $('#dr_availableEntities').val(data.availableEntities);
  resetDataFields();
  $('#breadcrumbDiv').empty();
  $('#widgetContent').hide();
  pageRefreshed = true;
  widgetMessageFlag = null;
  hideWidget("dispDiv");
}

function processDeclineCallback(data){
  //$("#widgetContent").unblock();
  $('#dr_availableEntities').val(data.availableEntities);
  resetDataFields();
  $('#breadcrumbDiv').empty();
  $('#widgetContent').hide();
  pageRefreshed = true;
  widgetMessageFlag = null;
  hideWidget("dispDiv");
}

function fillParamsArray(params){
  if($("#dr_decisionConfigId").val() != null && $("#dr_decisionConfigId").val() != undefined)
    addParam(params, "dr_decisionConfigId", $("#dr_decisionConfigId").val());
  else
    addParam(params, "dr_decisionConfigId", "");
  if($("#dr_decisionTypeCode").val() != null && $("#dr_decisionTypeCode").val() != undefined)
    addParam(params, "dr_decisionTypeCode", $("#dr_decisionTypeCode").val());
  if($("#dr_decisionStatus").val() != null && $("#dr_decisionStatus").val() != undefined)
    addParam(params, "dr_decisionStatus", $("#dr_decisionStatus").val());
  if($("#dr_dispositionStatus").val() != null && $("#dr_dispositionStatus").val() != undefined)
    addParam(params, "dr_dispositionStatus", $("#dr_dispositionStatus").val());
  if($("#dr_availableEntities").val() != null && $("#dr_availableEntities").val() != undefined)
    addParam(params, "dr_availableEntities", $("#dr_availableEntities").val());
  if($("#dr_selectedEntities").val() != null && $("#dr_selectedEntities").val() != undefined)
    addParam(params, "dr_selectedEntities", $("#dr_selectedEntities").val());
  if($("#dr_currentLevel").val() != null && $("#dr_currentLevel").val() != undefined)
    addParam(params, "dr_currentLevel", $("#dr_currentLevel").val());
  if($("#dr_currentLevelParentValueId").val() != null && $("#dr_currentLevelParentValueId").val() != undefined)
    addParam(params, "dr_currentLevelParentValueId", $("#dr_currentLevelParentValueId").val());
  if($("#dr_currentLevelSelectedValueId").val() != null && $("#dr_currentLevelSelectedValueId").val() != undefined)
    addParam(params, "dr_currentLevelSelectedValueId", $("#dr_currentLevelSelectedValueId").val());
  if($("#dr_currentLevelComment").val() != null && $("#dr_currentLevelComment").val() != undefined)
    addParam(params, "dr_currentLevelComment", $("#dr_currentLevelComment").val());
  if($("#dr_previousValueId").val() != null && $("#dr_previousValueId").val() != undefined)  
    addParam(params, "dr_previousValueId", $("#dr_previousValueId").val());
  if($("#dr_resultId").val() != null && $("#dr_resultId").val() != undefined)
    addParam(params, "dr_resultId", $("#dr_resultId").val());
  if($("#dr_previousLevel").val() != null && $("#dr_previousLevel").val() != undefined)
    addParam(params, "dr_previousLevel", $("#dr_previousLevel").val());
  if($("#dr_dnisId").val() != null && $("#dr_dnisId").val() != undefined)
    addParam(params, "dr_dnisId", $("#dr_dnisId").val());
    //we are hard coding dnis number for testing purpose
    //addParam(params, "dr_dnisId", "3500");
}




function assignHiddenFields(data){
  if(data.decisionResult != null){
    var decResult = data.decisionResult;
    $('#dr_decisionConfigId').val(decResult.decisionConfigId);
    $('#dr_currentLevel').val(decResult.currentLevel);
    $('#dr_resultId').val(decResult.resultId);
    $('#dr_availableEntities').val(data.availableEntities);
    
  }
  if(data.parentNode != null){
    var pNode = data.parentNode;
    $('#dr_currentLevelParentValueId').val(pNode.valueId);
    
  }
  
}


function resetDataFields(){
  $('#dr_currentLevel').val("");
  $('#dr_currentLevelParentValueId').val("");
  $('#dr_currentLevelComment').val("");
  $('#dr_currentLevelSelectedValueId').val("");
  $('#dr_decisionConfigId').val("");
  $('#dr_decisionStatus').val("");
  $('#dr_decisionTypeCode').val("");
  $('#dr_dispositionStatus').val("");
  $('#dr_openFlag').val("");
  $('#dr_previousValueId').val("");
  $('#dr_resultId').val("");
  $('#dr_previousLevel').val("");
  $('#dr_selectedEntities').val("");
  


}


function showDeclineOnOptional(){
    if(widgetMessageFlag == null){
      $("#widgetMsgDiv").html("")
      $("#widgetMsgDiv").hide();
      $('#decBtnDiv').hide();
      if(v_entityDetails != null && v_entityDetails.length > 0){
        $.each(v_entityDetails, function(i, entity){
        if(entity.required){
           $("#sp"+entity.entityId).css("color", "black");
        }
       });
      }
      
    }else if(widgetMessageFlag == 'O'){
      //Showing decline button when submit is true and disposition is optional
      $("#widgetMsgDiv").html(optionalMessage);
      $("#widgetMsgDiv").show();
      $('#decBtnDiv').show();
      //$("#widgetMsgDiv").attr({scrollTop: $("#widgetMsgDiv").attr("scrollHeight")});

      
    }else if(widgetMessageFlag == 'M'){
      $("#widgetMsgDiv").html(mandatoryMessage);
      $("#widgetMsgDiv").show();
      //$("#widgetMsgDiv").attr({ scrollTop: $("#widgetMsgDiv").attr("scrollHeight") });
      $('#decBtnDiv').hide();
    
    if(widgetMessageFlag != null){
      if(v_entityDetails != null && v_entityDetails.length > 0){
        $.each(v_entityDetails, function(i, entity){
        if(entity.required && entity.status.toUpperCase() == 'NONE'){
         if($('#'+entity.entityId).is(':checked')){
            $("#tr"+entity.entityId).removeClass();
            $("#tr"+entity.entityId).addClass('entityReqSel');
          }else{
            $("#tr"+entity.entityId).removeClass();
            $("#tr"+entity.entityId).addClass('entityReq');
          }
        }
       });
      }
    }
  }
}

function assignToHiddenField(){
  $('#dr_currentLevelComment').val($("#commentsTA").val());
}


function padSpaces(lname, maxSize){
  var retLName = lname.replace(/^\s+/,'').replace(/\s+$/,'');
  var spaceCnt = 0;
  //retLName = lname;
  if(lname != null && lname != "" && lname != undefined){
    var len = lname.length;
    var initVal = maxSize - len;
    
    for(var i=0; i < initVal; i++){
      retLName += "&#160;";  
      spaceCnt++  
    }
  
  }
  //alert("spaceCnt : "+spaceCnt);
  return retLName;
}


function handleDispNoFound(){
  //$("#dispDiv").unblock();
  $("#processDiv").hide();
  displayWidgetContent();
  $('#errorDiv').empty();
  $('#errorDiv').append(document.createElement('br'));
  $('#errorDiv').append(document.createTextNode("Call Disposition is not available.")); 
  $('#errorDiv').append(document.createElement('br'));
  resetDataFields();
  $('#dr_openFlag').val("Y");
  widgetErrorFlag = "Y";
  pageRefreshed = true;
  $('#breadcrumbDiv').empty();
  $('#widgetContent').hide();
  $("#errorDiv").show();  
}


function hideWidgetContent(){
  $("#breadcrumbTbl").hide();
  $("#errorDiv").hide();
  $("#widgetContent").hide();
}

function displayWidgetContent(){
  $("#breadcrumbTbl").show();
  $("#errorDiv").show();
  $("#widgetContent").show();
}

/**
 * Update the dr_availableEntities hidden field and set the manditory flag
 * @param orderDetailId - the orderId for which we want to update the call disposition
 * @param manditoryFlag - "true" or "false".  True means that call disp for this order is manditory
 */
function updateManditoryFlagForEntity(orderDetailId, manditoryFlag) {
	//take the dr_avilableEntities, split it by commas, and take the last one and set its "manditory" flag to true.
	//dr_availableEntities is a comma separated list that looks like this "order_detail_id:12345:false:none,order_detail_id:45678:false:none"
	// we need the change the third element for our order detail id to true
	if ($('#dr_availableEntities').val()!= "") {
		var availableEntities = $('#dr_availableEntities').val().split(",");
		var new_availableEntities = "";
		for (var i=0; i<availableEntities.length; i++) {
			var entityData = availableEntities[i].split(":");
			if (orderDetailId == entityData[1]) {
				//set the 3rd value to true
				entityData[2] = manditoryFlag;
			}
			//recombine the entities
			for (var j=0;j<entityData.length; j++) {
				
			}
			new_availableEntities += entityData[0] + ":" + entityData[1] + ":" + entityData[2] + ":" + entityData[3];
			if (i < availableEntities.length-1) {
				new_availableEntities += ",";
			}
		}
		$('#dr_availableEntities').val(new_availableEntities);
	}
}