<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
  <head>
     <title>Decision widget xsl test page</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>

    <script type="text/javascript" src="js/clock.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/util.js"/>
    <script language="javascript" type="text/javascript">

    </script>
  </head>
  <body id="body">
  <form name="theform" method="post">
    Test xsl page
  </form>
  </body>
</html>
</xsl:template>
</xsl:stylesheet>