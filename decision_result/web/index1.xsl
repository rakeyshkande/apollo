<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>


<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:template match="/">


<html>
  <head>
    <META http-equiv="Content-Type" content="text/html"/>
    <title>Call Disposition Index</title>
    <title>Widget Header Screen</title>     
    <link rel="stylesheet" type="text/css" href="css/decisionResult.css"/>
    <link rel="stylesheet" type="text/css" href="css/display.css"/>
    <link rel="stylesheet" type="text/css" href="css/pages.css"/>
    <!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/> -->
    <link rel="stylesheet" type="text/css" href="css/jquery-1.8.css"/>
    <script type="text/javascript" src="js/clock.js"></script>    
    <!--<script type="text/javascript" src="js/prototype.js"></script>-->
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/jquery-1.4.4.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="js/jquery_curvycorners_packed.js"></script> 
    <script type="text/javascript" src="js/decisionResult.js"></script>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script> -->
    <script type="text/javascript" src="js/jquery-1.8.js"></script>
    <script type="text/javascript" >
      function extAllowed(){
        var exitAllowed = dcsnExitAllowed();
        if (!exitAllowed) {
            loadCallDispMandatory();
           
        } else{
            loadCallDispOptional(); 
            

        
        }
        
      }
    </script>
   
  </head>
  <body>
  <!----><xsl:call-template name="decisionResultData"/>
  <!-- Testing purpose only 
  <input id="dr_dispositionStatus" type="hidden" value=""/>
  <input id="dr_currentLevel" type="hidden"/>
  <input id="dr_availableEntities" type="hidden"/>
  <input id="dr_decisionConfigId" type="hidden"/>
  <input id="dr_decisionTypeCode" type="hidden"/>
  <input id="dr_currentLevelParentValueId" type="hidden"/>
  <input id="dr_currentLevelSelectedValueId" type="hidden"/>
  <input id="dr_selectedEntities" type="hidden"/>
  <input id="dr_resultId" type="hidden"/>
  <input id="dr_openFlag" type="hidden" />
  <input id="dr_resetFlag" type="hidden" />
  <input id="dr_decisionStatus" type="hidden" />-->
  
  <input type="hidden" name="securitytoken"           id="securitytoken" value="testToken"/>
  <input type="hidden" name="context"                 id="context"       value="testContext"/>
  <input type="hidden" name="applicationcontext"      id="applicationcontext" value="testAppContext"/>
  
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr> 
        <td width="150px" align="left" style="float:left;vertical-align:middle;padding-top:.4em;"><img src="images/wwwftdcom_131x32.gif" alt="ftd.com" border="0" height="32"></img></td>
        <td align="center" class="pageHeader">
          Header Title
        </td>
        <td width="150px" align="right" style="vertical-align:middle;padding-top:.4em;" class="clock">
            <span id="time"> <script type="text/javascript">startClock();</script></span><br></br>
			<a onclick="loadCallDisp(); return false;" href="#">Call Disposition</a><br></br>
      <a onclick="extAllowed();" href="#">call Exit Allowed</a>
			<!--<a onclick="ShowHide('dispDiv'); return false;" href="#">Call Disposition</a><br>-->
        </td>
      </tr>			
      <tr>
        <td colspan="3"></td>
      </tr>
    
    </table>
  
  <xsl:variable name="source-html" select="document('includes/decisionResult.html')"></xsl:variable>  
  <xsl:copy-of select="$source-html/node()"/>


    
  </body>
  
</html>


</xsl:template>
</xsl:stylesheet>
