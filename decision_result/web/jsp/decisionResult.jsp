<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%
  String decisionConfigId = (String)request.getAttribute("dr_decisionConfigId");
  String decisionStatus = (String)request.getAttribute("dr_decisionStatus");
  String dispositionStatus = (String)request.getAttribute("dr_dispositionStatus");
  String decisionTypeCode = (String)request.getAttribute("dr_decisionTypeCode");

%>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <title>PREVIEW <%=decisionStatus%></title>
    <!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"></link> -->
    <link rel="stylesheet" type="text/css" href="css/jquery-1.8.css"/>
    <link rel="stylesheet" type="text/css" href="css/decisionResultPreview.css"></link>
    <link rel="stylesheet" type="text/css" href="css/display.css"></link>
    <link rel="stylesheet" type="text/css" href="css/pages.css"></link>
    <script type="text/javascript" src="js/clock.js"></script>    
    <!--<script type="text/javascript" src="js/prototype.js"></script>-->
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/jquery-1.4.4.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="js/jquery_curvycorners_packed.js"></script>
    <script type="text/javascript" src="js/jquery-1.8.js"></script>   
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script> -->
    <script type="text/javascript" src="js/decisionResult.js"></script>
    <script type="text/javascript">
      function assignParams(){
                
        document.getElementById('dr_decisionConfigId').value = "<%=decisionConfigId%>";
        document.getElementById('dr_decisionStatus').value = "<%=decisionStatus%>";
        document.getElementById('dr_dispositionStatus').value = "<%=dispositionStatus%>";
        document.getElementById('dr_decisionTypeCode').value = "<%=decisionTypeCode%>";
      }
    </script>
    
  </head>
  <body onload="assignParams()">
  <jsp:include page="/includes/security.jsp" flush="true"/>
  <input id="dr_dispositionStatus" type="hidden" value="view"/>
  <input id="dr_currentLevelComment" type="hidden"/>
  <input id="dr_availableEntities" type="hidden"/>
  <input id="dr_decisionConfigId" type="hidden"/>
  <input id="dr_decisionTypeCode" type="hidden"/>
  <input id="dr_currentLevelParentValueId" type="hidden"/>
  <input id="dr_currentLevelSelectedValueId" type="hidden"/>
  <input id="dr_selectedEntities" type="hidden"/>
  <input id="dr_resultId" type="hidden"/>
  <input id="dr_openFlag" type="hidden" />
  <input id="dr_resetFlag" type="hidden" />
  <input id="dr_decisionStatus" type="hidden" />
  <input type="hidden" id="dr_dnisId" />
  <input type="hidden" id="dr_previousValueId" />
  <input type="hidden" id="dr_previousLevel" />
  <table>
    <tr>
      <td align="right">
        <a href="#" onclick="javascript:viewCallDisposition()" >Call Disposition</a></br>
      </td>
    </tr>
    <tr align="center">
      <td>
         
          
          <%@include file="/includes/decisionResult.html" %>
        
          

        
      </td>
    </tr>
  
  
  
  </table> 
  </body>
  
</html>

