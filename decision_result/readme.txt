To run the decision result app locally, do the following steps
	- create a project, add the sources,
	- add the required libraries as per build file
	- Turn off the security filter in web.xml
	- GO to getUserId() in DecisionResultAction and DecisionResultBO, 
		uncomment the code to return userId as anonymous if it is null.

Thats it ..!