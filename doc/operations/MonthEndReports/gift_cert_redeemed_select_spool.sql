with count_gc as (select count(cgc.gc_coupon_number) count_gc_number,
     cgcr.request_number
     from clean.gc_coupons cgc,
     clean.gc_coupon_request cgcr
     where cgc.request_number=cgcr.request_number
     group by cgcr.request_number)
select 'request_number,gc_number,issue_date,coupon_type,gc_coupon_type,program_name,company,redemption_date,redemption_amt,issue_amt,paid_amt,count_gc'
from dual
union 
   select cgcr.request_number ||','||
          cgc.gc_coupon_number ||','||
          cgcr.issue_date ||','||
          nvl(decode(cgcr.gc_coupon_type,'GC','GC','CP','CP\CS','CS','CP\CS'),'UNK') ||','||
          nvl(cgcr.gc_coupon_type,'UNK') ||','||
          cgcr.program_name ||','||
          facm.company_name ||','||
          to_char(ch.redemption_date,'mm/dd/yyyy') ||','||
          ch.redemption_amount ||','||
          cgcr.issue_amount ||','||
          cgcr.paid_amount ||','|| 1
   from clean.gc_coupons cgc
   join clean.gc_coupon_request cgcr
   on (cgc.request_number=cgcr.request_number)
   join count_gc  count_gc
   on (cgcr.request_number= count_gc.request_number)
   join clean.gc_coupon_history ch
   on ( cgc.gc_coupon_number=ch.gc_coupon_number)
   left outer join ftd_apps.company_master facm
   on (cgcr.company_id=facm.company_id)
   where ch.status = 'Active'
   and ch.redemption_date is not null 
   and trunc(ch.redemption_date) between trunc(to_date('02/01/2013', 'mm/dd/yyyy')) and trunc(to_date('02/28/2013', 'mm/dd/yyyy'));
-- order by cgcr.gc_coupon_type desc, cgcr.request_number, cgc.gc_coupon_number;
