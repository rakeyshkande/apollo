select 'sort1,sort2,change_category,change_type,source_code,description,source_type,company_id,start_date,end_date,updated_by,requested_by,price_header_id,price_code_description,snh_id,service_fee_code_description,pst_code,promotion_code,updated_on,redemption_rate_description'
from dual
union
	SELECT	decode(FASM.operation$, 'DEL', '3', 'INS', '2', '1') ||','||
		decode(FASM.operation$, 'DEL', '4', 'INS', '3', 'UPD_OLD', '2', 'UPD_NEW', '1', '0')||','||
	        decode(FASM.operation$, 'DEL', 'Expired', 'INS', 'New', 'UPD_OLD', 'Updated', 'UPD_NEW', 'Updated', 'Updated') ||','||
	        decode(FASM.operation$, 'DEL', 'Expired', 'INS', 'New', 'UPD_OLD', 'Old', 'UPD_NEW', 'Updated', 'Change?') ||','||
		FASM.source_code	  ||','||
		'"'||FASM.description	  ||'",'||
		FASM.source_type	  ||','||
		FASM.company_id		  ||','||
		FASM.start_date		  ||','||
		FASM.end_date		  ||','||
		FASM.updated_by		  ||','||
		concat(substr(au.first_name,1,1),au.last_name) ||','||
		FASM.price_header_id	  ||','||
		faph.description	  ||','||
		FASM.snh_id		  ||','||
		fasnh.description	  ||','||
		FASM.promotion_code	  ||','||
		FASM.bonus_promotion_code ||','||
		to_char(FASM.timestamp$, 'mm/dd/yyyy hh:mi:ss') ||','||
                mprr.description          
	FROM 	ftd_apps.source_master$ FASM
	JOIN	ftd_apps.price_header faph
		   on (faph.price_header_id = FASM.price_header_id)
	JOIN	ftd_apps.snh fasnh
		   on (fasnh.snh_id = FASM.snh_id)
	LEFT
	OUTER
	JOIN	aas.users au
		   on (to_char(au.user_id) = FASM.requested_by)
	LEFT
	OUTER
	JOIN	ftd_apps.miles_points_redemption_rate mprr
		   on (mprr.mp_redemption_rate_id = FASM.mp_redemption_rate_id)
	WHERE	trunc(FASM.timestamp$) between to_date('01/01/2013','mm/dd/yyyy') and to_date('01/31/2013','mm/dd/yyyy')
;