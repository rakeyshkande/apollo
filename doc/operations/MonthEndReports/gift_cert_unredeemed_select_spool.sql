--select 'request_number,issue_date,certificate_number,program_name,company,source_code,issue_amt,paid_amt,total_liability,coupon_count'
select 'request_number,issue_date,certificate_number,program_name,company,issue_amt,paid_amt,total_liability,coupon_count'
from dual
union 
  select cgcr.request_number||','||
         cgcr.issue_date||','||
         cgc.gc_coupon_number||','||
         cgcr.program_name||','||
         facm.company_name||','||
--         cgcr.source_code||','||
         cgcr.issue_amount||','||
         cgcr.paid_amount||','||
         cgcr.paid_amount * cgcr.quantity||','||
         cgcr.quantity
  from   clean.gc_coupons cgc, clean.gc_coupon_request cgcr, ftd_apps.company_master facm
  where  cgc.gc_coupon_status in ('Issued Active','Reinstate')
  and    cgcr.request_number = cgc.request_number
  and    cgcr.expiration_date is null
  and    cgcr.gc_coupon_type='GC'
  and    cgcr.issue_date between to_date('02/01/2013','mm/dd/yyyy')
                               and to_date('02/28/2013','mm/dd/yyyy')
  and    cgcr.company_id=facm.company_id(+)
  ;
  --order by cgcr.issue_date, cgcr.request_number, cgc.gc_coupon_number;

