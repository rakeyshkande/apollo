--select 'request_number,gc_number,issue_date,program_name,company,source_code,issue_amt,paid_amt,count_gc_number'
select 'request_number,gc_number,issue_date,program_name,company,issue_amt,paid_amt,count_gc_number'
from dual
union 
select cgcr.request_number || ',' ||
             cgc.gc_coupon_number || ',' ||
             to_char(cgcr.issue_date,'mm/dd/yyyy') || ',' ||
           cgcr.program_name || ',' ||
           facm.company_name || ',' ||
           --cgcr.source_code || ',' ||
           cgcr.issue_amount || ',' ||
           cgcr.paid_amount || ',' ||'1'
from clean.gc_coupons cgc
join clean.gc_coupon_request cgcr
on (cgc.request_number=cgcr.request_number)
join (select count(cgc.gc_coupon_number) count_gc_number,
                        cgcr.request_number
            from clean.gc_coupons cgc,
                    clean.gc_coupon_request cgcr
            where cgc.request_number=cgcr.request_number
            group by cgcr.request_number) count_gc
on (cgcr.request_number=count_gc.request_number)
join ftd_apps.company_master facm
on (cgcr.company_id=facm.company_id)
WHERE trunc(cgc.updated_on) between to_date('02/01/2013','mm/dd/yyyy')
                               and to_date('02/28/2013','mm/dd/yyyy')
and cgc.gc_coupon_status='Cancelled'
and cgcr.gc_coupon_type='GC';
--order by cgcr.request_number;

