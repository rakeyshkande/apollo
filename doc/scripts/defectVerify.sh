#!/bin/sh
if head -1 < $1 | grep '^#[0-9]*[ ]*$' > /dev/null; then
  exit 0
else
  echo 
  echo "****************************************************************************************************************"
  echo "ERROR!!! - THE FTD CVS POLICE SAY:"
  echo "The first line of a comment must only contain '#' followed by the defect number (use #0 if no associated defect)" 
  echo "For example:"
  echo "#5555"
  echo "Fixed some stuff..."
  echo "****************************************************************************************************************"
  exit 1
fi
