#!/usr/bin/perl
#
# cvsCommitLogParser.pl
#
# Script to parse the cvs commit log and create html pages detailing and
# summarizing the commit activity per several defect/developer/module combinations
#

# Constants
#
$LAST_BUILD = "=====LAST_BUILD_3402=====";
$SUMMARY_HEADER = "DEV 3.4.0.3 Release Notes";
$OUT_SUMMARY    = "summary3403.html";
$OUT_DB_SUMMARY = "summaryDb.340.html";
$OUT_DEVEL      = "dev340";
$CVS_BRANCH     = "FIX_3_4_0";
$OUT_DEFECTS    = "index.340.html";

# Set to 1 if only want dump of who modified each file.
# This is used to help sort files for PCI code reviews.
# Also need to set LAST_BUILD appropriately (e.g. =====START_3200=====).
#
$PRINT_FILES_AND_USERS = 0;

# Set to 1 if only want dump of files added on head.
# This is to help find new files for a branch that were actually 
# added to head first and then branched (since added to head first 
# they wouldn't show up in the list of changes for a branch).
#
$PRINT_ADDED_ON_HEAD = 0;

$DATETIME = `date`;

$ROW_COLOR1   = "#ffffff";
$FILE_COLOR1A = "#ffffff";
$FILE_COLOR1B = "#eeeef2";

$ROW_COLOR2   = "#e4e4d0";
$FILE_COLOR2A = "#e4e4d0";
$FILE_COLOR2B = "#d4d4c0";

$ROW_COLOR3 = "#f4f4e0";
$ROW_COLOR4 = "#ffffff";
#$ROW_COLOR4 = "#f0f0f4";

# Hashes to store parsed data
#
%defects = '';
%summaryModules = '';
%summaryDbOnly  = '';
%summaryDbOnlyDefects  = '';
%summaryDefects = '';
%allDevelopers = '';
%summaryFilesAndUsers = '';

$b_getDefectNum = 0;
$b_getFiles = 0;
$currDefectNum = "";
$currTag = "";
@currFiles = ("");
$newSinceLastBuild = 0;


# If only want list of files added to head
#
if ($PRINT_ADDED_ON_HEAD == 1) {
  dumpFilesAddedToHeadOnly();
  exit;
}


#--------------------------------------------------------------------
#
# Parse cvs commit log and save data to hashes
#
while(<>) {
  
    # "Log Message:" line is end of file list.  
    # Also, line after this will be the defect num.
    #
    if (m#^Log Message:#) {
        $b_getDefectNum = 1;
        $b_getFiles = 0;

    # Once we have defect num we can save all data to hashes.
    # Note there may be multiple defects fixed on single commit.  Assumption is
    # users will enter one defect number per line in comments.  Below should
    # handle this since we don't reset b_getDefectNum until blank line
    #
    } elsif ($b_getDefectNum == 1) {
        ($currDefectNum) = m/\#(\d+)/;
        
        if ($currDefectNum eq "") {
            # If here then we already got defect number and saved to hashes (or directory was added)
            # so skip and reset to get ready for next defect.
            $b_getDefectNum = 0;
            @currFiles = ("");
            $currTag = "";
            next; 
        }	  
        if (($currTag eq "") || ($currTag ne $CVS_BRANCH)) {
            # If currTag empty then must have been added to head (since there was no "Tag:" line)
            # If currTag not CVS_BRANCH then must be another branch 
            # so skip and reset to get ready for next defect.
            $b_getDefectNum = 0;
            @currFiles = ("");
            $currTag = "";
            next; 
        }	  
        $defects{$currDefectNum}{UPDATED} = $newSinceLastBuild;
        $defects{$currDefectNum}{NAMES}{$name}{UPDATED} = $newSinceLastBuild;
        $defects{$currDefectNum}{MODULES}{$module}{UPDATED} = $newSinceLastBuild;
        foreach $afile (@currFiles) {
            $allDevelopers{$name}{$currDefectNum}{$module}{$afile} = $newSinceLastBuild;
            $defects{$currDefectNum}{MODULES}{$module}{FILE}{$afile}{UPDATED} = $newSinceLastBuild;
            $defects{$currDefectNum}{MODULES}{$module}{FILE}{$afile}{$name} = $newSinceLastBuild;
            $defects{$currDefectNum}{NAMES}{$name}{MODULES}{$module}{UPDATED} = $newSinceLastBuild;
            $defects{$currDefectNum}{NAMES}{$name}{MODULES}{$module}{FILE}{$afile} = $newSinceLastBuild;
            if (($newSinceLastBuild == 1) && ($currDefectNum ne "0") && ($currDefectNum ne "0000")) {
              $summaryModules{$module}{$afile}{$currDefectNum}{$name} = 1;
              $summaryDefects{$currDefectNum}{$name} = 1;
            }
            if ($module =~ m#/database#) {
              $summaryDbOnly{$module}{$afile}{$currDefectNum}{$name} = 1;
              $summaryDbOnlyDefects{$currDefectNum}{$name} = 1;
            }
            # These are only for list of who modified each file
            if ($PRINT_FILES_AND_USERS == 1) {
              if ($afile ne "") {
                $moduleFile = $dir . "/" . $afile;
                $summaryFilesAndUsers{$moduleFile}{$name}{$currDefectNum} = 1;
              }
            }
        }

    # Parse filenames for this commit
    #
    } elsif ($b_getFiles == 1) {
        # There may be an additional "Added Files:" or "Modified Files:" line after the first "Tag:"
        # so ignore those lines
        if (m#Added Files:# || m#Modified Files:# || m#Tag:# || m#Removed Files:#) {
            next;
        }
        push(@currFiles, split());
  
    # Start of a new entry is a smiley
    #
    } elsif (m#^:-\)#) {
        $b_getDefectNum = 0;
        $b_getFiles = 0;
        $currDefectNum = "";
        $currTag = "";
        @currFiles = ("");
        ($name, $date, $dir) = m#:-\)\s+(\w+)\s+(\w+\s+\w+\s+\d+\s+\d+:\d+:\d+\s+\w+\s+\d+)\s+(\S+)#;
        if ($dir =~ m#FTD/database#) {
            ($module) = ($dir =~ m#FTD(/database[/]*[^/]*)#);
        } elsif ($dir =~ m#FTD#) {
            ($module) = ($dir =~ m#FTD/([^/]+)#);
            if ($module eq "") {
                $module = "FTD";  # Top level
            }
        } else {
            next;  # Not an FTD module
        }

    # Lines after "Tag:" will be files checked in
    #
    } elsif (m#^\s*Tag:\s+(\w+)#) {
        $currTag = $1;
        $b_getFiles = 1;
    
    # Look for line signifying last QA build 
    #
    } elsif (m#^$LAST_BUILD#) {
        $newSinceLastBuild = 1;
    }
}


# If only want list of who modified each file
#
if ($PRINT_FILES_AND_USERS == 1) {
  dumpFilesAndUsers();
  exit;
}
		
#--------------------------------------------------------------------
#
# Constants used for printing results
#
$HTML_TOP = "
<html>
<head>
<link rel='stylesheet' type='text/css' href='cvsCommitLog.css'/>
<script type='text/javascript' src='menus.js'></script>
</head>
<body bgcolor=\"#ffffff\">
<center>
<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr bgcolor=\"#777777\">
<td valign=\"top\" class=\"Big1White\">%s</td>
<td></td>
</tr><tr>
<td colspan=\"2\" class=\"med1Grey\"><br/><span class=\"big1Grey\">CVS commits sorted by defect/module - mouseover for details</span>
     <br/>Note <span class=\"colorRed\">Red</span> reflects changes that have not been deployed to QA yet<br/><br/>
     Last updated: %s<br/><br/>
     Summary for each developer: %s<br/>
     Summary for <a href=\"%s\">Database module only</a><br/><br/>
</td>
</tr><tr>
<td></td>
<td> 
<ul id=\"nav\">
";

$HTML_BOTTOM = "
</ul>
</td>
</tr>
</table>
</center>
</body>
</html>
";

$SUMMARY_TOP = "
<html>
<head>
<link rel='stylesheet' type='text/css' href='cvsCommitLog.css'/>
</head>
<body bgcolor=\"#ffffff\">
<div id=\"main\">
<center>
<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr bgcolor=\"#777777\">
<td valign=\"top\" align=\"left\" class=\"Big1White\">%s</td>
</tr>
</table>
<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr class=\"summaryHeader\" bgcolor=\"#c4c4a5\"><td>Defect</td><td>Developers</td></tr>
";

$SUMMARY_MIDDLE = "
</table>
<br/>
<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr class=\"summaryHeader\" bgcolor=\"#c4c4a5\"><td>Module</td><td>File</td><td>Defect</td><td>Developers</td></tr>
";

$SUMMARY_BOTTOM = "
</table>
<br/><span class=\"tiny1Light\">Last Update: %s</span>
</center>
</div>
</body>
</html>
";

$DEVEL_TOP = "
<html>
<head>
<link rel='stylesheet' type='text/css' href='cvsCommitLog.css'/>
<script type='text/javascript' src='menus.js'></script>
</head>
<body bgcolor=\"#ffffff\">
<center>
<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr bgcolor=\"#777777\">
<td colspan=\"2\" valign=\"top\" class=\"Big1White\">%s - %s</td>
</tr><tr>
<td colspan=\"2\" class=\"med1Grey\"><br/><span class=\"big1Grey\">CVS commits for developer - flat list</span>
     <br/>Note <span class=\"colorRed\">Red</span> reflects changes that have not been deployed to QA yet<br/><br/>
     Last updated: %s<br/>
     </td>
</tr><tr>
<td></td>
<td> 
<ul id=\"developers\">
";

$DEVEL_BOTTOM = "
</ul>
</td>
</tr>
</table>
</center>
</body>
</html>
";

		
#--------------------------------------------------------------------
#
# Print result detail html page
#
$develLinks = "";
foreach $devel (sort keys %allDevelopers) {
  $outdevel = $OUT_DEVEL . $devel . ".html";
  if ($devel ne "") {
    $devel =~ s/cvs//;
    $develLinks .= "<a href=\"" . $outdevel . "\">" . $devel . "</a> ";
  }
}
open OUT_DEFECTS, "> $OUT_DEFECTS" or die "Can't open $OUT_DEFECTS";
printf OUT_DEFECTS "$HTML_TOP", $CVS_BRANCH, $DATETIME, $develLinks, $OUT_DB_SUMMARY;
foreach $dnum (sort keys %defects) {
    if (($dnum eq "") || ($dnum eq "0000") || ($dnum eq "0")) {
        next;
    }
    if ($defects{$dnum}{UPDATED} == 1) {
        printf OUT_DEFECTS "<li class=\"big1Red\">Defect %s:</li>\n", $dnum;
    } else {
        printf OUT_DEFECTS "<li class=\"big1Grey\">Defect %s:</li>\n", $dnum;
    }
    print OUT_DEFECTS "<li class=\"med2Grey\">Modules</li>";
    
    # Dump modules
    #
    foreach $cmodule (sort keys %{$defects{$dnum}{MODULES}}) {
        if ($defects{$dnum}{MODULES}{$cmodule}{UPDATED} == 1) {
            printf OUT_DEFECTS "  <li><a href=\"#\"><span class=\"colorRed\">%s</span></a>\n<ul>", $cmodule;
        } else {
            printf OUT_DEFECTS "  <li><a href=\"#\">%s</a>\n<ul>", $cmodule;
        }
        foreach $cfile (sort keys %{$defects{$dnum}{MODULES}{$cmodule}{FILE}}) {
            if ($cfile ne "") {
                if ($defects{$dnum}{MODULES}{$cmodule}{FILE}{$cfile}{UPDATED} == 1) {
                    printf OUT_DEFECTS "<li><a href=\"#\" class=\"code\"><span class=\"colorRed\">%s</span></a>\n<ul>", $cfile;
                } else {
                    printf OUT_DEFECTS "<li><a href=\"#\" class=\"code\">%s</a>\n<ul>", $cfile;
                }
                foreach $cfname (sort keys %{$defects{$dnum}{MODULES}{$cmodule}{FILE}{$cfile}}) {
                    if ($cfname eq "UPDATED") {
                        next;
                    }
                    if ($defects{$dnum}{MODULES}{$cmodule}{FILE}{$cfile}{$cfname} == 1) {
                        printf OUT_DEFECTS "<li><span class=\"colorRed\">%s</span></li>", $cfname;
                    } else {
                        printf OUT_DEFECTS "<li>%s</li>", $cfname;
                    }
                }
                print OUT_DEFECTS "</ul>\n";
            }
        }
        print OUT_DEFECTS "</ul></li>\n";
    }
    
    # Dump developers
    #
    print OUT_DEFECTS "<li class=\"med2Grey\">Developers</li>";
    foreach $cname (sort keys %{$defects{$dnum}{NAMES}}) {
        if ($defects{$dnum}{NAMES}{$cname}{UPDATED} == 1) {
            printf OUT_DEFECTS "  <li><a href=\"#\"><span class=\"colorRed\">%s</span></a>\n<ul>\n", $cname;
        } else {
            printf OUT_DEFECTS "  <li><a href=\"#\">%s</a>\n<ul>\n", $cname;
        }
        foreach $cmodule (sort keys %{$defects{$dnum}{NAMES}{$cname}{MODULES}}) {
            if ($cmodule ne "") {
                if ($defects{$dnum}{NAMES}{$cname}{MODULES}{$cmodule}{UPDATED} == 1) {
                    printf OUT_DEFECTS "  <li><a href=\"#\"><span class=\"colorRed\">%s</span></a>\n<ul class=\"code\">\n", $cmodule;
                } else {
                    printf OUT_DEFECTS "  <li><a href=\"#\">%s</a>\n<ul class=\"code\">\n", $cmodule;
                }
                foreach $cfname (sort keys %{$defects{$dnum}{NAMES}{$cname}{MODULES}{$cmodule}{FILE}}) {
                    if ($cfname eq "") {
                        next;
                    }
                    if ($defects{$dnum}{NAMES}{$cname}{MODULES}{$cmodule}{FILE}{$cfname} == 1) {
                        printf OUT_DEFECTS "<li><span class=\"colorRed\">%s</span></li>\n", $cfname;
                    } else {
                        printf OUT_DEFECTS "<li>%s</li>\n", $cfname;
                    }
                }
                print OUT_DEFECTS "</ul>\n</li>";
            } else {
                next;
            }
        }
        print OUT_DEFECTS "</ul>\n";
    }
    
    print OUT_DEFECTS "<li></li>\n";  # Space between each defect
}
print OUT_DEFECTS "$HTML_BOTTOM";
close(OUT_DEFECTS);

		
#--------------------------------------------------------------------
#
# Print developer html pages of all their changes for this branch
#
foreach $devel (sort keys %allDevelopers) {
    $outdevel = $OUT_DEVEL . $devel . ".html";
    open OUT_DEVEL, "> $outdevel" or die "Can't open $outdevel";
    printf OUT_DEVEL "$DEVEL_TOP", $CVS_BRANCH, $devel, $DATETIME;

    foreach $dnum (sort keys %{$allDevelopers{$devel}}) {
        if (($dnum eq "") || ($dnum eq "0000") || ($dnum eq "0")) {
            next;
        }
        printf OUT_DEVEL "<li><br/><span class=\"big1Grey\">Defect %s</span>\n  <ul>\n", $dnum;
        foreach $cmodule (sort keys %{$allDevelopers{$devel}{$dnum}}) {
            printf OUT_DEVEL "  <li><span class=\"med1Black\">%s</span>\n    <ul>\n", $cmodule;
            foreach $cfile (sort keys %{$allDevelopers{$devel}{$dnum}{$cmodule}}) {
                if ($cfile ne "") {
                    if ($allDevelopers{$devel}{$dnum}{$cmodule}{$cfile} == 1) {
                        printf OUT_DEVEL "    <li class=\"code2Red\">%s</li>\n", $cfile;
                    } else {
                        printf OUT_DEVEL "    <li class=\"code2\">%s</li>\n", $cfile;
                    }
                }
            }
            print OUT_DEVEL "    </ul>\n  </li>\n";
        }
        print OUT_DEVEL "  </ul>\n</li>\n";
    }
    print OUT_DEVEL "$DEVEL_BOTTOM";
    close(OUT_DEVEL);
}

		
#--------------------------------------------------------------------
#
# Print summary html page of changes since last build
#
open OUT_SUMMARY, "> $OUT_SUMMARY" or die "Can't open $OUT_SUMMARY";
printf OUT_SUMMARY "$SUMMARY_TOP", $SUMMARY_HEADER;
$bgcolor = $ROW_COLOR1;

# Defect summary
#
foreach $defect (sort keys %summaryDefects) {
    $authList = "";
    foreach $auth (sort keys %{$summaryDefects{$defect}}) {
      if ($authList ne "") {
        $authList .= ", ";
      } 
      $authList .= $auth;
    }
    if ($bgcolor eq $ROW_COLOR1) {
      $bgcolor = $ROW_COLOR2;
    } else {
      $bgcolor = $ROW_COLOR1;
    }
    printf OUT_SUMMARY "<tr class=\"summary\" bgcolor=\"%s\"><td class=\"border1\">%s</td><td class=\"border1\">%s</td></tr>", $bgcolor, $defect, $authList;
}

print OUT_SUMMARY "$SUMMARY_MIDDLE";


# Module/file summary
#
$bgcolor = $ROW_COLOR1;
$filecolor = $FILE_COLOR1A;
foreach $mname (sort keys %summaryModules) {
    if ($mname ne "") {
        $pmodule = $mname;
        $pclass = " class=\"border1\"";
        $pclassMod = " class=\"border1\"";
        foreach $cfile (sort keys %{$summaryModules{$mname}}) {
            if ($cfile ne "") {
                $pfile = $cfile;
                $pclass = " class=\"border1\"";
                foreach $defect (sort keys %{$summaryModules{$mname}{$cfile}}) {
                    $authList = "";
                    foreach $auth (sort keys %{$summaryModules{$mname}{$cfile}{$defect}}) {
                      if ($authList ne "") {
                        $authList .= ", ";
                      } 
                      $authList .= $auth;
                    }
                    printf OUT_SUMMARY "<tr class=\"summary\" bgcolor=\"%s\"><td%s>%s</td>" . 
                                       "<td%s bgcolor=\"%s\">%s</td><td%s bgcolor=\"%s\">%s</td>" .
                                       "<td%s bgcolor=\"%s\">%s</td></tr>\n", 
                                       $bgcolor, $pclassMod, $pmodule, 
                                       $pclass, $filecolor, $pfile, $pclass, $filecolor, $defect, 
                                       $pclass, $filecolor, $authList;
                    $pmodule = "&nbsp;";
                    $pfile = "";
                    $pclass = " class=\"border0\"";
                    if ($bgcolor eq $ROW_COLOR1) {
                        $pclassMod = " class=\"border0a\"";
                    } else {
                        $pclassMod = " class=\"border0b\"";
                    }
                }
                if ($filecolor eq $FILE_COLOR2B) {
                  $filecolor = $FILE_COLOR2A;
                } elsif ($filecolor eq $FILE_COLOR2A) {
                  $filecolor = $FILE_COLOR2B;
                } elsif ($filecolor eq $FILE_COLOR1B) {
                  $filecolor = $FILE_COLOR1A;
                } else {
                  $filecolor = $FILE_COLOR1B;
                }
            }     
        }
        print OUT_SUMMARY "</tr>\n";
        if ($bgcolor eq $ROW_COLOR1) {
          $bgcolor = $ROW_COLOR2;
          $filecolor = $FILE_COLOR2A;
        } else {
          $bgcolor = $ROW_COLOR1;
          $filecolor = $FILE_COLOR1A;
        }
    }
}
printf OUT_SUMMARY "$SUMMARY_BOTTOM", $DATETIME;
close(OUT_SUMMARY);

		
#--------------------------------------------------------------------
#
# Print summary html page of only database changes
#
# !!! THIS IS LOGIC SIMILAR TO ABOVE SUMMARY - SO IT SHOULD BE MADE INTO A SUBROUTINE !!!
#
open OUT_SUMMARY, "> $OUT_DB_SUMMARY" or die "Can't open $OUT_DB_SUMMARY";
printf OUT_SUMMARY "$SUMMARY_TOP", $CVS_BRANCH . " - Database Module Changes Only";
$bgcolor = $ROW_COLOR1;

# Defect summary
#
foreach $defect (sort keys %summaryDbOnlyDefects) {
    $authList = "";
    foreach $auth (sort keys %{$summaryDbOnlyDefects{$defect}}) {
      if ($authList ne "") {
        $authList .= ", ";
      } 
      $authList .= $auth;
    }
    if ($bgcolor eq $ROW_COLOR1) {
      $bgcolor = $ROW_COLOR2;
    } else {
      $bgcolor = $ROW_COLOR1;
    }
    printf OUT_SUMMARY "<tr class=\"summary\" bgcolor=\"%s\"><td class=\"border1\">%s</td><td class=\"border1\">%s</td></tr>", $bgcolor, $defect, $authList;
}

print OUT_SUMMARY "$SUMMARY_MIDDLE";


# Module/file summary
#
$bgcolor = $ROW_COLOR1;
$filecolor = $FILE_COLOR1A;
foreach $mname (sort keys %summaryDbOnly) {
    if ($mname ne "") {
        $pmodule = $mname;
        $pclass = " class=\"border1\"";
        $pclassMod = " class=\"border1\"";
        foreach $cfile (sort keys %{$summaryDbOnly{$mname}}) {
            if ($cfile ne "") {
                $pfile = $cfile;
                $pclass = " class=\"border1\"";
                foreach $defect (sort keys %{$summaryDbOnly{$mname}{$cfile}}) {
                    $authList = "";
                    foreach $auth (sort keys %{$summaryDbOnly{$mname}{$cfile}{$defect}}) {
                      if ($authList ne "") {
                        $authList .= ", ";
                      } 
                      $authList .= $auth;
                    }
                    printf OUT_SUMMARY "<tr class=\"summary\" bgcolor=\"%s\"><td%s>%s</td>" . 
                                       "<td%s bgcolor=\"%s\">%s</td><td%s bgcolor=\"%s\">%s</td>" .
                                       "<td%s bgcolor=\"%s\">%s</td></tr>\n", 
                                       $bgcolor, $pclassMod, $pmodule, 
                                       $pclass, $filecolor, $pfile, $pclass, $filecolor, $defect, 
                                       $pclass, $filecolor, $authList;
                    $pmodule = "&nbsp;";
                    $pfile = "";
                    $pclass = " class=\"border0\"";
                    if ($bgcolor eq $ROW_COLOR1) {
                        $pclassMod = " class=\"border0a\"";
                    } else {
                        $pclassMod = " class=\"border0b\"";
                    }
                }
                if ($filecolor eq $FILE_COLOR2B) {
                  $filecolor = $FILE_COLOR2A;
                } elsif ($filecolor eq $FILE_COLOR2A) {
                  $filecolor = $FILE_COLOR2B;
                } elsif ($filecolor eq $FILE_COLOR1B) {
                  $filecolor = $FILE_COLOR1A;
                } else {
                  $filecolor = $FILE_COLOR1B;
                }
            }     
        }
        print OUT_SUMMARY "</tr>\n";
        if ($bgcolor eq $ROW_COLOR1) {
          $bgcolor = $ROW_COLOR2;
          $filecolor = $FILE_COLOR2A;
        } else {
          $bgcolor = $ROW_COLOR1;
          $filecolor = $FILE_COLOR1A;
        }
    }
}
printf OUT_SUMMARY "$SUMMARY_BOTTOM", $DATETIME;
close(OUT_SUMMARY);


exit;


# Subroutine to print list of files added to head.  
# This is to help find new files for a branch that were actually 
# added to head first and then branched.  Since added to head first 
# they won't show up in the list of changes for a branch.
#
sub dumpFilesAddedToHeadOnly {
    $lineBuffer = "";
    $dNum = "";
    $b_getDefectNum = 0;
    $b_addedFiles = 0;
    $b_tag = 0;
    while(<>) {
        $lineBuffer .= $_;
        if (m#:-\)#) {
          $lineBuffer = $_;
          $b_getDefectNum = 0;
          $b_addedFiles = 0;
          $b_tag = 0;
          $dNum = "";
        } elsif (($b_getDefectNum == 1) && ($b_addedFiles == 1) && ($b_tag == 0) && 
                 (($dNum ne "0000") && ($dNum ne "0")) && (m#^\s*$#)) {
          printf "%s\n", $lineBuffer;
          $lineBuffer = "";
          $b_getDefectNum = 0;
          $b_addedFiles = 0;
          $b_tag = 0;
          $dNum = "";
        } elsif (m#^Log Message:#) {
          $b_getDefectNum = 1;
        } elsif ($b_getDefectNum) {
          if (m/#(\d+)/) {
            $dNum = $1;
          }
        } elsif (m#^Added Files:#) {
          $b_addedFiles = 1;
        } elsif (m#^\s+Tag:#) {
          $b_tag = 1;
        }
    }
}

# Subroutine to dump list of who modified each file.  
# This is used to help decide reviewers for PCI code reviews.
#
sub dumpFilesAndUsers {
    foreach $mfile (sort keys %summaryFilesAndUsers) {
        foreach $aname (sort keys %{$summaryFilesAndUsers{$mfile}}) {
            $defectStr = "";
            foreach $defect (sort keys %{$summaryFilesAndUsers{$mfile}{$aname}}) {
              $defectStr .= $defect . " ";
            }
            printf "%s,%s,%s\n", $mfile, $aname, $defectStr;
        }
    }
}
