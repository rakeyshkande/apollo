#!/usr/bin/sh
#
# Creates directories needed for apollo applications
#
mkdir /u02/log4j_logs
mkdir /u02/archive
mkdir /u02/archive/floristdataload
mkdir /u02/solr
mkdir /u02/solr/facility
mkdir /u02/solr/facility/data
mkdir /u02/solr/facility/data/index
mkdir /u02/solr/product
mkdir /u02/solr/product/data
mkdir /u02/solr/product/data/index
mkdir /u02/solr/sourcecode
mkdir /u02/solr/sourcecode/data
mkdir /u02/solr/sourcecode/data/index
mkdir /u02/solr/iotw
mkdir /u02/solr/iotw/data
mkdir /u02/solr/iotw/data/index
mkdir /u02/config
mkdir /u02/config/cache
mkdir /u02/config/pgp

mkdir /u02/apollo/
mkdir /u02/apollo/accounting
mkdir /u02/apollo/accounting/eod
mkdir /u02/apollo/accounting/eod/archive
mkdir /u02/apollo/accounting/eod/archive/bml
mkdir /u02/apollo/accounting/eod/archive/ua
mkdir /u02/apollo/accounting/eod/archive/ua/inbound
mkdir /u02/apollo/accounting/eod/archive/ua/outbound
mkdir /u02/apollo/accounting/giftcert
mkdir /u02/apollo/accounting/giftcert/feed
mkdir /u02/apollo/accounting/giftcert/feed/archive
mkdir /u02/apollo/accounting/giftcert/feed/intuit
mkdir /u02/apollo/accounting/giftcert/feed/intuit/inbound
mkdir /u02/apollo/accounting/giftcert/feed/intuit/outbound
mkdir /u02/apollo/accounting/giftcert/feed/GCGENERIC
mkdir /u02/apollo/accounting/giftcert/feed/GCGENERIC/inbound
mkdir /u02/apollo/accounting/giftcert/feed/GCGENERIC/outbound
mkdir /u02/apollo/accounting/giftcert/electronic
mkdir /u02/apollo/accounting/recon
mkdir /u02/apollo/accounting/recon/archive
mkdir /u02/apollo/orderprocessing
mkdir /u02/apollo/orderprocessing/escalate
mkdir /u02/apollo/orderprocessing/escalate/archive
mkdir /u02/apollo/orderprocessing/recrof
mkdir /u02/apollo/orderprocessing/susres
mkdir /u02/apollo/orderprocessing/samswine
mkdir /u02/apollo/orderprocessing/samswine/inbound
mkdir /u02/apollo/orderprocessing/samswine/outbound
mkdir /u02/apollo/orderprocessing/samswine/archive
mkdir /u02/apollo/orderprocessing/winecom
mkdir /u02/apollo/orderprocessing/winecom/inbound
mkdir /u02/apollo/orderprocessing/winecom/outbound
mkdir /u02/apollo/orderprocessing/winecom/archive
mkdir /u02/apollo/orderprocessing/pc
mkdir /u02/apollo/orderprocessing/pc/inbound
mkdir /u02/apollo/pdb
mkdir /u02/apollo/pdb/sif
mkdir /u02/apollo/pdb/sif/archive

chmod -R 775 /u02/apollo
chmod -R 775 /u02/archive
