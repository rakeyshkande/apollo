#!/usr/bin/perl
# Check that status of the keyring
$keyring = `/bin/ssh-add -L`;
chomp($keyring);

if($keyring eq "The agent has no identities.") {
  print "You have not setup your keyring.\n";
  exit 1;
}

# Front End Nodes Array
my @nodes = qw{ gold platinum nickel titanium aluminum };
print "\nGathering application status on:\n";

foreach $node (@nodes) {
  print "$node\n";
  $getState=`ssh $node "/u01/app/oracle/product/10gR3/opmn/bin/opmnctl status -fmt %prt40%sta8"`;
  push(@status,$getState);
}

print "\nGenerating HTML Production Application Status\n\n";

# Open the output and start building the Production Application Status Page
open (OUTFILE, ">/var/apache2/htdocs/ftd/applist/T2000_index.php") || die ("Could not open file. $!");
print OUTFILE "<html><head><title>App List</title></head><body>";
print OUTFILE "<pre>";
print OUTFILE (@status);
print OUTFILE "</pre></body></html>";
close (OUTFILE);

#######################################################################################################################
#
# Find the application versions on all of the application servers
#
print "\nParsing application versions on ";

open (VERFILE, ">/tmp/appDeployStatus.out") || die ("Could not open file. $!");

foreach $node (@nodes) {
  print "\n$node";
  print VERFILE "NODENAME:$node\n";
  $version=`ssh $node "cd /u01/app/oracle/product/10gR3/j2ee; find . -name MANIFEST.MF -ls -exec grep Build-Release {} \\;"`;
  print VERFILE $version;
}

close (VERFILE);
