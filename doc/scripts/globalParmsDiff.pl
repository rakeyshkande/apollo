#!/usr/bin/perl

# Description:
#   Dumps html list of which frp.global_parms (or secure_config) parameters are 
#   missing/present between the Devel, QA, and production environments.
#   This script assumes the data for the two database columns (context, name) 
#   are present in the comma-delimited text files defined below.
#
#   Also can be used to get list of params that are in dev/qa, but not in
#   production, then check the release script to ensure they're defined.
#   Set DO_RELEASE_CHECK = 1 for this option.
#   Note this option is crude and not necessarily robust - use with caution.
#

$DO_GLOBAL_PARMS  = 1;  # Set to 1 to read global_parms files or 0 for secure_config files
$DO_RELEASE_CHECK = 0;  # Set to 1 to alternatively check if non-prod param names are in release script. 
$RELEASE_SCRIPT   = "Fix_2_4_Release_Script.sql";

$ONLY_DUMP_DIFFS = 1;  # Set to 1 if only want list of parameters that 
                       # don't exist in all three environments.
                       
$INCLUDE_PROD = 1;     # Set to 0 to only compare Dev and QA environments.
                       # 1 means all three environments.

%hGlobalParms  = '';

if ($DO_GLOBAL_PARMS == 1) {
  # CSV files from frp.global_parms
  $GLOBAL_DEV  = "dataGlobalDev.csv";
  $GLOBAL_QA   = "dataGlobalQa.csv";
  $GLOBAL_PROD = "dataGlobalProd.csv";
} else {
  # CSV files from frp.secure_config
  $GLOBAL_DEV  = "dataSecureDev.csv";
  $GLOBAL_QA   = "dataSecureQa.csv";
  $GLOBAL_PROD = "dataSecureProd.csv";
}


# Read Development data into hash
#
open FH_GLOBAL_DEV, "< $GLOBAL_DEV" or die "Can't open $GLOBAL_DEV";
while(<FH_GLOBAL_DEV>) {
   if (m/^([^,]+),(.+)/) {
     $hGlobalParms{$1}{$2}{"dev"} = 1;
   }
}
close FH_GLOBAL_DEV;

# Read QA data into hash
#
open FH_GLOBAL_QA, "< $GLOBAL_QA" or die "Can't open $GLOBAL_QA";
while(<FH_GLOBAL_QA>) {
   if (m/^([^,]+),(.+)/) {
     $hGlobalParms{$1}{$2}{"qa"} = 1;
   }
}
close FH_GLOBAL_QA;

# Read Production data into hash
#
if ($INCLUDE_PROD == 1) {
    open FH_GLOBAL_PROD, "< $GLOBAL_PROD" or die "Can't open $GLOBAL_PROD";
    while(<FH_GLOBAL_PROD>) {
       if (m/^([^,]+),(.+)/) {
         $hGlobalParms{$1}{$2}{"prod"} = 1;
       }
    }
    close FH_GLOBAL_PROD;
}

# Check if non-production params are defined in database release script
#
if ($DO_RELEASE_CHECK == 1) {
  &checkReleaseScript();
  exit;
}


# Create list of parameters, indicating which are present in each environment
#
print "<style type=\"text/css\">td {font-family: arial,helvetica,sans-serif; font-size: 10pt}</style>\n";
print "<table border=\"1\" align=\"left\" cellpadding=\"1\" cellspacing=\"0\">\n";
print "<tr bgcolor=\"#cccccc\"><td>Context</td><td>Name</td><td>Dev</td><td>QA</td><td>Production</td></tr>";
foreach $context ( sort keys %hGlobalParms ) {
  foreach $name ( sort keys %{ $hGlobalParms{$context} } ) {
    $envStr = "";
    $allEnv = 1;
    if ($hGlobalParms{$context}{$name}{"dev"}) {
      $envStr = "<td align=\"center\">X</td>";
    } else {
      $envStr = "<td>&nbsp;</td>";
      $allEnv = 0;
    }
    if ($hGlobalParms{$context}{$name}{"qa"}) {
      $envStr .= "<td align=\"center\">X</td>";
    } else {
      $envStr .= "<td>&nbsp;</td>";
      $allEnv = 0;
    }
    if ($INCLUDE_PROD == 1) {
        if ($hGlobalParms{$context}{$name}{"prod"}) {
          $envStr .= "<td align=\"center\">X</td>";
        } else {
          $envStr .= "<td>&nbsp;</td>";
          $allEnv = 0;
        }
    }
    if (($ONLY_DUMP_DIFFS == 0) || ($allEnv == 0)) {
      print "<tr><td>$context</td><td>$name</td>$envStr</tr>\n";
    }
  }
}
print "</table>";
exit;


#-------------------------------
#
# Subroutine to check if non-production params are defined in database release script
#
sub checkReleaseScript {

    # Read Release script into hash
    #
    open FH_RELEASE_SCRIPT, "< $RELEASE_SCRIPT" or die "Can't open $RELEASE_SCRIPT";
    $currInsert = "";
    while(<FH_RELEASE_SCRIPT>) {
       if ( ($DO_GLOBAL_PARMS == 1 && m/insert\s+into\s+frp\.global_parms/i) ||
            ($DO_GLOBAL_PARMS == 0 && m/insert\s+into\s+frp\.secure_config/i) || 
            ($currInsert ne "")) {
         $currInsert .= $_;
         if (m/;/) {
           #print "$currInsert\n";
           if ($currInsert =~ m/'([^']+)'[^']+'([^']+)'/) {
              $hGlobalParms{$1}{$2}{"release"} = 1;
           } else {
              print "Error - no match\n";
           }
           $currInsert = "";
         }
       }
    }
    close FH_RELEASE_SCRIPT;
    
    # Check if param names that are not in production are in release script
    #
    print "<style type=\"text/css\">td {font-family: arial,helvetica,sans-serif; font-size: 10pt}</style>\n";
    print "<table border=\"1\" align=\"left\" cellpadding=\"1\" cellspacing=\"0\">\n";
    print "<tr bgcolor=\"#cccccc\"><td>Context</td><td>Name</td><td>Dev</td><td>QA</td><td>Release script</td></tr>";
    foreach $context ( sort keys %hGlobalParms ) {
        foreach $name ( sort keys %{ $hGlobalParms{$context} } ) {
            if ($hGlobalParms{$context}{$name}{"prod"}) {
              next;
            }
            if ($hGlobalParms{$context}{$name}{"dev"}) {
              $envStr = "<td align=\"center\">X</td>";
            } else {
              $envStr = "<td>&nbsp;</td>";
            }
            if ($hGlobalParms{$context}{$name}{"qa"}) {
              $envStr .= "<td align=\"center\">X</td>";
            } else {
              $envStr .= "<td>&nbsp;</td>";
            }
            if ($hGlobalParms{$context}{$name}{"release"}) {
              $envStr .= "<td align=\"center\">X</td>";
            } else {
              $envStr .= "<td>&nbsp;</td>";
            }
            print "<tr><td>$context</td><td>$name</td>$envStr</tr>\n";
        }
    }
    print "</table>";
}