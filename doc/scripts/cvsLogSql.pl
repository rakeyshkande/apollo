#!/usr/bin/perl
#
# Script to create SQL directly from a CVS commit entry and directly insert into database.
# This script needs to be hooked to CVS via loginfo.
#
# NOTE: The following environment variables need to be set for this script to work:
#    export LD_LIBRARY_PATH=/opt/instantclient_11_2
#    export ORACLE_HOME=/tmp  
#
use DBI;

$OUTFILE    = "/repository/apollo/cvs/CVSROOT/cvscommit.sql";
$LOGFILE    = "/repository/apollo/cvs/CVSROOT/commitlog.txt";
$ERRFILE    = "/repository/apollo/cvs/CVSROOT/errorlog.txt";
$DB_CONNECT = "dbi:Oracle:host=10.222.114.22;sid=borgt1;port=1521";
$DB_LOGIN   = "cvs/pass";

$b_getDefectNum = 0;
$b_getFiles = 0;
$currDefectNum = "";
$currTag = "";
@currFiles = ("");

open OUTSQL, ">> $OUTFILE" or die "!!! NOTIFY BUILDMASTER IMMEDIATELY !!! CAN'T WRITE TO: $OUTFILE";
open OUTLOG, ">> $LOGFILE" or die "!!! NOTIFY BUILDMASTER IMMEDIATELY !!! CAN'T WRITE TO: $LOGFILE";

while(<>) {
  print OUTLOG "$_";

  # Entry starts with smiley
  #
  if (m#^:-\)#) {
     if (m#,\s+20#) {  # New date format
        ($cname, $day, $cdate, $cdir) = m#:-\)\s+(\w+)\s+(\w+),\s+(\w+\s+\d+,\s+\d+\s+\d+:\d+:\d+\s+\w+)\s+\w+\s+(\S+)#;
     } else {           # Old date format
        ($cname, $cdate, $cdir) = m#:-\)\s+(\w+)\s+(\w+\s+\w+\s+\d+\s+\d+:\d+:\d+\s+\w+\s+\d+)\s+(\S+)#;
     }

  # Lines after "Modified|Removed|Added Files:" will be files checked in
  #
  } elsif (m#^\w+ed\s+Files:\s*$#) {
     if ($b_getFiles == 0) {
        $currTag = "trunk";      # Default branch to trunk - "Tag:" line will follow if on branch
     }
     $b_getFiles = 1;

  # Lines after "Tag:" will be files checked in
  #
  } elsif (m#^\s*Tag:\s+(\w+)#) {
     $currTag = $1;
     $b_getFiles = 1;

  # "Log Message:" line is end of file list.
  # Also, line after this will be the defect num.
  #
  } elsif (m#^Log Message:#) {
      $b_getDefectNum = 1;
      $b_getFiles = 0;

  # Parse filenames for this commit
  #
  } elsif ($b_getFiles == 1) {
     # There may be an additional "Added Files:" or "Modified Files:" line after the first "Tag:"
     # so ignore those lines
     if (m#Added Files:# || m#Modified Files:# || m#Tag:# || m#Removed Files:#) {
         next;
     }
     push(@currFiles, split());

  # Once we have defect num we can save all data to hashes.
  # Note there may be multiple defects fixed on single commit.  Assumption is
  # users will enter one defect number per line in comments.  Below should
  # handle this since we don't reset b_getDefectNum until blank line
  #
  } elsif ($b_getDefectNum == 1) {

      ($currDefectNum, $currUseCaseNums) = m/^\#(\d+)\s*(.*)/;

      if ($currDefectNum eq "") {
          # If here then we already got defect number and saved to hashes (or directory was added)
          # so skip and reset to get ready for next defect.
          $b_getDefectNum = 0;
          @currFiles = ("");
          $currTag = "";
          next;
      }
      if ($currTag eq "") {
          # If currTag empty then must have been added to head (since there was no "Tag:" line)
          # so skip and reset to get ready for next defect.
          $b_getDefectNum = 0;
          @currFiles = ("");
          $currTag = "";
          next;
      }

      # Connect to DB and run SQL insert statement for each file.
      # Use eval/do as try/catch.
      #
      eval {
         my $dbh = DBI->connect($DB_CONNECT, $DB_LOGIN, '');
         foreach $afile (@currFiles) {
             if ($afile ne "") {
               $moduleFile = $cdir . "/" . $afile;
               $sqlstr = sprintf("insert into cvs.CVS_COMMITS(FILENAME, BRANCH, COMMITTED_BY, DEFECT_NUM, BUILD, USE_CASES, COMMITTED_ON) values ('%s','%s','%s','%s', '%s', '%s', to_date('%s','MONTH DD, YYYY HH:MI:SS AM'))\n",
                  $moduleFile, $currTag, $cname, $currDefectNum, "TBD", $currUseCaseNums, $cdate);
               printf OUTSQL $sqlstr;
               my $sth = $dbh->prepare($sqlstr);
               $sth->execute();
             }
         }
         $dbh->disconnect();
         1;
      } or do {
         open ERRLOG, ">> $ERRFILE" or die "!!! NOTIFY BUILDMASTER IMMEDIATELY !!! CAN'T WRITE TO: $ERRFILE";
         printf ERRLOG "Error logging for defect: $currDefectNum for files: @currFiles\n";
         close(ERRLOG);
      }
  }
}

print OUTLOG "\n";
close(OUTSQL);
close(OUTLOG);
