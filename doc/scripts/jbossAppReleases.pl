#!/usr/bin/perl
#
# jbossAppReleases.pl - Script to dump release numbers for each deployed EAR 
#
$JBOSS_APP_HOME = '/u01/app/jboss-5.1.0.GA/server';
%manHash = '';
%dirHash = '';


# Get list of all container directories for reference.
# We do this since there are some containers that have no EARs (maybe just WAR)
# so although we can't determine version, we can at least include them in list.
$dirListStr = `ls -lp $JBOSS_APP_HOME`;
@dirList = split(/\n/, $dirListStr);

# Pre-populate hash of directories to assume no EAR found
foreach(@dirList) {
   ($container) = m# (\S+)/$#;
   $dirHash{$container}{"NoEarFound"} = "true";
}


# Get list of all ears for reference
$earListStr = `find $JBOSS_APP_HOME -name *.ear`;
@earList = split(/\n/, $earListStr);

# Pre-populate hash of Manifest files with "Unknown" value
foreach(@earList) {
   ($container,$module) = m#/server/([^/]+)/.+/([^/]+)\.ear$#;
   $manHash{$container}{$module} = "Unknown";
   $dirHash{$container}{"NoEarFound"} = "false";
}


# Get list of Manifest files from oldest to newest
$manListStr = `find $JBOSS_APP_HOME -name MANIFEST.MF | grep "/tmp/" | xargs ls -lrt`;
@manList = split(/\n/, $manListStr);

# Build hash of Manifest files.  Since list is oldest to newest, only newest of each container/module will remain
foreach(@manList) {
   ($container,$module) = m#/([^/]+)/tmp/.+/([^\.]+)\.war/META-INF#;
   ($filepath) = m#(\S+MANIFEST.MF)$#;
   if ($manHash{$container}{$module}) {
      # We only add if container/module pair exists from our 'find' of all ears, since there may be
      # old tmp directories for ears that are no longer deployed.
      $manHash{$container}{$module} = $filepath;
   }
}

$nodename = `uname -n`;
$nodename =~ s/\s+//g;
print "\n===$nodename===\n\n";

# Loop over Manifest files and extract Build-Number
foreach $containerEntry (sort keys %manHash) {
   foreach $moduleEntry (sort keys %{$manHash{$containerEntry}}) {
      $fileType = 'Exploded';
      $manFilename = $manHash{$containerEntry}{$moduleEntry};
      $manFilename =~ s/^\s+//;
      if ($manFilename eq "Unknown") {
         # No Manifest file found, which most likely means EAR didn't contain WAR file, so explode EAR in tmp to locate Manifest
         $fileType = 'EAR';
         if (!(-e "/tmp/jbossAppReleases")) {
            `mkdir /tmp/jbossAppReleases`;
         } elsif (-e "/tmp/jbossAppReleases/META-INF/MANIFEST.MF") {
            `rm /tmp/jbossAppReleases/META-INF/MANIFEST.MF`;
         }
         $efile = `find $JBOSS_APP_HOME -name $moduleEntry.ear`;
         chomp($efile);
         `cd /tmp/jbossAppReleases; jar xf $efile META-INF/MANIFEST.MF`;
         $manFilename = "/tmp/jbossAppReleases/META-INF/MANIFEST.MF";
         #printf "%30s %28s %s\n", $containerEntry, $moduleEntry, $manFilename;
     } 
     if (length($manFilename) > 1) {
        $rlse = 'Unknown';
         open MF, "< $manFilename" or die "Can't open Manifest file: $manFilename";
         while(<MF>) {
            if (m/Build-Number:\s*(.+)$/) {
              $rlse = $1;
            $rlse =~ s/\s+//g;
           }
         }
         close MF;
         #printf "%30s %28s %12s %s\n", $containerEntry, $moduleEntry, $rlse, $fileType;
         printf "%s:%s:%s:%s\n", $containerEntry, $moduleEntry, $rlse, $fileType;
      }     
   }
}
# Loop over directories and list those not containing EARs
foreach $containerEntry (sort keys %dirHash) {
   if ($containerEntry eq "" || $containerEntry eq "TEMPLATE") {
      next;
   }
   if ($dirHash{$containerEntry}{"NoEarFound"} eq "true") {
      printf "%s:%s:%s:%s\n", $containerEntry, "NoEarFound", "Unknown", "None";
   }
}
exit;
