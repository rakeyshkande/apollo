#!/usr/bin/perl
#
# jbossAppReleaseAggregator.pl [collect|parse]
#
# This script aggregates the output of jbossAppReleases.pl that is run on each of the Apollo
# front/back-end nodes, then parses it to create an HTML file listing all Apollo containers 
# and the release number for each deployed module.
#
# Input Parameter:
#    None      - Do everything
#    'collect' - Just collect output from all nodes and write to file
#    'parse'   - Just parse output file and create HTML 
#
%allHosts = '';
%allApps = '';
$activeColor = "#ffffff";
$collect = 1;
$parse = 1;
if ($ARGV[0] eq 'collect') {
  $parse = 0;
} elsif ($ARGV[0] eq 'parse') {
  $collect = 0;
}


#--------
#
# Constants
#
%NODE_IPS = (
  nickel => '10.222.71.95',
  titanium => '10.222.71.94',
  aluminum => '10.222.71.93',
  chrome => '10.222.71.36',
  ruby => '10.222.71.131',
  sapphire => '10.222.71.133',
  emerald => '10.222.71.132',
  palladium => '10.222.71.35'
);

%F5_POOLS = (
  aafes => 'consumer_aafes',
  accounting => 'consumer_accounting',
  address_verification_service => 'consumer_avs',
  b2b => 'consumer_b2b',
  com => 'consumer_com',
  dashboard => 'consumer_dashboard', 
  decision_framework => 'consumer_decision',
  email_ops_admin => 'consumer_emailops_admin',
  fit => 'consumer_fit',
  florist_maint => 'consumer_florist',
  florist_service => 'consumer-floristservice',
  joe => 'consumer_joe',
  marketing => 'consumer_marketing',
  newsletter_novator => 'consumer_newsletter',
  order_gatherer => 'consumer_og',
  order_scrub_ui => 'consumer_scrub',
  partner_reward => 'consumer_partner_reward',
  payment_service => 'consumer_paymentservice',
  pdb => 'consumer_pdb',
  product_images => 'consumer_productimages',
  queue => 'consumer_queue',
  scheduler => 'consumer_scheduler',
  security_menus_and_admin => 'consumer_security',
  text_search => 'consumer_text_search',
  web_services => 'consumer_webservices'
);

$COLLECTION_SCRIPT = "/home/gsergey/jbossAppReleases.pl";

$ROW_COLOR_1 = "#f0f0f0";
$ROW_COLOR_2 = "#ffffff";
$OC4J_COLOR_1 = "#f8f8ee";
$OC4J_COLOR_2 = "#f0f0cc";
$ACTIVE_COLOR_1 = "#E7F0E3";
$ACTIVE_COLOR_2 = "#C7DEC0";
$INACTIVE_COLOR_1 = "#ffe5e5";
$INACTIVE_COLOR_2 = "#ffd0d0";

format HTML_HEAD_1 = 
<head>
  <META http-equiv="Content-Type" content="text/html">
  <title>FTD - Product Dependencies</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css">
</head>
<body>
<center>
<div class="label">
.

format HTML_HEAD_2 =
</div>
</center>
  <div id="content" style="display:block">
     <table id="" width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
       <tr>
         <td class="ColHeaderBig">Application Deployment Versions</td>
       </tr>
       <tr>
         <td>
           <table align="left" cellspacing="2" cellpadding="0" width="95%">
.

$HTML_FOOT = "
</table>
</td></tr></table>
</div>
</body>
";


#--------
#
# Collect/aggregate data from each of the app servers
#
if ($collect == 1) {

   # Check that status of the keyring
   #
   $keyring = `/bin/ssh-add -L`;
   chomp($keyring);

   if($keyring eq "The agent has no identities.") {
     print "You have not setup your keyring.\n";
     exit 1;
   }

   # Find the application versions on all of the application servers.
   # Write to local file since easier to debug if there are issues.
   #
   open (AGGFILE, ">/tmp/jbossAppRelease.out") || die ("Could not open file for output. $!");
   foreach $node (sort keys %NODE_IPS) {
     print "Gathering application status on: $node\n";
     $version=`ssh $node "$COLLECTION_SCRIPT"`;
     print AGGFILE $version;
   }
   close (AGGFILE);
}


#--------
#
# Parse aggregate data and create HTML page
#
if ($parse == 1) {

   # Open the aggregate file and parse into hashes
   #
   open (AGGFILE, "</tmp/jbossAppRelease.out") || die ("Could not open file for read. $!");

   while(<AGGFILE>) {
      if (m#^===([^=]+)===#) {
        $curHost = $1;
        $allHosts{$curHost} = 1;
      }
      @mdata = split(/:/);
      if ($#mdata == 3) {
         $cver = $mdata[2];
         $cver =~ s/RLSE_//;
         $allApps{$mdata[0]}{$mdata[1]}{$curHost} = $cver;
      }
   }
   close(AGGFILE);

   # Dump hashes as HTML
   #
   $~ = 'HTML_HEAD_1';
   write();
   $timedate=`date`;
   print $timedate;
   $~ = 'HTML_HEAD_2';
   write();
   print "<tr class=\"label\" bgcolor=\"#d0d0d0\">";
   print "<td>JBoss Container</td><td>F5 Pool</td><td>Application</td>";
   foreach $host (sort keys %allHosts) {
      if ($host eq "") {next;}
      print "<td>$host<br/>$NODE_IPS{$host}</td>\n";
   }
   print "</tr>\n";
   foreach $oc4j (sort keys %allApps) {
      $oc4jDisplay = $oc4j;
      if ($oc4jColor eq $OC4J_COLOR_2) {$oc4jColor = $OC4J_COLOR_1;}
      else {$oc4jColor = $OC4J_COLOR_2;}
      $firstApp = 1;
      foreach $app (sort keys %{$allApps{$oc4j}}) {
         if ($firstApp == 1) {
            $firstApp = 0;
            $hr = "style=\"border-top:solid 1px #aaaaaa\"";
         } else {
            $hr = "";
         }
         if ($rowColor eq $ROW_COLOR_2) {$rowColor = $ROW_COLOR_1;}
         else {$rowColor = $ROW_COLOR_2;}
         print "<tr bgcolor=\"$rowColor\">";
         if ($hr eq "") {
            print "<td bgcolor=\"$oc4jColor\"></td><td bgcolor=\"$oc4jColor\"></td><td $hr>$app</td>";
         } else {
            $f5pool = $F5_POOLS{$oc4j};
            if ($f5pool eq "") {
               $f5pool = "-";
            }
            print "<td $hr bgcolor=\"$oc4jColor\">$oc4jDisplay</td><td $hr bgcolor=\"$oc4jColor\">$f5pool</td><td $hr>$app</td>";
         }
         foreach $host (sort keys %allHosts) {
            if ($host eq "") {next;}
            if ($allApps{$oc4j}{$app}{$host}) {
               $curRlse = $allApps{$oc4j}{$app}{$host};
               if (length($curRlse) > 0) {
                  print "<td $hr>$curRlse</td>";
               } else {
                  print "<td $hr>-</td>";
               }
            } else {
              print "<td $hr>-</td>";
            }
         } # end foreach $host
         print "</tr>\n";
      } # end foreach $app
   } # end foreach $oc4j

   print "$HTML_FOOT\n";
}

exit 1;