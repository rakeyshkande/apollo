set define off

----
---- START Add New Preferred Partner Release Script
----

----
---- Defect @@@defect.number@@@
----

--*****************************************************
-- Partner Master / Partner Program / Program Reward
--*****************************************************

insert into FTD_APPS.PARTNER_MASTER (
    PARTNER_NAME,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    FILE_SEQUENCE_PREFIX, 
    PREFERRED_PROCESSING_RESOURCE,
    PREFERRED_PARTNER_FLAG,
    REPLY_EMAIL_ADDRESS,
    DEFAULT_PHONE_SOURCE_CODE,
    DEFAULT_WEB_SOURCE_CODE,
    BIN_PROCESSING_FLAG,
    FLORIST_RESEND_ALLOWED_FLAG,
    DISPLAY_NAME) 
values (
    '@@@partner.name@@@',
    SYSDATE, 'DEFECT_@@@defect.number@@@', SYSDATE, 'DEFECT_@@@defect.number@@@',
    null,
    '@@@partner.preferred.resource.uppercase@@@',
    '@@@partner.preferredPartnerFlag@@@',
    '@@@partner.email.address@@@',
    '@@@partner.defaultPhoneSourceCode@@@',
    '@@@partner.defaultWebSourceCode@@@',
    '@@@partner.binprocessingflag@@@',
    '@@@partner.floristResendAllowedFlag@@@',
    '@@@partner.displayName@@@');

insert into FTD_APPS.PARTNER_PROGRAM (
    PROGRAM_NAME,
    PARTNER_NAME,
    PROGRAM_TYPE,
    EMAIL_EXCLUDE_FLAG,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    LAST_POST_DATE,
    PROGRAM_LONG_NAME,
    MEMBERSHIP_DATA_REQUIRED) 
values (
    '@@@partner.program.name@@@',
    '@@@partner.name@@@',
    '@@@partner.program.type@@@',
    '@@@partner.program.emailExcludeFlag@@@',
    SYSDATE, 'DEFECT_@@@defect.number@@@', SYSDATE, 'DEFECT_@@@defect.number@@@',
    null,
    null,
    '@@@partner.program.membership.data.required@@@');

insert into FTD_APPS.PROGRAM_REWARD (
    PROGRAM_NAME,
    CALCULATION_BASIS,
    POINTS,
    REWARD_TYPE,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    CUSTOMER_INFO,
    REWARD_NAME,
    PARTICIPANT_ID_LENGTH_CHECK,
    PARTICIPANT_EMAIL_CHECK,
    MAXIMUM_POINTS,
    BONUS_CALCULATION_BASIS,
    BONUS_POINTS,
    BONUS_SEPARATE_DATA)
values (
    '@@@partner.program.name@@@',
    '@@@program.reward.calculationBasis@@@',
    '@@@program.reward.points@@@',
    '@@@program.reward.type@@@',
    sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@',
    null, null, null, null, null, null, null, null);



--*************************************************
-- Security
--*************************************************

--Create a new resource
insert into aas.resources     (resource_id, context_id, description, created_on, updated_on, updated_by)
values                        ('@@@partner.preferred.resource.uppercase@@@','Order Proc','Controls access to view/update @@@partner.displayName@@@ order and customer information', sysdate, sysdate, 'DEFECT_@@@defect.number@@@');

--Create a new resource group
insert into aas.acl           (acl_name, created_on, updated_on, updated_by, acl_level)
values                        ('@@@partner.resource.group@@@', sysdate, sysdate, 'DEFECT_@@@defect.number@@@', null);

--Create relationship between resource and resource group
insert into aas.rel_acl_rp    (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values                        ('@@@partner.resource.group@@@','@@@partner.preferred.resource.uppercase@@@','Order Proc','View',sysdate,sysdate,'DEFECT_@@@defect.number@@@');

--Create a new role
insert into aas.role          (role_id, role_name, context_id, description, created_on, updated_on, updated_by)
values                        (aas.role_id.nextval, '@@@partner.resource.role@@@' , 'Order Proc', '@@@partner.name@@@ CSR View', sysdate, sysdate, 'DEFECT_@@@defect.number@@@' );


--Create relationship between role and resource group
insert into aas.rel_role_acl  (role_id, acl_name, created_on, updated_on, updated_by)
values                        ((select ROLE_ID from aas.role where ROLE_NAME = '@@@partner.resource.role@@@'), '@@@partner.resource.group@@@', sysdate, sysdate, 'DEFECT_@@@defect.number@@@' );


--*************************************************
-- Global Parms
--*************************************************

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('PREFERRED_PARTNER_CONFIG', '@@@partner.name@@@_DCON_EMAIL_TITLE', 'DCONSYS - @@@partner.name@@@', 'DEFECT_7091', 
       sysdate, 'DEFECT_7091', sysdate, 'The title of the Delivery Confirmation Stock Email for @@@partner.name@@@.');

insert into frp.global_parms ( context, name, value, created_on, created_by, updated_on, updated_by, description) values
( 'PREFERRED_PARTNER_CONFIG', '@@@partner.name@@@_DCON_QUEUE_DELAY', '@@@partner.dconqueue.delay@@@', sysdate, 'SYS', sysdate, 'SYS',
'The number of hours past 12:00am on the day after the delivery date to send an @@@partner.name@@@ order to the DC Queue.');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('ins_novator_mailbox_monitor', '@@@partner.name@@@ MAIL SERVER', '@@@partner.mail.server@@@', 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'mail server to retrieve @@@partner.displayName@@@ customer service emails'); 

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('ins_novator_mailbox_monitor', '@@@partner.name@@@ EXCLUDE DOMAINS','@@@partner.exclude.domains@@@', 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'mail server to retrieve @@@partner.displayName@@@ customer service emails'); 

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('ins_novator_mailbox_monitor', '@@@partner.name@@@ DEFAULT PARTNER NAME','@@@partner.name@@@', 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'mail server to retrieve @@@partner.displayName@@@ customer service emails');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values 
('VALIDATION_SERVICE', '@@@partner.name@@@_MAX_ORDER_TOTAL', '@@@partner.shopping.cart.limit@@@', 'SYS', SYSDATE, 'SYS', SYSDATE, 
'Maximum allowable @@@partner.name@@@ shopping cart limit before placing entire cart into FRAUD');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values 
('VALIDATION_SERVICE', '@@@partner.name@@@_MAX_ITEM_TOTAL', '@@@partner.single.order.limit@@@', 'SYS', SYSDATE, 'SYS', SYSDATE, 
'Maximum allowable @@@partner.name@@@ single order limit before placing into FRAUD');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values 
('VALIDATION_SERVICE', '@@@partner.name@@@_MAX_ITEM_ADDON_TOTAL', '@@@partner.order.max.add.on.limit@@@', 'SYS', SYSDATE, 'SYS', SYSDATE, 
'Maximum allowable @@@partner.name@@@ addon limit before placing into FRAUD');

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values 
('ORDER_PROCESSING', '@@@partner.name@@@_LP_INDICATOR_TIMEFRAME', '@@@partner.lp.threshold.time@@@', 'SYS', SYSDATE, 'SYS', SYSDATE, 
'Timeframe in HOURS where allowed number of @@@partner.name@@@ orders specified in LP_INDICATOR_THRESHOLD before placing subsequent orders into FRAUD');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values 
('ORDER_PROCESSING', '@@@partner.name@@@_LP_INDICATOR_THRESHOLD', '@@@partner.lp.threshold.orders@@@', 'SYS', SYSDATE, 'SYS', SYSDATE, 
'Number of @@@partner.name@@@ orders allowable in LP_INDICATOR_TIMEFRAME before placing subsequent orders into FRAUD');


--*************************************************
-- Content Detail
--*************************************************

insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'COM_SOURCE_CODE_RESTRICTION'), 
    '@@@partner.name@@@',
    null, 
    'Stop: You are attempting to change an order to a @@@partner.name@@@ source code.  Please advise the customer:<br><br>''I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you.''<br><br>Transfer the call to @@@partner.transfer.extension@@@', 
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'SOURCE_CODE_RESTRICTION'), 
    '@@@partner.name@@@',
    null, 
    'Stop: You are attempting to change an order to a @@@partner.name@@@ source code.  Please advise the customer: "I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you." Transfer the call to @@@partner.transfer.extension@@@', 
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'ORDER_ACCESS_RESTRICTION'), 
    '@@@partner.name@@@',
    null, 
    'Stop: This is a @@@partner.name@@@ order. Please advise the customer: <br/><br/>"I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you." <br/><br/>Transfer the call to @@@partner.transfer.extension@@@', 
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'PHONE'), 
    '@@@partner.name@@@',
    null, 
    '@@@partner.phone.number@@@', 
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'STOCK_EMAIL' AND CONTENT_NAME = 'TOKEN'), 
    'TEXT',
    '@@@partner.name@@@_EMAIL_LINK', 
    '@@@partner.email.link@@@', 
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'STOCK_EMAIL' AND CONTENT_NAME = 'TOKEN'), 
    'HTML',
    '@@@partner.name@@@_EMAIL_LINK', 
    '@@@partner.email.link.html@@@', 
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PHONE_NUMBER' AND CONTENT_NAME = 'CALL_CENTER_CONTACT_NUMBER'), 
    '@@@partner.name@@@',
    null,
    '@@@partner.phone.number@@@',
    'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@', sysdate);
			
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval,
    (select content_master_id from ftd_apps.content_master
        where CONTENT_NAME = 'TRANSFER_EXTENSION'), 
    '@@@partner.preferred.resource.uppercase@@@',
    null,
    '@@@partner.transfer.extension@@@',
    'DEFECT_@@@defect.number@@@', SYSDATE, 'DEFECT_@@@defect.number@@@', SYSDATE);


--*************************************************
-- Secure Config for Email Request processing
--*************************************************

-- ***NOTE 1:*** The username and password will need to be changed with the production values
-- ***NOTE 2:*** Run as ops$oracle - encryption authorization needed

INSERT INTO FRP.SECURE_CONFIG (
    CONTEXT,
    NAME,
    VALUE,
    DESCRIPTION,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    KEY_NAME)
VALUES (
    'email_request_processing',
    '@@@partner.name@@@_mailbox_monitor_USERNAME',
    global.encryption.encrypt_it('@@@partner.erp.mailbox.username@@@', '@@@partner.erp.mailbox.secure.config.keyname@@@'),
    'Login to pull erp emails for @@@partner.name@@@.',
    sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@',
    '@@@partner.erp.mailbox.secure.config.keyname@@@');

INSERT INTO FRP.SECURE_CONFIG (
    CONTEXT,
    NAME,
    VALUE,
    DESCRIPTION,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    KEY_NAME)
VALUES (
    'email_request_processing',
    '@@@partner.name@@@_mailbox_monitor_PASSWORD',
    global.encryption.encrypt_it('@@@partner.erp.mailbox.password@@@', '@@@partner.erp.mailbox.secure.config.keyname@@@'),
    'Password to pull erp emails for @@@partner.name@@@.',
    sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@',
    '@@@partner.erp.mailbox.secure.config.keyname@@@');

	
--*************************************************
-- Message Tokens
--*************************************************

insert into CLEAN.MESSAGE_TOKENS (TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE) 
values ('@@@partner.preferred.resource.lowercase@@@.phone', 'ALL', 'text', '@@@partner.phone.number@@@', null); 

insert into CLEAN.MESSAGE_TOKENS (TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE) 
values ('@@@partner.preferred.resource.lowercase@@@.email.link','ALL', 'function', '@@@partner.email.link.xsl@@@', null);
 

--*************************************************
-- Stock Messages
--*************************************************

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_APO','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">APOLOGIES</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Our Sincere Apologies re: <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">On behalf of FTD.COM, please accept our sincere apologies for your recent experience.  We always strive to provide the very finest service and we regret that this was not the case.\n\nWe assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',100,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_ASB','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">ADDON SUBSTITUTION @@@partner.displayName@@@</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Our sincere apologies re: <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order from <xsl:value-of select="header/company_name"/>.\n\nWe regret to inform you that the special add-on item you requested is currently unavailable.  We have substituted a similar item to ensure that your order is received on the scheduled delivery date.\n\nWe apologize for any inconvenience that this may cause.\n\nPlease feel free to contact us if we can be of any assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',2500,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_AUVL','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">ADDON UNAVAILABLE @@@partner.displayName@@@</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Our sincere apologies re: <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order from <xsl:value-of select="header/company_name"/>.\n\nWe regret to inform you that the special add-on item you requested is currently unavailable and will not be included with the delivery of your gift.\n\nWe have refunded your <xsl:value-of select="header/payment_method_text"/> in the amount of $<xsl:value-of select="format-number(header/order_amount, ''#,###,##0.00'')"/>.\n\nWe apologize for any inconvenience that this may cause.\n\nPlease feel free to contact us if we can be of any assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',2400,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_AVSB','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">ADDON VASE SUBSTITUTION @@@partner.displayName@@@</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Our sincere apologies re: <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order from <xsl:value-of select="header/company_name"/>.\n\nWe regret to inform you that the special add-on vase you requested is currently unavailable.  We have substituted a similar vase to ensure the timely delivery of your gift.\n\nWe apologize for any inconvenience that this may cause.\n\nPlease feel free to contact us if we can be of any assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',2600,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_CAN','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">CANCEL ORDER REQUEST</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Order Cancel Confirmation <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">At your request, your order has been cancelled.  Please note that you will not see a refund to your <xsl:value-of select="header/payment_method_text"/>. The pending charge will not be processed and will simply drop off.\n\nWe hope you''ll give us the opportunity to assist you in the future.  Please feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',200,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_CC','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">CREDIT CARD AUTH</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Payment Authorization Error <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nWe regret to inform you that the credit card you selected for payment would not authorize.\nPlease contact us to verify that we have the correct information so that we can ensure the delivery is completed without delay.\n\nWe look forward to hearing from you.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',300,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_DD','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">DELIVERY DELAY</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Delivery Date Change <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that there is a one day delay in the delivery of your order.  We sincerely apologize for any inconvenience this may cause.\n\nYour new delivery date is: <xsl:value-of select="items/item/delivery_date"/>\n\nThe shipping fees have been adjusted where applicable.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',400,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_DDW','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">DELIVERY DELAY - WEATHER</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Weather Related Delivery Delay <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that, due to severe weather conditions in the delivery area, your order may be slightly delayed.  Please be assured that the delivery will be completed as soon as the weather permits.\n\nWe sincerely apologize for any inconvenience this may cause.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',500,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_DUP','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">DUPLICATE ORDERS</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Duplicate Orders Received <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nIn reviewing your account, we noted that you have placed two identical orders with us. Please contact us to verify that you did intend to place both orders that we can ensure the delivery is completed without delay.\n\nWe look forward to hearing from you.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',600,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_FUN','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">FUNERAL ORDER - CANCEL</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Order Cancel Notification <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that, despite our best efforts, we were unable to complete the delivery of your order due to the time constraints of the funeral service.  We apologize for the notification of this inconvenience via e-mail, but we were unable to reach you at the phone number provided.\n\nAt this time, your order has been cancelled.  Please note that you will not see a refund to your <xsl:value-of select="header/payment_method_text"/>.  The pending charge will not be processed and will simply drop off.\n\nWe sincerely apologize for any inconvenience this may have caused.  We hope you''ll give us the opportunity to assist you in the future.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',700,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_GEN','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">GENERIC REPLY TEMPLATE</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Your FTD.COM Order <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\n\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',800,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_LP','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">LP - CANNOT PROCESS YOUR ORDER</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Unable to Process Your Order <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that we are unable to process your order.\n\nAt this time, the order has been cancelled and the <xsl:value-of select="header/payment_method_text"/> has not been charged.  \n\nWe sincerely apologize for any inconvenience this may cause.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',900,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_MOD','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MODIFY ORDER REQUEST</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Changes to Your Order <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nWe received your request to modify your order and are pleased to inform you that we will be able to make the change(s) that you requested and your order will arrive without delay.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',1100,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_NCR','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">NO CUSTOMER RESPONSE</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Your FTD.COM Order Inquiry <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nWe received an inquiry from you via email regarding your order.  Despite our best efforts, we have been unable to reach you.\n\nDue to the amount of time that has passed, we are closing the inquiry.  We are hoping that we have not received a reply because the matter has already been resolved.\n\nHowever, please feel free to contact us if you need further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',1300,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_NOFF','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">NO FLORIST - CANCEL</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Order Cancel Notification <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that, despite our best efforts, we were unable to locate a filling florist in the delivery area.  We would like to offer our sincere apologies for any inconvenience this may cause.\n\nAt this time, your order has been cancelled.  Please note that you will not see a refund to your <xsl:value-of select="header/payment_method_text"/>.  The pending charge will not be processed and will simply drop off.\n\nWe assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',1400,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_PEND','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">PENDING ORDER - CANCEL</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Order Cancel Notification <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that we are unable to complete the delivery of your order at this time.  We have attempted to contact you by phone and email for additional information regarding your order without success and sincerely apologize for any inconvenience this may cause.\n\nAt this time, your order has been cancelled.  Please note that you will not see a refund to your <xsl:value-of select="header/payment_method_text"/>.  The pending charge will not be processed and will simply drop off.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',1500,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_PROD','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">PRODUCT UNAVAILABLE - CANCEL</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Order Cancel Notification <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">We regret to inform you that, despite our best efforts, we were unable to fill your order with the selected item.  We would like to offer our sincere apologies for any inconvenience this may cause.\n\nAt this time, your order has been cancelled.  Please note that you will not see a refund to your <xsl:value-of select="header/payment_method_text"/>.  The pending charge will not be processed and will simply drop off.\n\nWe assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',1800,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_RECP','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MISSING DELIVERY INFORMATION</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Missing Delivery Information <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nTo complete the delivery, we will need additional information.\n\nPlease contact us to ensure we have all the necessary details.\n\nWe look forward to hearing from you.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',1000,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_SC','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">SECOND CHOICE NEEDED</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Second Choice Selection Needed <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nWe regret to inform you that the item you selected is no longer available in the delivery area.  We sincerely apologize for any inconvenience this may cause.\n\nPlease contact us so that we may assist you in selecting another product.\n\nWe look forward to hearing from you.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',2000,1,'ALL',null, '@@@partner.name@@@');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'@@@partner.preferred.resource.uppercase@@@_SUB','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">SUBSTITUTION REQUIRED</xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Substitution Notification <xsl:value-of select="items/item/order_number"/></xsl:template></xsl:stylesheet>','<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Thank you for your recent order.\n\nWe regret to inform you that the item you ordered was unavailable in the delivery area. To ensure the timely delivery of your gift, a similar item has been substituted.  When it is necessary to substitute, the utmost care and attention is given to your order to ensure that it is as similar as possible to the requested item.\n\nWe sincerely apologize for any inconvenience this may cause.\n\nPlease feel free to contact us if we can be of any further assistance.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\nContact us:\n\nEmail us via this link: http://www.ftd.com/<xsl:value-of select="items/item/item_source_code"/>/custserv/\n\nPhone: ~@@@partner.preferred.resource.lowercase@@@phonenumber~\n</xsl:template></xsl:stylesheet>',2100,1,'ALL',null, '@@@partner.name@@@');


--*************************************************
-- Stock Email
--*************************************************

begin

--************************************************************************************************************************************************
--cx.after.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.after.@@@partner.name@@@',
'Thank you for contacting FTD.  

We received your recent request to cancel your order.  Unfortunately, your order was already in the process of being delivered.  

We are so sorry that we were unable to accommodate your request.  

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'cx.after.@@@partner.name@@@';

--************************************************************************************************************************************************
--cx.autocancel.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.autocancel.@@@partner.name@@@',
'Thank you for contacting FTD.  

As you requested, your order has been cancelled. We are in the process of refunding your
~payment.method.text~.  You will be notified by email once that is completed.  

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'cx.autocancel.@@@partner.name@@@';

--************************************************************************************************************************************************
--cx.autocancelrefund.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.autocancelrefund.@@@partner.name@@@',
'Thank you for contacting FTD.

As you requested, your order has been canceled and your ~payment.method.text~ has been refunded. 

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'cx.autocancelrefund.@@@partner.name@@@';

--************************************************************************************************************************************************
--cx.cancelled.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.cancelled.@@@partner.name@@@',
'Thank you for contacting FTD.  

This email confirms that your order has been cancelled.  We look forward to assisting you with your floral and gift giving needs in the future.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'cx.cancelled.@@@partner.name@@@';

--************************************************************************************************************************************************
--cx.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.queue.@@@partner.name@@@',
'Thank you for contacting FTD.  

We have received your request to cancel your FTD order. We will cancel your order if it is not already in the process of being designed or delivered.  A customer service agent will contact you with the status of your cancellation request once the investigation is complete.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'cx.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--default.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'default.queue.@@@partner.name@@@',
'Thank you for contacting FTD.  

Our goal is to answer your email within 24 hours. 

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'default.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--di.after.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.after.@@@partner.name@@@',
'Thank you for you inquiry regarding your recent purchase from FTD.

We are pleased to inform you that we have received your order and it has been processed for delivery on ~auto.delivery.date~. When we receive confirmation of delivery we will contact you.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'@@@partner.name@@@' from clean.stock_email where TITLE = 'di.after.@@@partner.name@@@';

--************************************************************************************************************************************************
--di.after.holiday.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.after.holiday.@@@partner.name@@@',
'Thank you for your recent request regarding the delivery of your order.

We are working to confirm the delivery and we will contact you upon completion.  Our goal is to provide a response within 48 hours.  Please note that due to holiday volumes, our investigation may require slightly more time than usual.  We appreciate your patience.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'di.after.holiday.@@@partner.name@@@';

--************************************************************************************************************************************************
--di.before.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.before.@@@partner.name@@@',
'Thank you for your recent request regarding the delivery of your order. 

We''ve confirmed that you order is scheduled to be delivered ~auto.delivery.date~. This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'di.before.@@@partner.name@@@';

--************************************************************************************************************************************************
--di.inquiry.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.inquiry.@@@partner.name@@@',
'Thank you for your recent request regarding the delivery of your order.  

Our Customer Service Department is currently reviewing your inquiry and we will contact you with further information.  Our goal is to have confirmation of delivery to you within 24 hours after the delivery date.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'di.inquiry.@@@partner.name@@@';

--************************************************************************************************************************************************
--di.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.queue.@@@partner.name@@@',
'Thank you for your recent request regarding the delivery of your order.

We are processing your request and will contact you upon completion. Our goal is to have a response to you within 24 hours.

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with FTD.

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'di.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--di.track.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.track.@@@partner.name@@@',
'Thank you for your recent request regarding the delivery of your order.  

We have confirmed that your order is scheduled to be delivered ~auto.delivery.date~ via ~auto.shipper~.  Your tracking information is as follows:

Tracking Number:  ~auto.track.num~
Click here to track:  ~auto.track.url~

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with FTD.

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'di.track.@@@partner.name@@@';

--************************************************************************************************************************************************
--mo.after.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'mo.after.@@@partner.name@@@',
'Thank you for contacting us regarding your order.  

We received your request to make a change to your order.  We found that your order was already in the process of being delivered and could not be changed.  We''re sorry that we are unable to honor your request.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Modify Order Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'mo.after.@@@partner.name@@@';

--************************************************************************************************************************************************
--mo.changed.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'mo.changed.@@@partner.name@@@',
'Thank you for your recent request to modify your order.  

The following changes have been made to your order:

~auto.order.change~

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with FTD.

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Modify Order Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'mo.changed.@@@partner.name@@@';

--************************************************************************************************************************************************
--mo.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'mo.queue.@@@partner.name@@@',
'Thank you for contacting us regarding your recent order.   

We received your request to make a change to your order.  We are processing your request and will contact you upon completion.  

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with FTD.

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Modify Order Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'mo.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--nd.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'nd.queue.@@@partner.name@@@',
'Thank you for contacting us regarding your recent order.  

We are sorry to hear that there are questions surrounding your delivery.  We will investigate this immediately and get back to you with the details.  Our goal is to respond to you within 24 hours.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Delivery Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'nd.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--nd.queue.holiday.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'nd.queue.holiday.@@@partner.name@@@',
'Thank you for contacting us regarding your recent order.  

We are sorry to hear that there are questions regarding the delivery of your order.  We will begin investigating immediately to get the details of the delivery.  Our goal is to respond to you within 48 hours.  However, due to holiday volumes, our investigation may require slightly more time than usual. We appreciate your patience.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Delivery Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'nd.queue.holiday.@@@partner.name@@@';

--************************************************************************************************************************************************
--oa.found.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'oa.found.@@@partner.name@@@',
'Thank you for your recent inquiry regarding your order.  

Our records indicate that your order was successfully received and processed for delivery as requested.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Order Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'oa.found.@@@partner.name@@@';

--************************************************************************************************************************************************
--oa.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'oa.queue.@@@partner.name@@@',
'Thank you for your inquiry regarding your recent order. 

We are in the process of reviewing your request and we will contact you with a confirmation.  

This is an automated response.  We will not receive replies to this email.    

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'oa.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--op.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'op.queue.@@@partner.name@@@',
'Thank you for your inquiry regarding your recent order.  

We are actively working toward an answer for you and will contact you upon completion.  Our goal is to respond to your email within 24 hours.  

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Inquiry Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'op.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--qc.co.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'qc.co.queue.@@@partner.name@@@',
'Thank you for the feedback.    

Once reviewed, we will pass your comments to the appropriate staff member or department.  If there are questions, we will contact you at the email address provided. 

This is an automated response.  We will not receive replies to this email. 

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~ Comment',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'qc.co.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--qc.queue.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'qc.queue.@@@partner.name@@@',
'Thank you for contacting us.  

We are in the process of reviewing your email and will contact you with a response upon completion of our review.  Our goal is to respond within 24 hours.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

FTD Customer Service

Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Re: ~auto.company.name~  Question',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'qc.queue.@@@partner.name@@@';

--************************************************************************************************************************************************
--TRACKING NUMBER.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'TRACKING NUMBER.@@@partner.name@@@',
'This email confirms that your order ~venus.order_number~ has shipped.  The ~venus.carrier_name~ tracking number is ~venus.tracking_number~.  To track the progress of your order, you can go to ~venus.carrier_url~ (please note that at holiday times, your tracking information may not immediately appear on the ~venus.carrier_name~ web site due to the high volume of packages shipping).

If you have any questions regarding the shipping or delivery status of your order, please do not reply to this email.  You can contact ~venus.carrier_name~ at:
~venus.carrier_url~
Phone: ~venus.carrier_phone~

Thank you for shopping with us.
Online: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Tracking Number',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'Y',
'N',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'TRACKING NUMBER.@@@partner.name@@@';

--************************************************************************************************************************************************
--APOLOGIES.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'APOLOGIES.@@@partner.name@@@',
'On behalf of FTD.COM, please accept our sincere apologies for your recent experience.  We always strive to provide our customers with the very finest service and we regret that this was not the case.

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Our Sincere Apologies re:',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'APOLOGIES.@@@partner.name@@@';

--************************************************************************************************************************************************
--CANCEL ORDER REQUEST.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CANCEL ORDER REQUEST.@@@partner.name@@@',
'At your request, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~.  

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We hope you''ll give us the opportunity to assist you in the future.  Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancel Confirmation',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'CANCEL ORDER REQUEST.@@@partner.name@@@';

--************************************************************************************************************************************************
--CANCEL ORDER REQUEST CANNOT CANCEL.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CANCEL ORDER REQUEST CANNOT CANCEL.@@@partner.name@@@',
'Thank you for your recent order.

We received your request to cancel your order and, unfortunately, we are unable to process the cancellation as your order is already in the process of being shipped or delivered.

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancellation Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'CANCEL ORDER REQUEST CANNOT CANCEL.@@@partner.name@@@';

--************************************************************************************************************************************************
--CREDIT CARD AUTH.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CREDIT CARD AUTH.@@@partner.name@@@',
'Thank you for your recent order.

We regret to inform you that the credit card you selected for payment would not authorize.  Please contact us to verify that we have the correct information so that we can ensure the delivery is completed without delay.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Payment Authorization Error',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'CREDIT CARD AUTH.@@@partner.name@@@';

--************************************************************************************************************************************************
--DELIVERY CONFIRMATION - Residence.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY CONFIRMATION - Residence.@@@partner.name@@@',
'We are pleased to inform you that the delivery of your order has been completed as scheduled.  
Please feel free to contact us if we can be of any further assistance.
Sincerely,
~csr.fname~
Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Confirmation of Delivery',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DELIVERY CONFIRMATION - Residence.@@@partner.name@@@';

--************************************************************************************************************************************************
--DELIVERY CONFIRMATION - Funeral Home.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY CONFIRMATION - Funeral Home.@@@partner.name@@@',
'We are pleased to inform you that the delivery of your order to ~facility name token~ has been completed as scheduled. 
Please feel free to contact us if we can be of any further assistance.
Sincerely,
~csr.fname~
Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Confirmation of Delivery',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DELIVERY CONFIRMATION - Funeral Home.@@@partner.name@@@';

--************************************************************************************************************************************************
--DELIVERY CONFIRMATION PENDING.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY CONFIRMATION PENDING.@@@partner.name@@@',
'Thank you for your recent order.

We have received your request to confirm that your order was delivered and as soon as we receive confirmation of delivery, we will notify you via e-mail.

If your order was delivered internationally, it may take up to 72 hours to receive a response.  We will promptly relay a confirmation to you upon receipt.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Confirmation of Delivery Pending',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DELIVERY CONFIRMATION PENDING.@@@partner.name@@@';

--************************************************************************************************************************************************
--DELIVERY DELAY.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY DELAY.@@@partner.name@@@',
'We regret to inform you that there is a one day delay in the delivery of your order.  We sincerely apologize for any inconvenience this may cause.

Your new delivery date is:

The shipping charges have been adjusted where applicable.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Delivery Date Change',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DELIVERY DELAY.@@@partner.name@@@';

--************************************************************************************************************************************************
--DELIVERY DELAY - WEATHER.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY DELAY - WEATHER.@@@partner.name@@@',
'We regret to inform you that, due to severe weather conditions in the delivery area, the delivery of your order may be slightly delayed.  Please be assured that the delivery will be completed as soon as the weather permits.

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Weather Related Delivery Delay',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DELIVERY DELAY - WEATHER.@@@partner.name@@@';

--************************************************************************************************************************************************
--DISCOUNT APPLIED.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DISCOUNT APPLIED.@@@partner.name@@@',
'We have received your request to apply a promotional discount to your order.  At this time, the discount has been applied and a refund in the amount of $~fref.amt~ has been posted to your ~payment.method.text~.

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  .  

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Discount Applied Confirmation',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DISCOUNT APPLIED.@@@partner.name@@@';

--************************************************************************************************************************************************
--DOOR TAG - ATTEMPTED DELIVERY.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DOOR TAG - ATTEMPTED DELIVERY.@@@partner.name@@@',
'Thank you for your recent order.

The delivery of your order was attempted as scheduled; however, no one was home to accept the delivery at the time.  The recipient has been notified to contact the florist directly in order to arrange for a convenient time to complete the delivery.  

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Delivery Attempted',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DOOR TAG - ATTEMPTED DELIVERY.@@@partner.name@@@';

--************************************************************************************************************************************************
--DUPLICATE ORDERS.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DUPLICATE ORDERS.@@@partner.name@@@',
'Thank you for your recent order.

In reviewing your account, we noted that you have placed two identical orders with us. Please contact us to verify that you did intend to place both orders that we can ensure the delivery is completed without delay.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Duplicate Orders Received',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DUPLICATE ORDERS.@@@partner.name@@@';

--************************************************************************************************************************************************
--FUNERAL ORDER - ADDRESS REQUEST.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'FUNERAL ORDER - ADDRESS REQUEST.@@@partner.name@@@',
'Thank you for your recent order.

We have been advised that the family is requesting your address so that they may send you a thank you card.  It is our policy not to give this information to the family without your permission.  Please contact us and let us know if we may honor their request.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Address Permission Request',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'FUNERAL ORDER - ADDRESS REQUEST.@@@partner.name@@@';

--************************************************************************************************************************************************
--FUNERAL ORDER - CANCEL.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'FUNERAL ORDER - CANCEL.@@@partner.name@@@',
'We regret to inform you that, despite our best efforts, we were unable to complete the delivery of your order due to the time constraints of the funeral service.  We apologize for the notification of this inconvenience via e-mail, but we were unable to reach you at the phone number provided.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We sincerely apologize for any inconvenience this may have caused.  We hope you''ll give us the opportunity to assist you in the future.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancel Notification',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'FUNERAL ORDER - CANCEL.@@@partner.name@@@';

--************************************************************************************************************************************************
--GENERIC REPLY TEMPLATE.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'GENERIC REPLY TEMPLATE.@@@partner.name@@@',
'Thank you for your recent order.



Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Your FTD.COM Order',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'GENERIC REPLY TEMPLATE.@@@partner.name@@@';

--************************************************************************************************************************************************
--LP - CANNOT PROCESS YOUR ORDER.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'LP - CANNOT PROCESS YOUR ORDER.@@@partner.name@@@',
'We regret to inform you that we are unable to process your order.

At this time, the order has been cancelled and the ~payment.method.text~ has not been charged.  

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Unable to Process Your Order',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'LP - CANNOT PROCESS YOUR ORDER.@@@partner.name@@@';

--************************************************************************************************************************************************
--MISSING ADD ON.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSING ADD ON.@@@partner.name@@@',
'On behalf of FTD.COM, please accept our sincere apologies for the missing item that was not included with your order.  We regret any inconvenience this may have caused.

At this time, we have refunded your ~payment.method.text~ in the amount of $~fref.amt~.  

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Our Sincere Apologies re:',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'MISSING ADD ON.@@@partner.name@@@';

--************************************************************************************************************************************************
--MISSING CARD SIGNATURE.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSING CARD SIGNATURE.@@@partner.name@@@',
'Thank you for your recent order.

In preparing for the delivery of your order, it has been brought to our attention that the card message was left unsigned. In the event that this was unintentional, we wanted to provide you the opportunity to add your name to the card.

If we do not hear back from you in time to process the change, we will ensure your order is delivered as scheduled.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Missing Card Signature',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'MISSING CARD SIGNATURE.@@@partner.name@@@';

--************************************************************************************************************************************************
--MISSING DELIVERY INFORMATION.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSING DELIVERY INFORMATION.@@@partner.name@@@',
'Thank you for your recent order.

To complete the delivery, we will need additional information.

Please contact us to ensure we have all the necessary details.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Missing Delivery Information',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'MISSING DELIVERY INFORMATION.@@@partner.name@@@';

--************************************************************************************************************************************************
--MODIFY ORDER - CANNOT CHANGE.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MODIFY ORDER - CANNOT CHANGE.@@@partner.name@@@',
'Thank you for your recent order.

We received your request to modify your order and, unfortunately, we cannot apply the change(s) you requested as your order is already in the process of being shipped or delivered.

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Changes to Your Order',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'MODIFY ORDER - CANNOT CHANGE.@@@partner.name@@@';

--************************************************************************************************************************************************
--MODIFY ORDER - UPDATE ORDER PROCESSED.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MODIFY ORDER - UPDATE ORDER PROCESSED.@@@partner.name@@@',
'Thank you for your recent order.

We received your request to modify your order and are pleased to inform you that we will be able to make the change(s) that you requested and your order will arrive without delay.

To accommodate your change(s), the original order was cancelled and a new order was created with the updated information.  As a result, you will see a refund and a new charge on your ~payment.method.text~.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Changes to Your Order',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'MODIFY ORDER - UPDATE ORDER PROCESSED.@@@partner.name@@@';

--************************************************************************************************************************************************
--MODIFY ORDER REQUEST.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MODIFY ORDER REQUEST.@@@partner.name@@@',
'Thank you for your recent order.

We received your request to modify your order and are pleased to inform you that we will be able to make the change(s) that you requested and your order will arrive without delay.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Changes to Your Order',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'MODIFY ORDER REQUEST.@@@partner.name@@@';

--************************************************************************************************************************************************
--NO FLORIST - CANCEL.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'NO FLORIST - CANCEL.@@@partner.name@@@',
'We regret to inform you that, despite our best efforts, we were unable to locate a filling florist to complete the delivery.  We would like to offer our sincere apologies for any inconvenience this may cause.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancel Notification',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'NO FLORIST - CANCEL.@@@partner.name@@@';

--************************************************************************************************************************************************
--ORDER CONFIRMATION - RECEIPT.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'ORDER CONFIRMATION - RECEIPT.@@@partner.name@@@',
'Thank you for your recent order.

Per your request, the details of your order are below.  

~order.details~

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Confirmation',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'ORDER CONFIRMATION - RECEIPT.@@@partner.name@@@';

--************************************************************************************************************************************************
--PENDING ORDER - CANCEL.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'PENDING ORDER - CANCEL.@@@partner.name@@@',
'We regret to inform you that we are unable to complete the delivery of your order at this time.  We have attempted to contact you by phone and email for additional information regarding your order without success and sincerely apologize for any inconvenience this may cause.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancel Notification',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'PENDING ORDER - CANCEL.@@@partner.name@@@';

--************************************************************************************************************************************************
--POSITIVE FEEDBACK REPLY.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'POSITIVE FEEDBACK REPLY.@@@partner.name@@@',
'Thank you for taking the time to let us know about the pleasant experience you had with one of our customer service representatives.  

We take a lot of pride in what we do and it means a great deal to us to hear about experiences such as yours.  Your comments will be passed on to the appropriate staff member for acknowledgment.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Thank You',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'POSITIVE FEEDBACK REPLY.@@@partner.name@@@';

--************************************************************************************************************************************************
--PRODUCT UNAVAILABLE - CANCEL.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'PRODUCT UNAVAILABLE - CANCEL.@@@partner.name@@@',
'We regret to inform you that, despite our best efforts, we were unable to fill your order with the selected item.  We would like to offer our sincere apologies for any inconvenience this may cause.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancel Notification',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'PRODUCT UNAVAILABLE - CANCEL.@@@partner.name@@@';

--************************************************************************************************************************************************
--QUALITY ISSUE - REDELIVERY.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'QUALITY ISSUE - REDELIVERY.@@@partner.name@@@',
'Thank you for your recent order.

We sincerely apologize for any concerns regarding the quality of the flowers received.   We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

At this time, we have arranged for a fresh replacement to be delivered for your recipient to enjoy.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Redelivery of Your Order',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'QUALITY ISSUE - REDELIVERY.@@@partner.name@@@';

--************************************************************************************************************************************************
--QUALITY ISSUE - REFUND.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'QUALITY ISSUE - REFUND.@@@partner.name@@@',
'Thank you for your recent order.

We sincerely apologize for any concerns regarding the quality of the flowers received.   We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

At your request, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~.  

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Order Cancel Confirmation',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'QUALITY ISSUE - REFUND.@@@partner.name@@@';

--************************************************************************************************************************************************
--REROUTING FEE.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'REROUTING FEE.@@@partner.name@@@',
'Thank you for your recent order.

We have received your request to change the address on your carrier-delivered product. 

Per our Delivery Policy, there will be an additional shipping fee charge for shipping address changes after the order has been placed and processed.  Please contact us to confirm this request and approve the additional charge.  

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Rerouting Fee Required',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'REROUTING FEE.@@@partner.name@@@';

--************************************************************************************************************************************************
--SECOND CHOICE NEEDED.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'SECOND CHOICE NEEDED.@@@partner.name@@@',
'Thank you for your recent order.

We regret to inform you that the item you selected is no longer available in the delivery area.  We sincerely apologize for any inconvenience this may cause.

Please contact us so that we may assist you in selecting another product.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Second Choice Selection Needed',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'SECOND CHOICE NEEDED.@@@partner.name@@@';

--************************************************************************************************************************************************
--SUBSTITUTION REQUIRED.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'SUBSTITUTION REQUIRED.@@@partner.name@@@',
'Thank you for your recent order.

We regret to inform you that the item you ordered was unavailable in the delivery area. To ensure the timely delivery of your gift, a similar item has been substituted.  When it is necessary to substitute, the utmost care and attention is given to your order to ensure that it is as similar as possible to the requested item.  

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Substitution Notification',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'SUBSTITUTION REQUIRED.@@@partner.name@@@';

--************************************************************************************************************************************************
--UNSUBSCRIBE REQUEST.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'UNSUBSCRIBE REQUEST.@@@partner.name@@@',
'Thank you for contacting us.

We have processed your request to unsubscribe from future notifications and special offers.  Please allow up to 72 hours for the removal of your email address from promotional campaigns that may already be underway.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Unsubscribe Request Processed',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'UNSUBSCRIBE REQUEST.@@@partner.name@@@';

--************************************************************************************************************************************************
--UPDATE ORDER - PROMOTION APPLIED.@@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'UPDATE ORDER - PROMOTION APPLIED.@@@partner.name@@@',
'Thank you for your recent order.

We have processed your request to have a promotion applied to your order.

To accommodate your change(s), the original order was cancelled and a new order was created with the updated information.  As a result, you will see a refund and a new charge on your ~payment.method.text~.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Promotion Applied',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'UPDATE ORDER - PROMOTION APPLIED.@@@partner.name@@@';


--************************************************************************************************************************************************
--DCONSYS - @@@partner.name@@@
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DCONSYS - @@@partner.name@@@',
'We are pleased to inform you that the delivery of your order to ~facility.name~ has been completed.
Please feel free to contact us if we can be of any further assistance.
Sincerely,
~csr.fname~
Contact us:
Email us via this link: ~@@@partner.preferred.resource.lowercase@@@.email.link~
Phone: ~@@@partner.preferred.resource.lowercase@@@.phone~
',
'Confirmation of Delivery',
SYSDATE,
'DEFECT_@@@defect.number@@@',
SYSDATE,
'DEFECT_@@@defect.number@@@',
'N',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, '@@@partner.name@@@' from clean.stock_email where TITLE = 'DCONSYS - @@@partner.name@@@';

end;


--*************************************************
-- Stock Letters
--*************************************************


begin

insert into clean.stock_letter (
    stock_letter_id,
    title,
    body,
    created_on, created_by, updated_on, updated_by)
values (
    clean.stock_letter_id_sq.nextval,
    'APOLOGY RECIPIENT @@@partner.name@@@',
    '~cur.date~

~recip.fname~ ~recip.lname~
~recip.address1~
~recip.city~, ~recip.state~ ~recip.zip~ ~recip.country~

Dear ~recip.fname~ ~recip.lname~,

On behalf of FTD, please accept our sincere apologies for your recent experience.  We always strive to provide our customers with the very finest service and we regret that this was not the case.  

We greatly value your feedback so that we have the opportunity to uphold our Good as Gold Guarantee.  We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,

~csr.fname~
',
    sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@');

insert into clean.stock_letter_company_ref (
    stock_letter_id,
    company_id,
    origin_id,
    partner_name)
values (
    (select stock_letter_id
        from clean.stock_letter
        where title = 'APOLOGY RECIPIENT @@@partner.name@@@'),
    'ALL',
    null,
    '@@@partner.name@@@');


insert into clean.stock_letter (
    stock_letter_id,
    title,
    body,
    created_on, created_by, updated_on, updated_by)
values (
    clean.stock_letter_id_sq.nextval,
    'APOLOGY CUSTOMER @@@partner.name@@@',
    '~cur.date~

~cust.fname~ ~cust.lname~
~cust.address1~
~cust.city~, ~cust.state~ ~cust.zip~

Dear ~cust.fname~ ~cust.lname~,

On behalf of FTD, please accept our sincere apologies for your recent experience.  We always strive to provide our customers with the very finest service and we regret that this was not the case.  

We greatly value your feedback so that we have the opportunity to uphold our Good as Gold Guarantee.  We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,

~csr.fname~
',
    sysdate, 'DEFECT_@@@defect.number@@@', sysdate, 'DEFECT_@@@defect.number@@@');

insert into clean.stock_letter_company_ref (
    stock_letter_id,
    company_id,
    origin_id,
    partner_name)
values (
    (select stock_letter_id
        from clean.stock_letter
        where title = 'APOLOGY CUSTOMER @@@partner.name@@@'),
    'ALL',
    null,
    '@@@partner.name@@@');

end;


----
---- END Add New Preferred Partner Release Script
----
