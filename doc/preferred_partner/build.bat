@echo off

if "%1" == "" (
    @echo "Must specify dev, qa, or prod"
) else (
    ant -file build.xml -Denvironment.name=%1
)
