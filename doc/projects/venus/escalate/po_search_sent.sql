
   select so.SO_ID
   from
   	   tn_supplier supp,
	   tn_supplier_order so,
	   tn_orderline_product op,
	   tn_status_summary_type sstp,
	   tn_supplier_sku ss,
	   tn_ship_instruction si,
	   tn_rel_ol_si olsi,
	   tn_merchant_ship_method msm
   where
            supp.SUPP_INFO_EXCHANGE_ROOT = :: SUPPLIER_INFOR_EXCHANGE_ROOT
	   and  supp.SUPP_ACTIVE = 1
	   and  so.SO_SUPP_ID = supp.SUPP_ID
	   and  so.so_active = 1
	   and  so.SO_CONF_NUMBER = ::PO_NUM
	   and  sstp.sstp_id = so.so_sstp_id
	   and  sstp.SSTP_ACTIVE = 1
	   and  sstp.sstp_name = :: STATE_NAME
	   and  op.op_so_id = so.so_id
	   and  op.op_active = 1
	   and  op.op_ship_req_date between ::SHIP_REQUEST_FROM_DATE and ::SHIP_REQUEST_TO_DATE
	   and  op.op_delivery_req_date between ::DELIVERY_REQ_FROM_DATE and ::DELIVERY_REQ_TO_DATE 
	   and  ss.SS_ID = op.op_ss_id
	   and  ss.SS_ACTIVE = 1
	   and  ss.SS_VENDOR_SPECIFIC_ID = ::SUPP_SKU_VENDOR_SPECIFIC_ID
	   and  olsi.olsi_ol_id = op.op_id
	   and  olsi.olsi_effective_status = 1
       and  olsi.olsi_effective_date = (
            SELECT 
               MAX(OLSI2.olsi_effective_date)
            FROM
               tn_rel_ol_si OLSI2
            WHERE
                   OLSI2.olsi_ol_id = OLSI.olsi_ol_id
               and OLSI2.olsi_active = 1
               and OLSI2.olsi_effective_status in ( 1, -1 )
               and OLSI2.olsi_effective_date <= sysdate 
            )
       and  si.si_id = olsi.olsi_si_id 
	   and  si.si_active = 1
	   and  msm.MSM_ID = SI.si_sm_id
	   and  msm.MSM_ACTIVE = 1
	   and  msm.MSM_DISPLAY_NAME = ::SHIP_METHOD 
