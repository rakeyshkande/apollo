
select
PKGI.pkgi_transaction_id TransactionID,
PKGI.pkgi_transaction_type TransactionType,
OP.op_id OP_ID,
SO.SO_SUPP_ID SUPP_ID,
OC_SHIPPER.oc_last ShipperName, 
CT_SHIPPER_ADDR.ct_address1 ShipperAddress1, 
CT_SHIPPER_ADDR.ct_address2 ShipperAddress2, 
CT_SHIPPER_ADDR.ct_city ShipperCity,
CT_SHIPPER_ADDR.ct_state_name ShipperState, 
CT_SHIPPER_ADDR.ct_zipcode ShipperPostalCode,
CT_SHIPPER_PHONE.ct_phone_number ShipperPhoneNumber,
CNTY_SHIPPER.CNTY_ISO3166_A3_CODE ShipperCountryCode,
CT_RECIPIENT_ADDR.ct_company_name RecipientCompany,
OC_RECIPIENT.oc_last RecipientContactName, 
CT_RECIPIENT_ADDR.ct_address1 RecipientAddress1, 
CT_RECIPIENT_ADDR.ct_address2 RecipientAddress2, 
CT_RECIPIENT_ADDR.ct_city RecipientCity,
CT_RECIPIENT_ADDR.ct_state_name RecipientState, 
CT_RECIPIENT_ADDR.ct_zipcode RecipientPostalCode,
CT_RECIPIENT_PHONE.ct_phone_number RecipientPhoneNumber,
CNTY_RECIPIENT.CNTY_ISO3166_A3_CODE RecipientCountryCode,
ORCA.orca_org_acct_num PayorAccountNumber,
PKGI.pkgi_weight PackageWeight,
'01' PackageType,
GSMT.GSMT_SERVICE_CODE ServiceType,
op.op_ship_req_date ShipDate, 
SI.si_payment_code PaymentType
from
     tn_merchant MRCH,
	 tn_rel_org_carr ORCA,
     tn_supplier_order SO,
	 tn_status_summary_type SSTP,
	 tn_orderline_product OP,
	 tn_rel_ol_si OLSI,
	 tn_ship_instruction SI,
	 tn_package_instruction PKGI,
	 tn_merchant_ship_method MSM,
	 tn_global_ship_method_type GSMT,
	 tn_order_customer OC_SHIPPER,
	 tn_order_customer_type OCTP_SHIPPER,
	 tn_site_user SU_SHIPPER,
	 tn_party PATY_SHIPPER,
	 tn_rel_party_contact PTCT_SHIPPER_ADDR,
	 tn_rel_party_contact PTCT_SHIPPER_PHONE,
	 tn_contact CT_SHIPPER_ADDR,
	 tn_contact CT_SHIPPER_PHONE,
	 tn_contact_type CTT_SHIPPER_ADDR,
	 tn_contact_type CTT_SHIPPER_PHONE,
	 tn_country CNTY_SHIPPER,
	 tn_order_customer OC_RECIPIENT,
	 tn_order_customer_type OCTP_RECIPIENT,
	 tn_site_user SU_RECIPIENT,
	 tn_party PATY_RECIPIENT,
	 tn_rel_party_contact PTCT_RECIPIENT_ADDR,
	 tn_rel_party_contact PTCT_RECIPIENT_PHONE,
	 tn_contact CT_RECIPIENT_ADDR,
	 tn_contact CT_RECIPIENT_PHONE,
	 tn_contact_type CTT_RECIPIENT_ADDR,
	 tn_contact_type CTT_RECIPIENT_PHONE,
	 tn_country CNTY_RECIPIENT
where  
   -- Get all the PO in the created state 
       SO.so_active = 1
   AND SSTP.sstp_id = SO.so_sstp_id
   AND SSTP.sstp_active = 1
   AND SSTP.sstp_name = 'created'
   -- Requested ship date is between now and 10 day later
   AND OP.op_so_id = SO.so_id
   AND OP.op_active = 1
   AND OP.op_ship_req_date < sysdate + 10
   --Get Package Instruction and Ship Instruction
   AND PKGI.pkgi_id = op.op_pkgi_id
   AND OLSI.olsi_ol_id = OP.op_id
   AND OLSI.olsi_si_id = SI.si_id
   -- Get Global Shipping Method Type
   AND MSM.MSM_ID = SI.si_sm_id
   AND MSM.MSM_GSMT_ID = GSMT.GSMT_ID
   -- Get Payer Account
   AND MRCH.MRCH_ID = MSM.MSM_MRCH_ID
   AND ORCA.orca_org_id = MRCH.MRCH_ORG_ID
   AND ORCA.orca_carr_id = GSMT.GSMT_CARR_ID
   -- Get Shipper Information
   AND OC_SHIPPER.oc_id = SI.si_shipper_oc_id
   AND OC_SHIPPER.oc_id = SU_SHIPPER.su_oc_id
   AND OCTP_SHIPPER.octp_id = OC_SHIPPER.oc_octp_id
   -- 
   AND OCTP_SHIPPER.octp_name = 'SHIP_FROM'
   AND SU_SHIPPER.su_paty_id = PATY_SHIPPER.paty_id
   AND PTCT_SHIPPER_ADDR.ptct_pt_id (+)= PATY_SHIPPER.paty_id
   AND PTCT_SHIPPER_ADDR.ptct_active (+)= 1
   AND PTCT_SHIPPER_ADDR.ptct_effective_status (+)= 1
   AND PTCT_SHIPPER_ADDR.ptct_effective_date = (
       SELECT max(PTCT2_SHIPPER_ADDR.ptct_effective_date)
                FROM    tn_rel_party_contact PTCT2_SHIPPER_ADDR
                WHERE   PTCT2_SHIPPER_ADDR.ptct_pt_id = PATY_SHIPPER.paty_id
                    AND PTCT2_SHIPPER_ADDR.ptct_active = 1
                    AND PTCT2_SHIPPER_ADDR.ptct_effective_status in (1, -1)
                    AND PTCT2_SHIPPER_ADDR.ptct_effective_date <= sysdate
        )
   -- Get shipper Address contact 
   AND CTT_SHIPPER_ADDR.ctt_name = 'ORDER_CONTACT_ADDRESS'
   AND PTCT_SHIPPER_ADDR.ptct_ctt_id = CTT_SHIPPER_ADDR.ctt_id
   AND CT_SHIPPER_ADDR.ct_id = PTCT_SHIPPER_ADDR.ptct_ct_id
   AND CNTY_SHIPPER.cnty_id = CT_SHIPPER_ADDR.ct_cnty_id
   -- Get Shipper Phone Contact
   AND PTCT_SHIPPER_PHONE.ptct_pt_id (+)= PATY_SHIPPER.paty_id
   AND PTCT_SHIPPER_PHONE.ptct_active (+) = 1
   AND PTCT_SHIPPER_PHONE.ptct_effective_status (+)= 1
   AND PTCT_SHIPPER_PHONE.ptct_effective_date = (
       SELECT max(PTCT2_SHIPPER_PHONE.ptct_effective_date)
                FROM    tn_rel_party_contact PTCT2_SHIPPER_PHONE
                WHERE   PTCT2_SHIPPER_PHONE.ptct_pt_id = PATY_SHIPPER.paty_id
                    AND PTCT2_SHIPPER_PHONE.ptct_active = 1
                    AND PTCT2_SHIPPER_PHONE.ptct_effective_status in (1, -1)
                    AND PTCT2_SHIPPER_PHONE.ptct_effective_date <= sysdate
        )
   AND CTT_SHIPPER_PHONE.ctt_name = 'ORDER_CONTACT_PHONE'
   AND PTCT_SHIPPER_PHONE.ptct_ctt_id = CTT_SHIPPER_PHONE.ctt_id
   AND CT_SHIPPER_PHONE.ct_id = PTCT_SHIPPER_PHONE.ptct_ct_id
   -- Get recipient Information
   AND OC_RECIPIENT.oc_id = SI.si_oc_id
   AND OC_RECIPIENT.oc_id = SU_RECIPIENT.su_oc_id
   AND OCTP_RECIPIENT.octp_id = OC_RECIPIENT.oc_octp_id
   AND OCTP_RECIPIENT.octp_name = 'SHIP_TO'
   AND SU_RECIPIENT.su_paty_id = PATY_RECIPIENT.paty_id
   AND PTCT_RECIPIENT_ADDR.ptct_pt_id (+)= PATY_RECIPIENT.paty_id
   AND PTCT_RECIPIENT_ADDR.ptct_active (+)= 1
   AND PTCT_RECIPIENT_ADDR.ptct_effective_status (+)= 1
   AND PTCT_RECIPIENT_ADDR.ptct_effective_date = (
       SELECT max(PTCT2_RECIPIENT_ADDR.ptct_effective_date)
                FROM    tn_rel_party_contact PTCT2_RECIPIENT_ADDR
                WHERE   PTCT2_RECIPIENT_ADDR.ptct_pt_id = PATY_RECIPIENT.paty_id
                    AND PTCT2_RECIPIENT_ADDR.ptct_active = 1
                    AND PTCT2_RECIPIENT_ADDR.ptct_effective_status in (1, -1)
                    AND PTCT2_RECIPIENT_ADDR.ptct_effective_date <= sysdate
        )
   -- Get Recipient address information
   AND CTT_RECIPIENT_ADDR.ctt_name = 'ORDER_CONTACT_ADDRESS'
   AND PTCT_RECIPIENT_ADDR.ptct_ctt_id = CTT_RECIPIENT_ADDR.ctt_id
   AND CT_RECIPIENT_ADDR.ct_id = PTCT_RECIPIENT_ADDR.ptct_ct_id
   AND CNTY_RECIPIENT.cnty_id = CT_RECIPIENT_ADDR.ct_cnty_id
   AND PTCT_RECIPIENT_PHONE.ptct_pt_id (+) = PATY_RECIPIENT.paty_id
   AND PTCT_RECIPIENT_PHONE.ptct_active (+) = 1
   AND PTCT_RECIPIENT_PHONE.ptct_effective_status (+) = 1
   AND PTCT_RECIPIENT_PHONE.ptct_effective_date = (
       SELECT max(PTCT2_RECIPIENT_PHONE.ptct_effective_date)
                FROM    tn_rel_party_contact PTCT2_RECIPIENT_PHONE
                WHERE   PTCT2_RECIPIENT_PHONE.ptct_pt_id = PATY_RECIPIENT.paty_id
                    AND PTCT2_RECIPIENT_PHONE.ptct_active = 1
                    AND PTCT2_RECIPIENT_PHONE.ptct_effective_status in (1, -1)
                    AND PTCT2_RECIPIENT_PHONE.ptct_effective_date <= sysdate
        )
   -- Get Recipient Phone information
   AND CTT_RECIPIENT_PHONE.ctt_name = 'ORDER_CONTACT_PHONE'
   AND PTCT_RECIPIENT_PHONE.ptct_ctt_id = CTT_RECIPIENT_PHONE.ctt_id
   AND CT_RECIPIENT_PHONE.ct_id = PTCT_RECIPIENT_PHONE.ptct_ct_id
