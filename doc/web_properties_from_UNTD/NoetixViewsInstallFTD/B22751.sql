--B22751.sql
-- Used in wnoetxu5.sql
--
-- Title                                                             
--    B22751.sql  
--                                                 
-- Purpose       : 
--    omit Acct$ (placeholder) from FA_Adjustments_SLA_GL_Je
--    
--
-- Requested by  : First Choice Expedictions
--
-- History: 
--    01-Oct-09 C Chavez script created
--    20-Aug-13 J Krieg modified for customer in 130820-000018
--    
--	

@utlspon B22751


update n_view_columns
set omit_flag = 'Y'
where view_name = 'FA_Adjustments_SLA_GL_Je'
and (column_label = 'Acct$' 
     and query_position = 4)
;

commit;



@utlspoff

--end B22751.sql