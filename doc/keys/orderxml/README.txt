key generated on 1/19/2011 by John Tomich

gpg --gen-key

Options:
RSA and RSA
keysize = 1024
No expiration date
userID = apolloprod
Passphrase used: yes
passphrase hint: existing production password

gpg --export-secret-keys -a -a apolloprod >apolloprod_private.txt  (command used to export private key) 2025 bytes

gpg --export -a apolloprod >apolloprod_public.txt (command used to export public key) 1005 bytes



