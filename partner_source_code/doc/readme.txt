This project is created to host partner code that needs maintenance. 

The following packages belong to United Mileage Plus Redemption project. They are compiled to united_ws.jar.
The code in these packages are created by converting the United WSDL's. Changes have been made in the code
so that web service endpoints are retrieved from the database.
1. com.uls
2. com.united