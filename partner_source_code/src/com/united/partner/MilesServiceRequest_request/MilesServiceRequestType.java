/**
 * MilesServiceRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.MilesServiceRequest_request;

public class MilesServiceRequestType  extends com.united.partner.request.ServiceRequestType  implements java.io.Serializable {
    private int miles;

    public MilesServiceRequestType() {
    }

    public MilesServiceRequestType(
           org.apache.axis.types.Token mpNumber,
           int miles) {
        super(
            mpNumber);
        this.miles = miles;
    }


    /**
     * Gets the miles value for this MilesServiceRequestType.
     * 
     * @return miles
     */
    public int getMiles() {
        return miles;
    }


    /**
     * Sets the miles value for this MilesServiceRequestType.
     * 
     * @param miles
     */
    public void setMiles(int miles) {
        this.miles = miles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MilesServiceRequestType)) return false;
        MilesServiceRequestType other = (MilesServiceRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.miles == other.getMiles();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getMiles();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MilesServiceRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:MilesServiceRequest-request.partner.united.com", "MilesServiceRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("miles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "miles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
