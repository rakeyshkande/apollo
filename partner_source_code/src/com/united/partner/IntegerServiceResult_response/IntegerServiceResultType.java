/**
 * IntegerServiceResultType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.IntegerServiceResult_response;

public class IntegerServiceResultType  extends com.united.partner.response.ServiceResult  implements java.io.Serializable {
    private int integerResult;

    public IntegerServiceResultType() {
    }

    public IntegerServiceResultType(
           boolean error,
           com.united.partner.err_response.ServiceErrorType serviceError,
           java.lang.String mpNumber,
           com.united.partner.response.ReturnType[] returns,
           int integerResult) {
        super(
            error,
            serviceError,
            mpNumber,
            returns);
        this.integerResult = integerResult;
    }


    /**
     * Gets the integerResult value for this IntegerServiceResultType.
     * 
     * @return integerResult
     */
    public int getIntegerResult() {
        return integerResult;
    }


    /**
     * Sets the integerResult value for this IntegerServiceResultType.
     * 
     * @param integerResult
     */
    public void setIntegerResult(int integerResult) {
        this.integerResult = integerResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IntegerServiceResultType)) return false;
        IntegerServiceResultType other = (IntegerServiceResultType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.integerResult == other.getIntegerResult();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getIntegerResult();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IntegerServiceResultType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:IntegerServiceResult-response.partner.united.com", "IntegerServiceResultType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("integerResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "integerResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
