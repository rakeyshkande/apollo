/**
 * ReturnType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.response;

public class ReturnType  implements java.io.Serializable {
    private org.apache.axis.types.Token returnName;  // attribute

    private org.apache.axis.types.Token returnValue;  // attribute

    public ReturnType() {
    }

    public ReturnType(
           org.apache.axis.types.Token returnName,
           org.apache.axis.types.Token returnValue) {
           this.returnName = returnName;
           this.returnValue = returnValue;
    }


    /**
     * Gets the returnName value for this ReturnType.
     * 
     * @return returnName
     */
    public org.apache.axis.types.Token getReturnName() {
        return returnName;
    }


    /**
     * Sets the returnName value for this ReturnType.
     * 
     * @param returnName
     */
    public void setReturnName(org.apache.axis.types.Token returnName) {
        this.returnName = returnName;
    }


    /**
     * Gets the returnValue value for this ReturnType.
     * 
     * @return returnValue
     */
    public org.apache.axis.types.Token getReturnValue() {
        return returnValue;
    }


    /**
     * Sets the returnValue value for this ReturnType.
     * 
     * @param returnValue
     */
    public void setReturnValue(org.apache.axis.types.Token returnValue) {
        this.returnValue = returnValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReturnType)) return false;
        ReturnType other = (ReturnType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.returnName==null && other.getReturnName()==null) || 
             (this.returnName!=null &&
              this.returnName.equals(other.getReturnName()))) &&
            ((this.returnValue==null && other.getReturnValue()==null) || 
             (this.returnValue!=null &&
              this.returnValue.equals(other.getReturnValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReturnName() != null) {
            _hashCode += getReturnName().hashCode();
        }
        if (getReturnValue() != null) {
            _hashCode += getReturnValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReturnType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:response.partner.united.com", "ReturnType"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("returnName");
        attrField.setXmlName(new javax.xml.namespace.QName("", "returnName"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("returnValue");
        attrField.setXmlName(new javax.xml.namespace.QName("", "returnValue"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
