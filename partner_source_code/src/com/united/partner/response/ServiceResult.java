/**
 * ServiceResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.response;


/**
 * error:        flase: successful call.  true:  call failed
 * 		serviceError: The error information, such as code and message, when
 * call failed.
 * 		mpNumber:     The active MP#.  Not in use today since the partners
 * are not ready to recompile their APPs.
 * 		Returns:      A list of the results.  Not in use today since the
 * partners are not ready to recompile their APPs.
 */
public abstract class ServiceResult  implements java.io.Serializable {
    private boolean error;

    private com.united.partner.err_response.ServiceErrorType serviceError;

    private java.lang.String mpNumber;

    private com.united.partner.response.ReturnType[] returns;

    public ServiceResult() {
    }

    public ServiceResult(
           boolean error,
           com.united.partner.err_response.ServiceErrorType serviceError,
           java.lang.String mpNumber,
           com.united.partner.response.ReturnType[] returns) {
           this.error = error;
           this.serviceError = serviceError;
           this.mpNumber = mpNumber;
           this.returns = returns;
    }


    /**
     * Gets the error value for this ServiceResult.
     * 
     * @return error
     */
    public boolean isError() {
        return error;
    }


    /**
     * Sets the error value for this ServiceResult.
     * 
     * @param error
     */
    public void setError(boolean error) {
        this.error = error;
    }


    /**
     * Gets the serviceError value for this ServiceResult.
     * 
     * @return serviceError
     */
    public com.united.partner.err_response.ServiceErrorType getServiceError() {
        return serviceError;
    }


    /**
     * Sets the serviceError value for this ServiceResult.
     * 
     * @param serviceError
     */
    public void setServiceError(com.united.partner.err_response.ServiceErrorType serviceError) {
        this.serviceError = serviceError;
    }


    /**
     * Gets the mpNumber value for this ServiceResult.
     * 
     * @return mpNumber
     */
    public java.lang.String getMpNumber() {
        return mpNumber;
    }


    /**
     * Sets the mpNumber value for this ServiceResult.
     * 
     * @param mpNumber
     */
    public void setMpNumber(java.lang.String mpNumber) {
        this.mpNumber = mpNumber;
    }


    /**
     * Gets the returns value for this ServiceResult.
     * 
     * @return returns
     */
    public com.united.partner.response.ReturnType[] getReturns() {
        return returns;
    }


    /**
     * Sets the returns value for this ServiceResult.
     * 
     * @param returns
     */
    public void setReturns(com.united.partner.response.ReturnType[] returns) {
        this.returns = returns;
    }

    public com.united.partner.response.ReturnType getReturns(int i) {
        return this.returns[i];
    }

    public void setReturns(int i, com.united.partner.response.ReturnType _value) {
        this.returns[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceResult)) return false;
        ServiceResult other = (ServiceResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.error == other.isError() &&
            ((this.serviceError==null && other.getServiceError()==null) || 
             (this.serviceError!=null &&
              this.serviceError.equals(other.getServiceError()))) &&
            ((this.mpNumber==null && other.getMpNumber()==null) || 
             (this.mpNumber!=null &&
              this.mpNumber.equals(other.getMpNumber()))) &&
            ((this.returns==null && other.getReturns()==null) || 
             (this.returns!=null &&
              java.util.Arrays.equals(this.returns, other.getReturns())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isError() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getServiceError() != null) {
            _hashCode += getServiceError().hashCode();
        }
        if (getMpNumber() != null) {
            _hashCode += getMpNumber().hashCode();
        }
        if (getReturns() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReturns());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReturns(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:response.partner.united.com", "ServiceResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceError");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serviceError"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:err-response.partner.united.com", "ServiceErrorType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mpNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mpNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Returns"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:response.partner.united.com", "ReturnType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
