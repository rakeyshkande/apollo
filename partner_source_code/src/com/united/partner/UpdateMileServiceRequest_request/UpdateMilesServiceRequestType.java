/**
 * UpdateMilesServiceRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.UpdateMileServiceRequest_request;

public class UpdateMilesServiceRequestType  extends com.united.partner.MilesServiceRequest_request.MilesServiceRequestType  implements java.io.Serializable {
    private java.lang.String bonusCode;

    private java.lang.String bonusType;

    private com.united.partner.UpdateMileServiceRequest_request.MileIndicatorFlag mileIndicator;

    private java.lang.Integer earnMo;

    private java.lang.Integer earnDay;

    private java.lang.Integer earnYr;

    public UpdateMilesServiceRequestType() {
    }

    public UpdateMilesServiceRequestType(
           org.apache.axis.types.Token mpNumber,
           int miles,
           java.lang.String bonusCode,
           java.lang.String bonusType,
           com.united.partner.UpdateMileServiceRequest_request.MileIndicatorFlag mileIndicator,
           java.lang.Integer earnMo,
           java.lang.Integer earnDay,
           java.lang.Integer earnYr) {
        super(
            mpNumber,
            miles);
        this.bonusCode = bonusCode;
        this.bonusType = bonusType;
        this.mileIndicator = mileIndicator;
        this.earnMo = earnMo;
        this.earnDay = earnDay;
        this.earnYr = earnYr;
    }


    /**
     * Gets the bonusCode value for this UpdateMilesServiceRequestType.
     * 
     * @return bonusCode
     */
    public java.lang.String getBonusCode() {
        return bonusCode;
    }


    /**
     * Sets the bonusCode value for this UpdateMilesServiceRequestType.
     * 
     * @param bonusCode
     */
    public void setBonusCode(java.lang.String bonusCode) {
        this.bonusCode = bonusCode;
    }


    /**
     * Gets the bonusType value for this UpdateMilesServiceRequestType.
     * 
     * @return bonusType
     */
    public java.lang.String getBonusType() {
        return bonusType;
    }


    /**
     * Sets the bonusType value for this UpdateMilesServiceRequestType.
     * 
     * @param bonusType
     */
    public void setBonusType(java.lang.String bonusType) {
        this.bonusType = bonusType;
    }


    /**
     * Gets the mileIndicator value for this UpdateMilesServiceRequestType.
     * 
     * @return mileIndicator
     */
    public com.united.partner.UpdateMileServiceRequest_request.MileIndicatorFlag getMileIndicator() {
        return mileIndicator;
    }


    /**
     * Sets the mileIndicator value for this UpdateMilesServiceRequestType.
     * 
     * @param mileIndicator
     */
    public void setMileIndicator(com.united.partner.UpdateMileServiceRequest_request.MileIndicatorFlag mileIndicator) {
        this.mileIndicator = mileIndicator;
    }


    /**
     * Gets the earnMo value for this UpdateMilesServiceRequestType.
     * 
     * @return earnMo
     */
    public java.lang.Integer getEarnMo() {
        return earnMo;
    }


    /**
     * Sets the earnMo value for this UpdateMilesServiceRequestType.
     * 
     * @param earnMo
     */
    public void setEarnMo(java.lang.Integer earnMo) {
        this.earnMo = earnMo;
    }


    /**
     * Gets the earnDay value for this UpdateMilesServiceRequestType.
     * 
     * @return earnDay
     */
    public java.lang.Integer getEarnDay() {
        return earnDay;
    }


    /**
     * Sets the earnDay value for this UpdateMilesServiceRequestType.
     * 
     * @param earnDay
     */
    public void setEarnDay(java.lang.Integer earnDay) {
        this.earnDay = earnDay;
    }


    /**
     * Gets the earnYr value for this UpdateMilesServiceRequestType.
     * 
     * @return earnYr
     */
    public java.lang.Integer getEarnYr() {
        return earnYr;
    }


    /**
     * Sets the earnYr value for this UpdateMilesServiceRequestType.
     * 
     * @param earnYr
     */
    public void setEarnYr(java.lang.Integer earnYr) {
        this.earnYr = earnYr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateMilesServiceRequestType)) return false;
        UpdateMilesServiceRequestType other = (UpdateMilesServiceRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.bonusCode==null && other.getBonusCode()==null) || 
             (this.bonusCode!=null &&
              this.bonusCode.equals(other.getBonusCode()))) &&
            ((this.bonusType==null && other.getBonusType()==null) || 
             (this.bonusType!=null &&
              this.bonusType.equals(other.getBonusType()))) &&
            ((this.mileIndicator==null && other.getMileIndicator()==null) || 
             (this.mileIndicator!=null &&
              this.mileIndicator.equals(other.getMileIndicator()))) &&
            ((this.earnMo==null && other.getEarnMo()==null) || 
             (this.earnMo!=null &&
              this.earnMo.equals(other.getEarnMo()))) &&
            ((this.earnDay==null && other.getEarnDay()==null) || 
             (this.earnDay!=null &&
              this.earnDay.equals(other.getEarnDay()))) &&
            ((this.earnYr==null && other.getEarnYr()==null) || 
             (this.earnYr!=null &&
              this.earnYr.equals(other.getEarnYr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBonusCode() != null) {
            _hashCode += getBonusCode().hashCode();
        }
        if (getBonusType() != null) {
            _hashCode += getBonusType().hashCode();
        }
        if (getMileIndicator() != null) {
            _hashCode += getMileIndicator().hashCode();
        }
        if (getEarnMo() != null) {
            _hashCode += getEarnMo().hashCode();
        }
        if (getEarnDay() != null) {
            _hashCode += getEarnDay().hashCode();
        }
        if (getEarnYr() != null) {
            _hashCode += getEarnYr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateMilesServiceRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:UpdateMileServiceRequest-request.partner.united.com", "UpdateMilesServiceRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bonusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bonusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bonusType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bonusType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mileIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mileIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:UpdateMileServiceRequest-request.partner.united.com", "mileIndicatorFlag"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("earnMo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "earnMo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("earnDay");
        elemField.setXmlName(new javax.xml.namespace.QName("", "earnDay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("earnYr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "earnYr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
