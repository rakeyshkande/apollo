/**
 * PartnerFlag.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.PartnerServiceRequest_request;

public class PartnerFlag implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PartnerFlag(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _ted = "ted";
    public static final java.lang.String _silverWings = "silverWings";
    public static final java.lang.String _redCarpet = "redCarpet";
    public static final java.lang.String _globalSvcs = "globalSvcs";
    public static final java.lang.String _millionMiler = "millionMiler";
    public static final java.lang.String _firstUSA = "firstUSA";
    public static final java.lang.String _smallBus = "smallBus";
    public static final java.lang.String _collegePlus = "collegePlus";
    public static final java.lang.String _sprint = "sprint";
    public static final java.lang.String _americanExp = "americanExp";
    public static final java.lang.String _passPlus = "passPlus";
    public static final java.lang.String _bankOne = "bankOne";
    public static final PartnerFlag ted = new PartnerFlag(_ted);
    public static final PartnerFlag silverWings = new PartnerFlag(_silverWings);
    public static final PartnerFlag redCarpet = new PartnerFlag(_redCarpet);
    public static final PartnerFlag globalSvcs = new PartnerFlag(_globalSvcs);
    public static final PartnerFlag millionMiler = new PartnerFlag(_millionMiler);
    public static final PartnerFlag firstUSA = new PartnerFlag(_firstUSA);
    public static final PartnerFlag smallBus = new PartnerFlag(_smallBus);
    public static final PartnerFlag collegePlus = new PartnerFlag(_collegePlus);
    public static final PartnerFlag sprint = new PartnerFlag(_sprint);
    public static final PartnerFlag americanExp = new PartnerFlag(_americanExp);
    public static final PartnerFlag passPlus = new PartnerFlag(_passPlus);
    public static final PartnerFlag bankOne = new PartnerFlag(_bankOne);
    public java.lang.String getValue() { return _value_;}
    public static PartnerFlag fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PartnerFlag enumeration = (PartnerFlag)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PartnerFlag fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PartnerFlag.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:PartnerServiceRequest-request.partner.united.com", "PartnerFlag"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
