/**
 * PartnerServiceRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.PartnerServiceRequest_request;

public class PartnerServiceRequestType  extends com.united.partner.request.ServiceRequestType  implements java.io.Serializable {
    private com.united.partner.PartnerServiceRequest_request.PartnerFlag partnerCode;

    public PartnerServiceRequestType() {
    }

    public PartnerServiceRequestType(
           org.apache.axis.types.Token mpNumber,
           com.united.partner.PartnerServiceRequest_request.PartnerFlag partnerCode) {
        super(
            mpNumber);
        this.partnerCode = partnerCode;
    }


    /**
     * Gets the partnerCode value for this PartnerServiceRequestType.
     * 
     * @return partnerCode
     */
    public com.united.partner.PartnerServiceRequest_request.PartnerFlag getPartnerCode() {
        return partnerCode;
    }


    /**
     * Sets the partnerCode value for this PartnerServiceRequestType.
     * 
     * @param partnerCode
     */
    public void setPartnerCode(com.united.partner.PartnerServiceRequest_request.PartnerFlag partnerCode) {
        this.partnerCode = partnerCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PartnerServiceRequestType)) return false;
        PartnerServiceRequestType other = (PartnerServiceRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.partnerCode==null && other.getPartnerCode()==null) || 
             (this.partnerCode!=null &&
              this.partnerCode.equals(other.getPartnerCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPartnerCode() != null) {
            _hashCode += getPartnerCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PartnerServiceRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:PartnerServiceRequest-request.partner.united.com", "PartnerServiceRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partnerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "partnerCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:PartnerServiceRequest-request.partner.united.com", "PartnerFlag"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
