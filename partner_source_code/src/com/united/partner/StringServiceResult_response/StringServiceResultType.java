/**
 * StringServiceResultType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.StringServiceResult_response;

public class StringServiceResultType  extends com.united.partner.response.ServiceResult  implements java.io.Serializable {
    private java.lang.String stringResult;

    public StringServiceResultType() {
    }

    public StringServiceResultType(
           boolean error,
           com.united.partner.err_response.ServiceErrorType serviceError,
           java.lang.String mpNumber,
           com.united.partner.response.ReturnType[] returns,
           java.lang.String stringResult) {
        super(
            error,
            serviceError,
            mpNumber,
            returns);
        this.stringResult = stringResult;
    }


    /**
     * Gets the stringResult value for this StringServiceResultType.
     * 
     * @return stringResult
     */
    public java.lang.String getStringResult() {
        return stringResult;
    }


    /**
     * Sets the stringResult value for this StringServiceResultType.
     * 
     * @param stringResult
     */
    public void setStringResult(java.lang.String stringResult) {
        this.stringResult = stringResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StringServiceResultType)) return false;
        StringServiceResultType other = (StringServiceResultType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.stringResult==null && other.getStringResult()==null) || 
             (this.stringResult!=null &&
              this.stringResult.equals(other.getStringResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStringResult() != null) {
            _hashCode += getStringResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StringServiceResultType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:StringServiceResult-response.partner.united.com", "StringServiceResultType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stringResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "stringResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
