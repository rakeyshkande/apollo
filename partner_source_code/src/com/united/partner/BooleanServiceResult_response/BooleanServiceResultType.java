/**
 * BooleanServiceResultType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.united.partner.BooleanServiceResult_response;

public class BooleanServiceResultType  extends com.united.partner.response.ServiceResult  implements java.io.Serializable {
    private boolean booleanResult;

    public BooleanServiceResultType() {
    }

    public BooleanServiceResultType(
           boolean error,
           com.united.partner.err_response.ServiceErrorType serviceError,
           java.lang.String mpNumber,
           com.united.partner.response.ReturnType[] returns,
           boolean booleanResult) {
        super(
            error,
            serviceError,
            mpNumber,
            returns);
        this.booleanResult = booleanResult;
    }


    /**
     * Gets the booleanResult value for this BooleanServiceResultType.
     * 
     * @return booleanResult
     */
    public boolean isBooleanResult() {
        return booleanResult;
    }


    /**
     * Sets the booleanResult value for this BooleanServiceResultType.
     * 
     * @param booleanResult
     */
    public void setBooleanResult(boolean booleanResult) {
        this.booleanResult = booleanResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BooleanServiceResultType)) return false;
        BooleanServiceResultType other = (BooleanServiceResultType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.booleanResult == other.isBooleanResult();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += (isBooleanResult() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BooleanServiceResultType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:BooleanServiceResult-response.partner.united.com", "BooleanServiceResultType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("booleanResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "booleanResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
