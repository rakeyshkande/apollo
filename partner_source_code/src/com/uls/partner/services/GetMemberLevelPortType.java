/**
 * GetMemberLevelPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public interface GetMemberLevelPortType extends java.rmi.Remote {
    public com.united.partner.StringServiceResult_response.StringServiceResultType getMemberLevel(com.united.partner.request.ServiceRequestType memberLevelServiceRequest) throws java.rmi.RemoteException;
}
