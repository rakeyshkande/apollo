/**
 * PartnerTotalBalanceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public class PartnerTotalBalanceServiceLocator extends org.apache.axis.client.Service implements com.uls.partner.services.PartnerTotalBalanceService {

    public PartnerTotalBalanceServiceLocator() {
    }


    public PartnerTotalBalanceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PartnerTotalBalanceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for getTotalBalance
    private java.lang.String getTotalBalance_address = "https://services.united.com/services/partner/getTotalBalance";

    public java.lang.String getgetTotalBalanceAddress() {
        return getTotalBalance_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String getTotalBalanceWSDDServiceName = "getTotalBalance";

    public java.lang.String getgetTotalBalanceWSDDServiceName() {
        return getTotalBalanceWSDDServiceName;
    }

    public void setgetTotalBalanceWSDDServiceName(java.lang.String name) {
        getTotalBalanceWSDDServiceName = name;
    }

    public com.uls.partner.services.GetTotalBalancePortType getgetTotalBalance() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getTotalBalance_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getgetTotalBalance(endpoint);
    }

    public com.uls.partner.services.GetTotalBalancePortType getgetTotalBalance(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.uls.partner.services.GetTotalBalanceBindingStub _stub = new com.uls.partner.services.GetTotalBalanceBindingStub(portAddress, this);
            _stub.setPortName(getgetTotalBalanceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setgetTotalBalanceEndpointAddress(java.lang.String address) {
        getTotalBalance_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.uls.partner.services.GetTotalBalancePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.uls.partner.services.GetTotalBalanceBindingStub _stub = new com.uls.partner.services.GetTotalBalanceBindingStub(new java.net.URL(getTotalBalance_address), this);
                _stub.setPortName(getgetTotalBalanceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("getTotalBalance".equals(inputPortName)) {
            return getgetTotalBalance();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:services.partner.uls.com", "PartnerTotalBalanceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:services.partner.uls.com", "getTotalBalance"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("getTotalBalance".equals(portName)) {
            setgetTotalBalanceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
