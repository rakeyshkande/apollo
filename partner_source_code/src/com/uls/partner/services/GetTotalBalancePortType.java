/**
 * GetTotalBalancePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public interface GetTotalBalancePortType extends java.rmi.Remote {
    public com.united.partner.IntegerServiceResult_response.IntegerServiceResultType getTotalBalance(com.united.partner.request.ServiceRequestType serviceRequest) throws java.rmi.RemoteException;
}
