/**
 * GetPartnerRegistrationFlagPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public interface GetPartnerRegistrationFlagPortType extends java.rmi.Remote {
    public com.united.partner.BooleanServiceResult_response.BooleanServiceResultType getPartnerRegistrationFlag(com.united.partner.PartnerServiceRequest_request.PartnerServiceRequestType partnerServiceRequest) throws java.rmi.RemoteException;
}
