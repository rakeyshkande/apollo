/**
 * PartnerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public interface PartnerService extends javax.xml.rpc.Service {
    public java.lang.String getgetPartnerRegistrationFlagAddress();

    public com.uls.partner.services.GetPartnerRegistrationFlagPortType getgetPartnerRegistrationFlag() throws javax.xml.rpc.ServiceException;

    public com.uls.partner.services.GetPartnerRegistrationFlagPortType getgetPartnerRegistrationFlag(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
