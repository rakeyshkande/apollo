/**
 * PartnerMemberLevelService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public interface PartnerMemberLevelService extends javax.xml.rpc.Service {
    public java.lang.String getgetMemberLevelAddress();

    public com.uls.partner.services.GetMemberLevelPortType getgetMemberLevel() throws javax.xml.rpc.ServiceException;

    public com.uls.partner.services.GetMemberLevelPortType getgetMemberLevel(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
