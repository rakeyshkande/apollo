/**
 * AuthenticateMemberPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public interface AuthenticateMemberPortType extends java.rmi.Remote {
    public com.united.partner.BooleanServiceResult_response.BooleanServiceResultType authenticateMember(com.united.partner.AuthServiceRequest_request.AuthServiceRequestType authServiceRequest) throws java.rmi.RemoteException;
}
