/**
 * PartnerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.uls.partner.services;

public class PartnerServiceLocator extends org.apache.axis.client.Service implements com.uls.partner.services.PartnerService {

    public PartnerServiceLocator() {
    }


    public PartnerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PartnerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for getPartnerRegistrationFlag
    private java.lang.String getPartnerRegistrationFlag_address = "https://services.united.com/services/partner/getPartnerRegistrationFlag";

    public java.lang.String getgetPartnerRegistrationFlagAddress() {
        return getPartnerRegistrationFlag_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String getPartnerRegistrationFlagWSDDServiceName = "getPartnerRegistrationFlag";

    public java.lang.String getgetPartnerRegistrationFlagWSDDServiceName() {
        return getPartnerRegistrationFlagWSDDServiceName;
    }

    public void setgetPartnerRegistrationFlagWSDDServiceName(java.lang.String name) {
        getPartnerRegistrationFlagWSDDServiceName = name;
    }

    public com.uls.partner.services.GetPartnerRegistrationFlagPortType getgetPartnerRegistrationFlag() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getPartnerRegistrationFlag_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getgetPartnerRegistrationFlag(endpoint);
    }

    public com.uls.partner.services.GetPartnerRegistrationFlagPortType getgetPartnerRegistrationFlag(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.uls.partner.services.GetPartnerRegistrationFlagBindingStub _stub = new com.uls.partner.services.GetPartnerRegistrationFlagBindingStub(portAddress, this);
            _stub.setPortName(getgetPartnerRegistrationFlagWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setgetPartnerRegistrationFlagEndpointAddress(java.lang.String address) {
        getPartnerRegistrationFlag_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.uls.partner.services.GetPartnerRegistrationFlagPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.uls.partner.services.GetPartnerRegistrationFlagBindingStub _stub = new com.uls.partner.services.GetPartnerRegistrationFlagBindingStub(new java.net.URL(getPartnerRegistrationFlag_address), this);
                _stub.setPortName(getgetPartnerRegistrationFlagWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("getPartnerRegistrationFlag".equals(inputPortName)) {
            return getgetPartnerRegistrationFlag();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:services.partner.uls.com", "PartnerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:services.partner.uls.com", "getPartnerRegistrationFlag"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("getPartnerRegistrationFlag".equals(portName)) {
            setgetPartnerRegistrationFlagEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
