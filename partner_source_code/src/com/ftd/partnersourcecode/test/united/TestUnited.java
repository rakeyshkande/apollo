package com.ftd.partnersourcecode.test.united;

import com.uls.partner.services.AuthenticateMemberBindingStub;
import com.uls.partner.services.GetMemberLevelBindingStub;
import com.uls.partner.services.GetPartnerRegistrationFlagBindingStub;
import com.uls.partner.services.GetTotalBalanceBindingStub;
import com.uls.partner.services.PartnerAuthenticateMemberServiceLocator;
import com.uls.partner.services.PartnerMemberLevelServiceLocator;
import com.uls.partner.services.PartnerServiceLocator;
import com.uls.partner.services.PartnerTotalBalanceServiceLocator;
import com.uls.partner.services.PartnerUpdateMilesServiceLocator;
import com.uls.partner.services.UpdateMilesBindingStub;

import com.united.partner.AuthServiceRequest_request.AuthServiceRequestType;
import com.united.partner.BooleanServiceResult_response.BooleanServiceResultType;
import com.united.partner.IntegerServiceResult_response.IntegerServiceResultType;
import com.united.partner.PartnerServiceRequest_request.PartnerFlag;
import com.united.partner.PartnerServiceRequest_request.PartnerServiceRequestType;
import com.united.partner.StringServiceResult_response.StringServiceResultType;
import com.united.partner.UpdateMileServiceRequest_request.MileIndicatorFlag;
import com.united.partner.UpdateMileServiceRequest_request.UpdateMilesServiceRequestType;
import com.united.partner.request.ServiceRequestType;

import org.apache.axis.client.Stub;


public class TestUnited 
{
/*
    public static void main(String[] args) throws Exception {
        setup();
        testAuthenticate();
        testGetMemberLevel();
        testGetPartnerRegistrationFlag();
        testGetTotalMiles();
        testUpdateMiles();
    }
    */
    
    public static void setup (String ks, String ts, String ksPassword, String tsPassword)throws Exception {
        System.setProperty("javax.net.ssl.keyStore", ks);
        System.setProperty("javax.net.ssl.trustStore", ts);
        System.setProperty("javax.net.ssl.keyStorePassword", ksPassword);
        System.setProperty("javax.net.ssl.trustStorePassword", tsPassword);
    }

    public static String testAuthenticate(String host, String accountNumber, String password) throws Exception {
        AuthenticateMemberBindingStub binding;
        boolean result = false;
        try {
            binding = (AuthenticateMemberBindingStub)new PartnerAuthenticateMemberServiceLocator().getauthenticateMember();
            binding._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, host+"/services/partner/authenticateMember");
            System.out.println("Sending request to:" + host+"/services/partner/authenticateMember");
            // Time out after a minute
            binding.setTimeout(60000);
            
            AuthServiceRequestType serviceRequest = new AuthServiceRequestType();
            // general account
            serviceRequest.setMpNumber(new org.apache.axis.types.Token(accountNumber));
            
            // cardholder
            //serviceRequest.setMpNumber(new org.apache.axis.types.Token("03114063680"));
            
            // elite
            // serviceRequest.setMpNumber(new org.apache.axis.types.Token("03056129188"));
            
            serviceRequest.setPassword(password);
            
            BooleanServiceResultType booleanResult = binding.authenticateMember(serviceRequest);
            
            result = booleanResult.isBooleanResult();
            System.out.print(result);
            
            
        } catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new Exception("JAX-RPC ServiceException caught: " + jre.getMessage(), jre);
        } catch (Exception e) {
            e.printStackTrace();
            
        }   
        return result? "True" : "False";
    }
    
    public static String testGetMemberLevel(String host, String accountNumber) throws Exception {
        GetMemberLevelBindingStub binding;
        String result = null;
        try {
            binding = (GetMemberLevelBindingStub)new PartnerMemberLevelServiceLocator().getgetMemberLevel();
            binding._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, host+"/services/partner/getMemberLevel");
            // Time out after a minute
            binding.setTimeout(60000);
            
            ServiceRequestType serviceRequest = new ServiceRequestType();
            // general account
            serviceRequest.setMpNumber(new org.apache.axis.types.Token(accountNumber));
            
            // cardholder
            //serviceRequest.setMpNumber(new org.apache.axis.types.Token("03114063680"));
            
            // elite
            // serviceRequest.setMpNumber(new org.apache.axis.types.Token("03056129188"));
            
            StringServiceResultType stringResult = binding.getMemberLevel(serviceRequest);
            
            result = stringResult.getStringResult();
            System.out.print(result);
            
            
        } catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new Exception("JAX-RPC ServiceException caught: " + jre.getMessage(), jre);
        } catch (Exception e) {
            e.printStackTrace();
            
        }    
        return result;   
    }
    
    public static String testGetPartnerRegistrationFlag (String host,String accountNumber)  throws Exception {
        GetPartnerRegistrationFlagBindingStub binding;
        boolean result = false;
        try {
            binding = (GetPartnerRegistrationFlagBindingStub)new PartnerServiceLocator().getgetPartnerRegistrationFlag();
            binding._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, host+"/services/partner/getPartnerRegistrationFlag");
            // Time out after a minute
            binding.setTimeout(60000);
            
            PartnerServiceRequestType serviceRequest = new PartnerServiceRequestType();
            // general account
            serviceRequest.setMpNumber(new org.apache.axis.types.Token(accountNumber));
            
            // cardholder
            //serviceRequest.setMpNumber(new org.apache.axis.types.Token("03114063680"));
            
            // elite
            // serviceRequest.setMpNumber(new org.apache.axis.types.Token("03056129188"));
            
            //bank one
            serviceRequest.setPartnerCode(PartnerFlag.fromString(PartnerFlag._bankOne));
            
            //first usa
            //serviceRequest.setPartnerCode(PartnerFlag.fromString(PartnerFlag._firstUSA));
            //small bus
            //serviceRequest.setPartnerCode(PartnerFlag.fromString(PartnerFlag._smallBus));
            
            
            
            BooleanServiceResultType booleanResult = binding.getPartnerRegistrationFlag(serviceRequest);
            
            result = booleanResult.isBooleanResult();
            System.out.print(result);
            
            
        } catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new Exception("JAX-RPC ServiceException caught: " + jre.getMessage(), jre);
        } catch (Exception e) {
            e.printStackTrace();
            
        }          
        return result? "True" : "False";
    }
    
    public static int testGetTotalMiles(String host, String accountNumber) throws Exception {
        GetTotalBalanceBindingStub binding;
        int result = 0;
        try {
            binding = (GetTotalBalanceBindingStub)new PartnerTotalBalanceServiceLocator().getgetTotalBalance();
            binding._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, host+"/services/partner/getTotalBalance");
            // Time out after a minute
            binding.setTimeout(60000);
            
            ServiceRequestType serviceRequest = new ServiceRequestType();
            // general account
            serviceRequest.setMpNumber(new org.apache.axis.types.Token(accountNumber));
            
            // cardholder
            //serviceRequest.setMpNumber(new org.apache.axis.types.Token("03114063680"));
            
            // elite
            // serviceRequest.setMpNumber(new org.apache.axis.types.Token("03056129188"));
            
            IntegerServiceResultType integerResult = binding.getTotalBalance(serviceRequest);
            
            result = integerResult.getIntegerResult();
            System.out.print(result);
            
        } catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new Exception("JAX-RPC ServiceException caught: " + jre.getMessage(), jre);
        } catch (Exception e) {
            e.printStackTrace();
            
        }      
        return result;   
    }
    
    public static String testUpdateMiles(String host, String accountNumber, String miles) throws Exception {
        UpdateMilesBindingStub binding;
        boolean result = false;
        try {
            binding = (UpdateMilesBindingStub)new PartnerUpdateMilesServiceLocator().getupdateMiles();
            binding._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, host+"/services/partner/updateMiles");
            // Time out after a minute
            binding.setTimeout(60000);
            
            //org.apache.axis.types.Token mpNumber,
            //int miles,
            //java.lang.String bonusCode,
            //java.lang.String bonusType,
            //com.united.partner.UpdateMileServiceRequest_request.MileIndicatorFlag mileIndicator,
            //java.lang.Integer earnMo,
            //java.lang.Integer earnDay,
            //java.lang.Integer earnYr
            
            UpdateMilesServiceRequestType serviceRequest = new UpdateMilesServiceRequestType();
            // general account
            serviceRequest.setMpNumber(new org.apache.axis.types.Token(accountNumber));
            
            // cardholder
            //serviceRequest.setMpNumber(new org.apache.axis.types.Token("03114063680"));
            
            // elite
            // serviceRequest.setMpNumber(new org.apache.axis.types.Token("03056129188"));
            serviceRequest.setMiles(Integer.parseInt(miles));
            serviceRequest.setBonusCode("AFW");
            serviceRequest.setBonusType("X1");
            serviceRequest.setMileIndicator(MileIndicatorFlag.fromString(MileIndicatorFlag._D));
            
            BooleanServiceResultType booleanResult = binding.updateMiles(serviceRequest);
            
            result = booleanResult.isBooleanResult();
            System.out.print(result);
            
            
        } catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new Exception("JAX-RPC ServiceException caught: " + jre.getMessage(), jre);
        } catch (Exception e) {
            e.printStackTrace();
            
        }      
        return result? "True" : "False";
    }
}