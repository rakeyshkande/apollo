package com.ftd.newsletterevents.bo.ejb;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface NewsletterPubEJBLocalHome extends EJBLocalHome 
{
  NewsletterPubEJBLocal create() throws CreateException;
}