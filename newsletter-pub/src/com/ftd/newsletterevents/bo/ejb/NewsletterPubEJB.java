package com.ftd.newsletterevents.bo.ejb;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;
import com.ftd.newsletterevents.core.NewsletterEventVO;
import com.ftd.newsletterevents.exception.FtdNewsletterException;
import com.ftd.newsletterevents.vo.StatusType;
import java.util.Date;

public interface NewsletterPubEJB extends EJBObject 
{
  void process(String emailAddress, String companyId, StatusType status, String operatorId, Date statusTimestamp) throws RemoteException, FtdNewsletterException;

  void process(String originAppName, String emailAddress, String companyId, StatusType status, String operatorId, Date statusTimestamp) throws RemoteException, FtdNewsletterException;

}