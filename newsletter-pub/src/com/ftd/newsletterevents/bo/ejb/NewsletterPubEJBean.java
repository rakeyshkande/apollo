package com.ftd.newsletterevents.bo.ejb;

import com.ftd.newsletterevents.core.ErrorService;
import com.ftd.newsletterevents.core.IConstants;
import com.ftd.newsletterevents.core.NewsletterEventVO;
import com.ftd.newsletterevents.core.XmlService;
import com.ftd.newsletterevents.exception.FtdNewsletterException;
import com.ftd.newsletterevents.vo.StatusType;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.Date;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

import javax.naming.InitialContext;


public class NewsletterPubEJBean implements SessionBean {
    protected static Logger logger = new Logger(NewsletterPubEJBean.class.getName());
    private SessionContext sessionContext;
    private Queue _QUEUE;
    private QueueConnectionFactory _QUEUE_CONNECTION_FACTORY;
    private int _JMS_PUB_RETRY_MAX_COUNT;
    private int _JMS_PUB_RETRY_WAIT_INTERVAL;
    private String _JMS_VALUE_ORIGIN_APP_NAME;

    protected void init() {
        // Initialize instance variables to support JMS queue publishing.
        InitialContext jndiContext = null;

        try {
            try {
                jndiContext = new InitialContext();

                // Initialize vars.
                _JMS_PUB_RETRY_MAX_COUNT = ((Integer) jndiContext.lookup(
                        "java:comp/env/dequeue-retry-count")).intValue();
                _JMS_PUB_RETRY_WAIT_INTERVAL = ((Integer) jndiContext.lookup(
                        "java:comp/env/dequeue-retry-interval")).intValue();
                _JMS_VALUE_ORIGIN_APP_NAME = (String) jndiContext.lookup("java:comp/env/origin-app-name");

                // Lookup the queue.
                _QUEUE = (Queue) jndiContext.lookup(IConstants.ALIAS_QUEUE_NAME);

                // Lookup the Connection factory
                _QUEUE_CONNECTION_FACTORY = (QueueConnectionFactory) jndiContext.lookup(
                        IConstants.ALIAS_QUEUE_CONNECTION_FACTORY);
            } catch (Throwable e) {
                logger.error("ejbCreate: Failed to initialize JMS publisher.", e);
            }
        } finally {
            if (jndiContext != null) {
                try {
                    jndiContext.close();
                } catch (Throwable e) {
                    logger.error("ejbCreate: JNDI context clean-up logic failed.",
                        e);
                }
            }
        }
    }

    public void ejbCreate() {
        init();
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbRemove() {
        // Clean up.
        _QUEUE_CONNECTION_FACTORY = null;
        _QUEUE = null;
    }

    public void setSessionContext(SessionContext ctx) {
        this.sessionContext = ctx;
    }

    public void process(String emailAddress, String companyId, StatusType status, String operatorId, Date statusTimestamp) throws FtdNewsletterException {
        // Verify inputs.
        if ((emailAddress == null) || (companyId == null) || (status == null)) {
            throw new FtdNewsletterException(
                "process: Inputs to process() can not be null.");
        }

        process(_JMS_VALUE_ORIGIN_APP_NAME, emailAddress, companyId, status, operatorId, statusTimestamp);
    }
    
    public void process(String originAppName, String emailAddress, String companyId, StatusType status, String operatorId, Date statusTimestamp) throws FtdNewsletterException {
        // Verify inputs.
        if ((originAppName == null) || (emailAddress == null) || (companyId == null) || (status == null)) {
            throw new FtdNewsletterException(
                "process: Inputs to process() can not be null.");
        }

        process(new NewsletterEventVO(emailAddress, companyId, status.toString(),
                operatorId, originAppName, new Long(statusTimestamp.getTime())), 0);
    }

    /**
    * The current jmsPubRetryCount is necessary to track the number of attempts
    * that have been made to recover from a network error.
    * Note that process() is a recursive method.
    * @param jmsPubRetryCount
    * @param neVO
    */
    protected void process(NewsletterEventVO neVO, int jmsPubRetryCount)
        throws FtdNewsletterException {
        QueueConnection queueConnection = null;
        QueueSession queueSession = null;
        QueueSender queueSender = null;

        try {
            try {
                // Retrieve a connection and a session on top of the connection
                queueConnection = _QUEUE_CONNECTION_FACTORY.createQueueConnection();
                queueSession = queueConnection.createQueueSession(true,
                        Session.AUTO_ACKNOWLEDGE);

                try {
                    // Create the publisher for any messages destined for the queue
                    queueSender = queueSession.createSender(_QUEUE);

                    // Send out the message
                    Message message = queueSession.createTextMessage(XmlService.marshallToXmlString(
                                com.ftd.newsletterevents.core.NewsletterEventVO.class,
                                neVO));
                    
                    // Set the origin app name within the JMS message, so that consumers can
                    // conveniently execute selector filters.
                    message.setStringProperty(IConstants.SELECTOR_KEY_ORIGIN_APP_NAME, neVO.getOriginAppName());

                    queueSender.send(message);
                } catch (Exception e) {
                    logger.error("process: JMS publish failed; attempting to verify if situation is recoverable.",
                        e);

                    if (ErrorService.isOracleRACException(e, queueSession)) {
                        // Failover logic.
                        if (jmsPubRetryCount < _JMS_PUB_RETRY_MAX_COUNT) {
                            // Wait the specified number of seconds for DB RAC to failover
                            // to a working instance.
                            try {
                                Thread.sleep(_JMS_PUB_RETRY_WAIT_INTERVAL * 1000);
                            } catch (InterruptedException ie) {
                                // continue
                            }

                            // Attempt to recreate the instance JMS vars
                            // and resubmit the message.
                            jmsPubRetryCount++;
                            logger.error("process: Attempt " +
                                jmsPubRetryCount +
                                " to recover from fatal network error...");
                            init();
                            process(neVO, jmsPubRetryCount);
                            logger.error("process: Attempt " +
                                jmsPubRetryCount +
                                " was SUCCESSFUL in recovering from fatal network error");
                        } else {
                            logger.error(
                                "process: Attempted unsuccessfully to recover " +
                                _JMS_PUB_RETRY_MAX_COUNT +
                                " times from fatal Oracle RAC error.");
                        }
                    } else {
                        // Simply pass the exception to the catch below.
                        throw e;
                    }
                }
            } catch (Throwable e) {
                logger.error("process: JMS publish failed in a manner that is programmatically unrecoverable.",
                    e);

                throw new FtdNewsletterException("process: Failed", e);
            }
        } finally {
            try {
                // Close publisher, session, and connection for queue
                if (queueSender != null) {
                    queueSender.close();
                }

                if (queueSession != null) {
                    queueSession.close();
                }

                if (queueConnection != null) {
                    queueConnection.close();
                }
            } catch (JMSException e) {
                logger.error("process: JMS clean-up logic failed.", e);
            }
        }
    }
}
