package com.ftd.newsletterevents.bo.ejb;
import javax.ejb.EJBLocalObject;
import com.ftd.newsletterevents.core.NewsletterEventVO;
import com.ftd.newsletterevents.exception.FtdNewsletterException;
import com.ftd.newsletterevents.vo.StatusType;
import java.util.Date;

public interface NewsletterPubEJBLocal extends EJBLocalObject 
{
  void process(String emailAddress, String companyId, StatusType status, String operatorId, Date statusTimestamp) throws FtdNewsletterException;

  void process(String originAppName, String emailAddress, String companyId, StatusType status, String operatorId, Date statusTimestamp) throws FtdNewsletterException;

}