package com.ftd.newsletterevents.bo.ejb;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface NewsletterPubEJBHome extends EJBHome 
{
  NewsletterPubEJB create() throws RemoteException, CreateException;
}