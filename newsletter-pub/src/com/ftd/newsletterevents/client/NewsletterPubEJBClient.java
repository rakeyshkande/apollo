package com.ftd.newsletterevents.client;

import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJB;
import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJBHome;
import com.ftd.newsletterevents.core.NewsletterEventVO;

import com.ftd.newsletterevents.vo.StatusType;
import java.util.Date;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.rmi.PortableRemoteObject;


public class NewsletterPubEJBClient {
    public static void main(String[] args) {
        NewsletterPubEJBClient newsletterPubEJBClient = new NewsletterPubEJBClient();

        try {
            Context context = getInitialContext();
            NewsletterPubEJBHome newsletterPubEJBHome = (NewsletterPubEJBHome) PortableRemoteObject.narrow(context.lookup(
                        "NewsletterPubEJB"), NewsletterPubEJBHome.class);
            NewsletterPubEJB newsletterPubEJB;

            // Use one of the create() methods below to create a new instance
            newsletterPubEJB = newsletterPubEJBHome.create();

            // Call any of the Remote methods below to access the EJB
            newsletterPubEJB.process("me@yahoo.com",
                    "FTD.com", StatusType.SUBSCRIBED, "OPS", new Date());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private static Context getInitialContext() throws NamingException {
        Hashtable env = new Hashtable();

        // Standalone OC4J connection details
        env.put(Context.INITIAL_CONTEXT_FACTORY,
            "com.evermind.server.rmi.RMIInitialContextFactory");
        env.put(Context.SECURITY_PRINCIPAL, "admin");
        env.put(Context.SECURITY_CREDENTIALS, "admin");
        env.put(Context.PROVIDER_URL, "ormi://FTDCOM4157/newsletter");

        return new InitialContext(env);
    }
}
