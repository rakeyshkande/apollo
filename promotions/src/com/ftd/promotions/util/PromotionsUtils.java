package com.ftd.promotions.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.ftd.promotions.common.PromotionsConstants;
import java.sql.Connection;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

public class PromotionsUtils
{

    private static Logger logger = new Logger("com.ftd.promotions.bo.PromotionsUtils");

    //DISCOUNT TYPES
    private static final String DISCOUNT_TYPE_MILES = "M";
    private static final String DISCOUNT_TYPE_DOLLARS = "D";
    private static final String DISCOUNT_TYPE_PERCENT = "P";

    //scale to which amounts are calculated
    private static final int AMOUNT_SCALE = 2;

    private static final BigDecimal hundreths = new BigDecimal(0.01);

    /**
    * Default constructor
    */

    public PromotionsUtils() throws Exception {
    }

    public static BigDecimal calculateDiscount(BigDecimal price, String discountType,
    		BigDecimal discountAmount) {
    	
    	logger.debug("calculateDiscount(" + price.toString() + ", " + discountType +
    			", " + discountAmount.toString() + ")");

    	BigDecimal discountedPrice = price;
    	BigDecimal amountOff = new BigDecimal("0");

        if (discountType.equals(DISCOUNT_TYPE_MILES)) {
            logger.debug("No discount for Miles/Points Discount Types");
        } else if (discountType.equals(DISCOUNT_TYPE_DOLLARS)) {
        	amountOff = discountAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
        } else if (discountType.equals(DISCOUNT_TYPE_PERCENT)) {
            amountOff = price.multiply(discountAmount).multiply(hundreths);
            amountOff = amountOff.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
        }
        discountedPrice = price.subtract(amountOff);
        discountedPrice = discountedPrice.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
        logger.debug("amountOff: " + amountOff.toString() + " discountedPrice: " + discountedPrice.toString());

    	return discountedPrice;
    }

    public static String formatAmount(String amount) throws Exception
    {
        DecimalFormat myFormatter = new DecimalFormat("#,###,##0.00");
        double doubleAmount = 0;
        String output = "0.00";
        
        try 
        {
            doubleAmount = Double.valueOf(amount.trim()).doubleValue();
            output = myFormatter.format(doubleAmount);
           
        } catch (NumberFormatException nfe) {
           
            // ignore. return initialized value.
            logger.error(nfe);
        } catch (IllegalArgumentException iae) {
            // ignore. return initialized value.
            logger.error(iae);
        }
        return output;
    }
    
	public static void sendPageSystemMessage(String logMessage)
    {
      Connection conn = null;
      try
      {
        conn = getNewConnection();
        String appSource = PromotionsConstants.SM_PAGE_SOURCE;
        String errorType = PromotionsConstants.SM_TYPE;
        String subject = PromotionsConstants.SM_PAGE_SUBJECT;
        int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(pageLevel);
        systemMessengerVO.setSource(appSource);
        systemMessengerVO.setType(errorType);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);

      }
      catch (Exception ex)
      {
        // Do not attempt to send system message it requires obtaining a
        // connection
        // and may end up in an infinite loop.
        logger.error(ex);
      }
      finally
      {
        if (conn != null)
        {
          try
          {
            conn.close();
          }
          catch (Exception e)
          {
            logger.error("Unable to close connection: " + e);
          }
        }
      }

    }

    public static void sendNoPageSystemMessage(String logMessage)
    {
      Connection conn = null;
      try
      {
        conn = getNewConnection();
        String appSource = PromotionsConstants.SM_NOPAGE_SOURCE;
        String errorType = PromotionsConstants.SM_TYPE;
        String subject = PromotionsConstants.SM_NOPAGE_SUBJECT;
        int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(pageLevel);
        systemMessengerVO.setSource(appSource);
        systemMessengerVO.setType(errorType);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);

      }
      catch (Exception ex)
      {
        // Do not attempt to send system message it requires obtaining a
        // connection
        // and may end up in an infinite loop.
        logger.error(ex);
      }
      finally
      {
        if (conn != null)
        {
          try
          {
            conn.close();
          }
          catch (Exception e)
          {
            logger.error("Unable to close connection: " + e);
          }
        }
      }

    }
    
    /**
     * Get a new database connection.
     * 
     * @return
     * @throws Exception
     */
    public static Connection getNewConnection() throws Exception
    {
      // get database connection
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      Connection conn = null;

      String datasource = "ORDER SCRUB";
      //configUtil.getPropertyNew(PromotionsConstants.PROPERTY_FILE, PromotionsConstants.DATASOURCE_NAME);
      conn = DataSourceUtil.getInstance().getConnection(datasource);

      return conn;
    }

}