package com.ftd.promotions.vo;

import java.util.ArrayList;
import java.util.List;


public class SourceCodeVO {
    
    private boolean iotwFlag = false;
    private String description;
    private String priceHeaderId = null;
    private List<PriceHeaderVO> priceHeaderList = new ArrayList<PriceHeaderVO>();
    private ProgramRewardVO programReward;
    private String sourceCode;
    private String autoPromotionEngine;
    private String apeProductCatalog;


    public void setPriceHeaderList(List<PriceHeaderVO> newpriceHeaderList) {
        this.priceHeaderList = newpriceHeaderList;
    }

    public List<PriceHeaderVO> getPriceHeaderList() {
        return priceHeaderList;
    }

    public void setPriceHeaderId(String newpriceHeaderId) {
        this.priceHeaderId = newpriceHeaderId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setProgramReward(ProgramRewardVO newprogramReward) {
        this.programReward = newprogramReward;
    }

    public ProgramRewardVO getProgramReward() {
        return programReward;
    }

    public void setIotwFlag(boolean newiotwFlag) {
        this.iotwFlag = newiotwFlag;
    }

    public boolean isIotwFlag() {
        return iotwFlag;
    }

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setAutoPromotionEngine(String autoPromotionEngine) {
		this.autoPromotionEngine = autoPromotionEngine;
	}

	public String getAutoPromotionEngine() {
		return autoPromotionEngine;
	}

	public void setApeProductCatalog(String apeProductCatalog) {
		this.apeProductCatalog = apeProductCatalog;
	}

	public String getApeProductCatalog() {
		return apeProductCatalog;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
