package com.ftd.promotions.vo;

import java.math.BigDecimal;

public class ProductVO
{
    private String productId;
    private BigDecimal standardPrice;
    
    public ProductVO()
    {
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductId()
    {
        return productId;
    }

	public void setStandardPrice(BigDecimal standardPrice) {
		this.standardPrice = standardPrice;
	}

	public BigDecimal getStandardPrice() {
		return standardPrice;
	}

}
