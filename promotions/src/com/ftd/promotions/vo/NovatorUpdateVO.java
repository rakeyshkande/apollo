package com.ftd.promotions.vo;

public class NovatorUpdateVO
{
    private boolean updateLive;
    private boolean updateContent;
    private boolean updateTest;
    private boolean updateUAT;
    
    public NovatorUpdateVO()
    {
    }

    public void setUpdateLive(boolean param)
    {
        this.updateLive = param;
    }

    public boolean isUpdateLive()
    {
        return updateLive;
    }

    public void setUpdateContent(boolean param)
    {
        this.updateContent = param;
    }

    public boolean isUpdateContent()
    {
        return updateContent;
    }

    public void setUpdateTest(boolean param)
    {
        this.updateTest = param;
    }

    public boolean isUpdateTest()
    {
        return updateTest;
    }

    public void setUpdateUAT(boolean param)
    {
        this.updateUAT = param;
    }

    public boolean isUpdateUAT()
    {
        return updateUAT;
    }
}
