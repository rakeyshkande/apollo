package com.ftd.promotions.vo;


public class PriceHeaderVO {
    
    private String priceHeaderId;
    private String minDollarAmt;
    private String maxDollarAmt;
    private String discountType;
    private String discountAmt;


    public void setPriceHeaderId(String newpriceHeaderId) {
        this.priceHeaderId = newpriceHeaderId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setMinDollarAmt(String newminDollarAmt) {
        this.minDollarAmt = newminDollarAmt;
    }

    public String getMinDollarAmt() {
        return minDollarAmt;
    }

    public void setMaxDollarAmt(String newmaxDollarAmt) {
        this.maxDollarAmt = newmaxDollarAmt;
    }

    public String getMaxDollarAmt() {
        return maxDollarAmt;
    }

    public void setDiscountType(String newdiscountType) {
        this.discountType = newdiscountType;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountAmt(String newdiscountAmt) {
        this.discountAmt = newdiscountAmt;
    }

    public String getDiscountAmt() {
        return discountAmt;
    }
}
