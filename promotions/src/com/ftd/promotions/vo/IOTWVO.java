package com.ftd.promotions.vo;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class IOTWVO {
    
    private String iotwId;
    private String sourceCode;
    private String productId;
    private ProductCheckVO productCheckVO;
    private Date startDate;
    private Date endDate;
    private String originalSourceCode;
    private String originalProductId;
    private Date originalStartDate;
    private Date originalEndDate;
    private String productPageMessage;
    private String calendarMessage;
    private String iotwSourceCode;
    private boolean specialOffer;
    private String wordingColor;
    private List discountOrProgram = new ArrayList();
    private List deliveryDiscountDates = new ArrayList();
    private String formattedDeliveryDiscountDates;
    private boolean productAvailable;
    private boolean status;
    private boolean deliveryDiscountDatesExist;
    protected String updatedBy;
    private String sortableSourceCode;
    private String createdBy;
 
    public String getFormattedDeliveryDiscountDates(){
        if(this.formattedDeliveryDiscountDates == null && getDeliveryDiscountDates() != null){
            IOTWDeliveryDateVO iotwDeliveryDateVO = null;
            StringBuffer dates = new StringBuffer();
            for(Iterator<IOTWDeliveryDateVO> it = getDeliveryDiscountDates().iterator(); it.hasNext();){
                iotwDeliveryDateVO = it.next();
                if(dates.length() > 0){
                    dates.append(", ");
                    dates.append(iotwDeliveryDateVO.getFormattedDiscountDate());
                } else {
                    dates.append(iotwDeliveryDateVO.getFormattedDiscountDate());
                }
            }
            setFormattedDeliveryDiscountDates(dates.toString());
        }
        return this.formattedDeliveryDiscountDates;
    }

    public void setFormattedDeliveryDiscountDates(String formattedDeliveryDiscountDates)
    {
        this.formattedDeliveryDiscountDates = formattedDeliveryDiscountDates;
    }

    public void setSourceCode(String sourceCode)
    {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductPageMessage(String productPageMessage)
    {
        this.productPageMessage = productPageMessage;
    }

    public String getProductPageMessage()
    {
        return productPageMessage;
    }

    public void setCalendarMessage(String calendarMessage)
    {
        this.calendarMessage = calendarMessage;
    }

    public String getCalendarMessage()
    {
        return calendarMessage;
    }

    public void setIotwSourceCode(String iotwSourceCode)
    {
        this.iotwSourceCode = iotwSourceCode;
    }

    public String getIotwSourceCode()
    {
        return iotwSourceCode;
    }

    public void setSpecialOffer(boolean specialOffer)
    {
        this.specialOffer = specialOffer;
    }

    public boolean isSpecialOffer()
    {
        return specialOffer;
    }

    public void setWordingColor(String wordingColor)
    {
        this.wordingColor = wordingColor;
    }

    public String getWordingColor()
    {
        return wordingColor;
    }

    public void setDeliveryDiscountDates(List deliveryDiscountDates)
    {
        this.deliveryDiscountDates = deliveryDiscountDates;
    }

    public List getDeliveryDiscountDates()
    {
        return deliveryDiscountDates;
    }

    public void setIotwId(String iotwId) {
        this.iotwId = iotwId;
    }

    public String getIotwId() {
        return iotwId;
    }


    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStartDate(Date newstartDate) {
        this.startDate = newstartDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date newendDate) {
        this.endDate = newendDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setProductAvailable(boolean newproductAvailable) {
        this.productAvailable = newproductAvailable;
    }

    public boolean isProductAvailable() {
        return productAvailable;
    }

    public void setDeliveryDiscountDatesExist(boolean newdeliveryDiscountDatesExist) {
        this.deliveryDiscountDatesExist = newdeliveryDiscountDatesExist;
    }

    public boolean isDeliveryDiscountDatesExist() {
        return deliveryDiscountDatesExist;
    }

    public void setOriginalSourceCode(String neworiginalSourceCode) {
        this.originalSourceCode = neworiginalSourceCode;
    }

    public String getOriginalSourceCode() {
        return originalSourceCode;
    }

    public void setOriginalProductId(String neworiginalProductId) {
        this.originalProductId = neworiginalProductId;
    }

    public String getOriginalProductId() {
        return originalProductId;
    }

    public void setOriginalStartDate(Date neworiginalStartDate) {
        this.originalStartDate = neworiginalStartDate;
    }

    public Date getOriginalStartDate() {
        return originalStartDate;
    }

    public void setOriginalEndDate(Date neworiginalEndDate) {
        this.originalEndDate = neworiginalEndDate;
    }

    public Date getOriginalEndDate() {
        return originalEndDate;
    }

    public void setUpdatedBy(String newupdatedBy) {
        this.updatedBy = newupdatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setDiscountOrProgram(List newdiscountOrProgram) {
        this.discountOrProgram = newdiscountOrProgram;
    }

    public List getDiscountOrProgram() {
        return discountOrProgram;
    }

    public void setProductCheckVO(ProductCheckVO newproductCheckVO) {
        this.productCheckVO = newproductCheckVO;
    }

    public ProductCheckVO getProductCheckVO() {
        return productCheckVO;
    }

    public void setSortableSourceCode(String sortableSourceCode) {
        this.sortableSourceCode = sortableSourceCode;
    }

    public String getSortableSourceCode() {
        return sortableSourceCode;
    }

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}
}
