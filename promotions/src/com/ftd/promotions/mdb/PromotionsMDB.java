package com.ftd.promotions.mdb;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.promotions.bo.IOTWBO;
import com.ftd.promotions.bo.PromotionsBO;

public class PromotionsMDB implements MessageDrivenBean, MessageListener 
{
	private static final long serialVersionUID = 1L;
	private MessageDrivenContext context;
	private Logger logger;
	private static String logContext = "com.ftd.promotions.mdb.PromotionsMDB";
  
	public void ejbCreate() {
		
	}

  public void onMessage(Message msg) {
      
    try {
      
        //get message and extract order detail_id
        TextMessage textMessage = (TextMessage)msg;
        String msgText = textMessage.getText();
        String action = msg.getJMSCorrelationID();
        logger.info("action: " + action);
        logger.info("msgText: " + msgText);
        
        if (action != null) {
        	if (action.equalsIgnoreCase("APE")) {
        		PromotionsBO pBO = new PromotionsBO();
        		pBO.processAPE();
        	}
        }
        
        if (action != null) {
        	if (action.equalsIgnoreCase("IOTW_FEED")) {
        		IOTWBO iotwBO = new IOTWBO();
        		iotwBO.sendAPEFeedToWebsite(msgText);
        	}
        }
        
        if (action != null) {
        	if (action.equalsIgnoreCase("IOTW_DELETE")) {
        		IOTWBO iotwBO = new IOTWBO();
        		iotwBO.removeAPEFeed(msgText);
        	}
        }
        
        logger.info("Finished");
    } catch (Exception e) { 
        logger.error(e);
    } catch (Throwable t) {
        logger.error("Thrown error: " + t);
    }
  }

  public void ejbRemove() 
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx) 
  {
      this.context = ctx;
      logger = new Logger(logContext);  
  }
}