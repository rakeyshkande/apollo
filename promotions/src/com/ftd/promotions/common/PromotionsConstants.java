package com.ftd.promotions.common;

public class PromotionsConstants
{

    public PromotionsConstants() throws Exception {
    }

    public final static String PROMOTIONS_CONTEXT = "PROMOTIONS_CONFIG";
    public final static String APE_DEFAULT_PRODUCT_PRICE = "default_product_price";
    
    public final static String APE_CREATED_BY = "APE";
    public final static String APE_IOTW_FEED_UPDATE = "IOTW_FEED";
    public final static String APE_IOTW_FEED_DELETE = "IOTW_DELETE";

    public static final String NOVATOR_CONFIG = "NOVATOR_CONFIG";
    public static final String DATASOURCE = "WOESDS";
    public static final String JOE_CONFIG_FILE = "oe_config.xml";
    
    public static final String NOVATOR_PRODUCTION_FEED_IP = "production_feed_ip";
    public static final String NOVATOR_PRODUCTION_FEED_PORT = "production_feed_port";
    public static final String NOVATOR_CONTENT_FEED_IP = "content_feed_ip";
    public static final String NOVATOR_CONTENT_FEED_PORT = "content_feed_port";
    public static final String NOVATOR_TEST_FEED_IP = "test_feed_ip";
    public static final String NOVATOR_TEST_FEED_PORT = "test_feed_port";
    public static final String NOVATOR_UAT_FEED_IP = "uat_feed_ip";
    public static final String NOVATOR_UAT_FEED_PORT = "uat_feed_port";
    public static final String NOVATOR_FEED_TIMEOUT = "feed_timeout";
    
    public static final String NOVATOR_CONTENT_FEED_CHECKED = "content_feed_checked";
    public static final String NOVATOR_PRODUCTION_FEED_CHECKED = "production_feed_checked";
    public static final String NOVATOR_TEST_FEED_CHECKED = "test_feed_checked";
    public static final String NOVATOR_UAT_FEED_CHECKED = "uat_feed_checked";
    // System messaging
    public static final String SM_PAGE_SOURCE = "APE_PAGE";
    public static final String SM_NOPAGE_SOURCE = "APE_NOPAGE";
    public static final String SM_PAGE_SUBJECT = "Automated Promotion Engine Message";
    public static final String SM_NOPAGE_SUBJECT = "NOPAGE Automated Promotion Engine Message";
    public static final String SM_TYPE = "System Exception";
}