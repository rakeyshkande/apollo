package com.ftd.promotions.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.promotions.vo.IOTWVO;
import com.ftd.promotions.vo.PriceHeaderVO;
import com.ftd.promotions.vo.ProductVO;
import com.ftd.promotions.vo.SourceCodeVO;

public class PromotionsDAO
{

    private Logger logger = new Logger("com.ftd.promotions.bo.PromotionsDAO");
    private Connection conn;

    public PromotionsDAO(Connection conn)
    {
        this.conn = conn;
    }

    public List getApeMasterSourceCodes() throws Exception {
    	logger.debug("getApeMasterSourceCodes()");
    	List sourceCodes = new ArrayList();
    	
    	try {
    	    DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_APE_MASTER_SOURCE_CODES");
	    
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
    	    while(rs.next()) {
	        	String sourceCode = rs.getString("source_code");
	        	sourceCodes.add(sourceCode);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	return sourceCodes;
    }
    
    public List getApeBaseSourceCodes(String masterSourceCode) throws Exception {
    	logger.debug("getApeBaseSourceCodes(" + masterSourceCode + ")");
    	List sourceCodes = new ArrayList();
    	
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_APE_BASE_SOURCE_CODES");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_MASTER_SOURCE_CODE", masterSourceCode);
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	    	    String sourceCode = rs.getString("base_source_code");
	    	    sourceCodes.add(sourceCode);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	return sourceCodes;
    }

	public void updateSourceCodePriceHeaderId(String sourceCode, String priceHeaderId) throws Exception
	{
	    logger.debug("updateSourceCodePriceHeaderId(" + 
	        sourceCode + ", " + priceHeaderId + ")");
	    
	    HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    inputParams.put("IN_PRICE_HEADER_ID", priceHeaderId);
	      
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("UPDATE_SOURCE_PRICE_HEADER");
	    dataRequest.setInputParams(inputParams);

	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	    String status = (String) outputs.get("OUT_STATUS");
	    if(status != null && status.equalsIgnoreCase("N")) {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	    }
	}

	public SourceCodeVO getSourceCodeDetails(String sourceCode) throws Exception
	{
    	logger.debug("getSourceCodeDetails(" + sourceCode + ")");
		SourceCodeVO sourceVO = null;
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_SOURCE_MASTER_BY_ID");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_SOURCE_CODE", sourceCode);
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	            sourceVO = new SourceCodeVO();
	            sourceVO.setSourceCode(rs.getString("source_code"));
	            sourceVO.setPriceHeaderId(rs.getString("price_header_id"));
	            sourceVO.setAutoPromotionEngine(rs.getString("auto_promotion_engine"));
	            sourceVO.setApeProductCatalog(rs.getString("ape_product_catalog"));
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
		
		return sourceVO;
	}

	public PriceHeaderVO getPriceHeaderDetails(String priceHeaderId, BigDecimal price) throws Exception
	{
		logger.debug("getPriceHeaderDetails(" + priceHeaderId + ", " + price.toString() + ")");
		PriceHeaderVO phdVO = null;
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_PRICE_HEADER_DETAILS");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_PRICE_HEADER_ID", priceHeaderId);
	        inputParams.put("IN_PRICE_AMT", new Double(price.doubleValue()));
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	            phdVO = new PriceHeaderVO();
	            phdVO.setPriceHeaderId(priceHeaderId);
	            phdVO.setMinDollarAmt(rs.getString("min_dollar_amt"));
	            phdVO.setMaxDollarAmt(rs.getString("max_dollar_amt"));
	            phdVO.setDiscountType(rs.getString("discount_type"));
	            phdVO.setDiscountAmt(rs.getString("discount_amt"));
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
		
		return phdVO;
	}
	
	public Map getActiveIOTWBySourceCode(String sourceCode) throws Exception {
		logger.debug("getIOTWBySourceCode(" + sourceCode + ")");
		
		HashMap iotwMap = new HashMap();
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_ACTIVE_IOTW_BY_SC");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_SOURCE_CODE", sourceCode);
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	        	IOTWVO iotwVO = new IOTWVO();
	        	iotwVO.setIotwId(rs.getString("iotw_id"));
	    	    String productId = rs.getString("product_id");
	    	    iotwVO.setProductId(productId);
	    	    iotwVO.setIotwSourceCode(rs.getString("iotw_source_code"));
	    	    iotwVO.setCreatedBy(rs.getString("created_by"));
	    	    iotwMap.put(productId, iotwVO);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
		
		return iotwMap;
	}

    public Map getActiveCatalogProducts(String productCatalog) throws Exception {
    	logger.debug("getActiveCatalogProducts(" + productCatalog + ")");
    	HashMap products = new HashMap();
    	
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_ACTIVE_CATALOG_PRODUCTS");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_PRODUCT_CATALOG", productCatalog);
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	    	    String productId = rs.getString("product_id");
	    	    BigDecimal standardPrice = rs.getBigDecimal("standard_price");
	    	    ProductVO prodVO = new ProductVO();
	    	    prodVO.setProductId(productId);
	    	    prodVO.setStandardPrice(standardPrice);
	    	    products.put(productId, prodVO);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	return products;
    }

    public IOTWVO getIotw(String productId, String sourceCode) throws Exception {
    	IOTWVO iotwVO = null;
    	
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_JOE_IOTW");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_SOURCE_CODE", sourceCode);
	        inputParams.put("IN_PRODUCT_ID", productId);
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	        	iotwVO = new IOTWVO();
	        	iotwVO.setIotwId(rs.getString("iotw_id"));
	    	    iotwVO.setProductId(productId);
	    	    iotwVO.setIotwSourceCode(rs.getString("iotw_source_code"));
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	
    	return iotwVO;
    }

	public SourceCodeVO getApeIOTWSourceCode(String masterSourceCode, String priceHeaderId) throws Exception
	{
    	logger.debug("getApeIOTWSourceCode(" + masterSourceCode + ", " +
    			priceHeaderId + ")");
		SourceCodeVO sourceVO = null;
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_APE_IOTW_SOURCE_CODE");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_MASTER_SOURCE_CODE", masterSourceCode);
	        inputParams.put("IN_PRICE_HEADER_ID", priceHeaderId);
	        dataRequest.setInputParams(inputParams);
	      
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	            sourceVO = new SourceCodeVO();
	            sourceVO.setSourceCode(rs.getString("source_code"));
	            sourceVO.setDescription(rs.getString("description"));
	            sourceVO.setIotwFlag(rs.getString("iotw_flag").equalsIgnoreCase("Y") ? true : false);
	            sourceVO.setPriceHeaderId(rs.getString("price_header_id"));
	            sourceVO.setAutoPromotionEngine(rs.getString("auto_promotion_engine"));
	            sourceVO.setApeProductCatalog(rs.getString("ape_product_catalog"));
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
		
		return sourceVO;
	}

	public String createSourceCode(String masterSourceCode, String priceHeaderId) throws Exception
	{
	    logger.debug("createSourceCode(" + masterSourceCode +
	    		", " + priceHeaderId + ")");
	    
	    String newSourceCode = null;
	    HashMap<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_MASTER_SOURCE_CODE", masterSourceCode);
	    inputParams.put("IN_PRICE_HEADER_ID", priceHeaderId);
	      
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.conn);
	    dataRequest.setStatementID("CREATE_SOURCE_CODE");
	    dataRequest.setInputParams(inputParams);

	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	    
	    newSourceCode = (String) outputs.get("OUT_SOURCE_CODE");
	    
	    String status = (String) outputs.get("OUT_STATUS");
	    if(status != null && status.equalsIgnoreCase("N")) {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	    }
	    
	    return newSourceCode;
	}

    public List getIotwWithApeDisabled() throws Exception {
    	logger.debug("getIotwWithApeDisabled()");
    	List iotwIds = new ArrayList();
    	
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_IOTW_WITH_APE_DISABLED");
	    
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	    	    String iotwId = rs.getString("iotw_id");
	    	    iotwIds.add(iotwId);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	return iotwIds;
    }

    public List getExpiredIotw() throws Exception {
    	logger.debug("getExpiredIotw()");
    	List iotwIds = new ArrayList();
    	
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_EXPIRED_IOTW");
	    
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	    	    String iotwId = rs.getString("iotw_id");
	    	    iotwIds.add(iotwId);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	return iotwIds;
    }

    public List getFutureIOTW(String sourceCode, String productId, Date endDate) throws Exception {
    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    	String displayDate = endDate == null ? "null" : sdf.format(endDate);
    	logger.debug("getFutureIOTW(" + sourceCode + ", " + productId +
    	        ", " + displayDate + ")");
    	List iotwList = new ArrayList();
    	try {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(this.conn);
	        dataRequest.setStatementID("GET_FUTURE_IOTW");
	    
	        HashMap inputParams = new HashMap();
	        inputParams.put("IN_SOURCE_CODE", sourceCode);
	        inputParams.put("IN_PRODUCT_ID", productId);
	        inputParams.put("IN_END_DATE", endDate == null ? null : new java.sql.Date(endDate.getTime()));
	        dataRequest.setInputParams(inputParams);

	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	    
	        while(rs.next()) {
	        	IOTWVO iotwVO = new IOTWVO();
	        	iotwVO.setIotwId(rs.getString("iotw_id"));
	    	    iotwVO.setProductId(productId);
	    	    iotwVO.setStartDate(rs.getDate("start_date"));
	    	    iotwVO.setEndDate(rs.getDate("end_date"));
	    	    iotwList.add(iotwVO);
	        }
    	} catch (Exception e) {
    		logger.error(e);
    	}
   		return iotwList;
    }
}