package com.ftd.promotions.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class FeedBO {

    private static Logger logger = new Logger("com.ftd.oe.bo.FeedBO");


    public FeedBO() {
    }

    /**
     * Evaluate Novator response
     * @param novatorResponse
     * @return
     */
    public void parseResponse(String novatorResponse) throws Exception
    {
        if(logger.isDebugEnabled())
            logger.debug("Novator response: " + novatorResponse);
        
        
        // Sometimes the response comes back with CON on the end.  
        // Remove CON if present
        if(novatorResponse.endsWith("CON"))
        {
            novatorResponse = novatorResponse.substring(0,(novatorResponse.length() - 3));
        }

        try
        {
            Document doc = JAXPUtil.parseDocument(novatorResponse);

            // Attempt to obtain update node
            Element updateElement = JAXPUtil.selectSingleNode(doc, "//update");
            if(updateElement == null)
                updateElement = JAXPUtil.selectSingleNode(doc, "//create");
            if(updateElement == null)
                updateElement = JAXPUtil.selectSingleNode(doc, "//remove");
            

            if (updateElement == null )         
            {
                if(logger.isDebugEnabled())
                    logger.debug("No update element exists within the response.");

                // update was not found...look for a validation error
                Element errorElement = JAXPUtil.selectSingleNode(doc, "//validationError");
                if (errorElement ==  null )  
                {
                    // No validation error found...don't know what this message means
                    throw new Exception("Unable to parse the following Novator response: " + novatorResponse);
                }
                else
                {
                    // Obtain the validation error description
                    String description = "";
                    Element errorDescriptionElement = JAXPUtil.selectSingleNode(doc, "//validationError/error/description");
                    if(errorDescriptionElement != null)
                    {
                        description = JAXPUtil.getTextValue(errorDescriptionElement);
                    }
                    throw new Exception("Novator validation error: " + description);
                }           
            }    
            else 
            {
                // Obtaint the status of the update
                String status = "";
                Element statusElement = JAXPUtil.selectSingleNode(doc, "//update/status");
                if(statusElement == null)
                    statusElement = JAXPUtil.selectSingleNode(doc, "//create/status");
                if(statusElement == null)
                    statusElement = JAXPUtil.selectSingleNode(doc, "//remove/status");
                
                if(statusElement != null)
                    status = JAXPUtil.getTextValue(statusElement);

                if (status.equals("success"))
                {            
                    // Do nothing
                }
                else
                {
                    throw new Exception("Novator feed was unsuccessful: " + status);
                }
            }        
        }//end try        
        catch (Throwable t)           
        {
            // Could not parse Novator response or an error occurred           
            logger.warn(t);
            throw new Exception("Novator feed error.  " + t.getMessage());
        }
    }
}
