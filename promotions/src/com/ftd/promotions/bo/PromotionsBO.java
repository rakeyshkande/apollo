package com.ftd.promotions.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.promotions.dao.IOTWDAO;
import com.ftd.promotions.dao.PromotionsDAO;
import com.ftd.promotions.util.PromotionsUtils;
import com.ftd.promotions.vo.IOTWDeliveryDateVO;
import com.ftd.promotions.vo.IOTWVO;
import com.ftd.promotions.vo.PriceHeaderVO;
import com.ftd.promotions.vo.ProductCheckVO;
import com.ftd.promotions.vo.ProductVO;
import com.ftd.promotions.vo.SourceCodeVO;
import com.ftd.promotions.common.PromotionsConstants;

public class PromotionsBO
{

    private Logger logger = new Logger("com.ftd.promotions.bo.PromotionsBO");

    /**
    * Default constructor
    */

    public PromotionsBO() throws Exception {
    }

    public void processAPE() throws Exception {
    	logger.debug("processAPE()");
    	
    	Connection conn = null;
    	try {
			logger.debug("Getting default product price");
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
	        String defaultProductPrice = cu.getFrpGlobalParm(PromotionsConstants.PROMOTIONS_CONTEXT,
        		PromotionsConstants.APE_DEFAULT_PRODUCT_PRICE);
	        BigDecimal defaultPrice = new BigDecimal(defaultProductPrice);
	        logger.debug("defaultPrice: " + defaultPrice);

			conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			PromotionsDAO pDAO = new PromotionsDAO(conn);
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
	        logger.debug("Delete all APE IOTW records for Source Codes with APE disabled");
	        List iotwIds = pDAO.getIotwWithApeDisabled();
	        for (int b=0; b<iotwIds.size(); b++) {
	        	String iotwId = (String) iotwIds.get(b);
	        	logger.debug("Deleting iotwId: " + iotwId);
	        	dispatchJMSMessage(iotwId, PromotionsConstants.APE_IOTW_FEED_DELETE, "3");
	        }

	        // Delete all expired IOTW records, even if they were not created by APE
	        logger.debug("Delete all expired IOTW records");
	        iotwIds = pDAO.getExpiredIotw();
	        for (int b=0; b<iotwIds.size(); b++) {
	        	String iotwId = (String) iotwIds.get(b);
	        	logger.debug("Deleting iotwId: " + iotwId);
	        	dispatchJMSMessage(iotwId, PromotionsConstants.APE_IOTW_FEED_DELETE, "3");
	        }

			List masterSourceCodes = pDAO.getApeMasterSourceCodes();
			for (int a=0; a<masterSourceCodes.size(); a++) {
				String masterSourceCode = (String) masterSourceCodes.get(a);
				logger.debug("masterSourceCode: " + masterSourceCode);

		        //CacheUtil cacheUtil = CacheUtil.getInstance();
		        //SourceMasterVO smVO = cacheUtil.getSourceCodeById(sourceCode);
		        //PriceHeaderDetailVO phdVO = cacheUtil.getPriceHeaderDetail(smVO.getPriceHeaderId(), defaultPrice);
		        SourceCodeVO masterSourceVO = pDAO.getSourceCodeDetails(masterSourceCode);
		        PriceHeaderVO phdVO = pDAO.getPriceHeaderDetails(masterSourceVO.getPriceHeaderId(), defaultPrice);
		        BigDecimal discountAmount = new BigDecimal(phdVO.getDiscountAmt());
		        
		        BigDecimal discountedMasterPrice = PromotionsUtils.calculateDiscount(defaultPrice,
		        		phdVO.getDiscountType(), discountAmount);
		        logger.debug("discountedMasterPrice: " + discountedMasterPrice);
		        BigDecimal lowestPrice = defaultPrice;
		        String lowestPriceHeaderId = null;

		        List baseSourceCodes = pDAO.getApeBaseSourceCodes(masterSourceCode);
				for (int b=0; b<baseSourceCodes.size(); b++) {
					String baseSourceCode = (String) baseSourceCodes.get(b);
					logger.debug("baseSourceCode: " + baseSourceCode);

					SourceCodeVO baseSourceVO = pDAO.getSourceCodeDetails(baseSourceCode);
			        if (baseSourceVO != null) {
    			        phdVO = pDAO.getPriceHeaderDetails(baseSourceVO.getPriceHeaderId(), defaultPrice);
    			        discountAmount = new BigDecimal(phdVO.getDiscountAmt());
			        
	    		        BigDecimal discountedBasePrice = PromotionsUtils.calculateDiscount(defaultPrice,
			        		phdVO.getDiscountType(), discountAmount);
		    	        logger.debug("discountedBasePrice: " + discountedBasePrice);
			        
			            if (discountedBasePrice.compareTo(lowestPrice) < 0) {
			            	logger.debug("New lowest price: " + baseSourceVO.getPriceHeaderId());
			            	lowestPrice = discountedBasePrice;
			        	    lowestPriceHeaderId = baseSourceVO.getPriceHeaderId();
			            } else if (lowestPriceHeaderId == null) {
			            	logger.debug("Defaulting lowestPriceHeaderId: " + baseSourceVO.getPriceHeaderId());
			            	lowestPriceHeaderId = baseSourceVO.getPriceHeaderId();
			            }
			        }
                }
				if (discountedMasterPrice.compareTo(lowestPrice) != 0) {
					logger.debug("Setting price header id to " + lowestPriceHeaderId);
					pDAO.updateSourceCodePriceHeaderId(masterSourceCode, lowestPriceHeaderId);
					discountedMasterPrice = lowestPrice;
					masterSourceVO.setPriceHeaderId(lowestPriceHeaderId);
				}
				
				// get all existing IOTW records for this source code
				Map iotwMap = pDAO.getActiveIOTWBySourceCode(masterSourceCode);
		        Iterator scIterator = iotwMap.keySet().iterator();
		        while (scIterator.hasNext()) {
		        	String productId = (String) scIterator.next();
					IOTWVO iotwVO = (IOTWVO) iotwMap.get(productId);
		    	    logger.debug("productId: " + productId + " " + iotwVO.getIotwSourceCode() + 
		    	    		" " + iotwVO.getCreatedBy());
				}
		        
		        Map catalogProducts = pDAO.getActiveCatalogProducts(masterSourceVO.getApeProductCatalog());
		        Iterator prodIterator = catalogProducts.keySet().iterator();
		        while (prodIterator.hasNext()) {
		        	String productId = (String) prodIterator.next();
		        	ProductVO prodVO = (ProductVO) catalogProducts.get(productId);
		        	BigDecimal productPrice = prodVO.getStandardPrice();
		        	BigDecimal existingMasterPrice = productPrice;
		        	BigDecimal lowestBasePrice = productPrice;
		        	lowestPriceHeaderId = null;
		        	logger.debug("*** catalog productId: " + productId + " " + productPrice.toString());

			        phdVO = pDAO.getPriceHeaderDetails(masterSourceVO.getPriceHeaderId(), productPrice);
			        discountAmount = new BigDecimal(phdVO.getDiscountAmt());
		        
    		        BigDecimal discountedProductPrice = PromotionsUtils.calculateDiscount(productPrice,
		        		phdVO.getDiscountType(), discountAmount);
	    	        logger.debug("discountedProductPrice: " + discountedProductPrice);
		        
		            if (discountedProductPrice.compareTo(existingMasterPrice) < 0) {
		            	logger.debug("Discounted price is lower: " + masterSourceVO.getPriceHeaderId());
		            	existingMasterPrice = discountedProductPrice;
		            }

		        	String iotwCreatedBy = null;
		        	String masterIOTWId = null;
		        	String baseIOTWId = null;
		        	
		        	IOTWVO iotwVO = (IOTWVO) iotwMap.get(productId);
		        	if (iotwVO != null) {
		        		iotwCreatedBy = iotwVO.getCreatedBy();
		        		masterIOTWId = iotwVO.getIotwId();
		        	}
		        	logger.debug("iotwCreatedBy: " + iotwCreatedBy);
	        		if (iotwCreatedBy != null && !iotwCreatedBy.equalsIgnoreCase(PromotionsConstants.APE_CREATED_BY)) {
	        			logger.debug("IOTW not created by APE, skipping");
	        			iotwMap.remove(productId);
	        		} else {
	        			if (iotwVO != null) {
							SourceCodeVO iotwSourceVO = pDAO.getSourceCodeDetails(iotwVO.getIotwSourceCode());
					        if (iotwSourceVO != null) {
		    			        phdVO = pDAO.getPriceHeaderDetails(iotwSourceVO.getPriceHeaderId(), productPrice);
		    			        discountAmount = new BigDecimal(phdVO.getDiscountAmt());
					        
			    		        BigDecimal existingIOTWPrice = PromotionsUtils.calculateDiscount(productPrice,
					        		phdVO.getDiscountType(), discountAmount);
				    	        logger.debug("existingIOTWPrice: " + existingIOTWPrice);
					        
					            if (existingIOTWPrice.compareTo(existingMasterPrice) < 0) {
					            	logger.debug("Existing IOTW price is lower: " + existingIOTWPrice.toString() + 
					            			" " + iotwSourceVO.getPriceHeaderId());
					            	existingMasterPrice = existingIOTWPrice;
					            }
					        }
	        			}
	    				for (int b=0; b<baseSourceCodes.size(); b++) {
	    					String baseSourceCode = (String) baseSourceCodes.get(b);
	    					logger.debug("baseSourceCode: " + baseSourceCode);

	    					iotwVO = pDAO.getIotw(productId, baseSourceCode);
		        			if (iotwVO != null) {
		        				logger.debug("found IOTW: " + iotwVO.getIotwSourceCode());
								SourceCodeVO iotwSourceVO = pDAO.getSourceCodeDetails(iotwVO.getIotwSourceCode());
						        if (iotwSourceVO != null) {
    	        			        phdVO = pDAO.getPriceHeaderDetails(iotwSourceVO.getPriceHeaderId(), productPrice);
	            			        discountAmount = new BigDecimal(phdVO.getDiscountAmt());
	    			        
	        	    		        BigDecimal discountedIotwPrice = PromotionsUtils.calculateDiscount(productPrice,
	    	    		        		phdVO.getDiscountType(), discountAmount);
	    		        	        logger.debug("discountedIotwPrice: " + discountedIotwPrice);
	    			        
	    		        	        if (baseIOTWId == null || (discountedIotwPrice.compareTo(lowestBasePrice) < 0)) {
	    			                	logger.debug("IOTW price: " + discountedIotwPrice.toString() +
	    			                			" " + iotwSourceVO.getPriceHeaderId());
	    			                	lowestBasePrice = discountedIotwPrice;
	    			        	        lowestPriceHeaderId = iotwSourceVO.getPriceHeaderId();
	    			        	        baseIOTWId = iotwVO.getIotwId();
	    			                }
						        }
	    			        }
	    				}
	    				logger.debug("existingMasterPrice: " + existingMasterPrice +
	    						" lowestBasePrice: " + lowestBasePrice +
	    						" baseIOTWId: " + baseIOTWId);
	    				if (baseIOTWId != null) {
	    					logger.debug("");
	    				    logger.debug("Base Source Codes have an IOTW");
	    				    logger.debug("masterIOTWId: " + masterIOTWId);
	    				    logger.debug("baseIOTWId: " + baseIOTWId);
	    					logger.debug("");
	    					if (existingMasterPrice.compareTo(lowestBasePrice) != 0) {
	    						logger.debug("Prices are different");
	    						// Was an APE source code already created for this Master Source Code / Price Header Id combination?
	    						SourceCodeVO newSourceVO = pDAO.getApeIOTWSourceCode(masterSourceCode, lowestPriceHeaderId);
	    						// If not, create one
	    						if (newSourceVO == null) {
	    							String newSourceCode = pDAO.createSourceCode(masterSourceCode, lowestPriceHeaderId);
	    							newSourceVO = new SourceCodeVO();
	    						    newSourceVO.setSourceCode(newSourceCode);
	    						}
	    						logger.debug("newSourceVO.getSourceCode(): " + newSourceVO.getSourceCode());
	    						IOTWDAO iotwDAO = new IOTWDAO();
	    						if (masterIOTWId != null) {
	    							logger.debug("Deleting previous IOTW record: " + masterIOTWId);
	                                dispatchJMSMessage(masterIOTWId, PromotionsConstants.APE_IOTW_FEED_DELETE, "3");
	    						}
	    						logger.debug("Creating new IOTW record");
	    						// Get the details for the existing IOTW record
	    						List<IOTWVO> iotwList = iotwDAO.getIOTW(conn, baseIOTWId, null, null, null, false);
	    						IOTWVO newIOTWVO = iotwList.get(0);
	    						newIOTWVO.setIotwId(null);
	    						newIOTWVO.setSourceCode(masterSourceCode);
	    						newIOTWVO.setIotwSourceCode(newSourceVO.getSourceCode());
	    						newIOTWVO.setUpdatedBy(PromotionsConstants.APE_CREATED_BY);
	    	                    ProductCheckVO productCheckVO = iotwDAO.getProductId(conn, productId);
	    	                    if(productCheckVO.isProduct() || productCheckVO.isUpsell()){
	    	                        newIOTWVO.setProductCheckVO(productCheckVO);
	    	                        newIOTWVO.setProductId(productCheckVO.getNormalizedId());
	    	                    }
	    	                    // check for a future IOTW and change the end date if found
	    	                    List futureIOTW = pDAO.getFutureIOTW(masterSourceCode, productId, newIOTWVO.getEndDate());
	    	                    for (int b=0; b<futureIOTW.size(); b++) {
	    	                    	IOTWVO futureVO = (IOTWVO) futureIOTW.get(b);
	    	                    	logger.debug("future: " + sdf.format(futureVO.getStartDate()) +
	    	                    			sdf.format(futureVO.getEndDate()));
	    	                    	Date newEndDate = futureVO.getStartDate();
	    	                    	Calendar cal = Calendar.getInstance();
	    	                    	cal.setTime(newEndDate);
	    	                    	cal.add(Calendar.DATE, -1);
	    	                    	newEndDate = cal.getTime();
	    	                    	logger.debug("newEndDate:" + sdf.format(newEndDate));
	    	                    	newIOTWVO.setEndDate(newEndDate);
	    	                    	break;
	    	                    }

							    String insertedIOTWId = iotwDAO.insert(conn, newIOTWVO);
	    						logger.debug("Inserted iotwId: " + insertedIOTWId);
	    						for (int c=0; c<newIOTWVO.getDeliveryDiscountDates().size(); c++) {
	    							IOTWDeliveryDateVO iddVO = (IOTWDeliveryDateVO) newIOTWVO.getDeliveryDiscountDates().get(c);
	    							iddVO.setIotwId(insertedIOTWId);
	    							iddVO.setUpdatedBy(PromotionsConstants.APE_CREATED_BY);
	    							iotwDAO.insertDeliveryDiscountDate(conn, iddVO);
	    						}
	    						// Create a JMS message for the website feed
                                dispatchJMSMessage(insertedIOTWId, PromotionsConstants.APE_IOTW_FEED_UPDATE, "30");

	    	                }
	    					iotwMap.remove(productId);
	    				}
	        		}
		        }
		        logger.debug("Remaining IOTW to be deleted");
		        // Delete any remaining IOTW records created by APE
		        scIterator = iotwMap.keySet().iterator();
		        while (scIterator.hasNext()) {
		        	String productId = (String) scIterator.next();
					IOTWVO iotwVO = (IOTWVO) iotwMap.get(productId);
		    	    logger.debug("productId: " + productId + " " + iotwVO.getIotwSourceCode() + 
		    	    		" " + iotwVO.getCreatedBy());
		    	    if (iotwVO.getCreatedBy() != null && iotwVO.getCreatedBy().equalsIgnoreCase(PromotionsConstants.APE_CREATED_BY)) {
		    	    	dispatchJMSMessage(iotwVO.getIotwId(), PromotionsConstants.APE_IOTW_FEED_DELETE, "3");
		    	    }
				}
		        
			}

    	} catch (Exception e) {			
			e.printStackTrace();
			logger.error("processAPE() Exception: " + e);
		} finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    logger.error("Unable to close connection: " + e);
                }
            }
        }

    }
    
    public void dispatchJMSMessage(String iotwId, String corrId, String delay) throws Exception
    {
        InitialContext context = new InitialContext();

        MessageToken token = null;
        token = new MessageToken();
        token.setStatus("Promotions");
        token.setJMSCorrelationID(corrId);
        token.setMessage(iotwId);
        token.setProperty("JMS_OracleDelay", String.valueOf(delay), "int");

        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);

        logger.debug("Message sent");
    }

}