package com.ftd.queue.tests;

import com.ftd.queue.bo.QueueMenuBO;
import com.ftd.queue.bo.QueueRequestBO;
import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.vo.QueueRequestVO;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.GregorianCalendar;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import junit.framework.*;
import junit.extensions.*;

import org.apache.struts.action.ActionForward;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLText;
import oracle.xml.parser.v2.XSLException;

import org.xml.sax.SAXException;

//public class TestIt extends TestCase
public class TestIt
{
    private Logger logger = new Logger("com.ftd.queue.tests.TestIt");

    private static final String PROPERTY_FILE = "test_it_config.xml";
    
    Connection conn = null;

    // get security manager instance
    ConfigurationUtil cu = null;

    String pgmToRun  = "";


    public TestIt()
    {
    }

    public static void main(String args[])
    {
        TestIt ti = new TestIt();

        ti.getDBConn();

        ti.runIt();

    }


    private void runIt()
    {
        getPgmToRun();
        
        if(pgmToRun.equalsIgnoreCase("QueueMenu"))
        {
            executeQueueMenu();
        }
        else if(pgmToRun.equalsIgnoreCase("QueueRequest"))
        {
            executeQueueRequest();
        }
/*
        else if(pgmToRun.equalsIgnoreCase("QueueTag"))
        {
            executeQueueTag();
        }
        else if(pgmToRun.equalsIgnoreCase("QueueItemRequest"))
        {
            executeQueueItemRequest();
        }
*/
        else
        {
            System.out.println("No action requested");
        }
    }


    private void getPgmToRun()
    {
        try
        {
            // get security manager instance
            cu = ConfigurationUtil.getInstance();
            pgmToRun = cu.getProperty(PROPERTY_FILE,"PGM_TO_RUN");
        }
        catch (IOException ioe)
        {
            logger.error(ioe);
        }
        catch (ParserConfigurationException  pcex)
        {
            logger.error(pcex);
        }
        catch (SAXException  sec)
        {
            logger.error(sec);
        }
        catch (TransformerException  tex)
        {
            logger.error(tex);
        }
        catch (XSLException  xex)
        {
            logger.error(xex);
        }
    }


    private void getDBConn()
    {
        try
        {
//##########################
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV5";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            conn = DriverManager.getConnection(database_, user_, password_);
//##########################
/*
            conn = DataSourceUtil.getInstance().getConnection(
                        ConfigurationUtil.getInstance().getProperty(QueueConstants.PROPERTY_FILE,
                                                                    QueueConstants.DATASOURCE_NAME));
*/
        }
        catch (ClassNotFoundException  cnfe)
        {
            logger.error(cnfe);
        }
        catch (SQLException  se)
        {
            logger.error(se);
        }
    }


    private void executeQueueMenu()
    {
        try
        {
            QueueMenuBO qmBO = new QueueMenuBO(conn);           // get queue menu tokens
//protected method in a different package            qmBO.setCsrId("1");        // local test csrId value
        
//            Document queue = qmBO.getMenuItems(request.getParameter(QueueConstants.COMMON_PARM_CONTEXT),
//                            request.getParameter(QueueConstants.COMMON_PARM_SEC_TOKEN));

System.out.println("start time: " + new GregorianCalendar().getTime());
            Document queue = qmBO.getMenuItems("context","securitytoken");
System.out.println("end time: " + new GregorianCalendar().getTime());
            ((XMLDocument) queue).print(System.out);
        }
        catch (IOException ioe)
        {
            logger.error(ioe);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (ParserConfigurationException  pcex)
        {
            logger.error(pcex);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (SAXException  sec)
        {
            logger.error(sec);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (TransformerException  tex)
        {
            logger.error(tex);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (XSLException  xex)
        {
            logger.error(xex);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (Exception e)
        {
            logger.error(e);
            ActionForward forward = new ActionForward("errorPage");
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
                //forward = new ActionForward(SecurityUtil.getErrorPage());
                ActionForward forward = new ActionForward("errorPage");
            }
        }
    }

    private void executeQueueRequest()
    {
        try
        {
            QueueRequestBO qrBO = new QueueRequestBO(conn);         // get queue menu tokens
            QueueRequestVO qrVO = new QueueRequestVO();             // get queue

            // get security manager instance
            ConfigurationUtil cu = ConfigurationUtil.getInstance();

            String queueType  = cu.getProperty(PROPERTY_FILE,"QUEUE_TYPE");

            String queueDesc  = cu.getProperty(PROPERTY_FILE,"QUEUE_DESC");            
            logger.debug("QueueDesc = " + queueDesc);
    
            String queueInd   = cu.getProperty(PROPERTY_FILE,"QUEUE_IND");
            logger.debug("QueueInd = " + queueInd);
    
            String sTagged    = cu.getProperty(PROPERTY_FILE,"TAGGED");
            logger.debug("Tagged = " + sTagged);
            boolean tagged    = sTagged.equalsIgnoreCase("true") ? true : false;
    
            String sAttEmail  = cu.getProperty(PROPERTY_FILE,"ATT_EMAIL");
            logger.debug("AttachedEMail = " + sAttEmail);
            boolean attEmail  = sAttEmail.equalsIgnoreCase("true") ? true : false;

            String user       = cu.getProperty(PROPERTY_FILE,"USER");
            logger.debug("CsrId = " + user);
    
            String groupId    = cu.getProperty(PROPERTY_FILE,"GROUP");
            logger.debug("Group = " + groupId);
    
            String sortColumn  = cu.getProperty(PROPERTY_FILE,"SORT_COLUMN");
            logger.debug("SortColumn = " + sortColumn);
        
            XMLDocument xmlDoc = null;
            Node sortOrderStateNode = null;
            String sortOrderState  = cu.getProperty(PROPERTY_FILE,"SORT_ORDER_STATE");
            if(sortOrderState != null  &&  sortOrderState.trim().length() > 0)
            {
                xmlDoc = (XMLDocument) DOMUtil.getDocument(sortOrderState);
                    ((XMLDocument) xmlDoc).print(System.out);
                sortOrderStateNode = xmlDoc.getFirstChild();
                logger.debug("SortOrderState = " + sortOrderState);
            }

    		String sSortRequested  = cu.getProperty(PROPERTY_FILE,"SORT_REQUESTED");
            boolean sortRequested  = false;
            if(sSortRequested != null  &&  sSortRequested.trim().length() > 0)
            {
                sortRequested  = sSortRequested.equalsIgnoreCase("true") ? true : false;
            }
            logger.debug("SortRequested = " + sSortRequested);

            String position   = cu.getProperty(PROPERTY_FILE,"POSITION");
            logger.debug("Position = " + position);

            // 2. save elements in queue request VO
            qrVO.setQueueType(queueType);
            qrVO.setTagged(tagged);
            qrVO.setAttEmail(attEmail);
            qrVO.setSortColumn(sortColumn);
            
            if(sortOrderState != null  &&  sortOrderState.trim().length() > 0)
            {
                qrVO.setSortOrderState(sortOrderStateNode);
            }
            
            qrVO.setSortRequested(sortRequested);
            qrVO.setPosition(position);
            qrVO.setUser(user);
            qrVO.setGroup(groupId);
            qrVO.setQueueDescription(queueDesc);
            qrVO.setQueueInd(queueInd);

System.out.println("start time: " + new GregorianCalendar().getTime());
            Document queue = qrBO.getQueueData(qrVO);
            
        XMLDocument queueXMLDoc = (XMLDocument) DOMUtil.getDefaultDocument();
        Element queueEle = queueXMLDoc.createElement("queue_request");

        Element newEle = queueXMLDoc.createElement("queue_type");
        Text newText = queueXMLDoc.createTextNode(qrVO.getQueueType());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);
                
        newEle = queueXMLDoc.createElement("queue_ind");
        newText = queueXMLDoc.createTextNode(qrVO.getQueueInd());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);
                
        newEle = queueXMLDoc.createElement("tagged");
        newText = queueXMLDoc.createTextNode(new Boolean(qrVO.getTagged()).toString());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);
                
        newEle = queueXMLDoc.createElement("attached");
        newText = queueXMLDoc.createTextNode(new Boolean(qrVO.getAttEmail()).toString());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);
                
        newEle = queueXMLDoc.createElement("user");
        if(qrVO.getUser() != null)
        {
            newText = queueXMLDoc.createTextNode(qrVO.getUser());
            newEle.appendChild(newText);
        }
        queueEle.appendChild(newEle);
        
        newEle = queueXMLDoc.createElement("group");
        if(qrVO.getGroupId() != null)
        {
            newText = queueXMLDoc.createTextNode(qrVO.getGroupId());
            newEle.appendChild(newText);
        }
        queueEle.appendChild(newEle);

        queue.getDocumentElement().appendChild(queueEle);
            
System.out.println("end time: " + new GregorianCalendar().getTime());
            ((XMLDocument) queue).print(System.out);
        }
        catch (IOException ioe)
        {
            logger.error(ioe);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (ParserConfigurationException  pcex)
        {
            logger.error(pcex);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (SAXException  sec)
        {
            logger.error(sec);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (TransformerException  tex)
        {
            logger.error(tex);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (XSLException  xex)
        {
            logger.error(xex);
            ActionForward forward = new ActionForward("errorPage");
        }
        catch (Exception e)
        {
            logger.error(e);
            ActionForward forward = new ActionForward("errorPage");
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
                //forward = new ActionForward(SecurityUtil.getErrorPage());
                ActionForward forward = new ActionForward("errorPage");
            }
        }
    }

}