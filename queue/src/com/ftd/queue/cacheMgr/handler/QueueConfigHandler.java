package com.ftd.queue.cacheMgr.handler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This class retrieves queue configuration information from database
 * configuration tables.
 *
 */
public class QueueConfigHandler extends CacheHandlerBase
{
    private Logger logger = new Logger(QueueConfigHandler.class.getName());

    private static final String COUNT_IND = "COUNT_INDICATOR";
    private static final String QUEUE_CONFIG = "QUEUE_CONFIG";
    private Map queueConfigMap = null;
	    
    /**
     * load queue configuration data
     * 
     * if queue configuration information is not returned, throw cache exception.
     * 
     * @param Connection      - database connection
     * @return Object         - map of configuration information
     * @throws CacheException - cachecontroller error
     */
    public Object load(Connection conn) throws CacheException {
        ResultSet queueConfigContextRS = null;
        try
        {
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.reset();
            request.setConnection(conn);
            HashMap inputParams = new HashMap();
            inputParams.put("CONTEXT_ID", QUEUE_CONFIG);
            request.setInputParams(inputParams);
            request.setStatementID("GET_QUEUE_CONFIG_DATA");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

            queueConfigMap = new HashMap();

            queueConfigContextRS = (ResultSet) dau.execute(request);
            while (queueConfigContextRS.next())      // get queue config context parms
            {
                // object(1) = "QUEUE_CONFIG" - we are requesting the config parms for this
                // object(2) = queue config parm name
                // object(3) = queue config parm value
                queueConfigMap.put((String) queueConfigContextRS.getObject(2), (String) queueConfigContextRS.getObject(3));
            }

            if (queueConfigMap.size()==0)
            {
                logger.error("Queue Configuration Information NOT returned from the database");
                throw new CacheException("Queue Configuration Information NOT returned from the database");
            }
        }
        catch (IOException ioe)
        {
            logger.error(ioe);
            throw new CacheException("IOException", ioe);
        }
        catch (ParserConfigurationException pce)
        {
            logger.error(pce);
            throw new CacheException("ParserConfigurationException", pce);
        }
        catch (SAXException saxe)
        {
            logger.error(saxe);
            throw new CacheException("SAXEException", saxe);
        }
        catch (SQLException sqle)
        {
            logger.error(sqle);
            throw new CacheException("SQLException", sqle);
        }
        catch (Exception e)
        {
            logger.error(e);
            throw new CacheException("Exception " +  e.toString());
        }
        finally
        {
            try
            {
        if(queueConfigContextRS != null)
        {
                  queueConfigContextRS.getStatement().close();
        }
            }
            catch (SQLException  sqle)
            {
                logger.error(sqle);
                throw new CacheException("SQLException", sqle);
            }
        }

        return (Object) queueConfigMap;
    }


    /**
     * set cached object
     * 
     * @param Object  - cached object
     * @return void
     * @throws CacheException
     */
      public void setCachedObject(Object cachedObject) throws CacheException
      {
    	  queueConfigMap = (Map) cachedObject; 
      }
      
      /**
       * get count indicator
       *   - default count indicator to no ("N")
       * 
       * @param none
       * @return boolean - identifies whether queue counts are to be included
       * @throws none
       */
      public boolean getCountIndicator()
      {
          String s = "N";                              //default to no ("N")

          logger.debug("QueueConfigHandler - queueConfigMap count = " + queueConfigMap.size());
          if((String) queueConfigMap.get(COUNT_IND) != null)
          {
              s = (String) queueConfigMap.get(COUNT_IND);
          }
          return new Boolean(s).booleanValue();
      }

}
