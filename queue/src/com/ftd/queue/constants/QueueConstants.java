package com.ftd.queue.constants;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.queue.util.BasePageBuilder;


/**
 * Static information for queues
 *
 * @author Matt Wilcoxen
 */

public class QueueConstants
{
  private Logger logger = new Logger("com.ftd.queue.constants.QueueConstants");

  //config:  file names
  public static final String PROPERTY_FILE = "queue_config.xml";
  public static final String QUEUE_CONFIG_HANDLER = "CACHE_NAME_QUEUE_CONFIG";
  public static final String QUEUE_CONFIG_CONTEXT = "QUEUE_CONFIG";

  //application:  outside urls
  public static final String CONS_MAIN_MENU_URL = "MAIN_MENU_URL";

  //application:  actions
  public static final String ACTN_ERROR = "Error";

  //application:
  public static final String APP_CONTEXT = "applicationcontext";
  public static final String APP_PARAMETERS = "parameters";

  //application: Entity Timer
  public static final String TIMER_COMMENT_ORIGIN_TYPE = "t_comment_origin_type";
  public static final String TIMER_ENTITY_HISTORY_ID = "t_entity_history_id";
  public static final String TIMER_COMMENT_ORIGIN = "t_comment_origin";
  public static final String TIMER_QUEUE_ENTITY_TYPE = "ORDER_DETAILS";
  public static final String TIMER_QUEUE_COMMENT_ORIGIN_TYPE = "QUEUE";

  //application: Other parameters that should be passed
  public static final String START_ORIGIN = "start_origin";

  //database connection:  data source name
  public static final String DATASOURCE_NAME = "CLEANDS";

  //security:  indicates if we are running locally or in an integrated environment
  public static final String SECURITY_IS_ON = "SECURITY_IS_ON";

  //security:  authorization data
  public static final String PERMISSION_VIEW = "View";
  public static final String PERMISSION_UPDATE = "Update";
  public static final String SEC_CONTEXT = "context";
  public static final String SEC_TOKEN = "securitytoken";

  //data filter data params
  public static final String RTQ_ATTACHED = "rtq_attached";
  public static final String RTQ_CURRENT_SORT_DIRECTION = "rtq_current_sort_direction";
  public static final String RTQ_FLAG = "rtq";
  public static final String RTQ_GROUP = "rtq_group";
  public static final String RTQ_MAX_RECORDS = "rtq_max_records";
  public static final String RTQ_POSITION = "rtq_position";
  public static final String RTQ_SORT_COLUMN = "rtq_sort_column";
  public static final String RTQ_SORT_ON_RETURN = "rtq_sort_on_return";
  public static final String RTQ_QUEUE_IND = "rtq_queue_ind";
  public static final String RTQ_QUEUE_TYPE = "rtq_queue_type";
  public static final String RTQ_TAGGED = "rtq_tagged";
  public static final String RTQ_USER = "rtq_user";
  public static final String RTQ_RECEIVED_DATE = "rtq_received_date";
  public static final String RTQ_DELIVERY_DATE = "rtq_delivery_date";
  public static final String RTQ_TAG_DATE = "rtq_tag_date";
  public static final String RTQ_LAST_TOUCHED = "rtq_last_touched";
  public static final String RTQ_TYPE = "rtq_type";
  public static final String RTQ_SYS = "rtq_sys";
  public static final String RTQ_TIMEZONE = "rtq_timezone";
  public static final String RTQ_SDG = "rtq_sdg";
  public static final String RTQ_DISPOSITION = "rtq_disposition";
  public static final String RTQ_PRIORITY = "rtq_priority";
  public static final String RTQ_NEW_ITEM = "rtq_new_item";
  public static final String RTQ_OCCASION = "rtq_occasion";
  public static final String RTQ_LOCATION = "rtq_location";
  public static final String RTQ_MEMBER_NUMBER = "rtq_member_num";
  public static final String RTQ_ZIP = "rtq_zip";
  public static final String RTQ_TAG_BY = "rtq_tag_by";
  public static final String RTQ_FILTER = "rtq_filter";


  //security:  queue resources requiring authorization for user access
  public static final String ADJ_SECURITY_RESOURCE = "ADJ";
  public static final String CREDIT_SECURITY_RESOURCE = "CREDIT";
  public static final String CHARGE_SECURITY_RESOURCE = "CHARGE";
  public static final String DEN_SECURITY_RESOURCE = "DEN";
  public static final String LP_SECURITY_RESOURCE = "LP";
  public static final String RECON_SECURITY_RESOURCE = "RECON";
  public static final String ZIP_SECURITY_RESOURCE = "ZIP";
  public static final String ARIBA_SECURITY_RESOURCE = "ARIBA";
  public static final String WALMART_SECURITY_RESOURCE = "WALMART";

  //queue indicators:
  public static final String ORDER_QUEUE = "Order";
  public static final String EMAIL_QUEUE = "Email";
  public static final String USER_QUEUE = "User";
  public static final String GROUP_QUEUE = "Group";
  public static final String MERCURY_QUEUE = "Mercury";

  //queue types:
  public static final String MERCURY_ADJ_QUEUE = "ADJ";
  public static final String MERCURY_ANS_QUEUE = "ANS";
  public static final String MERCURY_ASK_QUEUE = "ASK";
  public static final String MERCURY_CAN_QUEUE = "CAN";
  public static final String MERCURY_CHARGE_QUEUE = "CHARGE";
  public static final String MERCURY_CON_QUEUE = "CON";
  public static final String MERCURY_CREDIT_QUEUE = "CREDIT";
  public static final String MERCURY_DEN_QUEUE = "DEN";
  public static final String MERCURY_FTD_QUEUE = "FTD";
  public static final String MERCURY_GEN_QUEUE = "GEN";
  public static final String MERCURY_LP_QUEUE = "LP";
  public static final String MERCURY_RECON_QUEUE = "RECON";
  public static final String MERCURY_REJ_QUEUE = "REJ";
  public static final String MERCURY_ZIP_QUEUE = "ZIP";
  public static final String MERCURY_ARIBA_QUEUE = "ARIBA";
  public static final String MERCURY_WALMART_QUEUE = "WALMART";
  public static final String EMAIL_OA_QUEUE = "OA";
  public static final String EMAIL_CX_QUEUE = "CX";
  public static final String EMAIL_MO_QUEUE = "MO";
  public static final String EMAIL_ND_QUEUE = "ND";
  public static final String EMAIL_QI_QUEUE = "QI";
  public static final String EMAIL_PD_QUEUE = "PD";
  public static final String EMAIL_BD_QUEUE = "BD";
  public static final String EMAIL_DI_QUEUE = "DI";
  public static final String EMAIL_QC_QUEUE = "QC";
  public static final String EMAIL_OT_QUEUE = "OT";
  public static final String EMAIL_SE_QUEUE = "SE";
  public static final String EMAIL_AMZN_QUEUE = "AMZN";
  public static final String DC_QUEUE = "DC";
  public static final String EMAIL_MRCNT_QUEUE = "MRCNT";

  public static final String EMAIL_SUGGESTION_QUEUE = "SE";
  public static final String EMAIL_ORDER_ACKNOWLEDGEMENT_QUEUE = "OA";
  public static final String EMAIL_ARIBA_QUEUE = "ARIBA";
  public static final String EMAIL_WALMART_QUEUE = "WALMART";

  //config file parms:
  public final static String SORT_ORDER_STATE = "SORT_ORDER_STATE";
  public final static String POSITION = "POSITION";
  public final static String MAX_RECORDS = "MAX_RECORDS";

  //global parms
  public static final String CONTEXT_QUEUE_CONFIG = "QUEUE_CONFIG";
  public static final String NAME_EXCLUDE_PARTNERS = "EXCLUDE_PARTNERS";

  //database:  stored procedure names - QUEUE_PKG
  public final static String DB_GET_QUEUE_MENU_DATA = "GET_QUEUE_MENU_DATA";
  public final static String DB_INSERT_QUEUE_TAG = "INSERT_QUEUE_TAG";

  //database:  stored procedure names - ORDER_QUERY_PKG
  public final static String DB_IS_LP_IND_HOLD = "IS_LP_IND_HOLD";

  //database:  stored procedure names - CUSTOMER_QUERY_PKG
  public final static String DB_GET_CUSTOMER_BY_ORDER = "GET_CUSTOMER_BY_ORDER";

  //database:  GET_QUEUE_MENU_DATA db stored procedure input parms
  public final static String DB_INDICATOR = "INDICATOR";
  //also uses CSR_ID

  //database:  GET_QUEUE db stored procedure input parms
  public final static String DB_CSR_ID = "CSR_ID";

  //database:  INSERT_QUEUE_TAG db stored procedure input parms
  public static final String DB_MESSAGE_ID = "MESSAGE_ID";
  public static final String DB_SEC_CONTEXT = "SEC_CONTEXT";
  public static final String DB_SEC_PERMISSION = "SEC_PERMISSION";

  //database:  IS_LP_IND_HOLD db stored procedure input parms
  public static final String DB_ORDER_GUID = "ORDER_GUID";

  //database:  GET_CUSTOMER_BY_ORDER db stored procedure input parms
  public static final String DB_ORDER_DETAIL_ID = "ORDER_DETAIL_ID";

  // XML
  public static final String FILTER_TYPE = "filter_type";
  public static final String FILTER_SYS = "filter_sys";
  public static final String FILTER_TIMEZONE = "filter_timezone";
  public static final String FILTER_SDG = "filter_sdg";
  public static final String FILTER_DISPOSITION = "filter_disposition";
  public static final String FILTER_PRIORITY = "filter_priority";
  public static final String FILTER_NEW_ITEM = "filter_new_item";
  public static final String FILTER_OCCASION = "filter_occasion";
  public static final String FILTER_LOCATION = "filter_location";
  public static final String FILTER_TYPE_XML = "TYPE_XML";
  public static final String FILTER_SYS_XML = "SYS_XML";
  public static final String FILTER_TIMEZONE_XML = "TIMEZONE_XML";
  public static final String FILTER_SDG_XML = "SDG_XML";
  public static final String FILTER_DISPOSITION_XML = "DISPOSITION_XML";
  public static final String FILTER_PRIORITY_XML = "PRIORITY_XML";
  public static final String FILTER_NEW_ITEM_XML = "NEW_ITEM_XML";
  public static final String FILTER_OCCASION_XML = "OCCASION_XML";
  public static final String FILTER_LOCATION_XML = "LOCATION_XML";
  public static final String NO_FILTER = "X";
}
