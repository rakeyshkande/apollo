package com.ftd.queue.dao;

import com.ftd.queue.constants.QueueConstants;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This data access object handles Queue Tag requests.
 *
 * @author Matt Wilcoxen
 */

public class QueueTagDAO
{
    private Logger logger = new Logger("com.ftd.queue.dao.QueueTagDAO");

    //database
    private DataRequest request;
    private Connection conn;

    /**
     * constructor
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public QueueTagDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * get queue data items
    *     1. build db stored procedure input parms
    *     2. get db data
    * 
    * @param String   - identifies message to be tagged to user
    * @param String   - customer service representative id
    * @param String   - security context
    * @param String   - security permission
    *
    * @return HashMap - of output parameters
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public Document insertTag(String msgId, String csrId, String sec_context, String sec_permission)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();

        // 1. build db stored procedure input parameters
        logger.debug("insertTag(): \n" +
            "input parameters to stored procedure " + QueueConstants.DB_INSERT_QUEUE_TAG + " are: \n" +
            "message_id = " + msgId + "\n" +
            "csr_id = " + csrId + "\n" +
            "context = " + sec_context + "\n" +
            "sec_permission = " + sec_permission);

        inputParams.put(QueueConstants.DB_MESSAGE_ID, new Long(msgId));
        inputParams.put(QueueConstants.DB_CSR_ID, csrId);
        inputParams.put(QueueConstants.DB_SEC_CONTEXT, sec_context);
        inputParams.put(QueueConstants.DB_SEC_PERMISSION, sec_permission);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(QueueConstants.DB_INSERT_QUEUE_TAG);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        Document taggedItems = (Document) dau.execute(request);

        return taggedItems;
    }

}