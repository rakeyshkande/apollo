package com.ftd.queue.dao;

import com.ftd.queue.constants.QueueConstants;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This data access object class retrieves Queue Menu information.
 *
 * @author Matt Wilcoxen
 */

public class QueueMenuDAO
{
    private Logger logger = new Logger("com.ftd.queue.dao.QueueMenuDAO");

    //database
    private DataRequest request;
    private Connection conn;

    /**
     * constructor
     * 
     * @param Connection
     * @return n/a
     * @throws none
     */
    public QueueMenuDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * get queue menu items
    * 
    * @param boolean    - identifies whether queue counts are to be displayed
    * @param String     - customer service rep identifier
    * @return HashMap   - of output parameters
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public HashMap getQueueMenuData(boolean countIndicator, String csrId) 
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap outParametersMap = new HashMap();

        HashMap inputParams = new HashMap();

        //db stored proc must have count indicator = "Y" or "N"
        String sCountIndicator = countIndicator ? "Y" : "N";
        inputParams.put(QueueConstants.DB_INDICATOR, sCountIndicator);
        inputParams.put(QueueConstants.DB_CSR_ID, csrId);

        // build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(QueueConstants.DB_GET_QUEUE_MENU_DATA);

        // get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        outParametersMap = (HashMap) dau.execute(request);

        return outParametersMap;
    }
}