package com.ftd.queue.dao;

import com.ftd.queue.constants.QueueConstants;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This data access object looks up a customer id given an order detail id.
 *
 * @author Matt Wilcoxen
 */

public class CustomerDAO
{
    private Logger logger = new Logger("com.ftd.queue.dao.CustomerDAO");

    //database
    private DataRequest request;
    private Connection conn;

    /**
     * constructor
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public CustomerDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * find customer id
    *     1. build db stored procedure input parms
    *     2. get db data
    * 
    * @param String        - order detail id
    * @return long         - customer id
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public long findCustomerId(String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();

        // 1. build db stored procedure input parameters
        inputParams.put(QueueConstants.DB_ORDER_DETAIL_ID, new Long(orderDetailId));

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(QueueConstants.DB_GET_CUSTOMER_BY_ORDER);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();

        long customerId = ((BigDecimal) dau.execute(request)).longValue();
        return customerId;
    }

}