package com.ftd.queue.dao;

import com.ftd.queue.constants.QueueConstants;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This data access object retrieves an indicator identifying whether an order is on hold
 * due to loss prevention.
 *
 * @author Matt Wilcoxen
 */

public class OrderDAO
{
    private Logger logger = new Logger("com.ftd.queue.dao.OrderDAO");

    //database
    private DataRequest request;
    private Connection conn;

    /**
     * constructor
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public OrderDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * determine if order is on hold due to loss prevention
    *     1. build db stored procedure input parms
    *     2. get db data
    * 
    * @param String        - order GUI Id
    * @param String        - order detail Id
    * @return boolean      - indicates if order is on hold for loss prevention
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public boolean isOnHoldForLossPreventionInd(String orderGuid, String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();

        // 1. build db stored procedure input parameters
        inputParams.put(QueueConstants.DB_ORDER_GUID, orderGuid);
        inputParams.put(QueueConstants.DB_ORDER_DETAIL_ID, orderDetailId);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(QueueConstants.DB_IS_LP_IND_HOLD);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();

        String sOnHoldForLossPrevention = (String) dau.execute(request);
        boolean onHoldForLossPrevention = sOnHoldForLossPrevention.equalsIgnoreCase("Y") ? true : false;
            
        return onHoldForLossPrevention;
    }

}