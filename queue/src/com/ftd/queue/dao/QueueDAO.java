package com.ftd.queue.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.PreferredPartnerUtility;
import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.vo.QueueRequestVO;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
* This data access object class retrieves Queue information.
*
* @author Matt Wilcoxen
*/

public class QueueDAO
{
  private static Logger logger = new Logger("com.ftd.queue.dao.QueueDAO");

  //database
  private DataRequest request;
  private Connection conn;
  private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");


  /**
* constructor
* @param conn - database connection
* @return n/a
* @throws
*/
  public QueueDAO(Connection conn)
  {
    super();
    this.conn = conn;
  }


  /**
* get queue data items
* 1. build db stored procedure input parms
* 2. get db data
*
* @param qrVO - used for input parameters to db stored procedure
* @param itemStartNum - return records beginning at specified record #
* @return HashMap - of output parameters
* @throws IOException
* @throws ParserConfigurationException
* @throws SAXException
* @throws SQLException
*/
  public HashMap getQueueData(QueueRequestVO qrVO, int itemStartNum)
    throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    HashMap outParametersMap = new HashMap();
    HashMap inputParams = new HashMap();
    logger.info("queueType: " + qrVO.getQueueType());
    
    HashMap partnerAccess = getPreferredPartnersWithAccess(qrVO.getSessionId());
    boolean isPreferredQueueWithAccess = false;
    if (qrVO.getQueueType() != null && partnerAccess != null) {
        String accessAllowed = (String) partnerAccess.get(qrVO.getQueueType());
        if (accessAllowed != null && accessAllowed.equalsIgnoreCase("Y")) {
            isPreferredQueueWithAccess = true;
        }
    }
    logger.info("isPreferredQueueWithAccess: " + isPreferredQueueWithAccess);

    // 1. build db stored procedure input parameters
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      inputParams.put("IN_ORIGIN_ID", qrVO.getQueueType());
      logger.debug("IN_ORIGIN_ID = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else if ( isPreferredQueueWithAccess )
    {
      inputParams.put("IN_PARTNER_NAME", qrVO.getQueueType());
      logger.debug("IN_PARTNER_NAME = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else
    {
      inputParams.put("IN_QUEUE_TYPE", qrVO.getQueueType());
      logger.debug("IN_QUEUE_TYPE = " + qrVO.getQueueType());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());

      inputParams.put("IN_CONTEXT", qrVO.getContext());
      logger.debug("IN_CONTEXT = " + qrVO.getContext());

      inputParams.put("IN_SESSION_ID", qrVO.getSessionId());
      logger.debug("IN_SESSION_ID = " + qrVO.getSessionId());
    }

    String sTagged = getTaggedInd(qrVO.getQueueInd(), qrVO.getTagged());
    inputParams.put("IN_TAGGED_INDICATOR", sTagged);
    logger.debug("IN_TAGGED_INDICATOR = " + sTagged);

    String sAttEmail = getAttEmailInd(qrVO.getQueueInd(), qrVO.getAttEmail());
    inputParams.put("IN_ATTACHED_EMAIL_INDICATOR", sAttEmail);
    logger.debug("IN_ATTACHED_EMAIL_INDICATOR = " + sAttEmail);

    inputParams.put("IN_SORT_ORDER", qrVO.getSortColumn());
    logger.debug("IN_SORT_ORDER = " + qrVO.getSortColumn());

    inputParams.put("IN_BEG_POS", new Long(new Integer(itemStartNum).longValue()));
    logger.debug("IN_BEG_POS = " + new Long(new Integer(itemStartNum).longValue()));

    inputParams.put("IN_MAX_NO_RECS", new Long(qrVO.getMaxRecords()));
    logger.debug("IN_MAX_NO_RECS = " + new Long(qrVO.getMaxRecords()));

    //determine if this is an ariba or walmart or premier collection or preferred partner queue type
    //if it is, CSR and GROUP ID are not needed
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1)            || 
        (qrVO.getQueueType().indexOf("ARIBA") != -1)              || 
        (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1) || 
        (isPreferredQueueWithAccess))
    {
      logger.debug("CSR_ID not used for Walmart or Ariba or Premier Collection or Preferred Partner orders " + qrVO.getUser());
      logger.debug("GROUP_ID not used for Walmart or Ariba or Premier Collection or Preferred Partner orders " + qrVO.getGroupId());
    }
    else
    {
      inputParams.put("IN_CSR_ID", qrVO.getUser());
      logger.debug("IN_CSR_ID = " + qrVO.getUser());

      inputParams.put("IN_GROUP_ID", qrVO.getGroupId());
      logger.debug("IN_GROUP_ID = " + qrVO.getGroupId());
    }

    inputParams.put("IN_SORT_DIRECTION", qrVO.getNewSortDirection());
    logger.debug("IN_SORT_DIRECTION = " + qrVO.getNewSortDirection());


    //build DataRequest object
    request = new DataRequest();
    request.reset();
    request.setConnection(conn);
    request.setInputParams(inputParams);

    //determine if this is an ariba or walmart queue type
    //if it is, use the specific stored proc to return all queue types
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      request.setStatementID("GET_QUEUE_WALMART_ARIBA");
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      request.setStatementID("GET_QUEUE_PRM_COL");
    }
    else if ( isPreferredQueueWithAccess )
    {
      request.setStatementID("GET_QUEUE_PARTNER");
    }
    else
    {
      request.setStatementID("GET_QUEUE");
    }

    Date beforeDate = new Date();
    logger.debug("before calling the stored proc = " + sdf.format(beforeDate));

    logger.debug("statementID = " + request.getStatementID());

    // 2. get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    outParametersMap = (HashMap) dau.execute(request);

    Date afterDate = new Date();
    logger.debug("after calling the stored proc  = " + sdf.format(afterDate));

    return outParametersMap;
  }


  /**
* get queue data items
* 1. build db stored procedure input parms
* 2. get db data
*
* @param qrVO - used for input parameters to db stored procedure
* @param itemStartNum - return records beginning at specified record #
* @return HashMap - of output parameters
* @throws IOException
* @throws ParserConfigurationException
* @throws SAXException
* @throws SQLException
*/
  public HashMap getQueueDataFiltered(QueueRequestVO qrVO, int itemStartNum)
    throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    HashMap outParametersMap = new HashMap();

    HashMap inputParams = new HashMap();

    HashMap partnerAccess = getPreferredPartnersWithAccess(qrVO.getSessionId());
    boolean isPreferredQueueWithAccess = false;
    if (qrVO.getQueueType() != null && partnerAccess != null) {
        String accessAllowed = (String) partnerAccess.get(qrVO.getQueueType());
        if (accessAllowed != null && accessAllowed.equalsIgnoreCase("Y")) {
            isPreferredQueueWithAccess = true;
        }
    }
    logger.info("isPreferredQueueWithAccess: " + isPreferredQueueWithAccess);

    // 1. build db stored procedure input parameters
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      inputParams.put("IN_ORIGIN_ID", qrVO.getQueueType());
      logger.debug("IN_ORIGIN_ID = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else if ( isPreferredQueueWithAccess )
    {
      inputParams.put("IN_PARTNER_NAME", qrVO.getQueueType());
      logger.debug("IN_PARTNER_NAME = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else
    {
      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());

      inputParams.put("IN_CONTEXT", qrVO.getContext());
      logger.debug("IN_CONTEXT = " + qrVO.getContext());

      inputParams.put("IN_SESSION_ID", qrVO.getSessionId());
      logger.debug("IN_SESSION_ID = " + qrVO.getSessionId());
    }

    inputParams.put("IN_QUEUE_TYPE", qrVO.getQueueType());
    logger.debug("IN_QUEUE_TYPE = " + qrVO.getQueueType());

    String sTagged = getTaggedInd(qrVO.getQueueInd(), qrVO.getTagged());
    inputParams.put("IN_TAGGED_INDICATOR", sTagged);
    logger.debug("IN_TAGGED_INDICATOR = " + sTagged);

    String sAttEmail = getAttEmailInd(qrVO.getQueueInd(), qrVO.getAttEmail());
    inputParams.put("IN_ATTACHED_EMAIL_INDICATOR", sAttEmail);
    logger.debug("IN_ATTACHED_EMAIL_INDICATOR = " + sAttEmail);

    inputParams.put("IN_SORT_ORDER", qrVO.getSortColumn());
    logger.debug("IN_SORT_ORDER = " + qrVO.getSortColumn());

    inputParams.put("IN_BEG_POS", new Long(new Integer(itemStartNum).longValue()));
    logger.debug("IN_BEG_POS = " + new Long(new Integer(itemStartNum).longValue()));

    inputParams.put("IN_MAX_NO_RECS", new Long(qrVO.getMaxRecords()));
    logger.debug("IN_MAX_NO_RECS = " + new Long(qrVO.getMaxRecords()));

    //determine if this is an ariba or walmart or premier collection or preferred partner queue type
    //if it is, CSR and GROUP ID are not needed
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1)            || 
        (qrVO.getQueueType().indexOf("ARIBA") != -1)              || 
        (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1) || 
        (isPreferredQueueWithAccess))
    {
      logger.debug("CSR_ID not used for Walmart or Ariba or Premier Collection or Preferred Partner orders " + qrVO.getUser());
      logger.debug("GROUP_ID not used for Walmart or Ariba or Premier Collection or Preferred Partner orders " + qrVO.getGroupId());
    }
    else
    {
      inputParams.put("IN_CSR_ID", qrVO.getUser());
      logger.debug("IN_CSR_ID = " + qrVO.getUser());

      inputParams.put("IN_GROUP_ID", qrVO.getGroupId());
      logger.debug("IN_GROUP_ID = " + qrVO.getGroupId());
    }

    inputParams.put("IN_SORT_DIRECTION", qrVO.getNewSortDirection());
    logger.debug("IN_SORT_DIRECTION = " + qrVO.getNewSortDirection());

    inputParams.put("IN_MESSAGE_TIMESTAMP", qrVO.getMessageTimestampFilter());
    logger.debug("IN_MESSAGE_TIMESTAMP = " + qrVO.getMessageTimestampFilter());

    inputParams.put("IN_DELIVERY_DATE", qrVO.getDeliveryDateFilter());
    logger.debug("IN_DELIVERY_DATE = " + qrVO.getDeliveryDateFilter());

    inputParams.put("IN_TAGGED_ON", qrVO.getTaggedOnFilter());
    logger.debug("IN_TAGGED_ON = " + qrVO.getTaggedOnFilter());

    inputParams.put("IN_LAST_TOUCHED", qrVO.getLastTouchedFilter());
    logger.debug("IN_LAST_TOUCHED = " + qrVO.getLastTouchedFilter());

    inputParams.put("IN_MESSAGE_TYPE", qrVO.getMessageTypeFilter());
    logger.debug("IN_MESSAGE_TYPE = " + qrVO.getMessageTypeFilter());

    inputParams.put("IN_SYSTEM", qrVO.getSystemFilter());
    logger.debug("IN_SYSTEM = " + qrVO.getSystemFilter());

    inputParams.put("IN_TIMEZONE", qrVO.getTimezoneFilter());
    logger.debug("IN_TIMEZONE = " + qrVO.getTimezoneFilter());

    inputParams.put("IN_SAME_DAY_GIFT", qrVO.getSameDayGiftFilter());
    logger.debug("IN_SAME_DAY_GIFT = " + qrVO.getSameDayGiftFilter());

    inputParams.put("IN_TAG_DISPOSITION", qrVO.getTagDispositionFilter());
    logger.debug("IN_TAG_DISPOSITION = " + qrVO.getTagDispositionFilter());

    inputParams.put("IN_TAG_PRIORITY", qrVO.getTagPriorityFilter());
    logger.debug("IN_TAG_PRIORITY = " + qrVO.getTagPriorityFilter());

    inputParams.put("IN_UNTAGGED_NEW_ITEMS", qrVO.getUntaggedNewItemFilter());
    logger.debug("IN_UNTAGGED_NEW_ITEMS = " + qrVO.getUntaggedNewItemFilter());

    inputParams.put("IN_OCCASION_ID", qrVO.getOccasionIdFilter());
    logger.debug("IN_OCCASION_ID = " + qrVO.getOccasionIdFilter());

    inputParams.put("IN_DELIVERY_LOCATION_TYPE", qrVO.getDeliveryLocationTypeFilter());
    logger.debug("IN_DELIVERY_LOCATION_TYPE = " + qrVO.getDeliveryLocationTypeFilter());

    inputParams.put("IN_MEMBER_NUMBER", qrVO.getMemberNumberFilter());
    logger.debug("IN_MEMBER_NUMBER = " + qrVO.getMemberNumberFilter());

    inputParams.put("IN_ZIP_CODE", qrVO.getZipCodeFilter());
    logger.debug("IN_ZIP_CODE = " + qrVO.getZipCodeFilter());

    inputParams.put("IN_TAGGED_BY", qrVO.getTaggedByFilter());
    logger.debug("IN_TAGGED_BY = " + qrVO.getTaggedByFilter());
    
    inputParams.put("IN_PRODUCT_ID", qrVO.getProductIdFilter());
    logger.debug("IN_PRODUCT_ID = " + qrVO.getProductIdFilter());

    //build DataRequest object
    request = new DataRequest();
    request.reset();
    request.setConnection(conn);
    request.setInputParams(inputParams);

    //determine if this is an ariba or walmart queue type
    //if it is, use the specific stored proc to return all queue types
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      request.setStatementID("GET_FILTERED_Q_WLMT_ARIBA");
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      request.setStatementID("GET_FILTERED_Q_PRM_COL");
    }
    else if ( isPreferredQueueWithAccess )
    {
      request.setStatementID("GET_FILTERED_Q_PARTNER");
    }
    else
    {
      request.setStatementID("GET_QUEUE_FILTERED");
    }

    Date beforeDate = new Date();
    logger.debug("before calling the stored proc = " + sdf.format(beforeDate));

    logger.debug("statementID = " + request.getStatementID());

    // 2. get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    outParametersMap = (HashMap) dau.execute(request);

    Date afterDate = new Date();
    logger.debug("after calling the stored proc  = " + sdf.format(afterDate));

    return outParametersMap;
  }

  /**
* Get values for dropdowns
*
* @param qrVO - used for input parameters to db stored procedure
* @return HashMap - of output parameters
* @throws IOException
* @throws ParserConfigurationException
* @throws SAXException
* @throws SQLException
*/
  public HashMap getFilterValues(QueueRequestVO qrVO)
    throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    HashMap outParametersMap = new HashMap();

    HashMap inputParams = new HashMap();

    HashMap partnerAccess = getPreferredPartnersWithAccess(qrVO.getSessionId());
    boolean isPreferredQueueWithAccess = false;
    if (qrVO.getQueueType() != null && partnerAccess != null) {
        String accessAllowed = (String) partnerAccess.get(qrVO.getQueueType());
        if (accessAllowed != null && accessAllowed.equalsIgnoreCase("Y")) {
            isPreferredQueueWithAccess = true;
        }
    }
    logger.info("isPreferredQueueWithAccess: " + isPreferredQueueWithAccess);

    // 1. build db stored procedure input parameters
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      inputParams.put("IN_ORIGIN_ID", qrVO.getQueueType());
      logger.debug("IN_ORIGIN_ID = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else if ( isPreferredQueueWithAccess )
    {
      inputParams.put("IN_PARTNER_NAME", qrVO.getQueueType());
      logger.debug("IN_PARTNER_NAME = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else
    {
      inputParams.put("IN_QUEUE_TYPE", qrVO.getQueueType());
      logger.debug("IN_QUEUE_TYPE = " + qrVO.getQueueType());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }

    String sTagged = getTaggedInd(qrVO.getQueueInd(), qrVO.getTagged());
    inputParams.put("IN_TAGGED_INDICATOR", sTagged);
    logger.debug("IN_TAGGED_INDICATOR = " + sTagged);

    String sAttEmail = getAttEmailInd(qrVO.getQueueInd(), qrVO.getAttEmail());
    inputParams.put("IN_ATTACHED_EMAIL_INDICATOR", sAttEmail);
    logger.debug("IN_ATTACHED_EMAIL_INDICATOR = " + sAttEmail);

    inputParams.put("IN_SORT_ORDER", qrVO.getSortColumn());
    logger.debug("IN_SORT_ORDER = " + qrVO.getSortColumn());

    inputParams.put("IN_CSR_ID", qrVO.getUser());
    logger.debug("IN_CSR_ID = " + qrVO.getUser());

    inputParams.put("IN_GROUP_ID", qrVO.getGroupId());
    logger.debug("IN_GROUP_ID = " + qrVO.getGroupId());

    inputParams.put("IN_SORT_DIRECTION", qrVO.getNewSortDirection());
    logger.debug("IN_SORT_DIRECTION = " + qrVO.getNewSortDirection());

    //build DataRequest object
    request = new DataRequest();
    request.reset();
    request.setConnection(conn);
    request.setInputParams(inputParams);

    //determine if this is an ariba or walmart queue type
    //if it is, use the specific stored proc to return all queue types
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      request.setStatementID("GET_FILTER_VALUES_WLMT_ARIBA");
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      request.setStatementID("GET_FILTER_VALUES_PRM_COL");
    }
    else if ( isPreferredQueueWithAccess )
    {
      request.setStatementID("GET_FILTER_VALUES_PARTNER");
    }
    else
    {
      request.setStatementID("GET_FILTER_VALUES");
    }

    Date beforeDate = new Date();
    logger.debug("before calling the stored proc = " + sdf.format(beforeDate));

    logger.debug("statementID = " + request.getStatementID());

    // 2. get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    outParametersMap = (HashMap) dau.execute(request);

    Date afterDate = new Date();
    logger.debug("after calling the stored proc  = " + sdf.format(afterDate));

    return outParametersMap;
  }


  /**
* get tagged indicator
* 1. translate tagged indicator to what the db stored procedure expects
*
* @param queueInd - queue indicator, e.g., mercury, user, etc.
* @param tagged - tagged indicator
* @return String - translated tagged indicator
* @throws
*/
  public String getTaggedInd(String queueInd, boolean tagged)
  {
    String sTagged = tagged? "Y": "N";
    if (queueInd.equalsIgnoreCase(QueueConstants.GROUP_QUEUE))
    {
      sTagged = "Y"; // always true for group
    }
    if (queueInd.equalsIgnoreCase(QueueConstants.USER_QUEUE))
    {
      sTagged = "Y"; // always true for user
    }
    if (queueInd.equalsIgnoreCase(QueueConstants.ORDER_QUEUE))
    {
      sTagged = "Y"; // always true for order
    }
    return sTagged;
  }


  /**
* get attached e-mail indicator
* 1. translate attached e-mail indicator to what the db stored procedure expects
*
* @param queueInd - queue indicator, e.g., mercury, user, etc.
* @param attEmail - attached e-mail indicator
* @return String - translated attached e-mail indicator
* @throws
*/
  public String getAttEmailInd(String queueInd, boolean attEmail)
  {
    String sAttEmail = attEmail? "Y": "N";
    if (queueInd.equalsIgnoreCase(QueueConstants.GROUP_QUEUE))
    {
      sAttEmail = null; // attached e-mail indicator not applicable
    }
    if (queueInd.equalsIgnoreCase(QueueConstants.USER_QUEUE))
    {
      sAttEmail = null; // attached e-mail indicator not applicable
    }
    if (queueInd.equalsIgnoreCase(QueueConstants.MERCURY_QUEUE))
    {
      sAttEmail = null; // attached e-mail indicator not applicable
    }
    if (queueInd.equalsIgnoreCase(QueueConstants.ORDER_QUEUE))
    {
      sAttEmail = null; // attached e-mail indicator not applicable
    }
    return sAttEmail;
  }


  /**
* get queue count
*
* @param qrVO - used for input parameters to db stored procedure
* @return HashMap - of output parameters
* @throws Exception
*/
  public int getQueueCount(QueueRequestVO qrVO)
    throws Exception
  {
    HashMap inputParams = new HashMap();

    HashMap partnerAccess = getPreferredPartnersWithAccess(qrVO.getSessionId());
    boolean isPreferredQueueWithAccess = false;
    if (qrVO.getQueueType() != null && partnerAccess != null) {
        String accessAllowed = (String) partnerAccess.get(qrVO.getQueueType());
        if (accessAllowed != null && accessAllowed.equalsIgnoreCase("Y")) {
            isPreferredQueueWithAccess = true;
        }
    }
    logger.info("isPreferredQueueWithAccess: " + isPreferredQueueWithAccess);

    // 1. build db stored procedure input parameters
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      inputParams.put("IN_ORIGIN_ID", qrVO.getQueueType());
      logger.debug("IN_ORIGIN_ID = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else if ( isPreferredQueueWithAccess )
    {
      inputParams.put("IN_PARTNER_NAME", qrVO.getQueueType());
      logger.debug("IN_PARTNER_NAME = " + qrVO.getQueueType());

      inputParams.put("IN_QUEUE_INDICATOR", qrVO.getQueueInd());
      logger.debug("IN_QUEUE_INDICATOR = " + qrVO.getQueueInd());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }
    else
    {
      inputParams.put("IN_QUEUE_TYPE", qrVO.getQueueType());
      logger.debug("IN_QUEUE_TYPE = " + qrVO.getQueueType());

      inputParams.put("IN_EXCLUDE_PARTNERS", qrVO.getExcludePartners());
      logger.debug("IN_EXCLUDE_PARTNERS = " + qrVO.getExcludePartners());
    }

    String sTagged = getTaggedInd(qrVO.getQueueInd(), qrVO.getTagged());
    inputParams.put("IN_TAGGED_INDICATOR", sTagged);
    logger.debug("IN_TAGGED_INDICATOR = " + sTagged);

    String sAttEmail = getAttEmailInd(qrVO.getQueueInd(), qrVO.getAttEmail());
    inputParams.put("IN_ATTACHED_EMAIL_INDICATOR", sAttEmail);
    logger.debug("IN_ATTACHED_EMAIL_INDICATOR = " + sAttEmail);

    inputParams.put("IN_SORT_ORDER", qrVO.getSortColumn());
    logger.debug("IN_SORT_ORDER = " + qrVO.getSortColumn());

    //determine if this is an ariba or walmart or premier collection or preferred partner queue type
    //if it is, CSR and GROUP ID are not needed
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1)            || 
        (qrVO.getQueueType().indexOf("ARIBA") != -1)              || 
        (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1) || 
        (isPreferredQueueWithAccess) )
    {
      logger.debug("CSR_ID not used for Walmart or Ariba or Premier Collection or Preferred Partner  orders " + qrVO.getUser());
      logger.debug("GROUP_ID not used for Walmart or Ariba or Premier Collection or Preferred Partner  orders " + qrVO.getGroupId());
    }
    else
    {
      inputParams.put("IN_CSR_ID", qrVO.getUser());
      logger.debug("IN_CSR_ID = " + qrVO.getUser());

      inputParams.put("IN_GROUP_ID", qrVO.getGroupId());
      logger.debug("IN_GROUP_ID = " + qrVO.getGroupId());
    }

    inputParams.put("IN_SORT_DIRECTION", qrVO.getNewSortDirection());
    logger.debug("IN_SORT_DIRECTION = " + qrVO.getNewSortDirection());

    //build DataRequest object
    request = new DataRequest();
    request.reset();
    request.setConnection(conn);
    request.setInputParams(inputParams);

    //determine if this is an ariba or walmart queue type
    //if it is, use the specific stored proc to return all queue types
    //for origin_id's of "CAT" and "ARI" (for ariba) or "WLMTI" (for walmart)
    if ((qrVO.getQueueType().indexOf("WALMART") != -1) || (qrVO.getQueueType().indexOf("ARIBA") != -1))
    {
      request.setStatementID("GET_QUEUE_COUNT_WLMT_ARIBA");
    }
    else if (qrVO.getQueueType().indexOf("PREMIER_COLLECTION") != -1)
    {
      request.setStatementID("GET_QUEUE_COUNT_PRM_COL");
    }
    else if ( isPreferredQueueWithAccess )
    {
      request.setStatementID("GET_QUEUE_COUNT_PARTNER");
    }
    else
    {
      request.setStatementID("GET_QUEUE_COUNT");
    }

    Date beforeDate = new Date();
    logger.debug("before calling the stored proc = " + sdf.format(beforeDate));

    logger.debug("statementID = " + request.getStatementID());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    BigDecimal bdCount = (BigDecimal) dataAccessUtil.execute(request);

    Date afterDate = new Date();
    logger.debug("after calling the stored proc  = " + sdf.format(afterDate));


    int iCount = 0;
    if (bdCount != null)
      iCount = bdCount.intValue();

    return iCount;

  }

  public static HashMap getPreferredPartnersWithAccess(String sessionId) throws IOException {

      HashMap outMap = new HashMap();

      try {
          outMap = PreferredPartnerUtility.getPreferredPartnersAccessForUser(sessionId);
      } catch (Exception e) {
          logger.error(e);
      }

      return outMap;
  }
}
