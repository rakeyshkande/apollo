package com.ftd.queue.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;


/**
 * This class is used to start and stop the Queue Timer
 *
 * @author Ali Lakani & Mike Kruger
 */
public class TimerDAO 
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.queue.dao.TimerDAO");

  //database:  common output fields from db stored procedures calls
  public final static String DB_OUT_STATUS  = "OUT_STATUS";
  public final static String DB_OUT_MESSAGE = "OUT_MESSAGE";
    
  public TimerDAO(Connection connection)
  {
    this.connection = connection;
  }
  

  /**
   * This method inserts a Timer entry into the entity history table.  
   * It starts the timer.
   * @param entityType - Type of entity timer
   * @param entityId - Id associated with the timer
   * @param commentOrigin - Where the Timer originated
   * @param csrId - the csr id
   * @param callLogId - the call log id if one exists
   * @return HashMap of timer result values
   * @throws java.lang.Exception
   */
  public HashMap insertEntityHistory(String entityType, String entityId, String commentOrigin,
	String csrId, String callLogId) 
	throws IOException, ParserConfigurationException,
			SAXException, SQLException
  {
    HashMap startResults = new HashMap();
    // get the list of order detail ids for this order guid
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_UPDATE_ENTITY_HISTORY");
    dataRequest.addInputParam("IN_ENTITY_HISTORY_ID",   null);
    dataRequest.addInputParam("IN_ENTITY_TYPE",         entityType);
    dataRequest.addInputParam("IN_ENTITY_ID",           entityId);
    dataRequest.addInputParam("IN_COMMENT_ORIGIN",      commentOrigin);
    dataRequest.addInputParam("IN_CSR_ID",              csrId);
    dataRequest.addInputParam("IN_CALL_LOG_ID",         callLogId);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    startResults = (HashMap) dataAccessUtil.execute(dataRequest);

	String dbCallResult = (String) startResults.get(DB_OUT_STATUS);
    if(dbCallResult != null  &&  dbCallResult.equalsIgnoreCase("N"))
    {
        String dbOutMessage = (String) startResults.get(DB_OUT_MESSAGE);
        throw new SQLException("CLEAN.COMMENT_HISTORY_PKG.INSERT_UPDATE_ENTITY_HISTORY" + 
                               " failed with out_message = " + dbOutMessage  +
                               " IN_ENTITY_HISTORY_ID = " + null + 
                               " IN_ENTITY_TYPE = " + entityType +
                               " IN_ENTITY_ID = " + entityId + 
                               " IN_COMMENT_ORIGIN = " + commentOrigin + 
                               " IN_CSR_ID = " + csrId + 
                               " IN_CALL_LOG_ID = " + callLogId);
    }
    
	return startResults;    
  }



  /**
   * This method updates a timer entry in the entity history table.   
   * It stops the timer.
   * @param entityHistoryId - Id assocaited with the timer entry
   * @throws java.lang.Exception
   */
  public void updateEntityHistory(String entityHistoryId) 
	throws IOException, ParserConfigurationException,
			SAXException, SQLException
  {
    HashMap stopResults = null;
	
	// get the list of order detail ids for this order guid
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_UPDATE_ENTITY_HISTORY");
    dataRequest.addInputParam("IN_ENTITY_HISTORY_ID",   entityHistoryId);
    dataRequest.addInputParam("IN_ENTITY_TYPE",         null);
    dataRequest.addInputParam("IN_ENTITY_ID",           null);
    dataRequest.addInputParam("IN_COMMENT_ORIGIN",      null);
    dataRequest.addInputParam("IN_CSR_ID",              null);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
    stopResults = (HashMap) dataAccessUtil.execute(dataRequest);

  }

}