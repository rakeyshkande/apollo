package com.ftd.queue.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashMap;


/**
 * ViewDAO - This class is used to interact with the csr_viewed_entities table.
 * 
 * @author Mike Kruger
 */


public class ViewDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.queue.dao.ViewDAO");
		
		//database:  common output fields from db stored procedures calls
		public final static String DB_OUT_STATUS  = "OUT_STATUS";
		public final static String DB_OUT_MESSAGE = "OUT_MESSAGE";
    
  /**
   * Constructor
   * @param connection Connection
   */
  public ViewDAO(Connection connection)
  {
    this.connection = connection;
  }


  /**
   * Method for updating the csr viewed record
   * 
   * @param csrId - csr id of user viewing the order
   * @param entityType - Entity type of viewing (Ex.  "ORDER_DETAILS")
	 * @param entityId - Id associated with the entity type (Ex.  order detail id)
   * 
   * @throws java.lang.Exception
   */
  public void updateCSRViewed(String csrId, String entityType, String entityId) 
        throws Exception
  {
        HashMap results = null;
				DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("UPDATE_CSR_VIEWED");

				if(logger.isDebugEnabled())
				{
					logger.debug("IN_CSR_ID:" + csrId);
					logger.debug("IN_ENTITY_TYPE:" + entityType);
					logger.debug("IN_ENTITY_ID:" + entityId);
				}

				dataRequest.addInputParam("IN_CSR_ID", 		    csrId);
        dataRequest.addInputParam("IN_ENTITY_TYPE", 	entityType);
        dataRequest.addInputParam("IN_ENTITY_ID", 		entityId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        results = (HashMap)dataAccessUtil.execute(dataRequest); 
				
				String dbCallResult = (String) results.get(DB_OUT_STATUS);
				if(dbCallResult != null  &&  dbCallResult.equalsIgnoreCase("N"))
				{
					String dbOutMessage = (String) results.get(DB_OUT_MESSAGE);
					throw new SQLException("CLEAN.CSR_VIEWED_LOCKED_PKG.UPDATE_CSR_VIEWED" + 
																	" failed with out_message = " + dbOutMessage  +
																	" IN_CSR_ID = " + csrId + 
																	" IN_ENTITY_TYPE = " + entityType +
																	" IN_ENTITY_ID = " + entityId);
				}
  }
}