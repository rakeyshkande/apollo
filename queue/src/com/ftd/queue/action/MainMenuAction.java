package com.ftd.queue.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.queue.constants.QueueConstants;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class redirects the user to the Main Menu, which, for the Queue 
 * application, is the Customer Service Menu.
 *
 * @author Mike Kruger
 */
public final class MainMenuAction extends Action 
{
	private static    Logger logger  = new Logger("com.ftd.queue.action.MainMenuAction");

    //forward names:
    private static final String MAIN_MENU = "MainMenu";

    /**
     * This is the Main Menu Action called from the Struts framework.
	 * The execute method redirects to the Customer Service Menu.
     * 
     * @param mapping  - ActionMapping used to select this instance
     * @param form     - ActionForm (optional) bean for this request
     * @param request  - HTTP Request we are processing
     * @param response - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
		HttpServletResponse response) 
        throws IOException, ServletException		 
	{
		ActionForward forward = null;
		String mainMenuUrl = null;
		
        try
        {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            mainMenuUrl = cu.getFrpGlobalParm(QueueConstants.QUEUE_CONFIG_CONTEXT,"login_page");
            
            mainMenuUrl = mainMenuUrl + "/security/Main.do"
            + "?adminAction=customerService"
            + "&context=" + (String)request.getParameter(QueueConstants.SEC_CONTEXT)
            + "&securitytoken=" + (String)request.getParameter(QueueConstants.SEC_TOKEN);
            forward = new ActionForward(mainMenuUrl, true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

		return forward;
    }
    
}
