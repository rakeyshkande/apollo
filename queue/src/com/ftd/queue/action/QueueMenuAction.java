package com.ftd.queue.action;

import com.ftd.queue.bo.QueueMenuBO;
import com.ftd.queue.constants.QueueConstants;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import com.ftd.queue.util.Timer;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This class is used to process a QueueMenu request.  It contains the main flow of logic needed for security
 * processing, XSL stylesheet determination, and makes calls to the business object to retrieve QueueMenu data.
 * The data received and XSL information is stored on the request object and the processing is forwarded to the
 * TransformerAction.
 *
 * @author Matt Wilcoxen
 */

public class QueueMenuAction extends Action 
{
    private Logger logger = new Logger("com.ftd.queue.action.QueueMenuAction");

    private static final String QUEUE_MENU_ACTION_XSL = "QueueMenuActionXSL";

    /**
     * This is the Queue Menu Action called from the Struts framework.
     * 
     * @param mapping  - ActionMapping used to select this instance
     * @param form     - ActionForm (optional) bean for this request
     * @param request  - HTTP Request we are processing
     * @param response - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, 
            HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
      Connection conn = null;
      Document queue = null;
      ActionForward forward = null;

      try
      {
        String sec_context = request.getParameter(QueueConstants.SEC_CONTEXT);
        String sec_token   = request.getParameter(QueueConstants.SEC_TOKEN);

        conn = getDBConnection();                           // get db connection

            // if a queue timer exists, stop the timer
        String entityHistoryId = (String)request.getParameter(QueueConstants.TIMER_ENTITY_HISTORY_ID);
        if(entityHistoryId != null)
  			{
          Timer timer = new Timer(conn);
          timer.stopTimer(entityHistoryId);

          /* Remove the queue timer information from the parameters Map
             This is just a precaution in case the Queue Timer information
             is passed within the Data Filter.
          */
          HashMap parameters = (HashMap) request.getAttribute(QueueConstants.APP_PARAMETERS);
          parameters.remove(QueueConstants.TIMER_ENTITY_HISTORY_ID);
          parameters.remove(QueueConstants.TIMER_COMMENT_ORIGIN_TYPE);
        }

        QueueMenuBO qmBO = new QueueMenuBO(conn);
        queue = qmBO.getMenuItems(sec_context, sec_token);  //get queue menu data

        File XSLFile = getXSL(mapping, request);	        //get XSL file name

        forward = mapping.findForward(QUEUE_MENU_ACTION_XSL);
        String xslFilePathAndName = forward.getPath(); //get real file name
          
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, queue, 
                            XSLFile, xslFilePathAndName, (HashMap)request.getAttribute(QueueConstants.APP_PARAMETERS));
        return null;
      }
      catch (ClassNotFoundException  cnfe)
      {
  			logInput(queue); 
        logger.error(cnfe);
  			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      catch (IOException  ioe)
      {
  			logInput(queue); 
        logger.error(ioe);
  			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      catch (ParserConfigurationException  pcex)
      {
  			logInput(queue); 
        logger.error(pcex);
        forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      catch (SAXException  sec)
      {
  			logInput(queue); 
        logger.error(sec);
  			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      catch (SQLException  sqle)
      {
  			logInput(queue); 
        logger.error(sqle);
        forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      catch (TransformerException  tex)
      {
  			logInput(queue); 
        logger.error(tex);
        forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      catch (Throwable  t)
      {
  			logInput(queue); 
        logger.error(t);
        forward = mapping.findForward(QueueConstants.ACTN_ERROR);
      }
      finally
      {
        try
        {
  				if(conn != null)
    			{
            conn.close();
        	}
        }
        catch (SQLException  se)
        {
          logger.error(se);
          forward = mapping.findForward(QueueConstants.ACTN_ERROR);
        }
      }

      return forward;
    }


    /**
     * Obtain connectivity with the database
     * 
     * @param none
     * @return Connection - db connection
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    private Connection getDBConnection()
        throws ClassNotFoundException, SQLException, Exception
    {
      Connection conn;
      conn = DataSourceUtil.getInstance().getConnection(
                 ConfigurationUtil.getInstance().getProperty(QueueConstants.PROPERTY_FILE,
                                                             QueueConstants.DATASOURCE_NAME));

      return conn;
    }


    /**
     * Retrieve name of Queue Menu XSL file
     * 
     * @param ActionMapping      - Struts action mapping
     * @param HttpServletRequest - user request information
     * @return File              - Queue Menu XSL File name
     * @throws none
     */
    public File getXSL(ActionMapping mapping, HttpServletRequest request)
    {
      File XSLFile;
      String XSLFileName = mapping.findForward(QUEUE_MENU_ACTION_XSL).getPath();

      XSLFile = new File(this.servlet.getServletContext().getRealPath(XSLFileName));

      return XSLFile;
    }


	/**
	 * Logs info in the error log
	 * 
	 * @document queue 
	 * @throws none
	 */
	public void logInput(Document queue)
	{
    StringWriter sw = null;
    
    try
    {
      if (queue!=null)
      {
        sw = new StringWriter(); 
        DOMUtil.print(queue, new PrintWriter(sw));
        logger.error("QueueMenuAction - logInput(): queue request DOM = \n" + sw.toString());
      }
    }
    catch(Exception e)
    {
      logger.error("QueueMenuAction - logInput(): Return results could not be printed to the log file");
      logger.error(e);
    }

  }


}
