package com.ftd.queue.action;

import com.ftd.queue.bo.QueueItemRequestBO;
import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.dao.OrderDAO;

import com.ftd.queue.dao.ViewDAO;
import com.ftd.queue.util.BasePageBuilder;
import com.ftd.security.SecurityManager;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.cache.vo.UserInfo;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * A queue item request action request will arrive via the Struts framework and will
 * be routed to the QueueItemRequestAction.  The QueueItemRequestAction will create
 * a database connection and call the QueueItemRequestBO to obtain additional attributes
 * required for the next screen in the process.  The QueueItemRequestBO will determine
 * what type of attributes to retrieve based upon the type of queue item requested.  
 * 
 * Here are the possibilities:
 *     Customer Account Screen - The getCustomerId method will be called to retrieve 
 *     the Customer Id.
 * 
 * Once the additional attributes are collected they will be placed in a Map and
 * passed back to the QueueItemRequestAction.  The QueueItemRequestAction will then
 * place the additional attributes on the request object, determine the appropriate
 * action (screen) to forward to, and forward to the next Action (screen).
 * 
 * This class is used to process a queue item request.  It contains the main flow of logic 
 * needed to obtain the appropriate information for the next Action in the flow and
 * determine, set and forward to the next Action in the flow.
 *
 * @author Matt Wilcoxen
 */

public class QueueItemRequestAction extends Action 
{
	private Logger logger = new Logger("com.ftd.queue.action.QueueItemRequestAction");

	private static final String EMPTY_STRING = "";
        
        // database config constants
    public static final String BASE_CONFIG = "BASE_CONFIG";
    public static final String BASE_URL = "BASE_URL";

  //forward names:
  private static final String CALL_OUT_SCRIPT_ITEM_REQUEST = "CallOutScriptItemRequest";
  private static final String COMMUNICATION_ITEM_REQUEST = "CommunicationItemRequest";
  private static final String UPDATE_ORDER_ITEM_REQUEST = "UpdateOrderItemRequest";
  private static final String MERCURY_MESSAGE_ITEM_REQUEST = "MercuryMessageItemRequest";
  private static final String LOSS_PREVENTION_ITEM_REQUEST = "LossPreventionItemRequest";
  private static final String CUSTOMER_ACCOUNT_REQUEST = "CustomerAccountRequest";
  private static final String EMAIL_SCREEN_ITEM_REQUEST = "EmailScreenItemRequest";

  //request parameters:
  private static final String R_WALMERT_ARIBA_QUEUE_TYPE="walmart_ariba_queue_type";
  private static final String R_ATTACHED = "attached";
  private static final String R_CSR_ID = "csr_id";	
  private static final String R_CURRENT_SORT_DIRECTION = "current_sort_direction";
  private static final String R_CUSTOMER_ID = "customer_id";
  private static final String R_EXTERNAL_ORDER_NUMBER = "external_order_number";	
  private static final String R_GROUP = "group";
  private static final String R_LINE_NUMBER = "line_number";
  private static final String R_MAX_RECORDS = "max_records";
  private static final String R_MERCURY_NUM = "mercury_num";	
  private static final String R_MESSAGE_ID = "message_id";
  private static final String R_MESSAGE_LINE_NUMBER = "message_line_number";
  private static final String R_MESSAGE_TYPE = "message_type";
  private static final String R_MSG_SUB_TYPE = "msg_sub_type";
  private static final String R_ORDER_DETAIL_ID = "order_detail_id";
  private static final String R_ORDER_GUID = "order_guid";
  private static final String R_POC_ID = "poc_id";
  private static final String R_POINT_OF_CONTACT = "point_of_contact";
  private static final String R_POSITION = "position";
  private static final String R_QUEUE_IND = "queue_ind";
  private static final String R_QUEUE_MESSAGE_ID = "queue_message_id";
  private static final String R_QUEUE_TYPE = "queue_type";
  private static final String R_QUEUE_TYPE_CHOSEN = "queue_type_chosen";
  private static final String R_REMOVE_EMAIL_QUEUE_FLAG = "remove_email_queue_flag";
  private static final String R_SECURITYTOKEN = "securitytoken";
  private static final String R_SORT_COLUMN = "sort_column";
  private static final String R_SORT_ON_RETURN = "sort_on_return";
  private static final String R_STATUS = "status";
  private static final String R_SYSTEM = "system";
  private static final String R_TAGGED = "tagged";
  private static final String R_TAGGED_CSR_ID = "tagged_csr_id";	
  private static final String R_USER = "user";
  private static final String R_RECEIVED_DATE_FILTER = "received_date_filter";
  private static final String R_DELIVERY_DATE_FILTER = "delivery_date_filter";
  private static final String R_TAG_DATE_FILTER = "tag_date_filter";
  private static final String R_LASTTOUCHED_FILTER = "lasttouched_filter";
  private static final String R_TYPE_FILTER = "type_filter";
  private static final String R_SYS_FILTER = "sys_filter";
  private static final String R_TIMEZONE_FILTER = "timezone_filter";
  private static final String R_SDG_FILTER = "sdg_filter";
  private static final String R_DISPOSITION_FILTER = "disposition_filter";
  private static final String R_PRIORITY_FILTER = "priority_filter";
  private static final String R_NEW_ITEM_FILTER = "new_item_filter";
  private static final String R_OCCASION_FILTER = "occasion_filter";
  private static final String R_LOCATION_FILTER = "location_filter";
  private static final String R_MEMBER_NUMBER_FILTER = "member_number_filter";
  private static final String R_ZIP_CODE_FILTER = "zip_code_filter";
  private static final String R_TAGBY_FILTER = "tagby_filter";
  private static final String R_FILTER = "filter";
  

  //Constants
  private static final String YES = "Y";
  private static final String NO = "N";

    
	// system types
	private static final String S_MERCURY = "MERC";	
	private static final String S_VENUS = "Venus";	

	private static final String QUEUE_IND_USER = "user";
	private static final String QUEUE_IND_GROUP = "group";
	private static final String QUEUE_IND_EMAIL = "email";
	private static final String QUEUE_IND_ORDER = "order";

  private static final String ADDL_PARMS = "addlParms";
  
  private static final String Q_MESSAGE_ID = "q_message_id";
  private static final String FTDM = "FTDM";

	private static final String CSR_VIEWED_ENTITY_TYPE = "ORDER_DETAILS";
		

	/**
	 * This is the Queue Menu Action called from the Struts framework.
     *     1. get db connection
     *     2. get request parameters
     *     3. set action forward based on queue type
     *            if queue type is loss prevention
     *                if order is on hold for loss prevention, set forward to loss prevention request
     *                otherwise, set forward to customer search item request
     *     4. get additional parameters for queue type
     *     5. save additional parameters onto request object
     *     6. forward to requested action
	 * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException, SQLException, Exception
	{
		String path = null;
		boolean redirect = true;
		ActionForward forward = null;

    Connection conn = null;
    boolean isOnHoldForLossPrevention = false;
		boolean isError = false;
		boolean securityIsOn = false;

		try
		{
			// get db connection 
			conn = getDBConnection() ;

			// get security manager instance
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
		    
		    String baseUrl = cu.getFrpGlobalParm(BASE_CONFIG,BASE_URL);

			String security = (String) cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.SECURITY_IS_ON);
			securityIsOn = security.equalsIgnoreCase("true") ? true : false;

      // get passed parameters from request
      String queueType  = request.getParameter(R_QUEUE_TYPE);
			String queueTypeOfItem = request.getParameter(R_QUEUE_TYPE);
      String queueInd  = request.getParameter(R_QUEUE_IND);
      String orderGuid = request.getParameter(R_ORDER_GUID);
      String orderDetailId = request.getParameter(R_ORDER_DETAIL_ID);
      String system = request.getParameter(R_SYSTEM);
      String messageId = request.getParameter(R_MESSAGE_ID);
      String virtualQueueType = request.getParameter(R_WALMERT_ARIBA_QUEUE_TYPE);
      String messageType = null;
			String securityToken = request.getParameter(R_SECURITYTOKEN);
			String csrId = EMPTY_STRING;

			// Obtain the csrId
			if(securityIsOn)
			{
				csrId = getCsrId(securityToken);				
			}
            
      if(queueInd.equalsIgnoreCase(QueueConstants.USER_QUEUE) || queueInd.equalsIgnoreCase(QueueConstants.GROUP_QUEUE))
			{
				queueTypeOfItem = request.getParameter(R_QUEUE_TYPE_CHOSEN);
			}
			
			logger.debug("execute(): passed data = \n" + 
                R_QUEUE_TYPE + " = " + queueType + "\n" +
                R_QUEUE_IND + " = " + queueInd + "\n" +
                R_ORDER_GUID + " = " + orderGuid + "\n" +
                R_ORDER_DETAIL_ID + " = " + orderDetailId + "\n" +
                R_MESSAGE_ID + " = " + messageId);

			// start Queue Timer
      String timerType = null;
			
      /* If the indicator is for email, group, user, or order use the indicator as the timer type
			   Else use the queueType as the timer type if it exists.
			   The timer type used must match what is in the clean.comment_origin_val table.  A "/Q" will be
			   appended to the timer type within the DAO
			*/
			if(queueInd.equalsIgnoreCase(QUEUE_IND_EMAIL) || queueInd.equalsIgnoreCase(QUEUE_IND_GROUP) 
			   || queueInd.equalsIgnoreCase(QUEUE_IND_USER) || queueInd.equalsIgnoreCase(QUEUE_IND_ORDER))
				timerType = queueInd.toUpperCase();
			else if(queueTypeOfItem != null && !queueTypeOfItem.equals(""))
				timerType = queueTypeOfItem.toUpperCase();
			else
			    throw new Exception("Invalid entity timer comment origin \"" + timerType +"\".  Please provide a valid comment origin.");
			
			QueueItemRequestBO qirBO = new QueueItemRequestBO(conn);
			HashMap timerResults = qirBO.startTimer(request.getParameter(QueueConstants.SEC_TOKEN), orderDetailId, timerType);
			
			// set Queue Timer information as parameters
			//HashMap parameters = (HashMap) request.getAttribute(QueueConstants.APP_PARAMETERS);
			String timerEntityHistoryId   = (String)timerResults.get(QueueConstants.TIMER_ENTITY_HISTORY_ID);
      String timerCommentOriginType = (String)timerResults.get(QueueConstants.TIMER_COMMENT_ORIGIN_TYPE);
      String timerCommentOrigin = (String)timerResults.get(QueueConstants.TIMER_COMMENT_ORIGIN);

			logger.debug("Started queue timer - " +
						  QueueConstants.TIMER_ENTITY_HISTORY_ID + ": " + timerEntityHistoryId + " " +
						  QueueConstants.TIMER_COMMENT_ORIGIN_TYPE + ": " + timerCommentOriginType + " " +
						  QueueConstants.TIMER_COMMENT_ORIGIN + ": " + timerCommentOrigin);

			/* Update csr viewed (last touched) for an order if an order detail id exists
			 * for the queue item chosen
			 */ 
			if(orderDetailId != null && !orderDetailId.equalsIgnoreCase(EMPTY_STRING))
			{
				ViewDAO vd = new ViewDAO(conn);
				vd.updateCSRViewed(csrId, CSR_VIEWED_ENTITY_TYPE, orderDetailId);				
			}

      // get additional parameters for queue type
			logger.debug("execute() - \n" +
				" b4 call to QueueItemRequest.retrieveParameters" +
				" queueType = " + queueType +
				" queueTypeOfItem = " + queueTypeOfItem +
				" orderDetailId = " + orderDetailId +
				" isOnHoldForLossPrevention = " + isOnHoldForLossPrevention);

      Map addlParms = qirBO.retrieveParameters(queueTypeOfItem,orderDetailId,isOnHoldForLossPrevention);

			// save additional parameters onto request object
			request.setAttribute(ADDL_PARMS,addlParms);

      // set action forward based on queue type
      if(queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_ADJ_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_REJ_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_FTD_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_ANS_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_ASK_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_CON_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_DEN_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_CAN_QUEUE)  ||
         queueTypeOfItem.equalsIgnoreCase(QueueConstants.DC_QUEUE))
      {
        messageType = request.getParameter(R_MESSAGE_TYPE);

				logger.debug("execute() - inside if(queueTypeOfItem.equals . . . \n " +
					"messageType = " + messageType);
                
				forward = mapping.findForward(COMMUNICATION_ITEM_REQUEST);
        path = "/html/simpleredirect.html?forward=" + baseUrl + forward.getPath()             
                + "&" + R_ORDER_DETAIL_ID + "=" + orderDetailId
                + generateDataFilerParams(request, timerResults, queueType, virtualQueueType);
					
				logger.debug("execute() - message is NOT FTDM - about to go to Communication Item \n" +
						"forward/path = " + path);
      }
      else if(queueTypeOfItem.equalsIgnoreCase(QueueConstants.ORDER_QUEUE)  ||
              queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_CREDIT_QUEUE)  ||
              queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_ZIP_QUEUE))
      {
				String externalOrderNum = request.getParameter(R_EXTERNAL_ORDER_NUMBER);
				
				forward = mapping.findForward(CUSTOMER_ACCOUNT_REQUEST);
                
        path = "/html/simpleredirect.html?forward=" + baseUrl + forward.getPath()             
                + "&" + "action=search"
                + "&" + "recipient_flag=y" 
                + "&" + "in_order_number=" + externalOrderNum
                + generateDataFilerParams(request, timerResults, queueType, virtualQueueType);
				
				logger.debug("execute() - mercury queue is Order - about to go to Recipient/Order \n" +
  					"forward/path = " + path);
			}
      else if(queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_GEN_QUEUE))
      {
				String taggedCsrId = request.getParameter(R_CSR_ID);
				
				if(taggedCsrId == null)
				{
				    taggedCsrId = "";
					if(securityIsOn)
					{
						taggedCsrId = csrId;
					}
				}

				forward = mapping.findForward(MERCURY_MESSAGE_ITEM_REQUEST);
                path = "/html/simpleredirect.html?forward=" + baseUrl + forward.getPath()             
					+ "&" + R_MESSAGE_ID + "=" + request.getParameter(R_MERCURY_NUM)
					+ "&" + R_QUEUE_MESSAGE_ID + "=" + messageId
          + "&" + "msg_message_type=Mercury" //There is only one type of GEN messsage, Mercury.
					+ "&" + R_TAGGED_CSR_ID + "=" + taggedCsrId
          + "&" + R_MSG_SUB_TYPE + "=" + queueType
					+ generateDataFilerParams(request, timerResults, queueType, virtualQueueType);

				logger.debug("execute() - mercury queue is GEN - about to go to Mercury Message Item \n" +
					"forward/path = " + path);
      }
      else if(queueTypeOfItem.equalsIgnoreCase(QueueConstants.MERCURY_LP_QUEUE))
      {
        OrderDAO oDAO = new OrderDAO(conn);
        isOnHoldForLossPrevention = oDAO.isOnHoldForLossPreventionInd(orderGuid,orderDetailId);
        if(isOnHoldForLossPrevention)
        {
					forward = mapping.findForward(LOSS_PREVENTION_ITEM_REQUEST);
          path = "/html/simpleredirect.html?forward=" + baseUrl + forward.getPath()             
            + "&" + R_CUSTOMER_ID + "=" + addlParms.get(R_CUSTOMER_ID).toString()
            + "&" + R_ORDER_GUID  + "=" + orderGuid
            + generateDataFilerParams(request, timerResults, queueType, virtualQueueType);

					logger.debug("execute() - mercury queue is LP  AND  IS on hold for loss prevention - about to go to Loss Prevention Item \n" +
  						"forward/path = " + path);

        }
        else
        {
					forward = mapping.findForward(CUSTOMER_ACCOUNT_REQUEST);
          path = "/html/simpleredirect.html?forward=" + baseUrl + forward.getPath()             
                  + "&action=customer_search" 
                  + "&" + R_CUSTOMER_ID + "=" + addlParms.get(R_CUSTOMER_ID).toString()
                  + generateDataFilerParams(request, timerResults, queueType, virtualQueueType);

					logger.debug("execute() - mercury queue is LP  AND  is NO on hold for loss prevention - about to go to Loss Prevention Item \n" +
						"forward/path = " + path);
        }
      }
      else if(queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_OA_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_CX_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_MO_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_ND_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_QI_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_PD_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_BD_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_DI_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_QC_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_OT_QUEUE)	||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_SE_QUEUE) ||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_AMZN_QUEUE) ||
        queueTypeOfItem.equalsIgnoreCase(QueueConstants.EMAIL_MRCNT_QUEUE))
      {
				String taggedCsrId = request.getParameter(R_CSR_ID);

				if(taggedCsrId == null)
				{
					taggedCsrId = "";
					if(securityIsOn)
					{
						taggedCsrId = csrId;
					}
				}
				
				forward = mapping.findForward(EMAIL_SCREEN_ITEM_REQUEST);
        path = "/html/simpleredirect.html?forward=" + baseUrl + forward.getPath()             
                + "&"
                + R_REMOVE_EMAIL_QUEUE_FLAG + "=Y" 
                + "&" + R_POC_ID + "=" + request.getParameter(R_POINT_OF_CONTACT)
                + "&" + R_MESSAGE_LINE_NUMBER + "=" + request.getParameter(R_LINE_NUMBER)
                + "&" + R_ORDER_DETAIL_ID + "=" + orderDetailId
                + "&" + R_MESSAGE_ID + "=" + messageId
                + "&" + R_STATUS + "=" + system
                + "&" + R_TAGGED_CSR_ID + "=" + taggedCsrId
                + "&" + R_ATTACHED + "=" + request.getParameter(R_ATTACHED)					
                + generateDataFilerParams(request, timerResults, queueType, virtualQueueType);

				logger.debug("execute() - queueInd = email_queue \n" +
					"forward/path = " + path);
      }
		}
		catch (TransformerException  te)
		{
			logger.error(te);
			isError = true;
		}
		catch (ClassNotFoundException  cnfe)
		{
			logger.error(cnfe);
			isError = true;
		}
		catch (IOException  ioe)
		{
			logger.error(ioe);
			isError = true;
		}
		catch (ParserConfigurationException  pcex)
		{
			logger.error(pcex);
			isError = true;
		}
		catch (SAXException  sec)
		{
			logger.error(sec);
			isError = true;
		}
		catch (SQLException  sqle)
		{
			logger.error(sqle);
			isError = true;
		}
		catch (Throwable  t)
		{
			logger.error(t);
			isError = true;
		}
		finally
		{
			try
			{
				if(conn != null)
				{
					conn.close();
				}
			}
			catch (SQLException se)
			{
				logger.error(se);
				isError = true;
			}
		}

        if(isError)
		{
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
			return forward;
		}
		else
		{
			if(path != null  &&  path.trim().length() > 0)
			{
				// 6. forward to requested action
				logger.debug("execute() - about to leave page and go to \n" +
					"forward/path = " + path);
		
				forward = new ActionForward(path, redirect);
				return forward;
			}
			else
			{
				logger.error("No forward path was specified.");
				forward = mapping.findForward(QueueConstants.ACTN_ERROR);
				return forward;
			}
		}
		
	}

	/**
	 * Obtains csr id
	 * 
	 * @param securityToken - session id of the current user
	 * @return String - csr id
     * @throws SQLException
     * @throws Exception
	 */
	private String getCsrId(String securityToken) 
		throws SQLException, Exception
	{
		SecurityManager sm = SecurityManager.getInstance();
		return sm.getUserInfo(securityToken).getUserID();
	}
    
    private String generateDataFilerParams(HttpServletRequest request, HashMap timerResults, String queueType, String virtualQueueType) throws Exception
    {
      if (virtualQueueType != null && !virtualQueueType.equalsIgnoreCase(EMPTY_STRING))
      {
          queueType = virtualQueueType;
      }
      String dataFilterParams = "&" + QueueConstants.TIMER_ENTITY_HISTORY_ID + "=" + (String)timerResults.get(QueueConstants.TIMER_ENTITY_HISTORY_ID)
                              + "&" + QueueConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + (String)timerResults.get(QueueConstants.TIMER_COMMENT_ORIGIN_TYPE)
                              + "&" + QueueConstants.START_ORIGIN + "=" + (String)timerResults.get(QueueConstants.TIMER_COMMENT_ORIGIN)
                              + "&" + QueueConstants.SEC_TOKEN + "=" + request.getParameter(QueueConstants.SEC_TOKEN)
                              + "&" + QueueConstants.SEC_CONTEXT + "=" + request.getParameter(QueueConstants.SEC_CONTEXT)
                              + "&" + QueueConstants.RTQ_FLAG + "=" + this.YES
                              + "&" + QueueConstants.RTQ_QUEUE_TYPE + "=" + queueType
                              + "&" + QueueConstants.RTQ_QUEUE_IND + "=" + request.getParameter(R_QUEUE_IND)
                              + "&" + QueueConstants.RTQ_ATTACHED + "=" + request.getParameter(R_ATTACHED)
                              + "&" + QueueConstants.RTQ_TAGGED + "=" + request.getParameter(R_TAGGED)
                              + "&" + QueueConstants.RTQ_USER + "=" + request.getParameter(R_USER)
                              + "&" + QueueConstants.RTQ_GROUP + "=" + request.getParameter(R_GROUP);

      String sortColumn = request.getParameter(R_SORT_COLUMN);
      String maxRecords = request.getParameter(R_MAX_RECORDS);
      String currentSortDirection = request.getParameter(R_CURRENT_SORT_DIRECTION);
      String position = request.getParameter(R_POSITION);
      String receivedDate = request.getParameter(R_RECEIVED_DATE_FILTER);
      String deliveryDate = request.getParameter(R_DELIVERY_DATE_FILTER);
      String tagDate = request.getParameter(R_TAG_DATE_FILTER);
      String lastTouched = request.getParameter(R_LASTTOUCHED_FILTER);
      String type = request.getParameter(R_TYPE_FILTER);
      String sys = request.getParameter(R_SYS_FILTER);
      String timezone = request.getParameter(R_TIMEZONE_FILTER);
      String sdg = request.getParameter(R_SDG_FILTER);
      String disposition = request.getParameter(R_DISPOSITION_FILTER);
      String priority = request.getParameter(R_PRIORITY_FILTER);
      String newItem = request.getParameter(R_NEW_ITEM_FILTER);
      String occasion = request.getParameter(R_OCCASION_FILTER);
      String location = request.getParameter(R_LOCATION_FILTER);
      String memberNum = request.getParameter(R_MEMBER_NUMBER_FILTER);
      String zip = request.getParameter(R_ZIP_CODE_FILTER);
      String tagBy = request.getParameter(R_TAGBY_FILTER);
      String filter = request.getParameter(R_FILTER);

      boolean sortOnReturn = (sortColumn!=null && !sortColumn.equalsIgnoreCase(""))?true:false;

      dataFilterParams  += "&" + QueueConstants.RTQ_SORT_COLUMN + "=" + sortColumn
                        +  "&" + QueueConstants.RTQ_MAX_RECORDS + "=" + maxRecords
                        +  "&" + QueueConstants.RTQ_CURRENT_SORT_DIRECTION + "=" + currentSortDirection
                        +  "&" + QueueConstants.RTQ_POSITION + "=" + position
                        +  "&" + QueueConstants.RTQ_SORT_ON_RETURN + "=" + (new Boolean(sortOnReturn)).toString()
                        +  "&" + QueueConstants.RTQ_RECEIVED_DATE + "=" + receivedDate
                        +  "&" + QueueConstants.RTQ_DELIVERY_DATE + "=" + deliveryDate
                        +  "&" + QueueConstants.RTQ_TAG_DATE + "=" + tagDate
                        +  "&" + QueueConstants.RTQ_LAST_TOUCHED + "=" + lastTouched
                        +  "&" + QueueConstants.RTQ_TYPE + "=" + type
                        +  "&" + QueueConstants.RTQ_SYS + "=" + sys
                        +  "&" + QueueConstants.RTQ_TIMEZONE + "=" + timezone
                        +  "&" + QueueConstants.RTQ_SDG + "=" + sdg
                        +  "&" + QueueConstants.RTQ_DISPOSITION + "=" + disposition
                        +  "&" + QueueConstants.RTQ_PRIORITY + "=" + priority
                        +  "&" + QueueConstants.RTQ_NEW_ITEM + "=" + newItem
                        +  "&" + QueueConstants.RTQ_OCCASION + "=" + occasion
                        +  "&" + QueueConstants.RTQ_LOCATION + "=" + location
                        +  "&" + QueueConstants.RTQ_MEMBER_NUMBER + "=" + memberNum
                        +  "&" + QueueConstants.RTQ_ZIP + "=" + zip
                        +  "&" + QueueConstants.RTQ_TAG_BY + "=" + tagBy 
                        +  "&" + QueueConstants.RTQ_FILTER + "=" + filter;
      return dataFilterParams;
    }

	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
	 * @return Connection - db connection
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     * @throws Exception
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(QueueConstants.PROPERTY_FILE,
															   QueueConstants.DATASOURCE_NAME));

		return conn;
	}

}