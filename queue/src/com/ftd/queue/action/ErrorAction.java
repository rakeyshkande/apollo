package com.ftd.queue.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.queue.constants.QueueConstants;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class redirects the user to the Error or Login page.. 
 *
 * @author Mike Kruger
 */
public final class ErrorAction extends Action 
{
	private static    Logger logger  = new Logger("com.ftd.queue.action.ErrorAction");

    //request parameters:
	private static final String R_ACTION = "action";
	private static final String ACTION_LOGIN = "login";

    //forward names:
    private static final String LOGIN_REQUEST = "LoginPage";
    private static final String ERROR_REQUEST = "ErrorPage";

    /**
     * This is the Error Action called from the Struts framework.
	 * The execute method redirects to the Error or Login page.
     * 
     * @param mapping  - ActionMapping used to select this instance
     * @param form     - ActionForm (optional) bean for this request
     * @param request  - HTTP Request we are processing
     * @param response - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
		HttpServletResponse response) 
        throws IOException, ServletException		 
	{		
		ActionForward forward = null;
		String action = null;
		
		try
		{
			action = request.getParameter(R_ACTION);
		    
			/* If action is equal to login take the user to the login page
			 * else take the user to the error page.
			 */
			if(action != null && action.trim().equals(ACTION_LOGIN))
			{
				try
				{
                                        ConfigurationUtil cu = ConfigurationUtil.getInstance();
                                        String loginUrl = cu.getFrpGlobalParm(QueueConstants.QUEUE_CONFIG_CONTEXT,"login_page");
                                        
                                        loginUrl = loginUrl + "/security/Login";
//					forward = mapping.findForward(LOGIN_REQUEST);
					forward = new ActionForward(loginUrl, true);
				}
				catch(Throwable t)
				{
					forward = mapping.findForward(ERROR_REQUEST);				
				}
			}
			else
			{
				forward = mapping.findForward(ERROR_REQUEST);
			}
		}
		catch(Throwable t)
		{
			logger.error(t);			
		}
		
		return forward;
    }
    
}
