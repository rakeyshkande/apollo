package com.ftd.queue.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.queue.bo.QueueRequestBO;
import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.util.Timer;
import com.ftd.queue.vo.QueueRequestVO;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;


/**
 * This class is used to process any queue request.  It contains the main flow of logic
 * needed for XSL stylesheet determination, obtaining a database connection, and passing information
 * to a business object to obtain queue data.  The queue data received, XSL file, HttpServletRequest,
 * HttpServletResponse, and security parameters are then passed to the TraxUtil class for
 * transformation.
 *
 * @author Matt Wilcoxen
 */

public class QueueRequestAction extends Action
{
	private Logger logger = new Logger("com.ftd.queue.action.QueueRequestAction");

  //queue request xsl filenames
  private static final String TAGGED_MERCURY_QUEUE_XSL = "TaggedMercuryQueueXSL";
  private static final String TAGGED_MERCURY_LP_QUEUE_XSL = "TaggedMercuryLPQueueXSL";
  private static final String UNTAGGED_MERCURY_QUEUE_XSL = "UntaggedMercuryQueueXSL";
  private static final String UNTAGGED_MERCURY_LP_QUEUE_XSL = "UntaggedMercuryLPQueueXSL";
  private static final String TAGGED_EMAIL_ATTACHED_QUEUE_XSL = "TaggedEmailAttachedQueueXSL";
  private static final String TAGGED_EMAIL_UNATTACHED_QUEUE_XSL = "TaggedEmailUnattachedQueueXSL";
  private static final String UNTAGGED_EMAIL_ATTACHED_QUEUE_XSL = "UntaggedEmailAttachedQueueXSL";
  private static final String UNTAGGED_EMAIL_UNATTACHED_QUEUE_XSL = "UntaggedEmailUnattachedQueueXSL";

  //request parameters:
  private static final String R_QUEUE_TYPE = "queue_type";
  private static final String R_QUEUE_DESC = "queue_desc";
  private static final String R_QUEUE_IND = "queue_ind";
  private static final String R_TAGGED = "tagged";
  private static final String R_ATTACHED = "attached";
  private static final String R_USER = "user";
  private static final String R_GROUP = "group";
  private static final String R_SORT_COLUMN = "sort_column";
  private static final String R_SORT_ORDER_STATE = "sort_order_state";
  private static final String R_SORT_REQUESTED = "sort_requested";
  private static final String R_POSITION = "position";
  private static final String R_MAX_RECORDS = "max_records";
  private static final String R_CURRENT_SORT_DIRECTION = "current_sort_direction";
  private static final String R_SORT_ON_RETURN = "sort_on_return";
  private static final String R_FILTER = "filter";
  private static final String R_MESSAGE_TIMESTAMP_FILTER = "received_date_filter";
  private static final String R_DELIVERY_DATE_FILTER = "delivery_date_filter";
  private static final String R_TAGGED_ON_FILTER = "tag_date_filter";
  private static final String R_LAST_TOUCHED_FILTER = "lasttouched_filter";
  private static final String R_MESSAGE_TYPE_FILTER = "type_filter";
  private static final String R_SYSTEM_FILTER = "sys_filter";
  private static final String R_TIMEZONE_FILTER = "timezone_filter";
  private static final String R_SAME_DAY_GIFT_FILTER = "sdg_filter";
  private static final String R_PRODUCT_ID_FILTER = "product_id_filter";
  private static final String R_TAG_DISPOSITION_FILTER ="disposition_filter";
  private static final String R_TAG_PRIORITY_FILTER = "priority_filter";
  private static final String R_UNTAGGED_NEW_ITEM_FILTER = "new_item_filter";
  private static final String R_OCCASION_ID_FILTER = "occasion_filter";
  private static final String R_DELIVERY_LOCATION_TYPE_FILTER = "location_filter";
  private static final String R_MEMBER_NUMBER_FILTER = "member_number_filter";
  private static final String R_ZIP_CODE_FILTER = "zip_code_filter";
  private static final String R_TAGGED_BY_FILTER = "tagby_filter";
  private static final String R_CONTEXT = "context";
  private static final String R_SESSION_ID = "securitytoken";

    private static final String X_QUEUE_REQUEST = "queue_request";

	/**
	 * This is the Queue Menu Action called from the Struts framework.
     *     1. build queue request VO
     *     2. get db connection
     *     3. get queue request DOM
     *     4. build queue-request node to add to queue request DOM
     *     5. get queue request XSL file
     *     6. save parameters onto request object
     *     7. transfer to queue request
	 *
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form,
                                HttpServletRequest request, HttpServletResponse response)
                 throws IOException, ServletException, Exception
	{
    Connection conn = null;
    Document queue = null;
    ActionForward forward = null;
    QueueRequestVO qrVO = new QueueRequestVO();
    
	try	{
      // 1. get db connection
			conn = getDBConnection();
			QueueRequestBO qrBO = new QueueRequestBO(conn);

      // 2. if a queue timer exists, stop the timer
			String entityHistoryId = (String)request.getParameter(QueueConstants.TIMER_ENTITY_HISTORY_ID);
			if(entityHistoryId != null)
			{
				Timer timer = new Timer(conn);
				timer.stopTimer(entityHistoryId);

				/* Remove the queue timer information from the parameters Map
				   This is just a precaution in case the Queue Timer information
				   is passed within the Data Filter.
				*/
				HashMap parameters = (HashMap) request.getAttribute(QueueConstants.APP_PARAMETERS);
				parameters.remove(QueueConstants.TIMER_ENTITY_HISTORY_ID);
				parameters.remove(QueueConstants.TIMER_COMMENT_ORIGIN_TYPE);
			}

      // 3. build queue request VO.  If filter = Y, call method with filter.  
      // Otherwise, call method without filter.
			qrVO = buildQueueRequestVO(request);

      // 4. get queue request DOM
       
           queue = qrBO.getQueueData(qrVO);
           
       
      // 5. build queue-request node to add to queue request DOM
      buildQueueRequestXMLDom(queue, qrVO);

      // 6. get queue request XSL file
			String xslName = getXSLName(qrVO);
			File xslFile = getXSLFile(mapping,request,xslName);

      // 7. save parameters onto request object
			request.setAttribute("QueueRequest",queue);
			request.setAttribute("Security","Security");
			request.setAttribute("QueueRequestXSL",xslFile);

      // 8. transfer to queue request
      forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name

      // Change to client side transform
     TraxUtil.getInstance().getInstance().transform(request, response, queue,
                        xslFile, xslFilePathAndName, (HashMap)request.getAttribute(QueueConstants.APP_PARAMETERS));

      return null;
		}
		catch (ClassNotFoundException  cnfe)
		{
			logInput(qrVO, queue);
      logger.error(cnfe);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		catch (IOException  ioe)
		{
			logInput(qrVO, queue);
			logger.error(ioe);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		catch (ParserConfigurationException  pcex)
		{
			logInput(qrVO, queue);
			logger.error(pcex);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		catch (SAXException  sec)
		{
			logInput(qrVO, queue);
			logger.error(sec);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		catch (SQLException  sqle)
		{
			logInput(qrVO, queue);
			logger.error(sqle);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		catch (TransformerException  tex)
		{
			logInput(qrVO, queue);
			logger.error(tex);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		catch (Throwable  t)
		{
			logInput(qrVO, queue);
			logger.error(t);
			forward = mapping.findForward(QueueConstants.ACTN_ERROR);
		}
		finally
		{
			try
			{
				if(conn != null)
				{
					conn.close();
				}
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward(QueueConstants.ACTN_ERROR);
			}
		}

        return forward;
	}


	/**
	 * Obtain connectivity with the database
	 *
	 * @param none
	 * @return Connection - db connection
	 * @throws ClassNotFoundException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     * @throws Exception
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException,
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
                ConfigurationUtil.getInstance().getProperty(QueueConstants.PROPERTY_FILE,
								QueueConstants.DATASOURCE_NAME));

		return conn;
	}


	/**
	 * build queue request value object
     *     1. get queue request VO elements from request
     *     2. if sort order state is specified, convert sort order state to xml format
     *     3. save elements in queue request VO
	 *
	 * @param  HttpServletRequets - user request information
	 * @return QueueRequestVO     - Queue Request value object
	 * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws XSLException
     * @throws TransformerException
	 */
	private QueueRequestVO buildQueueRequestVO(HttpServletRequest request)
        throws IOException, ParserConfigurationException, SAXException,
               TransformerException
	{
    ConfigurationUtil cu = ConfigurationUtil.getInstance();
    QueueRequestVO qrVO = new QueueRequestVO();

    /***************************************************************************
    // 1. get queue request VO elements from request
    ***************************************************************************/
    String queueType            = request.getParameter(R_QUEUE_TYPE);
    String queueDesc            = request.getParameter(R_QUEUE_DESC);
    String queueInd             = request.getParameter(R_QUEUE_IND);
    String sTagged              = request.getParameter(R_TAGGED);
    String sAttEmail            = request.getParameter(R_ATTACHED);
    String user                 = request.getParameter(R_USER);
    String groupId              = request.getParameter(R_GROUP);
    String sortColumn           = request.getParameter(R_SORT_COLUMN);
    String maxRecords           = request.getParameter(R_MAX_RECORDS);
    String currentSortDirection = request.getParameter(R_CURRENT_SORT_DIRECTION);
    String sortOnReturn         = request.getParameter(R_SORT_ON_RETURN);
    String sSortRequested       = request.getParameter(R_SORT_REQUESTED);
    String sortOrderState       = request.getParameter(R_SORT_ORDER_STATE);
    String position             = request.getParameter(R_POSITION);
    // FILTERS //
    String filter               = request.getParameter(R_FILTER);
    // CALENDERS FILTERS//
    Date messageTimestampFilter= parseCalendarFilterValue(request.getParameter(R_MESSAGE_TIMESTAMP_FILTER));
    Date deliveryDateFilter   = parseCalendarFilterValue(request.getParameter(R_DELIVERY_DATE_FILTER));
    Date taggedOnFilter       = parseCalendarFilterValue(request.getParameter(R_TAGGED_ON_FILTER));
    Date lastTouchedFilter    = parseCalendarFilterValue(request.getParameter(R_LAST_TOUCHED_FILTER));
    // DROPDOWNS FILTERS //
    String systemFilter         = parseDropdownFilterValue(request.getParameter(R_SYSTEM_FILTER));
    String timezoneFilter       = parseDropdownFilterValue(request.getParameter(R_TIMEZONE_FILTER));
    String sameDayGiftFilter    = parseDropdownFilterValue(request.getParameter(R_SAME_DAY_GIFT_FILTER));
    String tagDispositionFilter = parseDropdownFilterValue(request.getParameter(R_TAG_DISPOSITION_FILTER));
    String tagPriorityFilter    = parseDropdownFilterValue(request.getParameter(R_TAG_PRIORITY_FILTER));
    String untaggedNewItemFilter= parseDropdownFilterValue(request.getParameter(R_UNTAGGED_NEW_ITEM_FILTER));
    String occasionIdFilter     = parseDropdownFilterValue(request.getParameter(R_OCCASION_ID_FILTER));
    String deliveryLocationTypeFilter = parseDropdownFilterValue(request.getParameter(R_DELIVERY_LOCATION_TYPE_FILTER));
    String messageTypeFilter    = parseDropdownFilterValue(request.getParameter(R_MESSAGE_TYPE_FILTER));
    // FREE FORM FILTERS //
    String memberNumberFilter   = parseFreeformFilterValue(StringUtils.trimToNull(request.getParameter(R_MEMBER_NUMBER_FILTER)));
    String zipCodeFilter        = parseFreeformFilterValue(StringUtils.trimToNull(request.getParameter(R_ZIP_CODE_FILTER)));
    String taggedByFilter       = parseFreeformFilterValue(StringUtils.trimToNull(request.getParameter(R_TAGGED_BY_FILTER)));
    String context              = request.getParameter(R_CONTEXT);
    String sessionId            = request.getParameter(R_SESSION_ID);
    String productIdFilter      = parseFreeformFilterValue(StringUtils.trimToNull(request.getParameter(R_PRODUCT_ID_FILTER)));

    /***************************************************************************
    // 2. if specified, convert sort order state to xml format
    ***************************************************************************/
    Document xmlDoc = null;
    Node sortOrderStateNode = null;

    if(sortOrderState != null  &&  sortOrderState.trim().length() > 0)
    {
      sortOrderState  = sortOrderState.replaceAll("&lt;","<");
      sortOrderState  = sortOrderState.replaceAll("&gt;",">");
      xmlDoc = DOMUtil.getDocument(sortOrderState);
      sortOrderStateNode = xmlDoc.getFirstChild();
    }

    /***************************************************************************
    // 3. massage the data for it to be saved in the VO.
    ***************************************************************************/
    boolean tagged        = sTagged.equalsIgnoreCase("true") ? true : false;
    boolean attEmail      = sAttEmail.equalsIgnoreCase("true") ? true : false;
    boolean sortRequested = false;
    boolean bSortOnReturn = false;

    if(sSortRequested != null  &&  sSortRequested.trim().length() > 0)
      sortRequested  = sSortRequested.equalsIgnoreCase("true") ? true : false;
    if (sortOnReturn !=null && sortOnReturn.trim().length() > 0)
      bSortOnReturn = sortOnReturn.equalsIgnoreCase("true") ? true : false;

    /***************************************************************************
    // 4. save elements in queue request VO
    ***************************************************************************/
		qrVO.setAttEmail(attEmail);
    qrVO.setCurrentSortDirection(currentSortDirection);
		qrVO.setGroup(groupId);
    qrVO.setMaxRecords(maxRecords);
    qrVO.setPosition(position);
		qrVO.setQueueDescription(queueDesc);
		qrVO.setQueueInd(queueInd);
		qrVO.setQueueType(queueType);
    qrVO.setSortColumn(sortColumn);
    qrVO.setSortOnReturn(bSortOnReturn);
    if(sortOrderState != null  &&  sortOrderState.trim().length() > 0)
      qrVO.setSortOrderState(sortOrderStateNode);
    if (bSortOnReturn)
      qrVO.setSortRequested(true);
    else
      qrVO.setSortRequested(sortRequested);
		qrVO.setTagged(tagged);
		qrVO.setUser(user);

    // set filters //
    qrVO.setFilter(filter);
    // CALENDARS FILTERS //
    qrVO.setMessageTimestampFilter(messageTimestampFilter);
    qrVO.setDeliveryDateFilter(deliveryDateFilter);
    qrVO.setTaggedOnFilter(taggedOnFilter);
    qrVO.setLastTouchedFilter(lastTouchedFilter);
    // DROPDOWNS FILTERS //
    qrVO.setSystemFilter(systemFilter);
    qrVO.setTimezoneFilter(timezoneFilter);
    qrVO.setSameDayGiftFilter(sameDayGiftFilter);
    qrVO.setTagDispositionFilter(tagDispositionFilter);
    qrVO.setTagPriorityFilter(tagPriorityFilter);
    qrVO.setUntaggedNewItemFilter(untaggedNewItemFilter);
    qrVO.setOccasionIdFilter(occasionIdFilter);
    qrVO.setDeliveryLocationTypeFilter(deliveryLocationTypeFilter);
    // FREE FORM FILTERS //
    qrVO.setMessageTypeFilter(messageTypeFilter);
    qrVO.setMemberNumberFilter(memberNumberFilter);
    qrVO.setZipCodeFilter(zipCodeFilter);
    qrVO.setTaggedByFilter(taggedByFilter);

    // security values
    qrVO.setContext(context);
    qrVO.setSessionId(sessionId);
    qrVO.setProductIdFilter(productIdFilter);

    return qrVO;
  }


	/**
	 * build queue request node for queue request document
     *     1. build queue request node
     *            <queue_request>
     *                <queue_type>/queue_type>
     *                <queue_ind></queue_ind>
     *                <tagged></tagged>
     *                <attached></attached>
     *                <user></user>
     *                <group></group>
     *            </queue_request>
     *     2. add user text to user node, if specified
     *     3. add group text to group node, if specified
     *     4. add queue request node to queue request document
	 *
	 * @param Document       - queue request document
	 * @param QueueRequestVO - queue request object
	 * @return Document      - updated queue request DOM
	 * @throws IOException
     * @throws ParserConfigurationException
	 */
	private Document buildQueueRequestXMLDom(Document queueDoc, QueueRequestVO qrVO)
        throws IOException, ParserConfigurationException, Exception
    {

        // 1. build queue request node
        Element queueEle = queueDoc.createElement(X_QUEUE_REQUEST);

        Element newEle = queueDoc.createElement(R_QUEUE_TYPE);
        Text newText = queueDoc.createTextNode(qrVO.getQueueType());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);

        newEle = queueDoc.createElement(R_QUEUE_IND);
        newText = queueDoc.createTextNode(qrVO.getQueueInd());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);

        newEle = queueDoc.createElement(R_TAGGED);
        newText = queueDoc.createTextNode(new Boolean(qrVO.getTagged()).toString());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);

        newEle = queueDoc.createElement(R_ATTACHED);
        newText = queueDoc.createTextNode(new Boolean(qrVO.getAttEmail()).toString());
        newEle.appendChild(newText);
        queueEle.appendChild(newEle);

        // 2. add user text to user node, if specified
        newEle = queueDoc.createElement(R_USER);
        if(qrVO.getUser() != null  &&  qrVO.getUser().trim().length() > 0)
        {
            newText = queueDoc.createTextNode(qrVO.getUser());
            newEle.appendChild(newText);
        }
        queueEle.appendChild(newEle);

        // 3. add group text to group node, if specified
        newEle = queueDoc.createElement(R_GROUP);
        if(qrVO.getGroupId() != null  &&  qrVO.getGroupId().trim().length() > 0)
        {
            newText = queueDoc.createTextNode(qrVO.getGroupId());
            newEle.appendChild(newText);
        }
        queueEle.appendChild(newEle);

        // 4. add queue request node to queue request document
        queueDoc.getDocumentElement().appendChild(queueEle);

        return queueDoc;
    }


	/**
	 * Retrieve name of Queue Request forward
     *     1. get file lookup name
	 *
	 * @param QueueRequestVO     - queue request value object
	 * @return                   - name of the forward to use
	 * @throws none
	 */
	public String getXSLName(QueueRequestVO qrVO)
	{
		String XSLFileLookUpName = null;
		String XSLFileName = null;

        // 1. get file lookup name from VO:  works for Order, User and Group
        XSLFileLookUpName = qrVO.getQueueInd();

		if(qrVO.getQueueInd().equalsIgnoreCase(QueueConstants.MERCURY_QUEUE))
		{
			if(qrVO.getTagged())
			{
                if(qrVO.getQueueType().equalsIgnoreCase(QueueConstants.MERCURY_LP_QUEUE))
                {
                    XSLFileLookUpName = TAGGED_MERCURY_LP_QUEUE_XSL;
                }
                else
                {
    				XSLFileLookUpName = TAGGED_MERCURY_QUEUE_XSL;
                }
			}
			else
			{
                if(qrVO.getQueueType().equalsIgnoreCase(QueueConstants.MERCURY_LP_QUEUE))
                {
                    XSLFileLookUpName = UNTAGGED_MERCURY_LP_QUEUE_XSL;
                }
                else
                {
    				XSLFileLookUpName = UNTAGGED_MERCURY_QUEUE_XSL;
                }
			}
		}

		if(qrVO.getQueueInd().equalsIgnoreCase(QueueConstants.EMAIL_QUEUE))
		{
			if(qrVO.getTagged())
			{
				if(qrVO.getAttEmail())
				{
					XSLFileLookUpName = TAGGED_EMAIL_ATTACHED_QUEUE_XSL;
				}
				else
				{
					XSLFileLookUpName = TAGGED_EMAIL_UNATTACHED_QUEUE_XSL;
				}
			}
			else
			{
				if(qrVO.getAttEmail())
				{
					XSLFileLookUpName = UNTAGGED_EMAIL_ATTACHED_QUEUE_XSL;
				}
				else
				{
					XSLFileLookUpName = UNTAGGED_EMAIL_UNATTACHED_QUEUE_XSL;
				}
			}
		}
		return XSLFileLookUpName;
	}

	/**
	 * Retrieve name and location of Queue Request XSL file
     *     1. get real file name
     *     2. get xsl file location
	 *
	 * @param ActionMapping      - Stuts mapping used for actionforward lookup
     * @param HttpServletRequest - user request
	 * @return File              - Queue Request XSL File name with real path
	 * @throws none
	 */
	public File getXSLFile(ActionMapping mapping, HttpServletRequest request, String xslFileLookUpName)
	{
		String xslFileName;
		File xslFile = null;

    // 1. get real file name
    ActionForward forward = mapping.findForward(xslFileLookUpName);
    xslFileName = forward.getPath();

    // 2. get xsl file location
    xslFile = new File(request.getSession().getServletContext().getRealPath(xslFileName));

    return xslFile;
  }


	/**
	 * Logs the output in the log file
	 *
	 * @param QueueRequestVO     - Vo that contains the dataStuts mapping used for actionforward lookup
	 * @document
	 * @throws none
	 */
	public void logInput(QueueRequestVO qrVO, Document queue)
	{
    logger.error("QueueType = " + qrVO.getQueueType());
    logger.error("QueueDesc = " + qrVO.getQueueDescription());
    logger.error("QueueInd = " + qrVO.getQueueInd());
    logger.error("Tagged = " + qrVO.getTagged());
    logger.error("AttachedEMail = " + qrVO.getAttEmail());
    logger.error("CsrId = " + qrVO.getUser());
    logger.error("Group = " + qrVO.getGroupId());
    logger.error("SortColumn = " + qrVO.getSortColumn());

    StringWriter sw = null;

    try
    {
      if (qrVO.getSortOrderState() != null)
      {
        NodeList nl = ((Element)qrVO.getSortOrderState()).getChildNodes();
        Document doc = DOMUtil.getDocument();
        DOMUtil.addSection(doc,nl);
        sw = new StringWriter();
        DOMUtil.print(doc, new PrintWriter(sw));
        logger.error("SortOrder = " + sw.toString());
      }
    }
    catch(Exception e)
    {
      logger.error("Sort Order could not be printed to the log file");
      logger.error(e);
    }

    try
    {
      if (queue!=null)
      {
        sw = new StringWriter();
        DOMUtil.print(queue, new PrintWriter(sw));
        logger.error("execute(): queue request DOM = \n" + sw.toString());
      }
    }
    catch(Exception e)
    {
      logger.error("Return results could not be printed to the log file");
      logger.error(e);
    }

    
  }

  /**
   * parseDropdownFilterValue
   * The method allows us to parse the difference between filtering on blank and
   * no filter. 
   */
  private String parseDropdownFilterValue(String dropdownValue){
      // if dropdown value is null, return null
      if (dropdownValue == null){
          return null;
      }// if dropdown value is "No Filter" or "null", return null 
      else if (dropdownValue.equalsIgnoreCase(QueueConstants.NO_FILTER) || dropdownValue.equalsIgnoreCase("null")){
          return null;
      }// if dropdown value is blank, return a space.
      else if (dropdownValue.equalsIgnoreCase("") || dropdownValue.equalsIgnoreCase(" ")){
          return " ";
      }
      return dropdownValue;
  }

    private Date parseCalendarFilterValue(String calendarValue){
        // if dropdown value is null or blank, return null
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        if (calendarValue == null || calendarValue.equalsIgnoreCase("")){
            return null;
        }// if dropdown value is "No Filter", return null 
        else{
            try {
                java.util.Date utilDate = sdf.parse(calendarValue);
                return new Date(utilDate.getTime());
            }
            catch(Exception e){
                // if the parse fails, return null
                logger.debug("Parse of calendar filter value "+calendarValue+" failed.");
                return null;
            }
            
        }// if dropdown value is blank, return a space.
    }

    private String parseFreeformFilterValue(String freeformValue){
             // if dropdown value is null, return null
        if (freeformValue == null){
            return null;
        }// if dropdown value is "No Filter" or "null", return null 
        else if (freeformValue.equalsIgnoreCase("null")){
            return null;
        }// if dropdown value is blank, return a space.
        return freeformValue;
    }


}