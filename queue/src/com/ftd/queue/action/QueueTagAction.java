package com.ftd.queue.action;

import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.dao.QueueTagDAO;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * This class processes a Queue Tag request.  It contains the main flow of logic needed
 * to obatain a database connection, call a data access object to tag a queue item to a 
 * CSR, XSL stylesheet determination, and transformation of the data received via xsl.
 *
 * @author Matt Wilcoxen
 */

public class QueueTagAction extends Action 
{
    private Logger logger = new Logger("com.ftd.queue.action.QueueTagAction");

    private static final String QUEUE_TAG_ACTION_XSL = "QueueTagActionXSL";

    //XML nodes:
    private static final String X_OUT_PARAMETERS = "out-parameters";
    private static final String X_TAG_STATUS = "TAG_STATUS";
    private static final String X_TAGS_CREATED_CNT = "TAGS_CREATED_CNT";
    private static final String X_ERR_MSG = "ERR_MSG";

    //request parameters:
	private static final String R_MESSAGE_ID = "message_id";

    private static final String ERROR_PAGE = "errorPage";

    /**
     * This is the Queue Tag Action called from the Struts framework.
     * 
     * @param mapping  - ActionMapping used to select this instance
     * @param form     - ActionForm (optional) bean for this request
     * @param request  - HTTP Request we are processing
     * @param response - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, 
            HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        Connection conn = null;
        Document taggedCntXmlDoc = null;
        String callPage = null;
        ActionForward forward = null;
        String errorMessage = null;
        File XSLFile = getXSL(mapping, request);             //get XSL file name

        UserInfo ui = null;
        String csrId = null;

        try
        {
            // 1. get security manager information
            String message_id  = request.getParameter(R_MESSAGE_ID);
            String sec_context = request.getParameter(QueueConstants.SEC_CONTEXT);
            String sec_token   = request.getParameter(QueueConstants.SEC_TOKEN);

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String security = (String) cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.SECURITY_IS_ON);
            boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
            if(securityIsOn)
            {
                SecurityManager sm = SecurityManager.getInstance();
                ui = sm.getUserInfo(sec_token);
                csrId = ui.getUserID();
            }

            // 2. tag queue items to csr
            conn = getDBConnection();
            QueueTagDAO qtDAO = new QueueTagDAO(conn);
        	taggedCntXmlDoc = qtDAO.insertTag(message_id,csrId,sec_context,QueueConstants.PERMISSION_UPDATE);

            if(logger.isDebugEnabled())
            {
                //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
                //            method is executed, make StringWriter go away through scope control.
                //            Both the flush and close methods were tested with but were of none affect.
                StringWriter sw = new StringWriter();       //string representation of xml document
                DOMUtil.print(taggedCntXmlDoc, new PrintWriter(sw));
                logger.debug("execute(): queue tag data from database = \n" + sw.toString());
                //for printing to the console --> taggedCntXmlDoc.print(System.out);
            }

            // 3. log error message if applicable
            ArrayList al = new ArrayList();
            al.add(X_OUT_PARAMETERS);
            al.add(X_ERR_MSG);
            String error_msg = DOMUtil.getNodeValue(taggedCntXmlDoc,al);
            if(error_msg != null  &&  error_msg.trim().length() > 0)
            {
                logger.warn("QueueTagAction: \n<ERR_MSG> is: " + error_msg);
            }
            
            forward = mapping.findForward(QUEUE_TAG_ACTION_XSL);
            String xslFilePathAndName = forward.getPath(); //get real file name
          
            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, taggedCntXmlDoc, 
                              XSLFile, xslFilePathAndName, null);

            return null;
        }
        catch (ClassNotFoundException  cnfe)
        {
            errorMessage = cnfe.getMessage();
            logger.error(cnfe);
            callPage = ERROR_PAGE;
        }
        catch (IOException  ioe)
        {
            errorMessage = ioe.getMessage();
            logger.error(ioe);
            callPage = ERROR_PAGE;
        }
        catch (ParserConfigurationException  pcex)
        {
            errorMessage = pcex.getMessage();
            logger.error(pcex);
            callPage = ERROR_PAGE;
        }
        catch (SAXException  sec)
        {
            errorMessage = sec.getMessage();
            logger.error(sec);
            callPage = ERROR_PAGE;
        }
        catch (SQLException  sqle)
        {
            errorMessage = sqle.getMessage();
            logger.error(sqle);
            callPage = ERROR_PAGE;
        }
        catch (TransformerException  tex)
        {
            errorMessage = tex.getMessage();
            logger.error(tex);
            callPage = ERROR_PAGE;
        }
        catch (Throwable  t)
        {
            errorMessage = t.getMessage();
            logger.error(t);
            callPage = ERROR_PAGE;
        }
        finally
        {
            try
            {
				if(conn != null)
				{
	                conn.close();
				}
                if(callPage != null  &&  callPage.equalsIgnoreCase(ERROR_PAGE))
                {
                    Document xmlDoc = createOutParms(errorMessage);
                    
                    forward = mapping.findForward(QUEUE_TAG_ACTION_XSL);
                    String xslFilePathAndName = forward.getPath(); //get real file name
                  
                    // Change to client side transform
                    TraxUtil.getInstance().getInstance().transform(request, response, taggedCntXmlDoc, 
                                      XSLFile, xslFilePathAndName, null);
                    return null;
                }
            }
            catch (SQLException  se)
            {
                logger.error(se);
            }
            catch (ParserConfigurationException  pcex)
            {
                logger.error(pcex);
            }
            catch (TransformerException  tex)
            {
                logger.error(tex);
            }
            catch (Exception  ex)
            {
                logger.error(ex);
            }
        }

		return null;
    }


    /**
     * Obtain connectivity with the database
     * 
     * @param none
     * @return Connection - db connection
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    private Connection getDBConnection()
        throws ClassNotFoundException, SQLException, Exception
    {
        Connection conn;
        conn = DataSourceUtil.getInstance().getConnection(
                   ConfigurationUtil.getInstance().getProperty(QueueConstants.PROPERTY_FILE,
                                                               QueueConstants.DATASOURCE_NAME));

        return conn;
    }


    /**
     * Retrieve name of Queue Tag XSL file
     * 
     * @param ActionMapping      - Struts action mapping
     * @param HttpServletRequest - user request information
     * @return File              - Queue Tag XSL File name
     * @throws none
     */
    public File getXSL(ActionMapping mapping, HttpServletRequest request)
    {
        File XSLFile;
        String XSLFileName = mapping.findForward(QUEUE_TAG_ACTION_XSL).getPath();

        XSLFile = new File(this.servlet.getServletContext().getRealPath(XSLFileName));

        return XSLFile;
    }


    /**
     * Due to an exception occuring earlier in the program, create a limited set of out parameters.
     *     <out-parameters>
     *         <TAG_STATUS>N</TAG_STATUS>
     *         <ERR_MSG>error-specific text</ERR_MSG>
     *     </out-parameters>
     * 
     *     1. create default document
     *     2. create error indicator element
     *     3. create error message element
     *
     * @param none    -
     * @return Object - Document of out-parameters of error page
     * @throws ParserConfigurationException
     */
    public Document createOutParms(String errMsg)
        throws ParserConfigurationException
    {
        Document xmlDoc = null;

        // 1. create default document
        xmlDoc = DOMUtil.getDefaultDocument();
        Element eleRoot = xmlDoc.createElement(X_OUT_PARAMETERS);
        xmlDoc.appendChild(eleRoot);
        
        // 2. create error indicator element
        createDocElement(xmlDoc, X_TAG_STATUS, "N");

        // 3. create error message element
        createDocElement(xmlDoc, X_ERR_MSG, errMsg);

        return xmlDoc;
    }


    /**
     * Add a particular element and value to a passed document.
     *
     * @param none         -
     * @return XMLDocument - updated Document of out-parameters
     * @throws none
     */
    public Document createDocElement(Document xmlDoc, String eleName, String eleText)
    {
        Element newEle = xmlDoc.createElement(eleName);
        Text newText = xmlDoc.createTextNode(eleText);
        newEle.appendChild(newText);

        xmlDoc.getDocumentElement().appendChild(newEle);

        return xmlDoc;
    }

}