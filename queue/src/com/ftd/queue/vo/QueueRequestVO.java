package com.ftd.queue.vo;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Date;

import org.w3c.dom.Node;

/**
 * Queue Request VO
 * 
 * @author matt.wilcoxen
 */
public class QueueRequestVO
{
    private Logger logger = new Logger("com.ftd.queue.vo.QueueRequestVO");

    private boolean attEmail;
    private String  groupId;
    private String  currentSortDirection; 
    private String  maxRecords;
    private String  position;
    private String  queueDescription;
    private String  queueInd;
    private String  queueType;
    private String  sortColumn;
    private String  newSortDirection;
    private boolean sortOnReturn; 
    private Node    sortOrderState;
    private boolean sortRequested;
    private boolean tagged;
    private String  user;
    private String filter;
    private Date messageTimestampFilter;
    private Date deliveryDateFilter;
    private Date taggedOnFilter;
    private Date lastTouchedFilter;
    private String messageTypeFilter;
    private String systemFilter;
    private String timezoneFilter;
    private String sameDayGiftFilter;
    private String tagDispositionFilter;
    private String tagPriorityFilter;
    private String untaggedNewItemFilter;
    private String occasionIdFilter;
    private String deliveryLocationTypeFilter;
    private String memberNumberFilter;
    private String zipCodeFilter;
    private String taggedByFilter;
    private String excludePartners;
    private String context;
    private String sessionId;
    private String productIdFilter;

    /**
     * constructor
     * 
     * @param n/a
     * @return n/a
     * @throws none
     */
    public QueueRequestVO()
    {
      attEmail = false;
      currentSortDirection = null;
      maxRecords = "";
      newSortDirection = null;
      position = "";
      groupId = "";
      queueDescription = "";
      queueInd = "";
      queueType = "";
      sortOnReturn = false; 
      sortRequested = false;
      tagged = false;
      user = "";
  	}


	/**
	 * Returns the queueType.
     *     The type of queue data requested.  This may be a Mercury message type, Email
     *     message type, User, Group, or Order queue.  Example: An example Mercury message
     *     type is REJ.
     * 
	 * @return String
	 */
    public String getQueueType()
    {
        return queueType;
    }


	/**
	 * Returns the queueDescription.
     *     Description of the queue being accessed.
     * 
	 * @return String
	 */
    public String getQueueDescription()
    {
        return queueDescription;
    }


	/**
	 * Returns the queueInd.
     *     Indicates the type of the queue.  This may be Mercury, Email, User, Group
     *     or Order.
     * 
	 * @return String
	 */
    public String getQueueInd()
    {
        return queueInd;
    }


	/**
	 * Returns the Tagged.
     *     Boolean value determining if a tagged Message was requested. If true, tagged
     *     messages were requested.
     * 
	 * @return boolean
	 */
    public boolean getTagged()
    {
        return tagged;
    }


	/**
	 * Returns the attEmail.
     *     Boolean value determining if a message with a corresponding order number 
     *     was requested.  If true, messages with corresponding order numbers were 
     *     requested. This variable only pertains to Email requests.
     * 
	 * @return boolean
	 */
    public boolean getAttEmail()
    {
        return attEmail;
    }


	/**
	 * Returns the User.
     *     The user queue requested.  This variable only pertains to user queue requests.
     * 
	 * @return String
	 */
    public String getUser()
    {
        return user;
    }


	/**
	 * Returns the Group Id.
     *     The group queue requested.  This variable only pertains to group queue requests.
     * 
	 * @return String
	 */
    public String getGroupId()
    {
        return groupId;
    }


	/**
	 * Returns the sortColumn.
     *     The column to be sorted on.
     *     
	 * @return String
	 */
    public String getSortColumn()
    {
        return sortColumn;
    }


	/**
	 * Returns the sortOrderState.
     *     Current sort order of columns.  This variable holds the current sort order 
     *     of all columns in a DOM Node.
	 * @return Node
	 */
    public Node getSortOrderState()
    {
        return sortOrderState;
    }


	/**
	 * Returns the sortOrder.
     *     Database sort order.
     *     
	 * @return String
	 */
/*    public String getSortOrder()
    {
        return sortOrder;
    }
*/

	/**
	 * Returns the newSortDirection.
     *     Database sort direction (asc or desc).
     *     
	 * @return String
	 */
    public String getNewSortDirection()
    {
        return newSortDirection;
    }


	/**
	 * Returns the Sort Requested.
     *     Boolean value determining if a column sort was requested.
     *     
	 * @return boolean
	 */
    public boolean getSortRequested()
    {
        return sortRequested;
    }


	/**
	 * Returns the Position.
     *     Current page position.  This variable is related to the page navigation.
     *     
	 * @return String
	 */
    public String getPosition()
    {
        return position;
    }


	/**
	 * Returns the maxRecords.
     *     Maximum number of records displayed per page.  This variable is related
     *     to the page navigation.
     *     
	 * @return String
	 */
    public String getMaxRecords()
    {
        return maxRecords;
    }


	/**
	 * Sets the queueType.
     * 
	 * @param String - The queueType to set
	 */
    public void setQueueType(String queueType)
    {
        this.queueType = queueType;
    }


	/**
	 * Sets the queueDescription.
     * 
	 * @param String - The queueDescription to set
	 */
    public void setQueueDescription(String queueDescription)
    {
        this.queueDescription = queueDescription;
    }


	/**
	 * Sets the queueInd.
     * 
	 * @param String - The queueInd to set
	 */
    public void setQueueInd(String queueInd)
    {
        this.queueInd = queueInd;
    }


	/**
	 * Sets the Tagged.
     * 
	 * @param boolean - The Tagged indicator to set
	 */
    public void setTagged(boolean tagged)
    {
        this.tagged = tagged;
    }


	/**
	 * Sets the attEmail.
     * 
	 * @param boolean - The attEmail indicator to set
	 */
    public void setAttEmail(boolean attEmail)
    {
        this.attEmail = attEmail;
    }


	/**
	 * Sets the User.
     * 
	 * @param String - The User to set
	 */
    public void setUser(String user)
    {
        this.user = user;
    }


	/**
	 * Sets the Group Id.
     * 
	 * @param String - The Group Id to set
	 */
    public void setGroup(String groupId)
    {
        this.groupId = groupId;
    }


	/**
	 * Sets the sortColumn.
     * 
	 * @param String - The sortColumn to set
	 */
    public void setSortColumn(String sortColumn)
    {
        this.sortColumn = sortColumn;
    }


	/**
	 * Sets the sortOrderState.
     * 
	 * @param String - The sortOrderState to set
	 */
    public void setSortOrderState(Node sortOrderState)
    {
        this.sortOrderState = sortOrderState;
    }


	/**
	 * Sets the newSortDirection.
     * 
	 * @param String - The newSortDirection to set
	 */
    public void setNewSortDirection(String newSortDirection)
    {
        this.newSortDirection = newSortDirection;
    }


	/**
	 * Sets the sortRequested.
     * 
	 * @param boolean - The sortRequested to set
	 */
    public void setSortRequested(boolean sortRequested)
    {
        this.sortRequested = sortRequested;
    }


	/**
	 * Sets the Position.
     * 
	 * @param String - The Position to set
	 */
    public void setPosition(String position)
    {
        this.position = position;
    }


	/**
	 * Sets the maxRecords.
     * 
	 * @param String - The maxRecords to set
	 */
    public void setMaxRecords(String maxRecords)
    {
        this.maxRecords = maxRecords;
    }

  public void setCurrentSortDirection(String currentSortDirection)
  {
    this.currentSortDirection = currentSortDirection;
  }

  public String getCurrentSortDirection()
  {
    return currentSortDirection;
  }

  public void setSortOnReturn(boolean sortOnReturn)
  {
    this.sortOnReturn = sortOnReturn;
  }

  public boolean isSortOnReturn()
  {
    return sortOnReturn;
  }

    public void setMessageTimestampFilter(Date messageTimestampFilter) {
        this.messageTimestampFilter = messageTimestampFilter;
    }

    public Date getMessageTimestampFilter() {
        return messageTimestampFilter;
    }

    public void setDeliveryDateFilter(Date deliveryDateFilter) {
        this.deliveryDateFilter = deliveryDateFilter;
    }

    public Date getDeliveryDateFilter() {
        return deliveryDateFilter;
    }

    public void setTaggedOnFilter(Date taggedOnFilter) {
        this.taggedOnFilter = taggedOnFilter;
    }

    public Date getTaggedOnFilter() {
        return taggedOnFilter;
    }

    public void setLastTouchedFilter(Date lastTouchedFilter) {
        this.lastTouchedFilter = lastTouchedFilter;
    }

    public Date getLastTouchedFilter() {
        return lastTouchedFilter;
    }

    public void setMessageTypeFilter(String messageTypeFilter) {
        this.messageTypeFilter = messageTypeFilter;
    }

    public String getMessageTypeFilter() {
        return messageTypeFilter;
    }

    public void setSystemFilter(String systemFilter) {
        this.systemFilter = systemFilter;
    }

    public String getSystemFilter() {
        return systemFilter;
    }

    public void setTimezoneFilter(String timezoneFilter) {
        this.timezoneFilter = timezoneFilter;
    }

    public String getTimezoneFilter() {
        return timezoneFilter;
    }

    public void setSameDayGiftFilter(String sameDayGiftFilter) {
        this.sameDayGiftFilter = sameDayGiftFilter;
    }

    public String getSameDayGiftFilter() {
        return sameDayGiftFilter;
    }

    public void setTagDispositionFilter(String tagDispositionFilter) {
        this.tagDispositionFilter = tagDispositionFilter;
    }

    public String getTagDispositionFilter() {
        return tagDispositionFilter;
    }

    public void setTagPriorityFilter(String tagPriorityFilter) {
        this.tagPriorityFilter = tagPriorityFilter;
    }

    public String getTagPriorityFilter() {
        return tagPriorityFilter;
    }

    public void setUntaggedNewItemFilter(String untaggedNewItemFilter) {
        this.untaggedNewItemFilter = untaggedNewItemFilter;
    }

    public String getUntaggedNewItemFilter() {
        return untaggedNewItemFilter;
    }

    public void setOccasionIdFilter(String occasionIdFilter) {
        this.occasionIdFilter = occasionIdFilter;
    }

    public String getOccasionIdFilter() {
        return occasionIdFilter;
    }

    public void setDeliveryLocationTypeFilter(String deliveryLocationTypeFilter) {
        this.deliveryLocationTypeFilter = deliveryLocationTypeFilter;
    }

    public String getDeliveryLocationTypeFilter() {
        return deliveryLocationTypeFilter;
    }

    public void setMemberNumberFilter(String memberNumberFilter) {
        this.memberNumberFilter = memberNumberFilter;
    }

    public String getMemberNumberFilter() {
        return memberNumberFilter;
    }

    public void setZipCodeFilter(String zipCodeFilter) {
        this.zipCodeFilter = zipCodeFilter;
    }

    public String getZipCodeFilter() {
        return zipCodeFilter;
    }

    public void setTaggedByFilter(String taggedByFilter) {
        this.taggedByFilter = taggedByFilter;
    }

    public String getTaggedByFilter() {
        return taggedByFilter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getFilter() {
        return filter;
    }

  public void setExcludePartners(String excludePartners)
  {
    this.excludePartners = excludePartners;
  }

  public String getExcludePartners()
  {
    return excludePartners;
  }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

	public String getProductIdFilter() {
		return productIdFilter;
	}

	public void setProductIdFilter(String productIdFilter) {
		this.productIdFilter = productIdFilter;
	}
}
