package com.ftd.queue.util;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.dao.TimerDAO;

import java.io.IOException;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;


/**
 * This class is used to start and stop the Queue Timer.  The Queue Timer will
 * be stored as entries to the entity history table.
  *
 * @author Ali Lakani & Mike Kruger
 */
public class Timer 
{
  private Connection con;
  private static Logger logger  = new Logger("com.ftd.queue.util.Timer");
  
  public Timer(Connection connection)
  {
   this.con = connection;   
  }
  
  /**
   * This method starts the Queue Timer.  
   *
   * @param entityType - Type of entity timer
   * @param entityId - Id associated with the timer
   * @param commentOrigin - Where the Timer originated
   * @param csrId - the csrId
   * @param callLogId - the call log id if one exists
   * @return HashMap of timer information
   * @throws java.lang.Exception
   */
	public HashMap startTimer(String entityType, String entityId, String commentOrigin,
                            String csrId, String commentOriginType, String callLogId)
		throws IOException, ParserConfigurationException,
				SAXException, SQLException  {
    HashMap timerMap = new HashMap();

    TimerDAO tDao = new TimerDAO(this.con);    
	
    HashMap timerResults = tDao.insertEntityHistory(entityType, entityId, commentOrigin, 
                                                    csrId, callLogId);
    
    timerMap = buildTimer(timerResults, commentOriginType, commentOrigin);
    
    return timerMap;
  }


  /**
   * This method builds the Queue Timer information that will be returned to 
   * the action.   
   * 
   * @param entityHistoryId - Id assocaited with the timer entry
   * @param commentOriginType - Where the Timer originated type
   * @param commentOrigin - Where the Timer originated
   * @return HashMap of timer information
   * @throws java.lang.Exception
   */
	private HashMap buildTimer(HashMap timerResults, String commentOriginType, String commentOrigin) {
    HashMap timerMap = new HashMap();

    String historyId = (String) timerResults.get("OUT_ENTITY_HISTORY_ID");
    
    timerMap.put(QueueConstants.TIMER_ENTITY_HISTORY_ID, historyId);
    timerMap.put(QueueConstants.TIMER_COMMENT_ORIGIN_TYPE, commentOriginType);
    timerMap.put(QueueConstants.TIMER_COMMENT_ORIGIN, commentOrigin);
    
   
    return timerMap;
  }



  /**
   * This method stops the Queue Timer.   
   * 
   * @param entityHistoryId - Id assocaited with the timer entry
   * @throws java.lang.Exception
   */
	public void stopTimer(String entityHistoryId) 
		throws IOException, ParserConfigurationException,
				SAXException, SQLException  {
    TimerDAO tDao = new TimerDAO(this.con);    
    
	tDao.updateEntityHistory(entityHistoryId);
    
  }

}
