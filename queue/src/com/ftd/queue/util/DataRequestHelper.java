package com.ftd.queue.util;

import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.queue.constants.QueueConstants;

import java.sql.Connection;

/**
 * This class is a singleton helper utility for generating DataRequest objects.
 *
 * @author Brian Munter
 */

public class DataRequestHelper
{
    private static DataRequestHelper DATA_REQUEST_HELPER = new DataRequestHelper();;

    /**
     * Returns an instance of DataRequestHelper.
     *
     */
    public static synchronized DataRequestHelper getInstance()
    {
   		return DATA_REQUEST_HELPER;
    }

    /**
     * Retrieves a DataRequest object based on connection properties specified
     * in the Scrub configuration file.
     * Calling code closes the connection.
     *
     * @exception Exception
     */
    public DataRequest getDataRequest() throws Exception
    {
        String dataSourceName = ConfigurationUtil.getInstance().getProperty(QueueConstants.PROPERTY_FILE,"DATABASE_CONNECTION" );

        DataSourceUtil dataUtil = DataSourceUtil.getInstance();
        Connection connection =  dataUtil.getConnection(dataSourceName);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        return dataRequest;
    }
}