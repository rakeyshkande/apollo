package com.ftd.queue.util;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.util.HashMap;
import org.w3c.dom.Document;


public class BasePageBuilder 
{
	// Stuff needed to deal with the host name for new ActionForward() issues
	private static final String ERROR_PAGE = "error_page";
	private static final String QUEUE_CONTEXT = "QUEUE_CONFIG";
	private static String rootURL = null;
	
	/** 
	 * Constructor
	 */
	public BasePageBuilder()
	{
	}
	
	/**
	 * Get the root of the URL.  This will be hostname and the application path.
	 * @return the root of the url.
	 */
	public static String getRootURL() throws Exception
	{
		if (rootURL == null)
		{
			StringBuffer sb = new StringBuffer();
			
			// This gives us a big longer of a url, but we just chop off what we don't want
			sb.append(ConfigurationUtil.getInstance().getFrpGlobalParm(QUEUE_CONTEXT,ERROR_PAGE));
			int chopIndex = sb.indexOf("html");
			rootURL = sb.substring(0,chopIndex);
		}
		return rootURL;
	}

    /*******************************************************************************************
     * buildXML()
     *******************************************************************************************/
      public Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
            throws Exception
      {

        //Create a CachedResultSet object using the cursor name that was passed.
        CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

        Document doc =  null;

        //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
        //can be/is called by various other methods, the top and botton names MUST be passed to it.
        XMLFormat xmlFormat = new XMLFormat();
        xmlFormat.setAttribute("type", "element");
        xmlFormat.setAttribute("top", topName);
        xmlFormat.setAttribute("bottom", bottomName );

        //call the DOMUtil's converToXMLDOM method
        doc = DOMUtil.convertToXMLDOM(rs, xmlFormat);

        //return the xml document.
        
        return doc;

      }
	
}