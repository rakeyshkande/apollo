package com.ftd.queue.bo;

import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.dao.QueueDAO;
import com.ftd.queue.vo.QueueRequestVO;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.queue.util.BasePageBuilder;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;

import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * This class contains the business logic to retrieve queue data, obtain navigation 
 * configuration information, and send the information to the QueueRequestAction
 * for further processing.
 *
 * @author Matt Wilcoxen
 */

public class QueueRequestBO
{
    private Logger logger = new Logger("com.ftd.queue.bo.QueueRequestBO");
    
    //column sort order
    private static final String ASC_ORDER = "asc";
    private static final String DESC_ORDER = "desc";
    
    //elements:
    private static final String X_CURRENT_SORT_DIRECTION = "current_sort_direction";
    private static final String X_DESC = "description";
    private static final String X_MAX_RECORDS = "max_records";
    private static final String X_NAV_NODE = "navigation";
    private static final String X_PAGE_COUNT = "page_count";
    private static final String X_POSITION = "position";
    private static final String X_RECORD_COUNT = "record_count";
    private static final String X_ROOT_NODE = "root";
    private static final String X_SHOW_FIRST = "show_first";
    private static final String X_SHOW_LAST = "show_last";
    private static final String X_SHOW_NEXT = "show_next";
    private static final String X_SHOW_PREVIOUS = "show_previous";
    private static final String X_SORT_COLUMN = "sort_column";
    private static final String X_STARTING_QUEUE_ITEM = "starting_queue_item";
    
    //database:  interrogate DOM returned from db
    private static final String QUEUE_NODE = "OUT_QUEUE";
    private static final String OUT_PARMS = "out-parameters";
    private static final String QUEUE_CNT_NODE = "OUT_QUEUE_CNT";
    private static final String QUEUE_DESC_NODE = "OUT_QUEUE_DESC";
    
    //additional description added to No Order # Found email queues
    private static final String QUEUE_DESC_NO_ORDER = "No Order # Found ";
    
    //database objects
    private Connection conn = null;
    
    //configuration utility
    private ConfigurationUtil cu = null;

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws n/a
     */
    public QueueRequestBO(Connection conn)
    {
      super();
      this.conn = conn;
    }


    /**
     * get queue data
     *   1. determine sort order
     *   2. get navigation parms
     *   3. calculate item # the user is on
     *   4. get queue request data from db
     *          - get queue data
     *          - get queue item count
     *          - get queue description
     *   5. add navigation node to document
     *   6. add sort_state node to document
     * 
     * @param QueueRequestVO - queue request value object
     * @return Document      - queue request data
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     */
    public Document getQueueData(QueueRequestVO  qrVO)
        throws IOException, ParserConfigurationException, SAXException, SQLException, 
               TransformerException, Exception
    {
      cu = ConfigurationUtil.getInstance();

      Document queueXMLDoc = null;
      Document getQueueParmsXMLDoc = null;
      Document xmlDoc  = null;

      HashMap outParmsMap = null;
      long queueCnt = 0L;

      // determine sort order
      determineSortState(qrVO);

      // get navigation parms
      String position = qrVO.getPosition();
      if(position == null    ||  position.trim().length() == 0)
      {
          position = cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.POSITION);
          qrVO.setPosition(position);
      }
      String maxRecords = qrVO.getMaxRecords();
      if(maxRecords == null  ||  maxRecords.trim().length() == 0)
      {
          maxRecords = cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.MAX_RECORDS);
          qrVO.setMaxRecords(maxRecords);
      }

      HashMap partnerAccess = QueueDAO.getPreferredPartnersWithAccess(qrVO.getSessionId());
      Iterator ppIterator = partnerAccess.keySet().iterator();
      String excludePartners = null;
      while(ppIterator.hasNext()) { 
          String ppName = (String)ppIterator.next();
          String accessAllowed = (String) partnerAccess.get(ppName);
          logger.debug("partner: " + ppName + " " + accessAllowed);
          if (excludePartners == null) {
              excludePartners = "'" + ppName + "'";
          } else {
              excludePartners = excludePartners + ",'" + ppName + "'";
          }
      }
      qrVO.setExcludePartners(excludePartners);

      //Issue 1245 - when the csr returns, say from Communication to Queue, 
      //we will now display the page that they came from.  'iPosition' refers
      //to the page that they left from.  If another CSR has worked on queues
      //such that that original page is no longer available, we will display the last
      //page
      int queueItemNumberStartCnt = calculateStartingPosition(qrVO); 


      // get queue request data from db
      QueueDAO qDAO = new QueueDAO(conn);		         // get queue
      if (qrVO.getFilter() != null && qrVO.getFilter().equalsIgnoreCase("Y")){
        outParmsMap = qDAO.getQueueDataFiltered(qrVO,queueItemNumberStartCnt);
      }else{
          outParmsMap = qDAO.getQueueData(qrVO,queueItemNumberStartCnt);
      }
      HashMap outFilterParmsMap = qDAO.getFilterValues(qrVO);
      
      // get queue document returned from db
      queueXMLDoc = (Document) outParmsMap.get(QUEUE_NODE);
      xmlDoc = buildQueueRoot(queueXMLDoc);
      
      // append filter XML to document // 
      BasePageBuilder basePageBuilder = new BasePageBuilder();
      Document filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_TYPE_CUR", QueueConstants.FILTER_TYPE, QueueConstants.FILTER_TYPE);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_SYS_CUR", QueueConstants.FILTER_SYS, QueueConstants.FILTER_SYS);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_TIMEZONE_CUR", QueueConstants.FILTER_TIMEZONE, QueueConstants.FILTER_TIMEZONE);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_SDG_CUR", QueueConstants.FILTER_SDG, QueueConstants.FILTER_SDG);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_DISPOSITION_CUR", QueueConstants.FILTER_DISPOSITION, QueueConstants.FILTER_DISPOSITION);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_PRIORITY_CUR", QueueConstants.FILTER_PRIORITY, QueueConstants.FILTER_PRIORITY);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = buildNewItemXML(outFilterParmsMap, "OUT_NEW_ITEM_CUR", QueueConstants.FILTER_NEW_ITEM, QueueConstants.FILTER_NEW_ITEM);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_OCCASION_CODE_CUR", QueueConstants.FILTER_OCCASION, QueueConstants.FILTER_OCCASION);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = basePageBuilder.buildXML(outFilterParmsMap, "OUT_LOCATION_TYPE_CUR", QueueConstants.FILTER_LOCATION, QueueConstants.FILTER_LOCATION);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));
      filter = buildFilterDefaultsXML(qrVO);
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(filter.getDocumentElement(),true));      
      // get queue count of items returned and description
      getQueueParmsXMLDoc = (Document) outParmsMap.get(OUT_PARMS);
 
      queueCnt = new Long(DOMUtil.getNodeValue(getQueueParmsXMLDoc,QUEUE_CNT_NODE)).longValue();
      String queueDesc = DOMUtil.getNodeValue(getQueueParmsXMLDoc,QUEUE_DESC_NODE);
        
      // add navigation node to document
      addNavigation(qrVO,xmlDoc,queueCnt,queueItemNumberStartCnt,queueDesc);

      // add sort_state node to document
      addSortOrderState(qrVO,xmlDoc);

      return xmlDoc;
    }


    /**
     * determine sort state
     * 
     * VERY IMPORTANT:      VERY IMPORTANT:      VERY IMPORTANT:      VERY IMPORTANT:      
     *     Sort order is maintained via the "sort state" node.  The contents represent 
     *     what the sort order ('ASC'ending or 'DESC'ending) WILL BE for each of the
     *     sortable columns for the CURRENT request.  ONLY the value for the column 
     *     that the user clicked on is used to process the user's request.  The sort
     *     order for the selected column is then changed to represent what the sort 
     *     order will be for the NEXT time the user clicks on this SAME column.
     *     
     *     Because of the above, if a screen displays where the data for a particular
     *     column (such as Order #) is in the same sorted order as is initially specified 
     *     for the column in the SORT_ORDER_STATE property of the queue_config.xml file, 
     *     and if the user clicks on that column, the data will be presented in the same 
     *     manner as they presently see it.  It would appear that the code is malfunctioning, 
     *     but in this case the current order displayed on the screen happens to match the
     *     initial sort order specified in the SORT_ORDER_STATE.  Subsequent clicks on the 
     *     same column will flip the order from ASC to DESC and vice versa.
     * VERY IMPORTANT:      VERY IMPORTANT:      VERY IMPORTANT:      VERY IMPORTANT:      
     * 
     *     1. get sort order state / set default sort order state if sort order state 
     *        not established
     *     2. if sort column specified
     *            if sort was requested
     *                current sort order is whatever is in the sort state node
     *                flip column sort order from asc to desc & vice versa for next sort 
     *                    request on same column
     *            otherwise
     *                current sort order is the opposite of what is in the sort state 
     *                    (to maintain the previously requested sort order across a refresh 
     *                    request and a paging request)
     *     3. update column sort direction
     * 
     * @param  QueueRequestVO - queue request value object
     * @return QueueRequestVO - queue request value object with column sort order updated
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private QueueRequestVO determineSortState(QueueRequestVO  qrVO)
        throws IOException, ParserConfigurationException, SAXException, 
               TransformerException, Exception
    {
      Element xmlEle = null;
  
      // 1. get sort order state / set default sort order state
      Node sortOrderStateNode = qrVO.getSortOrderState();
      if(sortOrderStateNode == null || qrVO.isSortOnReturn())
      {
          sortOrderStateNode = createSortOrderState();
          qrVO.setSortOrderState(sortOrderStateNode);
      }

      // 2. if sort column specified, flip column sort order from asc to desc & vice versa
      String sortColumn = qrVO.getSortColumn();

      if(sortColumn != null  &&  sortColumn.trim().length() > 0)
      {
        xmlEle = (Element) sortOrderStateNode;
        NodeList nl = xmlEle.getElementsByTagName(sortColumn);
        Text xmlText = (Text) nl.item(0).getFirstChild();
        String reqColumnSortOrder = xmlText.getTextContent();
        String nextColumnSortOrder = reqColumnSortOrder;
        String reqSortOrder = "";

        //if sort was requested, flip column sort order from asc to desc & vice versa
        if(qrVO.getSortRequested())
        {
          //flip column sort order from asc to desc & vice versa
          if(qrVO.isSortOnReturn())
          {
            if (qrVO.getCurrentSortDirection().equalsIgnoreCase(ASC_ORDER))
            {
              reqSortOrder = ASC_ORDER;
              nextColumnSortOrder = DESC_ORDER;
            }
            else
            {
              reqSortOrder = DESC_ORDER;
              nextColumnSortOrder = ASC_ORDER;
            }
          }
          else if(reqColumnSortOrder.equalsIgnoreCase(ASC_ORDER))
          {
            reqSortOrder = ASC_ORDER;
            nextColumnSortOrder = DESC_ORDER;
            qrVO.setCurrentSortDirection(ASC_ORDER);
          }
          else if(reqColumnSortOrder.equalsIgnoreCase(DESC_ORDER))
          {
            reqSortOrder = DESC_ORDER;
            nextColumnSortOrder = ASC_ORDER;
            qrVO.setCurrentSortDirection(DESC_ORDER);
          }
        }
        //maintain sort order from previous sort request by flipping sort order
        else                                        //refresh or paging request
        {
          //flip column sort order from asc to desc & vice versa
          if(reqColumnSortOrder.equalsIgnoreCase(ASC_ORDER))
          {
            reqSortOrder = DESC_ORDER;
            nextColumnSortOrder = ASC_ORDER;
            qrVO.setCurrentSortDirection(DESC_ORDER);
          }
          else if(reqColumnSortOrder.equalsIgnoreCase(DESC_ORDER))
          {
            reqSortOrder = ASC_ORDER;
            nextColumnSortOrder = DESC_ORDER;
            qrVO.setCurrentSortDirection(ASC_ORDER);
          }
        }

        // 3. update column sort order
        //set next sort order for selected column
        xmlText.setData(nextColumnSortOrder);           
        //set sort order of selected column for this request
        qrVO.setNewSortDirection(reqSortOrder);
      }
      return qrVO;
    }
    
    
    /**
     * create sort order node
     *     1. get default sort order state
     *     2. convert string sort order state to a node structure
     * 
     * @param none
     * @return Node - of sort order
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private Node createSortOrderState()
        throws IOException, ParserConfigurationException, SAXException, 
               TransformerException, Exception
    {
        // 1. get default sort order state
        String sortOrderState = cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.SORT_ORDER_STATE);
        
        // 2. convert string sort order state to a node structure
        Document xmlDoc = (Document) DOMUtil.getDocument(sortOrderState);

        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
        	StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(xmlDoc, new PrintWriter(sw));
            logger.debug("QueueRequestBO - createSortOrderState(): sort order node from config file = \n" + sw.toString());
        }

        Node sortOrderStateNode = xmlDoc.getFirstChild();

        return sortOrderStateNode;
    }
    
    
    /**
     * build queue root document
     *     1. build root document
     *     2. add DOM node returned from db stored proc to root doc
     * 
     * @param XMLDocument    - DOM returned from db stored proc call
     * @return Document      - updated DOM representing queue request
     * @throws IOException
     * @throws ParserConfigurationException
     */
    private Document buildQueueRoot(Document dbXMLDoc)
        throws IOException, ParserConfigurationException
    {
      // 1. build root document
      Document xmlDoc = (Document) DOMUtil.getDefaultDocument();
      Element root = xmlDoc.createElement(X_ROOT_NODE);
      xmlDoc.appendChild(root);

      // 2. add DOM node returned from db stored procedure to root doc
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(dbXMLDoc.getDocumentElement(),true));

      return xmlDoc;        
    }


    /**
     * add navigation node to root document
     *     1. get navigation parms
     *     2. calculate the # of pages of data
     *     3. build navigation node
     *            <navigation>
     *                <sort_column></sort_column>
     *                <position></position>
     *                <max_records></max_records>
     *                <page_count></page_count>
     *                <starting_queue_item></starting_queue_item>
     *                <record_count></record_count>
     *                <description></description>
     *                <show_first></show_first>
     *                <show_previous></show_previous>
     *                <show_next></show_next>
     *                <show_last></show_last>
     *            </navigation>
     *     4. add navigation node to root document
     * 
     * @param QueueRequestVO - queue request value object
     * @param XMLDocument    - DOM representing queue request
     * @param long           - count of items returned for queue
     * @param int            - item # to begin display
     * @param String         - queue description
     * @return Document      - updated DOM representing queue request
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private Document addNavigation(QueueRequestVO qrVO, Document xmlDoc, long queueCnt, int itemStartNum, String queueDesc)
        throws IOException, ParserConfigurationException, SAXException,
               TransformerException
    {
      // 1. get navigation parms
      String position = qrVO.getPosition();
      String maxRecords = qrVO.getMaxRecords();
      
      // 2. calculate the # of pages of data
      int iPosition = (new Integer(position)).intValue();
      int iMaxRecords = (new Integer(maxRecords)).intValue();
      int iQueueCnt = (new Long(queueCnt)).intValue();
      
      int pageCnt = iQueueCnt / iMaxRecords;
      if((iQueueCnt % iMaxRecords) > 0)
      {
        pageCnt += 1;
      }
      if(pageCnt == 0)
      {
        pageCnt++;                                  //always indicate 1 page even if no data
      }
      
      // 3. build navigation node
      Document navXMLDoc = DOMUtil.getDefaultDocument();
      Element navEle = navXMLDoc.createElement(X_NAV_NODE);
      
      Element newEle = null;
      Text newText = null;
      
      newEle = navXMLDoc.createElement(X_SORT_COLUMN);
      newText = null;
      if(qrVO.getSortColumn() != null  &&  qrVO.getSortColumn().trim().length() > 0)
      {
        newText = navXMLDoc.createTextNode(qrVO.getSortColumn());
        newEle.appendChild(newText);
      }
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_POSITION);
      newText = navXMLDoc.createTextNode(new String(new Integer(iPosition).toString()));
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_MAX_RECORDS);
      newText = navXMLDoc.createTextNode(new String(new Integer(iMaxRecords).toString()));
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_PAGE_COUNT);
      newText = navXMLDoc.createTextNode(new String(new Integer(pageCnt).toString()));
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_STARTING_QUEUE_ITEM);
      newText = navXMLDoc.createTextNode(new String(new Integer(itemStartNum).toString()));
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_RECORD_COUNT);
      newText = navXMLDoc.createTextNode(new String(new Integer(iQueueCnt).toString()));
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_CURRENT_SORT_DIRECTION);
      newText = navXMLDoc.createTextNode(qrVO.getCurrentSortDirection());
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      if(qrVO.getQueueInd().equals(QueueConstants.EMAIL_QUEUE))
      {	
        if(!qrVO.getAttEmail())
          queueDesc = QUEUE_DESC_NO_ORDER + queueDesc;
      }
      
      newEle = navXMLDoc.createElement(X_DESC);
      newText = navXMLDoc.createTextNode(queueDesc);
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      String showFirst = "Y";
      String showPrevious = "Y";
      String showNext = "Y";
      String showLast = "Y";
      
      if(pageCnt ==1)                                 // only 1 page of data?
      {
        showFirst = "N";
        showPrevious = "N";
        showNext = "N";
        showLast = "N";
      }
      else
      {
        if(iPosition == 1)                          // on first page?
        {
          showFirst = "N";
          showPrevious = "N";
        }
        if(iPosition == pageCnt)                    // on last page?
        {
          showNext = "N";
          showLast = "N";
        }
      }
      
      newEle = navXMLDoc.createElement(X_SHOW_FIRST);
      newText = navXMLDoc.createTextNode(showFirst);
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_SHOW_PREVIOUS);
      newText = navXMLDoc.createTextNode(showPrevious);
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_SHOW_NEXT);
      newText = navXMLDoc.createTextNode(showNext);
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      newEle = navXMLDoc.createElement(X_SHOW_LAST);
      newText = navXMLDoc.createTextNode(showLast);
      newEle.appendChild(newText);
      navEle.appendChild(newEle);
      
      navXMLDoc.appendChild(navEle);
      
      // 4. add navigation node to root document
      xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(navXMLDoc.getDocumentElement(),true));
      
      return navXMLDoc;
    }


    /**
     * add sort order state to root document
     *     1. get sort order state node
     *     2. convert sort order state node to a string
     *     3. add sort order state node to root document
     * 
     * @param QueueRequestVO - queue request value object
     * @param XMLDocument    - DOM representing queue request
     * @return Document      - updated DOM representing queue request
     * @throws Exception 
     */

    private Document addSortOrderState(QueueRequestVO qrVO, Document xmlDoc)
        throws Exception
    {
      // 1. get sort order state node
      Node sortOrderStateNode = qrVO.getSortOrderState();
      
      // 2. convert sort order state node to a string
      Document doc = sortOrderStateNode.getOwnerDocument();
      String nodeString = null;
      //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
      //            method is executed, make StringWriter go away through scope control.
      //            Both the flush and close methods were tested with but were of non-affect.
      {
          StringWriter sw = new StringWriter();               //string representation of xml document
          DOMUtil.print(doc, new PrintWriter(sw));
          nodeString = sw.toString();
      }

      nodeString = nodeString.replaceAll("<","&lt;");     // < sign conversion
      nodeString = nodeString.replaceAll(">","&gt;");     // > sign conversion
      nodeString = nodeString.replaceAll(" ","");         // space conversion
      nodeString = nodeString.replaceAll("\\n","");       // new line conversion
      nodeString = nodeString.replaceAll("\\r","");       // carriage return conversion

      // 3. append sort order state child to document root
      Element newEle = xmlDoc.createElement("sort_state");
      Text newText = xmlDoc.createTextNode(nodeString);
      newEle.appendChild(newText);
      xmlDoc.getDocumentElement().appendChild(newEle);
      
      return xmlDoc;
    }


    /**
     * calculate the starting position for the resultset, and set a new value 
     * for the currentPagePosition if the currentPagePosition has expired since
     * last viewing the page.
     * 
     * @param QueueRequestVO - queue request value object
     * @return int - starting position
     * @throws Exception
     */

    private int calculateStartingPosition(QueueRequestVO qrVO)
        throws Exception
    {
      int queueItemNumberStartCnt = 0; 
      int maxRecords = new Integer(qrVO.getMaxRecords()).intValue(); 
      int currentPagePosition = (new Integer(qrVO.getPosition())).intValue();
      
      //find the total # of records. 
      int totalRecords = 0;
      QueueDAO qDAO = new QueueDAO(conn);		         // get queue
      totalRecords = qDAO.getQueueCount(qrVO);
  
      //calculate the total # of pages. 
      int totalPages = totalRecords / maxRecords; 
      if ( (totalRecords % maxRecords) > 0 )        
        totalPages++; 

      //calculate the starting position. 
      if (currentPagePosition > totalPages)
      {
        queueItemNumberStartCnt = ((totalPages - 1) * maxRecords) + 1;
        qrVO.setPosition(new Integer(totalPages).toString());
      }
      else
        queueItemNumberStartCnt = ((currentPagePosition - 1) * maxRecords) + 1;
        
      //return the starting position. 
      return queueItemNumberStartCnt; 
    
    }
    
    /*******************************************************************************************
     * buildNewItemXML()
     *******************************************************************************************/
      public Document buildNewItemXML(HashMap outMap, String cursorName, String topName, String bottomName)
            throws Exception
      {

        //Create a CachedResultSet object using the cursor name that was passed.
        CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

        Document doc =  null;
        String xmlTop = topName;
        String xmlBottom = bottomName;

        if (rs == null)  {
            return null;
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        Element rowset = null, row = null, column = null;

        rowset = doc.createElement(xmlTop);

        // loop thru the result set and generate xml
        boolean hasNumGreaterThanZero = false;
        int i = 0;
        while (rs.next())  
          {
              i++;
              int columnCount = rs.getColumnCount();
              for (int j = 0; j < columnCount; )  
              {
                Integer col=new Integer(++j);
                String value=rs.getString(j)!=null?rs.getString(j).trim():null;
                if(value!=null&&value.trim().length()>0)
                {
                    int intValue = new Integer(value).intValue();
                    if (intValue > 0){
                        hasNumGreaterThanZero = true;
                    }
                }
              }// end for
          } // end while
        // if there a zero value, add a blank value to the dropdown 
        // to find items that are not new
        row = doc.createElement(xmlBottom);
        row.setAttribute("num", String.valueOf(i));
        column = doc.createElement("untagged_new_items");
        column.appendChild(doc.createTextNode(" "));
        row.appendChild(column);
        rowset.appendChild(row);
        // if there are new items in the list, add a Y to the dropdown
        if (hasNumGreaterThanZero){
            row = doc.createElement(xmlBottom);
            row.setAttribute("num", String.valueOf(i));
            column = doc.createElement("untagged_new_items");
            column.appendChild(doc.createTextNode("Y"));
            row.appendChild(column);
            rowset.appendChild(row);
        }
        doc.appendChild(rowset);
        return doc;
          
      }


    public Document buildFilterDefaultsXML(QueueRequestVO qrVO)
          throws Exception
    {
      Document doc =  null;
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      factory.setIgnoringElementContentWhitespace(true);
      factory.setNamespaceAware(false);
      DocumentBuilder builder = factory.newDocumentBuilder();
      doc = builder.newDocument();
      Element rowset = null, row = null, column = null;
      rowset = doc.createElement("filter_defaults");
      String filter = qrVO.getFilter();
      String receivedDate, deliveryDate, tagDate, lasttouchedDate, type, sys, timezone, sdg, productId;
      String disposition, priority, newItem, occasion, location, memberNum, zip, tagBy;
      // if filter is turned on, set filter variables...otherwise, set everything to defaults
      if (filter != null && filter.equalsIgnoreCase("Y")){
          receivedDate = calendarFilterValueToString(qrVO.getMessageTimestampFilter());
          deliveryDate = calendarFilterValueToString(qrVO.getDeliveryDateFilter());
          tagDate = calendarFilterValueToString(qrVO.getTaggedOnFilter());
          lasttouchedDate = calendarFilterValueToString(qrVO.getLastTouchedFilter());
          type = dropdownFilterValueToString(qrVO.getMessageTypeFilter());
          sys = dropdownFilterValueToString(qrVO.getSystemFilter());
          timezone = dropdownFilterValueToString(qrVO.getTimezoneFilter());
          sdg = dropdownFilterValueToString(qrVO.getSameDayGiftFilter());
          disposition = dropdownFilterValueToString(qrVO.getTagDispositionFilter());
          priority = dropdownFilterValueToString(qrVO.getTagPriorityFilter());
          newItem = dropdownFilterValueToString(qrVO.getUntaggedNewItemFilter());
          occasion = dropdownFilterValueToString(qrVO.getOccasionIdFilter());
          location = dropdownFilterValueToString(qrVO.getDeliveryLocationTypeFilter());
          memberNum = qrVO.getMemberNumberFilter();
          zip = qrVO.getZipCodeFilter();
          tagBy = qrVO.getTaggedByFilter();
          productId = qrVO.getProductIdFilter();
      }else{
          receivedDate = null;
          deliveryDate = null;
          tagDate = null;
          lasttouchedDate = null;
          type = QueueConstants.NO_FILTER;
          sys = QueueConstants.NO_FILTER;
          timezone = QueueConstants.NO_FILTER;
          sdg = QueueConstants.NO_FILTER;
          disposition = QueueConstants.NO_FILTER;
          priority = QueueConstants.NO_FILTER;
          newItem = QueueConstants.NO_FILTER;
          occasion = QueueConstants.NO_FILTER;
          location = QueueConstants.NO_FILTER;
          memberNum = null;
          zip = null;
          tagBy = null;
          productId = null;
      }
                                  
      // create received date default XML //
      row = doc.createElement("filter_filter_default");
      row.appendChild(doc.createTextNode(filter));
      rowset.appendChild(row);
      // create received date default XML //
      row = doc.createElement("filter_received_date_default");
      row.appendChild(doc.createTextNode(receivedDate));
      rowset.appendChild(row);
      // create delivery date default XML //
      row = doc.createElement("filter_delivery_date_default");
      row.appendChild(doc.createTextNode(deliveryDate));
      rowset.appendChild(row);
      // create tag date default XML //
      row = doc.createElement("filter_tag_date_default");
      row.appendChild(doc.createTextNode(tagDate));
      rowset.appendChild(row);
      // create last touched date default XML //
      row = doc.createElement("filter_lasttouched_default");
      row.appendChild(doc.createTextNode(lasttouchedDate));
      rowset.appendChild(row);
      // create type default XML //
      row = doc.createElement("filter_type_default");
      row.appendChild(doc.createTextNode(type));
      rowset.appendChild(row);
      // create sys default XML //
      row = doc.createElement("filter_sys_default");
      row.appendChild(doc.createTextNode(sys));
      rowset.appendChild(row);
      // create timezone default XML //
      row = doc.createElement("filter_timezone_default");
      row.appendChild(doc.createTextNode(timezone));
      rowset.appendChild(row);
       // create sdg default XML //
      row = doc.createElement("filter_sdg_default");
      row.appendChild(doc.createTextNode(sdg));
      rowset.appendChild(row);  
      // create disposition default XML //
      row = doc.createElement("filter_disposition_default");
      row.appendChild(doc.createTextNode(disposition));
      rowset.appendChild(row);  
      // create priority default XML //
      row = doc.createElement("filter_priority_default");
      row.appendChild(doc.createTextNode(priority));
      rowset.appendChild(row);  
      // create newItem default XML //
      row = doc.createElement("filter_new_item_default");
      row.appendChild(doc.createTextNode(newItem));
      rowset.appendChild(row);    
      // create occasion default XML //
      row = doc.createElement("filter_occasion_default");
      row.appendChild(doc.createTextNode(occasion));
      rowset.appendChild(row);    
      // create location default XML //
      row = doc.createElement("filter_location_default");
      row.appendChild(doc.createTextNode(location));
      rowset.appendChild(row);      
      // create member number default XML //
      row = doc.createElement("filter_member_num_default");
      row.appendChild(doc.createTextNode(memberNum));
      rowset.appendChild(row);      
      // create zip default XML //
      row = doc.createElement("filter_zip_code_default");
      row.appendChild(doc.createTextNode(zip));
      rowset.appendChild(row);      
      // create tag by default XML //
      row = doc.createElement("filter_tag_by_default");
      row.appendChild(doc.createTextNode(tagBy));
      rowset.appendChild(row);      
      // create product id default XML //
     row = doc.createElement("filter_product_id_default");
     row.appendChild(doc.createTextNode(productId));
     rowset.appendChild(row);  
        
      doc.appendChild(rowset);
      return doc;
        
    }

    private String calendarFilterValueToString(Date calendarValue){
        // if calendar value is null or blank, return null
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        if (calendarValue == null){
            return "";
        }// if calendar value is date, return parsed date
        else{
            try {
                java.util.Date utilDate = new Date(calendarValue.getTime());
                return sdf.format(utilDate);
            }
            catch(Exception e){
                // if the parse fails, return blank
                logger.debug("Parse of calendar filter value "+calendarValue+" failed.");
                return "";
            }
        }
    }

    private String dropdownFilterValueToString(String dropdownValue){
        // if dropdown value is null or blank, return null
        if (dropdownValue == null){
            return QueueConstants.NO_FILTER;
        }// if dropdown value is a space, return empty string (The database 
        //  needs a space and the xsl needs an empty string). 
        else if(dropdownValue.equalsIgnoreCase(" ")){
            return "";
        }else{
            return dropdownValue;
        }
    }
        
}