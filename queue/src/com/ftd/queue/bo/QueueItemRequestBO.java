package com.ftd.queue.bo;

import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.dao.CustomerDAO;
import com.ftd.queue.dao.TimerDAO;
import com.ftd.queue.util.Timer;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 * This class contains the business logic to determine if additional parameters are
 * required based on the queue item type.  If additional parameters are needed it will
 * call the appropriate method to obtain those parameters, place the parameters in a
 * Map, and return the parameters.
 *
 * @author Matt Wilcoxen
 */

public class QueueItemRequestBO
{
    private Logger logger = new Logger("com.ftd.queue.bo.QueueItemRequestBO");

    //database:  interrogate DOM returned from db
    private static final String QUEUE_NODE = "QUEUE";
    private static final String OUT_PARMS = "out-parameters";
    private static final String CUST_ID = "customer_id";

    //database objects
    private Connection conn = null;

    //configuration utility
    private ConfigurationUtil cu = null;

    //security:  security on/off ind, security manager
    private boolean securityIsOn = false;
    private SecurityManager sm = null;
    private UserInfo ui = null;

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws n/a
     */
    public QueueItemRequestBO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
     * retrieve item request parameters
     *     1. if queue selected is loss prevention  and  item is NOT held for loss prevention
     *            get customer id
     * 
     * @param  String  - queue type
     * @param  String  - order detail id
     * @param  boolean - on hold for loss prevention indicator
     * @return Map     - additional parameters
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     */
    public Map retrieveParameters(String queueType, String orderDetailId, 
                                  boolean onHoldForLossPreventionInd)
        throws IOException, ParserConfigurationException, SAXException, 
               SQLException
    {
        Map addlParms = new HashMap();

        // 1. if queue selected is loss prevention  and  item is NOT held for loss prevention
        if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_LP_QUEUE)  &&
          (! onHoldForLossPreventionInd))
        {
            addlParms.put(CUST_ID,new Long(getCustomerId(orderDetailId)));
        }

        return addlParms;
    }
    
    
    /**
     * starts the queue timer
     *     
     * @param  String  - security token (session id)
     * @param  String  - entity id
	 * @param  String  - queue type
     * @return HashMap  - timer result information
     * @throws Exception
     */
	public HashMap startTimer(String securitytoken, String entityId, String queueType)
		throws IOException, ParserConfigurationException,
				SAXException, SQLException, TransformerException,
				Exception{
		String csrId = null;
		Timer timer = new Timer(this.conn);

		//hashmap that will contain the output from the stored proc
		HashMap timerResults = new HashMap();

        // obtain csrId
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String security = (String) cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.SECURITY_IS_ON);
        securityIsOn = security.equalsIgnoreCase("true") ? true : false;
        if(securityIsOn)
        {
            sm = SecurityManager.getInstance();
            ui = sm.getUserInfo(securitytoken);
            csrId = ui.getUserID();
        }
    
		// start the queue timer
		timerResults =  timer.startTimer( QueueConstants.TIMER_QUEUE_ENTITY_TYPE, entityId, 
                                        queueType + "/Q", csrId,
                                        QueueConstants.TIMER_QUEUE_COMMENT_ORIGIN_TYPE, 
                                        null);
										
		return timerResults;
	}


    /**
     * get customer id
     *     1. get customer id
     * 
     * @param String - order detail id
     * @return long  - customer id
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     */
    private long getCustomerId(String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, 
               SQLException
    {
        HashMap outParmsMap = new HashMap();

        // 1. get customer id
        CustomerDAO cDAO = new CustomerDAO(conn);
        long customerId = cDAO.findCustomerId(orderDetailId);

        return customerId;
    }

}