package com.ftd.queue.bo;

import com.ftd.queue.cacheMgr.handler.QueueConfigHandler;
import com.ftd.queue.constants.QueueConstants;
import com.ftd.queue.dao.QueueMenuDAO;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.queue.dao.QueueDAO;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

/**
 * This class contains the business logic to retrieve QueueMenu data, organize dropdown information,
 * and send the information back to the QueueMenuAction for further processing.  The queue menu
 * DOM is built from the lowest level (3) outward to level 0 (root).  Reference the following sample 
 * sequence of steps:
 *   1. level 3 is built first
 *   2. level 2 is built,
 *   3. level 3 is appended to level 2, etc.
 * 
 * Translate sample DOM 'extract' returned from the db below:
 *     <DROPDOWNLIST>
 *          <QUEUE num="1" csr_count="5012" queue_indicator="user"/>
 *     </DROPDOWNLIST>
 *     <DROPDOWNLIST>
 *         <QUEUE num="1" cs_group_id="G01" description="Group 01" tagged_count="0"/>
 *     </DROPDOWNLIST>
 *     <DROPDOWNLIST>
 *         <QUEUE num="12" queue_type="REJ" description="Reject Queue" 
 *          queue_indicator="Mercury" tagged_count="0" untagged_count="749" 
 *          tagged_unattached_count="0" untagged_unattached_count="0"/>
 *     </DROPDOWNLIST>
 * 
 * into sample output below:  (Please note that the notation L0, L1, L2, L3 refers 
 * to the node level in the produced output.  The values L0, L1, L2 and L3 - 
 * representing node levels 0, 1, 2 and 3 respectively - are not part of the output.)
 * L0  <root>
 * L1      <DROPDOWNLIST displayCount="true">
 * L2          <TAGGEDITEMSBYGROUP>
 * L3              <QUEUE queueType="Group 01" desc="Group 01" queueInd="group" cnt="0" grpId="G01"/>
 *             </TAGGEDITEMSBYGROUP>
 *             <TAGGEDITEMSBYUSER>
 *                 <QUEUE queueType="*** USER-ID ***" desc="*** DESC ***" queueInd="user" cnt="5012"/>
 *             </TAGGEDITEMSBYUSER>
 *             <UNTAGGEDMERCURYMESSAGES>
 *                 <QUEUE queueType="REJ" desc="Reject Queue" queueInd="Mercury" cnt="749"/>
 *             </UNTAGGEDMERCURYMESSAGES>
 *         </DROPDOWNLIST>
 *     </root>
 *
 * @author Matt Wilcoxen
 */

public class QueueMenuBO
{
    private Logger logger = new Logger("com.ftd.queue.bo.QueueMenuBO");

    //nodes:  interrogate DOM returned from db
    private static final String QUEUE_NODE = "QUEUE";

    //attributes:   interrogate Queue nodes returned in DOM from db
    private static final String CSR_COUNT = "csr_count";
    private static final String QUEUE_TYPE = "queue_type";
    private static final String QUEUE_DESC = "description";
    private static final String QUEUE_IND  = "queue_indicator";
    private static final String QUEUE_TAG_CNT = "tagged_count";
    private static final String QUEUE_UNTAG_CNT = "untagged_count";
    private static final String QUEUE_TAG_NO_CNT = "tagged_unattached_count";
    private static final String QUEUE_UNTAG_NO_CNT = "untagged_unattached_count";
    private static final String QUEUE_CS_GROUP_ID = "cs_group_id";
    private static final String QUEUE_GROUP = "group";
    private static final String QUEUE_USER = "user";

    //nodes:  built for Queue Menu data
    private static final String UNTAGGED_MERCURY_MESSAGES_NODE = "UNTAGGEDMERCURYMESSAGES";
    private static final String TAGGED_ITEMS_BY_TYPE_NODE = "TAGGEDITEMSBYTYPE";
    private static final String TAGGED_ITEMS_BY_GROUP_NODE = "TAGGEDITEMSBYGROUP";
    private static final String TAGGED_ITEMS_BY_USER_NODE = "TAGGEDITEMSBYUSER";
    private static final String UNTAGGED_EMAIL_NODE = "UNTAGGEDEMAIL";
    private static final String TAGGED_EMAIL_NODE = "TAGGEDEMAIL";
    private static final String UNTAGGED_EMAIL_NOORDER_NODE = "UNTAGGEDEMAILNOORDER";
    private static final String TAGGED_EMAIL_NOORDER_NODE = "TAGGEDEMAILNOORDER";

    //attributes:  used to build Queue nodes for Queue Menu data
    private static final String ATTR_QUEUE_TYPE = "queueType";
    private static final String ATTR_QUEUE_IND = "queueInd";
    private static final String ATTR_QUEUE_DESC = "desc";
    private static final String ATTR_QUEUE_CNT = "cnt";
    private static final String ATTR_GROUP_ID = "grpId";

    //defines Queue node output sort order in DOM returned for Queue Menu data
    private static final String QUEUE_NODE_ORDER[] = 
                                    {UNTAGGED_MERCURY_MESSAGES_NODE,
                                     TAGGED_ITEMS_BY_TYPE_NODE,
                                     TAGGED_ITEMS_BY_GROUP_NODE,
                                     TAGGED_ITEMS_BY_USER_NODE,
                                     UNTAGGED_EMAIL_NODE,
                                     TAGGED_EMAIL_NODE,
                                     UNTAGGED_EMAIL_NOORDER_NODE,
                                     TAGGED_EMAIL_NOORDER_NODE};

    //security:  security on/off ind, security manager
    private boolean securityIsOn = false;
    private SecurityManager sm = null;
    private UserInfo ui = null;
    private String csrId = null;
    
    //database objects
    private Connection conn = null;

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws n/a
     */
    public QueueMenuBO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
     * get queue menu items
     *   1. get queue config 'Count Indicator'
     *   2. get security manager instance and csr-id
     *   3. get queue menu data from DAO
     *   4. organize (create) queue menu DOM using db data
     *   5. return queue menu DOM to caller
     *   
     * @param String    - parameter context
     * @param String    - security token
     * @return Document - of queue menu items
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
     * @throws Exception
     */
    public Document getMenuItems(String parm_context, String sec_token)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
            TransformerException, Exception
    {
        boolean countIndicator = false;
        HashMap outParmsMap = null;
        Document queue;

        QueueConfigHandler qch = (QueueConfigHandler) 
            CacheManager.getInstance().getHandler(QueueConstants.QUEUE_CONFIG_HANDLER);
        countIndicator = qch.getCountIndicator();

        // get security manager instance
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String security = (String) cu.getProperty(QueueConstants.PROPERTY_FILE,QueueConstants.SECURITY_IS_ON);
        securityIsOn = security.equalsIgnoreCase("true") ? true : false;
        if(securityIsOn)
        {
            sm = SecurityManager.getInstance();
            ui = sm.getUserInfo(sec_token);
            csrId = ui.getUserID();
        }

        QueueMenuDAO qMDAO = new QueueMenuDAO(conn);          // get queue menu drop downs
        outParmsMap = qMDAO.getQueueMenuData(countIndicator,csrId);
        queue = organizeDropdowns(outParmsMap,countIndicator,parm_context,sec_token);

        // add dropdowns to document
        return queue;
    }


    /**
     * Organize queue menu dropdowns values
     *   1. organize 'queue' queue menu data
     *   2. organize 'group' queue menu data
     *   3. organize 'user' queue menu data
     *   4. assemble queue menu DOM using nodes created above
     *   5. return queue menu DOM to caller
     * 
     * @param HashMap   - map of DOMs returned by DAO
     * @param boolean   - indicator to identify if counts are included
     * @param String    - parm context
     * @param String    - security token
     * @return Document - contains organized queue menu dropdowns' values
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     * @throws Exception
     */
    private Document organizeDropdowns(HashMap outParmsMap, boolean countIndicator,
            String parmContext, String securityToken)
        throws IOException, ParserConfigurationException, SAXException, 
            TransformerException, Exception
    {
        Element dropdownListEle;
        Element element;                                // used to process returned db data
        String childName = "";
        HashMap dropdownNodeMap = new HashMap();        // hold dropdown nodes b4 they are
                                                        //     added to dropdownlist node
        Document queue;
        Document xmlDoc = null;

        queue = DOMUtil.getDocument();	    		// create document with root element
        dropdownListEle = (Element) queue.createElement("DROPDOWNLIST");
        String sCountIndicator = countIndicator ? "true" : "false";
        dropdownListEle.setAttribute("displayCount",sCountIndicator);

        // get (Mercury, e-mail, order) queue document returned from db
        xmlDoc = (Document) outParmsMap.get("QUEUE_TYPE_1");
        organizeQueueDropdowns(queue,xmlDoc,dropdownNodeMap,parmContext,securityToken);

        // get group document returned from db
        xmlDoc = (Document) outParmsMap.get("QUEUE_TYPE_2");
        organizeGroupDropdowns(queue,xmlDoc,dropdownNodeMap);

        // get user document returned from db
        xmlDoc = (Document) outParmsMap.get("QUEUE_TYPE_3");
        organizeUserDropdowns(queue,xmlDoc,dropdownNodeMap,securityToken);

        assembleDropdownNode(queue,dropdownListEle,dropdownNodeMap);

        return queue;
    }


    /**
     * Organize queue menu queue dropdowns values
     *   1. get security authorization for select queues
     *   2. for each queue
     *          pull attribute values off queue node being processed
     *          if mercury queue and user is authorized to work queue
     *              - create queue child nodes (unattached-tagged and attached-tagged)in output DOM
     *          if e-mail queue
     *              - create queue child nodes (unattached-tagged and unattached-untagged) in output DOM
     *                    - except for e-mail suggestion queue and e-mail order acknowledgement queue,
     *                      also create queue child nodes 
     *                      (attached-tagged and attached-untagged) in output DOM
     *          if order queue
     *               - create queue child node (attached-tagged) in output DOM
     *          endif
     *      endfor
     *   3. return queue menu DOM
     * 
     * @param Document    - DOM being built for end user
     * @param XMLDocument - DOM containing Mercury, E-mail, Order queue nodes returned from db
     * @param HashMap     - nodes being built for end user DOM
     * @param String      - parm context
     * @param String      - security token
     * @return Document   - DOM being built for end user
     * @throws Exception
     */
    private Document organizeQueueDropdowns(Document queue, Document xmlDoc, 
            HashMap dropdownNodeMap, String parmContext, String secToken)
        throws Exception
    {
        Element element;                                // process data from db
        String childName = "";
        //hold attribute key
        String attrKey[] = {ATTR_QUEUE_TYPE,
                            ATTR_QUEUE_DESC,
                            ATTR_QUEUE_IND,
                            ATTR_QUEUE_CNT};
        String attrValue[] = {"","","",""};             // hold attributes for grandchild node
 
        boolean adjAuthorized = false;
        boolean chargeAuthorized = false;
        boolean creditAuthorized = false;
        boolean denAuthorized = false;
        boolean lPAuthorized = false;
        boolean reconAuthorized = false;
        boolean zipAuthorized = false;
        boolean aribaAuthorized = false;
        boolean walmartAuthorized = false;
        HashMap partnerAccess = null;

        if(securityIsOn)
        {
            partnerAccess = QueueDAO.getPreferredPartnersWithAccess(secToken);

            // get security authorization information for select queues
            adjAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.ADJ_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            chargeAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.CHARGE_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            creditAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.CREDIT_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            denAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.DEN_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            lPAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.LP_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            reconAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.RECON_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            zipAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.ZIP_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            aribaAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.ARIBA_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
            walmartAuthorized = sm.assertPermission(parmContext, secToken, 
                QueueConstants.WALMART_SECURITY_RESOURCE, QueueConstants.PERMISSION_VIEW);
        }

        String queueType = "";		        			// drop down value
        String queueDesc = "";
        String queueInd  = "";	        				// Mercury, Email, Order, etc.
        String queueTagCnt = "";	    				// tagged count
        String queueUnTagCnt = "";	    				// untagged count
        String queueTagNOCnt = "";	    				// tagged no order count
        String queueUnTagNOCnt = "";	    			// untagged no order count
        boolean showQueue = true;                       // user authorized to see queue

        NodeList nl = xmlDoc.getElementsByTagName(QUEUE_NODE);
        if (nl.getLength() > 0)
        {
            for (int i = 0; i < nl.getLength(); i++)
            {
                element = (Element) nl.item(i);
                queueType = element.getAttribute(QUEUE_TYPE);
                queueDesc = element.getAttribute(QUEUE_DESC);
                queueInd  = element.getAttribute(QUEUE_IND);
                queueTagCnt = element.getAttribute(QUEUE_TAG_CNT);
                queueUnTagCnt = element.getAttribute(QUEUE_UNTAG_CNT);
                queueTagNOCnt = element.getAttribute(QUEUE_TAG_NO_CNT);
                queueUnTagNOCnt = element.getAttribute(QUEUE_UNTAG_NO_CNT);
                showQueue = true;                       // user can view queue

                attrValue[0] = queueType;               //queueType
                attrValue[1] = queueDesc;               //desc
                attrValue[2] = queueInd;                //queueInd

                if(queueInd.equalsIgnoreCase(QueueConstants.MERCURY_QUEUE))
                {
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_ADJ_QUEUE))
                    {
                        showQueue = false;
                        if(adjAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_CHARGE_QUEUE))
                    {
                        showQueue = false;
                        if(chargeAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_CREDIT_QUEUE))
                    {
                        showQueue = false;
                        if(creditAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_DEN_QUEUE))
                    {
                        showQueue = false;
                        if(denAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_LP_QUEUE))
                    {
                        showQueue = false;
                        if(lPAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_RECON_QUEUE))
                    {
                        showQueue = false;
                        if(reconAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_ZIP_QUEUE))
                    {
                        showQueue = false;
                        if(zipAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_ARIBA_QUEUE))
                    {
                        showQueue = false;
                        if(aribaAuthorized)
                        {
                            showQueue = true;
                        }
                    }
                    if(queueType.equalsIgnoreCase(QueueConstants.MERCURY_WALMART_QUEUE))
                    {
                        showQueue = false;
                        if(walmartAuthorized)
                        {
                            showQueue = true;
                        }
                    }

                    boolean isPreferredQueueWithAccess = false;
                    if (queueType != null && partnerAccess != null) {
                        String accessAllowed = (String) partnerAccess.get(queueType);
                        if (accessAllowed != null) {
                            showQueue = false;
                            if (accessAllowed.equalsIgnoreCase("Y")) {
                                isPreferredQueueWithAccess = true;
                                showQueue = true;
                            }
                            logger.info("Mercury partner: " + queueType + " " + isPreferredQueueWithAccess);
                        }
                    }
                    if(showQueue)
                    {
                        childName = UNTAGGED_MERCURY_MESSAGES_NODE;
                        attrValue[3] = queueUnTagCnt;          //cnt
                        buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);

                        childName = TAGGED_ITEMS_BY_TYPE_NODE;
                        attrValue[3] = queueTagCnt;            //cnt
                        buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
                    }
                }

                if(queueInd.equalsIgnoreCase(QueueConstants.EMAIL_QUEUE))
                {
                  boolean isPreferredQueueWithAccess = false;
                  if (queueType != null && partnerAccess != null) {
                      String accessAllowed = (String) partnerAccess.get(queueType);
                      if (accessAllowed != null) {
                          showQueue = false;
                          if (accessAllowed.equalsIgnoreCase("Y")) {
                              isPreferredQueueWithAccess = true;
                              showQueue = true;
                          }
                          logger.info("Email partner: " + queueType + " " + isPreferredQueueWithAccess);
                      }
                  }
                  if(showQueue)
                  {
                    if( ! (queueType.equalsIgnoreCase(QueueConstants.EMAIL_ARIBA_QUEUE))  &&
                        ! (queueType.equalsIgnoreCase(QueueConstants.EMAIL_WALMART_QUEUE)))
                    {
                        childName = UNTAGGED_EMAIL_NOORDER_NODE;
                        attrValue[3] = queueUnTagNOCnt;            //cnt
                        buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);

                        childName = TAGGED_EMAIL_NOORDER_NODE;
                        attrValue[3] = queueTagNOCnt;              //cnt
                        buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
                    }
//                  if( ! (queueType.equalsIgnoreCase(QueueConstants.EMAIL_SUGGESTION_QUEUE))  &&
//                      ! (queueType.equalsIgnoreCase(QueueConstants.EMAIL_ORDER_ACKNOWLEDGEMENT_QUEUE)))
                    if( ! (queueType.equalsIgnoreCase(QueueConstants.EMAIL_SUGGESTION_QUEUE)))
 
                    {
                        childName = TAGGED_EMAIL_NODE;
                        attrValue[3] = queueTagCnt;             //cnt
                        buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
    
                        childName = UNTAGGED_EMAIL_NODE;
                        attrValue[3] = queueUnTagCnt;           //cnt
                        buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
                    }
                  }
                }

                if(queueInd.equalsIgnoreCase(QueueConstants.ORDER_QUEUE))
                {
                    childName = TAGGED_ITEMS_BY_TYPE_NODE;
                    attrValue[3] = queueTagCnt;                 //cnt
                    buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
                }
            }
        }

        return queue;
    }


    /**
     * Organize queue menu group dropdowns values
     *   1. for each queue
     *          pull attribute values off group queue node being processed
     *          build queue child node - translate input DOM into output DOM
     *      endfor
     *   2. return queue menu DOM
     * 
     * @param Document    - DOM being built for end user
     * @param XMLDocument - DOM containing user queue nodes returned from db
     * @param HashMap     - nodes being built for end user DOM
     * @return Document   - DOM being built for end user
     */
    private Document organizeGroupDropdowns(Document queue, Document xmlDoc, 
            HashMap dropdownNodeMap)
    {
        Element element;                                    // used to process returned db data
        String childName = "";
        //hold attribute key
        String attrKey[] = {ATTR_QUEUE_TYPE,
                            ATTR_QUEUE_DESC,
                            ATTR_QUEUE_IND,
                            ATTR_QUEUE_CNT};
        String attrValue[] = {"","","",""};                 // hold attribute key value
 
  	    String groupId = "";
   	    String queueType = "";
   	    String queueDesc = "";
   	    String queueInd  = "";	    		         		// Mercury, Email, Order, etc.
        String queueTagCnt = "";		    		    	// tagged count

        NodeList nl = xmlDoc.getElementsByTagName(QUEUE_NODE);
        if (nl.getLength() > 0)
        {
            for (int i = 0; i < nl.getLength(); i++)
            {
     		    element = (Element) nl.item(i);

                groupId = element.getAttribute(QUEUE_CS_GROUP_ID);
    		    queueType = groupId;
    		    queueDesc = element.getAttribute(QUEUE_DESC);
                queueInd  = QUEUE_GROUP;
                queueTagCnt = element.getAttribute(QUEUE_TAG_CNT);

               	childName = TAGGED_ITEMS_BY_GROUP_NODE;
                attrValue[0] = queueType;                   //queueType
                attrValue[1] = queueDesc;                   //desc
                attrValue[2] = queueInd;                    //queueInd="group"
                attrValue[3] = queueTagCnt;                 //cnt
                buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
            }
	    }
        return queue;
    }


    /**
     * Organize queue menu user dropdowns values
     *   1. pull attribute values off user queue node being processed
     *   2. build queue child node - translate input DOM into output DOM
     *   3. return queue menu DOM
     * 
     * @param Document    - DOM being built for end user
     * @param XMLDocument - DOM containing user queue nodes returned from db
     * @param HashMap     - nodes being built for end user DOM
     * @param String      - security token
     * @return Document   - DOM being built for end user
     * @throws Exception
     */
    private Document organizeUserDropdowns(Document queue, Document xmlDoc, 
            HashMap dropdownNodeMap, String securityToken)
        throws Exception
    {
        Element element;                                    // used to process returned db data
        String childName = "";
        //hold attribute key
        String attrKey[] = {ATTR_QUEUE_TYPE,
                            ATTR_QUEUE_DESC,
                            ATTR_QUEUE_IND,
                            ATTR_QUEUE_CNT};
        String attrValue[] = {"","","",""};                 // hold attribute key value

        String userId = "*** TEST USER-ID " + csrId + " ***";
        String queueDesc = "*** TEST DESC ***";
        String csrCount = "";		        		     	// # items tagged to user

        if(securityIsOn)
        {
            // get user information
            userId = csrId;
            queueDesc = csrId;
        }

        NodeList nl = xmlDoc.getElementsByTagName(QUEUE_NODE);
        if (nl.getLength() > 0)                             // user item returned?
        {
            element = (Element) nl.item(0);
            csrCount = element.getAttribute(CSR_COUNT);
            attrValue[0] = userId;                          //queueType
            attrValue[1] = queueDesc;                       //desc
            attrValue[2] = QUEUE_USER;                      //queueInd="user"
            attrValue[3] = csrCount;                        //cnt

            childName = TAGGED_ITEMS_BY_USER_NODE;
            buildChildNode(queue,dropdownNodeMap,childName,QUEUE_NODE,attrKey,attrValue);
        }

        return queue;
    }


    /**
     * Build child node for queue menu DOM.  This method creates level 2 child nodes.
     * A level 3 child node (grandchild) is first created for the level 2 (child) node.
     *
     *   1. create grandchild node (level 3)
     *   2. if child node (level 2) does not exist
     *          create child node (level 2)
     *      endif
     *   3. append grandchild node (level 3) to child node (level 2)
     *   4. place child node (level 2) into hashmap for later processing
     * 
     * @param Document - contains queue menu dropdowns' values
     * @param HashMap  - nodes being built for end user DOM
     * @param String   - child (queue grouping) element name
     * @param String   - grandchild (queue instance) element name
     * @param String[] - grandchild element attribute keys
     * @param String[] - grandchild attribute key values
     * @return void    - none
     * @throws none
     */
    private void buildChildNode(Document queue, HashMap dropdownNode, String childName,
            String grandChildName, String attrKey[], String attrValue[])
    {
        Element grandChildEle = buildGrandChildNode(queue,grandChildName,attrKey,attrValue);
        Element childEle = (Element) dropdownNode.get(childName);

        if(childEle != null)                                // child  exist?
        {
            //get child node to append grandchild to
            childEle.appendChild(grandChildEle);
        }
        else
        {
            //create child node to append grandchild to
            childEle = (Element) queue.createElement(childName);
            childEle.appendChild(grandChildEle);
        }

        // add child node to dropdown hashmap
        dropdownNode.put(childName,childEle);
    }


    /**
     * Build grandchild node (Queue) for queue type child node
     * 
     * @param Document - contains queue menu dropdowns' values
     * @param String   - grandchild (queue instance) element name
     * @param String[] - grandchild attribute keys
     * @param String[] - grandchild attribute key values
     * @return Element - grandchild node
     * @throws none
     */
    private Element buildGrandChildNode(Document queue, String grandChildName, 
            String attrKey[], String attrValue[])
    {
        Element grandChildEle = (Element) queue.createElement(grandChildName);
        for(int i=0; i < attrKey.length; i++)
        {
            grandChildEle.setAttribute(attrKey[i],attrValue[i]);
        }

        return grandChildEle;
    }


    /**
     * Assemble dropdown node within queue root document.  It builds the level 1 DROPDOWNLIST 
     * node by apending level 2 child nodes (e.g., UNTAGGEDMERCURYMESSAGES, TAGGEDITEMSBYTYPE, etc.) 
     * which were previously created and stored in a hashmap.  The order in which the level 2 child
     * nodes are added to the parent is dependent upon the order established in the QUEUE_NODE_ORDER 
     * string array.  Lastly, it adds the level 1 DROPDOWNLIST node to the level 0 root node.
     * 
     * @param Document - queue menu document
     * @param Element  - dropdownlist node
     * @param HashMap  - hashmap of dropdown nodes
     * @return void
     * @throws none
     */
    private void assembleDropdownNode(Document queue, Element dropdownListEle, 
            HashMap dropdownNodeMap)
    {
        Element childNode = null;
        for(int i=0; i < QUEUE_NODE_ORDER.length; i++)
        {
            //get established queue order from string array
            //get corresponding child node from hashmap
            //add child node to dropdown node
            childNode = (Element) dropdownNodeMap.get(QUEUE_NODE_ORDER[i]);
            if(childNode != null)                   // entry exist for child node?
            {
                //add child queue to dropdownlist
                dropdownListEle.appendChild(childNode);
            }
        }

        //add dropdownlist child to root node
        Node firstChild = queue.getFirstChild();
        firstChild.appendChild(dropdownListEle);
    }


    /**
     * IMPORTANT:
     *     This method should ONLY be called by a test Java program for the purposes
     *     of testing locally.  The test driver program would call this method to
     *     set the csrId so that we can test.  Otherwise, we have no means to obtain
     *     the csrId.  This method should never be called in an integrated environment.  
     *     Hence, the reason that this method is protected.
     * 
     * @param String  - csrId
     * @return void
     * @throws none
     */
    protected void setCsrId(String csrId)
    {
        this.csrId = csrId;
    }

}