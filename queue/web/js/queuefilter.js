var filterColumn = false;
var  member_number_exp = '[0-9][0-9]\-[0-9][0-9][0-9][0-9]';
var  zip_code_exp1 = '[0-9][0-9][0-9][0-9][0-9]';
var  zip_code_exp2 = '[A-Z][0-9][A-Z][0-9][A-Z][0-9]';
var  zip_code_exp3 = '[A-Z][0-9][A-Z][0-9][A-Z]';


function setFilterColumnFocus() {
filterColumn = true;
}

function unSetFilterColumnFocus() {
filterColumn = false;
}

function checkMemberNumberFilter() {
  var member_number_filter = document.getElementById('member_number_filter');
  if(member_number_filter.value == null || member_number_filter.value.length <= 0 )
	return true;
  var re = new RegExp(member_number_exp);
  if (member_number_filter.value.match(re)) {
    return true;
  } else {
    return false;
  }
}

function checkZipCodeFilter() {
  var zip_code_filter = document.getElementById('zip_code_filter');
  if(zip_code_filter.value == null || zip_code_filter.value.length <= 0 )
	return true;

  var re1 = new RegExp(zip_code_exp1);
  var re2 = new RegExp(zip_code_exp2);
  var re3 = new RegExp(zip_code_exp3);
  if (zip_code_filter.value.match(re1) || zip_code_filter.value.match(re2) || zip_code_filter.value.match(re3)) {
    return true;
  } else {
    return false;
  }
}

function checkTagByFilter() {
  var tagby_filter = document.getElementById('tagby_filter');
  if(tagby_filter.value == null || tagby_filter.value.length <= 0 )
	return true;
  var len = tagby_filter.value.length;
  if(len >= 6 && len <= 20 && alphanumeric(tagby_filter.value)) {
	return true;
  }
  return false;
}

function checkSysFilterSelectedForMemberNumber()  {
	var sys_filter = document.getElementById('sys_filter');
	var member_number_filter = document.getElementById('member_number_filter');
    if(member_number_filter.value == null || member_number_filter.value.length <= 0 )
		return true;

	var sysValue = sys_filter.options[sys_filter.selectedIndex].text;
	sysValue = Trim(sysValue);

	if( (sysValue == 'X') ||
			(sysValue == '') ||
			( (sysValue != 'MERC') &&
				(sysValue != 'MERCURY') &&
				(sysValue != 'ARGO') &&
				(sysValue != 'VENUS')
			)
		)
		return false;
	else
		return true;
}


function alphanumeric(alphane)
{
	var numaric = alphane;
	for(var j=0; j<numaric.length; j++)
		{
		  var alphaa = numaric.charAt(j);
		  var hh = alphaa.charCodeAt(0);
		  if((hh > 47 && hh<59) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
		  {
		  }
		else	{
			 return false;
		  }
		}
 return true;
}



// helper script that uses the calendar
// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.

  if (cal.dateClicked )
	// if we add this call we close the calendar on single-click.
	// just to exemplify both cases, we are using this only for the 1st
	// and the 3rd field, while 2nd and 4th will still require double-click.
	cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(displayFieldId, format) {
  var el = document.getElementById(displayFieldId);
  if (_dynarch_popupCalendar != null) {
	// we already have some calendar created
	_dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
	// first-time call, create the calendar.
	var cal = new Calendar(1, null, selected, closeHandler);
	// uncomment the following line to hide the week numbers
	// cal.weekNumbers = false;
   cal.showsTime = true;

   _dynarch_popupCalendar = cal;                  // remember it in the global var
	cal.setRange(1900, 2070);        // min/max year allowed.
	cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el, "Br");        // show the calendar

  return false;
}

