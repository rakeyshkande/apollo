var requestMade = false;

/***************************************
this is when u click the printer button
***********************************/
function doprint()
{
window.print();
}

/**************************************************************
   Calls helper methods to limit navigation options.
**********************************************************/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = KeyHandler;
}

/********************************************************
   Disables the right button mouse click.
********************************************************/
function contextMenuHandler()
{
return false;
}

/*********************************************************
   Disables the various navigation keyboard commands.
**********************************************************/
function KeyHandler()
{
 // backspace
	if (window.event && window.event.keyCode == 8)
	{
		if	(
				document.activeElement.type != "text" &&
			 	document.activeElement.type != "textarea" &&
				document.activeElement.type != "password"
			)
		{

		window.event.cancelBubble = true;
         	window.event.returnValue = false;
         	return false;
      		}

   }


   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*************************************
this is to disable the navigation buttons
********************************/

     function buttonDis(page, form)
    {
setNavigationHandlers();
	if (form.page_count.value == 0)
	{
	document.getElementById("firstpage").disabled= "true";
	document.getElementById("previouspage").disabled= "true";
	document.getElementById("nextpage").disabled= "true";
	document.getElementById("lastpage").disabled= "true";
   	return;}
   	if (form.page_count.value == 1)
   	{
   	document.getElementById("firstpage").disabled= "true";
   	document.getElementById("previouspage").disabled= "true";
   	document.getElementById("nextpage").disabled= "true";
   	document.getElementById("lastpage").disabled= "true";
   	return;}
   	if (page < 2)
   	{
   	document.getElementById("firstpage").disabled= "true";
   	document.getElementById("previouspage").disabled= "true";
   	return;}
	if (page == form.page_count.value)
	{
	document.getElementById("nextpage").disabled= "true";
   	document.getElementById("lastpage").disabled= "true";
	return;}

}

/*****************************
 this will take you to the first page
********************************/
function goToFirstPage()
{
document.forms[0].position.value = 1;
performQueueRequest();

}

/************************
this will take u to one previous page
****************************************/

function goToPreviousPage()
{
document.forms[0].position.value--;
performQueueRequest();

}

/**************************
this will take u to one next page
**************************************/

function goToNextPage()
{
document.forms[0].position.value++;
performQueueRequest();

}


/*************************
this will take u to the last page
******************************************/

function goToLastPage()
{
document.forms[0].position.value = document.forms[0].page_count.value;
performQueueRequest();

}


/***************************************
 this will take you to a specific page
****************************************/
function goToSpecificPage()
{
	var jumpToPage = document.getElementById('jumpToPage');
	document.forms[0].position.value = jumpToPage.options[jumpToPage.selectedIndex].value;
	performQueueRequest();
}

/**************************************
to check which key has been pressed
***************************************/

function checkKey()

{
  var tempKey = window.event.keyCode;
  var first = show_first.toLowerCase();
  var last = show_last.toLowerCase();
  var next = show_next.toLowerCase();
  var previous = show_previous.toLowerCase();

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && previous == 'y')
  {
  goToPreviousPage();
	resetPosition();
  }

  //Down arrow pressed
  else if (window.event.altKey && tempKey == 40 && next ==  'y')
  {
	goToNextPage();
	resetPosition();
  }

  //Right arrow pressed
  else if (window.event.altKey && tempKey == 39 && last == 'y')
  {
  	goToLastPage();
	resetPosition();
  }

  //Left arrow pressed
  else if (window.event.altKey && tempKey == 37 && first == 'y')
  {
	goToFirstPage();
	resetPosition();
  }

}


/*******************************************
Reset the position variable
*********************************************/
    function resetPosition()
    {
	  //Reset position
	  position = document.forms[0].position.value;
    }


/*******************************************
To go to the Queuemenu page
*********************************************/
function doQueueMenu()
{
  if(requestMade == false)
  {
    requestMade = true;
    document.forms[0].action = "QueueMenu.do";
    document.forms[0].submit();
  }
}
/********************************************
To go to the Mainmenu page
*********************************************/
function doMainMenu()
{
  if(requestMade == false)
  {
    requestMade = true;
    document.forms[0].action = "MainMenu.do";
    document.forms[0].submit();
  }
}

/******************************************
 the common function
********************************************/
function performQueueRequest()
{
  if(requestMade == false)
  {
    requestMade = true;
	document.forms[0].action = "QueueRequest.do";
    document.forms[0].submit();
  }
}

/******************************************
 the common function
********************************************/
function performQueueRequestRefresh()
{
    document.forms[0].filter.value = "N";
    performQueueRequest();
}


/******************************************
 the common function
********************************************/
function performFilterRequest()
{
	if(document.getElementById('member_number_filter') != null) {
		if(checkMemberNumberFilter() == false) {
			alert("Invalid member number.  Field should match format :NN-NNNNAA ");
			return;
		}
		else {
			if(document.getElementById('sys_filter') != null && checkSysFilterSelectedForMemberNumber() == false) {
				alert("Member Number requires a valid Sys filter to be selected. Please select a valid filter for SYS.");
				return;
			}
		}
	}
	if(document.getElementById('zip_code_filter') != null && checkZipCodeFilter() == false) {
		alert("Invalid zip code");
		return;
	}
	if(document.getElementById('tagby_filter') != null && checkTagByFilter() == false) {
		alert("Invalid tag by. Field length should be between 6-20 and alphanumeric.");
		return;
	}
  if(requestMade == false)
  {
    requestMade = true;
    document.forms[0].position.value = "1";
    document.forms[0].filter.value = "Y";
    document.forms[0].action = "QueueRequest.do";
    document.forms[0].submit();
  }
}

/********************************************
function which is invoked when an item is selected from the queuerequest list
**********************************************/

 function performQueueItemRequest()
 {
  if(requestMade == false)
  {
    requestMade = true;
    document.forms[0].action = "QueueItemRequest.do";
    document.forms[0].submit();
  }
}

/**********************************************
 the key press evaluation function
***********************************************/

function evaluateKeyPress()

{
  var a;
  keypress = window.event.keyCode;
  if (keypress == 13)
     {
     doEntryAction();
     }
     else
     {
	if (keypress == 48)
	   a = 0;
	if (keypress == 49)
	   a = 1;
	if (keypress == 50)
	   a = 2;
	if (keypress == 51)
	   a = 3;
	if (keypress == 52)
	   a = 4;
	if (keypress == 53)
	   a = 5;
	if (keypress == 54)
	   a = 6;
	if (keypress == 55)
	   a = 7;
	if (keypress == 56)
	   a = 8;
	if (keypress == 57)
	   a = 9;


       if (filterColumn == false  && a >= 0 && a <=9)
          {
	     document.getElementById("numberEntry").focus();
          }
          else
          {
          }
       }
}

/*********************************************************
Find the row based on csr entry in the search box.
*********************************************************/

function doRowCalculation(searchValue){

   var val = stripZeros(searchValue);
   var num = val;
   var page = position;
if((position > 1 ) )
{
       page--;
       var num = (val - (page * max_records));
}

   return num;

}
/*******************************************************
Remove any leading zeros from the search number
********************************************************/
function stripZeros(num){
   var num,newTerm
   while (num.charAt(0) == "0") {
         newTerm = num.substring(1, num.length);
         num = newTerm;
   }
   if (num == "")
       num = "0";
       return num;
}

/*****************************************************
 to check whether the number entered is valid or not
********************************************************/
function isInteger(s)
{

	for (var i = 0; i < s.length; i++)
	{
		var c = s.charAt(i);
		if (!((c >= "0") && (c <= "9")))
		{
			return false;

		}
	}
	return true;

}

/*****************************************
 this is for sorting of the columns
*****************************************/

function columnSort(form, x)
{
form.sort_requested.value = true;
form.sort_column.value = x;
performQueueRequest();
}

/***************************************************
this is for the tagging function and calling values in the iframe
****************************************************/
function tagSelected()
{
tag_frame.location="QueueTag.do?message_id=" + document.forms[0].message_id.value + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
}

/***************************************************
function to check the entry in the entry box
***************************************************/

function checkEntry()
{
if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
}



/***************************************************
function to trim the value
***************************************************/

function Trim(value)
{
  return value.replace(/^\s+/,'').replace(/\s+$/,'');
}





