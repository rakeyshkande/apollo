/******************************************************
 This function is executed when the csr
 uses the search box. If the csr hits the 'enter' key
 the associated td with the same id will be located and the customer
 id is grabbed from the hidden field and a search is performed.
*******************************************************/
function doEntryAction()
{

   if(window.event.keyCode != 13)
	 return false;
	 
   var eve = document.getElementById("numberEntry").value;
   
              
	if (eve != null && eve != "" && isInteger(eve))
		{
        	if(eve > rec)
   		{
   		alert ('This line number does not exist');
        	}
        		else
        		{
			var searchNumber = doRowCalculation(eve);
			var td = document.getElementById('td_' + searchNumber);

 				if(td == null)
				{
				alert('This line number is not found on this page');
				}

					else
					{
					document.forms[0].order_detail_id.value = td.childNodes[3].value;
					document.forms[0].message_id.value = td.childNodes[4].value;
					document.forms[0].order_guid.value = td.childNodes[5].value;
					performQueueItemRequest();

				}
	    		}
        	}
}
