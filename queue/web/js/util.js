/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;
      
      for(i = 0; i < document.forms[0].elements.length; i++){
         if(document.forms[0].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }
      
      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altiLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }     
}

/*
   Returns the style object for the given element's field name.
*/
function getStyleObject(fieldName)
{
   if (document.getElementById && document.getElementById(fieldName))
      return document.getElementById(fieldName).style;
}

/*
   Limits input to digits only.      
*/
function digitOnlyListener()
{
   var input = event.keyCode;
   if (input < 48 || input > 57){
       event.returnValue = false;
   }
}

/*
   The limitTextarea will squelch any characters that exceed the limit value included in the method call.
   textarea - the textarea object
   limit - the number of characters allowed
*/
function limitTextarea(textarea, limit)
{
   if (textarea.value.length > limit)
      textarea.value = textarea.value.substring(0, limit);
}

/*
   The following function changes the control's style sheet to 'errorField'.
   elements - an Array of control names on the form
*/
function setErrorFields(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}
function setErrorField(element)
{
    var element = document.getElementById(element);
    if (element != null){
        element.className = "errorField";
    }
}

/*
    The following functions attached the 'onfocus' and 'onblur' events the the input
    elements.  When a control on the page gains focus, the control's border changes to red.
    When focus is lost, the control's border changes back to default.

    elements - an Array of control names on the form
    element - a control name on the form
*/
function addDefaultListenersArray(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null)
            addDefaultListenersSingle(elements[i]);
    }
}
function addDefaultListenersSingle(element)
{
    document.getElementById(element).attachEvent("onfocus", fieldFocus);
    document.getElementById(element).attachEvent("onblur", fieldBlur);
}
function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = "red";
}
function fieldBlur()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}

/*
    The following functions change the cursor UI when an image triggers a 'mouseover' event.

    elements - an Array of control names on the form
*/
function addImageCursorListener(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.attachEvent("onmouseover", imageOver);
            element.attachEvent("onmouseout", imageOut);
        }
    }
}
function imageOver()
{
    document.forms[0].style.cursor = "hand";
}
function imageOut()
{
    document.forms[0].style.cursor = "default";
}

/*
    The following functions are used to dis/enable fields.

    elements - an Array of control names on the form
    access - boolean value used to set the disabled property
*/
function disableFields(elements)
{
    setFieldsAccess(elements, true)
}
function enableFields(elements)
{
    setFieldsAccess(elements, false)
}
function setFieldsAccess(elements, access)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            setFieldAccess(element, access)
        }
    }
}
function setFieldAccess(element, access)
{
   element.disabled = access;
}

/*
   The following functions are used to show and hide any 
   select boxes that would normally appear in front of a popup.
*/
function getAbsolutePos(element) {
   var r = { x: element.offsetLeft, y: element.offsetTop };
   if (element.offsetParent) {
      var tmp = getAbsolutePos(element.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
   }
   return r;
}

function hideShowCovered(popup)
{
   function contains(a, b) {
      while (b.parentNode)
         if ((b = b.parentNode) == a)
            return true;
      return false;
   };

   var p = getAbsolutePos(popup);
   var EX1 = p.x;
   var EX2 = popup.offsetWidth + EX1;
   var EY1 = p.y;
   var EY2 = popup.offsetHeight + EY1;

   var tagsToHide = new Array("select");
   for (var k = 0; k < tagsToHide.length; k++) {
      var tags = document.getElementsByTagName(tagsToHide[k]);
      var tag = null;

      for (var i = 0; i < tags.length; i++) {
         tag = tags[i];
         if (!contains(this.div, tag)){
            p = getAbsolutePos(tag);
            var CX1 = p.x;
            var CX2 = tag.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = tag.offsetHeight + CY1;

            if ( (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1) ) {
               if (!tag.saveVisibility) {
                  tag.saveVisibility = tag.currentStyle["visibility"];
               }
               tag.style.visibility = tag.saveVisibility;
            } else {
               if (!tag.saveVisibility) {
                  tag.saveVisibility =  tag.currentStyle["visibility"];
               }
               tag.style.visibility = "hidden";
            }
         }
      }
   }
}

/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div's table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  content - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
function showWaitMessage(content, wait, message){
   var content = document.getElementById(content);
   var height = content.offsetHeight;
   var waitDiv = document.getElementById(wait + "Div").style;
   var waitMessage = document.getElementById(wait + "Message");
   _waitTD = document.getElementById(wait + "TD");

   content.style.display = "none";
   waitDiv.display = "block";
   waitDiv.height = height;
   waitMessage.innerHTML = (message) ? message : "Processing";
   clearWaitMessage();
   updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage(){
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}
function updateWaitMessage(){
   setTimeout("updateWaitMessage()", 1000);
   setTimeout("showPeriod()", 1000);
}
function showPeriod(){
   _waitTD.innerHTML += "&nbsp;.";
}


/*
   This is a helper function to return the two security parameters
   required by the security framework.

   areOnlyParams -   is used to change the url string (? vs. &) depending if these
                     are the only parameters in the url
*/
function getSecurityParams(areOnlyParams){
   return   ((areOnlyParams) ? "?" : "&") +
            "securitytoken=" + document.getElementById("securitytoken").value +
            "&context=" + document.getElementById("context").value;
}