/******************************************************
 This function is executed when the csr
 uses the search box. If the csr hits the 'enter' key
 the associated td with the same id will be located and the customer
 id is grabbed from the hidden field and a search is performed.
*******************************************************/
function doEntryAction()
{

   if(window.event.keyCode != 13)
	 return false;
	 
   var eve = document.getElementById("numberEntry").value;
   
              
	if (eve != null && eve != "" && isInteger(eve))
		{
        	if(eve > rec)
   		{
   		alert ('This line number does not exist');
        	}
        		else
        		{
			var searchNumber = doRowCalculation(eve);
			var td = document.getElementById('td_' + searchNumber);

 				if(td == null)
				{
				alert('This line number is not found on this page');
				}

					else
					{
					if(checkSystem(td))
					{
					document.forms[0].line_number.value = document.getElementById('numberEntry').value;
					document.forms[0].order_detail_id.value = td.childNodes[3].value;
					document.forms[0].message_id.value = td.childNodes[4].value;
					document.forms[0].external_order_number.value = td.childNodes[5].value;
					document.forms[0].message_type.value = td.childNodes[6].value;
					document.forms[0].system.value = td.childNodes[7].value;
					document.forms[0].order_guid.value = td.childNodes[8].value;
					document.forms[0].csr_id.value = td.childNodes[9].value;
					document.forms[0].point_of_contact.value = td.childNodes[10].value;
					document.forms[0].queue_type_chosen.value = td.childNodes[11].value;
					document.forms[0].mercury_num.value = td.childNodes[12].value;
                    if (td.childNodes[13].value.toUpperCase() == "Y")
                    {
                      document.forms[0].attached.value = "true";
                    }
                    else
                    {
                      document.forms[0].attached.value = "false";
                    }
                    
					performQueueItemRequest();
	  
					}
				}
	    		}
        	}
}

/*********************************************
this is to find out whether an order is in scrub 
or pending or removed for all the screens 
except the email screen
***********************************************/
function checkSystem(td)
{
var x = td.childNodes[5].value;
var y = x.toLowerCase();


if (y == "scrub")
{
   alert("This order is in Scrub");
   return false;
}
if (y == "pend" )
{
   alert("This order is Pending");
   return false;
}
if (y == "rmvd" )
{
   alert("This order is Removed");
   return false;
}
 else
   {
   return true;
   }
}


/*********************************************/