function Header(){
var xml = new ActiveXObject("Microsoft.XMLDOM");
xml.async = false;
var xmlFile = "xml/header.xml";
xml.load(xmlFile);
var xsl = new ActiveXObject("Microsoft.XMLDOM");
xsl.async = false;
var xslFile = "xsl/header.xsl";
xsl.load(xslFile);
document.write(xml.transformNode(xsl));
}

function QueuesMenu(){
var xml = new ActiveXObject("Microsoft.XMLDOM");
xml.async = false;
var xmlFile = "xml/dropdown.xml";
xml.load(xmlFile);
var xsl = new ActiveXObject("Microsoft.XMLDOM");
xsl.async = false;
var xslFile = "xsl/dropdown.xsl";
xsl.load(xslFile);
document.write(xml.transformNode(xsl));
document.formtosort.gotosort.disabled = true;
}

function SortQueues(namepage){
var xml = new ActiveXObject("Microsoft.XMLDOM");
xml.async = false;
var xmlFile = "xml/queues.xml";
xml.load(xmlFile);

var root = xml.documentElement;
var attrib = xml.createAttribute("queueType");
variab = variab.replace("+","&#160;");
variab = variab.replace("+","&#160;");
attrib.value = variab;
root.setAttributeNode(attrib);

var xsl = new ActiveXObject("Microsoft.XMLDOM");
xsl.async = false;
var xslFile = "xsl/"+namepage+".xsl";
xsl.load(xslFile);
document.write(xml.transformNode(xsl));
}

function changeDD(nameselect)
{
var selects = new Array("A","B","C","D","E","F","G","H");
var pages = new Array("untagged_reject","tagged_mercury","tagged_group","tagged_tlayn","tagged_orders","loss_prevention","untagged_email","tagged_email");
for (i=0; i < selects.length; i++)
	{
	if (selects[i]==nameselect){document.formtosort.page.value =pages[i];}
		else {document.all(selects[i]).value = '';}
	}
document.formtosort.gotosort.disabled = false;
}

function getDataTime()
{
var theDate = new Date();
var theDay = theDate.getDate();
var theMonth = theDate.getMonth();
var theYear = theDate.getFullYear();
var theMinutes = theDate.getMinutes();
var theHours = theDate.getHours();
var theSeconds = theDate.getSeconds();

var fullDataTime = theMonth +"/"+ theDay +"/"+ theYear +"&#160;&#160;"+ theHours +":"+ theMinutes +":"+ theSeconds;
return fullDataTime;
}