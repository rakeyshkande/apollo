
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:output method="html" indent="yes"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:variable name="cnt" select="root/DROPDOWNLIST/@displayCount" />
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Queues Main Menu</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript" language="JavaScript">
				
			   var cnt = <xsl:value-of select="root/DROPDOWNLIST/@displayCount"/>;
			   var untagmerc = new Array();
			   var tagtype = new Array();
			   var taggroup = new Array();
			   var taguser = new Array();
			   var untagemail = new Array();
			   var tagemail = new Array();
			   var untagnoord = new Array();
			   var tagnoord = new Array();
			   var requestMade = false;
			
                                
				<![CDATA[
/*********************
initial body on load
**********************/

function init(){
document.getElementById('untagged_merc_msg').focus();
document.getElementById("GO").disabled= true;
localStorage.setItem('isVipPopupShown','N');
}

/************************************************
the function for all the drop downs
*************************************************/		

function setSelected(array,dropdown)
{

  var queueTypeConstant = 0;
  var descConstant = 1;
  var queueIndConstant = 2;
  var taggedConstant = 3;
  var attachedConstant = 4;
  var userConstant = 5;
  var groupConstant = 6;

  var selected = document.getElementById(dropdown).value;

if (selected != null && selected != '')
{
var data = array[selected];

  document.forms[0].queue_type.value = data[queueTypeConstant];
  document.forms[0].queue_desc.value = data[descConstant];
  document.forms[0].queue_ind.value = data[queueIndConstant];
  document.forms[0].tagged.value = data[taggedConstant];
  document.forms[0].attached.value = data[attachedConstant];
  document.forms[0].user.value = data[userConstant];
  document.forms[0].group.value = data[groupConstant];


}
  
  }
  
/**************************************************************************
function for the first drop down menu 'A' ,the untagged mercury messages
**************************************************************************/

function setUntaggedMercurySelected(dropdown)
{
 setSelected(untagmerc,dropdown);
}

/**************************************************************************
function for the second drop down menu 'B' ,the tagged item by type messages
**************************************************************************/
function setTaggedTypeSelected(dropdown)
{
 setSelected(tagtype,dropdown);
}

/**************************************************************************
function for the third drop down menu 'C' ,the tagged items by group messages
**************************************************************************/

function setTaggedGroupSelected(dropdown)
{
 setSelected(taggroup,dropdown);
}

/**************************************************************************
function for the fourth drop down menu 'D' ,the tagged to user messages
**************************************************************************/

function setTaggedUserSelected(dropdown)
{
 setSelected(taguser,dropdown);
}

/**************************************************************************
function for the fifth drop down menu 'E' ,the untagged email messages
**************************************************************************/

function setUntaggedEmailSelected(dropdown)
{
 setSelected(untagemail,dropdown);
}

/**************************************************************************
function for the sixth drop down menu 'F' ,the tagged email messages
**************************************************************************/

function setTaggedEmailSelected(dropdown)
{
 setSelected(tagemail,dropdown);
}

/********************************************************************************
function for the seventh drop down menu 'G' ,the untagged email no order # found messages
**********************************************************************************/

function setUntaggedEmailNoOrdSelected(dropdown)
{
 setSelected(untagnoord,dropdown);
}

/********************************************************************************
function for the eighth drop down menu 'H' ,the tagged email no order # found messages
********************************************************************************/

function setTaggedEmailNoOrdSelected(dropdown)
{
 setSelected(tagnoord,dropdown);
}

/*************************************************************************************
This function is set to disable the other dropdowns once a particular dropdown is selected
***************************************************************************************/

function disableDropDowns(obj)

{

     if ("untagged_email" != obj.name)
     {
          document.forms[0].untagged_email.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("untagged_merc_msg" != obj.name)
     {
          document.forms[0].untagged_merc_msg.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("tagged_by_type" != obj.name)
     {
          document.forms[0].tagged_by_type.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("tagged_email" != obj.name)
     {
          document.forms[0].tagged_email.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("tagged_group" != obj.name)
     {
          document.forms[0].tagged_group.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("untagged_email_no_ord_num" != obj.name)
     {
          document.forms[0].untagged_email_no_ord_num.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("items_tagged_to_usr" != obj.name)
     {
          document.forms[0].items_tagged_to_usr.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     if ("tagged_email_no_ord_num" != obj.name)
     {
          document.forms[0].tagged_email_no_ord_num.options[0].selected = true;
          document.getElementById("GO").disabled= false;
     }
     
     if (obj.value == null || obj.value == '')
     document.getElementById("GO").disabled= true;
}

/*****************************
function for the "GO" button
********************************/
    function doQueueRequest()
    {
      if(requestMade == false)
	  {
	    requestMade = true;
		document.forms[0].action = "QueueRequest.do";
        document.forms[0].submit();
	  }
    }
/**************************************************
function for the main menu button.Value is not yet set
****************************************************/

function doMainMenu()
    {
      if(requestMade == false)
	  {
	    requestMade = true;
	    document.forms[0].action = "MainMenu.do";
	    document.forms[0].submit();
	  }
	}

/**************************************************/
     ]]></script>
			</head>
			<body onload="javascript:init();">
			
				<!-- Header-->
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
					<tr>
						<td rowspan="2" width="29%" align="left" >
							<img src="images/logo.gif" width="131" height="32" border="0" align="absmiddle"/>
						</td>
						<td width="40%" align="center" colspan="1" class="Header">Queues Main Menu</td>
						<td width="29%" align="right" id="time" class="Label"></td>
						<script type="text/javascript">startClock();</script>
					</tr>
					<tr>
						<td align="center" class="Label"></td>
						<td align="right">
							<button class="BlueButton" tabindex="10" accesskey="M" onclick="javascript:doMainMenu();">(M)ain Menu</button>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<hr/>
						</td>
					</tr>
				</table>
				<!-- Display table-->
				<table width="98%" align="center" cellspacing="1" class="mainTable">
					<tr>
						<td>
							<table width="100%" class="innerTable" align="center">
								<tr>
									<td colspan="2" align="center">
										<br/>
										<form name="QueueMenu" action="/QueueRequest.do" method="post">
          				    <xsl:call-template name="securityanddata"/>
											<input type="hidden" name="queue_type" value=""/>
											<input type="hidden" name="queue_ind" value=""/>
											<input type="hidden" name="queue_desc" value=""/>
											<input type="hidden" name="tagged" value=""/>
											<input type="hidden" name="attached" value=""/>
											<input type="hidden" name="user" value=""/>
											<input type="hidden" name="group" value=""/>
											<table width="98%" height="225"  border="0" align="center" cellpadding="0" cellspacing="0" class="forms">
												<tr>
													<td width="16%" height="61">&nbsp;</td>
													<td width="47%">
														<b>A. Untagged Mercury Messages</b>
														<br/>
														<Select name="untagged_merc_msg" class="select" tabindex="1" onChange="disableDropDowns(document.forms[0].untagged_merc_msg), setUntaggedMercurySelected('untagged_merc_msg');">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/UNTAGGEDMERCURYMESSAGES/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
																<xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
                                            
																</option>
																<script type="text/javascript" >
																         untagmerc[<xsl:value-of select="position()"/>] = new Array("<xsl:value-of select="@queueType"/>","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","false","","","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
													<td width="37%">
														<b>E. Untagged Email</b>
														<br/>
														<Select name="untagged_email" class="select" tabindex="5" onChange="disableDropDowns(document.forms[0].untagged_email),setUntaggedEmailSelected('untagged_email')">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/UNTAGGEDEMAIL/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
				                                            <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																         untagemail[<xsl:value-of select="position()"/>] = new Array("<xsl:value-of select="@queueType"/>","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","false","true","","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td>
														<b>B. Tagged Items by Type</b>
														<br/>
														<Select name="tagged_by_type" class="select" tabindex="2" onChange="disableDropDowns(document.forms[0].tagged_by_type), setTaggedTypeSelected('tagged_by_type')">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/TAGGEDITEMSBYTYPE/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
					                                        <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																         tagtype[<xsl:value-of select="position()"/>] = new Array("<xsl:value-of select="@queueType"/>","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","true","","","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
													<td>
														<b>F. Tagged Email</b>
														<br/>
														<Select name="tagged_email" class="select" tabindex="6" onChange="disableDropDowns(document.forms[0].tagged_email), setTaggedEmailSelected('tagged_email')">
															<option ></option>
															<xsl:for-each select="root/DROPDOWNLIST/TAGGEDEMAIL/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
                                                             <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																         tagemail[<xsl:value-of select="position()"/>] = new Array("<xsl:value-of select="@queueType"/>","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","true","true","","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
												</tr>
												<tr>
													<td height="59">&nbsp;</td>
													<td>
														<b>C. Tagged Items by Group</b>
														<br/>
														<Select name="tagged_group" class="select" tabindex="3" onChange="disableDropDowns(document.forms[0].tagged_group), setTaggedGroupSelected('tagged_group')">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/TAGGEDITEMSBYGROUP/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
					                                       <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																        taggroup[<xsl:value-of select="position()"/>] = new Array("","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","true","","","<xsl:value-of select="@queueType"/>");
																</script>
															</xsl:for-each>
														</Select>
													</td>
													<td>
														<b>G. Untagged Email No Order # Found</b>
														<br/>
														<Select name="untagged_email_no_ord_num" class="select" tabindex="7" onChange="disableDropDowns(document.forms[0].untagged_email_no_ord_num), setUntaggedEmailNoOrdSelected('untagged_email_no_ord_num')">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/UNTAGGEDEMAILNOORDER/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
                                                           <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																         untagnoord[<xsl:value-of select="position()"/>] = new Array("<xsl:value-of select="@queueType"/>","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","false","false","","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td>
														<b>D. Items Tagged to <xsl:value-of select="root/DROPDOWNLIST/TAGGEDITEMSBYUSER/QUEUE/@queueType"/></b>
														<br/>
														<Select name="items_tagged_to_usr" class="select" tabindex="4" onChange="disableDropDowns(document.forms[0].items_tagged_to_usr), setTaggedUserSelected('items_tagged_to_usr')">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/TAGGEDITEMSBYUSER/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
                                                             <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																         taguser[<xsl:value-of select="position()"/>] = new Array("","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","true","","<xsl:value-of select="@queueType"/>","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
													<td>
														<b>H. Tagged Email No Order # Found</b>
														<br/>
														<Select name="tagged_email_no_ord_num" class="select" tabindex="8" onChange="disableDropDowns(document.forms[0].tagged_email_no_ord_num), setTaggedEmailNoOrdSelected('tagged_email_no_ord_num')">
															<option></option>
															<xsl:for-each select="root/DROPDOWNLIST/TAGGEDEMAILNOORDER/QUEUE">
																<option value="{position()}">
																	<xsl:value-of select="@desc"/>
			                                                   <xsl:if test="$cnt = 'true'">
											                 &nbsp; (<xsl:value-of select="@cnt"/>)
											                </xsl:if>
																</option>
																<script type="text/javascript" >
																         tagnoord[<xsl:value-of select="position()"/>] = new Array("<xsl:value-of select="@queueType"/>","<xsl:value-of select="@desc"/>","<xsl:value-of select="@queueInd"/>","true","false","","");
																</script>
															</xsl:for-each>
														</Select>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<div align="center">
														    <button name="GO" class="BlueButton" tabindex="9" accesskey="G" onclick="doQueueRequest();">(G)o</button>
														</div>
													</td>
												</tr>
											</table>
										</form>
									</td>
								</tr>
							</table>
							<!--End Forms-->
						</td>
					</tr>
				</table>
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
					<tr>
						<td width="20%" align="right">
							<button class="BlueButton" tabindex="10" accesskey="M" onclick="javascript:doMainMenu();">(M)ain Menu</button>
						</td>
					</tr>
				</table>
				<!--Copyright bar-->
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>