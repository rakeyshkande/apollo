
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="footer.xsl"/>
	<xsl:import href="header.xsl"/>
	<xsl:import href="queue_footer.xsl"/>
	<xsl:import href="filter.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
	<xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:template match="/">
		<xsl:variable name="queue_type" select="root/queue_request/queue_type" />
		<html>
			<head>
				<title>Untagged Mercury
					<xsl:value-of select="root/navigation/description"/>
				</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>

				<!-- import the calendar script -->
				<script type="text/javascript" src="js/calendar.js"></script>

				<!-- import the language module -->
				<script type="text/javascript" src="js/lang/calendar-en.js"></script>

				<script type="text/javascript" src="js/queuedisplay.js" />
				<script type="text/javascript" src="js/queuefilter.js" />
				<script type="text/javascript" src="js/system_untagged_mercury.js" />
				<script type="text/javascript" src="js/clock.js"/>
				<script type="text/javascript">

    var show_previous = "<xsl:value-of select="root/navigation/show_previous"/>";
    var show_next     = "<xsl:value-of select="root/navigation/show_next"/>";
    var show_first    = "<xsl:value-of select="root/navigation/show_first"/>";
    var show_last     = "<xsl:value-of select="root/navigation/show_last"/>";
    var page_count = "<xsl:value-of select="root/navigation/page_count"/>";
    var position = "<xsl:value-of select="root/navigation/position"/>";
    var max_records = "<xsl:value-of select="root/navigation/max_records"/>";
    var keypress = "0";
    var pos = "<xsl:value-of select="root/navigation/starting_queue_item" />";
    var sort_column ="<xsl:value-of select="root/navigation/sort_column" />";
    var rec =<xsl:value-of select="root/navigation/record_count" />;

<![CDATA[
/* initial body on load*/

function init(){
document.getElementById('numberEntry').focus();
}


]]>
				</script>
			</head>
			<body onload="javascript:init(), buttonDis(form1.position.value, form1);"  onkeypress="javascript: evaluateKeyPress();" onkeydown="javascript:checkKey();">
				<!-- Header-->
				<xsl:call-template name="IncludeHeader"/>
				<form name="form1" method="post" action="">
					<xsl:call-template name="securityanddata"/>
					<div id="iframe_div">
						<iframe id="tag_frame" name="tag_frame" width="0px" height="0px" border="0" ></iframe>
					</div>
					<input type="hidden" name="position" value="{root/navigation/position}"/>
					<input type="hidden" name="page_count" value="{root/navigation/page_count}"/>
					<input type="hidden" name="queue_type" value="{root/queue_request/queue_type}"/>
					<input type="hidden" name="walmart_ariba_queue_type" value="{root/queue_request/queue_type}"/>
					<input type="hidden" name="queue_ind" value="{root/queue_request/queue_ind}"/>
					<input type="hidden" name="tagged" value="{root/queue_request/tagged}"/>
					<input type="hidden" name="attached" value="{root/queue_request/attached}"/>
					<input type="hidden" name="user" value="{root/queue_request/user}"/>
					<input type="hidden" name="group" value="{root/queue_request/group}"/>
					<input type="hidden" name="sort_order_state" value="{root/sort_state}"/>
					<input type="hidden" name="sort_column" value="{root/navigation/sort_column}"/>
					<input type="hidden" name="order_detail_id" value=""/>
					<input type="hidden" name="message_id" value=""/>
					<input type="hidden" name="message_type" value=""/>
					<input type="hidden" name="mercury_num" value=""/>
					<input type="hidden" name="sort_requested" value="false"/>
					<input type="hidden" name="submitTest" value="false"/>
					<input type="hidden" name="external_order_number" value=""/>
					<input type="hidden" name="max_records" value="{root/navigation/max_records}"/>
					<input type="hidden" name="current_sort_direction" value="{root/navigation/current_sort_direction}"/>
                                        <input type="hidden" name="filter" value="{root/filter_defaults/filter_filter_default}"/>
					<!-- Display table-->
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" class="innerTable" align="center">
									<tr>
										<td align="left">
											<div align="center">
												<table width="100%" border="0" cellpadding="0" cellspacing="5">
													<tr align="left" class="label" valign="bottom">
														<th>
															<div align="center">Line</div>
														</th>
														<xsl:choose>
															<xsl:when test="$queue_type='CREDIT'">
																<th onclick="columnSort(form1, 'master_order_number')">
																	<div align="center" class="cursor">
																		<u>Shopping
																			<br/>Cart Order#
																		</u>
																	</div>
																</th>
															</xsl:when>
															<xsl:otherwise>
																<th onclick="columnSort(form1, 'external_order_number')">
																	<div align="center" class="cursor">
																		<u>Order#</u>
																	</div>
																</th>
															</xsl:otherwise>
														</xsl:choose>
														<th>
															<div align="center">Mercury#</div>
														</th>
														<th onclick="columnSort(form1, 'message_timestamp')">
															<div align="center" class="cursor">
																<u>Receive
																	<br/>
					      Date/Time
																</u>
															</div>
														</th>
														<th onclick="columnSort(form1, 'message_type')">
															<div align="center" class="cursor">
																<u>Type</u>
															</div>
														</th>
														<th onclick="columnSort(form1, 'delivery_date')">
															<div align="center" class="cursor">
																<u>Delivery
																	<br/>
					      Date
																</u>
															</div>
														</th>
														<th onclick="columnSort(form1, 'system')">
															<div align="center" class="cursor">
																<u>Sys</u>
															</div>
														</th>
														<th onclick="columnSort(form1, 'timezone')">
															<div align="center" class="cursor">
																<u>Time
																	<br/>
						  Zone
																</u>
															</div>
														</th>
														<th onclick="columnSort(form1, 'product_id')">
															<div align="center" class="cursor">
																<u>SKU</u>
															</div>
														</th>
                                                                                                                <th onclick="columnSort(form1, 'delivery_location_type')">
															<div align="center" class="cursor">
																<u>Delivery
																	<br/>
					      Location Type
																</u>
															</div>
														</th>
														<th onclick="columnSort(form1, 'member_number')">
															<div align="center" class="cursor">
																<u>Member
																	<br/>
					      Number
																</u>
															</div>
														</th>
                                                                                                                <th onclick="columnSort(form1, 'zip_code')">
															<div align="center" class="cursor">
																<u>Zip
																	<br/>
					      Code
																</u>
															</div>
														</th>
                                                                                                                <th onclick="columnSort(form1, 'occasion')">
															<div align="center" class="cursor">
																<u>Occasion</u>
															</div>
														</th>
													</tr>
													<tr>
														<td colspan="3"> </td>
														<xsl:call-template name="calendarFilter">
															<xsl:with-param name="calendarName">received_date_filter</xsl:with-param>
															<xsl:with-param name="defaultValue" select="root/filter_defaults/filter_received_date_default"/>
														</xsl:call-template>
														<xsl:call-template name="dropDownFilter">
															<xsl:with-param name="optionName">type_filter</xsl:with-param>
															<xsl:with-param name="list" select="root/filter_type/filter_type"/>
															<xsl:with-param name="optionValue" >message_type</xsl:with-param>
                              <xsl:with-param name="optionText" >message_type</xsl:with-param>
                              <xsl:with-param name="optionTextConcat1" >message_type_count</xsl:with-param>
															<xsl:with-param name="optionSelected" select="root/filter_defaults/filter_type_default"/>
														</xsl:call-template>
														<xsl:call-template name="calendarFilter">
															<xsl:with-param name="calendarName">delivery_date_filter</xsl:with-param>
															<xsl:with-param name="defaultValue" select="root/filter_defaults/filter_delivery_date_default"/>
														</xsl:call-template>
														<xsl:call-template name="dropDownFilter">
															<xsl:with-param name="optionName">sys_filter</xsl:with-param>
															<xsl:with-param name="list" select="root/filter_sys/filter_sys"/>
															<xsl:with-param name="optionValue" >system</xsl:with-param>
															<xsl:with-param name="optionSelected" select="root/filter_defaults/filter_sys_default"/>
														</xsl:call-template>
														<xsl:call-template name="dropDownFilter">
															<xsl:with-param name="optionName">timezone_filter</xsl:with-param>
															<xsl:with-param name="list" select="root/filter_timezone/filter_timezone"/>
															<xsl:with-param name="optionValue" >timezone</xsl:with-param>
															<xsl:with-param name="optionSelected" select="root/filter_defaults/filter_timezone_default"/>
														</xsl:call-template>
														<xsl:call-template name="textFilter">
															<xsl:with-param name="textName">product_id_filter</xsl:with-param>
															<xsl:with-param name="defaultSize" >10</xsl:with-param>
															<xsl:with-param name="defaultValue" select="root/filter_defaults/filter_product_id_default"/>
														</xsl:call-template>
														<xsl:call-template name="dropDownFilter">
															<xsl:with-param name="optionName">location_filter</xsl:with-param>
															<xsl:with-param name="list" select="root/filter_location/filter_location"/>
															<xsl:with-param name="optionValue" >delivery_location_type</xsl:with-param>
															<xsl:with-param name="optionSelected" select="root/filter_defaults/filter_location_default"/>
															<xsl:with-param name="optionDisplayLength">3</xsl:with-param>
														</xsl:call-template>
														<xsl:call-template name="textFilter">
															<xsl:with-param name="textName">member_number_filter</xsl:with-param>
															<xsl:with-param name="defaultSize" >9</xsl:with-param>
															<xsl:with-param name="defaultValue" select="root/filter_defaults/filter_member_num_default"/>
														</xsl:call-template>
														<xsl:call-template name="textFilter">
															<xsl:with-param name="textName">zip_code_filter</xsl:with-param>
															<xsl:with-param name="defaultSize" >5</xsl:with-param>
															<xsl:with-param name="defaultValue" select="root/filter_defaults/filter_zip_code_default"/>
														</xsl:call-template>
														<xsl:call-template name="dropDownFilter">
															<xsl:with-param name="optionName">occasion_filter</xsl:with-param>
															<xsl:with-param name="list" select="root/filter_occasion/filter_occasion"/>
															<xsl:with-param name="optionValue" >occasion_id</xsl:with-param>
															<xsl:with-param name="optionText" >occasion</xsl:with-param>
															<xsl:with-param name="optionSelected" select="root/filter_defaults/filter_occasion_default"/>
															<xsl:with-param name="optionDisplayLength">7</xsl:with-param>
														</xsl:call-template>
													</tr>
													<tr>
														<td colspan="13">
															<hr/>
														</td>
													</tr>
													<xsl:for-each select="root/queue/item">
														<xsl:variable name="same_day_gift" select="same_day_gift" />
														<tr valign="top">
															<td id="td_{@num}" align="center">
																<script type="text/javascript"><![CDATA[
document.write(pos++ + ".");

  ]]></script>
																<input type="hidden" name="number" value="{@num}"/>
																<input type="hidden" name="td_order_detail_id" value="{order_detail_id}"/>
																<input type="hidden" name="td_message_id" value="{message_id}"/>
																<input type="hidden" name="td_message_type" value="{message_type}"/>
																<input type="hidden" name="td_mercury_num" value="{mercury_number}"/>
																<input type="hidden" name="system" value="{system}"/>
																<input type="hidden" name="td_external_order_number" value="{external_order_number}"/>
																<input type="hidden" name="td_queue_type_chosen" value="{queue_type}"/>
															</td>
															<xsl:choose>
																<xsl:when test="$queue_type='CREDIT'">
																	<td>
																		<div align="center">
																			<a style="text-decoration:none" title="{master_order_number}">
																				<xsl:choose>
																					<xsl:when test="string-length(master_order_number) > 20">
																						<xsl:value-of select="substring(master_order_number,1,17)"/>...
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:value-of select="master_order_number"/>
																					</xsl:otherwise>
																				</xsl:choose>
																			</a>
																		</div>
																	</td>
																</xsl:when>
																<xsl:otherwise>
																	<td>
																		<div align="center">
																			<a style="text-decoration:none" title="{external_order_number}">
																				<xsl:choose>
																					<xsl:when test="string-length(external_order_number) > 20">
																						<xsl:value-of select="substring(external_order_number,1,17)"/>...
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:value-of select="external_order_number"/>
																					</xsl:otherwise>
																				</xsl:choose>
																			</a>
                                      <xsl:if test="premier_collection_flag != '' and string-length(premier_collection_flag) > 0 and premier_collection_flag = 'Y'">
                                        <a style="font:bold;color:#d1bc61;background:black">
                                          &nbsp;LUX
                                        </a>
                                      </xsl:if>
																		</div>
																	</td>
																</xsl:otherwise>
															</xsl:choose>
															<td>
																<div align="center">
																	<a style="text-decoration:none" title="{mercury_number}">
																		<xsl:choose>
																			<xsl:when test="string-length(mercury_number) > 11">
																				<xsl:value-of select="substring(mercury_number,1,8)"/>...
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="mercury_number"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	</a>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="message_timestamp"/>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="message_type"/>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="delivery_date"/>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="system"/>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="timezone"/>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="product_id"/>
																</div>
															</td>
                                                            <td>
																<div align="center">
																	<a style="text-decoration:none" title="{delivery_location_type}">
																		<xsl:choose>
																			<xsl:when test="string-length(delivery_location_type) > 3">
																				<xsl:value-of select="translate(substring(delivery_location_type,1,3),$smallcase,$uppercase)"/>...
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="translate(delivery_location_type,$smallcase,$uppercase)"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	</a>
																</div>
															</td>
															<td>
																<div align="center">
																	<xsl:value-of select="member_number"/>
																</div>
															</td>
                                                                                                                        <td>
																<div align="center">
																	<a style="text-decoration:none" title="{zip_code}">
																		<xsl:choose>
																			<xsl:when test="string-length(zip_code) > 5">
																				<xsl:value-of select="substring(zip_code,1,5)"/>
                                                                                                                                                        </xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="zip_code"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	</a>
																</div>
															</td>
                                                                                                                        <td>
																<div align="center">
																	<a style="text-decoration:none" title="{occasion}">
																		<xsl:choose>
																			<xsl:when test="string-length(occasion) > 7">
																				<xsl:value-of select="translate(substring(occasion,1,7),$smallcase,$uppercase)"/>...
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="translate(occasion,$smallcase,$uppercase)"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	</a>
																</div>
															</td>
														</tr>
													</xsl:for-each>
													<tr valign="top">
														<td colspan="6">
															<div align="right">
																<span class="LabelRight">To access the order type the line number and press ENTER.......</span>
															</div>
														</td>
														<td>&nbsp;</td>
														<td colspan="2">
															<div align="right">
																<span class="LabelRight">Page&nbsp;
																	<xsl:for-each select="root/navigation">
																		<xsl:value-of select="position" />
																	</xsl:for-each>
											&nbsp;of&nbsp;
																	<xsl:for-each select="root/navigation">
																		<xsl:value-of select="page_count" />
																	</xsl:for-each>
																</span>
															</div>
														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
									<tr>
										<td align="left">
											<div align="center"></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!--Footer-->
					<xsl:call-template name="QueueFooter">
						<xsl:with-param name="displayFilter" select="true()"/>
					</xsl:call-template>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--to get the header title-->
	<xsl:variable name="xpath" select="root/navigation/description "/>
	<xsl:variable name="untag" select="'Untagged Mercury '"/>
	<xsl:variable name="combine" select="concat($untag, $xpath)"/>
	<xsl:template name="IncludeHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="$combine"/>
		</xsl:call-template>
	</xsl:template>
	<!--end-->
</xsl:stylesheet>