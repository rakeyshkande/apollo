<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="QueueFooter" match="/">
<xsl:param name="displayFilter"/>




    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/queuedisplay.js" />
    <script type="text/javascript" language="javascript">
     <![CDATA[


    ]]>
    </script>
   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">

	  <tr>
			<td rowspan="5" width="56%" align="right">
				<button class="BlueButton" tabindex="1" accesskey="R" onClick="javascript:performQueueRequestRefresh()">(R)efresh</button><br/><br/>
			</td>
			<xsl:if test="$displayFilter">
				<td rowspan="5" width="5%" />
				<td rowspan="5" width="56%" >
					<button class="BlueButton" tabindex="2" accesskey="F" onClick="javascript:performFilterRequest()">(F)ilter</button><br/><br/>
				</td>
			</xsl:if>
	  </tr>


	  <tr>
		<td align="right" height="21">
      <button class="arrowButton" tabindex="5" name="firstpage" onClick="goToFirstPage();">7</button>
      <button class="arrowButton" tabindex="6" name="previouspage" onClick="goToPreviousPage();">3</button>
      <button class="arrowButton" tabindex="7" name="nextpage"  onClick="goToNextPage();">4</button>
      <button class="arrowButton" tabindex="8" name="lastpage"  onClick="goToLastPage();">8</button>
		</td>
		<tr>
			<td align="right">
				<select id="jumpToPage" name="jumpToPage" tabindex="3">
					<option>1</option>
					<script>
						for (var i = 2; i &lt;= page_count; i++)
						{
						  document.write("<option value=" + i + ">" + i + "</option>");
						}
					</script>
				</select>
				<button style="width: 125px;" class="BlueButton" tabindex="4" accesskey="G" onclick="javascript:goToSpecificPage();">(G)o To Page</button>
			</td>
 	  </tr>
	  </tr>
      <tr>
      	<td align="right">
            <button style="width: 125px;" class="BlueButton" tabindex="9" accesskey="Q" onclick="javascript:doQueueMenu();">(Q)ueue Menu</button><br/>
      	</td>
 	  </tr>
 	  <tr>
         <td align="right">
			<button style="width: 125px;" class="BlueButton" tabindex="10" accesskey="M" width="150px" onclick="javascript:doMainMenu();">&nbsp; &nbsp;&nbsp;(M)ain Menu</button>
         </td>
 	  </tr>
</table>
</xsl:template>
</xsl:stylesheet>