<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
    <xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
	

  	<xsl:output method="html" indent="yes"/>
	<xsl:template name="securityanddata" match="/">
	    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
	    <input type="hidden" name="context" value="{$context}"/>
	</xsl:template>
</xsl:stylesheet>
