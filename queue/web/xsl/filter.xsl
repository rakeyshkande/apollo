<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
 
<xsl:template name="dropDownFilter">
	<xsl:param name="optionName"/>
	<xsl:param name="list"/>
	<xsl:param name="optionValue"/>
	<xsl:param name="optionSelected"/>
	<xsl:param name="optionText" />
  <xsl:param name="optionTextConcat1" />
	<xsl:param name="optionDisplayLength" select="0"/>
	<xsl:param name="overrideDisplay"/>
	<xsl:param name="overrideDisplay1From"/>
	<xsl:param name="overrideDisplay1To"/>

	<xsl:variable name="uc" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<xsl:variable name="lc" select="'abcdefghijklmnopqrstuvwxyz'"/>

	<xsl:variable name="noFilterOptionValue">X</xsl:variable>
	<STYLE TYPE="text/css">
	.red {color:#FF0000}
	</STYLE>

	<td>
		<div align="center">
		<select name="{$optionName}" id="{$optionName}">
			<option class="red">
				<xsl:attribute name="value">
				   <xsl:value-of select="$noFilterOptionValue"/>
				</xsl:attribute>
				<xsl:if test="translate($optionSelected, $lc, $uc) = translate($noFilterOptionValue, $lc, $uc)">
						<xsl:attribute name="SELECTED">TRUE</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="$noFilterOptionValue" />
			</option>
		  <xsl:for-each select="$list">
				<option>
						<xsl:attribute name="value">
			           <xsl:value-of select="*[local-name() = $optionValue]"/>
						</xsl:attribute>

						<xsl:if test="translate($optionSelected, $lc, $uc) = translate(*[local-name() = $optionValue], $lc, $uc)">
							<xsl:attribute name="SELECTED">TRUE</xsl:attribute>
						</xsl:if>

						<xsl:choose>
							<xsl:when test="string-length($optionText) = 0">
								<xsl:choose>
									<xsl:when test="$optionDisplayLength > 0 and string-length(*[local-name() = $optionValue]) > $optionDisplayLength">
										<xsl:value-of select="substring(*[local-name() = $optionValue],1,$optionDisplayLength)"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="$overrideDisplay">
												<xsl:choose>
													<xsl:when test="*[local-name() = $optionValue] = $overrideDisplay1From">
														<xsl:value-of select="$overrideDisplay1To" />
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="*[local-name() = $optionValue]" />
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="*[local-name() = $optionValue]" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$optionDisplayLength > 0">
                    <xsl:choose>
                      <xsl:when test="(string-length(*[local-name() = $optionText])) &lt;= $optionDisplayLength">
                        <xsl:value-of select="*[local-name() = $optionText]" />
                          <xsl:if test="(string-length(*[local-name() = $optionText]) + 3) &lt; $optionDisplayLength">
                            <xsl:if test="string-length(*[local-name() = $optionTextConcat1]) > 0 and $optionDisplayLength >= (string-length(*[local-name() = $optionText]) + 3 + string-length(*[local-name() = $optionTextConcat1]))">
                              &nbsp;(<xsl:choose>
                                        <xsl:when test="(string-length(*[local-name() = $optionText]) + 3 + string-length(*[local-name() = $optionTextConcat1])) &lt; $optionDisplayLength">
                                          <xsl:value-of select="substring(*[local-name() = $optionTextConcat1],1,$optionDisplayLength)"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <xsl:value-of select="*[local-name() = $optionTextConcat1]" />
                                        </xsl:otherwise>
                                     </xsl:choose>)
                            </xsl:if>
                          </xsl:if>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="substring(*[local-name() = $optionText],1,$optionDisplayLength)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="*[local-name() = $optionText]" />
                      <xsl:if test="string-length(*[local-name() = $optionTextConcat1]) > 0">
                        &nbsp;(<xsl:value-of select="*[local-name() = $optionTextConcat1]"/>)
                      </xsl:if>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>

				</option>
		  </xsl:for-each>
		</select>
		</div>
	</td>
</xsl:template>

<xsl:template name="textFilter">
	<xsl:param name="textName"/>
	<xsl:param name="defaultSize"/>
	<xsl:param name="defaultValue"/>
	<td>
		<div align="center">
			<INPUT NAME="{$textName}" id="{$textName}" value="{$defaultValue}">
			<xsl:attribute name="SIZE"><xsl:value-of select="$defaultSize" /></xsl:attribute>
			<xsl:attribute name="onblur">unSetFilterColumnFocus();</xsl:attribute>
			<xsl:attribute name="onfocus">setFilterColumnFocus();</xsl:attribute>
			</INPUT>
		</div>
	</td>
</xsl:template>

<xsl:template name="calendarFilter">
	<xsl:param name="calendarName"/>
	<xsl:param name="defaultValue"/>
	<xsl:variable name="defaultDisplaySize" select="5"/>
	<td >
	<div align="center">
	<input type="text" name="{$calendarName}" id="{$calendarName}" value="{$defaultValue}"  readonly="true">
		<xsl:attribute name="SIZE"><xsl:value-of select="$defaultDisplaySize" /></xsl:attribute>
		<xsl:attribute name="onclick">return showCalendar('<xsl:value-of select="$calendarName" />', '%m/%d/%y');</xsl:attribute>
                <xsl:attribute name="onfocusin">document.body.focus();</xsl:attribute>
	</input>
	</div>
	</td>
</xsl:template>
</xsl:stylesheet>