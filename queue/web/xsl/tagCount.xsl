<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template name="TagCount" match="/">

<html>
<head>
<script type="text/javascript">
<![CDATA[
/********************************************************
to check the status and the count
********************************************************/
function tagAction()
{
var val = document.getElementById('status').value;


    if(val == 'N')
    {
    parent.location.href='error.do';
    }
    else
    {
    document.getElementById('count').value;
    cnt = document.getElementById('count').value;
    
        if(cnt == 0)
        {
		alert('Item has been tagged or removed');
        }
        else
               if(cnt > 1)
               {
               alert ( 'You have tagged ' + cnt + ' items');
               }

         if(cnt > 0)
         {
         parent.performQueueItemRequest();
         }
    }

}


]]>
</script>

</head>
<body onload="tagAction();">
<form>

<input type="hidden" name="status" value="{out-parameters/TAG_STATUS}"/>
<input type="hidden" name="count" value="{out-parameters/TAGS_CREATED_CNT}"/>
</form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>