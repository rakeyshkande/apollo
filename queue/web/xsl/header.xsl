<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="header" match="/">


<xsl:param name="headerName"/>


    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/clock.js"/>
    <script type="text/javascript" src="js/queuedisplay.js" />
    <script type="text/javascript" language="javascript">
    
     <![CDATA[
    ]]>
    </script>
   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
         <td rowspan="2" width="29%" align="left" >
            <img border="0" src="images/logo.gif" align="absmiddle"/>
			
            <input type="text" size="4" maxlength="6" name="numberEntry" onkeypress="checkEntry();"/>
            
         </td>
         <td width="40%" align="center" colspan="1" class="header"><xsl:value-of select="$headerName"/></td>
         <td width="29%" align="right" id="time" class="Label"></td><script type="text/javascript">startClock();</script>
	 </tr>
      <tr>
		 <td align="center" class="Label"><span class="TopHeading">TO SORT CLICK ON COLUMN HEADER </span></td>
         <td align="right"><img src="images/printer.jpg" width="30" height="30" align="absmiddle" onclick="doprint()"/>
           <button class="BlueButton" tabindex="6" accesskey="Q" onclick="javascript:doQueueMenu();">(Q)ueue Menu</button></td></tr>
	  <tr>
         <td colspan="3">
            <hr/>
         </td>
      </tr>
   </table>
  </xsl:template>
</xsl:stylesheet>