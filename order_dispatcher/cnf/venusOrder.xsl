<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE scltPO SYSTEM "sclt_po.dtd">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>
  <xsl:param name="orderDetailId"/>
  <xsl:variable name="space" select="' '"/>
  <xsl:variable name="empty" select="''"/>
  <xsl:variable name="documentID" select="000000000"/>
  <xsl:variable name="merchantRefNumber" select="FTD"/>
  <xsl:variable name="poLineRefNumber" select="001"/>
  <xsl:variable name="quantity" select="1"/>
  <xsl:variable name="shipFromPartyType" select="SHIP_FROM"/>
  <xsl:variable name="shipFromAddress1" select="3113 Woodcreek Dr."/>
  <xsl:variable name="shipFromAddress2" select="$empty"/>
  <xsl:variable name="shipFromCity" select="Downers Grove"/>
  <xsl:variable name="shipFromState" select="IL"/>
  <xsl:variable name="shipFromPostalCode" select="60515"/>
  <xsl:variable name="shipFromContactType" select="PHONE"/>
  <xsl:variable name="shipToPartyType" select="SHIP_TO"/>
  <xsl:variable name="shipToContactType" select="PHONE"/>
  <xsl:variable name="allowBackOrder" select="0"/>
  <xsl:variable name="allowPartialShipment" select="0"/>
  <xsl:variable name="paymentTermDisplayString" select="Net 30"/>
  <xsl:variable name="skuClass" select="NORMAL"/>
  <xsl:variable name="carrierCode" select="FEDEX"/>
  <xsl:variable name="packageWeightUnit" select="lb"/>
  <xsl:variable name="methodOfShippingPayment" select="3"/>
  <xsl:variable name="numberOfLineItems" select="1"/>
  <xsl:template match="/">
    <!-- Root element of generated document -->
    <scltPO version="5.0" type="Production">
      <documentHeader>
        <documentId>
          <xsl:value-of select="$documentID"/>
        </documentId>
        <generationDate>
          <xsl:value-of select="concat(substring(/order/meta/xmlGenerated/date, 7, 4), substring(/order/meta/xmlGenerated/date, 1, 2),substring(/order/meta/xmlGenerated/date, 4, 2))"/>
        </generationDate>
        <generationTime>          
          <xsl:value-of select="concat(substring(/order/meta/xmlGenerated/time, 1, 2), substring(/order/meta/xmlGenerated/time, 4, 2),substring(/order/meta/xmlGenerated/time, 7, 2))"/>
        </generationTime>
      </documentHeader>
      <purchaseOrder>
        <purchaseOrderHeader>
          <purchaseOrderNumber>E1462639</purchaseOrderNumber>
          <purchaseOrderDate>
            <!-- will need to revisit and put in the right format -->
            <xsl:value-of select="concat(substring(/order/header/order_date, 7, 4), substring(/order/header/order_date, 1, 2),substring(/order/header/order_date, 4, 2))"/>
          </purchaseOrderDate>
          <merchantRefNumber>
            <xsl:value-of select="merchantRefNumber"/>
          </merchantRefNumber>
          <vendorRefNumber>91-3087AA</vendorRefNumber>
          <xsl:call-template name="SHIP-FROM-PARTY"/>
          <xsl:call-template name="SHIP-TO-PARTY"/>
          <allowBackOrder>
            <xsl:value-of select="$allowBackOrder"/>
          </allowBackOrder>
          <allowPartialShipment>
            <xsl:value-of select="$allowPartialShipment"/>
          </allowPartialShipment>
          <paymentTerm>
            <displayString>
              <xsl:value-of select="$paymentTermDisplayString"/>
            </displayString>
          </paymentTerm>
          <flexField>
            <name>sysadmin</name>
            <value>N</value>
          </flexField>
          <flexField>
            <name>process_batch</name>
            <value>1</value>
          </flexField>
        </purchaseOrderHeader>
        <purchaseOrderDetails>
          <purchaseOrderLineItem>
            <purchaseOrderLineRefNumber>
              <xsl:value-of select="$poLineRefNumber"/>
            </purchaseOrderLineRefNumber>
            <quantity>
              <xsl:value-of select="$poLineRefNumber"/>
            </quantity>
            <wholesaleUnitPrice>14.45</wholesaleUnitPrice>
            <skuDetails>
              <skuClass>
                <xsl:value-of select="$skuClass"/>
              </skuClass>
              <merchantSkuRefNumber>
                <xsl:choose>
                  <xsl:when test="/order/items/item[order_detail_id = $orderDetailId]/subcodeid and normalize-space(/order/items/item[order_detail_id = $orderDetailId]/subcodeid) != ''">
                    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/subcodeid"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/productid"/>
                  </xsl:otherwise>
                </xsl:choose>
              </merchantSkuRefNumber>
              <vendorSkuRefNumber>F052</vendorSkuRefNumber>
              <skuDescription>12 Mixed Color Med Roses Wrapped</skuDescription>
              <weight>4</weight>
            </skuDetails>
            <customerName>
              <xsl:value-of select="concat(/order/header/buyer_first_name, ' ', /order/header/buyer_last_name)"/>
            </customerName>
            <salesOrderDetails>
              <salesOrderRefNumber>
                <xsl:value-of select="/order/items/item/order_detail_id"/>
              </salesOrderRefNumber>
              <salesOrderDate>
                <!-- will need to revisit and put in the right format -->
                <xsl:value-of select="concat(substring(/order/header/order_date, 7, 4), substring(/order/header/order_date, 1, 2),substring(/order/header/order_date, 4, 2))"/>
              </salesOrderDate>
            </salesOrderDetails>
            <allowBackOrder>
              <xsl:value-of select="$allowBackOrder"/>
            </allowBackOrder>
            <allowPartialShipment>
              <xsl:value-of select="$allowPartialShipment"/>
            </allowPartialShipment>
            <shippingInstruction>
              <carrierCode>
                <xsl:value-of select="$carrierCode"/>
              </carrierCode>
              <shippingMethodCode>
                <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/shipping_method"/>
              </shippingMethodCode>
              <methodOfPayment>
                <xsl:value-of select="$methodOfShippingPayment"/>
              </methodOfPayment>
            </shippingInstruction>
            <requestedShipDate>
              <xsl:value-of select="concat(substring(/order/items/item[order_detail_id = $orderDetailId]/ship_date, 7, 4), substring(/order/items/item[order_detail_id = $orderDetailId]/ship_date, 1, 2),substring(/order/items/item[order_detail_id = $orderDetailId]/ship_date, 4, 2))"/>
            </requestedShipDate>
            <requestedDeliveryDate>
              <xsl:value-of select="concat(substring(/order/items/item[order_detail_id = $orderDetailId]/delivery_date, 7, 4), substring(/order/items/item[order_detail_id = $orderDetailId]/delivery_date, 1, 2),substring(/order/items/item[order_detail_id = $orderDetailId]/delivery_date, 4, 2))"/>
            </requestedDeliveryDate>
            <packagingInstruction>
              <transactionType>001</transactionType>
              <packageWeight>4</packageWeight>
              <packageWeightUnit>
                <xsl:value-of select="$packageWeightUnit"/>
              </packageWeightUnit>
            </packagingInstruction>
            <giftMessage>
              <xsl:choose>
                <xsl:when test="normalize-space(concat(/order/items/item[order_detail_id = $orderDetailId]/card_message, /order/items/item[order_detail_id = $orderDetailId]/card_signature)) = ''">
              NO CARD MESSAGE OR SIGNATURE
                 </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="concat(/order/items/item[order_detail_id = $orderDetailId]/card_message, ' ',/order/items/item[order_detail_id = $orderDetailId]/card_signature)"/>
                </xsl:otherwise>
              </xsl:choose>
            </giftMessage>
            <xsl:call-template name="SHIP-FROM-PARTY"/>
            <xsl:call-template name="SHIP-TO-PARTY"/>
          </purchaseOrderLineItem>
        </purchaseOrderDetails>
        <purchaseOrderSummary>
          <numberOfLineItems>
            <xsl:value-of select="$numberOfLineItems"/>
          </numberOfLineItems>
        </purchaseOrderSummary>
      </purchaseOrder>
      <documentSummary>
        <documentId>
          <xsl:value-of select="$documentID"/>
        </documentId>
      </documentSummary>
    </scltPO>
  </xsl:template>
  <xsl:template name="SHIP-FROM-PARTY">
    <party>
      <partyType>
        <xsl:value-of select="$shipFromPartyType"/>
      </partyType>
      <partyName><xsl:value-of select="/order/header/company/name"/></partyName>
      <address>
        <addressLine1>
          <xsl:value-of select="$shipFromAddress1"/>
        </addressLine1>
        <addressLine2>
          <xsl:value-of select="$shipFromAddress2"/>
        </addressLine2>
        <city>
          <xsl:value-of select="$shipFromCity"/>
        </city>
        <stateCode>
          <xsl:value-of select="$shipFromState"/>
        </stateCode>
        <postalCode>
          <xsl:value-of select="$shipFromPostalCode"/>
        </postalCode>
      </address>
      <contact>
        <contactType>
          <xsl:value-of select="$shipFromContactType"/>
        </contactType>
        <phoneNumber><xsl:value-of select="/order/header/company/phone"/></phoneNumber>
      </contact>
    </party>
  </xsl:template>
  <xsl:template name="SHIP-TO-PARTY">
    <party>
      <partyType>
        <xsl:value-of select="$shipToPartyType"/>
      </partyType>
      <partyName>
        <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_first_name"/>
        <xsl:value-of select="$space"/>
        <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_last_name"/>
      </partyName>
      <address>
        <addressLine1>
          <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_address1"/>
        </addressLine1>
        <addressLine2>
          <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_address2"/>
        </addressLine2>
        <city>
          <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_city"/>
        </city>
        <stateCode>
          <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_state"/>
        </stateCode>
        <postalCode>
          <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_postal_code"/>
        </postalCode>
      </address>
      <contact>
        <contactType>
          <xsl:value-of select="$shipToContactType"/>
        </contactType>
        <phoneNumber>
          <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_phone"/>
        </phoneNumber>
      </contact>
    </party>
  </xsl:template>
</xsl:stylesheet>
