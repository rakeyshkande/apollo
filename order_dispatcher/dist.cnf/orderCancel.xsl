<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
  
  <xsl:param name="orderDetailId"/>
  <xsl:param name="itemCount"/>
  
  <xsl:variable name="transmission_type" select="'cancel'"/>
  <xsl:variable name="indentifier" select="'CSERV'"/>
  <xsl:variable name="type" select="'CX'"/>
  <xsl:variable name="sub_type" select="'AT'"/>
  <xsl:variable name="delimiter" select="'~~'"/>
  <xsl:variable name="space" select="' '"/>
  <xsl:variable name="empty" select="''"/>
  <xsl:variable name="eof" select="'EOF'"/>
  <xsl:variable name="home" select="'HOME'"/>
  <xsl:variable name="del_date" select="/order/items/item/delivery_date"/>
  <xsl:variable name="date2" select="/order/items/item/second_delivery_date"/>
  
  <xsl:template match="/">
    <xsl:value-of select="$indentifier"/>
    <xsl:value-of select="$delimiter"/>
    <!-- Identifier -->
    <xsl:value-of select="/order/walmart_sequence_list/walmart_sequence"/>
    <xsl:value-of select="$delimiter"/>
    <!-- Unique Sequence for HP -->
    <xsl:value-of select="$type"/>
    <xsl:value-of select="$delimiter"/>
    <!-- Buyer -->
   <xsl:value-of select="/order/header/buyer_first_name"/>
   <xsl:value-of select="$delimiter"/>
   <!-- Buyer fist name -->
   <xsl:value-of select="/order/header/buyer_last_name"/>
   <xsl:value-of select="$delimiter"/>
   <!-- Buyer last name -->
   <xsl:value-of select="/order/header/buyer_email_address"/>
   <xsl:value-of select="$delimiter"/>
   <!-- Buyer email -->
   <xsl:value-of select="/order/header/buyer_daytime_phone"/>
   <xsl:value-of select="$delimiter"/>
   <!--Buyer daytime phone-->
   <xsl:value-of select="/order/header/buyer_evening_phone"/>
   <xsl:value-of select="$delimiter"/>
   <!--Buyer evening phone-->
   <xsl:value-of select="/order/items/item/order_number"/>
   <xsl:value-of select="$delimiter"/>
  <!--Confirmation number-->
   <xsl:value-of select="concat(substring($del_date,7,4),'/',substring($del_date,1,2),'/',substring($del_date,4,2))"/>
   <xsl:value-of select="$delimiter"/>
   <!--Delviery Date-->
   <xsl:if test="$date2 != ''">
    <xsl:value-of select="concat(substring($date2,7,4),'/',substring($date2,1,2),'/',substring($date2,4,2))"/>
   </xsl:if>
   <xsl:value-of select="$delimiter"/>
   <!--Second Delviery Date-->
   <xsl:value-of select="$sub_type"/>
   <xsl:value-of select="$delimiter"/>
   <!--sub type-->
   <!--Recipient-->
   <xsl:value-of select="/order/items/item/recip_first_name"/>
   <xsl:value-of select="$delimiter"/>
   <!--Recip first-name-->
   <xsl:value-of select="/order/items/item/recip_last_name"/>
   <xsl:value-of select="$delimiter"/>
   <!--Recip last-name-->
   <xsl:if test="/order/items/item/ship_to_type != $home">
    <xsl:value-of select="/order/items/item/ship_to_type"/>
   </xsl:if>
   <xsl:value-of select="$delimiter"/>
   <!--Institution-->
   <xsl:value-of select="/order/items/item/recip_address1"/>
   <xsl:value-of select="$delimiter"/>
   <!--Street address1-->
   <xsl:value-of select="/order/items/item/recip_city"/>
   <xsl:value-of select="$delimiter"/>
   <!--City-->
   <xsl:value-of select="/order/items/item/recip_state"/>
   <xsl:value-of select="$delimiter"/>
   <!--Recip State-->
   <xsl:value-of select="/order/items/item/recip_postal_code"/>
   <xsl:value-of select="$delimiter"/>
   <!--postal code-->
   <xsl:value-of select="/order/items/item/recip_country"/>
   <xsl:value-of select="$delimiter"/>
   <!--Recip country-->
   <xsl:value-of select="/order/items/item/recip_phone"/>
   <xsl:value-of select="$delimiter"/>
   <!--Recip phone-->
   <xsl:value-of select="$delimiter"/>
   <!--New Delivery Date 1-->
   <xsl:value-of select="$delimiter"/>
   <!--New Delivery Date 2-->
   <xsl:value-of select="/order/items/item/productid"/>
   <xsl:value-of select="$delimiter"/>
   <!--Product id -->
   <xsl:value-of select="/order/items/item/card_message"/>
   <xsl:value-of select="$delimiter"/>
   <!--Card Message-->
   <xsl:value-of select="/order/items/item/card_signature"/>
   <xsl:value-of select="$delimiter"/>
   <!--Card Signature-->
   <xsl:value-of select="/order/items/item/order_comments"/>
   <xsl:value-of select="$delimiter"/>
   <!--Order comments-->
   <xsl:value-of select="$eof"/>
  <!--EOF marker-->
  </xsl:template>
</xsl:stylesheet>