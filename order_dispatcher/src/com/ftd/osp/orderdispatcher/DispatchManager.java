package com.ftd.osp.orderdispatcher;

import com.ftd.osp.orderdispatcher.valueobjects.DispatchConfig;
import com.ftd.osp.orderdispatcher.valueobjects.Rule;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * Loads the dispatch manager from the configuration file which maps the 
 * status to prepared statements and xsl the file.
 *
 * @author Brian Munter
 */

public class DispatchManager
{
    private Logger logger;
    private static DispatchConfig DISPATCH_CONFIG;
    private static DispatchManager DISPATCH_MANAGER;
    private final static String DISPATCH_MANAGER_CONFIG_FILE = "dispatch_manager.xml";

    private final static String DM_ID = "id";
    private final static String DM_XSL = "xsl";
    private final static String DM_DR_XSL = "disasterRecoveryXSL";
    private final static String DM_FINAL_STATUS = "final_status";
    private final static String DM_RULE = "rule";
    private final static String DM_STATUS = "status";
    
    /** 
     * The private Constructor for DispatchManager
     * 
     * @exception TransformerException
     * @exception XSLException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @exception IOException
     */
    private DispatchManager() throws TransformerException,  
                                     SAXException, ParserConfigurationException, 
                                     IOException
    {
        logger = new Logger("com.ftd.osp.orderdispatcher.DispatchManager");
        this.initDispatchConfig();
    }

    /**
     * Retrieves an instance of DispatchManager
     * 
     * @exception TransformerException
     * @exception XSLException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @exception IOException
     * @return an instance of DispatchManager
     */
    public static DispatchManager getInstance() throws TransformerException,  
                                                                    SAXException, ParserConfigurationException, 
                                                                    IOException
    {
	// modified method to only synchronize when singleton needs init
        if ( DISPATCH_MANAGER == null )
        {
	    synchronized(DM_ID) {
	       DISPATCH_MANAGER = new DispatchManager();
	    }
        }

        return DISPATCH_MANAGER;
    }

    /**
     * Retrieves the dispatch config object
     * 
     * @return the DispatchConfig object
     */
    public DispatchConfig getDispatchConfig() throws TransformerException,  
                                                     SAXException, ParserConfigurationException, 
                                                     IOException
    {
        return DISPATCH_CONFIG;
    }

    /**
     * Initializes the dispatch configuration objects from the xml file.
     * 
     * @exception TransformerException
     * @exception XSLException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @exception IOException
     */
    private synchronized void initDispatchConfig() throws TransformerException,  
                                                          SAXException, ParserConfigurationException, 
                                                          IOException
    {
        DISPATCH_CONFIG = new DispatchConfig();

        Element ruleNode = null, xslNode = null, drxslNode = null, finalStatusNode = null, statementsNode = null, statementNode = null;
        NodeList rule_nl = null, xsl_nl = null, drxsl_nl = null, finalStatus_nl = null, statements_nl = null, statement_nl = null;
        Text xslText = null, drxslText = null, finalStatusText = null;
        Rule rule = null;

        // Load dispatch manager xml file into an xml document
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance(); 
        Document doc = DOMUtil.getDocument(ResourceUtil.getInstance().getResourceFileAsStream(DISPATCH_MANAGER_CONFIG_FILE));
        logger.debug("Loading dispatch configuration from xml: " + DISPATCH_MANAGER_CONFIG_FILE);
        
        // Get a list of all rules
        rule_nl = doc.getElementsByTagName(DM_RULE);

        // Read all rules
        for (int i = 0; i < rule_nl.getLength(); i++)  
        {
            ruleNode = (Element) rule_nl.item(i);
            if (ruleNode != null) 
            {
                rule = new Rule();
                    
                // Add all rule attributes
                rule.setAttribute(DM_STATUS, ruleNode.getAttribute(DM_STATUS));

                // Add the final status
                finalStatus_nl = ruleNode.getElementsByTagName(DM_FINAL_STATUS);

                // There can only be one final status node
                finalStatusNode = (Element) finalStatus_nl.item(0);
                if (finalStatusNode != null)  
                {
                    finalStatusText = (Text) finalStatusNode.getFirstChild();
                    rule.setFinalStatus(finalStatusText.getData());
                }

                // Add the xsl
                xsl_nl = ruleNode.getElementsByTagName(DM_XSL);

                // There can only be one xsl node
                xslNode = (Element) xsl_nl.item(0);
                if (xslNode != null)  
                {
                    xslText = (Text) xslNode.getFirstChild();
                    rule.setXslFile(xslText.getData());
                }

                // Add the disaster recovery xsl
                drxsl_nl = ruleNode.getElementsByTagName(DM_DR_XSL);
                
                // There can only be one DR xsl node
                drxslNode = (Element) drxsl_nl.item(0);
                if (drxslNode != null)
                {
                    drxslText = (Text) drxslNode.getFirstChild();
                    rule.setDisasterRecoveryXSLFile(drxslText.getData());
                }
                
                DISPATCH_CONFIG.setRule(rule.getAttribute(DM_STATUS), rule);
            }
        }
    }
}
