package com.ftd.osp.orderdispatcher.util;

import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.j2ee.*;
import com.ftd.osp.utilities.dataaccess.*;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import com.ftd.osp.utilities.*;
import com.ftd.osp.utilities.plugins.*;
import java.sql.*;
import java.util.*;
import java.text.*;

public class NewsletterConverter 
{
    private final static String DISPATCH_CONFIG_FILE = "dispatch_config.xml";
    private final static String DISPATCH_CONFIG_CONTEXT = "ORDER_DISPATCHER_CONFIG";
    private static final String SEPARATOR = "~~";
    private static final String TRANSMISSION_TYPE = "NEWS";
    private static final String SUBSCRIBE = "S";
    private static final String UNSUBSCRIBE = "U";

    private Logger logger;

    private String sFTDRequest;
    private String sDateStamp;
    private String sTimeStamp;
    private String sOrigin;
    private String sEmailAddress;
    private String sFirstName;
    private String sLastName;
    private String sDayPhone;
    private String sEveningPhone;
    private String sOldEmailAddress;
    private String sCoBrandCode;
    private String sCoBrandRequest;

    public NewsletterConverter()
    {
        logger = new Logger("com.ftd.osp.orderdispatcher.util.NewsletterConverter");
    }

    public boolean processNewsletterTransmission(OrderVO order, Connection connection)
    {
        try
        {
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest(); 
            dataRequest.setConnection(connection);   
        
            // Initialize transmission
            this.initializeTransmission();
                
            if(order.getStatus() != null)
            {
                // Build transmission values
                Long emailId = this.buildTransmission(order);

                if(emailId != null)
                {
                    logger.info("Preparing newsletter transmission");
                    
                    // Pull HP string from values
                    String hpString = this.getHPSocketString();
                    logger.info("Transformation for newsletter hp complete: " + hpString);

                    // Send transmission to HP
                    this.sendToHP(hpString);

                    // Update newsletter status
                    this.updateNewsletterStatus(emailId, dataRequest, dataAccessUtil);
                }
                else
                {
                    logger.info("Newsletter transmission does not exist or already occurred for this order");
                }
                
            }
            else
            {
                logger.error("ORDER STATUS IS NULL FOR GUID: " + order.getGUID());
                return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return true;
    }

    private Long buildTransmission(OrderVO order)
    {
        Long emailId = null;
        String newsletterFlag = null;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
        sDateStamp=formatter.format(cal.getTime());
        formatter = new SimpleDateFormat ("HHmmss");
        sTimeStamp=formatter.format(cal.getTime());
        sOrigin="OI";

        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            BuyerVO buyer = (BuyerVO) order.getBuyer().get(0);

            sFirstName = buyer.getFirstName();
            sLastName = buyer.getLastName();

            if(buyer.getBuyerPhones() != null && buyer.getBuyerPhones().size() > 0)
            {
                BuyerPhonesVO buyerPhone = null;
                for(int i = 0; i < buyer.getBuyerPhones().size(); i++)
                {
                    buyerPhone = (BuyerPhonesVO) buyer.getBuyerPhones().get(i);
                    if(buyerPhone != null && buyerPhone.getPhoneType() != null && buyerPhone.getPhoneType().equals("HOME"))
                    {
                        sEveningPhone = buyerPhone.getPhoneNumber();
                    }
                    else if(buyerPhone != null && buyerPhone.getPhoneType() != null && buyerPhone.getPhoneType().equals("WORK"))
                    {
                        sDayPhone = buyerPhone.getPhoneNumber();
                    }
                }
            }

            if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() > 0)
            {
                BuyerEmailsVO emailAddress = (BuyerEmailsVO) buyer.getBuyerEmails().get(0);
                emailId = new Long(emailAddress.getBuyerEmailId());
                sEmailAddress = emailAddress.getEmail();
                newsletterFlag = emailAddress.getNewsletter();
            }
        }
        else
        {
            sEmailAddress = "";
            sFirstName = "";
            sLastName = "";
            sDayPhone = "";
            sEveningPhone = "";
        }
        sOldEmailAddress="";

        if(newsletterFlag != null && (newsletterFlag.equals("Y") || newsletterFlag.equals("N"))) 
        {
            if(order.getOrderOrigin() != null && order.getOrderOrigin().equals("GIFTP"))
            {
                sFTDRequest=" ";
                sCoBrandCode="GIFT";
                if(newsletterFlag.equals("Y"))
                    sCoBrandRequest=SUBSCRIBE;
                else if(newsletterFlag.equals("N"))
                    sCoBrandRequest=UNSUBSCRIBE;
            }
            else if(order.getOrderOrigin() != null && (order.getOrderOrigin().equals("BLUMI") || order.getOrderOrigin().equals("BLUMP")))
            {
                sFTDRequest=" ";
                sCoBrandCode="HIGH";
                if(newsletterFlag.equals("Y"))
                    sCoBrandRequest=SUBSCRIBE;
                else if(newsletterFlag.equals("N"))
                    sCoBrandRequest=UNSUBSCRIBE;
            }
            else
            {
                if(newsletterFlag.equals("Y"))
                    sFTDRequest=SUBSCRIBE;
                else if(newsletterFlag.equals("N"))
                    sFTDRequest=UNSUBSCRIBE;

                sCoBrandCode="          ";
                sCoBrandRequest=" ";
            }
        }
        else
        {
            return null;
        }

        return emailId;
    }

    private String getHPSocketString() throws Exception
    {
        StringBuffer result = new StringBuffer();
        result.append( TRANSMISSION_TYPE );
        result.append("      ");
        result.append( sFTDRequest );
        result.append( sDateStamp );
        result.append( sTimeStamp );
        result.append( sOrigin );

        //EMail Address
        String tmpString;
        if( sEmailAddress==null )
            tmpString="";
        else
            tmpString = sEmailAddress;
        tmpString+="                                                  ";
        result.append(tmpString.substring(0,50));

        //First Name
        if( sFirstName==null )
            tmpString="";
        else
            tmpString = sFirstName;
        tmpString+="                    ";
        result.append(tmpString.substring(0,20));

        //Last Name
        if( sLastName==null )
            tmpString="";
        else
            tmpString = sLastName;
        tmpString+="                    ";
        result.append(tmpString.substring(0,20));

        //Daytime phone
        if( sDayPhone==null )
            tmpString = "";
        else
            tmpString = sDayPhone;
        tmpString+="                    ";
        result.append(tmpString.substring(0,20));

        //sEveningPhone phone
        if( sEveningPhone==null )
            tmpString="";
        else
            tmpString=sEveningPhone;
        tmpString+="                    ";
        result.append(tmpString.substring(0,20));

        //Old EMail Address
        result.append("                                                  ");

        //Cobrand
        if( sCoBrandCode==null )
            tmpString="";
        else
            tmpString=sCoBrandCode;
        tmpString+="          ";
        result.append(tmpString.substring(0,10));

        //Co-Brand Request
        if( sCoBrandRequest==null )
            tmpString="";
        else
            tmpString=sCoBrandRequest;
        result.append(tmpString);
        
        return result.toString();
    }

    private void initializeTransmission()
    {
        sFTDRequest = "";
        sDateStamp = "";
        sTimeStamp = "";
        sOrigin = "";
        sEmailAddress = "";
        sFirstName = "";
        sLastName = "";
        sDayPhone = "";
        sEveningPhone = "";
        sOldEmailAddress = "";
        sCoBrandCode = "";
        sCoBrandRequest = "";
    }

    private boolean sendToHP(String hpString) throws Exception
    {
        boolean result = false;

        if ( hpString == null) 
        {
            logger.error("HP String is null");
            return result;
        }

        String hpIpAddress = null;
        Integer port = null;

        if((ConfigurationUtil.getInstance().getProperty(DISPATCH_CONFIG_FILE, "hpIpAddress") != null && !ConfigurationUtil.getInstance().getProperty(DISPATCH_CONFIG_FILE, "hpIpAddress").equals(""))
            && ((ConfigurationUtil.getInstance().getFrpGlobalParm(DISPATCH_CONFIG_CONTEXT, "hpNewsletterPort") != null && !ConfigurationUtil.getInstance().getFrpGlobalParm(DISPATCH_CONFIG_CONTEXT, "hpNewsletterPort").equals(""))))
        {
            hpIpAddress = ConfigurationUtil.getInstance().getProperty(DISPATCH_CONFIG_FILE, "hpIpAddress");
            port = new Integer(ConfigurationUtil.getInstance().getFrpGlobalParm(DISPATCH_CONFIG_CONTEXT, "hpNewsletterPort"));

            logger.debug("Connecting to " + hpIpAddress + " at port " + port + " for newsletter submission");
        }
        else
        {
            logger.debug("Invalid port or ip address specified in configuration file for newsletter submission");
        }
    
        SocketClient client = null;

        try 
        {
            client = new SocketClient(hpIpAddress, port.intValue());
            client.open(); 
            client.send(hpString);
            
            logger.debug("Newsletter HP String has been sent to the HP successfully");
            client.close();            

            result = true;
        } 
        finally 
        {
            if ( client != null )
            {
                try 
                {
                    client.close();
                } 
                catch ( Exception e ) 
                {
                    logger.error(e);
                }
            }
        }
      
        return result;
    }

    private boolean updateNewsletterStatus(Long emailId, DataRequest dataRequest, DataAccessUtil dataAccessUtil) throws Exception
    {
        String status = "";
        
        if((sFTDRequest != null && sFTDRequest.equals(SUBSCRIBE))
           || (sCoBrandRequest != null && sCoBrandRequest.equals(SUBSCRIBE)))
        {
            status = SUBSCRIBE;
        }
        else
        {
            status = UNSUBSCRIBE;
        }
    
        // Update newsletter to processed
        dataRequest.reset();
        dataRequest.setStatementID("UPDATE_NEWSLETTERS");
        dataRequest.addInputParam("BUYER_EMAIL_ID", emailId);
        dataRequest.addInputParam("NEWSLETTER_STATUS", status);
        dataAccessUtil.execute(dataRequest); 

        return true;
    }
}