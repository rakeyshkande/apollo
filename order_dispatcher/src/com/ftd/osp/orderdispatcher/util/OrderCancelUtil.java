package com.ftd.osp.orderdispatcher.util;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import com.ftd.osp.utilities.plugins.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class OrderCancelUtil 
{

  private static OrderCancelUtil orderCancelUtil = new OrderCancelUtil();
  private static boolean initialized;
  // private static long startNumber;
  private static String machineId;
  private static String hostFullName;
  private static final String POP_GET_MAX_HP_SEQUENCE = "POP.GET_MAX_HP_SEQUENCE";
  private static final String POP_INSERT_HP_SEQUENCE = "POP.INSERT_HP_SEQUENCE";
  private static final String OUT_SEQUENCE = "OUT_SEQUENCE";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String ORDER_CANCEL = "OrderCancel";

 private Logger logger;
 
  public OrderCancelUtil()
  { 
    logger = new Logger("com.ftd.osp.orderdispatcher.util.OrderCancelUtil");
    this.init();    
  }
  
  
   public static OrderCancelUtil getInstance()
  {
     return orderCancelUtil;
  }
  
  /**
   * Returns the larges sequence number + 1 to the caller
   * and calls the update method to insert the value to the database
   * 
   * 
   * @return 
   */
  public synchronized String getSequenceNumber(Connection connection,boolean digitOnly)
  {
    // getLargestStartNumber(connection);
    long startNumber = getLargestStartNumber(connection,hostFullName);
    
    String snum = machineId + startNumber;
    String num = snum.substring(machineId.length());
    
    //insert the number into the sequence table
    insertSequenceHP(Long.parseLong(num), hostFullName, connection);
    
    //based on passed in parmater either return the concatened value or just the digits
    String retValue = null;
    if(digitOnly)
    {
        retValue = Long.toString(startNumber);
    }
    else
    {
        retValue = snum;
    }
    
    return retValue;
  }
  
  /**
   * Returns the larges sequence number + 1 to the caller
   * and calls the update method to insert the value to the database
   * 
   * 
   * @return 
   */
  public synchronized String getPartnerSequenceNumber(Connection connection, String sequenceName)
  {
    // getLargestStartNumber(connection);
    long startNumber = getLargestStartNumber(connection, sequenceName);
    
    String snum = machineId + startNumber;
    String num = snum.substring(machineId.length());
    
    //insert the number into the sequence table
    insertSequenceHP(Long.parseLong(num), sequenceName, connection);
    
    return snum;
  }
  
  /* Return machine id*/
  public String getMachineId()
  {
      return machineId;
  }

  private synchronized void init()
  {
      if(!initialized)
      {
        String hostname = null;
        String ip_address = null;      

        try
        {
          hostname = InetAddress.getLocalHost().getHostName();
          ip_address = InetAddress.getLocalHost().getHostAddress();
          hostFullName = hostname; 
          machineId = hostname.substring(0, 3); 
          this.initialized = true;
        }
        catch(Exception e)
        {
          machineId = "tmp";
        } 
        initialized = true;
      }
  }
  
  /**
   * Retrieves the larges number from the HP_SEQUENCE table and adds 1
   */
  private synchronized long getLargestStartNumber(Connection connection, String sequenceName)
  {
     long startNumber = 1;
  
     try
     {
            DataRequest dataRequest = new DataRequest(); 
            HashMap inputParms = new HashMap();
            inputParms.put("IN_PROCESS_TYPE", ORDER_CANCEL );
            inputParms.put("IN_SERVER", sequenceName);
            dataRequest.setInputParams(inputParms);
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(POP_GET_MAX_HP_SEQUENCE);
            BigDecimal sequence = (BigDecimal)DataAccessUtil.getInstance().execute(dataRequest);
            
            String seq = "";
            if(sequence != null)
            {
                seq = String.valueOf(sequence.longValue());
            }
            
            if(seq != null && !seq.equals(""))
            {
                startNumber = (Long.parseLong(seq) + 1);
            }
        }
        catch(Exception e)
        {
          startNumber = 1;
          logger.error(e.toString());
          e.printStackTrace();
        }
        
        return startNumber;
  }
  
  /**
   * Updates the HP_SEQUENCE table witht the host machine name,
   * next sequence number and the process doing the update
   * @param nextSeq
   * @param hostMachine
   */
  private synchronized void insertSequenceHP(long nextSeq, String hostMachine, Connection connection)
  {
      try
        {
            DataRequest dataRequest = new DataRequest(); 
            HashMap inputParms = new HashMap();
            inputParms.put("IN_PROCESS_TYPE", ORDER_CANCEL );
            inputParms.put("IN_SERVER", hostMachine);
            inputParms.put("IN_SEQUENCE", String.valueOf(nextSeq));
            dataRequest.setInputParams(inputParms);
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(POP_INSERT_HP_SEQUENCE);
            Map outputs = (Map)DataAccessUtil.getInstance().execute(dataRequest);
            
            String status = (String)outputs.get(STATUS_PARAM);
            if(status.equalsIgnoreCase("N"))
            {
              String message = (String)outputs.get(MESSAGE_PARAM);
              logger.error(ORDER_CANCEL + ":  " + message);
              throw new Exception();
            }
            else
            {
              logger.debug(ORDER_CANCEL + " Next HP_SEQUENCE: " + hostMachine + " " + nextSeq);
            }
            
        }
        catch(Throwable e)
        {
          logger.error("Unable to insert next sequence number for host " + hostFullName );
          logger.error(e.toString());
          
        }
    
  }
  

  /**
   * Adds the unique sequence to the OrderCancel document 
   * and other OrderCancel specific values.
   * @param document
   * @param sequence
   * @return 
   * @throws java.lang.Exception
   */
  public synchronized  NodeList generateOrderCancelNodes(Document document, String sequence) throws Exception
  {
  
    if (sequence == null && sequence.equals(""))
    {
      logger.error(ORDER_CANCEL + "Util" + " string 'sequence' must contain a value"); 
      throw new Exception("String object 'sequence' must contain a value");
    }
    StringBuffer machineSeq = new StringBuffer(sequence);
    sequence = machineSeq.insert(machineId.length(), "-").toString();
  
    Element walMartDataRoot = document.createElement("walmart_data_root");
    Element walMartSequenceList = document.createElement("walmart_sequence_list");
    
    walMartDataRoot.appendChild(walMartSequenceList);
    
    Element walMartSequence = null;
   
    walMartSequence = document.createElement("walmart_sequence");  
    walMartSequence.appendChild(document.createTextNode(sequence));   
    walMartSequenceList.appendChild(walMartSequence);

    return walMartDataRoot.getChildNodes();
  }//end generateOrderCancelNodes
}