package com.ftd.osp.orderdispatcher.util;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.ftd.osp.utilities.plugins.Logger;



/**
 * Manages the state of and access to an HTTP connection
 * 
 * @author Anshu Gaind
 * @version $Id: HTTPHelper.java,v 1.3 2011/06/30 15:17:29 gsergeycvs Exp $
 */

public class HTTPHelper 
{
   
  private Logger logger  = new Logger("com.ftd.osp.orderdispatcher.util.HttpHelper");
  

  
  
  /**
   * Posts the parameters to the specified HTTP location
   * 
   * @param httpLocation
   * @param parameters
   * @return 
   * @throws java.io.IOException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public String doPost(String urlString, Map parameters)
    throws IOException
  {
    
    int result;
    
    String response;
    
    PostMethod post = new PostMethod(urlString);
    NameValuePair nvPair = null;
    NameValuePair[] nvPairArray = new NameValuePair[parameters.size()];
    int i = 0;
    String name, value;
    
    for(Iterator iter = parameters.keySet().iterator(); iter.hasNext();i++)
    {
      name = (String) iter.next();
      value = (String) parameters.get(name);
      nvPair = new NameValuePair(name, value);
      nvPairArray[i] = nvPair;
    }

    post.setRequestBody(nvPairArray);
    
    HttpClient httpclient = new HttpClient();
    // Execute request
    try {
        result = httpclient.executeMethod(post);
        response = post.getResponseBodyAsString();
    } finally {
        // Release current connection to the connection pool once you are done
        post.releaseConnection();
    }
    String message = "Http Response Code is " + result;
    logger.debug(message);
    
    if ( result != HttpURLConnection.HTTP_OK ) 
    {
      throw new IOException(message);
    }    
    
    return response;
  }
  
}