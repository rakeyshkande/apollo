package com.ftd.osp.orderdispatcher.test;

import junit.framework.*;

import com.ftd.osp.utilities.dataaccess.util.OSPConnectionPool;
import com.ftd.osp.utilities.vo.*;
import java.sql.*;
import com.ftd.osp.orderdispatcher.*;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class OrderRouterTest extends TestCase  {


        // Integration
  //  public static final String DB_CONNECTION_STRING = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:INT1";
  //  public static final String DB_USERNAME = "osp";
  //  public static final String DB_PASSWORD = "privy~owe";


    // Dev
  public static final String DB_CONNECTION_STRING = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV5";
  public static final String DB_USERNAME = "osp";
  public static final String DB_PASSWORD = "osp";

    
    // Hera
   // public static final String DB_CONNECTION_STRING = "jdbc:oracle:thin:@TANTALUS.FTDI.COM:1521:HERA1";
   // public static final String DB_USERNAME = "osp";
    //public static final String DB_PASSWORD = "lil!xt";

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public OrderRouterTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testOrderRouter() {

  
    
        OrderRouter orderRouter = new OrderRouter();
        BusinessConfig businessConfig = new BusinessConfig();
        MessageToken messageToken = new MessageToken();
            
        try
        {
            // Set database connection
            businessConfig.setConnection(OSPConnectionPool.getInstance().getConnection(DB_CONNECTION_STRING, DB_USERNAME, DB_PASSWORD));

            // Set order id
            //messageToken.setMessage("FTD_GUID_-3611505980718788907068338282901313396651018010004160-6562644530-6055101106521022421-17921378070-122809908605411858380-1771112660251-52581962368121630732173-119370388474-399152074214/1");
        // messageToken.setMessage("FTD_GUID_10009707520-5442346650-3130211280-12670633820696051727013573911400-18900551150-16462513241-21126591660-30179335703422522630-2124059292251-200047780468-134540777010050934113235-322208165240/1wlmti");
            businessConfig.setInboundMessageToken(messageToken);
            
            // Execute the order router
            orderRouter.execute(businessConfig);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try { businessConfig.getConnection().close(); }
            catch(SQLException se) {}
        }   
    }

   
  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite() {
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    
    suite.addTest(new OrderRouterTest("testOrderRouter"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(OrderRouterTest.class);
    junit.textui.TestRunner.run(suite());
  }


}