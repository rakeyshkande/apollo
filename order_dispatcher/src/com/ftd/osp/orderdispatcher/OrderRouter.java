package com.ftd.osp.orderdispatcher;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messagegenerator.bo.OrderEmailGeneratorBO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.orderdispatcher.util.HTTPHelper;
import com.ftd.osp.orderdispatcher.util.OrderCancelUtil;
import com.ftd.osp.orderdispatcher.valueobjects.DispatchConfig;
import com.ftd.osp.orderdispatcher.valueobjects.GiftCertMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.BusinessConfig;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.PartnerMappingVO;




/**
 * Primary class for the order dispatcher which manages the execution of the
 * components within the order dispatcher package.
 *
 * @author Brian Munter
 */

public class OrderRouter implements IBusinessObject
{
    private Logger logger;
    private final static String DISPATCH_CONFIG_FILE = "dispatch_config.xml";
    private final static String DISPATCH_CONFIG_CONTEXT = "ORDER_DISPATCHER_CONFIG";
    private final static String FINAL_STATUS_NA = "NA";
    private final static Pattern PATTERN = Pattern.compile("~~\\d{12,24}~~");  //find and 12-24 digit number wrapped in double tildes
    private boolean isWlmti = false;
    private final static String FULL_INSERT = "Full";
    private final static String SKELETON_INSERT = "Skeleton";
    private final static String CLEAN_REMOVED_STATUS = "Removed";
    private final static String CLEAN_PENDING_STATUS = "Pending";
    private final static String ORDER_PROCESSING_STATUS = "ORDER PROCESSING";
    private final static String WALMART_ORDER_INDICATOR = "walmart_order_number";
    
  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";  
  
  public static final String PROCESS_CREATE_ORD_STATUS_UPDATES = "PROCESS_CREATE_ORD_STATUS_UPDATES";
  public final static String JMS_PIPELINE_FOR_EM_PARTNERS = "SUCCESS";
	

    /**
     * The Constructor for OrderRouter
     */
    public OrderRouter()
    {
        logger = new Logger("com.ftd.osp.orderdispatcher.OrderRouter");
    }


    /**
     * Executes the order dispatcher.
     * @param businessConfig the BusinessConfig object which contains the information necessary for the order to be dispatched
     */
    public void execute(BusinessConfig businessConfig)
    {
        try
        {
            //get value fron config file
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            String dispatchTo = config.getProperty(DISPATCH_CONFIG_FILE,"DISPATCH_TO");
            
            if(dispatchTo == null || dispatchTo.length() == 0)
            {
                throw new Exception("DISPATCH_TO value in dispatcher config file is empty or does not exist.");
            }
            else if (dispatchTo.equals("CLEAN"))
            {
                executeClean(businessConfig);
            }
            else
            {
                throw new Exception("Invalid DISPATCH_TO value in dispatcher config file.  Value=" + dispatchTo + ".  Valid value is CLEAN.");
            }
            
        }
        catch(Exception e)
        {
          logger.error(e);
          logger.debug("Order Router: Error retrieving token: " + e.toString());
          MessageToken messageToken = new MessageToken();
          messageToken.setStatus("ROLLBACK");
          businessConfig.setOutboundMessageToken(messageToken);
        }   
    }

    /**
     * Executes the order dispatcher and sends order to Clean schema.
     * 
     * @param businessConfig the BusinessConfig object which contains the information necessary for the order to be dispatched
     */
    public void executeClean(BusinessConfig businessConfig)
    {
        try
        {
           String orderMessage = (String) businessConfig.getInboundMessageToken().getMessage();
           ConfigurationUtil config = ConfigurationUtil.getInstance();
            if( orderMessage.lastIndexOf(config.getProperty(DISPATCH_CONFIG_FILE,"origin_walmart")) > -1 )
            {
               
                executeWalmartCancellationClean(businessConfig);
            }
            else
            {
                executeOrdersClean(businessConfig);
            }  
        }
        catch(Exception e)
        {
          logger.debug("Order Router: Error retrieving token: " + e.toString());
          MessageToken messageToken = new MessageToken();
          messageToken.setStatus("ROLLBACK");
          businessConfig.setOutboundMessageToken(messageToken);
        }   
    }

 /*
  * 4/17/05 emueller
  * This is a copy of the executeOrdersHP method.  This method is used to dispatch an 
  * order to the Clean schema.
  */
  private void executeOrdersClean(BusinessConfig businessConfig) throws Exception
  {
         Connection connection = null;
        try 
        {
            String orderMessage = (String) businessConfig.getInboundMessageToken().getMessage();
            connection = businessConfig.getConnection();


            logger.debug("JMS message:" + orderMessage);

            // Parse guid and item number from order message
            String orderGuid = orderMessage.substring(0, orderMessage.lastIndexOf("/"));
            String lineNumber = orderMessage.substring(orderMessage.lastIndexOf("/") + 1, orderMessage.length());

            logger.debug("Guid="+orderGuid);
            logger.debug("Line Number=" + lineNumber);

            // Load instances of helper classes
            DispatchConfig dispatchConfig = DispatchManager.getInstance().getDispatchConfig();
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
            String sendJMS = configUtil.getProperty(DISPATCH_CONFIG_FILE,"JMS_MESSAGE_TO_ORDER_PROCESSING");


            // Load complete order from dao
            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
            OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);            

            //get the order detail line
            OrderDetailsVO orderDetailVO = this.getDetailRecord(order,lineNumber);

            //NOT NEEDED FOR GOING TO CLEAN
            //this.preOrderProcessingClean(order, lineNumber, configUtil, orderDetailVO.getStatus(), connection);


            if(orderDetailVO != null)
            {
                String xslFile = null;
                String hpString = null;
                String itemId = null;
                Document document = null;
                
                if(orderDetailVO.getStatus() != null && orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.RECEIVED_ITEM)))
                {
                    // This happens when an order is retrieved from the queue, but the transaction from the previous
                    // queue processor has not fully committed it's transaction.  This order just needs to be put back
                    // on the queue.
                    logger.error("Status is not applicable for " + orderGuid + " with status of " + orderDetailVO.getStatus());
                    throw new OrderNotReadyException();
                }

                    if(orderDetailVO.getStatus() != null && orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.PROCESSED_ITEM)))
                    {
                        //This condition will occur when the dispatcher tires to process a FULL dispatch and then a SKELTON dispatch.
                        //This is not typical, but this was seen on production because there are several servers running dispatcher
                        //and one server may pick items off the queuer faster then others.  This will not cause any problems with the
                        //order, but it has been causing a null pointer error to show up in the logs.
                        logger.debug("Attempted to dispatcher order that has a status of 2006. Detail=" + orderDetailVO.getOrderDetailId());                        
                    }
                    else if(orderDetailVO.getStatus() != null)
                    {                       
                        
                        //If valid order detail record insert entire detail record into clean schema
                        if (orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.VALID_ITEM))){

                            logger.debug("Inserting full order detail into Clean. Detail id: " + orderDetailVO.getOrderDetailId());
                            this.insertIntoClean(connection,orderDetailVO.getOrderDetailId(), FULL_INSERT);
                            
                            logger.debug("Inserting to clean: " + orderDetailVO.getOrderDetailId());
                        	// Order Status Update should not halt the regular process
							boolean isPOSUSuccess = true;
							StringBuffer payLoad = null;
							try { 
								PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), connection); 
								 
								if(partnerMappingVO != null) { 
									logger.debug("PartnerMappingVO: " + partnerMappingVO.getPartnerId() + ", " + partnerMappingVO.getFtdOriginMapping() + ", " + partnerMappingVO.isSendOrdStatusUpdFeed());
								}
								if (partnerMappingVO != null && partnerMappingVO.isSendOrdStatusUpdFeed()) {									
									payLoad = new StringBuffer().append("orderNumbers:").append(orderDetailVO.getExternalOrderNumber())
											.append("|").append("operation:").append("ACCEPTED");									
									isPOSUSuccess = sendJMSMessage(JMS_PIPELINE_FOR_EM_PARTNERS, PROCESS_CREATE_ORD_STATUS_UPDATES, payLoad.toString()); 
									logger.debug("Inserted JMS message for PROCESS_CREATE_ORD_STATUS_UPDATES");
								}
							} catch (Exception e) {
								logger.error("Unable to send JMS message for partner order status update feed, ", e);								
							} finally {
								if(!isPOSUSuccess) {
									String posuErrorMessage = "Unable to send JMS message for partner order status update feed. Payload: " + payLoad;
									logger.error(posuErrorMessage);
									sendSystemMessage(connection, posuErrorMessage, "PARTNER INTEGRATION", "PI Order Status Update Error");
								}
							}
							
							 //send  out notification to order processing
                            if(sendJMS.equals("ON")){
                                MessageToken messageToken = new MessageToken();
                                messageToken.setMessage(Long.toString(orderDetailVO.getOrderDetailId()));          
                                messageToken.setStatus(ORDER_PROCESSING_STATUS);
                                Dispatcher dispatcher = Dispatcher.getInstance();       
                                dispatcher.dispatchTextMessage(new InitialContext(),messageToken);  
                            }
							
                        }
                        else if (orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.PENDING_ITEM)))
                        {
                            //something should probably be added to handle pending, removed and walmart cancels
                            logger.debug("Order in pending status: " + orderDetailVO.getOrderDetailId());
                            this.updateCleanDisposition(connection,orderDetailVO.getOrderDetailId(),CLEAN_PENDING_STATUS);                                                  
                        }
                        else if (orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.REMOVED_ITEM)))
                        {
                            //something should probably be added to handle pending, removed and walmart cancels
                            logger.debug("Order in removed status. Detail id: " + orderDetailVO.getOrderDetailId());
                            this.updateCleanDisposition(connection,orderDetailVO.getOrderDetailId(),CLEAN_REMOVED_STATUS);
                            
                            // Check if novator needs to be notified that gift cert order was cancelled
                            this.cleanUpGiftCertificates(order, connection);
                        }
                        else
                        {
                            //else, the order is in scrub
                        
                            logger.debug("Inserting skeleton order detail into Clean. Detail id: " + orderDetailVO.getOrderDetailId() + " Current Status:" + orderDetailVO.getStatus());
                            this.insertIntoClean(connection,orderDetailVO.getOrderDetailId(),SKELETON_INSERT);                                                  
                        }                        
 
                        // Update status
                            DataRequest dataRequest = new DataRequest();
                            dataRequest.setConnection(connection);

                            // Update if final status is not NA
                            if(dispatchConfig.getRule(orderDetailVO.getStatus()).getFinalStatus() != null &&
                               !dispatchConfig.getRule(orderDetailVO.getStatus()).getFinalStatus().equals(FINAL_STATUS_NA))
                            {
                                dataRequest.setStatementID("order_detail_status_update");
                                dataRequest.addInputParam("item id", new Long(orderDetailVO.getOrderDetailId()));
                                dataRequest.addInputParam("updated status", dispatchConfig.getRule(orderDetailVO.getStatus()).getFinalStatus());

                                DataAccessUtil.getInstance().execute(dataRequest);
                                logger.debug("Order detail status updated to '" + dispatchConfig.getRule(orderDetailVO.getStatus()).getFinalStatus() + "' for item number: " + itemId);
                            }

                            // Check if entire order is complete and set status and email
                            dataRequest.reset();
                            dataRequest.setStatementID("order_complete_update");
                            dataRequest.addInputParam("order guid", orderGuid);
                            HashMap orderStatusMap = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);

                            String outStatus = null;
                            String outMessage = null;
                            if(orderStatusMap != null && orderStatusMap.size() > 0)
                            {
                                outStatus = (String) orderStatusMap.get("status");
                                outMessage = (String) orderStatusMap.get("message");

                                if(outStatus != null && outStatus.equals("Y"))
                                {
                                    logger.debug("Primary order status has been updated to 1008");
                                   //do not send email confirmations for Amazon.com || Wal-Mart orders only
                                   //Please note...origin == null for orders coming from Web OE
                                   if( sendConfirmationEmail(connection,order,orderDetailVO.getExternalOrderNumber()) )
                                   {
                                     
                                      if(outMessage != null && outMessage.equals("SEND EMAIL CONFIRMATION") && this.emailAddressExists(order))
                                      {
                                    	  OrderEmailGeneratorBO buildOrderEmail = new OrderEmailGeneratorBO(connection);
  
                                          OrderDetailsVO item = this.getItem(order, lineNumber);
                                          if(item.getReinstateFlag() != null && item.getReinstateFlag().equals("Y"))
                                          {
                                              // Send single item
                                              buildOrderEmail.generateReinstateConfirmationEmail(order, Long.toString(item.getOrderDetailId()));
                                          }
                                          else
                                          {
                                              // Send normal order
                                              buildOrderEmail.generateConfirmationEmail(order);
                                          }
                                      }
                                   }
                                }
                            }
                        
                    }
                    else
                    {
                        logger.error("Status is null for " + orderGuid + " with status of " + orderDetailVO.getStatus());
                    }
                }
            else
            {
                logger.info("No items found for guid " + orderGuid + " with line number " + lineNumber);
            }


              try {
                if(connection != null)
                {
                  connection.close();                
                }
              }catch(Exception e)
              {
                //do nothing
              }            


        }
        catch (OrderNotReadyException ex)
        {
            MessageToken messageToken = new MessageToken();
            messageToken.setStatus("RB_WARNING");
            businessConfig.setOutboundMessageToken(messageToken);
        }
        catch (Exception ex)
        {
        
                
                logger.error(ex);

                try
                {
                    SystemMessengerVO sysVo = new SystemMessengerVO();
                    sysVo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                    StringWriter stringWriter = new StringWriter();
                    ex.printStackTrace(new PrintWriter(stringWriter));
                    sysVo.setMessage(stringWriter.toString());
                    sysVo.setSource("ORDER DISPATCHER");
                    sysVo.setType("EXCEPTION");
                    SystemMessenger.getInstance().send(sysVo,connection,false);
                    
                }
                catch(Exception ee)
                {
                    logger.error(ee);
                    logger.error("Could not log exception to SystemMessenger");
                }


              MessageToken messageToken = new MessageToken();
              messageToken.setStatus("ROLLBACK");
              businessConfig.setOutboundMessageToken(messageToken);
              

      
        }
        finally
        {
  
        }

  }

    /*
     * This method check if the passed in order was created as part of modify
     * order.
     */
     private boolean isModifyOrder(Connection conn,String externalOrderNumber) throws Exception
     {
         boolean isModifyOrder = false;         
    
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
        
         dataRequest.setStatementID("IS_MODIFY_ORDER");
         Map paramMap = new HashMap();   
         
         paramMap.put("IN_EXTERNAL_ORDER_NUMBER",externalOrderNumber);
        
         dataRequest.setInputParams(paramMap);
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         String output = (String) dataAccessUtil.execute(dataRequest);
         
         if(output != null && output.equalsIgnoreCase("Y"))
         {
             isModifyOrder = true;
         }
        
         return isModifyOrder;
     }
 


    /* This method takes in an order object and finds the request line from within
     * that order and returns it. */
    private OrderDetailsVO getDetailRecord(OrderVO order, String lineNumber)
    {
        List lineItems = order.getOrderDetail();
        
        OrderDetailsVO foundOrderDetail = null;
        
        //only search if order contains detail records
        if(lineItems != null)
        {
            //loop through each detail record to find the line we're looking for
            Iterator iter = lineItems.iterator();           
            while(iter.hasNext() && foundOrderDetail == null)
            {
                OrderDetailsVO orderDetail = (OrderDetailsVO)iter.next();
                if(orderDetail.getLineNumber().equals(lineNumber))
                {
                    //found the line
                    foundOrderDetail = orderDetail;
                }//end if found
            }//end while loop
        }//end if details null
        
        return foundOrderDetail;
    }

    private void insertIntoClean(Connection conn,long orderDetailId, String insertType)
        throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        List resultList = new ArrayList();
        
        dataRequest.setStatementID("DISPATCH_ORDER");
        Map paramMap = new HashMap();   
        
        paramMap.put("IN_ORDER_DETAIL_ID",Long.toString(orderDetailId));
        paramMap.put("IN_INSERT_TYPE",insertType);        
        
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }

    }

    private void updateCleanDisposition(Connection conn,long orderDetailId, String disposition)
        throws Exception
    {
    
        //get dispatcher updated by value
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String dispatcherUpdatedBy = config.getProperty(DISPATCH_CONFIG_FILE,"UDPATED_BY");    
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        List resultList = new ArrayList();
        
        dataRequest.setStatementID("UPDATE_CLEAN_DISPOSITION");
        Map paramMap = new HashMap();   
        
        paramMap.put("IN_ORDER_DETAIL_ID",Long.toString(orderDetailId));
        paramMap.put("IN_DISPOSITION",disposition);        
        paramMap.put("IN_UPDATED_BY",dispatcherUpdatedBy);        
        
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }

    }

    private void cleanUpGiftCertificates(OrderVO order, Connection conn)
        throws Exception
    {
        logger.debug("** cleanUpGiftCertificates - entered");
        if(order.getPayments() != null && order.getPayments().size() > 0)
        {
            logger.debug("** cleanUpGiftCertificates - payments exist");
            
            PaymentsVO payment = null;
            Iterator paymentIterator = order.getPayments().iterator();
            
            while(paymentIterator.hasNext())
            {
                payment = (PaymentsVO) paymentIterator.next();
                logger.debug("** cleanUpGiftCertificates - paymentType:" + payment.getPaymentType());
                
                // If payment is a gift cert
                if(payment.getPaymentType() != null && payment.getPaymentMethodType().equals("G"))
                {
                    logger.debug("** cleanUpGiftCertificates - found gift cert:" + payment.getGiftCertificateId());
                    
                    String couponNumber = payment.getGiftCertificateId();
                    String redemptionType = this.getGiftCertRedemptionType(couponNumber, conn);
                    
                    // If novator needs to be notified
                    if(redemptionType.equals("I")) 
                    {
                        // Create message object
                        GiftCertMessageVO giftCertMsg = new GiftCertMessageVO();
                        giftCertMsg.setCouponNumber(couponNumber);
                        giftCertMsg.setRequestType("Reinstate");
                        giftCertMsg.setRetry(0);
                        
                        // Create message token
                        MessageToken messageToken = new MessageToken();
                        messageToken.setStatus("NOVATOR_GIFT_CERT");    
                        messageToken.setMessage(giftCertMsg.toXML());   
                    
                        // Dipatch message
                        Dispatcher dispatcher = Dispatcher.getInstance();
                        dispatcher.dispatchTextMessage(new InitialContext(), messageToken); 
                        
                        logger.debug("** cleanUpGiftCertificates - sending novator message:" + giftCertMsg.toXML());
                    }
                }
            }
        }
    }
    
   private void executeWalmartCancellationClean(BusinessConfig businessConfig)
   {
   Connection connection = null;
    try{

        //get DB connection
        connection = businessConfig.getConnection();        
        
        // Parse guid and item number from order message
        String orderMessage = (String) businessConfig.getInboundMessageToken().getMessage();

        orderMessage = orderMessage.substring(0, orderMessage.lastIndexOf("wlmti"));
        String orderGuid = orderMessage.substring(0, orderMessage.lastIndexOf("/"));
        String lineNumber = orderMessage.substring(orderMessage.lastIndexOf("/") + 1, orderMessage.length());
        logger.debug("ORDER GUID" + orderGuid);
        logger.debug("Line Number: " + lineNumber);         
        
        // Load complete order from dao
        ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
        OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);   
        
        //get the order detail line
        OrderDetailsVO orderDetailVO = this.getDetailRecord(order,lineNumber);
        
        //get cancellations (aka email requet) serlvet location from config file
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String cancelServlet = config.getFrpGlobalParm(DISPATCH_CONFIG_CONTEXT,"CANCEL_SERVLET");     
        
        //get xml to post to servlet
        String postXML = getCancelXML(connection,orderGuid, lineNumber, WALMART_ORDER_INDICATOR);
        
        logger.debug("Posting XML Cancel to " + cancelServlet);
        logger.debug(postXML);
        
        //make parameter map
        HashMap parms = new HashMap();
        parms.put("EmailRequest",postXML);
        
        //Post XML to servlet
        HTTPHelper httpHelper = new HTTPHelper();
        httpHelper.doPost(cancelServlet,parms);

        //the connection close is not in a finally block because if an error
        //occurs a rollback is down by the framework.
          try{
            if(connection != null)
                connection.close();
          }
          catch(Exception e){
            //do nothing
          }   
    }
    catch(Throwable ex)
    {
      logger.error("Unable to process Wal-Mart cancellation request");
      logger.error(ex);
       
      MessageToken messageToken = new MessageToken();
      messageToken.setStatus("ROLLBACK");
      businessConfig.setOutboundMessageToken(messageToken);
    }
   }


    /*
     * Create that XML that should be sent to the cancel servlet.
     * A string buffer is being used to create the XML document 
     * because it is faster then creating an actual XML Document.
     */
     private String getCancelXML(Connection conn, String orderGuid, String lineNumber, String orderIndicator) throws Exception
     {
        StringBuffer xml = new StringBuffer();

        //retrieve order information
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CANCEL_DATA");
        dataRequest.addInputParam("ORDER_GUID", orderGuid);
        dataRequest.addInputParam("LINE_NUMBER", lineNumber);
        CachedResultSet cancelData =  (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);        

        //if no data was returned throw exception
        if(cancelData == null || !cancelData.next() )
        {
            throw new Exception("Order not found in database. Guid=" + orderGuid + " Line=" + lineNumber);
        }
        
        //extract data from cached result set
        String orderDetailId = getStringValue(cancelData.getObject(2));
        String masterOrderNumber = getStringValue(cancelData.getObject(3));
        String externalOrderNumber = getStringValue(cancelData.getObject(4));

        //The data is grabbed from the DB, then formatted to the desired format.
        //Ths is done in multiple steps because the data object returned when
        //running in a local instance is different then from running on the 
        //app server
        String deliveryDate = "";
        if(cancelData.getObject(5) != null){
            String cancelDateString = cancelData.getObject(5).toString();
            logger.debug("Cancel date string:" + cancelDateString);

    		SimpleDateFormat sdfInput = new SimpleDateFormat( "yyyy-MM-dd" );
		    java.util.Date utilDate = sdfInput.parse(cancelDateString);
            deliveryDate =  formatDateToString(utilDate);
        }

        String customerId = getStringValue(cancelData.getObject(6));
        String buyerEmailAddress = getStringValue(cancelData.getObject(7));        
        String companyId = getStringValue(cancelData.getObject(11));            
        String dispositionCode = cancelData.getObject(12).toString();
  
        //obtain seqeuence numbers
        OrderCancelUtil cancelUtil = new OrderCancelUtil();
        String sequenceNumber = cancelUtil.getPartnerSequenceNumber(conn,"walmart-");          
        String machineId = cancelUtil.getMachineId();

        xml.append("<?xml version=\"1.0\"?>");

        xml.append("<email-request>");
        xml.append("<identifier>CSERV</identifier>");

        xml.append("<company-id>");
        xml.append(companyId);
        xml.append("</company-id>");

        xml.append("<server-id>");
        xml.append(machineId);
        xml.append("</server-id>");
        
        xml.append("<sequence-number>");
        xml.append(sequenceNumber);
        xml.append("</sequence-number>");

        xml.append("<type>CX</type>");

        xml.append("<order-number>");
        xml.append(externalOrderNumber);
        xml.append("</order-number>");
        
        xml.append("<delivery-date1>");
        xml.append(deliveryDate);
        xml.append("</delivery-date1>");
        
        xml.append("<sub-type>AT</sub-type>");
        
        xml.append("<billing-email>");
        xml.append(buyerEmailAddress);
        xml.append("</billing-email>");
        
//        xml.append("<order-detail-id>");
//        xml.append(orderDetailId);
//        xml.append("</order-detail-id>");
        
//        xml.append("<order-guid>");
//        xml.append(orderGuid);
//        xml.append("</order-guid>");
        
//        xml.append("<order-external-number>");
//        xml.append(externalOrderNumber);
//        xml.append("</order-external-number>");
        
//        xml.append("<order-master-number>");
//        xml.append(masterOrderNumber);
//        xml.append("</order-master-number>");
        
//        xml.append("<customer-id>");
//        xml.append(customerId);
//        xml.append("</customer-id>");
        
//        xml.append("<order-disp-code>");
//        xml.append(dispositionCode);
//        xml.append("</order-disp-code>");
        
//        xml.append("<order-indicator>");
//        xml.append(orderIndicator);
//        xml.append("</order-indicator>");
        
        xml.append("</email-request>");
        
        return xml.toString();
     }




    private String getStringValue(Object obj)
    {
        return obj == null ? "" : obj.toString();
    }

/**
 * Description: Takes a SQL Date in the format yyyy-mm-dd and formats it
 *              as MM/dd/yyyy (which is the format we show in the screens).
 *
 *
 * @return String
 */
public static String formatDateToString(java.util.Date date)
throws Exception
{
	String strDate = "";

    ConfigurationUtil config = ConfigurationUtil.getInstance();
    String cancleDateFormat = config.getProperty(DISPATCH_CONFIG_FILE,"CANCEL_DATE_FORMAT");

	if (date != null) {
		strDate = date.toString();
		SimpleDateFormat dfOut = new SimpleDateFormat(cancleDateFormat);

		strDate = dfOut.format(date);
	}

	return strDate;
}


    private boolean emailAddressExists(OrderVO order)
    {
        boolean addressExists = false;

        BuyerVO buyer = null;
        BuyerEmailsVO buyerEmail = null;

        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO) order.getBuyer().get(0);
            if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() > 0)
            {
                buyerEmail = (BuyerEmailsVO) buyer.getBuyerEmails().get(0);
                if(buyerEmail != null && buyerEmail.getEmail() != null && !buyerEmail.getEmail().trim().equals("") )
                {
                    addressExists = true;
                }
            }
        }

        return addressExists;
    }

    private OrderDetailsVO getItem(OrderVO order, String lineNumber) throws Exception
    {
        OrderDetailsVO item = null;
        List itemList = order.getOrderDetail();

        if(itemList != null && itemList.size() > 0)
        {
            OrderDetailsVO currentItem = null;
            for(int i = 0; i < itemList.size(); i++)
            {
                item = (OrderDetailsVO) itemList.get(i);
                if(item.getLineNumber() != null && item.getLineNumber().equals(lineNumber))
                {
                    return item;
                }
            }
        }

        return null;
    }

  private boolean sendConfirmationEmail(Connection conn, OrderVO order, String externalOrderNumber) 
        throws TransformerException, IOException, SAXException, ParserConfigurationException, Exception
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        boolean sendEmail = true;
       
        Iterator it = null;
        String key = null;
        String value = null;   
        
        //load list of origins from dispatch_config.xml file
        //where confirmation emails will not be sent out for the order
        ArrayList excludeOrigins = new ArrayList();
        Map map = configUtil.getProperties(DISPATCH_CONFIG_FILE);
        it = map.keySet().iterator();
        
        while(it.hasNext())
        {
           key = (String)it.next();
           value = (String)map.get(key);
       
           if(key.startsWith("CONFIRM_EMAILS_OFF_"))
           {
               excludeOrigins.add(value);
           }
        }
        
        //Add calyx order origins to excluding list 
        excludeOrigins = excludeCalyxOrigins(configUtil, value, excludeOrigins);
        
       try
       {
			String excludedOrigin = null;
			if (order.getOrderOrigin() != null) {
				
				// loop through the exclude origins list to see if we need to turn off sending confirmation emails for this order
				for (int i = 0; i < excludeOrigins.size(); i++) {
					excludedOrigin = excludeOrigins.get(i).toString();
					if (order.getOrderOrigin().equalsIgnoreCase(excludedOrigin)) {
						sendEmail = false;
						break;
					}
				}
				
				// Do not send confirmation emails for Mercent orders.
				if(sendEmail) {
					MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
					if (mercentOrderPrefixes != null && mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
						sendEmail = false;
					}
				}
				
				if(sendEmail) {
					PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), conn);
					if(partnerMappingVO != null && !"Y".equals(partnerMappingVO.getSendOrdConfEmail())) {
						logger.info("Not Sending Confirmation Email for partner orders for partner origin - " + order.getOrderOrigin());
						sendEmail = false;
					}
				}
			}

		} catch (Exception e) {
			logger.error("Problems occured in send confirmation email");
		}
        
       //do not send email for orders that were created as part of modify order
       if(sendEmail)
       {
            if(isModifyOrder(conn,externalOrderNumber))
            {
                sendEmail = false;
            }
       }
       logger.info("MasterOrderNumber :"+order.getMasterOrderNumber());
       logger.info("OrderOrigin :"+order.getOrderOrigin());
       logger.info("sendEmail :"+sendEmail);
        
       return sendEmail;
  }

   /**
   * exclude origins when global parm is set to "N"
   */
	private ArrayList excludeCalyxOrigins(ConfigurationUtil configUtil, String value, ArrayList excludeOrigins){
		try {
			String sendOrderConfMailsForCalyx = configUtil.getFrpGlobalParm(
					DISPATCH_CONFIG_CONTEXT, "SEND_CALYX_CONFIRMATION_EMAILS");
			String calyxOrigins = configUtil.getFrpGlobalParm("GLOBAL_CONFIG",
					"CALYX_ORDER_ORIGINS");
			if ("N".equalsIgnoreCase(sendOrderConfMailsForCalyx)) {
				if (calyxOrigins != null && calyxOrigins.length() > 0) {
					String[] calyxOrderOrigins = calyxOrigins.split(",");
					logger.info("Excluding order confirmations for calyx orders");
					for (String origin : calyxOrderOrigins) {
						excludeOrigins.add(origin);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error occcurred while excluding the calyx order origins(Order Confirmation emails will be sent for Calyx orders).");
		}
		return excludeOrigins;
	}
  
  
    public String getGiftCertRedemptionType(String gccNumber, Connection conn)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getGiftCertRedemptionType");
            logger.debug("Coupon Number : " + gccNumber);
        }
        
        DataRequest request = new DataRequest();
        String redemptionType = "";
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REQUEST_NUMBER", null);
            inputParams.put("IN_GC_COUPON_NUMBER", gccNumber);

            // build DataRequest object
            request.setConnection(conn);
            request.setInputParams(inputParams);
            request.setStatementID("GET_GCCREQ_BY_REQNUM_OR_GCCNUM");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            
            while(outputs.next())
            { 
                redemptionType = outputs.getString("order_source");
                logger.debug("Redemption Type : " + redemptionType);
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getGiftCertRedemptionType");
            } 
        } 
        return redemptionType;
    }
    
    /**
	 * @param status
	 * @param corrId
	 * @param message
	 * @return
	 */
	public boolean sendJMSMessage(String status, String corrId, String message) {
		boolean success = true;
		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus(status);
			messageToken.setJMSCorrelationID(corrId);
			messageToken.setMessage(message);
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} catch (Exception e) {
			logger.error(e);
			success = false;
		}
		return success;
	}
	
	/** Method to send a system message if there is any error processing feed request/scheduled jobs.
	 * @param connection
	 * @param message
	 * @param source
	 * @param Subject 
	 */
	public void sendSystemMessage(Connection connection, String message, String source, String subject) {
		String messageId;
		try {
			logger.error("Sending System Message: " + message);
			
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(source);
			sysMessage.setType("ERROR");
			sysMessage.setMessage(message);
			sysMessage.setSubject(subject);

			SystemMessenger sysMessenger = SystemMessenger.getInstance();
			messageId = sysMessenger.send(sysMessage, connection, false);

			if (messageId == null) {				
				logger.error("Error occured while attempting to send a system message. System message is not sent ");
			}
		} catch(Exception e) {
			logger.error("Error caught sending system sytem message for partner integration Feed Error," + message + ", " + e);
		}
	} 
  
}//End OrderRouter Class
