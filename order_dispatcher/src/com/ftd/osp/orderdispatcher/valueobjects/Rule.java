package com.ftd.osp.orderdispatcher.valueobjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Rule 
{
    private String xslFile;
    private String disasterRecoveryXSLFile;
    private String finalStatus;
    private HashMap attributes;

    public Rule() 
    {
        attributes = new HashMap();
    }

    public String getAttribute(String name) 
    {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) 
    {
        attributes.put(name.trim(), value.trim());
    }

    public String getXslFile() 
    {
        return xslFile;
    }

    public void setXslFile(String newXslFile) 
    {
        xslFile = newXslFile.trim();
    }

    public String getDisasterRecoveryXSLFile()
    {
        return disasterRecoveryXSLFile;
    }
    
    public void setDisasterRecoveryXSLFile(String newDisasterRecoveryXSLFile)
    {
        disasterRecoveryXSLFile = newDisasterRecoveryXSLFile.trim();
    }
    
    public String getFinalStatus() 
    {
        return finalStatus;
    }

    public void setFinalStatus(String newFinalStatus) 
    {
        finalStatus = newFinalStatus.trim();
    }
}