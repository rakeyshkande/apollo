package com.ftd.osp.orderdispatcher.valueobjects;

public interface XMLInterface 
{

  public String toXML() throws IllegalAccessException, ClassNotFoundException;
}