package com.ftd.osp.orderdispatcher.valueobjects;

import com.ftd.osp.orderdispatcher.valueobjects.Rule;

import java.util.HashMap;
import java.util.Map;

public class DispatchConfig 
{
    private String xslFile;
    private HashMap rules;
    private HashMap attributes;

    public DispatchConfig() 
    {
        rules = new HashMap();
        attributes = new HashMap();
    }

    public String getAttribute(String name) 
    {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) 
    {
        attributes.put(name.trim(), value.trim());
    }

    public void setRule(String ruleId, Rule rule)
    {
        this.rules.put(ruleId, rule);
    }

    public Rule getRule(String ruleId){
        return (Rule)this.rules.get(ruleId);
    }

    public Map getRules(){
        return rules;
    }

    public int getRulesSize()
    {
        return rules.size();
    }

    public String getXslFile() 
    {
        return xslFile;
    }

    public void setXslFile(String newXslFile) 
    {
        xslFile = newXslFile.trim();
    }
}