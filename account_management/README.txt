The Project fodlers are set up as follows:
------------------------------------------
Folders: 
cnf - Project Configuration
res - Application Resource Files
src - Java Source Files
web - Web Application Files
test - JUnit Tests and Files

JDeveloper Setup:
------------------------------------------

Create Project: account_management

Create Application: account_management.
Add Folders: cfn, src

Libraries:
Xpp3_min-1.1.4c.jar
Xstream-1.3.1.jar
Commons-io-1.3.1.jar
Dispatcher.jar
MyReferenceUtilitiesJAR
MyReferenceCommonsLangJar
MyReferenceLog4J
Servlet Runtime
JSP Runtime
Struts Runtime
Oracle XML Parser v2
J2EE


Create Application: account_management_test
Add Folders: test

Libraries:
true	JUnit Runtime
true	Commons-io-1.3.1.jar
true	Oracle JDBC


Configuring the Test Database
------------------------------------------
1. The test/com.ftd.osp.account.test.AbstractDBTest contains a connection URL for the 
Development Database. Update this URL for the specific branch being worked on.

Executing Unit tests from command line.
------------------------------------------

The Build file contains a test target that builds and executes JUnit tests. This target
does not execute by default.
1. If Using Ant < 1.7, copy junit.jar into your ant/lib folder.
2. Execute the following:
> cnf int
> ant test
The results will be stored in build/tests/reports.


