package com.ftd.osp.account.feed.bo;

import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;

import junit.framework.TestCase;

/**
 * Tests the overriden AccountNovatorFeedUtilTest to handle different
 * responses from the website
 */
public class AccountNovatorFeedUtilTest extends TestCase
{


  /**
   * Tests the truncate response method to ensure it handles various permutations of the accountTask_response response
   */
  public void testTruncateResponse()
  {
    AccountNovatorFeedUtil accountNovatorFeedUtil = new AccountNovatorFeedUtil();
    
    assertEquals("", accountNovatorFeedUtil.truncateResponse(""));
    
    assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>", accountNovatorFeedUtil.truncateResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>"));
    assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>", accountNovatorFeedUtil.truncateResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>      "));
    assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>", accountNovatorFeedUtil.truncateResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>CON(content)"));
    assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>INVALID_ACCOUNT</status_code><error_message>Account doesn't exists in Website</error_message></accountTask_response>", accountNovatorFeedUtil.truncateResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>INVALID_ACCOUNT</status_code><error_message>Account doesn't exists in Website</error_message></accountTask_response>CON"));
  }


  
  /**
   * Tests parsing of an invalid Response
   * Expect status to be returned as an error
   */
  public void testParseResponseInvalidResponse()
    throws Exception
  {
    AccountNovatorFeedUtil accountNovatorFeedUtil = new AccountNovatorFeedUtil();
    
    NovatorFeedResponseVO response = accountNovatorFeedUtil.parseResponse("test");
    
    assertFalse(response.isSuccess());
    assertNotNull(response.getErrorString());
    assertFalse(response.getIsTransmitException());
    assertNull(response.getTransmitThrowable());
  }
  
  /**
   * Tests parsing of an SUCCESS Response
   * Expect status to be returned as success
   */
  public void testParseResponseSuccessResponse()
    throws Exception
  {
    AccountNovatorFeedUtil accountNovatorFeedUtil = new AccountNovatorFeedUtil();
    
    NovatorFeedResponseVO response = accountNovatorFeedUtil.parseResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>SUCCESS </status_code></accountTask_response>CON(content)");
    
    assertTrue(response.isSuccess());
    assertFalse(response.getIsTransmitException());
    assertNull(response.getTransmitThrowable());
  }
  
  /**
   * Tests parsing of an ERROR Response
   * Expect status to be returned as error
   */
  public void testParseResponseErrorResponse()
    throws Exception
  {
    AccountNovatorFeedUtil accountNovatorFeedUtil = new AccountNovatorFeedUtil();
    
    NovatorFeedResponseVO response = accountNovatorFeedUtil.parseResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>INVALID_ACCOUNT </status_code><error_message>Account doesn't exists in Website</error_message></accountTask_response>CON");
    
    System.out.println(response.getErrorString());
    assertFalse(response.isSuccess());
    assertNotNull(response.getErrorString());
    assertFalse(response.getIsTransmitException());
    assertNull(response.getTransmitThrowable());
  }  
  
  
  /**
   * Tests parsing of an Response with missing status_code node
   * Expect status to be returned as success
   */
  public void testParseResponseResponseMissingStatusCode()
    throws Exception
  {
    AccountNovatorFeedUtil accountNovatorFeedUtil = new AccountNovatorFeedUtil();
    
    NovatorFeedResponseVO response = accountNovatorFeedUtil.parseResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response></accountTask_response>CON(content)");
    
    assertFalse(response.isSuccess());
    assertFalse(response.getIsTransmitException());
    assertNull(response.getTransmitThrowable());
  }    
  
  /**
   * Tests parsing of an Response with missing status_code node
   * Expect status to be returned as success
   */
  public void testParseResponseResponseMissingErrorMessage()
    throws Exception
  {
    AccountNovatorFeedUtil accountNovatorFeedUtil = new AccountNovatorFeedUtil();
    
    NovatorFeedResponseVO response = accountNovatorFeedUtil.parseResponse("<?xml version=\"1.0\" encoding=\"UTF-8\"?><accountTask_response><status_code>INVALID_ACCOUNT</status_code></accountTask_response>CON");
    
    assertFalse(response.isSuccess());
    assertFalse(response.getIsTransmitException());
    assertNull(response.getTransmitThrowable());
  }   
}
