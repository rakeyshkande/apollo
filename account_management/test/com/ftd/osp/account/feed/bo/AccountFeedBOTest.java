package com.ftd.osp.account.feed.bo;

import com.ftd.osp.account.test.AbstractDBTest;
import com.ftd.osp.account.dao.AccountDAO;
import static com.ftd.osp.account.test.TestUtils.getDate;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestSuite;

import junit.textui.TestRunner;

import org.apache.commons.lang.StringUtils;


/**
 * Tests the Account Feed BO
 */
public class AccountFeedBOTest
  extends AbstractDBTest
{
  AccountDAO accountDAO;
  AccountFeedBO accountFeedBO;
  List<String> systemMessages = new ArrayList<String>();
  List<String> feedXMLs = new ArrayList<String>();
  List<Long> programIds = new ArrayList<Long>();

  static final String userId = "junit";

  public AccountFeedBOTest()
  {
  }

  public static void main(String[] args)
  {
    TestRunner runner = new TestRunner();
    runner.run(new TestSuite(AccountFeedBOTest.class));
  }

  /**
   * Sets up the BO and DO for the test.
   */
  public void setUp()
    throws Exception
  {
    super.setUp();
    accountDAO = new AccountDAO();
    systemMessages.clear();
    programIds.clear();
    feedXMLs.clear();
    accountFeedBO = new AccountFeedBO()
        {
          protected void sendSystemMessage(String message, String type)
          {
            systemMessages.add(type + ":" + message);
          }
          
           protected void sendFeedXMLToWebSite(Connection connection, String preTransformedXML)
           {
             feedXMLs.add(preTransformedXML);
           }
        };
        
    
  }


  /**
   * Feed program info for invalid programs
   */
  public void testFeedProgramInfoInvalidProgramId()
  {
    // Check that programs don't exist
    assertNull(accountDAO.getAccountByProgramId(getConnection(), 0L));
    assertNull(accountDAO.getAccountByProgramId(getConnection(), 0L));
    programIds.add(0L);
    programIds.add(0L);
    
    accountFeedBO.feedProgramInfo(getConnection(), programIds);
    assertEquals(2, systemMessages.size()); // Error message for each invalid Program
  }


  /**
   * Feed program info for valid programs and Accounts
   */
  public void testFeedProgramInfoValidPrograms()
  {
    long accountId1 = accountDAO.createActiveAccount(getConnection(), "unittest1@test.com", "FTD", userId);
    long accountId2 = accountDAO.createActiveAccount(getConnection(), "unittest2@test.com", "FTD", userId);
    long accountId3 = accountDAO.createActiveAccount(getConnection(), "unittest3@test.com", "FTD", userId);    

    programIds.add(accountDAO.addProgramToAccount(getConnection(), accountId1, "FREESHIP", "A", "N", getDate("2012-02-02 00:00:00"), getDate("2011-02-01 00:00:00"), "ORD001", userId));
    programIds.add(accountDAO.addProgramToAccount(getConnection(), accountId2, "FREESHIP", "I", "N", getDate("2012-03-02 00:00:00"), getDate("2011-03-01 00:00:00"), "ORD002", userId));
    programIds.add(accountDAO.addProgramToAccount(getConnection(), accountId3, "FREESHIP", "A", "Y", getDate("2012-04-02 00:00:00"), getDate("2011-04-01 00:00:00"), "ORD003", userId));
    
    accountFeedBO.feedProgramInfo(getConnection(), programIds);
    assertEquals(0, systemMessages.size()); // No Errors
    assertEquals(3, feedXMLs.size());
    assertEquals(1, StringUtils.countMatches(feedXMLs.get(0), "<program>"));    
    assertEquals(1, StringUtils.countMatches(feedXMLs.get(1), "<program>"));    
    assertEquals(1, StringUtils.countMatches(feedXMLs.get(2), "<program>"));    
    
    System.out.println(super.getName());
    System.out.println(feedXMLs.get(0));
    System.out.println(feedXMLs.get(1));
    System.out.println(feedXMLs.get(2));
  }
  
   /**
   * Feed multiple programs info for an Accounts
   */
  public void testFeedProgramInfoMultipleProgramsPerAccount()
  {
    long accountId1 = accountDAO.createActiveAccount(getConnection(), "unittest1@test.com", "FTD", userId);

    long programId = accountDAO.addProgramToAccount(getConnection(), accountId1, "FREESHIP", "A", "N", getDate("2012-02-02 00:00:00"), getDate("2011-02-01 00:00:00"), "ORD001", userId);
    
    programIds.add(programId);
    programIds.add(programId);
    programIds.add(programId);

    accountFeedBO.feedProgramInfo(getConnection(), programIds);
    assertEquals(0, systemMessages.size()); // No Errors
    assertEquals(1, feedXMLs.size());
    assertEquals(3, StringUtils.countMatches(feedXMLs.get(0), "<program>"));
    
    System.out.println(super.getName());
    System.out.println(feedXMLs.get(0));
    
  }
}
