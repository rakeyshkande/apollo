package com.ftd.osp.account.dao;


import com.ftd.osp.account.vo.AccountEmailVO;
import com.ftd.osp.account.vo.AccountMasterVO;

import com.ftd.osp.account.vo.AccountProgramVO;

import com.thoughtworks.xstream.XStream;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Mock Implementation of the AccountDAO to support Unit Testing.
 * Overrides the DAO Implementation to use local storage of VOs.
 * Used as DAO not implemented yet. Also, enable Unit test without DB.
 */
public class AccountDAOMockImpl
  extends AccountDAO
{
  private long accountMasterIdSeq = 1;
  private long accountEmailIdSeq = 1;
  private long accountProgramIdSeq = 1;

  List<AccountMasterVO> mockAccounts = new ArrayList<AccountMasterVO>();

  List<AccountEmailVO> mockEmails = new ArrayList<AccountEmailVO>();
  
  List<AccountProgramVO> mockPrograms = new ArrayList<AccountProgramVO>();

  Map<Long, AccountMasterVO> mapProgramIdToAccountMaster = new HashMap<Long, AccountMasterVO>();
  
  /**
   * @return The List of Mock Accounts
   */
  public List<AccountMasterVO> getMockAccounts()
  {
    return mockAccounts;
  }

  /**
   * Find an account by Id
   * @param accountMasterId
   * @return
   */
  public AccountMasterVO getAccountMasterById(long accountMasterId)
  {
    for (AccountMasterVO accountMasterVO: mockAccounts)
    {
      if (accountMasterVO.getAccountMasterId() == accountMasterId)
      {
        return accountMasterVO;
      }
    }

    return null;
  }
  
  /**
   * Find an account program by Id
   * @param accountProgramId
   * @return
   */
  public AccountProgramVO getAccountProgramById(long accountProgramId)
  {
    for (AccountProgramVO accountProgramVO: mockPrograms)
    {
      if (accountProgramVO.getAccountProgramId() == accountProgramId)
      {
        return accountProgramVO;
      }
    }

    return null;    
  }

  // Overridded Mock DAO Methods

  public long createActiveAccount(Connection connection, String emailAddress, String userId)
  {
    AccountMasterVO accountMasterVO = getActiveAccount(connection, emailAddress);

    if (accountMasterVO != null)
    {
      return accountMasterVO.getAccountMasterId();
    }

    // Create a new Account
    accountMasterVO = new AccountMasterVO();
    accountMasterVO.setAccountMasterId(accountMasterIdSeq++);

    mockAccounts.add(accountMasterVO);
    insertAccountEmail(connection, accountMasterVO.getAccountMasterId(), emailAddress, true, userId);

    return accountMasterVO.getAccountMasterId();
  }


  public AccountMasterVO getActiveAccount(Connection connection, String emailAddress)
  {
    for (AccountMasterVO accountMasterVO: mockAccounts)
    {

      for (AccountEmailVO accountEmailVO: accountMasterVO.getAccountEmails())
      {
        if (accountEmailVO.isActiveFlag() && emailAddress.equals(accountEmailVO.getEmailAddress()))
        {
          return accountMasterVO;
        }
      }
    }

    return null;
  }

  public void insertAccountEmail(Connection connection, long accountMasterId, String emailAddress, boolean active, 
                                 String userId)
  {
    AccountMasterVO accountMasterVO = getAccountMasterById(accountMasterId);
    AccountEmailVO accountEmailVO = new AccountEmailVO();

    accountEmailVO.setAccountEmailId(accountEmailIdSeq++);
    accountEmailVO.setEmailAddress(emailAddress);
    accountEmailVO.setActiveFlag(active);

    accountMasterVO.getAccountEmails().add(accountEmailVO);
    mockEmails.add(accountEmailVO);

  }

  public void setAccountEmailInactive(Connection connection, String emailAddress, String userId)
  {
    AccountMasterVO accountMasterVO = getActiveAccount(connection, emailAddress);
    
    if(accountMasterVO != null)
    {
      for (AccountEmailVO accountEmailVO: accountMasterVO.getAccountEmails())
      {
        if (accountEmailVO.isActiveFlag() && emailAddress.equals(accountEmailVO.getEmailAddress()))
        {
          accountEmailVO.setActiveFlag(false);
          break;
        }
      }    
    }   
  }

  public long addProgramToAccount(Connection connection, long accountMasterId, String programName, String programStatus, 
                                  String autoRenewFlag, Date expirationDate, Date startDate, String externalOrderNumber, String userId)
  {
     AccountMasterVO accountMasterVO = getAccountMasterById(accountMasterId);
     
     AccountProgramVO accountProgramVO = new AccountProgramVO();
     accountProgramVO.setAccountProgramId(accountProgramIdSeq ++);
     accountProgramVO.setProgramName(programName);
     accountProgramVO.setAccountProgramStatus(programStatus);
     accountProgramVO.setAutoRenewFlag(autoRenewFlag);
     accountProgramVO.setExpirationDate(expirationDate);
     accountProgramVO.setExternalOrderNumber(externalOrderNumber);
     accountProgramVO.setStartDate(startDate);
     
     
     accountMasterVO.getAccountPrograms().add(accountProgramVO);
     mockPrograms.add(accountProgramVO);
     mapProgramIdToAccountMaster.put(new Long(accountProgramVO.getAccountProgramId()), accountMasterVO);
     
     return accountProgramVO.getAccountProgramId();
  }
  
  public void updateAccountProgram(Connection connection, long accountProgramId, String programStatus, String autoRenewFlag, 
                                   Date expirationDate, Date startDate, String externalOrderNumber, String userId)
  {
     AccountProgramVO accountProgramVO  = getAccountProgramById(accountProgramId);
     
     accountProgramVO.setAccountProgramStatus(programStatus);
     accountProgramVO.setAutoRenewFlag(autoRenewFlag);
     accountProgramVO.setExpirationDate(expirationDate);
     accountProgramVO.setStartDate(startDate);
     accountProgramVO.setExternalOrderNumber(externalOrderNumber);
  }
  
  
  public AccountMasterVO getAccountByProgramId(Connection connection, long accountProgramId)
  {
     AccountProgramVO accountProgramVO  = getAccountProgramById(accountProgramId);
     AccountMasterVO accountMasterVO = mapProgramIdToAccountMaster.get(new Long(accountProgramId));
     
     if(accountMasterVO == null || accountProgramVO == null)
     {
       return null;
     }
     
     // return a copy with only this program in it
     XStream xstream = new XStream(); 
     AccountMasterVO retVal  = (AccountMasterVO) xstream.fromXML(xstream.toXML(accountMasterVO));
     
     retVal.getAccountPrograms().clear();
     retVal.getAccountPrograms().add(accountProgramVO);
     return retVal;
  }
  
  
   public List<AccountProgramVO> getProgramsByExternalOrderNumber(Connection connection, String externalOrderNumber)
  {
    List<AccountProgramVO> retVal = new ArrayList<AccountProgramVO>();
    
    for(AccountProgramVO accountProgramVO: mockPrograms)
    {
      if(externalOrderNumber.equals(accountProgramVO.getExternalOrderNumber()))  
      {
        retVal.add(accountProgramVO);
      }
    }
    
    return retVal;
  }
}
