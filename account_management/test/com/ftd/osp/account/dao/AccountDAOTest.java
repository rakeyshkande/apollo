package com.ftd.osp.account.dao;

import com.ftd.osp.account.test.AbstractDBTest;
import static com.ftd.osp.account.test.TestUtils.getDate;
import com.ftd.osp.account.vo.AccountEmailVO;
import com.ftd.osp.account.vo.AccountMasterVO;
import com.ftd.osp.account.vo.AccountProgramVO;

import java.util.Date;
import java.util.List;

import junit.framework.TestSuite;

import junit.textui.TestRunner;


/**
 * Tests the Account DAO.
 * 
 * Note: Email addresses are tested with mix'ed case. But the Account Maintenance DB Packages will always convert to lower case,
 * so, ensure that the comparisons are done against lower case.
 */
public class AccountDAOTest extends AbstractDBTest
{

  AccountDAO accountDAO;
  
  public AccountDAOTest()
  {
  }
  
  public static void main(String[] args)
  {
    TestRunner runner = new TestRunner();
    runner.run(new TestSuite(AccountDAOTest.class));      
  }
  
  public void setUp() throws Exception
  {
    super.setUp();
    accountDAO = new AccountDAO();
  }
  
  /////////////////////////////// Helper Methods       //////////////////////////////
  private void assertEmailStatus(List<AccountEmailVO> emailList, String emailAddress, boolean status)
  {
    // Account APIs convert the email addresses to lower case
    emailAddress = emailAddress.toLowerCase();
    for (AccountEmailVO accountEmail: emailList)
    {
      if (emailAddress.equals(accountEmail.getEmailAddress()))
      {
        assertEquals(status, accountEmail.isActiveFlag());
        return;
      }
    }    
    
    fail("Email Address not Found - " + emailAddress);
  }  
  
  private void assertProgramStatus(List<AccountProgramVO> programList, String programName, String programStatus, String autoRenewFlag, Date expirationDate, Date startDate, String externalOrderNumber)
  {
    for(AccountProgramVO accountProgramVO: programList)
    {
      if(programName.equals(accountProgramVO.getProgramName())
        && programStatus.equals(accountProgramVO.getAccountProgramStatus())
        && autoRenewFlag.equals(accountProgramVO.getAutoRenewFlag())
        && expirationDate.equals(accountProgramVO.getExpirationDate())
        && startDate.equals(accountProgramVO.getStartDate())
        && externalOrderNumber.equals(accountProgramVO.getExternalOrderNumber())
      )
      {
        return;
      }
    }
    
    fail("Missing Program By Status- " + programName);
  }  
  
  /////////////////////////////// Test Methods       //////////////////////////////  
  /**
   * Tests the create account function
   */
  public void testCreateNewAccount()
  {
    // Create a new account
    long accountId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
    assertTrue(accountId > 0);
    
    // Create the same account again and assert the same ID is returned
    long accountId1 = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
    assertEquals(accountId, accountId1);    
    
  }


  /**
   * Tests adding a Program to an Account twice with the same order number.
   * The Program is only added once to the database.
   */
  public void testAddProgramToAccountsSameProgramOrder()
  {
    // Create an account to test programs against
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
        
    // Add a program to the account
    long programId = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", getDate("2011-06-01 00:00:00"), getDate("2011-01-01 00:00:00"), "ORDER001", "junit");
    assertTrue(programId > 0);
    
    // Add a program to the account agaom.assert has the same id as before.
    long programId2 = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", getDate("2011-06-01 00:00:00"), getDate("2011-01-01 00:00:00"), "ORDER001","junit");
    assertEquals(programId, programId2);
  }
  
   /**
   * Tests adding a Program to an Account with 2 order numbers.
   * The Program is twice.
   */
  public void testAddProgramToAccountsDifferentProgramOrder()
  {
    // Create an account to test programs against
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
        
    // Add a program to the account
    long programId = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", getDate("2011-06-01 00:00:00"), getDate("2011-01-01 00:00:00"), "ORDER001", "junit");
    assertTrue(programId > 0);
    
    // Add a program to the account agaom.assert has the same id as before.
    long programId2 = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", getDate("2011-06-01 00:00:00"), getDate("2011-01-01 00:00:00"), "ORDER002","junit");
    assertTrue(programId != programId2);
  }
    
  /**
   * Tests retrieving an Active Account from the database
   * Verifies that timestamps are truncated on the insert
   * 
   * This also tests insertAccounEmail
   */
  public void testGetActiveAccountWithProgramsAndEmails()
  {
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com"));
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");  
    accountDAO.insertAccountEmail(getConnection(), accountMasterId, "Email1@Test.com", false, "junit");
    accountDAO.insertAccountEmail(getConnection(), accountMasterId, "Email2@Test.com", false, "junit");
    Date startDate = getDate("2011-01-01 00:00:00");
    Date expirationDateInserted = getDate("2011-06-01 23:59:59");
    Date expirationDateActual = getDate("2011-06-01 00:00:00");
    
    String externalOrderNumber = "ORDER001";
    long programId = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", expirationDateInserted, startDate, externalOrderNumber, "junit");
    
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com");
    assertNotNull(accountMasterVO);
    assertNotNull(accountMasterVO.getAccountEmails());
    assertEquals(3, accountMasterVO.getAccountEmails().size());
    
    assertEmailStatus(accountMasterVO.getAccountEmails(), "UnitTest@Test.com", true);
    assertEmailStatus(accountMasterVO.getAccountEmails(), "Email1@Test.com", false);
    assertEmailStatus(accountMasterVO.getAccountEmails(), "Email2@Test.com", false);
    
    assertEquals(1, accountMasterVO.getAccountPrograms().size()); 
    AccountProgramVO accountProgramVO = accountMasterVO.getAccountPrograms().get(0);
    assertEquals(programId, accountProgramVO.getAccountProgramId());
    assertEquals(startDate, accountProgramVO.getStartDate());
    assertEquals(expirationDateActual, accountProgramVO.getExpirationDate());
    assertEquals(externalOrderNumber, accountProgramVO.getExternalOrderNumber());
  }
  
  /**
   * Tests retrieving an Active Account from the database
   */
  public void testGetActiveAccountWithEmailOnly()
  {
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com"));
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");  
    
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com");
    assertNotNull(accountMasterVO);
    assertNotNull(accountMasterVO.getAccountEmails());
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEquals(0, accountMasterVO.getAccountPrograms().size()); 
  }  
  
  
  /**
   * Tests setting an Account's Email Inactive. The account should not be accessible via the
   * getActiveAccount method after this call
   */
  public void testSetAccountEmailInactive()
  {
    // Ensure no existing account
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com"));
    
    // Verify no harm calling when account email not active
    accountDAO.setAccountEmailInactive(getConnection(),  "FTD", "UnitTest@Test.com", "junit"); 
    
    // Create an Account
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");  
    
    // Set account Inactive, and verify account is inactive
    accountDAO.setAccountEmailInactive(getConnection(),   "FTD", "UnitTest@Test.com", "junit"); 
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com"));
  }
  
  
  /**
   * Tests adding a Program to an Account
   */
  public void testUpdateAccountProgram()
  {
    // Create an account to test programs against
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
    
    Date originalStartDate = getDate("2011-01-01 00:00:00");
    Date originalExpirationDate = getDate("2011-06-01 00:00:00");
    String originalExternalOrderNumber = "ORDER001";    
            
    // Add a program to the account
    long programId = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", originalExpirationDate, originalStartDate, originalExternalOrderNumber, "junit");
    assertTrue(programId > 0);
    
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com");
    assertEquals(1, accountMasterVO.getAccountPrograms().size());
    AccountProgramVO originalProgram = accountMasterVO.getAccountPrograms().get(0);
    assertEquals(originalStartDate, originalProgram.getStartDate());
    assertEquals(originalExpirationDate, originalProgram.getExpirationDate());
    assertEquals(originalExternalOrderNumber, originalProgram.getExternalOrderNumber());
    
    // Update the Program and Confirm
    Date newStartDate = getDate("2011-02-01 00:00:00");
    Date newExpirationDate = getDate("2011-07-01 00:00:00");    
    String newaccountNumber = "ORDER002";
    accountDAO.updateAccountProgram(getConnection(), programId, "I", "N", newExpirationDate, newStartDate, newaccountNumber, "junit");
    
    accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com");
    assertEquals(1, accountMasterVO.getAccountPrograms().size());

    AccountProgramVO updatedProgram = accountMasterVO.getAccountPrograms().get(0);
    
    assertEquals(programId, updatedProgram.getAccountProgramId());
    assertEquals("FREESHIP", updatedProgram.getProgramName());
    assertEquals("N", updatedProgram.getAutoRenewFlag());
    assertEquals("I", updatedProgram.getAccountProgramStatus());
    assertEquals(newStartDate, updatedProgram.getStartDate());
    assertEquals(newExpirationDate, updatedProgram.getExpirationDate());
    assertEquals(newaccountNumber, updatedProgram.getExternalOrderNumber());
  }
  
  
    /**
   * Tests retrieving the Account/Program Information by the Program Id
   */
  public void testGetAccountByProgramIdNoProgram()
  {
    AccountMasterVO accountMasterVO = accountDAO.getAccountByProgramId(getConnection(), -1L);
    assertNull(accountMasterVO);    
  }
  
  /**
   * Tests retrieving the Account/Program Information by the Program Id
   */
  public void testGetAccountByProgramId()
  {
    // Create an account to test programs against
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
    
    Date originalStartDate = getDate("2011-01-01 00:00:00");
    Date originalExpirationDate = getDate("2011-06-01 00:00:00");
    String originalExternalOrderNumber = "ORDER001";    
    
    // Add a program to the account
    long programId = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", originalExpirationDate, originalStartDate, originalExternalOrderNumber, "junit");
    assertTrue(programId > 0);
    
    // Retrieve the Account/Program Information by the Program Id
    AccountMasterVO accountMasterVO = accountDAO.getAccountByProgramId(getConnection(), programId);
    assertNotNull(accountMasterVO);
    
    assertEquals("UnitTest@Test.com".toLowerCase(), accountMasterVO.getActiveEmailAddress());
    assertTrue(accountMasterVO.isAccountActive());
    
    assertEquals(1, accountMasterVO.getAccountPrograms().size());
    AccountProgramVO accountProgram = accountMasterVO.getAccountPrograms().get(0);
    
    assertEquals(programId, accountProgram.getAccountProgramId());    
    assertNotNull(accountProgram.getUpdatedOn());
    assertEquals(originalExternalOrderNumber, accountProgram.getExternalOrderNumber());
  }
  
  /**
   * Tests retrieving the Account/Program Information by the Program Id
   * with an inactive Program
   */
  public void testGetAccountByProgramIdWithInactiveProgram()
  {
    // Create an account to test programs against
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest@Test.com", "FTD", "junit");
    
    Date originalStartDate = getDate("2011-01-01 00:00:00");
    Date originalExpirationDate = getDate("2011-06-01 00:00:00");
    String originalExternalOrderNumber = "ORDER001";    
    
    // Add a program to the account
    long programId = accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", originalExpirationDate, originalStartDate, originalExternalOrderNumber, "junit");
    assertTrue(programId > 0);
    
    accountDAO.setAccountEmailInactive(getConnection(), "FTD", "UnitTest@Test.com", "junit");
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "UnitTest@Test.com")); // Account should not be returned
    
    // Retrieve the Account/Program Information by the Program Id, the Account is returned
    AccountMasterVO accountMasterVO = accountDAO.getAccountByProgramId(getConnection(), programId);
    assertNotNull(accountMasterVO);
    assertNull(accountMasterVO.getActiveEmailAddress());
    assertFalse(accountMasterVO.isAccountActive());
    assertNotNull(accountMasterVO.getAccountPrograms().get(0).getUpdatedOn());    
    assertEquals(originalExternalOrderNumber, accountMasterVO.getAccountPrograms().get(0).getExternalOrderNumber());
  }  
  
  
   /**
   * Tests retrieving the Account/Program Information by the Program Id
   * with no order tied to the order number requested
   */
  public void testGetProgramsByOrderWithInvalidOrder()
  {
    final Date startDate = getDate("2011-01-01 00:00:00");
    final Date expirationDate = getDate("2011-06-01 00:00:00");
    final String externalOrderNumber = "ORDER001";   
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest1@Test.com", "FTD", "junit");
    accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber, "junit");    
  
    List<AccountProgramVO> accountPrograms = accountDAO.getProgramsByExternalOrderNumber(getConnection(), "junitjunkorder");
  
    assertNotNull(accountPrograms);
    assertEquals(0, accountPrograms.size());
  }  
 
 
 
  /**
   * Tests retrieving the Account/Program Information by the Program Id
   * with existing programs with the order.
   * 
   * There are 3 account programs inserted. 2 with the same order number.
   * Expect the test to retrieve 2 program records
   */
  public void testGetProgramsByOrderWithOrderMultiplePrograms()
  {
    final Date startDate = getDate("2011-01-01 00:00:00");
    final Date expirationDate = getDate("2011-06-01 00:00:00");
    final String externalOrderNumber = "ORDER001";    
    
    // Create an account to test programs against
    long accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest1@Test.com", "FTD", "junit");
    accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber, "junit");  
    accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest2@Test.com", "FTD", "junit");
    accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "I", "N", expirationDate, startDate, externalOrderNumber, "junit");    
    accountMasterId = accountDAO.createActiveAccount(getConnection(), "UnitTest3@Test.com", "FTD", "junit");
    accountDAO.addProgramToAccount(getConnection(), accountMasterId, "FREESHIP", "A", "Y", expirationDate, startDate, "junitjunkorder", "junit");      
  
    List<AccountProgramVO> accountPrograms = accountDAO.getProgramsByExternalOrderNumber(getConnection(), externalOrderNumber);
  
    assertNotNull(accountPrograms);
    assertEquals(2, accountPrograms.size());
    
    assertProgramStatus(accountPrograms, "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);
    assertProgramStatus(accountPrograms, "FREESHIP", "I", "N", expirationDate, startDate, externalOrderNumber);
  }   
}
