package com.ftd.osp.account.xml.bo;

import com.ftd.osp.account.common.AccountConstants;
import static com.ftd.osp.account.test.TestUtils.getDate;
import com.ftd.osp.account.vo.AccountProgramVO;
import com.ftd.osp.account.xml.vo.AccountManagementTaskVO;
import com.ftd.osp.account.xml.vo.IdentityVO;

import java.io.InputStream;

import java.io.StringReader;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import javax.xml.validation.Validator;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

import org.apache.commons.io.IOUtils;

import org.xml.sax.InputSource;


public class AccountXMLBOTest extends TestCase
{
  AccountXMLBO accountXMLBO;
  
  public AccountXMLBOTest()
  {
  }
  
    public static void main(String[] args)
    {
      TestRunner runner = new TestRunner();
      runner.run(new TestSuite(AccountXMLBOTest.class));      
    }
    
  protected void setUp()
  {
     accountXMLBO = new AccountXMLBO();
    
  }
  
 
  /**
   * Returns the passed in resource file as a String
   * @param filePath
   * @return
   */
  private String getTextFileFromResource(String filePath)
  {
    try
    {
      InputStream is = getClass().getResourceAsStream(filePath);
      return IOUtils.toString(is);
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Tests the Parsing for a Merge with 3 Accounts.
   */
  public void testParseAccountManagementTask_MergeAccountsXML()
  {
    String xml = getTextFileFromResource("/adminTask_Merge3Accounts.xml");
    
    AccountManagementTaskVO accountManagementTaskVO = accountXMLBO.parseAccountManagementTaskFromXML(xml);
    
    assertNotNull(accountManagementTaskVO);
    
    assertEquals("merge", accountManagementTaskVO.getTaskType());
    assertEquals("primary@someplace.com", accountManagementTaskVO.getIdentity().getEmailAddress());
    
    assertEquals(3, accountManagementTaskVO.getAccounts().size());
    
    assertEquals("merged1@someplace.com", accountManagementTaskVO.getAccounts().get(0).getEmailAddress());
    assertEquals("merged2@someplace.com", accountManagementTaskVO.getAccounts().get(1).getEmailAddress());
    assertEquals("merged3@someplace.com", accountManagementTaskVO.getAccounts().get(2).getEmailAddress());    
  }
  
  /**
   * Tests the Parsing for a Merge with 3 Accounts with unknown tags. Verifies
   * the parse ignores new tags and attributes.
   */
  public void testParseAccountManagementTask_MergeAccountsWithUnknownTagsXML()
  {
    String xml = getTextFileFromResource("/adminTask_Merge3AccountsWithUnknownTags.xml");
    
    AccountManagementTaskVO accountManagementTaskVO = accountXMLBO.parseAccountManagementTaskFromXML(xml);
    
    assertNotNull(accountManagementTaskVO);
    
    assertEquals("merge", accountManagementTaskVO.getTaskType());
    assertEquals("primary@someplace.com", accountManagementTaskVO.getIdentity().getEmailAddress());
    
    assertEquals(3, accountManagementTaskVO.getAccounts().size());
    
    assertEquals("merged1@someplace.com", accountManagementTaskVO.getAccounts().get(0).getEmailAddress());
    assertEquals("merged2@someplace.com", accountManagementTaskVO.getAccounts().get(1).getEmailAddress());
    assertEquals("merged3@someplace.com", accountManagementTaskVO.getAccounts().get(2).getEmailAddress());    
  }  


  /**
   * Tests parsing the Delete Accounts XML 
   */
  public void testParseAccountManagementTask_DeleteAccountsXML()
  {
    String xml = getTextFileFromResource("/adminTask_DeleteAccount.xml");
    
    AccountManagementTaskVO accountManagementTaskVO = accountXMLBO.parseAccountManagementTaskFromXML(xml);
    
    assertNotNull(accountManagementTaskVO);
    
    assertEquals("delete", accountManagementTaskVO.getTaskType());
    assertEquals("user1@noplace.com", accountManagementTaskVO.getIdentity().getEmailAddress());
    
    assertNull(accountManagementTaskVO.getAccounts());
   
  }
  
  /**
   * Tests parsing an Update Account XML
   */
  public void testParseAccountManagementTask_UpdateAccountEmailXML()
  {
    String xml = getTextFileFromResource("/adminTask_UpdateAccountEmail.xml");
    
    AccountManagementTaskVO accountManagementTaskVO = accountXMLBO.parseAccountManagementTaskFromXML(xml);
    
    assertNotNull(accountManagementTaskVO);
    
    assertEquals("merge", accountManagementTaskVO.getTaskType());
    assertEquals("user1@oldplace.com", accountManagementTaskVO.getIdentity().getEmailAddress());
    
    assertEquals(1, accountManagementTaskVO.getAccounts().size());
    
    assertEquals("user2@newplace.com", accountManagementTaskVO.getAccounts().get(0).getEmailAddress());
  }  

 
  /**
   * Tests parsing an unrecognized XML File
   */
  public void testParseAccountManagementTask_UnknownXMLFile()
  {
    String xml = getTextFileFromResource("/adminTask_UnknownRootTag.xml");
    
    try 
    {
      accountXMLBO.parseAccountManagementTaskFromXML(xml);
    } catch (Exception e)
    {
      return;
    }
    
    fail("File parse did not fail");
  }  
  
  
  /**
   * Tests the conversion of the AccountManagementTaskVO to XML where the VO is empty
   * Verify no NullPointer Exceptions
   */
  public void testConvertAccountManagementTask_EmptyVO()
  {
    AccountManagementTaskVO accountManagementTaskVO = new AccountManagementTaskVO();
    String xml = accountXMLBO.convertAccountManagementTaskToXML(accountManagementTaskVO);
    assertNotNull(xml);
    assertTrue(xml.length() > 0);
    AccountManagementTaskVO parsedValue = accountXMLBO.parseAccountManagementTaskFromXML(xml);
    assertNotNull(parsedValue);
  }
  
  
  /**
   * Helper method to generate/populate an Account Program VO
   * @param programName
   * @param programStatus
   * @param autoRenewFlag
   * @param expirationDate
   * @param startDate
   * @param updateOn
   * @return
   */
  private AccountProgramVO getAccountProgramVO(String programName, String programStatus, String autoRenewFlag, Date expirationDate, Date startDate, String externalOrderNumber, Date updateOn)
  {
    AccountProgramVO accountProgramVO = new AccountProgramVO();
    accountProgramVO.setAccountProgramId(0);
    accountProgramVO.setAccountProgramStatus(programStatus);
    accountProgramVO.setAutoRenewFlag(autoRenewFlag);
    accountProgramVO.setProgramName(programName);
    accountProgramVO.setExpirationDate(expirationDate);
    accountProgramVO.setStartDate(startDate);
    accountProgramVO.setExternalOrderNumber(externalOrderNumber);
    accountProgramVO.setUpdatedOn(updateOn);
    return accountProgramVO;    
  }
  
  /**
   * 
   * @param value
   * @return
   */
  private String wrapInCDATA(String tag, String value)
  {
    return "<" + tag + ">" + "<![CDATA[" + value + "]]>" + "</" + tag + ">";
  }
    
  /**
   * Tests the conversion of the AccountManagementTaskVO to XML for the AccountUpdate task
   */
  public void testConvertAccountManagementTask_FeedAccountUpdate()
  {
    AccountManagementTaskVO accountManagementTaskVO = new AccountManagementTaskVO();
    accountManagementTaskVO.setTaskType(AccountConstants.TASK_TYPE_ACCOUNT_INFO_UPDATED);
    accountManagementTaskVO.setIdentity(new IdentityVO());
    accountManagementTaskVO.getIdentity().setEmailAddress("user@test.com");
    accountManagementTaskVO.setPrograms(new ArrayList<AccountProgramVO>());
    accountManagementTaskVO.getPrograms().add(getAccountProgramVO("FREESHIP", "I", "Y", getDate("2012-02-02 00:00:00"), getDate("2011-02-01 00:00:00"), "ORDER001", getDate("2011-02-15 12:42:34")));

    String xml = accountXMLBO.convertAccountManagementTaskToFeedAccountUpdateXML(accountManagementTaskVO);
    System.out.println(xml);
    assertNotNull(xml);
    assertTrue(xml.contains(wrapInCDATA("emailAddress", "user@test.com")));
    assertTrue(xml.contains(wrapInCDATA("type", "FREESHIP")));
    assertTrue(xml.contains(wrapInCDATA("status", "I")));
    assertTrue(xml.contains(wrapInCDATA("autoRenewFlag", "Y")));
    assertTrue(xml.contains(wrapInCDATA("expirationDate", "2012-02-02 00:00:00")));
    assertTrue(xml.contains(wrapInCDATA("startDate", "2011-02-01 00:00:00")));
    assertTrue(xml.contains(wrapInCDATA("lastUpdatedDate", "2011-02-15 12:42:34")));
    validateXML(xml, getTextFileFromResource("/schema_feedAccountUpdateToWeb_programInfo.xml"));
  }
  
  
 
  /**
   * Tests the conversion of the AccountManagementTaskVO to XML for the Feed Account Update task
   */
  public void testConvertAccountManagementTask_JmsFeedAccountUpdate()
  {
    AccountManagementTaskVO accountManagementTaskVO = new AccountManagementTaskVO();
    accountManagementTaskVO.setTaskType(AccountConstants.TASK_TYPE_FEED_PROGRAM_INFO_WEB);
    accountManagementTaskVO.setPrograms(new ArrayList<AccountProgramVO>());
    AccountProgramVO programId = new AccountProgramVO();
    programId.setAccountProgramId(101);
    accountManagementTaskVO.getPrograms().add(programId);
    
    String xml = accountXMLBO.convertAccountManagementTaskToXML(accountManagementTaskVO);
    assertNotNull(xml);
    assertFalse(xml.contains(wrapInCDATA("name", "FREESHIP")));
    assertFalse(xml.contains(wrapInCDATA("status", "I"))); 
    assertTrue(xml.contains(wrapInCDATA("accountProgramId", "101"))); 
  }


  /**
   * Converts a String value into an InputSource
   * @param value
   * @return
   */
  private StreamSource stringToInputSource(String value)
  {
    StringReader reader = new StringReader(value);
    return new StreamSource(reader);
  }
  /**
   * Validates the passed in XML againt the passed in Schema
   * If this fails, either update the schema, or ensure that the
   * XML is not generating unexpected tags (May need to omit fields)
   * @param xml
   * @param schema
   */
  private void validateXML(String xmlToValidate, String schemaText)
  {
    try 
    {
      SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
      Schema schema = factory.newSchema(stringToInputSource(schemaText));
      Validator validator = schema.newValidator();

      validator.validate(stringToInputSource(xmlToValidate));
    } catch (Throwable t)
    {
      throw new RuntimeException("Failed to validate XML: " + t.getMessage(), t);
    }
  
  
  }
}
