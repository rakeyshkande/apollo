package com.ftd.osp.account.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

public class TestUtils
{
  
   /**
   * Used for generating Date Strings.
   * Format: YYYY-MM-DD HH:MM:SS
   * @param date
   * @return
   */
  public static Date getDate(String date)
  {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    try
    {
      return df.parse(date);
    }
    catch (ParseException e)
    {
      throw new RuntimeException(e);
    }
  }  
}
