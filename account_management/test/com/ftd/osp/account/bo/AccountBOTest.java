package com.ftd.osp.account.bo;

import com.ftd.osp.account.dao.AccountDAO;
import com.ftd.osp.account.test.AbstractDBTest;
import com.ftd.osp.account.vo.AccountEmailVO;
import com.ftd.osp.account.vo.AccountMasterVO;
import com.ftd.osp.account.vo.AccountProgramVO;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.TestSuite;

import junit.textui.TestRunner;

import org.apache.commons.lang.StringUtils;


/**
 * Tests the Account BO using the AccountDAO.
 * Note: Update the setUp to utilize the AccountDAOMockImpl to test new/unimplemented database methods.
 */
public class AccountBOTest
  extends AbstractDBTest
{

  AccountDAO accountDAO;
  AccountBO accountBO;
  AccountQueueDispatcherBO accountQueueDispatcherBO;
  List<String> accountQueueXMLs = new ArrayList<String>();

  static final String userId = "junit";

  public AccountBOTest()
  {
  }

  public static void main(String[] args)
  {
    TestRunner runner = new TestRunner();
    runner.run(new TestSuite(AccountBOTest.class));
  }


  /**
   * Sets up the BO and DO for the test.
   */
  public void setUp() throws Exception
  {
    super.setUp();
    accountDAO = new AccountDAO();
    accountQueueDispatcherBO = new AccountQueueDispatcherBO()
    {
      public void addRequestToAccountTaskQueue(String xmlRequest)
      {
        accountQueueXMLs.add(xmlRequest);
      }
    };
    
    accountBO = new AccountBO(accountDAO, accountQueueDispatcherBO);
    
    accountQueueXMLs.clear();
  }

  /////////////////////////////// Helper Methods       //////////////////////////////
  private void assertEmailStatus(List<AccountEmailVO> emailList, String emailAddress, boolean status)
  {
    for (AccountEmailVO accountEmail: emailList)
    {
      if (emailAddress.equals(accountEmail.getEmailAddress()))
      {
        assertEquals(status, accountEmail.isActiveFlag());
        return;
      }
    }    
    
    fail("Email Address not Found - " + emailAddress);
  }
  
  private void assertProgramStatus(List<AccountProgramVO> programList, String programName, String programStatus, String autoRenewFlag, Date expirationDate, Date startDate, String externalOrderNumber)
  {
    for(AccountProgramVO accountProgramVO: programList)
    {
      if(accountProgramVO.getProgramName().equals(programName))
      {
        assertEquals(programStatus, accountProgramVO.getAccountProgramStatus());
        assertEquals(autoRenewFlag, accountProgramVO.getAutoRenewFlag());
        assertEquals(expirationDate, accountProgramVO.getExpirationDate());
        assertEquals(startDate, accountProgramVO.getStartDate());
        assertEquals(externalOrderNumber, accountProgramVO.getExternalOrderNumber());

        return;
      }
    }
    
    fail("Missing Program - " + programName);
  }
  
     /**
   * Used for generating Date Strings.
   * Format: YYYY-MM-DD HH:MM:SS
   * @param date
   * @return
   */
  protected Date getDate(String date)
  {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try
    {
      return df.parse(date);
    }
    catch (ParseException e)
    {
      throw new RuntimeException(e);
    }
  }

  /////////////////////////////// Test Update Accounts //////////////////////////////
  
  /**
   * Updates An account where the original account and New account does not exist
   *
   * Pre Conditions
   * account1@test.com: Does not Exist
   * account2@test.com: Does not Exist
   *
   * Post Conditions
   * account1@test.com: Does not Exist
   * account2@test.com: Exists, Active
   *
   */
  public void testUpdateAccountNoOriginalNoNewAccount()
  {
    // Confirm both accounts do not exist
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com"));

    // Update the Account
    accountBO.updateAccount(getConnection(), "FTD", "account1@test.com", "account2@test.com", userId);

    // Confirm new account exists
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEquals("account2@test.com", accountMasterVO.getAccountEmails().get(0).getEmailAddress());
  }

  /**
   * Updates an account where the original account does not exist, the new account exists.
   *
   * Pre Conditions
   * account1@test.com: Does not Exist
   * account2@test.com: Exists, Active
   *
   * Post Conditions
   * account1@test.com: Does not Exist
   * account2@test.com: Exists, Active
   *
   */
  public void testUpdateAccountNoOriginalYesNewAccount()
  {
    accountBO.createActiveAccount(getConnection(), "account2@test.com", "FTD", userId);

    // Confirm old accounts does not exist, new account exists
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com"));

    // Update the Account
    accountBO.updateAccount(getConnection(), "FTD", "account1@test.com", "account2@test.com", userId);

    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com"));
  }


  /**
   * Updates an account where the original account and the new Account Exists
   *
   * Pre Conditions
   * account1@test.com: Exists, Active
   * account2@test.com: Exists, Active
   *
   * Post Conditions
   * account1@test.com: Inactive
   * account2@test.com: Exists, Active
   *
   */
  public void testUpdateAccountYesOriginalYesNewAccount()
  {
    long accountOne = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    long accountTwo = accountBO.createActiveAccount(getConnection(), "account2@test.com", "FTD", userId);

    // Confirm oboth accounts exist and are valid
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com"));

    // Update the Account
    accountBO.updateAccount(getConnection(), "FTD", "account1@test.com", "account2@test.com", userId);

    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com"));
  }


  /**
   * Updates an account where the original account and the new does not exist.
   * The Original Account will have 2 Email Records assigned to it, 1 active
   * and 1 inactive
   *
   * Pre Conditions
   * account1@test.com: Exists, Active
   * account2@test.com: Does not Exist
   *
   * Post Conditions
   * account1@test.com: Inactive
   * account2@test.com: Exists, Active, has multiple Emails on the Account
   *
   */
  public void testUpdateAccountYesOriginalNoNewAccount()
  {
    long accountOne = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);

    // Confirm oboth accounts exist and are valid
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com").getAccountEmails().size());
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com"));

    // Update the Account
    accountBO.updateAccount(getConnection(), "FTD", "account1@test.com", "account2@test.com", userId);

    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "account1@test.com"));

    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "account2@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(2, accountMasterVO.getAccountEmails().size());

    assertEmailStatus(accountMasterVO.getAccountEmails(), "account1@test.com", false);
    assertEmailStatus(accountMasterVO.getAccountEmails(), "account2@test.com", true);
  }


  /////////////////////////////// Test Merge Accounts //////////////////////////////
 
   /**
   * Merge An account where the parent account and child account does not exist.
   * Expect the parent to be created
   *
   * Pre Conditions
   * child@test.com: Does not Exist
   * parent@test.com: Does not Exist
   *
   * Post Conditions
   * child@test.com: Does not Exist
   * parent@test.com: Exists, Active
   *
   */
  public void testMergeAccountNoParentNoChild()
  {
    // Confirm both accounts do not exist
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));

    // Update the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "child@test.com", userId);

    // Child inactivated
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    
    // Confirm new parent account exists
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEquals("parent@test.com", accountMasterVO.getAccountEmails().get(0).getEmailAddress());
  }
 
   /**
   * Merge An account where the parent account exists and child account does not exist.
   * Expect the parent to be created, it will have no programs
   *
   * Pre Conditions
   * child@test.com: Does not Exist
   * parent@test.com: Exist, Active
   *
   * Post Conditions
   * child@test.com: Does not Exist
   * parent@test.com: Exists, Active
   *
   */
  public void testMergeAccountYesParentNoChild()
  {
    accountBO.createActiveAccount(getConnection(), "parent@test.com", "FTD", userId);
  
    // Confirm both accounts do not exist
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));

    // Update the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "child@test.com", userId);

    // Child inactivated
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    
    // Confirm new parent account exists
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEquals("parent@test.com", accountMasterVO.getAccountEmails().get(0).getEmailAddress());
    assertEquals(0, accountMasterVO.getAccountPrograms().size());
  }  

   /**
   * Merge An account where the parent account does not exists and child account exist.
   * Expect the parent to be created, child to be inactivated. 
   * Parent will have child Emails and Child Programs
   *
   * Pre Conditions
   * child@test.com: Exist, Active
   * parent@test.com: Does not Exist
   *
   * Post Conditions
   * child@test.com: Inactive
   * parent@test.com: Exists, Active
   *
   */
  public void testMergeAccountNoParentYesChild()
  {
    Date childStartDate = null;
    Date childExpirationDate = getDate("2012-06-02 00:00:00");
    long childAccount = accountBO.createActiveAccount(getConnection(), "child@test.com", "FTD", userId);
    String externalOrderNumber = "TESTORD1";
    accountBO.addProgramToAccount(getConnection(), childAccount, "FREESHIP", "A", 
                                  "Y", childExpirationDate, childStartDate, externalOrderNumber, userId);
    
    // Confirm both accounts
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com").getAccountPrograms().size());
  
    // Update the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "child@test.com", userId);

    // Child inactivated
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    
    // Confirm new parent account exists
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(2, accountMasterVO.getAccountEmails().size());
    
    assertEmailStatus(accountMasterVO.getAccountEmails(), "child@test.com", false);
    assertEmailStatus(accountMasterVO.getAccountEmails(), "parent@test.com", true);    
    
    assertEquals(1, accountMasterVO.getAccountPrograms().size());
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "N", childExpirationDate, childStartDate, externalOrderNumber);
  }


   /**
   * Merge An account where the parent account exists and child account exist.
   * Expect the parent to be merged, child to be inactivated. 
   * Parent will have child Emails and Child Programs. Note Child program auto renew
   * will be false.
   *
   * Pre Conditions
   * child@test.com: Exist, Active
   * parent@test.com: Exists, Active
   *
   * Post Conditions
   * child@test.com: Inactive
   * parent@test.com: Exists, Active
   *
   */
  public void testMergeAccountYesParentYesChild()
  {
    Date childStartDate = getDate("2011-06-01 00:00:00");;
    Date childExpirationDate = getDate("2012-06-02 00:00:00");
    long childAccount = accountBO.createActiveAccount(getConnection(), "child@test.com", "FTD", userId);
    String externalOrderNumber = "TESTORD1";
    accountBO.addProgramToAccount(getConnection(), childAccount, "FREESHIP", "A", 
                                  "Y", childExpirationDate, childStartDate, externalOrderNumber, userId);

    long parentAccount = accountBO.createActiveAccount(getConnection(), "parent@test.com", "FTD", userId);                                  
    
    // Confirm accounts
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com").getAccountPrograms().size());
  
    // Update the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "child@test.com", userId);
  
    // Child inactivated
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    
    // Confirm new parent account updated
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(2, accountMasterVO.getAccountEmails().size());
    
    assertEmailStatus(accountMasterVO.getAccountEmails(), "child@test.com", false);
    assertEmailStatus(accountMasterVO.getAccountEmails(), "parent@test.com", true);    
    
    assertEquals(1, accountMasterVO.getAccountPrograms().size());
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "N", childExpirationDate, childStartDate, externalOrderNumber);
  }



   /**
   * Merge An account where the parent account exists and child account exist.
   * Expect the parent to be merged, child to be inactivated. 
   * Parent will have child Emails and Child Programs
   *
   * Pre Conditions
   * child@test.com: Exist, Active, Free Ship Not Auto Renew, Later Expiration Date
   * parent@test.com: Exists, Active, Free Ship Auto Renew, Earlier Expiration Date
   *
   * Post Conditions
   * child@test.com: Inactive
   * parent@test.com: Exists, Active, Free Ship Auto Renew, Later Expiration Date
   *
   * Programs
   * child@test.com
   * parent@test.com
   */
  public void testMergeAccountYesParentYesChildParentFreeShipReplaced()
  { 
    Date childStartDate = getDate("2011-06-01 00:00:00");;
    Date childExpirationDate = getDate("2012-06-02 00:00:00");
    long childAccount = accountBO.createActiveAccount(getConnection(), "child@test.com", "FTD", userId);
    String childOrderNumber = "TESTORD2";
    String parentOrderNumber = "TESTORD1";
    
    accountBO.addProgramToAccount(getConnection(), childAccount, "FREESHIP", "A", 
                                  "N", childExpirationDate, childStartDate, childOrderNumber, userId);

    Date parentStartDate = getDate("2011-04-01 00:00:00");;
    Date parentExpirationDate = getDate("2012-04-02 00:00:00");
    long parentAccount = accountBO.createActiveAccount(getConnection(), "parent@test.com", "FTD", userId);                                  
    accountBO.addProgramToAccount(getConnection(), parentAccount, "FREESHIP", "A", 
                                  "Y", parentStartDate, parentExpirationDate, parentOrderNumber, userId);    

    // Confirm accounts
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com").getAccountPrograms().size());
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com").getAccountPrograms().size());
  
    // Update the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "child@test.com", userId);

    // Child inactivated
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "child@test.com"));
    
    // Confirm new parent account updated
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(2, accountMasterVO.getAccountEmails().size());
    
    assertEmailStatus(accountMasterVO.getAccountEmails(), "child@test.com", false);
    assertEmailStatus(accountMasterVO.getAccountEmails(), "parent@test.com", true);    
    
    assertEquals(1, accountMasterVO.getAccountPrograms().size());
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", childExpirationDate, childStartDate, childOrderNumber);
  }
  
  
   /**
   * Tests Updating Program Status by Order and Program where order number is not found.
   *
   * Pre Conditions
   * Order Exists
   *
   * Post Conditions
   * No messages to the Queue. 
   *
   */  
  public void testUpdateProgramStatusByOrderAndProgramNoOrderExists()
  {
    final Date startDate = getDate("2011-04-01 00:00:00");;
    final Date expirationDate = getDate("2012-04-02 00:00:00");
    final String externalOrderNumber = "TESTORD1";    
    long account = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    accountBO.addProgramToAccount(getConnection(), account, "FREESHIP", "A", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);    
    
    
    accountBO.updateProgramStatusByOrder(getConnection(), "JunkOrderNumber", "FREESHIP", "I", userId);
    assertEquals(0, accountQueueXMLs.size());
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account1@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);
  }
  
  
   /**
   * Tests Updating Program Status by Order And Program where order number and Program is found.
   *
   * Pre Conditions
   * Order Exists
   *
   * Post Conditions
   * 1 Program Update send in Queue
   */  
  public void testUpdateProgramStatusByOrderAndProgramOrderExists()
  {
    final Date startDate = getDate("2011-04-01 00:00:00");;
    final Date expirationDate = getDate("2012-04-02 00:00:00");
    final String externalOrderNumber = "TESTORD1";    
    long account = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    accountBO.addProgramToAccount(getConnection(), account, "FREESHIP", "A", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);    
    
    
    accountBO.updateProgramStatusByOrder(getConnection(), externalOrderNumber, "FREESHIP", "I", userId);
    assertEquals(1, accountQueueXMLs.size());
    assertEquals(1, StringUtils.countMatches(accountQueueXMLs.get(0), "<program>"));    
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account1@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "I", "Y", expirationDate, startDate, externalOrderNumber);
  }
  
   /**
   * Tests Updating Program Status by Order and Program where order number is found but there is no state change.
   * Pre Conditions
   * Order Exists
   *
   * Post Conditions
   * No Updates sent
   *
   */  
  public void testUpdateProgramStatusByOrderAndProgramOrderExistsNoStateChange()
  {
    final Date startDate = getDate("2011-04-01 00:00:00");;
    final Date expirationDate = getDate("2012-04-02 00:00:00");
    final String externalOrderNumber = "TESTORD1";    
    long account = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    accountBO.addProgramToAccount(getConnection(), account, "FREESHIP", "A", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);    
    
    
    accountBO.updateProgramStatusByOrder(getConnection(), externalOrderNumber, "FREESHIP", "A", userId);
    assertEquals(0, accountQueueXMLs.size());
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account1@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);
  }  
  
   /**
   * Tests Updating Program Status by Order and Program where order number is found but Program is not the same.
   *
   * Pre Conditions
   * No Order
   *
   * Post Conditions
   * No messages to the Queue. 
   *
   */  
  public void testUpdateProgramStatusByOrderAndProgramOrderExistsNotProgram()
  {
    final Date startDate = getDate("2011-04-01 00:00:00");;
    final Date expirationDate = getDate("2012-04-02 00:00:00");
    final String externalOrderNumber = "TESTORD1";    
    long account = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    accountBO.addProgramToAccount(getConnection(), account, "FREESHIP", "A", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);    
    
    
    accountBO.updateProgramStatusByOrder(getConnection(), externalOrderNumber, "JUNITNOTAPROG", "I", userId);
    assertEquals(0, accountQueueXMLs.size());
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account1@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);
  }
  
   /**
   * Tests Updating Program Status by Order where order number is found on multiple accounts 
   * (Not really a valid scenario, but simulate the multiple program scenario).
   *
   * Pre Conditions
   * No Order
   *
   * Post Conditions
   * 2 Program Updates sent in Queue
   *
   */  
  public void testUpdateProgramStatusByOrderWhereOrderExistsMultipleAccounts()
  {
    final Date startDate = getDate("2011-04-01 00:00:00");;
    final Date expirationDate = getDate("2012-04-02 00:00:00");
    final String externalOrderNumber = "TESTORD1";    
    long account1 = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    long account2 = accountBO.createActiveAccount(getConnection(), "account2@test.com", "FTD", userId);
    
    accountBO.addProgramToAccount(getConnection(), account1, "FREESHIP", "I", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);    
                                  
    accountBO.addProgramToAccount(getConnection(), account2, "FREESHIP", "I", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);                                      
    
    
    accountBO.updateProgramStatusByOrder(getConnection(), externalOrderNumber, "A", userId);
    assertEquals(1, accountQueueXMLs.size());
    assertEquals(2, StringUtils.countMatches(accountQueueXMLs.get(0), "<program>"));      

    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account1@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);
    accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account2@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);    
  }
  
   /**
   * Tests Updating Program Status by Order where order number is found on multiple accounts 
   * but only 1 status is changed
   * (Not really a valid scenario, but simulate the multiple program scenario).
   *
   * Pre Conditions
   * No Order
   *
   * Post Conditions
   * 1 Program Update send in Queue
   *
   */  
  public void testUpdateProgramStatusByOrderWhereOrderExistsMultipleAccountsOneStatusChange()
  {
    final Date startDate = getDate("2011-04-01 00:00:00");;
    final Date expirationDate = getDate("2012-04-02 00:00:00");
    final String externalOrderNumber = "TESTORD1";    
    long account1 = accountBO.createActiveAccount(getConnection(), "account1@test.com", "FTD", userId);
    long account2 = accountBO.createActiveAccount(getConnection(), "account2@test.com", "FTD", userId);
    
    accountBO.addProgramToAccount(getConnection(), account1, "FREESHIP", "I", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);    
                                  
    accountBO.addProgramToAccount(getConnection(), account2, "FREESHIP", "A", 
                                  "Y", expirationDate, startDate, externalOrderNumber, userId);                                      
    
    
    accountBO.updateProgramStatusByOrder(getConnection(), externalOrderNumber, "A", userId);
    assertEquals(1, accountQueueXMLs.size());
    assertEquals(1, StringUtils.countMatches(accountQueueXMLs.get(0), "<program>"));      

    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account1@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);
    accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD",  "account2@test.com");
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", expirationDate, startDate, externalOrderNumber);    
  }  
  
  
  
   /**
   * Merge the same account multiple times.
   * Expect the parent to be exist, no change to the parent. No duplicate email on parent.
   *
   * Pre Conditions
   * parent@test.com: Exists, Active, Free Ship Auto Renew 
   *
   * Post Conditions
   * parent@test.com: Exists, Active, Free Ship Auto Renew 
   *
   * Programs
   * parent@test.com
   */
  public void testMergeAccountYesParentMultipleTimesSameAccount()
  { 
    String parentOrderNumber = "TESTORD1";
    Date parentStartDate = getDate("2011-04-01 00:00:00");;
    Date parentExpirationDate = getDate("2012-04-02 00:00:00");
    long parentAccount = accountBO.createActiveAccount(getConnection(), "parent@test.com", "FTD", userId);                                  
    accountBO.addProgramToAccount(getConnection(), parentAccount, "FREESHIP", "A", 
                                  "Y", parentStartDate, parentExpirationDate, parentOrderNumber, userId);    

    // Confirm accounts
    assertNotNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com").getAccountPrograms().size());
    assertEquals(1, accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com").getAccountEmails().size());

    // Merge the Account multiple times
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);
   
    // Confirm parent account unchanged
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEmailStatus(accountMasterVO.getAccountEmails(), "parent@test.com", true);    
    
    assertEquals(1, accountMasterVO.getAccountPrograms().size());
    assertProgramStatus(accountMasterVO.getAccountPrograms(), "FREESHIP", "A", "Y", parentStartDate, parentExpirationDate, parentOrderNumber);
  }
  
  
   /**
   * Merge the same account multiple times.
   * Expect the parent to be exist, no change to the parent. No duplicate email on parent.
   *
   * Pre Conditions
   * parent@test.com: Does not exist
   *
   * Post Conditions
   * parent@test.com: Exists, Active
   *
   */
  public void testMergeAccountNoParentMultipleTimesSameAccount()
  { 
    // Confirm accounts
    assertNull(accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com"));

    // Merge the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);
    
    AccountMasterVO accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEmailStatus(accountMasterVO.getAccountEmails(), "parent@test.com", true);    
    
    // Merge the Account
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);    
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);    
    accountBO.mergeAccount(getConnection(), "FTD", "parent@test.com", "parent@test.com", userId);    
    accountMasterVO = accountDAO.getActiveAccount(getConnection(), "FTD", "parent@test.com");
    assertNotNull(accountMasterVO);
    assertEquals(1, accountMasterVO.getAccountEmails().size());
    assertEmailStatus(accountMasterVO.getAccountEmails(), "parent@test.com", true);    

  }  
    
}
