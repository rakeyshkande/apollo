package com.ftd.osp.account.bo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import junit.framework.TestCase;

import com.ftd.customer.client.CustomerRestServiceClient;
import com.ftd.customer.client.FreeShippingRestServiceClient;
import com.ftd.customer.core.domain.CustomerCCData;
import com.ftd.customer.core.domain.CustomerFSAutoRenewCCResult;
import com.ftd.customer.core.domain.CustomerFSHistory;
import com.ftd.customer.core.domain.CustomerFSMDetails;
import com.ftd.customer.core.domain.CustomerFullProfile;
import com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest;
import com.ftd.customer.services.rest.core.request.FSMAutoRenewCCResultRequest;
import com.ftd.customer.services.rest.core.response.GenericResponse;
import com.ftd.order.client.OrderRestServiceClient;
import com.ftd.order.core.domain.OrderInfo;
import com.ftd.order.rest.core.request.OrderServiceRequest;
import com.ftd.osp.account.bo.FSAutoRenewBO.ccStatus;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.account.vo.CommonCreditCardVO;
import com.ftd.osp.account.vo.CreditCardVO;
import com.ftd.util.validation.ErrorMessage;
import com.ftd.util.validation.ErrorMessages;

public class AccountCAMSTest extends TestCase{
		
	public static final String camsURL = "http://cams-qa01.ftdi.com/customer/";
	public static final String camsOrderURL = "http://cams-qa01.ftdi.com/order/";
	
	/*public static final String camsURL = "http://10.222.72.161/customer/";
	public static final String camsOrderURL = "http://10.222.72.161/order/";*/
	
	public void testUpdateProgramStatusInCAMS()
	{
		
		AccountBO accountBO = new AccountBO();
		try{
			FreeShippingRestServiceClient client = new FreeShippingRestServiceClient(camsURL, "apollo", "a90l1o");
			
			CustomerFSMDetailsRequest req = new CustomerFSMDetailsRequest();
			
			CustomerFSMDetails fsmDetails = new CustomerFSMDetails();
			req.setEmail("janaki122132@sodium.ftdi.com");
			//sending status as "0" for inactive status
			fsmDetails.setFsMember("1");
			fsmDetails.setFsmOrderNumber("abc");
			
			req.setCustomerFSMDetails(fsmDetails);
		
			
			GenericResponse<Integer> response = client.updateCustomerFSMStatus(req);
			ErrorMessages errors = response.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			for (ErrorMessage errMsg : errMsgs) {
				if("101".equals(errMsg.getCode())){
					System.out.println("Send System Message : "+errMsg.getMessage());
				}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void testGetFSMDetails()
	{
		try{
			/*BufferedReader br = null;
			 
			try {
	 
				String sCurrentLine;
	 
				br = new BufferedReader(new FileReader(new File("c:\\cams-password.txt")));
	 
				while ((sCurrentLine = br.readLine()) != null) {
					System.out.println(sCurrentLine);
				}
	 
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}	*/
			FreeShippingRestServiceClient  fsRestServiceClient = new FreeShippingRestServiceClient(camsURL, "apollo", "a90l1o");
			GenericResponse<CustomerFSMDetails> response = fsRestServiceClient.getFSM("fsmarcc@ftd.com");
			CustomerFSMDetails details  = response.getData();
			System.out.println("Start Date : "+details.getFsStartDate());
			System.out.println("End Date : "+details.getFsEndDate());
			System.out.println("Join Date : "+details.getFsJoinDate());
			
			Calendar sdCal = Calendar.getInstance();
			Calendar edCal = Calendar.getInstance();
			Calendar jdCal = Calendar.getInstance();
			
			sdCal.setTime(details.getFsStartDate());
			edCal.setTime(details.getFsEndDate());
			jdCal.setTime(details.getFsJoinDate());
			
			sdCal.add(sdCal.HOUR, 1);
			edCal.add(edCal.HOUR, 1);
			jdCal.add(jdCal.HOUR, 1);			
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			System.out.println("Start Date : "+dateFormat.format(sdCal.getTime()));
			System.out.println("End Date : "+dateFormat.format(edCal.getTime()));
			System.out.println("Join Date : "+dateFormat.format(jdCal.getTime()));
			 
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testGetFSHistory() throws Exception
	{
		try{
			FreeShippingRestServiceClient  fsRestServiceClient = new FreeShippingRestServiceClient(camsURL, "apollo", "a90l1o");
			//FreeShippingRestServiceClient  fsRestServiceClient = new FreeShippingRestServiceClient(camsURL);
			 
			  List<String> emailList = new ArrayList<String>();
			  List<String> eventTypeList = new ArrayList<String>();
			  emailList.add("fsmseven@ftd.com");
			  eventTypeList.add("fsm_order");
			  eventTypeList.add("fs_renewed");
			  GenericResponse<List<CustomerFSHistory>> fsmHistoryResponse = fsRestServiceClient.getCustomerFSHistory(emailList, eventTypeList);
			  if(fsmHistoryResponse.getErrors().getErrors().size() > 0)
			  {
				  System.out.println("Errors Found");
				  throw new RuntimeException();
			  }
			  
			  List<CustomerFSHistory> custFsmHistList = fsmHistoryResponse.getData();
			  
			  System.out.println(custFsmHistList.size());
		}catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			fail();
		}
	}
	
	public void testIsFSBenefitsApplicable()
	{
		try{
			Calendar cal = Calendar.getInstance();
			FreeShippingRestServiceClient  fsRestServiceClient = new FreeShippingRestServiceClient(camsURL, "apollo", "a90l1o");
			//cal.add(Calendar.MONTH, 24);
			System.out.println(cal.getTime());
			//System.out.println(System.currentTimeMillis()/1000);
			System.out.println(cal.getTime().getTime()/1000);
	
			GenericResponse<Boolean> res = fsRestServiceClient.isFSBenefitsApplicable("swfs515@ftd.com", (cal.getTime().getTime())/1000);
			boolean flag = res.getData();
			assertTrue(flag); 
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void testCancelOrder()
	{
		try{
			OrderRestServiceClient client = new OrderRestServiceClient(camsOrderURL, "apollo", "a90l1o1");
			OrderServiceRequest req = new OrderServiceRequest();
			//req.setOrderNumber("FMZ10028");
			req.setOrderNumber("abc");
			com.ftd.order.rest.core.response.GenericResponse<Integer> res =  client.cancelOrder(req);
			ErrorMessages errors = res.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			for (ErrorMessage errMsg : errMsgs) {
				if("507".equals(errMsg.getCode())){
					fail(errMsg.getMessage());
				}else if ("509".equals(errMsg.getCode())){
					assertTrue("509".equals(errMsg.getCode()));
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void testFullyRefundOrder()
	{
		try
		{
			
			OrderRestServiceClient client = new OrderRestServiceClient(camsOrderURL, "apollo", "a90l1o1");
			OrderServiceRequest req = new OrderServiceRequest();
			//req.setOrderNumber("FMZ10030");
			req.setOrderNumber("abc");
			com.ftd.order.rest.core.response.GenericResponse<Integer> res = client.refundOrder(req);
			ErrorMessages errors = res.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			for (ErrorMessage errMsg : errMsgs) {
				if("507".equals(errMsg.getCode())){
					fail(errMsg.getMessage());
				}else if ("508".equals(errMsg.getCode())){
					assertTrue("508".equals(errMsg.getCode()));
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void testUpdateOrder()
	{
		try
		{
			
			OrderRestServiceClient client = new OrderRestServiceClient(camsOrderURL, "apollo", "a90l1o1");
			
			OrderServiceRequest req = new OrderServiceRequest();
			
			//req.setOrderNumber("FMZ10030");
			OrderInfo orderInfo = new OrderInfo();
			orderInfo.setChannel("abc");
			req.setOrderInfo(orderInfo);
			com.ftd.order.rest.core.response.GenericResponse<Integer> res = client.updateOrder(req);
			ErrorMessages errors = res.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			for (ErrorMessage errMsg : errMsgs) {
				if("507".equals(errMsg.getCode())){
					fail(errMsg.getMessage());
				}else if ("508".equals(errMsg.getCode())){
					assertTrue("508".equals(errMsg.getCode()));
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void testUpdateCustomerFSMStatus()
	{
		try{
			boolean status = false;
			FreeShippingRestServiceClient fsServiceClient = new FreeShippingRestServiceClient(camsURL, "apollo", "a90l1o1");
			String emailId = "spatsa@sodium.ftdi.com";
			int serviceDuration = 12;
			CustomerFSMDetailsRequest fsmDetailsRequest = new CustomerFSMDetailsRequest();
			fsmDetailsRequest.setAutoRenewPeriod(serviceDuration);
			fsmDetailsRequest.setEmail(emailId);
			CustomerFSMDetails customerFSMDetails = new CustomerFSMDetails();
			customerFSMDetails.setFsmOrderNumber("FNB3192606");
			fsmDetailsRequest.setCustomerFSMDetails(customerFSMDetails);
			GenericResponse<Integer> response = fsServiceClient.updateCustomerFSMStatus(fsmDetailsRequest);
			ErrorMessages errors = response.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			System.out.println(errMsgs.size());
		}catch(Exception e)
		{
			System.out.println("e.getMessage() : "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void testgetCustomerFSMandCCData()
	{
		try{
			CustomerRestServiceClient client = new CustomerRestServiceClient(camsURL, "apollo", "a90l1o"); 
			String emailId = "fsmdecc@ftd.com";
			
			GenericResponse<CustomerFullProfile> fullProfileResp = client.getCustomerFSMAndCCData(emailId);
			  if(fullProfileResp == null || fullProfileResp.getErrors()!=null && fullProfileResp.getErrors().hasErrors()){
				  
				  List<ErrorMessage> errors = fullProfileResp.getErrors().getErrors();
				  for (ErrorMessage errorMessage : errors) {
					System.out.println(errorMessage.getMessage());
				  }
			      
			  }
		}catch(Exception e)
		{
			System.out.println("e.getMessage() : "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void testCCStatus()
	{
		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		long epochDateTime = (cal.getTime().getTime())/1000;
		try{
			FreeShippingRestServiceClient client = new FreeShippingRestServiceClient("https://cams-qa01.ftdi.com/customer/", "apollo", "a90l1o"); 
			String emailId = "spatsa@sodium.ftdi.com";
			FSMAutoRenewCCResultRequest req = new FSMAutoRenewCCResultRequest();
			CustomerFSAutoRenewCCResult ccResult = new CustomerFSAutoRenewCCResult();
			ccResult.setCcType("Auto-Renew");
			ccResult.setEmail(emailId);
			ccResult.setResult("Missing");
			ccResult.setTransactionTime(epochDateTime);
			//ccResult.setDescription("Missing Credit Card Number");
			req.setCcResult(ccResult);
			
			GenericResponse<Integer> fullProfileResp = client.saveFSMAutoRenewCCResult(req);
			  if(fullProfileResp == null || fullProfileResp.getErrors()!=null && fullProfileResp.getErrors().hasErrors()){
				  
				  List<ErrorMessage> errors = fullProfileResp.getErrors().getErrors();
				  for (ErrorMessage errorMessage : errors) {
					System.out.println(errorMessage.getMessage());
				  }
			      
			  }
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	

}
