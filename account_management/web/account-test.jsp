<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<html>
<head>
<title>Account Management Test</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/ftd.css">

<script type="text/javascript" language="JavaScript">

function init() {
    document.getElementById('updateTask').style.display = 'none';
    document.getElementById('deleteTask').style.display = 'none';
    document.getElementById('mergeTask').style.display = 'none';
    document.getElementById('updateAccountTask').style.display = 'none';
    
    document.theform.taskType.options[0].selected = true;
    document.theform.taskType.focus();
}

function changeTaskType() {
    var divName = document.theform.taskType.value;
    if (divName == 'update') {
        document.getElementById('deleteTask').style.display = 'none';
        document.getElementById('mergeTask').style.display = 'none';
        document.getElementById('updateTask').style.display = 'block';
        document.getElementById('updateAccountTask').style.display = 'none';
    } else if (divName == 'delete') {
        document.getElementById('deleteTask').style.display = 'block';
        document.getElementById('mergeTask').style.display = 'none';
        document.getElementById('updateTask').style.display = 'none';
        document.getElementById('updateAccountTask').style.display = 'none';
    } else if (divName == 'merge') {
        document.getElementById('deleteTask').style.display = 'none';
        document.getElementById('mergeTask').style.display = 'block';
        document.getElementById('updateTask').style.display = 'none';
        document.getElementById('updateAccountTask').style.display = 'none';
    }else if (divName == 'updateAccount') {
        document.getElementById('deleteTask').style.display = 'none';
        document.getElementById('mergeTask').style.display = 'none';
        document.getElementById('updateTask').style.display = 'none';
        document.getElementById('updateAccountTask').style.display = 'block';
    } else {
        document.getElementById('deleteTask').style.display = 'none';
        document.getElementById('mergeTask').style.display = 'none';
        document.getElementById('updateTask').style.display = 'none';
        document.getElementById('updateAccountTask').style.display = 'none';
    }
}

function doSubmit() {
    var doc = createXmlDocument("accountTask");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = document.theform.taskType.value;
    root.setAttributeNode(attr);
        
    var identity = doc.createElement("identity");
    root.appendChild(identity);

	var param;
	
    var divName = document.theform.taskType.value;
    if (divName == 'update') {
        param = createElementWithCData(doc,'emailAddress', document.theform.oldEmailAddress.value);
        identity.appendChild(param);
        
        account = doc.createElement("account");
        root.appendChild(account);
        
        param = createElementWithCData(doc,'emailAddress', document.theform.newEmailAddress.value);
        account.appendChild(param);
    }
    if (divName == 'delete') {
        param = createElementWithCData(doc,'emailAddress', document.theform.emailAddress.value);
        identity.appendChild(param);
    }
    if (divName == 'merge') {
        param = createElementWithCData(doc,'emailAddress', document.theform.primaryEmailAddress.value);
        identity.appendChild(param);

        account = doc.createElement("account");
        root.appendChild(account);
        param = createElementWithCData(doc,'emailAddress', document.theform.mergeEmailAddress1.value);
        account.appendChild(param);
        
        if (document.theform.mergeEmailAddress2.value != '') {
            account = doc.createElement("account");
            root.appendChild(account);
            param = createElementWithCData(doc,'emailAddress', document.theform.mergeEmailAddress2.value);
            account.appendChild(param);
        }
        if (document.theform.mergeEmailAddress3.value != '') {
            account = doc.createElement("account");
            root.appendChild(account);
            param = createElementWithCData(doc,'emailAddress', document.theform.mergeEmailAddress3.value);
            account.appendChild(param);
        }
        if (document.theform.mergeEmailAddress4.value != '') {
            account = doc.createElement("account");
            root.appendChild(account);
            param = createElementWithCData(doc,'emailAddress', document.theform.mergeEmailAddress4.value);
            account.appendChild(param);
        }
    }
    if (divName == 'updateAccount') {
    	param = createElementWithCData(doc,'emailAddress', document.theform.autoRenewEmailAddress.value);
        identity.appendChild(param);
        
        account = doc.createElement("account");
        root.appendChild(account);
        
        param = createElementWithCData(doc,'autoRenewFlag', document.theform.autoRenewFlag.value);
        account.appendChild(param);        
        
    }

    //alert(doc.xml);

    var url = "accountAdmin.do" + "?accountTask=" + doc.xml;
    performAction(url);
    //alert(url);
}

    function performAction(url) {
        document.forms[0].action = url;
        document.forms[0].submit();
    }

function createXmlDocument(rootTagName, namespaceURL) { 
    if (!rootTagName) rootTagName = ""; 
    if (!namespaceURL) namespaceURL = ""; 
    if (document.implementation && document.implementation.createDocument) { 
        // This is the W3C standard way to do it 
        return document.implementation.createDocument(namespaceURL, rootTagName, null); 
    } 
    else 
    { 
        // This is the IE way to do it 
        // Create an empty document as an ActiveX object 
	// If there is no root element, this is all we have to do 
	var doc = new ActiveXObject("MSXML2.DOMDocument"); 
	// If there is a root tag, initialize the document 
	if (rootTagName) { 
            // Look for a namespace prefix 
            var prefix = ""; 
            var tagname = rootTagName; 
            var p = rootTagName.indexOf(':'); 
            if (p != -1) { 
                prefix = rootTagName.substring(0, p); 
                tagname = rootTagName.substring(p+1); 
            } 
            // If we have a namespace, we must have a namespace prefix 
            // If we don't have a namespace, we discard any prefix 
            if (namespaceURL) { 
                if (!prefix) prefix = "a0"; // What Firefox uses 
            } 
            else 
            {
                prefix = "";
            }
            // Create the root element (with optional namespace) as a 
            // string of text 
            var text = "<" + (prefix?(prefix+":"):"") +  tagname + 
            (namespaceURL ?(" xmlns:" + prefix + '="' + namespaceURL +'"') :"") + "/>"; 
            // And parse that text into the empty document 
            doc.loadXML(text); 
        }
        
        return doc; 
    }
}

function createElementWithText( xmlDocument,elementName,elementText ) {
    var el = xmlDocument.createElement(elementName);
    if( elementText!=null && elementText.length>0) {
        el.appendChild(xmlDocument.createTextNode(elementText+''));
    }
    
    return el;
}

function createElementWithCData( xmlDocument,elementName,elementText ) {
    var el = xmlDocument.createElement(elementName);
    if( elementText!=null && elementText.length>0) {
        el.appendChild(xmlDocument.createCDATASection(elementText+''));
    }
    
    return el;
}

</script>
</head>

<body onLoad="init()">

<form name="theform" method="post" action="">

   <table width="905" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

<table width="100%" border="0">
<tr>
    <td class="TblHeader">Account Management</td>
</tr>
</table>

<table with="800" border="0">
<tr>
    <td width="250" class="LabelRight">Task Type:&nbsp;</td>
    <td>
        <select name="taskType" onChange="changeTaskType()" tabindex="10">
            <option value="">-- select task --</option>
            <option value="update">Update</option>
            <option value="delete">Delete</option>
            <option value="merge">Merge</option>
            <option value="updateAccount">UpdateAccount</option>
        </select>
    </td>
</tr>
</table>

<div id="updateTask" style="display:none">
<table width="800" border="0">
    <tr>
        <td width="250" class="LabelRight">Old email address:&nbsp;</td>
        <td>
            <input type="text" name="oldEmailAddress" size="50" tabindex="100"/>
        </td>
    </tr>
    <tr>
        <td class="LabelRight">New email address:&nbsp;</td>
        <td>
            <input type="text" name="newEmailAddress" size="50" tabindex="110"/>
        </td>
    </tr>
</table>
</div>

<div id="deleteTask" style="display:none">
<table width="800" border="0">
    <tr>
        <td width="250" class="LabelRight">Email address:&nbsp;</td>
        <td>
            <input type="text" name="emailAddress" size="50" tabindex="200"/>
        </td>
    </tr>
</table>
</div>

<div id="mergeTask" style="display:none">
<table width="800" border="0">
    <tr>
        <td width="250" class="LabelRight">Primary email address:&nbsp;</td>
        <td>
            <input type="text" name="primaryEmailAddress" size="50" tabindex="300"/>
        </td>
    </tr>
    <tr>
        <td class="LabelRight">Merge email address 1:&nbsp;</td>
        <td>
            <input type="text" name="mergeEmailAddress1" size="50" tabindex="310"/>
        </td>
    </tr>
    <tr>
        <td class="LabelRight">Merge email address 2:&nbsp;</td>
        <td>
            <input type="text" name="mergeEmailAddress2" size="50" tabindex="320"/>
        </td>
    </tr>
    <tr>
        <td class="LabelRight">Merge email address 3:&nbsp;</td>
        <td>
            <input type="text" name="mergeEmailAddress3" size="50" tabindex="330"/>
        </td>
    </tr>
    <tr>
        <td class="LabelRight">Merge email address 4:&nbsp;</td>
        <td>
            <input type="text" name="mergeEmailAddress4" size="50" tabindex="340"/>
        </td>
    </tr>
</table>
</div>
<div id="updateAccountTask" style="display:none">
<table width="800" border="0">
    <tr>
        <td width="250" class="LabelRight">Email Address:&nbsp;</td>
        <td>
            <input type="text" name="autoRenewEmailAddress" size="50" tabindex="100"/>
        </td>    
        <td class="LabelRight">Auto Renew Flag:&nbsp;</td>
        <td>
            <select name="autoRenewFlag">
            	<option value="Y">Y</option>
            	<option value="N">N</option>
            </select>
        </td>
    </tr>    
</table>
</div>
<table width="800" border="0">
<tr>
    <td width="250">&nbsp;</td>
    <td align="left">
        <button class="BlueButton" onClick="doSubmit()" tabindex="900" accesskey="s">(S)ave</button>&nbsp;&nbsp;
    </td>
</tr>
</table>

</td>
</tr>
</table>

</form>

</body>
</html>