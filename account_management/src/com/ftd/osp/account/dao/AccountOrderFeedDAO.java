package com.ftd.osp.account.dao;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ftd.order.core.domain.AlternatePayment;
import com.ftd.order.core.domain.CreditCard;
import com.ftd.order.core.domain.GiftCard;
import com.ftd.order.core.domain.Order;
import com.ftd.order.core.domain.OrderInfo;
import com.ftd.order.core.domain.OrderItemAddonDetails;
import com.ftd.order.core.domain.OrderItemDetails;
import com.ftd.order.core.domain.OrderItemInfo;
import com.ftd.order.core.domain.PaymentInfo;
import com.ftd.order.core.domain.Tax;
import com.ftd.order.core.domain.TaxInfo;
import com.ftd.order.core.domain.TotalTax;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.account.vo.AccountOrderFeedVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * Provides database Access to the Account Schema.
 */
public class AccountOrderFeedDAO
{
  private static Logger logger = new Logger(AccountOrderFeedDAO.class.getName());
  private static final String DB_CUSTOMER_PHONE_WORK = "Day";
  private static final String DB_CUSTOMER_PHONE_HOME = "Evening";
  private static final String DB_DATE_FORMAT = "MM/dd/yyyy";
  private static final String ORDER_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
  private static final String DB_ORIGINAL_BILL = "Original";
  private static final String DB_FREE_SHIP_TYPE = "SERVICES";
  private static final String DB_FREE_SHIP_SUB_TYPE = "FREESHIP";
  private static final String DB_CC_AUTH_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  private static final String DB_GIFT_CARD_PAYMENT_TYPE = "GD";
  private static final String DB_GIFT_CERT_PAYMENT_TYPE = "GC";
  private static final String CAMS_CONFIRMED_STATUS = "Confirmed";
  private static final String STATE_TYPE = "State";
  private static final String COUNTY_TYPE = "County";
  private static final String CITY_TYPE = "City";
  private static final String ZIP_TYPE = "Zip";
  
  public AccountOrderFeedDAO() {
  }


  /**
   * Determine if an order feed is necessary by checking if the time the order
   * was originally enqueued is after the last successful feed timestamp.
   * If feed is necessary, order_guid is returned, otherwise null is returned.
   * 
   * @param connection
   * @param orderKey
   * @param keyType
   * @param enqueueTime
   * @return order_guid if feed necessary, null otherwise
   */
  public String isOrderFeedNeeded(Connection connection, String orderKey, String keyType, Date enqueueTime) throws Exception
  {
	String out_guid = null;  
	String out_status = null;
    Map paramMap = new HashMap();
    paramMap.put("IN_ORDER_KEY", orderKey);
    paramMap.put("IN_KEY_TYPE", keyType);
    paramMap.put("IN_ENQUEUE_TIME", toSqlTimestamp(enqueueTime));

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("ACCT_MGMT_IS_ORDER_FEED_NEEDED");
    dataRequest.setInputParams(paramMap);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    out_status = (String) outputs.get("OUT_STATUS");

    if ("Y".equalsIgnoreCase(out_status)) {
        out_guid = (String) outputs.get("OUT_ORDER_GUID");
    } else if ("E".equalsIgnoreCase(out_status)) {
        String errMessage = (String) outputs.get("OUT_MESSAGE");
        throw new Exception("Failure in isOrderFeedNeeded: " + errMessage);
    }
    return out_guid;
  }

  
  /**
   * Set status in account order feed table to reflect feed is in-progress
   * 
   * @param connection
   * @param orderGuid
   */
  private String startOrderFeedAttempt(Connection connection, String orderGuid) throws Exception {
	String out_status = null;
	Map paramMap = new HashMap();

	paramMap.put("IN_ORDER_GUID", orderGuid);
	  
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("ACCT_MGMT_UPDATE_ORDER_FEED_ATTEMPT");
    dataRequest.setInputParams(paramMap);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    out_status = (String) outputs.get("OUT_STATUS");
    if ("N".equalsIgnoreCase(out_status)) {
        String errMessage = (String) outputs.get("OUT_MESSAGE");
        //throw new Exception("Failure in startOrderFeedAttempt: " + errMessage);
        logger.error("Failure in startOrderFeedAttempt: " + errMessage);
        return "N";
    }
    return "Y";
  }
  
  
  /**
   * Set status in account order feed table to reflect feed is in-progress
   * and retrieve all data needed for the order feed to CAMS.
   * 
   * @param connection
   * @param orderGuid
   * @return AccountOrderFeedVO object containing order feed info
   */
  public AccountOrderFeedVO lockAndLoadOrderFeed(Connection connection, String orderGuid) throws Exception {
	AccountOrderFeedVO aofvo = null;
    OrderInfo camsOrderInfo = null;
    Map orderMap = null;
    Order camsOrder = null;
    List<OrderItemInfo> camsOrderItemInfoList = null;
    PaymentInfo camsPaymentInfo = null;
    HashMap<String,List<OrderItemAddonDetails>> camsOrderItemAddonHash = null;
    HashMap<String,List<OrderItemDetails>> orderBillsHash = null;
    HashMap<String,Float> orderSavingsHash = null;
    Map<String, TaxInfo> orderTaxes = null;
    
	// Indicate we are starting order feed attempt
	//
	String flag = startOrderFeedAttempt(connection, orderGuid);
	
	if (flag.equalsIgnoreCase("N"))
	{
		return null;
	}
		
	
	// Get data for order feed and insert in CAMS objects
	//
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("ACCT_MGMT_GET_ALL_CART_INFO");
    dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);

    // Get order data
    //
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    orderMap = (Map) dataAccessUtil.execute(dataRequest);
      
    // Map data to CAMS objects
    //
    camsOrder = mapToCamsOrder(orderMap);   
    orderBillsHash = mapJustOrderBills(orderMap);
    orderSavingsHash = mapJustOrderSavings(orderMap);
    orderTaxes = getTaxSplit(orderMap);
    camsOrderItemAddonHash = mapToCamsOrderItemAddonDetails(orderMap);
    camsOrderItemInfoList = mapToCamsOrderItemInfoList(orderMap, orderBillsHash, orderSavingsHash, camsOrderItemAddonHash, orderTaxes);
    if (camsOrderItemInfoList == null) {
        // Just bail out if no cart item data
        return null;
    }
    
    camsPaymentInfo = mapToCamsPaymentInfoList(orderMap);
    aofvo = new AccountOrderFeedVO();
    camsOrderInfo = new OrderInfo();
    camsOrderInfo.setOrder(camsOrder);
    camsOrderInfo.setOrderItemInfo(camsOrderItemInfoList);
    camsOrderInfo.setPaymentInfo(camsPaymentInfo);
    aofvo.setCamsOrderInfo(camsOrderInfo);
    
    if (logger.isDebugEnabled()) {
        logger.debug(aofvo.toString());
    }

    return aofvo;
  }

  /**
   * Internal method to map cart-level cursors from DB result set to CAMS feed object
   * @param orderMap
   * @return
   */
  private Order mapToCamsOrder(Map orderMap) {
	  Order camsOrder = new Order();
      CachedResultSet rs = null;
      SimpleDateFormat sdf = new SimpleDateFormat(DB_DATE_FORMAT);
      SimpleDateFormat odSDF = new SimpleDateFormat(ORDER_DATE_FORMAT);
      Date odate = null;
      
      // Main order info
      //
      rs = (CachedResultSet) orderMap.get("OUT_CART_CUR");
      while(rs != null && rs.next()) {
    	  camsOrder.setMasterOrderNumber(rs.getString("master_order_number"));
    	  camsOrder.setBillingEmail(rs.getString("email_address"));
    	  camsOrder.setChannelId(rs.getString("origin_id")+"-"+rs.getString("company_id"));
    	  camsOrder.setDiscountAccount(rs.getString("membership_number"));
          camsOrder.setDiscountFirstName(rs.getString("membership_first_name"));
          camsOrder.setDiscountLastName(rs.getString("membership_last_name"));
          camsOrder.setDiscountProgram(rs.getString("membership_type"));

          try {
        	  odate = odSDF.parse(rs.getString("order_timestamp"));
          } catch (Exception e) {
        	  odate = null;
        	  logger.error("Error parsing order date for CAMS feed: " + rs.getString("order_timestamp"));
          }
          if(odate != null)
          {
        	  camsOrder.setOrderDate(new Date(odate.getTime()));
          }
          else
          {
        	  camsOrder.setOrderDate(odate);
          }
      }
      // Order total
      rs = (CachedResultSet) orderMap.get("OUT_CART_TOTAL_CUR");
      while(rs != null && rs.next()) {  // Should be only one item in cursor
          camsOrder.setTotalCharge(rs.getFloat("cart_total"));
      }
      
      // Get billing name and address
      //
      rs = (CachedResultSet) orderMap.get("OUT_CART_BUYER_CUR");
      if (rs != null && rs.next()) {  // Should be only one item in cursor
    	  camsOrder.setBillingFirstName(rs.getString("first_name"));
    	  camsOrder.setBillingLastName(rs.getString("last_name"));
    	  camsOrder.setBillingAddress(rs.getString("address_1"));
    	  camsOrder.setBillingAddress2(rs.getString("address_2"));
    	  camsOrder.setBillingCity(rs.getString("city"));
    	  camsOrder.setBillingState(rs.getString("state"));
    	  camsOrder.setBillingZip(rs.getString("zip_code"));
    	  camsOrder.setBillingCountry(rs.getString("country"));
      }
      
      // For phones, get use the first for each type (home/work) since it's the most recent
      //
      rs = (CachedResultSet) orderMap.get("OUT_CART_BUYER_PHONES_CUR");
      boolean gotHomePhone = false;
      boolean gotWorkPhone = false;
      while(rs != null && rs.next()) {
    	  String curPhone = rs.getString("phone_number");
    	  String curType = rs.getString("phone_type");
    	  String curExt = rs.getString("extension");
    	  String curPhoneType = rs.getString("phone_number_type");
    	  String curSmsOptIn = rs.getString("sms_opt_in");
    	  if (DB_CUSTOMER_PHONE_WORK.equals(curType)) {
    		  if (!gotWorkPhone) {
    			  camsOrder.setBillingWorkPhone(curPhone);
    			  camsOrder.setBillingWorkPhoneExt(curExt);
    			  camsOrder.setBillingWorkPhoneOpt(curSmsOptIn);
    			  camsOrder.setBillingWorkPhoneType(curPhoneType);
    			  gotWorkPhone = true;
    		  }
    	  } else if (DB_CUSTOMER_PHONE_HOME.equals(curType)) {
    		  if (!gotHomePhone) {
				  camsOrder.setBillingHomePhone(curPhone);
				  camsOrder.setBillingHomePhoneExt(curExt);
				  camsOrder.setBillingHomePhoneOpt(curSmsOptIn);
				  camsOrder.setBillingHomePhoneType(curPhoneType);
				  gotHomePhone = true;    		  
    		  }
    	  }
      }
      
	  return camsOrder;
  }

  
  /**
   * Internal method to map addon cursors from DB result set to a hash of CAMS feed objects.
   * The hash is required since we need addon info for a specific order_detail_id 
   * when we're building the CAMS object for each cart item.
   *  
   * @param orderMap
   * @return
   */
  private HashMap<String,List<OrderItemAddonDetails>> mapToCamsOrderItemAddonDetails(Map orderMap) {
	  HashMap<String,List<OrderItemAddonDetails>> addonHash = null;
	  String cOrderDetailId = null;
	  OrderItemAddonDetails cAddonDetail = null;
      CachedResultSet rs = null;
      List<OrderItemAddonDetails> cAddonList = null;
      
      rs = (CachedResultSet) orderMap.get("OUT_ORDER_ADDON_CUR");
      while(rs != null && rs.next()) {
    	  if (addonHash == null) {
    		  addonHash = new HashMap<String,List<OrderItemAddonDetails>>();  // Only want to create hash if any addons exist
    	  }
    	  cOrderDetailId = rs.getString("order_detail_id");
    	  cAddonDetail = new OrderItemAddonDetails();
    	  cAddonDetail.setAddOnCode(rs.getString("add_on_code"));
    	  cAddonDetail.setAddOnValue(rs.getString("add_on_code"));
    	  cAddonDetail.setAddOnCount(rs.getInt("add_on_quantity"));
    	  cAddonDetail.setAddOnType(rs.getString("addon_type"));
    	  cAddonDetail.setAddOnPrice(rs.getFloat("price"));
    	  if (addonHash.containsKey(cOrderDetailId)) {
    	      cAddonList = addonHash.get(cOrderDetailId);
    	  } else {
    	      cAddonList = new ArrayList<OrderItemAddonDetails>();
    	  }
    	  cAddonList.add(cAddonDetail);
    	  addonHash.put(cOrderDetailId, cAddonList);
      }
      return addonHash;
  }


  /**
   * Internal method to map order-bill cursors from DB result set to a hash of CAMS objects.
   * The hash is required since we need order-bill info for a specific order_detail_id 
   * when we're building the CAMS object for each cart item.
   *   
   * Note we're storing data in CAMS OrderItemDetails objects, but these objects will 
   * NOT be sent in CAMS feed.  Instead, fields will be copied from here to another 
   * OrderItemDetails object (in mapToCamsOrderItemInfoList) before sending to CAMS.
   * In other words we're just using these objects for lack of a better place to put the data.
   *  
   * @param orderMap
   * @return
   */
  private HashMap<String,List<OrderItemDetails>> mapJustOrderBills(Map orderMap) {
      HashMap<String,List<OrderItemDetails>> billHash = new HashMap<String,List<OrderItemDetails>>();
      String cOrderDetailId = null;
      String cOrderDetailFreeShippingFlag = null;
      String cOrderDetailOriginalOrderHasSDU = null;
      String cOrderNumber = null;
      OrderItemDetails cBillDetail = null;
      CachedResultSet rs = null;
      List<OrderItemDetails> cBillsList = null;
      
      rs = (CachedResultSet) orderMap.get("OUT_ORDER_BILLS_CUR");
      while(rs != null && rs.next()) {
          cOrderDetailId = rs.getString("order_detail_id");
          cOrderNumber = rs.getString("external_order_number");
          cOrderDetailFreeShippingFlag = rs.getString("free_shipping_flag");
          cOrderDetailOriginalOrderHasSDU = rs.getString("original_order_has_sdu");
          
          cBillDetail = new OrderItemDetails();
          cBillDetail.setAddVerfStatus(rs.getString("total_type"));  // Hijack this field to save indication if Add-bill or not
          cBillDetail.setDiscountAmount(rs.getFloat("discount_amount")); 
          cBillDetail.setTotalCharge(rs.getFloat("bill_total"));
          cBillDetail.setTax(rs.getFloat("tax"));         
          cBillDetail.setShippingCost(rs.getFloat("shipping_fee"));
          cBillDetail.setServiceCharge(rs.getFloat("service_fee"));
          cBillDetail.setSamedayUpcharge(rs.getFloat("same_day_upcharge"));
          cBillDetail.setProductPrice(rs.getFloat("product_amount")); 
          // Service and ship fee sum is saved here, but will only be used if FS was applied.
          // In those cases, this sum will represent any additional fee (e.g., International fee).
          cBillDetail.setEffectiveFsCharge(rs.getFloat("serv_ship_fee"));
          
          String regex = "C\\d";
          Pattern p = Pattern.compile(regex);

          Matcher m = p.matcher(cOrderNumber);
          boolean isOeOrder = false;
          isOeOrder = m.lookingAt();
          if(isOeOrder){ 
        	  if( "Y".equalsIgnoreCase(cOrderDetailFreeShippingFlag) &&  "Y".equalsIgnoreCase(cOrderDetailOriginalOrderHasSDU)){
        		  
        		  cBillDetail.setEffectiveFsCharge(cBillDetail.getEffectiveFsCharge()-cBillDetail.getSamedayUpcharge());
        	  }
        	  
        	  else{
        		  cBillDetail.setServiceCharge(cBillDetail.getServiceCharge()-cBillDetail.getSamedayUpcharge());
        	  }
        	 
          }
          
          if (cBillDetail.getDiscountAmount() != null) {
              cBillDetail.setDiscountPrice(cBillDetail.getProductPrice() - cBillDetail.getDiscountAmount());
          } else {
              cBillDetail.setDiscountPrice(cBillDetail.getProductPrice());              
          }
          
          if (billHash.containsKey(cOrderDetailId)) {
              cBillsList = billHash.get(cOrderDetailId);
          } else {
              cBillsList = new ArrayList<OrderItemDetails>();
          }
          cBillsList.add(cBillDetail);
          billHash.put(cOrderDetailId, cBillsList);
      }
      return billHash;
  }


  /**
   * Internal method to map order-fees-saved (due to Free-shipping) cursor from DB result set to a hash.
   * The hash is required since we need fees-saved info for a specific order_detail_id 
   * when we're building the CAMS object for each cart item.
   *  
   * @param orderMap
   * @return
   */
  private HashMap<String,Float> mapJustOrderSavings(Map orderMap) {
      HashMap<String,Float> savingsHash = new HashMap<String,Float>();
      String cOrderDetailId = null;
      Float cSavings = null;
      CachedResultSet rs = null;
      
      rs = (CachedResultSet) orderMap.get("OUT_ORDER_FEES_SAVED_CUR");
      while(rs != null && rs.next()) {
          cOrderDetailId = rs.getString("order_detail_id");
          cSavings = rs.getFloat("order_fees_saved"); 
          if (cSavings != null) {
              savingsHash.put(cOrderDetailId, cSavings);
          }
      }
      return savingsHash;
  }
  
  
  /**
   * Internal method to map item-level cursors from DB result set to CAMS feed object.
   * As we're building object for each cart item, we extract appropriate billing/addon
   * info from the hashes. 
   * NOTE: If all cart items are in either in scrub, pending or removed, this method 
   * returns a null object (so the caller knows there's nothing to feed to CAMS).
   * 
   * @param orderMap  DB result set
   * @param orderBillsHash Hash (by order_detail_id) of all order_bills on entire cart
   * @param orderSavingsHash Hash (by order_detail_id) of all Free-Shipping savings on entire cart
 * @param orderTaxes 
   * @return camsOrderItemAddonHash Hash (by order_detail_id) of all add_ons on entire cart
   */
  private List<OrderItemInfo> mapToCamsOrderItemInfoList(
          Map orderMap, 
          HashMap<String,List<OrderItemDetails>> orderBillsHash, 
          HashMap<String,Float> orderSavingsHash, 
          HashMap<String,List<OrderItemAddonDetails>> camsOrderItemAddonHash, Map<String, TaxInfo> orderTaxes
          ) {
      
      Date cdate = null;
      String cOrderDetailId = null;
      CachedResultSet rs = null;
      OrderItemInfo cOrderItemInfo = null;
      SimpleDateFormat sdf = new SimpleDateFormat(DB_DATE_FORMAT);
      SimpleDateFormat odSDF = new SimpleDateFormat(ORDER_DATE_FORMAT);
      List<OrderItemInfo> outOrderItemInfoList = null;
      
      // Loop over each item in cart
      //
      rs = (CachedResultSet) orderMap.get("OUT_ORDERS_CUR");
      while(rs != null && rs.next()) {

          cOrderDetailId = rs.getString("order_detail_id");

          // Handle item data
          //
          OrderItemDetails cOrderItemDetail = new OrderItemDetails();
          
          try {
        	  cdate = sdf.parse(rs.getString("delivery_date"));
          } catch (Exception e) { cdate = null; } // Just ignore if no date
          cOrderItemDetail.setDeliveryDate(cdate);
          try {
        	  cdate = sdf.parse(rs.getString("delivery_date_range_end"));
          } catch (Exception e) { cdate = null; } // Just ignore if no date
          cOrderItemDetail.setDeliveryDateEnd(cdate);
          try {
        	  cdate = odSDF.parse(rs.getString("order_timestamp"));
          } catch (Exception e) { cdate = null; } // Just ignore if no date
          if(cdate != null)
          {
        	  cOrderItemDetail.setOrderDate(new Date(cdate.getTime()));
          }
          else
          {
        	  cOrderItemDetail.setOrderDate(cdate);
          }
          
          cOrderItemDetail.setOrderNumber(rs.getString("external_order_number"));

          // Status will be null for complete order, or S (In-scrub), P (Pending), R (Removed)
          cOrderItemDetail.setStatus(rs.getString("scrub_status"));
          // CAMS doesn't want orders that are in Scrub/Pending/Removed
          if (cOrderItemDetail.getStatus() != null) {
              if (logger.isDebugEnabled()) {
                  logger.debug("Item in scrub/pending/removed, so skipping CAMS feed for: " + cOrderItemDetail.getOrderNumber());
              }        
              continue;
          } else {
              // CAMS wants a real value for status instead of null, so appease them
              cOrderItemDetail.setStatus(CAMS_CONFIRMED_STATUS);
          }
          
          cOrderItemDetail.setDeliveryMethod(rs.getString("ship_method"));
          cOrderItemDetail.setDeliveryNote(rs.getString("special_instructions"));
          cOrderItemDetail.setGiftMessage(rs.getString("card_message"));
          cOrderItemDetail.setGiftSignature(rs.getString("card_signature"));
          cOrderItemDetail.setMarkcode(rs.getString("source_code"));
          cOrderItemDetail.setOccasionId(rs.getString("occasion"));
        //    	  curOid.setOccasionId(rs.getString("occasion_description"));  Do this if they want description
          cOrderItemDetail.setProductCount(rs.getInt("quantity"));
          if (DB_FREE_SHIP_TYPE.equals(rs.getString("product_type")) && 
              DB_FREE_SHIP_SUB_TYPE.equals(rs.getString("product_sub_type"))) {
              // This is free shipping item
              cOrderItemDetail.setProductType("FS");
          } else {
              // Size_indicator reflects A,B,C for Good,Better,Best
              cOrderItemDetail.setProductType(rs.getString("size_indicator"));                 
          }
          cOrderItemDetail.setShippingFirstName(rs.getString("first_name"));
          cOrderItemDetail.setShippingLastName(rs.getString("last_name"));
          cOrderItemDetail.setShippingPhone(rs.getString("recipient_phone_number"));
          cOrderItemDetail.setShippingPhoneExt(rs.getString("recipient_extension"));
          cOrderItemDetail.setShippingAddressType(CommonUtils.getAddressTypeCode(rs.getString("address_type")));
          cOrderItemDetail.setShippingBusinessName(rs.getString("business_name"));
          cOrderItemDetail.setShippingAddress(rs.getString("address_1"));
          cOrderItemDetail.setShippingAddress2(rs.getString("address_2"));
          cOrderItemDetail.setShippingCity(rs.getString("city"));
          cOrderItemDetail.setShippingState(rs.getString("state"));
          cOrderItemDetail.setShippingZip(rs.getString("zip_code"));
          cOrderItemDetail.setShippingCountry(rs.getString("country"));
          cOrderItemDetail.setProductId(rs.getString("novator_id"));
          cOrderItemDetail.setProductName(rs.getString("product_name"));
          cOrderItemDetail.setRewards(rs.getFloat("miles_points"));
          cOrderItemDetail.setPrevOrderNumber(rs.getString("orig_external_order_number"));
          
          // Handle order_bill data
          //
          Float cEffectiveFsCharge = null;
          if ((orderBillsHash != null) &&
              (orderBillsHash.containsKey(cOrderDetailId))
              ) {
              List<OrderItemDetails> cList = orderBillsHash.get(cOrderDetailId);
              Iterator<OrderItemDetails> ci = cList.iterator();
              while(ci.hasNext()) {
                  OrderItemDetails coid = ci.next();
                  // We hijacked addVerfStatus field to hold AddBill indicator.  
                  // Since we're not handling AddBills for now, just use original bill data. 
                  if (DB_ORIGINAL_BILL.equals(coid.getAddVerfStatus())) {
                      cOrderItemDetail.setDiscountAmount(coid.getDiscountAmount()); 
                      cOrderItemDetail.setDiscountPrice(coid.getDiscountPrice());
                      cOrderItemDetail.setTotalCharge(coid.getTotalCharge());
                      cOrderItemDetail.setTax(coid.getTax());
                      cOrderItemDetail.setShippingCost(coid.getShippingCost());
                      cOrderItemDetail.setServiceCharge(coid.getServiceCharge());
                      cOrderItemDetail.setSamedayUpcharge(coid.getSamedayUpcharge());
                      cOrderItemDetail.setProductPrice(coid.getProductPrice()); 
                      cEffectiveFsCharge = coid.getEffectiveFsCharge();
                  }
              }
          }
          
          // Handle any free-shipping savings data
          //
          cOrderItemDetail.setEffectiveFsCharge((float) 0.0);  // HYD wants this 0 by default
          if ((orderSavingsHash != null) &&
              (orderSavingsHash.containsKey(cOrderDetailId))
              ) {
              Float fs = orderSavingsHash.get(cOrderDetailId);
              cOrderItemDetail.setFsSavings(fs);

              // If the FS savings are > 0 then save the Effective FS Charges.
              // This value represents any additional charges (e.g. International fee)
              // 
              if (fs != null && fs.compareTo((float) 0.0) > 0) {
                  cOrderItemDetail.setEffectiveFsCharge(cEffectiveFsCharge);
              }
          }
          
          // Handle any addon data
          //
          cOrderItemInfo = new OrderItemInfo();
          cOrderItemInfo.setOrderItemDetails(cOrderItemDetail);
          cOrderItemInfo.setTaxInfo(orderTaxes.get(cOrderDetailId));
          setTotalTaxRate(cOrderItemInfo.getTaxInfo(), cOrderItemDetail.getTotalCharge());
          if ((camsOrderItemAddonHash != null) && 
              (camsOrderItemAddonHash.containsKey(cOrderDetailId))
              ) {
              cOrderItemInfo.setOrderItemAddonInfo(camsOrderItemAddonHash.get(cOrderDetailId));
          }

          if (outOrderItemInfoList == null) {
              outOrderItemInfoList = new ArrayList<OrderItemInfo>();
          }
          outOrderItemInfoList.add(cOrderItemInfo);      
      }	  
	  
	  return outOrderItemInfoList;
  }
  

/**
   * Internal method to map item-level cursors from DB result set to CAMS feed object.
   * As we're building object for each cart item, we extract appropriate billing/addon
   * info from the hashes.
   * 
   * @param orderMap  DB result set
   * @param orderBillsHash Hash (by order_detail_id) of all order_bills on entire cart
   * @return camsOrderItemAddonHash Hash (by order_detail_id) of all add_ons on entire cart
   */
  private PaymentInfo mapToCamsPaymentInfoList(Map orderMap) {
      CachedResultSet rs = null;
      String cexp = null;
      //Date cdate = null;
      //SimpleDateFormat sdf = new SimpleDateFormat(DB_CC_AUTH_DATE_FORMAT);
      String ccType = null;
      String paymentType = null;
      String paymentTypeCams = null;
      CreditCard ccPayment = null;
      List<CreditCard> ccPaymentList = null;
      GiftCard gdPayment = null;
      List<GiftCard> gdPaymentList = null;
      AlternatePayment altPayment = null;
      List<AlternatePayment> altPaymentList = null;
      PaymentInfo cPaymentInfo = new PaymentInfo();
      
      // Loop over each item in cart
      //
      rs = (CachedResultSet) orderMap.get("OUT_PAYMENT_CC_CUR");
      while(rs != null && rs.next()) {
          ccType = rs.getString("cc_type");
          paymentType = rs.getString("payment_type");
          paymentTypeCams = CommonUtils.getPaymentTypeFromCode(paymentType);
          
          // Gift Card/Certificate payment
          //
          if (DB_GIFT_CARD_PAYMENT_TYPE.equals(paymentType)) {
              gdPayment = new GiftCard();
              gdPayment.setGiftCardNumber(rs.getString("cc_number_masked"));
              gdPayment.setPaymentType(paymentTypeCams);
              gdPayment.setCreatedDate(rs.getDate("created_on"));
              gdPayment.setModifiedDate(rs.getDate("updated_on"));
              gdPayment.setPaymentAmount(rs.getFloat("credit_amount"));
              if (gdPaymentList == null) {
                  gdPaymentList = new ArrayList<GiftCard>();
              }
              gdPaymentList.add(gdPayment);
          } else if (DB_GIFT_CERT_PAYMENT_TYPE.equals(paymentType)){
        	  gdPayment = new GiftCard();
              gdPayment.setGiftCardNumber(rs.getString("gc_coupon_number"));
              gdPayment.setPaymentType(paymentTypeCams);
              gdPayment.setCreatedDate(rs.getDate("created_on"));
              gdPayment.setModifiedDate(rs.getDate("updated_on"));
              gdPayment.setPaymentAmount(rs.getFloat("credit_amount"));
              if (gdPaymentList == null) {
                  gdPaymentList = new ArrayList<GiftCard>();
              }
              gdPaymentList.add(gdPayment);
          // Credit Card payment
          //
          } else if (ccType != null) {
             ccPayment = new CreditCard(); 
             ccPayment.setCcType(CommonUtils.getCreditCardTypeFromCode(ccType));
             ccPayment.setCcLastDigits(rs.getString("cc_number_masked"));
             try {
                 cexp = rs.getString("cc_expiration");
                 if (cexp != null) {
                     String[] csplit = cexp.split("/");
                     if (csplit.length == 2) {
                         ccPayment.setCcExpMonth(csplit[0]);
                         ccPayment.setCcExpYear(csplit[1]);
                     }
                 }
             } catch (Exception e) { cexp = null; } // Just ignore if no date
             ccPayment.setPaymentType(paymentTypeCams);
             ccPayment.setCreatedDate(rs.getDate("created_on"));
             ccPayment.setModifiedDate(rs.getDate("updated_on"));
             ccPayment.setPaymentAmount(rs.getFloat("credit_amount"));

             ccPayment.setPayPassWalletIndicator(rs.getString("wallet_indicator"));
             
             if (ccPaymentList == null) {
                 ccPaymentList = new ArrayList<CreditCard>();
             }
             ccPaymentList.add(ccPayment);
          // Alternate payment
          } else {
              altPayment = new AlternatePayment();
              altPayment.setAltPointsUsed(rs.getFloat("miles_points_credit_amt"));  // ???
              altPayment.setPaymentType(paymentTypeCams);
              altPayment.setCreatedDate(rs.getDate("created_on"));
              altPayment.setModifiedDate(rs.getDate("updated_on"));
              altPayment.setPaymentAmount(rs.getFloat("credit_amount"));
              if (altPaymentList == null) {
                  altPaymentList = new ArrayList<AlternatePayment>();
              }
              altPaymentList.add(altPayment);
          }
          
          /*
          String cAddress = rs.getString("address_line_1");
          if (rs.getString("address_line_2") != null) {
              cAddress += " " + rs.getString("address_line_2");
          }
          cPayment.setAddress(cAddress);
          cPayment.setCity(rs.getString("city"));
          cPayment.setState(rs.getString("state"));
          cPayment.setZipCode(rs.getString("zip_code"));
          cPayment.setCountry(rs.getString("country"));
          try {
              cdate = sdf.parse(rs.getString("auth_date"));
          } catch (Exception e) { cdate = null; } // Just ignore if no date
          cPayment.setAuthDate(cdate);
          */
      }
      cPaymentInfo.setGiftCardPayment(gdPaymentList);
      cPaymentInfo.setCreditCardPayment(ccPaymentList);
      cPaymentInfo.setAlternatePayment(altPaymentList);
      
      return cPaymentInfo;
  }
  
  
  
  
  /**
   * Set status in account order feed table to reflect feed attempt is complete
   * (either success or failure).
   * 
   * @param connection
   * @param wasFeedSuccessful (true if feed was successful, false otherwise)
   * @param orderGuid
   */
  public AccountOrderFeedVO doneWithOrderFeedAttempt(Connection connection, boolean wasFeedSuccessful, String orderGuid) throws Exception {
	AccountOrderFeedVO aofvo = null;
	String out_status = null;
	Map paramMap = new HashMap();

	paramMap.put("IN_ORDER_GUID", orderGuid);
	  
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    if (wasFeedSuccessful) {
    	dataRequest.setStatementID("ACCT_MGMT_UPDATE_ORDER_FEED_SUCCESS");
    } else {
        dataRequest.setStatementID("ACCT_MGMT_UPDATE_ORDER_FEED_FAILED");            
    }
    dataRequest.setInputParams(paramMap);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    out_status = (String) outputs.get("OUT_STATUS");
    
    if ("N".equalsIgnoreCase(out_status)) {
        String errMessage = (String) outputs.get("OUT_MESSAGE");
        throw new RuntimeException("Failure in doneWithOrderFeedAttempt: " + errMessage);
    }
    return aofvo;
  }
  
  
  /**
   * Converts a Util Date to a SQL Timestamp. Returns null if the passed in parameter is null
   * @param utilDate
   * @return
   */
  private java.sql.Timestamp toSqlTimestamp(Date utilDate)
  {
    if (utilDate == null)
    {
      return null;
    }


    java.sql.Timestamp retVal = new java.sql.Timestamp(utilDate.getTime());
    return retVal;
  }

  /**
   * Converts a SQL Timestamp to a util date. Returns null if the pass in parameter is null
   * @param timestamp
   * @return
   */
  private Date toUtilDate(java.sql.Timestamp timestamp)
  {
    if (timestamp == null)
    {
      return null;
    }

    return new Date(timestamp.getTime());
  }
  
  
  
	public Map<String, TaxInfo> getTaxSplit(Map<String, Object> orderMap) {
		CachedResultSet rs;
		Map<String, TaxInfo> orderTaxes = null;
		
		try {	
			
			 rs = (CachedResultSet)orderMap.get("OUT_ORDER_TAXES_CUR");
			 orderTaxes = new HashMap<String, TaxInfo>();
			 
		    while(rs.next()) {
		    	TaxInfo taxInfo = null;
		    	List<Tax> taxList = null;
		    	
		    	if(orderTaxes.containsKey(rs.getString("order_detail_id"))) {
		    		taxInfo = orderTaxes.get(rs.getString("order_detail_id"));	    		
		    		taxList = taxInfo.getTax();
		    	} else {
		    		taxInfo = new TaxInfo();	
		    		taxList = new ArrayList<Tax>();
		    		taxInfo.setTax(taxList);
		    		orderTaxes.put(rs.getString("order_detail_id"), taxInfo);
		    	}
		    	
		    	if("Y".equals(rs.getString("is_total"))) {
	    			TotalTax totalTax = new TotalTax();
	    			totalTax.setRate(rs.getFloat("tax_rate"));
	    			totalTax.setAmount(rs.getFloat("tax_amount"));
	    			totalTax.setDescription(rs.getString("tax_description")); 
	    			taxInfo.setTotalTax(totalTax); 
	    			 
	    		} else {
	    			Tax tax = new Tax();
	    			tax.setDescription(rs.getString("tax_description"));
	    			tax.setAmount(rs.getFloat("tax_amount")); 
	    			tax.setRate(rs.getFloat("tax_rate"));
	    			tax.setType(rs.getString("tax_type"));
	    			taxList.add(tax);
	    		}	    		
	    	}
		    
		    
			if (orderTaxes != null && orderTaxes.size() > 0) {
				Set<String> itemSet = orderTaxes.keySet();

				// when tax split is null but total tax is greater than zero, default rate applied. Set the tax performed to false.
				for (String orderItem : itemSet) {

					if (orderTaxes.get(orderItem) != null) {
						
						TaxInfo itemTax = orderTaxes.get(orderItem);
						itemTax.setTaxServicePerformed("Y"); // by default it is true.

						TotalTax totalTax = itemTax.getTotalTax();
						if (totalTax != null && totalTax.getAmount() > 0) { // tax applied
							if (itemTax.getTax() == null || itemTax.getTax().size() == 0) { // if there is no tax split saved, service is not called. Cannot be compared with total rate, because total rate can be 0 when service is called.								
								itemTax.setTaxServicePerformed("N"); 
							}
						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("Unable to get the total tax values, " , e);
		}
		return orderTaxes;
	}
	
	private void setTotalTaxRate(TaxInfo taxInfo, Float totalAmount) {
		Float totalTaxRate = null;
		if(taxInfo != null && taxInfo.getTotalTax() != null && (taxInfo.getTotalTax().getRate() == null || taxInfo.getTotalTax().getRate() == 0) && taxInfo.getTotalTax().getAmount() != null && taxInfo.getTotalTax().getAmount() > 0) {
			logger.info("Total tax rate is NULL or 0, but Total tax amount. This can happen when Tax service is down. So doing reverse calculation of Total Tax Rate");
			totalTaxRate = taxInfo.getTotalTax().getAmount()/(totalAmount - taxInfo.getTotalTax().getAmount());
			// Setting the scale to only 5. If we don't set the scale, then the decimal precision goes upto 8.
			taxInfo.getTotalTax().setRate(new Float(Math.floor(totalTaxRate*100000)/100000));			
			logger.info("Calculated total tax rate is: " + taxInfo.getTotalTax().getRate() + ", Total amount: " + totalAmount + ", Total tax amount: " + taxInfo.getTotalTax().getAmount());
		}
		
	}
  
}
