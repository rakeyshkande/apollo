package com.ftd.osp.account.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.account.vo.AutoRenewOrderVO;
import com.ftd.osp.account.vo.CommonCreditCardVO;
import com.ftd.osp.account.vo.CustomerAccount;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class FreeShippingEmailDAO 
{
	private static Logger logger = new Logger(FreeShippingEmailDAO.class.getName());
	
	private Connection conn;
	public FreeShippingEmailDAO(Connection conn) {
		this.conn = conn;
	}
	
	/**
     * Get the customer info by orderGuid
     *
     * @param orderGuid
     * @return CustomerAccount
     * @throws java.lang.Exception
     */
  public CustomerAccount getCustomerByOrder(String orderGuid) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    CustomerAccount cVO = new CustomerAccount();
    dataRequest.setConnection(this.conn);
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_GUID", orderGuid);

    /* build DataRequest object */
    dataRequest.setStatementID("GET_CUST_INFO_BY_ORDER");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    while (outputs.next())
    {
      cVO.setAddress1(outputs.getString("address_1"));
      cVO.setCustomerId(outputs.getLong("customer_id"));
      cVO.setZipCode(outputs.getString("zip_code"));
      cVO.setState(outputs.getString("state"));
      cVO.setCompanyId(outputs.getString("company_id"));
    }
    return cVO;
  }
  
	  public AutoRenewOrderVO getCCAuthParamsByExtOrdNo(String externalOrderNo) throws Exception
	  {
		   logger.debug("getCCAuthParamsByExtOrdNo() :"+externalOrderNo);
		    DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(this.conn);
		    HashMap<String, Object> inputParams = new HashMap<String, Object>();
		    inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNo);

		    /* build DataRequest object */
		    dataRequest.setStatementID("GET_CC_AUTH_PARAMS_BY_EXTORDNO");
		    dataRequest.setInputParams(inputParams);

		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		    AutoRenewOrderVO autoRenewOrderVO = null;
		    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		    String status = String.valueOf(outputs.get("OUT_STATUS"));
		    if (status!= null && status.equalsIgnoreCase("Y"))
		    {
		    	if("Y".equalsIgnoreCase(status))
		    	{
		    		logger.debug("got cc_auth params.");
		    		autoRenewOrderVO = new AutoRenewOrderVO();
		    		String productId = String.valueOf(outputs.get("OUT_PROD_ID"));
		    		String productPrice = String.valueOf(outputs.get("OUT_PROD_PRICE"));
			    	String orderGuid = String.valueOf(outputs.get("OUT_ORDER_GUID"));
			    	autoRenewOrderVO.setOrderGuid(orderGuid);
			    	autoRenewOrderVO.setExternalOrderNumber(externalOrderNo);
			    	autoRenewOrderVO.setProductId(productId);
			    	autoRenewOrderVO.setProductPrice(productPrice);
		    	}
		    }
		    return autoRenewOrderVO;
	  }

		public AutoRenewOrderVO createAutoRenewOrder(
				CustomerAccount account, 
				CommonCreditCardVO commonCreditCardVO,
				String productId,
				String productPrice) throws Exception
		{
			logger.debug("Creating auto renewal order.");
			DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(this.conn);
		    HashMap<String, Object> inputParams = new HashMap<String, Object>();
		    inputParams.put("IN_EXTERNAL_ORDER_NUMBER", account.getFsMembershipVO().getExternalOrderNumber());
		    inputParams.put("IN_EMAIL_ADDRESS", account.getFsMembershipVO().getEmailAddress());
		    inputParams.put("IN_CC_ID", commonCreditCardVO.getCcId());
		    inputParams.put("IN_CC_TYPE", commonCreditCardVO.getCreditCardType().substring(0, 2).toUpperCase());
		    inputParams.put("IN_CC_NUMBER", commonCreditCardVO.getCreditCardNumber());
		    inputParams.put("IN_CC_EXPIRATION", commonCreditCardVO.getExpirationDate());
		    inputParams.put("IN_KEY_NAME", commonCreditCardVO.getKeyName());
		    inputParams.put("IN_APPROVAL_CODE", commonCreditCardVO.getApprovalCode());
		    inputParams.put("IN_APPROVAL_VERBIAGE", commonCreditCardVO.getVerbiage());
		    inputParams.put("IN_APPROVAL_ACTION_CODE", commonCreditCardVO.getActionCode());
		    inputParams.put("IN_CC_AUTH_PROVIDER",commonCreditCardVO.getCcAuthProvider());
		    
		    inputParams.put("IN_PROD_ID", productId);		    
		    inputParams.put("IN_PROD_PRICE", productPrice);
		    
	        String delimitedPaymentExtFields = null;
			if(commonCreditCardVO.getPaymentExtMap()!=null && commonCreditCardVO.getPaymentExtMap().size()>0){
	        	Map<String,Object> paymentExtInfo = new HashMap(commonCreditCardVO.getPaymentExtMap());
	        	Map<String,Object> cardSpecificDetailsMap = (Map<String, Object>) paymentExtInfo.remove(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
	        	if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
	        		String deilimitedCardSpecificDetails = FieldUtils.getDelimitedStringFromMap(cardSpecificDetailsMap,":::","///");
	        		if(deilimitedCardSpecificDetails!=null){
	        			paymentExtInfo.put(PaymentExtensionConstants.CARD_SPECIFIC_GROUP, deilimitedCardSpecificDetails);
	        		}        		
	        	}
	        	delimitedPaymentExtFields = FieldUtils.getDelimitedStringFromMap(paymentExtInfo,"###","&&&");
	        }
	        inputParams.put("IN_PAYMENT_EXT_INFO",delimitedPaymentExtFields);
	        inputParams.put("IN_AVS_RESULT_CODE", commonCreditCardVO.getAVSIndicator());
		    dataRequest.setStatementID("INSERT_FS_AUTORENEW_ORDER");
		    dataRequest.setInputParams(inputParams);

		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		    AutoRenewOrderVO autoRenewOrderVO = null;
		    
		    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		    String status = (String) outputs.get("OUT_STATUS");
		    if (status!= null && "Y".equalsIgnoreCase(status))
		    {
	    		autoRenewOrderVO = new AutoRenewOrderVO();
	    		String masterOrderNo = (String) outputs.get("OUT_MASTER_ORDER_NUMBER");
		    	autoRenewOrderVO.setMasterOrderNumber(masterOrderNo);
		    	logger.debug("created auto renewal order.");
	    	} else {
	    		logger.debug("Auto renewal order creation failed");
	    	}
			return autoRenewOrderVO;
		}


		public boolean checkEmailIdInProcessing(String emailId) throws Exception 
		{

			DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(this.conn);
		    HashMap<String, Object> inputParams = new HashMap<String, Object>();
		    inputParams.put("IN_EMAIL_ADDRESS", emailId);
		    
		    dataRequest.setStatementID("CHECK_FS_PROCESSING_EMAILS");
		    dataRequest.setInputParams(inputParams);

		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		    String status = (String) dataAccessUtil.execute(dataRequest);
		    return("Y".equalsIgnoreCase(status));
		    
		}


		public void insertFSEmailProcessingRecord(String emailId) throws Exception
		{
			logger.debug("Inserting FS Email Processing Record:"+emailId);
			DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(this.conn);
		    HashMap<String, Object> inputParams = new HashMap<String, Object>();
		    inputParams.put("IN_EMAIL_ADDRESS", emailId);
		    
		    dataRequest.setStatementID("INSERT_FS_PROCESSING_EMAIL");
		    dataRequest.setInputParams(inputParams);

		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		    String status = (String) dataAccessUtil.execute(dataRequest);
		    
		    if (!"Y".equalsIgnoreCase(status))
		    {
	    	  throw new RuntimeException("Failed to insert FS Email Processing Record.");

		    }
		    logger.debug("Inserted successfully.");
		}
		
		public void deleteFSEmailProcessingRecord(String emailId) throws Exception
		{
			logger.debug("Deleting FS Email Processing Record:"+emailId);
			DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(this.conn);
		    HashMap<String, Object> inputParams = new HashMap<String, Object>();
		    inputParams.put("IN_EMAIL_ADDRESS", emailId);
		    
		    dataRequest.setStatementID("DELETE_FS_PROCESSING_EMAIL");
		    dataRequest.setInputParams(inputParams);

		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		    String status = (String) dataAccessUtil.execute(dataRequest);
		    
		    if (!"Y".equalsIgnoreCase(status))
		    {
	    	  throw new RuntimeException("Failed to delete FS Email Processing Record.");

		    }
		    logger.debug("Deleted successfully.");
		}
}
