package com.ftd.osp.account.vo;

public class CCData {
	public String ccNumber;
	public String expirationMonth;
	public String expirationYear;
	public String cardType;
	public String cardPurpose;
	
	public String getCcNumber() {
		return ccNumber;
	}
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	public String getExpirationMonth() {
		return expirationMonth;
	}
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	public String getExpirationYear() {
		return expirationYear;
	}
	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardPurpose() {
		return cardPurpose;
	}
	public void setCardPurpose(String cardPurpose) {
		this.cardPurpose = cardPurpose;
	}
}
