package com.ftd.osp.account.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents an Account and its related information
 */
public class AccountMasterVO
{
  private long accountMasterId;
  private List<AccountEmailVO> accountEmails = new ArrayList<AccountEmailVO>();
  private List<AccountProgramVO> accountPrograms = new ArrayList<AccountProgramVO>();


  public void setAccountMasterId(long accountMasterId)
  {
    this.accountMasterId = accountMasterId;
  }

  public long getAccountMasterId()
  {
    return accountMasterId;
  }

  public void setAccountEmails(List<AccountEmailVO> accountEmails)
  {
    this.accountEmails = accountEmails;
  }

  /**
   * @return List of account Emails. Never <code>null</code>
   */
  public List<AccountEmailVO> getAccountEmails()
  {
    if (accountEmails == null)
    {
      accountEmails = new ArrayList<AccountEmailVO>();
    }
    return accountEmails;
  }

  public void setAccountPrograms(List<AccountProgramVO> accountPrograms)
  {
    this.accountPrograms = accountPrograms;
  }

  /**
   * @return List of account Programs. Never <code>null</code>
   */
  public List<AccountProgramVO> getAccountPrograms()
  {
    if (accountPrograms == null)
    {
      accountPrograms = new ArrayList<AccountProgramVO>();
    }

    return accountPrograms;
  }

  /**
   * Iterates the email list and retrieves the Email Address that is Active.
   * Note: It is expected that the list only contains at most a single email address that is
   * in active status.
   * @return The active email address or <code>null</code> if there is no active email record
   */
  public String getActiveEmailAddress()
  {
    List<AccountEmailVO> accountEmails = getAccountEmails();
    for (AccountEmailVO accountEmailVO: accountEmails)
    {
      if (accountEmailVO.isActiveFlag())
      {
        return accountEmailVO.getEmailAddress();
      }
    }

    return null;
  }

  /**
   * @return Returns <code>true</code> if the account has an active email address. Else <code>false</code>
   */
  public boolean isAccountActive()
  {
    if (getActiveEmailAddress() == null)
    {
      return false;
    }

    return true;
  }
}
