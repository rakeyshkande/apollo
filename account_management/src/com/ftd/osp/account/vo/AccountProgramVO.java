package com.ftd.osp.account.vo;

import java.util.Date;

/**
 * Represents Program Information for an Account
 */
public class AccountProgramVO
{
  private long accountProgramId;
  private long programMasterId;
  
  /**
   * Name from the Program Master Table
   */
  private String programName; 

  private String accountProgramStatus;
  
  private String autoRenewFlag;
  
  private Date expirationDate;
  
  private Date startDate;
  
  private String externalOrderNumber;

  /**
   * The last time the Program was updated
   */
  private Date updatedOn;
  private String displayName;
  
  public void setAccountProgramId(long accountProgramId)
  {
    this.accountProgramId = accountProgramId;
  }

  public long getAccountProgramId()
  {
    return accountProgramId;
  }

  public void setProgramMasterId(long programMasterId)
  {
    this.programMasterId = programMasterId;
  }

  public long getProgramMasterId()
  {
    return programMasterId;
  }

  public void setProgramName(String programName)
  {
    this.programName = programName;
  }

  public String getProgramName()
  {
    return programName;
  }

  public void setAutoRenewFlag(String autoRenewFlag)
  {
    this.autoRenewFlag = autoRenewFlag;
  }

  public String getAutoRenewFlag()
  {
    return autoRenewFlag;
  }

  public void setExpirationDate(Date expirationDate)
  {
    this.expirationDate = expirationDate;
  }

  public Date getExpirationDate()
  {
    return expirationDate;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }

  public Date getStartDate()
  {
    return startDate;
  }
  
  public void setAccountProgramStatus(String accountProgramStatus)
  {
    this.accountProgramStatus = accountProgramStatus;
  }

  public String getAccountProgramStatus()
  {
    return accountProgramStatus;
  }
  
  public boolean isAccountProgramStatusActive()
  {
    return "A".equalsIgnoreCase(accountProgramStatus);
  }

  public void setUpdatedOn(Date updatedOn)
  {
    this.updatedOn = updatedOn;
  }

  public Date getUpdatedOn()
  {
    return updatedOn;
  }

  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = externalOrderNumber;
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

	public String getDisplayName() {
		return displayName;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
  
}
