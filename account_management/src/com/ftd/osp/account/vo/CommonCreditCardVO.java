package com.ftd.osp.account.vo;

import java.util.HashMap;
import java.util.Map;

public class CommonCreditCardVO extends CreditCardVO 
{
	private static final long serialVersionUID = 1L;
	private String validationStatus; // 'D' - Declined, 'A' - Approved, 'E' - System Error //
	  private String creditCardType;
	  private String creditCardDnsType;
	  private String ccId; 
	  private String name;
	  private String address2;
	  private String city;
	  private String country;
	  private String customerId;
	  private boolean ccSkipAuth;
	  private String keyName;
	  private String ccAuthProvider;
	  
	  private Map<String, Object> paymentExtMap;
	  
	  public CommonCreditCardVO()
	  {
	  }

	  public void setValidationStatus(String validationStatus)
	  {
	    this.validationStatus = validationStatus;
	  }


	  public String getValidationStatus()
	  {
	    return validationStatus;
	  }


	  public void setCreditCardType(String creditCardType)
	  {
	    this.creditCardType = creditCardType;
	  }


	  public String getCreditCardType()
	  {
	    return creditCardType;
	  }


	  public void setCreditCardDnsType(String creditCardDnsType)
	  {
	    this.creditCardDnsType = creditCardDnsType;
	  }


	  public String getCreditCardDnsType()
	  {
	    return creditCardDnsType;
	  }


	  public void setAddress2(String address2)
	  {
	    this.address2 = address2;
	  }


	  public String getAddress2()
	  {
	    return address2;
	  }


	  public void setCcId(String ccId)
	  {
	    this.ccId = ccId;
	  }


	  public String getCcId()
	  {
	    return ccId;
	  }


	  public void setCity(String city)
	  {
	    this.city = city;
	  }


	  public String getCity()
	  {
	    return city;
	  }


	  public void setCountry(String country)
	  {
	    this.country = country;
	  }


	  public String getCountry()
	  {
	    return country;
	  }


	  public void setCustomerId(String customerId)
	  {
	    this.customerId = customerId;
	  }


	  public String getCustomerId()
	  {
	    return customerId;
	  }


	  public void setName(String name)
	  {
	    this.name = name;
	  }


	  public String getName()
	  {
	    return name;
	  }

	    public void setCcSkipAuth(boolean _ccSkipAuth) {
	        this.ccSkipAuth = _ccSkipAuth;
	    }

	    public boolean isCcSkipAuth() {
	        return ccSkipAuth;
	    }

		public String getKeyName() {
			return keyName;
		}

		public void setKeyName(String keyName) {
			this.keyName = keyName;
		}
	    
		public Map<String, Object> getPaymentExtMap() {	
			if(this.paymentExtMap == null) {
				paymentExtMap = new HashMap<String, Object>();
			}
			return paymentExtMap;
		}

		public String getCcAuthProvider() {
			return ccAuthProvider;
		}

		public void setCcAuthProvider(String ccAuthProvider) {
			this.ccAuthProvider = ccAuthProvider;
		}
}
