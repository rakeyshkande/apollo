package com.ftd.osp.account.vo;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ftd.order.core.domain.AlternatePayment;
import com.ftd.order.core.domain.CreditCard;
import com.ftd.order.core.domain.GiftCard;
import com.ftd.order.core.domain.Order;
import com.ftd.order.core.domain.OrderInfo;
import com.ftd.order.core.domain.OrderItemAddonDetails;
import com.ftd.order.core.domain.OrderItemDetails;
import com.ftd.order.core.domain.OrderItemInfo;
import com.ftd.order.core.domain.PaymentInfo;
import com.ftd.order.core.domain.Tax;

/**
 * Represents the data for an Order Feed to CAMS.
 * Currently only have need for OrderInfo object, but may need others in future.
 */
public class AccountOrderFeedVO
{
  private OrderInfo camsOrderInfo;
  
  public OrderInfo getCamsOrderInfo() {
	return camsOrderInfo;
  }

  public void setCamsOrderInfo(OrderInfo camsOrderInfo) {
	this.camsOrderInfo = camsOrderInfo;
  }

  
  // For testing purposes only.  Not a comprehensive dump.
  //
  public String toString() {
      StringBuffer obuff = new StringBuffer();
      Order camsOrder = camsOrderInfo.getOrder();
    
      // Order
	  obuff.append("\nmasterOrderNumber:  '" + camsOrder.getMasterOrderNumber() + "'\n");
      obuff.append("billingEmail:         '" + camsOrder.getBillingEmail() + "'\n");
      obuff.append("channelId:            '" + camsOrder.getChannelId() + "'\n");
      obuff.append("orderDate:            '" + camsOrder.getOrderDate() + "'\n");
      obuff.append("billingFirstName:     '" + camsOrder.getBillingFirstName() + "'\n");
      obuff.append("billingLastName:      '" + camsOrder.getBillingLastName() + "'\n");
      obuff.append("billingAddress:       '" + camsOrder.getBillingAddress() + "'\n");
      obuff.append("billingAddress2:      '" + camsOrder.getBillingAddress2() + "'\n");
      obuff.append("billingCity:          '" + camsOrder.getBillingCity() + "'\n");
      obuff.append("billingState:         '" + camsOrder.getBillingState() + "'\n");
      obuff.append("billingZip:           '" + camsOrder.getBillingZip() + "'\n");
      obuff.append("billingCountry:       '" + camsOrder.getBillingCountry() + "'\n");
      obuff.append("billingWorkPhone:     '" + camsOrder.getBillingWorkPhone() + "'\n");
      obuff.append("billingWorkPhoneExt:  '" + camsOrder.getBillingWorkPhoneExt() + "'\n");
      obuff.append("billingWorkPhoneType: '" + camsOrder.getBillingWorkPhoneType() + "'\n");
      obuff.append("billingWorkSmsOptIn:  '" + camsOrder.getBillingWorkPhoneOpt() + "'\n");
      obuff.append("billingHomePhone:     '" + camsOrder.getBillingHomePhone() + "'\n");
      obuff.append("billingHomePhoneExt:  '" + camsOrder.getBillingHomePhoneExt() + "'\n");
      obuff.append("billingHomePhoneType: '" + camsOrder.getBillingHomePhoneType() + "'\n");
      obuff.append("billingHomeSmsOptIn:  '" + camsOrder.getBillingHomePhoneOpt() + "'\n");
      obuff.append("totalCharge:          '" + camsOrder.getTotalCharge() + "'\n");
      obuff.append("discountAccount:      '" + camsOrder.getDiscountAccount() + "'\n");
      obuff.append("discountLastName:     '" + camsOrder.getDiscountLastName() + "'\n");
      
     
      // Order item
      List<OrderItemInfo> camsOrderItemInfoList = camsOrderInfo.getOrderItemInfo();
      Iterator<OrderItemInfo> ooli = camsOrderItemInfoList.iterator();
      while (ooli.hasNext()) {
          OrderItemInfo citem = ooli.next();
          OrderItemDetails cd = citem.getOrderItem();
          obuff.append("deliveryMethod:       '" + cd.getDeliveryMethod() + "'\n");
          obuff.append("deliveryNote:         '" + cd.getDeliveryNote() + "'\n");
          obuff.append("orderNumber:          '" + cd.getOrderNumber() + "'\n");
          obuff.append("giftMessage:          '" + cd.getGiftMessage() + "'\n");
          obuff.append("occasionId:           '" + cd.getOccasionId() + "'\n");
          obuff.append("productCount:         '" + cd.getProductCount() + "'\n");
          obuff.append("productType:          '" + cd.getProductType() + "'\n");
          obuff.append("status:               '" + cd.getStatus() + "'\n");          
          obuff.append("shippingFirstName:    '" + cd.getShippingFirstName() + "'\n");
          obuff.append("shippingLastName:     '" + cd.getShippingLastName() + "'\n");
          obuff.append("shippingPhone:        '" + cd.getShippingPhone() + "'\n");
          obuff.append("shippingPhoneExt:     '" + cd.getShippingPhoneExt() + "'\n");
          obuff.append("shippingAddressType:  '" + cd.getShippingAddressType() + "'\n");
          obuff.append("shippingBusinessName: '" + cd.getShippingBusinessName() + "'\n");
          obuff.append("shippingAddress:      '" + cd.getShippingAddress() + "'\n");
          obuff.append("shippingAddress2:     '" + cd.getShippingAddress2() + "'\n");
          obuff.append("shippingCity:         '" + cd.getShippingCity() + "'\n");
          obuff.append("shippingState:        '" + cd.getShippingState() + "'\n");
          obuff.append("shippingZip:          '" + cd.getShippingZip() + "'\n");
          obuff.append("shippingCountry:      '" + cd.getShippingCountry() + "'\n");
          obuff.append("deliveryDate:         '" + cd.getDeliveryDate() + "'\n");
          obuff.append("deliveryDateEnd:      '" + cd.getDeliveryDateEnd() + "'\n");
          obuff.append("orderDate:            '" + cd.getOrderDate() + "'\n");
          obuff.append("productId:            '" + cd.getProductId() + "'\n");
          obuff.append("productName:          '" + cd.getProductName() + "'\n");

          obuff.append("discountAmount:       '" + cd.getDiscountAmount() + "'\n"); 
          obuff.append("discountPrice:        '" + cd.getDiscountPrice() + "'\n"); 
          obuff.append("totalCharge:          '" + cd.getTotalCharge() + "'\n");
          obuff.append("tax:                  '" + cd.getTax() + "'\n");
          obuff.append("shippingCost:         '" + cd.getShippingCost() + "'\n");
          obuff.append("serviceCharge:        '" + cd.getServiceCharge() + "'\n");
          obuff.append("samedayUpcharge:      '" + cd.getSamedayUpcharge() + "'\n");
          obuff.append("productPrice:         '" + cd.getProductPrice() + "'\n");
          obuff.append("prevOrderNumber:      '" + cd.getPrevOrderNumber() + "'\n");
          obuff.append("effectiveFsCharge:    '" + cd.getEffectiveFsCharge() + "'\n");
          obuff.append("fsSavings:            '" + cd.getFsSavings() + "'\n");
          
			try {
				if (citem.getTaxInfo() != null) {
					if (citem.getTaxInfo().getTax() != null) {
						int count = 0;
						for (Tax tax : citem.getTaxInfo().getTax()) {
							count++;
							obuff.append("---tax" + count + " Split ---\n");
							obuff.append("rate:                  '" + tax.getRate() + "'\n");
							obuff.append("amount:                '" + tax.getAmount() + "'\n");
							obuff.append("description:           '" + tax.getDescription() + "'\n");
						}
					}

					if (citem.getTaxInfo().getTotalTax() != null) {
						obuff.append("---Total tax---\n");
						obuff.append("rate:                  '" + citem.getTaxInfo().getTotalTax().getRate() + "'\n");
						obuff.append("amount:                '" + citem.getTaxInfo().getTotalTax().getAmount() + "'\n");
						obuff.append("description:           '" + citem.getTaxInfo().getTotalTax().getDescription() + "'\n");
					}
				}
			} catch (Exception e) {
				obuff.append("Error caught constructing the tax split for log, continuing with other elements \n");
			}
                  

          List<OrderItemAddonDetails> cAddonList = citem.getOrderItemAddonInfo();
          if (cAddonList != null) {
              Iterator<OrderItemAddonDetails> ci = cAddonList.iterator();
              while(ci.hasNext()) {
                  OrderItemAddonDetails cadd = ci.next();
                  obuff.append("addOnCode:            '" + cadd.getAddOnCode() + "'\n");
                  obuff.append("addOnValue:           '" + cadd.getAddOnValue() + "'\n");
                  obuff.append("addOnCount:           '" + cadd.getAddOnCount() + "'\n");
                  obuff.append("addOnPrice:           '" + cadd.getAddOnPrice() + "'\n");
                  obuff.append("addOnType:            '" + cadd.getAddOnType() + "'\n");
              }
          }
      }
      PaymentInfo cPayInfo = camsOrderInfo.getPaymentInfo();
      if (cPayInfo != null) {
          List<CreditCard> ccPaymentList = cPayInfo.getCreditCardPayment();
          List<GiftCard> gdPaymentList = cPayInfo.getGiftCardPayment();
          List<AlternatePayment> altPaymentList = cPayInfo.getAlternatePayment();
          if (ccPaymentList != null) {
              Iterator<CreditCard> cci = ccPaymentList.iterator();
              while(cci.hasNext()) {
                  CreditCard ccPayment = cci.next();
                  obuff.append("---CC payment---\n");
                  obuff.append("paymentType:          '" + ccPayment.getPaymentType() + "'\n");
                  obuff.append("paymentAmount:        '" + ccPayment.getPaymentAmount() + "'\n");
                  obuff.append("modifiedDate:         '" + ccPayment.getModifiedDate() + "'\n");
                  obuff.append("createdDate:          '" + ccPayment.getCreatedDate() + "'\n");
                  obuff.append("ccType:               '" + ccPayment.getCcType() + "'\n");
                  obuff.append("ccLastDigits:         '" + ccPayment.getCcLastDigits() + "'\n");
                  obuff.append("ccExpMonth:           '" + ccPayment.getCcExpMonth() + "'\n");
                  obuff.append("ccExpYear:            '" + ccPayment.getCcExpYear() + "'\n");
                  obuff.append("walletIndicator:      '" + ccPayment.getPayPassWalletIndicator() + "'\n");
              }
          }
          if (gdPaymentList != null) {
              Iterator<GiftCard> gdi = gdPaymentList.iterator();
              while(gdi.hasNext()) {
                  GiftCard gdPayment = gdi.next();
                  obuff.append("---Gift Card payment---\n");
                  obuff.append("paymentType:          '" + gdPayment.getPaymentType() + "'\n");
                  obuff.append("paymentAmount:        '" + gdPayment.getPaymentAmount() + "'\n");
                  obuff.append("modifiedDate:         '" + gdPayment.getModifiedDate() + "'\n");
                  obuff.append("createdDate:          '" + gdPayment.getCreatedDate() + "'\n");
                  obuff.append("giftCardNumber:       '" + gdPayment.getGiftCardNumber() + "'\n");
              }
          }
          if (altPaymentList != null) {
              Iterator<AlternatePayment> api = altPaymentList.iterator();
              while (api.hasNext()) {
                  AlternatePayment altPayment = api.next();
                  obuff.append("---Alternate payment---\n");
                  obuff.append("paymentType:          '" + altPayment.getPaymentType() + "'\n");
                  obuff.append("paymentAmount:        '" + altPayment.getPaymentAmount() + "'\n");
                  obuff.append("modifiedDate:         '" + altPayment.getModifiedDate() + "'\n");
                  obuff.append("createdDate:          '" + altPayment.getCreatedDate() + "'\n");
                  obuff.append("altPointsUsed:        '" + altPayment.getAltPointsUsed() + "'\n");
              }
          }
      }
	  return obuff.toString();
  }
  
}
