/**
 * 
 */
package com.ftd.osp.account.vo;

import java.util.Date;

/**
 * @author skatam
 *
 */
public class FSMembershipVO 
{
	private String emailAddress;
	private String externalOrderNumber;
	private Integer duration;
	private Date startDate;
	private Date endDate;
	private String membershipStatus;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getExternalOrderNumber() {
		return externalOrderNumber;
	}
	public void setExternalOrderNumber(String externalOrderNumber) {
		this.externalOrderNumber = externalOrderNumber;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getMembershipStatus() {
		return membershipStatus;
	}
	public void setMembershipStatus(String membershipStatus) {
		this.membershipStatus = membershipStatus;
	}

}
