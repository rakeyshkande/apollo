package com.ftd.osp.account.vo;

/**
 * Represents the Email for an Account
 */
public class AccountEmailVO
{
  private long  accountEmailId;
  private String emailAddress;
  private String activeFlag;


  public void setAccountEmailId(long accountEmailId)
  {
    this.accountEmailId = accountEmailId;
  }

  public long getAccountEmailId()
  {
    return accountEmailId;
  }

  public void setEmailAddress(String emailAddress)
  {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress()
  {
    return emailAddress;
  }

  public void setActiveFlag(String activeFlag)
  {
    this.activeFlag = activeFlag;
  }
  
  public void setActiveFlag(boolean active)
  {
    this.activeFlag = active ? "Y" : "N";
  }

  public String getActiveFlag()
  {
    return activeFlag;
  }
  
  public boolean isActiveFlag()
  {
    return "Y".equalsIgnoreCase(activeFlag);
  }
}
