package com.ftd.osp.account.vo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * @author skatam
 *
 */
public class CustomerAccount 
{
	private long 	customerId;
	private String emailId;
	private String 	address1;
	private String 	city;
	private String 	state;
	private String 	zipCode;
	private String  companyId;
	
	private FSMembershipVO fsMembershipVO;
	private List<CCData> ccDataList;
	

	public boolean hasCreditCards() {
		return ccDataList!=null && !ccDataList.isEmpty();
	}

	public List<CCData> getCcData() {
		return ccDataList;
	}

	public void setCcData(List<CCData> ccData) {
		this.ccDataList = ccData;
	}

	public CCData getAutoRenewalCC() {
		List<CCData> list = this.getCreditCardOfType("AutoRenew");
		if(list != null && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}

	public CCData getSavedBillingCC() {
		List<CCData> list = this.getCreditCardOfType("Billing");
		if(list != null && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}

	public List<CCData> getOtherCCs() {
		return this.getCreditCardOfTypeNot("AutoRenew", "Billing");
	}

	private List<CCData> getCreditCardOfType(String... types)
	{
		List<CCData> ccList = new ArrayList<CCData>();
		if(!hasCreditCards()){
			return ccList;
		}
		for (CCData cc : ccDataList) 
		{
			if(types!=null && types.length>0)
			{
				boolean flag = false;
				for (String type : types) 
				{
					if(type.equalsIgnoreCase(cc.getCardPurpose())){
						flag = true;
						break;
					}
				}
				if(flag){
					ccList.add(cc);
				}
			}
		}
		return ccList;
	}
	
	public FSMembershipVO getFsMembershipVO() {
		return fsMembershipVO;
	}

	public void setFsMembershipVO(FSMembershipVO fsMembershipVO) {
		this.fsMembershipVO = fsMembershipVO;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmailId() {
		return this.emailId;
	}
	private List<CCData> getCreditCardOfTypeNot(String... types)
	{
		List<CCData> ccList = new ArrayList<CCData>();
		if(!hasCreditCards()){
			return ccList;
		}
		
		for (CCData cc : ccDataList) 
		{
			if(types!=null && types.length>0)
			{
				boolean flag = true;
				for (String type : types) 
				{
					if(type.equalsIgnoreCase(cc.getCardPurpose())){
						flag = false;
						break;
					}
				}
				if(flag){
					ccList.add(cc);
				}
			}
			
		}
		Collections.sort(ccList, new DataDescComparator());
		return ccList;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public List<CCData> getCCDataList() {
		return ccDataList;
	}

	public void setCCDataList(List<CCData> ccDataList) {
		this.ccDataList = ccDataList;
	}
		
}
/**
 * Sorts the CustomerCCData object in expiry_date desc order.
 * @author skatam
 *
 */
class DataDescComparator implements Comparator<CCData>
{
	@Override
	public int compare(CCData o1, CCData o2) 
	{
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		int year = 0;
		int month = 0;
		year = Integer.parseInt(o1.getExpirationYear());
		month = Integer.parseInt(o1.getExpirationMonth());
		
		cal1.set(Calendar.YEAR, year);
		cal1.set(Calendar.MONTH, month);
		Date date1 = cal1.getTime();
		
		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		year = Integer.parseInt(o2.getExpirationYear());
		month = Integer.parseInt(o2.getExpirationMonth());
		
		cal2.set(Calendar.YEAR, year);
		cal2.set(Calendar.MONTH, month);
		Date date2 = cal2.getTime();
		
		return date2.compareTo(date1);
	}
	
}
