package com.ftd.osp.account.mdb;

import java.sql.Connection;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.osp.account.bo.AccountBO;
import com.ftd.osp.account.bo.FSAutoRenewBO;
import com.ftd.osp.account.common.AccountConstants;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.account.feed.bo.AccountFeedBO;
import com.ftd.osp.account.vo.FSMembershipVO;
import com.ftd.osp.account.xml.bo.AccountXMLBO;
import com.ftd.osp.account.xml.vo.AccountManagementTaskVO;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * Processes inbound Account Messages from the JMS:ACCOUNT_QUEUE
 * This bean is configured for Container Managed Transactions
 */
public class AccountMDB
  implements MessageDrivenBean, MessageListener
{
  private static Logger logger = new Logger(AccountMDB.class.getName());

  MessageDrivenContext messageDrivenContext;

  /**
   * Common Application Utilities
   */
  CommonUtils commonUtils = new CommonUtils();
  
  /**
   * BO for performing Account Feeds
   */
  AccountFeedBO accountFeedBO;
  
  /**
   * BO to update call CAMS
   */
  
  AccountBO accountBO;

  /**
   * BO for processing XML
   */
  AccountXMLBO accountXMLBO = new AccountXMLBO();
  
  FSAutoRenewBO fsAutoRenewBO = new FSAutoRenewBO();
  
  /**
   * ejbCreate
   */
  public void ejbCreate()
  {
  }

  /**
   * MDB Constructor.
   */
  public AccountMDB()
  {
    // Instantiate the Feed BO to send Messages to the System Messenger
    accountFeedBO = new AccountFeedBO()
    {
        protected void sendSystemMessage(String message, String type)
        {
          commonUtils.sendSystemMessage(message, type);
        }
    };
    
    accountBO = new AccountBO();
  }

  /**
   * Called when the Bean is removed.
   * @see javax.ejb.MessageDrivenBean
   */
  public void ejbRemove()
  {
  }

  /**
   * Registers the message driven context with the bean
   * @param messageDrivenContext
   * @see javax.ejb.MessageDrivenBean
   */
  public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext)
  {
    this.messageDrivenContext = messageDrivenContext;
  }


  /**
   * Handle the inbound message. Expects a TextMessage containing the AccountTask XML
   * @param message
   */
  public void onMessage(Message message)
  {
    Connection connection = null;
    String ExternalOrderNumber="";
    String payLoad = null;
    try
    {
      TextMessage textMessage  = (TextMessage) message;
      boolean redeliveryFlag = message.getJMSRedelivered();
      
      if(textMessage != null){
    	  payLoad = textMessage.getText();
      }
      logger.info("message.getJMSRedelivered() ============>>>>>>>>>>>>>>: "+ redeliveryFlag);
      logger.debug(":AccountMDB textMessage ->"+textMessage.getText());
      AccountManagementTaskVO accountManagementTaskVO = getAccountManagementTask(textMessage);

      if(accountManagementTaskVO == null)
      {
        // There was a parse exception, nothing to do
        return;
      }
      ExternalOrderNumber=accountManagementTaskVO.getFsMembershipVO().getExternalOrderNumber();
      if (AccountConstants.TASK_TYPE_MEMBERSHIP_STATUS_UPDATE.equalsIgnoreCase(accountManagementTaskVO.getTaskType()))
      {
    	  logger.info("Type : "+accountManagementTaskVO.getTaskType()
    			  +" Email : "+accountManagementTaskVO.getFsMembershipVO().getEmailAddress()
    			  +" External Order Number : "+accountManagementTaskVO.getFsMembershipVO().getExternalOrderNumber()
    			  +" Status : "+accountManagementTaskVO.getFsMembershipVO().getMembershipStatus());
    	 FTDCAMSUtils.updateProgramStatus(accountManagementTaskVO.getFsMembershipVO().getEmailAddress(), accountManagementTaskVO.getFsMembershipVO().getExternalOrderNumber(), 
    	 accountManagementTaskVO.getFsMembershipVO().getMembershipStatus(), redeliveryFlag, payLoad);
    	
        
      }
	  else if(AccountConstants.TASK_TYPE_FS_AUTORENEW_UPDATE.
    		  	equalsIgnoreCase(accountManagementTaskVO.getTaskType()))
      {
    	  connection = commonUtils.getConnection();
          executeTaskSendFSUpdates(connection, accountManagementTaskVO);
      }
      else if(AccountConstants.TASK_TYPE_FS_PROCESSING_EMAIL.
    		  equalsIgnoreCase(accountManagementTaskVO.getTaskType()))
      {
    	  connection = commonUtils.getConnection();
          executeTaskProcessFSAutoRenew(connection, accountManagementTaskVO);
      }
      else if(AccountConstants.TASK_TYPE_FULLY_REFUND_ORDER_UPDATE.
    		  equalsIgnoreCase(accountManagementTaskVO.getTaskType()))
      {
    	  if(accountManagementTaskVO.getFsMembershipVO() != null)
    	  {
    		  accountBO.updateFullyRefundStatusInCAMS(accountManagementTaskVO.getFsMembershipVO().getExternalOrderNumber(), redeliveryFlag, payLoad);
    	  }
      }
      else if(AccountConstants.TASK_TYPE_CANCEL_ORDER_UPDATE.
    		  equalsIgnoreCase(accountManagementTaskVO.getTaskType()))
      {
    	  if(accountManagementTaskVO.getFsMembershipVO() != null)
    	  {
    		  accountBO.updateCancelOrderStatusInCAMS(accountManagementTaskVO.getFsMembershipVO().getExternalOrderNumber(), redeliveryFlag, payLoad);
    	  }
      }
      else
      {
        logger.error("Unknown Task Type: " + accountManagementTaskVO.getTaskType());
        commonUtils.sendSystemMessage("Unknown Task Type: " + accountManagementTaskVO.getTaskType(), "Error");
      }
    }
    catch (CustProfileTimeoutException timeoutEx){
     logger.error("Error proccessing JMS message: " + timeoutEx.getMessage(), timeoutEx);
     commonUtils.sendSystemMessage("CustProfileTimeoutException for the Payload "+ payLoad , timeoutEx ,"NOPAGE CustProfileTimeout Error having ExternalOrderNumber = "+ExternalOrderNumber);
     throw timeoutEx; // Rethrow so the Message is retried
    }
    catch (RuntimeException e)
    {
      logger.error("Error proccessing JMS message: " + e.getMessage(), e);
      commonUtils.sendSystemMessage("Error proccessing JMS message", e);
      throw e; // Rethrow so the Message is retried
    }
    catch (Throwable t)
    {
      logger.error("Error proccessing JMS message: " + t.getMessage(), t);
      commonUtils.sendSystemMessage("Error proccessing JMS message", t);
      throw new RuntimeException(t.getMessage(), t);
    }
    finally
    {
      commonUtils.closeConnection(connection);
    }
  }

  /**
   * Extracts the Account Text and parses it. Parsing exceptions are not rethrown since there
   * is no point to retrying the JMS Message.
   * They are logged in the System Message once.
   * @param textMessage
   * @return
   */
  private AccountManagementTaskVO getAccountManagementTask(TextMessage textMessage)
    throws JMSException
  {
      String msgData;
      AccountManagementTaskVO accountManagementTaskVO = null;
      
      msgData = textMessage.getText();
      
      logger.debug("Received Message");
      logger.debug(msgData);

      try
      {
        accountManagementTaskVO = accountXMLBO.parseAccountManagementTaskFromXML(msgData);    
      } catch (Throwable t)
      {
        logger.error("Failed to parse XML: " + t.getMessage(), t);
        commonUtils.sendSystemMessage("Error parsing JMS message", t);
      }
      
      return accountManagementTaskVO;
  }

  


	private void executeTaskProcessFSAutoRenew(Connection conn, AccountManagementTaskVO taskVO) throws Exception 
	{
		FSMembershipVO vo = taskVO.getFsMembershipVO();
		try {
			logger.debug("fsAutoRenewBO.processFSAutoRenew("+vo.getEmailAddress()+")");
			fsAutoRenewBO.processFSAutoRenew(conn, vo.getEmailAddress());
			} catch(CustProfileTimeoutException timeout){
			throw timeout;
		} catch (Exception e) {
			fsAutoRenewBO.deleteFSProcessingEmail(vo.getEmailAddress());
			logger.error("Error proccessing FS-AutoRenew for Email(" +vo.getEmailAddress()+") :"+ e.getMessage(), e);
		    commonUtils.sendSystemMessage("Error proccessing FS-AutoRenew for Email(" +vo.getEmailAddress()+").", e);
		    throw e;
		}		
	}

	private void executeTaskSendFSUpdates(Connection conn, AccountManagementTaskVO taskVO) throws Exception 
	{
		FSMembershipVO vo = taskVO.getFsMembershipVO();		
		String errorMsg = "Error in Sending FS Service Duration Updates for email("+vo.getEmailAddress()+")";
		try {
			logger.debug("fsAutoRenewBO.sendFSUpdateInfo("+vo.getEmailAddress()+")");
			FTDCAMSUtils.sendFSUpdateInfo(vo.getEmailAddress(), vo.getDuration(), vo.getExternalOrderNumber());
		} catch(CustProfileTimeoutException timeout) {
			throw timeout;
		}catch (Exception e) {
			logger.error(errorMsg + e.getMessage(), e);
		    commonUtils.sendSystemMessage(errorMsg, e);
		    throw e;
		}
	}
}
