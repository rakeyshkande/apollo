package com.ftd.osp.account.mdb;



import java.util.Calendar;
import java.util.Date;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.osp.account.bo.FSAutoRenewBO;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class AccountManagementMDB implements MessageDrivenBean, MessageListener 
{
	private static final long serialVersionUID = 1L;

	private static Logger logger = new Logger(AccountManagementMDB.class.getName());
	MessageDrivenContext messageDrivenContext;
	CommonUtils commonUtils = new CommonUtils();
	private static final String DEFAULT_PAYLOAD = "AUTORENEW";
	private static final Integer FS_AUTO_RENEW_DEFAULT_VALUE = 0;
    private static final Integer MAX_AUTO_RENEW_DEFAULT_RANGE = 30;
	
	@Override
	public void ejbRemove() throws EJBException {
		
	}
	public void ejbCreate()
	  {
		
	  }
	@Override
	public void setMessageDrivenContext(MessageDrivenContext arg0) throws EJBException {
		this.messageDrivenContext = arg0;
	}

	@Override
	public void onMessage(Message message) 
	{
		logger.info("***onMessage() of AccountManagementMDB*******"+message);
		TextMessage textMessage = (TextMessage) message;
		String payload = "";
		try 
		{
			FSAutoRenewBO fsAutoRenewBO = new FSAutoRenewBO();
			payload = textMessage.getText();
			logger.debug("Payload :"+payload);
			if(payload != null && payload.trim().length()!=0){
				payload = payload.trim();
			}else {
				logger.error("Empty payload received.");
				return;
			}
			
			//If payload is equal to default payload as in table, then evaluate start and end dates
			if(DEFAULT_PAYLOAD.equals(payload))
			{
				Date startDate = null;
				Date endDate = new Date();
	            logger.info("-------------------"+endDate);
	            startDate = getStartDate(endDate);
	            logger.info("-------------------"+startDate);
				fsAutoRenewBO.processAutoRenew(startDate, endDate);
			} else 
			{
				fsAutoRenewBO.reprocessAutoRenew(payload);
			}
		 }
		catch(CustProfileTimeoutException timeout){
			     logger.error("Error proccessing JMS message: " + timeout.getMessage(), timeout);
			     commonUtils.sendSystemMessage("CustProfileTimeoutException for the Payload "+ payload, timeout ,"NOPAGE CustProfileTimeout Error .");
			     throw timeout; // Rethrow so the Message is retried
		}
		catch(Exception t) 
		{
			logger.error("Error proccessing JMS message: " + t.getMessage(), t);
		    commonUtils.sendSystemMessage("Error proccessing JMS message", t);
		    throw new RuntimeException(t.getMessage(), t);
		}
	}
	
	private static Date getStartDate(Date endDate) throws Exception{
        //set MAX_AUTO_RENEW_DATE_RANGE to 30 days in GlobalParam configuration
		//And Adjust FS_AUTO_RENEW_DATE_RANGE between 1 to 30.
        
        Calendar cal = Calendar.getInstance();
        Date startDate= null;
        cal.setTime(endDate);
        int fsAutoRenewDateRange  = FS_AUTO_RENEW_DEFAULT_VALUE;
        int maxAutoRenewDateRange = MAX_AUTO_RENEW_DEFAULT_RANGE;
        //This Default configuration will run the Job for currentDay.
        try{
             int fsDateRange       = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICES_FREESHIPPING","FS_AUTO_RENEW_DATE_RANGE")); 
             fsAutoRenewDateRange  = fsDateRange > fsAutoRenewDateRange ? fsDateRange :fsAutoRenewDateRange;
             maxAutoRenewDateRange = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICES_FREESHIPPING","MAX_AUTO_RENEW_DATE_RANGE"));
            }
        catch(Exception e){
               logger.error("Error while parsing the global param's value.Running AUTORENEW job for today only. fsAutoRenewDateRange: "+fsAutoRenewDateRange+" MAX_AUTO_RENEW_DATE_RANGE:"+maxAutoRenewDateRange);
           }
         if(fsAutoRenewDateRange >=1 && fsAutoRenewDateRange <=maxAutoRenewDateRange)
         {
            cal.add(Calendar.DATE, -(fsAutoRenewDateRange));
            startDate = cal.getTime();
         }
        else{
            startDate = endDate;
        }
        logger.debug("Running job at startDate: "+startDate+" endDate : "+endDate);
        logger.debug("fsAutoRenewDateRange :"+fsAutoRenewDateRange+" MAX_AUTO_RENEW_DATE_RANGE : "+maxAutoRenewDateRange);
        return startDate;
}

}
