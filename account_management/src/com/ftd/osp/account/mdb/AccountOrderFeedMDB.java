package com.ftd.osp.account.mdb;

import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.osp.account.dao.AccountOrderFeedDAO;
import com.ftd.osp.account.vo.AccountOrderFeedVO;
import com.ftd.osp.account.feed.bo.AccountOrderFeedBO;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;
import java.util.Date;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;


/**
 * Processes inbound Account Order Feed Messages from the JMS:ACCOUNT_ORDER_FEED
 * This bean is configured for Container Managed Transactions
 */
public class AccountOrderFeedMDB
  implements MessageDrivenBean, MessageListener
{
	public static final String JMS_RETRY_DELAY_IN_SEC  = "15";
	public static final String KEY_ORDER_DETAIL        = "UPDATE_OD";
	public static final String KEY_ORDER_GUID          = "UPDATE_OG";
	public static final String KEY_REMOVE_ORDER_DETAIL = "REMOVE_OD";
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = new Logger(AccountOrderFeedMDB.class.getName());

    MessageDrivenContext messageDrivenContext;

  /**
   * Common Application Utilities
   */
  CommonUtils commonUtils = new CommonUtils();
    
  /**
   * ejbCreate
   */
  public void ejbCreate()
  {
  }

  /**
   * MDB Constructor.
   */
  public AccountOrderFeedMDB()
  {
  }

  /**
   * Called when the Bean is removed.
   * @see javax.ejb.MessageDrivenBean
   */
  public void ejbRemove()
  {
  }

  /**
   * Registers the message driven context with the bean
   * @param messageDrivenContext
   * @see javax.ejb.MessageDrivenBean
   */
  public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext)
  {
    this.messageDrivenContext = messageDrivenContext;
  }


  /**
   * Handle the inbound message. Expects a TextMessage containing the order 
   * that should be fed to CAMS.
   * @param message
   */
  public void onMessage(Message message)
  {
    Connection connection = null;
    long enqueueTimestampSec;
    Date enqueueTimestamp = null;
    String orderId = null;
    String orderKey = null;
    int orderAttemptCount = 0;
    String orderGuid = null;
    AccountOrderFeedVO aofvo = null;   
    String camsEnabled = null;
    AccountOrderFeedDAO fdao = null;
    TextMessage textMessage = null;
    String payLoad = null;

    try {
    	logger.info("AccountOrderFeedMDB:::"+message);
      textMessage = (TextMessage) message;
      payLoad = textMessage.getText();
      String messageTokens[] = parseOrderKey(textMessage);
      if (messageTokens == null) {
          return;  // There were problems parsing, so just bail (we don't want to re-enqueue JMS)
      }
      orderId = messageTokens[0];
      orderKey = messageTokens[1];
      orderAttemptCount = Integer.parseInt(messageTokens[2]);
      
      connection = commonUtils.getConnection();
      fdao = new AccountOrderFeedDAO();
      camsEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE","CAMS_FEED_ENABLED_FLAG");
      logger.info(orderKey+"  "+orderId+" "+orderAttemptCount);
      if (KEY_REMOVE_ORDER_DETAIL.equals(orderKey)) {
    	  
    	  // Order was removed, so send removal feed to CAMS
    	  //
		  logger.debug("Removal feed");
		  if ("Y".equalsIgnoreCase(camsEnabled)) {
			  AccountOrderFeedBO feedBo = new AccountOrderFeedBO();
			  feedBo.sendRemovalFeed(orderId);
		  } else {
    		  // CAMS feed is disabled, so log system message			  
		      logger.info("CAMS feed disabled, so order removal feed not sent: " + orderId);
		      commonUtils.sendSystemMessage("CAMS feed disabled, so order removal feed not sent: " + orderId,"");
		  }
    	  
      } else {
    	  
    	  // Order was updated in some way, so determine if order feed to CAMS is necessary
    	  //
    	  enqueueTimestampSec = message.getJMSTimestamp();
          enqueueTimestamp = new Date(enqueueTimestampSec);

    	  orderGuid = fdao.isOrderFeedNeeded(connection, orderId, orderKey, enqueueTimestamp);
    	  if (orderGuid != null) {
    		  
    		  // Order feed is necessary, so get order info and send to CAMS.
    		  //
    		  if (logger.isDebugEnabled()) {
    			  logger.debug("Need order feed for: " + orderId + "," + orderGuid);
    		  }
    		  if ("Y".equalsIgnoreCase(camsEnabled)) {
    		      // Get order info
    			  aofvo = fdao.lockAndLoadOrderFeed(connection, orderGuid);

    			  // Send feed
    			  if (aofvo != null) {
        			  AccountOrderFeedBO feedBo = new AccountOrderFeedBO();
        			  feedBo.sendOrderFeed(aofvo);
                      // Set status in order feed table to reflect success 
                      fdao.doneWithOrderFeedAttempt(connection, true, orderGuid);                 
    			  } else {
                      // Set status in order feed table to reflect nothing sent 
                      fdao.doneWithOrderFeedAttempt(connection, false, orderGuid);                     			      
    			  }

    		  } else {
        		  // CAMS feed is disabled, so simply set status in order feed table
    			  // to reflect order is out-of-sync with CAMS.
        		  fdao.doneWithOrderFeedAttempt(connection, false, orderGuid);    			      			  
    		  }
    		  
    	  } else {
    		  if (logger.isDebugEnabled()) {logger.debug("Feed not needed");}
    	  }
      }

    // Something bad happened
    //
    }catch (CustProfileTimeoutException timeoutEx){
        logger.error("Error proccessing JMS message: " + timeoutEx.getMessage(), timeoutEx);
        commonUtils.sendSystemMessage("CustProfileTimeoutException for the Payload "+ payLoad, timeoutEx ,"NOPAGE CustProfileTimeout Error having Order_Guid = "+orderId);
        throw timeoutEx; // Rethrow so the Message is retried
       }catch (Throwable t) {
      logger.error("Error while processing CAMS order feed message: " + t.getMessage(), t);
      
      // Retry by re-enqueueing JMS message.
      // Note we re-enqueue instead of just rolling back since we need to commit changes to the 
      // order_feed table regardless if the feed succeeds or not (i.e., the status needs to reflect
      // any errors).
      if (!resendJms(orderId, orderKey, orderAttemptCount)) {
    	  commonUtils.sendSystemMessage("Error processing CAMS order feed message: '" + textMessage + "' - ", t);
      }
      
    } finally {
      commonUtils.closeConnection(connection);
    }
  }

  
  /**
   * Extracts order key, key type, and attempt count from JMS message.
   * Each should be separated by commas. e.g.:
   *   12345,UPDATE_OD,1
   * Parsing exceptions are not rethrown since there is no point to retrying 
   * a JMS Message with an invalid format. 
   * @param textMessage
   * @return
   */
  private String[] parseOrderKey(TextMessage textMessage)
    throws JMSException
  {
      String msgData;
      String orderKey[] = null;
      
      msgData = textMessage.getText();
	  if (logger.isDebugEnabled()) {
		  logger.debug("Received Order Feed Message: " + msgData);
	  }

	  try {
        orderKey = msgData.split(",");
        if (orderKey == null || 
        	(!orderKey[1].equals(KEY_ORDER_GUID) &&
        	 !orderKey[1].equals(KEY_ORDER_DETAIL) &&
        	 !orderKey[1].equals(KEY_REMOVE_ORDER_DETAIL)
        	) || orderKey.length != 3) {
        	throw new Exception("parseOrderKey error");
        }
      } catch (Throwable t) {
    	orderKey = null;
        logger.error("Error parsing order feed JMS message: '" + msgData + "' " + t.getMessage(), t);
        commonUtils.sendSystemMessage("Error parsing order feed JMS message: '" + msgData + "' ", t);
      }
      
      return orderKey;
  }


  /**
   * Re-enqueues JMS for order (if within max attempt count).
   * 
   * @param textMessage
   * @return
   */
  private boolean resendJms(String curOrderId, String curOrderKey, int curOrderAttemptCount) 
  {
	  boolean jmsResentFlag = false;
	  
	  try {
		  int maxAttempts = Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE","CAMS_MAX_ATTEMPTS"));
		  if (curOrderAttemptCount < maxAttempts) {
			  
			  // Construct and re-enqueue JMS message to ACCOUNT_ORDER_FEED since we are below max attempt count
			  //
			  String msg = curOrderId + "," + curOrderKey + "," + (curOrderAttemptCount + 1);
              MessageToken msgToken = new MessageToken();
              msgToken.setStatus("ACCOUNT ORDER FEED");  // Set status to the dispatcher.xml pipeline attribute value 
              msgToken.setMessage(msg);
              msgToken.setJMSCorrelationID(msg);
              msgToken.setProperty("JMS_OracleDelay", JMS_RETRY_DELAY_IN_SEC, "long");                
     
              Dispatcher dispatcher = Dispatcher.getInstance();
              dispatcher.dispatchTextMessage(new InitialContext(), msgToken);
			  jmsResentFlag = true;
		  } else {
			  logger.error("Maximum unsuccessful attempts for sending order info to CAMS was exceeded");
			  jmsResentFlag = false;
		  }
	  } catch (Throwable t) {
        logger.error("Error attempting to re-enqueue account_order_feed for: '" + curOrderId + "' " + t.getMessage(), t);
        commonUtils.sendSystemMessage("Error attempting to re-enqueue account_order_feed for: '" + curOrderId + "' ", t);
	  }
	  return jmsResentFlag;
  }
}
