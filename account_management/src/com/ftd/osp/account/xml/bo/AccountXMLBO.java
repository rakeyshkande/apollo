package com.ftd.osp.account.xml.bo;

import com.ftd.osp.account.vo.AccountProgramVO;
import com.ftd.osp.account.vo.FSMembershipVO;
import com.ftd.osp.account.xml.vo.AccountManagementTaskVO;
import com.ftd.osp.account.xml.vo.AccountVO;
import com.ftd.osp.account.xml.vo.IdentityVO;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.io.StringWriter;
import java.io.Writer;

/**
 * Provides methods to convert Account Domain Objects from/to XML
 * The implementation utilizes the XStream Library
 */
public class AccountXMLBO
{

  /**
   * Size of generated XML for optimizing XML writers
   */
  private static final int XML_SIZE_HINT = 8096;

  /**
   * Does commmon XStream configuration for the AccountManagementTask XML
   * @param xstream
   */
  private void configureXStreamForAccountManagementTaskXML(XStream xstream)
  {
    // Add Mappings for the account task
    xstream.alias("accountTask", AccountManagementTaskVO.class);
    xstream.aliasAttribute(AccountManagementTaskVO.class, "taskType", "type");
    xstream.alias("fsMembershipVO", FSMembershipVO.class);
  }

  /**
   * @return Returns an instance of Xstream for parsing the AccountManagementTask XML
   */
  private XStream getXStreamForParsingAccountTask()
  {
    XStream xstream = new XStream()
      {
        /*
         * Custom mapper to ignore Unexpected Elements in the XML
         * See CustomMapperTest.testCanBeUsedToOmitUnexpectedElements in XStream
         */

        protected MapperWrapper wrapMapper(MapperWrapper next)
        {
          return new MapperWrapper(next)
            {
              public boolean shouldSerializeMember(Class definedIn, String fieldName)
              {

                boolean retVal = definedIn != Object.class? super.shouldSerializeMember(definedIn, fieldName): false;

                if (!retVal)
                {
                  try
                  {
                    // This is an unexpected element, do we know the class for it? - This is to handle the implicit collection for Account
                    Class realClazz = realClass(fieldName);
                    if (realClazz != null)
                    {
                      retVal = true;
                    }
                  }
                  catch (Exception e)
                  {
                  }
                }
                return retVal;
              }
            };
        }
      };

    configureXStreamForAccountManagementTaskXML(xstream);
    return xstream;
  }

  /**
   * @return Returns an instance of Xstream for generating the AccountManagementTask XML with CDATA wrapping the fields
   */
  private XStream getXStreamForGeneratingAccountTaskXML()
  {
    XStream xstream = new XStream(new XppDriver()
        {
          public HierarchicalStreamWriter createWriter(Writer out)
          {
            return new CompactWriter(out)
              {
                protected void writeText(QuickWriter writer, String text)
                {
                  if(text != null && text.length() > 0)
                  {
                    writer.write("<![CDATA[");
                    writer.write(text);
                    writer.write("]]>");
                  } else
                  {
                    writer.write(text);
                  }
                }
              };
          }
        });

    configureXStreamForAccountManagementTaskXML(xstream);
    xstream.registerConverter(new DateConverter("yyyy-MM-dd HH:mm:ss", new String[0]));
    
    return xstream;
  }

  /**
   * Parses the passed in xml for an Account Management Task
   * @param xml
   * @return A valid AccountManagementTaskVO instance
   * @throws RuntimeException on an errors parsing the XML
   */
  public AccountManagementTaskVO parseAccountManagementTaskFromXML(String xml)
  {
    XStream xstream = getXStreamForParsingAccountTask();

    try
    {
      AccountManagementTaskVO retVal = (AccountManagementTaskVO) xstream.fromXML(xml);
      return retVal;
    }
    catch (Throwable t)
    {
      throw new RuntimeException("Failed to parse xml: " + t.toString(), t);
    }
  }


  /**
   * Converts the passed in Account Management Task VO to XML
   * @param accountManagementTaskVO The VO to convert
   * @return
   */
  public String convertAccountManagementTaskToXML(AccountManagementTaskVO accountManagementTaskVO)
  {
    XStream xstream = getXStreamForGeneratingAccountTaskXML();

    StringWriter xmlDataWriter = new StringWriter(XML_SIZE_HINT);
    xstream.toXML(accountManagementTaskVO, xmlDataWriter);
    
    String xmlData = xmlDataWriter.toString();
    return xmlData;    
  }


  /**
   * Converts the passed in Account Management Task VO to XML for the Account Update 
   * Feed. This will omit internal DB IDs and other fields that are not intended for
   * the feed
   * 
   * @param accountManagementTaskVO The VO to convert
   * @return
   */
  public String convertAccountManagementTaskToFeedAccountUpdateXML(AccountManagementTaskVO accountManagementTaskVO)
  {
    XStream xstream = getXStreamForGeneratingAccountTaskXML();
    
    // Omit Feed specific fields
    xstream.omitField(AccountProgramVO.class, "accountProgramId");

    StringWriter xmlDataWriter = new StringWriter(XML_SIZE_HINT);
    xstream.toXML(accountManagementTaskVO, xmlDataWriter);
    
    String xmlData = xmlDataWriter.toString();
    return xmlData;        
    
  }


}
