package com.ftd.osp.account.xml.vo;

/**
 * The Identity of an account is composed of an email address.
 */
public class IdentityVO
{
  
  /**
   * The email address identifying the account
   */
  private String emailAddress;

  public void setEmailAddress(String emailAddress)
  {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress()
  {
    return emailAddress;
  }
}
