package com.ftd.osp.account.xml.vo;

import com.ftd.osp.account.vo.FSMembershipVO;


/**
 * Represents an Account Management Task
 */
public class AccountManagementTaskVO
{
  /**
   * The type of task to perform
   */
  private String taskType;
   
  private FSMembershipVO fsMembershipVO;

public String getTaskType() {
	return taskType;
}

public void setTaskType(String taskType) {
	this.taskType = taskType;
}

public FSMembershipVO getFsMembershipVO() {
	return fsMembershipVO;
}

public void setFsMembershipVO(FSMembershipVO fsMembershipVO) {
	this.fsMembershipVO = fsMembershipVO;
}


  
  
}
