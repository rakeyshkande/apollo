package com.ftd.osp.account.xml.vo;

/**
 * The details of an Account. An account inherits identity attributes from the IdentityVO
 */
public class AccountVO extends IdentityVO
{
	private String autoRenewFlag;

	public String getAutoRenewFlag() {
		return autoRenewFlag;
	}

	public void setAutoRenewFlag(String autoRenewFlag) {
		this.autoRenewFlag = autoRenewFlag;
	}
}
