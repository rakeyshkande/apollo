package com.ftd.osp.account.web.action;

import com.ftd.osp.account.bo.AccountQueueDispatcherBO;
import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This is a Test Action that submits the passed in XML to the Account JMS queue
 * Expects parameters: accountTask=<XML> contains XML to post to the queue
 */
public class JMSTestAction
  extends Action
{

  protected static Logger logger = new Logger(JMSTestAction.class.getName());
  private int SUCCESS_CODE = 200;
  private int ERROR_CODE = 600;
  private String SUCCESS_MSG = "SUCCESS";
  private String ERROR_MSG = "INTERNAL_ERROR";

  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
    String requestXML = request.getParameter("accountTask");
    logger.info(requestXML);
    if (requestXML == null || requestXML.equals(""))
    {
      String msg = ERROR_MSG + " - Request XML is empty";
      logger.error(msg);
      HttpServletResponseUtil.sendError(response, ERROR_CODE, ERROR_MSG);
      return null;
    }

    try
    {
      AccountQueueDispatcherBO accountMessagingBO = new AccountQueueDispatcherBO();
      accountMessagingBO.addRequestToAccountTaskQueue(requestXML);
      HttpServletResponseUtil.sendError(response, SUCCESS_CODE, SUCCESS_MSG);
    }
    catch (Exception e)
    {

      logger.error(e);
      HttpServletResponseUtil.sendError(response, ERROR_CODE, ERROR_MSG);
    }
    return null;
  }
}
