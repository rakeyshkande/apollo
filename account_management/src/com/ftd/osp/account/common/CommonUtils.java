package com.ftd.osp.account.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;

import com.ftd.customer.client.CCDataRestServiceClient;
import com.ftd.customer.client.CustomerRestServiceClient;
import com.ftd.customer.client.FreeShippingRestServiceClient;
import com.ftd.order.client.OrderRestServiceClient;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;


/**
 * Provides common utility functionality for the application.
 * Note: The methods here should only be invoked by application code (Web, MDB), not BOs.
 * This is because, the BOs may be exposed externally via accountmanagement-api.jar file.
 */
public class CommonUtils
{
  protected static Logger logger = new Logger(CommonUtils.class.getName());
  private static  int timetoStopaMethod=10000;//setting Default Value
  public CommonUtils()
  {
  }

  /**
   * Returns a database connection.
   * @return
   * @throws Exception
   */
  public Connection getConnection()
    throws Exception
  {
    String datasource = 
      ConfigurationUtil.getInstance().getProperty(AccountConstants.PROPERTY_FILE, AccountConstants.DATASOURCE_NAME);
    Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

    return conn;
  }
  
  /**
   * Closes a connection quietly. Does nothing if the connection is null.
   * Handles any close exceptions and logs.
   * @param connection
   */
  public void closeConnection(Connection connection)
  {
    if(connection == null)
    {
      return;
    }
    
    try
    {
      connection.close();
    } catch (Exception e)
    {
      logger.warn("Error closing connection: " + e.getMessage(), e);
    }
  }

  /**
   * Returns a User Transaction Object
   * @return
   */
  public UserTransaction getUserTransaction()
    throws Exception
  {
    InitialContext iContext = new InitialContext();
    String jndi_usertransaction_entry = 
      ConfigurationUtil.getInstance().getProperty(AccountConstants.PROPERTY_FILE, AccountConstants.JNDI_USERTRANSACTION_ENTRY);
    UserTransaction userTransaction = (UserTransaction) iContext.lookup(jndi_usertransaction_entry);

    return userTransaction;
  }

  /**
   * This method sends a message to the System Messenger.
   *
   * @param String message
   * @returns String message id
   */
  public void sendSystemMessage(String message, String type)
  {
	  sendSystemMessage(message, type, null);
  }

  public void sendSystemMessage(String message, String type, String subject)
  {
    try
    {
      //build system vo
      // SystemMessage closes connection passed in. Therefore, create a new connection.
      Connection connection = getConnection();
      SystemMessengerVO sysMessage = new SystemMessengerVO();
      sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
      sysMessage.setSource(AccountConstants.MSG_SOURCE);
      sysMessage.setType(type);
      sysMessage.setMessage(message);
      if(subject != null)
    	  sysMessage.setSubject(subject);
      SystemMessenger.getInstance().send(sysMessage, connection);
    }
    catch (Throwable t)
    {
      logger.error("Sending system message failed. Message=" + message);
      logger.error(t);
    }
  }


  /**
   * Formats the Message and Throwable, and sends it to the System Messenger
   * @param message
   * @param t
   */
  public void sendSystemMessage(String message, Throwable t)
  {
    sendSystemMessage(throwableToString(message + ":", t), "System Exception");
  }
  
  public void sendSystemMessage(String message, Throwable t , String subject)
  {
	  sendSystemMessage(message, "System Exception" , subject);
  }
  

  /**
   * Formats a throwable into a String, usefull for sending to exception to the System Message
   * @param header
   * @param t
   * @return
   */
  public String throwableToString(String header, Throwable t)
  {
    StringBuffer retVal = new StringBuffer(header);
    retVal.append(t.toString());

    if (t.getCause() != null)
    {
      retVal.append(throwableToString(";", t.getCause()));
    }

    String str = retVal.toString();
    str = str.toString().replaceAll("java\\..*Exception", "Exception");
    str = str.toString().replaceAll("com\\..*Exception", "Exception");
    
    return str;
  }

  public static String getCAMSServiceURL()
  {
	try {
		return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "CAMS_SERVICE_URL");
	} catch (Exception e) {
		throw new RuntimeException(e.getMessage());
	}
  }
  
  public static String getCAMSOrderServiceURL()
  {
	try {
		return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "CAMS_SERVICE_ORDER_URL");
	} catch (Exception e) {
		throw new RuntimeException(e.getMessage());
	}
  }
  
  public static String getCAMSUserName()
  {
	  try {
			return ConfigurationUtil.getInstance().getSecureProperty("SERVICE", "CAMS_SERVICE_USER_NAME");
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
  }
  
  public static String getCAMSPassword()
  {
	  try {
			return CommonUtils.getPasswordFromSSHStore("SERVICE", "CAMS_SERVICE_PASSWORD_FILE");
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
  }
  enum ccTypesENUM {Visa, Master, Amex, Discover, Diners, Carte}
  public static String getCreditCardTypeCode(String creditCardType){
	  String returnVal = null;
	  try
	  {
		  if (creditCardType == null)
		  {
			  throw new RuntimeException("Credit Card Type cannot be null ");
		  }
		  
		  if (creditCardType.equalsIgnoreCase(ccTypesENUM.Visa.toString()))
		  {
			  returnVal = "VI";
		  }
		  else if (creditCardType.equalsIgnoreCase(ccTypesENUM.Master.toString()))
		  {
			  returnVal = "MC";
		  }
		  else if (creditCardType.equalsIgnoreCase(ccTypesENUM.Amex.toString()))
		  {
			  returnVal = "AX";
		  }
		  else if (creditCardType.equalsIgnoreCase(ccTypesENUM.Discover.toString()))
		  {
			  returnVal = "DI";
		  }
		  else if (creditCardType.equalsIgnoreCase(ccTypesENUM.Diners.toString()))
		  {
			  returnVal = "DC";
		  }
		  else if (creditCardType.equalsIgnoreCase(ccTypesENUM.Carte.toString()))
		  {
			  returnVal = "CB";
		  }
		  return returnVal;
		  
	  }
	  catch(Exception e)
	  {
		  throw new RuntimeException(e.getMessage());
	  }
  }

  enum ccTypeCodesENUM {VI, MC, AX, DI, DC, CB}
  public static String getCreditCardTypeFromCode(String creditCardCode){
      String returnVal = creditCardCode;
      try
      {
          if (creditCardCode.equalsIgnoreCase(ccTypeCodesENUM.VI.toString()))
          {
              returnVal = "Visa";
          }
          else if (creditCardCode.equalsIgnoreCase(ccTypeCodesENUM.MC.toString()))
          {
              returnVal = "Master";
          }
          else if (creditCardCode.equalsIgnoreCase(ccTypeCodesENUM.AX.toString()))
          {
              returnVal = "Amex";
          }
          else if (creditCardCode.equalsIgnoreCase(ccTypeCodesENUM.DI.toString()))
          {
              returnVal = "Discover";
          }
          else if (creditCardCode.equalsIgnoreCase(ccTypeCodesENUM.DC.toString()))
          {
              returnVal = "Diners";
          }
          else if (creditCardCode.equalsIgnoreCase(ccTypeCodesENUM.CB.toString()))
          {
              returnVal = "Carte";
          }
          return returnVal;
          
      }
      catch(Exception e)
      {
          throw new RuntimeException(e.getMessage());
      }
  }

  enum addressTypeCodeENUM {R, B, F, C, H, O}
  public static String getAddressTypeCode(String addressType){
	  String returnVal = null;
	  if ("Residence".equalsIgnoreCase(addressType) || "Home".equalsIgnoreCase(addressType))
	  {
		  returnVal = addressTypeCodeENUM.R.toString();
	  }
	  else if("BUSINESS".equalsIgnoreCase(addressType))
	  {
		  returnVal = addressTypeCodeENUM.B.toString();
	  }
	  else if("CEMETERY".equalsIgnoreCase(addressType))
	  {
		  returnVal = addressTypeCodeENUM.C.toString();
	  }
	  else if("FUNERAL HOME".equalsIgnoreCase(addressType))
	  {
		  returnVal = addressTypeCodeENUM.F.toString();
	  }
	  else if("HOSPITAL".equalsIgnoreCase(addressType) || "NURSING HOME".equalsIgnoreCase(addressType))
	  {
		  returnVal = addressTypeCodeENUM.H.toString();
	  }
	  else
	  {
		  returnVal = addressTypeCodeENUM.O.toString();
	  }
	  return returnVal;
  }
  
  enum paymentTypeCodesENUM {GD, GC, IN, PP, BM, UA, NC}
  public static String getPaymentTypeFromCode(String payCode){
      String returnVal = payCode;
      // We have this method since HYD has their own payment types
      // (although many now match ours)
      try
      {
          if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.GD.toString()))
          {
              returnVal = "GD";
          }
          else if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.GC.toString()))
          {
              returnVal = "GC";
          }
          else if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.IN.toString()))
          {
              returnVal = "IN";
          }
          else if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.PP.toString()))
          {
              returnVal = "Paypal";
          }
          else if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.BM.toString()))
          {
              returnVal = "BML";
          }
          else if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.UA.toString()))
          {
              returnVal = "UA";
          }
          else if (payCode.equalsIgnoreCase(paymentTypeCodesENUM.NC.toString()))
          {
              returnVal = "NC";
          }
         else 
          {
              // Else assume credit card
              returnVal = "CC";
          }
          return returnVal;
          
      }
      catch(Exception e)
      {
          throw new RuntimeException(e.getMessage());
      }
  }
  
  public static CustomerRestServiceClient getCustomerRestServiceClient() {
		return new CustomerRestServiceClient(getCAMSServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	public static FreeShippingRestServiceClient getFreeShippingRestServiceClient() {
		return new FreeShippingRestServiceClient(getCAMSServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	public static CCDataRestServiceClient getCCDataRestServiceClient() {
		return new CCDataRestServiceClient(getCAMSServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	public static OrderRestServiceClient getOrderRestServiceClient(){
		return new OrderRestServiceClient(getCAMSOrderServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	public static Date getDate(int year, int month, int date) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DATE, date);
		return calendar.getTime();
	}
	
	/**
	 * This method is used to get the file name from secured config and read the file and get the stored password for given context and name.
	 * @param context
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static String getPasswordFromSSHStore(String context, String name) throws Exception
	{
		String passwordString = null;
		BufferedReader br = null;
		try
		{
			String pwdFile= ConfigurationUtil.getInstance().getSecureProperty(context, name);
			
			if (pwdFile != null){
				br = new BufferedReader(new FileReader(new File(pwdFile)));
				 
				passwordString = br.readLine();
			}
			
		}catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			throw e;
		}finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
		return passwordString;
	}
	
	public static Boolean isCreditCardExpired(String expirationYear, String expirationMonth ) throws Exception
	{
		boolean ret_val = false;
		Date currentDate = new Date();
		Date expirationDate;
		try{
			int expYear = Integer.parseInt(expirationYear);
			int expMonth = Integer.parseInt(expirationMonth);
			expYear = expYear + 2000;
			expMonth = expMonth - 1;
			Calendar expCal = Calendar.getInstance();
		    expCal.set(Calendar.MONTH,expMonth);
		    expCal.set(Calendar.YEAR,expYear);
			int lastDate = expCal.getActualMaximum(Calendar.DATE);
			expCal.set(Calendar.DATE,lastDate);
			expirationDate = expCal.getTime();
			if (expirationDate.before(currentDate))
			{
				ret_val=true;
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		return ret_val;
	}
	public static int getCamsResponseTimoutValue()
	{
		int localtimetoStopaMethod=timetoStopaMethod;
		try 
		{
			String socketTimeout = ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE","CAMS_WS_SOCKET_TIMEOUT");
			localtimetoStopaMethod=Integer.parseInt(socketTimeout);
		}
		catch(Exception e)
		{
			localtimetoStopaMethod=timetoStopaMethod;
			e.printStackTrace();
			
		} 
		return localtimetoStopaMethod;
	}
}
