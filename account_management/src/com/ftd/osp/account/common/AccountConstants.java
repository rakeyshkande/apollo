package com.ftd.osp.account.common;

/**
 * Common Constants for the application.
 */
public interface AccountConstants
{
    public final static String PROPERTY_FILE   = "accountmanagement_config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    public static final String JNDI_USERTRANSACTION_ENTRY                   = "jndi_usertransaction_entry";

    /**
     * Message Source for System Messages
     */
    public static final String MSG_SOURCE = "ACCOUNT MANAGEMENT";
    
    
    /**
     * Task Type for transmitting updated Account Information
     */
    public static final String TASK_TYPE_ACCOUNT_INFO_UPDATED = "accountInfo";
    
    
    /**
     * Account Task Type for Feeding Account Information to the Web
     */
    public static final String TASK_TYPE_FEED_PROGRAM_INFO_WEB = "feedProgramInfoToWeb";
    
    /**
     * Account Task Type for updating CAMS
     */
    public static final String TASK_TYPE_MEMBERSHIP_STATUS_UPDATE = "FS_MEMBERSHIP_STATUS_UPDATE";
	public static final String TASK_TYPE_FS_AUTORENEW_UPDATE = "FS_AUTORENEW_UPDATE";
    public static final String TASK_TYPE_FS_PROCESSING_EMAIL = "FS_PROCESSING_EMAIL";
    public static final String TASK_TYPE_FULLY_REFUND_ORDER_UPDATE = "FULLY_REFUND_ORDER_UPDATE";
    public static final String TASK_TYPE_CANCEL_ORDER_UPDATE = "CANCEL_ORDER_UPDATE";

}
