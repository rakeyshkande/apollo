package com.ftd.osp.account.common;

import com.ftd.op.order.bo.OrderAPIBO;


/**
* This locator class provides access to external or dependenct services.
* The class is a singleton.
*/
public class MessagingServiceLocator
{

  private static MessagingServiceLocator messagingServiceLocator;
 
  private OrderAPIBO orderAPIBO;

  /**
  * Private constructor to enforce singleton. 
  * 
  * @see #getInstance()
  */
  private MessagingServiceLocator() throws Exception
  {
     orderAPIBO = new OrderAPIBO();
  }

  /**
  * Initializes and returns a reference to the Locator singleton instance.
  */
  public static MessagingServiceLocator getInstance() throws Exception
  {
    if(messagingServiceLocator==null)
    {
      messagingServiceLocator=new MessagingServiceLocator();
    }
    return messagingServiceLocator;
  }

  /**
  * @return Reference to the Order API
  */
  public  OrderAPIBO getOrderAPI() throws Exception
  {
      return orderAPIBO;
  }
}