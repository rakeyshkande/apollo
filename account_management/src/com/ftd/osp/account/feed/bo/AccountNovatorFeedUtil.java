package com.ftd.osp.account.feed.bo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

/**
 * Extends the NovatorFeedUtil to handle Account Specific Functionality
 */
public class AccountNovatorFeedUtil
  extends NovatorFeedUtil
{
  private static Logger logger = new Logger(AccountNovatorFeedUtil.class.getName());

  /**
     * Parses and evaluates the feed response from Novator.
     *
     * Response Format:
     * Success:  <?xml version="1.0" encoding="UTF-8"?><accountTask_response><status_code>SUCCESS</status_code></accountTask_response>CON(content)
     * Error: <?xml version="1.0" encoding="UTF-8"?><accountTask_response><status_code>INVALID_ACCOUNT</status_code><error_message>Account doesn't exists in Website</error_message></accountTask_response>CON
     * @param xmlResponse
     *
     * @return Interpreted response information
     * @throws Exception
     */
  protected NovatorFeedResponseVO parseResponse(String xmlResponse)
    throws Exception
  {
    String originalXMLResponse = xmlResponse;
    NovatorFeedResponseVO responseVO = new NovatorFeedResponseVO();

    if (logger.isDebugEnabled())
      logger.debug("Novator response: " + xmlResponse);

    // Make sure this is an accountTask response
    if (!xmlResponse.contains("</accountTask_response"))
    {
      // This is an invalid response
      responseVO.setSuccess(false);
      responseVO.setErrorString("Invalid response, expected <accountTask_response> tag in response: " + originalXMLResponse);
      return responseVO;
    }

    // Truncate the string to the end of the accountTask response (Since there are multiple flavors of CON)
    xmlResponse = truncateResponse(xmlResponse);

    try
    {
      Document doc = JAXPUtil.parseDocument(xmlResponse);

      // Attempt to obtain update node
      Element statusCodeElement = JAXPUtil.selectSingleNode(doc, "//status_code");

      if (statusCodeElement == null)
      {
        if (logger.isDebugEnabled())
          logger.debug("No status_code element exists within the response.");

        // No validation error found...don't know what this message means
        responseVO.setSuccess(false);
        responseVO.setErrorString("Missing status_code tag in Novator response: " + originalXMLResponse);
      }
      else
      {
        // Obtain the status of the update
        String statusCode = JAXPUtil.getTextValue(statusCodeElement);
        
        if (statusCode.equalsIgnoreCase("SUCCESS"))
        {
          responseVO.setSuccess(true);
        }
        else
        {
          responseVO.setSuccess(false);
         
          String errorMessage = "";
          Element errorMessageElement = JAXPUtil.selectSingleNode(doc, "//error_message"); 
          
          if(errorMessageElement != null)
          {
            errorMessage = ": " + JAXPUtil.getTextValue(errorMessageElement);
          }
          responseVO.setErrorString("Novator feed was unsuccessful: " + statusCode + errorMessage);
        }
      }
    }
    catch (Throwable t)
    {
      // Could not parse Novator response or an error occurred, set Exception so we retry        
      // Could have received a truncated/mangled response etc.
      logger.warn(t);
      responseVO.setSuccess(false);
      responseVO.setErrorString("Novator feed error.  " + t.getMessage());
      responseVO.setIsTransmitException(true);
      responseVO.setTransmitThrowable(t);
    }

    return responseVO;


  }

  /**
   * Ensures the XML ends with the </accountTask_response> end tag, and strips out everything after
   * @param xmlString
   * @return
   */
  protected String truncateResponse(String xmlString)
  {
    int endIndex = xmlString.lastIndexOf("</accountTask_response");
    
    if(endIndex < 0)
    {
      return xmlString; 
    }
    
    endIndex = xmlString.indexOf(">", endIndex);
    
    if(endIndex < 0)
    {
      return xmlString; 
    }
    
    return xmlString.substring(0, endIndex + 1);
  }
}
