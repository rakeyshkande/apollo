package com.ftd.osp.account.feed.bo;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.order.client.OrderRestServiceClient;
import com.ftd.order.rest.core.request.OrderServiceRequest;
import com.ftd.order.rest.core.response.GenericResponse;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.account.vo.AccountOrderFeedVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.util.validation.ErrorMessage;
import com.ftd.util.validation.ErrorMessages;


/**
 * This BO handles communication with the CAMS service.
 */
public class AccountOrderFeedBO
{
	private static String camserrmsg="TimeoutException For CAMS";
    private static Logger logger = new Logger(AccountOrderFeedBO.class.getName());
  
  public AccountOrderFeedBO()
  {
  }

  /**
   * Send order removal feed data to CAMS
   * 
   * @param aofvo
   * @throws Exception
   */
  public void sendRemovalFeed(String orderDetailId) throws Exception {
	logger.debug("Sending removal feed to CAMS");  
  }

  /**
   * Send order feed data to CAMS
   * l
   * @param aofvo
   * @throws Exception
   */
  public void sendOrderFeed(AccountOrderFeedVO aofvo) throws Exception {
	  logger.debug("Sending order feed to CAMS");  	  
      
      
      if (aofvo != null) {
          OrderRestServiceClient client = CommonUtils.getOrderRestServiceClient();
          OrderServiceRequest req = new OrderServiceRequest();
          req.setOrderInfo(aofvo.getCamsOrderInfo());
          GenericResponse<Integer> resp = null;
          if(FTDCAMSUtils.CAMS_ENABLE_FLAG.equalsIgnoreCase(FTDCAMSUtils.getCAMSTimeoutEnabledFlag()))
        	  resp = this.updateOrderWithTimeoutException(client,req);
          else
        	  resp = client.updateOrder(req);

          ErrorMessages errors = resp.getErrors();
          List<ErrorMessage> errMsgs = errors.getErrors();
          for (ErrorMessage errMsg : errMsgs) {
              if (logger.isDebugEnabled()) {
                  logger.debug("CAMS feed response: " + errMsg.getCode() + " - " + errMsg.getMessage());
              }
              // ???
          }
      }
  }
  private  GenericResponse<Integer> updateOrderWithTimeoutException(final OrderRestServiceClient client,final  OrderServiceRequest req) throws InterruptedException, ExecutionException
	{
	    GenericResponse<Integer> fsmListResp = null;
		logger.debug("CAMS_WS_SOCKET_TIMEOUT is "+CommonUtils.getCamsResponseTimoutValue());
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<Integer>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<Integer>>() 
			    {
				   public GenericResponse<Integer> call() 
				      {  
					     return client.updateOrder(req);
					  }
				});
		try{
			fsmListResp= (GenericResponse<Integer>)future.get(CommonUtils.getCamsResponseTimoutValue(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("updateOrderWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(camserrmsg);
	       }
		finally
		{
			 future.cancel(true);
		}
		return fsmListResp;
	}
  
}
