package com.ftd.osp.account.feed.bo;

import java.sql.Connection;

import com.ftd.osp.account.xml.bo.AccountXMLBO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataPreTransformedXmlVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This BO implements the various Feeds to the Web Site.
 * The class is abstract as it needs to send System Messages. Override and implement the
 * {@link #sendSystemMessage(String message, String status)} method.
 * 
 */
public abstract class AccountFeedBO
{
  private static Logger logger = new Logger(AccountFeedBO.class.getName());

  /**
   * The DAO for accessing the database.
   */
  //private AccountDAO accountDAO;
 
  /**
   * BO utilized for generating XML
   */
  private AccountXMLBO accountXMLBO;
  
  
  /*public AccountFeedBO()
  {
    accountDAO = new AccountDAO();
    accountXMLBO = new AccountXMLBO();
  }*/
  
  /**
   * Retrieve the account information from the Database for the programs in the program Ids list. (More than 1 Account may be returned if multiple
   * program IDs are provided in the list).
   * Feeds each Account retrieved to the Web Site. 
   * 
   * @param connection
   * @param programIds
   * @throws RuntimeException On any errors during the feed
   */
  /*public void feedProgramInfo(Connection connection, List<Long> programIds)
  {
    List<AccountMasterVO> accountsToFeed = getAccountsProgramsForFeed(connection, programIds);
    
    for(AccountMasterVO accountMasterVO: accountsToFeed)
    {
      logger.debug("Sending Program Update for Account: " + accountMasterVO.getAccountMasterId());
      // Build the Feed XML and send to the Feed
      AccountManagementTaskVO accountManagementTaskVO = new AccountManagementTaskVO();
      accountManagementTaskVO.setTaskType(AccountConstants.TASK_TYPE_ACCOUNT_INFO_UPDATED);
      accountManagementTaskVO.setIdentity(new IdentityVO());
      accountManagementTaskVO.getIdentity().setEmailAddress(accountMasterVO.getActiveEmailAddress());
      accountManagementTaskVO.setPrograms(accountMasterVO.getAccountPrograms());
      
      String feedXML = accountXMLBO.convertAccountManagementTaskToFeedAccountUpdateXML(accountManagementTaskVO);
      sendFeedXMLToWebSite(connection, feedXML);
    }
  }*/
  
  /**
   * 
   * @param connection
   * @param programIds
   * @return
   */
  /*private List<AccountMasterVO> getAccountsProgramsForFeed(Connection connection, List<Long> programIds)
  {
    List<AccountMasterVO> accountMasterVOList = new ArrayList<AccountMasterVO>();
    Map<Long, AccountMasterVO> accountIdToAccountMasterMap = new HashMap<Long, AccountMasterVO>();
    
    for(Long programId: programIds)
    {
      if(programId == null)
      {
        continue;
      }
      
      AccountMasterVO accountMasterVO  = accountDAO.getAccountByProgramId(connection, programId);
      
      if(accountMasterVO == null || !accountMasterVO.isAccountActive())
      {
        logger.warn("No active account to feed for program Id: " + programId);
        sendSystemMessage("No active account to feed for program Id: " + programId, "Warn");
        continue;
      }
      
      if(accountIdToAccountMasterMap.containsKey(accountMasterVO.getAccountMasterId()))
      {
        // Add the program to the Account Master, so we send all Program Updates for the same account
        accountIdToAccountMasterMap.get(accountMasterVO.getAccountMasterId()).getAccountPrograms().addAll(accountMasterVO.getAccountPrograms());
      } else 
      {
        accountMasterVOList.add(accountMasterVO);
        accountIdToAccountMasterMap.put(accountMasterVO.getAccountMasterId(), accountMasterVO);
      }
    }
    
    return accountMasterVOList;  
  }*/

  /**
   * Sends the preTransformedXML as a Feed to Website.
   * 
   * If the response is an Exception related to Feed Communication, throws an exception so the feed is retried.
   * Else, failures are sent in a System Message.
   * 
   * @param connection
   * @param preTransformedXML
   * @throws RuntimeException On failure in sending the Feed containing the Error String
   */
  protected void sendFeedXMLToWebSite(Connection connection, String preTransformedXML)
  {
    if(logger.isDebugEnabled())
    {
      logger.debug("Sending XML Feed: " + preTransformedXML);
    }
    
    NovatorFeedDataPreTransformedXmlVO novatorFeedDataPreTransformedXmlVO = new NovatorFeedDataPreTransformedXmlVO();
    novatorFeedDataPreTransformedXmlVO.setFeedXML(preTransformedXML);
    
    AccountNovatorFeedUtil novatorFeedUtil = new AccountNovatorFeedUtil();
    NovatorFeedResponseVO responseVO = novatorFeedUtil.sendToNovator(connection, novatorFeedDataPreTransformedXmlVO, null);
    
    if(responseVO.getIsTransmitException())
    {
      String errorMessage = "Feed Error: " + responseVO.getErrorString();
      logger.error(errorMessage);
      sendSystemMessage(errorMessage, "ERROR");
      throw new RuntimeException(errorMessage, responseVO.getTransmitThrowable());
    } else if(!responseVO.isSuccess())
    {
      String errorMessage = "Feed Error: " + responseVO.getErrorString();
      logger.warn(errorMessage);    
      sendSystemMessage("Feed Error: " + responseVO.getErrorString(), "WARN");
    }
  }

  /**
   * Sends a System Message with the passed in message and status.
   * Override this to invoke the appropriate SystemMessenger
   * @param message The System Message
   * @param type The Type value for the System Message
   */
  protected abstract void sendSystemMessage(String message, String type);

}
