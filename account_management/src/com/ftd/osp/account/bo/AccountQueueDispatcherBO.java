package com.ftd.osp.account.bo;

import com.ftd.osp.account.common.AccountConstants;
import com.ftd.osp.account.vo.AccountProgramVO;
import com.ftd.osp.account.xml.bo.AccountXMLBO;
import com.ftd.osp.account.xml.vo.AccountManagementTaskVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.util.LinkedList;
import java.util.List;

import javax.naming.InitialContext;


/**
 * Queues JMS Messages to the Account Queue.
 *
 * This Business Object utilizes the Dispatcher Framework. Ensure that the
 * location and status for the ACCOUNT TASK QUEUE are defined in the dispatcher.xml
 * file.
 */
public class AccountQueueDispatcherBO
{
  private static Logger logger = new Logger(AccountQueueDispatcherBO.class.getName());

  /**
   * Utilized for generating Account XML
   */
  private AccountXMLBO accountXMLBO;


  public AccountQueueDispatcherBO()
  {
    accountXMLBO = new AccountXMLBO();
  }


  

  /**
   * Adds the passed in request to the Account JMS Queue
   * @param xmlRequest
   */
  public void addRequestToAccountTaskQueue(String xmlRequest)
  {
    Dispatcher dispatcher;
    try
    {
      dispatcher = Dispatcher.getInstance();

      MessageToken token = new MessageToken();
      token.setStatus("QUEUE ACCOUNT TASK");
      token.setMessage(xmlRequest);

      // enlist the JMS transaction, with the other application transaction
      dispatcher.dispatchTextMessage(new InitialContext(), token);
    }
    catch (Throwable t)
    {
      logger.error("Failed to send JMS Message: " + t.getMessage(), t);
      throw new RuntimeException("Failed to send JMS Message: " + t.getMessage(), t);
    }
  }
}
