package com.ftd.osp.account.bo;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.naming.InitialContext;

import com.ftd.customer.core.domain.CustomerCCData;
import com.ftd.customer.core.domain.CustomerFullProfile;
import com.ftd.customer.services.rest.core.response.GenericResponse;
import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.ProfileServiceUtils;
import com.ftd.op.common.CallingSystemGlobalParamMapper;
import com.ftd.op.order.to.CreditCardTO;
import com.ftd.osp.account.common.AccountConstants;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.account.common.MessagingServiceLocator; 
import com.ftd.osp.account.dao.FreeShippingEmailDAO;
import com.ftd.osp.account.vo.AutoRenewOrderVO;
import com.ftd.osp.account.vo.CCData;
import com.ftd.osp.account.vo.CommonCreditCardVO;
import com.ftd.osp.account.vo.CustomerAccount;
import com.ftd.osp.account.vo.FSMembershipVO;
import com.ftd.osp.account.xml.bo.AccountXMLBO;
import com.ftd.osp.account.xml.vo.AccountManagementTaskVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.CustomerCCandFSMDetails;
import com.ftd.osp.utilities.vo.CustomerFSMDetails;
import com.ftd.osp.utilities.vo.MembershipData;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.ProfileCCData; 
import org.apache.commons.lang.StringUtils;
import com.ftd.ftdutilities.CreditCardNotFoundException;

/**
 * @author skatam
 *
 */
public class FSAutoRenewBO 
{
	private static Logger logger = new Logger(FSAutoRenewBO.class.getName());
	
	CommonUtils commonUtils = new CommonUtils();
	
	public void processAutoRenew(Date startDate, Date endDate) throws Exception
	{
		logger.debug("FSAutoRenewBO.processAutoRenew():"+startDate+":"+endDate);
		List<String> emailIds = null;
			try 
			{
				emailIds = FTDCAMSUtils.getExpiredEmailsByExpireDate(startDate, endDate);
			} catch (CustProfileTimeoutException timeout) {
				 throw timeout;
			} catch (Exception e) {
				logger.error("Error occurred while getting expired emails ", e);
				commonUtils.sendSystemMessage("Error occurred while getting expired emails.",  e);
				throw e;
			}
		if(emailIds == null || emailIds.isEmpty())
		{			
			logger.info("No email ids to process for freeshipping auto renewal.");
			return;
		}		
		for (String emailId : emailIds) 
		{
			logger.debug("Processing FS AutoRenewal for email :"+emailId);
			Connection conn = null;
			try 
			{
				conn = commonUtils.getConnection();
				FreeShippingEmailDAO fsEmailDAO = new FreeShippingEmailDAO(conn);
				boolean emailInProcessing = fsEmailDAO.checkEmailIdInProcessing(emailId);
				logger.debug("Email exists with today's date :"+emailInProcessing);
				if(emailInProcessing){
					continue;
				}
				this.enqueueFSAutoRenewEmail(emailId);			    
			    fsEmailDAO.insertFSEmailProcessingRecord(emailId);
			}
			catch (Throwable t)
		    {
		      logger.error("Error in enqueue-ing FS AutoRenewal for email :"+emailId+". Terminating the process. Error: " + t.getMessage(), t);
		      commonUtils.sendSystemMessage("Error in enqueue-ing FS AutoRenewal for email :"+emailId+". Terminating the process. Error: " + t.getMessage(), t);
		      throw new RuntimeException(t.getMessage(), t);
		    }
		    finally
		    {
		      commonUtils.closeConnection(conn);
		    }
		}
	}
	
	public void reprocessAutoRenew(String emailId) 
	{
		logger.debug("Reprocessing FS AutoRenew for email :"+emailId);
		if(emailId == null || emailId.trim().length() == 0){
			logger.error("EmailAddress is null or empty.");
			return;
		}
		emailId = emailId.trim();
	    
		CustomerFSMDetails fsmDetails=null;
		try 
		{
			fsmDetails = FTDCAMSUtils.getFSM(emailId);
			if(fsmDetails == null){
				logger.info("No FS details found for the email::"+emailId);
				return;
			}
			logger.info("FSDetails for email::"+emailId+ " are "+fsmDetails.toString());
		}
		catch(CustProfileTimeoutException timeout) {
			throw timeout;
		} catch (Exception e) {
			logger.error("Error in getting FSM Details for Email("+emailId+"): " + e.getMessage(), e);
			commonUtils.sendSystemMessage("Error in getting FSM Details for Email("+emailId+")", e);
			return;
		}
		Date today = trimTimeStamp(new Date());
		Date fsEndDate = trimTimeStamp(fsmDetails.getFsEndDate());
		if(fsEndDate==null || fsEndDate.after(today))
		{
			logger.debug("FS Expiry date for "+emailId+" is :"+fsEndDate+". No need to process now.");
			return;
		}
		else
		{
			Connection conn = null;
			try
			{
				conn = commonUtils.getConnection();
				logger.debug("Enqueuing email "+emailId+ " for Autorenewal ");
				this.enqueueFSAutoRenewEmail(emailId);
			    
			    FreeShippingEmailDAO fsEmailDAO = new FreeShippingEmailDAO(conn);
				boolean emailInProcessing = fsEmailDAO.checkEmailIdInProcessing(emailId);
				if(!emailInProcessing){
					fsEmailDAO.insertFSEmailProcessingRecord(emailId);
				}
			}
			catch (Throwable t)
		    {
		      logger.error("Error in Re-processing FSAutoRenewEmail("+emailId+"): " + t.getMessage(), t);
		      commonUtils.sendSystemMessage("Error in Re-processing FSAutoRenewEmail("+emailId+"):", t);
		    }
		    finally
		    {
		      commonUtils.closeConnection(conn);
		    }
				
		}
	}
		
	private void enqueueFSAutoRenewEmail(String emailId)
	{
		logger.debug("Queueing Email "+emailId+" for FS_PROCESSING_EMAIL Task.");
		//Send JMS msg to ACCOUNT queue
	   AccountManagementTaskVO vo = new AccountManagementTaskVO();
	   vo.setTaskType(AccountConstants.TASK_TYPE_FS_PROCESSING_EMAIL);
	   FSMembershipVO fsMembershipVO = new FSMembershipVO();
	   fsMembershipVO.setEmailAddress(emailId);
	   vo.setFsMembershipVO(fsMembershipVO);
	  
	   AccountXMLBO bo = new AccountXMLBO();
	   String requestXML = bo.convertAccountManagementTaskToXML(vo);
		  
		AccountQueueDispatcherBO accountMessagingBO = new AccountQueueDispatcherBO();
	    accountMessagingBO.addRequestToAccountTaskQueue(requestXML);
	    logger.debug("requestXML : "+requestXML);
	    logger.debug("JMS message sent successfully to ACCOUNT queue with FS_PROCESSING_EMAIL task type.");
	}
	
	public void processFSAutoRenew(Connection conn, String emailId) throws Exception
	{
		if(emailId == null || emailId.trim().length() == 0){
			logger.error("EmailAddress is null or empty.");
			return;
		}
		CustomerAccount account = this.getFSAccount(emailId);
		if(account == null){
			logger.error("FSAccount not found for ("+emailId+").");
			return;
		}
		if(!account.hasCreditCards()){
			logger.info("No credit cards for email: "+emailId);
			logger.info("Sending Missing status of Auto-Renewal Credit Card");
			FTDCAMSUtils.sendCreditCardStatus(ccStatus.Missing.toString(), "Auto-Renew", emailId, null, null, null);
			logger.info("Sending Missing status of Billing Credit Card");
			FTDCAMSUtils.sendCreditCardStatus(ccStatus.Missing.toString(), "Billing", emailId, null, null, null);
			return;
		}
		FreeShippingEmailDAO fsEmailDAO = new FreeShippingEmailDAO(conn);
		String externalOrderNo = account.getFsMembershipVO().getExternalOrderNumber();
		if(externalOrderNo == null){
			logger.info("External Order Number is null.");
			return;
		}
		AutoRenewOrderVO renewOrderVO = fsEmailDAO.getCCAuthParamsByExtOrdNo(externalOrderNo);
		if(renewOrderVO == null){
			logger.info("Invalid SKU.");
			return;
		}
		String orderGuid = renewOrderVO.getOrderGuid();
		String productId = renewOrderVO.getProductId();
		String productPrice = renewOrderVO.getProductPrice();
		logger.debug("orderGuid:"+orderGuid);
		logger.debug("productId:"+productId);
		logger.debug("productPrice:"+productPrice);
		
		CommonCreditCardVO commonCreditCardVO = 
				this.getAuthenticatedCreditCardVO(conn, account,orderGuid, productPrice, emailId);
		if(commonCreditCardVO == null)
		{
			logger.info("No CreditCard Authorized successfully.");
			return;
		}
		AutoRenewOrderVO orderVO = fsEmailDAO.createAutoRenewOrder(
				account, commonCreditCardVO, productId, productPrice);
		if(orderVO != null) {
			logger.info("***Autorenew OrderVO: " + orderVO.getExternalOrderNumber() + ", " + orderVO.getOrderGuid() + ", " + orderVO.getMasterOrderNumber());
		}
		this.sendJMSMessageToJoeOrderDispatcher(orderVO);
		
	}
	
	private CustomerAccount getFSAccount(String emailId) throws Exception
	{
		  CustomerAccount account = null;
		  String profileSvcEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm(FTDCAMSUtils.SERVICE, FTDCAMSUtils.PROFILE_SERVICE_ENABLED);
		  GenericResponse<CustomerFullProfile> fullProfileResp = null;

		  if("Y".equalsIgnoreCase(profileSvcEnabled)) {
			  CustomerCCandFSMDetails custDetails = null;
			  try{
				  custDetails = ProfileServiceUtils.getFSAccount(emailId); //Call Profile Service - Profile service utils takes care of processing error response
				  account = transformToCustomerAccount(emailId, null, custDetails);
			  }
			  catch(CreditCardNotFoundException e){
				  fullProfileResp = FTDCAMSUtils.getCustomerFSMAndCCData(emailId); //Call CAMS - CAMS Takes care of processing error response
				  account = transformToCustomerAccount(emailId, fullProfileResp, null);
			  }
		  } 
		  else {
			  fullProfileResp = FTDCAMSUtils.getCustomerFSMAndCCData(emailId); //Call CAMS - CAMS Takes care of processing error response
			  account = transformToCustomerAccount(emailId, fullProfileResp, null);
		  }
		  
		  if(account != null) {
			  account.setEmailId(emailId);
		  }

		  return account;
	}
	
	private static CustomerAccount transformToCustomerAccount(String emailId, GenericResponse<CustomerFullProfile> fullProfileResp, CustomerCCandFSMDetails custCCandFSMDetails){
		CustomerAccount account = null;
		Date formattedDate = null;
		SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
		if(fullProfileResp != null){
			  account = new CustomerAccount();
			  CustomerFullProfile fullProfile = fullProfileResp.getData();
			  com.ftd.customer.core.domain.CustomerFSMDetails fsMDetails = fullProfile.getCustomerFSMDetails();
			  FSMembershipVO fsMembershipVO = new FSMembershipVO();
			  fsMembershipVO.setEmailAddress(emailId);
			  fsMembershipVO.setExternalOrderNumber(fsMDetails.getFsmOrderNumber());
			  fsMembershipVO.setStartDate(fsMDetails.getFsStartDate());
			  fsMembershipVO.setEndDate(fsMDetails.getFsEndDate());
			  account.setFsMembershipVO(fsMembershipVO);
			  
			  List<CustomerCCData> ccDataList = fullProfile.getCustomerCCDataList();
			  List<CCData> ccDetailList = new ArrayList<CCData>();
			  for(CustomerCCData ccData : ccDataList){
				  CCData ccDetail = new CCData();
				  ccDetail.setCardPurpose(ccData.getCcPurpose());
				  ccDetail.setCardType(ccData.getCcType());
				  ccDetail.setCcNumber(ccData.getEncryptedCc());
				  ccDetail.setExpirationMonth(ccData.getExpMonth());
				  ccDetail.setExpirationYear(ccData.getExpYear());
				  
				  ccDetailList.add(ccDetail);
			  }
			  account.setCcData(ccDetailList);
		  } else if(custCCandFSMDetails != null) {
			  account = new CustomerAccount();
			  
			  MembershipData fsmDetails = custCCandFSMDetails.getFsmDetails();
			  FSMembershipVO fsMembershipVO = new FSMembershipVO(); 
			  
			  fsMembershipVO.setEmailAddress(emailId);
			  fsMembershipVO.setDuration(Integer.parseInt(fsmDetails.getTerm()));
			  
			  formattedDate = isoFormat.parse(fsmDetails.getExpireDate());
			  fsMembershipVO.setEndDate(formattedDate);
			  
			  formattedDate = isoFormat.parse(fsmDetails.getStartDate());
			  fsMembershipVO.setStartDate(formattedDate);
			  
			  fsMembershipVO.setExternalOrderNumber(fsmDetails.getOrderNumber());
			  if("Active".equalsIgnoreCase(fsmDetails.getStatus())){
				 fsMembershipVO.setMembershipStatus("1");
			  } else{
				 fsMembershipVO.setMembershipStatus("0");
			  }
			  account.setFsMembershipVO(fsMembershipVO);
			  
			  List<ProfileCCData> ccDataList = custCCandFSMDetails.getCreditCards();
			  List<CCData> custCCDataList = new ArrayList<CCData>();
			  CCData ccDetail = null;
			  if(ccDataList != null && ccDataList.size() > 0){
				  for(ProfileCCData ccData : ccDataList){
					    ccDetail = new CCData();
					    ccDetail.setCardPurpose(ccData.getCardPurpose());
					    ccDetail.setCardType(ccData.getCardType());
					    
					    if("AMERICANEXPRESS".equals(ccData.getCardType())) {
					    	ccDetail.setCardType("Amex");
					    } else if ("MASTERCARD".equals(ccData.getCardType())) {
					    	ccDetail.setCardType("Master");
					    }
					    
					    //try{ // detokenize through PG to get card number
					    	//PaymentDetokenizationResponse cardVO = PaymentServiceRequest.getCreditCardsFromToken(ccData.getTokenId(), ccData.getMerchantReferenceId());
					    	//if(cardVO != null){
				    		ccDetail.setCcNumber(ccData.getCardNumber());
				    		ccDetail.setExpirationMonth(ccData.getExpirationMonth());
				    		//ccDetail.setExpirationYear(ccData.getExpirationYear());
				    		if(ccData.getExpirationYear() != null && ccData.getExpirationYear().length() == 4) {
				    			SimpleDateFormat yySDf = new SimpleDateFormat("yy");
				    			SimpleDateFormat yyyySDf = new SimpleDateFormat("yyyy");	 
				    			ccDetail.setExpirationYear(yySDf.format(yyyySDf.parse(ccData.getExpirationYear())));
				    		} else { 					    		
				    			ccDetail.setExpirationYear(ccData.getExpirationYear());
				    		}
					    	//}else {
					    	//	logger.error("Null response received while detokenizing");
					    	//}
					    //} catch(Exception e){
					    //	logger.error("Error Occurred while detokenizing the card through Payment Gateway Service");
					    //	ProfileServiceUtils.sendSystemMessage("Error occurred while detokenizing the "+ccDetail.getCardPurpose()+ " card ");
					    //}
					    custCCDataList.add(ccDetail);
					    logger.info("Profile month and Year:: " + ccData.getExpirationMonth() + ", " + ccData.getExpirationYear() + ", Formated Year:: " + ccDetail.getExpirationYear());
				  }
				  
			  }
			  account.setCcData(custCCDataList);
		  }
		}catch(Exception e){
			logger.error("Error occurred while transforming to customer Account");
		}
		return account;
	}
	
	enum ccStatus {Expired, Missing, Success, Declined}
	private CommonCreditCardVO getAuthenticatedCreditCardVO(Connection conn,
			CustomerAccount account, String orderGuid, String amount, String emailAddress)
			throws Exception 
	{
		CommonCreditCardVO resultVO = null;

		CCData creditCardVO = account.getAutoRenewalCC();
		String cardNumber = null;
		String lastFourDigits = null;
		
		if (creditCardVO != null && !StringUtils.isEmpty(creditCardVO.getCcNumber())) {
			cardNumber = creditCardVO.getCcNumber();
			lastFourDigits = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
			
			if (CommonUtils.isCreditCardExpired(creditCardVO.getExpirationYear(), creditCardVO.getExpirationMonth())){
				FTDCAMSUtils.sendCreditCardStatus(ccStatus.Expired.toString(), "Auto-Renew", emailAddress, creditCardVO.getExpirationMonth(), creditCardVO.getExpirationYear(), lastFourDigits);
				logger.info("Sending Expired status of Auto-Renewal Credit Card");
			}else{
				logger.debug("Authorizing AutoRenewalCC");
				resultVO = authorizeCC(conn, creditCardVO, amount, orderGuid);
				if (resultVO != null) {
					FTDCAMSUtils.sendCreditCardStatus(ccStatus.Success.toString(), "Auto-Renew", emailAddress, creditCardVO.getExpirationMonth(), creditCardVO.getExpirationYear(), lastFourDigits);
					logger.info("Sending Success status of Auto-Renewal Credit Card");
					return resultVO;
					 
				}else{
					logger.info("Sending Declined status of Auto-Renewal Credit Card");
					FTDCAMSUtils.sendCreditCardStatus(ccStatus.Declined.toString(), "Auto-Renew", emailAddress, creditCardVO.getExpirationMonth(), creditCardVO.getExpirationYear(), lastFourDigits);
				}
			}
		}else{
			logger.info("Sending Missing status of Auto-Renewal Credit Card");
			FTDCAMSUtils.sendCreditCardStatus(ccStatus.Missing.toString(), "Auto-Renew", emailAddress, null, null, null);
		}
			
		creditCardVO = account.getSavedBillingCC();
		if (creditCardVO != null && !StringUtils.isEmpty(creditCardVO.getCcNumber())) {
			cardNumber = creditCardVO.getCcNumber();
			lastFourDigits = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
			
			if (CommonUtils.isCreditCardExpired(creditCardVO.getExpirationYear(), creditCardVO.getExpirationMonth())){
				logger.info("Sending Expired status of Billing Credit Card");
				FTDCAMSUtils.sendCreditCardStatus(ccStatus.Expired.toString(), "Billing", emailAddress, creditCardVO.getExpirationMonth(), creditCardVO.getExpirationYear(), lastFourDigits);
				
			}else{
				logger.debug("Authorizing Billing CC");
				resultVO = authorizeCC(conn, creditCardVO, amount, orderGuid);
				if (resultVO != null) {
					logger.info("Sending Success status of Billing Credit Card");
					FTDCAMSUtils.sendCreditCardStatus(ccStatus.Success.toString(), "Billing", emailAddress, creditCardVO.getExpirationMonth(), creditCardVO.getExpirationYear(), lastFourDigits);
					return resultVO;
				}else{
					logger.info("Sending Decline status of Billing Credit Card");
					FTDCAMSUtils.sendCreditCardStatus(ccStatus.Declined.toString(), "Billing", emailAddress, creditCardVO.getExpirationMonth(), creditCardVO.getExpirationYear(), lastFourDigits);
				}
			}
		}else{
			logger.info("Sending Missing status of Billing Credit Card");
			FTDCAMSUtils.sendCreditCardStatus(ccStatus.Missing.toString(), "Billing", emailAddress, null, null, null);
		}
		List<CCData> otherCCs = account.getOtherCCs();
		if(otherCCs!=null){
			logger.debug("Authorizing Other CCs");
			for (CCData otherCC : otherCCs) 
			{
				resultVO = authorizeCC(conn, otherCC, amount, orderGuid);
				if (resultVO != null) {
					return resultVO;
				}
			}
		}
		return null;
	}
	
	private CommonCreditCardVO authorizeCC(
			Connection conn,CCData creditCardVO,
			String amount, String orderGuid) throws Exception
	{
		CommonCreditCardVO resultVO = null;
		CommonCreditCardVO cccVO = null;
		if(creditCardVO != null)
		{
			cccVO = new CommonCreditCardVO();
			cccVO.setCreditCardNumber(creditCardVO.getCcNumber());
			String keyName = ConfigurationUtil.getInstance().getFrpGlobalParm("Ingrian", "Current Key");
			cccVO.setKeyName(keyName);
			String ccTypeCode = CommonUtils.getCreditCardTypeCode(creditCardVO.getCardType());
			if(ccTypeCode == null){
				logger.debug("Invalid Credit Card Type "+ creditCardVO.getCardType());
				return null;
			}
			cccVO.setCreditCardType(ccTypeCode);
			
			cccVO.setAmount(amount);
			cccVO.setExpirationDate(creditCardVO.getExpirationYear()+""+creditCardVO.getExpirationMonth());//yyMM
			
			resultVO = this.authorize(conn,cccVO, orderGuid);
			if(resultVO == null)
			{
				logger.debug("Authorization result is null.");
				
				return null;
			}
			else if (resultVO != null && "E".equalsIgnoreCase(resultVO.getValidationStatus())) {
				logger.error("Payment service is unable to perform the authorization for Order Guid: " + orderGuid + ". This request will be re-tried. Please check if the request succeded on the next attempt or not");
				throw new Exception("Payment service is unable to perform the authorization for Order Guid: " + orderGuid + ". This request will be re-tried. Please check if the request succeded on the next attempt or not");
			}
			else
			{
				logger.debug("Authorization ValidationStatus is :"+resultVO.getValidationStatus());
				if("A".equalsIgnoreCase(resultVO.getValidationStatus()))
				{
					return resultVO;
				}
			}			
		}
		return null;
	}
	
	/**
   * authorize the credit card
   * @param CommonCreditCardVO
   * @param String  - orderGuid
   * @return CommonCreditCardVO
   * @throws Exception
   */
	 private CommonCreditCardVO authorize(Connection conn, 
			  							  CommonCreditCardVO cccVO,
			  							  String orderGuid)
	         throws Exception
	  {
		
	    FreeShippingEmailDAO freeShippingEmailDAO = new FreeShippingEmailDAO(conn);
	    CustomerAccount custVO = freeShippingEmailDAO.getCustomerByOrder(orderGuid);
	    CreditCardTO ccTO = new CreditCardTO();
	    ccTO.setAmount(cccVO.getAmount());
	    ccTO.setCreditCardNumber(cccVO.getCreditCardNumber());
	    ccTO.setCreditCardType(cccVO.getCreditCardType());
	    ccTO.setCustomerId(String.valueOf(custVO.getCustomerId()));
	    ccTO.setExpirationDate(cccVO.getExpirationDate());
	    ccTO.setAddressLine(custVO.getAddress1());
	    ccTO.setZipCode(custVO.getZipCode());
	    ccTO.setCreditCardDnsType(custVO.getCompanyId());
	    ccTO.setCallingSystem(CallingSystemGlobalParamMapper.FS_AUTO_RENEW);
	    
	    //Deliberately making Cardinal tokens empty, since cardinal tokens expired after 90 days.
	    ccTO.setCavv("");
	    ccTO.setXid("");
	    ccTO.setEci("");
	    ccTO.setUcaf("");
	    
	    ccTO = MessagingServiceLocator.getInstance().getOrderAPI().validateCreditCard(ccTO, conn);
	    
	    logger.debug("status = " + ccTO.getValidationStatus());
	    logger.debug("actionCode = " + ccTO.getActionCode());
	    
	    cccVO.setValidationStatus(ccTO.getValidationStatus()); 
	    cccVO.setCreditCardType(ccTO.getCreditCardType());
	    cccVO.setCreditCardDnsType(ccTO.getCreditCardDnsType());
		cccVO.setCcId(ccTO.getCcId());
	    cccVO.setName(ccTO.getName());
		cccVO.setAddress2(ccTO.getAddress2());
	    cccVO.setCity(ccTO.getCity());
		cccVO.setCountry(ccTO.getCountry());
		cccVO.setCustomerId(ccTO.getCustomerId());
		cccVO.setAddressLine(ccTO.getAddressLine());
	    cccVO.setZipCode(ccTO.getZipCode());
		cccVO.setCreditCardNumber(ccTO.getCreditCardNumber());
	    cccVO.setExpirationDate(ccTO.getExpirationDate().substring(0,2) + "/" + ccTO.getExpirationDate().substring(2,4));
	    cccVO.setUserData(ccTO.getUserData());
	    cccVO.setTransactionType(ccTO.getTransactionType());
	    cccVO.setAmount(ccTO.getAmount());
	    cccVO.setApprovedAmount(ccTO.getApprovedAmount());
	    cccVO.setStatus(ccTO.getStatus());
	    cccVO.setActionCode(ccTO.getActionCode());
	    cccVO.setVerbiage(ccTO.getVerbiage());
	    cccVO.setApprovalCode(ccTO.getApprovalCode());
		cccVO.setDateStamp(ccTO.getDateStamp());
	    cccVO.setTimeStamp(ccTO.getTimeStamp());
	    cccVO.setAVSIndicator(ccTO.getAVSIndicator());
	    cccVO.setBatchNumber(ccTO.getBatchNumber());
	    cccVO.setItemNumber(ccTO.getItemNumber());
	    cccVO.setAcquirerReferenceData(ccTO.getAcquirerReferenceData());
	    cccVO.setUnderMinAuthAmt(ccTO.isUnderMinAuthAmt());
	    // Populating auth provider and additional payment information required for BAMS related transactions
	    cccVO.setCcAuthProvider(ccTO.getCcAuthProvider());
	    cccVO.getPaymentExtMap().putAll(ccTO.getPaymentExtMap());
	    
	    logger.debug("approvalCode = " + ccTO.getApprovalCode());
	    
	    return cccVO;
	  }
	  
		
	private void sendJMSMessageToJoeOrderDispatcher(AutoRenewOrderVO orderVO) throws Exception
	{
		MessageToken token = null;
        token = new MessageToken();
        token.setStatus("JOE Order Dispatcher");
        token.setMessage(orderVO.getMasterOrderNumber());

        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(new InitialContext(), token);
	}
	
	public static final Date trimTimeStamp(Date date)
	{
		if(date == null){
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		Calendar resultCal = Calendar.getInstance();
		resultCal.clear();
		resultCal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		resultCal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		resultCal.set(Calendar.DATE, cal.get(Calendar.DATE));
		return resultCal.getTime();
	}
	
	public static final String formatDate(Date date, String dateFromat)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		return sdf.format(date);
	}
	
	
	public void deleteFSProcessingEmail(String emailId) throws Exception {
		logger.debug("Deleting Email Id: " + emailId + " from FS Processing email table");
		Connection conn = null;
		
		try {
			conn = commonUtils.getConnection();
			FreeShippingEmailDAO freeShippingEmailDAO = new FreeShippingEmailDAO(conn);
			freeShippingEmailDAO.deleteFSEmailProcessingRecord(emailId);
		}
		finally {
			if (conn != null) {
				commonUtils.closeConnection(conn);
			}
		}
	}
}
