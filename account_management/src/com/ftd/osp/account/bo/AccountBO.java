package com.ftd.osp.account.bo;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.order.client.OrderRestServiceClient;
import com.ftd.order.rest.core.request.OrderServiceRequest;
import com.ftd.osp.account.common.CommonUtils;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.util.validation.ErrorMessage;
import com.ftd.util.validation.ErrorMessages;

/**
 * Provides business methods for working with Accounts
 */
public class AccountBO
{
  private static Logger logger = new Logger(AccountBO.class.getName());
  CommonUtils commonUtils = new CommonUtils();
  private static  int timetoStopaMethod=10000;//setting Default Value
  private static String camserrmsg="TimeoutException For CAMS";
		
	public void updateFullyRefundStatusInCAMS(String externalOrderNumber, boolean redeliveryFlag, String payLoad) throws Exception
	{
		try
		{
			OrderRestServiceClient client = CommonUtils.getOrderRestServiceClient();
			OrderServiceRequest request = new OrderServiceRequest();
			request.setOrderNumber(externalOrderNumber);
			com.ftd.order.rest.core.response.GenericResponse<Integer> response = null;
			if(FTDCAMSUtils.CAMS_ENABLE_FLAG.equalsIgnoreCase(FTDCAMSUtils.getCAMSTimeoutEnabledFlag()))
				response =this.updateFullyRefundStatusInCAMSWithTimeoutException(client,request);
			else
				response = client.refundOrder(request);
			ErrorMessages errors = response.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			for (ErrorMessage errMsg : errMsgs) {
				if("507".equals(errMsg.getCode())){
					logger.error(errMsg.getMessage());
					throw new Exception(errMsg.getMessage());
				}else if ("508".equals(errMsg.getCode())){
					logger.info(errMsg.getMessage());
				}
			}
		}
		catch(CustProfileTimeoutException timeout)
		{
			throw timeout;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage(), e);
			if(redeliveryFlag)
				commonUtils.sendSystemMessage("Please use the payload to perform manual updates : "+payLoad+"  "+e.getMessage(), "CAMS_Error");
			throw new RuntimeException(e.getMessage(), e);
			
		}
		
	}
	
	public void updateCancelOrderStatusInCAMS(String externalOrderNumber, boolean redeliveryFlag, String payLoad) throws Exception
	{
		try
		{

			OrderRestServiceClient client = CommonUtils.getOrderRestServiceClient();
			OrderServiceRequest request = new OrderServiceRequest();
			request.setOrderNumber(externalOrderNumber);
			com.ftd.order.rest.core.response.GenericResponse<Integer> response =null;
			if(FTDCAMSUtils.CAMS_ENABLE_FLAG.equalsIgnoreCase(FTDCAMSUtils.getCAMSTimeoutEnabledFlag()))
			  response = this.updateCancelOrderStatusInCAMSWithTimeoutException(client,request);
			else
				response = client.cancelOrder(request);
			ErrorMessages errors = response.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			for (ErrorMessage errMsg : errMsgs) {
				if("507".equals(errMsg.getCode())){
					logger.error(errMsg.getMessage());
					throw new Exception(errMsg.getMessage());
				}else if ("509".equals(errMsg.getCode())){
					logger.info(errMsg.getMessage());
				}
			}
		}
		catch(CustProfileTimeoutException timeout)
		{
			logger.error(timeout.getMessage(), timeout);
			throw timeout;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage(), e);
			if(redeliveryFlag)
				commonUtils.sendSystemMessage("Please use the payload to perform manual updates : "+payLoad+"  "+e.getMessage(), "CAMS_Error");
			throw new RuntimeException(e.getMessage(), e);
		}
				
	}
	private com.ftd.order.rest.core.response.GenericResponse<Integer>  updateCancelOrderStatusInCAMSWithTimeoutException(final OrderRestServiceClient client,final OrderServiceRequest request) throws InterruptedException, ExecutionException
	{
		com.ftd.order.rest.core.response.GenericResponse<Integer> response =null;
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<com.ftd.order.rest.core.response.GenericResponse<Integer>> future = executorService.submit(new java.util.concurrent.Callable<com.ftd.order.rest.core.response.GenericResponse<Integer>>() 
		    {
			   public com.ftd.order.rest.core.response.GenericResponse<Integer> call() 
			      {  
				     return client.cancelOrder(request);
				  }
			});
		try{
			response= (com.ftd.order.rest.core.response.GenericResponse<Integer>)future.get(CommonUtils.getCamsResponseTimoutValue(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("updateCancelOrderStatusInCAMSWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(camserrmsg);
	       }
		finally
		{
			 future.cancel(true);
		}
		return response;
	}
	private com.ftd.order.rest.core.response.GenericResponse<Integer> updateFullyRefundStatusInCAMSWithTimeoutException(final OrderRestServiceClient client,final OrderServiceRequest request) throws InterruptedException, ExecutionException 
	{
		com.ftd.order.rest.core.response.GenericResponse<Integer> response=null;
		logger.debug("CAMS_WS_SOCKET_TIMEOUT is "+CommonUtils.getCamsResponseTimoutValue());
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<com.ftd.order.rest.core.response.GenericResponse<Integer>> future = executorService.submit(new java.util.concurrent.Callable<com.ftd.order.rest.core.response.GenericResponse<Integer>>() 
		    {
			   public com.ftd.order.rest.core.response.GenericResponse<Integer> call() 
			      {  
				     return client.refundOrder(request);
				  }
			});
		try{
			response= (com.ftd.order.rest.core.response.GenericResponse<Integer>)future.get(CommonUtils.getCamsResponseTimoutValue(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("updateFullyRefundStatusInCAMSWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(camserrmsg);
	       }
		finally
		{
			 future.cancel(true);
		}
		return response;
		
	}
}
