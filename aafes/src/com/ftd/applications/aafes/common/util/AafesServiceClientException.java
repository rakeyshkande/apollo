package com.ftd.applications.aafes.common.util;

/**
 * The purpose of this class is to be able to distinguish the exeption that
 * happens when there is an unexpected/non-complient response received from the  
 * FTD West/Skynet order server.  Properly formatted errors should not be reported with this 
 * exception.
 */
public class AafesServiceClientException extends Exception {

    public AafesServiceClientException(Throwable t) {
        super(t);
    }
    public AafesServiceClientException(String message) {
        super(message);
    }
    public AafesServiceClientException(String message, Throwable t) {
        super(message,t);
    }
}
