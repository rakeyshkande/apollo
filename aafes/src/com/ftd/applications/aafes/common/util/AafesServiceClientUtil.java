package com.ftd.applications.aafes.common.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.aafes.odin.val.milstarvendor_asmx.MilitaryStar;
import com.ftd.applications.aafes.vo.AafesRequestVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;

/**
 * @author kkeyan
 *
 */
public class AafesServiceClientUtil {
	
	private static final String AAFES_SERVICE = "AAFES Service";

	private static final String ORDER_SCRUB = "ORDER SCRUB";

	private static final String DELM_COMMA_SPACE = ", ";

	private static final String TAG_NM_RETURN_MESSAGE = "ReturnMessage";

	private static final String TAG_NM_AUTH_TKT = "AuthTkt";

	private static final String TAG_NM_AUTH_CODE = "AuthCode";

	private static final String TAG_NM_RETURN_CODE = "ReturnCode";

	private static final String TAG_NM_RESPONSE = "Response";

	private static final String REQ_TYPE_SETTLE = "S";

	private static final String REQ_TYPE_CREDIT = "C";

	private static final String REQ_TYPE_APPROVAL = "A";

	private static Logger logger = new Logger("com.ftd.applications.aafes.common.util.AafesServiceClientUtil");
	
	private static final String msApprovalReqTemplate="<?xml version=\"1.0\" encoding=\"utf-8\"?><MSApproval xmlns=\"MSApproval\"><Request ID=\"$req.reqId\">"
												+ "<CCNumber>$req.ccNumber</CCNumber><Amount>$req.amount</Amount><FacNbr>$req.facNbr</FacNbr>"
												+ "<Region>$req.region</Region><ZipCode>$req.zip</ZipCode><CID>$req.cid</CID>"
												+ "</Request></MSApproval>";												
												
	private static final String msCreditReqTemplate="<?xml version=\"1.0\" encoding=\"utf-8\"?><MSCredit xmlns=\"MSCredit\"><Request ID=\"$req.reqId\">"
												+ "<CCNumber>$req.ccNumber</CCNumber><Amount>$req.amount</Amount><FacNbr>$req.facNbr</FacNbr>"
												+ "<AuthCode>$req.authcode</AuthCode><AuthTkt>$req.ticket</AuthTkt><Region>$req.region</Region>"
												+ "</Request></MSCredit>";
	 
	private static final String msSettleReqTemplate="<?xml version=\"1.0\" encoding=\"utf-8\"?><MSSettle xmlns=\"MSSettle\"><Request ID=\"$req.reqId\">"
												+ "<CCNumber>$req.ccNumber</CCNumber><Amount>$req.amount</Amount><FacNbr>$req.facNbr</FacNbr>"
												+ "<AuthCode>$req.authcode</AuthCode><AuthTkt>$req.ticket</AuthTkt><Region>$req.region</Region>"
												+ "</Request></MSSettle>";											
	
	
	private static String getAafesServiceRequest(AafesRequestVO aafesRequestVO) throws AafesServiceClientException{
	
		VelocityContext context = new VelocityContext();
        context.put("req", aafesRequestVO);
        StringWriter swOut = new StringWriter();
        try {
         VelocityEngine ve = new VelocityEngine(); 
         ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		 ve.evaluate( context, swOut, "log tag name", getTemplateByRequestType(aafesRequestVO.getRequestType()));
			
		} catch (AafesServiceClientException e) {
			logger.error("Error(Exception) parsing aafes template !!"+e.getMessage() );
			throw e;
			
		} catch (Exception e) {
			logger.error("Error(Exception) parsing aafes template !!"+e.getMessage() );
			throw new AafesServiceClientException("Error(Exception) parsing aafes template !!"+e.getMessage());
		}		
        //logger.debug(swOut.toString());		
		return swOut.toString();
	}

	

	public  String processAafesRequest(AafesRequestVO requestVo, String aafes_service_url)throws Exception {
		String response="";		
		String requestType=requestVo.getRequestType();
		
		try{
			 logger.info("request=="+requestVo.toString());			 
			 String operationNm="";
			 long startTime = (new Date()).getTime();
			 
			 if(requestVo.isAafesTestMode()){
				 String testParams[]=getTestResponse(requestType,requestVo.getAafesTestResponse());
				 if(testParams!=null && testParams.length >1){
						 response =testParams[0];
						 operationNm=testParams[1];
				 }else{
					 logger.error("AAFES Test global parameters are not set properly !!");
				 }
			 }else{
				 
				 String request=getAafesServiceRequest(requestVo);
				 URL wsdlURL = new URL(aafes_service_url);	
				 MilitaryStar ms=new MilitaryStar(wsdlURL);
				 
					if(REQ_TYPE_APPROVAL.equalsIgnoreCase(requestType)){
						response =  ms.getMilitaryStarSoap().msApproval(request);
						operationNm="MSApproval";
					}
					else if (REQ_TYPE_CREDIT.equalsIgnoreCase(requestType)){
						response =  ms.getMilitaryStarSoap().msCredit(request);
						operationNm="MSCredit";
					}
					else if (REQ_TYPE_SETTLE.equalsIgnoreCase(requestType)){
						response =  ms.getMilitaryStarSoap().msSettlement(request);
						operationNm="MSSettle";
					}
			 }
					if(response!=null && response.length() >0){
						logger.info("Logging time stamp to log table !!!");						
						logServiceResponseTracking(requestVo.getReqId(),operationNm,startTime);
						 if(! requestVo.isAafesTestMode()){
							 response=getFormattedAafesResponse(response);		
						 }
					}	
					
//		      logger.info("Request==>"+request); 
		      logger.info("Response=="+response);
		 }catch(Exception e){
		     	e.printStackTrace();
		     	logger.error("Error, processing aafes"+e.getMessage());
		 }
		
		return response;
	}
	
		
	private static String getTemplateByRequestType(String requestType) throws AafesServiceClientException{
		String template="";
		if(REQ_TYPE_APPROVAL.equalsIgnoreCase(requestType))
			template=msApprovalReqTemplate;
		else if (REQ_TYPE_CREDIT.equalsIgnoreCase(requestType))
			template=msCreditReqTemplate;
		else if (REQ_TYPE_SETTLE.equalsIgnoreCase(requestType))
			template=msSettleReqTemplate;
		else 
			throw new AafesServiceClientException("Invalid request type");
		
	return template;
	}	
	
	
	private static String[] getTestResponse(String requestType, String aafesResponnseParameters){
		
		
			StringTokenizer stk=new StringTokenizer(aafesResponnseParameters,"|");
			String resParam[]=new String[3];
			
			
			for(int i=0;i < 3;i++){
				resParam[i]=stk.nextToken();
			}

		
		logger.info("global TEST param response"+resParam[0]+":"+resParam[1]+":"+resParam[2]);
		String[] template=new String[2];
		try{
				if(resParam!=null && resParam.length>0){
					if(REQ_TYPE_APPROVAL.equalsIgnoreCase(requestType)){
						template[0]=resParam[0];
						template[1]="MSApproval TEST";
					}else if (REQ_TYPE_CREDIT.equalsIgnoreCase(requestType)){
						template[0]=resParam[1];
						template[1]="MSCredit TEST";
					}else if (REQ_TYPE_SETTLE.equalsIgnoreCase(requestType)){
						template[0]=resParam[2];
						template[1]="MSSettle TEST";
				}
			}else{
				logger.error("AAFES Test Response Params are not set properly, in the global param !!!");
			}
		}catch(ArrayIndexOutOfBoundsException e){
			logger.error("AAFES Test Response Parameters are not set properly, in the global param !!!");
		}catch(Exception e){
			logger.error("ERROR :AAFES Test Response Parameters are not set properly, in the global param !!!");
		}
		
		logger.info("response TEST template"+template[0]+":"+template[1]);
		return template;
	}
	
	
		 private static String getFormattedAafesResponse(String personalizationStr){
		 
			 String responseString="";
			 try {
				 DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				 InputSource is=new InputSource(new StringReader(personalizationStr));
				 Document doc;
					doc = builder.parse(is);
				 doc.getDocumentElement().normalize();			 
				 NodeList nList = doc.getElementsByTagName(TAG_NM_RESPONSE);
		
				 for (int temp = 0; temp < nList.getLength(); temp++) {
				     Element element = (Element) nList.item(temp);
				     	responseString +=appendAafesResponse(element,TAG_NM_RETURN_CODE,1);
				     	responseString +=DELM_COMMA_SPACE+appendAafesResponse(element,TAG_NM_AUTH_CODE,6);
				     	responseString +=DELM_COMMA_SPACE+appendAafesResponse(element,TAG_NM_AUTH_TKT,14);
				     	responseString +=DELM_COMMA_SPACE+appendAafesResponse(element,TAG_NM_RETURN_MESSAGE,1);
				     	
				     }
			 	} catch (SAXException e) {
			 		logger.error("Error(SAXException) parsing personalization info !!"+e.getMessage());
				} catch (IOException e) {
					logger.error("Error(IOException) parsing personalization info !!"+e.getMessage());
				} catch (ParserConfigurationException e) {
					logger.error("Error(ParserConfigurationException) parsing personalization info !!"+e.getMessage());
				}
				
			// logger.info("responseString"+responseString);
		 return responseString;
	}



		private static String appendAafesResponse(Element element, String tagname,int len) {
			String returnCode="";
			NodeList name = element.getElementsByTagName(tagname);
			             Element line = (Element) name.item(0);
			             
			            if (line != null) {
							returnCode = line.getTextContent();				
						}else{
							returnCode=padLeftSpaces(returnCode,len);
						}
			             
			return returnCode;
		}



	
	public static String getRequestIdString(String ReqType){
		String redId="";
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmmssSSS");
		redId=ReqType+formatter.format(new Date());
		return redId;
	}
	
	
private static String padLeftSpaces(String str, int n) {
	String retVal="";
	if(str.length() < n){
		retVal=String.format("%1$" + (n- str.length())+ "s", str);
	}
    return retVal;
  }



private void logServiceResponseTracking(String requestId,String serviceMethod, long startTime) {
	
	 Connection con=null;
	try {
		
		logger.info("logServiceResponseTracking!!!");		
		con=DataSourceUtil.getInstance().getConnection(ORDER_SCRUB);
		long responseTime = System.currentTimeMillis() - startTime;
		ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
		srtVO.setServiceName(AAFES_SERVICE);
		srtVO.setServiceMethod(serviceMethod);
		srtVO.setTransactionId(requestId);
		srtVO.setResponseTime(responseTime);
		srtVO.setCreatedOn(new Date());
		ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
		srtUtil.insert(con, srtVO);
	} catch (Exception e) {
		logger.error("Error in logServiceResponseTracking"+e.getMessage());
		logger.error("Exception occured while persisting AAFES service response times.");
	}
	   finally
	    {
	      if (con != null)
	      {
	        try
	        {
	          con.close();
	        }
	        catch (SQLException sx)
	        {
	          logger.error(sx);
	        }
	      }
	    }
	
}
  
}
