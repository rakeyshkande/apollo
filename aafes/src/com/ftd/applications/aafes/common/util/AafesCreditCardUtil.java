package com.ftd.applications.aafes.common.util;

import java.io.*;
import java.net.*;
import java.security.Security;

import javax.net.ssl.HttpsURLConnection;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
     * AafesCreditCardUtil
     * This Class sends and receives messages for Military Star Charge Cards.
     */
public class AafesCreditCardUtil 
{
	private static PrintWriter out;
	private static BufferedReader in;
        private static Logger logger = new Logger("com.ftd.applications.aafes.common.util.AafesCreditCardUtil");
	
    /**
    * Constructor for the AafesCreditCardUtil
    *
    */
    public AafesCreditCardUtil()
    {
        out = null;
        in = null;
    }
    
   /**
   * Send data.
   *
   * @param java.lang.String - packet to be sent
   * @return String - Response Packet
   */   
    public static String send(String transmitData) throws Exception
    {
        String upsResponse = null;  
        ConfigurationUtil cu = ConfigurationUtil.getInstance();    
        HttpURLConnection connection = null;
        
        try
        {
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
      
            /**
             * We no longer need to specify com.sun.net.ssl as the ssl handler package since javax.net.ssl is 
             * default and the correct one to use:
             * 
             * http://download.oracle.com/javase/6/docs/technotes/guides/security/jsse/JSSERefGuide.html
             * 
             * Note to previous JSSE users: In past Sun JSSE releases, you had to set the java.protocol.handler.pkgs 
             * system property during JSSE installation. This step is no longer required unless you wish to obtain an 
             * instance of com.sun.net.ssl.HttpsURLConnection. For more information, see Code Using 
             * HttpsURLConnection Class... in the Troubleshooting section.
             * 
             */
            //System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");
            URL url = new URL(cu.getFrpGlobalParm("AAFES_CONFIG", "aafes.auth.default.server"));
            logger.info("Sending request to:" + url);
            
            // Send data
            connection = (HttpURLConnection) url.openConnection();
            
            // Setup HTTP POST parameters
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
           
            // POST data
            OutputStream out = connection.getOutputStream();
            out.write(transmitData.getBytes());
            out.close();  

            // get data from URL connection
            upsResponse = readURLConnection(connection);
        }
        catch (Exception e)
        {
            logger.error(e);
            throw new Exception("Unable to retrieve credit card authorization");
        }
        finally
        {
            if(connection != null) {
                connection.disconnect();
            }
        }
      
        return upsResponse;
    }
    
    private static String readURLConnection(URLConnection uc) throws Exception
    {
        StringBuffer buffer = new StringBuffer();
        StringBuffer logbuffer = new StringBuffer();
        BufferedReader reader = null;

        String strValue = "Y";

        boolean bDebugLog;
        if( strValue==null || !strValue.equals("Y") ) 
          bDebugLog=false;
        else
          bDebugLog=true;

        try
        {
            reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String line = null;
            int letter = 0;
            while ((letter = reader.read()) != -1)
            {
                buffer.append((char) letter);
                
                if( bDebugLog==true ) {
                  logbuffer.append(Integer.toHexString(letter));
                  logbuffer.append(" ");
                }
            }
	    /******************** Defect 1819 ****************
		// no need to log the following. Has credit card number
		// same information already in AafesCreditCardServlet

            if( bDebugLog==true ) {
              	Util util = new Util();
            	util.getLogManager().info(logbuffer.toString());
            	util.getLogManager().info(buffer.toString());
            }
	    ********************* End Defect 1819 ***********/
        }
        catch (Exception e)
        {
            logger.error(e);
            throw new Exception("Unable to read credit card authorization retrieved");
        }
        finally
        {
            try
            {
                reader.close();
            }
            catch (IOException io)
            {
                logger.error(io);
                throw new Exception("Unable to close URLReader");
            }
        }
        return buffer.toString();
    }   
}
