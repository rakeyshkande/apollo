package com.ftd.applications.aafes.vo;

import java.io.Serializable;

/**
 * @author kkeyan
 *
 */
public class AafesRequestVO implements Serializable{

	
	private static final int SHOW_SIZE = 4;
	private static final String CC_MASK = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"; 
	private String requestType;
	private String ccNumber;	
	private String amount;	
	private String facNbr;
	private String authcode;
	private String ticket;
	private String cid;
	private String zip;
	private String region;
	private String reqId; 
	private boolean aafesTestMode;
	private String aafesTestResponse;
	
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getCcNumber() {
		return ccNumber;
	}
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getFacNbr() {
		return facNbr;
	}
	public void setFacNbr(String facNbr) {
		this.facNbr = facNbr;
	}
	public String getAuthcode() {
		return authcode;
	}
	public void setAuthcode(String authcode) {
		this.authcode = authcode;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	
	public boolean isAafesTestMode() {
		return aafesTestMode;
	}
	public void setAafesTestMode(boolean aafesTestMode) {
		this.aafesTestMode = aafesTestMode;
	}
	public String getAafesTestResponse() {
		return aafesTestResponse;
	}
	public void setAafesTestResponse(String aafesTestResponse) {
		this.aafesTestResponse = aafesTestResponse;
	}
	@Override
	public String toString() {
		return "AafesRequestVO [requestType=" + requestType + ", ccNumber=" + maskCCNumber(ccNumber) + ", amount=" + amount
				+ ", facNbr=" + facNbr + ", authcode=" + authcode + ", ticket=" + ticket + ", cid=" + cid + ", zip="
				+ zip + ", region=" + region + ", reqId=" + reqId + ", aafesTestMode=" + aafesTestMode
				+ ", aafesTestResponse=" + aafesTestResponse + "]";
	}
	

	private String maskCCNumber(String ccNumber){
		
		String data="";
		 if(ccNumber.length() > SHOW_SIZE) {
	         data =  CC_MASK.substring(0, ccNumber.length() - SHOW_SIZE) // credit card masked except for last 4 digit
	                 + ccNumber.substring(ccNumber.length() - SHOW_SIZE); // credit card last 4 digit
	                
	    }
		 return data;
	}
	
}
