package com.ftd.applications.aafes.presentation;

import java.text.SimpleDateFormat;

import java.util.Calendar;


public class Util
{
    //private LogManager lm = null;
    private static final String CC_MASK = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"; 
    private static int SHOW_SIZE = 4;

    public Util()
    {
    }

   public static String maskCreditCard(String data, String sIndicator, String eIndicator) {
        // find start position of cc number
        int ccStartPos = data.indexOf(sIndicator) + sIndicator.length();
        int ccEndPos = data.indexOf(eIndicator);

        if(data.indexOf(sIndicator) > 0 && ccEndPos > 0) {
            String ccNumber = data.substring(ccStartPos, ccEndPos);
            if(ccNumber.length() > SHOW_SIZE) {
                 data = data.substring(0, ccStartPos) // data before credit card
                         + CC_MASK.substring(0, ccNumber.length() - SHOW_SIZE) // credit card masked except for last 4 digit
                         + ccNumber.substring(ccNumber.length() - SHOW_SIZE) // credit card last 4 digit
                         + data.substring(data.indexOf(eIndicator));
            }
        }
        
        return data;
    }

}
