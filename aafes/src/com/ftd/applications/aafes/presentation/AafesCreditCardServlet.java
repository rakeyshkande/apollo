// Decompiled by DJ v3.6.6.79 Copyright 2004 Atanas Neshkov  Date: 1/10/2007 6:26:00 PM
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AafesCreditCardServlet.java

package com.ftd.applications.aafes.presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.applications.aafes.common.util.AafesServiceClientUtil;
import com.ftd.applications.aafes.vo.AafesRequestVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;


// Referenced classes of package com.ftd.applications.aafes.presentation:
//            Util

public class AafesCreditCardServlet extends HttpServlet
{
    private static Logger logger = new Logger("com.ftd.applications.aafes.presentation.AafesCreditCardServlet");
    public AafesCreditCardServlet()
    {
    }
    
    private String aafes_service_url;
    private String aafes_region;
    private String aafes_test_mode;
    private String aafes_test_responses;

    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
        try {
        	ConfigurationUtil cu=ConfigurationUtil.getInstance();
			aafes_service_url=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_service_url");
			aafes_region=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_region");
			aafes_test_mode=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_test_mode");
			aafes_test_responses=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_test_responses");
		} catch (Exception e) {
			logger.error("Error initializing aafes servlet"+e.getMessage());
			//e.printStackTrace();
		}
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
    	
    	if(aafes_service_url==null || aafes_region ==null){    	
    		initAafesParams();
    	}
    	
    	
        Util util = new Util();
        PrintWriter out2 = null;
        AafesRequestVO requestVo=new AafesRequestVO();
        String aafesResponse ="";
        
        logger.debug("processing doGet method");
        String type = "";
        if(request.getParameter("type") != null)
            type = request.getParameter("type");
        
        String ccnumber = "";
        if(request.getParameter("ccnumber") != null)
            ccnumber = request.getParameter("ccnumber");
        String amount = "";
        if(request.getParameter("amount") != null)
            amount = request.getParameter("amount");
        String facnbr = "";
        if(request.getParameter("facnbr") != null)
            facnbr = request.getParameter("facnbr");
        
        String authcode = "";
        if(request.getParameter("authcode") != null)
            authcode = request.getParameter("authcode");
        String ticket = "";
        if(request.getParameter("ticket") != null)
            ticket = request.getParameter("ticket");
        
        String cid = "";
        if(request.getParameter("cid") != null)
            cid = request.getParameter("cid");
        
        String zip = "";
        if(request.getParameter("zip") != null)
        	zip = request.getParameter("zip");
        
        requestVo.setRequestType(type);
        requestVo.setCcNumber(ccnumber);
        requestVo.setAmount(amount);
        requestVo.setFacNbr(facnbr);
        requestVo.setAuthcode(authcode);
        requestVo.setTicket(ticket);
        requestVo.setZip(zip);
        requestVo.setCid(cid);
        requestVo.setRegion(aafes_region);
        requestVo.setReqId(AafesServiceClientUtil.getRequestIdString(type));
     
        try
        {
        	
        	if(aafes_test_mode !=null && aafes_test_mode.equals("Y")){        		
        		logger.warn("AAFES test mode is enabled !!");
        		requestVo.setAafesTestMode(true);
        		requestVo.setAafesTestResponse(aafes_test_responses);
        	}
	        	
        	
        	AafesServiceClientUtil aafesServiceClient=new AafesServiceClientUtil();
	        aafesResponse =  aafesServiceClient.processAafesRequest(requestVo,aafes_service_url);
	        logger.debug("complete send method");
        	
            response.setContentType("text/html; charset=windows-1252");
            out2 = response.getWriter();
            out2.write(aafesResponse);
            logger.debug(aafesResponse);
        }
        catch(Throwable e)
        {
            logger.error(e.toString(), e);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/systemerror.htm");
            if(dispatcher != null)
                dispatcher.forward(request, response);
        }
        finally {
            try {
                out2.flush();
            } catch (NullPointerException npe) {
                //OK
            }
        }
        
    }

	private void initAafesParams() {
		try {
			ConfigurationUtil cu=ConfigurationUtil.getInstance();
			aafes_service_url=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_service_url");
			aafes_region=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_region");
			aafes_test_mode=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_test_mode");
			aafes_test_responses=cu.getFrpGlobalParm("AAFES_CONFIG", "aafes_test_responses");
		} catch (Exception e) {
			
		}
	}
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
}