@ECHO OFF  
 REM Locations: Change these* to *match your environment 
 set FTD_LIB_DIR=C:\FTD\lib
   REM Create the class path 
 set AXISCP=.
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\axis-1.4.jar
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\jaxrpc.jar
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\mail.jar
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\saaj.jar
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\commons-logging-1.0.4.jar
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\commons-discovery-0.2.jar
 set AXISCP=%AXISCP%;%FTD_LIB_DIR%\wsdl4j-1.5.1.jar
 ECHO set up classpath 
 ECHO Should now be able to call 
 ECHO     adminclient, tcpmon, wsdl2java, java2wsdl ... 
  