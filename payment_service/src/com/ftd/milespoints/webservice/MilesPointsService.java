/**
 * 
 */
package com.ftd.milespoints.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.ftd.milespoints.webservice.vo.MilesPointsException;
import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.milespoints.webservice.vo.PartnerResponse;
import com.ftd.milespoints.webservice.vo.UpdateMilesResp;

/**
 * @author cjohnson
 *
 */
@WebService
public interface MilesPointsService {
	
	public PartnerResponse authenticateMemberCheckMiles(@WebParam(name="milesPointsRequest")MilesPointsRequest membership) throws MilesPointsException;
	
	public PartnerResponse checkMiles(@WebParam(name="milesPointsRequest")MilesPointsRequest membership) throws MilesPointsException;
	
	public UpdateMilesResp updateMiles(@WebParam(name="milesPointsRequest")MilesPointsRequest membership) throws MilesPointsException;
	

}
