/**
 * 
 */
package com.ftd.milespoints.webservice;

import javax.jws.WebService;

import org.springframework.stereotype.Service;

import com.ftd.milespoints.common.bo.IMilesPointsBO;
import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.partner.bo.UAMilesPointsBOImpl;
import com.ftd.milespoints.webservice.vo.MilesPointsException;
import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.milespoints.webservice.vo.PartnerResponse;
import com.ftd.milespoints.webservice.vo.UpdateMilesResp;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.util.CommonUtil;
import com.ftd.ps.webservice.PaymentServiceBase;

/**
 * @author cjohnson
 *
 */
@Service
@WebService(endpointInterface = "com.ftd.milespoints.webservice.MilesPointsService")
public class MilesPointsServiceImpl extends PaymentServiceBase implements MilesPointsService {
	
	Logger logger = new Logger(MilesPointsServiceImpl.class.getName());

	@Override
	public PartnerResponse authenticateMemberCheckMiles(
			MilesPointsRequest request) throws MilesPointsException {
		
		
		try {
			IMilesPointsBO mpbo = getMilesPointsBOImpl(request.getMembershipType());
			return mpbo.processAuthenticateMemberCheckMiles(request);
		} catch (Throwable e) {
			throw new MilesPointsException(e);
		}
	}

	@Override
	public PartnerResponse checkMiles(MilesPointsRequest request) throws MilesPointsException {
		
		CommonUtil util = new CommonUtil();
		
		//Today we only have United
		try {
			if(!checkClient(request.getClientUserName(), request.getClientPassword(), util)) {
				logger.warn(" Invalid client credentials detected");
				return new PartnerResponse();
			}
			IMilesPointsBO mpbo = getMilesPointsBOImpl(request.getMembershipType());
			
			PartnerResponse errorResponse = mpbo.validateCheckMiles(request);
			if (errorResponse != null) {
				return errorResponse;
			}
			
			return mpbo.processCheckMiles(request);
		} catch (Throwable e) {
			util.sendNoPageSystemMessage("Exception caught in MilesPointsServlet::doPost." + e.getMessage());
			throw new MilesPointsException(e);
		}
	}

	@Override
	public UpdateMilesResp updateMiles(MilesPointsRequest request) throws MilesPointsException {
		
		CommonUtil util = new CommonUtil();
		try {		
			if(!checkClient(request.getClientUserName(), request.getClientPassword(), util)) {
				logger.warn(" Invalid client credentials detected");
				return new UpdateMilesResp();
			}
			IMilesPointsBO mpbo = getMilesPointsBOImpl(request.getMembershipType());
			
			PartnerResponse errorResponse = mpbo.validateUpdateMiles(request);
			if (errorResponse != null) {
				//return errorResponse;
			}
			
			return mpbo.processUpdateMiles(request);
		
		}catch(Throwable e) {
			util.sendNoPageSystemMessage("Exception caught in MilesPointsServlet::doPost." + e.getMessage());
			throw new MilesPointsException(e);
		}
	}
	
	
    /**
     * Returns the IMilesPointsBO implementation class. The factory assumes that the implementation
     * class are named in this format:
     * 1. Resides in package: com.ftd.milespointsservices.partner.service
     * 2. Class name starts with membership type. i.e. UA for United
     * 3. Class name ends with MilesPointsBOImpl.
     * @param membershipType
     * @return
     * @throws Throwable
     */
    public IMilesPointsBO getMilesPointsBOImpl(String membershipType) throws Throwable 
    {
        IMilesPointsBO serviceImpl = null;
        String className = null;
        
        try {
            className = MilesPointsConstants.BO_CLASS_NAME_PREFIX + 
                        membershipType +
                        MilesPointsConstants.BO_CLASS_NAME_SUFFIX;
                        
            serviceImpl = (IMilesPointsBO) Class.forName(className).newInstance();
        } finally {
        }
        return serviceImpl;
    }  

}
