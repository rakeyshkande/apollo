package com.ftd.milespoints.webservice.vo;

import com.ftd.milespoints.common.util.CommonUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Top level object for the miles points request.
 * 
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package like com.ftd.milespoints.webservice.vo.v1
 */
public class MilesPointsRequest
{
    protected String clientUserName;
    protected String clientPassword;
    protected String membershipType;    // Identified by payment method id. UA for United Redemption.
    protected String membershipNumber;  
    protected String password;
    protected int totalMilesPoints;     // Widely agreed that generally there are no decimal points to all programs.
    protected int milesPointsRequested;        // Miles to be checked or updated.
    protected String updateMileIndicator;  // Indicate if deduct miles or credit miles.
    protected String paymentId;            // Input paymentId for the request.
    protected boolean returnBooleanResult; // Use this field to convey boolean result.
    protected String returnErrorCode;     // Use this field to convey error code.
    protected String returnErrorMessage;   // Use this field to convey error result associated with failure boolean result.
    protected String returnStringResult;   // Use this field to convey string result.

    public MilesPointsRequest() {}
    
    public MilesPointsRequest(MilesPointsRequest v) {
        this.membershipType = v.membershipType;
        this.membershipNumber = v.membershipNumber;
        this.password = v.password;
        this.totalMilesPoints = v.totalMilesPoints;
        this.milesPointsRequested = v.milesPointsRequested;
        this.updateMileIndicator = v.updateMileIndicator;
        this.paymentId = v.paymentId;
        this.returnBooleanResult = v.returnBooleanResult;
        this.returnErrorCode = v.returnErrorCode;
        this.returnErrorMessage = v.returnErrorMessage;
        this.returnStringResult = v.returnStringResult;
    }
     
    public String objectToString() throws Exception {
        return "membershipType=" + membershipType + ";" +
               "membershipNumber=" + membershipNumber==null? "":(membershipNumber.length()>4? membershipNumber.substring(membershipNumber.length()-4) : membershipNumber) + ";";
    }
     
    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setTotalMilesPoints(int totalMilesPoints) {
        this.totalMilesPoints = totalMilesPoints;
    }

    public int getTotalMilesPoints() {
        return totalMilesPoints;
    }

    public void setMilesPointsRequested(int milesPointsRequested) {
        this.milesPointsRequested = milesPointsRequested;
    }

    public int getMilesPointsRequested() {
        return milesPointsRequested;
    }

    public void setReturnErrorMessage(String returnErrorMessage) {
        this.returnErrorMessage = returnErrorMessage;
    }

    public String getReturnErrorMessage() {
        return returnErrorMessage;
    }

    public void setReturnBooleanResult(boolean returnBooleanResult) {
        this.returnBooleanResult = returnBooleanResult;
    }

    public boolean isReturnBooleanResult() {
        return returnBooleanResult;
    }

    public void setUpdateMileIndicator(String updateMileIndicator) {
        this.updateMileIndicator = updateMileIndicator;
    }

    public String getUpdateMileIndicator() {
        return updateMileIndicator;
    }

    public void setReturnStringResult(String returnStringResult) {
        this.returnStringResult = returnStringResult;
    }

    public String getReturnStringResult() {
        return returnStringResult;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setReturnErrorCode(String returnErrorCode) {
        this.returnErrorCode = returnErrorCode;
    }

    public String getReturnErrorCode() {
        return returnErrorCode;
    }

	public void setClientUserName(String clientUserName) {
		this.clientUserName = clientUserName;
	}

	public String getClientUserName() {
		return clientUserName;
	}

	public void setClientPassword(String clientPassword) {
		this.clientPassword = clientPassword;
	}

	public String getClientPassword() {
		return clientPassword;
	}
}
