package com.ftd.milespoints.webservice.vo;

import com.ftd.milespoints.common.util.CommonUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * This class encapsulate a membership object used in miles / points services.
 * 
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package
 * 
 */
public class PartnerResponse
{
    // fields to be returned in XML
    protected String membershipType;    // Identified by payment method id. UA for United Redemption.
    protected String membershipNumber; 
    protected String authSuccess;
    protected String authErrorCode;
    protected String conSuccess;
    protected String generalMember;
    protected String milesVerify;     
    protected String transactionId;
	protected String externalConfirmationNumber;
    
    // fields not returned in XML
    protected String errorMessage;
    
    public PartnerResponse () {
        this.setAuthSuccess("N");
        this.setGeneralMember("N");
        this.setConSuccess("Y");
        this.setMilesVerify("N");
    }     
    
    public String toXMLString() throws Exception {
        Document doc = JAXPUtil.createDocument();
        CommonUtil commonUtil = new CommonUtil();
        
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "mpServiceResponse", "");
        doc.appendChild(root);
        
        Element newElement = JAXPUtil.buildSimpleXmlNode(doc,"membershipType", this.getMembershipType()==null? "" : this.getMembershipType());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"membershipNumber", this.getMembershipNumber()==null? "" : this.getMembershipNumber());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"authSuccess", this.getAuthSuccess()==null? "N" : this.getAuthSuccess());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"authErrorCode", this.getAuthErrorCode()==null? "" : this.getAuthErrorCode());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"conSuccess", this.getConSuccess()==null? "N" : this.getConSuccess());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"generalMember", this.getGeneralMember()==null? "N" : this.getGeneralMember());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"milesVerify", this.getMilesVerify()==null? "N" : this.getMilesVerify());
        root.appendChild(newElement);   
        return commonUtil.outputXml(doc);
    }   
    
    public static PartnerResponse stringToObject(String s) throws Exception {
        PartnerResponse prvo = new PartnerResponse();
        if(s != null) {
            Document doc = (Document)JAXPUtil.parseDocument(s);
            Node n;
            
            n = DOMUtil.selectSingleNode(doc, "/mpServiceResponse/membershipType");
            if (n != null && n.getFirstChild() != null) {
                prvo.setMembershipType(n.getFirstChild().getNodeValue());
            }
            
            n = DOMUtil.selectSingleNode(doc, "/mpServiceResponse/membershipNumber");
            if (n != null && n.getFirstChild() != null) {
                prvo.setMembershipNumber(n.getFirstChild().getNodeValue());
            }
            
            n = DOMUtil.selectSingleNode(doc, "/mpServiceResponse/authErrorCode");
            if (n != null && n.getFirstChild() != null) {
                prvo.setAuthErrorCode(n.getFirstChild().getNodeValue());
            }
            
            n = DOMUtil.selectSingleNode(doc, "/mpServiceResponse/conSuccess");
            if (n != null && n.getFirstChild() != null) {
                prvo.setConSuccess(n.getFirstChild().getNodeValue());
            }
            
            n = DOMUtil.selectSingleNode(doc, "/mpServiceResponse/milesVerify");
            if (n != null && n.getFirstChild() != null) {
                prvo.setMilesVerify(n.getFirstChild().getNodeValue());
            }
        }
        return prvo;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public void setAuthSuccess(String authSuccess) {
        this.authSuccess = authSuccess;
    }

    public String getAuthSuccess() {
        return authSuccess;
    }

    public void setAuthErrorCode(String authErrorCode) {
        this.authErrorCode = authErrorCode;
    }

    public String getAuthErrorCode() {
        return authErrorCode;
    }

    public void setConSuccess(String conSuccess) {
        this.conSuccess = conSuccess;
    }

    public String getConSuccess() {
        return conSuccess;
    }

    public void setGeneralMember(String generalMember) {
        this.generalMember = generalMember;
    }

    public String getGeneralMember() {
        return generalMember;
    }

    public void setMilesVerify(String milesVerify) {
        this.milesVerify = milesVerify;
    }

    public String getMilesVerify() {
        return milesVerify;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
	
    public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getExternalConfirmationNumber() {
		return externalConfirmationNumber;
	}

	public void setExternalConfirmationNumber(String externalConfirmationNumber) {
		this.externalConfirmationNumber = externalConfirmationNumber;
	}

}
