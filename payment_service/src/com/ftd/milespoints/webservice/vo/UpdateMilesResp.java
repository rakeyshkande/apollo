package com.ftd.milespoints.webservice.vo;

public class UpdateMilesResp {
	
   
	private String transactionId;
	private String externalConfirmationNumber;
	private String bonusCode;
	private String bonusType;
	private String loyaltyProgramNumber;
	private String postedAccountNumber;
	private String accountBalance;
	private String returnBooleanResult;
	
	 public String getTransactionId() {
			return transactionId;
		}
		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}
		public String getExternalConfirmationNumber() {
			return externalConfirmationNumber;
		}
		public void setExternalConfirmationNumber(String externalConfirmationNumber) {
			this.externalConfirmationNumber = externalConfirmationNumber;
		}
		public String getBonusCode() {
			return bonusCode;
		}
		public void setBonusCode(String bonusCode) {
			this.bonusCode = bonusCode;
		}
		public String getBonusType() {
			return bonusType;
		}
		public void setBonusType(String bonusType) {
			this.bonusType = bonusType;
		}
		public String getLoyaltyProgramNumber() {
			return loyaltyProgramNumber;
		}
		public void setLoyaltyProgramNumber(String loyaltyProgramNumber) {
			this.loyaltyProgramNumber = loyaltyProgramNumber;
		}
		public String getPostedAccountNumber() {
			return postedAccountNumber;
		}
		public void setPostedAccountNumber(String postedAccountNumber) {
			this.postedAccountNumber = postedAccountNumber;
		}
		public String getAccountBalance() {
			return accountBalance;
		}
		public void setAccountBalance(String accountBalance) {
			this.accountBalance = accountBalance;
		}
		public void setReturnBooleanResult(String returnBooleanResult) {
			this.returnBooleanResult = returnBooleanResult;
		}
		public String getReturnBooleanResult() {
			return returnBooleanResult;
		}

}
