/**
 * 
 */
package com.ftd.milespoints.webservice.vo;

/**
 * @author cjohnson
 *
 */
public class MilesPointsException extends Exception {
	
    public MilesPointsException(Throwable cause)
    {
        super(cause);
    }

}
