package com.ftd.milespoints.partner.test;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.common.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.ftd.partnersourcecode.test.united.TestUnited;


import java.util.ArrayList;
import java.util.List;

public class TestUnitedServlet extends HttpServlet
{

    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private Logger logger = new Logger("com.ftd.milespointsservices.common.servlet.MilesPointsServlet");
    private List accounts = null;
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        accounts = new ArrayList();

        accounts.add("03117162337");
        accounts.add("03117162981");
        accounts.add("03117162973");
        accounts.add("03133842518");
        accounts.add("03127793505");
        accounts.add("03117162264");
        accounts.add("03127793483");
        accounts.add("03128951490");
        accounts.add("03063873585");
        accounts.add("03170613693");
        accounts.add("03162405585");
        accounts.add("00465937777");
        accounts.add("03195907748");
        accounts.add("03150067714");
        accounts.add("03125446174");
        accounts.add("03178774157");
        accounts.add("03194865707");
        accounts.add("03157349936");
        accounts.add("03149927017");
        accounts.add("03130356035");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException
    {
        doPost(request, response);
    }
    
    /**
     * http://cobalt2.ftdi.com:7778/mps/TestUnitedServlet?function=authenticateMember&accountNumber=03129326696&password=qatest&miles=0
     * http://cobalt2.ftdi.com:7778/mps/TestUnitedServlet?function=getAll
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException
    {
    	/*
        try {
            PrintWriter out = response.getWriter();
            String function = request.getParameter("function");
            String accountNumber = request.getParameter("accountNumber");
            String password = request.getParameter("password");
            String miles = request.getParameter("miles");
            
            String host = null;
            String ks = null; //keystore
            String ts = null; //truststore
            String ksPassword = null;
            String tsPassword = null;
            
            CommonUtil util = new CommonUtil();
            // Retrieve host and keystore info.
            host = util.getGlobalParm("UA" + MilesPointsConstants._ENDPOINT_ENVIRONMENT);
            // This will require a change to ops admin to set up the actual cert location due to "value" field size limitation.
            // Hard code for 3.0.3 release and correct this in 3.1
            //ks = util.getSecureProperty(MilesPointsConstants.UA_SSL_KEYSTORE);
            //ts = util.getSecureProperty(MilesPointsConstants.UA_SSL_TRUSTSTORE);
            ks = "/u01/app/oracle/product/10gR3/jdk/jre/lib/security/apolloCertKeystore.jks";
            ts = "/u01/app/oracle/product/10gR3/jdk/jre/lib/security/apolloCertKeystore.jks";
            ksPassword = util.getSecureProperty(MilesPointsConstants.UA_SSL_KEYSTORE_PASSWORD);
            tsPassword = util.getSecureProperty(MilesPointsConstants.UA_SSL_TRUSTSTORE_PASSWORD);         
            
            TestUnited.setup(ks,ts,ksPassword,tsPassword);
            
            if(function == null) {
                function = "authenticateMember";
            }
                       
            if(function.equals("authenticateMember")) {
                out.println(TestUnited.testAuthenticate(host, accountNumber, password));
            } else if (function.equals("getMemberLevel")) {
                out.println(TestUnited.testGetMemberLevel(host, accountNumber));
            } else if(function.equals("getPartnerRegistrationFlag")) {
                out.println(TestUnited.testGetPartnerRegistrationFlag(host, accountNumber));
            } else if(function.equals("getTotalMiles")) {
                out.println(TestUnited.testGetTotalMiles(host,accountNumber));
            } else if(function.equals("updateMiles")) {
                out.println(TestUnited.testUpdateMiles(host,accountNumber, miles));
            } else if(function.equals("getAll")) {
                out.println(getAllMiles(host));
            }
            
            out.println("TestUnitedServlet");
            out.close();
        } catch (Throwable t) {
            logger.error("Failed to perform Test: " + t.getMessage(), t);
        }
        */
    }
    
    private String getAllMiles(String host) {
        String returnStr = "";
        if(accounts != null) {
            for(int i = 0; i < accounts.size(); i++) {
                try {
                    //returnStr += "Account Number:" + accounts.get(i) + " Miles: " + TestUnited.testGetTotalMiles(host,(String)accounts.get(i)) + "\n";
                } catch (Exception e) {
                    returnStr += "Account Number:" + accounts.get(i) + " - Miles cannot be retrieved due to error: " + e.getMessage() + "\n";
                }
            }
        }
        return returnStr;
    }
    
}    