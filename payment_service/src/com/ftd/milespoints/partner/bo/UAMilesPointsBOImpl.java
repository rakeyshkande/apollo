package com.ftd.milespoints.partner.bo;

import static com.ftd.milespoints.common.constant.MilesPointsConstants.ACCTG_CONFIG_CONTEXT;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.CONFIG_CONTEXT;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_BONUS_CODE;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_BONUS_TYPE;

import java.sql.Connection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;

import com.ftd.milespoints.common.bo.IMilesPointsBO;
import com.ftd.milespoints.common.bo.MilesPointsBOBase;
import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.common.factory.MilesPointsObjectFactory;
import com.ftd.milespoints.common.service.IMilesPointsServices;
import com.ftd.milespoints.common.util.CommonUtil;
import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.milespoints.webservice.vo.PartnerResponse;
import com.ftd.milespoints.webservice.vo.UpdateMilesResp;
import com.ftd.osp.utilities.id.IdGenerator;
import com.ftd.osp.utilities.id.IdGeneratorFactory;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.util.WebServiceClientFactory;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import com.ual.cust.ptvl.isstkt.prodsvc.GetPartnerProfilePortType;
import com.ual.cust.ptvl.isstkt.prodsvc.ItineraryNbrType;
import com.ual.cust.ptvl.isstkt.prodsvc.ItineraryNbrType.PNR;
import com.ual.cust.ptvl.isstkt.prodsvc.MemberSummaryType;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerProfile;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerRetrvProfile;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesPort;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesResponse;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerRetrvProfile.ProgramMembership;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.LoyaltyProgram;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Passenger;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesRequest;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Pricing;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Redemption;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Travel;
import com.ual.cust.ptvl.isstkt.prodsvc.PricingType.OrderData;
import com.ual.cust.ptvl.isstkt.prodsvc.PricingType.TaxInfo;
import com.ual.cust.ptvl.isstkt.prodsvc.TravelType.FirstTravelItem;
import com.ual.cust.ptvl.isstkt.prodsvc.ProgramMembershipType;


/**
 * This is the United Mileage Plus implementation of the miles / points business object.
 * The business object is responsible for validating request parameters and fulfilling
 * the request.
 */

public class UAMilesPointsBOImpl extends MilesPointsBOBase implements IMilesPointsBO
{
    private Logger logger = new Logger("com.ftd.milespointsservices.partner.bo.UAMilesPointsBOImpl");
    
    WebServiceClientFactory wscf;
    CommonUtil util;

	private static final String UA_PARTNER_CODE = "UA_PARTNER_CODE";
    
    public UAMilesPointsBOImpl() {
    	wscf = new WebServiceClientFactory();
    	util = new CommonUtil();
    }
    
    /**
     * constructor that allows us to inject a wscf
     * @param wscf
     */
    public UAMilesPointsBOImpl(WebServiceClientFactory wscf, CommonUtil util) {
    	this.wscf = wscf;
    	this.util = util;
    }

    /**
     * This is the "validate" method for requestServiceName "AuthenticateMemberCheckMiles".
     * The following rules are checked:
     * 1. membershipType is not null
     * 2. membershipNumber is not null
     * 3. password is not null
     * 4. milesPointsAmt is greater than 0
     * 
     * @param membershipvo
     * @return
     * @throws Throwable
     */
    public PartnerResponse validateAuthenticateMemberCheckMiles(MilesPointsRequest membershipvo) throws Throwable 
    {
        String errorMessage = "";

        String membershipType = membershipvo.getMembershipType();
        String membershipNumber = membershipvo.getMembershipNumber();
        String password = membershipvo.getPassword();
        int milesPointsAmt = membershipvo.getMilesPointsRequested();

        if(membershipType == null || membershipType.equals("")) {
            
            errorMessage = MilesPointsConstants.ERROR_INVALID_MEMBER_TYPE_CODE;
        }
        else if(membershipNumber == null || membershipNumber.equals("")) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MEMBERSHIP_NUMBER_CODE;
        }
        else if(password == null || password.equals("")) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_PASSWORD_CODE;
        }
        else if(milesPointsAmt < 0) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MILES_POINTS_CODE;
        }
        
        if(errorMessage != null && errorMessage.length() > 0) {
            PartnerResponse prvo = new PartnerResponse();
            prvo.setAuthErrorCode(errorMessage);
            return prvo;
        }
        
        return null;
        
    }

    /**
     * This is the "process" method for requestServiceName "AuthenticateMemberCheckMiles".
     * It invokes the service functionality to authenticate member and check miles.
     * 
     * @param membershipvo
     * @return
     * @throws Throwable
     */
    public PartnerResponse processAuthenticateMemberCheckMiles(MilesPointsRequest membershipvo) throws Throwable 
    {
    	PartnerRetrvProfile retrvProfile = new PartnerRetrvProfile();
    	
    	//create a request based on the membershipVO.
    	ProgramMembership membership = new ProgramMembership();
    	membership.setMemberID(membershipvo.getMembershipNumber());
    	retrvProfile.setProgramMembership(membership);
    	retrvProfile.setVersion("3.0.0");
    	
    	//make the remote call
    	GetPartnerProfilePortType client = wscf.getUnitedRetreiveProfileService();
    	PartnerProfile profile = client.getPartnerProfileOperation(retrvProfile);
    	
    	//create the response based on the united response
    	PartnerResponse prvo = new PartnerResponse();
    	List<ProgramMembershipType> membershipTypes = profile.getProgramMembership();
    	for (ProgramMembershipType type :membershipTypes) {
    		prvo.setGeneralMember("Y");
    		prvo.setMembershipNumber(type.getMemberID());
    		prvo.setMembershipType(type.getMemberLevel());
    		// global check to see if we need to check member level
    		if (super.requireAuthenticatePrivs(membershipvo.getMembershipType()) ) {
    			// is this a valid member level?
    			if (isValidMemberLevel(type.getMemberLevel())) {
    				prvo.setAuthSuccess("Y");
    			} else {
    				prvo.setErrorMessage(MilesPointsConstants.ERROR_MEMBER_LEVEL_NOT_AUTHORIZED);
    			}
    		} else {
    			prvo.setAuthSuccess("Y");
    		}
    		
    		if (prvo.getAuthSuccess().equals("Y") && type.getMemberSummary().size() >0) {
    			MemberSummaryType summary = type.getMemberSummary().get(0);
    			if (membershipvo.getMilesPointsRequested() <= summary.getNewBalance()) {
    				prvo.setMilesVerify("Y");
    			} else {
    				prvo.setErrorMessage(MilesPointsConstants.ERROR_NOT_ENOUGH_MILES);
    			}
    		}
    	}
        return prvo;
        
    }
    
    /**
     * This is the "validate" method for requestServiceName "CheckMiles".
     * The following rules are checked:
     * 1. membershipType is not null
     * 2. membershipNumber is not null
     * 
     * @param membershipvo
     * @return
     * @throws Throwable
     */
    public PartnerResponse validateCheckMiles(MilesPointsRequest membershipvo) throws Throwable 
    {
        String errorMessage = "";

        String membershipType = membershipvo.getMembershipType();
        String membershipNumber = membershipvo.getMembershipNumber();
        int milesPointsAmt = membershipvo.getMilesPointsRequested();

        if(membershipType == null || membershipType.equals("")) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MEMBER_TYPE_MSG;
        }
        else if(membershipNumber == null || membershipNumber.equals("")) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MEMBERSHIP_NUMBER_MSG;
        }
        else if(milesPointsAmt < 0) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MILES_POINTS_MSG;
        }
        
        if(errorMessage != null && errorMessage.length() > 0) {
            PartnerResponse prvo = new PartnerResponse();
            prvo.setAuthErrorCode(errorMessage);
            return prvo;
        }
        
        return null;
    }
    
    /**
     * This is the "process" method for requestServiceName "CheckMiles".
     * It invokes the service functionality to check miles.
     * 
     * @param membershipvo
     * @return
     * @throws Throwable
     */
    public PartnerResponse processCheckMiles(MilesPointsRequest membershipvo) throws Throwable 
    {
    	
    	PartnerRetrvProfile retrvProfile = new PartnerRetrvProfile();

    	
    	//create a request based on the membershipVO.
    	ProgramMembership membership = new ProgramMembership();
    	membership.setMemberID(membershipvo.getMembershipNumber());
    	retrvProfile.setProgramMembership(membership);
    	retrvProfile.setVersion("3.0.0");
    	
    	//make the remote call
    	GetPartnerProfilePortType client = wscf.getUnitedRetreiveProfileService();
    	PartnerProfile profile = client.getPartnerProfileOperation(retrvProfile);
    	
    	//create the response based on the united response
    	PartnerResponse prvo = new PartnerResponse();
    	List<ProgramMembershipType> membershipTypes = profile.getProgramMembership();
    	for (ProgramMembershipType type : membershipTypes) {
    		if (type.getMemberID().equals(membership.getMemberID())) {
	    		prvo.setGeneralMember("Y");
	    		prvo.setMembershipNumber(type.getMemberID());
	    		prvo.setMembershipType(type.getMemberLevel());
	    		prvo.setAuthSuccess("Y");
	    		if (type.getMemberSummary().size() >0) {
	    			MemberSummaryType summary = type.getMemberSummary().get(0);
	    			if (membershipvo.getMilesPointsRequested() <= summary.getNewBalance()) {
	    				prvo.setMilesVerify("Y");
	    			} else {
	    				prvo.setErrorMessage(MilesPointsConstants.ERROR_NOT_ENOUGH_MILES);
	    			}
	    		}
    		}
    	}
    	
        return prvo;

    }    
    
    /**
     * This is the "validate" method for requestServiceName "UpdateMiles".
     * The following rules are checked:
     * 1. membershipType is not null
     * 2. membershipNumber is not null
     * 
     * @param membershipvo
     * @return
     * @throws Throwable
     */
    public PartnerResponse validateUpdateMiles(MilesPointsRequest membershipvo) throws Throwable 
    {
        String errorMessage = "";

        String membershipType = membershipvo.getMembershipType();
        String membershipNumber = membershipvo.getMembershipNumber();
        int milesPointsAmt = membershipvo.getMilesPointsRequested();

        if(membershipType == null || membershipType.equals("")) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MEMBER_TYPE_CODE;
        }
        else if(membershipNumber == null || membershipNumber.equals("")) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MEMBERSHIP_NUMBER_CODE;
        }
        else if(milesPointsAmt == 0) {
            errorMessage = MilesPointsConstants.ERROR_INVALID_MILES_POINTS_CODE;
        }
        
        if(errorMessage != null && errorMessage.length() > 0) {
            PartnerResponse prvo = new PartnerResponse();
            prvo.setAuthErrorCode(errorMessage);
            return prvo;
        }
        return null;       
    }
    
    /**
     * This is the "process" method for requestServiceName "UpdateMiles".
     * It invokes the service functionality to update miles.
     * 
     * @param mpRequest
     * @return
     * @throws Throwable
     */
    public UpdateMilesResp processUpdateMiles(MilesPointsRequest mpRequest) throws Throwable 
    {

        CommonUtil commonUtil = new CommonUtil();
        Connection conn = commonUtil.getDBConnection();
        UpdateMilesResp resp = new UpdateMilesResp();
        
        PartnerUpdateMilesPort updateMilesSvc = wscf.getUnitedUpdateMilesService();
        IdRequestVO idRequest = new IdRequestVO("payment service", IdGeneratorFactory.UNITED_REQUEST_TYPE);
        IdGenerator idGenerator = IdGeneratorFactory.getGenerator(idRequest);
        //add the request header with our unique transaction ID
        String transactionId = idGenerator.execute(idRequest, conn);
        wscf.addRequestHeaderToUARequest(updateMilesSvc, transactionId);
        
        PartnerUpdateMilesRequest req = new PartnerUpdateMilesRequest();
        req.setVersion("1.0.0");
        //set the memberID
        LoyaltyProgram lp = new LoyaltyProgram();
        lp.setLoyaltyProgramNbr(mpRequest.getMembershipNumber());
        req.setLoyaltyProgram(lp);
        
        //set passenger
        req.setPassenger(null);
        
        //set Itinerary
        ItineraryNbrType iNbrType = new ItineraryNbrType();
        PNR pnr = new PNR();
        pnr.setRecordLocator("1234567");  //not exactly sure what this is
        iNbrType.setPNR(pnr);
        req.setItineraryNbr(iNbrType);
        
        //set firstLeg
        req.setFirstLeg(null);
        
        //set Travel
        Travel travel = new Travel();
        travel.setBonusCode(util.getGlobalParm(ACCTG_CONFIG_CONTEXT, UA_BONUS_CODE));
        travel.setPartnerCode(util.getGlobalParm(CONFIG_CONTEXT, UA_PARTNER_CODE ));
        travel.setBonusType(getBonusType());
        FirstTravelItem fti = new FirstTravelItem();
        fti.setNbrOfItem("0");
        fti.setSupplierName("    ");
        travel.setFirstTravelItem(fti);
        req.setTravel(travel);
        
        //set Redemption info
        Redemption redemption = new Redemption();
        redemption.setTotalPoints(mpRequest.getMilesPointsRequested());
        redemption.setDebitCreditInd("D");
        XMLGregorianCalendar xmlCal = new XMLGregorianCalendarImpl(new GregorianCalendar());
        redemption.setEarnDate(xmlCal);
        req.setRedemption(redemption);
        
        //Pricing
        Pricing pricing = new Pricing();
        pricing.setSystemCurrencyCode("USD");
        pricing.setPosSystemConversionRate(1.0f);
        OrderData od = new OrderData();
        od.setTotalPoints(Integer.toString((mpRequest.getMilesPointsRequested())));
        od.setTotalCashAmount("0.00");
        od.setNonRedeemCashtAmount("0.00");
        od.setCustPaymentAmount("0.00");
        od.setBookingFeeAmount("0.00");
        od.setMpFeeAmount("0.00");
        od.setCurrencyCode("USD");
        pricing.setOrderData(od);
        TaxInfo taxInfo = new TaxInfo();
        taxInfo.setTaxAmount("0.00");
        pricing.setTaxInfo(taxInfo);
        req.setPricing(pricing);
        resp.setBonusCode(req.getTravel().getBonusCode());
		resp.setBonusType(req.getTravel().getBonusType());
		resp.setTransactionId(transactionId);
        
        PartnerUpdateMilesResponse response = null;
        try {
			response = updateMilesSvc.partnerUpdateMiles(req);
			resp.setReturnBooleanResult("Y");
			resp.setExternalConfirmationNumber(response.getConfirmation().getConfirmationNbr());
			resp.setAccountBalance(response.getConfirmation().getBalanceInfo().getAccountBalance().toString());
			resp.setLoyaltyProgramNumber(response.getConfirmation().getLoyaltyProgramNbr());
			resp.setPostedAccountNumber(response.getConfirmation().getPostedAccountNbr());
			
		} catch (Exception e) {
			//this might mean that the request was not approved
			logger.error("Error while calling United update miles: " + e.getMessage());
			e.printStackTrace();
			commonUtil.sendSystemMessage("Exception caught in UpdateMiles: " + e.getMessage());
			resp = new UpdateMilesResp();
			resp.setReturnBooleanResult("N");
		} finally {
			conn.close();
		}

        return resp;
    }        
    
    /*
     * gets the bonus type from global parms and pads it with spaces to 3 characters on the right
     */
    private String getBonusType() throws Throwable {
    	String bonusType = util.getGlobalParm(ACCTG_CONFIG_CONTEXT, UA_BONUS_TYPE);
    	
    	while (bonusType.length() < 3) {
    		bonusType += " ";
    	}
    	
    	return bonusType;
	}

   
    /**
     * Compare the input member level with a global parm. The gobal parm
     * UA_VALID_MEMBER_LEVEL is a list of member levels delimited by comma.
     * @param memberLevel
     * @return
     * @throws Throwable
     */
    private boolean isValidMemberLevel(String memberLevel) throws Throwable
    {
        CommonUtil util = new CommonUtil();
        String validLevels = util.getGlobalParm(MilesPointsConstants.UA_VALID_MEMBER_LEVEL);
        if (validLevels == null) {
            validLevels = "";
        }
        
        return this.isMatchingToken(validLevels, memberLevel);       
    }
    
    private boolean isMatchingToken(String tokenString, String matchString) throws Exception {
        StringTokenizer st = new StringTokenizer(tokenString, ",");
        String s = "";
        while(st.hasMoreTokens()) {
            s = st.nextToken();
            if(s.equalsIgnoreCase(matchString)) {
                return true;
            }
        }
        return false;               
    }
}