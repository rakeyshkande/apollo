package com.ftd.milespoints.common.client;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.common.util.CommonUtil;
import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.milespoints.webservice.vo.PartnerResponse;
//import com.ftd.milespointsservices.common.util.HttpUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.HashMap;


/**
 * This is the client class that allows external callers to query miles / points functions.
 */

public class MilesPointsServiceClient
{
    private Logger logger = new Logger(MilesPointsServiceClient.class.getName());

    /**
     * Check if the member has sufficient miles on account to cover requested miles points.
     * @param membershipType
     * @param membershipNumber
     * @param milesPointsAmt
     * @return
     * @throws Throwable
     */
    public String verifyMilesPoints(String membershipType, String membershipNumber, int milesPointsAmt) throws Throwable
    {
        logger.info("verifyMilesPoints(String membershipType, String membershipNumber, int milesPointsAmt)");
        CommonUtil commonUtil = new CommonUtil();
        String url = commonUtil.getGlobalParm(MilesPointsConstants.MILES_POINTS_SERVLET_URL);
        HashMap params = new HashMap();
        params.put(MilesPointsConstants.REQUEST_SERVICE_NAME, MilesPointsConstants.REQUEST_SERVICE_NAME_VERIFY_MILES);
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_TYPE, membershipType);
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_NUMBER, membershipNumber);
        params.put(MilesPointsConstants.REQUEST_MILES_POINTS_AMT, String.valueOf(milesPointsAmt));
        
        //HttpUtil httpUtil = new HttpUtil();
        String response = null;
        
        try {
            //response = httpUtil.send(url, params);
        } catch (Exception e) {
            // send system message
            logger.error(e);
            commonUtil.sendSystemMessage("Exception caught when verifying miles. Miles Points Service Proxy may be down." + e.getMessage());
            return MilesPointsConstants.RETURN_AUTH_ERROR;
        }
        
        return this.parseResponse(response);
        
    }
    
    public String verifyMilesPoints(MilesPointsRequest mvo) throws Throwable
    {
        logger.info("verifyMilesPoints(MembershipVO)");
        CommonUtil commonUtil = new CommonUtil();
        String url = commonUtil.getGlobalParm(MilesPointsConstants.MILES_POINTS_SERVLET_URL);
        
        HashMap params = new HashMap();
        params.put(MilesPointsConstants.REQUEST_SERVICE_NAME, MilesPointsConstants.REQUEST_SERVICE_NAME_VERIFY_MILES);
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_TYPE, mvo.getMembershipType());
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_NUMBER, mvo.getMembershipNumber());
        params.put(MilesPointsConstants.REQUEST_MILES_POINTS_AMT, String.valueOf(mvo.getMilesPointsRequested()));
        
        //HttpUtil httpUtil = new HttpUtil();
        String response = null;
        
        try {
            //response = httpUtil.send(url, params);
        } catch (Exception e) {
            // send system message
            logger.error(e);
            commonUtil.sendSystemMessage("Exception caught when verifying miles. Miles Points Service Proxy may be down." + e.getMessage());
            return MilesPointsConstants.RETURN_AUTH_ERROR;
        }

        return this.parseResponse(response);
    }    
    
    /**
     * Update miles to member's account. Miles points will be deducted from account
     * if milesPointsAmt is positive. Miles points will be credited if milesPointsAmt
     * is negative.
     * @param membershipType
     * @param membershipNumber
     * @param milesPointsAmt
     * @return
     * @throws Throwable
     */
    public String updateMilesPoints(String membershipType, String membershipNumber, String paymentId, int milesPointsAmt) throws Throwable
    {
        CommonUtil commonUtil = new CommonUtil();
        String url = commonUtil.getGlobalParm(MilesPointsConstants.MILES_POINTS_SERVLET_URL);
        HashMap params = new HashMap();
        params.put(MilesPointsConstants.REQUEST_SERVICE_NAME, MilesPointsConstants.REQUEST_SERVICE_NAME_UPDATE_MILES);
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_TYPE, membershipType);
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_NUMBER, membershipNumber);
        params.put(MilesPointsConstants.REQUEST_PAYMENT_ID, paymentId);
        params.put(MilesPointsConstants.REQUEST_MILES_POINTS_AMT, String.valueOf(milesPointsAmt));
        
        //HttpUtil httpUtil = new HttpUtil();
        String response = null;
        
        try {
            //response = //httpUtil.send(url, params);
        } catch (Exception e) {
            // send system message
            logger.error(e);
            commonUtil.sendSystemMessage("Exception caught when updating miles. Miles Points Service Proxy may be down." + e.getMessage());
            return MilesPointsConstants.RETURN_AUTH_ERROR;
        }
        
        return this.parseResponse(response);
    }
   
    public String updateMilesPoints(MilesPointsRequest mvo) throws Throwable
    {
        CommonUtil commonUtil = new CommonUtil();
        String url = commonUtil.getGlobalParm(MilesPointsConstants.MILES_POINTS_SERVLET_URL);
        HashMap params = new HashMap();
        params.put(MilesPointsConstants.REQUEST_SERVICE_NAME, MilesPointsConstants.REQUEST_SERVICE_NAME_UPDATE_MILES);
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_TYPE, mvo.getMembershipType());
        params.put(MilesPointsConstants.REQUEST_MEMBERSHIP_NUMBER, mvo.getMembershipNumber());
        params.put(MilesPointsConstants.REQUEST_PAYMENT_ID, mvo.getPaymentId());
        params.put(MilesPointsConstants.REQUEST_MILES_POINTS_AMT, String.valueOf(mvo.getMilesPointsRequested()));
        
        //HttpUtil httpUtil = new HttpUtil();
        String response = null;
        
        try {
            //response = httpUtil.send(url, params);
        } catch (Exception e) {
            logger.error(e);
            // send system message
            commonUtil.sendSystemMessage("Exception caught when updating miles. Miles Points Service Proxy may be down." + e.getMessage());
            return MilesPointsConstants.RETURN_AUTH_ERROR;
        }
        return this.parseResponse(response);
    }
   
    protected String parseResponse(String xmlResponse) throws Throwable
    {
        String returnVal = "";
        if (xmlResponse != null) {
            xmlResponse = xmlResponse.trim();
            PartnerResponse prvo = PartnerResponse.stringToObject(xmlResponse);
            boolean conSuccess = "Y".equals(prvo.getConSuccess());
            boolean milesVerify = "Y".equals(prvo.getMilesVerify());
            String errorCode = prvo.getAuthErrorCode();
            int ec = 0;
            
            if(milesVerify) {
                returnVal = MilesPointsConstants.RETURN_AUTH_APPROVED;
            } else {
                if(errorCode != null) {
                    try {
                        ec = Integer.parseInt(errorCode);
                    } catch (Exception e) {
                        // error code is not numeric. Consider it a system failure.
                        conSuccess = false;
                    }
                    
                    if (ec < 0) {
                        // Negative error code. Consider it a system failure.
                        conSuccess = false;
                    }
                }
                
                if(conSuccess) {
                    returnVal = MilesPointsConstants.RETURN_AUTH_DECLINED;
                } else {
                    returnVal = MilesPointsConstants.RETURN_AUTH_ERROR;
                }
            }
        }
        return returnVal;
    }
}