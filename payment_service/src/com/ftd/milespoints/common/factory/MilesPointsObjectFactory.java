package com.ftd.milespoints.common.factory;

import com.ftd.milespoints.common.bo.IMilesPointsBO;
import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.common.service.IMilesPointsServices;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This is the factory class for miles points objects.
 */

public class MilesPointsObjectFactory
{
    /**
     * Singleton instance of the object.
     */
    private static MilesPointsObjectFactory INSTANCE;
    private Logger logger = new Logger("com.ftd.milespointsservices.common.factory.MilesPointsObjectFactory");
    
    public MilesPointsObjectFactory() {}
    
    /**
     * Returns the MilesPointsServiceFactory instance. It ensures that there's only one instance of the class.
     * @return
     */
    public static MilesPointsObjectFactory getInstance() 
    {
        if (INSTANCE == null) {
            INSTANCE = new MilesPointsObjectFactory();
        }
        return INSTANCE;
    }
    
    /**
     * Returns the IMilesPointsService implementation class. The factory assumes that the implementation
     * class are named in this format:
     * 1. Resides in package: com.ftd.milespointsservices.partner.service
     * 2. Class name starts with membership type. i.e. UA for United
     * 3. Class name ends with MilesPointsServiceImpl.
     * @param membershipType
     * @return
     * @throws Throwable
     */
    public IMilesPointsServices getMilesPointsServiceImpl(String membershipType) throws Throwable 
    {
        IMilesPointsServices serviceImpl = null;
        String className = null;
        
        try {
            className = MilesPointsConstants.SERVICE_CLASS_NAME_PREFIX + 
                        membershipType+
                        MilesPointsConstants.SERVICE_CLASS_NAME_SUFFIX;
                        
            serviceImpl = (IMilesPointsServices) Class.forName(className).newInstance();
        } finally {
        }
        return serviceImpl;
    }
    
    /**
     * Returns the IMilesPointsBO implementation class. The factory assumes that the implementation
     * class are named in this format:
     * 1. Resides in package: com.ftd.milespointsservices.partner.service
     * 2. Class name starts with membership type. i.e. UA for United
     * 3. Class name ends with MilesPointsBOImpl.
     * @param membershipType
     * @return
     * @throws Throwable
     */
    public IMilesPointsBO getMilesPointsBOImpl(String membershipType) throws Throwable 
    {
        IMilesPointsBO serviceImpl = null;
        String className = null;
        
        try {
            className = MilesPointsConstants.BO_CLASS_NAME_PREFIX + 
                        membershipType +
                        MilesPointsConstants.BO_CLASS_NAME_SUFFIX;
                        
            serviceImpl = (IMilesPointsBO) Class.forName(className).newInstance();
        } finally {
        }
        return serviceImpl;
    }  

}