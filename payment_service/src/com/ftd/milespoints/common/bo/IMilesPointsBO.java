package com.ftd.milespoints.common.bo;

import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.milespoints.webservice.vo.PartnerResponse;
import com.ftd.milespoints.webservice.vo.UpdateMilesResp;

import javax.servlet.http.HttpServletRequest;


/**
 * This is the interface used in miles / points services.
 */

public interface IMilesPointsBO 
{
    public MilesPointsRequest getRequestParams(HttpServletRequest request) throws Throwable;
    public PartnerResponse validateAuthenticateMemberCheckMiles(MilesPointsRequest membershipvo) throws Throwable;
    public PartnerResponse validateCheckMiles(MilesPointsRequest membershipvo) throws Throwable;
    public PartnerResponse validateUpdateMiles(MilesPointsRequest membershipvo) throws Throwable;
    public PartnerResponse processAuthenticateMemberCheckMiles(MilesPointsRequest membershipvo) throws Throwable;
    public PartnerResponse processCheckMiles(MilesPointsRequest membershipvo) throws Throwable;
    public UpdateMilesResp processUpdateMiles(MilesPointsRequest membershipvo) throws Throwable;
}