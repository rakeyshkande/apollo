package com.ftd.milespoints.common.bo;

import static com.ftd.milespoints.common.constant.MilesPointsConstants.CONFIG_CONTEXT;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.SSL_JAVA_KEYSTORE_NAME;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.SSL_JAVA_KEYSTORE_PASSWORD;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.SSL_JAVA_TRUSTSTORE_NAME;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.SSL_JAVA_TRUSTSTORE_PASSWORD;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_SSL_KEYSTORE;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_SSL_KEYSTORE_PASSWORD;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_SSL_TRUSTSTORE;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.util.CommonUtil;


/**
 * This is the base business objects for the miles / points services.
 */

public abstract class MilesPointsBOBase
{
    private Logger logger = new Logger("com.ftd.milespointsservices.common.bo.MilesPointsBOBase");
    
    public MilesPointsBOBase ()
    {
        // Load the security key stores as system properties.
        CommonUtil util = new CommonUtil();
        try {
            System.getProperties().put(SSL_JAVA_KEYSTORE_NAME,util.getFrpGlobalParm(CONFIG_CONTEXT,UA_SSL_KEYSTORE));
            System.getProperties().put(SSL_JAVA_TRUSTSTORE_NAME,util.getFrpGlobalParm(CONFIG_CONTEXT,UA_SSL_KEYSTORE));
            System.getProperties().put(SSL_JAVA_KEYSTORE_PASSWORD,util.getSecureGlobalParm(CONFIG_CONTEXT, UA_SSL_KEYSTORE_PASSWORD));
            System.getProperties().put(SSL_JAVA_TRUSTSTORE_PASSWORD,util.getSecureGlobalParm(CONFIG_CONTEXT, UA_SSL_KEYSTORE_PASSWORD));
        } catch (Throwable t) {
            logger.error(t);
            //send page
            util.sendNoPageSystemMessage("Exception caught in MilesPointsBOBase::MilesPointsBOBase." + t.getMessage());
        }
    } 
    
    /**
     * Retrieves the authenticate privs flag from global parm. The flag indicates
     * if the authenticateMember call needs to restrict member base by making 
     * more specific checks. The global parm is in the format of XX_AUTHENTICATE_PRIVS_FLAG
     * where XX is the membership type, UA for united.
     * 
     * @param membershipType
     * @return
     * @throws Throwable
     */
    protected boolean requireAuthenticatePrivs (String membershipType) throws Throwable
    {
        CommonUtil utils = new CommonUtil();
        String value =  utils.getFrpGlobalParm(CONFIG_CONTEXT,membershipType + MilesPointsConstants._AUTHENTICATE_PRIVS_FLAG);
        
        if(value != null && value.equals("Y")) {
            return true;
        }
        return false;
    }
    

    /**
     * This is the method to retrieve servlet request parameters.
     * @param request
     * @return
     * @throws Throwable
     */
    public MilesPointsRequest getRequestParams (HttpServletRequest request) throws Throwable 
    {
        MilesPointsRequest membershipvo = null;
        
        try {
            String membershipType = request.getParameter(MilesPointsConstants.REQUEST_MEMBERSHIP_TYPE);
            String membershipNumber = request.getParameter(MilesPointsConstants.REQUEST_MEMBERSHIP_NUMBER);
            String password = request.getParameter(MilesPointsConstants.REQUEST_PASSWORD);
            String milesPointsAmt = request.getParameter(MilesPointsConstants.REQUEST_MILES_POINTS_AMT);
            String paymentId = request.getParameter(MilesPointsConstants.REQUEST_PAYMENT_ID);
            
            membershipvo = new MilesPointsRequest();
            membershipvo.setMembershipType(membershipType);
            membershipvo.setMembershipNumber(membershipNumber);
            membershipvo.setPassword(password);
            membershipvo.setPaymentId(paymentId);
            try {
                membershipvo.setMilesPointsRequested(Integer.parseInt(milesPointsAmt));
            } catch (Exception nfe) {
                membershipvo.setMilesPointsRequested(-1);
            }
            
        } catch (Exception e){
            logger.error(e);
            throw e;
        }     
        return membershipvo;
    }
    

    protected Map executeInsertUpdateStmt(Connection conn, String stmt, Map inputParams) throws Throwable
    {
        logger.info("executeInsertUpdateStmt " + stmt);
        DataRequest request = new DataRequest();
        String status = "";
        Map outputs = null;
        try {
           // build DataRequest object
           request.setConnection(conn);
           request.setInputParams(inputParams);
           request.setStatementID(stmt);

           // get data
           DataAccessUtil dau = DataAccessUtil.getInstance();
           outputs = (Map) dau.execute(request);
           request.reset();
           status = (String) outputs.get("OUT_STATUS");
           if(!MilesPointsConstants.COMMON_VALUE_YES.equals(status))
           {
               String errorMessage = (String) outputs.get("OUT_MESSAGE");
               logger.error(errorMessage);
               throw new Exception(errorMessage);
           }
           logger.debug("Exiting executeInsertUpdateStmt. Status is:" + status);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return outputs;
        
    }

}