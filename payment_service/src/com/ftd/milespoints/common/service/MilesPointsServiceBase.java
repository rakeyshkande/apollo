package com.ftd.milespoints.common.service;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.util.CommonUtil;


/**
 * This is the base service objects for the miles / points services.
 */

public abstract class MilesPointsServiceBase
{
    private Logger logger = new Logger("com.ftd.milespointsservices.common.service.MilesPointsServiceBase");

    /**
     * This is the method to retrieve the service endpoint.
     * @param membershipType
     * @return
     * @throws Throwable
     */
    public String getServiceEndpoint (String membershipType) throws Throwable 
    {
        String endpoint = "";
        
        try {
            CommonUtil util = new CommonUtil();
            endpoint =  util.getFrpGlobalParm(MilesPointsConstants.CONFIG_CONTEXT,membershipType + MilesPointsConstants._ENDPOINT_ENVIRONMENT);
            
            return endpoint;
        } catch (Exception e){
            logger.error(e);
        }     
        return endpoint;
    }
    
    /**
     * Maps and returns the name of the partner service for the given requested service name.
     * @param membershipType
     * @param serviceName
     * @return
     * @throws Throwable
     */
    public String getPartnerServiceName (String membershipType, String serviceName) throws Throwable 
    {
        String sn = null;
        String configFile = null;
        
        try {
            configFile = MilesPointsConstants.SERVICE_NAME_MAPPING_CONFIG_FILE_PREFIX + 
                        membershipType +
                        MilesPointsConstants.SERVICE_NAME_MAPPING_CONFIG_FILE_SUFFIX;
                        
            sn = ConfigurationUtil.getInstance().getProperty(configFile, serviceName);
        } catch (Exception e){
            logger.error(e);
            throw e;
        } 
        return sn;
    }    
    
    /**
     * Retrieves the authenticate privs flag from global parm. The flag indicates
     * if the authenticateMember call needs to restrict member base by making 
     * more specific checks. The global parm is in the format of XX_AUTHENTICATE_PRIVS_FLAG
     * where XX is the membership type, UA for united.
     * 
     * @param membershipType
     * @return
     * @throws Throwable
     */
    protected boolean requireAuthenticatePrivs (String membershipType) throws Throwable
    {
        CommonUtil utils = new CommonUtil();
        String value =  utils.getFrpGlobalParm(MilesPointsConstants.CONFIG_CONTEXT,membershipType + MilesPointsConstants._AUTHENTICATE_PRIVS_FLAG);
        
        if(value != null && value.equals("Y")) {
            return true;
        }
        return false;
    }
    
    /**
     * Gets the timeout value for the call. Default to one minute if not set.
     * @param membershipType
     * @return
     * @throws Throwable
     */
    protected int getAPIConnectionTimeout(String membershipType) throws Throwable
    {
        CommonUtil utils = new CommonUtil();
        String value =  utils.getFrpGlobalParm(MilesPointsConstants.CONFIG_CONTEXT,membershipType + MilesPointsConstants._API_CONNECTION_TIMEOUT);
        
        if(value != null && !value.equals("")) {
            return Integer.parseInt(value);
        }
        return 60000;      
    }    
}