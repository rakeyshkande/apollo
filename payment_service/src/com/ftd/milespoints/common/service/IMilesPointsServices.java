package com.ftd.milespoints.common.service;

import com.ftd.milespoints.webservice.vo.MilesPointsRequest;
import com.ftd.milespoints.webservice.vo.PartnerResponse;


/**
 * This is the interface used in miles / points services.
 */

public interface IMilesPointsServices 
{
    //public PartnerResponseVO authenticateMember(MembershipVO membershipvo) throws Throwable;
    public PartnerResponse checkMilesPoints(MilesPointsRequest membershipvo) throws Throwable;
    public PartnerResponse updateMilesPoints(MilesPointsRequest membershipvo) throws Throwable;
    public PartnerResponse authenticateMemberCheckMilesPoints(MilesPointsRequest membershipvo) throws Throwable;
}