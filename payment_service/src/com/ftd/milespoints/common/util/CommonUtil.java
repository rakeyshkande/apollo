package com.ftd.milespoints.common.util;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;



import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This is the common utilities class.
 */

public class CommonUtil
{
    /**
     * Returns the global parm with given parmName in context MILES_POINTS_SERVICES_CONFIG.
     * @param parmName
     * @return
     * @throws Throwable
     */
    private Logger logger = new Logger("com.ftd.milespointsservices.common.util.CommonUtil");
    
    public String getGlobalParm(String parmName) throws Throwable
    {
        return getGlobalParm(MilesPointsConstants.CONFIG_CONTEXT, parmName);
    }
    
    public String getGlobalParm(String context, String parmName) throws Throwable
    {
        return ConfigurationUtil.getInstance().getFrpGlobalParm(context, parmName);
        
    }    
    
    /**
     * Returns the secure config with given parmName in context MILES_POINTS_SERVICES_CONFIG.
     * @param parmName
     * @return
     * @throws Throwable
     */
    public String getSecureProperty(String parmName) throws Throwable
    {
        return ConfigurationUtil.getInstance().getSecureProperty(MilesPointsConstants.SECURE_CONFIG_CONTEXT, parmName);
        
    }    
    
    /**
     *  convert a XML Document object to a String
     * @param convertDoc - Document
     * @return String
     * @throws IOException
     */
     /*
    public String xmlToString(Document convertDoc) 
        throws IOException
    {
        String xmlText = "";

        StringWriter sw = new StringWriter();       
        DOMUtil.print(((Document) convertDoc), new PrintWriter(sw));
        xmlText=sw.toString();

       return xmlText;
    }
    */
    
    public String outputXml(Document doc) throws Exception {
        StringWriter sw = new StringWriter();
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(sw);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();
        serializer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
        serializer.transform(domSource, streamResult); 
        return sw.toString();
    } 
    
    public void sendSystemMessage(String logMessage) {
        Connection con = null;
        try {
             con = this.getDBConnection();
             String appSource = MilesPointsConstants.SM_SOURCE;
             String errorType = MilesPointsConstants.SM_TYPE;
             String subject = MilesPointsConstants.SM_SUBJECT;
             int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

             SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
             systemMessengerVO.setLevel(pageLevel);
             systemMessengerVO.setSource(appSource);
             systemMessengerVO.setType(errorType);
             systemMessengerVO.setSubject(subject);
             systemMessengerVO.setMessage(logMessage);
             String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);
             if(result != null) {
                 logger.info("result is:" + result);
             }
         
         } catch (Exception ex) {
            // Do not attempt to send system message it requires obtaining a connection
            // and may end up in an infinite loop.
             logger.error(ex);
         } finally {
             if(con != null) {
                try {
                    con.close();
                } catch (SQLException sx) {
                    logger.error(sx);
                }
             }
         }
    
    }
    
    public Connection getDBConnection () throws Exception {
        Connection connection=null;
        try{
            /* create a connection to the database */
            connection = DataSourceUtil.getInstance().getConnection(MilesPointsConstants.DATASOURCE_NAME);       
        } catch (Exception e){
            logger.error(e);
            throw e;
        }
        return connection;
    }
    
    public Date calculateOffSetDate(int dayOffSet) 
        throws IOException, SAXException, ParserConfigurationException, 
        TransformerException ,ParseException
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering calculateOffSetDate");
        }
        
        Date offsetDate;
        
        try{
            /* retrieve system date */
            GregorianCalendar currentDate = new GregorianCalendar(); 
            /* change date based on batch date offset */
            currentDate.add(GregorianCalendar.DATE, dayOffSet);
            /* format date */
            Date d = currentDate.getTime();
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (
                       "MMddyy");
            
            String s = sdfOutput.format(d);
            offsetDate = sdfOutput.parse(s);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting calculateOffSetDate");
            } 
       } 
       return offsetDate;
    }    
    
}