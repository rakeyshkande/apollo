package com.ftd.milespoints.common.exception;

/**
 * Exception representing an error with the Miles Points Service
 */
public class MilesPointsServiceException extends Exception
{
    public MilesPointsServiceException(Throwable cause)
    {
        super(cause);
    }

    public MilesPointsServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MilesPointsServiceException(String message)
    {
        super(message);
    }

    public MilesPointsServiceException()
    {
    }
}
