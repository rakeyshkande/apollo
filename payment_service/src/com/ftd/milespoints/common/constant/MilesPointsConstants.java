package com.ftd.milespoints.common.constant;

/**
 * This is the constant class.
 */

public class MilesPointsConstants
{
    /////// Begin United Mileage Plus Redemption Constants ////////
    // Config file names
    public static final String SERVICE_NAME_MAPPING_CONFIG_FILE_PREFIX = "service_name_mapping_";
    public static final String SERVICE_NAME_MAPPING_CONFIG_FILE_SUFFIX = ".xml";
    public static final String SERVICE_CLASS_NAME_PREFIX = "com.ftd.milespoints.partner.service.";
    public static final String SERVICE_CLASS_NAME_SUFFIX = "MilesPointsServiceImpl";
    public static final String BO_CLASS_NAME_PREFIX = "com.ftd.milespoints.partner.bo.";
    public static final String BO_CLASS_NAME_SUFFIX = "MilesPointsBOImpl";
    public static final String BO_CLASS_NAME_BASE = "com.ftd.milespoints.common.bo.MilesPointsBOBase";
    public static final String BO_METHOD_PREFIX_VALIDATE = "validate";
    public static final String BO_METHOD_PREFIX_PROCESS = "process";
    public static final String CLIENT_CLASS_NAME_PREFIX = "com.ftd.milespoints.partner.service.";
    public static final String CLIENT_CLASS_NAME_SUFFIX = "MilesPointsClientImpl";
    
    // Global parm context and names
    public static final String CONFIG_CONTEXT = "SERVICE";
    public static final String ACCTG_CONFIG_CONTEXT = "ACCOUNTING_CONFIG";
    public static final String SECURE_CONFIG_CONTEXT = "miles_points_services";
    public static final String _ENDPOINT_ENVIRONMENT = "_ENDPOINT_ENVIRONMENT";
    public static final String _AUTHENTICATE_PRIVS_FLAG = "_AUTHENTICATE_PRIVS_FLAG";
    public static final String _API_CONNECTION_TIMEOUT = "_API_CONNECTION_TIMEOUT";
    public static final String UA_BONUS_CODE = "UA_BONUS_CODE";
    public static final String UA_BONUS_TYPE = "UA_BONUS_TYPE";
    public static final String UA_VALID_MEMBER_LEVEL = "UA_VALID_MEMBER_LEVEL";
    public static final String UA_PARTNER_SYS_ERROR = "UA_PARTNER_SYS_ERROR";
    public static final String UA_PROFILE_SERVICE_ENDPOINT_URL = "UA_PROFILE_SERVICE_ENDPOINT_URL";
    public static final String UA_UPDATE_MILES_SERVICE_ENDPOINT_URL = "UA_UPDATE_MILES_SERVICE_ENDPOINT_URL";
	public static final String UA_SERVICE_USERNAME = "UA_SERVICE_USERNAME";
	public static final String UA_SERVICE_PASSWORD = "UA_SERVICE_PASSWORD";
    
    // Partner Web Service Names
    public static final String SERVICE_NAME_AUTHENTICATE_MEMBER = "SERVICE_NAME_AUTHENTICATE_MEMBER";
    public static final String SERVICE_NAME_GET_MEMBER_LEVEL = "SERVICE_NAME_GET_MEMEBER_LEVEL";
    public static final String SERVICE_NAME_GET_PARTNER_REGISTRATION_FLAG = "SERVICE_NAME_GET_PARTNER_REGISTRATION_FLAG";
    public static final String SERVICE_NAME_GET_TOTAL_MILES = "SERVICE_NAME_GET_TOTAL_MILES";
    public static final String SERVICE_NAME_UPDATE_MILES = "SERVICE_NAME_UPDATE_MILES";
    
    // United keystore and password
    public static final String SSL_JAVA_KEYSTORE_NAME = "javax.net.ssl.keyStore";
    public static final String SSL_JAVA_TRUSTSTORE_NAME = "javax.net.ssl.trustStore";
    public static final String SSL_JAVA_KEYSTORE_PASSWORD = "javax.net.ssl.keyStorePassword";
    public static final String SSL_JAVA_TRUSTSTORE_PASSWORD = "javax.net.ssl.trustStorePassword";
    public static final String UA_SSL_KEYSTORE = "UA_SSL_KEYSTORE";
    public static final String UA_SSL_TRUSTSTORE = "UA_SSL_TRUSTSTORE";
    public static final String UA_SSL_KEYSTORE_PASSWORD = "UA_SSL_KEYSTORE_PASSWORD";
    public static final String UA_SSL_TRUSTSTORE_PASSWORD = "UA_SSL_KEYSTORE_PASSWORD";    
    
    // Servlet request parameter name
    public static final String REQUEST_MEMBERSHIP_TYPE = "membershipType";
    public static final String REQUEST_MEMBERSHIP_NUMBER ="membershipNumber";
    public static final String REQUEST_PASSWORD = "password";
    public static final String REQUEST_MILES_POINTS_AMT = "milesAmt";
    public static final String REQUEST_SERVICE_NAME = "serviceName";
    public static final String REQUEST_PAYMENT_ID = "paymentId";
    public static final String REQUEST_SERVICE_NAME_VERIFY_MILES = "CheckMiles";
    public static final String REQUEST_SERVICE_NAME_UPDATE_MILES = "UpdateMiles";
    public static final String REQUEST_SERVICE_NAME_AUTH_CHECK_MILES = "AuthenticateMemberCheckMiles";
    
    // Servlet request input validation
    public static final String ERROR_CANNOT_HANDLE_REQUEST_CODE = "-0000ftd";
    public static final String ERROR_INVALID_MEMBER_TYPE_CODE = "-5998ftd";
    public static final String ERROR_INVALID_MEMBERSHIP_NUMBER_CODE = "-10010ftd";
    public static final String ERROR_INVALID_PASSWORD_CODE = "-12ftd";
    public static final String ERROR_INVALID_SERVICE_REQUEST_CODE = "-5999ftd";
    public static final String ERROR_INVALID_MILES_POINTS_CODE = "-5997ftd";
    public static final String ERROR_INVALID_MEMBER_TYPE_MSG = "The requested membership type is invalid.";
    public static final String ERROR_INVALID_MEMBERSHIP_NUMBER_MSG = "The requested membership number is empty.";
    public static final String ERROR_INVALID_PASSWORD_MSG = "The requested password is empty.";
    public static final String ERROR_INVALID_MILES_POINTS_MSG = "The requested miles points are invalid.";
    public static final String ERROR_INVALID_SERVICE_REQUEST_MSG = "The requested service name is invalid.";
    public static final String ERROR_NOT_ENOUGH_MILES = "Our apologies, the mileage level of the product you are attempting to purchase " +
					"exceeds your current mileage balance.<br/><br/>You may purchase an item for fewer miles, " +
					"or purchase asdditional miles from the United Mileage Plus website. Or, you may complete " +
					"your transaction with a credit card below.";
    public static final String ERROR_MEMBER_LEVEL_NOT_AUTHORIZED = "Our apologies, the United Mileage Plus account you entered does not allow " +
					"FTD purchases with the Mileage Plus miles.  Please review the FAQs for more information " +
					"on who is eligibile to redeem miles for FTD purchases.  You may complete your transaction " +
					"with a credit card below";
    
    public static final String UA_MILE_INDICATOR_DEDUCT = "D";
    public static final String UA_MILE_INDICATOR_CREDIT = "C";
    
    public static final String COMMON_VALUE_YES = "Y";
    public static final String COMMON_VALUE_NO = "N";
    /////// End United Mileage Plus Redemption Constants ////////
    
    // Servlet URL
    public static final String MILES_POINTS_SERVLET_URL = "MILES_POINTS_SERVLET_URL";
    
    // Global Parameter - trust self signed cert flag
    public static final String CLIENT_TRUSTS_ALL_SSL_CERTS_FLAG = "CLIENT_TRUSTS_ALL_SSL_CERTS_FLAG";
    
    // System Message app source
    public static final String SM_SOURCE = "MILES POINTS SERVICES";
    public static final String SM_TYPE = "System Exception";
    public static final String SM_SUBJECT = "Miles Points Services System Message";
    
    // Date source name
    public static final String DATASOURCE_NAME = "CLEAN";
    
    public static final String STMT_INSERT_AP_BILLING_HEARDER = "INSERT_AP_BILLING_HEADER_NO_DUP";
    public static final String STMT_INSERT_AP_BILLING_DETAIL_PREFIX = "INSERT_AP_BILLING_DETAIL_";
    
    // Auth return codes.
    public static final String RETURN_AUTH_APPROVED = "A";
    public static final String RETURN_AUTH_DECLINED = "D";
    public static final String RETURN_AUTH_ERROR = "E";
    
    // UA error code that needs special treatment
    public static final String UA_AUTH_ERROR_RESTRICTED_ACCESS = "15";
    public static final String UA_ERROR_OTHER = "-55";

	
}