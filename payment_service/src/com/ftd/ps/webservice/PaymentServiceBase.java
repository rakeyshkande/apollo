package com.ftd.ps.webservice;

import org.apache.commons.lang.StringUtils;

import com.ftd.ps.common.constant.PaymentServiceConstants;
import com.ftd.ps.common.util.CommonUtil;
import com.ftd.ps.webservice.vo.PaymentRequest;

public class PaymentServiceBase {
	
	/**
	   * @return Returns a boolean - checks if the client name / password is
	   *         correct. If incorrect, return false.
	   */
	  public static boolean checkClient(String username, String password, CommonUtil psUtil) throws Exception
	  {
	    if (StringUtils.isBlank(username))
	      return false;

	    if (StringUtils.isBlank(password))
	      return false;

	    String validClientUserName = psUtil.getSecureGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_CLIENT_USER_NAME);

	    if (!StringUtils.equalsIgnoreCase(username, validClientUserName))
	      return false;

	    String validClientPassword = psUtil.getSecureGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_CLIENT_PASSWORD);

	    if (!StringUtils.equalsIgnoreCase(password, validClientPassword))
	      return false;

	    return true;
	  }

}
