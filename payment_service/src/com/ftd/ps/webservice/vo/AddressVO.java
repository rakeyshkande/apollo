package com.ftd.ps.webservice.vo;

/**
 * This class gets used as part of the Payment Service response.
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package and then a new method should be exposed on the wsdl
 * using the new VOs.
 * 
 * @author alakhani
 *
 */
public class AddressVO {
	
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	
	public String getAddress1() 
  {
		return address1;
	}
	
  public void setAddress1(String address1) 
  {
		this.address1 = address1;
	}
	
  public String getAddress2() {
		return address2;
	}
	
  public void setAddress2(String address2) 
  {
		this.address2 = address2;
	}
	
  public String getCity() 
  {
		return city;
	}
	
  public void setCity(String city) 
  {
		this.city = city;
	}
	
  public String getState() 
  {
		return state;
	}
	
  public void setState(String state) 
  {
		this.state = state;
	}
	
  public String getZip() 
  {
		return zip;
	}
	
  public void setZip(String zip) 
  {
		this.zip = zip;
	}
	
  public String getCountry() 
  {
		return country;
	}
	
  public void setCountry(String country) 
  {
		this.country = country;
	}

	

	

}
