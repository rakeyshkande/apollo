package com.ftd.ps.webservice.vo;

import java.util.List;


/**
 * object to hold the top level response for the payment service.
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package 
 * 
 * @author alakhani
 *
 */
public class PaymentResponse {
    
  private AmountVO approvedAmountVO;
  private String authorizationCode;
  private AmountVO balanceAmountVO;
  private CardVO cardVO;
  private String conversionRate;
  private String transactionId;
  private Boolean isSuccessful;

  //internal error code and message
  private String errorCode;
  private String errorMessage;

  //SVS response code and message
  private String responseCode;
  private String responseMessage;

  
  public AmountVO getApprovedAmountVO() 
  {
    return approvedAmountVO;
  }

  public void setApprovedAmountVO(AmountVO approvedAmountVO) 
  {
    this.approvedAmountVO = approvedAmountVO;
  }

  public String getAuthorizationCode() 
  {
    return authorizationCode;
  }

  public void setAuthorizationCode(String authorizationCode) 
  {
    this.authorizationCode = authorizationCode;
  }

  public AmountVO getBalanceAmountVO() 
  {
    return balanceAmountVO;
  }

  public void setBalanceAmountVO(AmountVO balanceAmountVO) 
  {
    this.balanceAmountVO = balanceAmountVO;
  }

  public CardVO getCardVO() 
  {
    return cardVO;
  }

  public void setCardVO(CardVO cardVO) 
  {
    this.cardVO = cardVO;
  }

  public String getConversionRate() 
  {
    return conversionRate;
  }

  public void setConversionRate(String conversionRate) 
  {
    this.conversionRate = conversionRate;
  }
  
  public String getTransactionId() 
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId) 
  {
    this.transactionId = transactionId;
  }

  public Boolean getIsSuccessful() 
  {
    return isSuccessful;
  }
  
  public void setIsSuccessful(Boolean isSuccessful) 
  {
    this.isSuccessful = isSuccessful;
  }

  public String getErrorCode() 
  {
    return errorCode;
  }

  public void setErrorCode(String errorCode) 
  {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() 
  {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) 
  {
    this.errorMessage = errorMessage;
  }

  public String getResponseCode() 
  {
    return responseCode;
  }

  public void setResponseCode(String responseCode) 
  {
    this.responseCode = responseCode;
  }

  public String getResponseMessage() 
  {
    return responseMessage;
  }

  public void setResponseMessage(String responseMessage) 
  {
    this.responseMessage = responseMessage;
  }

}
