package com.ftd.ps.webservice.vo;

/**
 * This class gets used as part of the Payment Service response.
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package and then a new method should be exposed on the wsdl 
 * using the new VOs.
 * 
 * @author alakhani
 *
 */
public class CardVO {
  
  private String cardCurrency;
  private String cardNumber;
  private String pinNumber;
  private String cardExpiration;
  private String cardTrackOne;
  private String cardTrackTwo;
  private String validationStatus; // 'D' - Declined, 'A' - Approved, 'E' - System Error //
  private boolean underMinAuthAmt;
  private String status;
  private String actionCode;
  private String verbiage;
  private String approvalCode;
  private String AVSIndicator;
  private String acquirerReferenceData;
  private String cscValue;
  private String cscResponseCode;
  private String cscValidatedFlag;

  public void setCardCurrency(String cardCurrency)
  {
    this.cardCurrency = cardCurrency;
  }

  public String getCardCurrency()
  {
    return cardCurrency;
  }

  public void setCardNumber(String cardNumber)
  {
    this.cardNumber = cardNumber;
  }

  public String getCardNumber()
  {
    return cardNumber;
  }

  public void setPinNumber(String pinNumber)
  {
    this.pinNumber = pinNumber;
  }

  public String getPinNumber()
  {
    return pinNumber;
  }

  public void setCardExpiration(String cardExpiration)
  {
    this.cardExpiration = cardExpiration;
  }

  public String getCardExpiration()
  {
    return cardExpiration;
  }

  public void setCardTrackOne(String cardTrackOne)
  {
    this.cardTrackOne = cardTrackOne;
  }

  public String getCardTrackOne()
  {
    return cardTrackOne;
  }

  public void setCardTrackTwo(String cardTrackTwo)
  {
    this.cardTrackTwo = cardTrackTwo;
  }

  public String getCardTrackTwo()
  {
    return cardTrackTwo;
  }

  public void setValidationStatus(String validationStatus)
  {
    this.validationStatus = validationStatus;
  }

  public String getValidationStatus()
  {
    return validationStatus;
  }

  public void setUnderMinAuthAmt(boolean underMinAuthAmt)
  {
    this.underMinAuthAmt = underMinAuthAmt;
  }

  public boolean isUnderMinAuthAmt()
  {
    return underMinAuthAmt;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    return status;
  }

  public void setActionCode(String actionCode)
  {
    this.actionCode = actionCode;
  }

  public String getActionCode()
  {
    return actionCode;
  }

  public void setVerbiage(String verbiage)
  {
    this.verbiage = verbiage;
  }

  public String getVerbiage()
  {
    return verbiage;
  }

  public void setApprovalCode(String approvalCode)
  {
    this.approvalCode = approvalCode;
  }

  public String getApprovalCode()
  {
    return approvalCode;
  }

  public void setAVSIndicator(String aVSIndicator)
  {
    this.AVSIndicator = aVSIndicator;
  }

  public String getAVSIndicator()
  {
    return AVSIndicator;
  }

  public void setAcquirerReferenceData(String acquirerReferenceData)
  {
    this.acquirerReferenceData = acquirerReferenceData;
  }

  public String getAcquirerReferenceData()
  {
    return acquirerReferenceData;
  }

  public void setCscValue(String cscValue)
  {
    this.cscValue = cscValue;
  }

  public String getCscValue()
  {
    return cscValue;
  }

  public void setCscResponseCode(String cscResponseCode)
  {
    this.cscResponseCode = cscResponseCode;
  }

  public String getCscResponseCode()
  {
    return cscResponseCode;
  }

  public void setCscValidatedFlag(String cscValidatedFlag)
  {
    this.cscValidatedFlag = cscValidatedFlag;
  }

  public String getCscValidatedFlag()
  {
    return cscValidatedFlag;
  }

}
