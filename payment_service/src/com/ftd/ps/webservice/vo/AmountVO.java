package com.ftd.ps.webservice.vo;

/**
 * This class defines the amounts as per instructed by SVS
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package and then a new method should be exposed on the wsdl 
 * using the new VOs.
 * 
 * @author alakhani
 *
 */
public class AmountVO {
	
	private Double amount;
	private String currency;
	
	
  public Double getAmount() 
  {
    return amount;
  }
  
  public void setAmount(Double amount) 
  {
    this.amount = amount;
  }

	public String getCurrency() 
  {
		return currency;
	}

	public void setCurrency(String currency) 
  {
		this.currency = currency;
	}

	

	

}
