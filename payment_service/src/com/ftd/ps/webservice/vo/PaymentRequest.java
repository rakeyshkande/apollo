package com.ftd.ps.webservice.vo;

/**
 * Top level object for the payment service request.
 * 
 * Changing this class will change the WSDL!!!!!
 * Since you should never change a WSDL, any changes to a contract should get put
 * into a different package
 * 
 * @author alakhani
 *
 */
public class PaymentRequest {
    
  private String sourceCode;
  private String clientUserName;
  private String clientPassword;
  private String transactionId;
  private String paymentType;
  private String invoiceNumber;
  private CardVO cardVO;
  private AddressVO addressVO;
  private AmountVO amountVO;

  public String getSourceCode() 
  {
    return sourceCode;
  }

  public void setSourceCode(String sourceCode) 
  {
    this.sourceCode = sourceCode;
  }

    public String getClientUserName() 
  {
        return clientUserName;
    }

    public void setClientUserName(String clientUserName) 
  {
        this.clientUserName = clientUserName;
    }

  public String getClientPassword() 
  {
    return clientPassword;
  }

  public void setClientPassword(String clientPassword) 
  {
    this.clientPassword = clientPassword;
  }

  public String getTransactionId() 
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId) 
  {
    this.transactionId = transactionId;
  }

  public String getPaymentType()
  {
    return paymentType;
  }

    public void setPaymentType(String paymentType)
  {
    this.paymentType = paymentType;
  }

    public String getInvoiceNumber()
  {
    return invoiceNumber;
  }

    public void setInvoiceNumber(String invoiceNumber)
  {
    this.invoiceNumber = invoiceNumber;
  }

    public CardVO getCardVO() 
  {
    return cardVO;
  }

  public void setCardVO(CardVO cardVO) 
  {
    this.cardVO = cardVO;
  }

  public AddressVO getAddressVO() 
  {
    return addressVO;
  }

  public void setAddressVO(AddressVO addressVO) 
  {
    this.addressVO = addressVO;
  }

  public AmountVO getAmountVO() 
  {
    return amountVO;
  }

  public void setAmountVO(AmountVO amountVO) 
  {
    this.amountVO = amountVO;
  }

}
