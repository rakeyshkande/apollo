package com.ftd.ps.webservice;

import java.sql.Connection;
import java.util.GregorianCalendar;

import javax.jws.WebService;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.constant.PaymentServiceConstants;
import com.ftd.ps.common.dao.PaymentServiceDAO;
import com.ftd.ps.common.util.CommonUtil;
import com.ftd.ps.common.util.SVSInterceptor;
import com.ftd.ps.common.util.WebServiceClientFactory;
import com.ftd.ps.webservice.vo.AmountVO;
import com.ftd.ps.webservice.vo.CardVO;
import com.ftd.ps.webservice.vo.PaymentRequest;
import com.ftd.ps.webservice.vo.PaymentResponse;
import com.storedvalue.giftcard.Amount;
import com.storedvalue.giftcard.BalanceInquiryRequest;
import com.storedvalue.giftcard.BalanceInquiryResponse2;
import com.storedvalue.giftcard.Card;
import com.storedvalue.giftcard.GiftCardOnline;
import com.storedvalue.giftcard.MerchandiseReturnRequest;
import com.storedvalue.giftcard.MerchandiseReturnResponse2;
import com.storedvalue.giftcard.Merchant;
import com.storedvalue.giftcard.PreAuthCompleteRequest;
import com.storedvalue.giftcard.PreAuthCompleteResponse2;
import com.storedvalue.giftcard.PreAuthRequest;
import com.storedvalue.giftcard.PreAuthResponse2;
import com.storedvalue.giftcard.ReturnCode;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * 
 * @author Ali Lakhani
 * 
 */
@Service
@WebService(endpointInterface = "com.ftd.ps.webservice.PaymentServiceClient")
public class PaymentServiceClientSVSImpl extends PaymentServiceBase implements PaymentServiceClient
{

  Logger logger = new Logger(PaymentServiceClientSVSImpl.class.getName());

  /**
   * this gets injected via spring-beans.xml
   */
  protected WebServiceClientFactory wscFactory;
  protected SVSInterceptor sInterceptor;

  @Autowired
  public void setWscFactory(WebServiceClientFactory wscFactory)
  {
    this.wscFactory = wscFactory;
  }

  @Autowired
  public void setSInterceptor(SVSInterceptor sInterceptor)
  {
    this.sInterceptor = sInterceptor;
  }

  /**
   * This method will call BalanceInquiryRequest on the SVS web service
   */
  @Override
  public PaymentResponse getBalance(PaymentRequest pmtRequest) throws Exception
  {
      return getBalance(pmtRequest, "getBalance");
  }

  /**
   * This method will call BalanceInquiryRequest on the SVS web service - it is the core of getBalance, 
   * allowing for calls from multiple methods. It will log and record the callingMethod
   * @param pmtRequest
   * @param callingMethod
   * @return
   * @throws Exception
   */
  private PaymentResponse getBalance(PaymentRequest pmtRequest, String callingMethod) throws Exception
  {
      logger.info("### PaymentServiceClientSVSImpl " + callingMethod + " #### - initiated");

      //instantiate the common utility
      CommonUtil psUtil = new CommonUtil();

      //instantiate the Xstream object
      XStream xstream = getXStreamObject();

      // define variables
      Connection conn = null;
      PaymentResponse pmtResponse = new PaymentResponse();
      PaymentServiceDAO psDao = null;
      String paymentTrackingId = null;
      String sBirResponse = null;
      BalanceInquiryResponse2 birResponse = null;
      pmtResponse.setIsSuccessful(true);
      BalanceInquiryRequest birRequest = new BalanceInquiryRequest();

      // validate client info
      // Note : If validation fails, PaymentResponse will still show a success to the client... no further processing shall commence.  
      boolean validClient = checkClient(pmtRequest.getClientUserName(), pmtRequest.getClientPassword(), psUtil);
      if (!validClient)
      {
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - Invalid client info detected");
          return pmtResponse;
      }

      boolean validTransactionId = checkTransactionId(pmtRequest, pmtResponse);
      if (!validTransactionId)
      {
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - Invalid transaction info detected");
          return pmtResponse;
      }

      // set the date
      GregorianCalendar gcCal = new GregorianCalendar();
      XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
      birRequest.setDate(xmlgcCal);

      // set invoice number
      if (StringUtils.isBlank(pmtRequest.getInvoiceNumber()))
          birRequest.setInvoiceNumber("00000000");
      else
      {
          int paddingSize = 8 - pmtRequest.getInvoiceNumber().length();
          birRequest.setInvoiceNumber(psUtil.pad(pmtRequest.getInvoiceNumber(), PaymentServiceConstants.PAD_LEFT, "0", paddingSize));
      }

      // set merchant
      birRequest.setMerchant(populateMerchant(psUtil));

      // set transaction amount
      Amount amt = new Amount();
      amt.setAmount(pmtRequest.getAmountVO().getAmount());
      amt.setCurrency(pmtRequest.getAmountVO().getCurrency());
      birRequest.setAmount(amt);

      // set routing id
      birRequest.setRoutingID(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_ROUTING_ID));

      // set Systems Trace Audit Number. Note that STAN is not required for
      // Duplicate-Check process
      birRequest.setStan(null);

      // set transaction id
      birRequest.setTransactionID(pmtRequest.getTransactionId());

      // set duplicate check
      birRequest.setCheckForDuplicate(true);

      // set the card with masked gift card info... only after we get the string to log in Database, we populate it the way we send it to SVS
      birRequest.setCard(populateCardMasked(pmtRequest, psUtil));

      try
      {
          //get the connection
          conn = psUtil.getNewConnection();

          //instantiate DAO
          psDao = new PaymentServiceDAO(conn);

          // use Xstream to convert the request object to be sent to SVS to string in xml format
          String sBirRequest = xstream.toXML(birRequest);

          // log the request being sent to SVS
          paymentTrackingId = insertPaymentTracking(psDao, pmtRequest.getClientUserName(), null, null, pmtRequest.getPaymentType(), sBirRequest, null, null, null,
                  pmtRequest.getSourceCode(), pmtRequest.getTransactionId(), callingMethod);

          // set the card with real gift card info
          birRequest.setCard(populateCard(pmtRequest));

          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - getting GiftCardOnline object");
          GiftCardOnline gco = wscFactory.getGiftCardOnline();

          // invoke the Client to add the interceptor... this is used to add the user name and password
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - adding user-id and password");
          Client cxfClient = ClientProxy.getClient(gco);
          cxfClient.getOutInterceptors().add(this.sInterceptor);

          // make the call to SVS
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - getBalance - calling SVS");
          birResponse = gco.balanceInquiry(birRequest);

          // parse BalanceInquiryResponse and set the pmtResponse
          parseResponse(pmtResponse, birResponse, psUtil);

          // use Xstream to convert the response object sent from SVS to a string in xml format
          sBirResponse = xstream.toXML(birResponse);          

          logger.info("### PaymentServiceClientSVSImpl " + callingMethod + " #### - balance - finish");
      }
      catch (Exception e)
      {
          logger.error(e);
          pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_FAILURE_CODE);
          pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_FAILURE_MESSAGE);
          pmtResponse.setIsSuccessful(false);
      }
      finally
      {
          try
          {
        	// set Error values
              if(pmtResponse.getIsSuccessful()) {
            	  setErrorResponse(pmtResponse, birResponse.getReturnCode());
              }
              if(paymentTrackingId != null) {
        		  // and log it
                  updatePaymentTracking(psDao, paymentTrackingId, null, pmtResponse.getErrorCode(), pmtResponse.getErrorMessage(), null, null, sBirResponse, pmtResponse.getResponseCode(),
                          pmtResponse.getResponseMessage(), null, null, callingMethod);
        	  }
              conn.close();              
          }
          catch (Exception e)
          {
              conn = null;
          }

      }

      return pmtResponse;
  }


  /**
   * This method will call BalanceInquiry and PreAuthRequest on the SVS web service only if the gift card has sufficient balance
   */
  @Override
  public PaymentResponse checkBalanceAndAuthorizeForExactAmount(PaymentRequest pmtRequest) throws Exception
  {
      String callingMethod = "checkBalanceAndAuthorizeForExactAmount";
      logger.info("### PaymentServiceClientSVSImpl " + callingMethod + " #### - initiated");

      PaymentResponse balanceResponse = getBalance(pmtRequest, callingMethod);
      if (balanceResponse.getErrorCode() != null || balanceResponse.getBalanceAmountVO() == null)
      {
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - Balance Inquiry failed.");
          balanceResponse.setResponseCode("-1");
          balanceResponse.setIsSuccessful(false);
          return balanceResponse;
      }

      Double balAmt = balanceResponse.getBalanceAmountVO().getAmount();
      Double authAmt = pmtRequest.getAmountVO().getAmount();
      if (balAmt.compareTo(authAmt) < 0)
      {
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - Balance Inquiry insufficient funds.");
          balanceResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_INSUFFICIENT_FUNDS);
          balanceResponse.setResponseCode("-1");
          balanceResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_SVS_FAILURE_MESSAGE);
          balanceResponse.setIsSuccessful(false);
          return balanceResponse;
      }         

      PaymentResponse authResponse = getAuthorization(pmtRequest, callingMethod);
      return authResponse;
  }



  /**
   * This method will call PreAuthRequest on the SVS web service
   */
  @Override
  public PaymentResponse getAuthorization(PaymentRequest pmtRequest) throws Exception
  {
      return getAuthorization(pmtRequest, "getAuthorization");
  }

  /**
   * This method will call PreAuthRequest on the SVS web service - it is the core of getBalance, 
   * allowing for calls from multiple methods. It will log and record the callingMethod.
   * @param pmtRequest
   * @param callingMethod
   * @return
   * @throws Exception
   */
  private PaymentResponse getAuthorization(PaymentRequest pmtRequest, String callingMethod) throws Exception
  {
      logger.info("### PaymentServiceClientSVSImpl " + callingMethod + " #### - initiated");

      //instantiate the common utility
      CommonUtil psUtil = new CommonUtil();

      //instantiate the Xstream object
      XStream xstream = getXStreamObject();

      // define variables
      Connection conn = null;
      PaymentResponse pmtResponse = new PaymentResponse();
      pmtResponse.setIsSuccessful(true);
      PreAuthRequest preauthRequest = new PreAuthRequest();
      PaymentServiceDAO psDao = null;
      PreAuthResponse2 preauthResponse = null;
      String sBirResponse = null;
      String paymentTrackingId = null;
      // validate client info
      // Note : If validation fails, PaymentResponse will still show a success to the client... no further processing shall commence.  
      boolean validClient = checkClient(pmtRequest.getClientUserName(), pmtRequest.getClientPassword(), psUtil);
      if (!validClient)
      {
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - Invalid client info detected");
          return pmtResponse;
      }

      boolean validTransactionId = checkTransactionId(pmtRequest, pmtResponse);
      if (!validTransactionId)
      {
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - Invalid transaction info detected");
          return pmtResponse;
      }

      // set the date
      GregorianCalendar gcCal = new GregorianCalendar();
      XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
      preauthRequest.setDate(xmlgcCal);

      // set invoice number
      if (StringUtils.isBlank(pmtRequest.getInvoiceNumber()))
          preauthRequest.setInvoiceNumber("00000000");
      else
      {
          int paddingSize = 8 - pmtRequest.getInvoiceNumber().length();
          preauthRequest.setInvoiceNumber(psUtil.pad(pmtRequest.getInvoiceNumber(), PaymentServiceConstants.PAD_LEFT, "0", paddingSize));
      }

      // set merchant
      preauthRequest.setMerchant(populateMerchant(psUtil));

      // set transaction amount
      Amount amt = new Amount();
      amt.setAmount(pmtRequest.getAmountVO().getAmount());
      amt.setCurrency(pmtRequest.getAmountVO().getCurrency());
      preauthRequest.setRequestedAmount(amt);

      // set routing id
      preauthRequest.setRoutingID(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_ROUTING_ID));

      // set Systems Trace Audit Number. Note that STAN is not required for
      // Duplicate-Check process
      preauthRequest.setStan(null);

      // set transaction id
      preauthRequest.setTransactionID(pmtRequest.getTransactionId());

      // set duplicate check
      preauthRequest.setCheckForDuplicate(true);

      // set the card with masked gift card info... only after we get the string to log in Database, we populate it the way we send it to SVS
      preauthRequest.setCard(populateCardMasked(pmtRequest, psUtil));

      try
      {
          //get the connection
          conn = psUtil.getNewConnection();

          //instantiate DAO
          psDao = new PaymentServiceDAO(conn);

          // use Xstream to convert the request object to be sent to SVS to string in xml format
          String sPreauthRequest = xstream.toXML(preauthRequest);

          // log the request being sent to SVS
          paymentTrackingId = insertPaymentTracking(psDao, pmtRequest.getClientUserName(), null, null, pmtRequest.getPaymentType(), sPreauthRequest, null, null, null,
                  pmtRequest.getSourceCode(), pmtRequest.getTransactionId(), callingMethod);

          // set the card with real gift card info
          preauthRequest.setCard(populateCard(pmtRequest));

          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - getting GiftCardOnline object");
          GiftCardOnline gco = wscFactory.getGiftCardOnline();

          // invoke the Client to add the interceptor... this is used to add the user name and password
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - adding user-id and password");
          Client cxfClient = ClientProxy.getClient(gco);
          cxfClient.getOutInterceptors().add(this.sInterceptor);

          // make the call to SVS
          logger.debug("### PaymentServiceClientSVSImpl " + callingMethod + " #### - getAuthorization - calling SVS");
          preauthResponse = gco.preAuth(preauthRequest);

          // parse PreAuthResponse and set the pmtResponse
          parseResponse(pmtResponse, preauthResponse, psUtil);

          // use Xstream to convert the response object sent from SVS to a string in xml format
          sBirResponse = xstream.toXML(preauthResponse);          

          logger.info("### PaymentServiceClientSVSImpl " + callingMethod + " #### - balance - finish");
      }
      catch (Exception e)
      {
          logger.error(e);
          pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_FAILURE_CODE);
          pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_FAILURE_MESSAGE);
          pmtResponse.setIsSuccessful(false);
      }
      finally
      {
          try
          {
        	  if(pmtResponse.getIsSuccessful()) {
        		  setErrorResponse(pmtResponse, preauthResponse.getReturnCode());  
        	  }                      	  
              if(paymentTrackingId != null) {
            	  updatePaymentTracking(psDao, paymentTrackingId, null, pmtResponse.getErrorCode(), pmtResponse.getErrorMessage(), null, null, sBirResponse, pmtResponse.getResponseCode(),
                      pmtResponse.getResponseMessage(), null, null, callingMethod);
              }
              conn.close();
          }
          catch (Exception e)
          {
              conn = null;
          }

      }  
      return pmtResponse;
  }

  /**
   * This method will call MerchandiseReturnRequest on the SVS web service
   */
  @Override
  public PaymentResponse refund(PaymentRequest pmtRequest) throws Exception
  {
    logger.info("refund - initiated");

    //instantiate the common utility
    CommonUtil psUtil = new CommonUtil();

    //instantiate the Xstream object
    XStream xstream = getXStreamObject();

    // define variables
    Connection conn = null;
    PaymentResponse pmtResponse = new PaymentResponse();
    pmtResponse.setIsSuccessful(true);
    MerchandiseReturnRequest mrRequest = new MerchandiseReturnRequest();
    String paymentTrackingId = null;
    MerchandiseReturnResponse2 mrResponse = null;
    String sMrResponse = null;
    PaymentServiceDAO psDao = null;

    // validate client info
    // Note : If validation fails, PaymentResponse will still show a success to the client... no further processing shall commence.  
    boolean validClient = checkClient(pmtRequest.getClientUserName(), pmtRequest.getClientPassword(), psUtil);
    if (!validClient)
    {
      logger.debug("Invalid client info detected");
      return pmtResponse;
    }

    boolean validTransactionId = checkTransactionId(pmtRequest, pmtResponse);
    if (!validTransactionId)
    {
      logger.debug("Invalid transaction info detected");
      return pmtResponse;
    }

    // set the date
    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    mrRequest.setDate(xmlgcCal);

    // set invoice number
    if (StringUtils.isBlank(pmtRequest.getInvoiceNumber()))
      mrRequest.setInvoiceNumber("00000000");
    else
    {
      int paddingSize = 8 - pmtRequest.getInvoiceNumber().length();
      mrRequest.setInvoiceNumber(psUtil.pad(pmtRequest.getInvoiceNumber(), PaymentServiceConstants.PAD_LEFT, "0", paddingSize));
    }

    // set merchant
    mrRequest.setMerchant(populateMerchant(psUtil));

    // set transaction amount
    Amount amt = new Amount();
    amt.setAmount(pmtRequest.getAmountVO().getAmount());
    amt.setCurrency(pmtRequest.getAmountVO().getCurrency());
    mrRequest.setReturnAmount(amt);

    // set routing id
    mrRequest.setRoutingID(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_ROUTING_ID));

    // set Systems Trace Audit Number. Note that STAN is not required for
    // Duplicate-Check process
    mrRequest.setStan(null);

    // set transaction id
    mrRequest.setTransactionID(pmtRequest.getTransactionId());

    // set duplicate check
    mrRequest.setCheckForDuplicate(true);

    // set the card with masked gift card info... only after we get the string to log in Database, we populate it the way we send it to SVS
    mrRequest.setCard(populateCardMasked(pmtRequest, psUtil));

    try
    {
      //get the connection
      conn = psUtil.getNewConnection();

      //instantiate DAO
      psDao = new PaymentServiceDAO(conn);

      // use Xstream to convert the request object to be sent to SVS to string in xml format
      String sMrRequest = xstream.toXML(mrRequest);

      // log the request being sent to SVS      
      paymentTrackingId = insertPaymentTracking(psDao, pmtRequest.getClientUserName(), null, null, pmtRequest.getPaymentType(), sMrRequest, null, null, null,
                                                       pmtRequest.getSourceCode(), pmtRequest.getTransactionId(), "refund");

      // set the card with real gift card info
      mrRequest.setCard(populateCard(pmtRequest));

      logger.debug("getting GiftCardOnline object");
      GiftCardOnline gco = wscFactory.getGiftCardOnline();

      // invoke the Client to add the interceptor... this is used to add the user name and password
      logger.debug("adding user-id and password");
      Client cxfClient = ClientProxy.getClient(gco);
      cxfClient.getOutInterceptors().add(this.sInterceptor);

      // make the call to SVS
      logger.debug("refund - calling SVS");
      mrResponse = gco.merchandiseReturn(mrRequest);

      // parse MerchandiseReturnResponse and set the pmtResponse
      parseResponse(pmtResponse, mrResponse, psUtil);

      // use Xstream to convert the response object sent from SVS to a string in xml format
      sMrResponse = xstream.toXML(mrResponse);      
      logger.info("refund - finish");
    }
    catch (Exception e)
    {
      logger.error(e);
      pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_FAILURE_CODE);
      pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_FAILURE_MESSAGE);
      pmtResponse.setIsSuccessful(false);
    }
    finally
    {
      try
      {
    	// set Error values    	  
          if(pmtResponse.getIsSuccessful()) {
        	  setErrorResponse(pmtResponse, mrResponse.getReturnCode());  
          }    	  
          if(paymentTrackingId != null) {    	  
        	  updatePaymentTracking(psDao, paymentTrackingId, null, pmtResponse.getErrorCode(), pmtResponse.getErrorMessage(), null, null, sMrResponse, pmtResponse.getResponseCode(),
                                pmtResponse.getResponseMessage(), null, null, "refund");
          }
        conn.close();
      }
      catch (Exception e)
      {
        conn = null;
      }

    }

    return pmtResponse;
  }

  /**
   * This method will call PreAuthCompleteRequest on the SVS web service
   */
  @Override
  public PaymentResponse settle(PaymentRequest pmtRequest) throws Exception
  {
    logger.info("settle - initiated");

    //instantiate the common utility
    CommonUtil psUtil = new CommonUtil();

    //instantiate the Xstream object
    XStream xstream = getXStreamObject();

    // define variables
    Connection conn = null;
    PaymentResponse pmtResponse = new PaymentResponse();
    pmtResponse.setIsSuccessful(true);
    PreAuthCompleteRequest pacRequest = new PreAuthCompleteRequest();
    String paymentTrackingId = null;
    PaymentServiceDAO psDao = null;
    String sPacResponse = null;
    PreAuthCompleteResponse2 pacResponse = null;

    // validate client info
    // Note : If validation fails, PaymentResponse will still show a success to the client... no further processing shall commence.  
    boolean validClient = checkClient(pmtRequest.getClientUserName(), pmtRequest.getClientPassword(), psUtil);
    if (!validClient)
    {
      logger.debug("Invalid client info detected");
      return pmtResponse;
    }

    boolean validTransactionId = checkTransactionId(pmtRequest, pmtResponse);
    if (!validTransactionId)
    {
      logger.debug("Invalid transaction info detected");
      return pmtResponse;
    }

    // set the date
    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    pacRequest.setDate(xmlgcCal);

    // set invoice number
    if (StringUtils.isBlank(pmtRequest.getInvoiceNumber()))
      pacRequest.setInvoiceNumber("00000000");
    else
    {
      int paddingSize = 8 - pmtRequest.getInvoiceNumber().length();
      pacRequest.setInvoiceNumber(psUtil.pad(pmtRequest.getInvoiceNumber(), PaymentServiceConstants.PAD_LEFT, "0", paddingSize));
    }

    // set merchant
    pacRequest.setMerchant(populateMerchant(psUtil));

    // set transaction amount
    Amount amt = new Amount();
    amt.setAmount(pmtRequest.getAmountVO().getAmount());
    amt.setCurrency(pmtRequest.getAmountVO().getCurrency());
    pacRequest.setTransactionAmount(amt);

    // set routing id
    pacRequest.setRoutingID(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_ROUTING_ID));

    // set Systems Trace Audit Number. Note that STAN is not required for
    // Duplicate-Check process
    pacRequest.setStan(null);

    // set transaction id
    pacRequest.setTransactionID(pmtRequest.getTransactionId());

    // set duplicate check
    pacRequest.setCheckForDuplicate(true);

    // set the card with masked gift card info... only after we get the string to log in Database, we populate it the way we send it to SVS
    pacRequest.setCard(populateCardMasked(pmtRequest, psUtil));

    try
    {
      //get the connection
      conn = psUtil.getNewConnection();

      //instantiate DAO
      psDao = new PaymentServiceDAO(conn);

      // use Xstream to convert the request object to be sent to SVS to string in xml format
      String sPacRequest = xstream.toXML(pacRequest);

      // log the request being sent to SVS
      paymentTrackingId = insertPaymentTracking(psDao, pmtRequest.getClientUserName(), null, null, pmtRequest.getPaymentType(), sPacRequest, null, null, null,
                                                       pmtRequest.getSourceCode(), pmtRequest.getTransactionId(), "settle");

      // set the card with real gift card info
      pacRequest.setCard(populateCard(pmtRequest));

      logger.debug("getting GiftCardOnline object");
      GiftCardOnline gco = wscFactory.getGiftCardOnline();

      // invoke the Client to add the interceptor... this is used to add the user name and password
      logger.debug("adding user-id and password");
      Client cxfClient = ClientProxy.getClient(gco);
      cxfClient.getOutInterceptors().add(this.sInterceptor);

      // make the call to SVS
      logger.debug("settle - calling SVS");
      pacResponse = gco.preAuthComplete(pacRequest);

      // parse PreAuthCompleteResponse and set the pmtResponse
      parseResponse(pmtResponse, pacResponse, psUtil);

      // use Xstream to convert the response object sent from SVS to a string in xml format
      sPacResponse = xstream.toXML(pacResponse);     

      logger.info("settle - finish");
    }
    catch (Exception e)
    {
      logger.error(e);
      pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_FAILURE_CODE);
      pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_FAILURE_MESSAGE);
      pmtResponse.setIsSuccessful(false);
    }
    finally
    {
      try
      {
    	if(pmtResponse.getIsSuccessful()) {
    		setErrorResponse(pmtResponse, pacResponse.getReturnCode());  
    	}
    	
    	if(paymentTrackingId != null) {
    		updatePaymentTracking(psDao, paymentTrackingId, null, pmtResponse.getErrorCode(), pmtResponse.getErrorMessage(), null, null, sPacResponse,
                pmtResponse.getResponseCode(), pmtResponse.getResponseMessage(), null, null, "settle");
    	}

        conn.close();
      }
      catch (Exception e)
      {
        conn = null;
      }

    }

    return pmtResponse;
  }

  /**
   * This method will call PreAuthCompleteRequest on the SVS web service
   */
  @Override
  public PaymentResponse cancelAuthorization(PaymentRequest pmtRequest) throws Exception
  {

	  logger.info("### PaymentServiceClientSVSImpl cancelAuthorization #### - initiated");

	    //instantiate the common utility
	    CommonUtil psUtil = new CommonUtil();

	    //instantiate the Xstream object
	    XStream xstream = getXStreamObject();

	    // define variables
	    Connection conn = null;
	    PaymentResponse pmtResponse = new PaymentResponse();
	    pmtResponse.setIsSuccessful(true);
	    PreAuthCompleteRequest preauthcompleteRequest = new PreAuthCompleteRequest();
	    String paymentTrackingId = null;
	    PaymentServiceDAO psDao = null;
	    String sBirResponse = null;
	    PreAuthCompleteResponse2 preauthcompleteResponse = null;

	    // validate client info
	    // Note : If validation fails, PaymentResponse will still show a success to the client... no further processing shall commence.  
	    boolean validClient = checkClient(pmtRequest.getClientUserName(), pmtRequest.getClientPassword(), psUtil);
	    if (!validClient)
	    {
	      logger.debug("### PaymentServiceClientSVSImpl cancelAuthorization #### - Invalid client info detected");
	      return pmtResponse;
	    }

	    boolean validTransactionId = checkTransactionId(pmtRequest, pmtResponse);
	    if (!validTransactionId)
	    {
	      logger.debug("### PaymentServiceClientSVSImpl cancelAuthorization #### - Invalid transaction info detected");
	      return pmtResponse;
	    }

	    // set the date
	    GregorianCalendar gcCal = new GregorianCalendar();
	    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
	    preauthcompleteRequest.setDate(xmlgcCal);

	    // set invoice number
	    if (StringUtils.isBlank(pmtRequest.getInvoiceNumber()))
	    	preauthcompleteRequest.setInvoiceNumber("00000000");
	    else
	    {
	      int paddingSize = 8 - pmtRequest.getInvoiceNumber().length();
	      preauthcompleteRequest.setInvoiceNumber(psUtil.pad(pmtRequest.getInvoiceNumber(), PaymentServiceConstants.PAD_LEFT, "0", paddingSize));
	    }

	    // set merchant
	    preauthcompleteRequest.setMerchant(populateMerchant(psUtil));

	    // set transaction amount
	    Amount amt = new Amount();
	    amt.setAmount(pmtRequest.getAmountVO().getAmount());
	    amt.setCurrency(pmtRequest.getAmountVO().getCurrency());
	    preauthcompleteRequest.setTransactionAmount(amt);

	    // set routing id
	    preauthcompleteRequest.setRoutingID(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_ROUTING_ID));

	    // set Systems Trace Audit Number. Note that STAN is not required for
	    // Duplicate-Check process
	    preauthcompleteRequest.setStan(null);

	    // set transaction id
	    preauthcompleteRequest.setTransactionID(pmtRequest.getTransactionId());

	    // set duplicate check
	    preauthcompleteRequest.setCheckForDuplicate(true);

	    // set the card with masked gift card info... only after we get the string to log in Database, we populate it the way we send it to SVS
	    preauthcompleteRequest.setCard(populateCardMasked(pmtRequest, psUtil));

	    try
	    {
	      //get the connection
	      conn = psUtil.getNewConnection();

	      //instantiate DAO
	      psDao = new PaymentServiceDAO(conn);

	      // use Xstream to convert the request object to be sent to SVS to string in xml format
	      String sPreauthcompleteRequest = xstream.toXML(preauthcompleteRequest);

	      // log the request being sent to SVS
	      paymentTrackingId = insertPaymentTracking(psDao, pmtRequest.getClientUserName(), null, null, pmtRequest.getPaymentType(), sPreauthcompleteRequest, null, null, null,
	                                                       pmtRequest.getSourceCode(), pmtRequest.getTransactionId(), "cancelAuthorization");

	      // set the card with real gift card info
	      preauthcompleteRequest.setCard(populateCard(pmtRequest));

	      logger.debug("### PaymentServiceClientSVSImpl cancelAuthorization #### - getting GiftCardOnline object");
	      GiftCardOnline gco = wscFactory.getGiftCardOnline();

	      // invoke the Client to add the interceptor... this is used to add the user name and password
	      logger.debug("### PaymentServiceClientSVSImpl cancelAuthorization #### - adding user-id and password");
	      Client cxfClient = ClientProxy.getClient(gco);
	      cxfClient.getOutInterceptors().add(this.sInterceptor);

	      // make the call to SVS
	      logger.debug("### PaymentServiceClientSVSImpl cancelAuthorization #### - cancelAuthorization - calling SVS");
	      preauthcompleteResponse = gco.preAuthComplete(preauthcompleteRequest);

	      // parse PreAuthCompleteResponse and set the pmtResponse
	      parseResponse(pmtResponse, preauthcompleteResponse, psUtil);

	      // use Xstream to convert the response object sent from SVS to a string in xml format
	      sBirResponse = xstream.toXML(preauthcompleteResponse);      

	      logger.info("### PaymentServiceClientSVSImpl getAuthorization #### - balance - finish");
	    }
	    catch (Exception e)
	    {
	      logger.error(e);
	      pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_FAILURE_CODE);
	      pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_FAILURE_MESSAGE);
	      pmtResponse.setIsSuccessful(false);
	    }
	    finally
	    {
	      try
	      {
	    	  if(pmtResponse.getIsSuccessful()) {
	    		 setErrorResponse(pmtResponse, preauthcompleteResponse.getReturnCode());  
	    	  }
	    	  
	    	  if(paymentTrackingId != null) {
	    		  updatePaymentTracking(psDao, paymentTrackingId, null, pmtResponse.getErrorCode(), pmtResponse.getErrorMessage(), null, null, sBirResponse, pmtResponse.getResponseCode(),
                      pmtResponse.getResponseMessage(), null, null, "cancelAuthorization");
	    	  }

	        conn.close();
	      }
	      catch (Exception e)
	      {
	        conn = null;
	      }

	    }  
    return pmtResponse;
  }

  /**
   * @return Returns an instance of Xstream to convert Request / Response
   *         objects to be converted to String to be logged in the DB
   */
  private XStream getXStreamObject()
  {

    XStream xstream = new XStream()
    {
      protected MapperWrapper wrapMapper(MapperWrapper next)
      {
        return new MapperWrapper(next)
          {
            public boolean shouldSerializeMember(Class definedIn, String fieldName)
            {
              boolean retVal = definedIn != Object.class ? super.shouldSerializeMember(definedIn, fieldName) : false;

              if (!retVal)
              {
                try
                {
                  Class realClazz = realClass(fieldName);
                  if (realClazz != null)
                  {
                    retVal = true;
                  }
                }
                catch (Exception e)
                {
                }
              }
              return retVal;
            }
          };
        }
    };

    return xstream;
  }

  /**
   * @return Card. This method will return a Card object where the Gift Card and
   *         Pin Numbers are masked. This method will be used primarily
   *         to get the Card object that can be stored in the DB without
   *         violating any PCI
   */
  private Card populateCardMasked(PaymentRequest pmtRequest, CommonUtil psUtil) throws Exception
  {
    Card card = new Card();

    if (pmtRequest.getCardVO() != null)
    {
      card.setCardCurrency(pmtRequest.getCardVO().getCardCurrency());
      card.setCardExpiration(pmtRequest.getCardVO().getCardExpiration());
      card.setCardTrackOne(pmtRequest.getCardVO().getCardTrackOne());
      card.setCardTrackTwo(pmtRequest.getCardVO().getCardTrackTwo());
      card.setCardNumber(psUtil.mask(pmtRequest.getCardVO().getCardNumber(), PaymentServiceConstants.MASK_LEFT, "*", 4));
      card.setPinNumber(psUtil.mask(pmtRequest.getCardVO().getPinNumber(), PaymentServiceConstants.MASK_LEFT, "*", 1));
    }

    return card;
  }

  /**
   * @return Card. This method will return a Card object where the Gift Card and
   *         Pin Numbers are NOT masked. This Card object should NEVER
   *         be stored in DB... should be used to call the SVS service.
   */
  private Card populateCard(PaymentRequest pmtRequest) throws Exception
  {
    Card card = new Card();

    if (pmtRequest.getCardVO() != null)
    {
      card.setCardCurrency(pmtRequest.getCardVO().getCardCurrency());
      card.setCardNumber(pmtRequest.getCardVO().getCardNumber());
      card.setCardTrackOne(pmtRequest.getCardVO().getCardTrackOne());
      card.setCardTrackTwo(pmtRequest.getCardVO().getCardTrackTwo());
      card.setCardExpiration(pmtRequest.getCardVO().getCardExpiration());
      card.setPinNumber(pmtRequest.getCardVO().getPinNumber());
    }

    return card;
  }

  /**
   * @return Merchant. Common method (used by all SVS transactions) to populate
   *         the Merchant object.
   */
  private Merchant populateMerchant(CommonUtil psUtil) throws Exception
  {
    Merchant merch = new Merchant();

    merch.setDivision(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_DIVISION_NUMBER));
    merch.setMerchantName(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_MERCHANT_NAME));
    merch.setMerchantNumber(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_MERCHANT_NUMBER));
    merch.setStoreNumber(psUtil.getFrpGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_STORE_NUMBER));

    return merch;
  }

  /**
   * Set the error codes
   */
  private void setErrorResponse(PaymentResponse pmtResponse, ReturnCode rc) throws Exception
  {
    if (rc.getReturnCode().equalsIgnoreCase(PaymentServiceConstants.PS_RC_SUCCESS))
    {
      pmtResponse.setErrorCode(null);
      pmtResponse.setErrorMessage(null);
      pmtResponse.setIsSuccessful(true);
    }
    else
    {
      pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_SVS_FAILURE_CODE);
      pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_SVS_FAILURE_MESSAGE);
      pmtResponse.setIsSuccessful(false);
    }
  }

  /**
   * Call the DAO to insert a TRACKING.PAYMENT_TRACKING record
   */
  private String insertPaymentTracking(PaymentServiceDAO psDao, String clientName, String errorCode, String errorMessage, String paymentType, String svsRequestString,
      String svsResponseString,
      String responseCode, String responseMessage, String sourceCode, String transactionId, String transactionType) throws Exception
  {
    String paymentTrackingId = psDao.insertPaymentTracking(clientName, errorCode, errorMessage, paymentType, svsRequestString, svsResponseString, responseCode,
                                                           responseMessage, sourceCode, transactionId, transactionType);
    return paymentTrackingId;
  }

  /**
   * Call the DAO to update a TRACKING.PAYMENT_TRACKING record
   */
  private void updatePaymentTracking(PaymentServiceDAO psDao, String paymentTrackingId, String clientName, String errorCode, String errorMessage, String paymentType,
      String svsRequestString,
      String svsResponseString, String responseCode, String responseMessage, String sourceCode, String transactionId, String transactionType) throws Exception
  {
    psDao.updatePaymentTracking(paymentTrackingId, clientName, errorCode, errorMessage, paymentType, svsRequestString, svsResponseString, responseCode, responseMessage,
                                sourceCode, transactionId, transactionType);
  }

  /**
   * @return Returns a boolean - checks if a transaction id was passed...
   */
  private boolean checkTransactionId(PaymentRequest pmtRequest, PaymentResponse pmtResponse) throws Exception
  {
    boolean validTransactionId = true;

    if (pmtRequest == null || pmtRequest.getTransactionId() == null
        || pmtRequest.getTransactionId().equalsIgnoreCase(""))
    {
      pmtResponse.setErrorCode(PaymentServiceConstants.PS_ERROR_TRANSACTION_ID_CODE);
      pmtResponse.setErrorMessage(PaymentServiceConstants.PS_ERROR_TRANSACTION_ID_MESSAGE);
      pmtResponse.setIsSuccessful(false);
      validTransactionId = false;
    }

    return validTransactionId;
  }

  /**
   * Parse BalanceInquiryResponse
   */
  private void parseResponse(PaymentResponse pmtResponse, BalanceInquiryResponse2 svsResponse, CommonUtil psUtil) throws Exception
  {

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(psUtil.mask((((Card) svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(psUtil.mask((((Card) svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));

      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card) svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card) svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card) svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card) svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card) svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card) svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }

    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode) svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode) svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }

  }

  /**
   * Parse PreAuthCompleteResponse
   */
  private void parseResponse(PaymentResponse pmtResponse, PreAuthCompleteResponse2 svsResponse, CommonUtil psUtil) throws Exception
  {

    if (svsResponse.getApprovedAmount() != null)
    {
      AmountVO approvedAmountVO = new AmountVO();
      approvedAmountVO.setAmount(((Amount) svsResponse.getApprovedAmount()).getAmount());
      approvedAmountVO.setCurrency(((Amount) svsResponse.getApprovedAmount()).getCurrency());
      pmtResponse.setApprovedAmountVO(approvedAmountVO);
    }

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(psUtil.mask((((Card) svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(psUtil.mask((((Card) svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));

      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card) svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card) svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card) svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card) svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card) svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card) svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }

    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode) svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode) svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }

  }

  /**
   * Parse MerchandiseReturnResponse
   */
  private void parseResponse(PaymentResponse pmtResponse, MerchandiseReturnResponse2 svsResponse, CommonUtil psUtil) throws Exception
  {

    if (svsResponse.getApprovedAmount() != null)
    {
      AmountVO approvedAmountVO = new AmountVO();
      approvedAmountVO.setAmount(((Amount) svsResponse.getApprovedAmount()).getAmount());
      approvedAmountVO.setCurrency(((Amount) svsResponse.getApprovedAmount()).getCurrency());
      pmtResponse.setApprovedAmountVO(approvedAmountVO);
    }

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(psUtil.mask((((Card) svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(psUtil.mask((((Card) svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));

      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card) svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card) svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card) svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card) svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card) svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card) svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }

    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode) svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode) svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }

  }

  /**
   * Parse PreAuthResponse
   */
  private void parseResponse(PaymentResponse pmtResponse, PreAuthResponse2 svsResponse, CommonUtil psUtil) throws Exception
  {

    if (svsResponse.getApprovedAmount() != null)
    {
      AmountVO approvedAmountVO = new AmountVO();
      approvedAmountVO.setAmount(((Amount) svsResponse.getApprovedAmount()).getAmount());
      approvedAmountVO.setCurrency(((Amount) svsResponse.getApprovedAmount()).getCurrency());
      pmtResponse.setApprovedAmountVO(approvedAmountVO);
    }

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(psUtil.mask((((Card) svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(psUtil.mask((((Card) svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));

      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card) svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card) svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card) svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card) svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card) svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card) svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }

    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode) svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode) svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }

  }

}
