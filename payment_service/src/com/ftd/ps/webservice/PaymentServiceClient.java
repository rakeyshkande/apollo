package com.ftd.ps.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.ftd.ps.webservice.vo.PaymentRequest;
import com.ftd.ps.webservice.vo.PaymentResponse;


/**
 * Web service interface for an payment service client
 * 
 * @author alakhani
 *
 */
@WebService
public interface PaymentServiceClient {
    
    /**
     *  given a payment request, depending on the payment type, verify the payment against payment service implementation. 
     * @param paymentRequest - contains the payment info
     * @return a paymentResponse 
     */
    PaymentResponse getBalance(@WebParam(name="pmtRequest")PaymentRequest pmtRequest) throws Exception;

    PaymentResponse getAuthorization(@WebParam(name="pmtRequest")PaymentRequest pmtRequest) throws Exception;

    PaymentResponse checkBalanceAndAuthorizeForExactAmount(@WebParam(name="pmtRequest")PaymentRequest pmtRequest) throws Exception;

    PaymentResponse refund(@WebParam(name="pmtRequest")PaymentRequest pmtRequest) throws Exception;

    PaymentResponse settle(@WebParam(name="pmtRequest")PaymentRequest pmtRequest) throws Exception;

    PaymentResponse cancelAuthorization(@WebParam(name="pmtRequest")PaymentRequest pmtRequest) throws Exception;

}

