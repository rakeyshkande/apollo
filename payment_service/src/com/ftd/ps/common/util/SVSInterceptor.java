package com.ftd.ps.common.util;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.binding.soap.interceptor.SoapPreProtocolOutInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.ps.common.constant.PaymentServiceConstants;

public class SVSInterceptor extends AbstractSoapInterceptor
{
  private static final Logger logger = new Logger(SVSInterceptor.class.getName());

  public String username;
  public String password;

  public void setUsername(String username)
  {
    this.username = username;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getUsername()
  {
    return username;
  }

  public String getPassword()
  {
    return password;
  }

  public SVSInterceptor() throws Exception
  {
    super(Phase.WRITE);

    CommonUtil util = new CommonUtil();
    setUsername(util.getSecureGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_USER_ID));
    setPassword(util.getSecureGlobalParm(PaymentServiceConstants.PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_PASSWORD));
    addAfter(SoapPreProtocolOutInterceptor.class.getName());
  }

  public void handleMessage(SoapMessage message) throws Fault
  {
    logger.info("SVSInterceptor handleMessage invoked");
    try
    {

      Document doc = (Document) DOMUtil.getDocumentBuilder().newDocument();

      Element elementSecurity = doc.createElement("Security");
      elementSecurity.setAttribute("xmlns", "http://schemas.xmlsoap.org/ws/2003/06/secext");

      Element elementCredentials = doc.createElement("UsernameToken");
      Element elementUser = doc.createElement("Username");
      elementUser.setTextContent(getUsername());
      Element elementPassword = doc.createElement("Password");
      elementPassword.setTextContent(getPassword());
      elementCredentials.appendChild(elementUser);
      elementCredentials.appendChild(elementPassword);

      elementSecurity.appendChild(elementCredentials);

      // Create Header object
      QName qnameCredentials = new QName("UsernameToken");
      Header header = new Header(qnameCredentials, elementSecurity);
      message.getHeaders().add(header);

    }
    catch (Exception e)
    {
    }

  }

}
