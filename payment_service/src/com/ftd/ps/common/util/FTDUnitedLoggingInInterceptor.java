/**
 * 
 */
package com.ftd.ps.common.util;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;

/**
 * @author cjohnson
 *
 */
public class FTDUnitedLoggingInInterceptor extends LoggingInInterceptor {
	
	com.ftd.osp.utilities.plugins.Logger logger = new com.ftd.osp.utilities.plugins.Logger(FTDUnitedLoggingInInterceptor.class.getName());

    /**
     * Transform the string before display. The implementation in this class 
     * does nothing. Override this method if you want to change the contents of the 
     * logged message before it is delivered to the output. 
     * For example, you can use this to mask out sensitive information.
     * @param originalLogString the raw log message.
     * @return transformed data
     */
	@Override
    protected String transform(String originalLogString) {
		String regex = "memberID=\"\\w+\"";
		String maskedLogString = originalLogString
									.replaceAll("memberID=\"\\w+\"", "memberID=\"xxxxxx\"");
		return maskedLogString;
    } 
	
	@Override
    public void handleMessage(Message message) throws Fault {
        if (writer != null || logger.isInfoEnabled()) {
            logging(message);
        }
    }
	
	@Override
    protected void log(String message) {
        message = transform(message);
        if (writer != null) {
            writer.println(message);
            // Flushing the writer to make sure the message is written 
            writer.flush();
        } else if (logger.isInfoEnabled()) {
            logger.info(message);
        }
    }
	


}
