package com.ftd.ps.common.util;

import static com.ftd.milespoints.common.constant.MilesPointsConstants.CONFIG_CONTEXT;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.binding.soap.interceptor.SoapPreProtocolOutInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.ps.common.constant.PaymentServiceConstants;

public class UnitedServiceInterceptor extends AbstractSoapInterceptor
{
  private static final Logger logger = new Logger(UnitedServiceInterceptor.class.getName());
  
  private static final String XSD_WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
  private static final String XSD_WSSE_PASSWORD = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";

  CommonUtil util;
  public String username;
  public String password;

  public void setUsername(String username)
  {
    this.username = username;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getUsername()
  {
    return username;
  }

  public String getPassword()
  {
    return password;
  }

  public UnitedServiceInterceptor()
  {
    super(Phase.WRITE);
    

    addAfter(SoapPreProtocolOutInterceptor.class.getName());
  }

  public void handleMessage(SoapMessage message) throws Fault
  {
    if(util == null) {
    	util = new CommonUtil();
    }

    try {
		setUsername(util.getFrpGlobalParm(CONFIG_CONTEXT, MilesPointsConstants.UA_SERVICE_USERNAME));
	    setPassword(util.getSecureGlobalParm(CONFIG_CONTEXT, MilesPointsConstants.UA_SERVICE_PASSWORD));
	}  catch (Exception e1) {
		e1.printStackTrace();
	}

	  
    System.out.println("SVSInterceptor handleMessage invoked");
    try
    {

      Document doc = (Document) DOMUtil.getDocumentBuilder().newDocument();

      Element elementSecurity = doc.createElement("Security");
      elementSecurity.setAttribute("xmlns", XSD_WSSE);

      Element elementCredentials = doc.createElement("UsernameToken");
      Element elementUser = doc.createElement("Username");
      elementUser.setTextContent(getUsername());
      Element elementPassword = doc.createElement("Password");
      elementPassword.setTextContent(getPassword());
      elementCredentials.appendChild(elementUser);
      elementCredentials.appendChild(elementPassword);

      elementSecurity.appendChild(elementCredentials);

      // Create Header object
      QName qnameCredentials = new QName("UsernameToken");
      Header header = new Header(qnameCredentials, elementSecurity);

      boolean hasSecurityHeader = false;
      for (Header hdr : message.getHeaders()) {
    	  if (hdr.getName().getLocalPart().equals("UsernameToken")) {
    		  hasSecurityHeader = true;
    	  }
      }
      
      if (!hasSecurityHeader) {
          message.getHeaders().add(header);
      }

    }
    catch (Exception e)
    {
    }

  }

}
