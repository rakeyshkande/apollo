/**
 * 
 */
package com.ftd.ps.common.util;

import java.io.OutputStream;
import java.util.logging.Level;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.message.Message;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * We are overriding a couple of methods from LoggingOutIntercepter and hooking
 * into our own FTD logging mechanism.
 * 
 * In addition, we mask out the memberID wherever it appears
 * 
 * @author cjohnson
 *
 */
public class FTDUnitedLoggingOutInterceptor extends LoggingOutInterceptor {
	
	Logger logger = new Logger(FTDUnitedLoggingOutInterceptor.class.getName());
	
	/**
	 * This is where we mask out the member ID
	 */
	@Override
	protected String transform(String originalLogString) {

		String maskedLogString = originalLogString
									.replaceAll("memberID=\"\\w+\"", "memberID=\"xxxxxx\"")
									.replaceAll("loyaltyProgramNbr=\"\\w+\"", "loyaltyProgramNbr=\"xxxxx\"");
		return maskedLogString;
	}

	@Override
	public void handleMessage(Message message) throws Fault {
		 final OutputStream os = message.getContent(OutputStream.class);
         if (os == null) {
             return;
         }
 
         if (logger.isInfoEnabled()) {
             // Write the output while caching it for the log message
             final CacheAndWriteOutputStream newOut = new CacheAndWriteOutputStream(os);
             message.setContent(OutputStream.class, newOut);
             newOut.registerCallback(new LoggingCallback(message, os));
         }
	}
	
	class LoggingCallback implements CachedOutputStreamCallback {
        
        private final Message message;
        private final OutputStream origStream;
        
        public LoggingCallback(final Message msg, final OutputStream os) {
            this.message = msg;
            this.origStream = os;
        }

        public void onFlush(CachedOutputStream cos) {  
            
        }
        
        public void onClose(CachedOutputStream cos) {
            String id = (String)message.getExchange().get(LoggingMessage.ID_KEY);
            if (id == null) {
                id = LoggingMessage.nextId();
                message.getExchange().put(LoggingMessage.ID_KEY, id);
            }
            final LoggingMessage buffer 
                = new LoggingMessage("Outbound Message\n---------------------------",
                                     id);
            
            Integer responseCode = (Integer)message.get(Message.RESPONSE_CODE);
            if (responseCode != null) {
                buffer.getResponseCode().append(responseCode);
            }
            
            String encoding = (String)message.get(Message.ENCODING);

            if (encoding != null) {
                buffer.getEncoding().append(encoding);
            }            
            
            String address = (String)message.get(Message.ENDPOINT_ADDRESS);
            if (address != null) {
                buffer.getAddress().append(address);
            }
            String ct = (String)message.get(Message.CONTENT_TYPE);
            if (ct != null) {
                buffer.getContentType().append(ct);
            }
            Object headers = message.get(Message.PROTOCOL_HEADERS);
            if (headers != null) {
                buffer.getHeader().append(headers);
            }

            if (cos.getTempFile() == null) {
                //buffer.append("Outbound Message:\n");
                if (cos.size() > limit) {
                    buffer.getMessage().append("(message truncated to " + limit + " bytes)\n");
                }
            } else {
                buffer.getMessage().append("Outbound Message (saved to tmp file):\n");
                buffer.getMessage().append("Filename: " + cos.getTempFile().getAbsolutePath() + "\n");
                if (cos.size() > limit) {
                    buffer.getMessage().append("(message truncated to " + limit + " bytes)\n");
                }
            }
            try {
                writePayload(buffer.getPayload(), cos, encoding, ct); 
            } catch (Exception ex) {
                //ignore
            }

            if (writer != null) {
                writer.println(transform(buffer.toString()));
            } else if (logger.isInfoEnabled()) {
                logger.info(transform(buffer.toString()));
            }
            try {
                //empty out the cache
                cos.lockOutputStream();
                cos.resetOut(null, false);
            } catch (Exception ex) {
                //ignore
            }
            message.setContent(OutputStream.class, 
                               origStream);
        }
    }

}
