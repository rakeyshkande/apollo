package com.ftd.ps.common.util;

import static com.ftd.milespoints.common.constant.MilesPointsConstants.CONFIG_CONTEXT;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_UPDATE_MILES_SERVICE_ENDPOINT_URL;
import static com.ftd.ps.common.constant.PaymentServiceConstants.PS_CONTEXT;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.ws.BindingProvider;

import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.headers.Header;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.constant.PaymentServiceConstants;
import com.storedvalue.giftcard.GiftCardOnline;
import com.storedvalue.giftcard.GiftCardOnlineService;
import com.ual.cust.ptvl.isstkt.prodsvc.GetPartnerProfilePortType;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerRetrvProfile_Service;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesPort;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesService;
import com.ual.hcp.has.RequestHeader;
import com.ual.hcp.has.RequestHeader.StatelessRequest;
import com.ual.hcp.has.RequestHeader.StatelessRequest.Hosted;
import com.ual.hcp.has.RequestHeader.StatelessRequest.Hosted.Role;
import com.ual.hcp.has.RequestHeader.StatelessRequest.Hosted.User;


/**
 * This class should be used to fetch web service client instances
 * 
 * @author alakhani
 *
 */
public class WebServiceClientFactory 
{
  
  private static final Logger logger = new Logger(WebServiceClientFactory.class.getName());
  
  protected GiftCardOnline iGiftCardOnline;
  protected CommonUtil util;
  
  protected UnitedServiceInterceptor unitedInterceptor;
  
  public WebServiceClientFactory() {
	  unitedInterceptor = new UnitedServiceInterceptor();
  }
  

  public GiftCardOnline getGiftCardOnline() throws CacheException, Exception 
  {
    if (util == null) {
    	util = new CommonUtil();
    }
    if (iGiftCardOnline == null) 
    {
      try 
      {
        String svsWsdlName = util.getFrpGlobalParm(PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_SERVICE_WSDL);
        String svsUrlString = util.getFrpGlobalParm(PS_CONTEXT, PaymentServiceConstants.PS_NAME_SVS_SERVICE_URL);

        Class class_v = this.getClass();
        ClassLoader classLoader = class_v.getClassLoader();
        URL svsWsdlLocation = classLoader.getResource(svsWsdlName);
       
        GiftCardOnlineService gcs = new GiftCardOnlineService(svsWsdlLocation); 
        
        Iterator iter = gcs.getPorts();
        while(iter.hasNext())
        {
          QName testQ = (QName)iter.next();
          gcs.addPort(testQ, "http://schemas.xmlsoap.org/soap/", svsUrlString);
        }
        
        this.iGiftCardOnline = gcs.getGiftCardOnline();

      } 
      catch (MalformedURLException e) 
      {
        logger.error("Error when getting svs web service: " + e.getMessage());
        e.printStackTrace();
      }
    }
    
    return iGiftCardOnline;
  }
  
  
	//private static final String XMLNS_WSU = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	private static final String XSD_WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String XSD_WSSE_PASSWORD = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";

	
	//private static String unitedPartnerRetrieveProfileUrlStr = "https://209.87.125.174/customerProfile/PartnerRetrvProfile/1.0.0 ";
	protected static URL partnerRetreiveProfileURL = null;
	protected static GetPartnerProfilePortType partnerProfileSvc;
	
	//private static String unitedPartnerUpdateMilesUrlStr = "https://209.87.125.174/redemption/PartnerUpdateMiles/1.0.0";
	protected static URL partnerUpdateMilesURL = null;
	protected static PartnerUpdateMilesPort partnerUpdateMilesSvc;
	
	/**
	 * Get a United Partner Profile cxf web service instance for the specified URL.
	 * The instance is a singleton, but you can change the url on the fly if needs be
	 * 
	 * @param endpointUrl
	 * @return GetPartnerProfilePortType
	 * @throws Exception 
	 */
	public GetPartnerProfilePortType getUnitedRetreiveProfileService() throws Exception {
		
	    if (util == null) {
	    	util = new CommonUtil();
	    }
		String endpointUrl = util.getFrpGlobalParm(CONFIG_CONTEXT, MilesPointsConstants.UA_PROFILE_SERVICE_ENDPOINT_URL);//"https://partnerservices-stage.united.com/customerProfile/PartnerRetrvProfile/1.0.0";
		
		if (partnerProfileSvc == null 
				|| (partnerRetreiveProfileURL != null && !partnerRetreiveProfileURL.toString().equalsIgnoreCase(endpointUrl))) {
			
			partnerRetreiveProfileURL = new URL(endpointUrl);
			
			URL wsdlFile = ResourceUtil.getInstance().getResource(PartnerRetrvProfile_Service.WSDL_LOCATION.getPath());

			PartnerRetrvProfile_Service ss = new PartnerRetrvProfile_Service(wsdlFile);

	        Iterator iter = ss.getPorts();
	        while(iter.hasNext())
	        {
	          QName testQ = (QName)iter.next();
	          ss.addPort(testQ, "http://schemas.xmlsoap.org/soap/", partnerRetreiveProfileURL.toString());
	        }

			partnerProfileSvc = ss.getDevGetPartnerProfileBindingName();

			//add the request header with no transaction id
			addRequestHeaderToUARequest(partnerProfileSvc, null);
			Client cxfClient = ClientProxy.getClient(partnerProfileSvc);
            cxfClient.getOutInterceptors().add(unitedInterceptor);
            cxfClient.getInInterceptors().add(new FTDUnitedLoggingInInterceptor());
            cxfClient.getOutInterceptors().add(new FTDUnitedLoggingOutInterceptor());

            setTimeoutOnCxfClient(cxfClient);
            
		}
		
		return partnerProfileSvc;
	}
	
	
	


	public PartnerUpdateMilesPort getUnitedUpdateMilesService() throws Exception {
		
	    if (util == null) {
	    	util = new CommonUtil();
	    }
		String endpointUrl = util.getFrpGlobalParm(CONFIG_CONTEXT, UA_UPDATE_MILES_SERVICE_ENDPOINT_URL);
		
		if (partnerUpdateMilesSvc == null ||
				partnerUpdateMilesURL != null && !partnerUpdateMilesURL.equals(endpointUrl)) {
			
			partnerUpdateMilesURL = new URL(endpointUrl);
		
			//get the wsdl that we bundled with our app
			URL wsdlFile = ResourceUtil.getInstance().getResource(PartnerUpdateMilesService.WSDL_LOCATION.getPath());
	        PartnerUpdateMilesService ss = new PartnerUpdateMilesService(wsdlFile);
	        Iterator iter = ss.getPorts();
	        while(iter.hasNext())
	        {
	          QName testQ = (QName)iter.next();
	          ss.addPort(testQ, "http://schemas.xmlsoap.org/soap/", partnerUpdateMilesURL.toString());
	        }
	        
	        partnerUpdateMilesSvc = ss.getDEVDevSandboxDataPowerPartnerUpdateMilesBinding();  
	        
			Client cxfClient = ClientProxy.getClient(partnerUpdateMilesSvc);
            cxfClient.getOutInterceptors().add(unitedInterceptor);
            cxfClient.getInInterceptors().add(new FTDUnitedLoggingInInterceptor());
            cxfClient.getOutInterceptors().add(new FTDUnitedLoggingOutInterceptor());
	        
		}
		
        return partnerUpdateMilesSvc;
		
	}
	
	
	/**
	 * This puts the <RequestHeader> element in the soap header that we send to United
	 * 
	 * @param uaClientProxy - the client proxy object
	 * @param transactionId - optional.  set a transaction ID if supplied
	 * @throws JAXBException
	 */
	public void addRequestHeaderToUARequest(Object uaClientProxy, String transactionId) throws JAXBException {
		final List<Header> headers = new ArrayList<Header>();			
		
		RequestHeader rh = new RequestHeader();
		rh.setApplicationID("PROFILE");
		rh.setServiceVersion("3.0.0");
		rh.setRequestID("R00Q01");
		rh.setReturnRuntimeInfo(true);
		rh.setTransactionID(transactionId);
		StatelessRequest sr = new StatelessRequest();
		Hosted hosted = new Hosted();
		User u = new User();
		u.setUserID("R00901");
		u.setOrganization("WB");
		u.setSourceOffice("CHI");
		Role role = new Role();
		role.setReferenceId("");
		role.setReferenceQualifier("");
		role.setAccessLevel("");
		
		hosted.setRole(role);
		hosted.setUser(u);
		sr.setHosted(hosted);
		
		rh.setStatelessRequest(sr);
      
	    QName authQName = new QName("http://www.ual.com/hcp/has", "RequestHeader", "has");
	    JAXBElement<RequestHeader> reqHeaderEl = new JAXBElement<RequestHeader>(authQName, RequestHeader.class, rh);
	
	    Header requestHeader = new Header(reqHeaderEl.getName(), reqHeaderEl, new JAXBDataBinding(RequestHeader.class));
	    headers.add(requestHeader);
	    
	    //here's where we actually attach the headers to the cxf client proxy instance
		((BindingProvider)uaClientProxy).getRequestContext().put(Header.HEADER_LIST, headers);
		
	}


	private void setTimeoutOnCxfClient(Client cxfClient) throws Exception {
        final HTTPConduit http = (HTTPConduit) cxfClient.getConduit();
        final HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        
        //set the timeout
        int timeout = 60000; //default to 60s
        String value =  util.getFrpGlobalParm(MilesPointsConstants.CONFIG_CONTEXT, "UA" + MilesPointsConstants._API_CONNECTION_TIMEOUT);
        if(value != null && !value.equals("")) {
            timeout = Integer.parseInt(value);
        }
        httpClientPolicy.setReceiveTimeout(timeout);
        httpClientPolicy.setAllowChunking(false);
        httpClientPolicy.setConnectionTimeout(timeout);

        http.setClient(httpClientPolicy);
		
	}

}
