package com.ftd.ps.common.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.ps.common.constant.PaymentServiceConstants;

public class CommonUtil
{
  protected static Logger logger = new Logger("com.ftd.ps.common.util.CommonUtil");

  public void sendPageSystemMessage(String logMessage)
  {
    Connection con = null;
    try
    {
      con = this.getNewConnection();
      String appSource = PaymentServiceConstants.SM_PAGE_SOURCE;
      String errorType = PaymentServiceConstants.SM_TYPE;
      String subject = PaymentServiceConstants.SM_PAGE_SUBJECT;
      int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

      SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
      systemMessengerVO.setLevel(pageLevel);
      systemMessengerVO.setSource(appSource);
      systemMessengerVO.setType(errorType);
      systemMessengerVO.setSubject(subject);
      systemMessengerVO.setMessage(logMessage);
      String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);

    }
    catch (Exception ex)
    {
      // Do not attempt to send system message it requires obtaining a
      // connection
      // and may end up in an infinite loop.
      logger.error(ex);
    }
    finally
    {
      if (con != null)
      {
        try
        {
          con.close();
        }
        catch (SQLException sx)
        {
          logger.error(sx);
        }
      }
    }

  }

  public void sendNoPageSystemMessage(String logMessage)
  {
    Connection con = null;
    try
    {
      con = this.getNewConnection();
      String appSource = PaymentServiceConstants.SM_PAGE_SOURCE;
      String errorType = PaymentServiceConstants.SM_TYPE;
      String subject = PaymentServiceConstants.SM_NOPAGE_SUBJECT;
      int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

      SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
      systemMessengerVO.setLevel(pageLevel);
      systemMessengerVO.setSource(appSource);
      systemMessengerVO.setType(errorType);
      systemMessengerVO.setSubject(subject);
      systemMessengerVO.setMessage(logMessage);
      String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);

    }
    catch (Exception ex)
    {
      // Do not attempt to send system message it requires obtaining a
      // connection
      // and may end up in an infinite loop.
      logger.error(ex);
    }
    finally
    {
      if (con != null)
      {
        try
        {
          con.close();
        }
        catch (SQLException sx)
        {
          logger.error(sx);
        }
      }
    }

  }

  /**
   * Get a new database connection.
   * 
   * @return
   * @throws Exception
   */
  public Connection getNewConnection() throws Exception
  {
    // get database connection
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    Connection conn = null;

    String datasource = configUtil.getPropertyNew(PaymentServiceConstants.PROPERTY_FILE, PaymentServiceConstants.DATASOURCE_NAME);
    conn = DataSourceUtil.getInstance().getConnection(datasource);

    return conn;
  }

  public String getFrpGlobalParm(String context, String name) throws CacheException, Exception
  {
    CacheUtil cacheUtil = CacheUtil.getInstance();
    return cacheUtil.getGlobalParm(context, name);
  }

  public String getSecureGlobalParm(String context, String name) throws CacheException, Exception
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    return configUtil.getSecureProperty(context, name);
  }

  /**
   * Get today's date formated
   * 
   * @return
   */
  public String now() throws Exception
  {
    return getDateFormated(PaymentServiceConstants.DATE_FORMAT_NOW, 0);
  }

  /**
   * Get a formated date relative to today.
   * 
   * @return
   */
  public String getDateFormated(String format, int offset) throws Exception
  {
    Date dt = getDate(offset);
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    return sdf.format(dt);
  }

  /**
   * Returns a date relative to today.
   * 
   * @param offset
   * @return
   */
  public Date getDate(int offset)
  {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, offset);
    return cal.getTime();
  }

  /**
   * Returns a date relative to today.
   * 
   * @param offset
   * @return
   */
  public Date getDateTruncated(int offset) throws Exception
  {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, offset);

    SimpleDateFormat sdf = new SimpleDateFormat(PaymentServiceConstants.DATE_FORMAT_DD);
    String dateString = sdf.format(cal.getTime());
    Date truncatedDate = sdf.parse(dateString);
    return truncatedDate;
  }

  public String dateToString(Date dt) throws Exception
  {
    SimpleDateFormat sdf = new SimpleDateFormat(PaymentServiceConstants.DATE_FORMAT_DD);
    return sdf.format(dt);
  }

  /**
   * Returns a date relative to today.
   * 
   * @param offset
   * @return
   */
  public Calendar getCalendarTruncated(int offset) throws Exception
  {
    Calendar cal = Calendar.getInstance();
    Date dt = getDateTruncated(offset);
    cal.setTime(dt);
    return cal;
  }

  /**
   * Pads the "value" with the "padChar". "size" refers to the total size of the
   * new string
   * 
   * @return
   */
  public String pad(String value, int padSide, String padChar, int size) throws Exception
  {
    // null check
    if (value == null)
      value = "";

    value = value.trim();
    String padded = value;

    for (int i = 0; i < size; i++)
    {
      if (padSide == PaymentServiceConstants.PAD_LEFT)
        padded = padChar + padded;
      else
        padded = padded + padChar;
    }// end for loop

    return padded;
  }

  /**
   * Pads the "value" with the "maskChar". "maskLength" refers to the characters
   * that need to be masked
   * 
   * @return
   */
  public String mask(String value, int maskSide, String maskChar, int numOfCharsToDisplay) throws Exception
  {

    // null check
    if (value == null)
      value = "";

    value = value.trim();
    String masked = value;

    if (value.length() > numOfCharsToDisplay)
    {
      if (maskSide == PaymentServiceConstants.MASK_RIGHT)
      {
        masked = value.substring(0, numOfCharsToDisplay);
        masked = pad(masked, PaymentServiceConstants.PAD_RIGHT, maskChar, value.length() - numOfCharsToDisplay);
      }
      else
      {
        masked = value.substring(masked.length() - numOfCharsToDisplay);
        masked = pad(masked, PaymentServiceConstants.PAD_LEFT, maskChar, value.length() - numOfCharsToDisplay);
      }
    }
    return masked;
  }

  public String getImageFormat(String url) throws Exception
  {
    if (url != null)
    {
      return "image/" + url.substring(url.lastIndexOf(".") + 1);
    }
    return null;
  }

}