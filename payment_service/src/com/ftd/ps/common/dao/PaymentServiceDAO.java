package com.ftd.ps.common.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * 
 * @author Ali Lakhani
 * 
 */
public class PaymentServiceDAO
{

  protected static Logger logger =
        new Logger(PaymentServiceDAO.class.getName());

  Connection conn;

  public PaymentServiceDAO(Connection conn)
  {
    this.conn = conn;
  }

  /**
   * 
   * @param
   * @return
   */
  public String insertPaymentTracking(String clientName, String errorCode, String errorMessage, String paymentType, String request, String response, String responseCode,
      String responseMessage, String sourceCode, String transactionId, String transactionType) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap<String, Object> inputParams = new HashMap<String, Object>();
    inputParams.put("IN_CLIENT_NAME", clientName);
    inputParams.put("IN_CREATED_BY", "insertPaymentTracking");
    inputParams.put("IN_ERROR_CODE", errorCode);
    inputParams.put("IN_ERROR_MESSAGE", errorMessage);
    inputParams.put("IN_PAYMENT_TYPE", paymentType);
    inputParams.put("IN_REQUEST", request);
    inputParams.put("IN_RESPONSE", response);
    inputParams.put("IN_RESPONSE_CODE", responseCode);
    inputParams.put("IN_RESPONSE_MESSAGE", responseMessage);
    inputParams.put("IN_SOURCE_CODE", sourceCode);
    inputParams.put("IN_TRANSACTION_ID", transactionId);
    inputParams.put("IN_TRANSACTION_TYPE", transactionType);
    inputParams.put("IN_UPDATED_BY", "insertPaymentTracking");

    dataRequest.setConnection(this.conn);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("INSERT_PAYMENT_TRACKING");
    dataRequest.setInputParams(inputParams);

    /* execute the store procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store procedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    return ((String) outputs.get("OUT_PAYMENT_TRACKING_ID"));
  }

  /**
   * 
   * @param
   * @return
   */
  public void updatePaymentTracking(String paymentTrackingId, String clientName, String errorCode, String errorMessage, String paymentType, String request, String response,
      String responseCode, String responseMessage, String sourceCode, String transactionId, String transactionType) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap<String, Object> inputParams = new HashMap<String, Object>();

    inputParams.put("IN_CLIENT_NAME", clientName);
    inputParams.put("IN_ERROR_CODE", errorCode);
    inputParams.put("IN_ERROR_MESSAGE", errorMessage);
    inputParams.put("IN_PAYMENT_TRACKING_ID", paymentTrackingId);
    inputParams.put("IN_PAYMENT_TYPE", paymentType);
    inputParams.put("IN_REQUEST", request);
    inputParams.put("IN_RESPONSE", response);
    inputParams.put("IN_RESPONSE_CODE", responseCode);
    inputParams.put("IN_RESPONSE_MESSAGE", responseMessage);
    inputParams.put("IN_SOURCE_CODE", sourceCode);
    inputParams.put("IN_TRANSACTION_ID", transactionId);
    inputParams.put("IN_TRANSACTION_TYPE", transactionType);
    inputParams.put("IN_UPDATED_BY", "updatePaymentTracking");

    dataRequest.setConnection(this.conn);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("UPDATE_PAYMENT_TRACKING");
    dataRequest.setInputParams(inputParams);

    /* execute the store procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store procedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }
}
