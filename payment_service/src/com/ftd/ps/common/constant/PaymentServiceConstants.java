package com.ftd.ps.common.constant;


public class PaymentServiceConstants
{
    public final static String PROPERTY_FILE = "ps-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    public final static String LOAD_TEST_CACHE = "DATASOURCE";
    public final static String TEST_MODE = "TEST_MODE";
    public static final String DATE_FORMAT_DD = "MM/dd/yyyy";
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final int    PAD_RIGHT = 0;
    public static final int    PAD_LEFT = 1;
    public static final int    MASK_RIGHT = 0;
    public static final int    MASK_LEFT = 1;
    
    	
    // DB statements
    public static final String DB_INSERT_TRANSACTION = "INSERT_PS_TRANSACTION";

    // Request parameters
    public static final String REQUEST_PARM_ACTION_TYPE = "serviceType";

    // System messaging
    public static final String SM_PAGE_SOURCE = "PAYMENT_SERVICE_PAGE";
    public static final String SM_NOPAGE_SOURCE = "PAYMENT_SERVICE_NOPAGE";
    public static final String SM_PAGE_SUBJECT = "Payment Service System Message";
    public static final String SM_NOPAGE_SUBJECT = "NOPAGE Payment Service System Message";
    public static final String SM_TYPE = "System Exception";
    
    // Global parameters
    public static final String PS_CONTEXT                    = "SERVICE";
    public static final String PS_NAME_SVS_SERVICE_URL       = "SVS_SERVICE_URL";
    public static final String PS_NAME_SVS_SERVICE_WSDL       = "SVS_SERVICE_WSDL";
    public static final String PS_NAME_SVS_MERCHANT_NAME     = "SVS_MERCHANT_NAME";
    public static final String PS_NAME_SVS_MERCHANT_NUMBER   = "SVS_MERCHANT_NUMBER";
    public static final String PS_NAME_SVS_STORE_NUMBER      = "SVS_STORE_NUMBER";
    public static final String PS_NAME_SVS_DIVISION_NUMBER   = "SVS_DIVISION_NUMBER";
    public static final String PS_NAME_SVS_ROUTING_ID        = "SVS_ROUTING_ID";

    // Secure configuration parameters
    public static final String PS_NAME_SVS_CLIENT_USER_NAME  = "SVS_CLIENT";
    public static final String PS_NAME_SVS_CLIENT_PASSWORD   = "SVS_HASHCODE";
    public static final String PS_NAME_SVS_USER_ID = "SVS_USER_ID";
    public static final String PS_NAME_SVS_PASSWORD = "SVS_PASSWORD";
    
    // Error and Return codes
    public static final String PS_ERROR_TRANSACTION_ID_CODE = "901";
    public static final String PS_ERROR_TRANSACTION_ID_MESSAGE = "Transaction Id not found";
    public static final String PS_ERROR_SVS_FAILURE_CODE = "900";
    public static final String PS_ERROR_SVS_FAILURE_MESSAGE = "Transaction denied";
    public static final String PS_ERROR_FAILURE_CODE = "999";
    public static final String PS_ERROR_FAILURE_MESSAGE = "Fatal error occurred. Contact admin/check logs";
    public static final String PS_RC_SUCCESS = "01";
    public static final String PS_ERROR_INSUFFICIENT_FUNDS = "5";

    
}