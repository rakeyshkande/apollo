
package com.ftd.ps.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for paymentRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="paymentRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressVO" type="{http://webservice.ps.ftd.com/}addressVO" minOccurs="0"/>
 *         &lt;element name="amountVO" type="{http://webservice.ps.ftd.com/}amountVO" minOccurs="0"/>
 *         &lt;element name="cardVO" type="{http://webservice.ps.ftd.com/}cardVO" minOccurs="0"/>
 *         &lt;element name="clientPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sourceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paymentRequest", propOrder = {
    "addressVO",
    "amountVO",
    "cardVO",
    "clientPassword",
    "clientUserName",
    "invoiceNumber",
    "paymentType",
    "sourceCode",
    "transactionId"
})
public class PaymentRequest {

    protected AddressVO addressVO;
    protected AmountVO amountVO;
    protected CardVO cardVO;
    protected String clientPassword;
    protected String clientUserName;
    protected String invoiceNumber;
    protected String paymentType;
    protected String sourceCode;
    protected String transactionId;

    /**
     * Gets the value of the addressVO property.
     * 
     * @return
     *     possible object is
     *     {@link AddressVO }
     *     
     */
    public AddressVO getAddressVO() {
        return addressVO;
    }

    /**
     * Sets the value of the addressVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressVO }
     *     
     */
    public void setAddressVO(AddressVO value) {
        this.addressVO = value;
    }

    /**
     * Gets the value of the amountVO property.
     * 
     * @return
     *     possible object is
     *     {@link AmountVO }
     *     
     */
    public AmountVO getAmountVO() {
        return amountVO;
    }

    /**
     * Sets the value of the amountVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountVO }
     *     
     */
    public void setAmountVO(AmountVO value) {
        this.amountVO = value;
    }

    /**
     * Gets the value of the cardVO property.
     * 
     * @return
     *     possible object is
     *     {@link CardVO }
     *     
     */
    public CardVO getCardVO() {
        return cardVO;
    }

    /**
     * Sets the value of the cardVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardVO }
     *     
     */
    public void setCardVO(CardVO value) {
        this.cardVO = value;
    }

    /**
     * Gets the value of the clientPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientPassword() {
        return clientPassword;
    }

    /**
     * Sets the value of the clientPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientPassword(String value) {
        this.clientPassword = value;
    }

    /**
     * Gets the value of the clientUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientUserName() {
        return clientUserName;
    }

    /**
     * Sets the value of the clientUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientUserName(String value) {
        this.clientUserName = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the sourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceCode() {
        return sourceCode;
    }

    /**
     * Sets the value of the sourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceCode(String value) {
        this.sourceCode = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}
