
package com.ftd.ps.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for paymentResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="paymentResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="approvedAmountVO" type="{http://webservice.ps.ftd.com/}amountVO" minOccurs="0"/>
 *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="balanceAmountVO" type="{http://webservice.ps.ftd.com/}amountVO" minOccurs="0"/>
 *         &lt;element name="cardVO" type="{http://webservice.ps.ftd.com/}cardVO" minOccurs="0"/>
 *         &lt;element name="conversionRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isSuccessful" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="responseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responseMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paymentResponse", propOrder = {
    "approvedAmountVO",
    "authorizationCode",
    "balanceAmountVO",
    "cardVO",
    "conversionRate",
    "errorCode",
    "errorMessage",
    "isSuccessful",
    "responseCode",
    "responseMessage",
    "transactionId"
})
public class PaymentResponse {

    protected AmountVO approvedAmountVO;
    protected String authorizationCode;
    protected AmountVO balanceAmountVO;
    protected CardVO cardVO;
    protected String conversionRate;
    protected String errorCode;
    protected String errorMessage;
    protected Boolean isSuccessful;
    protected String responseCode;
    protected String responseMessage;
    protected String transactionId;

    /**
     * Gets the value of the approvedAmountVO property.
     * 
     * @return
     *     possible object is
     *     {@link AmountVO }
     *     
     */
    public AmountVO getApprovedAmountVO() {
        return approvedAmountVO;
    }

    /**
     * Sets the value of the approvedAmountVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountVO }
     *     
     */
    public void setApprovedAmountVO(AmountVO value) {
        this.approvedAmountVO = value;
    }

    /**
     * Gets the value of the authorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Sets the value of the authorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Gets the value of the balanceAmountVO property.
     * 
     * @return
     *     possible object is
     *     {@link AmountVO }
     *     
     */
    public AmountVO getBalanceAmountVO() {
        return balanceAmountVO;
    }

    /**
     * Sets the value of the balanceAmountVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountVO }
     *     
     */
    public void setBalanceAmountVO(AmountVO value) {
        this.balanceAmountVO = value;
    }

    /**
     * Gets the value of the cardVO property.
     * 
     * @return
     *     possible object is
     *     {@link CardVO }
     *     
     */
    public CardVO getCardVO() {
        return cardVO;
    }

    /**
     * Sets the value of the cardVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardVO }
     *     
     */
    public void setCardVO(CardVO value) {
        this.cardVO = value;
    }

    /**
     * Gets the value of the conversionRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConversionRate() {
        return conversionRate;
    }

    /**
     * Sets the value of the conversionRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConversionRate(String value) {
        this.conversionRate = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the isSuccessful property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSuccessful() {
        return isSuccessful;
    }

    /**
     * Sets the value of the isSuccessful property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSuccessful(Boolean value) {
        this.isSuccessful = value;
    }

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * Sets the value of the responseMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseMessage(String value) {
        this.responseMessage = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}
