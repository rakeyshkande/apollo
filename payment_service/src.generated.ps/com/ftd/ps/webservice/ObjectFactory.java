
package com.ftd.ps.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.ps.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Settle_QNAME = new QName("http://webservice.ps.ftd.com/", "settle");
    private final static QName _SettleResponse_QNAME = new QName("http://webservice.ps.ftd.com/", "settleResponse");
    private final static QName _CheckBalanceAndAuthorizeForExactAmountResponse_QNAME = new QName("http://webservice.ps.ftd.com/", "checkBalanceAndAuthorizeForExactAmountResponse");
    private final static QName _GetAuthorization_QNAME = new QName("http://webservice.ps.ftd.com/", "getAuthorization");
    private final static QName _GetAuthorizationResponse_QNAME = new QName("http://webservice.ps.ftd.com/", "getAuthorizationResponse");
    private final static QName _CheckBalanceAndAuthorizeForExactAmount_QNAME = new QName("http://webservice.ps.ftd.com/", "checkBalanceAndAuthorizeForExactAmount");
    private final static QName _Refund_QNAME = new QName("http://webservice.ps.ftd.com/", "refund");
    private final static QName _GetBalance_QNAME = new QName("http://webservice.ps.ftd.com/", "getBalance");
    private final static QName _CancelAuthorizationResponse_QNAME = new QName("http://webservice.ps.ftd.com/", "cancelAuthorizationResponse");
    private final static QName _RefundResponse_QNAME = new QName("http://webservice.ps.ftd.com/", "refundResponse");
    private final static QName _GetBalanceResponse_QNAME = new QName("http://webservice.ps.ftd.com/", "getBalanceResponse");
    private final static QName _Exception_QNAME = new QName("http://webservice.ps.ftd.com/", "Exception");
    private final static QName _CancelAuthorization_QNAME = new QName("http://webservice.ps.ftd.com/", "cancelAuthorization");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.ps.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelAuthorization }
     * 
     */
    public CancelAuthorization createCancelAuthorization() {
        return new CancelAuthorization();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link RefundResponse }
     * 
     */
    public RefundResponse createRefundResponse() {
        return new RefundResponse();
    }

    /**
     * Create an instance of {@link GetBalanceResponse }
     * 
     */
    public GetBalanceResponse createGetBalanceResponse() {
        return new GetBalanceResponse();
    }

    /**
     * Create an instance of {@link CancelAuthorizationResponse }
     * 
     */
    public CancelAuthorizationResponse createCancelAuthorizationResponse() {
        return new CancelAuthorizationResponse();
    }

    /**
     * Create an instance of {@link GetBalance }
     * 
     */
    public GetBalance createGetBalance() {
        return new GetBalance();
    }

    /**
     * Create an instance of {@link GetAuthorization }
     * 
     */
    public GetAuthorization createGetAuthorization() {
        return new GetAuthorization();
    }

    /**
     * Create an instance of {@link GetAuthorizationResponse }
     * 
     */
    public GetAuthorizationResponse createGetAuthorizationResponse() {
        return new GetAuthorizationResponse();
    }

    /**
     * Create an instance of {@link Refund }
     * 
     */
    public Refund createRefund() {
        return new Refund();
    }

    /**
     * Create an instance of {@link CheckBalanceAndAuthorizeForExactAmount }
     * 
     */
    public CheckBalanceAndAuthorizeForExactAmount createCheckBalanceAndAuthorizeForExactAmount() {
        return new CheckBalanceAndAuthorizeForExactAmount();
    }

    /**
     * Create an instance of {@link SettleResponse }
     * 
     */
    public SettleResponse createSettleResponse() {
        return new SettleResponse();
    }

    /**
     * Create an instance of {@link CheckBalanceAndAuthorizeForExactAmountResponse }
     * 
     */
    public CheckBalanceAndAuthorizeForExactAmountResponse createCheckBalanceAndAuthorizeForExactAmountResponse() {
        return new CheckBalanceAndAuthorizeForExactAmountResponse();
    }

    /**
     * Create an instance of {@link Settle }
     * 
     */
    public Settle createSettle() {
        return new Settle();
    }

    /**
     * Create an instance of {@link PaymentRequest }
     * 
     */
    public PaymentRequest createPaymentRequest() {
        return new PaymentRequest();
    }

    /**
     * Create an instance of {@link AmountVO }
     * 
     */
    public AmountVO createAmountVO() {
        return new AmountVO();
    }

    /**
     * Create an instance of {@link PaymentResponse }
     * 
     */
    public PaymentResponse createPaymentResponse() {
        return new PaymentResponse();
    }

    /**
     * Create an instance of {@link AddressVO }
     * 
     */
    public AddressVO createAddressVO() {
        return new AddressVO();
    }

    /**
     * Create an instance of {@link CardVO }
     * 
     */
    public CardVO createCardVO() {
        return new CardVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Settle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "settle")
    public JAXBElement<Settle> createSettle(Settle value) {
        return new JAXBElement<Settle>(_Settle_QNAME, Settle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SettleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "settleResponse")
    public JAXBElement<SettleResponse> createSettleResponse(SettleResponse value) {
        return new JAXBElement<SettleResponse>(_SettleResponse_QNAME, SettleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckBalanceAndAuthorizeForExactAmountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "checkBalanceAndAuthorizeForExactAmountResponse")
    public JAXBElement<CheckBalanceAndAuthorizeForExactAmountResponse> createCheckBalanceAndAuthorizeForExactAmountResponse(CheckBalanceAndAuthorizeForExactAmountResponse value) {
        return new JAXBElement<CheckBalanceAndAuthorizeForExactAmountResponse>(_CheckBalanceAndAuthorizeForExactAmountResponse_QNAME, CheckBalanceAndAuthorizeForExactAmountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthorization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "getAuthorization")
    public JAXBElement<GetAuthorization> createGetAuthorization(GetAuthorization value) {
        return new JAXBElement<GetAuthorization>(_GetAuthorization_QNAME, GetAuthorization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthorizationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "getAuthorizationResponse")
    public JAXBElement<GetAuthorizationResponse> createGetAuthorizationResponse(GetAuthorizationResponse value) {
        return new JAXBElement<GetAuthorizationResponse>(_GetAuthorizationResponse_QNAME, GetAuthorizationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckBalanceAndAuthorizeForExactAmount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "checkBalanceAndAuthorizeForExactAmount")
    public JAXBElement<CheckBalanceAndAuthorizeForExactAmount> createCheckBalanceAndAuthorizeForExactAmount(CheckBalanceAndAuthorizeForExactAmount value) {
        return new JAXBElement<CheckBalanceAndAuthorizeForExactAmount>(_CheckBalanceAndAuthorizeForExactAmount_QNAME, CheckBalanceAndAuthorizeForExactAmount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Refund }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "refund")
    public JAXBElement<Refund> createRefund(Refund value) {
        return new JAXBElement<Refund>(_Refund_QNAME, Refund.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "getBalance")
    public JAXBElement<GetBalance> createGetBalance(GetBalance value) {
        return new JAXBElement<GetBalance>(_GetBalance_QNAME, GetBalance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAuthorizationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "cancelAuthorizationResponse")
    public JAXBElement<CancelAuthorizationResponse> createCancelAuthorizationResponse(CancelAuthorizationResponse value) {
        return new JAXBElement<CancelAuthorizationResponse>(_CancelAuthorizationResponse_QNAME, CancelAuthorizationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefundResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "refundResponse")
    public JAXBElement<RefundResponse> createRefundResponse(RefundResponse value) {
        return new JAXBElement<RefundResponse>(_RefundResponse_QNAME, RefundResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "getBalanceResponse")
    public JAXBElement<GetBalanceResponse> createGetBalanceResponse(GetBalanceResponse value) {
        return new JAXBElement<GetBalanceResponse>(_GetBalanceResponse_QNAME, GetBalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAuthorization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ps.ftd.com/", name = "cancelAuthorization")
    public JAXBElement<CancelAuthorization> createCancelAuthorization(CancelAuthorization value) {
        return new JAXBElement<CancelAuthorization>(_CancelAuthorization_QNAME, CancelAuthorization.class, null, value);
    }

}
