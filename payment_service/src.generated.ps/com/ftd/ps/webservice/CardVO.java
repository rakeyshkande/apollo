
package com.ftd.ps.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AVSIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acquirerReferenceData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="approvalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardExpiration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardTrackOne" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardTrackTwo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cscResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cscValidatedFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cscValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="underMinAuthAmt" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="validationStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="verbiage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardVO", propOrder = {
    "avsIndicator",
    "acquirerReferenceData",
    "actionCode",
    "approvalCode",
    "cardCurrency",
    "cardExpiration",
    "cardNumber",
    "cardTrackOne",
    "cardTrackTwo",
    "cscResponseCode",
    "cscValidatedFlag",
    "cscValue",
    "pinNumber",
    "status",
    "underMinAuthAmt",
    "validationStatus",
    "verbiage"
})
public class CardVO {

    @XmlElement(name = "AVSIndicator")
    protected String avsIndicator;
    protected String acquirerReferenceData;
    protected String actionCode;
    protected String approvalCode;
    protected String cardCurrency;
    protected String cardExpiration;
    protected String cardNumber;
    protected String cardTrackOne;
    protected String cardTrackTwo;
    protected String cscResponseCode;
    protected String cscValidatedFlag;
    protected String cscValue;
    protected String pinNumber;
    protected String status;
    protected boolean underMinAuthAmt;
    protected String validationStatus;
    protected String verbiage;

    /**
     * Gets the value of the avsIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSIndicator() {
        return avsIndicator;
    }

    /**
     * Sets the value of the avsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSIndicator(String value) {
        this.avsIndicator = value;
    }

    /**
     * Gets the value of the acquirerReferenceData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquirerReferenceData() {
        return acquirerReferenceData;
    }

    /**
     * Sets the value of the acquirerReferenceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquirerReferenceData(String value) {
        this.acquirerReferenceData = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the approvalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * Sets the value of the approvalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalCode(String value) {
        this.approvalCode = value;
    }

    /**
     * Gets the value of the cardCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCurrency() {
        return cardCurrency;
    }

    /**
     * Sets the value of the cardCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCurrency(String value) {
        this.cardCurrency = value;
    }

    /**
     * Gets the value of the cardExpiration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardExpiration() {
        return cardExpiration;
    }

    /**
     * Sets the value of the cardExpiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardExpiration(String value) {
        this.cardExpiration = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardTrackOne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardTrackOne() {
        return cardTrackOne;
    }

    /**
     * Sets the value of the cardTrackOne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardTrackOne(String value) {
        this.cardTrackOne = value;
    }

    /**
     * Gets the value of the cardTrackTwo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardTrackTwo() {
        return cardTrackTwo;
    }

    /**
     * Sets the value of the cardTrackTwo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardTrackTwo(String value) {
        this.cardTrackTwo = value;
    }

    /**
     * Gets the value of the cscResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCscResponseCode() {
        return cscResponseCode;
    }

    /**
     * Sets the value of the cscResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCscResponseCode(String value) {
        this.cscResponseCode = value;
    }

    /**
     * Gets the value of the cscValidatedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCscValidatedFlag() {
        return cscValidatedFlag;
    }

    /**
     * Sets the value of the cscValidatedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCscValidatedFlag(String value) {
        this.cscValidatedFlag = value;
    }

    /**
     * Gets the value of the cscValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCscValue() {
        return cscValue;
    }

    /**
     * Sets the value of the cscValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCscValue(String value) {
        this.cscValue = value;
    }

    /**
     * Gets the value of the pinNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPinNumber() {
        return pinNumber;
    }

    /**
     * Sets the value of the pinNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPinNumber(String value) {
        this.pinNumber = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the underMinAuthAmt property.
     * 
     */
    public boolean isUnderMinAuthAmt() {
        return underMinAuthAmt;
    }

    /**
     * Sets the value of the underMinAuthAmt property.
     * 
     */
    public void setUnderMinAuthAmt(boolean value) {
        this.underMinAuthAmt = value;
    }

    /**
     * Gets the value of the validationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationStatus() {
        return validationStatus;
    }

    /**
     * Sets the value of the validationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationStatus(String value) {
        this.validationStatus = value;
    }

    /**
     * Gets the value of the verbiage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerbiage() {
        return verbiage;
    }

    /**
     * Sets the value of the verbiage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerbiage(String value) {
        this.verbiage = value;
    }

}
