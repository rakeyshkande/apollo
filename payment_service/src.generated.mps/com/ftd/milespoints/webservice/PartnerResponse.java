
package com.ftd.milespoints.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for partnerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="partnerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authErrorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authSuccess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conSuccess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="externalConfirmationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="generalMember" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="membershipNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="membershipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="milesVerify" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "partnerResponse", propOrder = {
    "authErrorCode",
    "authSuccess",
    "conSuccess",
    "errorMessage",
    "externalConfirmationNumber",
    "generalMember",
    "membershipNumber",
    "membershipType",
    "milesVerify",
    "transactionId"
})
public class PartnerResponse {

    protected String authErrorCode;
    protected String authSuccess;
    protected String conSuccess;
    protected String errorMessage;
    protected String externalConfirmationNumber;
    protected String generalMember;
    protected String membershipNumber;
    protected String membershipType;
    protected String milesVerify;
    protected String transactionId;

    /**
     * Gets the value of the authErrorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthErrorCode() {
        return authErrorCode;
    }

    /**
     * Sets the value of the authErrorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthErrorCode(String value) {
        this.authErrorCode = value;
    }

    /**
     * Gets the value of the authSuccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthSuccess() {
        return authSuccess;
    }

    /**
     * Sets the value of the authSuccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthSuccess(String value) {
        this.authSuccess = value;
    }

    /**
     * Gets the value of the conSuccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConSuccess() {
        return conSuccess;
    }

    /**
     * Sets the value of the conSuccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConSuccess(String value) {
        this.conSuccess = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the externalConfirmationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalConfirmationNumber() {
        return externalConfirmationNumber;
    }

    /**
     * Sets the value of the externalConfirmationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalConfirmationNumber(String value) {
        this.externalConfirmationNumber = value;
    }

    /**
     * Gets the value of the generalMember property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralMember() {
        return generalMember;
    }

    /**
     * Sets the value of the generalMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralMember(String value) {
        this.generalMember = value;
    }

    /**
     * Gets the value of the membershipNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipNumber() {
        return membershipNumber;
    }

    /**
     * Sets the value of the membershipNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipNumber(String value) {
        this.membershipNumber = value;
    }

    /**
     * Gets the value of the membershipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipType() {
        return membershipType;
    }

    /**
     * Sets the value of the membershipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipType(String value) {
        this.membershipType = value;
    }

    /**
     * Gets the value of the milesVerify property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMilesVerify() {
        return milesVerify;
    }

    /**
     * Sets the value of the milesVerify property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMilesVerify(String value) {
        this.milesVerify = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}
