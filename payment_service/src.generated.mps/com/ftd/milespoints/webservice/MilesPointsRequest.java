
package com.ftd.milespoints.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for milesPointsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milesPointsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="membershipNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="membershipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="milesPointsRequested" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="returnBooleanResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="returnErrorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="returnErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="returnStringResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalMilesPoints" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="updateMileIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milesPointsRequest", propOrder = {
    "clientPassword",
    "clientUserName",
    "membershipNumber",
    "membershipType",
    "milesPointsRequested",
    "password",
    "paymentId",
    "returnBooleanResult",
    "returnErrorCode",
    "returnErrorMessage",
    "returnStringResult",
    "totalMilesPoints",
    "updateMileIndicator"
})
public class MilesPointsRequest {

    protected String clientPassword;
    protected String clientUserName;
    protected String membershipNumber;
    protected String membershipType;
    protected int milesPointsRequested;
    protected String password;
    protected String paymentId;
    protected boolean returnBooleanResult;
    protected String returnErrorCode;
    protected String returnErrorMessage;
    protected String returnStringResult;
    protected int totalMilesPoints;
    protected String updateMileIndicator;

    /**
     * Gets the value of the clientPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientPassword() {
        return clientPassword;
    }

    /**
     * Sets the value of the clientPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientPassword(String value) {
        this.clientPassword = value;
    }

    /**
     * Gets the value of the clientUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientUserName() {
        return clientUserName;
    }

    /**
     * Sets the value of the clientUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientUserName(String value) {
        this.clientUserName = value;
    }

    /**
     * Gets the value of the membershipNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipNumber() {
        return membershipNumber;
    }

    /**
     * Sets the value of the membershipNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipNumber(String value) {
        this.membershipNumber = value;
    }

    /**
     * Gets the value of the membershipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipType() {
        return membershipType;
    }

    /**
     * Sets the value of the membershipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipType(String value) {
        this.membershipType = value;
    }

    /**
     * Gets the value of the milesPointsRequested property.
     * 
     */
    public int getMilesPointsRequested() {
        return milesPointsRequested;
    }

    /**
     * Sets the value of the milesPointsRequested property.
     * 
     */
    public void setMilesPointsRequested(int value) {
        this.milesPointsRequested = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the paymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Sets the value of the paymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentId(String value) {
        this.paymentId = value;
    }

    /**
     * Gets the value of the returnBooleanResult property.
     * 
     */
    public boolean isReturnBooleanResult() {
        return returnBooleanResult;
    }

    /**
     * Sets the value of the returnBooleanResult property.
     * 
     */
    public void setReturnBooleanResult(boolean value) {
        this.returnBooleanResult = value;
    }

    /**
     * Gets the value of the returnErrorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnErrorCode() {
        return returnErrorCode;
    }

    /**
     * Sets the value of the returnErrorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnErrorCode(String value) {
        this.returnErrorCode = value;
    }

    /**
     * Gets the value of the returnErrorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnErrorMessage() {
        return returnErrorMessage;
    }

    /**
     * Sets the value of the returnErrorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnErrorMessage(String value) {
        this.returnErrorMessage = value;
    }

    /**
     * Gets the value of the returnStringResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnStringResult() {
        return returnStringResult;
    }

    /**
     * Sets the value of the returnStringResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnStringResult(String value) {
        this.returnStringResult = value;
    }

    /**
     * Gets the value of the totalMilesPoints property.
     * 
     */
    public int getTotalMilesPoints() {
        return totalMilesPoints;
    }

    /**
     * Sets the value of the totalMilesPoints property.
     * 
     */
    public void setTotalMilesPoints(int value) {
        this.totalMilesPoints = value;
    }

    /**
     * Gets the value of the updateMileIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateMileIndicator() {
        return updateMileIndicator;
    }

    /**
     * Sets the value of the updateMileIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateMileIndicator(String value) {
        this.updateMileIndicator = value;
    }

}
