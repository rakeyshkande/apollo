
package com.ftd.milespoints.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.milespoints.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckMiles_QNAME = new QName("http://webservice.milespoints.ftd.com/", "checkMiles");
    private final static QName _CheckMilesResponse_QNAME = new QName("http://webservice.milespoints.ftd.com/", "checkMilesResponse");
    private final static QName _AuthenticateMemberCheckMilesResponse_QNAME = new QName("http://webservice.milespoints.ftd.com/", "authenticateMemberCheckMilesResponse");
    private final static QName _UpdateMiles_QNAME = new QName("http://webservice.milespoints.ftd.com/", "updateMiles");
    private final static QName _AuthenticateMemberCheckMiles_QNAME = new QName("http://webservice.milespoints.ftd.com/", "authenticateMemberCheckMiles");
    private final static QName _MilesPointsException_QNAME = new QName("http://webservice.milespoints.ftd.com/", "MilesPointsException");
    private final static QName _UpdateMilesResponse_QNAME = new QName("http://webservice.milespoints.ftd.com/", "updateMilesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.milespoints.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateMiles }
     * 
     */
    public UpdateMiles createUpdateMiles() {
        return new UpdateMiles();
    }

    /**
     * Create an instance of {@link AuthenticateMemberCheckMilesResponse }
     * 
     */
    public AuthenticateMemberCheckMilesResponse createAuthenticateMemberCheckMilesResponse() {
        return new AuthenticateMemberCheckMilesResponse();
    }

    /**
     * Create an instance of {@link CheckMilesResponse }
     * 
     */
    public CheckMilesResponse createCheckMilesResponse() {
        return new CheckMilesResponse();
    }

    /**
     * Create an instance of {@link CheckMiles }
     * 
     */
    public CheckMiles createCheckMiles() {
        return new CheckMiles();
    }

    /**
     * Create an instance of {@link UpdateMilesResponse }
     * 
     */
    public UpdateMilesResponse createUpdateMilesResponse() {
        return new UpdateMilesResponse();
    }

    /**
     * Create an instance of {@link MilesPointsException }
     * 
     */
    public MilesPointsException createMilesPointsException() {
        return new MilesPointsException();
    }

    /**
     * Create an instance of {@link AuthenticateMemberCheckMiles }
     * 
     */
    public AuthenticateMemberCheckMiles createAuthenticateMemberCheckMiles() {
        return new AuthenticateMemberCheckMiles();
    }

    /**
     * Create an instance of {@link MilesPointsRequest }
     * 
     */
    public MilesPointsRequest createMilesPointsRequest() {
        return new MilesPointsRequest();
    }

    /**
     * Create an instance of {@link PartnerResponse }
     * 
     */
    public PartnerResponse createPartnerResponse() {
        return new PartnerResponse();
    }

    /**
     * Create an instance of {@link UpdateMilesResp }
     * 
     */
    public UpdateMilesResp createUpdateMilesResp() {
        return new UpdateMilesResp();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckMiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "checkMiles")
    public JAXBElement<CheckMiles> createCheckMiles(CheckMiles value) {
        return new JAXBElement<CheckMiles>(_CheckMiles_QNAME, CheckMiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckMilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "checkMilesResponse")
    public JAXBElement<CheckMilesResponse> createCheckMilesResponse(CheckMilesResponse value) {
        return new JAXBElement<CheckMilesResponse>(_CheckMilesResponse_QNAME, CheckMilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticateMemberCheckMilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "authenticateMemberCheckMilesResponse")
    public JAXBElement<AuthenticateMemberCheckMilesResponse> createAuthenticateMemberCheckMilesResponse(AuthenticateMemberCheckMilesResponse value) {
        return new JAXBElement<AuthenticateMemberCheckMilesResponse>(_AuthenticateMemberCheckMilesResponse_QNAME, AuthenticateMemberCheckMilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "updateMiles")
    public JAXBElement<UpdateMiles> createUpdateMiles(UpdateMiles value) {
        return new JAXBElement<UpdateMiles>(_UpdateMiles_QNAME, UpdateMiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticateMemberCheckMiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "authenticateMemberCheckMiles")
    public JAXBElement<AuthenticateMemberCheckMiles> createAuthenticateMemberCheckMiles(AuthenticateMemberCheckMiles value) {
        return new JAXBElement<AuthenticateMemberCheckMiles>(_AuthenticateMemberCheckMiles_QNAME, AuthenticateMemberCheckMiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MilesPointsException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "MilesPointsException")
    public JAXBElement<MilesPointsException> createMilesPointsException(MilesPointsException value) {
        return new JAXBElement<MilesPointsException>(_MilesPointsException_QNAME, MilesPointsException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.milespoints.ftd.com/", name = "updateMilesResponse")
    public JAXBElement<UpdateMilesResponse> createUpdateMilesResponse(UpdateMilesResponse value) {
        return new JAXBElement<UpdateMilesResponse>(_UpdateMilesResponse_QNAME, UpdateMilesResponse.class, null, value);
    }

}
