
package com.ftd.milespoints.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMilesResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMilesResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountBalance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bonusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bonusType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="externalConfirmationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loyaltyProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postedAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="returnBooleanResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMilesResp", propOrder = {
    "accountBalance",
    "bonusCode",
    "bonusType",
    "externalConfirmationNumber",
    "loyaltyProgramNumber",
    "postedAccountNumber",
    "returnBooleanResult",
    "transactionId"
})
public class UpdateMilesResp {

    protected String accountBalance;
    protected String bonusCode;
    protected String bonusType;
    protected String externalConfirmationNumber;
    protected String loyaltyProgramNumber;
    protected String postedAccountNumber;
    protected String returnBooleanResult;
    protected String transactionId;

    /**
     * Gets the value of the accountBalance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountBalance() {
        return accountBalance;
    }

    /**
     * Sets the value of the accountBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountBalance(String value) {
        this.accountBalance = value;
    }

    /**
     * Gets the value of the bonusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBonusCode() {
        return bonusCode;
    }

    /**
     * Sets the value of the bonusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBonusCode(String value) {
        this.bonusCode = value;
    }

    /**
     * Gets the value of the bonusType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBonusType() {
        return bonusType;
    }

    /**
     * Sets the value of the bonusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBonusType(String value) {
        this.bonusType = value;
    }

    /**
     * Gets the value of the externalConfirmationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalConfirmationNumber() {
        return externalConfirmationNumber;
    }

    /**
     * Sets the value of the externalConfirmationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalConfirmationNumber(String value) {
        this.externalConfirmationNumber = value;
    }

    /**
     * Gets the value of the loyaltyProgramNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyProgramNumber() {
        return loyaltyProgramNumber;
    }

    /**
     * Sets the value of the loyaltyProgramNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyProgramNumber(String value) {
        this.loyaltyProgramNumber = value;
    }

    /**
     * Gets the value of the postedAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostedAccountNumber() {
        return postedAccountNumber;
    }

    /**
     * Sets the value of the postedAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostedAccountNumber(String value) {
        this.postedAccountNumber = value;
    }

    /**
     * Gets the value of the returnBooleanResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnBooleanResult() {
        return returnBooleanResult;
    }

    /**
     * Sets the value of the returnBooleanResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnBooleanResult(String value) {
        this.returnBooleanResult = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

}
