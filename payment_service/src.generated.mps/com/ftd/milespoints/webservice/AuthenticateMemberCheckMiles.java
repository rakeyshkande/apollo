
package com.ftd.milespoints.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for authenticateMemberCheckMiles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="authenticateMemberCheckMiles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="milesPointsRequest" type="{http://webservice.milespoints.ftd.com/}milesPointsRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "authenticateMemberCheckMiles", propOrder = {
    "milesPointsRequest"
})
public class AuthenticateMemberCheckMiles {

    protected MilesPointsRequest milesPointsRequest;

    /**
     * Gets the value of the milesPointsRequest property.
     * 
     * @return
     *     possible object is
     *     {@link MilesPointsRequest }
     *     
     */
    public MilesPointsRequest getMilesPointsRequest() {
        return milesPointsRequest;
    }

    /**
     * Sets the value of the milesPointsRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MilesPointsRequest }
     *     
     */
    public void setMilesPointsRequest(MilesPointsRequest value) {
        this.milesPointsRequest = value;
    }

}
