
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="trnsCDindicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="trnsCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="trnsDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionType", propOrder = {
    "trnsCDindicator",
    "trnsCode",
    "trnsDescription"
})
public class TransactionType {

    @XmlElement(required = true, nillable = true)
    protected String trnsCDindicator;
    @XmlElement(required = true, nillable = true)
    protected String trnsCode;
    @XmlElement(required = true, nillable = true)
    protected String trnsDescription;

    /**
     * Gets the value of the trnsCDindicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrnsCDindicator() {
        return trnsCDindicator;
    }

    /**
     * Sets the value of the trnsCDindicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrnsCDindicator(String value) {
        this.trnsCDindicator = value;
    }

    /**
     * Gets the value of the trnsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrnsCode() {
        return trnsCode;
    }

    /**
     * Sets the value of the trnsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrnsCode(String value) {
        this.trnsCode = value;
    }

    /**
     * Gets the value of the trnsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrnsDescription() {
        return trnsDescription;
    }

    /**
     * Sets the value of the trnsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrnsDescription(String value) {
        this.trnsDescription = value;
    }

}
