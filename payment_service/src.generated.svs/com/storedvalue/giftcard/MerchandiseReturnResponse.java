
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="merchandiseReturnReturn" type="{http://giftcard.storedvalue.com}MerchandiseReturnResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "merchandiseReturnReturn"
})
@XmlRootElement(name = "merchandiseReturnResponse")
public class MerchandiseReturnResponse {

    @XmlElement(required = true, nillable = true)
    protected MerchandiseReturnResponse2 merchandiseReturnReturn;

    /**
     * Gets the value of the merchandiseReturnReturn property.
     * 
     * @return
     *     possible object is
     *     {@link MerchandiseReturnResponse2 }
     *     
     */
    public MerchandiseReturnResponse2 getMerchandiseReturnReturn() {
        return merchandiseReturnReturn;
    }

    /**
     * Sets the value of the merchandiseReturnReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchandiseReturnResponse2 }
     *     
     */
    public void setMerchandiseReturnReturn(MerchandiseReturnResponse2 value) {
        this.merchandiseReturnReturn = value;
    }

}
