
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnhancedBalanceInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnhancedBalanceInquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="balanceAmount" type="{http://giftcard.storedvalue.com}Amount"/>
 *         &lt;element name="card" type="{http://giftcard.storedvalue.com}Card"/>
 *         &lt;element name="conversionRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionHistList" type="{http://giftcard.storedvalue.com}ArrayOfTransactionElement"/>
 *         &lt;element name="returnCode" type="{http://giftcard.storedvalue.com}ReturnCode"/>
 *         &lt;element name="preAuthAmount" type="{http://giftcard.storedvalue.com}Amount"/>
 *         &lt;element name="invoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnhancedBalanceInquiryResponse", propOrder = {
    "authorizationCode",
    "balanceAmount",
    "card",
    "conversionRate",
    "stan",
    "transactionID",
    "transactionHistList",
    "returnCode",
    "preAuthAmount",
    "invoiceNumber"
})
public class EnhancedBalanceInquiryResponse2 {

    @XmlElement(required = true, nillable = true)
    protected String authorizationCode;
    @XmlElement(required = true, nillable = true)
    protected Amount balanceAmount;
    @XmlElement(required = true, nillable = true)
    protected Card card;
    @XmlElement(required = true, nillable = true)
    protected String conversionRate;
    @XmlElement(required = true, nillable = true)
    protected String stan;
    @XmlElement(required = true, nillable = true)
    protected String transactionID;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfTransactionElement transactionHistList;
    @XmlElement(required = true, nillable = true)
    protected ReturnCode returnCode;
    @XmlElement(required = true, nillable = true)
    protected Amount preAuthAmount;
    @XmlElement(required = true, nillable = true)
    protected String invoiceNumber;

    /**
     * Gets the value of the authorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Sets the value of the authorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Gets the value of the balanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Sets the value of the balanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBalanceAmount(Amount value) {
        this.balanceAmount = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link Card }
     *     
     */
    public Card getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link Card }
     *     
     */
    public void setCard(Card value) {
        this.card = value;
    }

    /**
     * Gets the value of the conversionRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConversionRate() {
        return conversionRate;
    }

    /**
     * Sets the value of the conversionRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConversionRate(String value) {
        this.conversionRate = value;
    }

    /**
     * Gets the value of the stan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStan() {
        return stan;
    }

    /**
     * Sets the value of the stan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStan(String value) {
        this.stan = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the transactionHistList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTransactionElement }
     *     
     */
    public ArrayOfTransactionElement getTransactionHistList() {
        return transactionHistList;
    }

    /**
     * Sets the value of the transactionHistList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTransactionElement }
     *     
     */
    public void setTransactionHistList(ArrayOfTransactionElement value) {
        this.transactionHistList = value;
    }

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnCode }
     *     
     */
    public ReturnCode getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnCode }
     *     
     */
    public void setReturnCode(ReturnCode value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the preAuthAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getPreAuthAmount() {
        return preAuthAmount;
    }

    /**
     * Sets the value of the preAuthAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setPreAuthAmount(Amount value) {
        this.preAuthAmount = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

}
