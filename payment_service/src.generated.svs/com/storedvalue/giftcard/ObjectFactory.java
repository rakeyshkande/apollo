
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.storedvalue.giftcard package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.storedvalue.giftcard
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IssueGiftCard }
     * 
     */
    public IssueGiftCard createIssueGiftCard() {
        return new IssueGiftCard();
    }

    /**
     * Create an instance of {@link IssueGiftCardRequest }
     * 
     */
    public IssueGiftCardRequest createIssueGiftCardRequest() {
        return new IssueGiftCardRequest();
    }

    /**
     * Create an instance of {@link Cancel }
     * 
     */
    public Cancel createCancel() {
        return new Cancel();
    }

    /**
     * Create an instance of {@link CancelRequest }
     * 
     */
    public CancelRequest createCancelRequest() {
        return new CancelRequest();
    }

    /**
     * Create an instance of {@link CardRechargeResponse }
     * 
     */
    public CardRechargeResponse createCardRechargeResponse() {
        return new CardRechargeResponse();
    }

    /**
     * Create an instance of {@link CardRechargeResponse2 }
     * 
     */
    public CardRechargeResponse2 createCardRechargeResponse2() {
        return new CardRechargeResponse2();
    }

    /**
     * Create an instance of {@link NetworkResponse }
     * 
     */
    public NetworkResponse createNetworkResponse() {
        return new NetworkResponse();
    }

    /**
     * Create an instance of {@link NetworkResponse2 }
     * 
     */
    public NetworkResponse2 createNetworkResponse2() {
        return new NetworkResponse2();
    }

    /**
     * Create an instance of {@link Network }
     * 
     */
    public Network createNetwork() {
        return new Network();
    }

    /**
     * Create an instance of {@link NetworkRequest }
     * 
     */
    public NetworkRequest createNetworkRequest() {
        return new NetworkRequest();
    }

    /**
     * Create an instance of {@link Tip }
     * 
     */
    public Tip createTip() {
        return new Tip();
    }

    /**
     * Create an instance of {@link TipRequest }
     * 
     */
    public TipRequest createTipRequest() {
        return new TipRequest();
    }

    /**
     * Create an instance of {@link CardRecharge }
     * 
     */
    public CardRecharge createCardRecharge() {
        return new CardRecharge();
    }

    /**
     * Create an instance of {@link CardRechargeRequest }
     * 
     */
    public CardRechargeRequest createCardRechargeRequest() {
        return new CardRechargeRequest();
    }

    /**
     * Create an instance of {@link CancelResponse }
     * 
     */
    public CancelResponse createCancelResponse() {
        return new CancelResponse();
    }

    /**
     * Create an instance of {@link CancelResponse2 }
     * 
     */
    public CancelResponse2 createCancelResponse2() {
        return new CancelResponse2();
    }

    /**
     * Create an instance of {@link EnhancedBalanceInquiry }
     * 
     */
    public EnhancedBalanceInquiry createEnhancedBalanceInquiry() {
        return new EnhancedBalanceInquiry();
    }

    /**
     * Create an instance of {@link EnhancedBalanceInquiryRequest }
     * 
     */
    public EnhancedBalanceInquiryRequest createEnhancedBalanceInquiryRequest() {
        return new EnhancedBalanceInquiryRequest();
    }

    /**
     * Create an instance of {@link PreAuthCompleteResponse }
     * 
     */
    public PreAuthCompleteResponse createPreAuthCompleteResponse() {
        return new PreAuthCompleteResponse();
    }

    /**
     * Create an instance of {@link PreAuthCompleteResponse2 }
     * 
     */
    public PreAuthCompleteResponse2 createPreAuthCompleteResponse2() {
        return new PreAuthCompleteResponse2();
    }

    /**
     * Create an instance of {@link IssueVirtualGiftCard }
     * 
     */
    public IssueVirtualGiftCard createIssueVirtualGiftCard() {
        return new IssueVirtualGiftCard();
    }

    /**
     * Create an instance of {@link IssueVirtualGiftCardRequest }
     * 
     */
    public IssueVirtualGiftCardRequest createIssueVirtualGiftCardRequest() {
        return new IssueVirtualGiftCardRequest();
    }

    /**
     * Create an instance of {@link BalanceInquiryResponse }
     * 
     */
    public BalanceInquiryResponse createBalanceInquiryResponse() {
        return new BalanceInquiryResponse();
    }

    /**
     * Create an instance of {@link BalanceInquiryResponse2 }
     * 
     */
    public BalanceInquiryResponse2 createBalanceInquiryResponse2() {
        return new BalanceInquiryResponse2();
    }

    /**
     * Create an instance of {@link IssueVirtualGiftCardResponse }
     * 
     */
    public IssueVirtualGiftCardResponse createIssueVirtualGiftCardResponse() {
        return new IssueVirtualGiftCardResponse();
    }

    /**
     * Create an instance of {@link IssueVirtualGiftCardResponse2 }
     * 
     */
    public IssueVirtualGiftCardResponse2 createIssueVirtualGiftCardResponse2() {
        return new IssueVirtualGiftCardResponse2();
    }

    /**
     * Create an instance of {@link IssueGiftCardResponse }
     * 
     */
    public IssueGiftCardResponse createIssueGiftCardResponse() {
        return new IssueGiftCardResponse();
    }

    /**
     * Create an instance of {@link IssueGiftCardResponse2 }
     * 
     */
    public IssueGiftCardResponse2 createIssueGiftCardResponse2() {
        return new IssueGiftCardResponse2();
    }

    /**
     * Create an instance of {@link Reversal }
     * 
     */
    public Reversal createReversal() {
        return new Reversal();
    }

    /**
     * Create an instance of {@link ReversalRequest }
     * 
     */
    public ReversalRequest createReversalRequest() {
        return new ReversalRequest();
    }

    /**
     * Create an instance of {@link CashBackResponse }
     * 
     */
    public CashBackResponse createCashBackResponse() {
        return new CashBackResponse();
    }

    /**
     * Create an instance of {@link CashBackResponse2 }
     * 
     */
    public CashBackResponse2 createCashBackResponse2() {
        return new CashBackResponse2();
    }

    /**
     * Create an instance of {@link MerchandiseReturn }
     * 
     */
    public MerchandiseReturn createMerchandiseReturn() {
        return new MerchandiseReturn();
    }

    /**
     * Create an instance of {@link MerchandiseReturnRequest }
     * 
     */
    public MerchandiseReturnRequest createMerchandiseReturnRequest() {
        return new MerchandiseReturnRequest();
    }

    /**
     * Create an instance of {@link CardActivationResponse }
     * 
     */
    public CardActivationResponse createCardActivationResponse() {
        return new CardActivationResponse();
    }

    /**
     * Create an instance of {@link CardActivationResponse2 }
     * 
     */
    public CardActivationResponse2 createCardActivationResponse2() {
        return new CardActivationResponse2();
    }

    /**
     * Create an instance of {@link PreAuthResponse }
     * 
     */
    public PreAuthResponse createPreAuthResponse() {
        return new PreAuthResponse();
    }

    /**
     * Create an instance of {@link PreAuthResponse2 }
     * 
     */
    public PreAuthResponse2 createPreAuthResponse2() {
        return new PreAuthResponse2();
    }

    /**
     * Create an instance of {@link PreAuthComplete }
     * 
     */
    public PreAuthComplete createPreAuthComplete() {
        return new PreAuthComplete();
    }

    /**
     * Create an instance of {@link PreAuthCompleteRequest }
     * 
     */
    public PreAuthCompleteRequest createPreAuthCompleteRequest() {
        return new PreAuthCompleteRequest();
    }

    /**
     * Create an instance of {@link PreAuth }
     * 
     */
    public PreAuth createPreAuth() {
        return new PreAuth();
    }

    /**
     * Create an instance of {@link PreAuthRequest }
     * 
     */
    public PreAuthRequest createPreAuthRequest() {
        return new PreAuthRequest();
    }

    /**
     * Create an instance of {@link CardActivation }
     * 
     */
    public CardActivation createCardActivation() {
        return new CardActivation();
    }

    /**
     * Create an instance of {@link CardActivationRequest }
     * 
     */
    public CardActivationRequest createCardActivationRequest() {
        return new CardActivationRequest();
    }

    /**
     * Create an instance of {@link MerchandiseReturnResponse }
     * 
     */
    public MerchandiseReturnResponse createMerchandiseReturnResponse() {
        return new MerchandiseReturnResponse();
    }

    /**
     * Create an instance of {@link MerchandiseReturnResponse2 }
     * 
     */
    public MerchandiseReturnResponse2 createMerchandiseReturnResponse2() {
        return new MerchandiseReturnResponse2();
    }

    /**
     * Create an instance of {@link EnhancedBalanceInquiryResponse }
     * 
     */
    public EnhancedBalanceInquiryResponse createEnhancedBalanceInquiryResponse() {
        return new EnhancedBalanceInquiryResponse();
    }

    /**
     * Create an instance of {@link EnhancedBalanceInquiryResponse2 }
     * 
     */
    public EnhancedBalanceInquiryResponse2 createEnhancedBalanceInquiryResponse2() {
        return new EnhancedBalanceInquiryResponse2();
    }

    /**
     * Create an instance of {@link RedemptionResponse }
     * 
     */
    public RedemptionResponse createRedemptionResponse() {
        return new RedemptionResponse();
    }

    /**
     * Create an instance of {@link RedemptionResponse2 }
     * 
     */
    public RedemptionResponse2 createRedemptionResponse2() {
        return new RedemptionResponse2();
    }

    /**
     * Create an instance of {@link ReloadResponse }
     * 
     */
    public ReloadResponse createReloadResponse() {
        return new ReloadResponse();
    }

    /**
     * Create an instance of {@link ReloadResponse2 }
     * 
     */
    public ReloadResponse2 createReloadResponse2() {
        return new ReloadResponse2();
    }

    /**
     * Create an instance of {@link ReversalResponse }
     * 
     */
    public ReversalResponse createReversalResponse() {
        return new ReversalResponse();
    }

    /**
     * Create an instance of {@link ReversalResponse2 }
     * 
     */
    public ReversalResponse2 createReversalResponse2() {
        return new ReversalResponse2();
    }

    /**
     * Create an instance of {@link TipResponse }
     * 
     */
    public TipResponse createTipResponse() {
        return new TipResponse();
    }

    /**
     * Create an instance of {@link TipResponse2 }
     * 
     */
    public TipResponse2 createTipResponse2() {
        return new TipResponse2();
    }

    /**
     * Create an instance of {@link CashBack }
     * 
     */
    public CashBack createCashBack() {
        return new CashBack();
    }

    /**
     * Create an instance of {@link CashBackRequest }
     * 
     */
    public CashBackRequest createCashBackRequest() {
        return new CashBackRequest();
    }

    /**
     * Create an instance of {@link BalanceInquiry }
     * 
     */
    public BalanceInquiry createBalanceInquiry() {
        return new BalanceInquiry();
    }

    /**
     * Create an instance of {@link BalanceInquiryRequest }
     * 
     */
    public BalanceInquiryRequest createBalanceInquiryRequest() {
        return new BalanceInquiryRequest();
    }

    /**
     * Create an instance of {@link Reload }
     * 
     */
    public Reload createReload() {
        return new Reload();
    }

    /**
     * Create an instance of {@link ReloadRequest }
     * 
     */
    public ReloadRequest createReloadRequest() {
        return new ReloadRequest();
    }

    /**
     * Create an instance of {@link Redemption }
     * 
     */
    public Redemption createRedemption() {
        return new Redemption();
    }

    /**
     * Create an instance of {@link RedemptionRequest }
     * 
     */
    public RedemptionRequest createRedemptionRequest() {
        return new RedemptionRequest();
    }

    /**
     * Create an instance of {@link ReturnCode }
     * 
     */
    public ReturnCode createReturnCode() {
        return new ReturnCode();
    }

    /**
     * Create an instance of {@link Merchant }
     * 
     */
    public Merchant createMerchant() {
        return new Merchant();
    }

    /**
     * Create an instance of {@link TransactionType }
     * 
     */
    public TransactionType createTransactionType() {
        return new TransactionType();
    }

    /**
     * Create an instance of {@link TransactionElement }
     * 
     */
    public TransactionElement createTransactionElement() {
        return new TransactionElement();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link ArrayOfTransactionElement }
     * 
     */
    public ArrayOfTransactionElement createArrayOfTransactionElement() {
        return new ArrayOfTransactionElement();
    }

    /**
     * Create an instance of {@link Card }
     * 
     */
    public Card createCard() {
        return new Card();
    }

}
