
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="balanceInquiryReturn" type="{http://giftcard.storedvalue.com}BalanceInquiryResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "balanceInquiryReturn"
})
@XmlRootElement(name = "balanceInquiryResponse")
public class BalanceInquiryResponse {

    @XmlElement(required = true, nillable = true)
    protected BalanceInquiryResponse2 balanceInquiryReturn;

    /**
     * Gets the value of the balanceInquiryReturn property.
     * 
     * @return
     *     possible object is
     *     {@link BalanceInquiryResponse2 }
     *     
     */
    public BalanceInquiryResponse2 getBalanceInquiryReturn() {
        return balanceInquiryReturn;
    }

    /**
     * Sets the value of the balanceInquiryReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BalanceInquiryResponse2 }
     *     
     */
    public void setBalanceInquiryReturn(BalanceInquiryResponse2 value) {
        this.balanceInquiryReturn = value;
    }

}
