
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reversalReturn" type="{http://giftcard.storedvalue.com}ReversalResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reversalReturn"
})
@XmlRootElement(name = "reversalResponse")
public class ReversalResponse {

    @XmlElement(required = true, nillable = true)
    protected ReversalResponse2 reversalReturn;

    /**
     * Gets the value of the reversalReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ReversalResponse2 }
     *     
     */
    public ReversalResponse2 getReversalReturn() {
        return reversalReturn;
    }

    /**
     * Sets the value of the reversalReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReversalResponse2 }
     *     
     */
    public void setReversalReturn(ReversalResponse2 value) {
        this.reversalReturn = value;
    }

}
