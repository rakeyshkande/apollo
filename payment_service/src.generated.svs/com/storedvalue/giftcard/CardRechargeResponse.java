
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardRechargeReturn" type="{http://giftcard.storedvalue.com}CardRechargeResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardRechargeReturn"
})
@XmlRootElement(name = "cardRechargeResponse")
public class CardRechargeResponse {

    @XmlElement(required = true, nillable = true)
    protected CardRechargeResponse2 cardRechargeReturn;

    /**
     * Gets the value of the cardRechargeReturn property.
     * 
     * @return
     *     possible object is
     *     {@link CardRechargeResponse2 }
     *     
     */
    public CardRechargeResponse2 getCardRechargeReturn() {
        return cardRechargeReturn;
    }

    /**
     * Sets the value of the cardRechargeReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardRechargeResponse2 }
     *     
     */
    public void setCardRechargeReturn(CardRechargeResponse2 value) {
        this.cardRechargeReturn = value;
    }

}
