
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="issueGiftCardReturn" type="{http://giftcard.storedvalue.com}IssueGiftCardResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issueGiftCardReturn"
})
@XmlRootElement(name = "issueGiftCardResponse")
public class IssueGiftCardResponse {

    @XmlElement(required = true, nillable = true)
    protected IssueGiftCardResponse2 issueGiftCardReturn;

    /**
     * Gets the value of the issueGiftCardReturn property.
     * 
     * @return
     *     possible object is
     *     {@link IssueGiftCardResponse2 }
     *     
     */
    public IssueGiftCardResponse2 getIssueGiftCardReturn() {
        return issueGiftCardReturn;
    }

    /**
     * Sets the value of the issueGiftCardReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssueGiftCardResponse2 }
     *     
     */
    public void setIssueGiftCardReturn(IssueGiftCardResponse2 value) {
        this.issueGiftCardReturn = value;
    }

}
