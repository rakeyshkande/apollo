
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Card complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Card">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardCurrency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pinNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardExpiration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardTrackOne" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardTrackTwo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Card", propOrder = {
    "cardCurrency",
    "cardNumber",
    "pinNumber",
    "cardExpiration",
    "cardTrackOne",
    "cardTrackTwo"
})
public class Card {

    @XmlElement(required = true, nillable = true)
    protected String cardCurrency;
    @XmlElement(required = true, nillable = true)
    protected String cardNumber;
    @XmlElement(required = true, nillable = true)
    protected String pinNumber;
    @XmlElement(required = true, nillable = true)
    protected String cardExpiration;
    @XmlElement(required = true, nillable = true)
    protected String cardTrackOne;
    @XmlElement(required = true, nillable = true)
    protected String cardTrackTwo;

    /**
     * Gets the value of the cardCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCurrency() {
        return cardCurrency;
    }

    /**
     * Sets the value of the cardCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCurrency(String value) {
        this.cardCurrency = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the pinNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPinNumber() {
        return pinNumber;
    }

    /**
     * Sets the value of the pinNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPinNumber(String value) {
        this.pinNumber = value;
    }

    /**
     * Gets the value of the cardExpiration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardExpiration() {
        return cardExpiration;
    }

    /**
     * Sets the value of the cardExpiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardExpiration(String value) {
        this.cardExpiration = value;
    }

    /**
     * Gets the value of the cardTrackOne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardTrackOne() {
        return cardTrackOne;
    }

    /**
     * Sets the value of the cardTrackOne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardTrackOne(String value) {
        this.cardTrackOne = value;
    }

    /**
     * Gets the value of the cardTrackTwo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardTrackTwo() {
        return cardTrackTwo;
    }

    /**
     * Sets the value of the cardTrackTwo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardTrackTwo(String value) {
        this.cardTrackTwo = value;
    }

}
