
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="redemptionReturn" type="{http://giftcard.storedvalue.com}RedemptionResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "redemptionReturn"
})
@XmlRootElement(name = "redemptionResponse")
public class RedemptionResponse {

    @XmlElement(required = true, nillable = true)
    protected RedemptionResponse2 redemptionReturn;

    /**
     * Gets the value of the redemptionReturn property.
     * 
     * @return
     *     possible object is
     *     {@link RedemptionResponse2 }
     *     
     */
    public RedemptionResponse2 getRedemptionReturn() {
        return redemptionReturn;
    }

    /**
     * Sets the value of the redemptionReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link RedemptionResponse2 }
     *     
     */
    public void setRedemptionReturn(RedemptionResponse2 value) {
        this.redemptionReturn = value;
    }

}
