
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cashBackReturn" type="{http://giftcard.storedvalue.com}CashBackResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cashBackReturn"
})
@XmlRootElement(name = "cashBackResponse")
public class CashBackResponse {

    @XmlElement(required = true, nillable = true)
    protected CashBackResponse2 cashBackReturn;

    /**
     * Gets the value of the cashBackReturn property.
     * 
     * @return
     *     possible object is
     *     {@link CashBackResponse2 }
     *     
     */
    public CashBackResponse2 getCashBackReturn() {
        return cashBackReturn;
    }

    /**
     * Sets the value of the cashBackReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashBackResponse2 }
     *     
     */
    public void setCashBackReturn(CashBackResponse2 value) {
        this.cashBackReturn = value;
    }

}
