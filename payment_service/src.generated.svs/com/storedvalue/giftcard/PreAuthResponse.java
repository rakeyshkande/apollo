
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="preAuthReturn" type="{http://giftcard.storedvalue.com}PreAuthResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "preAuthReturn"
})
@XmlRootElement(name = "preAuthResponse")
public class PreAuthResponse {

    @XmlElement(required = true, nillable = true)
    protected PreAuthResponse2 preAuthReturn;

    /**
     * Gets the value of the preAuthReturn property.
     * 
     * @return
     *     possible object is
     *     {@link PreAuthResponse2 }
     *     
     */
    public PreAuthResponse2 getPreAuthReturn() {
        return preAuthReturn;
    }

    /**
     * Sets the value of the preAuthReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreAuthResponse2 }
     *     
     */
    public void setPreAuthReturn(PreAuthResponse2 value) {
        this.preAuthReturn = value;
    }

}
