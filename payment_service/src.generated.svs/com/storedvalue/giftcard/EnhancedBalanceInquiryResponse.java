
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enhancedBalanceInquiryReturn" type="{http://giftcard.storedvalue.com}EnhancedBalanceInquiryResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enhancedBalanceInquiryReturn"
})
@XmlRootElement(name = "enhancedBalanceInquiryResponse")
public class EnhancedBalanceInquiryResponse {

    @XmlElement(required = true, nillable = true)
    protected EnhancedBalanceInquiryResponse2 enhancedBalanceInquiryReturn;

    /**
     * Gets the value of the enhancedBalanceInquiryReturn property.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedBalanceInquiryResponse2 }
     *     
     */
    public EnhancedBalanceInquiryResponse2 getEnhancedBalanceInquiryReturn() {
        return enhancedBalanceInquiryReturn;
    }

    /**
     * Sets the value of the enhancedBalanceInquiryReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedBalanceInquiryResponse2 }
     *     
     */
    public void setEnhancedBalanceInquiryReturn(EnhancedBalanceInquiryResponse2 value) {
        this.enhancedBalanceInquiryReturn = value;
    }

}
