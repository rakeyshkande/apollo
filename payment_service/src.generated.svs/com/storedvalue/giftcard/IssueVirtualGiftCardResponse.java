
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="issueVirtualGiftCardReturn" type="{http://giftcard.storedvalue.com}IssueVirtualGiftCardResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issueVirtualGiftCardReturn"
})
@XmlRootElement(name = "issueVirtualGiftCardResponse")
public class IssueVirtualGiftCardResponse {

    @XmlElement(required = true, nillable = true)
    protected IssueVirtualGiftCardResponse2 issueVirtualGiftCardReturn;

    /**
     * Gets the value of the issueVirtualGiftCardReturn property.
     * 
     * @return
     *     possible object is
     *     {@link IssueVirtualGiftCardResponse2 }
     *     
     */
    public IssueVirtualGiftCardResponse2 getIssueVirtualGiftCardReturn() {
        return issueVirtualGiftCardReturn;
    }

    /**
     * Sets the value of the issueVirtualGiftCardReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssueVirtualGiftCardResponse2 }
     *     
     */
    public void setIssueVirtualGiftCardReturn(IssueVirtualGiftCardResponse2 value) {
        this.issueVirtualGiftCardReturn = value;
    }

}
