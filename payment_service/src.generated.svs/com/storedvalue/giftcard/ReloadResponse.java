
package com.storedvalue.giftcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reloadReturn" type="{http://giftcard.storedvalue.com}ReloadResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reloadReturn"
})
@XmlRootElement(name = "reloadResponse")
public class ReloadResponse {

    @XmlElement(required = true, nillable = true)
    protected ReloadResponse2 reloadReturn;

    /**
     * Gets the value of the reloadReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ReloadResponse2 }
     *     
     */
    public ReloadResponse2 getReloadReturn() {
        return reloadReturn;
    }

    /**
     * Sets the value of the reloadReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReloadResponse2 }
     *     
     */
    public void setReloadReturn(ReloadResponse2 value) {
        this.reloadReturn = value;
    }

}
