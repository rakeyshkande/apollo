/**
 * 
 */
package com.ftd.ps.common.util;


import static com.ftd.milespoints.common.constant.MilesPointsConstants.CONFIG_CONTEXT;
import static com.ftd.milespoints.common.constant.MilesPointsConstants.UA_UPDATE_MILES_SERVICE_ENDPOINT_URL;
import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ftd.milespoints.common.constant.MilesPointsConstants;
import com.ftd.milespoints.common.util.CommonUtil;
import com.ftd.milespoints.webservice.vo.PartnerResponse;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.MPRedemptionVO;
import com.ftd.osp.utilities.id.IdGenerator;
import com.ftd.osp.utilities.id.IdGeneratorFactory;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.common.util.WebServiceClientFactory;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import com.ual.cust.ptvl.isstkt.prodsvc.GetPartnerProfilePortType;
import com.ual.cust.ptvl.isstkt.prodsvc.ItineraryNbrType;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerProfile;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerRetrvProfile;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesRequest;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesResponse;
import com.ual.cust.ptvl.isstkt.prodsvc.ItineraryNbrType.PNR;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerRetrvProfile.ProgramMembership;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.LoyaltyProgram;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Pricing;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Redemption;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Travel;
import com.ual.cust.ptvl.isstkt.prodsvc.PricingType.OrderData;
import com.ual.cust.ptvl.isstkt.prodsvc.PricingType.TaxInfo;
import com.ual.cust.ptvl.isstkt.prodsvc.TravelType.FirstTravelItem;
import com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesPort;

/**
 * @author cjohnson
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:**/spring-beans.xml"})
public class TestUnitedWebServiceInterface {
	
	protected static Connection connection;
    static String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie";
	
	private static Logger log = new Logger(TestUnitedWebServiceInterface.class.getName());
	
	//this is the thing we're testing
	WebServiceClientFactory cf = new WebServiceClientFactory();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		connection = setupConnection();
		
		URL ks = ResourceUtil.getInstance().getResource("UAKeystore.jks");
		
		Properties properties = System.getProperties();
		properties.put("javax.net.ssl.keyStore", ks.getPath());
		properties.put("javax.net.ssl.keyStorePassword", "importkey");
		properties.put("javax.net.ssl.trustStore", ks.getPath());
		properties.put("javax.net.ssl.trustStorePassword", "importkey");
		
		com.ftd.ps.common.util.CommonUtil psUtil = createMock(com.ftd.ps.common.util.CommonUtil.class);
		expect(psUtil.getFrpGlobalParm("SERVICE", MilesPointsConstants.UA_PROFILE_SERVICE_ENDPOINT_URL))
			.andReturn("https://209.87.125.174/customerProfile/PartnerRetrvProfile/1.0.0").anyTimes();
		expect(psUtil.getFrpGlobalParm("SERVICE", MilesPointsConstants.UA_UPDATE_MILES_SERVICE_ENDPOINT_URL))
			.andReturn("https://209.87.125.174/redemption/PartnerUpdateMiles/1.0.0").anyTimes();
	    expect(psUtil.getFrpGlobalParm(CONFIG_CONTEXT, MilesPointsConstants.UA_SERVICE_USERNAME)).andReturn("SVC-TFTDPR").anyTimes();
	    expect(psUtil.getSecureGlobalParm(CONFIG_CONTEXT, MilesPointsConstants.UA_SERVICE_PASSWORD)).andReturn("mJNfbn5rGo").anyTimes();
	    expect(psUtil.getFrpGlobalParm(MilesPointsConstants.CONFIG_CONTEXT, "UA" + MilesPointsConstants._API_CONNECTION_TIMEOUT)).andReturn("60000").anyTimes();
		replay(psUtil);
		cf.util = psUtil;
		
		UnitedServiceInterceptor unitedInterceptor = new UnitedServiceInterceptor();
		unitedInterceptor.util = psUtil;
		cf.unitedInterceptor = unitedInterceptor;
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetMemberProfie() throws Exception {
		
		GetPartnerProfilePortType svc = null;
		try {
			svc = cf.getUnitedRetreiveProfileService();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(svc != null);
		
		PartnerRetrvProfile profileReq = new PartnerRetrvProfile();
		profileReq.setVersion("3.0.0");
		ProgramMembership membership = new ProgramMembership();
		membership.setMemberID("03129325380");
		profileReq.setProgramMembership(membership);
		
		PartnerProfile profile = null;

		profile = svc.getPartnerProfileOperation(profileReq);

		
		assertTrue(profile != null);
		
	}
	
	@Test
	public void testUpdateMiles() throws Exception {
		
		
        PartnerResponse prvo = null;
        
        PartnerUpdateMilesPort updateMilesSvc = cf.getUnitedUpdateMilesService();
        IdRequestVO idRequest = new IdRequestVO("payment service", IdGeneratorFactory.UNITED_REQUEST_TYPE);
        IdGenerator idGenerator = IdGeneratorFactory.getGenerator(idRequest);
        //add the request header with our unique transaction ID
        cf.addRequestHeaderToUARequest(updateMilesSvc, idGenerator.execute(idRequest, connection));
        
        PartnerUpdateMilesRequest req = new PartnerUpdateMilesRequest();
        req.setVersion("1.0.0");
        //set the memberID
        LoyaltyProgram lp = new LoyaltyProgram();
        lp.setLoyaltyProgramNbr("03129325380");
        req.setLoyaltyProgram(lp);
        
        //set passenger
        req.setPassenger(null);
        
        //set Itinerary
        ItineraryNbrType iNbrType = new ItineraryNbrType();
        PNR pnr = new PNR();
        pnr.setRecordLocator("1234567");
        iNbrType.setPNR(pnr);
        req.setItineraryNbr(iNbrType);
        
        //set firstLeg
        req.setFirstLeg(null);
        
        //set Travel
        Travel travel = new Travel();
        travel.setBonusCode("AFW");
        travel.setPartnerCode("OP");
        travel.setBonusType("X1 ");
        FirstTravelItem fti = new FirstTravelItem();
        fti.setNbrOfItem("0");
        fti.setSupplierName("    ");
        travel.setFirstTravelItem(fti);
        req.setTravel(travel);
        
        //set Redemption info
        Redemption redemption = new Redemption();
        redemption.setTotalPoints(1);
        redemption.setDebitCreditInd("D");
        XMLGregorianCalendar xmlCal = new XMLGregorianCalendarImpl(new GregorianCalendar());
        redemption.setEarnDate(xmlCal);
        req.setRedemption(redemption);
        
        //Pricing
        Pricing pricing = new Pricing();
        pricing.setSystemCurrencyCode("USD");
        pricing.setPosSystemConversionRate(1.0f);
        OrderData od = new OrderData();
        od.setTotalPoints("1000");
        od.setTotalCashAmount("0.00");
        od.setCurrencyCode("USD");
        pricing.setOrderData(od);
        TaxInfo taxInfo = new TaxInfo();
        taxInfo.setTaxAmount("0.00");
        pricing.setTaxInfo(taxInfo);
        req.setPricing(pricing);
        
        PartnerUpdateMilesResponse response = null;
        
        try {
			response = updateMilesSvc.partnerUpdateMiles(req);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(response != null);
		
		System.out.println("member has " + response.getConfirmation().getBalanceInfo().getAccountBalance() + " miles remaining");
	}
	
	 protected static Connection setupConnection()
	    {
	        if (connection == null)
	        {
	            try
	            {
	                String driver_ = "oracle.jdbc.driver.OracleDriver";
	                String user_ = "osp";
	                String password_ = "osp";
	    
	                Class.forName(driver_);                                                           
	                connection = DriverManager.getConnection(database_, user_, password_);
	            }
	            catch (Exception e)
	            {
	                throw new RuntimeException(e);
	            }
	        }
	    
	        return connection;
	    }

}
