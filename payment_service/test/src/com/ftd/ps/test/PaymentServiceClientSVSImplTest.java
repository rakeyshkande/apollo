package com.ftd.ps.test;

import java.io.Writer;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;

import com.ftd.ps.common.constant.PaymentServiceConstants;
import com.ftd.ps.common.util.CommonUtil;
import com.ftd.ps.webservice.vo.AmountVO;
import com.ftd.ps.webservice.vo.CardVO;
import com.ftd.ps.webservice.vo.PaymentResponse;
import com.storedvalue.giftcard.Amount;
import com.storedvalue.giftcard.BalanceInquiryRequest;
import com.storedvalue.giftcard.BalanceInquiryResponse2;
import com.storedvalue.giftcard.Card;
import com.storedvalue.giftcard.GiftCardOnline;
import com.storedvalue.giftcard.GiftCardOnlineService;
import com.storedvalue.giftcard.MerchandiseReturnRequest;
import com.storedvalue.giftcard.MerchandiseReturnResponse2;
import com.storedvalue.giftcard.Merchant;
import com.storedvalue.giftcard.PreAuthCompleteRequest;
import com.storedvalue.giftcard.PreAuthCompleteResponse2;
import com.storedvalue.giftcard.PreAuthRequest;
import com.storedvalue.giftcard.PreAuthResponse2;
import com.storedvalue.giftcard.ReturnCode;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class PaymentServiceClientSVSImplTest
{

  /**
   * @param args
   */
  public static void main(String[] args) throws Exception
  {
    try
    {
      //URL url = new URL("https://ws-tpcert.storedvalue.com:443/gcweb/services/GiftCardOnline?wsdl");
      //URL url = new URL("file:wsdl/giftCardOnlineasdfaCert.wsdl");
      //URL wsdlURL = GiftCardOnlineService.class.getResource ("giftCardOnline.wsdl");

      GiftCardOnlineService service = new GiftCardOnlineService();
/*
      ServicePort client = service.getServicePort();
      BindingProvider provider = (BindingProvider)client;
      // You can set the address per request here
      provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://my/new/url/to/the/service");
*/      
      
      
      GiftCardOnlineService gcs = new GiftCardOnlineService();
      GiftCardOnline gco = gcs.getGiftCardOnline();
      CommonUtil util = new CommonUtil();

      // MINE
/*      String testCardNumber = "6006492147013900373";
      String testPinNumber = "6611";
      String testTransactionId = "303760ALI0000003";
*/
      
        String testCardNumber = "6006492147013900480";
        String testPinNumber = "5482";
        String testTransactionId = "6037302000002912";
       

      //getBalance(gco, util, testCardNumber, testPinNumber, testTransactionId);
      //settle(gco, util, testCardNumber, testPinNumber, testTransactionId);
      getAuthorization(gco, util, testCardNumber, testPinNumber, testTransactionId);
      //cancelAuthorization(gco, util, testCardNumber, testPinNumber, testTransactionId);
      //refund(gco, util, testCardNumber, testPinNumber, testTransactionId);

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * This method will call PreAuthCompleteRequest on the SVS web service
   */
  private static void cancelAuthorization(GiftCardOnline gco, CommonUtil util, String testCardNumber, String testPinNumber, String testTransactionId) throws Exception
  {
    PreAuthCompleteRequest pacRequest = new PreAuthCompleteRequest();

    Card card = new Card();
    card.setCardCurrency("USD");
    card.setCardNumber(util.mask(testCardNumber, PaymentServiceConstants.MASK_LEFT, "*", 4));
    card.setCardTrackOne(null);
    card.setCardTrackTwo(null);
    card.setPinNumber(util.mask(testPinNumber, PaymentServiceConstants.MASK_LEFT, "*", 1));
    pacRequest.setCard(card);

    Amount amt = new Amount();
    amt.setAmount(0);
    amt.setCurrency("USD");
    pacRequest.setTransactionAmount(amt);

    Merchant merch = new Merchant();
    merch.setDivision("00000");
    merch.setMerchantName("FTD");
    merch.setMerchantNumber("067303");
    merch.setStoreNumber("0000009999");
    pacRequest.setMerchant(merch);

    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    pacRequest.setDate(xmlgcCal);
    pacRequest.setInvoiceNumber("00000000");
    pacRequest.setRoutingID("6006492147003000000");
    pacRequest.setStan(null);
    pacRequest.setTransactionID(testTransactionId);
    pacRequest.setCheckForDuplicate(true);

    // Code for Interceptor to put user id and password as part of SOAP header
    SVSInterceptorTest sInterceptor = new SVSInterceptorTest();
    sInterceptor.setUsername("FTD-uat");
    sInterceptor.setPassword("D@1syu@t");

    Client cxfClient = ClientProxy.getClient(gco);
    cxfClient.getOutInterceptors().add(sInterceptor);

    XStream xstream = getXStreamObject();

    String sPacRequest = xstream.toXML(pacRequest);
    System.out.println(sPacRequest);
    System.out.println("******************************************************");

    // put logging there

    card.setCardNumber(testCardNumber);
    card.setPinNumber(testPinNumber);

    PreAuthCompleteResponse2 pacResponse = gco.preAuthComplete(pacRequest);

    PaymentResponse pmtResponse = new PaymentResponse();
    parseResponse(util, pmtResponse, pacResponse);
    String sPacResponse = xstream.toXML(pacResponse);
    System.out.println(sPacResponse);
    System.out.println("******************************************************");

    System.out.println("hi cancelAuthorization is finished");

  }

  /**
   * This method will call PreAuthRequest on the SVS web service
   */
  private static void getAuthorization(GiftCardOnline gco, CommonUtil util, String testCardNumber, String testPinNumber, String testTransactionId) throws Exception
  {
    PreAuthRequest paRequest = new PreAuthRequest();

    Card card = new Card();
    card.setCardCurrency("USD");
    card.setCardNumber(util.mask(testCardNumber, PaymentServiceConstants.MASK_LEFT, "*", 4));
    card.setCardTrackOne(null);
    card.setCardTrackTwo(null);
    card.setPinNumber(util.mask(testPinNumber, PaymentServiceConstants.MASK_LEFT, "*", 1));
    paRequest.setCard(card);

    Amount amt = new Amount();
    amt.setAmount(60.00);
    amt.setCurrency("USD");
    paRequest.setRequestedAmount(amt);

    Merchant merch = new Merchant();
    merch.setDivision("00000");
    merch.setMerchantName("FTD");
    merch.setMerchantNumber("067303");
    merch.setStoreNumber("0000009999");
    paRequest.setMerchant(merch);

    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    paRequest.setDate(xmlgcCal);
    paRequest.setInvoiceNumber("00000000");
    paRequest.setRoutingID("6006492147003000000");
    paRequest.setStan(null);
    paRequest.setTransactionID(testTransactionId);
    paRequest.setCheckForDuplicate(true);

    // Code for Interceptor to put user id and password as part of SOAP header
    SVSInterceptorTest sInterceptor = new SVSInterceptorTest();
    sInterceptor.setUsername("FTD-uat");
    sInterceptor.setPassword("D@1syu@t");

    Client cxfClient = ClientProxy.getClient(gco);
    cxfClient.getOutInterceptors().add(sInterceptor);

    XStream xstream = getXStreamObject();

    String sPaRequest = xstream.toXML(paRequest);
    System.out.println(sPaRequest);
    System.out.println("******************************************************");

    // put logging there

    card.setCardNumber(testCardNumber);
    card.setPinNumber(testPinNumber);

    PreAuthResponse2 paResponse = gco.preAuth(paRequest);

    PaymentResponse pmtResponse = new PaymentResponse();
    parseResponse(util, pmtResponse, paResponse);
    String sPaResponse = xstream.toXML(paResponse);
    System.out.println(sPaResponse);
    System.out.println("******************************************************");

    System.out.println("hi getAuthorization is finished");

  }

  /**
   * This method will call BalanceInquiryRequest on the SVS web service
   */
  private static void getBalance(GiftCardOnline gco, CommonUtil util, String testCardNumber, String testPinNumber, String testTransactionId) throws Exception
  {
    BalanceInquiryRequest biRequest = new BalanceInquiryRequest();

    Card card = new Card();
    card.setCardCurrency("USD");
    card.setCardNumber(util.mask(testCardNumber, PaymentServiceConstants.MASK_LEFT, "*", 4));
    card.setCardTrackOne(null);
    card.setCardTrackTwo(null);
    card.setPinNumber(util.mask(testPinNumber, PaymentServiceConstants.MASK_LEFT, "*", 1));
    biRequest.setCard(card);

    Amount amt = new Amount();
    amt.setAmount(0);
    amt.setCurrency("USD");
    biRequest.setAmount(amt);

    Merchant merch = new Merchant();
    merch.setDivision("00000");
    merch.setMerchantName("FTD");
    merch.setMerchantNumber("067303");
    merch.setStoreNumber("0000009999");
    biRequest.setMerchant(merch);

    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    biRequest.setDate(xmlgcCal);
    biRequest.setInvoiceNumber("00000000");
    biRequest.setRoutingID("6006492147003000000");
    biRequest.setStan(null);
    biRequest.setTransactionID(testTransactionId);
    biRequest.setCheckForDuplicate(true);

    // Code for Interceptor to put user id and password as part of SOAP header
    SVSInterceptorTest sInterceptor = new SVSInterceptorTest();
    sInterceptor.setUsername("FTD-uat");
    sInterceptor.setPassword("D@1syu@t");

    Client cxfClient = ClientProxy.getClient(gco);
    cxfClient.getOutInterceptors().add(sInterceptor);

    XStream xstream = getXStreamObject();

    String sBiRequest = xstream.toXML(biRequest);
    System.out.println(sBiRequest);
    System.out.println("******************************************************");

    // put logging there

    card.setCardNumber(testCardNumber);
    card.setPinNumber(testPinNumber);

    BalanceInquiryResponse2 biResponse = gco.balanceInquiry(biRequest);

    PaymentResponse pmtResponse = new PaymentResponse();
    parseResponse(util, pmtResponse, biResponse);
    String sBiResponse = xstream.toXML(biResponse);
    System.out.println(sBiResponse);
    System.out.println("******************************************************");

    System.out.println("hi getBalance is finished");

  }

  /**
   * This method will call MerchandiseReturnRequest on the SVS web service
   */
  private static void refund(GiftCardOnline gco, CommonUtil util, String testCardNumber, String testPinNumber, String testTransactionId) throws Exception
  {
    MerchandiseReturnRequest mrRequest = new MerchandiseReturnRequest();

    Card card = new Card();
    card.setCardCurrency("USD");
    card.setCardNumber(util.mask(testCardNumber, PaymentServiceConstants.MASK_LEFT, "*", 4));
    card.setCardTrackOne(null);
    card.setCardTrackTwo(null);
    card.setPinNumber(util.mask(testPinNumber, PaymentServiceConstants.MASK_LEFT, "*", 1));
    mrRequest.setCard(card);

    Amount amt = new Amount();
    amt.setAmount(0);
    amt.setCurrency("USD");
    mrRequest.setReturnAmount(amt);

    Merchant merch = new Merchant();
    merch.setDivision("00000");
    merch.setMerchantName("FTD");
    merch.setMerchantNumber("067303");
    merch.setStoreNumber("0000009999");
    mrRequest.setMerchant(merch);

    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    mrRequest.setDate(xmlgcCal);
    mrRequest.setInvoiceNumber("00000000");
    mrRequest.setRoutingID("6006492147003000000");
    mrRequest.setStan(null);
    mrRequest.setTransactionID(testTransactionId);
    mrRequest.setCheckForDuplicate(true);

    // Code for Interceptor to put user id and password as part of SOAP header
    SVSInterceptorTest sInterceptor = new SVSInterceptorTest();
    sInterceptor.setUsername("FTD-uat");
    sInterceptor.setPassword("D@1syu@t");

    Client cxfClient = ClientProxy.getClient(gco);
    cxfClient.getOutInterceptors().add(sInterceptor);

    XStream xstream = getXStreamObject();

    String sMrRequest = xstream.toXML(mrRequest);
    System.out.println(sMrRequest);
    System.out.println("******************************************************");

    // put logging there

    card.setCardNumber(testCardNumber);
    card.setPinNumber(testPinNumber);

    MerchandiseReturnResponse2 mrResponse = gco.merchandiseReturn(mrRequest);

    PaymentResponse pmtResponse = new PaymentResponse();
    parseResponse(util, pmtResponse, mrResponse);
    String sMrResponse = xstream.toXML(mrResponse);
    System.out.println(sMrResponse);
    System.out.println("******************************************************");

    System.out.println("hi refund is finished");

  }

  /**
   * This method will call PreAuthCompleteRequest on the SVS web service
   */
  private static void settle(GiftCardOnline gco, CommonUtil util, String testCardNumber, String testPinNumber, String testTransactionId) throws Exception
  {
    PreAuthCompleteRequest pacRequest = new PreAuthCompleteRequest();

    Card card = new Card();
    card.setCardCurrency("USD");
    card.setCardNumber(util.mask(testCardNumber, PaymentServiceConstants.MASK_LEFT, "*", 4));
    card.setCardTrackOne(null);
    card.setCardTrackTwo(null);
    card.setPinNumber(util.mask(testPinNumber, PaymentServiceConstants.MASK_LEFT, "*", 1));
    pacRequest.setCard(card);

    Amount amt = new Amount();
    amt.setAmount(1.11);
    amt.setCurrency("USD");
    pacRequest.setTransactionAmount(amt);

    Merchant merch = new Merchant();
    merch.setDivision("00000");
    merch.setMerchantName("FTD");
    merch.setMerchantNumber("067303");
    merch.setStoreNumber("0000009999");
    pacRequest.setMerchant(merch);

    GregorianCalendar gcCal = new GregorianCalendar();
    XMLGregorianCalendar xmlgcCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcCal);
    pacRequest.setDate(xmlgcCal);
    pacRequest.setInvoiceNumber("00000000");
    pacRequest.setRoutingID("6006492147003000000");
    pacRequest.setStan(null);
    pacRequest.setTransactionID(testTransactionId);
    pacRequest.setCheckForDuplicate(true);

    // Code for Interceptor to put user id and password as part of SOAP header
    SVSInterceptorTest sInterceptor = new SVSInterceptorTest();
    sInterceptor.setUsername("FTD-uat");
    sInterceptor.setPassword("D@1syu@t");

    Client cxfClient = ClientProxy.getClient(gco);
    cxfClient.getOutInterceptors().add(sInterceptor);

    XStream xstream = getXStreamObject();

    String sPacRequest = xstream.toXML(pacRequest);
    System.out.println(sPacRequest);
    System.out.println("******************************************************");

    // put logging there

    card.setCardNumber(testCardNumber);
    card.setPinNumber(testPinNumber);

    PreAuthCompleteResponse2 pacResponse = gco.preAuthComplete(pacRequest);

    PaymentResponse pmtResponse = new PaymentResponse();
    parseResponse(util, pmtResponse, pacResponse);
    String sPacResponse = xstream.toXML(pacResponse);
    System.out.println(sPacResponse);
    System.out.println("******************************************************");

    System.out.println("hi settle is finished");

  }

  private static void parseResponse(CommonUtil util, PaymentResponse pmtResponse, BalanceInquiryResponse2 svsResponse) throws Exception
  {

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(util.mask((((Card)svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(util.mask((((Card)svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));
      
      CardVO cardVO = new CardVO();
      Card testcard = (Card)svsResponse.getCard();
      cardVO.setCardCurrency(((Card)svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card)svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card)svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card)svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card)svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card)svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }
  
    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode)svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode)svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }

  }

  private static void parseResponse(CommonUtil util, PaymentResponse pmtResponse, PreAuthCompleteResponse2 svsResponse) throws Exception
  {

    if (svsResponse.getApprovedAmount() != null)
    {
      AmountVO approvedAmountVO = new AmountVO();
      approvedAmountVO.setAmount(((Amount) svsResponse.getApprovedAmount()).getAmount());
      approvedAmountVO.setCurrency(((Amount) svsResponse.getApprovedAmount()).getCurrency());
      pmtResponse.setApprovedAmountVO(approvedAmountVO);
    }

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(util.mask((((Card)svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(util.mask((((Card)svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));
      
      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card)svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card)svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card)svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card)svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card)svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card)svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }
  
    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode)svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode)svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }
  

  }

  private static void parseResponse(CommonUtil util, PaymentResponse pmtResponse, MerchandiseReturnResponse2 svsResponse) throws Exception
  {

    if (svsResponse.getApprovedAmount() != null)
    {
      AmountVO approvedAmountVO = new AmountVO();
      approvedAmountVO.setAmount(((Amount) svsResponse.getApprovedAmount()).getAmount());
      approvedAmountVO.setCurrency(((Amount) svsResponse.getApprovedAmount()).getCurrency());
      pmtResponse.setApprovedAmountVO(approvedAmountVO);
    }

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(util.mask((((Card)svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(util.mask((((Card)svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));
      
      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card)svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card)svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card)svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card)svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card)svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card)svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }
  
    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode)svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode)svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }

  }

  private static void parseResponse(CommonUtil util, PaymentResponse pmtResponse, PreAuthResponse2 svsResponse) throws Exception
  {

    if (svsResponse.getApprovedAmount() != null)
    {
      AmountVO approvedAmountVO = new AmountVO();
      approvedAmountVO.setAmount(((Amount) svsResponse.getApprovedAmount()).getAmount());
      approvedAmountVO.setCurrency(((Amount) svsResponse.getApprovedAmount()).getCurrency());
      pmtResponse.setApprovedAmountVO(approvedAmountVO);
    }

    if (svsResponse.getAuthorizationCode() != null)
    {
      pmtResponse.setAuthorizationCode(svsResponse.getAuthorizationCode());
    }

    if (svsResponse.getBalanceAmount() != null)
    {
      AmountVO balanceAmountVO = new AmountVO();
      balanceAmountVO.setAmount(((Amount) svsResponse.getBalanceAmount()).getAmount());
      balanceAmountVO.setCurrency(((Amount) svsResponse.getBalanceAmount()).getCurrency());
      pmtResponse.setBalanceAmountVO(balanceAmountVO);
    }

    if (svsResponse.getCard() != null)
    {
      svsResponse.getCard().setCardNumber(util.mask((((Card)svsResponse.getCard()).getCardNumber()), PaymentServiceConstants.MASK_LEFT, "*", 4));
      svsResponse.getCard().setPinNumber(util.mask((((Card)svsResponse.getCard()).getPinNumber()), PaymentServiceConstants.MASK_LEFT, "*", 1));
      
      CardVO cardVO = new CardVO();
      cardVO.setCardCurrency(((Card)svsResponse.getCard()).getCardCurrency());
      cardVO.setCardExpiration(((Card)svsResponse.getCard()).getCardExpiration());
      cardVO.setCardTrackOne(((Card)svsResponse.getCard()).getCardTrackOne());
      cardVO.setCardTrackTwo(((Card)svsResponse.getCard()).getCardTrackTwo());
      cardVO.setCardNumber(((Card)svsResponse.getCard()).getCardNumber());
      cardVO.setPinNumber(((Card)svsResponse.getCard()).getPinNumber());
      pmtResponse.setCardVO(cardVO);
    }

    if (svsResponse.getConversionRate() != null)
    {
      pmtResponse.setConversionRate(svsResponse.getConversionRate());
    }
  
    if (svsResponse.getReturnCode() != null)
    {
      pmtResponse.setResponseCode(((ReturnCode)svsResponse.getReturnCode()).getReturnCode());
      pmtResponse.setResponseMessage(((ReturnCode)svsResponse.getReturnCode()).getReturnDescription());

    }

    if (svsResponse.getTransactionID() != null)
    {
      pmtResponse.setTransactionId(svsResponse.getTransactionID());
    }
  

  }


  private static XStream getXStreamObject()
  {
/*    
    XStream xstream = new XStream(new JsonHierarchicalStreamDriver()
    {
      public HierarchicalStreamWriter createWriter(Writer writer)
      {
        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
      }
    });
*/

    
    XStream xstream = new XStream()
    {
      protected MapperWrapper wrapMapper(MapperWrapper next)
      {
        return new MapperWrapper(next)
          {
            public boolean shouldSerializeMember(Class definedIn, String fieldName)
            {
              boolean retVal = definedIn != Object.class? super.shouldSerializeMember(definedIn, fieldName): false;

              if (!retVal)
              {
                try
                {
                  Class realClazz = realClass(fieldName);
                  if (realClazz != null)
                  {
                    retVal = true;
                  }
                }
                catch (Exception e)
                {
                }
              }
              return retVal;
            }
          };
      }
    };


/*    
    XStream xstream = new XStream(new XppDriver()
    {
      public HierarchicalStreamWriter createWriter(Writer out)
      {
        return new CompactWriter(out)
          {
            protected void writeText(QuickWriter writer, String text)
            {
              if(text != null && text.length() > 0)
              {
                writer.write("<![CDATA[");
                writer.write(text);
                writer.write("]]>");
              } else
              {
                writer.write(text);
              }
            }
          };
      }
    });

*/    

  return xstream;
}

}