package com.ftd.ps.test;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Types;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.id.vo.IdRequestVO;

public class SVSIDGeneratorTest {
	
	public static void main(String[] args) {
		Connection conn =null;
		try {
			IdRequestVO idRequestVO = new IdRequestVO("APOLLO","SVS_TRANS_ID");		
			StringBuffer svsTransactionId = new StringBuffer("");
			
			//connection = DataRequestHelper.getDBConnection();	
			conn = getConnection();
			/* Step 1 : 
			 This value is supposed to come from global parameters �SVS_TRANSACTION_ID_PREFIX� of context SERVICE.
			 Please verify this value from one in the frp.global_parms table 
			 Use this as digit 1 through 6. 
			 */
			 svsTransactionId.append("603730");
			 
					
			/* Step 2:
			 * This value is supposed to come from global parameters �ID_REQUEST_SVS_CLIENT_APOLLO" of context GLOBAL_CONFIG. 
			 * Use this as digit 7. */
			 svsTransactionId.append("2");
			 
			 BigDecimal transId = getSvsTransactionId(conn);
			 if(transId == null || transId.doubleValue() <= 0) {
				throw new Exception("Invalid Transaction Id returned");
			 }
			 String formattedString = String.format("%09d", transId.intValue());			 
			 svsTransactionId.append(formattedString);
			 
			 
			 //return svsTransactionId.toString();
				    	    	
	    	System.out.println("New Trasaction Id generated is : " + svsTransactionId.toString());
		} catch (Exception e){
			System.out.println("Error caught in main method: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					System.out.println("Error caught closing connection : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}		
	}
	
	
	/** As SVS transaction ID Util classes are developed as part of FIX-5_4_0, 
	 * Database SID is defined as Grover.
	 * @return
	 * @throws Exception
	 */
	private static Connection getConnection() throws Exception {
		String driver_ = "oracle.jdbc.driver.OracleDriver";
		String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:grover";
		String user_ = "osp";
		String password_ = "osp";

		Class.forName(driver_);
		Connection conn = DriverManager.getConnection(database_, user_,	password_);
		return conn;
	}
	
	
    /**Method to call Function "GET_NEXT_SVS_TRANSACTION_SEQ" in schema GLOBAL and package GLOBAL_PKG.
     *  This is to get the next value of the sequence CLEAN.SVS_TRANS_ID_SEQ
     * @param connection
     * @return
     */
    public static BigDecimal getSvsTransactionId(Connection connection) throws Exception {
    	String GET_NEXT_SVS_TRANSACTION_SEQ = "GET_NEXT_SVS_TRANSACTION_SEQ";
    	BigDecimal svsTransactionId = null;
	
		try {
			
			CallableStatement cs;
			cs = connection.prepareCall("{? = call GLOBAL.GLOBAL_PKG.GET_NEXT_SVS_TRANSACTION_SEQ()}");
			cs.registerOutParameter(1, Types.NUMERIC);
			cs.execute();
			svsTransactionId = cs.getBigDecimal(1);
			
		} catch (Exception e) {
			System.out.println("Error caught retrieving SVS transaction ID : " + e.getMessage());
			throw new Exception("Error caught retrieving SVS transaction ID : " + e.getMessage());
		} 
		return svsTransactionId;
	}	

}
