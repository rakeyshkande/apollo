
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EliteQualifyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EliteQualifyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="eqm" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="eqs" type="{http://www.w3.org/2001/XMLSchema}double" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EliteQualifyType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class EliteQualifyType {

    @XmlAttribute(name = "eqm")
    protected Double eqm;
    @XmlAttribute(name = "eqs")
    protected Double eqs;

    /**
     * Gets the value of the eqm property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getEqm() {
        return eqm;
    }

    /**
     * Sets the value of the eqm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setEqm(Double value) {
        this.eqm = value;
    }

    /**
     * Gets the value of the eqs property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getEqs() {
        return eqs;
    }

    /**
     * Sets the value of the eqs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setEqs(Double value) {
        this.eqs = value;
    }

}
