
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PassengerReferenceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PassengerReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="passengerRefNbr" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="passengerFirstNameRefNbr" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="passengerLastNameRefNbr" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PassengerReferenceType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class PassengerReferenceType {

    @XmlAttribute(name = "passengerRefNbr", required = true)
    protected BigInteger passengerRefNbr;
    @XmlAttribute(name = "passengerFirstNameRefNbr", required = true)
    protected BigInteger passengerFirstNameRefNbr;
    @XmlAttribute(name = "passengerLastNameRefNbr", required = true)
    protected BigInteger passengerLastNameRefNbr;

    /**
     * Gets the value of the passengerRefNbr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPassengerRefNbr() {
        return passengerRefNbr;
    }

    /**
     * Sets the value of the passengerRefNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPassengerRefNbr(BigInteger value) {
        this.passengerRefNbr = value;
    }

    /**
     * Gets the value of the passengerFirstNameRefNbr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPassengerFirstNameRefNbr() {
        return passengerFirstNameRefNbr;
    }

    /**
     * Sets the value of the passengerFirstNameRefNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPassengerFirstNameRefNbr(BigInteger value) {
        this.passengerFirstNameRefNbr = value;
    }

    /**
     * Gets the value of the passengerLastNameRefNbr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPassengerLastNameRefNbr() {
        return passengerLastNameRefNbr;
    }

    /**
     * Sets the value of the passengerLastNameRefNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPassengerLastNameRefNbr(BigInteger value) {
        this.passengerLastNameRefNbr = value;
    }

}
