
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProgramMembershipType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProgramMembershipType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProgramContact" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ProgramContactType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PromotionHistory" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ContactHistoryType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MemberSummary" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}MemberSummaryType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EliteQualify" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}EliteQualifyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CreditCard" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}CreditCardType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="memberID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="updatedMemberID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="memberLevel" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="memberStatus" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="memberStatusDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="warningIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="enrollDate" type="{http://www.w3.org/2001/XMLSchema}date" />
 *       &lt;attribute name="enrollCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="millionMilerIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="globalServiceIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="addrChgDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="accountType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="profitScore" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="behavioralSegCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProgramMembershipType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "programContact",
    "promotionHistory",
    "memberSummary",
    "eliteQualify",
    "creditCard"
})
public class ProgramMembershipType {

    @XmlElement(name = "ProgramContact", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<ProgramContactType> programContact;
    @XmlElement(name = "PromotionHistory", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<ContactHistoryType> promotionHistory;
    @XmlElement(name = "MemberSummary", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<MemberSummaryType> memberSummary;
    @XmlElement(name = "EliteQualify", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<EliteQualifyType> eliteQualify;
    @XmlElement(name = "CreditCard", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<CreditCardType> creditCard;
    @XmlAttribute(name = "memberID")
    protected String memberID;
    @XmlAttribute(name = "updatedMemberID")
    protected String updatedMemberID;
    @XmlAttribute(name = "memberLevel")
    protected String memberLevel;
    @XmlAttribute(name = "memberStatus")
    protected String memberStatus;
    @XmlAttribute(name = "memberStatusDesc")
    protected String memberStatusDesc;
    @XmlAttribute(name = "warningIndicator")
    protected String warningIndicator;
    @XmlAttribute(name = "enrollDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar enrollDate;
    @XmlAttribute(name = "enrollCode")
    protected String enrollCode;
    @XmlAttribute(name = "millionMilerIndicator")
    protected String millionMilerIndicator;
    @XmlAttribute(name = "globalServiceIndicator")
    protected String globalServiceIndicator;
    @XmlAttribute(name = "addrChgDate")
    protected String addrChgDate;
    @XmlAttribute(name = "accountType")
    protected String accountType;
    @XmlAttribute(name = "profitScore")
    protected String profitScore;
    @XmlAttribute(name = "behavioralSegCode")
    protected String behavioralSegCode;

    /**
     * Gets the value of the programContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the programContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProgramContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProgramContactType }
     * 
     * 
     */
    public List<ProgramContactType> getProgramContact() {
        if (programContact == null) {
            programContact = new ArrayList<ProgramContactType>();
        }
        return this.programContact;
    }

    /**
     * Gets the value of the promotionHistory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the promotionHistory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPromotionHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactHistoryType }
     * 
     * 
     */
    public List<ContactHistoryType> getPromotionHistory() {
        if (promotionHistory == null) {
            promotionHistory = new ArrayList<ContactHistoryType>();
        }
        return this.promotionHistory;
    }

    /**
     * Gets the value of the memberSummary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memberSummary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemberSummary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MemberSummaryType }
     * 
     * 
     */
    public List<MemberSummaryType> getMemberSummary() {
        if (memberSummary == null) {
            memberSummary = new ArrayList<MemberSummaryType>();
        }
        return this.memberSummary;
    }

    /**
     * Gets the value of the eliteQualify property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eliteQualify property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEliteQualify().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EliteQualifyType }
     * 
     * 
     */
    public List<EliteQualifyType> getEliteQualify() {
        if (eliteQualify == null) {
            eliteQualify = new ArrayList<EliteQualifyType>();
        }
        return this.eliteQualify;
    }

    /**
     * Gets the value of the creditCard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditCard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreditCardType }
     * 
     * 
     */
    public List<CreditCardType> getCreditCard() {
        if (creditCard == null) {
            creditCard = new ArrayList<CreditCardType>();
        }
        return this.creditCard;
    }

    /**
     * Gets the value of the memberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberID() {
        return memberID;
    }

    /**
     * Sets the value of the memberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberID(String value) {
        this.memberID = value;
    }

    /**
     * Gets the value of the updatedMemberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdatedMemberID() {
        return updatedMemberID;
    }

    /**
     * Sets the value of the updatedMemberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdatedMemberID(String value) {
        this.updatedMemberID = value;
    }

    /**
     * Gets the value of the memberLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberLevel() {
        return memberLevel;
    }

    /**
     * Sets the value of the memberLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberLevel(String value) {
        this.memberLevel = value;
    }

    /**
     * Gets the value of the memberStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberStatus() {
        return memberStatus;
    }

    /**
     * Sets the value of the memberStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberStatus(String value) {
        this.memberStatus = value;
    }

    /**
     * Gets the value of the memberStatusDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberStatusDesc() {
        return memberStatusDesc;
    }

    /**
     * Sets the value of the memberStatusDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberStatusDesc(String value) {
        this.memberStatusDesc = value;
    }

    /**
     * Gets the value of the warningIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarningIndicator() {
        return warningIndicator;
    }

    /**
     * Sets the value of the warningIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarningIndicator(String value) {
        this.warningIndicator = value;
    }

    /**
     * Gets the value of the enrollDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnrollDate() {
        return enrollDate;
    }

    /**
     * Sets the value of the enrollDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnrollDate(XMLGregorianCalendar value) {
        this.enrollDate = value;
    }

    /**
     * Gets the value of the enrollCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrollCode() {
        return enrollCode;
    }

    /**
     * Sets the value of the enrollCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrollCode(String value) {
        this.enrollCode = value;
    }

    /**
     * Gets the value of the millionMilerIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMillionMilerIndicator() {
        return millionMilerIndicator;
    }

    /**
     * Sets the value of the millionMilerIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMillionMilerIndicator(String value) {
        this.millionMilerIndicator = value;
    }

    /**
     * Gets the value of the globalServiceIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalServiceIndicator() {
        return globalServiceIndicator;
    }

    /**
     * Sets the value of the globalServiceIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalServiceIndicator(String value) {
        this.globalServiceIndicator = value;
    }

    /**
     * Gets the value of the addrChgDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddrChgDate() {
        return addrChgDate;
    }

    /**
     * Sets the value of the addrChgDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddrChgDate(String value) {
        this.addrChgDate = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the profitScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitScore() {
        return profitScore;
    }

    /**
     * Sets the value of the profitScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitScore(String value) {
        this.profitScore = value;
    }

    /**
     * Gets the value of the behavioralSegCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehavioralSegCode() {
        return behavioralSegCode;
    }

    /**
     * Sets the value of the behavioralSegCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehavioralSegCode(String value) {
        this.behavioralSegCode = value;
    }

}
