
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Row type defined for seat map
 * 
 * <p>Java class for RowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RowType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Seat" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="seat" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="1"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="seatAvailabilityInd" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="1"/>
 *                       &lt;enumeration value="A"/>
 *                       &lt;enumeration value="C"/>
 *                       &lt;enumeration value="N"/>
 *                       &lt;enumeration value="O"/>
 *                       &lt;enumeration value="S"/>
 *                       &lt;enumeration value=" "/>
 *                       &lt;enumeration value="0"/>
 *                       &lt;enumeration value="1"/>
 *                       &lt;enumeration value="2"/>
 *                       &lt;enumeration value="3"/>
 *                       &lt;enumeration value="4"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="isExitSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="isAisleSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="isWindowSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="isSmokingAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="aisleLocationInd">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="1"/>
 *                       &lt;whiteSpace value="preserve"/>
 *                       &lt;enumeration value="L"/>
 *                       &lt;enumeration value="R"/>
 *                       &lt;enumeration value=" "/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="isRearFacingSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="seatDepartureControlStatusCode">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="1"/>
 *                       &lt;whiteSpace value="preserve"/>
 *                       &lt;enumeration value=" "/>
 *                       &lt;enumeration value="N"/>
 *                       &lt;enumeration value="S"/>
 *                       &lt;enumeration value="C"/>
 *                       &lt;enumeration value="G"/>
 *                       &lt;enumeration value=":"/>
 *                       &lt;enumeration value="T"/>
 *                       &lt;enumeration value="."/>
 *                       &lt;enumeration value="Z"/>
 *                       &lt;enumeration value="W"/>
 *                       &lt;enumeration value="B"/>
 *                       &lt;enumeration value="*"/>
 *                       &lt;enumeration value="D"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="rowNbr" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;length value="2"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="cabinClassCode">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;length value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="isPremierZone" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isOverTheWing" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isExitRow" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isUpperDeckRow" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isEconomyPlus" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RowType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "seat"
})
public class RowType {

    @XmlElement(name = "Seat", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", required = true)
    protected List<RowType.Seat> seat;
    @XmlAttribute(name = "rowNbr", required = true)
    protected String rowNbr;
    @XmlAttribute(name = "cabinClassCode")
    protected String cabinClassCode;
    @XmlAttribute(name = "isPremierZone", required = true)
    protected boolean isPremierZone;
    @XmlAttribute(name = "isOverTheWing", required = true)
    protected boolean isOverTheWing;
    @XmlAttribute(name = "isExitRow", required = true)
    protected boolean isExitRow;
    @XmlAttribute(name = "isUpperDeckRow", required = true)
    protected boolean isUpperDeckRow;
    @XmlAttribute(name = "isEconomyPlus")
    protected Boolean isEconomyPlus;

    /**
     * Gets the value of the seat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RowType.Seat }
     * 
     * 
     */
    public List<RowType.Seat> getSeat() {
        if (seat == null) {
            seat = new ArrayList<RowType.Seat>();
        }
        return this.seat;
    }

    /**
     * Gets the value of the rowNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRowNbr() {
        return rowNbr;
    }

    /**
     * Sets the value of the rowNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRowNbr(String value) {
        this.rowNbr = value;
    }

    /**
     * Gets the value of the cabinClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinClassCode() {
        return cabinClassCode;
    }

    /**
     * Sets the value of the cabinClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinClassCode(String value) {
        this.cabinClassCode = value;
    }

    /**
     * Gets the value of the isPremierZone property.
     * 
     */
    public boolean isIsPremierZone() {
        return isPremierZone;
    }

    /**
     * Sets the value of the isPremierZone property.
     * 
     */
    public void setIsPremierZone(boolean value) {
        this.isPremierZone = value;
    }

    /**
     * Gets the value of the isOverTheWing property.
     * 
     */
    public boolean isIsOverTheWing() {
        return isOverTheWing;
    }

    /**
     * Sets the value of the isOverTheWing property.
     * 
     */
    public void setIsOverTheWing(boolean value) {
        this.isOverTheWing = value;
    }

    /**
     * Gets the value of the isExitRow property.
     * 
     */
    public boolean isIsExitRow() {
        return isExitRow;
    }

    /**
     * Sets the value of the isExitRow property.
     * 
     */
    public void setIsExitRow(boolean value) {
        this.isExitRow = value;
    }

    /**
     * Gets the value of the isUpperDeckRow property.
     * 
     */
    public boolean isIsUpperDeckRow() {
        return isUpperDeckRow;
    }

    /**
     * Sets the value of the isUpperDeckRow property.
     * 
     */
    public void setIsUpperDeckRow(boolean value) {
        this.isUpperDeckRow = value;
    }

    /**
     * Gets the value of the isEconomyPlus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsEconomyPlus() {
        return isEconomyPlus;
    }

    /**
     * Sets the value of the isEconomyPlus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEconomyPlus(Boolean value) {
        this.isEconomyPlus = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="seat" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="1"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="seatAvailabilityInd" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="1"/>
     *             &lt;enumeration value="A"/>
     *             &lt;enumeration value="C"/>
     *             &lt;enumeration value="N"/>
     *             &lt;enumeration value="O"/>
     *             &lt;enumeration value="S"/>
     *             &lt;enumeration value=" "/>
     *             &lt;enumeration value="0"/>
     *             &lt;enumeration value="1"/>
     *             &lt;enumeration value="2"/>
     *             &lt;enumeration value="3"/>
     *             &lt;enumeration value="4"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="isExitSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="isAisleSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="isWindowSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="isSmokingAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="aisleLocationInd">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="1"/>
     *             &lt;whiteSpace value="preserve"/>
     *             &lt;enumeration value="L"/>
     *             &lt;enumeration value="R"/>
     *             &lt;enumeration value=" "/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="isRearFacingSeat" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="seatDepartureControlStatusCode">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="1"/>
     *             &lt;whiteSpace value="preserve"/>
     *             &lt;enumeration value=" "/>
     *             &lt;enumeration value="N"/>
     *             &lt;enumeration value="S"/>
     *             &lt;enumeration value="C"/>
     *             &lt;enumeration value="G"/>
     *             &lt;enumeration value=":"/>
     *             &lt;enumeration value="T"/>
     *             &lt;enumeration value="."/>
     *             &lt;enumeration value="Z"/>
     *             &lt;enumeration value="W"/>
     *             &lt;enumeration value="B"/>
     *             &lt;enumeration value="*"/>
     *             &lt;enumeration value="D"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Seat {

        @XmlAttribute(name = "seat", required = true)
        protected String seat;
        @XmlAttribute(name = "seatAvailabilityInd", required = true)
        protected String seatAvailabilityInd;
        @XmlAttribute(name = "isExitSeat")
        protected Boolean isExitSeat;
        @XmlAttribute(name = "isAisleSeat")
        protected Boolean isAisleSeat;
        @XmlAttribute(name = "isWindowSeat")
        protected Boolean isWindowSeat;
        @XmlAttribute(name = "isSmokingAllowed")
        protected Boolean isSmokingAllowed;
        @XmlAttribute(name = "aisleLocationInd")
        protected String aisleLocationInd;
        @XmlAttribute(name = "isRearFacingSeat")
        protected Boolean isRearFacingSeat;
        @XmlAttribute(name = "seatDepartureControlStatusCode")
        protected String seatDepartureControlStatusCode;

        /**
         * Gets the value of the seat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeat() {
            return seat;
        }

        /**
         * Sets the value of the seat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeat(String value) {
            this.seat = value;
        }

        /**
         * Gets the value of the seatAvailabilityInd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeatAvailabilityInd() {
            return seatAvailabilityInd;
        }

        /**
         * Sets the value of the seatAvailabilityInd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeatAvailabilityInd(String value) {
            this.seatAvailabilityInd = value;
        }

        /**
         * Gets the value of the isExitSeat property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsExitSeat() {
            return isExitSeat;
        }

        /**
         * Sets the value of the isExitSeat property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsExitSeat(Boolean value) {
            this.isExitSeat = value;
        }

        /**
         * Gets the value of the isAisleSeat property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsAisleSeat() {
            return isAisleSeat;
        }

        /**
         * Sets the value of the isAisleSeat property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsAisleSeat(Boolean value) {
            this.isAisleSeat = value;
        }

        /**
         * Gets the value of the isWindowSeat property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsWindowSeat() {
            return isWindowSeat;
        }

        /**
         * Sets the value of the isWindowSeat property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsWindowSeat(Boolean value) {
            this.isWindowSeat = value;
        }

        /**
         * Gets the value of the isSmokingAllowed property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsSmokingAllowed() {
            return isSmokingAllowed;
        }

        /**
         * Sets the value of the isSmokingAllowed property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsSmokingAllowed(Boolean value) {
            this.isSmokingAllowed = value;
        }

        /**
         * Gets the value of the aisleLocationInd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAisleLocationInd() {
            return aisleLocationInd;
        }

        /**
         * Sets the value of the aisleLocationInd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAisleLocationInd(String value) {
            this.aisleLocationInd = value;
        }

        /**
         * Gets the value of the isRearFacingSeat property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsRearFacingSeat() {
            return isRearFacingSeat;
        }

        /**
         * Sets the value of the isRearFacingSeat property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsRearFacingSeat(Boolean value) {
            this.isRearFacingSeat = value;
        }

        /**
         * Gets the value of the seatDepartureControlStatusCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeatDepartureControlStatusCode() {
            return seatDepartureControlStatusCode;
        }

        /**
         * Sets the value of the seatDepartureControlStatusCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeatDepartureControlStatusCode(String value) {
            this.seatDepartureControlStatusCode = value;
        }

    }

}
