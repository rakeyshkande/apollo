
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClubMembershipType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClubMembershipType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="unitedClubMember" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="silverWingsMember" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClubMembershipType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class ClubMembershipType {

    @XmlAttribute(name = "unitedClubMember")
    protected String unitedClubMember;
    @XmlAttribute(name = "silverWingsMember")
    protected String silverWingsMember;

    /**
     * Gets the value of the unitedClubMember property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitedClubMember() {
        return unitedClubMember;
    }

    /**
     * Sets the value of the unitedClubMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitedClubMember(String value) {
        this.unitedClubMember = value;
    }

    /**
     * Gets the value of the silverWingsMember property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSilverWingsMember() {
        return silverWingsMember;
    }

    /**
     * Sets the value of the silverWingsMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSilverWingsMember(String value) {
        this.silverWingsMember = value;
    }

}
