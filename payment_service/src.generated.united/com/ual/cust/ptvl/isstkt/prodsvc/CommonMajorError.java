
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommonMajorError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonMajorError">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="majorErrorCode" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonMajorError", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class CommonMajorError {

    @XmlAttribute(name = "majorErrorCode", required = true)
    protected int majorErrorCode;

    /**
     * Gets the value of the majorErrorCode property.
     * 
     */
    public int getMajorErrorCode() {
        return majorErrorCode;
    }

    /**
     * Sets the value of the majorErrorCode property.
     * 
     */
    public void setMajorErrorCode(int value) {
        this.majorErrorCode = value;
    }

}
