
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AffiliationProfileType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AffiliationProfileType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClubMembership" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ClubMembershipType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AffiliationProfileType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "clubMembership"
})
public class AffiliationProfileType {

    @XmlElement(name = "ClubMembership", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<ClubMembershipType> clubMembership;

    /**
     * Gets the value of the clubMembership property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the clubMembership property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClubMembership().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClubMembershipType }
     * 
     * 
     */
    public List<ClubMembershipType> getClubMembership() {
        if (clubMembership == null) {
            clubMembership = new ArrayList<ClubMembershipType>();
        }
        return this.clubMembership;
    }

}
