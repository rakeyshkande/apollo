
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MemberSummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MemberSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="highYieldMilesBalance" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="choiceMilesBalance" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="newBalance" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="currentBalanceExpiry" type="{http://www.w3.org/2001/XMLSchema}date" />
 *       &lt;attribute name="ptdFlightMiles" type="{http://www.w3.org/2001/XMLSchema}double" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberSummaryType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class MemberSummaryType {

    @XmlAttribute(name = "highYieldMilesBalance")
    protected Double highYieldMilesBalance;
    @XmlAttribute(name = "choiceMilesBalance")
    protected Double choiceMilesBalance;
    @XmlAttribute(name = "newBalance")
    protected Double newBalance;
    @XmlAttribute(name = "currentBalanceExpiry")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar currentBalanceExpiry;
    @XmlAttribute(name = "ptdFlightMiles")
    protected Double ptdFlightMiles;

    /**
     * Gets the value of the highYieldMilesBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getHighYieldMilesBalance() {
        return highYieldMilesBalance;
    }

    /**
     * Sets the value of the highYieldMilesBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setHighYieldMilesBalance(Double value) {
        this.highYieldMilesBalance = value;
    }

    /**
     * Gets the value of the choiceMilesBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getChoiceMilesBalance() {
        return choiceMilesBalance;
    }

    /**
     * Sets the value of the choiceMilesBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setChoiceMilesBalance(Double value) {
        this.choiceMilesBalance = value;
    }

    /**
     * Gets the value of the newBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getNewBalance() {
        return newBalance;
    }

    /**
     * Sets the value of the newBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setNewBalance(Double value) {
        this.newBalance = value;
    }

    /**
     * Gets the value of the currentBalanceExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCurrentBalanceExpiry() {
        return currentBalanceExpiry;
    }

    /**
     * Sets the value of the currentBalanceExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCurrentBalanceExpiry(XMLGregorianCalendar value) {
        this.currentBalanceExpiry = value;
    }

    /**
     * Gets the value of the ptdFlightMiles property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPtdFlightMiles() {
        return ptdFlightMiles;
    }

    /**
     * Sets the value of the ptdFlightMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPtdFlightMiles(Double value) {
        this.ptdFlightMiles = value;
    }

}
