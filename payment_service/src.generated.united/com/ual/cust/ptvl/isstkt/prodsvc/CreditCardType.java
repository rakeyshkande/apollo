
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditCardClub" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}CreditCardClubType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="cardIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "creditCardClub"
})
public class CreditCardType {

    @XmlElement(name = "CreditCardClub", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<CreditCardClubType> creditCardClub;
    @XmlAttribute(name = "cardIndicator")
    protected String cardIndicator;

    /**
     * Gets the value of the creditCardClub property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditCardClub property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditCardClub().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreditCardClubType }
     * 
     * 
     */
    public List<CreditCardClubType> getCreditCardClub() {
        if (creditCardClub == null) {
            creditCardClub = new ArrayList<CreditCardClubType>();
        }
        return this.creditCardClub;
    }

    /**
     * Gets the value of the cardIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardIndicator() {
        return cardIndicator;
    }

    /**
     * Sets the value of the cardIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardIndicator(String value) {
        this.cardIndicator = value;
    }

}
