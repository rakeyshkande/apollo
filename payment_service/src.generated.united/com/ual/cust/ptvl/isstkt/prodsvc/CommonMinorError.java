
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommonMinorError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonMinorError">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="minorErrorCode" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="minorErrorType" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;minLength value="1"/>
 *             &lt;maxLength value="3"/>
 *             &lt;enumeration value="E"/>
 *             &lt;enumeration value="W"/>
 *             &lt;enumeration value="I"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="minorErrorText" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;minLength value="1"/>
 *             &lt;maxLength value="200"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonMinorError", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class CommonMinorError {

    @XmlAttribute(name = "minorErrorCode", required = true)
    protected int minorErrorCode;
    @XmlAttribute(name = "minorErrorType", required = true)
    protected String minorErrorType;
    @XmlAttribute(name = "minorErrorText", required = true)
    protected String minorErrorText;

    /**
     * Gets the value of the minorErrorCode property.
     * 
     */
    public int getMinorErrorCode() {
        return minorErrorCode;
    }

    /**
     * Sets the value of the minorErrorCode property.
     * 
     */
    public void setMinorErrorCode(int value) {
        this.minorErrorCode = value;
    }

    /**
     * Gets the value of the minorErrorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinorErrorType() {
        return minorErrorType;
    }

    /**
     * Sets the value of the minorErrorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinorErrorType(String value) {
        this.minorErrorType = value;
    }

    /**
     * Gets the value of the minorErrorText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinorErrorText() {
        return minorErrorText;
    }

    /**
     * Sets the value of the minorErrorText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinorErrorText(String value) {
        this.minorErrorText = value;
    }

}
