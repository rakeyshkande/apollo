
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ual.cust.ptvl.isstkt.prodsvc package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ual.cust.ptvl.isstkt.prodsvc
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType }
     * 
     */
    public PartnerUpdateMilesType createPartnerUpdateMilesType() {
        return new PartnerUpdateMilesType();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesResponseType }
     * 
     */
    public PartnerUpdateMilesResponseType createPartnerUpdateMilesResponseType() {
        return new PartnerUpdateMilesResponseType();
    }

    /**
     * Create an instance of {@link TravelType }
     * 
     */
    public TravelType createTravelType() {
        return new TravelType();
    }

    /**
     * Create an instance of {@link AgentType }
     * 
     */
    public AgentType createAgentType() {
        return new AgentType();
    }

    /**
     * Create an instance of {@link PricingType }
     * 
     */
    public PricingType createPricingType() {
        return new PricingType();
    }

    /**
     * Create an instance of {@link RowType }
     * 
     */
    public RowType createRowType() {
        return new RowType();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesResponseType.Confirmation }
     * 
     */
    public PartnerUpdateMilesResponseType.Confirmation createPartnerUpdateMilesResponseTypeConfirmation() {
        return new PartnerUpdateMilesResponseType.Confirmation();
    }

    /**
     * Create an instance of {@link FirstLegType }
     * 
     */
    public FirstLegType createFirstLegType() {
        return new FirstLegType();
    }

    /**
     * Create an instance of {@link ItineraryNbrType }
     * 
     */
    public ItineraryNbrType createItineraryNbrType() {
        return new ItineraryNbrType();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesRequest }
     * 
     */
    public PartnerUpdateMilesRequest createPartnerUpdateMilesRequest() {
        return new PartnerUpdateMilesRequest();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.LoyaltyProgram }
     * 
     */
    public PartnerUpdateMilesType.LoyaltyProgram createPartnerUpdateMilesTypeLoyaltyProgram() {
        return new PartnerUpdateMilesType.LoyaltyProgram();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.Passenger }
     * 
     */
    public PartnerUpdateMilesType.Passenger createPartnerUpdateMilesTypePassenger() {
        return new PartnerUpdateMilesType.Passenger();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.Travel }
     * 
     */
    public PartnerUpdateMilesType.Travel createPartnerUpdateMilesTypeTravel() {
        return new PartnerUpdateMilesType.Travel();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.Redemption }
     * 
     */
    public PartnerUpdateMilesType.Redemption createPartnerUpdateMilesTypeRedemption() {
        return new PartnerUpdateMilesType.Redemption();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.Pricing }
     * 
     */
    public PartnerUpdateMilesType.Pricing createPartnerUpdateMilesTypePricing() {
        return new PartnerUpdateMilesType.Pricing();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.NumberOfItem }
     * 
     */
    public PartnerUpdateMilesType.NumberOfItem createPartnerUpdateMilesTypeNumberOfItem() {
        return new PartnerUpdateMilesType.NumberOfItem();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesType.Booking }
     * 
     */
    public PartnerUpdateMilesType.Booking createPartnerUpdateMilesTypeBooking() {
        return new PartnerUpdateMilesType.Booking();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesResponse }
     * 
     */
    public PartnerUpdateMilesResponse createPartnerUpdateMilesResponse() {
        return new PartnerUpdateMilesResponse();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link CommonMinorError }
     * 
     */
    public CommonMinorError createCommonMinorError() {
        return new CommonMinorError();
    }

    /**
     * Create an instance of {@link CommonErrorDetailsType }
     * 
     */
    public CommonErrorDetailsType createCommonErrorDetailsType() {
        return new CommonErrorDetailsType();
    }

    /**
     * Create an instance of {@link CommonMajorError }
     * 
     */
    public CommonMajorError createCommonMajorError() {
        return new CommonMajorError();
    }

    /**
     * Create an instance of {@link PassengerReferenceType }
     * 
     */
    public PassengerReferenceType createPassengerReferenceType() {
        return new PassengerReferenceType();
    }

    /**
     * Create an instance of {@link TravelType.FirstTravelItem }
     * 
     */
    public TravelType.FirstTravelItem createTravelTypeFirstTravelItem() {
        return new TravelType.FirstTravelItem();
    }

    /**
     * Create an instance of {@link AgentType.Agency }
     * 
     */
    public AgentType.Agency createAgentTypeAgency() {
        return new AgentType.Agency();
    }

    /**
     * Create an instance of {@link PricingType.OrderData }
     * 
     */
    public PricingType.OrderData createPricingTypeOrderData() {
        return new PricingType.OrderData();
    }

    /**
     * Create an instance of {@link PricingType.TaxInfo }
     * 
     */
    public PricingType.TaxInfo createPricingTypeTaxInfo() {
        return new PricingType.TaxInfo();
    }

    /**
     * Create an instance of {@link RowType.Seat }
     * 
     */
    public RowType.Seat createRowTypeSeat() {
        return new RowType.Seat();
    }

    /**
     * Create an instance of {@link PartnerUpdateMilesResponseType.Confirmation.BalanceInfo }
     * 
     */
    public PartnerUpdateMilesResponseType.Confirmation.BalanceInfo createPartnerUpdateMilesResponseTypeConfirmationBalanceInfo() {
        return new PartnerUpdateMilesResponseType.Confirmation.BalanceInfo();
    }

    /**
     * Create an instance of {@link FirstLegType.Origin }
     * 
     */
    public FirstLegType.Origin createFirstLegTypeOrigin() {
        return new FirstLegType.Origin();
    }

    /**
     * Create an instance of {@link FirstLegType.Destination }
     * 
     */
    public FirstLegType.Destination createFirstLegTypeDestination() {
        return new FirstLegType.Destination();
    }

    /**
     * Create an instance of {@link ItineraryNbrType.PNR }
     * 
     */
    public ItineraryNbrType.PNR createItineraryNbrTypePNR() {
        return new ItineraryNbrType.PNR();
    }

    /**
     * Create an instance of {@link ItineraryNbrType.Reference }
     * 
     */
    public ItineraryNbrType.Reference createItineraryNbrTypeReference() {
        return new ItineraryNbrType.Reference();
    }

}
