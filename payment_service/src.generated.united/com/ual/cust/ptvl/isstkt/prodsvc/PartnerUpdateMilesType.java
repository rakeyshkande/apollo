
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PartnerUpdateMilesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartnerUpdateMilesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoyaltyProgram">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="loyaltyProgramNbr" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="11"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Passenger" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="leadPassengerFirstName">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="50"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="leadPassengerLastName">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="50"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItineraryNbr" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ItineraryNbrType" minOccurs="0"/>
 *         &lt;element name="FirstLeg" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}FirstLegType" minOccurs="0"/>
 *         &lt;element name="Travel" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}TravelType">
 *                 &lt;attribute name="bonusCode">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="3"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="bonusType">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="3"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="propertyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="partnerCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Redemption" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="totalPoints" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="earnDate" type="{http://www.w3.org/2001/XMLSchema}date" />
 *                 &lt;attribute name="debitCreditInd">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;length value="1"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Pricing" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}PricingType">
 *                 &lt;attribute name="systemCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="posSystemConversionRate" type="{http://www.w3.org/2001/XMLSchema}float" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NumberOfItem" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="nbrOfTickets" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="nbrOfRewards" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Booking" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Agent" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AgentType" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="bookingChannel">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="20"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerUpdateMilesType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "loyaltyProgram",
    "passenger",
    "itineraryNbr",
    "firstLeg",
    "travel",
    "redemption",
    "pricing",
    "numberOfItem",
    "booking"
})
@XmlSeeAlso({
    PartnerUpdateMilesRequest.class
})
public class PartnerUpdateMilesType {

    @XmlElement(name = "LoyaltyProgram", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", required = true)
    protected PartnerUpdateMilesType.LoyaltyProgram loyaltyProgram;
    @XmlElement(name = "Passenger", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PartnerUpdateMilesType.Passenger passenger;
    @XmlElement(name = "ItineraryNbr", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected ItineraryNbrType itineraryNbr;
    @XmlElement(name = "FirstLeg", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected FirstLegType firstLeg;
    @XmlElement(name = "Travel", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PartnerUpdateMilesType.Travel travel;
    @XmlElement(name = "Redemption", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PartnerUpdateMilesType.Redemption redemption;
    @XmlElement(name = "Pricing", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PartnerUpdateMilesType.Pricing pricing;
    @XmlElement(name = "NumberOfItem", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PartnerUpdateMilesType.NumberOfItem numberOfItem;
    @XmlElement(name = "Booking", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PartnerUpdateMilesType.Booking booking;

    /**
     * Gets the value of the loyaltyProgram property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.LoyaltyProgram }
     *     
     */
    public PartnerUpdateMilesType.LoyaltyProgram getLoyaltyProgram() {
        return loyaltyProgram;
    }

    /**
     * Sets the value of the loyaltyProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.LoyaltyProgram }
     *     
     */
    public void setLoyaltyProgram(PartnerUpdateMilesType.LoyaltyProgram value) {
        this.loyaltyProgram = value;
    }

    /**
     * Gets the value of the passenger property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.Passenger }
     *     
     */
    public PartnerUpdateMilesType.Passenger getPassenger() {
        return passenger;
    }

    /**
     * Sets the value of the passenger property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.Passenger }
     *     
     */
    public void setPassenger(PartnerUpdateMilesType.Passenger value) {
        this.passenger = value;
    }

    /**
     * Gets the value of the itineraryNbr property.
     * 
     * @return
     *     possible object is
     *     {@link ItineraryNbrType }
     *     
     */
    public ItineraryNbrType getItineraryNbr() {
        return itineraryNbr;
    }

    /**
     * Sets the value of the itineraryNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItineraryNbrType }
     *     
     */
    public void setItineraryNbr(ItineraryNbrType value) {
        this.itineraryNbr = value;
    }

    /**
     * Gets the value of the firstLeg property.
     * 
     * @return
     *     possible object is
     *     {@link FirstLegType }
     *     
     */
    public FirstLegType getFirstLeg() {
        return firstLeg;
    }

    /**
     * Sets the value of the firstLeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link FirstLegType }
     *     
     */
    public void setFirstLeg(FirstLegType value) {
        this.firstLeg = value;
    }

    /**
     * Gets the value of the travel property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.Travel }
     *     
     */
    public PartnerUpdateMilesType.Travel getTravel() {
        return travel;
    }

    /**
     * Sets the value of the travel property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.Travel }
     *     
     */
    public void setTravel(PartnerUpdateMilesType.Travel value) {
        this.travel = value;
    }

    /**
     * Gets the value of the redemption property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.Redemption }
     *     
     */
    public PartnerUpdateMilesType.Redemption getRedemption() {
        return redemption;
    }

    /**
     * Sets the value of the redemption property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.Redemption }
     *     
     */
    public void setRedemption(PartnerUpdateMilesType.Redemption value) {
        this.redemption = value;
    }

    /**
     * Gets the value of the pricing property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.Pricing }
     *     
     */
    public PartnerUpdateMilesType.Pricing getPricing() {
        return pricing;
    }

    /**
     * Sets the value of the pricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.Pricing }
     *     
     */
    public void setPricing(PartnerUpdateMilesType.Pricing value) {
        this.pricing = value;
    }

    /**
     * Gets the value of the numberOfItem property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.NumberOfItem }
     *     
     */
    public PartnerUpdateMilesType.NumberOfItem getNumberOfItem() {
        return numberOfItem;
    }

    /**
     * Sets the value of the numberOfItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.NumberOfItem }
     *     
     */
    public void setNumberOfItem(PartnerUpdateMilesType.NumberOfItem value) {
        this.numberOfItem = value;
    }

    /**
     * Gets the value of the booking property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesType.Booking }
     *     
     */
    public PartnerUpdateMilesType.Booking getBooking() {
        return booking;
    }

    /**
     * Sets the value of the booking property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesType.Booking }
     *     
     */
    public void setBooking(PartnerUpdateMilesType.Booking value) {
        this.booking = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Agent" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AgentType" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="bookingChannel">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="20"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "agent"
    })
    public static class Booking {

        @XmlElement(name = "Agent", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
        protected AgentType agent;
        @XmlAttribute(name = "bookingChannel")
        protected String bookingChannel;

        /**
         * Gets the value of the agent property.
         * 
         * @return
         *     possible object is
         *     {@link AgentType }
         *     
         */
        public AgentType getAgent() {
            return agent;
        }

        /**
         * Sets the value of the agent property.
         * 
         * @param value
         *     allowed object is
         *     {@link AgentType }
         *     
         */
        public void setAgent(AgentType value) {
            this.agent = value;
        }

        /**
         * Gets the value of the bookingChannel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBookingChannel() {
            return bookingChannel;
        }

        /**
         * Sets the value of the bookingChannel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBookingChannel(String value) {
            this.bookingChannel = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="loyaltyProgramNbr" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="11"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class LoyaltyProgram {

        @XmlAttribute(name = "loyaltyProgramNbr", required = true)
        protected String loyaltyProgramNbr;

        /**
         * Gets the value of the loyaltyProgramNbr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyaltyProgramNbr() {
            return loyaltyProgramNbr;
        }

        /**
         * Sets the value of the loyaltyProgramNbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyaltyProgramNbr(String value) {
            this.loyaltyProgramNbr = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="nbrOfTickets" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="nbrOfRewards" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class NumberOfItem {

        @XmlAttribute(name = "nbrOfTickets")
        protected Integer nbrOfTickets;
        @XmlAttribute(name = "nbrOfRewards")
        protected Integer nbrOfRewards;

        /**
         * Gets the value of the nbrOfTickets property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNbrOfTickets() {
            return nbrOfTickets;
        }

        /**
         * Sets the value of the nbrOfTickets property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNbrOfTickets(Integer value) {
            this.nbrOfTickets = value;
        }

        /**
         * Gets the value of the nbrOfRewards property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNbrOfRewards() {
            return nbrOfRewards;
        }

        /**
         * Sets the value of the nbrOfRewards property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNbrOfRewards(Integer value) {
            this.nbrOfRewards = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="leadPassengerFirstName">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="50"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="leadPassengerLastName">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="50"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Passenger {

        @XmlAttribute(name = "leadPassengerFirstName")
        protected String leadPassengerFirstName;
        @XmlAttribute(name = "leadPassengerLastName")
        protected String leadPassengerLastName;

        /**
         * Gets the value of the leadPassengerFirstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLeadPassengerFirstName() {
            return leadPassengerFirstName;
        }

        /**
         * Sets the value of the leadPassengerFirstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLeadPassengerFirstName(String value) {
            this.leadPassengerFirstName = value;
        }

        /**
         * Gets the value of the leadPassengerLastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLeadPassengerLastName() {
            return leadPassengerLastName;
        }

        /**
         * Sets the value of the leadPassengerLastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLeadPassengerLastName(String value) {
            this.leadPassengerLastName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}PricingType">
     *       &lt;attribute name="systemCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="posSystemConversionRate" type="{http://www.w3.org/2001/XMLSchema}float" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Pricing
        extends PricingType
    {

        @XmlAttribute(name = "systemCurrencyCode")
        protected String systemCurrencyCode;
        @XmlAttribute(name = "posSystemConversionRate")
        protected Float posSystemConversionRate;

        /**
         * Gets the value of the systemCurrencyCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSystemCurrencyCode() {
            return systemCurrencyCode;
        }

        /**
         * Sets the value of the systemCurrencyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSystemCurrencyCode(String value) {
            this.systemCurrencyCode = value;
        }

        /**
         * Gets the value of the posSystemConversionRate property.
         * 
         * @return
         *     possible object is
         *     {@link Float }
         *     
         */
        public Float getPosSystemConversionRate() {
            return posSystemConversionRate;
        }

        /**
         * Sets the value of the posSystemConversionRate property.
         * 
         * @param value
         *     allowed object is
         *     {@link Float }
         *     
         */
        public void setPosSystemConversionRate(Float value) {
            this.posSystemConversionRate = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="totalPoints" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="earnDate" type="{http://www.w3.org/2001/XMLSchema}date" />
     *       &lt;attribute name="debitCreditInd">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="1"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Redemption {

        @XmlAttribute(name = "totalPoints")
        protected Integer totalPoints;
        @XmlAttribute(name = "earnDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar earnDate;
        @XmlAttribute(name = "debitCreditInd")
        protected String debitCreditInd;

        /**
         * Gets the value of the totalPoints property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTotalPoints() {
            return totalPoints;
        }

        /**
         * Sets the value of the totalPoints property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTotalPoints(Integer value) {
            this.totalPoints = value;
        }

        /**
         * Gets the value of the earnDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEarnDate() {
            return earnDate;
        }

        /**
         * Sets the value of the earnDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEarnDate(XMLGregorianCalendar value) {
            this.earnDate = value;
        }

        /**
         * Gets the value of the debitCreditInd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDebitCreditInd() {
            return debitCreditInd;
        }

        /**
         * Sets the value of the debitCreditInd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDebitCreditInd(String value) {
            this.debitCreditInd = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}TravelType">
     *       &lt;attribute name="bonusCode">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="3"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="bonusType">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;length value="3"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="propertyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="partnerCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Travel
        extends TravelType
    {

        @XmlAttribute(name = "bonusCode")
        protected String bonusCode;
        @XmlAttribute(name = "bonusType")
        protected String bonusType;
        @XmlAttribute(name = "propertyCode")
        protected String propertyCode;
        @XmlAttribute(name = "partnerCode")
        protected String partnerCode;

        /**
         * Gets the value of the bonusCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBonusCode() {
            return bonusCode;
        }

        /**
         * Sets the value of the bonusCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBonusCode(String value) {
            this.bonusCode = value;
        }

        /**
         * Gets the value of the bonusType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBonusType() {
            return bonusType;
        }

        /**
         * Sets the value of the bonusType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBonusType(String value) {
            this.bonusType = value;
        }

        /**
         * Gets the value of the propertyCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPropertyCode() {
            return propertyCode;
        }

        /**
         * Sets the value of the propertyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPropertyCode(String value) {
            this.propertyCode = value;
        }

        /**
         * Gets the value of the partnerCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartnerCode() {
            return partnerCode;
        }

        /**
         * Sets the value of the partnerCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartnerCode(String value) {
            this.partnerCode = value;
        }

    }

}
