
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartnerUpdateMilesResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartnerUpdateMilesResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Confirmation">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BalanceInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="accountBalance" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                           &lt;attribute name="choiceBalance" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="confirmationNbr" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="20"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="loyaltyProgramNbr" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="11"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="postedAccountNbr">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="11"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerUpdateMilesResponseType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "confirmation"
})
@XmlSeeAlso({
    PartnerUpdateMilesResponse.class
})
public class PartnerUpdateMilesResponseType {

    @XmlElement(name = "Confirmation", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", required = true)
    protected PartnerUpdateMilesResponseType.Confirmation confirmation;

    /**
     * Gets the value of the confirmation property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerUpdateMilesResponseType.Confirmation }
     *     
     */
    public PartnerUpdateMilesResponseType.Confirmation getConfirmation() {
        return confirmation;
    }

    /**
     * Sets the value of the confirmation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerUpdateMilesResponseType.Confirmation }
     *     
     */
    public void setConfirmation(PartnerUpdateMilesResponseType.Confirmation value) {
        this.confirmation = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BalanceInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="accountBalance" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                 &lt;attribute name="choiceBalance" type="{http://www.w3.org/2001/XMLSchema}int" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="confirmationNbr" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="20"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="loyaltyProgramNbr" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="11"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="postedAccountNbr">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="11"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "balanceInfo"
    })
    public static class Confirmation {

        @XmlElement(name = "BalanceInfo", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
        protected PartnerUpdateMilesResponseType.Confirmation.BalanceInfo balanceInfo;
        @XmlAttribute(name = "confirmationNbr", required = true)
        protected String confirmationNbr;
        @XmlAttribute(name = "loyaltyProgramNbr", required = true)
        protected String loyaltyProgramNbr;
        @XmlAttribute(name = "postedAccountNbr")
        protected String postedAccountNbr;

        /**
         * Gets the value of the balanceInfo property.
         * 
         * @return
         *     possible object is
         *     {@link PartnerUpdateMilesResponseType.Confirmation.BalanceInfo }
         *     
         */
        public PartnerUpdateMilesResponseType.Confirmation.BalanceInfo getBalanceInfo() {
            return balanceInfo;
        }

        /**
         * Sets the value of the balanceInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link PartnerUpdateMilesResponseType.Confirmation.BalanceInfo }
         *     
         */
        public void setBalanceInfo(PartnerUpdateMilesResponseType.Confirmation.BalanceInfo value) {
            this.balanceInfo = value;
        }

        /**
         * Gets the value of the confirmationNbr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmationNbr() {
            return confirmationNbr;
        }

        /**
         * Sets the value of the confirmationNbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmationNbr(String value) {
            this.confirmationNbr = value;
        }

        /**
         * Gets the value of the loyaltyProgramNbr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyaltyProgramNbr() {
            return loyaltyProgramNbr;
        }

        /**
         * Sets the value of the loyaltyProgramNbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyaltyProgramNbr(String value) {
            this.loyaltyProgramNbr = value;
        }

        /**
         * Gets the value of the postedAccountNbr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostedAccountNbr() {
            return postedAccountNbr;
        }

        /**
         * Sets the value of the postedAccountNbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostedAccountNbr(String value) {
            this.postedAccountNbr = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="accountBalance" type="{http://www.w3.org/2001/XMLSchema}int" />
         *       &lt;attribute name="choiceBalance" type="{http://www.w3.org/2001/XMLSchema}int" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class BalanceInfo {

            @XmlAttribute(name = "accountBalance")
            protected Integer accountBalance;
            @XmlAttribute(name = "choiceBalance")
            protected Integer choiceBalance;

            /**
             * Gets the value of the accountBalance property.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getAccountBalance() {
                return accountBalance;
            }

            /**
             * Sets the value of the accountBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setAccountBalance(Integer value) {
                this.accountBalance = value;
            }

            /**
             * Gets the value of the choiceBalance property.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getChoiceBalance() {
                return choiceBalance;
            }

            /**
             * Sets the value of the choiceBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setChoiceBalance(Integer value) {
                this.choiceBalance = value;
            }

        }

    }

}
