
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderData" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="totalPoints" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="totalCashAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="nonRedeemCashtAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="custPaymentAmount">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="8"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="bookingFeeAmount">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="10"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="mpFeeAmount">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="10"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="currencyCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TaxInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="taxAmount">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="10"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricingType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "orderData",
    "taxInfo"
})
@XmlSeeAlso({
    com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Pricing.class
})
public class PricingType {

    @XmlElement(name = "OrderData", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PricingType.OrderData orderData;
    @XmlElement(name = "TaxInfo", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected PricingType.TaxInfo taxInfo;

    /**
     * Gets the value of the orderData property.
     * 
     * @return
     *     possible object is
     *     {@link PricingType.OrderData }
     *     
     */
    public PricingType.OrderData getOrderData() {
        return orderData;
    }

    /**
     * Sets the value of the orderData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingType.OrderData }
     *     
     */
    public void setOrderData(PricingType.OrderData value) {
        this.orderData = value;
    }

    /**
     * Gets the value of the taxInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PricingType.TaxInfo }
     *     
     */
    public PricingType.TaxInfo getTaxInfo() {
        return taxInfo;
    }

    /**
     * Sets the value of the taxInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingType.TaxInfo }
     *     
     */
    public void setTaxInfo(PricingType.TaxInfo value) {
        this.taxInfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="totalPoints" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="totalCashAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="nonRedeemCashtAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="custPaymentAmount">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="8"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="bookingFeeAmount">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="10"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="mpFeeAmount">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="10"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="currencyCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OrderData {

        @XmlAttribute(name = "totalPoints")
        protected String totalPoints;
        @XmlAttribute(name = "totalCashAmount")
        protected String totalCashAmount;
        @XmlAttribute(name = "nonRedeemCashtAmount")
        protected String nonRedeemCashtAmount;
        @XmlAttribute(name = "custPaymentAmount")
        protected String custPaymentAmount;
        @XmlAttribute(name = "bookingFeeAmount")
        protected String bookingFeeAmount;
        @XmlAttribute(name = "mpFeeAmount")
        protected String mpFeeAmount;
        @XmlAttribute(name = "currencyCode", required = true)
        protected String currencyCode;

        /**
         * Gets the value of the totalPoints property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalPoints() {
            return totalPoints;
        }

        /**
         * Sets the value of the totalPoints property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalPoints(String value) {
            this.totalPoints = value;
        }

        /**
         * Gets the value of the totalCashAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalCashAmount() {
            return totalCashAmount;
        }

        /**
         * Sets the value of the totalCashAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalCashAmount(String value) {
            this.totalCashAmount = value;
        }

        /**
         * Gets the value of the nonRedeemCashtAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNonRedeemCashtAmount() {
            return nonRedeemCashtAmount;
        }

        /**
         * Sets the value of the nonRedeemCashtAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNonRedeemCashtAmount(String value) {
            this.nonRedeemCashtAmount = value;
        }

        /**
         * Gets the value of the custPaymentAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustPaymentAmount() {
            return custPaymentAmount;
        }

        /**
         * Sets the value of the custPaymentAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustPaymentAmount(String value) {
            this.custPaymentAmount = value;
        }

        /**
         * Gets the value of the bookingFeeAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBookingFeeAmount() {
            return bookingFeeAmount;
        }

        /**
         * Sets the value of the bookingFeeAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBookingFeeAmount(String value) {
            this.bookingFeeAmount = value;
        }

        /**
         * Gets the value of the mpFeeAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMpFeeAmount() {
            return mpFeeAmount;
        }

        /**
         * Sets the value of the mpFeeAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMpFeeAmount(String value) {
            this.mpFeeAmount = value;
        }

        /**
         * Gets the value of the currencyCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Sets the value of the currencyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="taxAmount">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="10"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TaxInfo {

        @XmlAttribute(name = "taxAmount")
        protected String taxAmount;

        /**
         * Gets the value of the taxAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxAmount() {
            return taxAmount;
        }

        /**
         * Sets the value of the taxAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxAmount(String value) {
            this.taxAmount = value;
        }

    }

}
