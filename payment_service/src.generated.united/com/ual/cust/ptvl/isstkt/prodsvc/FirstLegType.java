
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FirstLegType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FirstLegType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Origin" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="originLocation" use="required" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AirportCode" />
 *                 &lt;attribute name="departureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Destination" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="destinationLocation" use="required" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AirportCode" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FirstLegType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "origin",
    "destination"
})
public class FirstLegType {

    @XmlElement(name = "Origin", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected FirstLegType.Origin origin;
    @XmlElement(name = "Destination", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected FirstLegType.Destination destination;

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link FirstLegType.Origin }
     *     
     */
    public FirstLegType.Origin getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link FirstLegType.Origin }
     *     
     */
    public void setOrigin(FirstLegType.Origin value) {
        this.origin = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link FirstLegType.Destination }
     *     
     */
    public FirstLegType.Destination getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link FirstLegType.Destination }
     *     
     */
    public void setDestination(FirstLegType.Destination value) {
        this.destination = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="destinationLocation" use="required" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AirportCode" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Destination {

        @XmlAttribute(name = "destinationLocation", required = true)
        protected String destinationLocation;

        /**
         * Gets the value of the destinationLocation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationLocation() {
            return destinationLocation;
        }

        /**
         * Sets the value of the destinationLocation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationLocation(String value) {
            this.destinationLocation = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="originLocation" use="required" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AirportCode" />
     *       &lt;attribute name="departureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Origin {

        @XmlAttribute(name = "originLocation", required = true)
        protected String originLocation;
        @XmlAttribute(name = "departureDate", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar departureDate;

        /**
         * Gets the value of the originLocation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginLocation() {
            return originLocation;
        }

        /**
         * Sets the value of the originLocation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginLocation(String value) {
            this.originLocation = value;
        }

        /**
         * Gets the value of the departureDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDepartureDate() {
            return departureDate;
        }

        /**
         * Sets the value of the departureDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDepartureDate(XMLGregorianCalendar value) {
            this.departureDate = value;
        }

    }

}
