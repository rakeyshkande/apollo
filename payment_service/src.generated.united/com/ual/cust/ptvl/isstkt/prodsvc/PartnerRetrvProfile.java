
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProgramMembership">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="memberID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}CommonVersion" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "programMembership"
})
@XmlRootElement(name = "PartnerRetrvProfile", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class PartnerRetrvProfile {

    @XmlElement(name = "ProgramMembership", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", required = true)
    protected PartnerRetrvProfile.ProgramMembership programMembership;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    /**
     * Gets the value of the programMembership property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerRetrvProfile.ProgramMembership }
     *     
     */
    public PartnerRetrvProfile.ProgramMembership getProgramMembership() {
        return programMembership;
    }

    /**
     * Sets the value of the programMembership property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerRetrvProfile.ProgramMembership }
     *     
     */
    public void setProgramMembership(PartnerRetrvProfile.ProgramMembership value) {
        this.programMembership = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="memberID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProgramMembership {

        @XmlAttribute(name = "memberID", required = true)
        protected String memberID;

        /**
         * Gets the value of the memberID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMemberID() {
            return memberID;
        }

        /**
         * Sets the value of the memberID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMemberID(String value) {
            this.memberID = value;
        }

    }

}
