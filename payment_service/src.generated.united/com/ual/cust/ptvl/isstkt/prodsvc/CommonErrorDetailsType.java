
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type for defining error details
 * 
 * <p>Java class for CommonErrorDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonErrorDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MajorError" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}CommonMajorError"/>
 *         &lt;element name="MinorError" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}CommonMinorError"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonErrorDetailsType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "majorError",
    "minorError"
})
public class CommonErrorDetailsType {

    @XmlElement(name = "MajorError", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", required = true)
    protected CommonMajorError majorError;
    @XmlElement(name = "MinorError", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", required = true)
    protected CommonMinorError minorError;

    /**
     * Gets the value of the majorError property.
     * 
     * @return
     *     possible object is
     *     {@link CommonMajorError }
     *     
     */
    public CommonMajorError getMajorError() {
        return majorError;
    }

    /**
     * Sets the value of the majorError property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonMajorError }
     *     
     */
    public void setMajorError(CommonMajorError value) {
        this.majorError = value;
    }

    /**
     * Gets the value of the minorError property.
     * 
     * @return
     *     possible object is
     *     {@link CommonMinorError }
     *     
     */
    public CommonMinorError getMinorError() {
        return minorError;
    }

    /**
     * Sets the value of the minorError property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonMinorError }
     *     
     */
    public void setMinorError(CommonMinorError value) {
        this.minorError = value;
    }

}
