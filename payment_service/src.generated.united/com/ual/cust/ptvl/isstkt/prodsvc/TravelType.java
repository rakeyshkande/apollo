
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TravelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TravelType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FirstTravelItem" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="supplierName">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="nbrOfItem">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="4"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "firstTravelItem"
})
@XmlSeeAlso({
    com.ual.cust.ptvl.isstkt.prodsvc.PartnerUpdateMilesType.Travel.class
})
public class TravelType {

    @XmlElement(name = "FirstTravelItem", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected TravelType.FirstTravelItem firstTravelItem;

    /**
     * Gets the value of the firstTravelItem property.
     * 
     * @return
     *     possible object is
     *     {@link TravelType.FirstTravelItem }
     *     
     */
    public TravelType.FirstTravelItem getFirstTravelItem() {
        return firstTravelItem;
    }

    /**
     * Sets the value of the firstTravelItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelType.FirstTravelItem }
     *     
     */
    public void setFirstTravelItem(TravelType.FirstTravelItem value) {
        this.firstTravelItem = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="supplierName">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="nbrOfItem">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="4"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FirstTravelItem {

        @XmlAttribute(name = "supplierName")
        protected String supplierName;
        @XmlAttribute(name = "nbrOfItem")
        protected String nbrOfItem;

        /**
         * Gets the value of the supplierName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSupplierName() {
            return supplierName;
        }

        /**
         * Sets the value of the supplierName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSupplierName(String value) {
            this.supplierName = value;
        }

        /**
         * Gets the value of the nbrOfItem property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNbrOfItem() {
            return nbrOfItem;
        }

        /**
         * Sets the value of the nbrOfItem property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNbrOfItem(String value) {
            this.nbrOfItem = value;
        }

    }

}
