
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmailAddressType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="emailAddress" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="privacyInd" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="validInd" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailAddressType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class EmailAddressType {

    @XmlAttribute(name = "emailAddress")
    protected String emailAddress;
    @XmlAttribute(name = "privacyInd")
    protected String privacyInd;
    @XmlAttribute(name = "validInd")
    protected String validInd;

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the privacyInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivacyInd() {
        return privacyInd;
    }

    /**
     * Sets the value of the privacyInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivacyInd(String value) {
        this.privacyInd = value;
    }

    /**
     * Gets the value of the validInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidInd() {
        return validInd;
    }

    /**
     * Sets the value of the validInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidInd(String value) {
        this.validInd = value;
    }

}
