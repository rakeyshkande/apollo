
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProgramContactType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProgramContactType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactAddress" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AddressType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ContactPhone" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ContactPhoneType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ContactOption" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ContactOptionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}EmailAddressType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SecondaryProfile" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}SecondaryProfileType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AffiliationProfile" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AffiliationProfileType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="middleInitial" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="suffix" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="company" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProgramContactType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "contactAddress",
    "contactPhone",
    "contactOption",
    "emailAddress",
    "secondaryProfile",
    "affiliationProfile"
})
public class ProgramContactType {

    @XmlElement(name = "ContactAddress", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<AddressType> contactAddress;
    @XmlElement(name = "ContactPhone", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<ContactPhoneType> contactPhone;
    @XmlElement(name = "ContactOption", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<ContactOptionType> contactOption;
    @XmlElement(name = "EmailAddress", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<EmailAddressType> emailAddress;
    @XmlElement(name = "SecondaryProfile", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<SecondaryProfileType> secondaryProfile;
    @XmlElement(name = "AffiliationProfile", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<AffiliationProfileType> affiliationProfile;
    @XmlAttribute(name = "lastName")
    protected String lastName;
    @XmlAttribute(name = "firstName")
    protected String firstName;
    @XmlAttribute(name = "middleInitial")
    protected String middleInitial;
    @XmlAttribute(name = "suffix")
    protected String suffix;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "company")
    protected String company;

    /**
     * Gets the value of the contactAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddressType }
     * 
     * 
     */
    public List<AddressType> getContactAddress() {
        if (contactAddress == null) {
            contactAddress = new ArrayList<AddressType>();
        }
        return this.contactAddress;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactPhone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactPhone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPhoneType }
     * 
     * 
     */
    public List<ContactPhoneType> getContactPhone() {
        if (contactPhone == null) {
            contactPhone = new ArrayList<ContactPhoneType>();
        }
        return this.contactPhone;
    }

    /**
     * Gets the value of the contactOption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactOption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactOptionType }
     * 
     * 
     */
    public List<ContactOptionType> getContactOption() {
        if (contactOption == null) {
            contactOption = new ArrayList<ContactOptionType>();
        }
        return this.contactOption;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailAddressType }
     * 
     * 
     */
    public List<EmailAddressType> getEmailAddress() {
        if (emailAddress == null) {
            emailAddress = new ArrayList<EmailAddressType>();
        }
        return this.emailAddress;
    }

    /**
     * Gets the value of the secondaryProfile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the secondaryProfile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecondaryProfile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecondaryProfileType }
     * 
     * 
     */
    public List<SecondaryProfileType> getSecondaryProfile() {
        if (secondaryProfile == null) {
            secondaryProfile = new ArrayList<SecondaryProfileType>();
        }
        return this.secondaryProfile;
    }

    /**
     * Gets the value of the affiliationProfile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the affiliationProfile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAffiliationProfile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AffiliationProfileType }
     * 
     * 
     */
    public List<AffiliationProfileType> getAffiliationProfile() {
        if (affiliationProfile == null) {
            affiliationProfile = new ArrayList<AffiliationProfileType>();
        }
        return this.affiliationProfile;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleInitial() {
        return middleInitial;
    }

    /**
     * Sets the value of the middleInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleInitial(String value) {
        this.middleInitial = value;
    }

    /**
     * Gets the value of the suffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Sets the value of the suffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuffix(String value) {
        this.suffix = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

}
