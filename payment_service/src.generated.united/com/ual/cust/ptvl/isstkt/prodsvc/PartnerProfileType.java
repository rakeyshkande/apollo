
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartnerProfileType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartnerProfileType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProgramMembership" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}ProgramMembershipType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}CommonVersion" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerProfileType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "programMembership"
})
@XmlSeeAlso({
    PartnerProfile.class
})
public class PartnerProfileType {

    @XmlElement(name = "ProgramMembership", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<ProgramMembershipType> programMembership;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    /**
     * Gets the value of the programMembership property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the programMembership property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProgramMembership().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProgramMembershipType }
     * 
     * 
     */
    public List<ProgramMembershipType> getProgramMembership() {
        if (programMembership == null) {
            programMembership = new ArrayList<ProgramMembershipType>();
        }
        return this.programMembership;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
