
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactOptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="contactOptionCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="htmlIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactOptionType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class ContactOptionType {

    @XmlAttribute(name = "contactOptionCode")
    protected String contactOptionCode;
    @XmlAttribute(name = "htmlIndicator")
    protected String htmlIndicator;

    /**
     * Gets the value of the contactOptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactOptionCode() {
        return contactOptionCode;
    }

    /**
     * Sets the value of the contactOptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactOptionCode(String value) {
        this.contactOptionCode = value;
    }

    /**
     * Gets the value of the htmlIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHtmlIndicator() {
        return htmlIndicator;
    }

    /**
     * Sets the value of the htmlIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHtmlIndicator(String value) {
        this.htmlIndicator = value;
    }

}
