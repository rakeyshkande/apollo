
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AirlinePrefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AirlinePrefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="homeAirport" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirlinePrefType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class AirlinePrefType {

    @XmlAttribute(name = "homeAirport")
    protected String homeAirport;

    /**
     * Gets the value of the homeAirport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomeAirport() {
        return homeAirport;
    }

    /**
     * Sets the value of the homeAirport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomeAirport(String value) {
        this.homeAirport = value;
    }

}
