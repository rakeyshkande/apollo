
package com.ual.cust.ptvl.isstkt.prodsvc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SecondaryProfileType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecondaryProfileType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AirlinePrefer" type="{http://www.ual.com/cust/ptvl/isstkt/prodsvc}AirlinePrefType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecondaryProfileType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc", propOrder = {
    "airlinePrefer"
})
public class SecondaryProfileType {

    @XmlElement(name = "AirlinePrefer", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
    protected List<AirlinePrefType> airlinePrefer;

    /**
     * Gets the value of the airlinePrefer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airlinePrefer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirlinePrefer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirlinePrefType }
     * 
     * 
     */
    public List<AirlinePrefType> getAirlinePrefer() {
        if (airlinePrefer == null) {
            airlinePrefer = new ArrayList<AirlinePrefType>();
        }
        return this.airlinePrefer;
    }

}
