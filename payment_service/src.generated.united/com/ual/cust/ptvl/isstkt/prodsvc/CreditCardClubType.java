
package com.ual.cust.ptvl.isstkt.prodsvc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardClubType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardClubType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="clubCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardClubType", namespace = "http://www.ual.com/cust/ptvl/isstkt/prodsvc")
public class CreditCardClubType {

    @XmlAttribute(name = "clubCode")
    protected String clubCode;

    /**
     * Gets the value of the clubCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClubCode() {
        return clubCode;
    }

    /**
     * Sets the value of the clubCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClubCode(String value) {
        this.clubCode = value;
    }

}
