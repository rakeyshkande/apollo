
package com.ual.hcp.has;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ual.hcp.has package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ual.hcp.has
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RequestHeader }
     * 
     */
    public RequestHeader createRequestHeader() {
        return new RequestHeader();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest }
     * 
     */
    public RequestHeader.StatelessRequest createRequestHeaderStatelessRequest() {
        return new RequestHeader.StatelessRequest();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Hosted }
     * 
     */
    public RequestHeader.StatelessRequest.Hosted createRequestHeaderStatelessRequestHosted() {
        return new RequestHeader.StatelessRequest.Hosted();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Trusted }
     * 
     */
    public RequestHeader.StatelessRequest.Trusted createRequestHeaderStatelessRequestTrusted() {
        return new RequestHeader.StatelessRequest.Trusted();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Trusted.Role }
     * 
     */
    public RequestHeader.StatelessRequest.Trusted.Role createRequestHeaderStatelessRequestTrustedRole() {
        return new RequestHeader.StatelessRequest.Trusted.Role();
    }

    /**
     * Create an instance of {@link RequestHeader.StatefulRequest }
     * 
     */
    public RequestHeader.StatefulRequest createRequestHeaderStatefulRequest() {
        return new RequestHeader.StatefulRequest();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Hosted.User }
     * 
     */
    public RequestHeader.StatelessRequest.Hosted.User createRequestHeaderStatelessRequestHostedUser() {
        return new RequestHeader.StatelessRequest.Hosted.User();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Hosted.Role }
     * 
     */
    public RequestHeader.StatelessRequest.Hosted.Role createRequestHeaderStatelessRequestHostedRole() {
        return new RequestHeader.StatelessRequest.Hosted.Role();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Trusted.User }
     * 
     */
    public RequestHeader.StatelessRequest.Trusted.User createRequestHeaderStatelessRequestTrustedUser() {
        return new RequestHeader.StatelessRequest.Trusted.User();
    }

    /**
     * Create an instance of {@link RequestHeader.StatelessRequest.Trusted.Role.ACLData }
     * 
     */
    public RequestHeader.StatelessRequest.Trusted.Role.ACLData createRequestHeaderStatelessRequestTrustedRoleACLData() {
        return new RequestHeader.StatelessRequest.Trusted.Role.ACLData();
    }

}
