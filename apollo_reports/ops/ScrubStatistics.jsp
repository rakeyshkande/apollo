<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ScrubStatistics" DTDVersion="9.0.2.0.0">
  <xmlSettings xmlTag="SCRUBSTATISTICS" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <reportHtmlEscapes>
    <beforeReportHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
]]>
    </beforeReportHtmlEscape>
    <beforeFormHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
<form method=post action="_action_">
<input name="hidden_run_parameters" type=hidden value="_hidden_">
<font color=red><!--error--></font>
<center>
<p><table border=0 cellspacing=0 cellpadding=0>
<tr>
<td><input type=submit></td>
<td width=15>
<td><input type=reset></td>
</tr>
</table>
<p><hr><p>
]]>
    </beforeFormHtmlEscape>
  </reportHtmlEscapes>
  <data>
    <userParameter name="P_StartDate" datatype="character" width="10"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_EndDate" datatype="character" width="10"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Scrub_Statics_Summary">
      <select>
      <![CDATA[select UPPER(svr.reason) as sort_reason, 
svr.reason,
count(distinct (upper(svr.reason) || svr.stats_validation_id))  reason_count
from frp.stats_validation_reasons svr,
   frp.stats_validation sv,
   scrub.orders ord,
   frp.stats_osp stats
where svr.stats_validation_id = sv.stats_validation_id
and sv.relator = ord.order_guid
and trunc(sv.timestamp) >= to_date(:P_StartDate, 'MM/DD/YYYY')
and trunc(sv.timestamp) <= to_date(:P_EndDate, 'MM/DD/YYYY')
and sv.external_order_number = stats.external_order_number
and stats.item_state = '3000'
and stats.test_order_flag = 'N'
and UPPER(ord.order_origin) <> 'BULK'
group by svr.reason]]>
      </select>
      <displayInfo x="1.15698" y="0.45825" width="1.67664" height="0.23962"/>
      <group name="G_Reason_Description">
        <displayInfo x="0.95605" y="1.20837" width="2.08801" height="0.60156"
        />
        <dataItem name="sort_reason" datatype="vchar2" columnOrder="15"
         width="500" defaultWidth="100000" defaultHeight="10000">
          <dataDescriptor expression="UPPER ( svr.reason )"
           descriptiveExpression="SORT_REASON" order="1" width="500"/>
        </dataItem>
        <dataItem name="reason" datatype="vchar2" columnOrder="13" width="500"
         defaultWidth="500000" defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="svr.reason"
           descriptiveExpression="REASON" order="2" width="500"/>
        </dataItem>
      </group>
      <group name="G_Reason_Count">
        <displayInfo x="1.16614" y="2.20715" width="1.65332" height="0.60156"
        />
        <dataItem name="reason_count" oracleDatatype="number" columnOrder="14"
         width="22" defaultWidth="90000" defaultHeight="10000">
          <xmlSettings xmlTag="COUNT"/>
          <dataDescriptor
           expression="count ( distinct ( upper ( svr.reason ) || svr.stats_validation_id ) )"
           descriptiveExpression="REASON_COUNT" order="3"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_TotalOrders" source="cf_totalordersformula"
     datatype="number" width="20" precision="10" valueIfNull="0"
     breakOrder="none">
      <displayInfo x="3.55200" y="0.53125" width="1.35425" height="0.21875"/>
    </formula>
    <formula name="CF_BypassOrders" source="cf_bypassordersformula"
     datatype="number" width="20" precision="10" valueIfNull="0"
     breakOrder="none">
      <displayInfo x="3.58337" y="0.86450" width="1.29175" height="0.21875"/>
    </formula>
    <formula name="CF_ScrubOrdersPercent"
     source="cf_reasontotalpercentformula" datatype="number" width="20"
     precision="10" valueIfNull="0" breakOrder="none">
      <displayInfo x="3.60413" y="1.53125" width="1.46875" height="0.19995"/>
    </formula>
    <formula name="CF_BypassPercent" source="cf_bypasspercentformula"
     datatype="number" width="20" precision="10" valueIfNull="0"
     breakOrder="none">
      <displayInfo x="3.60425" y="1.16675" width="1.28125" height="0.23962"/>
    </formula>
    <formula name="CF_OrdersScrub" source="cf_ordersscrubformula"
     datatype="number" width="20" precision="10" breakOrder="none">
      <displayInfo x="3.62500" y="1.86450" width="1.35413" height="0.19995"/>
    </formula>
  </data>
  <layout>
  <section name="main">
    <body width="7.93750">
      <location x="0.06250"/>
      <frame name="M_G_1_GRPFR">
        <geometryInfo x="0.00000" y="0.06250" width="7.81250" height="2.18750"
        />
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_1" source="G_Reason_Description"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.31250" width="7.81250"
           height="0.18750"/>
          <generalLayout verticalElasticity="variable"/>
          <visualSettings fillPattern="transparent"/>
          <frame name="M_G_reason_GRPFR">
            <geometryInfo x="5.93750" y="0.31250" width="1.31250"
             height="0.18750"/>
            <generalLayout verticalElasticity="variable"/>
            <visualSettings fillPattern="transparent"/>
            <repeatingFrame name="R_G_reason" source="G_Reason_Count"
             printDirection="down" minWidowRecords="1" columnMode="no">
              <geometryInfo x="6.06250" y="0.31250" width="1.06250"
               height="0.18750"/>
              <visualSettings fillPattern="transparent"/>
              <field name="F_reason_count" source="reason_count"
               alignment="end">
                <font face="Courier New" size="10"/>
                <geometryInfo x="6.12500" y="0.31250" width="0.93750"
                 height="0.18750"/>
                <visualSettings fillPattern="transparent"/>
              </field>
            </repeatingFrame>
          </frame>
          <field name="F_reason" source="reason" alignment="start">
            <font face="Courier New" size="10"/>
            <geometryInfo x="0.18750" y="0.31250" width="5.50000"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <frame name="M_G_1_FTR">
          <geometryInfo x="0.00000" y="0.87500" width="7.81250"
           height="0.25000"/>
          <advancedLayout printObjectOnPage="lastPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_Total_Orders_Scrub" source="CF_OrdersScrub"
           formatMask="NNN,NNN" alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="4.43750" y="0.93750" width="0.93750"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
          </field>
          <text name="B_Total">
            <geometryInfo x="0.18750" y="0.93750" width="2.56250"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Total Orders to Scrub Queue:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_ReasonTotalPercent" source="CF_ScrubOrdersPercent"
           formatMask="-NNNN%" alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="5.81250" y="0.93750" width="0.62500"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
          </field>
        </frame>
        <field name="F_BypassOrders" source="CF_BypassOrders"
         formatMask="NNN,NNN" spacing="single" alignment="end">
          <font face="Courier New" size="10" bold="yes"/>
          <geometryInfo x="4.43750" y="1.25000" width="0.93750"
           height="0.18750"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_TotalOrders" source="CF_TotalOrders"
         formatMask="NNN,NNN" spacing="single" alignment="end">
          <font face="Courier New" size="10" bold="yes"/>
          <geometryInfo x="4.43750" y="1.56250" width="0.93750"
           height="0.18750"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <text name="B_6">
          <textSettings justify="center"/>
          <geometryInfo x="3.31250" y="1.93750" width="1.50000"
           height="0.18750"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <field name="F_BypassPercent" source="CF_BypassPercent"
         formatMask="-NNNN%" alignment="end">
          <font face="Courier New" size="10" bold="yes"/>
          <geometryInfo x="5.81250" y="1.25000" width="0.62500"
           height="0.18750"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <text name="B_1">
          <geometryInfo x="0.18750" y="1.25000" width="3.06250"
           height="0.18750"/>
          <visualSettings fillBackgroundColor="Totals"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[Total Orders that bypassed Scrub:]]>
            </string>
          </textSegment>
        </text>
        <text name="B_2">
          <geometryInfo x="0.18750" y="1.56250" width="2.06250"
           height="0.13538"/>
          <visualSettings fillBackgroundColor="Totals"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[Total Orders Received:]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_1_HDR">
          <geometryInfo x="0.00000" y="0.06250" width="7.75000"
           height="0.25000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_reason">
            <textSettings justify="center"/>
            <geometryInfo x="0.25000" y="0.06250" width="0.87500"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Reason]]>
              </string>
            </textSegment>
          </text>
          <text name="B_reason_count">
            <textSettings justify="center"/>
            <geometryInfo x="6.37500" y="0.06250" width="0.93750"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="TableHeading"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Count]]>
              </string>
            </textSegment>
          </text>
          <line name="B_4" arrow="none">
            <geometryInfo x="6.68750" y="0.25000" width="0.43750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="6.68750" y="0.25000"/>
              <point x="7.12500" y="0.25000"/>
            </points>
          </line>
          <line name="B_5" arrow="none">
            <geometryInfo x="0.43750" y="0.25000" width="0.50000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="0.43750" y="0.25000"/>
              <point x="0.93750" y="0.25000"/>
            </points>
          </line>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_OR$BODY_SECTION">
        <textSettings justify="center"/>
        <geometryInfo x="2.50000" y="0.25000" width="3.08374" height="0.37500"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Order Processing System
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Scrub Statistics Summary Report]]>
          </string>
        </textSegment>
      </text>
      <field name="F_Date" source="CurrentDate" formatMask="MM/DD/RRRR"
       spacing="single" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="0.06250" y="0.25000" width="1.06250" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
      <field name="F_Time" source="CurrentDate" formatMask="HH:MI AM"
       spacing="single" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="6.62500" y="0.25000" width="1.25000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
      <field name="F_StartDate" source="P_StartDate" spacing="single"
       alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="2.93750" y="0.62500" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
      <field name="F_EndDate" source="P_EndDate" spacing="single"
       alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="4.25000" y="0.62500" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
      <text name="B_3">
        <textSettings justify="center"/>
        <geometryInfo x="3.93750" y="0.62500" width="0.23962" height="0.16663"
        />
        <visualSettings lineBackgroundColor="black"/>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[to]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_totalordersformula" returnType="number">
      <textSource>
      <![CDATA[function CF_TotalOrdersFormula return Number is
  totalOrders number;
begin
  select count(distinct concat( master_order_number, external_order_number)) into totalOrders
    from frp.stats_osp
    where trunc(timestamp) >= to_date(:P_StartDate,'MM/DD/YYYY')
    and trunc(timestamp) <= to_date(:P_EndDate,'MM/DD/YYYY')
    and item_state = '3004'
    and test_order_flag = 'N';   
  return totalOrders;
end;
]]>
      </textSource>
    </function>
    <function name="cf_bypassordersformula" returnType="number">
      <textSource>
      <![CDATA[function CF_BypassOrdersFormula return Number is
  bypassOrders number;
begin
  select count(distinct concat( master_order_number, external_order_number))into bypassOrders
    from frp.stats_osp
    where trunc(timestamp) >= to_date(:P_StartDate,'MM/DD/YYYY')
    and trunc(timestamp) <= to_date(:P_EndDate,'MM/DD/YYYY')
    and item_state = '3004'
    and relator is null
    and test_order_flag = 'N';
  return bypassOrders;
end;]]>
      </textSource>
    </function>
    <function name="cf_bypasspercentformula" returnType="number">
      <textSource>
      <![CDATA[function CF_BypassPercentFormula return Number is
  bypassPercent number;
begin

	if (:cf_totalOrders = 0 ) then
	  bypassPercent := 0;
	else
    bypassPercent := :cf_bypassOrders * 100 / :cf_totalOrders;
    if bypassPercent < 1 then 
    	bypassPercent := 0;
    end if;
	end if;
	
  return bypassPercent;
end;]]>
      </textSource>
    </function>
    <function name="cf_reasontotalpercentformula" returnType="number">
      <textSource>
      <![CDATA[function CF_ReasonTotalPercentFormula return Number is
  reasonTotalPercent number;
begin

	if (:cf_OrdersScrub = 0) then
		reasonTotalPercent := 0;
	else
    reasonTotalPercent := :cf_OrdersScrub * 100 / :cf_totalOrders;
    if reasonTotalPercent < 1 then 
    	reasonTotalPercent := 0;
    end if;
	end if;
	
  return reasonTotalPercent;
end;]]>
      </textSource>
    </function>
    <function name="cf_ordersscrubformula" returnType="number">
      <textSource>
      <![CDATA[function CF_OrdersScrubFormula return Number is
 scrubOrders number;
begin
  select count(distinct concat( master_order_number, external_order_number))into scrubOrders
    from frp.stats_osp
    where trunc(timestamp) >= to_date(:P_StartDate,'MM/DD/YYYY')
    and trunc(timestamp) <= to_date(:P_EndDate,'MM/DD/YYYY')
    and item_state = '3004'
    and relator is not null
    and test_order_flag = 'N';
  return scrubOrders;
end;
]]>
      </textSource>
    </function>
  </programUnits>
  <colorPalette>
    <color index="190" displayName="TextColor" value="#336699"/>
    <color index="191" displayName="TableHeading" value="#cccc99"/>
    <color index="192" displayName="TableCell" value="#f7f7e7"/>
    <color index="193" displayName="Totals" value="#ffffcc"/>
  </colorPalette>
  <reportPrivate defaultReportType="tabBrkLeft" templateName="rwbeige"/>
</report>
</rw:objects>
-->
<HTML>
<rw:style id="rwbeige">
<link rel="StyleSheet" type="text/css" href="css/rwbeige.css">
</rw:style>

<TITLE> Your Title </TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>
<BODY bgColor=#ffffff link=#000000 vLink=#000000>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ffffff rowSpan=2 vAlign=center width=188> 
      <P><IMG src="images/rwbeige_logo.gif" width="135" height="36"><br>
      </P>
      </TD>
    <TD bgColor=#ffffff height=40 vAlign=top><IMG alt="" height=1 src="images/stretch.gif" width=5></TD>
    <TD align=right bgColor=#ffffff vAlign=bottom>&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE bgColor=#ff0000 border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 class="OraColumnHeader">&nbsp; </TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 vAlign=top class="OraColumnHeader"><IMG alt="" border=0 height=17 src="images/topcurl.gif" width=30></TD>
    <TD vAlign=top width="100%">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD bgColor=#000000 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#9a9c9a height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#b3b4b3 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
		</TBODY>
	  </TABLE>
	</TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="29%" valign="top"> 
      <TABLE width="77%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_d_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation goes here </TD>
        </TR>
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_r_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation Item</TD>
        </TR>
      </TABLE>
    </TD>
    <TD width="71%">
      <DIV align="center">
	    <!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MG1GRPFR">
<table class="OraTable">
<caption>  </caption>
 <!-- Header -->
 <thead>
  <tr>
   <th <rw:id id="HBreason" asArray="no"/> class="OraColumnHeader"> Reason </th>
   <th <rw:id id="HBreasoncount" asArray="no"/> class="OraColumnHeader"> Count </th>
  </tr>
 </thead>
 <!-- Body -->
 <tbody>
  <rw:foreach id="R_G_1_1" src="G_1">
   <rw:foreach id="R_G_reason_1" src="G_reason">
    <tr>
     <td <rw:id id="HFreason" breakLevel="R_G_1_1" asArray="no"/> class="OraRowHeader"><rw:field id="reason" src="reason" breakLevel="R_G_1_1" breakValue="&nbsp;"> F_reason </rw:field></td>
     <td <rw:headers id="HFreasoncount" src="HBreasoncount, HBreason, HFreason"/> class="OraCellNumber"><rw:field id="F_reason_count" src="reason_count" nullValue="&nbsp;"> F_reason_count </rw:field></td>
    </tr>
   </rw:foreach>
  </rw:foreach>
 </tbody>
 <!-- Report Level Summary -->
 <tr>
  <th class="OraTotalText"> &nbsp; </th>
  <td <rw:headers id="HFSumreasoncountPerReport" src="HBreasoncount"/> class="OraTotalNumber">Total: <rw:field id="F_Sumreason_countPerReport" src="Sumreason_countPerReport" nullValue="&nbsp;"> F_Sumreason_countPerReport </rw:field></td>
 </tr>
</table>
</rw:dataArea> <!-- id="MG1GRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->

	  </DIV>
    </TD>
  </TR>
</TABLE>
<P><BR>
</P>
<TD align=middle vAlign=top width="100%">&nbsp;</TD>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ff0000 colSpan=2 class="OraColumnHeader"><IMG alt=" " border=0 height=4 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ffffff>&nbsp;</TD>
    <TD align=right bgColor=#ffffff>&nbsp;</TD>
  </TR></TBODY></TABLE>
</BODY>
</HTML>

 
<!--
</rw:report> 
-->

