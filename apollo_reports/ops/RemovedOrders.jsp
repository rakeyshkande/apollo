<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="RemovedOrders" DTDVersion="9.0.2.0.0">
  <xmlSettings xmlTag="REMOVEDORDERSXXX" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <reportHtmlEscapes>
    <beforeReportHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
]]>
    </beforeReportHtmlEscape>
    <beforeFormHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
<form method=post action="_action_">
<input name="hidden_run_parameters" type=hidden value="_hidden_">
<font color=red><!--error--></font>
<center>
<p><table border=0 cellspacing=0 cellpadding=0>
<tr>
<td><input type=submit></td>
<td width=15>
<td><input type=reset></td>
</tr>
</table>
<p><hr><p>
]]>
    </beforeFormHtmlEscape>
  </reportHtmlEscapes>
  <data>
    <userParameter name="P_StartDate" datatype="character" width="10"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_EndDate" datatype="character" width="10"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_Origin" datatype="character" width="250"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Removed_Orders">
      <select>
      <![CDATA[select
  orders.order_origin,
  orders.source_code,
  source.description,
  order_details.external_order_number,
  order_details.ariba_po_number,
  disposition_codes.disposition_description,
  order_disposition.sent_email_flag,
  to_char(order_disposition.comment_date,'MM/DD/YY') as remove_date,
  to_char(order_disposition.comment_date,'hh24:mi:ss') as remove_time,
  order_disposition.csr_id
from
  scrub.orders,
  scrub.order_details,
  scrub.order_disposition,
  ftd_apps.source,
  scrub.disposition_codes
where
  order_details.status in ('2004','2008')
  and trunc(order_disposition.comment_date) >= to_date(:P_StartDate,'MM/DD/YYYY') 
  and trunc(order_disposition.comment_date) <= to_date(:P_EndDate,'MM/DD/YYYY') 
  and order_details.order_detail_id = order_disposition.order_detail_id
  and order_details.order_guid = orders.order_guid
  and orders.source_code = source.source_code
  and lower(orders.order_origin) <> 'test'
  and order_disposition.disposition_id = disposition_codes.disposition_id
order by
  orders.order_origin,
  order_disposition.comment_date,
  order_details.external_order_number;]]>
      </select>
      <displayInfo x="1.23523" y="0.30200" width="1.49548" height="0.19995"/>
      <group name="G_order_origin">
        <displayInfo x="1.22058" y="0.93958" width="1.53845" height="0.43066"
        />
        <dataItem name="order_origin" datatype="vchar2" columnOrder="14"
         width="4000" defaultWidth="100000" defaultHeight="10000">
          <dataDescriptor expression="orders.order_origin"
           descriptiveExpression="ORDER_ORIGIN" order="1" width="4000"/>
        </dataItem>
      <filter type="plsql" plsqlFilter="g_order_origingroupfilter"
       numberOfRecords="0"/>
      </group>
      <group name="G_external_order_number">
        <displayInfo x="0.89124" y="1.87915" width="2.19763" height="1.96875"
        />
        <dataItem name="csr_id" datatype="vchar2" columnOrder="23" width="100"
         defaultWidth="100000" defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="order_disposition.csr_id"
           descriptiveExpression="CSR_ID" order="10" width="100"/>
        </dataItem>
        <dataItem name="external_order_number" datatype="vchar2"
         columnOrder="15" width="4000" defaultWidth="100000"
         defaultHeight="10000">
          <dataDescriptor expression="order_details.external_order_number"
           descriptiveExpression="EXTERNAL_ORDER_NUMBER" order="4"
           width="4000"/>
        </dataItem>
        <dataItem name="ariba_po_number" datatype="vchar2" columnOrder="16"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="order_details.ariba_po_number"
           descriptiveExpression="ARIBA_PO_NUMBER" order="5" width="4000"/>
        </dataItem>
        <dataItem name="source_code" datatype="vchar2" columnOrder="20"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="orders.source_code"
           descriptiveExpression="SOURCE_CODE" order="2" width="4000"/>
        </dataItem>
        <dataItem name="description" datatype="vchar2" columnOrder="18"
         width="25" defaultWidth="200000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="source.description"
           descriptiveExpression="DESCRIPTION" order="3" width="25"/>
        </dataItem>
        <dataItem name="remove_time" datatype="vchar2" columnOrder="22"
         width="75" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor
           expression="to_char ( order_disposition.comment_date , &apos;hh24:mi:ss&apos; )"
           descriptiveExpression="REMOVE_TIME" order="9" width="75"/>
        </dataItem>
        <dataItem name="remove_date" datatype="vchar2" columnOrder="21"
         width="75" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor
           expression="to_char ( order_disposition.comment_date , &apos;MM/DD/YY&apos; )"
           descriptiveExpression="REMOVE_DATE" order="8" width="75"/>
        </dataItem>
        <dataItem name="sent_email_flag" datatype="character"
         oracleDatatype="aFixedChar" columnOrder="17" width="1"
         defaultWidth="10000" defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="order_disposition.sent_email_flag"
           descriptiveExpression="SENT_EMAIL_FLAG" order="7"
           oracleDatatype="aFixedChar" width="1"/>
        </dataItem>
        <dataItem name="disposition_description" datatype="vchar2"
         columnOrder="19" width="80" defaultWidth="200000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor
           expression="disposition_codes.disposition_description"
           descriptiveExpression="DISPOSITION_DESCRIPTION" order="6"
           width="80"/>
        </dataItem>
      </group>
    </dataSource>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="13.43750" height="6.50000">
      <location x="0.06250"/>
      <frame name="M_G_Removed_Orders_Enclosing">
        <geometryInfo x="0.00000" y="0.00000" width="13.43750"
         height="2.00000"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_order_origin" source="G_order_origin"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.62500" width="13.43750"
           height="0.68750"/>
          <generalLayout verticalElasticity="variable"/>
          <visualSettings fillPattern="transparent"/>
          <repeatingFrame name="R_G_external_order_number"
           source="G_external_order_number" printDirection="down"
           minWidowRecords="1" columnMode="no">
            <geometryInfo x="0.12500" y="1.00000" width="13.31250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_external_order_number"
             source="external_order_number" alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="0.31250" y="1.00000" width="1.12500"
               height="0.18750"/>
            </field>
            <field name="F_ariba_po_number" source="ariba_po_number"
             alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="1.62500" y="1.00000" width="0.75000"
               height="0.18750"/>
            </field>
            <field name="F_source_code" source="source_code" alignment="start"
              >
              <font face="Courier New" size="10"/>
              <geometryInfo x="2.81250" y="1.00000" width="0.56250"
               height="0.18750"/>
            </field>
            <field name="F_description" source="description" alignment="start"
              >
              <font face="Courier New" size="10"/>
              <geometryInfo x="3.62500" y="1.00000" width="2.68750"
               height="0.18750"/>
            </field>
            <field name="F_updated_by" source="csr_id" alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="8.37500" y="1.00000" width="1.12500"
               height="0.18750"/>
            </field>
            <field name="F_sent_email_flag" source="sent_email_flag"
             alignment="center">
              <font face="Courier New" size="10"/>
              <geometryInfo x="9.68750" y="1.00000" width="0.37500"
               height="0.18750"/>
            </field>
            <field name="F_disposition_description"
             source="disposition_description" alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="10.25000" y="1.00000" width="3.18750"
               height="0.18750"/>
              <generalLayout verticalElasticity="variable"/>
            </field>
            <field name="F_remove_time" source="remove_time"
             alignment="center">
              <font face="Courier New" size="10"/>
              <geometryInfo x="6.50000" y="1.00000" width="0.81250"
               height="0.18750"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_remove_date" source="remove_date"
             alignment="center">
              <font face="Courier New" size="10"/>
              <geometryInfo x="7.43750" y="1.00000" width="0.75000"
               height="0.18750"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </repeatingFrame>
          <field name="F_order_origin" source="order_origin" alignment="start"
            >
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="1.06250" y="0.75000" width="2.25000"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <text name="B_14">
            <textSettings justify="center"/>
            <geometryInfo x="0.06250" y="0.75000" width="0.83337"
             height="0.16663"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Category:]]>
              </string>
            </textSegment>
          </text>
        </repeatingFrame>
        <text name="B_2">
          <textSettings justify="center"/>
          <geometryInfo x="5.87500" y="1.75000" width="1.37500"
           height="0.18750"/>
          <visualSettings fillBackgroundColor="cyan"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_Headings">
          <geometryInfo x="0.06250" y="0.06250" width="13.37500"
           height="0.56250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_external_order_number">
            <geometryInfo x="0.12500" y="0.37500" width="1.12500"
             height="0.25000"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Confirmation]]>
              </string>
            </textSegment>
          </text>
          <text name="B_ariba_po_number">
            <geometryInfo x="1.62500" y="0.37500" width="0.87500"
             height="0.25000"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[PO Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_source_code">
            <geometryInfo x="2.75000" y="0.37500" width="0.56250"
             height="0.25000"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Source]]>
              </string>
            </textSegment>
          </text>
          <text name="B_updated_on">
            <textSettings justify="center"/>
            <geometryInfo x="6.56250" y="0.06250" width="0.68750"
             height="0.56250"/>
            <visualSettings fillBackgroundColor="TableHeading"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Order
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Removed
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Time]]>
              </string>
            </textSegment>
          </text>
          <text name="B_updated_by">
            <textSettings justify="center"/>
            <geometryInfo x="7.43750" y="0.06250" width="0.75000"
             height="0.50000"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Order
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Removed
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Date]]>
              </string>
            </textSegment>
          </text>
          <text name="B_sent_email_flag">
            <textSettings justify="center"/>
            <geometryInfo x="9.56250" y="0.06250" width="0.56250"
             height="0.50000"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Email Sent
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[(Y/N)]]>
              </string>
            </textSegment>
          </text>
          <text name="B_disposition_description">
            <geometryInfo x="10.25000" y="0.37500" width="1.06250"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Disposition]]>
              </string>
            </textSegment>
          </text>
          <line name="B_3" arrow="none">
            <geometryInfo x="2.75000" y="0.56250" width="0.50000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="2.75000" y="0.56250"/>
              <point x="3.25000" y="0.56250"/>
            </points>
          </line>
          <line name="B_4" arrow="none">
            <geometryInfo x="6.56250" y="0.56250" width="0.62500"
             height="0.00000"/>
            <visualSettings fillBackgroundColor="TableHeading"
             linePattern="solid"/>
            <points>
              <point x="6.56250" y="0.56250"/>
              <point x="7.18750" y="0.56250"/>
            </points>
          </line>
          <line name="B_5" arrow="none">
            <geometryInfo x="1.62500" y="0.56250" width="0.75000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="1.62500" y="0.56250"/>
              <point x="2.37500" y="0.56250"/>
            </points>
          </line>
          <line name="B_6" arrow="none">
            <geometryInfo x="0.12500" y="0.56250" width="1.00000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="0.12500" y="0.56250"/>
              <point x="1.12500" y="0.56250"/>
            </points>
          </line>
          <text name="B_7">
            <geometryInfo x="3.62500" y="0.37500" width="1.68750"
             height="0.16663"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Source Description]]>
              </string>
            </textSegment>
          </text>
          <line name="B_8" arrow="none">
            <geometryInfo x="3.62500" y="0.56128" width="1.50000"
             height="0.00122"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="3.62500" y="0.56250"/>
              <point x="5.12500" y="0.56128"/>
            </points>
          </line>
          <line name="B_9" arrow="none">
            <geometryInfo x="7.50000" y="0.56250" width="0.56250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="7.50000" y="0.56250"/>
              <point x="8.06250" y="0.56250"/>
            </points>
          </line>
          <line name="B_10" arrow="none">
            <geometryInfo x="10.25000" y="0.56250" width="1.00000"
             height="0.00000"/>
            <visualSettings fillBackgroundColor="r25g50b75"
             linePattern="solid"/>
            <points>
              <point x="10.25000" y="0.56250"/>
              <point x="11.25000" y="0.56250"/>
            </points>
          </line>
          <line name="B_11" arrow="none">
            <geometryInfo x="9.62500" y="0.56250" width="0.43750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="9.62500" y="0.56250"/>
              <point x="10.06250" y="0.56250"/>
            </points>
          </line>
          <text name="B_12">
            <textSettings justify="center"/>
            <geometryInfo x="8.37500" y="0.25000" width="0.75000"
             height="0.37500"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Removed
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[By]]>
              </string>
            </textSegment>
          </text>
          <line name="B_13" arrow="none">
            <geometryInfo x="8.37500" y="0.56250" width="0.68750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="8.37500" y="0.56250"/>
              <point x="9.06250" y="0.56250"/>
            </points>
          </line>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_OR$BODY_SECTION">
        <textSettings justify="center"/>
        <geometryInfo x="5.18750" y="0.06250" width="2.75000" height="0.37500"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Order Processing System
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Removed Orders Report]]>
          </string>
        </textSegment>
      </text>
      <field name="F_StartDate" source="P_StartDate" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="5.43750" y="0.43750" width="1.06250" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
      <field name="F_EndDate" source="P_EndDate" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="6.81250" y="0.43750" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
      <field name="F_Date" source="CurrentDate" formatMask="MM/DD/RRRR"
       spacing="single" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="0.25000" y="0.25000" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_Time" source="CurrentDate" formatMask="HH:MI AM"
       spacing="single" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="12.00000" y="0.25000" width="1.00000"
         height="0.18750"/>
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <text name="B_1">
        <textSettings justify="center"/>
        <geometryInfo x="6.56250" y="0.43750" width="0.16663" height="0.16663"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[-]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="g_order_origingroupfilter">
      <textSource>
      <![CDATA[function G_order_originGroupFilter return boolean is
  retVal boolean;
begin
	retVal := true;
  if (upper(:P_Origin) <> 'ALL') then
  	if (upper(:P_Origin) <> upper(:order_origin)) then
  		retVal := false;
  	end if;
  end if;
  return (retVal);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <colorPalette>
    <color index="190" displayName="TextColor" value="#336699"/>
    <color index="191" displayName="TableHeading" value="#cccc99"/>
    <color index="192" displayName="TableCell" value="#f7f7e7"/>
    <color index="193" displayName="Totals" value="#ffffcc"/>
  </colorPalette>
  <reportPrivate defaultReportType="masterDetail" templateName="rwbeige"
   sectionTitle="Removed Orders Report"/>
</report>
</rw:objects>
-->
<HTML>
<rw:style id="rwbeige">
<link rel="StyleSheet" type="text/css" href="css/rwbeige.css">
</rw:style>

<TITLE> Your Title </TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>
<BODY bgColor=#ffffff link=#000000 vLink=#000000>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ffffff rowSpan=2 vAlign=center width=188> 
      <P><IMG src="images/rwbeige_logo.gif" width="135" height="36"><br>
      </P>
      </TD>
    <TD bgColor=#ffffff height=40 vAlign=top><IMG alt="" height=1 src="images/stretch.gif" width=5></TD>
    <TD align=right bgColor=#ffffff vAlign=bottom>&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE bgColor=#ff0000 border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 class="OraColumnHeader">&nbsp; </TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 vAlign=top class="OraColumnHeader"><IMG alt="" border=0 height=17 src="images/topcurl.gif" width=30></TD>
    <TD vAlign=top width="100%">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD bgColor=#000000 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#9a9c9a height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#b3b4b3 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
		</TBODY>
	  </TABLE>
	</TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="29%" valign="top"> 
      <TABLE width="77%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_d_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation goes here </TD>
        </TR>
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_r_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation Item</TD>
        </TR>
      </TABLE>
    </TD>
    <TD width="71%">
      <DIV align="center">
	    <!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MGorderoriginGRPFR">
<rw:foreach id="R_G_order_origin_1" src="G_order_origin">
 <table class="OraTable">
 <caption class="OraHeader"> Removed Orders Report <br>Order Origin <rw:field id="F_order_origin" src="order_origin" breakLevel="R_G_order_origin_1" breakValue="&nbsp;"> F_order_origin </rw:field><br>
 </caption>
   <tr>
    <td valign="top">
    <table class="OraTable" summary="Removed Orders Report">
     <!-- Header -->
     <thead>
      <tr>
       <th <rw:id id="HBexternalordernumber" asArray="no"/> class="OraColumnHeader"> External Order Number </th>
       <th <rw:id id="HBaribaponumber" asArray="no"/> class="OraColumnHeader"> Ariba Po
Number </th>
       <th <rw:id id="HBsourcecode" asArray="no"/> class="OraColumnHeader"> Source
Code </th>
       <th <rw:id id="HBdescription" asArray="no"/> class="OraColumnHeader"> Description </th>
       <th <rw:id id="HBupdatedon" asArray="no"/> class="OraColumnHeader"> Updated On </th>
       <th <rw:id id="HBupdatedby" asArray="no"/> class="OraColumnHeader"> Updated By </th>
       <th <rw:id id="HBsentemailflag" asArray="no"/> class="OraColumnHeader"> Sent
Email Flag </th>
       <th <rw:id id="HBdispositiondescription" asArray="no"/> class="OraColumnHeader"> Disposition
Description </th>
      </tr>
     </thead>
     <!-- Body -->
     <tbody>
      <rw:foreach id="R_G_external_order_number_1" src="G_external_order_number">
       <tr>
        <td <rw:headers id="HFexternalordernumber" src="HBexternalordernumber"/> class="OraCellText"><rw:field id="F_external_order_number" src="external_order_number" nullValue="&nbsp;"> F_external_order_number </rw:field></td>
        <td <rw:headers id="HFaribaponumber" src="HBaribaponumber"/> class="OraCellText"><rw:field id="F_ariba_po_number" src="ariba_po_number" nullValue="&nbsp;"> F_ariba_po_number </rw:field></td>
        <td <rw:headers id="HFsourcecode" src="HBsourcecode"/> class="OraCellText"><rw:field id="F_source_code" src="source_code" nullValue="&nbsp;"> F_source_code </rw:field></td>
        <td <rw:headers id="HFdescription" src="HBdescription"/> class="OraCellText"><rw:field id="F_description" src="description" nullValue="&nbsp;"> F_description </rw:field></td>
        <td <rw:headers id="HFupdatedon" src="HBupdatedon"/> class="OraCellText"><rw:field id="F_updated_on" src="updated_on" nullValue="&nbsp;"> F_updated_on </rw:field></td>
        <td <rw:headers id="HFupdatedby" src="HBupdatedby"/> class="OraCellText"><rw:field id="F_updated_by" src="updated_by" nullValue="&nbsp;"> F_updated_by </rw:field></td>
        <td <rw:headers id="HFsentemailflag" src="HBsentemailflag"/> class="OraCellText"><rw:field id="F_sent_email_flag" src="sent_email_flag" nullValue="&nbsp;"> F_sent_email_flag </rw:field></td>
        <td <rw:headers id="HFdispositiondescription" src="HBdispositiondescription"/> class="OraCellText"><rw:field id="F_disposition_description" src="disposition_description" nullValue="&nbsp;"> F_disposition_description </rw:field></td>
       </tr>
      </rw:foreach>
     </tbody>
     <tr>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</rw:foreach>
<table class="OraTable" summary="Removed Orders Report">
</table>
</rw:dataArea> <!-- id="MGorderoriginGRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->

	  </DIV>
    </TD>
  </TR>
</TABLE>
<P><BR>
</P>
<TD align=middle vAlign=top width="100%">&nbsp;</TD>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ff0000 colSpan=2 class="OraColumnHeader"><IMG alt=" " border=0 height=4 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ffffff>&nbsp;</TD>
    <TD align=right bgColor=#ffffff>&nbsp;</TD>
  </TR></TBODY></TABLE>
</BODY>
</HTML>

 
<!--
</rw:report> 
-->

