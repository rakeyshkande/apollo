<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="OrdersScrubbedByCSR" DTDVersion="9.0.2.0.0"
 beforeReportTrigger="beforereport" afterReportTrigger="afterreport">
  <xmlSettings xmlTag="ORDERSSCRUBBEDBYCSR" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <reportHtmlEscapes>
    <beforeReportHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
]]>
    </beforeReportHtmlEscape>
    <beforeFormHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
<form method=post action="_action_">
<input name="hidden_run_parameters" type=hidden value="_hidden_">
<font color=red><!--error--></font>
<center>
<p><table border=0 cellspacing=0 cellpadding=0>
<tr>
<td><input type=submit></td>
<td width=15>
<td><input type=reset></td>
</tr>
</table>
<p><hr><p>
]]>
    </beforeFormHtmlEscape>
  </reportHtmlEscapes>
  <data>
    <userParameter name="P_StartDate" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_EndDate" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Order_Scrub_By_CSR">
      <select>
      <![CDATA[select
  stats.out_csr_id,
  users.last_name || ', ' ||  users.first_name as user_name,
  call_center.description,
  count(*)
from
  rpt.temp_scrub_list stats,
  aas.identity identity,
  aas.users users,
  ftd_apps.call_center call_center
where report_id = :CP_Seq_number
  and stats.out_item_state in ('3006','3008','3010')
  and stats.out_status = '2007'
  and stats.out_test_order_flag = 'N'
  and upper(stats.out_csr_id) = upper(identity.identity_id)
  and identity.user_id = users.user_id
  and users.call_center_id = call_center.call_center_id
  and trunc(stats.out_timestamp) >= to_date(:P_StartDate,'MM/DD/YYYY')
  and trunc(stats.out_timestamp) <= to_date(:P_EndDate,'MM/DD/YYYY')
  and in_status <> out_status
group by
  call_center.description,
  users.last_name,
  users.first_name,
  stats.out_csr_id]]>
      </select>
      <displayInfo x="1.01074" y="0.36462" width="1.91663" height="0.19995"/>
      <group name="G_Call_Center">
        <displayInfo x="0.83325" y="1.09583" width="2.27087" height="0.60156"
        />
        <dataItem name="description" datatype="vchar2" columnOrder="13"
         width="50" defaultWidth="300000" defaultHeight="10000">
          <dataDescriptor expression="call_center.description"
           descriptiveExpression="DESCRIPTION" order="3" width="50"/>
        </dataItem>
        <summary name="SumcountPerdescription" source="count" function="sum"
         width="22" precision="38" reset="G_Call_Center" compute="report">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_Detail_Information">
        <displayInfo x="1.22070" y="2.36121" width="1.48804" height="0.77246"
        />
        <dataItem name="user_name" datatype="vchar2" columnOrder="15"
         width="542" defaultWidth="100000" defaultHeight="10000">
          <dataDescriptor
           expression="users.last_name || &apos;, &apos; || users.first_name"
           descriptiveExpression="USER_NAME" order="2" width="542"/>
        </dataItem>
        <dataItem name="out_csr_id" datatype="vchar2" columnOrder="16"
         width="100" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="stats.out_csr_id"
           descriptiveExpression="OUT_CSR_ID" order="1" width="100"/>
        </dataItem>
        <dataItem name="count" oracleDatatype="number" columnOrder="14"
         width="22" defaultWidth="90000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="count ( * )"
           descriptiveExpression="COUNT(*)" order="4" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <summary name="SumcountPerReport" source="count" function="sum" width="22"
     precision="38" reset="report" compute="report">
      <displayInfo x="3.50000" y="0.42712" width="1.43750" height="0.19995"/>
    </summary>
    <placeholder name="CP_Seq_number" datatype="character" width="30"
     precision="10">
      <displayInfo x="3.54175" y="0.84387" width="1.43750" height="0.21875"/>
    </placeholder>
  </data>
  <layout>
  <section name="main">
    <body width="7.93750">
      <location x="0.06250"/>
      <frame name="M_G_Order_Scrub_Enclosing">
        <geometryInfo x="0.00000" y="0.00000" width="7.93750" height="2.68750"
        />
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_Call_Center" source="G_Call_Center"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.31250" width="7.87500"
           height="1.12500"/>
          <generalLayout verticalElasticity="variable"/>
          <visualSettings fillPattern="transparent"/>
          <frame name="M_G_Detail_Information">
            <geometryInfo x="0.00000" y="0.37500" width="7.81250"
             height="1.00000"/>
            <generalLayout verticalElasticity="variable"/>
            <visualSettings fillPattern="transparent"/>
            <repeatingFrame name="R_G_Details" source="G_Detail_Information"
             printDirection="down" minWidowRecords="1" columnMode="no">
              <geometryInfo x="0.00000" y="0.87500" width="7.68750"
               height="0.18750"/>
              <visualSettings fillPattern="transparent"/>
              <field name="F_csr_id" source="out_csr_id" alignment="start">
                <font face="Courier New" size="10"/>
                <geometryInfo x="3.50000" y="0.87500" width="1.00000"
                 height="0.18750"/>
              </field>
              <field name="F_count" source="count" alignment="end">
                <font face="Courier New" size="10"/>
                <geometryInfo x="5.81250" y="0.87500" width="0.93750"
                 height="0.18750"/>
              </field>
              <field name="F_last_name" source="user_name" alignment="start">
                <font face="Courier New" size="10"/>
                <geometryInfo x="0.68750" y="0.87500" width="2.62500"
                 height="0.18750"/>
              </field>
            </repeatingFrame>
            <text name="B_Total">
              <geometryInfo x="4.68750" y="1.18750" width="0.62500"
               height="0.18750"/>
              <visualSettings fillBackgroundColor="Totals"/>
              <textSegment>
                <font face="Courier New" size="10" bold="yes"/>
                <string>
                <![CDATA[Total:]]>
                </string>
              </textSegment>
            </text>
            <line name="B_6" arrow="none">
              <geometryInfo x="5.81250" y="1.12500" width="0.93750"
               height="0.00000"/>
              <visualSettings fillPattern="transparent" linePattern="solid"/>
              <points>
                <point x="5.81250" y="1.12500"/>
                <point x="6.75000" y="1.12500"/>
              </points>
            </line>
            <field name="F_Call_Center_Total" source="SumcountPerdescription"
             alignment="end">
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="5.87500" y="1.18750" width="0.87500"
               height="0.18750"/>
              <visualSettings fillBackgroundColor="Totals"/>
            </field>
            <field name="F_Call_Center" source="description" alignment="start"
              >
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="0.00000" y="0.50000" width="4.43750"
               height="0.25000"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
            </field>
          </frame>
        </repeatingFrame>
        <frame name="M_G_Total_Orders">
          <geometryInfo x="0.00000" y="1.62500" width="7.87500"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="lastPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_Grand_Total" source="SumcountPerReport"
           alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="5.87500" y="1.75000" width="0.87500"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
          </field>
          <text name="B_SumcountPerReport">
            <textSettings justify="center"/>
            <geometryInfo x="3.25000" y="1.75000" width="2.06250"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Total Orders Scrubbed:]]>
              </string>
            </textSegment>
          </text>
        </frame>
        <text name="B_5">
          <textSettings justify="end"/>
          <geometryInfo x="3.12500" y="2.37500" width="1.20837"
           height="0.16663"/>
          <visualSettings fillBackgroundColor="green"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_1_HDR">
          <geometryInfo x="0.00000" y="0.00000" width="7.50000"
           height="0.25000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_csr_id">
            <geometryInfo x="3.50000" y="0.00000" width="0.68750"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[User ID]]>
              </string>
            </textSegment>
          </text>
          <text name="B_count">
            <geometryInfo x="5.56250" y="0.00000" width="1.50000"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="TableHeading"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Orders Scrubbed]]>
              </string>
            </textSegment>
          </text>
          <text name="B_last_name">
            <geometryInfo x="0.75000" y="0.00000" width="0.75000"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[CSR Name]]>
              </string>
            </textSegment>
          </text>
          <line name="B_2" arrow="none">
            <geometryInfo x="0.75000" y="0.18750" width="0.68750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="0.75000" y="0.18750"/>
              <point x="1.43750" y="0.18750"/>
            </points>
          </line>
          <line name="B_3" arrow="none">
            <geometryInfo x="3.50000" y="0.18750" width="0.56250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="3.50000" y="0.18750"/>
              <point x="4.06250" y="0.18750"/>
            </points>
          </line>
          <line name="B_4" arrow="none">
            <geometryInfo x="5.56250" y="0.18750" width="1.31250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="5.56250" y="0.18750"/>
              <point x="6.87500" y="0.18750"/>
            </points>
          </line>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_OR$BODY_SECTION">
        <textSettings justify="center"/>
        <geometryInfo x="2.50000" y="0.25000" width="3.04211" height="0.37500"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Order Processing System
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Orders Scrubbed by CSR]]>
          </string>
        </textSegment>
      </text>
      <field name="F_StartDate" source="P_StartDate" spacing="single"
       alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="3.06250" y="0.62500" width="0.87500" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_EndDate" source="P_EndDate" spacing="single"
       alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="4.12500" y="0.62500" width="0.87500" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_Date" source="CurrentDate" formatMask="MM/DD/YYYY"
       spacing="single" alignment="start">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="0.37500" y="0.25000" width="1.18750" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_Time" source="CurrentDate" formatMask="HH:MI AM"
       spacing="single" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="7.06250" y="0.25000" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <text name="B_1">
        <textSettings justify="end"/>
        <geometryInfo x="3.94788" y="0.63538" width="0.11462" height="0.17712"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[-]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="beforereport">
      <textSource>
      <![CDATA[function BeforeReport return boolean is
  seq_number number;
begin
  select temp_scrub_list_sq.nextval into seq_number
  from dual;
  :CP_seq_number := seq_number;
  RPT.MASTER_SCRUB_LIST_PKG.MASTER_SCRUB_IN_OUT(:CP_seq_number, :P_Startdate, :P_Enddate);
  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="afterreport">
      <textSource>
      <![CDATA[function AfterReport return boolean is
begin
 delete from temp_scrub_list where report_id = :CP_Seq_number;
 commit; 
 return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <colorPalette>
    <color index="190" displayName="TextColor" value="#336699"/>
    <color index="191" displayName="TableHeading" value="#cccc99"/>
    <color index="192" displayName="TableCell" value="#f7f7e7"/>
    <color index="193" displayName="Totals" value="#ffffcc"/>
  </colorPalette>
  <reportPrivate defaultReportType="masterDetail" templateName="rwbeige"
   sectionTitle="Orders Scrubbed by CSR"/>
</report>
</rw:objects>
-->
<HTML>
<rw:style id="rwbeige">
<link rel="StyleSheet" type="text/css" href="css/rwbeige.css">
</rw:style>

<TITLE> Your Title </TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>
<BODY bgColor=#ffffff link=#000000 vLink=#000000>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ffffff rowSpan=2 vAlign=center width=188> 
      <P><IMG src="images/rwbeige_logo.gif" width="135" height="36"><br>
      </P>
      </TD>
    <TD bgColor=#ffffff height=40 vAlign=top><IMG alt="" height=1 src="images/stretch.gif" width=5></TD>
    <TD align=right bgColor=#ffffff vAlign=bottom>&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE bgColor=#ff0000 border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 class="OraColumnHeader">&nbsp; </TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 vAlign=top class="OraColumnHeader"><IMG alt="" border=0 height=17 src="images/topcurl.gif" width=30></TD>
    <TD vAlign=top width="100%">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD bgColor=#000000 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#9a9c9a height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#b3b4b3 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
		</TBODY>
	  </TABLE>
	</TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="29%" valign="top"> 
      <TABLE width="77%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_d_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation goes here </TD>
        </TR>
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_r_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation Item</TD>
        </TR>
      </TABLE>
    </TD>
    <TD width="71%">
      <DIV align="center">
	    <!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MG2GRPFR">
<rw:foreach id="R_G_2_1" src="G_2">
 <table class="OraTable">
 <caption class="OraHeader"> Orders Scrubbed by CSR <br>Description <rw:field id="F_description" src="description" breakLevel="R_G_2_1" breakValue="&nbsp;"> F_description </rw:field><br>
 </caption>
   <tr>
    <td valign="top">
    <table class="OraTable" summary="Orders Scrubbed by CSR">
     <!-- Header -->
     <thead>
      <tr>
       <th <rw:id id="HBcsrid" asArray="no"/> class="OraColumnHeader"> Csr Id </th>
       <th <rw:id id="HBcount" asArray="no"/> class="OraColumnHeader"> Count </th>
       <th <rw:id id="HBlastname" asArray="no"/> class="OraColumnHeader"> Last Name </th>
       <th <rw:id id="HBfirstname" asArray="no"/> class="OraColumnHeader"> First Name </th>
      </tr>
     </thead>
     <!-- Body -->
     <tbody>
      <rw:foreach id="R_G_1_1" src="G_1">
       <tr>
        <td <rw:headers id="HFcsrid" src="HBcsrid"/> class="OraCellText"><rw:field id="F_csr_id" src="csr_id" nullValue="&nbsp;"> F_csr_id </rw:field></td>
        <td <rw:headers id="HFcount" src="HBcount"/> class="OraCellNumber"><rw:field id="F_count" src="count" nullValue="&nbsp;"> F_count </rw:field></td>
        <td <rw:headers id="HFlastname" src="HBlastname"/> class="OraCellText"><rw:field id="F_last_name" src="last_name" nullValue="&nbsp;"> F_last_name </rw:field></td>
        <td <rw:headers id="HFfirstname" src="HBfirstname"/> class="OraCellText"><rw:field id="F_first_name" src="first_name" nullValue="&nbsp;"> F_first_name </rw:field></td>
       </tr>
      </rw:foreach>
     </tbody>
     <tr>
      <th class="OraTotalText"> &nbsp; </th>
      <th class="OraTotalText"> &nbsp; </th>
      <td <rw:headers id="HFSumcountPerdescription" src="HBcount"/> class="OraTotalNumber">Total: <rw:field id="F_SumcountPerdescription" src="SumcountPerdescription" nullValue="&nbsp;"> F_SumcountPerdescription </rw:field></td>
      <th class="OraTotalText"> &nbsp; </th>
      <th class="OraTotalText"> &nbsp; </th>
     </tr>
     <tr>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</rw:foreach>
<table class="OraTable" summary="Orders Scrubbed by CSR">
 <tr>
  <th class="OraTotalNumber"> Total: <rw:field id="F_SumcountPerReport" src="SumcountPerReport" nullValue="&nbsp;"> F_SumcountPerReport </rw:field></th>
 </tr>
</table>
</rw:dataArea> <!-- id="MG2GRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->

	  </DIV>
    </TD>
  </TR>
</TABLE>
<P><BR>
</P>
<TD align=middle vAlign=top width="100%">&nbsp;</TD>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ff0000 colSpan=2 class="OraColumnHeader"><IMG alt=" " border=0 height=4 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ffffff>&nbsp;</TD>
    <TD align=right bgColor=#ffffff>&nbsp;</TD>
  </TR></TBODY></TABLE>
</BODY>
</HTML>

 
<!--
</rw:report> 
-->

