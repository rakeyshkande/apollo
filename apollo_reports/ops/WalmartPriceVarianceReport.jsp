<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="WalmartPriceVarianceReport" DTDVersion="9.0.2.0.0">
  <xmlSettings xmlTag="WALMARTPRICEVARIANCEREPORT" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_Begin_date" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_END_DATE" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Walmart_Price_Variance">
      <select>
      <![CDATA[SELECT 
    v.POP_ORDER_ITEM_NUMBER AS "ORDER NUMBER",
    to_char(v.order_timestamp,'MM/DD/YY') as "ORDER DATE",
    to_char(v.order_timestamp,'hh:mi AM') as "TIME",
    v.product_id AS "FTD SKU #",
    v.FTD_PRICE AS "FTD Price", 
    v.POP_PRICE AS "Wal-Mart Net Price",
    v.POP_PRICE-v.FTD_PRICE as "Discount",
   (round  ( (( v.POP_PRICE-v.FTD_PRICE) / v.FTD_PRICE ) * -1,2 )) as "Discount Percent",
    v.FTD_COMPUTED_COST as "FTD Wholesale",
    v.PARTNER_COST as "Wal-Mart Wholesale",
    v.PARTNER_COST-v.FTD_COMPUTED_COST as "Cost Variance"
FROM POP.PRICE_VARIANCE v
WHERE PARTNER_ID='WLMTI'  
      AND v.order_timestamp >=TO_DATE(:P_BEGIN_DATE, 'MM/DD/YYYY ')
      AND v.order_timestamp < TO_DATE(:P_END_DATE,'MM/DD/YYYY') +1
ORDER BY 
     v.pop_order_item_number;
]]>
      </select>
      <displayInfo x="0.97571" y="0.05518" width="2.13940" height="0.19995"/>
      <group name="G_Report_Detail">
        <displayInfo x="0.82788" y="0.75476" width="2.44836" height="2.31055"
        />
        <dataItem name="ORDER_NUMBER" datatype="vchar2" columnOrder="13"
         width="32" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="v.POP_ORDER_ITEM_NUMBER"
           descriptiveExpression="ORDER NUMBER" order="1" width="32"/>
        </dataItem>
        <dataItem name="ORDER_DATE" datatype="vchar2" columnOrder="14"
         width="75" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor
           expression="to_char ( v.order_timestamp , &apos;MM/DD/YY&apos; )"
           descriptiveExpression="ORDER DATE" order="2" width="75"/>
        </dataItem>
        <dataItem name="TIME" datatype="vchar2" columnOrder="15" width="75"
         defaultWidth="100000" defaultHeight="10000" breakOrder="none">
          <dataDescriptor
           expression="to_char ( v.order_timestamp , &apos;hh:mi AM&apos; )"
           descriptiveExpression="TIME" order="3" width="75"/>
        </dataItem>
        <dataItem name="FTD_SKU_#" datatype="vchar2" columnOrder="16"
         defaultWidth="100000" defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="v.product_id"
           descriptiveExpression="FTD SKU #" order="4" width="10"/>
        </dataItem>
        <dataItem name="FTD_Price" oracleDatatype="number" columnOrder="17"
         width="22" defaultWidth="90000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="v.FTD_PRICE"
           descriptiveExpression="FTD Price" order="5" oracleDatatype="number"
           width="22" scale="2" precision="12"/>
        </dataItem>
        <dataItem name="Wal_Mart_Net_Price" oracleDatatype="number"
         columnOrder="18" width="22" defaultWidth="90000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="v.POP_PRICE"
           descriptiveExpression="Wal-Mart Net Price" order="6"
           oracleDatatype="number" width="22" scale="2" precision="12"/>
        </dataItem>
        <dataItem name="Discount" oracleDatatype="number" columnOrder="19"
         width="22" defaultWidth="90000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="v.POP_PRICE - v.FTD_PRICE"
           descriptiveExpression="Discount" order="7" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="Discount_Percent" oracleDatatype="number"
         columnOrder="20" width="22" defaultWidth="90000"
         defaultHeight="10000" breakOrder="none">
          <xmlSettings xmlTag="DISCOUNT1"/>
          <dataDescriptor
           expression="( round ( ( ( v.POP_PRICE - v.FTD_PRICE ) / v.FTD_PRICE ) * - 1 , 2 ) )"
           descriptiveExpression="Discount Percent" order="8"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="FTD_Wholesale" oracleDatatype="number"
         columnOrder="21" width="22" defaultWidth="90000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="v.FTD_COMPUTED_COST"
           descriptiveExpression="FTD Wholesale" order="9"
           oracleDatatype="number" width="22" scale="2" precision="12"/>
        </dataItem>
        <dataItem name="Wal_Mart_Wholesale" oracleDatatype="number"
         columnOrder="22" width="22" defaultWidth="90000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="v.PARTNER_COST"
           descriptiveExpression="Wal-Mart Wholesale" order="10"
           oracleDatatype="number" width="22" scale="2" precision="12"/>
        </dataItem>
        <dataItem name="Cost_Variance" oracleDatatype="number"
         columnOrder="23" width="22" defaultWidth="90000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="v.PARTNER_COST - v.FTD_COMPUTED_COST"
           descriptiveExpression="Cost Variance" order="11"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_Report_Title" source="cf_report_titleformula"
     datatype="character" width="80" precision="10" breakOrder="none">
      <displayInfo x="3.56262" y="0.93750" width="1.45825" height="0.19995"/>
    </formula>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="12.93750" height="8.12500">
      <location x="0.00000" y="0.37500"/>
      <frame name="M_G_Walmart_Price_Enclosing">
        <geometryInfo x="0.06250" y="0.50000" width="12.87500"
         height="1.18750"/>
        <generalLayout verticalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
        <visualSettings fillPattern="transparent"/>
        <text name="B_1">
          <textSettings justify="center"/>
          <geometryInfo x="0.25000" y="0.75000" width="0.62500"
           height="0.37500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:P_SOURCE_CODE != &apos;All&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" exception="2" lowValue="All" conjunction="1"
              />
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Order Number]]>
            </string>
          </textSegment>
        </text>
        <text name="B_3">
          <textSettings justify="center"/>
          <geometryInfo x="2.37500" y="0.93750" width="0.56250"
           height="0.18750"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_3formattrigger"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Time ]]>
            </string>
          </textSegment>
        </text>
        <text name="B_4">
          <textSettings justify="center"/>
          <geometryInfo x="3.37500" y="0.75000" width="0.62500"
           height="0.37500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:P_VENDOR_CODE != &apos;All&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" exception="2" lowValue="All" conjunction="1"
              />
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[FTD
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[SKU # ]]>
            </string>
          </textSegment>
        </text>
        <text name="B_7">
          <textSettings justify="center"/>
          <geometryInfo x="5.50000" y="0.62500" width="0.81250"
           height="0.50000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Wal-Mart
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Net
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Price]]>
            </string>
          </textSegment>
        </text>
        <repeatingFrame name="R_G_Detail" source="G_Report_Detail"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.06250" y="1.18750" width="12.81250"
           height="0.31250"/>
          <generalLayout verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_Order_Number" source="ORDER_NUMBER" spacing="single"
           alignment="center">
            <font face="Courier New" size="10"/>
            <geometryInfo x="0.06250" y="1.25000" width="0.93750"
             height="0.18750"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_SOURCE_CODE != &apos;All&apos;)">
                  <font face="Courier New" size="10"/>
                  <formatVisualSettings fillPattern="transparent"/>
                <cond name="first" exception="2" lowValue="All"
                 conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Order_Time" source="TIME" spacing="single"
           alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="2.25000" y="1.25000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_FTD_Price" source="FTD_Price" formatMask="N,NNN.00"
           spacing="single" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="4.18750" y="1.25000" width="0.93750"
             height="0.18750"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_VENDOR_CODE != &apos;All&apos;)">
                  <font face="Courier New" size="10"/>
                  <formatVisualSettings fillPattern="transparent"/>
                <cond name="first" exception="2" lowValue="All"
                 conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_SKU_Number" source="FTD_SKU_#" alignment="center">
            <font face="Courier New" size="10"/>
            <geometryInfo x="3.37500" y="1.25000" width="0.62500"
             height="0.18750"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_VENDOR_CODE != &apos;All&apos;)">
                  <font face="Courier New" size="10"/>
                  <formatVisualSettings fillPattern="transparent"/>
                <cond name="first" exception="2" lowValue="All"
                 conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_WM_Net_Price" source="Wal_Mart_Net_Price"
           formatMask="N,NNN.00" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="5.50000" y="1.25000" width="0.75000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Cost_Variance" source="Cost_Variance"
           formatMask="(NNN,NN0.00)" spacing="single" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="11.06250" y="1.25000" width="0.93750"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Order_Date" source="ORDER_DATE" alignment="center">
            <font face="Courier New" size="10"/>
            <geometryInfo x="1.25000" y="1.25000" width="0.75000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_WM_Wholesale" source="Wal_Mart_Wholesale"
           formatMask="N,NNN.00" spacing="single" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="9.81250" y="1.25000" width="0.87500"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_FTD_Wholesale" source="FTD_Wholesale"
           formatMask="N,NNN.00" spacing="single" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="8.62500" y="1.25000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Disc_Percent" source="Discount_Percent"
           formatMask="(NNNNNNNNNN.00)" spacing="single" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="7.50000" y="1.25000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Discount" source="Discount" formatMask="(NN,NNN.00)"
           spacing="single" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="6.56250" y="1.25000" width="0.68750"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <text name="B_6">
          <textSettings justify="center"/>
          <geometryInfo x="4.43750" y="0.75000" width="0.62500"
           height="0.37500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:P_VENDOR_CODE != &apos;All&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" exception="2" lowValue="All" conjunction="1"
              />
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[FTD
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Price]]>
            </string>
          </textSegment>
        </text>
        <text name="B_5">
          <textSettings justify="center"/>
          <geometryInfo x="8.62500" y="0.75000" width="0.87500"
           height="0.37500"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[FTD
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Wholesale
]]>
            </string>
          </textSegment>
        </text>
        <text name="B_8">
          <textSettings justify="center"/>
          <geometryInfo x="1.31250" y="0.75000" width="0.61462"
           height="0.37500"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_8formattrigger"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Order
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Date]]>
            </string>
          </textSegment>
        </text>
        <line name="B_9" arrow="none">
          <geometryInfo x="0.06250" y="1.12500" width="0.87500"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="0.06250" y="1.12500"/>
            <point x="0.93750" y="1.12500"/>
          </points>
        </line>
        <line name="B_10" arrow="none">
          <geometryInfo x="1.25000" y="1.12500" width="0.75000"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="1.25000" y="1.12500"/>
            <point x="2.00000" y="1.12500"/>
          </points>
        </line>
        <line name="B_12" arrow="none">
          <geometryInfo x="2.37500" y="1.12500" width="0.50000"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="2.37500" y="1.12500"/>
            <point x="2.87500" y="1.12500"/>
          </points>
        </line>
        <line name="B_14" arrow="none">
          <geometryInfo x="4.25000" y="1.12500" width="0.87500"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="4.25000" y="1.12500"/>
            <point x="5.12500" y="1.12500"/>
          </points>
        </line>
        <line name="B_15" arrow="none">
          <geometryInfo x="5.50000" y="1.12500" width="0.81250"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="5.50000" y="1.12500"/>
            <point x="6.31250" y="1.12500"/>
          </points>
        </line>
        <line name="B_16" arrow="none">
          <geometryInfo x="3.31250" y="1.12500" width="0.68750"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="3.31250" y="1.12500"/>
            <point x="4.00000" y="1.12500"/>
          </points>
        </line>
        <line name="B_17" arrow="none">
          <geometryInfo x="8.62500" y="1.12500" width="0.87500"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="8.62500" y="1.12500"/>
            <point x="9.50000" y="1.12500"/>
          </points>
        </line>
        <text name="B_2">
          <textSettings justify="center"/>
          <geometryInfo x="6.50000" y="0.93750" width="0.87500"
           height="0.18750"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Discount]]>
            </string>
          </textSegment>
        </text>
        <line name="B_11" arrow="none">
          <geometryInfo x="6.56250" y="1.12500" width="0.75000"
           height="0.00000"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="6.56250" y="1.12500"/>
            <point x="7.31250" y="1.12500"/>
          </points>
        </line>
        <text name="B_18">
          <textSettings justify="center"/>
          <geometryInfo x="7.56250" y="0.75000" width="0.77087"
           height="0.37500"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Discount
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[%]]>
            </string>
          </textSegment>
        </text>
        <line name="B_19" arrow="none">
          <geometryInfo x="7.56250" y="1.12500" width="0.75000"
           height="0.00000"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="7.56250" y="1.12500"/>
            <point x="8.31250" y="1.12500"/>
          </points>
        </line>
        <text name="B_20">
          <textSettings justify="center"/>
          <geometryInfo x="9.87500" y="0.75000" width="0.81250"
           height="0.37500"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Wal-Mart
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Wholesale]]>
            </string>
          </textSegment>
        </text>
        <line name="B_21" arrow="none">
          <geometryInfo x="9.81250" y="1.12500" width="0.93750"
           height="0.00000"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="9.81250" y="1.12500"/>
            <point x="10.75000" y="1.12500"/>
          </points>
        </line>
        <text name="B_22">
          <textSettings justify="center"/>
          <geometryInfo x="11.25000" y="0.75000" width="0.70837"
           height="0.37500"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Cost
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Variance]]>
            </string>
          </textSegment>
        </text>
        <line name="B_23" arrow="none">
          <geometryInfo x="11.12500" y="1.12500" width="0.87500"
           height="0.00000"/>
          <visualSettings fillPattern="transparent" linePattern="solid"/>
          <points>
            <point x="11.12500" y="1.12500"/>
            <point x="12.00000" y="1.12500"/>
          </points>
        </line>
      </frame>
      <text name="B_13">
        <textSettings justify="center"/>
        <geometryInfo x="4.93750" y="0.06250" width="2.62500" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="enclosingObject"/>
        <textSegment>
          <font face="Courier New" size="10"/>
          <string>
          <![CDATA[Wal-Mart Price Variance Report]]>
          </string>
        </textSegment>
      </text>
    </body>
  </section>
  </layout>
  <programUnits>
    <function name="cf_report_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Report_TitleFormula return Char is
  lv_title char (40);
begin
  lv_title := 'Wal-Mart Price Variance Edit Report';
  
  return lv_title;
end;]]>
      </textSource>
    </function>
    <function name="b_8formattrigger">
      <textSource>
      <![CDATA[function B_8FormatTrigger return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
