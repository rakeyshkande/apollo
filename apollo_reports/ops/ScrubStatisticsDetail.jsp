<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ScrubStatisticsDetail" DTDVersion="9.0.2.0.0">
  <xmlSettings xmlTag="SCRUBSTATISTICSDETAILXXX" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <reportHtmlEscapes>
    <beforeReportHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
]]>
    </beforeReportHtmlEscape>
    <beforeFormHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
<form method=post action="_action_">
<input name="hidden_run_parameters" type=hidden value="_hidden_">
<font color=red><!--error--></font>
<center>
<p><table border=0 cellspacing=0 cellpadding=0>
<tr>
<td><input type=submit></td>
<td width=15>
<td><input type=reset></td>
</tr>
</table>
<p><hr><p>
]]>
    </beforeFormHtmlEscape>
  </reportHtmlEscapes>
  <data>
    <userParameter name="P_StartDate" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_EndDate" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Scrub_Stat_Detail">
      <select>
      <![CDATA[select distinct UPPER(svr.reason) as sort_reason,
sv.external_order_number,
svr.error_value,
svr.reason
from frp.stats_validation_reasons svr,
        frp.stats_validation sv,
        scrub.orders ord,
        frp.stats_osp stats
where svr.stats_validation_id = sv.stats_validation_id
and sv.relator = ord.order_guid
and trunc(sv.timestamp) >= to_date(:P_StartDate,'MM/DD/YYYY') 
and trunc(sv.timestamp) <= to_date(:P_EndDate,'MM/DD/YYYY')
and sv.external_order_number = stats.external_order_number
and stats.item_state = '3000'
and stats.test_order_flag = 'N'
and UPPER(ord.order_origin) <> 'BULK'
order by 
svr.reason,
sv.external_order_number desc
]]>
      </select>
      <displayInfo x="0.96948" y="0.31519" width="2.05200" height="0.19995"/>
      <group name="G_Validation_Error">
        <displayInfo x="0.89087" y="0.99695" width="2.23059" height="0.60156"
        />
        <dataItem name="sort_reason" datatype="vchar2" columnOrder="16"
         width="500" defaultWidth="100000" defaultHeight="10000">
          <dataDescriptor expression="UPPER ( svr.reason )"
           descriptiveExpression="SORT_REASON" order="1" width="500"/>
        </dataItem>
        <dataItem name="reason" datatype="vchar2" columnOrder="13" width="500"
         defaultWidth="1000000" defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="svr.reason"
           descriptiveExpression="REASON" order="4" width="500"/>
        </dataItem>
      </group>
      <group name="G_Confirmation_Number">
        <displayInfo x="0.91162" y="1.94995" width="2.19763" height="0.60156"
        />
        <dataItem name="external_order_number" datatype="vchar2"
         columnOrder="14" width="100" defaultWidth="200000"
         defaultHeight="10000" breakOrder="descending">
          <dataDescriptor expression="sv.external_order_number"
           descriptiveExpression="EXTERNAL_ORDER_NUMBER" order="2" width="100"
          />
        </dataItem>
        <dataItem name="error_value" datatype="vchar2" columnOrder="15"
         width="500" defaultWidth="1000000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="svr.error_value"
           descriptiveExpression="ERROR_VALUE" order="3" width="500"/>
        </dataItem>
      </group>
    </dataSource>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="13.50000" height="6.50000">
      <location x="0.00000"/>
      <frame name="M_G_Scrub_Stat_Detail_Enclosin">
        <geometryInfo x="0.00000" y="0.00000" width="13.50000"
         height="1.56250"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_Validation_Error"
         source="G_Validation_Error" printDirection="down" minWidowRecords="1"
         columnMode="no">
          <geometryInfo x="0.00000" y="0.62500" width="13.50000"
           height="0.37500"/>
          <generalLayout verticalElasticity="variable"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <repeatingFrame name="R_G_Confirmation_Number"
           source="G_Confirmation_Number" printDirection="down"
           minWidowRecords="1" columnMode="no">
            <geometryInfo x="6.18750" y="0.75000" width="7.31250"
             height="0.18750"/>
            <generalLayout verticalElasticity="variable"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_external_order_number"
             source="external_order_number" alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="6.25000" y="0.75000" width="0.93750"
               height="0.12500"/>
            </field>
            <field name="F_error_value" source="error_value" alignment="start"
              >
              <font face="Courier New" size="10"/>
              <geometryInfo x="7.37500" y="0.75000" width="6.12500"
               height="0.12500"/>
              <generalLayout verticalElasticity="variable"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </repeatingFrame>
          <field name="F_reason" source="reason" alignment="start">
            <font face="Courier New" size="10"/>
            <geometryInfo x="0.06250" y="0.75000" width="6.06250"
             height="0.12500"/>
            <generalLayout verticalElasticity="variable"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
          </field>
        </repeatingFrame>
        <text name="B_8">
          <textSettings justify="center"/>
          <geometryInfo x="6.37500" y="1.31250" width="1.31250"
           height="0.16663"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_Headings">
          <geometryInfo x="0.00000" y="0.06250" width="13.39587"
           height="0.43750"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_2">
            <geometryInfo x="0.12500" y="0.25000" width="2.06250"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="cyan"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Scrub Validation Error]]>
              </string>
            </textSegment>
          </text>
          <line name="B_3" arrow="none">
            <geometryInfo x="0.12500" y="0.43750" width="1.81250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent"
             fillBackgroundColor="cyan" linePattern="solid"/>
            <points>
              <point x="0.12500" y="0.43750"/>
              <point x="1.93750" y="0.43750"/>
            </points>
          </line>
          <text name="B_4">
            <textSettings justify="center"/>
            <geometryInfo x="6.18750" y="0.06250" width="1.12500"
             height="0.37500"/>
            <visualSettings fillBackgroundColor="cyan"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Confirmation Number]]>
              </string>
            </textSegment>
          </text>
          <line name="B_5" arrow="none">
            <geometryInfo x="6.18750" y="0.43750" width="1.00342"
             height="0.00000"/>
            <visualSettings fillPattern="transparent"
             fillBackgroundColor="cyan" linePattern="solid"/>
            <points>
              <point x="6.18750" y="0.43750"/>
              <point x="7.19092" y="0.43750"/>
            </points>
          </line>
          <line name="B_7" arrow="none">
            <geometryInfo x="7.37500" y="0.43750" width="0.93750"
             height="0.00000"/>
            <visualSettings fillBackgroundColor="cyan" linePattern="solid"/>
            <points>
              <point x="7.37500" y="0.43750"/>
              <point x="8.31250" y="0.43750"/>
            </points>
          </line>
          <text name="B_6">
            <textSettings justify="center"/>
            <geometryInfo x="7.31250" y="0.25000" width="1.06250"
             height="0.22913"/>
            <visualSettings fillBackgroundColor="cyan"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Field Value]]>
              </string>
            </textSegment>
          </text>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_OR$BODY_SECTION">
        <textSettings justify="center"/>
        <geometryInfo x="5.56250" y="0.25000" width="2.87561" height="0.37500"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Order Processing System
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Scrub Statistics Detail Report]]>
          </string>
        </textSegment>
      </text>
      <field name="F_Date" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="start">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="0.25000" y="0.25000" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings linePattern="solid" lineForegroundColor="white"
         lineBackgroundColor="black"/>
      </field>
      <field name="F_Time" source="CurrentDate" formatMask="HH:MI AM"
       alignment="start">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="11.68750" y="0.12500" width="1.18750"
         height="0.18750"/>
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings linePattern="solid" lineForegroundColor="white"
         lineBackgroundColor="black"/>
      </field>
      <field name="F_StartDate" source="P_StartDate" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="5.87500" y="0.62500" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings linePattern="solid" lineForegroundColor="white"
         lineBackgroundColor="black"/>
      </field>
      <field name="F_EndDate" source="P_EndDate" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="7.18750" y="0.62500" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings linePattern="solid" lineForegroundColor="white"
         lineBackgroundColor="black"/>
      </field>
      <text name="B_1">
        <textSettings justify="center"/>
        <geometryInfo x="6.90625" y="0.62500" width="0.21997" height="0.13660"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings lineForegroundColor="white"
         lineBackgroundColor="black"/>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[-]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <colorPalette>
    <color index="190" displayName="TextColor" value="#ffffff"/>
    <color index="191" displayName="custom6" value="#93065d"/>
    <color index="192" displayName="Background2" value="#e4c1d6"/>
    <color index="193" displayName="Background3" value="#c982ae"/>
  </colorPalette>
  <reportPrivate defaultReportType="tabular" templateName="rwwine"
   sectionTitle="Scrub Statistics Detail Report"/>
</report>
</rw:objects>
-->

<HTML>
<HEAD>
<rw:style id="rwwine">
<link rel="StyleSheet" type="text/css" href="css/rwwine.css">
</rw:style>

<TITLE> Your Title </TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>

<BODY bgColor=#ffffff link=#000000 vLink=#000000>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ffffff rowSpan=2 vAlign=center width=188> 
      <p><IMG src="images/rwwine_logo.gif" width="135" height="36"><br>
      </p>
      </TD>
    <TD bgColor=#ffffff height=40 vAlign=top><IMG alt="" height=1 src="images/stretch.gif" width=5></TD>
    <TD align=right bgColor=#ffffff vAlign=bottom>&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE bgColor=#ff0000 border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD class="OraColumnHeader">&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 vAlign=top class="OraColumnHeader"><IMG alt="" border=0 height=17 src="images/topcurl.gif" width=30></TD>
    <TD vAlign=top width="100%">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD bgColor=#000000 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#9a9c9a height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#b3b4b3 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="29%" valign="top"> 
      <TABLE width="77%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/wine_d_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation goes here </TD>
        </TR>
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/wine_r_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation Item</TD>
        </TR>
      </TABLE>
    </TD>
    <TD width="71%">
      <DIV align="center">
	    <!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MGexternalordernumberGRPF">
<table class="OraTable">
<caption> Scrub Statistics Detail Report </caption>
 <!-- Header -->
 <thead>
  <tr>
   <th <rw:id id="HBexternalordernumber" asArray="no"/> class="OraColumnHeader"> External Order Number </th>
   <th <rw:id id="HBreason" asArray="no"/> class="OraColumnHeader"> Reason </th>
   <th <rw:id id="HBerrorvalue" asArray="no"/> class="OraColumnHeader"> Error Value </th>
  </tr>
 </thead>
 <!-- Body -->
 <tbody>
  <rw:foreach id="R_G_external_order_number_1" src="G_external_order_number">
   <tr>
    <td <rw:headers id="HFexternalordernumber" src="HBexternalordernumber"/> class="OraCellText"><rw:field id="F_external_order_number" src="external_order_number" nullValue="&nbsp;"> F_external_order_number </rw:field></td>
    <td <rw:headers id="HFreason" src="HBreason"/> class="OraCellText"><rw:field id="F_reason" src="reason" nullValue="&nbsp;"> F_reason </rw:field></td>
    <td <rw:headers id="HFerrorvalue" src="HBerrorvalue"/> class="OraCellText"><rw:field id="F_error_value" src="error_value" nullValue="&nbsp;"> F_error_value </rw:field></td>
   </tr>
  </rw:foreach>
 </tbody>
</table>
</rw:dataArea> <!-- id="MGexternalordernumberGRPF" -->
<!-- End of Data Area Generated by Reports Developer -->

	  </DIV>
    </TD>
  </TR>
</TABLE>
<P><BR>
</P>
<TD align=middle vAlign=top width="100%">&nbsp;</TD>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ff0000 colSpan=2 class="OraColumnHeader"><IMG alt=" " border=0 height=4 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ffffff>&nbsp;</TD>
    <TD align=right bgColor=#ffffff>&nbsp;</TD>
  </TR>
  </TBODY>
</TABLE>
<p>&nbsp;</p>
</BODY>
</HTML>
<!--
</rw:report>
-->
