<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="CSRProductivity" DTDVersion="9.0.2.0.0"
 beforeReportTrigger="beforereport" afterReportTrigger="afterreport">
  <xmlSettings xmlTag="CSRPRODUCTIVITY" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <reportHtmlEscapes>
    <beforeReportHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
]]>
    </beforeReportHtmlEscape>
    <beforeFormHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
<form method=post action="_action_">
<input name="hidden_run_parameters" type=hidden value="_hidden_">
<font color=red><!--error--></font>
<center>
<p><table border=0 cellspacing=0 cellpadding=0>
<tr>
<td><input type=submit></td>
<td width=15>
<td><input type=reset></td>
</tr>
</table>
<p><hr><p>
]]>
    </beforeFormHtmlEscape>
  </reportHtmlEscapes>
  <data>
    <userParameter name="P_StartDate" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_EndDate" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_CSR_Scrub_Productivity">
      <select>
      <![CDATA[select
  stats.out_csr_id,
  users.last_name ||', '|| users.first_name as CSR_Name,
  call_center.description,
  count(decode(stats.out_status,'2004','1')) Removed,
  to_min_sec(avg(decode(stats.out_status,'2004',(time_diff(stats.in_timestamp,
  stats.out_timestamp))))) Removed_Handle_Time,
  count(decode(stats.out_status,'2005','1')) Pending,
  to_min_sec(avg(decode(stats.out_status,'2007',(time_diff(stats.in_timestamp,
  stats.out_timestamp))))) Scrubbed_Handle_Time,
  count(decode(stats.out_status,'2007','1')) Scrubbed
from
  rpt.temp_scrub_list stats,
  aas.identity identity,
  aas.users users,
  ftd_apps.call_center call_center
where
  stats.report_id = :CP_Seq_number
  and stats.out_item_state IN ('3006','3008','3010')
  and stats.out_status in ('2004','2005','2007')
  and stats.out_test_order_flag = 'N'
  and stats.in_status <> stats.out_status
  and upper(stats.out_csr_id) = upper(identity.identity_id)
  and identity.user_id = users.user_id
  and users.call_center_id = call_center.call_center_id
  and trunc(stats.in_timestamp) >= to_date(:P_StartDate, 'MM/DD/YYYY')
  and trunc(stats.in_timestamp) <= to_date(:P_EndDate, 'MM/DD/YYYY')
group by
  call_center.description,
  users.last_name,
  users.first_name,
  stats.out_csr_id
  





]]>
      </select>
      <displayInfo x="0.95837" y="0.33398" width="1.94788" height="0.19995"/>
      <group name="G_Center_Group">
        <displayInfo x="0.80225" y="0.93750" width="2.27087" height="1.11426"
        />
        <dataItem name="description" datatype="vchar2" columnOrder="13"
         width="50" defaultWidth="300000" defaultHeight="10000">
          <dataDescriptor expression="call_center.description"
           descriptiveExpression="DESCRIPTION" order="3" width="50"/>
        </dataItem>
        <summary name="SumScrubbedPerdescription" source="Scrubbed"
         function="sum" width="22" precision="38" reset="G_Center_Group"
         compute="report">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="SumRemovedPerdescription" source="Removed"
         function="sum" width="22" precision="38" reset="G_Center_Group"
         compute="report">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="SumPendingPerdescription" source="Pending"
         function="sum" width="22" precision="38" reset="G_Center_Group"
         compute="report">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_User_Count" source="out_csr_id" function="count"
         width="20" precision="10" reset="G_Center_Group" compute="report">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_Detail">
        <displayInfo x="1.01392" y="2.70740" width="1.84143" height="1.45605"
        />
        <dataItem name="out_csr_id" datatype="vchar2" columnOrder="18"
         width="100" defaultWidth="100000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor expression="stats.out_csr_id"
           descriptiveExpression="OUT_CSR_ID" order="1" width="100"/>
        </dataItem>
        <dataItem name="Removed_Handle_Time" datatype="vchar2"
         columnOrder="19" width="4000" defaultWidth="100000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor
           expression="to_min_sec ( avg ( decode ( stats.out_status , &apos;2004&apos; , ( time_diff ( stats.in_timestamp , stats.out_timestamp ) ) ) ) )"
           descriptiveExpression="REMOVED_HANDLE_TIME" order="5" width="4000"
          />
        </dataItem>
        <dataItem name="Scrubbed_Handle_Time" datatype="vchar2"
         columnOrder="20" width="4000" defaultWidth="100000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor
           expression="to_min_sec ( avg ( decode ( stats.out_status , &apos;2007&apos; , ( time_diff ( stats.in_timestamp , stats.out_timestamp ) ) ) ) )"
           descriptiveExpression="SCRUBBED_HANDLE_TIME" order="7" width="4000"
          />
        </dataItem>
        <dataItem name="CSR_Name" datatype="vchar2" columnOrder="17"
         width="542" defaultWidth="100000" defaultHeight="10000">
          <dataDescriptor
           expression="users.last_name || &apos;, &apos; || users.first_name"
           descriptiveExpression="CSR_NAME" order="2" width="542"/>
        </dataItem>
        <dataItem name="Scrubbed" oracleDatatype="number" columnOrder="16"
         width="22" defaultWidth="90000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor
           expression="count ( decode ( stats.out_status , &apos;2007&apos; , &apos;1&apos; ) )"
           descriptiveExpression="SCRUBBED" order="8" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="Removed" oracleDatatype="number" columnOrder="14"
         width="22" defaultWidth="90000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor
           expression="count ( decode ( stats.out_status , &apos;2004&apos; , &apos;1&apos; ) )"
           descriptiveExpression="REMOVED" order="4" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="Pending" oracleDatatype="number" columnOrder="15"
         width="22" defaultWidth="90000" defaultHeight="10000"
         breakOrder="none">
          <dataDescriptor
           expression="count ( decode ( stats.out_status , &apos;2005&apos; , &apos;1&apos; ) )"
           descriptiveExpression="PENDING" order="6" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <placeholder name="CP_Seq_number" datatype="character" width="32"
     precision="20">
      <displayInfo x="3.62500" y="2.02087" width="1.37500" height="0.19995"/>
    </placeholder>
    <summary name="SumPendingPerReport" source="Pending" function="sum"
     width="22" precision="38" reset="report" compute="report">
      <displayInfo x="3.80811" y="0.23950" width="1.49402" height="0.19995"/>
    </summary>
    <summary name="SumRemovedPerReport" source="Removed" function="sum"
     width="22" precision="38" reset="report" compute="report">
      <displayInfo x="3.89465" y="0.66663" width="1.42920" height="0.19995"/>
    </summary>
    <summary name="SumScrubbedPerReport" source="Scrubbed" function="sum"
     width="22" precision="38" reset="report" compute="report">
      <displayInfo x="3.90625" y="1.08325" width="1.38538" height="0.19995"/>
    </summary>
    <summary name="CS_Total_User_ID_Count" source="CS_User_Count"
     function="sum" width="20" precision="10" reset="report" compute="report">
      <displayInfo x="3.97913" y="1.59375" width="1.38550" height="0.21875"/>
    </summary>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="13.43750" height="6.50000">
      <location x="0.00000"/>
      <frame name="M_G_CSR_Scrub_Enclosing">
        <geometryInfo x="0.00000" y="0.00000" width="13.37500"
         height="2.50000"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_Call_Center" source="G_Center_Group"
         printDirection="down" maxRecordsPerPage="1" minWidowRecords="1"
         columnMode="no">
          <geometryInfo x="0.00000" y="0.62500" width="13.00000"
           height="1.06250"/>
          <generalLayout verticalElasticity="variable"/>
          <visualSettings fillPattern="transparent"/>
          <frame name="M_G_1_GRPFR">
            <geometryInfo x="0.00000" y="0.62500" width="13.00000"
             height="0.37500"/>
            <generalLayout verticalElasticity="variable"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_Call_Center" source="description" alignment="start"
              >
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="0.06250" y="0.75000" width="1.50000"
               height="0.18750"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
            </field>
          </frame>
          <repeatingFrame name="R_G_Detail" source="G_Detail"
           printDirection="down" minWidowRecords="1" columnMode="no">
            <geometryInfo x="0.06250" y="1.06250" width="12.93750"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_CSR_Name" source="CSR_Name" alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="0.12500" y="1.06250" width="3.62500"
               height="0.18750"/>
            </field>
            <field name="F_csr_id" source="out_csr_id" alignment="start">
              <font face="Courier New" size="10"/>
              <geometryInfo x="4.00000" y="1.06250" width="1.81250"
               height="0.18750"/>
            </field>
            <field name="F_Scrubbed" source="Scrubbed" alignment="end">
              <font face="Courier New" size="10"/>
              <geometryInfo x="5.93750" y="1.06250" width="0.68750"
               height="0.18750"/>
            </field>
            <field name="F_Removed" source="Removed" alignment="end">
              <font face="Courier New" size="10"/>
              <geometryInfo x="9.00000" y="1.06250" width="0.68750"
               height="0.18750"/>
            </field>
            <field name="F_Pending" source="Pending" alignment="end">
              <font face="Courier New" size="10"/>
              <geometryInfo x="11.81250" y="1.06250" width="0.68750"
               height="0.18750"/>
            </field>
            <field name="F_scrub_handle_time" source="Scrubbed_Handle_Time"
             spacing="single" alignment="end">
              <font face="Courier New" size="10"/>
              <geometryInfo x="7.62500" y="1.06250" width="0.75000"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException
                   label="(:Scrubbed_Handle_Time LIKE &apos;:&apos;)">
                    <font face="Courier New" size="10"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="Scrubbed_Handle_Time"
                   exception="9" lowValue="&apos;:&apos;" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_scrub_handle_timeformattrigg"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_remove_handle_time" source="Removed_Handle_Time"
             spacing="single" alignment="end">
              <font face="Courier New" size="10"/>
              <geometryInfo x="10.56250" y="1.06250" width="0.75000"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException
                   label="(:Removed_Handle_Time LIKE &apos;:&apos;)">
                    <font face="Courier New" size="10"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="Removed_Handle_Time"
                   exception="9" lowValue="&apos;:&apos;" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_remove_handle_timeformattrig"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </repeatingFrame>
          <frame name="M_G_1_FTR">
            <geometryInfo x="0.00000" y="1.31250" width="13.00000"
             height="0.31250"/>
            <advancedLayout printObjectOnPage="lastPage"
             basePrintingOn="anchoringObject"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_SumScrubbedPerdescription"
             source="SumScrubbedPerdescription" alignment="end">
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="5.93750" y="1.43750" width="0.68750"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="Totals"/>
            </field>
            <field name="F_SumRemovedPerdescription"
             source="SumRemovedPerdescription" alignment="end">
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="9.00000" y="1.43750" width="0.68750"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="Totals"/>
            </field>
            <field name="F_SumPendingPerdescription"
             source="SumPendingPerdescription" alignment="end">
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="11.81250" y="1.43750" width="0.68750"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="Totals"/>
            </field>
            <text name="B_Total">
              <textSettings justify="end"/>
              <geometryInfo x="3.00000" y="1.43750" width="0.68750"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="Totals"/>
              <textSegment>
                <font face="Courier New" size="10" bold="yes"/>
                <string>
                <![CDATA[Totals:]]>
                </string>
              </textSegment>
            </text>
            <line name="B_12" arrow="none">
              <geometryInfo x="5.93750" y="1.37500" width="0.68750"
               height="0.00000"/>
              <visualSettings fillPattern="transparent" linePattern="solid"/>
              <points>
                <point x="5.93750" y="1.37500"/>
                <point x="6.62500" y="1.37500"/>
              </points>
            </line>
            <line name="B_13" arrow="none">
              <geometryInfo x="9.12500" y="1.37500" width="0.68750"
               height="0.00000"/>
              <visualSettings fillPattern="transparent" linePattern="solid"/>
              <points>
                <point x="9.12500" y="1.37500"/>
                <point x="9.81250" y="1.37500"/>
              </points>
            </line>
            <line name="B_14" arrow="none">
              <geometryInfo x="11.81250" y="1.37500" width="0.68750"
               height="0.00000"/>
              <visualSettings fillPattern="transparent" linePattern="solid"/>
              <points>
                <point x="11.81250" y="1.37500"/>
                <point x="12.50000" y="1.37500"/>
              </points>
            </line>
            <field name="F_User_Count" source="CS_User_Count" alignment="end">
              <font face="Courier New" size="10" bold="yes"/>
              <geometryInfo x="4.00000" y="1.43750" width="1.12500"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </frame>
        </repeatingFrame>
        <frame name="M_G_Grand_Totals">
          <geometryInfo x="2.56250" y="1.81250" width="10.43750"
           height="0.18750"/>
          <advancedLayout printObjectOnPage="lastPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_SumScrubbedPerReport" source="SumScrubbedPerReport"
           alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="5.87500" y="1.81250" width="0.75000"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
          </field>
          <text name="B_SumScrubbedPerReport">
            <textSettings justify="end"/>
            <geometryInfo x="2.62500" y="1.81250" width="1.06250"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Grand Total:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_SumRemovedPerReport" source="SumRemovedPerReport"
           alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="9.00000" y="1.81250" width="0.68750"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
          </field>
          <field name="F_SumPendingPerReport" source="SumPendingPerReport"
           alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="11.81250" y="1.81250" width="0.68750"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="Totals"/>
          </field>
          <field name="F_Total_User_ID_Count" source="CS_Total_User_ID_Count"
           alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="4.06250" y="1.81250" width="1.06250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </frame>
        <text name="B_11">
          <textSettings justify="center"/>
          <geometryInfo x="6.37500" y="2.25000" width="1.31250"
           height="0.16663"/>
          <visualSettings fillBackgroundColor="green"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_Report_Hdrs">
          <geometryInfo x="0.00000" y="0.06250" width="13.00000"
           height="0.56250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_last_name">
            <geometryInfo x="0.25000" y="0.37500" width="0.75000"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[CSR Name]]>
              </string>
            </textSegment>
          </text>
          <text name="B_csr_id">
            <geometryInfo x="4.06250" y="0.37500" width="0.68750"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[User ID]]>
              </string>
            </textSegment>
          </text>
          <text name="B_Scrubbed">
            <textSettings justify="center"/>
            <geometryInfo x="5.93750" y="0.06250" width="0.81250"
             height="0.56250"/>
            <visualSettings fillBackgroundColor="TableHeading"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Total
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Orders
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Scrubbed]]>
              </string>
            </textSegment>
          </text>
          <text name="B_Removed">
            <textSettings justify="center"/>
            <geometryInfo x="9.00000" y="0.06250" width="0.81250"
             height="0.56250"/>
            <visualSettings fillBackgroundColor="TableHeading"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Total
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Orders
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Removed]]>
              </string>
            </textSegment>
          </text>
          <text name="B_Pending">
            <textSettings justify="center"/>
            <geometryInfo x="11.81250" y="0.06250" width="0.81250"
             height="0.50000"/>
            <visualSettings fillBackgroundColor="TableHeading"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Total
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Orders
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Pending]]>
              </string>
            </textSegment>
          </text>
          <line name="B_2" arrow="none">
            <geometryInfo x="0.25000" y="0.56250" width="0.68750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="0.25000" y="0.56250"/>
              <point x="0.93750" y="0.56250"/>
            </points>
          </line>
          <line name="B_3" arrow="none">
            <geometryInfo x="4.06250" y="0.56250" width="0.56250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="4.06250" y="0.56250"/>
              <point x="4.62500" y="0.56250"/>
            </points>
          </line>
          <line name="B_4" arrow="none">
            <geometryInfo x="6.00000" y="0.56250" width="0.68750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="6.00000" y="0.56250"/>
              <point x="6.68750" y="0.56250"/>
            </points>
          </line>
          <line name="B_5" arrow="none">
            <geometryInfo x="9.12500" y="0.56250" width="0.56250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="9.12500" y="0.56250"/>
              <point x="9.68750" y="0.56250"/>
            </points>
          </line>
          <line name="B_6" arrow="none">
            <geometryInfo x="11.93750" y="0.56250" width="0.56250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="11.93750" y="0.56250"/>
              <point x="12.50000" y="0.56250"/>
            </points>
          </line>
          <text name="B_7">
            <textSettings justify="center"/>
            <geometryInfo x="10.43750" y="0.06250" width="0.93750"
             height="0.55212"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Removed
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Avg Handle
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Time]]>
              </string>
            </textSegment>
          </text>
          <line name="B_8" arrow="none">
            <geometryInfo x="10.50000" y="0.56250" width="0.81250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="10.50000" y="0.56250"/>
              <point x="11.31250" y="0.56250"/>
            </points>
          </line>
          <text name="B_9">
            <textSettings justify="center"/>
            <geometryInfo x="7.50000" y="0.06250" width="0.94788"
             height="0.52087"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Scrubbed
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Avg Handle
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Time]]>
              </string>
            </textSegment>
          </text>
          <line name="B_10" arrow="none">
            <geometryInfo x="7.56250" y="0.56250" width="0.81250"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="7.56250" y="0.56250"/>
              <point x="8.37500" y="0.56250"/>
            </points>
          </line>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_OR$BODY_SECTION">
        <textSettings justify="center"/>
        <geometryInfo x="5.50000" y="0.12500" width="3.04211" height="0.37500"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Order Processing System
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[CSR Scrub Productivity]]>
          </string>
        </textSegment>
      </text>
      <field name="F_StartDate" source="P_StartDate" spacing="single"
       alignment="end">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="5.93750" y="0.56250" width="1.00000" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_EndDate" source="P_EndDate" spacing="single"
       alignment="start">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="7.18750" y="0.56250" width="0.87500" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_Date" source="CurrentDate" formatMask="MM/DD/YYYY"
       spacing="single" alignment="start">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="0.18750" y="0.12500" width="1.12500" height="0.18750"
        />
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <field name="F_Time" source="CurrentDate" formatMask="HH:MI AM"
       spacing="single" alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="11.68750" y="0.12500" width="1.00000"
         height="0.18750"/>
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="anchoringObject"/>
        <visualSettings fillPattern="transparent"/>
      </field>
      <text name="B_1">
        <textSettings justify="center"/>
        <geometryInfo x="7.00000" y="0.56250" width="0.12500" height="0.16663"
        />
        <textSegment>
          <font face="Courier New" size="10"/>
          <string>
          <![CDATA[-]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="f_scrub_handle_timeformattrigg">
      <textSource>
      <![CDATA[function F_scrub_handle_timeFormatTrigg return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:Scrubbed_Handle_Time LIKE ':')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_remove_handle_timeformattrig">
      <textSource>
      <![CDATA[function F_remove_handle_timeFormatTrig return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:Removed_Handle_Time LIKE ':')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="afterreport">
      <textSource>
      <![CDATA[function AfterReport return boolean is
begin
 delete from temp_scrub_list where report_id = :CP_Seq_number;
 commit; 
 return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="beforereport">
      <textSource>
      <![CDATA[function BeforeReport return boolean is
  seq_number number;
begin
  select temp_scrub_list_sq.nextval into seq_number
  from dual;
  :CP_seq_number := seq_number;
  RPT.MASTER_SCRUB_LIST_PKG.MASTER_SCRUB_IN_OUT(:CP_seq_number, :P_Startdate, :P_Enddate);
  return (TRUE);
end;
  ]]>
      </textSource>
    </function>
  </programUnits>
  <colorPalette>
    <color index="190" displayName="TextColor" value="#336699"/>
    <color index="191" displayName="TableHeading" value="#cccc99"/>
    <color index="192" displayName="TableCell" value="#f7f7e7"/>
    <color index="193" displayName="Totals" value="#ffffcc"/>
  </colorPalette>
  <reportPrivate defaultReportType="masterDetail" templateName="rwbeige"/>
</report>
</rw:objects>
-->
<HTML>
<rw:style id="rwbeige">
<link rel="StyleSheet" type="text/css" href="css/rwbeige.css">
</rw:style>

<TITLE> Your Title </TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>
<BODY bgColor=#ffffff link=#000000 vLink=#000000>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ffffff rowSpan=2 vAlign=center width=188> 
      <P><IMG src="images/rwbeige_logo.gif" width="135" height="36"><br>
      </P>
      </TD>
    <TD bgColor=#ffffff height=40 vAlign=top><IMG alt="" height=1 src="images/stretch.gif" width=5></TD>
    <TD align=right bgColor=#ffffff vAlign=bottom>&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE bgColor=#ff0000 border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 class="OraColumnHeader">&nbsp; </TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 vAlign=top class="OraColumnHeader"><IMG alt="" border=0 height=17 src="images/topcurl.gif" width=30></TD>
    <TD vAlign=top width="100%">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD bgColor=#000000 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#9a9c9a height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#b3b4b3 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
		</TBODY>
	  </TABLE>
	</TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="29%" valign="top"> 
      <TABLE width="77%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_d_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation goes here </TD>
        </TR>
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/blue_r_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation Item</TD>
        </TR>
      </TABLE>
    </TD>
    <TD width="71%">
      <DIV align="center">
	    <!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MG2GRPFR">
<rw:foreach id="R_G_2_1" src="G_2">
 <table class="OraTable">
 <caption class="OraHeader">  <br>Description <rw:field id="F_description" src="description" breakLevel="R_G_2_1" breakValue="&nbsp;"> F_description </rw:field><br>
 </caption>
   <tr>
    <td valign="top">
    <table class="OraTable" summary="">
     <!-- Header -->
     <thead>
      <tr>
       <th <rw:id id="HBlastname" asArray="no"/> class="OraColumnHeader"> Last Name </th>
       <th <rw:id id="HBfirstname" asArray="no"/> class="OraColumnHeader"> First Name </th>
       <th <rw:id id="HBcsrid" asArray="no"/> class="OraColumnHeader"> Csr Id </th>
       <th <rw:id id="HBScrubbed" asArray="no"/> class="OraColumnHeader"> Scrubbed </th>
       <th <rw:id id="HBRemoved" asArray="no"/> class="OraColumnHeader"> Removed </th>
       <th <rw:id id="HBPending" asArray="no"/> class="OraColumnHeader"> Pending </th>
      </tr>
     </thead>
     <!-- Body -->
     <tbody>
      <rw:foreach id="R_G_1_1" src="G_1">
       <tr>
        <td <rw:headers id="HFlastname" src="HBlastname"/> class="OraCellText"><rw:field id="F_last_name" src="last_name" nullValue="&nbsp;"> F_last_name </rw:field></td>
        <td <rw:headers id="HFfirstname" src="HBfirstname"/> class="OraCellText"><rw:field id="F_first_name" src="first_name" nullValue="&nbsp;"> F_first_name </rw:field></td>
        <td <rw:headers id="HFcsrid" src="HBcsrid"/> class="OraCellText"><rw:field id="F_csr_id" src="csr_id" nullValue="&nbsp;"> F_csr_id </rw:field></td>
        <td <rw:headers id="HFScrubbed" src="HBScrubbed"/> class="OraCellNumber"><rw:field id="F_Scrubbed" src="Scrubbed" nullValue="&nbsp;"> F_Scrubbed </rw:field></td>
        <td <rw:headers id="HFRemoved" src="HBRemoved"/> class="OraCellNumber"><rw:field id="F_Removed" src="Removed" nullValue="&nbsp;"> F_Removed </rw:field></td>
        <td <rw:headers id="HFPending" src="HBPending"/> class="OraCellNumber"><rw:field id="F_Pending" src="Pending" nullValue="&nbsp;"> F_Pending </rw:field></td>
       </tr>
      </rw:foreach>
     </tbody>
     <tr>
      <th class="OraTotalText"> &nbsp; </th>
      <th class="OraTotalText"> &nbsp; </th>
      <th class="OraTotalText"> &nbsp; </th>
      <th class="OraTotalText"> &nbsp; </th>
      <td <rw:headers id="HFSumScrubbedPerdescription" src="HBScrubbed"/> class="OraTotalNumber">Total: <rw:field id="F_SumScrubbedPerdescription" src="SumScrubbedPerdescription" nullValue="&nbsp;"> F_SumScrubbedPerdescription </rw:field></td>
      <td <rw:headers id="HFSumRemovedPerdescription" src="HBRemoved"/> class="OraTotalNumber">Total: <rw:field id="F_SumRemovedPerdescription" src="SumRemovedPerdescription" nullValue="&nbsp;"> F_SumRemovedPerdescription </rw:field></td>
      <td <rw:headers id="HFSumPendingPerdescription" src="HBPending"/> class="OraTotalNumber">Total: <rw:field id="F_SumPendingPerdescription" src="SumPendingPerdescription" nullValue="&nbsp;"> F_SumPendingPerdescription </rw:field></td>
     </tr>
     <tr>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</rw:foreach>
<table class="OraTable" summary="">
 <tr>
  <th class="OraTotalNumber"> Sumcountperreport <rw:field id="F_SumcountPerReport" src="SumcountPerReport" nullValue="&nbsp;"> F_SumcountPerReport </rw:field></th>
  <th class="OraTotalNumber"> Total: <rw:field id="F_SumScrubbedPerReport" src="SumScrubbedPerReport" nullValue="&nbsp;"> F_SumScrubbedPerReport </rw:field></th>
  <th class="OraTotalNumber"> Total: <rw:field id="F_SumRemovedPerReport" src="SumRemovedPerReport" nullValue="&nbsp;"> F_SumRemovedPerReport </rw:field></th>
  <th class="OraTotalNumber"> Total: <rw:field id="F_SumPendingPerReport" src="SumPendingPerReport" nullValue="&nbsp;"> F_SumPendingPerReport </rw:field></th>
 </tr>
</table>
</rw:dataArea> <!-- id="MG2GRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->

	  </DIV>
    </TD>
  </TR>
</TABLE>
<P><BR>
</P>
<TD align=middle vAlign=top width="100%">&nbsp;</TD>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ff0000 colSpan=2 class="OraColumnHeader"><IMG alt=" " border=0 height=4 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ffffff>&nbsp;</TD>
    <TD align=right bgColor=#ffffff>&nbsp;</TD>
  </TR></TBODY></TABLE>
</BODY>
</HTML>

 
<!--
</rw:report> 
-->

