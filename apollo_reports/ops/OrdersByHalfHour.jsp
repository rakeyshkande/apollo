<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="OrdersByHalfHour" DTDVersion="9.0.2.0.0"
 beforeReportTrigger="beforereport" afterReportTrigger="afterreport">
  <xmlSettings xmlTag="ORDERSBYHALFHOUR" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <reportHtmlEscapes>
    <beforeReportHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
]]>
    </beforeReportHtmlEscape>
    <beforeFormHtmlEscape>
    <![CDATA[<html>
<body dir=&Direction bgcolor="#ffffff">
<form method=post action="_action_">
<input name="hidden_run_parameters" type=hidden value="_hidden_">
<font color=red><!--error--></font>
<center>
<p><table border=0 cellspacing=0 cellpadding=0>
<tr>
<td><input type=submit></td>
<td width=15>
<td><input type=reset></td>
</tr>
</table>
<p><hr><p>
]]>
    </beforeFormHtmlEscape>
  </reportHtmlEscapes>
  <data>
    <userParameter name="P_StartDate" datatype="character" width="30"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Scrub_ByHalf_Hour">
      <select>
      <![CDATA[select
  A.time_row,
  sum(A.order_count)
from
( select
  count(out_item_state) order_count,
  concat(concat(to_char(out_timestamp,'HH24'),':'),
  global.utl_pkg.date_half_hour(to_char(out_timestamp,'MI'))) as time_row
from rpt.temp_scrub_list,
        rpt.hours hour
where report_id = :CP_seq_number
   and trunc(out_timestamp) = to_date(:P_StartDate,'MM/DD/YYYY')
   and  hour.time = concat(concat(to_char(out_timestamp,'HH24'),':'), global.utl_pkg.date_half_hour(to_char(out_timestamp,'MI')))
   and out_item_state = '3004'
   and out_status in ('2002','2003')
   and out_test_order_flag = 'N'
group by
   to_char(out_timestamp,'HH24'),
   global.utl_pkg.date_half_hour(to_char(out_timestamp,'MI'))
UNION
  select 0,hours.time
  from rpt.hours
) A
group by
A.time_row
   order by
A.time_row]]>
      </select>
      <displayInfo x="0.70984" y="0.31250" width="1.51038" height="0.19995"/>
      <group name="G_Detail_Info">
        <displayInfo x="0.62842" y="1.17908" width="1.67407" height="0.60156"
        />
        <dataItem name="time_row" datatype="vchar2" columnOrder="12"
         width="4000" defaultWidth="100000" defaultHeight="10000">
          <dataDescriptor expression="A.time_row"
           descriptiveExpression="TIME_ROW" order="1" width="4000"/>
        </dataItem>
        <dataItem name="sum_A_order_count" oracleDatatype="number"
         columnOrder="13" width="22" defaultWidth="90000"
         defaultHeight="10000" breakOrder="none">
          <dataDescriptor expression="sum ( A.order_count )"
           descriptiveExpression="SUM(A.ORDER_COUNT)" order="2"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <summary name="SumcountPerReport" source="sum_A_order_count"
     function="sum" width="22" precision="38" reset="report" compute="report">
      <displayInfo x="2.88538" y="0.58325" width="1.23743" height="0.19995"/>
    </summary>
    <placeholder name="CP_seq_number" datatype="character" width="34"
     precision="32">
      <displayInfo x="2.98962" y="1.02087" width="1.27075" height="0.21875"/>
    </placeholder>
  </data>
  <layout>
  <section name="main">
    <body width="8.00000">
      <location x="0.00000"/>
      <frame name="M_G_Scrub_Byhalfhour_enclosing">
        <geometryInfo x="0.06250" y="0.00000" width="7.87500" height="1.56250"
        />
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_Detail" source="G_Detail_Info"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.06250" y="0.43750" width="7.87500"
           height="0.18750"/>
          <generalLayout verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_count" source="sum_A_order_count" alignment="end">
            <font face="Courier New" size="10"/>
            <geometryInfo x="3.31250" y="0.43750" width="0.93750"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_countformattrigger"/>
          </field>
          <field name="F_Time" source="time_row" alignment="start">
            <font face="Courier New" size="10"/>
            <geometryInfo x="2.18750" y="0.43750" width="0.68750"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <frame name="M_G_Report_Totals">
          <geometryInfo x="0.06250" y="0.62500" width="7.87500"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="lastPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_SumcountPerReport" source="SumcountPerReport"
           alignment="end">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="3.31250" y="0.75000" width="0.93750"
             height="0.18750"/>
          </field>
          <text name="B_Total">
            <geometryInfo x="2.12500" y="0.75000" width="0.62500"
             height="0.18750"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Total:]]>
              </string>
            </textSegment>
          </text>
          <line name="B_3" arrow="none">
            <geometryInfo x="3.31250" y="0.68750" width="0.93750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="3.31250" y="0.68750"/>
              <point x="4.25000" y="0.68750"/>
            </points>
          </line>
        </frame>
        <text name="B_4">
          <textSettings justify="center"/>
          <geometryInfo x="2.87500" y="1.31250" width="2.25000"
           height="0.18750"/>
          <textSegment>
            <font face="Courier New" size="10" bold="yes"/>
            <string>
            <![CDATA[End of Report
]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_Headings">
          <geometryInfo x="0.06250" y="0.06250" width="7.87500"
           height="0.25000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_Time">
            <geometryInfo x="2.18750" y="0.06250" width="0.37500"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="r25g50b75"/>
            <textSegment>
              <font face="Courier New" size="10" bold="yes"/>
              <string>
              <![CDATA[Time]]>
              </string>
            </textSegment>
          </text>
          <line name="B_2" arrow="none">
            <geometryInfo x="2.18750" y="0.25000" width="0.31250"
             height="0.00000"/>
            <visualSettings fillBackgroundColor="r25g50b75"
             linePattern="solid"/>
            <points>
              <point x="2.18750" y="0.25000"/>
              <point x="2.50000" y="0.25000"/>
            </points>
          </line>
          <field name="F_Date_Requested" source="P_StartDate"
           alignment="center">
            <font face="Courier New" size="10" bold="yes"/>
            <geometryInfo x="3.25000" y="0.06250" width="1.00000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <line name="B_1" arrow="none">
            <geometryInfo x="3.25000" y="0.25000" width="1.00000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="3.25000" y="0.25000"/>
              <point x="4.25000" y="0.25000"/>
            </points>
          </line>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_OR$BODY_SECTION">
        <textSettings justify="center"/>
        <geometryInfo x="2.87500" y="0.12500" width="2.25000" height="0.37500"
        />
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Order Processing System
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="10" bold="yes"/>
          <string>
          <![CDATA[Scrub Orders By Half Hour]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="0.18750" y="0.18750" width="0.66663" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <field name="F_Current_time" source="CurrentDate" formatMask="HH:MI AM"
       alignment="right">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="6.43750" y="0.12500" width="0.93750" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <field name="F_Date1" source="P_StartDate" spacing="single"
       alignment="center">
        <font face="Courier New" size="10" bold="yes"/>
        <geometryInfo x="2.87500" y="0.50000" width="2.25000" height="0.18750"
        />
        <generalLayout verticalElasticity="variable"/>
        <advancedLayout printObjectOnPage="allPage"
         basePrintingOn="enclosingObject"/>
        <visualSettings lineBackgroundColor="black"/>
      </field>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="f_countformattrigger">
      <textSource>
      <![CDATA[function F_countFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:sum_A_order_count != '0')
  then
    srw.set_text_color('black');
    srw.set_font_face('Courier New');
    srw.set_font_size(10);
    srw.set_font_weight(srw.bold_weight);
    srw.set_font_style(srw.plain_style);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="beforereport">
      <textSource>
      <![CDATA[function BeforeReport return boolean is
  seq_number number;
begin
  select temp_scrub_list_sq.nextval into seq_number
  from dual;
  :CP_seq_number := seq_number;
 RPT.MASTER_SCRUB_LIST_PKG.MASTER_SCRUB_IN_OUT(:CP_seq_number, :P_Startdate,:P_StartDate);
  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="afterreport">
      <textSource>
      <![CDATA[function AfterReport return boolean is
begin
 delete from temp_scrub_list where report_id = :CP_Seq_number;
 commit; 
 return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <colorPalette>
    <color index="190" displayName="TextColor" value="#ffffff"/>
    <color index="191" displayName="custom6" value="#93065d"/>
    <color index="192" displayName="Background2" value="#e4c1d6"/>
    <color index="193" displayName="Background3" value="#c982ae"/>
  </colorPalette>
  <reportPrivate defaultReportType="tabular" templateName="rwwine"/>
</report>
</rw:objects>
-->

<HTML>
<HEAD>
<rw:style id="rwwine">
<link rel="StyleSheet" type="text/css" href="css/rwwine.css">
</rw:style>

<TITLE> Your Title </TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>

<BODY bgColor=#ffffff link=#000000 vLink=#000000>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ffffff rowSpan=2 vAlign=center width=188> 
      <p><IMG src="images/rwwine_logo.gif" width="135" height="36"><br>
      </p>
      </TD>
    <TD bgColor=#ffffff height=40 vAlign=top><IMG alt="" height=1 src="images/stretch.gif" width=5></TD>
    <TD align=right bgColor=#ffffff vAlign=bottom>&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE bgColor=#ff0000 border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD class="OraColumnHeader">&nbsp; </TD>
  </TR></TBODY></TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#ff0000 vAlign=top class="OraColumnHeader"><IMG alt="" border=0 height=17 src="images/topcurl.gif" width=30></TD>
    <TD vAlign=top width="100%">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR>
          <TD bgColor=#000000 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#9a9c9a height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        <TR>
          <TD bgColor=#b3b4b3 height=1><IMG alt="" border=0 height=1 src="images/stretch.gif" width=1></TD>
        </TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="29%" valign="top"> 
      <TABLE width="77%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/wine_d_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation goes here </TD>
        </TR>
        <TR> 
          <TD width="10%" class="OraCellText"><IMG src="images/wine_r_arrow.gif" width="10" height="10"></TD>
          <TD width="90%" class="OraCellText">Navigation Item</TD>
        </TR>
      </TABLE>
    </TD>
    <TD width="71%">
      <DIV align="center">
	    <!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MG1GRPFR">
<table class="OraTable">
<caption>  </caption>
 <!-- Header -->
 <thead>
  <tr>
   <th <rw:id id="HBhour" asArray="no"/> class="OraColumnHeader"> Hour </th>
   <th <rw:id id="HBcount" asArray="no"/> class="OraColumnHeader"> Count </th>
  </tr>
 </thead>
 <!-- Body -->
 <tbody>
  <rw:foreach id="R_G_1_1" src="G_1">
   <tr>
    <td <rw:headers id="HFhour" src="HBhour"/> class="OraCellText"><rw:field id="F_hour" src="hour" nullValue="&nbsp;"> F_hour </rw:field></td>
    <td <rw:headers id="HFcount" src="HBcount"/> class="OraCellNumber"><rw:field id="F_count" src="count" nullValue="&nbsp;"> F_count </rw:field></td>
   </tr>
  </rw:foreach>
 </tbody>
 <!-- Report Level Summary -->
 <tr>
  <th class="OraTotalText"> &nbsp; </th>
  <td <rw:headers id="HFSumcountPerReport" src="HBcount"/> class="OraTotalNumber">Total: <rw:field id="F_SumcountPerReport" src="SumcountPerReport" nullValue="&nbsp;"> F_SumcountPerReport </rw:field></td>
 </tr>
</table>
</rw:dataArea> <!-- id="MG1GRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->

	  </DIV>
    </TD>
  </TR>
</TABLE>
<P><BR>
</P>
<TD align=middle vAlign=top width="100%">&nbsp;</TD>
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ff0000 colSpan=2 class="OraColumnHeader"><IMG alt=" " border=0 height=4 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#000000 colSpan=2><IMG alt=" " border=0 height=1 src="images/stretch.gif" width=1></TD>
  </TR>
  <TR>
    <TD bgColor=#ffffff>&nbsp;</TD>
    <TD align=right bgColor=#ffffff>&nbsp;</TD>
  </TR>
  </TBODY>
</TABLE>
<p>&nbsp;</p>
</BODY>
</HTML>
<!--
</rw:report>
-->
