/**
 * 
 */
package com.ftd.pi.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.bo.PartnerOrderBO;
import com.ftd.pi.bo.PartnerOrderFeedBO;
import com.ftd.pi.exceptions.PartnerException;
import static com.ftd.pi.utils.PIConstants.*;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerOrderFeed;

/**
 * @author skatam
 *
 */
public class PartnerOrderService 
{
	private Logger logger = new Logger(PartnerOrderService.class.getName());
	
	public String savePartnerOrderFeed(PartnerOrderFeed partnerOrderFeed)
	{
		String orderFeedId = null;
		Connection conn = null;
		try 
		{
			conn = PartnerUtils.getConnection();
			PartnerOrderFeedBO partnerOrderFeedBO = new PartnerOrderFeedBO(conn);
			orderFeedId = partnerOrderFeedBO.savePartnerOrderFeed(partnerOrderFeed);
			logger.debug("Sending Partner Order Feed ID :"+orderFeedId+" to JMS queue.");
			
			boolean success = PartnerUtils.sendJMSMessage(JMS_PIPELINE_FOR_EM_PARTNERS, 
									PROCESS_PARTNER_ORDERS, orderFeedId);
			if (!success)
			{
				  logger.error("Unable to send JMS message for partner order feed id = "+ orderFeedId);
				  partnerOrderFeedBO.updateOrderFeedStatus(orderFeedId, "JMS_ERROR");
			} 
			else
			{
				  logger.debug("Sent successfully");
			}
		}
		catch (PartnerException e) {
			throw e;
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) { 
				logger.error("Unable to close the connection:" + e);
			}
		}
		return orderFeedId;
	}
	
	public void processPartnerOrderFeedByStatus(String status) 
	{
		logger.debug("********* Processing Partner Orders ***********");
		List<PartnerOrderFeed> orderFeeds = null;
		Connection conn = null;
		try {
			conn = PartnerUtils.getConnection();
			orderFeeds = new PartnerOrderFeedBO(conn).getPartnerOrderFeeds(status);
		} 
		catch (PartnerException e) {
			throw e;
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error("Unable to close the connection:" + e);
			}
		}
		
		for (PartnerOrderFeed partnerOrderFeed : orderFeeds) 
		{
			processPartnerOrderFeed(partnerOrderFeed.getOrderFeedId());
		}
	}
	
	public void processPartnerOrderFeed(String partnerOrderFeedId) 
	{
		Connection conn = null;
		PartnerOrderFeedBO partnerOrderFeedBO = null;
		try {
			conn = PartnerUtils.getConnection();
			partnerOrderFeedBO = new PartnerOrderFeedBO(conn);
			PartnerOrderFeed partnerOrderFeed = partnerOrderFeedBO.getPartnerOrderFeed(partnerOrderFeedId);
			partnerOrderFeedBO.processPartnerOrderFeed(partnerOrderFeed);
			partnerOrderFeedBO.updateOrderFeedStatus(partnerOrderFeedId, "DONE");
		} 
		catch (PartnerException e) {
			partnerOrderFeedBO.updateOrderFeedStatus(partnerOrderFeedId, "ERROR");
			throw e;
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) { 
				logger.error("Unable to close the connection:" + e);
			}
		}
	}
	
	public void processPartnerOrder(String partnerId, String partnerOrderNumber, int retryCount)
	{
		Connection conn = null;
		try {
			conn = PartnerUtils.getConnection();
			PartnerOrderBO partnerOrderBO = new PartnerOrderBO(conn);
			partnerOrderBO.processPartnerOrder(partnerId, partnerOrderNumber, retryCount);	
		} 
		catch (PartnerException e) {
			throw e;
		}
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) { 
				logger.error("Unable to close the connection:" + e);
			}
		}
			
	}

	public void processOGErrorOrders() {

		Connection conn = null;
		try {
			conn = PartnerUtils.getConnection();
			PartnerOrderBO partnerOrderBO = new PartnerOrderBO(conn);
			partnerOrderBO.processOGErrorOrders();	
		} 
		catch (PartnerException e) {
			throw e;
		}
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error("Unable to close the connection:" + e);
			}
		}
			
	
	}	
	
}
