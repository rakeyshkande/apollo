package com.ftd.pi.web;

import static com.ftd.pi.utils.PIConstants.EMPTY_XML_ERROR;
import static com.ftd.pi.utils.PIConstants.ERROR_CODE;
import static com.ftd.pi.utils.PIConstants.SUCCESS_CODE;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.partner.vo.Order;
import com.ftd.pi.services.PartnerOrderService;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerOrderFeed;

public class PartnerOrderGatherer extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    private Logger logger = new Logger(PartnerOrderGatherer.class.getName());
    
    public void init(ServletConfig config) throws ServletException
    {
      super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      PrintWriter out = response.getWriter();
      out.println("The Partner Order Gatherer!");
      out.close();
    }


    public void doPost(HttpServletRequest request, 
    					HttpServletResponse response) throws ServletException, IOException 
    {
      String orderXML = request.getParameter("Order");
	  //commented this line as part of PCI code review
      //logger.info("Partner Order XML: \n" + orderXML);

      if(StringUtils.isBlank(orderXML))
      {
        this.returnError(response, EMPTY_XML_ERROR);
        return;
      }
      
      try 
      {
    	  PartnerOrderService partnerOrderService = new PartnerOrderService();
    	  Order partnerFeed = (Order) PartnerUtils.marshallAsObject(orderXML, Order.class);
          PartnerOrderFeed partnerOrderFeed = new PartnerOrderFeed(null,partnerFeed);
          String orderFeedId = partnerOrderService.savePartnerOrderFeed(partnerOrderFeed);
          logger.debug("Order Feed Id: "+orderFeedId);
          this.returnSuccess(response);
          
      } catch (Exception e) {
    	  logger.error(e);
    	  this.returnError(response, "Couldn't process Order XML");
      } 

    }
 
   private void returnSuccess(HttpServletResponse response) 
   {
       try {
           HttpServletResponseUtil.sendError(response, SUCCESS_CODE, "Success");
       } catch(Exception ex) {
           logger.error(ex.toString());
       }
   }

   private void returnError(HttpServletResponse response, String errorReason) 
   {
       try 
       {
           HttpServletResponseUtil.sendError(response, ERROR_CODE, errorReason);
       } 
       catch(Exception ex) 
       {
           logger.error("XML file transmission failed: " + ex.toString());
       }
   }

}