package com.ftd.pi.web;

import static com.ftd.pi.utils.PIConstants.EMPTY_XML_ERROR;
import static com.ftd.pi.utils.PIConstants.ERROR_CODE;
import static com.ftd.pi.utils.PIConstants.SUCCESS_CODE;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.HttpServletResponseUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.bo.PartnerOrderStatusUpdateBO;
import com.ftd.pi.partner.vo.VenusOrderStatus;
import com.ftd.pi.utils.PartnerUtils;


public class PartnerOrderStatusUpdate extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Logger logger = new Logger(PartnerOrderStatusUpdate.class.getName());

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();
		out.println("The Partner Order Order Status Update");
		out.close();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		String orderStatusXML = request.getParameter("OrderStatus"); 

		if (StringUtils.isBlank(orderStatusXML)) {
			this.returnError(response, EMPTY_XML_ERROR);
			return;
		}

		try {
			
			PartnerOrderStatusUpdateBO partnerOrderStatusUpdateBO = new PartnerOrderStatusUpdateBO();
			VenusOrderStatus orderStatus = (VenusOrderStatus) PartnerUtils.marshallAsObject(orderStatusXML, VenusOrderStatus.class);
			
			partnerOrderStatusUpdateBO.processVenusOrdStatusUpdate(orderStatus);			
			this.returnSuccess(response);

		} catch (Exception e) {
			logger.error(e);
			this.returnError(response,"Couldn't process Parnter Order Staus Updates Feed XML");
		}

	}

	private void returnSuccess(HttpServletResponse response)
	{
		try {
			HttpServletResponseUtil
					.sendError(response, SUCCESS_CODE, "Success");
		} catch (Exception ex) {
			logger.error(ex.toString());
		}
	}

	private void returnError(HttpServletResponse response, String errorReason)
	{
		try 
		{
			HttpServletResponseUtil.sendError(response, ERROR_CODE, errorReason);
		}
		catch (Exception ex) 
		{
			logger.error("XML file transmission failed: " + ex.toString());
		}
	}
}
