package com.ftd.pi.bo;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.OrderInvoiceDAO;
import com.ftd.pi.dao.PartnerOrderStatusUpdateDAO;
import com.ftd.pi.orderstatus.vo.CustomFields;
import com.ftd.pi.orderstatus.vo.CustomFields.Data;
import com.ftd.pi.orderstatus.vo.Order;
import com.ftd.pi.orderstatus.vo.OrderItem;
import com.ftd.pi.orderstatus.vo.OrderStatus;
import com.ftd.pi.partner.vo.FTDFeed;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;

/**
 * Invoice feeds were introduced specifically for Ariba partners, 
 * but may be used for other partners in the future.
 *
 */
public class PartnerOrderInvoiceBO extends PartnerFeedBO { 

	private static Logger logger = new Logger(PartnerOrderInvoiceBO.class.getName()); 
	
	/**
	 * Persist invoice feeds for all orders that are ready to be invoiced, 
	 * then trigger the feeds so they're sent to the ESB. 
	 * 
	 * @throws Exception
	 */
	public void createInvoiceFeed() throws Exception {
	    if (logger.isDebugEnabled()) {
            logger.debug("********* Entering createInvoiceFeed() *********");
	    }
	    Connection conn = null;
	    StringBuffer systemMessage = new StringBuffer();
	    Set<String> invoiceOrders = null;
	    
	    try {
    	    conn = PartnerUtils.getConnection();
    	    
    	    // Get list of orders to invoice
    	    //
    	    OrderInvoiceDAO invoiceDao = new OrderInvoiceDAO(conn);
    	    invoiceOrders = invoiceDao.getAribaOrdersToInvoice();
   	    
    	    if (invoiceOrders != null && invoiceOrders.size() > 0) {
    	        
                // Loop over orders to invoice, get necessary order info, construct feed, then save to DB
                //
                PartnerOrderStatusUpdateDAO posuDAO = new PartnerOrderStatusUpdateDAO(conn);
    	        for (String masterOrder : invoiceOrders) {
    	            Order orderInfo = getInvoiceDataForOrder(masterOrder, posuDAO);  // Get all order data
    	            addPartnerCustomLogicForInvoice(orderInfo);
    	            
    	            FTDFeed invoiceFeed = new FTDFeed();             
    	            OrderStatus orderStatus = new OrderStatus();
    	            orderStatus.getOrder().add(orderInfo);
    	            invoiceFeed.setOrderStatus(orderStatus);
    	            
    	            if (logger.isDebugEnabled()) {
    	                logger.debug("Saving feed for " + masterOrder);
    	            }
    	            String feedId = savePartnerFeed(invoiceFeed, PIConstants.INVOICE_TYPE, conn);
    	            
    	            // Reflect that order has been included in feed
    	            invoiceDao.updateInvoiceStatusToSent(masterOrder);
    	            
    	            // For each item in cart, add entry in order_status_update table
    	            List<OrderItem> itemList = orderInfo.getOrderItem();
    	            for (OrderItem curItem : itemList) {
                        // TODO - Origin???
                        String orderStatusUpdId = posuDAO.insertOrdStatusUpdateRef(feedId, orderInfo.getOriginId(), curItem.getFTDOrderItemID(), PIConstants.INVOICE_TYPE);
                        logger.info("Succesfully created order status update (invoice) reference with Id: " + orderStatusUpdId);
    	            }
    	        } // end for
    	        
    	        // The loop created the feed records, so now trigger MDB to send them to ESB
    	        //
    	        invoiceDao.startSendInvoiceFeedsToEsb();
    	        
    	    } else {
    	        logger.info("No orders to invoice");
    	    }
    	    
            if (logger.isDebugEnabled()) {
                logger.debug("********* Exiting createInvoiceFeed() *********");
            }
	    } catch (Exception e) {
            logger.error(" Error caught creating order invoice feeds. Sending system message. " + e);
            e.printStackTrace();
            systemMessage.append("Error caught creating order invoice feeds: " + e);
            PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
	        
	    } finally {
	        PartnerUtils.closeConnection(conn);
	    }
	}

	
	/**
	 * Get all order data necessary to construct an invoice for that order
	 * 
	 * @param masterOrderNumber
	 * @return
	 * @throws Exception 
	 */
	private Order getInvoiceDataForOrder(String masterOrderNumber, PartnerOrderStatusUpdateDAO posuDAO) throws Exception {
	    Order orderInfo = null;

	    orderInfo = posuDAO.getPartnerOrderinfo(masterOrderNumber);
	            
	    return orderInfo;
	}
	
    
    /**
     * Add HeaderCustomFields to order object so PI-ESB will know it's an invoice request
     * 
     * @param order
     */
    private void addPartnerCustomLogicForInvoice(Order order) {
        CustomFields headerCustFields = new CustomFields();
                    
        headerCustFields.getData().add(new Data("DOC_TYPE", PIConstants.INVOICE_TYPE));  // TODO - ???  
        headerCustFields.getData().add(new Data("Operation", "New"));   
        headerCustFields.getData().add(new Data("action", PIConstants.INVOICE_TYPE));
        headerCustFields.getData().add(new Data("process_item", order.getFTDOrderID()));
        
        PartnerUtils.getTimestampString(new Date());
        String invoiceDateTime = PartnerUtils.getTimestampString(new Date());
        headerCustFields.getData().add(new Data("updated_on", invoiceDateTime));                
        order.setHeaderCustomFields(headerCustFields);
    }       
}
