package com.ftd.pi.bo;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.PartnerOrderStatusUpdateDAO;
import com.ftd.pi.orderstatus.vo.CustomFields;
import com.ftd.pi.orderstatus.vo.CustomFields.Data;
import com.ftd.pi.orderstatus.vo.Order;
import com.ftd.pi.orderstatus.vo.OrderItem;
import com.ftd.pi.orderstatus.vo.OrderStatus;
import com.ftd.pi.orderstatus.vo.OrderStatusEnum;
import com.ftd.pi.partner.vo.FTDFeed;
import com.ftd.pi.partner.vo.VenusOrderStatus;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;

public class PartnerOrderStatusUpdateBO extends PartnerFeedBO { 

	private static Logger logger = new Logger(PartnerOrderStatusUpdateBO.class.getName()); 
	

	/** This is the request to persist the FTD order status updates from the partner (Partner Fulfilling vendor orders) 
	 * to venus_order_status
	 * @param orderStatus
	 * @return
	 */
	public String processVenusOrdStatusUpdate(VenusOrderStatus orderStatus) {
		if (logger.isDebugEnabled()) {
			logger.debug("********* Entering saveVenusOrdStatusUpdate() *********");
		}
		
		Connection conn = null;
		PartnerOrderStatusUpdateDAO partnerOrderStatusUpdateDAO;	
		StringBuffer systemMessage = new StringBuffer();
		
		try {
			conn = PartnerUtils.getConnection();
			partnerOrderStatusUpdateDAO = new PartnerOrderStatusUpdateDAO(conn);
			return partnerOrderStatusUpdateDAO.saveVenusOrderStatusUpdate(orderStatus);
			
		} catch(Exception e) {
			logger.error("Error caught saving venus order status updates. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught saving venus order status updates." + e);

		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);			
		}

		if (logger.isDebugEnabled()) {
			logger.debug("********* Exiting saveVenusOrdStatusUpdate() *********");
		}
		return null;
	}
	
	/**
	 * The method is to process the request for creating the partner order
	 * status updates for Apollo fulfilling orders
	 * 
	 * @param operation
	 * @param orderList
	 */
	public void createPartnerOrdStatusUpdate(String status, String orderList) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("********* Entering createPartnerOrdStatusUpdate() *********, operation: " + status);
		}
		
		Connection conn = null;
		StringBuffer systemMessage = new StringBuffer();
		Set<String> orders = null;
		PartnerOrderStatusUpdateDAO posuDAO = null;
		StringBuffer ordersToReprocess =null;
		
		try {
			conn = PartnerUtils.getConnection();
			posuDAO =  new PartnerOrderStatusUpdateDAO(conn);
			
			OrderStatusEnum statusType = OrderStatusEnum.fromValue(status);	
			if(statusType == null) {
				logger.error("Invalid Action: " + status);
				systemMessage.append("Status provided is not valid/not supported currently. Status provided: " + status);
				return;				
			}
			
			ConfigurationUtil configUtil = new ConfigurationUtil();
			String ordStatusToProcess = configUtil.getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT,"ORD_STAT_TO_PROCESS");
			if(ordStatusToProcess == null || ordStatusToProcess.indexOf(status) < 0) {
				logger.error("Unsupported Action: " + status);
				systemMessage.append("Status provided is not valid/not supported currently. Status provided: " + status);
				return;
			}			
			
			if(OrderStatusEnum.ADJUSTED.value().equals(status) 
					|| OrderStatusEnum.ACCEPTED.value().equals(status) 
					|| OrderStatusEnum.REMOVED.value().equals(status)
					|| OrderStatusEnum.SCRUB_ADJUSTED.value().equals(status))		
			{		
				if(orderList == null || orderList.trim().length() == 0) {				
					if(!OrderStatusEnum.ADJUSTED.value().equals(status)) {
						systemMessage.append("No order info provided, cannot create order status updates for status: " + status);
						throw new Exception("No order info provided, cannot create order status updates for status: " + status);
					}				
					if(logger.isDebugEnabled()) {
						logger.debug("Order info is not provided, Get the transactions that are Billed (ADJUSTED) today"); 
					}
					
					orders = posuDAO.getAdjOrdForStatusUpdate(null);				
					if(orders == null || orders.size() == 0) {
						logger.info("No OrderInfo is retrieved for order status updates for the status 'ADJUSTED'"); 
						return;
					}
					
				} else {				
					String[] ordersToProcess = orderList.split(",");		
					if(ordersToProcess.length == 0) {
						systemMessage.append("Invalid order info provided, cannot create order status updates for status: " + status);
						throw new Exception("Invalid order info provided, cannot create order status updates for status: " + status);
					}
					orders = new HashSet<String>(Arrays.asList(ordersToProcess));
				}
			
				//boolean checkIfStatUpdAllowed = OrderStatusEnum.ADJUSTED.value().equals(status) ? false  : true;		
				ordersToReprocess = processOrderStatusUpdates(orders, conn, true, statusType);				
				
			} else if(OrderStatusEnum.SHIPPED.value().equals(status)) {
				if(orderList == null || orderList.trim().length() == 0)
				{	
					orders = posuDAO.getShipOrdForStatusUpdate();				
					if(orders == null || orders.size() == 0){
						logger.info("No OrderInfo is retrieved for order status updates for the status 'SHIPPED'"); 
						return;
					}
				}else 
				{
					String[] ordersToProcess = orderList.split(",");		
					if(ordersToProcess.length == 0) {
						systemMessage.append("Invalid order info provided, cannot create order Ship status updates for status: " + status);
						throw new Exception("Invalid order info provided, cannot create order Ship status updates for status: " + status);
					}
					orders = new HashSet<String>(Arrays.asList(ordersToProcess));
				}	
				
				ordersToReprocess = processOrderShipStatusUpdates(orders, conn, statusType);	
			}	
			
			if(ordersToReprocess != null && ordersToReprocess.length() > 0) {
				systemMessage
						.append("Action seems to be partially failed. Unable to create the partner order status feeds, order(s) to be reprocessed: ")
						.append(ordersToReprocess).append("status: ").append(status);
			}
			
		} catch(Exception e) {
			logger.error("Error caught processing create order status updates for operation, " + status + ". sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught processing create order status updates for operation,").append(status).append(e);
		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);			
		}

		if (logger.isDebugEnabled()) {
			logger.debug("********* Exiting createPartnerOrdStatusUpdate() *********");
		}
	}

	/**
	 * @param orders
	 * @param conn
	 * @param checkIfAllowed
	 * @param orderStatus
	 * @return
	 * @throws Exception
	 */
	private StringBuffer processOrderStatusUpdates(Set<String> orders, Connection conn, 
			boolean checkIfAllowed, OrderStatusEnum orderStatus) throws Exception { 
		PartnerOrderStatusUpdateDAO posuDAO = new PartnerOrderStatusUpdateDAO(conn);
		StringBuffer ordersToReprocess = new StringBuffer();
		
		try {
			boolean isStatusUpdAllowed = true;
			
			for (String orderItemNumber : orders) {				
				if(checkIfAllowed) {
					isStatusUpdAllowed =  posuDAO.isPtnOrderStatusAllowed(orderItemNumber, (OrderStatusEnum.REMOVED == orderStatus || OrderStatusEnum.SCRUB_ADJUSTED == orderStatus ) ? "Y" : "N");
				}				
				if(!isStatusUpdAllowed) {
					logger.error("Order is not allowed to send order status updates. Sending System message.");
					
					PartnerUtils.sendNoPageSystemMessage("OrderStatusUpdate will not be created for \n already Invoiced order "
							+ "\n or \n when partner did not opt for order status updates "
							+ "\n or \n order origin does not match with partner ftd origin. OrderNumber: " + orderItemNumber);
					continue;
				}	
				
				if(OrderStatusEnum.ACCEPTED == orderStatus && posuDAO.isOrdStatusExists(orderStatus.value(), orderItemNumber)) {
					logger.error("Order is not allowed to send updates for 'Accepted' status. Sending System message.");
					
					PartnerUtils.sendNoPageSystemMessage("OrderStatusUpdate will not be created for "
							+ "order that is previously Accepted order. OrderNumber: " + orderItemNumber);
					continue;
				}				
				
				Order orderInfo = posuDAO.getPartnerOrderinfo(orderItemNumber);				
				if(orderInfo == null) {
					logger.error("Unable to get the order information, skipping order status update for the order: " + orderItemNumber);	
					PartnerUtils.sendNoPageSystemMessage("Unable to get the order information, skipping order status update for the order: " + orderItemNumber);
					continue;
				}			
				addPartnerCustomLogic(orderInfo, orderStatus, orderItemNumber, posuDAO);
				insertPartnerFeed(orderInfo, conn, ordersToReprocess, orderItemNumber, orderStatus.value()); 
			}
			
		} catch(Exception e) {
			logger.error("Error caught gathering order info for order status updates, " + e);
			throw e;
		}		
		return ordersToReprocess;
	}	
	
	/**
	 * @param orders
	 * @param conn
	 * @param checkIfAllowed
	 * @param orderStatus
	 * @return
	 * @throws Exception
	 */
	private StringBuffer processOrderShipStatusUpdates(Set<String> orders, Connection conn, OrderStatusEnum orderStatus) throws Exception
	{ 
		PartnerOrderStatusUpdateDAO posuDAO = new PartnerOrderStatusUpdateDAO(conn);
		StringBuffer ordersToReprocess = new StringBuffer();
		Order orderInfo = null;
		
		try {
			boolean isShipStatusUpdAllowed = true;
			
			for (String orderItemNumber : orders) {
				
				isShipStatusUpdAllowed =  posuDAO.isPtnOrderShipStatusAllowed(orderItemNumber);
								
				if(!isShipStatusUpdAllowed) {
					logger.error("Order is not allowed to send order ship status updates. Sending System message.");
					
					PartnerUtils.sendNoPageSystemMessage("Order Ship Status Update will not be created for \n when partner did not opt for order status updates "
							+ "\n or \n order origin does not match with partner ftd origin. OrderNumber: " + orderItemNumber);
					continue;
				}	
				
				orderInfo = posuDAO.getPartnerOrderinfo(orderItemNumber);
				
				if(orderInfo == null) {
					logger.error("Unable to get the order information, skipping order ship status update for the order: " + orderItemNumber);	
					PartnerUtils.sendNoPageSystemMessage("Unable to get the order information, skipping order Ship status update for the order: " + orderItemNumber);
					continue;
				}				
				addPartnerCustomLogic(orderInfo, orderStatus, orderItemNumber, posuDAO); 
				insertPartnerFeed(orderInfo, conn, ordersToReprocess, orderItemNumber, orderStatus.value()); 
			}
			
		} catch(Exception e) {
			logger.error("Error caught gathering order info for order status updates, " + e);
			throw e;
		}		
		return ordersToReprocess;
	}	
	
	/** Add custom logic for Order Status update feed
	 * @param order
	 * @param orderStatus
	 * @param orderItem
	 * @param posuDAO
	 */
	private void addPartnerCustomLogic(Order order, OrderStatusEnum orderStatus, String orderItem, PartnerOrderStatusUpdateDAO posuDAO) {
		
		if("ARI".equalsIgnoreCase(order.getOriginId())) {			
			boolean isOrdStatusExists = true;
			
			// TODO FOR SHIP and INVOICE types set fields accordingly
			CustomFields headerCustFields = new CustomFields();
			
			String orderSatusValue = orderStatus.value();
			//Specific to confirmation DOC
			if(orderStatus == OrderStatusEnum.ACCEPTED || orderStatus == OrderStatusEnum.REMOVED || orderStatus == OrderStatusEnum.ADJUSTED|| orderStatus == OrderStatusEnum.SCRUB_ADJUSTED) {					
				headerCustFields.getData().add(new Data("DOC_TYPE", PIConstants.ARI_CONFIRMATION));
				
				if(orderStatus == OrderStatusEnum.SCRUB_ADJUSTED){
					orderSatusValue = "ADJUSTED";
					isOrdStatusExists = true;
				}else if(orderStatus == OrderStatusEnum.ACCEPTED || orderStatus == OrderStatusEnum.REMOVED) {
					isOrdStatusExists = posuDAO.isOrdStatusExists(orderSatusValue, orderItem);
				}else{
					isOrdStatusExists = posuDAO.isOrdStatusExists("", orderItem);
				}
				logger.debug("isOrderStatus:" + isOrdStatusExists+" orderStatus:"+orderStatus);
				String operation = isOrdStatusExists ? "Update" : "New";
				
				headerCustFields.getData().add(new Data("Operation", operation));	
				
			} else if(orderStatus == OrderStatusEnum.SHIPPED) {
				headerCustFields.getData().add(new Data("DOC_TYPE", PIConstants.ARI_SHIPMENT));					
				headerCustFields.getData().add(new Data("Operation", "New"));				
			}	
			
			headerCustFields.getData().add(new Data("action", orderSatusValue ));
			headerCustFields.getData().add(new Data("process_item", orderItem));
			headerCustFields.getData().add(new Data("updated_on",  PartnerUtils.getTimestampString(new Date())));			
			order.setHeaderCustomFields(headerCustFields);
		}		
	}	
	
	/**
	 * @param orderInfo
	 * @param conn
	 * @param ordersToReprocess
	 * @param orderItemNumber
	 * @param orderStatusType
	 */
	private void insertPartnerFeed(Order orderInfo, Connection conn, StringBuffer ordersToReprocess, String orderItemNumber, String orderStatusType) {
		
		PartnerOrderStatusUpdateDAO posuDAO = new PartnerOrderStatusUpdateDAO(conn);
		FTDFeed osuFtdFeed = new FTDFeed();				
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.getOrder().add(orderInfo);
		osuFtdFeed.setOrderStatus(orderStatus);
		orderStatusType = (orderStatusType.equalsIgnoreCase("SCRUB_ADJUSTED")) ? "ADJUSTED" : orderStatusType ;
		logger.debug("orderStatusType:##" + orderStatusType);
		String feedId = savePartnerFeed(osuFtdFeed, PIConstants.ORD_STATUS_UPD, conn);
		
		if (!StringUtils.isEmpty(feedId)) {
			logger.info("Succesfully created partner order status update feed with Id: " + feedId);
			for (OrderItem orderItem : orderInfo.getOrderItem()) {
				if(orderItem.getFTDOrderItemID().equalsIgnoreCase(orderItemNumber)) {
					String orderStatusUpdId = posuDAO.insertOrdStatusUpdateRef(feedId, orderInfo.getOriginId(), orderItemNumber, orderStatusType);
					logger.info("Succesfully created partner order status update reference with Id: " + orderStatusUpdId);
					break;
				}
			}			
		} else {
			logger.error("Unable to save the partner order status feed, order should be reprocessed: " + orderItemNumber);
			if(ordersToReprocess.length() > 0) {
				ordersToReprocess.append(",");
			}
			ordersToReprocess.append(orderItemNumber);
		}		
	}
}
