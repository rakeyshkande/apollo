/**
 * 
 */
package com.ftd.pi.bo;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.Marshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.PartnerOrderDAO;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.ftd.FTDOrder;

/**
 * @author skatam
 *
 */
public class FTDOrderGathererBO 
{
	private Logger logger = new Logger(FTDOrderGathererBO.class.getName());
	
	//private Connection conn;
	private PartnerOrderDAO partnerOrderDAO;
	
    
    public FTDOrderGathererBO(Connection conn) {
    	//this.conn = conn;
    	partnerOrderDAO = new PartnerOrderDAO(conn);
		
	}
    
	public void sendToOG(String partnerId, String partnerOrderNumber, FTDOrder ftdOrder) 
	{
		logger.debug("Sending partnerOrderNumber :"+partnerOrderNumber+" to FTD OrderGatherer.");
		boolean sentToOG = this.sendFTDOrderToOrderGatherer(partnerId, partnerOrderNumber, ftdOrder);
		logger.debug("sentToOG :"+sentToOG);
	}

	private boolean sendFTDOrderToOrderGatherer(String partnerId, String partnerOrderNumber, FTDOrder ftdOrder) {
		logger.debug("Sending FTD Order :"+ftdOrder+" to FTD OrderGatherer.");
		
		boolean success = false;

        try {

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String orderGathererURL = cu.getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT,
            		PIConstants.PARTNER_ORDER_GATHERER_URL);
            
            logger.debug("orderGathererURL: " + orderGathererURL);

            Map<String, Object> props = new HashMap<String, Object>();
    		props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    		
            String strXml = PartnerUtils.marshallAsXML(ftdOrder, props);
            logger.debug("Transformed FTD OrderXML being send to OG:\n"+strXml);
            //TODO
            /*
            if(1==1)
            {
            	return true;
            }
            */
            String response=null;
            int result=-1;
            
            PostMethod post = new PostMethod(orderGathererURL);
            NameValuePair nvPair = null;
            NameValuePair[] nvPairArray = new NameValuePair[1];
            String name, value;
            
            name = "Order";
            value = strXml;
            nvPair = new NameValuePair(name, value);
            nvPairArray[0] = nvPair;

            post.setRequestBody(nvPairArray);
            
            HttpClient httpclient = new HttpClient();
            
            // Execute request
            try {
                result = httpclient.executeMethod(post);
                response = post.getResponseBodyAsString();
            } 
            catch (Exception e) {
				e.printStackTrace();
			}
            finally {
                // Release current connection to the connection pool once you are done
                post.releaseConnection();
            }
            String message = "Http Response Code is " + result;
            logger.debug(message);
            logger.info(response);
            /*if (result == 200) {
                success = true;
                System.out.println(success);
            }*/
            if( result==200 )
            {
            	success = true;
            	this.partnerOrderDAO.updateOrderStatus(partnerId, partnerOrderNumber,PIConstants.ORDER_STATUS_DONE);
            }
            else
            {   
                //if( result==600 ) 
                {
                	success = false;
                	//log the error and send email to IT and marketing
                    logger.error(response);
                    partnerOrderDAO.updateOrderStatus(partnerId, partnerOrderNumber,PIConstants.ORDER_STATUS_GATHERER_ERROR);
                   
                    //send no-page email
                    PartnerUtils.sendNoPageSystemMessage("Partner-Order-Number: "+partnerOrderNumber+"\n ERROR-Details: "+response);
                }
                
             }
        } catch (Exception e) {
            logger.error("",e);
            success = false;
        }
        return success;
	}
}
