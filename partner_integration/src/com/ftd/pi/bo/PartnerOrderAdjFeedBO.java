package com.ftd.pi.bo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.PartnerOrderAdjDAO;
import com.ftd.pi.partner.vo.FTDFeed;
import com.ftd.pi.partner.vo.OrderAdjustment;
import com.ftd.pi.partner.vo.OrderItemAdjustment;
import com.ftd.pi.partner.vo.OrderItemAdjustment.ItemPriceAdjustments;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerMappingVO;
import com.ftd.pi.vo.PartnerOrderAdjFeedVO;
import com.ftd.pi.vo.PartnerOrderItemAdjFeedVO;

/**
 * @author smeka
 * 
 */
public class PartnerOrderAdjFeedBO extends PartnerFeedBO {
	private static Logger logger = new Logger(PartnerOrderAdjFeedBO.class.getName());

	/**
	 * Method to prepare the partner adjustment feeds and save to partner feed
	 * table.
	 */
	public void createPartnerFeed(String partnerId) {

		if (logger.isDebugEnabled()) {
			logger.debug("********* Entering createPartnerFeed() *********");
		}

		Connection conn = null;
		PartnerOrderAdjDAO feedDAO = null;
		StringBuffer systemMessage = new StringBuffer();

		try {
			conn = PartnerUtils.getConnection();
			feedDAO = new PartnerOrderAdjDAO(conn);

			// Get all the adjustment feeds from partner order adjustment table for a given partner
			// id which are not already sent to feed master. Here partner Id is optional.
			Map<String, PartnerOrderAdjFeedVO> orderAdjustments =  feedDAO.getPartnerFeedData(partnerId, PIConstants.NEW_FEED_STATUS);

			if (orderAdjustments == null || orderAdjustments.isEmpty()) {
				logger.info("No new partner Order Adjustments Feed data found.");
				return;
			}

			// Construct Feed XML and save to feed master.
			Set<String> ftdOrderIds = orderAdjustments.keySet();
			for (String ftdOrderId : ftdOrderIds) {
				PartnerOrderAdjFeedVO partnerOrderAdj = orderAdjustments.get(ftdOrderId);
				String feedId = savePartnerFeed(this.buildOrderAdjFeed(partnerOrderAdj), PIConstants.ADJUSTMENT_TYPE, conn);
			
				if (feedId != null && !StringUtils.isEmpty(feedId)) {
					logger.info("Succesfully created partner adjustment feed with Id: "	+ feedId);

					// get the list of primary feeds/ order adjustments to be updated.
					List<String> partnerFeedDataIds = new ArrayList<String>();
					for (PartnerOrderItemAdjFeedVO itemAdjustVO : partnerOrderAdj.getItemAdjFeeds()) {
						partnerFeedDataIds.add(itemAdjustVO.getPartnerOrderItemAdjId());
					}
									
					// Update Feed ID for all adjustments of a partner order.
					if (!feedDAO.updatePartnerFeedId(partnerFeedDataIds, feedId, PIConstants.ADJUSTMENT_TYPE, PIConstants.SENT_FEED_STATUS)) {
						systemMessage.append("Unable to update the status for the partner adjustment feeds: " + partnerFeedDataIds);
					}
				} else {
					systemMessage.append("Unable to insert the partner feed for the order, " + ftdOrderId);
				}
			}

		} catch (Exception e) {
			logger.error(" Error caught creating partner order adjustments feeds. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught creating partner adjustment feeds." + e);

		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);			
		}

		if (logger.isDebugEnabled()) {
			logger.debug("********* Exiting createPartnerFeed() *********");
		}
	}

	/**Builds order adjustment FTD feed.
	 * @param partnerOrderAdj
	 * @return
	 */
	private FTDFeed buildOrderAdjFeed(PartnerOrderAdjFeedVO partnerOrderAdj) {

		FTDFeed ftdPartnerFeed = new FTDFeed();
		
		// set common fields like FTD order number, partner order number etc
		OrderAdjustment orderAdjustment = new OrderAdjustment();
		orderAdjustment.setPartnerId(partnerOrderAdj.getPartnerId());
		orderAdjustment.setPartnerOrderID(partnerOrderAdj.getPartnerOrderNumber());
		orderAdjustment.setFTDOrderID(partnerOrderAdj.getFtdOrderNumber());
		
		boolean includeAddon = false;
		
		PartnerMappingVO partnerMappingVO = getPartnerMapping(partnerOrderAdj.getPartnerId());
		
		if(partnerMappingVO != null){
			includeAddon = partnerMappingVO.getAddonSubscription();
		}
		
		logger.info("Including Addon's in the partner feed : "+includeAddon);

		// set order item adjustments for each of the order item in an order.
		List<PartnerOrderItemAdjFeedVO> itemAdjFeeds = partnerOrderAdj.getItemAdjFeeds();

		for (PartnerOrderItemAdjFeedVO partnerOrderItemAdj : itemAdjFeeds) {
			OrderItemAdjustment itemAdjustment = new OrderItemAdjustment();
			itemAdjustment.setPartnerOrderItemID(partnerOrderItemAdj.getPartnerOrderItemNumber());
			itemAdjustment.setFTDOrderItemID(partnerOrderItemAdj.getFtdExternalOrderNumber());
			itemAdjustment.setAdjustmentType(partnerOrderItemAdj.getAdjustmentType());
			itemAdjustment.setAdjustmentReason(partnerOrderItemAdj.getAdjustmentReason());

			ItemPriceAdjustments itemPriceAdjs = new ItemPriceAdjustments();
			if (partnerOrderItemAdj.getItemPrincipalAmount() != null
					&& partnerOrderItemAdj.getItemPrincipalAmount().doubleValue() >= 0) {
				itemPriceAdjs.setItemPrincipalAmount(partnerOrderItemAdj.getItemPrincipalAmount());
			}
			if (includeAddon && partnerOrderItemAdj.getItemAddOnAmount() != null
					&& partnerOrderItemAdj.getItemAddOnAmount().doubleValue() >= 0) {
				itemPriceAdjs.setItemAddOnAmount(partnerOrderItemAdj.getItemAddOnAmount());
			}
			if (partnerOrderItemAdj.getItemTaxAmount() != null) {
				itemPriceAdjs.setItemTaxAmount(partnerOrderItemAdj.getItemTaxAmount());
			}
			if (partnerOrderItemAdj.getShippingPrincipalAmount() != null
					&& partnerOrderItemAdj.getShippingPrincipalAmount().doubleValue() >= 0) {
				itemPriceAdjs.setShippingPrincipalAmount(partnerOrderItemAdj.getShippingPrincipalAmount());
			}
			/*if (partnerOrderItemAdj.getShippingTaxAmount() != null) {
				itemPriceAdjs.setShippingTaxAmount(partnerOrderItemAdj.getShippingTaxAmount());
			}*/
			itemAdjustment.setItemPriceAdjustments(itemPriceAdjs);

			orderAdjustment.getOrderItemAdjustment().add(itemAdjustment);			
		}
		ftdPartnerFeed.getOrderAdjustment().add(orderAdjustment);
		return ftdPartnerFeed;
	}

	private PartnerMappingVO getPartnerMapping(String partnerId) {
		
				
		PartnerMappingVO partnerMapVo = new PartnerMappingVO();
		StringBuffer systemMessage = new StringBuffer();
		Connection conn = null;
		try {			
			conn = PartnerUtils.getConnection();
			PartnerOrderAdjDAO orderAdjFeedDAO = new PartnerOrderAdjDAO(conn);
			partnerMapVo = orderAdjFeedDAO.getPartnerMapping(partnerId);			
		} catch (Exception e) {
			logger.error("Error caught getting the partner mapping: ", e);
			systemMessage.append("Error caught getting the partner mapping: " + e);
		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);
		}
		
		return partnerMapVo;
	}

	/** Method to identify if there are any refunds applied for partner orders and create/insert adjustment records.
	 * @param partnerId
	 * @throws Exception
	 */
	public void processCreateRefundAdjustments(String partnerId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("********* Entering processRefundFeed() *********");
		}
		StringBuffer systemMessage = new StringBuffer();
		Connection conn = null;
		try {			
			conn = PartnerUtils.getConnection();
			PartnerOrderAdjDAO orderAdjFeedDAO = new PartnerOrderAdjDAO(conn);
			orderAdjFeedDAO.processPartnerOrderRefunds(partnerId);			
		} catch (Exception e) {
			logger.error("Error caught processing refunds for partner orders: ", e);
			systemMessage.append("Error caught processing refunds for partner orders: " + e);
		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);
		}
	}
	
	
}
