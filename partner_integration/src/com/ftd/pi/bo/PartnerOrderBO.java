/**
 * 
 */
package com.ftd.pi.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.adapters.FTDOrderAdapter;
import com.ftd.pi.dao.PartnerMappingDAO;
import com.ftd.pi.dao.PartnerOrderAddOnDAO;
import com.ftd.pi.dao.PartnerOrderDAO;
import com.ftd.pi.dao.PartnerOrderItemDAO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.partner.vo.AddOn;
import com.ftd.pi.partner.vo.AddOns;
import com.ftd.pi.partner.vo.Order;
import com.ftd.pi.partner.vo.OrderItem;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerMappingVO;
import com.ftd.pi.vo.PartnerOrderAddOnsVO;
import com.ftd.pi.vo.PartnerOrderDetailVO;
import com.ftd.pi.vo.PartnerOrderVO;
import com.ftd.pi.vo.ftd.FTDOrder;
import com.ftd.pi.vo.ftd.Item;

/**
 * @author skatam
 *
 */
public class PartnerOrderBO 
{

	private Logger logger = new Logger(PartnerOrderBO.class.getName());
	
	private Connection conn;
	private PartnerMappingDAO partnerMappingDAO;
	private PartnerOrderDAO partnerOrderDAO;
	private PartnerOrderItemDAO partnerOrderItemDAO;
	private PartnerOrderAddOnDAO partnerOrderAddOnDAO;
    
    public PartnerOrderBO(Connection conn) {
    	this.conn = conn;
    	partnerOrderDAO = new PartnerOrderDAO(conn);
		partnerMappingDAO = new PartnerMappingDAO();
		partnerOrderItemDAO = new PartnerOrderItemDAO(conn);
	    partnerOrderAddOnDAO = new PartnerOrderAddOnDAO(conn);
	}
	
	public void processPartnerOrder(String partnerId, String partnerOrderNumber, int retryCount) 
	{
		Order order = this.partnerOrderDAO.getPartnerOrder(partnerId,partnerOrderNumber);
		if(order == null){
			
			if(retryCount<=3){
				retryCount += 1;
			boolean success = PartnerUtils.sendJMSMessageWithDelay(PIConstants.JMS_PIPELINE_FOR_EM_PARTNERS, 
					PIConstants.PROCESS_INBOUND_ORDER, partnerId+":"+partnerOrderNumber+":"+retryCount,true);
			
				if (!success)
				{
				throw new PartnerException("Unable to retry JMS message for partner order number = "+ partnerOrderNumber);
				}else{
				  logger.debug("Retried JMS message sent successfully");
				}
				
			}else{
				logger.error("Retry Limit Exceeded for : "+partnerOrderNumber+".");
				return;
			}
			logger.error("No Partner Order found with partnerOrderNumber: "+partnerOrderNumber+". Retrying the order processing.");
			return;
		}
		//String partnerId = order.getPartnerId();
		PartnerMappingVO partnerMappingVO = this.partnerMappingDAO.getPIPartnerMapping(partnerId);
		boolean validPartner = (partnerMappingVO!=null && 
											partnerMappingVO.getFtdOriginMapping()!=null && 
											partnerMappingVO.getFtdOriginMapping().trim().length() > 0);
		
		logger.debug("validPartner :"+validPartner);
		if(!validPartner)
		{
			String msg = "Received Partner Order with Invalid PartnerId :"+partnerId+". Not processing this Order.";
			logger.error(msg);
			PartnerUtils.sendPageSystemMessage(msg);
			return;
		}
		String origin = partnerMappingVO.getFtdOriginMapping();
		List<String> duplicateOrderItems = this.checkForDuplicateOrderItems(order, origin);//TODO
		if(duplicateOrderItems!= null && !duplicateOrderItems.isEmpty())
		{
			String msg = "Received duplicate Partner Order Item(s) :"+duplicateOrderItems+". Not processing this Order.";
			logger.error(msg);
			PartnerUtils.sendPageSystemMessage(msg);
		}
		else
		{
			FTDOrder ftdOrder = this.buildFTDOrder(partnerOrderNumber, order);
			if(ftdOrder != null)
			{
				this.partnerOrderDAO.saveFTDOrderXML(partnerOrderNumber, ftdOrder);
				boolean success = false;
				try {
					success = this.savePartnerOrderDetails(partnerOrderNumber,order, ftdOrder);
				} catch (Exception e) {
					String msg = "Unable to save Partner Order Item details. Error :"+ e;
					logger.error(msg, e);
					PartnerUtils.sendPageSystemMessage(msg);
				}
				if(success)
				{
					new FTDOrderGathererBO(conn).sendToOG(partnerId, partnerOrderNumber, ftdOrder);
				}
			}
			
		}	
	}
	
	private boolean savePartnerOrderDetails(String partnerOrderNumber,Order partnerOrder, FTDOrder ftdOrder) 
	{
		String partnerId = partnerOrder.getPartnerId();
		List<Item> items = ftdOrder.getItems();
		int itemCount = items.size();
		for (int i = 0; i < itemCount; i++) 
		{
			Item item = items.get(i);
			String orderItemNumber = item.getOrderItemNumber();
			OrderItem partnerOrderItem = getPartnerOrderItem(partnerOrder, orderItemNumber);
			PartnerOrderDetailVO orderDetailVO = new PartnerOrderDetailVO();
			//TODO
			orderDetailVO.setPartnerItemNumber(orderItemNumber);
			orderDetailVO.setConfirmationNumber(item.getOrderNumber());
			orderDetailVO.setPartnerId(partnerId);
			orderDetailVO.setPartnerOrderNumber(partnerOrderNumber);
			orderDetailVO.setProductId(item.getProductid());
			//TODO

			 String shipping = item.getServiceFee();
			String tax = null;
			try {
				tax = item.getTaxes().getTotalTax().getTotalAmount().toString();
			} catch (Exception e) {
				logger.info("Tax is not set on item");
				tax = "0"; 
			}
           
			 orderDetailVO.setDiscountCode(item.getItemSourceCode());
			 orderDetailVO.setRetailPrice(new BigDecimal(item.getProductPrice()));
			 orderDetailVO.setSalePrice(new BigDecimal(item.getDiscountedProductPrice()));
			 orderDetailVO.setPlacement(partnerOrderItem.getCustomFieldValue("Placement"));//TODO move to constants
		     orderDetailVO.setAddOnRetailPrice(item.getAddOnAmount().add(item.getAddOnDiscountAmount()));
		     orderDetailVO.setAddOnSalePrice(item.getAddOnAmount());
			 
		     orderDetailVO.setLegacyID(partnerOrderItem.getCustomFieldValue("LegacyID"));
		     
		     if (shipping == null || shipping.equals("")) {
            	  orderDetailVO.setShippingAmount(new BigDecimal("0"));
              } else {
            	  orderDetailVO.setShippingAmount(new BigDecimal(shipping));
              }
              if (tax == null || tax.equals("")) {
            	  orderDetailVO.setTaxAmount(new BigDecimal("0"));
              } else {
            	  orderDetailVO.setTaxAmount(new BigDecimal(tax));
              }
			this.partnerOrderItemDAO.insertPartnerOrderDetails(partnerOrderNumber, orderDetailVO);
		    
			this.savePartnerOrderDetailAddOns(orderItemNumber, partnerOrderItem.getAddOns());
		}
		return true;
	}
	
	private void savePartnerOrderDetailAddOns(String orderItemNumber,
			AddOns partnerAddOns) {
		long partnerOrderDetailId;
		if ((partnerAddOns != null) && (partnerAddOns.getAddOn() != null)
				&& (!partnerAddOns.getAddOn().isEmpty())) {
			List<AddOn> partnerAddOnList = partnerAddOns.getAddOn();

			partnerOrderDetailId = this.partnerOrderItemDAO
					.getPartnerOrderDetailId(orderItemNumber).longValue();

			if (partnerOrderDetailId == 0L) {
				throw new PartnerException(
						"Partner Order Detail Id is expected and it can't be empty. Exiting the process.");
			}

			for (AddOn partnerAddOn : partnerAddOnList) {
				PartnerOrderAddOnsVO partnerOrderAddOnsVO = new PartnerOrderAddOnsVO();
				partnerOrderAddOnsVO.setAddOnId(partnerAddOn.getId());
				partnerOrderAddOnsVO.setRetailPrice(partnerAddOn
						.getAddOnRetailPrice());
				partnerOrderAddOnsVO.setSalePrice(partnerAddOn
						.getAddOnSalePrice());
				partnerOrderAddOnsVO.setQuantity(partnerAddOn.getQuantity());
				partnerOrderAddOnsVO
						.setPartnerOrderDetailId(partnerOrderDetailId);
				partnerOrderAddOnsVO.setCreatedBy("SYS");
				partnerOrderAddOnsVO.setUpdatedBy("SYS");

				this.partnerOrderAddOnDAO
						.insertPartnerOrderAddOns(partnerOrderAddOnsVO);
			}
		}
	}
	
	private OrderItem getPartnerOrderItem(Order partnerOrder, String orderItemNumber)
	{
		List<OrderItem> items = partnerOrder.getOrderItem();
		for (OrderItem orderItem : items) {
			if(orderItem.getPartnerOrderItemID().equals(orderItemNumber)){
				return orderItem;
			}
		}
		return null;
	}

	private FTDOrder buildFTDOrder(String partnerOrderNumber, Order order) {
		logger.debug("Building FTDOrder for Partner Order:"+partnerOrderNumber);
		FTDOrderAdapter ftdOrderAdapter = new FTDOrderAdapter(conn);
		FTDOrder ftdOrder = null;
		try {
			ftdOrder = ftdOrderAdapter.getAsFTDOrder(order);
		} catch (Exception e) {
			logger.error(e);
			String msg = "Error in converting Partner Order XML to FTD Order XML. PartnerOrderNumber: "+partnerOrderNumber;
			logger.error(msg);
			PartnerUtils.sendPageSystemMessage(msg);
		}	
		return ftdOrder;
	}

	private List<String> checkForDuplicateOrderItems(Order order, String origin) {
		List<String> duplicateOrderItemIds = new ArrayList<String>();
		List<OrderItem> items = order.getOrderItem();
		//List<Item> items = ftdOrder.getItems();
		int itemCount = items.size();
		//First check for duplicate OrderItemID in the same Order
		Set<String> orderItemIds = new HashSet<String>();
		for (int i = 0; i < itemCount; i++) 
		{
			OrderItem item = items.get(i);
			boolean added = orderItemIds.add(order.getPartnerId()+"_"+item.getPartnerOrderItemID());
			if(!added){
				duplicateOrderItemIds.add(order.getPartnerId()+"_"+item.getPartnerOrderItemID());
			}
		}
		if(!duplicateOrderItemIds.isEmpty()){
			return duplicateOrderItemIds;
		}
		for (int i = 0; i < itemCount; i++) 
		{
			OrderItem item = items.get(i);
			if(this.partnerOrderItemDAO.isDuplicateOrderItem(order.getPartnerId(), item.getPartnerOrderItemID())){
				duplicateOrderItemIds.add(order.getPartnerId()+"_"+item.getPartnerOrderItemID());
			}
		}
		return duplicateOrderItemIds;
	}

	public void processOGErrorOrders() 
	{
		logger.debug("Processing OG Error Orders.");
		List<PartnerOrderVO> orders = partnerOrderDAO.getFTDOrderXmlsByStatus(PIConstants.ORDER_STATUS_GATHERER_ERROR);
		for (PartnerOrderVO partnerOrderVO : orders) 
		{
			String partnerId = partnerOrderVO.getPartnerId();
			String partnerOrderNumber = partnerOrderVO.getPartnerOrderNumber();
			logger.debug("Resending FTD Order for PartnerOrderNumber :"+partnerOrderNumber);
			FTDOrder ftdOrder = null;
			if(partnerOrderVO.getFtdXml() != null){
		    	ftdOrder = (FTDOrder) PartnerUtils.marshallAsObject(partnerOrderVO.getFtdXml(), FTDOrder.class);
		    }
			new FTDOrderGathererBO(conn).sendToOG(partnerId, partnerOrderNumber, ftdOrder);
		}
			
	}

}
