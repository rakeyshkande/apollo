package com.ftd.pi.bo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.PartnerOrderFulfillmentDAO;
import com.ftd.pi.partner.vo.DeliveryStatusEnum;
import com.ftd.pi.partner.vo.DeliveryTypeEnum;
import com.ftd.pi.partner.vo.FTDFeed;
import com.ftd.pi.partner.vo.OrderFulfillment;
import com.ftd.pi.partner.vo.OrderItemFulfillment;
import com.ftd.pi.partner.vo.OrderItemFulfillment.FulfillmentData;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerOrderFulfillFeedVO;
import com.ftd.pi.vo.PartnerOrderItemFulFillFeedVO;

/**
 * @author smeka
 *
 */
public class PartnerOrderFulfillBO extends PartnerFeedBO {
	private static Logger logger = new Logger(PartnerOrderFulfillBO.class.getName());

	/** (non-Javadoc)
	 * @see com.ftd.pi.bo.PartnerFeedBO#createPartnerFeed(java.lang.String)
	 */
	public void createPartnerFeed(String partnerId) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering createPartnerFeed() *********");
		}
		
		Connection conn = null;
		PartnerOrderFulfillmentDAO feedDAO = null;
		StringBuffer systemMessage = new StringBuffer();
		
		try {			
			conn = PartnerUtils.getConnection();
			feedDAO = new PartnerOrderFulfillmentDAO(conn);
			
			Map<String, PartnerOrderFulfillFeedVO> orderFulfillments = feedDAO.getPartnerFeedData(partnerId, PIConstants.NEW_FEED_STATUS);
			
			if(orderFulfillments == null || orderFulfillments.isEmpty()){
	    		logger.info("No new Partner Order Fulfillments Feed data found");
	    		return;
	    	}		
			
			Set<String> keySet = orderFulfillments.keySet();
			
			for (String ftdOrderId : keySet) {					
				PartnerOrderFulfillFeedVO partnerOrderFulfillFeed = orderFulfillments.get(ftdOrderId);
				
				String feedId = savePartnerFeed(this.buildOrderFulfillFeed(partnerOrderFulfillFeed), partnerOrderFulfillFeed.getFeedType(), conn);
				
				if(feedId !=null && !StringUtils.isEmpty(feedId)) {
					logger.info("Succesfully created feed with Id: " + feedId);
				
					List<String> partnerFeedDataIds = new ArrayList<String>(orderFulfillments.size());				
					for (PartnerOrderItemFulFillFeedVO orderItemFulfill : partnerOrderFulfillFeed.getItemFulFillFeeds()) {
						partnerFeedDataIds.add(orderItemFulfill.getPartnerOrderItemFulFillId());
					}
					
					if(!feedDAO.updatePartnerFeedId(partnerFeedDataIds, feedId, PIConstants.FULFILLMENT_TYPE, PIConstants.SENT_FEED_STATUS)) {
						systemMessage.append("Unable to update the feed Id for the Partner fulfillment orders." + partnerFeedDataIds);
					}
				} else {
					systemMessage.append("Unable to insert the Partner feed for the Partner fulfillment orders." + ftdOrderId);
				}
			}
		
		} catch (Exception e) {
			logger.error(" Error caught creating Partner order fulfillment feeds. sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught creating Partner fulfillment feeds." + e.getMessage());
		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);			
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("********* Exiting createPartnerFeed() *********");
		}	
	}

	/** Build Order Fulfillment feed.
	 * @param partnerOrderFulfillFeed
	 * @return
	 */
	private FTDFeed buildOrderFulfillFeed (PartnerOrderFulfillFeedVO partnerOrderFulfillFeed) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering buildOrderFulfillFeed() *********");
		}
		FTDFeed ftdFeed = new FTDFeed();	
		OrderFulfillment orderFulfillment = new OrderFulfillment();
		orderFulfillment.setPartnerOrderID(partnerOrderFulfillFeed.getPartnerOrderNumber());
		orderFulfillment.setFTDOrderID(partnerOrderFulfillFeed.getFtdOrderNumber());
		orderFulfillment.setPartnerId(partnerOrderFulfillFeed.getPartnerId());
		
		for (PartnerOrderItemFulFillFeedVO fulfillFeed : partnerOrderFulfillFeed.getItemFulFillFeeds()) {				
			
			OrderItemFulfillment itemFulfillment = new OrderItemFulfillment();
			itemFulfillment.setPartnerOrderItemID(fulfillFeed.getPartnerOrderItemNumber());
			itemFulfillment.setFTDOrderItemID(fulfillFeed.getFtdExternalOrderNumber());
			
			FulfillmentData fulfillmentData = new FulfillmentData();
			if(!StringUtils.isEmpty(fulfillFeed.getCarrier())) {
				DeliveryTypeEnum deliveryType = DeliveryTypeEnum.fromValue(fulfillFeed.getCarrier());
				if(deliveryType != null) {
					fulfillmentData.setDeliveryType(deliveryType);
				}
			}
			fulfillmentData.setDeliveryDate(fulfillFeed.getDeliveryDate());
			fulfillmentData.setShippingMethod(fulfillFeed.getShippingMethod());
			fulfillmentData.setShippingTrackingNumber(fulfillFeed.getShippingTrackingNumber());
			
			// Populate the delivery status information if it is present
			String deliverystatus = fulfillFeed.getDeliveryStatus();
			if(deliverystatus != null && !deliverystatus.isEmpty()) {
				fulfillmentData.setDeliveryStatus(DeliveryStatusEnum.fromValue(deliverystatus));
				fulfillmentData.setDeliveryStatusDateTime(fulfillFeed.getDeliveryStatusDateTime());
			}
			
			itemFulfillment.setFulfillmentData(fulfillmentData);
			orderFulfillment.getOrderItemFulfillment().add(itemFulfillment);		
		}	
		ftdFeed.getOrderFulfillment().add(orderFulfillment);
		if(logger.isDebugEnabled()) {
			logger.debug("********* End buildOrderFulfillFeed() *********");
		}
		return ftdFeed;
	}

	/**Check if there are any partner orders delivered by FLORIST and insert fulfillment records for the same.
	 * @param partnerId
	 */
	public void processFloristFulfilledOrders(String partnerId) {
		if (logger.isDebugEnabled()) {
			logger.debug("********* Entering processFloristFulfilledOrders() *********");
		}
		StringBuffer systemMessage = new StringBuffer();
		Connection conn = null;
		try {
			conn = PartnerUtils.getConnection();
			PartnerOrderFulfillmentDAO feedDAO = new PartnerOrderFulfillmentDAO(conn);
			feedDAO.createFloristOrderFullfillmentData(partnerId);
		} catch (Exception e) {
			systemMessage.append("Error caught and failed to insert fulfillment records for partner florist orders, " + e);
			logger.error("Error caught and failed to insert fulfillment records for partner florist orders: ", e);
			e.printStackTrace();
		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("********* End processFloristFulfilledOrders() *********");
		}

	}

}
