/**
 * 
 */
package com.ftd.pi.bo;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.Marshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.PartnerFeedDAO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.partner.vo.FTDFeed;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.EsbResponse;

/**
 * @author smeka
 *
 */
public class PartnerFeedBO {
	private static Logger logger = new Logger(PartnerFeedBO.class.getName());

	/** MARSHALL the FTD feed sent and saves the partner FTD feed to Apollo DB.
	 * @param ftdFeed
	 * @param feedType
	 * @param conn
	 * @return
	 */
	public String savePartnerFeed(FTDFeed ftdFeed, String feedType, Connection conn) {		
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering createFeed() for " + feedType + "********* ");		
		}		
		try {
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			String feedContent = PartnerUtils.marshallAsXML(ftdFeed, props);	

			//if(logger.isDebugEnabled()) {
				//logger.debug("Partner Feed XML : " + feedContent);
			//}
			return new PartnerFeedDAO(conn).insertPartnerFeed(feedContent, feedType);	
			
		} catch(Exception e){
			throw new PartnerException("Unable to save the feed to apollo for feedType: " + feedType + ", " + e.getMessage());			
		} 
	}
	
	/** Perform the job to send the partner feeds of a given type to ESB.
	 * @param feedType
	 */
	public void sendPartnerFeed(String feedType) {		
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering sendPartnerFeed() for " + feedType + "********* ");			
		}	
		Connection conn = null;
		PartnerFeedDAO feedDAO = null;
		StringBuffer systemMessage = new StringBuffer();
		try {
			conn = PartnerUtils.getConnection();
			feedDAO = new PartnerFeedDAO(conn);
			
			CachedResultSet feeds = feedDAO.getPartnerFeedsNotSent(feedType);
			
			if(feeds == null) {
				logger.info("No new feeds to be sent to ESB");
				return;
			}
			
			String status = null;
			String feedId = null;
			
			while(feeds.next()) {
				try {
					feedId = feeds.getString("FEED_ID");
					
					logger.info("**********Sending feed to ESB, " + feedId + " **************");
					status = sendToESB(feeds.getClob("FEED_XML"), feedType);
					
					try {
						logger.info("**********Updating feed status as, " + status + " **************");
						feedDAO.updateFeedStatus(feedId, status);
					} catch (Exception e) {
						logger.error("Error caught updating feed status: " + feedId + ", " + e);
						systemMessage.append("Error updating feed status: " + feedId + ", " + e);
					}
					
				} catch (Exception e) {
					logger.error("Error caught sending feed to ESB: " + feedId + ", " + e);
					systemMessage.append("Error caught sending feed to ESB: " + feedId + ", " + e);
				}
			}			
		} catch (Exception e) {
			logger.error(" Error caught sending feeds to ESB. Sending system message. " + e);
			e.printStackTrace();
			systemMessage.append("Error caught sending feeds to ESB." + e);
		} finally {
			if (systemMessage.length() > 0) {
			    PartnerUtils.sendNoPageSystemMessage(systemMessage.toString());
			}
			PartnerUtils.closeConnection(conn);			
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("********* Exiting sendPartnerFeed() *********");
		}
	}

	/**
	 * This method sends the Adjustment/Fulfillment feed to ESB. It makes a HTTP
	 * call to ESB to send the feeds. Response is captured, if its 200 then
	 * "sent" is returned and in case of failure "failurefailuremessage" is
	 * returned
	 * 
	 * @param clob
	 * @param feedType
	 * @return Response String
	 * @throws SQLException
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	private String sendToESB(Clob clob, String feedType) throws SQLException, Exception {
		
		String errorMsg = null;
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String esbUrl;

		// Retrieving esb url from global parameters
		if (feedType.equals(PIConstants.ADJUSTMENT_TYPE)) {
			esbUrl = cu.getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT, PIConstants.ESB_ADJUSTMENT_GATHERER_URL);
			
		} else if (feedType.equals(PIConstants.FULFILLMENT_TYPE) || feedType.equals(PIConstants.DELIVERTY_CONFIRMATION_TYPE)) {
			esbUrl = cu.getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT, PIConstants.ESB_FULFILLMENT_GATHERER_URL);
			
		} else if (feedType.equals(PIConstants.ORD_STATUS_UPD) || feedType.equals(PIConstants.INVOICE_TYPE)) {
			esbUrl = cu.getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT, PIConstants.ESB_ORDSTATUS_UPD_GATHERER_URL);
		} else {
			return PIConstants.FAILED_FEED_STATUS + "Invalid Feed type";
		}

		if (esbUrl == null || esbUrl.isEmpty()) {
			logger.error("Empty/null ESB url retrieved for feed type: " + feedType);
			throw new Exception("Empty/null ESB url retrieved for feed type: " + feedType);
		}

		// Retrieving ESB credentials from secure configuration table
		String esbWSAuthUsername = cu.getSecureProperty(PIConstants.PI_SECURE_CONTEXT, PIConstants.ESB_AUTH_USERNAME);
		String esbWSAuthPassword = cu.getSecureProperty(PIConstants.PI_SECURE_CONTEXT, PIConstants.ESB_AUTH_PASSWORD);

		if (esbWSAuthUsername == null || esbWSAuthUsername.isEmpty()) {
			logger.error("Empty/null ESB username retrieved");
			throw new Exception("Empty/null ESB username retrieved");
		}

		if (esbWSAuthPassword == null || esbWSAuthPassword.isEmpty()) {
			logger.error("Empty/null ESB password retrieved");
			throw new Exception("Empty/null ESB password retrieved");
		}

		PostMethod post = new PostMethod(esbUrl);
		post.setRequestHeader("Content-Type", "application/xml");
		post.setRequestHeader("FTD_AUTH_USERNAME", esbWSAuthUsername);
		post.setRequestHeader("FTD_AUTH_PASSWORD", esbWSAuthPassword);

		if (logger.isDebugEnabled()) {
			logger.debug("Sending feed to ESB. Feed content:" + clob.getSubString(1L, (int) clob.length()));
		}
		
		// Setting feed content into request body
		post.setRequestBody(clob.getSubString(1L, (int) clob.length()));

		HttpClient client = new HttpClient();
		
		// Making HTTP post call to ESB
		int result = client.executeMethod(post);
		String response = null;
		if (result == 200) {
			response = post.getResponseBodyAsString();
			EsbResponse responseObj = (EsbResponse) PartnerUtils.marshallAsObject(response, EsbResponse.class);
			if (responseObj.getStatusCode() == 500) {
				errorMsg = new String(responseObj.getFailureDescription());
			}
		} else {
			errorMsg = new String(response);
		}

		if (errorMsg != null) {
			logger.error("Failed in sending feed to ESB. Error message:" + errorMsg);
			return PIConstants.FAILED_FEED_STATUS + "Error:" + errorMsg;
		}

		if (logger.isDebugEnabled()) {
			logger.info("Feed successfully send to ESB. " + clob.getSubString(1L, (int) clob.length()));
		}
		return PIConstants.SENT_FEED_STATUS;
	}		
}
