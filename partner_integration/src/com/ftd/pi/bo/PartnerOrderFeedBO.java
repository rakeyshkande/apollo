/**
 * 
 */
package com.ftd.pi.bo;

import java.sql.Connection;
import java.util.List;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.PartnerMappingDAO;
import com.ftd.pi.dao.PartnerOrderDAO;
import com.ftd.pi.dao.PartnerOrderFeedDAO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.partner.vo.Order;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerOrderFeed;

/**
 * @author skatam
 *
 */
public class PartnerOrderFeedBO 
{

	private Logger logger = new Logger(PartnerOrderFeedBO.class.getName());
	
	//private Connection conn;
	private PartnerMappingDAO partnerMappingDAO;
	private PartnerOrderFeedDAO partnerOrderFeedDAO;
	private PartnerOrderDAO partnerOrderDAO;
    
    public PartnerOrderFeedBO(Connection conn) {
    	//this.conn = conn;
    	partnerOrderDAO = new PartnerOrderDAO(conn);
		partnerOrderFeedDAO = new PartnerOrderFeedDAO(conn);
		partnerMappingDAO = new PartnerMappingDAO();
	}


	public String savePartnerOrderFeed(PartnerOrderFeed partnerOrderFeed) 
	{
		return partnerOrderFeedDAO.savePartnerOrderFeed(partnerOrderFeed);
	}

	public List<PartnerOrderFeed> getPartnerOrderFeeds(String status) 
	{
		return this.partnerOrderFeedDAO.getPartnerOrderFeeds(status);
	}

	public PartnerOrderFeed getPartnerOrderFeed(String partnerOrderFeedId) {
		String partnerOrderXML = this.partnerOrderFeedDAO.getPartnerOrderFeed(partnerOrderFeedId);
		Order order = (Order) PartnerUtils.marshallAsObject(partnerOrderXML, Order.class);
		return new PartnerOrderFeed(partnerOrderFeedId, order);
	}
	
	public void processPartnerOrderFeed(PartnerOrderFeed partnerOrderFeed) 
	{
		//commented this line as part of PCI code review
		//logger.debug("Processing OrderFeed:\n "+partnerOrderFeed.getAsXmlString());
		Order order = partnerOrderFeed.getOrder();
			
		boolean validPartnerId = partnerMappingDAO.isValidPartnerId(order.getPartnerId());
		if(!validPartnerId)
		{
			String msg = "Received Partner Order with Invalid PartnerId :"+order.getPartnerId()+
							" in OrderFeed with ID :"+partnerOrderFeed.getOrderFeedId()+". Not processing this Order.";
			logger.error(msg);
			PartnerUtils.sendPageSystemMessage(msg);
		}
		else
		{
			boolean duplicateOrder = this.partnerOrderDAO.isDuplicateOrder(order);
    		if(duplicateOrder)
    		{
    			String msg = "Received a duplicate Partner Order. PartnerId:"
					+order.getPartnerId()+", PartnerOrderId: "+order.getPartnerOrderID()
					+" in OrderFeed with ID :"+partnerOrderFeed.getOrderFeedId();
    			logger.error(msg);
    			PartnerUtils.sendPageSystemMessage(msg);
    		}
    		else
    		{
    			String partnerOrderNumber = this.partnerOrderDAO.insertPartnerOrder(partnerOrderFeed.getOrderFeedId(), order);
    	    	logger.debug("partnerOrderNumber : "+partnerOrderNumber);
    			this.triggerPartnerOrderProcessing(order.getPartnerId()+":"+partnerOrderNumber);
    	    	
    		}
		}    		
	}
	
	public void updateOrderFeedStatus(String orderFeedId, String orderStatus) {
		this.partnerOrderFeedDAO.updateOrderFeedStatus(orderFeedId, orderStatus);
	}

	private void triggerPartnerOrderProcessing(String partnerOrderID) {
		logger.debug("Sending Partner Order ID :"+partnerOrderID+" to JMS queue.");
		
		int retryCount = 0;
		boolean success = PartnerUtils.sendJMSMessage(PIConstants.JMS_PIPELINE_FOR_EM_PARTNERS, 
								PIConstants.PROCESS_INBOUND_ORDER, partnerOrderID+":"+retryCount);

	      if (!success)
	      {
	        throw new PartnerException("Unable to send JMS message for partner order number = "+ partnerOrderID);
	      }else{
	    	  logger.debug("Sent successfully");
	      }
	}


}
