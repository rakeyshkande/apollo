package com.ftd.pi.dao;

import java.sql.Connection;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerOrderFulfillFeedVO;
import com.ftd.pi.vo.PartnerOrderItemFulFillFeedVO;

/**
 * @author smeka
 *
 */
public class PartnerOrderFulfillmentDAO extends PartnerFeedDAO {
	
	private static Logger logger = new Logger(PartnerOrderFulfillmentDAO.class.getName());
	private static final String DELIVERY_CONFIRMATION_STATUS = "Delivered";
	private static final String DELIVERY_STATUS_DATETIME_TYPE = "yyyy-dd-MM HH:mm:ss";
	public PartnerOrderFulfillmentDAO(Connection conn) {
		this.connection = conn;
	}

	/** Get the fulfillment records inserted into partner order fulfillment tables.
	 * @param partnerId
	 * @param feedStatus
	 * @return
	 */
	public Map<String, PartnerOrderFulfillFeedVO> getPartnerFeedData(String partnerId, String feedStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getPartnerFeedData() *********");
		}
		
		try {
			Map<String, PartnerOrderFulfillFeedVO> orderFulfillData = new HashMap<String, PartnerOrderFulfillFeedVO>();

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID(GET_ORD_FULFILL_BY_STATUS_STMT);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_PARTNER_ID, partnerId);
			inputParams.put(IN_FEED_STATUS, feedStatus);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			
			while (crs.next()) {
				PartnerOrderFulfillFeedVO feedDetail = null;
				
				if(!orderFulfillData.containsKey(crs.getString(MASTER_ORDER_NUMBER))) {
					feedDetail = new PartnerOrderFulfillFeedVO();					
					feedDetail.setFeedType(PIConstants.FULFILLMENT_TYPE);					
					feedDetail.setFtdOrderNumber(crs.getString(MASTER_ORDER_NUMBER));
			    	feedDetail.setPartnerOrderNumber(crs.getString(PARTNER_ORDER_NUMBER));
			    	feedDetail.setPartnerId(crs.getString(PARTNER_ID));
			    	feedDetail.setPartnerName(crs.getString(PARTNER_NAME));
			    	orderFulfillData.put(crs.getString(MASTER_ORDER_NUMBER), feedDetail);
				} else {
					feedDetail = orderFulfillData.get(crs.getString(MASTER_ORDER_NUMBER));
				}
			    
				PartnerOrderItemFulFillFeedVO orderItemFulfillData = new PartnerOrderItemFulFillFeedVO();
				orderItemFulfillData.setPartnerOrderItemFulFillId(crs.getString(PTN_ORDER_FULFILLMENT_ID));
				orderItemFulfillData.setShippingMethod(crs.getString(SHIPPING_METHOD));
				orderItemFulfillData.setCarrier(crs.getString(CARRIER_NAME));
				orderItemFulfillData.setShippingTrackingNumber(crs.getString(TRACKING_NUMBER));
				
				orderItemFulfillData.setPartnerOrderItemNumber(crs.getString(PARTNER_ORDER_ITEM_NUMBER));
				orderItemFulfillData.setFtdExternalOrderNumber(crs.getString(CONFIRMATION_NUMBER));
				orderItemFulfillData.setItemSKU(crs.getString(ITEM_PROD_ID));
						    	
		    	if(crs.getString(DELIVERY_DATE) != null) {
		    	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM");	//2013-27-03	    	   
		    	    Calendar c  = Calendar.getInstance();
		    	    c.setTime(sdf.parse(crs.getString(DELIVERY_DATE)));
		    	    orderItemFulfillData.setDeliveryDate(PartnerUtils.toXMLGregorianCalendar(c.getTime()));
		    	}
		    	// Below details are populated based on the delivery status value. 
		    	String deliveryStatus = crs.getString(DELIVERY_STATUS);
		    	if(deliveryStatus != null && !deliveryStatus.isEmpty()) {
		    		if(DELIVERY_CONFIRMATION_STATUS.equalsIgnoreCase(deliveryStatus)) {
		    			// change the feed type
		    			feedDetail.setFeedType(PIConstants.DELIVERTY_CONFIRMATION_TYPE);
		    			orderItemFulfillData.setDeliveryStatus(deliveryStatus);		    		
			    		
			    		SimpleDateFormat sdf = new SimpleDateFormat(DELIVERY_STATUS_DATETIME_TYPE);	//2001-12-31 13:00:00	   
			    		GregorianCalendar gc  = new GregorianCalendar();
			    	    gc.setTime(sdf.parse(crs.getString(DELIVERY_STATUS_DATETIME)));
			    	    
			    		orderItemFulfillData.setDeliveryStatusDateTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(gc));
		    		}		    		
		    	}
		    	feedDetail.getItemFulFillFeeds().add(orderItemFulfillData);
			}
			return orderFulfillData;
			
		} catch(Exception e) {
			logger.error("Error caught geting fulfillment records for partner orders: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}

	/** Checks if there is any FLORIST partner order fulfilled, 
	 * and inserts a record for the same in Partner fulfillment tables.
	 * @param partnerId
	 */
	public void createFloristOrderFullfillmentData(String partnerId) {
		if (logger.isDebugEnabled()) {
			logger.debug("********* Entering insertFloristFulfilledOrders() *********");
		}
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.reset();
			dataRequest.setConnection(this.connection);
			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_PARTNER_ID, partnerId);
			dataRequest.setInputParams(inputParams);
			dataRequest.setStatementID(INSERT_FLORIST_FULFILLMENT_DATA_STMT);
			DataAccessUtil dau = DataAccessUtil.getInstance();
			
			@SuppressWarnings("rawtypes")
			Map result = (Map) dau.execute(dataRequest);

			String status = (String) result.get(OUT_STATUS);
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N")) {
				String message = (String) result.get(OUT_MESSAGE);
				throw new Exception(message);
			}
		} catch (Exception e) {
			logger.error("Error caught inserting florist fulfillment records for partner orders: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}

}
