/**
 * 
 */
package com.ftd.pi.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.vo.PartnerOrderAddOnsVO;

/**
 * @author kdatchan
 *
 */
public class PartnerOrderAddOnDAO 
{
	private Logger logger = new Logger(PartnerOrderAddOnDAO.class.getName());
	
	private Connection conn;
	public PartnerOrderAddOnDAO(Connection conn) {
		this.conn = conn;
	}

	@SuppressWarnings("unchecked")
	public void insertPartnerOrderAddOns(PartnerOrderAddOnsVO partnerOrderAddOnsVO) 
	{
		Map<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_PARTNER_ORDER_DETAIL_ID", partnerOrderAddOnsVO.getPartnerOrderDetailId());
	    inputParams.put("IN_ADD_ON_ID", partnerOrderAddOnsVO.getAddOnId());
	    inputParams.put("IN_ADD_ON_RETAIL_PRICE", partnerOrderAddOnsVO.getRetailPrice());
	    inputParams.put("IN_ADD_ON_SALE_PRICE", partnerOrderAddOnsVO.getSalePrice());
	    inputParams.put("IN_QUANTITY", partnerOrderAddOnsVO.getQuantity());
	    inputParams.put("IN_CREATED_BY", partnerOrderAddOnsVO.getCreatedBy());
	    inputParams.put("IN_UPDATED_BY", partnerOrderAddOnsVO.getUpdatedBy());
	    
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("INSERT_PARTNER_ORDER_ADD_ONS");
	
	    try {
			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("insertPartnerOrderAddOns status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
				StringBuilder message = new StringBuilder("Unable to insert PartnerOrderAddOns"); 
				message.append((String) result.get("OUT_MESSAGE"));
				throw new Exception(message.toString());
			}
		} catch (Exception e) {
			throw new PartnerException(e);
		}
	}

}
