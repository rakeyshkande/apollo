/**
 * 
 */
package com.ftd.pi.dao;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PartnerMappingsHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerMappingVO;

/**
 * @author skatam
 *
 */
public class PartnerMappingDAO {
	private Logger logger = new Logger(PartnerMappingDAO.class.getName());

	public PartnerMappingVO getPIPartnerMapping(String partnerId) {		 
		PartnerMappingVO partnerMappingVO = PartnerUtils.getPIPtnMappingVO(getUtilPartnerMapping(partnerId));		
		logger.debug("partnerMapping: " + partnerMappingVO);
		return partnerMappingVO;
	}
	
	public com.ftd.osp.utilities.vo.PartnerMappingVO getUtilPartnerMapping(String partnerId) {
		logger.debug("getPartnerMapping :" + partnerId);
		
		PartnerMappingsHandler partnerMappingHandler = (PartnerMappingsHandler) CacheManager.getInstance().getHandler("CACHE_NAME_PARTNER_MAPPINGS");
		com.ftd.osp.utilities.vo.PartnerMappingVO mappingVO = partnerMappingHandler.getPartnerMappingVO(partnerId);	 
		
		logger.debug("partnerMapping: " + mappingVO);
		return mappingVO;
	}


	public boolean isValidPartnerId(String partnerId) {
		if (partnerId == null || partnerId.trim().length() == 0) {
			return false;
		}
		PartnerMappingVO mappingVO = this.getPIPartnerMapping(partnerId);
		return mappingVO != null && mappingVO.getFtdOriginMapping() != null && mappingVO.getFtdOriginMapping().trim().length() > 0;
	}
}
