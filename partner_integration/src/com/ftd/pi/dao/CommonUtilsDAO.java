/**
 * 
 */
package com.ftd.pi.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.pi.exceptions.PartnerException;

/**
 * @author skatam
 *
 */
public class CommonUtilsDAO {

	private Logger logger = new Logger(CommonUtilsDAO.class.getName());
	
	private Connection conn;
	public CommonUtilsDAO(Connection conn) {
		this.conn = conn;
	}
	public Map<String,Object> getIOTWSourceCode(String sourceCode, String itemSku, String dateReceived)
	{
		Map<String,Object> iotwMap = new HashMap<String,Object>();
		logger.debug("getIOTWSourceCode()");

	    String iotwSourceCode;
		BigDecimal discount;
		String discountType;
		try {
			DataRequest request = new DataRequest();
			  request.reset();
			  request.setConnection(conn);
			  Map<String,Object> inputParams = new HashMap<String,Object>();
			  inputParams.put("IN_SOURCE_CODE", sourceCode);
			  inputParams.put("IN_PRODUCT_ID", itemSku);
			  inputParams.put("IN_TIME_RECEIVED", dateReceived);
			  request.setInputParams(inputParams);
			  request.setStatementID("GET_IOTW_SOURCE_CODE");

			  CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
			  
			  
			  iotwSourceCode = sourceCode;
			  discount = new BigDecimal("0");
			  discountType = "";
			  while ( results != null && results.next() ) 
			  {
				  iotwSourceCode = results.getString("IOTW_SOURCE");
				  discount = results.getBigDecimal("DISCOUNT");
				  discountType = results.getString("DISCOUNT_TYPE");
			  }
			  iotwMap.put("iotwSourceCode", iotwSourceCode);
		      iotwMap.put("discount", discount);
		      iotwMap.put("discountType", discountType);
		      logger.info("iotwSourceCode: " + iotwSourceCode);
		      
		} catch (Exception e) {
			throw new PartnerException(e);
		}
	      
		return iotwMap;
	}
	
	public Map<String,Object> getPdbPriceData(String sku)
	{
		Map<String,Object> pdbPriceMap = new HashMap<String,Object>();
		  logger.debug("getPdbPriceData()");

	      try {
			DataRequest request = new DataRequest();
			  request.reset();
			  request.setConnection(conn);
			  Map<String,Object> inputParams = new HashMap<String,Object>();
			  inputParams.put("IN_PRODUCT_ID", sku);        
			  request.setInputParams(inputParams);
			  request.setStatementID("GET_PDB_DATA");

			  CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
			  
			  while ( results != null && results.next() ) 
			  {
				  pdbPriceMap.put("standardPrice", results.getBigDecimal("STANDARD_PRICE"));
				  pdbPriceMap.put("deluxePrice", results.getBigDecimal("DELUXE_PRICE"));
				  pdbPriceMap.put("premiumPrice", results.getBigDecimal("PREMIUM_PRICE"));
				  pdbPriceMap.put("shipMethodFlorist", results.getString("SHIP_METHOD_FLORIST"));
				  pdbPriceMap.put("shipMethodCarrier", results.getString("SHIP_METHOD_CARRIER"));
			  }
		} catch (Exception e) {
			throw new PartnerException(e);
		} 
	      
	      return pdbPriceMap;
	}
	
	public Map<String,Object> getProductByTimestamp(String productId, Date priceDateTime)
	{		
		Map<String,Object> productPriceMap = new HashMap<String,Object>();
        // Query the historical product data.
		logger.debug("getProductByTimestamp()");

		try 
		{
			DataRequest request = new DataRequest();
			request.setConnection(conn);
			request.setStatementID("GET_PRODUCT_DETAILS_BY_TIMESTAMP");
			request.addInputParam("IN_PRODUCT_ID", productId);
			request.addInputParam("IN_DATE_TIME", new java.sql.Timestamp(priceDateTime.getTime()));
			DataAccessUtil dau = DataAccessUtil.getInstance();
			CachedResultSet rs = (CachedResultSet)dau.execute(request);
			rs.reset();
			request.reset();
			
			// Only one record (or none) should return.
			if (rs.next()) 
			{
			    // Record the historical standard price.
				BigDecimal standardPrice = rs.getBigDecimal("standard_price");
				productPriceMap.put("standardPrice", standardPrice);
			    // Historical premium and deluxe prices.
				BigDecimal deluxePrice = rs.getBigDecimal("deluxe_price");
				productPriceMap.put("deluxePrice", deluxePrice);
				BigDecimal premiumPrice = rs.getBigDecimal("premium_price");
				productPriceMap.put("premiumPrice", premiumPrice);                        
			}
		} catch (Exception e) {
			throw new PartnerException(e);
		} 
        
        return productPriceMap;
	}
	
	public StateMasterVO getStateByName(String state) {
		logger.debug("getStateByName(" + state + ")");
		StateMasterVO stateVO = new StateMasterVO();

	    try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_STATE_NAME", state);        
			request.setInputParams(inputParams);
			request.setStatementID("GET_STATE_BY_NAME");

			CachedResultSet rs = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
			while (rs.next()) {
				stateVO = new StateMasterVO();
				stateVO.setStateMasterId(rs.getString("state_master_id"));
				stateVO.setStateName(rs.getString("state_name"));
				stateVO.setCountryCode(rs.getString("country_code"));
			}
		} catch (Exception e) {
			throw new PartnerException(e);
		} 
		return stateVO;
	}

	@SuppressWarnings("unchecked")
	public String getPartnerBuyerSequence() {
		String sequence = null;
		logger.debug("getPartnerBuyerSequence()");
		
		try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);		
			Map<String,Object> inputParams = new HashMap<String,Object>();
			request.setInputParams(inputParams);
			request.setStatementID("GET_AMAZON_BUYER_SEQUENCE");
			DataAccessUtil dau = DataAccessUtil.getInstance();
			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			sequence = (String) result.get("OUT_BUYER_SEQUENCE");
			String status = (String) result.get("OUT_STATUS");
			logger.info(" ****BuyerSquence StoreProc Status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
				String message = (String) result.get("OUT_MESSAGE");
				throw new Exception(message);
			}
		}  catch (Exception e) {
			throw new PartnerException(e);
		}					
		return sequence;
	}
	
	public boolean isSourceCodeExist(String sourceCode){
		if((sourceCode == null) || sourceCode.isEmpty()){
			return false;
		}
		else{
			try{
				CachedResultSet rs = getSourceCodeDtl(sourceCode);
	            rs.reset();
                while(rs.next())
                {
                    String sourceCodeEntry = (String)rs.getObject(1);
                    if(sourceCodeEntry!=null && !sourceCodeEntry.isEmpty()){
                    	return true;
                    }
                }
			}catch(Exception e){
				throw new PartnerException(e);
			}
			return false;
		}		
	}
	
	public String getSourceCodeSurcharge(String sourceCode) {
		logger.debug("getSourceCodeSurcharge(): " + sourceCode);
		try {
			CachedResultSet rs = getSourceCodeDtl(sourceCode);
			rs.reset();
			while (rs.next()) {
				String applySurcharge = (String) rs.getObject("APPLY_SURCHARGE_CODE");
				if("ON".equalsIgnoreCase(applySurcharge)) {
					logger.debug("Surcharge is ON: " + rs.getObject("SURCHARGE_AMOUNT"));
					return (rs.getBigDecimal("SURCHARGE_AMOUNT")).toString();
				}
			}
		} catch (Exception e) {
			throw new PartnerException(e);
		}
		
		return null;
	}

	public CachedResultSet getSourceCodeDtl(String sourceCode) throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.addInputParam("SOURCE_CODE", sourceCode);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_SOURCE_CODE_DETAILS");
        DataAccessUtil dau = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        return rs;
	}
	
	public HashMap<String, ArrayList<AddOnVO>> getActiveAddonList(String productId, Integer occassionId,
			String sourceCode, boolean returnAllAddOnFlag,
			String isFloristDelivery, String isDropshipDelivery) throws Exception {
		AddOnUtility aau = new AddOnUtility();
		logger.info("productId: " +productId+" occassionId: "+occassionId+" sourceCode: " +sourceCode+ " returnAllAddOnFlag: "+returnAllAddOnFlag+
				"isFloristDelivery: " +isFloristDelivery+ " isDropshipDelivery: "+isDropshipDelivery);
		return aau.getActiveAddonListByProductIdAndOccasionAndSourceCodeAndDeliveryType(
				productId, occassionId, sourceCode, returnAllAddOnFlag,
				isFloristDelivery, isDropshipDelivery, this.conn);		
	}
	
	public AddOnVO getAddOnById(String productId) throws Exception {
		AddOnUtility aau = new AddOnUtility();
		return aau.getAddOnById(productId, null, null, this.conn);
	}
	

}
