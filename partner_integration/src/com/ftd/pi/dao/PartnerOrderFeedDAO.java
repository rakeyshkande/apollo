/**
 * 
 */
package com.ftd.pi.dao;

import java.sql.Clob;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.vo.PartnerOrderFeed;

/**
 * @author skatam
 *
 */
public class PartnerOrderFeedDAO 
{
	private Logger logger = new Logger(PartnerOrderFeedDAO.class.getName());
	
	private Connection conn;
	public PartnerOrderFeedDAO(Connection conn) {
		this.conn = conn;
	}
	
	@SuppressWarnings("unchecked")
	public String savePartnerOrderFeed(PartnerOrderFeed partnerOrderFeed) 
	{
		logger.debug("********** Save Partner Order Feed **************");

		String orderFeed = partnerOrderFeed.getAsXmlString();

		//commented this line as part of PCI code review
		//logger.debug("OrderFeed XML :\n"+orderFeed);
		
		Map<String,Object> inputParams = new HashMap<String,Object>();
		
		inputParams.put("IN_FEED_CONTENT", orderFeed);

		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setInputParams(inputParams);
		request.setStatementID("INSERT_PARTNER_ORDER_FEED");
		
		String orderReportId = null;
		
		Map<String,Object> result;
		try 
		{
			DataAccessUtil dau = DataAccessUtil.getInstance();
			result = (Map<String,Object>) dau.execute(request);
			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N")) 
			{
				String message = (String) result.get("OUT_MESSAGE");
				throw new PartnerException(message);
			}
			orderReportId = (String) result.get("OUT_ORDER_FEED_ID");
			logger.debug("OrderFeed Id :"+orderReportId);
		} catch (Exception e) {
			throw new PartnerException(e);
		}
		return orderReportId;
	}
	
	public String getPartnerOrderFeed(String partnerOrderFeedId) 
	{
		
		Map<String,Object> inputParams = new HashMap<String,Object>();
		inputParams.put("IN_ORDER_FEED_ID", partnerOrderFeedId);

		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setInputParams(inputParams);
		request.setStatementID("GET_PARTNER_ORDER_FEED");
		String orderFeedXML = null;
	      try 
	      {
				CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(request);
				if(results != null && results.next()) 
				{
					Clob xmlCLOB = results.getClob("FEED_CONTENT");
					orderFeedXML = xmlCLOB.getSubString(1L, (int) xmlCLOB.length());
				}
	      } catch (Exception e) {
	    	  throw new PartnerException(e);
	      }
		return orderFeedXML;
	}

	@SuppressWarnings("unchecked")
	public void updateOrderFeedStatus(String orderFeedId, String orderStatus) {

		logger.debug("updateOrderFeedStatus()");
		
	    try {
	    	Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_PARTNER_ORDER_FEED_ID", orderFeedId);
			inputParams.put("IN_ORDER_STATUS", orderStatus);

			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("UPDATE_ORDER_FEED_STATUS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new PartnerException(message);
			}
		}  catch (Exception e) {
			throw new PartnerException(e);
		}	
	}

	public List<PartnerOrderFeed> getPartnerOrderFeeds(String orderStatus) {
		logger.debug("getPartnerOrderFeeds()");
		
		List<PartnerOrderFeed> orderFeedVOs = new ArrayList<PartnerOrderFeed>();
		
	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      Map<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_ORDER_STATUS", orderStatus);
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_ORDER_FEEDS_BY_STATUS");

	      try 
	      {
	    	  CachedResultSet crs = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
				
				while(crs.next())
				{
					PartnerOrderFeed orderFeed = new PartnerOrderFeed();
					orderFeed.setOrderFeedId(crs.getString("PARTNER_ORDER_FEED_ID"));
					/*
					Clob xmlCLOB = crs.getClob("FEED_CONTENT");
					String feedXml = xmlCLOB.getSubString(1L, (int) xmlCLOB.length());
					Order order = (Order) PartnerUtils.marshallAsObject(feedXml, Order.class);
					orderFeed.setOrder(order);
					*/
					orderFeedVOs.add(orderFeed);
					
				}
				
	      } catch (Exception e) {
	    	  throw new PartnerException(e);
	      }	      
		return orderFeedVOs;
	}
}
