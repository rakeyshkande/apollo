/**
 * 
 */
package com.ftd.pi.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.utils.PIConstants;

/**
 * @author smeka
 *
 */
public class PartnerFeedDAO {

	// Procedure Statements
	protected static final String GET_ORD_ADJS_BY_STATUS_STMT = "GET_ORDER_ADJS_BY_STATUS";
	protected static final String UPDATE_PTN_FEED_ID_STMT = "UPDATE_PTN_FEED_ID";
	protected static final String INSERT_FEED_TO_FEEDMASTER_STMT = "INSERT_FEED_TO_FEEDMASTER";
	protected static final String PROCESS_PTN_REFUNDS_STMT = "PROCESS_PTN_REFUNDS";
	protected static final String GET_ORD_FULFILL_BY_STATUS_STMT = "GET_ORDER_FULFILL_BY_STATUS";
	protected static final String INSERT_FLORIST_FULFILLMENT_DATA_STMT = "INSERT_FLORIST_FULFILLMENT_DATA";
	protected static final String GET_FEEDS_NOT_SENT_BY_TYPE_STMT = "GET_FEEDS_NOT_SENT_BY_TYPE";
	protected static final String UPDATE_PTN_FEED_STATUS_STMT = "UPDATE_PTN_FEED_STATUS";
	protected static final String GET_PARTNER_MAPPING = "GET_PARTNER_MAPPING";
	
	
	// result set fields
	protected static final String CONFIRMATION_NUMBER = "CONFIRMATION_NUMBER";
	protected static final String MASTER_ORDER_NUMBER = "MASTER_ORDER_NUMBER";
	protected static final String PARTNER_ORDER_NUMBER = "PARTNER_ORDER_NUMBER";
	protected static final String PARTNER_ORDER_ITEM_NUMBER = "PARTNER_ORDER_ITEM_NUMBER";
	protected static final String PARTNER_ID = "PARTNER_ID";
	protected static final String PARTNER_NAME = "PARTNER_NAME";
	protected static final String PARTNER_ORDER_ADJUSTMENT_ID = "PARTNER_ORDER_ADJUSTMENT_ID";
	protected static final String ADJUSTMENT_TYPE = "ADJUSTMENT_TYPE";
	protected static final String ADJUSTMENT_REASON = "ADJUSTMENT_REASON";
	protected static final String PRINCIPAL_AMT = "PRINCIPAL_AMT";
	protected static final String ADDON_AMT = "ADDON_AMT";
	protected static final String TAX_AMT = "TAX_AMT";
	protected static final String SHIPPING_AMT = "SHIPPING_AMT";
	protected static final String PTN_ORDER_FULFILLMENT_ID = "PARTNER_ORDER_FULFILLMENT_ID";
	protected static final String SHIPPING_METHOD = "SHIPPING_METHOD";
	protected static final String CARRIER_NAME = "CARRIER_NAME";
	protected static final String TRACKING_NUMBER = "TRACKING_NUMBER";
	protected static final String ITEM_PROD_ID = "PRODUCT_ID";
	protected static final String DELIVERY_DATE = "DELIVERY_DATE";
	protected static final String DELIVERY_STATUS = "DELIVERY_STATUS";
	protected static final String DELIVERY_STATUS_DATETIME = "DELIVERY_STATUS_DATETIME";
	
	// input values
	protected static final String SYSTEM_USER = "SYS";
	
	// input parameters
	protected static final String IN_FEED_DATA_ID = "IN_FEED_DATA_ID";
	protected static final String IN_FEED_STATUS = "IN_FEED_STATUS";
	protected static final String IN_PARTNER_ID = "IN_PARTNER_ID";
	protected static final String IN_FEED_ID = "IN_FEED_ID";
	protected static final String IN_FEED_TYPE = "IN_FEED_TYPE";
	protected static final String IN_UPDATED_BY = "IN_UPDATED_BY";
	protected static final String IN_FEED_XML = "IN_FEED_XML";
	protected static final String IN_USER_NAME = "IN_USER_NAME";

	// output parameters 
	protected static final Object OUT_STATUS = "OUT_STATUS";
	protected static final Object OUT_MESSAGE = "OUT_MESSAGE";
	protected static final Object OUT_FEED_ID = "OUT_FEED_ID";
	
	protected Connection connection;
	private Logger logger = new Logger(PartnerFeedDAO.class.getName());
	
	
	/**
	 * @param connection
	 */
	public PartnerFeedDAO(Connection connection) {
		this.connection = connection;
	}
	
	/** Default Constructor.*/
	public PartnerFeedDAO() {	}	

	/** Update the feed Id mapping for partner order adjustments/ partner order fulfillments etc feed data in partner schema.
	 * @param partnerFeedDataIds
	 * @param feedId
	 * @param feedType
	 * @param feedStatus
	 * @return
	 */
	public boolean updatePartnerFeedId(List<String> partnerFeedDataIds, String feedId, String feedType, String feedStatus) {
		
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering updatePartnerFeedId() *********");
		}

		try {
			for (String partnerFeedDataId : partnerFeedDataIds) {
				HashMap<String,Object> inputParams = new HashMap<String,Object>();

				inputParams.put(IN_FEED_ID, feedId);
				inputParams.put(IN_FEED_TYPE, feedType);
				inputParams.put(IN_UPDATED_BY, SYSTEM_USER);
				inputParams.put(IN_FEED_STATUS, feedStatus);

				// The input parameter IN_FEED_DATA_ID can be the id of any type of feed				
				if(logger.isDebugEnabled()) {
					logger.debug("Updating feed Id for partner order feed data id: " + partnerFeedDataId + " of feed Type : " + feedType);
				}
				inputParams.put(IN_FEED_DATA_ID, partnerFeedDataId);

				DataRequest dataRequest = new DataRequest();
				dataRequest.setConnection(this.connection);
				dataRequest.setStatementID(UPDATE_PTN_FEED_ID_STMT);
				dataRequest.setInputParams(inputParams);

				DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

				@SuppressWarnings("unchecked")
				Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

				String status = (String) outputs.get(OUT_STATUS);
				if (status != null && status.equalsIgnoreCase("N")) {
					logger.error("Failed to update feed id for a set of partner orders adjusted: " + outputs.get(OUT_MESSAGE));
					throw new SQLException((String) outputs.get(OUT_MESSAGE));
				}				
			}
			return true;
			
		} catch(Exception e) {
			logger.error("Error caught updating feed id for a set of partner orders adjusted: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}
	
	/** Insert the partner feeds to Feed master table of partner schema (PTN_PI). 
	 * @param feedContent
	 * @param feedType
	 * @return
	 */
	public String insertPartnerFeed(String feedContent, String feedType) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering insertPartnerFeed() *********");
		}

		try {
			HashMap<String,Object> inputParams = new HashMap<String,Object>();		    

			inputParams.put(IN_FEED_XML, feedContent);
			inputParams.put(IN_FEED_TYPE, feedType);
			inputParams.put(IN_FEED_STATUS, PIConstants.NEW_FEED_STATUS);
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID(INSERT_FEED_TO_FEEDMASTER_STMT);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			@SuppressWarnings("unchecked")
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

			String status = (String) outputs.get(OUT_STATUS);
			if (status != null && status.equalsIgnoreCase("N")) {
				logger.error("Failed to save feed to feed master: " + outputs.get(OUT_MESSAGE));
				throw new SQLException((String) outputs.get(OUT_MESSAGE));
			}

			return (String) outputs.get(OUT_FEED_ID);

		} catch(Exception e) {
			logger.error("Error caught saving parter feed to feed master: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}
	
	/**Get the partner feeds that are not sent to ESB. feed_status != 'SENT'.
	 * Feeds can be New/ failure reason will be updated to feed_status column 
	 * when not sent successfully to ESB.
	 * @param feedType
	 * @return
	 */
	public CachedResultSet getPartnerFeedsNotSent(String feedType) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getPartnerFeeds() *********");
		}	
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID(GET_FEEDS_NOT_SENT_BY_TYPE_STMT);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_FEED_TYPE, feedType);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			return (CachedResultSet) dataAccessUtil.execute(dataRequest);
			
		} catch(Exception e) {
			logger.error("Error caught getting partner feeds not yet sent to ESB: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}
	
	public boolean updateFeedStatus(String feedId, String feedStatus) {	
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering updateFeedStatus(), " + feedId + ", " + feedStatus +" *********");
		}
		
		try {
			HashMap<String, Object> inputParams = new HashMap<String, Object>();

			inputParams.put(IN_FEED_ID, feedId);
			inputParams.put(IN_FEED_STATUS, feedStatus);
			inputParams.put(IN_UPDATED_BY, SYSTEM_USER);

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);

			dataRequest.setStatementID(UPDATE_PTN_FEED_STATUS_STMT);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			@SuppressWarnings("unchecked")
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

			String status = (String) outputs.get(OUT_STATUS);
			if (status != null && status.equalsIgnoreCase("N")) {
				logger.error("Failed to update feed status: " + outputs.get(OUT_MESSAGE));
				throw new SQLException((String) outputs.get(OUT_MESSAGE));
			}

			return true;
			
		} catch(Exception e) {
			logger.error("Error caught updating feed status: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}
	
	/** Method to send a system message if there is any error processing feed request/scheduled jobs.
	 * @param message
	 * @param connection
	 * @throws Exception
	 */
	public void sendSystemMessage(String message) throws Exception {
		String messageId;
		try {
			logger.error("Sending System Message: " + message);
			
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(PIConstants.PTN_SYS_MSG_SOURCE);
			sysMessage.setType(PIConstants.PTN_SYS_MSG_TYPE_ERR);
			sysMessage.setMessage(message);
			sysMessage.setSubject(PIConstants.PTN_SYS_MSG_SUBJ);

			SystemMessenger sysMessenger = SystemMessenger.getInstance();
			messageId = sysMessenger.send(sysMessage, this.connection, false);

			if (messageId == null) {				
				logger.error("Error occured while attempting to send a system message. System message is not sent ");
			}
		} catch(Exception e) {
			logger.error("Error caught sending system sytem message for partner integration Feed Error," + message + ", " + e);
		}
	}
}
