
package com.ftd.pi.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.orderstatus.vo.Address;
import com.ftd.pi.orderstatus.vo.ContactData;
import com.ftd.pi.orderstatus.vo.CustomFields;
import com.ftd.pi.orderstatus.vo.CustomFields.Data;
import com.ftd.pi.orderstatus.vo.DeliveryInfo;
import com.ftd.pi.orderstatus.vo.Order;
import com.ftd.pi.orderstatus.vo.OrderItem;
import com.ftd.pi.orderstatus.vo.OrderStatusInfo;
import com.ftd.pi.orderstatus.vo.PhoneNumber;
import com.ftd.pi.orderstatus.vo.ProdIdentification;
import com.ftd.pi.orderstatus.vo.ShipmentInfo;
import com.ftd.pi.partner.vo.AddOn;
import com.ftd.pi.partner.vo.AddOns;
import com.ftd.pi.partner.vo.GiftWrapData;
import com.ftd.pi.partner.vo.ItemPrice;
import com.ftd.pi.partner.vo.Name;
import com.ftd.pi.partner.vo.OrderTotals;
import com.ftd.pi.partner.vo.RejectReason;
import com.ftd.pi.partner.vo.VenusOrderStatus;
import com.ftd.pi.utils.PartnerUtils;


/**
 * @author bsurimen
 *
 */
public class PartnerOrderStatusUpdateDAO {
	
private Logger logger = new Logger(PartnerOrderStatusUpdateDAO.class.getName());
	
	private Connection conn;
	public PartnerOrderStatusUpdateDAO(Connection conn) 
	{
		this.conn = conn;
	}
	
	/**
	 * @param orderStatus
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String saveVenusOrderStatusUpdate(VenusOrderStatus orderStatus) 
	{
		logger.debug("********** Save Partner Order Status Update Feed **************");

		Map<String,Object> inputParams = new HashMap<String,Object>();
		String status = "N";
		try 
		{
			
		inputParams.put("IN_VENUS_ORDER_NUMBER", orderStatus.getFTDVendorOrderID());
		inputParams.put("IN_PARTNER_ORDER_NUMBER", new BigDecimal(orderStatus.getVendorOrderNumber()));
		inputParams.put("IN_PARTNER_ID", orderStatus.getPartnerId());
		inputParams.put("IN_TRACKING_NUMBER", orderStatus.getTrackingNumber());
		inputParams.put("IN_CARRIER", orderStatus.getCarrier());
		inputParams.put("IN_ORDER_STATUS", orderStatus.getStatus().value());
		
		
		inputParams.put("IN_PRINTED", PartnerUtils.getSQLDate(orderStatus.getPrintedOnDate()));
		inputParams.put("IN_SHIPPED", PartnerUtils.getSQLDate(orderStatus.getShipDate()));
		inputParams.put("IN_DELIVERED", PartnerUtils.getSQLDate(orderStatus.getDeliveredOnDate()));
		inputParams.put("IN_REJECTED", PartnerUtils.getSQLDate(orderStatus.getRejectedOnDate()));
		
		RejectReason rejectReason = orderStatus.getRejectReason();
		
		if(rejectReason!=null)
		{	
			if(rejectReason.getCode()!=null && !rejectReason.getCode().equals(""))
			inputParams.put("IN_REJECT_CODE", new BigDecimal(rejectReason.getCode()));
			
			inputParams.put("IN_REJECT_MESSAGE", rejectReason.getMessage());
		}
		
		
		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setInputParams(inputParams);
		request.setStatementID("INSERT_PARTNER_ORDER_STATUS");
		
		Map<String,Object> result;
		
			DataAccessUtil dau = DataAccessUtil.getInstance();
			result = (Map<String,Object>) dau.execute(request);
			status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N")) 
			{
				String message = (String) result.get("OUT_MESSAGE");
				logger.info("OUT_MESSAGE  :"+message);
				throw new PartnerException(message);
			}
			
		} catch (Exception e) {
			throw new PartnerException(e);
		}
		return status;		
	}
	

	/**  Check if we order status is allowed for the given order number
	 * @param orderNumber
	 * @param isScrubOrder
	 * @return
	 */
	public boolean isPtnOrderStatusAllowed(String orderNumber, String isScrubOrder) {
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.addInputParam("IN_ORDER_NUMBER", orderNumber);
			dataRequest.addInputParam("IN_ORD_REMOVED", isScrubOrder);
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("IS_PTN_ORD_STATUS_ALLOWED"); 
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    	String isOrdStatusAllowed = (String) dataAccessUtil.execute(dataRequest);
	    	
	    	if("Y".equals(isOrdStatusAllowed)) {
	    		return true;
	    	}	    	
		} catch (Exception e) {
			throw new PartnerException("Unable to check if its allowed to send status update for the given order: ", e);
		}
		return false;
	}

	/** Get partner orders adjusted for Status update - 
	 * 		Gets the partner Orders with Refunds that are Billed for a given date and 
	 * 		partner has opted for price updates/ order status updates.
	 *   
	 * @param billedDate
	 * @return
	 */
	public Set<String> getAdjOrdForStatusUpdate(Date billedDate) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getAdjOrdForStatusUpdate() *********, Date: " + billedDate);
		}		
		try {
			Set<String> orders = new HashSet<String>();

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID("GET_PTN_ADJ_FOR_STATUS_UPDATE");
			

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			if(billedDate == null) {
				billedDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			}
			inputParams.put("IN_BATCH_DATE", billedDate); 
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);			
			
			while (crs.next()) { 
				// For ARIBA orders - Do not process the orders that have only Fee/ Tax updates today. Process only when there is adjustment to Unit price
				if("ARI".equalsIgnoreCase(crs.getString("origin_id")) 
						&& ("Y".equals(crs.getString("is_only_tax_fee_refund")))) {
					logger.info("Refund exists only for fee/ tax: Do not process. RefundId: " + crs.getString("refund_id"));
					continue;
				} 				
				orders.add(crs.getString("external_order_number"));
			}			
			return orders;
			
		} catch(Exception e) {
			logger.error("Error caught getting the orders with refunds billed for a given date");
			logger.error(e);
			throw new PartnerException(e.getMessage());
		}
	}

	/** Save the order status update for an item, status type and its association with feed_id. 
	 *  This is helpful for constructing custom fields.
	 * @param feedId
	 * @param originId
	 * @param orderItemNumber
	 * @param orderStatus
	 * @return
	 */
	public String insertOrdStatusUpdateRef(String feedId, String originId, String orderItemNumber, String orderStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering insertOrdStatusUpdate() *********");
		}
		try {
			HashMap<String,Object> inputParams = new HashMap<String,Object>();	
			
			inputParams.put("IN_ORDER_NUM", orderItemNumber);  
			inputParams.put("IN_ORD_STATUS_TYPE", orderStatus);
			inputParams.put("IN_FEED_ID", feedId);
			
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID("INSERT_ORD_STATUS_UPD_REF");
			
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			@SuppressWarnings("unchecked")
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);

			String status = (String) outputs.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {
				logger.error("Failed to save feed to feed master: " + outputs.get("OUT_MESSAGE"));
				throw new SQLException((String) outputs.get("OUT_MESSAGE"));
			}

			return (String) outputs.get("OUT_ORD_STATUS_UPDATE_ID");

		} catch(Exception e) {
			logger.error("Error caught saving order status update reference: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}		
	}

	/** is order status update sent for a given status
	 * @param status
	 * @param orderItemNumber
	 * @return
	 */
	public boolean isOrdStatusExists(String status, String orderItemNumber) {
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.addInputParam("IN_ORDER_NUMBER", orderItemNumber);
			dataRequest.addInputParam("IN_TYPE", status);
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("IS_ORD_STATUS_UPDATE_EXISTS"); 
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    	String isOrdStatusExists = (String) dataAccessUtil.execute(dataRequest);
	    	
	    	if("Y".equals(isOrdStatusExists)) {
	    		return true;
	    	}	    	
		} catch (Exception e) {
			throw new PartnerException("Unable to check if order status update exists for a given status type: ", e);
		}
		return false;
	}
	
	/** Get the complete partner order information for a given order number.
	 *  Note the order number can be either the external_order_number or the master_order_number
	 * @param orderItemNumber 
	 * @return
	 * @throws Exception 
	 */
	public Order getPartnerOrderinfo(String orderItemNumber) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getPartnerOrderinfo() *********, orderItemNumber: " + orderItemNumber);
		}
		Order orderInfo = null;
		Map<String, OrderItem> orderItems = null;
		try {		
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID("GET_PTN_ORDER_INFO");

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put("IN_ORDER_NUMBER", orderItemNumber); 
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			@SuppressWarnings("unchecked")
			Map<String, CachedResultSet> orderInfoMap =  (Map<String, CachedResultSet>) dataAccessUtil.execute(dataRequest);
			
			// Process Order Header
			CachedResultSet resultset = (CachedResultSet) orderInfoMap.get("OUT_PTN_ORD_CUR");
			int count = 1;			 
			while(resultset.next()) {				
				if(count++ == 1){
					orderInfo = new Order();
					orderItems = new HashMap<String, OrderItem>();
					
					orderInfo.setPartnerId(resultset.getString("PARTNER_ID"));
					orderInfo.setPartnerOrderID(resultset.getString("PARTNER_ORDER_NUMBER"));
					orderInfo.setFTDOrderID(resultset.getString("MASTER_ORDER_NUMBER"));
					orderInfo.setOriginId(resultset.getString("order_origin"));  
					
					String orderdate = resultset.getString("ORDER_DATE"); //Format Thu Jun 27 02:28:19 CDT 2016										
					if(orderdate != null) {
						DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
						orderInfo.setOrderDate(PartnerUtils.getTimestampString(dateFormat.parse(orderdate)));
					}				
					orderInfo.setCurrencyCode("USD"); // Set USD by default  
					orderInfo.setOrderGuid(resultset.getString("order_guid")); 
					orderInfo.setSourceCode(resultset.getString("source_code"));
					orderInfo.setAllItemsProcessed(true);
				}
				
				OrderItem orderItem = new OrderItem();
				orderItem.setPartnerOrderItemID(resultset.getString("PARTNER_ORDER_ITEM_NUMBER"));
				orderItem.setFTDOrderItemID(resultset.getString("EXTERNAL_ORDER_NUMBER"));	
				orderItem.setOrderDetailId(resultset.getString("ORDER_DETAIL_ID"));	
				orderItem.setCleanOrder("Y".equals(resultset.getString("is_clean_order")));
				if(!orderItem.isCleanOrder()) {
					orderInfo.setAllItemsProcessed(false);
				}				
				orderItems.put(orderItem.getOrderDetailId(), orderItem);
			}
			
			if(orderInfo == null || orderItems ==  null || orderItems.size() == 0) {
				throw new PartnerException("Unable to get the order info for the order. Cannot process order status update request");
			}
			
			// Populate Buyer info 
			orderInfo.setBillingData(getBuyerContactInfo((CachedResultSet) orderInfoMap.get("OUT_BUYER_CUR")));
			
			// populateOrderItemInfo
			populateOrderItemInfo((CachedResultSet) orderInfoMap.get("OUT_ORD_DTL_CUR"), orderItems);
			
			// Order Add-on info 
			populateAddonInfo((CachedResultSet) orderInfoMap.get("OUT_ORD_ADDON_CUR"), orderItems);
			
			// Order Total - Original total/ Billing, also includes additional billing
			resultset = (CachedResultSet) orderInfoMap.get("OUT_ORD_TOTAL_CUR");
			populateItemPrice(resultset, "ORDER_TOTAL", orderItems);
			
			// Refund Total - also includes removed orders
			populateItemPrice((CachedResultSet) orderInfoMap.get("OUT_ORD_REFUND_CUR"), "REFUND_TOTAL", orderItems);
			
			// Calculate the Item total and cart total
			calculateItemTotal(orderItems); 
			
			// calculate cart total
			calculateCartTotal(orderInfo, orderItems);
			
			// populate Shipment Info 
			populateShipmentInfo((CachedResultSet) orderInfoMap
					.get("OUT_ORD_SHIP_INFO_CUR"), (CachedResultSet) orderInfoMap.get("OUT_FLR_ORD_STAT_INFO_CUR"), orderItems);
			
			// Populate Order Item info 	
			List<OrderItem> list = new ArrayList<OrderItem>(orderItems.values());
			orderInfo.getOrderItem().addAll(list);
					
		} catch(Exception e) {
			logger.error("Error caught geting the complete partner order info for OrderStatusUpdates for orderNumber: " + orderItemNumber);
			logger.error(e);
			throw new PartnerException(e.getMessage());
		}
		return orderInfo;
	}
	
	/** Get Buyer information from scrub.buyer/ clean.customer as per the status of the order.
	 * @param resultset 
	 * @return
	 */
	private ContactData getBuyerContactInfo(CachedResultSet resultset) {				 
		ContactData contact = new ContactData();
		try {
			int count = 1;
			
			while(resultset.next()) {	
				if(count++ == 1){					
					Address address = new Address();
					address.setAddress1(resultset.getString("ADDRESS_1"));
					address.setAddress2(resultset.getString("ADDRESS_2")); 
					address.setCity(resultset.getString("CITY"));
					address.setState(resultset.getString("STATE"));
					address.setCountry(resultset.getString("COUNTRY"));
					address.setPostalCode(resultset.getString("ZIP_CODE"));
					contact.setAddress(address);
					
					Name name = new Name();
					name.setFirstName(resultset.getString("FIRST_NAME"));
					name.setLastName(resultset.getString("LAST_NAME"));
					name.setMiddleName(null);
					name.setBusinessName(resultset.getString("BUSINESS_NAME"));
					contact.setName(name);	
					
					contact.setEmailAddress(resultset.getString("buyer_email_address"));
				}				
				if(!StringUtils.isEmpty(resultset.getString("PHONE_NUMBER"))) {
					PhoneNumber phoneNumber = new PhoneNumber();
					phoneNumber.setExtension(resultset.getString("EXTENSION"));
					phoneNumber.setType(resultset.getString("PHONE_TYPE"));
					phoneNumber.setNumber(resultset.getString("PHONE_NUMBER"));
					contact.getPhoneNumber().add(phoneNumber);
				}				
			}		 
			
		} catch (Exception e) {
			throw new PartnerException("Unable to get the order buyer info for the order: ", e);
		}
		return contact;
	}
	
	/** Get Delivery Information for an item
	 * @param deliveryInfo 
	 * @param orderCur
	 * @return
	 */	
	private DeliveryInfo getDeliveryInfo(DeliveryInfo deliveryInfo, CachedResultSet orderCur) {	
		
		if(deliveryInfo.getFulfillmentData() == null) {
			ContactData contact = new ContactData();
			Address address = new Address();
			address.setAddress1(orderCur.getString("ADDRESS_1"));
			address.setAddress2(orderCur.getString("ADDRESS_2")); 
			address.setCity(orderCur.getString("CITY"));
			address.setState(orderCur.getString("STATE"));
			address.setCountry(orderCur.getString("COUNTRY"));
			address.setPostalCode(orderCur.getString("ZIP_CODE"));
			contact.setAddress(address);
			
			Name name = new Name();
			name.setFirstName(orderCur.getString("FIRST_NAME"));
			name.setLastName(orderCur.getString("LAST_NAME"));
			name.setMiddleName(null);
			name.setBusinessName(orderCur.getString("BUSINESS_NAME"));
			contact.setName(name);				 
			contact.setEmailAddress(null);				
			deliveryInfo.setFulfillmentData(contact);
			
			String deliveryDate = orderCur.getString("DELIVERY_DATE");
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
			if(deliveryDate != null) {				
				try {
					deliveryInfo.setDeliveryStartDate(PartnerUtils.getTimestampString(dateFormat.parse(deliveryDate)));
				} catch (ParseException e) {
					deliveryInfo.setDeliveryStartDate(deliveryDate);
				}
			}
		
			String deliveryEndDate = orderCur.getString("DELIVERY_DATE_RANGE_END");
			if(deliveryEndDate != null) {				
				try {
					deliveryInfo.setDeliveryEndDate(PartnerUtils.getTimestampString(dateFormat.parse(deliveryEndDate)));
				} catch (ParseException e) {
					deliveryInfo.setDeliveryEndDate(deliveryEndDate);
				}
			}
			
			String shipDate = orderCur.getString("ship_date");
			if(shipDate != null) { 
				try {
					deliveryInfo.setEstShipDate(PartnerUtils.getTimestampString(dateFormat.parse(shipDate)));
				} catch (ParseException e) {
					deliveryInfo.setEstShipDate(shipDate);
				}
			}  else {				
				try {
					deliveryInfo.setEstShipDate(PartnerUtils.getTimestampString(dateFormat.parse(deliveryDate)));
				} catch (ParseException e) {
					deliveryInfo.setEstShipDate(deliveryDate);
				}
			}			
			//deliveryInfo.setDeliveryEndDate(deliveryInfo.getDeliveryEndDate());			
			deliveryInfo.setDeliveryLocationType(orderCur.getString("ADDRESS_TYPE"));
			deliveryInfo.setMorningDelivery(orderCur.getString("ORDER_HAS_MORNING_DELIVERY"));
			deliveryInfo.setComments(orderCur.getString("status")); 
			
			
			
		}
		
		if(!StringUtils.isEmpty(orderCur.getString("PHONE_NUMBER"))) {
			PhoneNumber phoneNumber = new PhoneNumber();
			phoneNumber.setExtension(orderCur.getString("EXTENSION"));
			phoneNumber.setType(orderCur.getString("PHONE_TYPE"));
			phoneNumber.setNumber(orderCur.getString("PHONE_NUMBER"));
			deliveryInfo.getFulfillmentData().getPhoneNumber().add(phoneNumber);
		}
		
		return deliveryInfo;
	}
	
	/** Get product information for an item
	 * @param resultset
	 * @return
	 */
	private ProdIdentification getProdInfo(CachedResultSet resultset) {
		ProdIdentification prodInfo = new ProdIdentification();
		
		prodInfo.setSKU(resultset.getString("PRODUCT_ID"));
		prodInfo.setTitle(StringEscapeUtils.escapeHtml(resultset.getString("PRODUCT_NAME")));		
		
		CustomFields additionalInfo = new CustomFields();
		additionalInfo.getData().add(new Data("WEB_PROD_ID", resultset.getString("NOVATOR_ID")));
		
		if(resultset.getString("NOVATOR_NAME") != null) {
			additionalInfo.getData().add(new Data("WEB_PROD_NAME", StringEscapeUtils.escapeHtml(resultset.getString("NOVATOR_NAME"))));
		}
		
		additionalInfo.getData().add(new Data("PRODUCT_TYPE", resultset.getString("PRODUCT_TYPE")));		
		additionalInfo.getData().add(new Data("PRODUCT_SUB_TYPE", resultset.getString("PRODUCT_SUB_TYPE")));
		
		if(resultset.getString("SHORT_DESCRIPTION") != null) {
			additionalInfo.getData().add(new Data("SHORT_DESCRIPTION", resultset.getString("SHORT_DESCRIPTION")));
		}
		if(resultset.getString("COLOR1_DESCRIPTION") != null) {
			additionalInfo.getData().add(new Data("ITEM_COLOR1_DESCRIPTION", resultset.getString("COLOR1_DESCRIPTION")));
		}
		if(resultset.getString("COLOR2_DESCRIPTION") != null) {
			additionalInfo.getData().add(new Data("ITEM_COLOR2_DESCRIPTION", resultset.getString("color2_description")));
		}
		prodInfo.setAdditionalInfo(additionalInfo);		
		return prodInfo;		
	}
	
	/** Populate Order Add-on Info
	 * @param resultset
	 * @param orderItems
	 */
	private void populateAddonInfo(CachedResultSet resultset, Map<String, OrderItem> orderItems) {
		while (resultset.next()) {
			OrderItem item = orderItems.get(resultset.getString("ORDER_DETAIL_ID"));
			
			if (item != null && item.getProdIdentification() != null) {
				AddOn addon = new AddOn();				
				addon.setId(resultset.getString("ADD_ON_CODE"));				
				BigDecimal addonAmt = resultset.getString("PRICE") == null ? new BigDecimal(0.00) : new BigDecimal(resultset.getString("PRICE"));
				BigDecimal addonDiscAmt = resultset.getString("add_on_discount_amount") == null ? new BigDecimal(0.00) : new BigDecimal(resultset.getString("add_on_discount_amount"));
				
				addon.setAddOnRetailPrice(addonAmt.add(addonDiscAmt));
				addon.setAddOnSalePrice(addonAmt);
				
				addon.setQuantity(resultset.getString("ADD_ON_QUANTITY") == null ? 0 : Integer.parseInt(resultset.getString("ADD_ON_QUANTITY")));				
				AddOns addons = item.getProdIdentification().getAddOns();
				if(addons == null) {					
					addons = new AddOns();
					item.getProdIdentification().setAddOns(addons);
				}				
				item.getProdIdentification().getAddOns().getAddOn().add(addon); 
				
			} else {
				logger.error("Invalid Order Item, dont process for this item, " + resultset.getString("ORDER_DETAIL_ID"));
			}
		}		
	}
	
	/** Populate Item price for Current billing and adjustments/refunds
	 * @param orderItemPriceCur
	 * @param orderItems
	 * @param priceType
	 * @return
	 */	
	private void populateItemPrice(CachedResultSet orderItemPriceCur, String priceType, Map<String, OrderItem> orderItems) {
		while(orderItemPriceCur.next()) {
			OrderItem item = orderItems.get(orderItemPriceCur.getString("order_detail_id"));
			if(item != null) {
				ItemPrice itemPrice = new ItemPrice();	
				itemPrice.setCommission(null); 
				
				BigDecimal addonAmt = orderItemPriceCur.getString("ADD_ON_AMOUNT") == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString("ADD_ON_AMOUNT"));
				BigDecimal addonDiscAmt = orderItemPriceCur.getString("add_on_discount_amount") == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString("add_on_discount_amount"));
				itemPrice.setItemAddOnRetailPrice(addonAmt);
				itemPrice.setItemAddOnSalePrice(addonAmt.subtract(addonDiscAmt));
				
				BigDecimal prodAmt = orderItemPriceCur.getString("PRODUCTS_AMOUNT") == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString("PRODUCTS_AMOUNT"));
				BigDecimal prodDiscAmt = orderItemPriceCur.getString("DISCOUNT_AMOUNT") == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString("DISCOUNT_AMOUNT"));
				itemPrice.setItemRetailPrice(prodAmt);
				itemPrice.setItemSalePrice(prodAmt.subtract(prodDiscAmt));
				
				itemPrice.setItemShippingServiceFeeTotal(orderItemPriceCur.getString("serv_ship_fee") == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString("serv_ship_fee")));
				itemPrice.setItemTaxTotal(orderItemPriceCur.getString("tax") == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString("tax")));
				itemPrice.setItemTotal(orderItemPriceCur.getString(priceType) == null ? new BigDecimal(0.00) : new BigDecimal(orderItemPriceCur.getString(priceType)));
				
				if("ORDER_TOTAL".equals(priceType)) {				
					item.setItemOrigPrice(itemPrice);
				}else if("REFUND_TOTAL".equals(priceType)) {
					item.setItemRefund(itemPrice);
				}
			}
		} 	
	}
	
	/** Order current total = Billing - refund
	 * @param orderItems
	 */
	private void calculateItemTotal(Map<String, OrderItem> orderItems) {
		for (OrderItem orderItem : orderItems.values()) {			
			if(orderItem != null && orderItem.getItemOrigPrice() != null) {
				ItemPrice itemPrice = new ItemPrice();				
				if(orderItem.getItemRefund() != null) {
					itemPrice.setItemAddOnRetailPrice(orderItem.getItemOrigPrice().getItemAddOnRetailPrice().subtract(orderItem.getItemRefund().getItemAddOnRetailPrice()));
					itemPrice.setItemAddOnSalePrice(orderItem.getItemOrigPrice().getItemAddOnSalePrice().subtract(orderItem.getItemRefund().getItemAddOnSalePrice()));
					itemPrice.setItemRetailPrice(orderItem.getItemOrigPrice().getItemRetailPrice().subtract(orderItem.getItemRefund().getItemRetailPrice()));
					itemPrice.setItemSalePrice(orderItem.getItemOrigPrice().getItemSalePrice().subtract(orderItem.getItemRefund().getItemSalePrice()));
					itemPrice.setItemShippingServiceFeeTotal(orderItem.getItemOrigPrice().getItemShippingServiceFeeTotal().subtract(orderItem.getItemRefund().getItemShippingServiceFeeTotal()));
					itemPrice.setItemTaxTotal(orderItem.getItemOrigPrice().getItemTaxTotal().subtract(orderItem.getItemRefund().getItemTaxTotal()));
					itemPrice.setItemTotal(orderItem.getItemOrigPrice().getItemTotal().subtract(orderItem.getItemRefund().getItemTotal()));					
				} else {
					itemPrice = orderItem.getItemOrigPrice();
				}
				orderItem.setItemPrice(itemPrice);
			}
		}
	}
	
	/** Populate all order Item info
	 * @param resultset
	 * @param orderItems
	 */
	private void populateOrderItemInfo(CachedResultSet resultset, Map<String, OrderItem> orderItems) {
		while (resultset.next()) {
			OrderItem item = orderItems.get(resultset.getString("ORDER_DETAIL_ID"));
			
			if (item != null) { 
				GiftWrapData giftWrapData = new GiftWrapData();
				giftWrapData.setGiftWrapMessage(resultset.getString("card_message"));
				item.setGiftWrapData(giftWrapData);
				item.setOccasion(resultset.getString("occasion_description"));					
				item.setSpecialInstructions(resultset.getString("special_instructions"));
				
				DeliveryInfo deliveryInfo = item.getDeliveryInfo() == null ? new DeliveryInfo() : item.getDeliveryInfo();
				item.setDeliveryInfo(getDeliveryInfo(deliveryInfo, resultset));
				item.setProdIdentification(getProdInfo(resultset));
				
			} else {
				logger.error("Invalid Order Item, dont process for this item, " + resultset.getString("ORDER_DETAIL_ID"));
			}
		}		
	}
	
	/** Populate item shipment info
	 * @param shipInfoCursor
	 * @param floralConfInfoCursor
	 * @param orderItems
	 */
	private void populateShipmentInfo(CachedResultSet shipInfoCursor, CachedResultSet floralConfInfoCursor, 
			Map<String, OrderItem> orderItems) {
		
		while(shipInfoCursor.next()) {
			
			if(StringUtils.isEmpty(shipInfoCursor.getString("venus_id"))) {
				continue;
			}
			
			OrderItem item = orderItems.get(shipInfoCursor.getString("ORDER_DETAIL_ID"));			
			if(item != null) {
				ShipmentInfo shipInfo = new ShipmentInfo();						 
				shipInfo.setCartonNumber(shipInfoCursor.getString("venus_order_number"));
				shipInfo.setComments(shipInfoCursor.getString("comments"));
				if(shipInfoCursor.getDate("ship_date") != null) {
					shipInfo.setShipStartDate(PartnerUtils.getTimestampString(shipInfoCursor.getDate("ship_date")));
					shipInfo.setShipEndDate(PartnerUtils.getTimestampString(shipInfoCursor.getDate("ship_date")));
				} else if(item.getDeliveryInfo() != null) {
					shipInfo.setShipStartDate(item.getDeliveryInfo().getEstShipDate());
					shipInfo.setShipEndDate(item.getDeliveryInfo().getEstShipDate());
				}
				shipInfo.setShipperNumber(shipInfoCursor.getString("shipper_number"));
				
				shipInfo.setShipSystem(shipInfoCursor.getString("shipping_system"));
				shipInfo.setTrackingNumber(shipInfoCursor.getString("tracking_number"));
				shipInfo.setShipCarrier(shipInfoCursor.getString("FINAL_CARRIER"));
				
				CustomFields additionalInfo = new CustomFields();
				additionalInfo.getData().add(new Data("SHIP_STATUS", shipInfoCursor.getString("ORDER_STATUS")));
				additionalInfo.getData().add(new Data("VENDOR_PRICE", shipInfoCursor.getString("vendor_price")));
				additionalInfo.getData().add(new Data("CARRIER_DESCRIPTION", shipInfoCursor.getString("carrier_name")));
				shipInfo.setAdditionalInfo(additionalInfo);
				
				OrderStatusInfo orderStatus = new OrderStatusInfo();
				orderStatus.setComments(shipInfo.getComments());
				orderStatus.setStatus(shipInfoCursor.getString("ORDER_STATUS"));
				
				item.setOrderStatusInfo(orderStatus);
				item.setShipmentInfo(shipInfo); 
			}
		}
			
		
		while(floralConfInfoCursor.next()) {
			
			if(StringUtils.isEmpty(floralConfInfoCursor.getString("mercury_id"))) {
				continue;
			}
			
			OrderItem item = orderItems.get(floralConfInfoCursor.getString("ORDER_DETAIL_ID"));
			
			if(item != null && item.getShipmentInfo() == null) {
				ShipmentInfo shipInfo = new ShipmentInfo();						 
				shipInfo.setCartonNumber(null);
				shipInfo.setComments(floralConfInfoCursor.getString("comments"));
				shipInfo.setShipEndDate(PartnerUtils.getTimestampString(floralConfInfoCursor.getDate("DELIVERY_DATE")));
				shipInfo.setShipperNumber(null);
				shipInfo.setShipStartDate(PartnerUtils.getTimestampString(floralConfInfoCursor.getDate("DELIVERY_DATE")));
				shipInfo.setShipSystem("FLORIST");
				shipInfo.setTrackingNumber("");//Empty String
				shipInfo.setShipCarrier("Other");
				
				CustomFields additionalInfo = new CustomFields();
				additionalInfo.getData().add(new Data("STATUS", floralConfInfoCursor.getString("ORDER_STATUS")));
				additionalInfo.getData().add(new Data("florist_price", floralConfInfoCursor.getString("florist_price")));
				additionalInfo.getData().add(new Data("CARRIER_DESCRIPTION", "Other"));
				shipInfo.setAdditionalInfo(additionalInfo);
				
				OrderStatusInfo orderStatus = new OrderStatusInfo();
				orderStatus.setComments(floralConfInfoCursor.getString("ORDER_STATUS"));  
				
				item.setShipmentInfo(shipInfo); 
			}
		}		
	}
	 
	
	/** Calculate Cart totals
	 * @param orderInfo
	 * @param orderItems
	 */
	private void calculateCartTotal(Order orderInfo, Map<String, OrderItem> orderItems) { 
		
		OrderTotals orderTotals = new OrderTotals();
		orderInfo.setOrderTotals(orderTotals);
		
		orderTotals.setOrderAddOnRetailPriceTotal(new BigDecimal(0.00));
		orderTotals.setOrderAddOnSalePriceTotal(new BigDecimal(0.00));
		orderTotals.setOrderRetailPriceTotal(new BigDecimal(0.00));
		orderTotals.setOrderSalePriceTotal(new BigDecimal(0.00));
		orderTotals.setOrderShippingServiceFeeTotal(new BigDecimal(0.00));
		orderTotals.setOrderTaxTotal(new BigDecimal(0.00));
		orderTotals.setOrderTotal(new BigDecimal(0.00));
		
		for (OrderItem orderItem : orderItems.values()) {			
			if(orderItem != null && orderItem.getItemPrice() != null) {
				orderTotals.setOrderAddOnRetailPriceTotal(orderTotals.getOrderAddOnRetailPriceTotal().add(orderItem.getItemPrice().getItemAddOnRetailPrice()));
				orderTotals.setOrderAddOnSalePriceTotal(orderTotals.getOrderAddOnSalePriceTotal().add(orderItem.getItemPrice().getItemAddOnSalePrice()));
				orderTotals.setOrderRetailPriceTotal(orderTotals.getOrderRetailPriceTotal().add(orderItem.getItemPrice().getItemRetailPrice()));
				orderTotals.setOrderSalePriceTotal(orderTotals.getOrderSalePriceTotal().add(orderItem.getItemPrice().getItemSalePrice()));
				orderTotals.setOrderShippingServiceFeeTotal(orderTotals.getOrderShippingServiceFeeTotal().add(orderItem.getItemPrice().getItemShippingServiceFeeTotal()));
				orderTotals.setOrderTaxTotal(orderTotals.getOrderTaxTotal().add(orderItem.getItemPrice().getItemTaxTotal()));
				orderTotals.setOrderTotal(orderTotals.getOrderTotal().add(orderItem.getItemPrice().getItemTotal()));			
			}
		}
	}
	
	/**
	 * @param orderNumber
	 * @param isScrubOrder
	 * @return
	 */
	public boolean isPtnOrderShipStatusAllowed(String orderNumber) {
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.addInputParam("IN_ORDER_NUMBER", orderNumber);
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("IS_PTN_ORD_SHIP_STATUS_ALLOWED"); 
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    	String isOrdStatusAllowed = (String) dataAccessUtil.execute(dataRequest);
	    	
	    	if("Y".equals(isOrdStatusAllowed)) {
	    		return true;
	    	}	    	
		} catch (Exception e) {
			throw new PartnerException("Unable to check if its allowed to send status update for the given order: ", e);
		}
		return false;
	}

	/** Get partner orders Ship for Status update - 
	 * 		Gets the partner Orders with status as 'SHIPPED' 
	 * 		partner has opted for Ship Status updates
	 *   
	 * @return Set of Orders
	 */
	public Set<String> getShipOrdForStatusUpdate() {
				
		try {
			Set<String> orders = new HashSet<String>();

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.conn);
			dataRequest.setStatementID("GET_SHIP_ORDERS_FOR_STATUS_UPDATE");
			
		
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);			
			
			while (crs.next()) { 
				
				orders.add(crs.getString("external_order_number"));
			}			
			return orders;
			
		} catch(Exception e) {
			logger.error("Error caught getting the orders with Shipped Status");
			logger.error(e);
			throw new PartnerException(e.getMessage());
		}
	}	
}
