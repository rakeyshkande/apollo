/**
 * 
 */
package com.ftd.pi.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.vo.PartnerMappingVO;
import com.ftd.pi.vo.PartnerOrderAdjFeedVO;
import com.ftd.pi.vo.PartnerOrderItemAdjFeedVO;

/**
 * @author smeka
 *
 */
public class PartnerOrderAdjDAO extends PartnerFeedDAO {
	
	private Logger logger = new Logger(PartnerOrderAdjDAO.class.getName());
	
	public PartnerOrderAdjDAO(Connection connection) {
		this.connection = connection;
	}

	/** Get the detail of the adjustments made to the partner orders. If partner Id is null, it will fetch all the 
	 * adjustments in partner adjustment table irrespective of partner id.
	 */
	public Map<String, PartnerOrderAdjFeedVO> getPartnerFeedData(String partnerId, String feedStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering getPartnerFeedData() *********");
		}	
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID(GET_ORD_ADJS_BY_STATUS_STMT);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_PARTNER_ID, partnerId);
			inputParams.put(IN_FEED_STATUS, feedStatus);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			
			Map<String,PartnerOrderAdjFeedVO> partnerOrderItemAdjs = new HashMap<String, PartnerOrderAdjFeedVO>();
			
			while (crs.next()) {
				
				PartnerOrderAdjFeedVO partnerOrderAdj;
												
				if(partnerOrderItemAdjs.get(crs.getString(MASTER_ORDER_NUMBER)) == null) {
					partnerOrderAdj = new PartnerOrderAdjFeedVO();				
					partnerOrderAdj.setFeedType(PIConstants.ADJUSTMENT_TYPE);
					
					partnerOrderAdj.setFtdOrderNumber(crs.getString(MASTER_ORDER_NUMBER));
			    	partnerOrderAdj.setPartnerOrderNumber(crs.getString(PARTNER_ORDER_NUMBER));	 		    	
			    	
			    	partnerOrderAdj.setPartnerId(crs.getString(PARTNER_ID));
			    	partnerOrderAdj.setPartnerName(crs.getString(PARTNER_NAME));
			    	partnerOrderItemAdjs.put(crs.getString(MASTER_ORDER_NUMBER), partnerOrderAdj);
			    	
				} else {
					partnerOrderAdj = partnerOrderItemAdjs.get(crs.getString(MASTER_ORDER_NUMBER));	
				}
				
				PartnerOrderItemAdjFeedVO partnerOrderItemAdj = new PartnerOrderItemAdjFeedVO();
				partnerOrderItemAdj.setPartnerOrderItemNumber(crs.getString(PARTNER_ORDER_ITEM_NUMBER));
				partnerOrderItemAdj.setFtdExternalOrderNumber(crs.getString(CONFIRMATION_NUMBER));	    	
				partnerOrderItemAdj.setPartnerOrderItemAdjId(crs.getString(PARTNER_ORDER_ADJUSTMENT_ID));	
				partnerOrderItemAdj.setAdjustmentType(crs.getString(ADJUSTMENT_TYPE));
				partnerOrderItemAdj.setAdjustmentReason(crs.getString(ADJUSTMENT_REASON));
				if(crs.getString(PRINCIPAL_AMT) != null) {
					partnerOrderItemAdj.setItemPrincipalAmount(new BigDecimal(crs.getString(PRINCIPAL_AMT)));
				}
				if(crs.getString(ADDON_AMT) != null) {
					partnerOrderItemAdj.setItemAddOnAmount(new BigDecimal(crs.getString(ADDON_AMT)));
				}
				if(crs.getString(TAX_AMT) != null) {
					partnerOrderItemAdj.setItemTaxAmount(new BigDecimal(crs.getString(TAX_AMT)));
				}
				if(crs.getString(SHIPPING_AMT) != null) {
					partnerOrderItemAdj.setShippingPrincipalAmount(new BigDecimal(crs.getString(SHIPPING_AMT)));
				}
				partnerOrderAdj.getItemAdjFeeds().add(partnerOrderItemAdj);
			}
					
			return partnerOrderItemAdjs;
			
		} catch(Exception e) {
			logger.error("Error caught getting adjusted partner order detail: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
	}

	/** PROCESS_PTN_REFUNDS procedure identifies if there are any refunds applied 
	 * for partner orders and create/insert adjustment records.
	 * @param partnerId	
	 */
	public void processPartnerOrderRefunds(String partnerId) {
		if(logger.isDebugEnabled()) {
			logger.debug("********* Entering processRefunds() *********");
		}
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			
			HashMap<String,Object> inputParams = new HashMap<String,Object>();
		    inputParams.put(IN_USER_NAME, SYSTEM_USER);
		    inputParams.put(IN_PARTNER_ID, partnerId);
			dataRequest.setStatementID(PROCESS_PTN_REFUNDS_STMT);
			dataRequest.setInputParams(inputParams);
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			@SuppressWarnings("rawtypes")
			Map outputs = (Map) dataAccessUtil.execute(dataRequest);
			  
			String status = (String) outputs.get("OUT_STATUS");
			if(status != null && status.equalsIgnoreCase("N")) {
				logger.error("Failed to process refunds for partner orders: " + outputs.get("OUT_MESSAGE"));
			    throw new SQLException((String) outputs.get("OUT_MESSAGE"));
			}
		} catch(Exception e) {
			logger.error("Error caught processing refunds for partner orders: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
		if(logger.isDebugEnabled()) {
			logger.debug("********* End processRefunds() *********");
		}
	}

	
	/** Get Partner Mapping for the given partnerId
	 *  @param partnerId	
	 */
	public PartnerMappingVO getPartnerMapping(String partnerId) {
		
		PartnerMappingVO partnerMapVo = new PartnerMappingVO();
		
		try{
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID(GET_PARTNER_MAPPING);

			HashMap<String, Object> inputParams = new HashMap<String, Object>();
			inputParams.put(IN_PARTNER_ID, partnerId);
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map outputs = (Map) dataAccessUtil.execute(dataRequest);
			
			CachedResultSet crs = (CachedResultSet) outputs.get("OUT_CUR");
			
			while (crs.next()) {
				partnerMapVo =  populatePartnerMappingVO(crs);
			}
		
		}catch(Exception e) {
			logger.error("Error caught getting partner mapping: " + e.getMessage());
			throw new PartnerException(e.getMessage());
		}
		
		return partnerMapVo;
	}
	
	/** Populate Partner Mapping data for the given partnerId
	 *  @param CachedResultSet	
	 */
	public PartnerMappingVO populatePartnerMappingVO(CachedResultSet crs) {	
		PartnerMappingVO channelMappingVO = new PartnerMappingVO();
		channelMappingVO.setPartnerId(crs.getString("PARTNER_ID"));
		channelMappingVO.setPartnerName(crs.getString("PARTNER_NAME"));
		channelMappingVO.setPartnerImage(crs.getString("PARTNER_IMAGE"));
		
		channelMappingVO.setFtdOriginMapping(crs.getString("FTD_ORIGIN_MAPPING"));
		channelMappingVO.setMasterOrderNoPrefix(crs.getString("MASTER_ORDER_NO_PREFIX"));
		channelMappingVO.setConfNumberPrefix(crs.getString("CONF_NUMBER_PREFIX"));
		
		channelMappingVO.setDefaultSourceCode(crs.getString("DEFAULT_SOURCE_CODE"));
		channelMappingVO.setDefaultRecalcSourceCode(crs.getString("DEFAULT_RECALC_SOURCE_CODE"));
		
		channelMappingVO.setSatDeliveryAllowedDropship(crs.getString("SAT_DELIVERY_ALLOWED_DROPSHIP"));
		channelMappingVO.setSatDeliveryAllowedFloral(crs.getString("SAT_DELIVERY_ALLOWED_FLORAL"));
		channelMappingVO.setSunDeliveryAllowedFloral(crs.getString("SUN_DELIVERY_ALLOWED_FLORAL"));

		//channelMappingVO.setSendFloristPdbPrice(crs.getString("SEND_FLORIST_PDB_PRICE"));
		channelMappingVO.setSendOrdConfEmail(crs.getString("SEND_ORD_CONF_EMAIL"));
		channelMappingVO.setSendShipConfEmail(crs.getString("SEND_SHIP_CONF_EMAIL"));
		//channelMappingVO.setCreateStockEmail(crs.getString("CREATE_STOCK_EMAIL"));
		channelMappingVO.setSendDelConfEmail(crs.getString("SEND_DELIVERY_CONF_EMAIL"));
		
		channelMappingVO.setPerformDropshipAVS("Y".equals(crs.getString("AVS_DROPSHIP")) ? true : false);
		channelMappingVO.setPerformFloristAVS("Y".equals(crs.getString("AVS_FLORAL")) ? true : false);
		
		channelMappingVO.setSendDconFeed("Y".equals(crs.getString("SEND_DCON_FEED")) ? true : false);

		channelMappingVO.setAddonSubscription("Y".equals(crs.getString("INCLUDE_ADDON")) ? true : false);
		
				
		return channelMappingVO;
	}
}
