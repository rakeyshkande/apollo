package com.ftd.pi.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;

public class OrderInvoiceDAO 
{
	private Logger logger = new Logger(OrderInvoiceDAO.class.getName());
	
	private Connection conn;
	public OrderInvoiceDAO(Connection conn) {
		this.conn = conn;
	}

    /** Get all Ariba orders that are ready to be invoiced.
     *  Returns cursor of master_order_numbers.
     */
    public Set<String> getAribaOrdersToInvoice() {
        if(logger.isDebugEnabled()) {
            logger.debug("********* Entering getAribaOrdersToInvoice() *********");
        }   
        HashSet<String> orders = new HashSet<String>();
        
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("GET_ARIBA_ORDERS_TO_INVOICE");

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            while (rs.next()) {
                orders.add(rs.getString("master_order_number"));
            }
            
        } catch(Exception e) {
            logger.error("Error caught getting Ariba orders to invoice: " + e.getMessage());
            throw new PartnerException(e.getMessage());
        }
        return orders;
    }
	

    /** Update the invoice status for the given cart to reflect it has been sent to Apollo PI
     */
    public void updateInvoiceStatusToSent(String masterOrderNumber) {
        if(logger.isDebugEnabled()) {
            logger.debug("Updating invoice status for " + masterOrderNumber);
        }   
        
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.reset();
            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("UPDATE_INVOICE_STATUS_TO_SENT");
            
            HashMap<String,Object> inputParams = new HashMap<String,Object>();
            inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            dataAccessUtil.execute(dataRequest);
            
        } catch(Exception e) {
            logger.error("Error caught updating invoice status for " + masterOrderNumber + ": " + e.getMessage());
            throw new PartnerException(e.getMessage());
        }
    }

    /** Initiate the sending of the invoice feeds to the ESB (by triggering the MDB)
     */
    public void startSendInvoiceFeedsToEsb() {
        if(logger.isDebugEnabled()) {
            logger.debug("Starting send of invoice feeds to the ESB");
        }   
        
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.reset();
            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("START_SEND_INVOICE_FEEDS");
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            dataAccessUtil.execute(dataRequest);
            
        } catch(Exception e) {
            logger.error("Error caught when trying to initiate sending of invoice feeds" + e.getMessage());
            throw new PartnerException(e.getMessage());
        }
        
    }
}
