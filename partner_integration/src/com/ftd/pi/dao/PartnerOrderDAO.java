/**
 * 
 */
package com.ftd.pi.dao;

import java.sql.Clob;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.partner.vo.Order;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerMappingVO;
import com.ftd.pi.vo.PartnerOrderVO;
import com.ftd.pi.vo.ftd.FTDOrder;

/**
 * @author skatam
 *
 */
public class PartnerOrderDAO 
{
	private Logger logger = new Logger(PartnerOrderDAO.class.getName());
	
	private Connection conn;
	public PartnerOrderDAO(Connection conn) {
		this.conn = conn;
	}
	

	@SuppressWarnings("unchecked")
	public String insertPartnerOrder(String orderFeedId, Order order) 
	{
		logger.debug("insertPartnerOrder()");
		PartnerMappingVO mappingVO = new PartnerMappingDAO().getPIPartnerMapping(order.getPartnerId().toUpperCase());
		String monPrefix = mappingVO.getMasterOrderNoPrefix();
		logger.debug("MasterOrderNumber Prefix :"+monPrefix);
		String partnerOrderID = order.getPartnerOrderID();
		String partnerOrderNumber = partnerOrderID;
		String masterOrderNumber = monPrefix + StringUtils.replaceChars(partnerOrderID,"-", "");
		
		String partnerOrderString =PartnerUtils.marshallAsXML(order, null); 
		String orderStatus = PIConstants.ORDER_STATUS_RECEIVED;

	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    Map<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_PARTNER_ORDER_NUMBER", partnerOrderNumber);
	    inputParams.put("IN_PARTNER_ID", order.getPartnerId());
	    //inputParams.put("IN_PARTNER_ORDER_ID", order.getPartnerOrderID());	    
	    inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
	    inputParams.put("IN_PARTNER_ORDER_FEED_ID", orderFeedId);
	    inputParams.put("IN_PARTNER_XML", partnerOrderString);
	    inputParams.put("IN_FTD_XML", null);
	    Date date = order.getOrderDate().toGregorianCalendar().getTime();
	    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
	    inputParams.put("IN_ORDER_DATE", sqlDate);
	    inputParams.put("IN_ORDER_STATUS", orderStatus);
	    inputParams.put("IN_CURRENCY_CODE", "USD");//TODO
	    
	    request.setInputParams(inputParams);
	    request.setStatementID("INSERT_PARTNER_ORDER");


	    Map<String,Object> result = null;
	    try 
		{
	    	DataAccessUtil dau = DataAccessUtil.getInstance();
			result = (Map<String,Object>) dau.execute(request);
			
		    String status = (String) result.get("OUT_STATUS");
		    logger.info("status: " + status);
		    if (status == null || status.equalsIgnoreCase("N"))
		    {
		      String message = (String) result.get("OUT_MESSAGE");
		      throw new Exception(message);
		    }
		} 
	    catch (Exception e) 
	    {
	    	throw new PartnerException(e);
		}
	    return partnerOrderNumber;
	  }
	

	@SuppressWarnings("unchecked")
	public boolean isDuplicateOrder(Order order) {
		boolean duplicate = true;
		try {
			Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_PARTNER_ID", order.getPartnerId());
			inputParams.put("IN_PARTNER_ORDER_NUMBER", order.getPartnerOrderID());

			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("CHECK_ORDER_EXISTS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new PartnerException(message);
			}else{
				String exists = (String) result.get("OUT_EXISTS");
				duplicate = "Y".equalsIgnoreCase(exists);
			}
		}  catch (Exception e) {
			throw new PartnerException(e);
		}
		return duplicate;
	}	
	
	public Order getPartnerOrder(String partnerId, String partnerOrderNumber) {
		logger.debug("Get Partner Order :"+partnerOrderNumber);
		
	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      Map<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_PARTNER_ID", partnerId);   
	      inputParams.put("IN_PARTNER_ORDER_NUMBER", partnerOrderNumber);        
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_PARTNER_ORDER_XML");

	      String partnerXML = null;
	      try 
	      {
				CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(request);
				if(results != null && results.next()) 
				{
					Clob xmlCLOB = results.getClob("PARTNER_XML");
					partnerXML = xmlCLOB.getSubString(1L, (int) xmlCLOB.length());
				}
	      } catch (Exception e) {
	    	  throw new PartnerException(e);
	      }
	      if(partnerXML != null){
	    	  Order order = (Order) PartnerUtils.marshallAsObject(partnerXML, Order.class);
	    	  return order;
	      }
		return null;
	}
	

	@SuppressWarnings("unchecked")
	public void saveFTDOrderXML(String partnerOrderNumber, FTDOrder ftdOrder) {
		logger.debug("Updating Partner Order :"+partnerOrderNumber+" to store FTDOrder XML");
		
		String ftd_xml = PartnerUtils.marshallAsXML(ftdOrder, null);
		Map<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_PARTNER_ORDER_NUMBER", partnerOrderNumber);
	    inputParams.put("IN_FTD_XML", ftd_xml);
	    inputParams.put("IN_ORDER_STATUS", "TRANSFORMED");
	
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("SAVE_FTD_ORDER_XML");
	
	    try {
			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new Exception(message);
			}
		}catch (Exception e) {
			throw new PartnerException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateOrderStatus(String partnerId, String partnerOrderNumber, String orderStatus)
	{
		logger.debug("updateOrderStatus()");
		
	    try {
	    	Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_PARTNER_ID", partnerId);
			inputParams.put("IN_PARTNER_ORDER_NUMBER", partnerOrderNumber);
			inputParams.put("IN_ORDER_STATUS", orderStatus);

			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("UPDATE_ORDER_STATUS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new PartnerException(message);
			}
		}  catch (Exception e) {
			throw new PartnerException(e);
		}
		
	}
	
	public List<PartnerOrderVO> getFTDOrderXmlsByStatus(String orderStatus) {
		logger.debug("getFTDOrderXmlsByStatus()");
		
		List<PartnerOrderVO> orderVOs = new ArrayList<PartnerOrderVO>();
		
	      DataRequest request = new DataRequest();
	      request.reset();
	      request.setConnection(conn);
	      Map<String,Object> inputParams = new HashMap<String,Object>();
	      inputParams.put("IN_ORDER_STATUS", orderStatus);
	      request.setInputParams(inputParams);
	      request.setStatementID("GET_FTD_ORDER_XMLS_BY_STATUS");

	      try 
	      {
	    	  CachedResultSet crs = (CachedResultSet)DataAccessUtil.getInstance().execute(request);
				
				while(crs.next())
				{
					PartnerOrderVO orderVO = new PartnerOrderVO();
					orderVO.setPartnerId(crs.getString("PARTNER_ID"));
					orderVO.setPartnerOrderNumber(crs.getString("PARTNER_ORDER_NUMBER"));
					Clob xmlCLOB = crs.getClob("FTD_XML");
					String ftdXml = xmlCLOB.getSubString(1L, (int) xmlCLOB.length());
					orderVO.setFtdXml(ftdXml);
					
					orderVOs.add(orderVO);
				}
				
	      } catch (Exception e) {
	    	  throw new PartnerException(e);
	      }	      
		return orderVOs;
	}
}
