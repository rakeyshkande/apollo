/**
 * 
 */
package com.ftd.pi.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.vo.PartnerOrderDetailVO;

/**
 * @author skatam
 *
 */
public class PartnerOrderItemDAO 
{
	private Logger logger = new Logger(PartnerOrderItemDAO.class.getName());
	
	private Connection conn;
	public PartnerOrderItemDAO(Connection conn) {
		this.conn = conn;
	}

	@SuppressWarnings("unchecked")
	public void insertPartnerOrderDetails(String partnerOrderNumber, PartnerOrderDetailVO orderDetailVO) 
	{
		Map<String,Object> inputParams = new HashMap<String,Object>();
	    inputParams.put("IN_PARTNER_ORDER_ITEM_NUMBER", orderDetailVO.getPartnerItemNumber());
	    inputParams.put("IN_PARTNER_ID", orderDetailVO.getPartnerId());
	    inputParams.put("IN_PARTNER_ORDER_NUMBER", orderDetailVO.getPartnerOrderNumber());
	    inputParams.put("IN_CONFIRMATION_NUMBER", orderDetailVO.getConfirmationNumber());
	    inputParams.put("IN_PRODUCT_ID", orderDetailVO.getProductId());
	    inputParams.put("IN_RETAIL_PRICE", orderDetailVO.getRetailPrice());
	    inputParams.put("IN_SALE_PRICE", orderDetailVO.getSalePrice());
	    inputParams.put("IN_SHIPPING_AMT", orderDetailVO.getShippingAmount());
	    inputParams.put("IN_TAX_AMT", orderDetailVO.getTaxAmount());
	    inputParams.put("IN_PLACEMENT", orderDetailVO.getPlacement());
	    inputParams.put("IN_DISCOUNT_CODE", orderDetailVO.getDiscountCode());
	    inputParams.put("IN_ADD_ON_RETAIL_PRICE", orderDetailVO.getAddOnRetailPrice());
	    inputParams.put("IN_ADD_ON_SALE_PRICE", orderDetailVO.getAddOnSalePrice());
	    inputParams.put("IN_LEGACY_ID", orderDetailVO.getLegacyID());
	    
	    logger.info("The legacy Id is : "+orderDetailVO.getLegacyID());
	    
	    DataRequest request = new DataRequest();
	    request.reset();
	    request.setConnection(conn);
	    request.setInputParams(inputParams);
	    request.setStatementID("INSERT_PARTNER_ORDER_DETAILS");
	
	    try {
			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			logger.info("savePartnerOrderDetailsInfo status: " + status);
			if (status == null || status.equalsIgnoreCase("N"))
			{
				StringBuilder message = new StringBuilder("Unable to insert PartnerOrderDetails"); 
				message.append((String) result.get("OUT_MESSAGE"));
				throw new Exception(message.toString());
			}
		} catch (Exception e) {
			throw new PartnerException(e);
		}
	}
	

	@SuppressWarnings("unchecked")
	public boolean isDuplicateOrderItem(String partnerId, String partnerOrderItemNumber) {
		boolean duplicate = true;
		try {
			Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_PARTNER_ID", partnerId);   
			inputParams.put("IN_PARTNER_ORDER_ITEM_NUMBER", partnerOrderItemNumber);
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("CHECK_ORDER_ITEM_EXISTS");

			DataAccessUtil dau = DataAccessUtil.getInstance();

			Map<String,Object> result = (Map<String,Object>) dau.execute(request);

			String status = (String) result.get("OUT_STATUS");
			
			if (status == null || status.equalsIgnoreCase("N"))
			{
			  String message = (String) result.get("OUT_MESSAGE");
			  throw new PartnerException(message);
			}else{
				String exists = (String) result.get("OUT_EXISTS");
				duplicate = "Y".equalsIgnoreCase(exists);
			}
		}  catch (Exception e) {
			throw new PartnerException(e);
		}
		logger.info("isDuplicateOrderItem : " + duplicate);
		return duplicate;
	}


	public String getConfirmationNumber(String confNumberPrefix) 
	{
		logger.debug("getConfirmationNumber()");
		String confNumber = "";
		try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setStatementID("GET_ORDER_CONFIRMATION_NUMBER");

			CachedResultSet results = 
				(CachedResultSet) DataAccessUtil.getInstance().execute(request);
			
			while (results != null && results.next()) {
				confNumber = confNumberPrefix+results.getString("confirmation_number");
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new PartnerException(e);
		}
		logger.info("confNumber: " + confNumber);
		return confNumber;
	}
	
	public String getSKUIdFromNovatorId(String novatorId){
		String skuId = null;
		try {
			Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_NOVATOR_ID", novatorId); 
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			request.setInputParams(inputParams);
			request.setStatementID("GET_SKU_ID_FROM_NOVATOR_ID");

			CachedResultSet results = 
				(CachedResultSet) DataAccessUtil.getInstance().execute(request);
			
			while (results != null && results.next()) {
				skuId = results.getString("PRODUCT_ID");
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new PartnerException(e);
		}
		if(skuId == null){
			skuId = novatorId;
			logger.info("There is no entry with the passed In novatorId:" + novatorId + ". Returning back same value as SKU ID");
		}
		else{
			logger.info("SKU Id is: " + skuId + " for the passedIn novatorId:" + novatorId);
		}
		return skuId;
	}

	public Long getPartnerOrderDetailId(String partnerOrderItemNumber) {
		Long partnerOrderDetailId = null;
		
		try {
			Map<String,Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_PARTNER_ORDER_ITEM_NUMBER", partnerOrderItemNumber);
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(this.conn);
			request.setInputParams(inputParams);
			request.setStatementID("GET_PARTNER_ORDER_DETAIL_ID");

			CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(request);

			while ((results != null) && (results.next())) {
				partnerOrderDetailId = Long.valueOf(results.getLong("partner_order_detail_id"));
			}
		} catch (Exception e) {
			throw new PartnerException(e);
		}
		this.logger.info("getPartnerOrderDetailId : " + partnerOrderDetailId);
		return Long.valueOf(partnerOrderDetailId.longValue());
	}

}
