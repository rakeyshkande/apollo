/**
 * 
 */
package com.ftd.pi.exceptions;

/**
 * @author skatam
 *
 */
public class PartnerException extends RuntimeException 
{
	private static final long serialVersionUID = 1L;

	public PartnerException() {
		super();
	}

	public PartnerException(String message, Throwable cause) {
		super(message, cause);
	}

	public PartnerException(String message) {
		super(message);
	}

	public PartnerException(Throwable cause) {
		super(cause);
	}
	
}
