/**
 * 
 */
package com.ftd.pi.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.naming.InitialContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.pi.dao.PartnerFeedDAO;
import com.ftd.pi.dao.PartnerMappingDAO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.vo.Address;
import com.ftd.pi.vo.PartnerMappingVO;

/**
 * @author skatam
 * 
 */
public class PartnerUtils 
{
	public static final String PI_CONFIG_XML = "pi_config.xml";
	public static final String JMS_RETRY_DELAY_IN_SEC  = "15";
	private static Logger logger = new Logger("com.ftd.pi.utils.PartnerUtils");

	private PartnerUtils() {}

	public static boolean sendJMSMessage(String status, String corrId,
			String message) {
		
		return sendJMSMessageWithDelay(status,corrId,message,false);

		/*boolean success = true;

		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus(status);
			messageToken.setJMSCorrelationID(corrId);
			messageToken.setMessage(message);
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} catch (Exception e) {
			logger.error(e);
			success = false;
		}

		return success;*/

	}
	
	public static boolean sendJMSMessageWithDelay(String status, String corrId,
			String message,boolean withDelay) {

		boolean success = true;

		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus(status);
			messageToken.setJMSCorrelationID(corrId);
			messageToken.setMessage(message);
			if(withDelay){
				messageToken.setProperty("JMS_OracleDelay", JMS_RETRY_DELAY_IN_SEC, "long");
			}
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} catch (Exception e) {
			logger.error(e);
			success = false;
		}

		return success;

	}

	/**
	 * Get a new database connection.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection() {
		//TODO
		if(true){
			//return getTestConnection();
		}
		Connection conn = null;

		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			String datasource = configUtil.getPropertyNew(PIConstants.PROPERTY_FILE, PIConstants.DATASOURCE_NAME);
			conn = DataSourceUtil.getInstance().getConnection(datasource);
		} catch (Exception e) {
			throw new PartnerException(e);
		}

		return conn;
	}

	public static String getFrpGlobalParm(String context, String name) {
		CacheUtil cacheUtil = CacheUtil.getInstance();
		try {
			return cacheUtil.getGlobalParm(context, name);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e);
		}
	}

	public static String getPartnerFrpGlobalParm(String name) {
		return getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT, name);
	}

	public static String getSecureGlobalParm(String context, String name) {
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			return configUtil.getSecureProperty(context, name);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e);
		}
	}

	public static String getPartnerSecureGlobalParm(String name) {
		return getSecureGlobalParm(PIConstants.PI_SECURE_CONTEXT, name);
	}

	public static void sendPageSystemMessage(String logMessage) {
		Connection conn = null;
		try {
			conn = getConnection();
			String appSource = PIConstants.SM_PAGE_SOURCE;
			String errorType = PIConstants.SM_TYPE;
			String subject = PIConstants.SM_PAGE_SUBJECT;
			int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

			SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
			systemMessengerVO.setLevel(pageLevel);
			systemMessengerVO.setSource(appSource);
			systemMessengerVO.setType(errorType);
			systemMessengerVO.setSubject(subject);
			systemMessengerVO.setMessage(logMessage);
			String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
			logger.debug(result);
		} catch (Exception ex) {
			// Do not attempt to send system message it requires obtaining a
			// connection
			// and may end up in an infinite loop.
			logger.error(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					logger.error("Unable to close connection: " + e);
				}
			}
		}

	}

	public static void sendNoPageSystemMessage(String logMessage) {
		Connection conn = null;
		try {
			conn = getConnection();
			String appSource = PIConstants.SM_NOPAGE_SOURCE;
			String errorType = PIConstants.SM_TYPE;
			String subject = PIConstants.SM_NOPAGE_SUBJECT;
			int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

			SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
			systemMessengerVO.setLevel(pageLevel);
			systemMessengerVO.setSource(appSource);
			systemMessengerVO.setType(errorType);
			systemMessengerVO.setSubject(subject);
			systemMessengerVO.setMessage(logMessage);
			String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
			logger.debug(result);
		} catch (Exception ex) {
			// Do not attempt to send system message it requires obtaining a
			// connection
			// and may end up in an infinite loop.
			logger.error(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					logger.error("Unable to close connection: " + e);
				}
			}
		}

	}

	/**
	 * Get the name of the server that the PI application is running on
	 * 
	 * @return String name of local host or blank if it can't be determined.
	 */
	public static String getLocalHostName() {
		String retVal = "";
		try {
			InetAddress addr = InetAddress.getLocalHost();
			retVal = addr.getHostName();
		} catch (UnknownHostException e) {
			retVal = "Unknown";
		}
		return retVal;
	}

	public static Calendar fromXMLGregorianCalendar(XMLGregorianCalendar xc) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(xc.toGregorianCalendar().getTimeInMillis());
		return c;
	}
	
	/**
	 * @param xmlCal
	 * @return java.sql.Timestamp
	 */
	public static java.sql.Timestamp getSQLDate(XMLGregorianCalendar xmlCal) {
		if (xmlCal == null)
			return null;
		else {
			java.util.Date dt = xmlCal.toGregorianCalendar().getTime();
			java.sql.Timestamp sqlDt = new java.sql.Timestamp(dt.getTime());
			return sqlDt;
		}
	}
	
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(date.getTime());
		XMLGregorianCalendar xc;
		try {
			xc = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR),
					gc.get(Calendar.MONTH) + 1,
					gc.get(Calendar.DAY_OF_MONTH),
					DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			logger.error(e);
			throw new RuntimeException(e.getMessage());
		}
		return xc;
	}

	/**
	 * Converts an ISO date string to a java.util.Date object The date formatter
	 * in jdk 1.4 interprets the timezone 'Z' formatter as -hh:mm. The ISO
	 * standared which Partner uses sends the timezone over as -hhmm.
	 * 
	 * @param azDateString
	 *            string to parse
	 * @return converted date object
	 * @throws java.text.ParseException
	 */
	public static java.sql.Date mrcDateString2Date(String azDateString)
			throws ParseException {
		String strDate = StringUtils.substringBeforeLast(azDateString, ":")
				+ StringUtils.substringAfterLast(azDateString, ":");

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = format.parse(strDate);
		return new java.sql.Date(date.getTime());
	}

	/**
	 * Converts a java.sql.Date to an ISO date string. The date formatter in jdk
	 * 1.4 interprets the timezone 'Z' formatter as -hhmm. The ISO standared
	 * which Partner uses sends the timezone over as -hh:mm.
	 * 
	 * @param date
	 *            date to parse
	 * @return converted string object
	 * @throws java.text.ParseException
	 */
	public static String sqlDate2MrcDateString(java.sql.Date date)
			throws ParseException {
		String retval = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (format != null && date != null) {
			java.util.Date testDate = new java.util.Date(date.getTime());
			if (testDate != null) {
				retval = format.format(testDate);
			}
		}
		return retval;
	}

	public static String marshallAsXML(Object object, Map<String, Object> props) {
		String xml = "";
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = jaxbContext.createMarshaller();
			if (props != null && props.size() > 0) {
				Set<String> keySet = props.keySet();
				for (String key : keySet) {
					marshaller.setProperty(key, props.get(key));
				}
			}
			StringWriter sw = new StringWriter();
			marshaller.marshal(object, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			throw new PartnerException(e);
		}
		return xml;

	}

	public static Object marshallAsObject(String xml, Class<?> clazz) {
		Object object = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader sw = new StringReader(xml);
			object = unmarshaller.unmarshal(sw);
		} catch (JAXBException e) {
			throw new PartnerException(e);
		}
		return object;
	}

	public static Object marshallAsObject(File xmlFile, Class<?> clazz) {
		Object object = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			object = unmarshaller.unmarshal(xmlFile);
		} catch (JAXBException e) {
			throw new PartnerException(e);
		}
		return object;
	}

	public static Object marshallAsObject(byte[] data, Class<?> clazz) {
		Object object = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			object = unmarshaller.unmarshal(bais);
			bais.close();
		} catch (JAXBException e) {
			throw new PartnerException(e);
		} catch (IOException e) {
			throw new PartnerException(e);
		}
		return object;
	}

	public static String getPIConfigProperty(String propertyName) {
		try {
			return ConfigurationUtil.getInstance().getProperty(PI_CONFIG_XML, propertyName);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public static String removeAllSpecialChars(String inStr)
    {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < inStr.length(); i++)
        {
            if(Character.isLetterOrDigit(inStr.charAt(i)))
            {
                sb.append(inStr.charAt(i));
            }
        }
        return sb.toString();
    }
	

	public static BigDecimal getSafeBigDecimal(BigDecimal val)
	{
		if(val == null){
			return new BigDecimal("0");
		}
		return val;
	}
	public static BigDecimal getAsBigDecimal(String val)
	{
		if(val != null && val.trim().length()> 0)
		{
			try {
				BigDecimal bd = new BigDecimal(val);
				return bd;
			} catch (Exception e) {
				return new BigDecimal("0");
			}
		}
		return new BigDecimal("0");
	}
	
	/** Check if the partner id is valid partner id. If not perform the operation for all the partners.
	 * @param messageTxt
	 * @param conn
	 * @return
	 */
	public static String getPartner(String messageTxt) {
		if(!StringUtils.isEmpty(messageTxt)) {
			if(logger.isDebugEnabled()) {
				logger.debug("Check if the partnerid /payload is valid - " + messageTxt);
			}
			Connection conn = null;
			try {
				if(!StringUtils.isEmpty(messageTxt)) {
					conn = getConnection();
					PartnerMappingVO partnerVO = new PartnerMappingDAO().getPIPartnerMapping(messageTxt);
					if(partnerVO != null && messageTxt.equals(partnerVO.getPartnerId())) {
						return messageTxt;
					}
				}
			} catch(Exception e) {
				logger.error("Error caught checking if the payload is valid partner message text: " + e);
			} finally {
				PartnerUtils.closeConnection(conn);
			}
		}
		return null;
	}
	
	/** Convenient method to send system messages for partner orders.
	 * @param systemMessage
	 * @param conn
	 */
	public static void sendSystemMessage(String systemMessage, Connection conn) {
		if (!systemMessage.isEmpty()) {
			try {
				new PartnerFeedDAO(conn).sendSystemMessage(systemMessage.toString());
			} catch (Exception e) {
				logger.error("Error caught sending system messages: " + e);
			}
		}
	}
	
	/** Close the connection.
	 * @param conn
	 */
	public static void closeConnection(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error("Error caught closing connection: " + e);
			}
		}
	}
	
	public static String getListAsString(String[] list, String seperator, String prefix, String suffix) {
		StringBuffer string = new StringBuffer();
		if (list != null && list.length > 0) {
			int count = 1;
			
			for (String element : list) { 
				
				if(count != 1) {
					string.append(seperator);
				}
				
				string.append(prefix).append(element).append(suffix); 				
				count++;
			}
		}
		return string.toString();
	}
	
	/**
	 * Method returns String formated Time in this format YYYY-MM-DDTHH:MM:SS-mS
	 * @return String
	 */
	public static String getTimestampString(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		
		if(date == null) {
			cal.setTime(new Date());
		} else {
			cal.setTime(date);
		}

		StringBuffer sb = new StringBuffer();

		sb.append(Integer.toString(cal.get(Calendar.YEAR)));
		sb.append('-');
		sb.append(zeroPad(cal.get(Calendar.MONTH) + 1));
		sb.append('-');
		sb.append(zeroPad(cal.get(Calendar.DATE)));
		sb.append('T');
		sb.append(zeroPad(cal.get(Calendar.HOUR_OF_DAY)));
		sb.append(':');
		sb.append(zeroPad(cal.get(Calendar.MINUTE)));
		sb.append(':');
		sb.append(zeroPad(cal.get(Calendar.SECOND)));
		sb.append('-');
		sb.append(zeroPad(getHourDifferenceBetweenGMTandChicagoTime()));
		sb.append(":00");

		return sb.toString();
	}
	
	private static String zeroPad(int i) {
		String val = Integer.toString(i);
		return i < 10 ? "0" + val : val;
	}
	
	/**
	 * Method gives hours difference between gmt and chicagoTime
	 * @return int
	 */
	private static int getHourDifferenceBetweenGMTandChicagoTime() {
		Calendar calGmt = new GregorianCalendar(TimeZone.getTimeZone(PIConstants.GMT));
		Calendar calChicago = new GregorianCalendar(TimeZone.getTimeZone(PIConstants.CHICAGO_TIME));
		int result = (calGmt.get(Calendar.HOUR)) - calChicago.get(Calendar.HOUR);
		return result < 0 ? result + 12 : result;
	}
	
	/**
	 * @param utilPtnMappingVO
	 * @return
	 */
	public static PartnerMappingVO getPIPtnMappingVO(com.ftd.osp.utilities.vo.PartnerMappingVO utilPtnMappingVO) {
		PartnerMappingVO partnerMappingVO = new PartnerMappingVO();
		partnerMappingVO.setPartnerId(utilPtnMappingVO.getPartnerId());
		partnerMappingVO.setPartnerName(utilPtnMappingVO.getPartnerName());
		partnerMappingVO.setFtdOriginMapping(utilPtnMappingVO.getFtdOriginMapping());
		partnerMappingVO.setMasterOrderNoPrefix(utilPtnMappingVO.getMasterOrderNoPrefix());
		partnerMappingVO.setConfNumberPrefix(utilPtnMappingVO.getConfNumberPrefix());
		partnerMappingVO.setPartnerImage(utilPtnMappingVO.getPartnerImage());
		partnerMappingVO.setDefaultSourceCode(utilPtnMappingVO.getDefSrcCode());
		partnerMappingVO.setDefaultRecalcSourceCode(utilPtnMappingVO.getDefRecalcSrcCode());
		partnerMappingVO.setSatDeliveryAllowedDropship(utilPtnMappingVO.getSatDeliveryAllowedDropship());
		partnerMappingVO.setSatDeliveryAllowedFloral(utilPtnMappingVO.getSatDeliveryAllowedFloral());
		partnerMappingVO.setSunDeliveryAllowedFloral(utilPtnMappingVO.getSunDeliveryAllowedFloral());
		partnerMappingVO.setSendFloristPdbPrice(utilPtnMappingVO.getSendFloristPdbPrice());
		partnerMappingVO.setSendOrdConfEmail(utilPtnMappingVO.getSendOrdConfEmail());
		partnerMappingVO.setSendShipConfEmail(utilPtnMappingVO.getSendShipConfEmail());
		partnerMappingVO.setSendDelConfEmail(utilPtnMappingVO.getSendDelConfEmail());

		partnerMappingVO.setPerformDropshipAVS(utilPtnMappingVO.isPerformDropshipAVS());
		partnerMappingVO.setPerformFloristAVS(utilPtnMappingVO.isPerformFloristAVS());

		partnerMappingVO.setSendDconFeed(utilPtnMappingVO.isSendDconFeed());
		partnerMappingVO.setSendAdjustmentFeed(utilPtnMappingVO.isSendAdjustmentFeed());
		partnerMappingVO.setSendFulfillmentFeed(utilPtnMappingVO.isSendFulfillmentFeed());
		partnerMappingVO.setSendOrdStatusUpdFeed(utilPtnMappingVO.isSendOrdStatusUpdFeed());
		partnerMappingVO.setSendInvoiceFeed(utilPtnMappingVO.isSendInvoiceFeed());
		partnerMappingVO.setDefaultBehaviour(utilPtnMappingVO.isDefaultBehaviour());

		Address address = new Address();
		address.setAddress1(utilPtnMappingVO.getBillingAddressLine1());
		address.setAddress2(utilPtnMappingVO.getBillingAddressLine2());
		address.setCity(utilPtnMappingVO.getBillingAddressCity());
		address.setState(utilPtnMappingVO.getBillingAddressState());
		address.setPostalCode(utilPtnMappingVO.getBillingAddressZipCode());
		address.setCountry(utilPtnMappingVO.getBillingAddressCountry());

		partnerMappingVO.setBillingAddress(address);

		return partnerMappingVO;
	}
}
