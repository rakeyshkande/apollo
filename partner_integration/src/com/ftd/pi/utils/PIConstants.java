/**
 * 
 */
package com.ftd.pi.utils;

/**
 * @author skatam
 * 
 */
public class PIConstants {

	private PIConstants() {
	}

	public final static String PROPERTY_FILE = "pi_config.xml";
	public final static String DATASOURCE_NAME = "DATASOURCE";

	public final static String PULL_ORDERS = "PULL_ORDERS";
	public final static String PROCESS_INBOUND_ORDER = "PROCESS_INBOUND_ORDER";
	public static final String PROCESS_PARTNER_ORDERS = "PROCESS_PARTNER_ORDERS";
	public static final String PROCESS_JMS_ERROR_ORDERS = "PROCESS_JMS_ERROR_ORDERS";
	public static final String PROCESS_OG_ERROR_ORDERS = "PROCESS_OG_ERROR_ORDERS";

	public static final String PROCESS_CREATE_ORD_ADJ_FEEDS = "PROCESS_CREATE_ORD_ADJ_FEEDS";
	public static final String PROCESS_PTN_REFUNDS_ORDERS = "PROCESS_PTN_REFUNDS_ORDERS";
	public static final String PROCESS_CREATE_FLORIST_FULFILL_PTN_DATA = "PROCESS_CREATE_FLORIST_FULFILL_PTN_DATA";
	public static final String PROCESS_CREATE_ORD_FULFILL_FEEDS = "PROCESS_CREATE_ORD_FULFILL_FEEDS";
	public static final String PROCESS_SEND_PARTNER_ADJ_FEEDS = "PROCESS_SEND_PARTNER_ADJ_FEEDS";
	public static final String PROCESS_SEND_PARTNER_FULFILL_FEEDS = "PROCESS_SEND_PARTNER_FULFILL_FEEDS";
	public static final String PROCESS_SEND_DELIVERY_CONFIRMATION_FEEDS = "PROCESS_SEND_PARTNER_DCON_FEEDS";
	public static final String PROCESS_CREATE_ORDER_STATUS_UPDATES = "PROCESS_CREATE_ORD_STATUS_UPDATES";
	public static final String PROCESS_SEND_ORDER_STATUS_UPDATES = "PROCESS_SEND_ORDER_STATUS_UPDATES";
    public static final String PROCESS_INVOICE_READY_ORDERS = "PROCESS_INVOICE_READY_ORDERS";
    public static final String PROCESS_SEND_INVOICE_READY_ORDERS = "PROCESS_SEND_INVOICE_READY_ORDERS";

	public static final String PARTNER_ORDER_GATHERER_URL = "ORDER_GATHERER_URL";
	public final static String ORDER_STATUS_RECEIVED = "RECEIVED";
	public static final String ORDER_STATUS_TRANSFORMED = "TRANSFORMED";
	public final static String ORDER_STATUS_DONE = "DONE";
	public final static String ORDER_STATUS_GATHERER_ERROR = "GATHERER ERROR";

	public final static String ORDER_FEED_STATUS_NEW = "NEW";
	public final static String ORDER_FEED_STATUS_DONE = "DONE";
	public final static String ORDER_FEED_STATUS_JMS_ERROR = "JMS_ERROR";

	public final static int ERROR_CODE = 600;
	public final static int SUCCESS_CODE = 200;
	public final static String EMPTY_XML_ERROR = "Received empty xml transmission.";

	// Global parameters
	public static final String PI_GLOBAL_CONTEXT = "PI_CONFIG";
	public static final String PI_SECURE_CONTEXT = "partner_integration";

	// JMS
	public final static String JMS_PIPELINE_FOR_EM_PARTNERS = "SUCCESS";

	// System messaging
	public static final String SM_PAGE_SOURCE = "PARTNER_INTEGRATION_PAGE";
	public static final String SM_NOPAGE_SOURCE = "PARTNER_INTEGRATION_NOPAGE";
	public static final String SM_PAGE_SUBJECT = "PartnerIntegration Message";
	public static final String SM_NOPAGE_SUBJECT = "NOPAGE PartnerIntegration Message";
	public static final String SM_TYPE = "System Exception";

	public static final boolean NEEDS_PROXY_CONFIG = true;
	public static final String PROXY_HOST = "squid.hyd.int.untd.com";
	public static final int PROXY_PORT = 3128;

	public static final String CATALOG_SUFFIX_STANDARD = "";
	public static final String CATALOG_SUFFIX_DELUXE = "_deluxe";
	public static final String CATALOG_SUFFIX_PREMIUM = "_premium";

	public static final String SKU_SUFFIX_STANDARD = "_A";
	public static final String SKU_SUFFIX_DELUXE = "_B";
	public static final String SKU_SUFFIX_PREMIUM = "_C";

	public static final String PTN_SYS_MSG_SOURCE = "PARTNER INTEGRATION";
	public static final String PTN_SYS_MSG_SUBJ = "Partner Integration Feed Error";
	public static final String PTN_SYS_MSG_TYPE_ERR = "ERROR";

	public static final String NEW_FEED_STATUS = "NEW";
	public static final String SENT_FEED_STATUS = "SENT";
	public static final String FAILED_FEED_STATUS = "FAILED";

	public static final String ADJUSTMENT_TYPE = "ADJUSTMENT";
	public static final String FULFILLMENT_TYPE = "FULFILLMENT";
	public static final String DELIVERTY_CONFIRMATION_TYPE = "DCON";
    public static final String INVOICE_TYPE = "INVOICE";

	public static final String DEFAULT_ADJ_REASON = "GeneralAdjustment";
	
	public static final String ESB_ADJUSTMENT_GATHERER_URL = "EsbAdjustmentGathererUrl";
	public static final String ESB_FULFILLMENT_GATHERER_URL = "EsbFulfillmentGathererUrl";
	public static final String ESB_ORDSTATUS_UPD_GATHERER_URL = "EsbOrderStatusUpdGathererUrl";
	public static final String PARTNER_ID = "partnerID";
	public static final String ESB_AUTH_USERNAME = "EsbAuthUsername";
	public static final String ESB_AUTH_PASSWORD = "EsbAuthpassword";
	public static final String ORD_STATUS_UPD = "ORD_STATUS_UPD";
	
	public static final String GMT = "GMT";
	public static final String CHICAGO_TIME = "America/Chicago";
	public static final String ARI_CONFIRMATION = "Confirmation";
	public static final String ARI_SHIPMENT = "Shipment";
	
	public static final String SHIPPED_TYPE = "SHIPPED";
	
}
