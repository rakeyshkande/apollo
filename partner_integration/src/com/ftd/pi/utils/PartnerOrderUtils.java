package com.ftd.pi.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ftd.pi.partner.vo.OrderItem;
import com.ftd.pi.vo.ftd.AddOn;
import com.ftd.pi.vo.ftd.AddOns;
import com.ftd.pi.vo.ftd.Item;

public class PartnerOrderUtils {
	
	public static AddOns getAddOns(OrderItem orderItem) {
		AddOns addOns = null;
		List<AddOn> addOnList = null;
		
		if (orderItem != null && orderItem.getAddOns() != null && orderItem.getAddOns().getAddOn() != null && !orderItem.getAddOns().getAddOn().isEmpty()) {
			addOnList = new ArrayList<AddOn>();
			addOns = new AddOns();
			AddOn ftdAddOn = null; 
			for (com.ftd.pi.partner.vo.AddOn partnerAddOn : orderItem.getAddOns().getAddOn()) {
				ftdAddOn = new AddOn();
				ftdAddOn.setId(partnerAddOn.getId());
				
				if (partnerAddOn.getAddOnRetailPrice().compareTo(partnerAddOn.getAddOnSalePrice()) > 0) {
					ftdAddOn.setPrice(partnerAddOn.getAddOnSalePrice());
					ftdAddOn.setDiscountAmount(getAddOnDiscountPrice(partnerAddOn.getAddOnRetailPrice(), partnerAddOn.getAddOnSalePrice()));
				}
				else if (partnerAddOn.getAddOnSalePrice().compareTo(BigDecimal.ZERO) > 0) {
					ftdAddOn.setPrice(partnerAddOn.getAddOnSalePrice());
					ftdAddOn.setDiscountAmount(new BigDecimal("0.00"));
				}
				else {
					ftdAddOn.setPrice(new BigDecimal("0.00"));
					ftdAddOn.setDiscountAmount(new BigDecimal("0.00"));
				}
				
				ftdAddOn.setQuantity(partnerAddOn.getQuantity());
				addOnList.add(ftdAddOn);
			}
			addOns.setAddOn(addOnList);
		}
		return addOns;	
	}
	
	private static BigDecimal getAddOnDiscountPrice(BigDecimal retailPrice, BigDecimal salePrice) {
		
		if (retailPrice != null && salePrice != null) {
			if (retailPrice.compareTo(salePrice) >= 0) {
				return retailPrice.subtract(salePrice);
			}
			else {
				return salePrice;
			}
		}
		return null;
	}

	public static BigDecimal getAddOnAmount(Item ftdOrderItem) {
		BigDecimal addOnAmount = BigDecimal.ZERO;
		
		if (ftdOrderItem != null && ftdOrderItem.getAddOns() != null && ftdOrderItem.getAddOns() != null && ftdOrderItem.getAddOns().getAddOn()!= null && !ftdOrderItem.getAddOns().getAddOn().isEmpty()) {
			for (AddOn ftdAddOn : ftdOrderItem.getAddOns().getAddOn()) {
				if(ftdAddOn.getPrice().compareTo(BigDecimal.ZERO) == 0){
					addOnAmount = addOnAmount.add(BigDecimal.ZERO);
				}
				else
					addOnAmount = addOnAmount.add(ftdAddOn.getPrice().multiply(new BigDecimal(ftdAddOn.getQuantity())));
			}
		}

		return addOnAmount;
	}

	public static BigDecimal getAddOnDiscountAmount(Item ftdOrderItem) {
		BigDecimal addOnDiscountAmount = BigDecimal.ZERO;
		
		if (ftdOrderItem != null && ftdOrderItem.getAddOns() != null && ftdOrderItem.getAddOns() != null && ftdOrderItem.getAddOns().getAddOn()!= null && !ftdOrderItem.getAddOns().getAddOn().isEmpty()) {
			for (AddOn ftdAddOn : ftdOrderItem.getAddOns().getAddOn()) {
				if(ftdAddOn.getDiscountAmount().compareTo(BigDecimal.ZERO) == 0)
				{
					addOnDiscountAmount = addOnDiscountAmount.add(BigDecimal.ZERO);
				}
				else
					addOnDiscountAmount = addOnDiscountAmount.add(ftdAddOn.getDiscountAmount().multiply(new BigDecimal(ftdAddOn.getQuantity())));
			}
		}

		return addOnDiscountAmount;
	}

}
