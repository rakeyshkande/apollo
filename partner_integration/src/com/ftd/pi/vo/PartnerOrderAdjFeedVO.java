package com.ftd.pi.vo;

import java.util.ArrayList;

import java.util.List;

/**
 * @author smeka
 *
 */
public class PartnerOrderAdjFeedVO extends PartnerFeedVO {
	
	private List<PartnerOrderItemAdjFeedVO> itemAdjFeeds;
	
	/**
	 * @return
	 */
	public List<PartnerOrderItemAdjFeedVO> getItemAdjFeeds() {
		if(itemAdjFeeds == null) {
			itemAdjFeeds = new ArrayList<PartnerOrderItemAdjFeedVO>();
		}
		return itemAdjFeeds;
	}
	
	/**
	 * @param itemAdjFeeds
	 */
	public void setItemAdjFeeds(List<PartnerOrderItemAdjFeedVO> itemAdjFeeds) {
		this.itemAdjFeeds = itemAdjFeeds;
	}	
}
