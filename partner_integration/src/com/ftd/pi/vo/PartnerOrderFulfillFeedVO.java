/**
 * 
 */
package com.ftd.pi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author smeka
 * 
 */
public class PartnerOrderFulfillFeedVO extends PartnerFeedVO {
	
	private List<PartnerOrderItemFulFillFeedVO> itemFulFillFeeds;
		
	/**
	 * @return
	 */
	public List<PartnerOrderItemFulFillFeedVO> getItemFulFillFeeds() {
		if(itemFulFillFeeds == null) {
			itemFulFillFeeds = new ArrayList<PartnerOrderItemFulFillFeedVO>();
		}
		return itemFulFillFeeds;
	}

	/**
	 * @param itemFulFillFeeds
	 */
	public void setItemFulFillFeeds(
			List<PartnerOrderItemFulFillFeedVO> itemFulFillFeeds) {
		this.itemFulFillFeeds = itemFulFillFeeds;
	}
}
