/**
 * 
 */
package com.ftd.pi.vo;

/**
 * @author smeka
 *
 */
public class PartnerFeedVO {
	private String feedId;
	private String feedType;
	private String feedStatus;
	
	private String transactionId;
	private String createdBy;
	private String updatedBy;
	
	private String partnerOrderNumber;
	private String ftdOrderNumber;
	
	private String partnerId;
	private String partnerName;
	
	public String getFeedId() {
		return feedId;
	}
	public void setFeedId(String feedId) {
		this.feedId = feedId;
	}
	public String getFeedType() {
		return feedType;
	}
	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}
	public String getFeedStatus() {
		return feedStatus;
	}
	public void setFeedStatus(String feedStatus) {
		this.feedStatus = feedStatus;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerOrderNumber() {
		return partnerOrderNumber;
	}
	public void setPartnerOrderNumber(String partnerOrderNumber) {
		this.partnerOrderNumber = partnerOrderNumber;
	}
	public String getFtdOrderNumber() {
		return ftdOrderNumber;
	}
	public void setFtdOrderNumber(String ftdOrderNumber) {
		this.ftdOrderNumber = ftdOrderNumber;
	}	
}
