package com.ftd.pi.vo;

import java.math.BigDecimal;

public class PartnerOrderDetailVO 
{
	private String partnerItemNumber;
	private String partnerId;
	private String partnerOrderId;
	private String partnerOrderNumber;
	private String partnerOrderItemId;
	private String confirmationNumber;
	private String productId;
	private BigDecimal retailPrice;
	private BigDecimal salePrice;
	private BigDecimal shippingAmount;
	private BigDecimal taxAmount;
	private String placement;
	private String discountCode;
	private BigDecimal addOnRetailPrice;
	private BigDecimal addOnSalePrice;
	private String legacyID;
	
	
	public String getPartnerItemNumber() {
		return partnerItemNumber;
	}
	public void setPartnerItemNumber(String partnerItemNumber) {
		this.partnerItemNumber = partnerItemNumber;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerOrderId() {
		return partnerOrderId;
	}
	public void setPartnerOrderId(String partnerOrderId) {
		this.partnerOrderId = partnerOrderId;
	}
	public String getPartnerOrderNumber() {
		return partnerOrderNumber;
	}
	public void setPartnerOrderNumber(String partnerOrderNumber) {
		this.partnerOrderNumber = partnerOrderNumber;
	}
	public String getPartnerOrderItemId() {
		return partnerOrderItemId;
	}
	public void setPartnerOrderItemId(String partnerOrderItemId) {
		this.partnerOrderItemId = partnerOrderItemId;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public BigDecimal getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice = retailPrice;
	}
	public BigDecimal getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}
	public BigDecimal getShippingAmount() {
		return shippingAmount;
	}
	public void setShippingAmount(BigDecimal shippingAmount) {
		this.shippingAmount = shippingAmount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getPlacement() {
		return placement;
	}
	public void setPlacement(String placement) {
		this.placement = placement;
	}
	public String getDiscountCode() {
		return discountCode;
	}
	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}
	/**
	 * @return the addOnRetailPrice
	 */
	public BigDecimal getAddOnRetailPrice() {
		return addOnRetailPrice;
	}
	/**
	 * @return the addOnSalePrice
	 */
	public BigDecimal getAddOnSalePrice() {
		return addOnSalePrice;
	}
	/**
	 * @param addOnRetailPrice the addOnRetailPrice to set
	 */
	public void setAddOnRetailPrice(BigDecimal addOnRetailPrice) {
		this.addOnRetailPrice = addOnRetailPrice;
	}
	/**
	 * @param addOnSalePrice the addOnSalePrice to set
	 */
	public void setAddOnSalePrice(BigDecimal addOnSalePrice) {
		this.addOnSalePrice = addOnSalePrice;
	}
	
	/**
	 * @return legacyID
	 */
	public String getLegacyID() {
		return legacyID;
	}
	/**
	 * @param legacyID
	 */
	public void setLegacyID(String legacyID) {
		this.legacyID = legacyID;
	}
	
	
	
}
