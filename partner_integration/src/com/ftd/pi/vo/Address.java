package com.ftd.pi.vo;

import com.ftd.pi.partner.vo.BillingAddress;
import com.ftd.pi.partner.vo.FulFillmentAddress;


public class Address {

    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    
    public static Address getAsAddress(BillingAddress billingAddress)
    {
    	return new Address(
    			billingAddress.getAddress1(), billingAddress.getAddress2(),
    			billingAddress.getCity(), billingAddress.getState(), 
    			billingAddress.getPostalCode(), billingAddress.getCountry());
    }
    public static Address getAsAddress(FulFillmentAddress fulFillmentAddress)
    {
    	return new Address(
    			fulFillmentAddress.getAddress1(), fulFillmentAddress.getAddress2(),
    			fulFillmentAddress.getCity(), fulFillmentAddress.getState(), 
    			fulFillmentAddress.getPostalCode(), fulFillmentAddress.getCountry());
    }
    public BillingAddress getAsBillingAddress()
    {
    	return new BillingAddress(
    			address1, address2,
    			city, state, postalCode, country);
    }
    public FulFillmentAddress getAsFulFillmentAddress()
    {
    	return new FulFillmentAddress(
    			address1, address2,
    			city, state, postalCode, country);
    }
    public Address() {
	}
    
	public Address(String address1, String address2, String city, String state,
			String postalCode, String country) {
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.country = country;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
    
    
}
