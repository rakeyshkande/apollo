/**
 * 
 */
package com.ftd.pi.vo;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.partner.vo.Order;

/**
 * @author skatam
 *
 */
public class PartnerOrderFeed 
{
	private String orderFeedId;
	private Order order;
	private String status;
	
	public PartnerOrderFeed() {
	}
	public PartnerOrderFeed(String orderFeedId, Order order) {
		this.orderFeedId = orderFeedId;
		this.order = order;
	}
	
	public String getOrderFeedId() {
		return orderFeedId;
	}
	public void setOrderFeedId(String orderFeedId) {
		this.orderFeedId = orderFeedId;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public String getAsXmlString() 
	{
    	String xml = "";
    	try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Order.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"FTD_Order.xsd");
			
			StringWriter sw = new StringWriter();
			
			marshaller.marshal(this.order, sw);
			xml = sw.toString();
			
		} catch (JAXBException e) {
			throw new PartnerException(e);
		}
		return xml;
	}
	
	

}
