/**
 * 
 */
package com.ftd.pi.vo;

/**
 * @author skatam
 * 
 */
public class PartnerMappingVO {
	private String partnerId;
	private String partnerName;
	private String ftdOriginMapping;
	private String masterOrderNoPrefix;
	private String partnerImage;
	private String confNumberPrefix;
	private String defSrcCode;
	private String defRecalcSrcCode;
	private Address billingAddress;
	private String satDeliveryAllowedDropship;
	private String satDeliveryAllowedFloral;
	private String sunDeliveryAllowedFloral;

	private String sendFloristPdbPrice;
	private String sendOrdConfEmail;
	private String sendShipConfEmail;
	private String sendDelConfEmail;

	private boolean performFloristAVS;
	private boolean performDropshipAVS;

	// Feed Config
	private boolean sendAdjustmentFeed;
	private boolean sendFulfillmentFeed;
	private boolean sendOrdStatusUpdFeed;
	private boolean sendShipStatusUpdFeed;
	private boolean sendInvoiceFeed;
	private boolean sendDconFeed;

	// Y when order received by POG
	private boolean isDefaultBehaviour;

	private boolean addonSubscription;

	private boolean isIncrTotalAllowed;

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getFtdOriginMapping() {
		return ftdOriginMapping;
	}

	public void setFtdOriginMapping(String ftdOriginMapping) {
		this.ftdOriginMapping = ftdOriginMapping;
	}

	public String getMasterOrderNoPrefix() {
		return masterOrderNoPrefix;
	}

	public void setMasterOrderNoPrefix(String masterOrderNoPrefix) {
		this.masterOrderNoPrefix = masterOrderNoPrefix;
	}

	public String getPartnerImage() {
		return partnerImage;
	}

	public void setPartnerImage(String partnerImage) {
		this.partnerImage = partnerImage;
	}

	public String getConfNumberPrefix() {
		return confNumberPrefix;
	}

	public void setConfNumberPrefix(String confNumberPrefix) {
		this.confNumberPrefix = confNumberPrefix;
	}

	public String getDefaultSourceCode() {
		return defSrcCode;
	}

	public void setDefaultSourceCode(String defSrcCode) {
		this.defSrcCode = defSrcCode;
	}

	public String getDefaultRecalSourceCode() {
		return defRecalcSrcCode;
	}

	public void setDefaultRecalcSourceCode(String defRecalcSrcCode) {
		this.defRecalcSrcCode = defRecalcSrcCode;
	}

	public void setBillingAddress(Address address) {
		this.billingAddress = address;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public String getDefSrcCode() {
		return defSrcCode;
	}

	public void setDefSrcCode(String defSrcCode) {
		this.defSrcCode = defSrcCode;
	}

	public String getDefRecalcSrcCode() {
		return defRecalcSrcCode;
	}

	public void setDefRecalcSrcCode(String defRecalcSrcCode) {
		this.defRecalcSrcCode = defRecalcSrcCode;
	}

	public String getSatDeliveryAllowedDropship() {
		return satDeliveryAllowedDropship;
	}

	public void setSatDeliveryAllowedDropship(String satDeliveryAllowedDropship) {
		this.satDeliveryAllowedDropship = satDeliveryAllowedDropship;
	}

	public String getSatDeliveryAllowedFloral() {
		return satDeliveryAllowedFloral;
	}

	public void setSatDeliveryAllowedFloral(String satDeliveryAllowedFloral) {
		this.satDeliveryAllowedFloral = satDeliveryAllowedFloral;
	}

	public String getSunDeliveryAllowedFloral() {
		return sunDeliveryAllowedFloral;
	}

	public void setSunDeliveryAllowedFloral(String sunDeliveryAllowedFloral) {
		this.sunDeliveryAllowedFloral = sunDeliveryAllowedFloral;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getSendFloristPdbPrice() {
		return sendFloristPdbPrice;
	}

	public void setSendFloristPdbPrice(String sendFloristPdbPrice) {
		this.sendFloristPdbPrice = sendFloristPdbPrice;
	}

	public String getSendOrdConfEmail() {
		return sendOrdConfEmail;
	}

	public void setSendOrdConfEmail(String sendOrdConfEmail) {
		this.sendOrdConfEmail = sendOrdConfEmail;
	}

	public String getSendShipConfEmail() {
		return sendShipConfEmail;
	}

	public void setSendShipConfEmail(String sendShipConfEmail) {
		this.sendShipConfEmail = sendShipConfEmail;
	}

	public String getSendDelConfEmail() {
		return sendDelConfEmail;
	}

	public void setSendDelConfEmail(String sendDelConfEmail) {
		this.sendDelConfEmail = sendDelConfEmail;
	}

	public boolean isPerformFloristAVS() {
		return performFloristAVS;
	}

	public void setPerformFloristAVS(boolean performFloristAVS) {
		this.performFloristAVS = performFloristAVS;
	}

	public boolean isPerformDropshipAVS() {
		return performDropshipAVS;
	}

	public void setPerformDropshipAVS(boolean performDropshipAVS) {
		this.performDropshipAVS = performDropshipAVS;
	}

	public boolean isSendDconFeed() {
		return sendDconFeed;
	}

	public void setSendDconFeed(boolean sendDconFeed) {
		this.sendDconFeed = sendDconFeed;
	}

	public void setAddonSubscription(boolean addonSubscription) {

		this.addonSubscription = addonSubscription;
	}

	public boolean getAddonSubscription() {
		return addonSubscription;
	}

	public boolean isSendAdjustmentFeed() {
		return sendAdjustmentFeed;
	}

	public void setSendAdjustmentFeed(boolean sendAdjustmentFeed) {
		this.sendAdjustmentFeed = sendAdjustmentFeed;
	}

	public boolean isSendFulfillmentFeed() {
		return sendFulfillmentFeed;
	}

	public void setSendFulfillmentFeed(boolean sendFulfillmentFeed) {
		this.sendFulfillmentFeed = sendFulfillmentFeed;
	}

	public boolean isSendOrdStatusUpdFeed() {
		return sendOrdStatusUpdFeed;
	}

	public void setSendOrdStatusUpdFeed(boolean sendOrdStatusUpdFeed) {
		this.sendOrdStatusUpdFeed = sendOrdStatusUpdFeed;
	}

	public boolean isSendShipStatusUpdFeed() {
		return sendShipStatusUpdFeed;
	}

	public void setSendShipStatusUpdFeed(boolean sendShipStatusUpdFeed) {
		this.sendShipStatusUpdFeed = sendShipStatusUpdFeed;
	}

	public boolean isSendInvoiceFeed() {
		return sendInvoiceFeed;
	}

	public void setSendInvoiceFeed(boolean sendInvoiceFeed) {
		this.sendInvoiceFeed = sendInvoiceFeed;
	}

	public boolean isDefaultBehaviour() {
		return isDefaultBehaviour;
	}

	public void setDefaultBehaviour(boolean isDefaultBehaviour) {
		this.isDefaultBehaviour = isDefaultBehaviour;
	}
	public boolean isIncrTotalAllowed() {
		return isIncrTotalAllowed;
	}
	public void setisIncrTotalAllowed(boolean isIncrTotalAllowed) {
		this.isIncrTotalAllowed = isIncrTotalAllowed;
	}

}
