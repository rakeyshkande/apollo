package com.ftd.pi.vo;

import java.math.BigDecimal;

/**
 * @author smeka
 *
 */
public class PartnerOrderItemAdjFeedVO {
	
	private String adjustmentType;
	private String adjustmentReason;	
	private String itemSKU;
	private BigDecimal itemPrincipalAmount;
	private BigDecimal itemAddOnAmount;
	private BigDecimal itemTaxAmount;
	private BigDecimal shippingPrincipalAmount;
	private BigDecimal shippingTaxAmount;
	
	private String partnerOrderItemNumber;
	private String ftdExternalOrderNumber;
	private String partnerOrderItemAdjId;	
		
	
	public String getAdjustmentReason() {
		return adjustmentReason;
	}
	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}
	public String getItemSKU() {
		return itemSKU;
	}
	public void setItemSKU(String itemSKU) {
		this.itemSKU = itemSKU;
	}	
	public BigDecimal getItemPrincipalAmount() {
		return itemPrincipalAmount;
	}
	public void setItemPrincipalAmount(BigDecimal itemPrincipalAmount) {
		this.itemPrincipalAmount = itemPrincipalAmount;
	}
	public BigDecimal getItemTaxAmount() {
		return itemTaxAmount;
	}
	public void setItemTaxAmount(BigDecimal itemTaxAmount) {
		this.itemTaxAmount = itemTaxAmount;
	}
	public BigDecimal getShippingPrincipalAmount() {
		return shippingPrincipalAmount;
	}
	public void setShippingPrincipalAmount(BigDecimal shippingPrincipalAmount) {
		this.shippingPrincipalAmount = shippingPrincipalAmount;
	}
	public BigDecimal getShippingTaxAmount() {
		return shippingTaxAmount;
	}
	public void setShippingTaxAmount(BigDecimal shippingTaxAmount) {
		this.shippingTaxAmount = shippingTaxAmount;
	}
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}	
	public String getPartnerOrderItemNumber() {
		return partnerOrderItemNumber;
	}
	public void setPartnerOrderItemNumber(String partnerOrderItemNumber) {
		this.partnerOrderItemNumber = partnerOrderItemNumber;
	}
	public String getPartnerOrderItemAdjId() {
		return partnerOrderItemAdjId;
	}
	public void setPartnerOrderItemAdjId(String partnerOrderItemAdjId) {
		this.partnerOrderItemAdjId = partnerOrderItemAdjId;
	}
	public String getFtdExternalOrderNumber() {
		return ftdExternalOrderNumber;
	}
	public void setFtdExternalOrderNumber(String ftdExternalOrderNumber) {
		this.ftdExternalOrderNumber = ftdExternalOrderNumber;
	}
	
	public BigDecimal getItemAddOnAmount() {
		return itemAddOnAmount;
	}
	public void setItemAddOnAmount(BigDecimal itemAddOnAmount) {
		this.itemAddOnAmount = itemAddOnAmount;
	}
}
