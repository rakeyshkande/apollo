package com.ftd.pi.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Respons object for ESB http call for publishing Adjustment and fulfillment
 * feeds
 * 
 * @author kvasant
 * 
 */
@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class EsbResponse {

	@XmlElement(name = "Status")
	String status;
	@XmlElement(name = "StatusCode")
	int statusCode;
	@XmlElement(name = "FailureDescription")
	String failureDescription;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getFailureDescription() {
		return failureDescription;
	}

	public void setFailureDescription(String failureDescription) {
		this.failureDescription = failureDescription;
	}
}
