/**
 * 
 */
package com.ftd.pi.vo;

/**
 * @author skatam
 *
 */
public class PartnerOrderVO 
{
	private String partnerOrderId;
	private String partnerOrderFeedId;
	private String partnerId;
	private String partnerOrderNumber;
	private String masterOrderNumber;
	private String partnerXml;
	private String ftdXml;
	private String orderDate;
	private String orderStatus;
	private String currencyCode;
	
	public String getPartnerOrderId() {
		return partnerOrderId;
	}
	public void setPartnerOrderId(String partnerOrderId) {
		this.partnerOrderId = partnerOrderId;
	}
	public String getPartnerOrderFeedId() {
		return partnerOrderFeedId;
	}
	public void setPartnerOrderFeedId(String partnerOrderFeedId) {
		this.partnerOrderFeedId = partnerOrderFeedId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerOrderNumber() {
		return partnerOrderNumber;
	}
	public void setPartnerOrderNumber(String partnerOrderNumber) {
		this.partnerOrderNumber = partnerOrderNumber;
	}
	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}
	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}
	public String getPartnerXml() {
		return partnerXml;
	}
	public void setPartnerXml(String partnerXml) {
		this.partnerXml = partnerXml;
	}
	public String getFtdXml() {
		return ftdXml;
	}
	public void setFtdXml(String ftdXml) {
		this.ftdXml = ftdXml;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
