package com.ftd.pi.vo;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * @author smeka
 * 
 */
public class PartnerOrderItemFulFillFeedVO {
	
	private String shippingTrackingNumber;
	private String shippingMethod;
	private String itemSKU;
	private int quantity;
	private String carrier;
	private XMLGregorianCalendar deliveryDate;
	
	private String partnerOrderItemFulFillId;	
	private String ftdExternalOrderNumber;	
	private String partnerOrderItemNumber;
	
	private String deliveryStatus;
	private XMLGregorianCalendar deliveryStatusDateTime;
	
	public String getShippingTrackingNumber() {
		return shippingTrackingNumber;
	}
	public void setShippingTrackingNumber(String shippingTrackingNumber) {
		this.shippingTrackingNumber = shippingTrackingNumber;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getItemSKU() {
		return itemSKU;
	}
	public void setItemSKU(String itemSKU) {
		this.itemSKU = itemSKU;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getPartnerOrderItemFulFillId() {
		return partnerOrderItemFulFillId;
	}
	public void setPartnerOrderItemFulFillId(String partnerOrderItemFulFillId) {
		this.partnerOrderItemFulFillId = partnerOrderItemFulFillId;
	}
	public String getPartnerOrderItemNumber() {
		return partnerOrderItemNumber;
	}
	public void setPartnerOrderItemNumber(String partnerOrderItemNumber) {
		this.partnerOrderItemNumber = partnerOrderItemNumber;
	}	
	public String getFtdExternalOrderNumber() {
		return ftdExternalOrderNumber;
	}
	public void setFtdExternalOrderNumber(String ftdExternalOrderNumber) {
		this.ftdExternalOrderNumber = ftdExternalOrderNumber;
	}
	public XMLGregorianCalendar getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(XMLGregorianCalendar deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public XMLGregorianCalendar getDeliveryStatusDateTime() {
		return deliveryStatusDateTime;
	}
	public void setDeliveryStatusDateTime(XMLGregorianCalendar deliveryStatusDateTime) {
		this.deliveryStatusDateTime = deliveryStatusDateTime;
	}
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
}
