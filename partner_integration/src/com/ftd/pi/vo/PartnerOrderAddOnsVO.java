package com.ftd.pi.vo;

import java.math.BigDecimal;
import java.util.Date;

public class PartnerOrderAddOnsVO 
{
	private long partnerOrderDetailId;
	private String addOnId;
	private BigDecimal retailPrice;
	private BigDecimal salePrice;
	private Integer quantity;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	
	/**
	 * @return the partnerOrderDetailId
	 */
	public long getPartnerOrderDetailId() {
		return partnerOrderDetailId;
	}
	/**
	 * @param partnerOrderDetailId the partnerOrderDetailId to set
	 */
	public void setPartnerOrderDetailId(long partnerOrderDetailId) {
		this.partnerOrderDetailId = partnerOrderDetailId;
	}
	/**
	 * @return the addOnId
	 */
	public String getAddOnId() {
		return addOnId;
	}
	/**
	 * @param addOnId the addOnId to set
	 */
	public void setAddOnId(String addOnId) {
		this.addOnId = addOnId;
	}
	/**
	 * @return the retailPrice
	 */
	public BigDecimal getRetailPrice() {
		return retailPrice;
	}
	/**
	 * @param retailPrice the retailPrice to set
	 */
	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice = retailPrice;
	}
	/**
	 * @return the salePrice
	 */
	public BigDecimal getSalePrice() {
		return salePrice;
	}
	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
