package com.ftd.pi.vo.ftd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class AvsAddressScores {

	@XmlElement(name="avs-score-medium-confidence")
	private int avsScoreMediumConfidence;
	@XmlElement(name="avs-score-address-line-different")
	private int avsScoreAddressLineDifferent;
	@XmlElement(name="avs-score-zip-different")
	private int avsScoreZipDifferent;
	@XmlElement(name="avs-score-city-different")
	private int avsScoreCityDifferent;
	@XmlElement(name="avs-score-low-confidence")
	private int avsScoreLowConfidence;
	@XmlElement(name="avs-score-high-confidence")
	private int avsScoreHighConfidence;
	@XmlElement(name="avs-score-address-type-other")
	private int avsScoreAddressTypeOther;
	@XmlElement(name="avs-score-zipplusfour-different")
	private int avsScoreZipplusfourDifferent;
	@XmlElement(name="avs-score-state-different")
	private int avsScoreStateDifferent;
	@XmlElement(name="avs-score-address-type-address")
	private int avsScoreAddressTypeAddress;
	public int getAvsScoreMediumConfidence() {
		return avsScoreMediumConfidence;
	}
	public void setAvsScoreMediumConfidence(int avsScoreMediumConfidence) {
		this.avsScoreMediumConfidence = avsScoreMediumConfidence;
	}
	public int getAvsScoreAddressLineDifferent() {
		return avsScoreAddressLineDifferent;
	}
	public void setAvsScoreAddressLineDifferent(int avsScoreAddressLineDifferent) {
		this.avsScoreAddressLineDifferent = avsScoreAddressLineDifferent;
	}
	public int getAvsScoreZipDifferent() {
		return avsScoreZipDifferent;
	}
	public void setAvsScoreZipDifferent(int avsScoreZipDifferent) {
		this.avsScoreZipDifferent = avsScoreZipDifferent;
	}
	public int getAvsScoreCityDifferent() {
		return avsScoreCityDifferent;
	}
	public void setAvsScoreCityDifferent(int avsScoreCityDifferent) {
		this.avsScoreCityDifferent = avsScoreCityDifferent;
	}
	public int getAvsScoreLowConfidence() {
		return avsScoreLowConfidence;
	}
	public void setAvsScoreLowConfidence(int avsScoreLowConfidence) {
		this.avsScoreLowConfidence = avsScoreLowConfidence;
	}
	public int getAvsScoreHighConfidence() {
		return avsScoreHighConfidence;
	}
	public void setAvsScoreHighConfidence(int avsScoreHighConfidence) {
		this.avsScoreHighConfidence = avsScoreHighConfidence;
	}
	public int getAvsScoreAddressTypeOther() {
		return avsScoreAddressTypeOther;
	}
	public void setAvsScoreAddressTypeOther(int avsScoreAddressTypeOther) {
		this.avsScoreAddressTypeOther = avsScoreAddressTypeOther;
	}
	public int getAvsScoreZipplusfourDifferent() {
		return avsScoreZipplusfourDifferent;
	}
	public void setAvsScoreZipplusfourDifferent(int avsScoreZipplusfourDifferent) {
		this.avsScoreZipplusfourDifferent = avsScoreZipplusfourDifferent;
	}
	public int getAvsScoreStateDifferent() {
		return avsScoreStateDifferent;
	}
	public void setAvsScoreStateDifferent(int avsScoreStateDifferent) {
		this.avsScoreStateDifferent = avsScoreStateDifferent;
	}
	public int getAvsScoreAddressTypeAddress() {
		return avsScoreAddressTypeAddress;
	}
	public void setAvsScoreAddressTypeAddress(int avsScoreAddressTypeAddress) {
		this.avsScoreAddressTypeAddress = avsScoreAddressTypeAddress;
	}
	
	
}
