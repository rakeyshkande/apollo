package com.ftd.pi.vo.ftd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="order")
@XmlAccessorType(XmlAccessType.FIELD)
public class FTDOrder {

	private Header header;
	@XmlElementWrapper(name="items")
	@XmlElement(name="item")
	private List<Item> items;
	
	
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public List<Item> getItems() 
	{
		if(items==null){
			items = new ArrayList<Item>();
		}
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
}
