package com.ftd.pi.vo.ftd;

import java.math.BigDecimal;

public class Tax {

	protected String type;
	protected String description;
	protected BigDecimal rate;
	protected BigDecimal amount;

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setType(String value) {
		this.type = value;
	}

	/**
	 * Gets the value of the description property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDescription(String value) {
		this.description = value;
	}

	/**
	 * Gets the value of the rate property.
	 * 
	 */
	public BigDecimal getRate() {
		return rate;
	}

	/**
	 * Sets the value of the rate property.
	 * 
	 */
	public void setRate(BigDecimal value) {
		this.rate = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 */
	public void setAmount(BigDecimal value) {
		this.amount = value;
	}

}
