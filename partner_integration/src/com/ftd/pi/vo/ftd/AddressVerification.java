package com.ftd.pi.vo.ftd;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressVerification {

	@XmlElement(name="recip-avs-performed")
	private String recipAvsPerformed;
	@XmlElement(name="recip-address-verification-result")
	private String recipAddressVerificationResult;
	@XmlElement(name="recip-address-verification-override-flag")
	private String recipAddressVerificationOverrideFlag;
	@XmlElement(name="avs-street-address")
	private String avsStreetAddress;
	@XmlElement(name="avs-city")
	private String avsCity;
	@XmlElement(name="avs-state")
	private String avsState;
	@XmlElement(name="avs-postal-code")
	private BigDecimal avsPostalCode;
	@XmlElement(name="avs-country")
	private String avsCountry;
	@XmlElement(name="avs-longitude")
	private BigDecimal avsLongitude;
	@XmlElement(name="avs-latitude")
	private BigDecimal avsLatitude;
	@XmlElement(name="avs-entity-type")
	private String avsEntityType;
	@XmlElement(name="avs-address-scores")
	private AvsAddressScores avsAddressScores;
	public String getRecipAvsPerformed() {
		return recipAvsPerformed;
	}
	public void setRecipAvsPerformed(String recipAvsPerformed) {
		this.recipAvsPerformed = recipAvsPerformed;
	}
	public String getRecipAddressVerificationResult() {
		return recipAddressVerificationResult;
	}
	public void setRecipAddressVerificationResult(
			String recipAddressVerificationResult) {
		this.recipAddressVerificationResult = recipAddressVerificationResult;
	}
	public String getRecipAddressVerificationOverrideFlag() {
		return recipAddressVerificationOverrideFlag;
	}
	public void setRecipAddressVerificationOverrideFlag(
			String recipAddressVerificationOverrideFlag) {
		this.recipAddressVerificationOverrideFlag = recipAddressVerificationOverrideFlag;
	}
	public String getAvsStreetAddress() {
		return avsStreetAddress;
	}
	public void setAvsStreetAddress(String avsStreetAddress) {
		this.avsStreetAddress = avsStreetAddress;
	}
	public String getAvsCity() {
		return avsCity;
	}
	public void setAvsCity(String avsCity) {
		this.avsCity = avsCity;
	}
	public String getAvsState() {
		return avsState;
	}
	public void setAvsState(String avsState) {
		this.avsState = avsState;
	}
	public BigDecimal getAvsPostalCode() {
		return avsPostalCode;
	}
	public void setAvsPostalCode(BigDecimal avsPostalCode) {
		this.avsPostalCode = avsPostalCode;
	}
	public String getAvsCountry() {
		return avsCountry;
	}
	public void setAvsCountry(String avsCountry) {
		this.avsCountry = avsCountry;
	}
	public BigDecimal getAvsLongitude() {
		return avsLongitude;
	}
	public void setAvsLongitude(BigDecimal avsLongitude) {
		this.avsLongitude = avsLongitude;
	}
	public BigDecimal getAvsLatitude() {
		return avsLatitude;
	}
	public void setAvsLatitude(BigDecimal avsLatitude) {
		this.avsLatitude = avsLatitude;
	}
	public String getAvsEntityType() {
		return avsEntityType;
	}
	public void setAvsEntityType(String avsEntityType) {
		this.avsEntityType = avsEntityType;
	}
	public AvsAddressScores getAvsAddressScores() {
		return avsAddressScores;
	}
	public void setAvsAddressScores(AvsAddressScores avsAddressScores) {
		this.avsAddressScores = avsAddressScores;
	}
	
		
}
