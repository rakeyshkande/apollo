package com.ftd.pi.vo.ftd;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Item {

	@XmlElement(name="add-ons")
	private AddOns addOns;
	@XmlElement(name = "order-number")
	private String orderNumber;
	@XmlElement(name = "item-source-code")
	private String itemSourceCode;
	@XmlElement(name = "delivery-date")
	private String deliveryDate;
	@XmlElement(name = "second-delivery-date")
	private String secondDeliveryDate;
	@XmlElement(name = "productid")
	private String productid;
	@XmlElement(name = "product-subcode-id")
	private String productSubcodeId;
	@XmlElement(name = "first-color-choice")
	private String firstColorChoice;
	@XmlElement(name = "second-color-choice")
	private String secondColorChoice;
	@XmlElement(name = "item-of-the-week-flag")
	private String itemOfTheWeekFlag;
	@XmlElement(name = "occassion")
	private String occassion;
	@XmlElement(name = "shipping-method")
	private String shippingMethod;
	@XmlElement(name = "size-indicator")
	private String sizeIndicator;
	@XmlElement(name = "product-price")
	private String productPrice;
	@XmlElement(name = "retail-variable-price")
	private String retailVariablePrice;
	@XmlElement(name = "discounted-product-price")
	private String discountedProductPrice;
	@XmlElement(name = "discount-amount")
	private String discountAmount;
	@XmlElement(name = "service-fee")
	private String serviceFee;
	@XmlElement(name = "drop-ship-charges")
	private String dropShipCharges;
	@XmlElement(name = "tax-amount")
	private String taxAmount;

	@XmlElement(name="add-on-amount")
	private BigDecimal addOnAmount;
	@XmlElement(name="add-on-discount-amount")
	private BigDecimal addOnDiscountAmount;
	@XmlElement(name = "order-total")
	private String orderTotal;
	@XmlElement(name = "miles-points")
	private int milesPoints;
	@XmlElement(name = "personal-greeting-id")
	private String personalGreetingId;
	@XmlElement(name = "bin-source-changed")
	private String binSourceChanged;
	@XmlElement(name = "recip-first-name")
	private String recipFirstName;
	@XmlElement(name = "recip-last-name")
	private String recipLastName;
	@XmlElement(name = "recip-address1")
	private String recipAddress1;
	@XmlElement(name = "recip-address2")
	private String recipAddress2;
	@XmlElement(name = "recip-city")
	private String recipCity;
	@XmlElement(name = "recip-state")
	private String recipState;
	@XmlElement(name = "recip-postal-code")
	private String recipPostalCode;
	@XmlElement(name = "recip-country")
	private String recipCountry;
	@XmlElement(name = "recip-phone")
	private String recipPhone;
	@XmlElement(name = "recip-phone-ext")
	private String recipPhoneExt;
	@XmlElement(name = "qms-result-code")
	private String qmsResultCode;
	@XmlElement(name = "ship-to-type")
	private String shipToType;
	@XmlElement(name = "ship-to-type-name")
	private String shipToTypeName;
	@XmlElement(name = "card-message")
	private String cardMessagespecialInstructions;
	@XmlElement(name = "order-comments")
	private String orderComments;
	@XmlElement(name = "florist")
	private String florist;
	@XmlElement(name = "address-verification")
	private AddressVerification addressVerification;
	@XmlElement(name = "order-item-number")
	private String orderItemNumber;
	@XmlElement(name = "shipping-tax-amount")
	private String shippingTaxAmount;
	@XmlElement(name = "quantity")
	private String quantity;
	@XmlElement(name = "commission")
	private String commission;
	@XmlElement(name = "fol_indicator")
	private String folIndicator;
	@XmlElement(name = "sunday-delivery-flag")
	private String sundayDeliveryFlag;

	@XmlElement(name = "sender-release-flag")
	private String senderReleaseFlag;
	@XmlElement(name = "product-substitution-acknowledgement")
	private String productSubstitutionAcknowledgement;
	@XmlElement(name = "pdb-price")
	private String pdbPrice;
	@XmlElement(name = "transaction")
	private String transaction;
	@XmlElement(name = "wholesale")
	private String wholesale;
	@XmlElement(name = "discount-type")
	private String discountType;
	@XmlElement(name = "special-instructions")
	private String specialInstructions;

	@XmlElement(name = "morning-delivery-fee")
	private String morningDeliveryFee;
	
	@XmlElement(name = "fuel-surcharge")
	private String fuelSurcharge; 

	@XmlElement(required = true)
	private Taxes taxes;
	
	@XmlElement(name = "legacy_id")
	private String legacyID;
	
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getSenderReleaseFlag() {
		return senderReleaseFlag;
	}

	public void setSenderReleaseFlag(String senderReleaseFlag) {
		this.senderReleaseFlag = senderReleaseFlag;
	}

	public String getProductSubstitutionAcknowledgement() {
		return productSubstitutionAcknowledgement;
	}

	public void setProductSubstitutionAcknowledgement(
			String productSubstitutionAcknowledgement) {
		this.productSubstitutionAcknowledgement = productSubstitutionAcknowledgement;
	}

	public String getPdbPrice() {
		return pdbPrice;
	}

	public void setPdbPrice(String pdbPrice) {
		this.pdbPrice = pdbPrice;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getWholesale() {
		return wholesale;
	}

	public void setWholesale(String wholesale) {
		this.wholesale = wholesale;
	}

	public String getSundayDeliveryFlag() {
		return sundayDeliveryFlag;
	}

	public void setSundayDeliveryFlag(String sundayDeliveryFlag) {
		this.sundayDeliveryFlag = sundayDeliveryFlag;
	}

	public String getFolIndicator() {
		return folIndicator;
	}

	public void setFolIndicator(String folIndicator) {
		this.folIndicator = folIndicator;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getShippingTaxAmount() {
		return shippingTaxAmount;
	}

	public void setShippingTaxAmount(String shippingTaxAmount) {
		this.shippingTaxAmount = shippingTaxAmount;
	}

	public String getOrderItemNumber() {
		return orderItemNumber;
	}

	public void setOrderItemNumber(String orderItemNumber) {
		this.orderItemNumber = orderItemNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getItemSourceCode() {
		return itemSourceCode;
	}

	public void setItemSourceCode(String itemSourceCode) {
		this.itemSourceCode = itemSourceCode;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getSecondDeliveryDate() {
		return secondDeliveryDate;
	}

	public void setSecondDeliveryDate(String secondDeliveryDate) {
		this.secondDeliveryDate = secondDeliveryDate;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getProductSubcodeId() {
		return productSubcodeId;
	}

	public void setProductSubcodeId(String productSubcodeId) {
		this.productSubcodeId = productSubcodeId;
	}

	public String getFirstColorChoice() {
		return firstColorChoice;
	}

	public void setFirstColorChoice(String firstColorChoice) {
		this.firstColorChoice = firstColorChoice;
	}

	public String getSecondColorChoice() {
		return secondColorChoice;
	}

	public void setSecondColorChoice(String secondColorChoice) {
		this.secondColorChoice = secondColorChoice;
	}

	public String getItemOfTheWeekFlag() {
		return itemOfTheWeekFlag;
	}

	public void setItemOfTheWeekFlag(String itemOfTheWeekFlag) {
		this.itemOfTheWeekFlag = itemOfTheWeekFlag;
	}

	public String getOccassion() {
		return occassion;
	}

	public void setOccassion(String occassion) {
		this.occassion = occassion;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getSizeIndicator() {
		return sizeIndicator;
	}

	public void setSizeIndicator(String sizeIndicator) {
		this.sizeIndicator = sizeIndicator;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getRetailVariablePrice() {
		return retailVariablePrice;
	}

	public void setRetailVariablePrice(String retailVariablePrice) {
		this.retailVariablePrice = retailVariablePrice;
	}

	public String getDiscountedProductPrice() {
		return discountedProductPrice;
	}

	public void setDiscountedProductPrice(String discountedProductPrice) {
		this.discountedProductPrice = discountedProductPrice;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(String serviceFee) {
		this.serviceFee = serviceFee;
	}

	public String getDropShipCharges() {
		return dropShipCharges;
	}

	public void setDropShipCharges(String dropShipCharges) {
		this.dropShipCharges = dropShipCharges;
	}

	/*
	 * public String getTaxAmount() { return taxAmount; } public void
	 * setTaxAmount(String taxAmount) { this.taxAmount = taxAmount; } public
	 * List<Tax> getTaxes() { return taxes; } public void setTaxes(List<Tax>
	 * taxes) { this.taxes = taxes; }
	 */
	public BigDecimal getAddOnAmount() {
		return addOnAmount;
	}

	public void setAddOnAmount(BigDecimal addOnAmount) {
		this.addOnAmount = addOnAmount;
	}

	public String getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(String orderTotal) {
		this.orderTotal = orderTotal;
	}

	public int getMilesPoints() {
		return milesPoints;
	}

	public void setMilesPoints(int milesPoints) {
		this.milesPoints = milesPoints;
	}

	public String getPersonalGreetingId() {
		return personalGreetingId;
	}

	public void setPersonalGreetingId(String personalGreetingId) {
		this.personalGreetingId = personalGreetingId;
	}

	public String getBinSourceChanged() {
		return binSourceChanged;
	}

	public void setBinSourceChanged(String binSourceChanged) {
		this.binSourceChanged = binSourceChanged;
	}

	public String getRecipFirstName() {
		return recipFirstName;
	}

	public void setRecipFirstName(String recipFirstName) {
		this.recipFirstName = recipFirstName;
	}

	public String getRecipLastName() {
		return recipLastName;
	}

	public void setRecipLastName(String recipLastName) {
		this.recipLastName = recipLastName;
	}

	public String getRecipAddress1() {
		return recipAddress1;
	}

	public void setRecipAddress1(String recipAddress1) {
		this.recipAddress1 = recipAddress1;
	}

	public String getRecipAddress2() {
		return recipAddress2;
	}

	public void setRecipAddress2(String recipAddress2) {
		this.recipAddress2 = recipAddress2;
	}

	public String getRecipCity() {
		return recipCity;
	}

	public void setRecipCity(String recipCity) {
		this.recipCity = recipCity;
	}

	public String getRecipState() {
		return recipState;
	}

	public void setRecipState(String recipState) {
		this.recipState = recipState;
	}

	public String getRecipPostalCode() {
		return recipPostalCode;
	}

	public void setRecipPostalCode(String recipPostalCode) {
		this.recipPostalCode = recipPostalCode;
	}

	public String getRecipCountry() {
		return recipCountry;
	}

	public void setRecipCountry(String recipCountry) {
		this.recipCountry = recipCountry;
	}

	public String getRecipPhone() {
		return recipPhone;
	}

	public void setRecipPhone(String recipPhone) {
		this.recipPhone = recipPhone;
	}

	public String getRecipPhoneExt() {
		return recipPhoneExt;
	}

	public void setRecipPhoneExt(String recipPhoneExt) {
		this.recipPhoneExt = recipPhoneExt;
	}

	public String getQmsResultCode() {
		return qmsResultCode;
	}

	public void setQmsResultCode(String qmsResultCode) {
		this.qmsResultCode = qmsResultCode;
	}

	public String getShipToType() {
		return shipToType;
	}

	public void setShipToType(String shipToType) {
		this.shipToType = shipToType;
	}

	public String getShipToTypeName() {
		return shipToTypeName;
	}

	public void setShipToTypeName(String shipToTypeName) {
		this.shipToTypeName = shipToTypeName;
	}

	public String getCardMessagespecialInstructions() {
		return cardMessagespecialInstructions;
	}

	public void setCardMessagespecialInstructions(
			String cardMessagespecialInstructions) {
		this.cardMessagespecialInstructions = cardMessagespecialInstructions;
	}

	public String getOrderComments() {
		return orderComments;
	}

	public void setOrderComments(String orderComments) {
		this.orderComments = orderComments;
	}

	public String getFlorist() {
		return florist;
	}

	public void setFlorist(String florist) {
		this.florist = florist;
	}

	public AddressVerification getAddressVerification() {
		return addressVerification;
	}

	public void setAddressVerification(AddressVerification addressVerification) {
		this.addressVerification = addressVerification;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getMorningDeliveryFee() {
		return morningDeliveryFee;
	}

	public void setMorningDeliveryFee(String morningDeliveryFee) {
		this.morningDeliveryFee = morningDeliveryFee;
	}

	public Taxes getTaxes() {
		return taxes;
	}

	public void setTaxes(Taxes taxes) {
		this.taxes = taxes;
	}
	
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the addOns
	 */
	public AddOns getAddOns() {
		return addOns;
	}

	/**
	 * @param addOns the addOns to set
	 */
	public void setAddOns(AddOns addOns) {
		this.addOns = addOns;
	}

	/**
	 * @return the addOnDiscountAmount
	 */
	public BigDecimal getAddOnDiscountAmount() {
		return addOnDiscountAmount;
	}

	/**
	 * @param addOnDiscountAmount the addOnDiscountAmount to set
	 */
	public void setAddOnDiscountAmount(BigDecimal addOnDiscountAmount) {
		this.addOnDiscountAmount = addOnDiscountAmount;
	}

	/**
	 * @return legacyID
	 */
	public String getLegacyID() {
		return legacyID;
	}

	/**
	 * @param legacyID
	 */
	public void setLegacyID(String legacyID) {
		this.legacyID = legacyID;
	}

	/**
	 * @return
	 */
	public String getFuelSurcharge() {
		return fuelSurcharge;
	}

	/**
	 * @param fuelSurcharge
	 */
	public void setFuelSurcharge(String fuelSurcharge) {
		this.fuelSurcharge = fuelSurcharge;
	}
	
}
