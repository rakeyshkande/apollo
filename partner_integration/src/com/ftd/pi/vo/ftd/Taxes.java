package com.ftd.pi.vo.ftd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.ftd.pi.partner.vo.Order;
import com.ftd.tax.service.client.dto.TotalTax;

@XmlAccessorType(XmlAccessType.FIELD)
public class Taxes {

        protected List<Tax> tax;
        
        @XmlElement(name = "total_tax", required = true)
        protected TotalTax totalTax;
        
        @XmlElement(name = "tax-service-performed", required = true)
        protected String taxServicePerformed;


        public List<Tax> getTax() {
            if (tax == null) {
                tax = new ArrayList<Tax>();
            }
            return this.tax;
        }

        /**
         * Gets the value of the totalTax property.
         * 
         * @return
         *     possible object is
         *     {@link Order.Items.Item.Taxes.TotalTax }
         *     
         */
        public TotalTax getTotalTax() {
            return totalTax;
        }

        /**
         * Sets the value of the totalTax property.
         * 
         * @param value
         *     allowed object is
         *     {@link Order.Items.Item.Taxes.TotalTax }
         *     
         */
        public void setTotalTax(TotalTax value) {
            this.totalTax = value;
        }

        /**
         * Gets the value of the taxServicePerformed property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxServicePerformed() {
            return taxServicePerformed;
        }

        /**
         * Sets the value of the taxServicePerformed property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxServicePerformed(String value) {
            this.taxServicePerformed = value;
        }

}
