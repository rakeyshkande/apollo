
package com.ftd.pi.adapters;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.AddressTypeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.OccasionHandler;
import com.ftd.osp.utilities.order.dao.RecalculateOrderDAO;
import com.ftd.osp.utilities.order.vo.AddressTypeVO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.TaxRequestVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.pi.dao.CommonUtilsDAO;
import com.ftd.pi.dao.PartnerMappingDAO;
import com.ftd.pi.dao.PartnerOrderItemDAO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.formatters.PartnerDataFormatter;
import com.ftd.pi.partner.vo.BillingAddress;
import com.ftd.pi.partner.vo.BillingData;
import com.ftd.pi.partner.vo.FulFillmentAddress;
import com.ftd.pi.partner.vo.FulfillmentData;
import com.ftd.pi.partner.vo.ItemPrice;
import com.ftd.pi.partner.vo.Name;
import com.ftd.pi.partner.vo.Order;
import com.ftd.pi.partner.vo.OrderItem;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerOrderUtils;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.PartnerMappingVO;
import com.ftd.pi.vo.ftd.AddOn;
import com.ftd.pi.vo.ftd.AddOns;
import com.ftd.pi.vo.ftd.FTDOrder;
import com.ftd.pi.vo.ftd.Header;
import com.ftd.pi.vo.ftd.Item;
import com.ftd.pi.vo.ftd.Tax;
import com.ftd.pi.vo.ftd.Taxes;
import com.ftd.tax.service.client.dto.TotalTax;

/**
 * @author skatam
 *
 */
public class FTDOrderAdapter 
{
	private static final String TRANSFORM_STATE_CODES = "TRANSFORM_STATE_CODES";
	private static final String ORDER_GATHERER_CONFIG = "ORDER_GATHERER_CONFIG"; 
	private static final String PARTNER_DATE_RANGE_CONFIG = "PARTNER_DATE_RANGE_CONFIG";
	private static final String TILT_SEPARATOR = "|";
	private static final String COLON_SEPARATOR = ":";
	
	private Logger logger = new Logger(FTDOrderAdapter.class.getName());
	
	private Connection conn = null;
	
	private PartnerMappingDAO partnerMappingDAO;
	private CommonUtilsDAO commonUtilsDAO;
	private PartnerOrderItemDAO partnerOrderItemDAO; 
	
	public FTDOrderAdapter(Connection conn) {
		this.conn = conn;
		partnerMappingDAO = new PartnerMappingDAO();
		commonUtilsDAO = new CommonUtilsDAO(conn);
		partnerOrderItemDAO = new PartnerOrderItemDAO(conn); 
	}
		
	public FTDOrder getAsFTDOrder(Order partnerOrder) 
	{
		FTDOrder ftdOrder = null;
		ftdOrder = this.transformToFTDOrder(partnerOrder);
		return ftdOrder;
	}
	
	private void populateHeader(Order partnerOrder, Header header, PartnerMappingVO partnerMappingVO)
	{
		Date dt = null;
		if(partnerOrder.getOrderDate() != null){
			dt = PartnerUtils.fromXMLGregorianCalendar(partnerOrder.getOrderDate()).getTime();
		} else{
			dt = new Date();
		}
		
		java.sql.Date orderDate = new java.sql.Date(dt.getTime());
		SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		header.setTransactionDate(format.format(orderDate));
		header.setSocketTimestamp(format.format(new java.util.Date()));
	    
    
        //Origin
		header.setOrigin(partnerMappingVO.getFtdOriginMapping());
	    String sourceCode = partnerMappingVO.getDefaultSourceCode();
	    logger.debug("Source code from PartnerMapping :" +sourceCode);
	    header.setSourceCode(sourceCode);
	  
	    //Generate the master order number
	    String masterOrderNumber = partnerMappingVO.getMasterOrderNoPrefix()+
	  								StringUtils.replaceChars(partnerOrder.getPartnerOrderID(),"-","");
	    header.setMasterOrderNumber(masterOrderNumber);
	
	    header.setNewsLetterFlag(PartnerUtils.getPIConfigProperty("news_letter_flag"));
	    header.setCcType(PartnerUtils.getPIConfigProperty("cc_type"));
		  
		this.setBuyerDetails(partnerOrder, header);
			
	}

	private FTDOrder transformToFTDOrder(Order partnerOrder)
	{
		FTDOrder ftdOrder = new FTDOrder();
		
		Header header = new Header();
		try 
		{		
			String partnerId = partnerOrder.getPartnerId().toUpperCase();
			PartnerMappingVO partnerMappingVO = partnerMappingDAO.getPIPartnerMapping(partnerId);
			this.populateHeader(partnerOrder, header, partnerMappingVO);

		Date dt = null;
		if(partnerOrder.getOrderDate() != null){
			dt = PartnerUtils.fromXMLGregorianCalendar(partnerOrder.getOrderDate()).getTime();
		} else{
			dt = new Date();
		}
		
		java.sql.Date orderDate = new java.sql.Date(dt.getTime());
		String sourceCode = header.getSourceCode();
		
        ftdOrder.setHeader(header);
        
        BigDecimal apollo_discountValue = new BigDecimal("0");
        BigDecimal apollo_itemDiscount = new BigDecimal("0");
	    String apollo_discountType = "";
	    String apollo_sourceCode = "";
	        
        
        //Now do the order items
        BigDecimal totalItemProdPrice = new BigDecimal("0");
        BigDecimal totalDiscount = new BigDecimal("0");
        BigDecimal totalShipping = new BigDecimal("0");
        BigDecimal totalTax = new BigDecimal("0");
        BigDecimal totalShippingTax = new BigDecimal("0");
        BigDecimal totalAddOnPrice = new BigDecimal("0");
        
        List<OrderItem> orderItems = partnerOrder.getOrderItem();
        
        for (OrderItem orderItem : orderItems) 
        {
        	Item ftdOrderItem = new Item();
            String partnerOrderItemID = orderItem.getPartnerOrderItemID();
            if( partnerOrderItemID==null) 
            {
              throw new PartnerException("No Partner order item code can be found for this item");  
            }
            
            ftdOrderItem.setOrderItemNumber(partnerOrderItemID);
            
            //IOTW source code
            String sku = orderItem.getSKU();
            if( sku==null) 
            {
            	sku = "";
            	//throw new PartnerException("No SKU found for order item");  
            }
            sku = sku.toUpperCase();
            String skuDeluxePremium = sku;
            
            /*
            if(sku.indexOf(PIConstants.CATALOG_SUFFIX_DELUXE) >= 0 
            		|| sku.indexOf(PIConstants.CATALOG_SUFFIX_PREMIUM) >= 0){
          	  sku = sku.substring(0, 4);
            }
            */
            
            if(sku.endsWith(PIConstants.SKU_SUFFIX_STANDARD)){
            	sku = sku.substring(0, sku.length()- PIConstants.SKU_SUFFIX_STANDARD.length());
            } else if(sku.endsWith(PIConstants.SKU_SUFFIX_DELUXE)){
            	sku = sku.substring(0, sku.length()- PIConstants.SKU_SUFFIX_DELUXE.length());
            } else if(sku.endsWith(PIConstants.SKU_SUFFIX_PREMIUM)){
            	sku = sku.substring(0, sku.length()- PIConstants.SKU_SUFFIX_PREMIUM.length());
            }
            
            //Product id (always send the novator id)
            
            
            //Get skuId from the novtorId
            sku = partnerOrderItemDAO.getSKUIdFromNovatorId(sku);
            ftdOrderItem.setProductid(sku);
            //ftdOrderItem.setQuantity(String.valueOf(orderItem.getQuantity()));
            ftdOrderItem.setQuantity("1");
            
            String dateReceived = new SimpleDateFormat("dd-MMM-yy").format(orderDate);
            Map<String,Object> iotwMap = commonUtilsDAO.getIOTWSourceCode(sourceCode, sku, dateReceived);
            
            apollo_sourceCode = (String) iotwMap.get("iotwSourceCode");
            apollo_discountValue = (BigDecimal) iotwMap.get("discount");
            apollo_discountType = (String) iotwMap.get("discountType");
            logger.debug("Apollo Discount info - sourceCode: " + apollo_sourceCode + " discountType: " + apollo_discountType +" discount: " + apollo_discountValue);
            //End IOTW source code
                        
            
            //look up the price in FTD_APPS.PRODUCT_MASTER
            logger.debug("Getting pdb data");
            BigDecimal pdb_price = new BigDecimal("0");
            Map<String,Object> pdbPriceMap = commonUtilsDAO.getPdbPriceData(sku);
            String shipMethodFlorist = (String) pdbPriceMap.get("shipMethodFlorist");
            if (shipMethodFlorist == null) shipMethodFlorist = "N";
            String shipMethodCarrier = (String) pdbPriceMap.get("shipMethodCarrier");
            if (shipMethodCarrier == null) shipMethodCarrier = "N";
            
            // get pdb_price
            pdb_price=getPdbPrice(pdbPriceMap, sku,skuDeluxePremium,orderDate);
            logger.debug("##pdb_price: "+pdb_price);
            
            FulfillmentData fulfillmentData = orderItem.getFulfillmentData();
            FulFillmentAddress address = fulfillmentData.getAddress();
            
            //Get Delivery date
            String shipMethod = "";
            Date deliveryDate = null;
            Date deliveryStartDate = this.getFormatedDeliveryDate(orderItem.getDeliveryStartDate());
            Date deliveryEndDate = this.getFormatedDeliveryDate(orderItem.getDeliveryEndDate());
            
            ConfigurationUtil configUtil = new ConfigurationUtil();
			
			String partnerDateRangeCheck = configUtil.getFrpGlobalParm(PIConstants.PI_GLOBAL_CONTEXT,PARTNER_DATE_RANGE_CONFIG);
            
            if(deliveryStartDate != null) {
            	deliveryDate = deliveryStartDate;            	
            	if(deliveryEndDate != null) {
	                //Not including the date range as both the dates are equal or delivery end date is earlier than start date 
	                if(deliveryStartDate.compareTo(deliveryEndDate) == 0 || deliveryStartDate.compareTo(deliveryEndDate) > 0){
	                	deliveryEndDate = null;
	                }                 
            	}
            } 
            
            if(orderItem.getCustomFieldValue("LegacyID") != null){
            	ftdOrderItem.setLegacyID(orderItem.getCustomFieldValue("LegacyID"));
            }
            
            String occasion = this.processOccasion(orderItem);
	        ftdOrderItem.setOccassion(occasion);
	        
            try {
            	AddOns includedAddons = PartnerOrderUtils.getAddOns(orderItem); 
            	AddOns freeAddons = this.processFreeAddons(ftdOrderItem.getProductid(), sourceCode, occasion, shipMethodFlorist, shipMethodCarrier, includedAddons);
            	ftdOrderItem.setAddOns(new AddOns());
            	if(includedAddons != null) {
            		
            		for(AddOn includedAddon : includedAddons.getAddOn())
            	    {
            		  ftdOrderItem.getAddOns().getAddOn().add(includedAddon);
            	    }
            	}
            	if(freeAddons != null) {
            	    for(AddOn freeAddon : freeAddons.getAddOn())
            	    {
            		  ftdOrderItem.getAddOns().getAddOn().add(freeAddon);
            	    }
            	}
	        }
            catch(Exception e){
            	logger.error("Error occurred while setting the Addons, sending system message." , e);
            	PartnerUtils.sendNoPageSystemMessage("Unable to set the free/included addons for product id: "+orderItem.getSKU()+" on the order: "+orderItem.getPartnerOrderItemID()+". Error: "+e.getMessage());
            }
            
			List<String> addonList = new ArrayList<String>();
			if (ftdOrderItem.getAddOns() != null && ftdOrderItem.getAddOns().getAddOn() != null) {
				List<AddOn> partnerAddOnList = ftdOrderItem.getAddOns().getAddOn();
				for (AddOn partnerAddOn : partnerAddOnList) {
					addonList.add(partnerAddOn.getId());
					logger.info("Addon id: "+partnerAddOn.getId());
				}
			}
			logger.info("sourceCode::"+sourceCode);
            if(deliveryDate != null)
            {
              String zipCode = address.getPostalCode();
              String country = address.getCountry();
          	  shipMethod = PASServiceUtil.getShipMethod(sku, deliveryDate, zipCode, country, shipMethodFlorist, addonList, sourceCode); 
          	  ftdOrderItem.setDeliveryDate(new SimpleDateFormat("MM/dd/yyyy").format(deliveryDate));
            }
            else
            {
				String zipCode = address.getPostalCode();
				String country = address.getCountry();
				if (zipCode.length() > 5) {
					zipCode = zipCode.substring(0, 5);
				}
				
				com.ftd.osp.utilities.vo.PartnerMappingVO utilMappingVO = partnerMappingDAO.getUtilPartnerMapping(partnerId);
				Date nextAvailableDate = PASServiceUtil.getPartnerNextAvailableDate(utilMappingVO, sku, zipCode, country, sourceCode, 30, shipMethodFlorist, addonList);
						 
				if (nextAvailableDate != null) {
					shipMethod = PASServiceUtil.getShipMethod(sku, nextAvailableDate, zipCode, country, shipMethodFlorist, addonList, sourceCode);
				}

				logger.info("Delivery date from pas service: "+ nextAvailableDate);
				if (nextAvailableDate != null) {
					ftdOrderItem.setDeliveryDate(
							new SimpleDateFormat("MM/dd/yyyy").format(nextAvailableDate));
				} else {
					ftdOrderItem.setDeliveryDate(null);
				}				
            }
            
            if(partnerDateRangeCheck.contains(partnerId) && deliveryStartDate !=null && deliveryEndDate != null){
				ftdOrderItem.setSecondDeliveryDate(new SimpleDateFormat("MM/dd/yyyy").format(deliveryEndDate));
			}else{
				ftdOrderItem.setSecondDeliveryDate(null);
			}
            
          //shipping-method
            logger.info("shipMethod: " + shipMethod + " shipMethodFlorist: " + shipMethodFlorist +
          		  " shipMethodCarrier: " + shipMethodCarrier);
            if (shipMethod != null) {
            		ftdOrderItem.setShippingMethod(shipMethod);
            } else {
	              ftdOrderItem.setShippingMethod("");
            }
            
            BigDecimal itemRetailPrice = new BigDecimal("0");
            BigDecimal itemSalePrice = new BigDecimal("0");           
            
            BigDecimal itemDiscount = new BigDecimal("0");
            BigDecimal itemShipping = new BigDecimal("0");
            BigDecimal itemTax = new BigDecimal("0");            
            BigDecimal commission = new BigDecimal("0");
            BigDecimal itemAddOnPrice = new BigDecimal("0");
            
            boolean isSalePriceIncluded = false;
            
            //Get the dollar amounts of the four price elements
            ItemPrice itemPrice = orderItem.getItemPrice();
            if(itemPrice != null) {
            	itemRetailPrice = PartnerUtils.getSafeBigDecimal(itemPrice.getItemRetailPrice());
            	itemSalePrice = PartnerUtils.getSafeBigDecimal(itemPrice.getItemSalePrice());
            	if(itemSalePrice.compareTo(BigDecimal.ZERO) > 0){            		
            		isSalePriceIncluded = true;
            	} else if(itemRetailPrice.compareTo(BigDecimal.ZERO) == 0) {
            		logger.warn("Neither Retail Price nor Sale Price is included in the order for  item number - " + partnerOrderItemID);            		
            	}
                itemShipping = PartnerUtils.getSafeBigDecimal(itemPrice.getItemShippingServiceFeeTotal());
                itemTax = PartnerUtils.getSafeBigDecimal(itemPrice.getItemTaxTotal());              
                //defect#132 - if discount code is not present in source table, default to partner source code
                boolean isValidDiscountCode = commonUtilsDAO.isSourceCodeExist(itemPrice.getItemDiscountCode());
                String itemSouceCode = (isValidDiscountCode==true)?itemPrice.getItemDiscountCode():sourceCode;
                ftdOrderItem.setItemSourceCode(itemSouceCode);
            }
            
           
            apollo_itemDiscount = this.calculateItemDiscount(apollo_discountType, apollo_discountValue, pdb_price, sourceCode);
            itemDiscount = (itemRetailPrice.compareTo(itemSalePrice) > 0)?itemRetailPrice.subtract(itemSalePrice):itemDiscount;
			
			BigDecimal itemProdPrice = new BigDecimal("0"); // Item product price or retail price (sale price + discount)
	        BigDecimal itemDisProdPrice = new BigDecimal("0"); // Item discounted product price or sale price. (retail price - discount)
	        
	        if(isSalePriceIncluded) {
	        	itemDisProdPrice = itemSalePrice;
	        	itemProdPrice = itemSalePrice.add(itemDiscount); // Item discount will be 0 is no discount applied.
	        } else {
	        	itemProdPrice = itemRetailPrice;
	        	itemDisProdPrice = itemRetailPrice.subtract(itemDiscount);
	        }
	        
			logger.info("Partner Prices for item number - " + partnerOrderItemID + " are \n ProductPrice :: " 
					+ itemProdPrice + " Discount :: " + itemDiscount + " DiscountedProductPrice :: " + itemDisProdPrice);
			
			//Price and Discount variance			
            if(itemProdPrice.compareTo(pdb_price) != 0 ) {
            	logger.warn("Partner sent Item product price :: " + itemProdPrice + " and pdb product price :: " + pdb_price +
            			" are not same for item number - " + partnerOrderItemID);            	            	
            }
            if(apollo_itemDiscount.compareTo(itemDiscount) != 0 ) {
            	logger.warn("Partner sent Item discount :: " + itemDiscount + " and pdb item discount :: " + apollo_itemDiscount + 
            			" are not same for item number - " + partnerOrderItemID);              	            	
            }
            
            // Populate addon details
            //ftdOrderItem.setAddOns(PartnerOrderUtils.getAddOns(orderItem));
            ftdOrderItem.setAddOnAmount(PartnerOrderUtils.getAddOnAmount(ftdOrderItem));
            ftdOrderItem.setAddOnDiscountAmount((PartnerOrderUtils.getAddOnDiscountAmount(ftdOrderItem)));
            
            itemAddOnPrice = (ftdOrderItem.getAddOnAmount() != null ? ftdOrderItem.getAddOnAmount() : itemAddOnPrice);
            
            //Increment the totals
            totalItemProdPrice = totalItemProdPrice.add(itemProdPrice);
            totalDiscount = totalDiscount.add(itemDiscount);
            totalShipping = totalShipping.add(itemShipping);
            totalTax = totalTax.add(itemTax);
            totalAddOnPrice = totalAddOnPrice.add(ftdOrderItem.getAddOnAmount());
            
            //Add the item amounts
            ftdOrderItem.setOrderTotal(itemDisProdPrice.add(itemShipping).add(itemTax).add(itemAddOnPrice).toString());
            ftdOrderItem.setTaxAmount(itemTax.toString());
            ftdOrderItem.setServiceFee(itemShipping.toString());
            
           
            if(itemDiscount != null && itemDiscount.compareTo(BigDecimal.ZERO) > 0) { 
	            logger.debug("XML discount-amount: " + itemDiscount.toString());
	            ftdOrderItem.setDiscountAmount(itemDiscount.toString());	            
	            
	            logger.debug("XML discount-type: " + apollo_discountType);
	            ftdOrderItem.setDiscountType(apollo_discountType);
            }
            
            //Commission
            String commissionStr = null;
            if(itemPrice != null && itemPrice.getCommission() != null)
            {
	            commissionStr = itemPrice.getCommission().toString();
	            if( commissionStr.length()>0 ) {
	              commission = new BigDecimal(commissionStr);
	              if (commission.compareTo(BigDecimal.ZERO) > 0){
	            	  commission.multiply(new BigDecimal(-1));
	              }
	            }
	            ftdOrderItem.setCommission(commission.toString());
            }
          	
            //Product price
            ftdOrderItem.setProductPrice(itemProdPrice.toString());
            logger.debug("XML product-price:" + itemProdPrice.toString());
            
            ftdOrderItem.setPdbPrice(pdb_price.toString());
            logger.debug("XML pdb-price: " + pdb_price.toString());
            
            ftdOrderItem.setDiscountedProductPrice(itemDisProdPrice.toString());
            logger.debug("XML discounted-product-price: " + itemDisProdPrice.toString());
            
            ftdOrderItem.setTransaction(itemProdPrice.add(itemShipping).add(itemTax).add(itemAddOnPrice).toString());
            ftdOrderItem.setWholesale(itemProdPrice.add(itemShipping).add(itemTax).add(itemAddOnPrice).subtract(commission) .toString());
          
	       
	        
	        if(orderItem.getGiftWrapData() != null)
	        {
	        	ftdOrderItem.setCardMessagespecialInstructions(orderItem.getGiftWrapData().getGiftWrapMessage());
	        }
	        
	        this.setFulfillmentData(partnerOrder,orderItem, ftdOrderItem);
	        String morningDelivery = StringUtils.trimToEmpty(fulfillmentData.getMorningDelivery());
	        logger.info("morningDelivery Flag: "+morningDelivery);
	        if("Y".equalsIgnoreCase(morningDelivery))
	        {
	        	RecalculateOrderDAO recalculateOrderDAO = new RecalculateOrderDAO();
	        	try {
	        		logger.debug("orderDate: "+orderDate+", sourceCode: "+sourceCode);
					String morningDeliveryrFee = recalculateOrderDAO.getDeliveryFeeData(conn, orderDate, sourceCode);
					logger.info("MorningDelivery Fee: "+morningDeliveryrFee);
					ftdOrderItem.setMorningDeliveryFee(morningDeliveryrFee);
				} catch (Exception e) {
					logger.error("", e);
					ftdOrderItem.setMorningDeliveryFee("0.0");
				}
	        } 
	        
	        ftdOrderItem.setFuelSurcharge(commonUtilsDAO.getSourceCodeSurcharge(partnerMappingVO.getDefaultSourceCode()));   
	        
	        String addrTypeCode = null;
	        String deliveryLocationType = orderItem.getDeliveryLocationType();
	        if(StringUtils.isBlank(deliveryLocationType)){
	        	addrTypeCode = PartnerUtils.getPIConfigProperty("ship_to_type");
	        } else {
	        	AddressTypeHandler addrTypeHandler = (AddressTypeHandler) CacheManager.getInstance().getHandler("CACHE_NAME_ADDRESS");
		        @SuppressWarnings("rawtypes")
				Map addrTypeMap = addrTypeHandler.getAddressTypeVOMap();
		        if(addrTypeMap.containsKey(deliveryLocationType.toUpperCase())){
		        	AddressTypeVO addressTypeVO = (AddressTypeVO) addrTypeMap.get(deliveryLocationType.toUpperCase());
		        	addrTypeCode = addressTypeVO.getCode();
		        } else {
		        	addrTypeCode = PartnerUtils.getPIConfigProperty("ship_to_type");
		        }
	        }
	        ftdOrderItem.setShipToType(addrTypeCode);
	        
	        ftdOrderItem.setSpecialInstructions(orderItem.getSpecialInstructions());
	        ftdOrderItem.setFolIndicator(PartnerUtils.getPIConfigProperty("fol_indicator"));
	        ftdOrderItem.setSundayDeliveryFlag(partnerMappingVO.getSunDeliveryAllowedFloral());
	        ftdOrderItem.setSenderReleaseFlag(PartnerUtils.getPIConfigProperty("sender_release_flag"));
	        ftdOrderItem.setProductSubstitutionAcknowledgement(PartnerUtils.getPIConfigProperty("product_sub_ack"));
	        
	       
	        ftdOrderItem.setLegacyID(orderItem.getCustomFieldValue("LegacyID"));
	        	
	        
	        
	        this.setTaxSplit(itemTax.toString(), ftdOrderItem);
            ftdOrder.getItems().add(ftdOrderItem);
	        
		} //End Item loop
        
        
        //Add the order count in
        header.setOrderCount(String.valueOf(orderItems.size()));
        
        //Add the order amount
        header.setOrderAmount((totalItemProdPrice.subtract(totalDiscount)).add(totalShipping).add(totalTax).add(totalShippingTax).add(totalAddOnPrice) .toString());
        
        //Add the discount total
        if( totalDiscount.compareTo(BigDecimal.ZERO) >0 )
        {
        	header.setDiscountTotal(totalDiscount.toString());
        }
        
        //Finally put in the confirmation numbers
        //We hold off on doing this till now to prevent gaps in sequence numbers because of errors
        this.setConformationNumber(ftdOrder, partnerMappingVO.getConfNumberPrefix());
        
        //populate joint cart indicator if exists in original order XML.
	    header.setJointCartIndicator(partnerOrder.getJointCart() != null ? partnerOrder.getJointCart().value() : null);  
        
		}
		catch (Exception e) 
		{
			throw new PartnerException(e);
		}
		return ftdOrder;
	}

	private AddOns processFreeAddons(String productId, String sourceCode,
			String occassion, String shipMethodFlorist,
			String shipMethodCarrier, AddOns includedAddOns) {
		int occassionId = 0;
		AddOns allfreeAddons = null;
		List<AddOn> addonList = null;

		try {
			occassionId = Integer.parseInt(occassion);
		} catch (Exception e) {
			logger.error("getExtraProductDetail - Occasion ID is non-numeric: "
					+ occassion + ", using default occassion '0'");
		}
		
		try {
			Map<String, ArrayList<AddOnVO>> activeVaseAddonMap = commonUtilsDAO
					.getActiveAddonList(productId, occassionId, sourceCode,
							false, shipMethodFlorist, shipMethodCarrier);

			if (activeVaseAddonMap != null && activeVaseAddonMap.size() > 0) {
				List<AddOnVO> allAvailvases = activeVaseAddonMap
						.get(AddOnVO.ADD_ON_VO_VASE_KEY);
				List<AddOnVO> allAvailaddOns = activeVaseAddonMap
						.get(AddOnVO.ADD_ON_VO_ADD_ON_KEY);

				// get free vase and add-on from all the available addons
				AddOnVO freeVase = this.getFreeVase(allAvailvases);
				List<AddOnVO> freeAddOns = this.getFreeAddons(allAvailaddOns);

				// if no free addons exists
				if (freeVase == null
						&& (freeAddOns == null || freeAddOns.size() < 1)) {
					return null;
				}

				allfreeAddons = new AddOns();
				// if order has addd-ons included
				addonList = new ArrayList<AddOn>();

				if (includedAddOns != null && includedAddOns.getAddOn() != null
						&& includedAddOns.getAddOn().size() > 0) {
					logger.info("Addons exists on the orders");
					// Add free vase only when it is not already included and
					// there is no other vase on item
					if (freeVase != null
							&& !isAddonAlreadyIncluded(includedAddOns, freeVase)
							&& !isAnotherVaseIncluded(includedAddOns)) {
						addonList.add(convertToFTDFreeAddon(freeVase));
					}
					// Add all the free addons only when it is not already
					// included
					if(freeAddOns!=null && freeAddOns.size()>0){
						for (AddOnVO addon : freeAddOns) {
							if (addon != null
									&& !isAddonAlreadyIncluded(includedAddOns,
											addon)) {
								addonList.add(convertToFTDFreeAddon(addon));
							}
						}
					}
				} else {
					logger.info("No addons exists on the order");
					if (freeVase != null) {
						logger.info("Free vase :" + freeVase.getAddOnId());
						addonList.add(convertToFTDFreeAddon(freeVase));
					}
					if (freeAddOns != null && freeAddOns.size() > 0) {
						for (AddOnVO addon : freeAddOns) {
							logger.info("Free addon :" + addon.getAddOnId());
							addonList.add(convertToFTDFreeAddon(addon));
						}
					}
				}
				allfreeAddons.setAddOn(addonList);
				return allfreeAddons;
			 }
			
		} catch (Exception e) {
			logger.error("Error caught obtaining free vase and free addon ", e);
		}
		return allfreeAddons;
	}

	private boolean isAddonAlreadyIncluded(AddOns includedAddOns,
			AddOnVO freeAddon) {
		// check if addon already included
		for (AddOn addOn : includedAddOns.getAddOn()) {
			if (addOn.getId().equalsIgnoreCase(freeAddon.getAddOnId())) {
				return true;
			}
		}

		return false;
	}

	private boolean isAnotherVaseIncluded(AddOns includedAddOns) {
		for (AddOn addOn : includedAddOns.getAddOn()) {
			try {
				AddOnVO addOnVO = commonUtilsDAO.getAddOnById(addOn.getId());
				if (AddOnVO.ADD_ON_TYPE_VASE_ID.equals(Integer.parseInt(addOnVO
						.getAddOnTypeId()))) {
					return true;
				}
			} catch (Exception e) {
				logger.error(
						"Unable to check if another vase exists on item, ", e);
			}
		}
		return false;
	}

	private AddOn convertToFTDFreeAddon(AddOnVO itemAddon) {

		AddOn ftdAddOn = new AddOn();
		ftdAddOn.setId(itemAddon.getAddOnId());
		ftdAddOn.setPrice(new BigDecimal("0.00"));
		ftdAddOn.setDiscountAmount(new BigDecimal("0.00"));
		if (itemAddon.getOrderQuantity() != null) {
			ftdAddOn.setQuantity(itemAddon.getOrderQuantity());
		} else
			ftdAddOn.setQuantity(1);
		return ftdAddOn;
	}

	private AddOnVO getFreeVase(List<AddOnVO> allAvailvases) {
		logger.info("**********getFreeVase()**********");
		if (allAvailvases != null && allAvailvases.size() > 0) {
			Map<Integer, AddOnVO> freeVases = new HashMap<Integer, AddOnVO>();

			for (AddOnVO vaseAddon : allAvailvases) {
				if (vaseAddon != null
						&& vaseAddon.getAddOnPrice() != null
						&& new BigDecimal(vaseAddon.getAddOnPrice())
								.compareTo(BigDecimal.ZERO) == 0) {
					try {
						freeVases.put(Integer.parseInt(vaseAddon
								.getDisplaySequenceNumber()), vaseAddon);
						logger.info("AddOnId: " + vaseAddon.getAddOnId());
					} catch (NumberFormatException e) {
						logger.error(
								"Displaysequence number not found/not a number",
								e);
					}
				}
			}

			logger.info("size is : " + freeVases.size());

			if (freeVases.size() == 0) {
				return null;
			} else if (freeVases.size() == 1) {
				for (AddOnVO vase : freeVases.values()) {
					logger.info("Free Addon id: " + vase.getAddOnId());
					return vase;
				}
			} else if (freeVases.size() > 1) {
				int min = freeVases.keySet().iterator().next();
				for (AddOnVO vase : freeVases.values()) {
					if (Integer.parseInt(vase.getDisplaySequenceNumber()) == min) {
						logger.info("Free Addon id : " + vase.getAddOnId());
						return vase;
					}
				}
				
			}
		}
		return null;
	}

	private List<AddOnVO> getFreeAddons(List<AddOnVO> allAvailaddOns) {
		logger.info("**********getFreeAddons()**********");
		List<AddOnVO> freeAddons = null;
		if (allAvailaddOns != null && allAvailaddOns.size() > 0) {
			freeAddons = new ArrayList<AddOnVO>();

			for (AddOnVO addon : allAvailaddOns) {
				if (addon != null
						&& addon.getAddOnPrice() != null
						&& new BigDecimal(addon.getAddOnPrice())
								.compareTo(BigDecimal.ZERO) == 0) {
					freeAddons.add(addon);
					logger.info("AddonId: " + addon.getAddOnId());
				}
			}
		}
		return freeAddons;
	}
	
	private void setTaxSplit(String itemTax, Item ftdOrderItem) {
		
		Taxes taxes = new Taxes();
     	ftdOrderItem.setTaxes(taxes);
     	
     	ItemTaxVO itemTaxVO = null;
     	TotalTax totalTaxVO = new TotalTax();
        BigDecimal totalTaxRate = null; 
        BigDecimal itemTaxAmt = null;
        BigDecimal totalTax = null;
        String defaultTaxRate = null;
        CalculateTaxUtil taxUtil = new CalculateTaxUtil();
        TaxRequestVO taxRequestVO = null;
        String companyId = "FTD";
        
		try {
			
			if(StringUtils.isEmpty(itemTax) || new BigDecimal(itemTax).compareTo(BigDecimal.ZERO) == 0) {
				logger.info("The order has no tax on item, " + ftdOrderItem.getOrderItemNumber());
				return;
			}
	        taxRequestVO = new TaxRequestVO();
	        
	        RecipientAddressesVO recipientVO = new RecipientAddressesVO();
	        recipientVO.setAddressLine1(ftdOrderItem.getRecipAddress1());
	        recipientVO.setAddressLine2(ftdOrderItem.getRecipAddress2());
	        recipientVO.setCity(ftdOrderItem.getRecipCity());
	        recipientVO.setCountry((ftdOrderItem.getRecipCountry().equalsIgnoreCase("USA"))? "US" : ftdOrderItem.getRecipCountry());
	        recipientVO.setStateProvince(ftdOrderItem.getRecipState());
	        recipientVO.setPostalCode(ftdOrderItem.getRecipPostalCode());
	        taxRequestVO.setRecipientAddress(recipientVO);
	        taxRequestVO.setCompanyId(companyId);
	        taxRequestVO.setConfirmationNumber(ftdOrderItem.getOrderItemNumber());
	        taxRequestVO.setShipMethod(ftdOrderItem.getShippingMethod());
	        taxRequestVO.setTaxableAmount(getTaxableAmount(ftdOrderItem));
	        
	        itemTaxVO = taxUtil.calculateTaxRates(taxRequestVO);
	               	
	        if(itemTaxVO == null || (itemTaxVO != null && itemTaxVO.getTotalTaxrate() == null)) {
        		throw new Exception("Cannot calculate tax split for the item");  
        	}
	        
	        totalTaxRate = itemTaxVO.getTotalTaxrate(); 
	        totalTax = new BigDecimal(itemTax);
	        
	        if(itemTaxVO.getTaxSplit() != null && itemTaxVO.getTaxSplit().size() >= 0) {
	        	
	        	// When there is split in response.
	        	for (TaxVO taxVO : itemTaxVO.getTaxSplit()) {	        		
	        		Tax tax = new Tax();
	        		if(taxVO != null && taxVO.getRate() != null) {
	        			tax.setType(taxVO.getName());
	        			tax.setDescription(taxVO.getDescription());
	        			tax.setRate(taxVO.getRate());	 
	        			
	        			itemTaxAmt = taxVO.getRate().multiply(totalTax).divide(totalTaxRate, new MathContext(50));
	        			itemTaxAmt = itemTaxAmt.setScale(5, BigDecimal.ROUND_HALF_UP);	        			
	        			
	        			tax.setAmount(itemTaxAmt);     			
	        			taxes.getTax().add(tax);
	        		}	        		
				}
	        	
	        	// finally set total rate
	        	totalTaxVO.setTotalAmount(totalTax);
	        	totalTaxVO.setDescription(itemTaxVO.getTotalTaxDescription());
	        	totalTaxVO.setTotalRate(totalTaxRate);
	        	taxes.setTotalTax(totalTaxVO);	
	        	taxes.setTaxServicePerformed("Y");
	        } 
	        

		} catch(Exception e){
			logger.error("Exception caught calculating tax rates for partner order, putting the tax to total bucket, " + ftdOrderItem.getOrderItemNumber());
			logger.info(e);        		
        	totalTaxVO.setTotalAmount(new BigDecimal(itemTax));
        	totalTaxVO.setDescription(taxUtil.getDefaultTaxLabel(ftdOrderItem.getRecipCountry(), companyId));
        	try {
				defaultTaxRate = taxUtil.reverseCalcTaxRate(getTaxableAmount(ftdOrderItem), new BigDecimal(itemTax)).toString();		
			} catch (Exception e1) {
				logger.error("Error caught getting default tax rate applying rate as 5%, " + e.getMessage());
				defaultTaxRate = "0.05";
			}
        	totalTaxVO.setTotalRate(new BigDecimal(defaultTaxRate));
        	taxes.setTotalTax(totalTaxVO);
        	taxes.setTaxServicePerformed("N");
			return;
		}
		
	}

	private BigDecimal getTaxableAmount(Item lineItem) {
		BigDecimal taxableAmount = new BigDecimal(0);
		
		if(!StringUtils.isEmpty(lineItem.getProductPrice())) {
			taxableAmount = taxableAmount.add(new BigDecimal(lineItem.getProductPrice()));			
		}
		
		if(lineItem.getAddOnAmount() != null) {
			taxableAmount = taxableAmount.add(lineItem.getAddOnAmount());			
		}
		
		if(!StringUtils.isEmpty(lineItem.getServiceFee())) {
			taxableAmount = taxableAmount.add(new BigDecimal(lineItem.getServiceFee()));			
		}
		
		if(!StringUtils.isEmpty(lineItem.getDiscountAmount())) {
			taxableAmount = taxableAmount.subtract(new BigDecimal(lineItem.getDiscountAmount()));			
		}
		return taxableAmount;
	}

	@SuppressWarnings("unchecked")
	private String processOccasion(OrderItem orderItem)
	{
		String occasionId = null;
		String occasion = StringUtils.trimToEmpty(orderItem.getOccasion());
        boolean validOccasion = false;
        OccasionHandler ocassionHandler = (OccasionHandler)CacheManager.getInstance().getHandler("CACHE_NAME_OCCASION");
        Map<Object,String> occasionIdMap = ocassionHandler.getOccasionIdMap();
        Set<Object> occasionIds = occasionIdMap.keySet();
        for (Object id : occasionIds) {
        	if(occasion.equals(occasionIdMap.get(id))){
        		occasionId = String.valueOf(id);
        		validOccasion = true;
        		break;
        	}
		}
        if(!validOccasion)
        {
        	occasionId = PartnerUtils.getPIConfigProperty("occasion");
        }
        return occasionId;
	}
	private void setBuyerDetails(Order partnerOrder,Header header)
	{
		try 
		{
			BillingData billingData = partnerOrder.getBillingData();
			String partnerOrderNumber = partnerOrder.getPartnerOrderID();
			
			Name buyerNameData = billingData.getName();
			String buyerFirstName = PartnerDataFormatter.formatName(partnerOrderNumber, buyerNameData.getFirstName(), "Buyer") ;
			String buyerLastName = PartnerDataFormatter.formatName(partnerOrderNumber,buyerNameData.getLastName(),"Buyer");
			String buyerBusinessName = PartnerDataFormatter.formatName(partnerOrderNumber,buyerNameData.getBusinessName(),"Buyer");
			logger.info(" *****buyerFirstName: " + buyerFirstName);
			logger.info(" *****buyerLastName: " + buyerLastName);
			
			header.setBuyerFirstName(buyerFirstName);
			header.setBuyerLastName(buyerLastName);
			header.setBuyerBusinessName(buyerBusinessName);

			// Buyer Daytime Phone
			String buyerDaytimePhoneNumber = "";
			String buyerEveningPhoneNumber = "";
			if(billingData.getPhoneNumber() != null)
			{
				if(StringUtils.isBlank(billingData.getPhoneNumber().getDayTimePhone())){
					buyerDaytimePhoneNumber = "999-999-9999";
				}
				else{
					buyerDaytimePhoneNumber = StringUtils.trimToEmpty(billingData.getPhoneNumber().getDayTimePhone());
				}				
				buyerEveningPhoneNumber = StringUtils.trimToEmpty(billingData.getPhoneNumber().getEveningPhone());
			}
			else{
				buyerDaytimePhoneNumber = "999-999-9999";
			}
			buyerDaytimePhoneNumber = PartnerDataFormatter.formatPhoneNumber(partnerOrderNumber, buyerDaytimePhoneNumber, "Buyer");
			buyerEveningPhoneNumber = PartnerDataFormatter.formatPhoneNumber(partnerOrderNumber, buyerEveningPhoneNumber,"Buyer");
			
			header.setBuyerDaytimePhone(buyerDaytimePhoneNumber);
			header.setBuyerEveningPhone(buyerEveningPhoneNumber);			
			
			boolean prefixSequence = false;
			BillingAddress billingAddress = billingData.getAddress();
			PartnerMappingVO partnerMappingVO = partnerMappingDAO.getPIPartnerMapping(partnerOrder.getPartnerId());
			if(billingAddress == null){
				billingAddress = partnerMappingVO.getBillingAddress().getAsBillingAddress();
				prefixSequence = true;
			}else { 
			if(StringUtils.isBlank(billingAddress.getAddress1())) {
				prefixSequence = true;
				billingAddress.setAddress1(partnerMappingVO.getBillingAddress().getAsBillingAddress().getAddress1());	
			}if(StringUtils.isBlank(billingAddress.getAddress2())) {
				billingAddress.setAddress2(partnerMappingVO.getBillingAddress().getAsBillingAddress().getAddress2());
			}if(StringUtils.isBlank(billingAddress.getCity())) {
				billingAddress.setCity(partnerMappingVO.getBillingAddress().getAsBillingAddress().getCity());
			}if(StringUtils.isBlank(billingAddress.getState())) {
				billingAddress.setState(partnerMappingVO.getBillingAddress().getAsBillingAddress().getState());
			}if(StringUtils.isBlank(billingAddress.getPostalCode())) {
				billingAddress.setPostalCode(partnerMappingVO.getBillingAddress().getAsBillingAddress().getPostalCode());
			}if(StringUtils.isBlank(billingAddress.getCountry())) {
				billingAddress.setCountry(partnerMappingVO.getBillingAddress().getAsBillingAddress().getCountry());
			 }
			}
			BillingAddress formattedBillingAddress = PartnerDataFormatter.formatAddress(conn, partnerOrderNumber, billingAddress,"Buyer");
			if(prefixSequence){
				// Buyer Address 1 is updated with the sequence number from database.
				String buyerSequence = commonUtilsDAO.getPartnerBuyerSequence();
				header.setBuyerAddress1(buyerSequence+"-"+formattedBillingAddress.getAddress1());
			} else {
				header.setBuyerAddress1(formattedBillingAddress.getAddress1());
			}
			header.setBuyerAddress2(formattedBillingAddress.getAddress2());
			header.setBuyerCity(formattedBillingAddress.getCity());
			header.setBuyerState(replaceInvalidRecipientState(formattedBillingAddress.getState()));
			header.setBuyerPostalCode(formattedBillingAddress.getPostalCode());
			header.setBuyerCountry(formattedBillingAddress.getCountry());
			
			// Buyer Email
			String buyerEmailAddress = billingData.getEmailAddress();
			header.setBuyerEmailAddress(buyerEmailAddress);

		} catch (Exception e) {
			throw new PartnerException(e.getCause());
		}
		
	}

	/** Convenient method to replace the recipient state code with the valid state code mapped in DB.
	 * @param state
	 * @return
	 */
	private String replaceInvalidRecipientState(String state) {
		if(logger.isDebugEnabled()) {
			logger.debug("replaceInvalidRecipientState()");
		}
		
		if(StringUtils.isEmpty(state) || StringUtils.trim(state).length() <= 0) {
			if(logger.isDebugEnabled()) {
				logger.debug("replaceInvalidRecipientState(), state cannot be null");
			}
			return state;
		}
		 
		try {
			ConfigurationUtil config = new ConfigurationUtil();			
			String validStatesMapping = config.getFrpGlobalParm(ORDER_GATHERER_CONFIG, TRANSFORM_STATE_CODES);
		
			if(logger.isDebugEnabled()) {
				logger.debug("validStatesMapping:" + validStatesMapping);
			}
			
			StringTokenizer tokenizeStates = new StringTokenizer(validStatesMapping, TILT_SEPARATOR);	
			String validStateMapping = null;
			 
			while (tokenizeStates.hasMoreElements()) {
				validStateMapping = (String) tokenizeStates.nextElement();				
				if(state.equalsIgnoreCase(validStateMapping.split(COLON_SEPARATOR)[0])) {
					state = StringUtils.trim(validStateMapping.split(COLON_SEPARATOR)[1]);
					if(logger.isDebugEnabled()) {
						logger.debug("Replaced with the state:" + state);
					} 
					
					break;
				}
			}
		} catch (Exception e) { 
			logger.error("Error caught checking if the recipient state: " + state + " has to be replaced: ", e);
		}
		return state;
	}
	
	private void setFulfillmentData(Order partnerOrder,OrderItem orderItem ,Item ftdOrderItem)
	{
		String partnerOrderNumber = partnerOrder.getPartnerOrderID();
		FulfillmentData fulfillmentData = orderItem.getFulfillmentData();
		String recipientFirstName = PartnerDataFormatter.formatName(partnerOrderNumber, fulfillmentData.getName().getFirstName(),"Recipient");
		String recipientLastName = PartnerDataFormatter.formatName(partnerOrderNumber, fulfillmentData.getName().getLastName(),"Recipient");
		String recipientBusinessName = PartnerDataFormatter.formatName(partnerOrderNumber, fulfillmentData.getName().getBusinessName(),"Recipient");

        ftdOrderItem.setRecipFirstName(recipientFirstName);
        ftdOrderItem.setRecipLastName(recipientLastName);
        ftdOrderItem.setShipToTypeName(recipientBusinessName);
        
        FulFillmentAddress address = fulfillmentData.getAddress();
        FulFillmentAddress formatAddress = PartnerDataFormatter.formatAddress(conn, partnerOrderNumber, address,"Recipient");
        ftdOrderItem.setRecipAddress1(formatAddress.getAddress1());
        ftdOrderItem.setRecipAddress2(formatAddress.getAddress2());
        ftdOrderItem.setRecipCity(formatAddress.getCity());
        ftdOrderItem.setRecipState(replaceInvalidRecipientState(formatAddress.getState()));
        ftdOrderItem.setRecipPostalCode(formatAddress.getPostalCode());
        ftdOrderItem.setRecipCountry(formatAddress.getCountry());
        
        String recipientPhone = "";
        if(fulfillmentData.getPhoneNumber().getDayTimePhone() != null){
        	recipientPhone = PartnerDataFormatter.formatPhoneNumber(partnerOrderNumber, fulfillmentData.getPhoneNumber().getDayTimePhone(),"Recipient");
        } else if(fulfillmentData.getPhoneNumber().getEveningPhone() != null){
        	recipientPhone = PartnerDataFormatter.formatPhoneNumber(partnerOrderNumber, fulfillmentData.getPhoneNumber().getEveningPhone(),"Recipient");
        } else {
        	recipientPhone = PartnerDataFormatter.formatPhoneNumber(partnerOrderNumber, recipientPhone,"Recipient");
        }
        ftdOrderItem.setRecipPhone(recipientPhone);
	    
	}
	
	private BigDecimal getPdbPrice(Map<String,Object> pdbPriceMap,String sku,String skuDeluxePremium,java.sql.Date orderDate) throws ParseException
	{
		BigDecimal pdb_price=new BigDecimal(0);  
		SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		
		if(skuDeluxePremium.endsWith(PIConstants.SKU_SUFFIX_DELUXE)){
	              if(pdbPriceMap.get("deluxePrice")!=null){
	            	  		pdb_price = (BigDecimal) pdbPriceMap.get("deluxePrice");
	            	  		logger.debug("Using deluxePrice: "+pdb_price);
	            	  }
	          }
	          else if(skuDeluxePremium.endsWith(PIConstants.SKU_SUFFIX_PREMIUM)){
	           	  if(pdbPriceMap.get("premiumPrice")!=null){
	           		  		pdb_price = (BigDecimal) pdbPriceMap.get("premiumPrice");
	           		  		logger.debug("Using premiumPrice: "+pdb_price);
	            	  }
	          }
	          else{
	           	  if(pdbPriceMap.get("standardPrice")!=null){
	           		  		pdb_price = (BigDecimal) pdbPriceMap.get("standardPrice");
	           		  		logger.debug("Using standardPrice: "+pdb_price);
	            	  }
	          }
         
         if(pdb_price.compareTo(BigDecimal.ZERO) ==0){ //get historical pdb data
	              Map<String,Object> productPriceMap = commonUtilsDAO.getProductByTimestamp(sku, format.parse(format.format(orderDate)));
	              if(skuDeluxePremium.endsWith(PIConstants.SKU_SUFFIX_DELUXE)){
	            	  if(productPriceMap.get("deluxePrice")!=null){
	            		  pdb_price = (BigDecimal) productPriceMap.get("deluxePrice");
	            		  logger.debug("Using deluxePrice: "+pdb_price);
	            	  }
	              }
	              else if(skuDeluxePremium.endsWith(PIConstants.SKU_SUFFIX_PREMIUM)){
	            	  if(productPriceMap.get("premiumPrice")!=null){
	            		  pdb_price = (BigDecimal) productPriceMap.get("premiumPrice");
	            		  logger.debug("Using premiumPrice: "+pdb_price);
	            	  }
	              }
	              else{
	            	  if(productPriceMap.get("standardPrice")!=null){
	            		  pdb_price = (BigDecimal) productPriceMap.get("standardPrice");
	            		  logger.debug("Using standardPrice: "+pdb_price);
	            	  }
	              }
         }
		
		return pdb_price;
	}
	
	private BigDecimal calculateItemDiscount(String apollo_discountType, 
											BigDecimal apollo_discountValue,
											BigDecimal pdb_price,
											String sourceCode)
	{
		BigDecimal itemDiscount=new BigDecimal(0);
		
		if( apollo_discountValue.compareTo(BigDecimal.ZERO)==0.0 ) 
         {
           itemDiscount= new BigDecimal("0");
         }
         else
         {
           if( StringUtils.equals("P",apollo_discountType) ) //Percentage discount
           {
             //Compute the undiscounted price.  What the next line does:
             //1.  Subtract the discount from 100
             //2.  Divide the pricicipal by the result of #1 by 100 for the undiscounted percentage 
             //3.  Multiply the amount of the principal by the undiscounted percentage giving you total price unrounded
             //4.  Multiply it by 100 to get the 1/100 position to the right of the decimal point
             //5.  Round off to the nearest decimal (aka 5/4 rounding)
             //6.  Divide the result of #5 by 100
             
             //Need to do this to get rid of the trailing non-significant digits
           	BigDecimal hundreths = new BigDecimal(0.01);
           	BigDecimal productPrice = pdb_price.setScale(2, BigDecimal.ROUND_DOWN);
           	BigDecimal discountPrice = productPrice.multiply(apollo_discountValue).multiply(hundreths);
           	discountPrice = discountPrice.setScale(2, BigDecimal.ROUND_DOWN);
           	
             
             itemDiscount = discountPrice;
             logger.debug("##for discount type P - - itemDiscount: "+itemDiscount);
          //   isDiscountApplied = true;
           }
           else if( StringUtils.equals("D",apollo_discountType) ) //Dollar discount
           {
             itemDiscount = apollo_discountValue;
             
             logger.debug("##for discount type D -  - itemDiscount: "+itemDiscount);
           //  isDiscountApplied = true;
           }
           else 
           {
             throw new PartnerException("Partner source code " + sourceCode + "is configured with an unsupported discount type of " + apollo_discountType );
           }
         }
		
		return itemDiscount;
	}
	
	private void setConformationNumber(FTDOrder ftdOrder,String confNumberPrefix)
	{
		List<Item> items = ftdOrder.getItems();
        logger.debug("No of Items in FTD Order XML: "+items.size());
        for (Item item : items) 
        {
        	logger.debug("SKU: "+item.getProductid());
        	String confNumber = partnerOrderItemDAO.getConfirmationNumber(confNumberPrefix);
        	item.setOrderNumber(confNumber);
		}
	}
	
	private Date getFormatedDeliveryDate(XMLGregorianCalendar dateInput) throws Exception 
	{
		Date deliveryDate = null;
		//XMLGregorianCalendar deliveryStartDate = orderItem.getDeliveryStartDate();
		if(dateInput != null)
		{
			logger.debug("Given Delivery Date: "+ dateInput.toString());
			Calendar deliveryCal = PartnerUtils.fromXMLGregorianCalendar(dateInput);
	        deliveryDate = deliveryCal.getTime();
		}		
		logger.debug("Delivery Date from Partner: "+deliveryDate);
		return deliveryDate;		
	}
}
