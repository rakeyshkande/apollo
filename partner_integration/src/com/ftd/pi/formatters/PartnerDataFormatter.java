/**
 * 
 */
package com.ftd.pi.formatters;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.dao.CommonUtilsDAO;
import com.ftd.pi.exceptions.PartnerException;
import com.ftd.pi.partner.vo.BillingAddress;
import com.ftd.pi.partner.vo.FulFillmentAddress;
import com.ftd.pi.utils.PartnerUtils;
import com.ftd.pi.vo.Address;

/**
 * @author skatam
 *
 */
public class PartnerDataFormatter 
{
	private static Logger logger = new Logger(PartnerDataFormatter.class.getName());
	

	public static String formatPhoneNumber(String partnerOrderNumber, String phone, String customerType)
	{
		String phoneNumber = StringUtils.trimToEmpty(phone);
		phoneNumber = PartnerUtils.removeAllSpecialChars(phoneNumber);
		if (phoneNumber.length() > 10) {
			logger.debug("Partner Order Number: "+ partnerOrderNumber
					+ "\n"+customerType+" Phone Number too long: " + phoneNumber);
			phoneNumber = phoneNumber.substring(0, 10);
		} else if (phoneNumber.length() < 10) {
			phoneNumber = "0000000000" + phoneNumber;
			phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
			logger.debug(customerType+" PhoneNumber too short, changed to: "+ phoneNumber);
		}
		return phoneNumber;
	}
	
	public static  String formatName(String partnerOrderNumber, String name, String customerType)
	{
		if(StringUtils.isBlank(name))
		{
			name = "NA";
		}
		if (name.length() > 20) {
			logger.debug("Partner Order Number: " + partnerOrderNumber 
					+ "\n"+customerType+" Name too long: "+ name);
			name = name.substring(0, 20);
		}
		return name;
	}
	public static BillingAddress formatAddress(Connection conn, String partnerOrderNumber, BillingAddress billingAddress, String customerType)
	{
		return formatAddress(conn, 
							partnerOrderNumber, 
							Address.getAsAddress(billingAddress), 
							customerType)
						.getAsBillingAddress();
	}
	public static FulFillmentAddress formatAddress(Connection conn, String partnerOrderNumber, FulFillmentAddress fulFillmentAddress, String customerType)
	{
		return formatAddress(conn, 
					partnerOrderNumber, 
					Address.getAsAddress(fulFillmentAddress), 
					customerType)
				.getAsFulFillmentAddress();
	}
	public static Address formatAddress(Connection conn, String partnerOrderNumber, Address address, String customerType)
	{
		Address formattedAddress = new Address();
		String addressOne = address.getAddress1(); 
        String addressTwo = address.getAddress2(); 
        String newAddressOne = addressOne;
        if (newAddressOne == null) newAddressOne = "";
        String newAddressTwo = addressTwo;
        if (newAddressTwo == null) newAddressTwo = "";
        
        if (newAddressOne.length() > 45) {
        	String tempAddress = newAddressOne.substring(45, newAddressOne.length());
        	if (newAddressTwo.length() > 0) tempAddress = tempAddress.concat("; ");
        	newAddressTwo = tempAddress.concat(newAddressTwo);
        	newAddressOne = newAddressOne.substring(0, 45);
        }
        if (newAddressTwo.length() > 45) {
        	logger.debug("Partner Order Number: " + partnerOrderNumber + 
         		"\n"+customerType+" Address2 too long: " + newAddressOne + "\n" + newAddressTwo);
        	newAddressTwo = newAddressTwo.substring(0, 45);
        }
        formattedAddress.setAddress1(newAddressOne);
        formattedAddress.setAddress2(newAddressTwo);
        
        //Recipient City
        String recipientCity = address.getCity(); 
        if (recipientCity == null) recipientCity = "";
        if (recipientCity.length() > 30) {
        	logger.debug("Partner Order Number: " + partnerOrderNumber + 
         		"\n"+customerType+" City too long: " + recipientCity);
         recipientCity = recipientCity.substring(0, 30);
        }
        formattedAddress.setCity(recipientCity);
        
        //Recipient State
        String recipientState = address.getState(); 
        if (recipientState == null) recipientState = "";
        CacheUtil cacheUtil = CacheUtil.getInstance();
        logger.info("checking State: " + recipientState);
        StateMasterVO stateVO=null;
		try {
			stateVO = cacheUtil.getStateById(recipientState);
		} catch (Exception e) {
			throw new PartnerException(e.getCause());
		}
        if (stateVO == null) {
        	logger.info("Not in cache, checking by name");
        	stateVO = new CommonUtilsDAO(conn).getStateByName(recipientState);
        	if (stateVO != null && stateVO.getStateMasterId() != null) {
        		logger.debug("found State: " + stateVO.getStateMasterId());
        		recipientState = stateVO.getStateMasterId();
        	}
        }
        if (recipientState.length() > 10) {
        	logger.debug("Partner Order Number: " + partnerOrderNumber + 
         		"\n"+customerType+" State too long: " + recipientState);
         recipientState = recipientState.substring(0, 10);
        }
        formattedAddress.setState(recipientState);
        
        //Recipient Postal
        String recipientZip = address.getPostalCode(); 
        if (recipientZip == null) recipientZip = "";
        recipientZip = recipientZip.replaceAll(" ", "");
        recipientZip = recipientZip.replaceAll("-", "");
        if (recipientZip.length() > 9) {
        	logger.debug("Partner Order Number: " + partnerOrderNumber + 
         		"\n"+customerType+" Zip too long: " + recipientZip);
         recipientZip = recipientZip.substring(0, 9);
        }
        formattedAddress.setPostalCode(recipientZip);
        
        //Recipient Country
        String recipientCountry = address.getCountry();
        if (recipientCountry == null) recipientCountry = "";
        formattedAddress.setCountry(recipientCountry);
        
		return formattedAddress;
	}
}
