package com.ftd.pi.mdb;

import static com.ftd.pi.utils.PIConstants.ORDER_FEED_STATUS_JMS_ERROR;
import static com.ftd.pi.utils.PIConstants.PROCESS_INBOUND_ORDER;
import static com.ftd.pi.utils.PIConstants.PROCESS_JMS_ERROR_ORDERS;
import static com.ftd.pi.utils.PIConstants.PROCESS_OG_ERROR_ORDERS;
import static com.ftd.pi.utils.PIConstants.PROCESS_PARTNER_ORDERS;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pi.bo.PartnerFeedBO;
import com.ftd.pi.bo.PartnerOrderFulfillBO;
import com.ftd.pi.bo.PartnerOrderAdjFeedBO;
import com.ftd.pi.bo.PartnerOrderStatusUpdateBO;
import com.ftd.pi.bo.PartnerOrderInvoiceBO;
import com.ftd.pi.services.PartnerOrderService;
import com.ftd.pi.utils.PIConstants;
import com.ftd.pi.utils.PartnerUtils;

public class PartnerMDB implements MessageDrivenBean, MessageListener 
{
	private static final long serialVersionUID = 1L;
	private MessageDrivenContext context;
	private Logger logger;
	private static String logContext = "com.ftd.pi.mdb.PartnerMDB";

	public void ejbCreate() {}

	public void ejbRemove() {}

	public void setMessageDrivenContext(MessageDrivenContext ctx) 
	{
		this.context = ctx;
		logger = new Logger(logContext);
		logger.debug("MessageDrivenContext initialized :"+this.context);
	}

	public void onMessage(Message msg) 
	{		
		if(logger.isDebugEnabled()) {
			logger.debug("**********PartnerMDB.onMessage()***************");
		}
		
		try 
		{
			TextMessage textMessage = (TextMessage) msg;
			String msgText = textMessage.getText();
			String action = msg.getJMSCorrelationID();
			
			String[] params = null;
			if(msgText != null) {
				params = msgText.split("\\|");
			}			
			if(params != null && params.length > 0) {
				String[] tempVariables = params[0].split("\\:");
				if(tempVariables != null && tempVariables.length > 0 && tempVariables[0].trim().equals("action")) {
					action = tempVariables[1];
				}
			}	
			
			logger.info("action: " + action);
			logger.info("msgText: " + msgText);
			
			if (action == null || action.equals("")) 
			{
				logger.error("Invalid action");
			}
			else if (action.equalsIgnoreCase(PROCESS_PARTNER_ORDERS)) 
			{
				logger.debug("***************PROCESS_PARTNER_ORDERS****************");
				PartnerOrderService orderService = new PartnerOrderService();
				orderService.processPartnerOrderFeed(msgText);
			}
			else if (action.equalsIgnoreCase(PROCESS_INBOUND_ORDER)) 
			{
				logger.debug("***************PROCESS_INBOUND_ORDER****************");
				PartnerOrderService orderService = new PartnerOrderService();
				//msgText = order.getPartnerId()+":"+partnerOrderNumber
				String[] strings = msgText.split(":");
				String partnerId = strings[0];
				String partnerOrderNumber = strings[1];
				int retryCount = Integer.parseInt(strings[2]);
				orderService.processPartnerOrder(partnerId,partnerOrderNumber,retryCount);
			}
			else if (action.equalsIgnoreCase(PROCESS_OG_ERROR_ORDERS)) 
			{
				logger.debug("***************PROCESS_OG_ERROR_ORDERS****************");
				PartnerOrderService orderService = new PartnerOrderService();
				orderService.processOGErrorOrders();
			}
			else if (action.equalsIgnoreCase(PROCESS_JMS_ERROR_ORDERS)) 
			{
				logger.debug("***************PROCESS_JMS_ERROR_ORDERS****************");
				PartnerOrderService orderService = new PartnerOrderService();
				orderService.processPartnerOrderFeedByStatus(ORDER_FEED_STATUS_JMS_ERROR);
			}
			// Create partner order adjustment records by processing the refunds on partner orders.
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_PTN_REFUNDS_ORDERS)) {
				logger.info("Request received to create refund/adjustment records for partner orders: " + msgText);
				new PartnerOrderAdjFeedBO().processCreateRefundAdjustments(PartnerUtils.getPartner(msgText));
			}
			// Create order adjustment feeds when there is new partner order adjustment records.
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_CREATE_ORD_ADJ_FEEDS)) {
				logger.debug("Request received to create partner order adjustment feeds: " + msgText);
				new PartnerOrderAdjFeedBO().createPartnerFeed(PartnerUtils.getPartner(msgText));
			}
			// Create FL order fulfillments by checking the mercury confirmed partner orders.
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_CREATE_FLORIST_FULFILL_PTN_DATA)) {				
				logger.info("Request received to create Florist fulfilled Partner Orders into partner fulfillment tables: " + msgText);
				new PartnerOrderFulfillBO().processFloristFulfilledOrders(PartnerUtils.getPartner(msgText));
				
				//To Create Ship Notice Requests for Floral Orders   
				new PartnerOrderStatusUpdateBO().createPartnerOrdStatusUpdate(PIConstants.SHIPPED_TYPE, null);
				
			}
			// Create order fulfilment feeds or delivery confirmation feeds when there is a new partner order fulfillment record.
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_CREATE_ORD_FULFILL_FEEDS)) {
				logger.info("Request received to create partner order fulfillment records for partner orders: " + msgText);
				new PartnerOrderFulfillBO().createPartnerFeed(PartnerUtils.getPartner(msgText));				
			}
			// Send the partner order adjustments to ESB
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_SEND_PARTNER_ADJ_FEEDS)) {
				logger.info("Request received to send partner order adjustment feeds to ESB");
				new PartnerFeedBO().sendPartnerFeed(PIConstants.ADJUSTMENT_TYPE);
			}
			// send the partner order fulfillments to ESB.
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_SEND_PARTNER_FULFILL_FEEDS)) {
				logger.info("Request received to send partner order fulfillment feeds to ESB");
				new PartnerFeedBO().sendPartnerFeed(PIConstants.FULFILLMENT_TYPE);		
			}
			// send the partner delivery confirmation feeds to ESB.
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_SEND_DELIVERY_CONFIRMATION_FEEDS)) {
				logger.info("Request received to send partner order delivery confirmation feeds to ESB");
				new PartnerFeedBO().sendPartnerFeed(PIConstants.DELIVERTY_CONFIRMATION_TYPE);		
			}	
            // Create order status update feeds (used for Ariba partners)
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_CREATE_ORDER_STATUS_UPDATES)) {
				logger.info("Request received to create partner order status feeds, " + msgText);
				
				String orderNumbers = null;
				String operation = null; 
				
				for (String param : params) {
					if(param.indexOf("orderNumbers") >= 0) {						
						if(param.split("\\:").length == 2) { 
							orderNumbers = param.split("\\:")[1].trim();
						}			  
					}					 
					if(param.indexOf("operation") >= 0) {
						if(param.split("\\:").length == 2) { 
							operation = param.split("\\:")[1].trim();
						} 												
					}									
				}				 
				if(operation == null || operation.trim().length() == 0) {							
					throw new Exception("Operation cannot be null, invalid request");
				} 
				new PartnerOrderStatusUpdateBO().createPartnerOrdStatusUpdate(operation.toUpperCase(), orderNumbers);
			}
			// Send order status update feeds to ESB (used for Ariba partners)
			else if (action.equalsIgnoreCase(PIConstants.PROCESS_SEND_ORDER_STATUS_UPDATES)) {
				logger.info("Request received to send order status updates to ESB");
				new PartnerFeedBO().sendPartnerFeed(PIConstants.ORD_STATUS_UPD);		
			}
            // Create invoice feeds (used for Ariba partners)
            else if (action.equalsIgnoreCase(PIConstants.PROCESS_INVOICE_READY_ORDERS)) {
                logger.info("Request received to create invoice records for certain partner orders (e.g., Ariba) : " + msgText);
                new PartnerOrderInvoiceBO().createInvoiceFeed();                
            }
            // Send invoice feeds (used for Ariba partners) to ESB.
            else if (action.equalsIgnoreCase(PIConstants.PROCESS_SEND_INVOICE_READY_ORDERS)) {
                logger.info("Request received to send invoice feeds to ESB (only for certain partners - e.g., Ariba): " + msgText);
                new PartnerFeedBO().sendPartnerFeed(PIConstants.INVOICE_TYPE);       
            }
			else 
			{
				logger.error("Invalid action: " + action);
			}

			logger.info("Finished");
		}
		catch (Exception e) 
		{
			logger.error(e);
		} catch (Throwable t) 
		{
			logger.error("Thrown error: " + t);
		}
	}

}