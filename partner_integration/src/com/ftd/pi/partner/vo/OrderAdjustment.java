//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.11 at 03:19:02 PM IST 
//


package com.ftd.pi.partner.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FTDOrderID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{}OrderItemAdjustment" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PartnerOrderID" use="required" type="{}OrderIdType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "partnerId",
    "ftdOrderID",
    "orderItemAdjustment"
})
@XmlRootElement(name = "OrderAdjustment")
public class OrderAdjustment {

    @XmlElement(name = "PartnerId", required = true)
    protected String partnerId;
    @XmlElement(name = "FTDOrderID")
    protected String ftdOrderID;
    @XmlElement(name = "OrderItemAdjustment", required = true)
    protected List<OrderItemAdjustment> orderItemAdjustment;
    @XmlAttribute(name = "PartnerOrderID", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String partnerOrderID;

    /**
     * Gets the value of the partnerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * Sets the value of the partnerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerId(String value) {
        this.partnerId = value;
    }

    /**
     * Gets the value of the ftdOrderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFTDOrderID() {
        return ftdOrderID;
    }

    /**
     * Sets the value of the ftdOrderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFTDOrderID(String value) {
        this.ftdOrderID = value;
    }

    /**
     * Gets the value of the orderItemAdjustment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderItemAdjustment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderItemAdjustment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderItemAdjustment }
     * 
     * 
     */
    public List<OrderItemAdjustment> getOrderItemAdjustment() {
        if (orderItemAdjustment == null) {
            orderItemAdjustment = new ArrayList<OrderItemAdjustment>();
        }
        return this.orderItemAdjustment;
    }

    /**
     * Gets the value of the partnerOrderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerOrderID() {
        return partnerOrderID;
    }

    /**
     * Sets the value of the partnerOrderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerOrderID(String value) {
        this.partnerOrderID = value;
    }

}
