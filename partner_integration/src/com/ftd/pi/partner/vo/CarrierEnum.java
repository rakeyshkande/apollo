//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.12 at 03:01:38 PM IST 
//


package com.ftd.pi.partner.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CarrierEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CarrierEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FedEx"/>
 *     &lt;enumeration value="UPS"/>
 *     &lt;enumeration value="USPS"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CarrierEnum")
@XmlEnum
public enum CarrierEnum {

    @XmlEnumValue("FedEx")
    FED_EX("FedEx"),
    UPS("UPS"),
    USPS("USPS"),
    Unknown("Unknown");
    private final String value;

    CarrierEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarrierEnum fromValue(String v) {
        for (CarrierEnum c: CarrierEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
