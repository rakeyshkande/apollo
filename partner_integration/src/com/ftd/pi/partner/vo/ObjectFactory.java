//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-27 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.10 at 12:23:11 PM IST 
//


package com.ftd.pi.partner.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.pi.partner.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.pi.partner.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustomFields }
     * 
     */
    public CustomFields createCustomFields() {
        return new CustomFields();
    }

    /**
     * Create an instance of {@link Name }
     * 
     */
    public Name createName() {
        return new Name();
    }

    /**
     * Create an instance of {@link CustomFields.CustomField }
     * 
     */
    public CustomFields.CustomField createCustomFieldsCustomField() {
        return new CustomFields.CustomField();
    }

    /**
     * Create an instance of {@link OrderItem }
     * 
     */
    public OrderItem createOrderItem() {
        return new OrderItem();
    }

    /**
     * Create an instance of {@link FulfillmentData }
     * 
     */
    public FulfillmentData createFulfillmentData() {
        return new FulfillmentData();
    }

    /**
     * Create an instance of {@link FulFillmentAddress }
     * 
     */
    public FulFillmentAddress createFulFillmentAddress() {
        return new FulFillmentAddress();
    }

    /**
     * Create an instance of {@link FulFillmentPhoneNumber }
     * 
     */
    public FulFillmentPhoneNumber createFulFillmentPhoneNumber() {
        return new FulFillmentPhoneNumber();
    }

    /**
     * Create an instance of {@link GiftWrapData }
     * 
     */
    public GiftWrapData createGiftWrapData() {
        return new GiftWrapData();
    }

    /**
     * Create an instance of {@link ItemPrice }
     * 
     */
    public ItemPrice createItemPrice() {
        return new ItemPrice();
    }

    /**
     * Create an instance of {@link OrderTotals }
     * 
     */
    public OrderTotals createOrderTotals() {
        return new OrderTotals();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link BillingData }
     * 
     */
    public BillingData createBillingData() {
        return new BillingData();
    }

    /**
     * Create an instance of {@link BillingAddress }
     * 
     */
    public BillingAddress createBillingAddress() {
        return new BillingAddress();
    }

    /**
     * Create an instance of {@link BillingPhoneNumber }
     * 
     */
    public BillingPhoneNumber createBillingPhoneNumber() {
        return new BillingPhoneNumber();
    }

    /**
     * Create an instance of {@link FTDFeed }
     * 
     */
    public FTDFeed createFTDFeed() {
        return new FTDFeed();
    }
    
    /**
     * Create an instance of {@link OrderItemAdjustment }
     * 
     */
    public OrderItemAdjustment createOrderItemAdjustment() {
        return new OrderItemAdjustment();
    }

    /**
     * Create an instance of {@link OrderAdjustment }
     * 
     */
    public OrderAdjustment createOrderAdjustment() {
        return new OrderAdjustment();
    }

    /**
     * Create an instance of {@link OrderItemAdjustment.ItemPriceAdjustments }
     * 
     */
    public OrderItemAdjustment.ItemPriceAdjustments createOrderItemAdjustmentItemPriceAdjustments() {
        return new OrderItemAdjustment.ItemPriceAdjustments();
    }
    
    /**
     * Create an instance of {@link OrderFulfillment }
     * 
     */
    public OrderFulfillment createOrderFulfillment() {
        return new OrderFulfillment();
    }
}
