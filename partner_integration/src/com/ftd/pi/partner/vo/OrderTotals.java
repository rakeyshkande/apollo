//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-27 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.10 at 12:23:11 PM IST 
//


package com.ftd.pi.partner.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderTotal" type="{}Currency" minOccurs="0"/>
 *         &lt;element name="OrderRetailPriceTotal" type="{}Currency" minOccurs="0"/>
 *         &lt;element name="OrderSalePriceTotal" type="{}Currency" minOccurs="0"/>
 *         &lt;element name="OrderShippingServiceFeeTotal" type="{}Currency" minOccurs="0"/>
 *         &lt;element name="OrderTaxTotal" type="{}Currency" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderTotal",
    "orderRetailPriceTotal",
    "orderSalePriceTotal",
    "orderAddOnRetailPriceTotal", 
    "orderAddOnSalePriceTotal",
    "orderShippingServiceFeeTotal",
    "orderTaxTotal"
})
@XmlRootElement(name = "OrderTotals")
public class OrderTotals {

    @XmlElement(name = "OrderTotal")
    protected BigDecimal orderTotal;
    @XmlElement(name = "OrderRetailPriceTotal")
    protected BigDecimal orderRetailPriceTotal;
    @XmlElement(name = "OrderSalePriceTotal")
    protected BigDecimal orderSalePriceTotal;
    @XmlElement(name="OrderAddOnRetailPriceTotal")
    protected BigDecimal orderAddOnRetailPriceTotal;
    @XmlElement(name="OrderAddOnSalePriceTotal")
    protected BigDecimal orderAddOnSalePriceTotal;
    @XmlElement(name = "OrderShippingServiceFeeTotal")
    protected BigDecimal orderShippingServiceFeeTotal;
    @XmlElement(name = "OrderTaxTotal")
    protected BigDecimal orderTaxTotal;

    /**
     * Gets the value of the orderTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    /**
     * Sets the value of the orderTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderTotal(BigDecimal value) {
        this.orderTotal = value;
    }

    /**
     * Gets the value of the orderRetailPriceTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderRetailPriceTotal() {
        return orderRetailPriceTotal;
    }

    /**
     * Sets the value of the orderRetailPriceTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderRetailPriceTotal(BigDecimal value) {
        this.orderRetailPriceTotal = value;
    }

    /**
     * Gets the value of the orderSalePriceTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderSalePriceTotal() {
        return orderSalePriceTotal;
    }

    /**
     * Sets the value of the orderSalePriceTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderSalePriceTotal(BigDecimal value) {
        this.orderSalePriceTotal = value;
    }

    /**
     * Gets the value of the orderShippingServiceFeeTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderShippingServiceFeeTotal() {
        return orderShippingServiceFeeTotal;
    }

    /**
     * Sets the value of the orderShippingServiceFeeTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderShippingServiceFeeTotal(BigDecimal value) {
        this.orderShippingServiceFeeTotal = value;
    }

    /**
     * Gets the value of the orderTaxTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderTaxTotal() {
        return orderTaxTotal;
    }

    /**
     * Sets the value of the orderTaxTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderTaxTotal(BigDecimal value) {
        this.orderTaxTotal = value;
    }

	/**
	 * @return the orderAddOnRetailPriceTotal
	 */
	public BigDecimal getOrderAddOnRetailPriceTotal() {
		return orderAddOnRetailPriceTotal;
	}

	/**
	 * @return the orderAddOnSalePriceTotal
	 */
	public BigDecimal getOrderAddOnSalePriceTotal() {
		return orderAddOnSalePriceTotal;
	}

	/**
	 * @param orderAddOnRetailPriceTotal the orderAddOnRetailPriceTotal to set
	 */
	public void setOrderAddOnRetailPriceTotal(BigDecimal orderAddOnRetailPriceTotal) {
		this.orderAddOnRetailPriceTotal = orderAddOnRetailPriceTotal;
	}

	/**
	 * @param orderAddOnSalePriceTotal the orderAddOnSalePriceTotal to set
	 */
	public void setOrderAddOnSalePriceTotal(BigDecimal orderAddOnSalePriceTotal) {
		this.orderAddOnSalePriceTotal = orderAddOnSalePriceTotal;
	}

}
