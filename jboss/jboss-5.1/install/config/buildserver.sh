# Copies the TEMPLATE server into the target server

if [ $# -lt 2 ]; then
  echo Requires Parameters 'ServerName' 'PortOffset' 
  exit 1 
fi


echo Generating Server: $1 with port offset $2


if [ -d "../server/$1" ]; then
    echo Server $1 already exists
else
    echo Creating Server $1
    cp -rp ../server/TEMPLATE ../server/$1

    echo Creating Startup Script /start_$1.sh

    cp apollo_base_template ../start_$1.sh 
    echo \$JBOSS_HOME/config/apollo_base_start.sh $1 $2 >> ../start_$1.sh
    chmod +x ../start_$1.sh


    echo Creating Shutdown Script: /stop_$1.sh
    cp apollo_base_template ../stop_$1.sh
    echo \$JBOSS_HOME/config/apollo_base_stop.sh $2 >> ../stop_$1.sh
    chmod +x ../stop_$1.sh

fi

