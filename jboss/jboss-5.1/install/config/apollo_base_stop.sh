pushd /u01/app/jboss-5.1.0.GA/bin
shutdown_port=`expr 8000 + $1`

JAVA_HOME="/usr/jdk/jdk1.6.0_20"
JAVA_OPTS="-Xms64m -Xmx64m"

echo shutdown.sh -s jnp://localhost:$shutdown_port
shutdown.sh -s jnp://localhost:$shutdown_port
popd

