
pushd /u01/app/jboss-5.1.0.GA/bin
 
echo run.sh -b 0.0.0.0 -P /u01/app/jboss-5.1.0.GA/config/apollo.properties -c $1 -Djboss.bind.port.offset=$2

rm -f /u02/log4j_logs/server.$1.log
rm -f /u02/log4j_logs/gclogs.$1.txt
export GC_LOG_FILE=/u02/log4j_logs/gclogs.$1.txt

run.sh -b 0.0.0.0 -P /u01/app/jboss-5.1.0.GA/config/apollo.properties -c $1 -Djboss.bind.port.offset=$2 >> /u02/log4j_logs/server.$1.log & 
popd

