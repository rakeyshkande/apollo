# Copies relevant files from TEMPLATE into the server based on updates  

if [ $# -lt 1 ]; then
  echo Requires Parameters 'ServerName'  
  exit 1 
fi


echo Updating Server: $1


if [ -d "../server/$1" ]; then
    mkdir ../server/backups/

    # Remove existing backup if one exists
    rm -fr ../server/backups/$1.bak
	
    echo Backing up Server
    mv ../server/$1 ../server/backups/$1.bak

    echo Generating new Server from Template 
    cp -rp ../server/TEMPLATE ../server/$1

    echo Copying .ear files from backup 
    cp -p ../server/backups/$1.bak/deploy/*.ear ../server/$1/deploy

    echo Server $1 updated to latest TEMPLATE 
else
    echo Server $1 does not exist 
fi

