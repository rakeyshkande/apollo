package converter.common.project;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;

public class EclipseProjectCreator {

    private boolean isNewProject = false;
	
	/**
	 * Pass in the path to the <project> folder will generate 
	 * Eclipse Project and Classpath files
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if(args.length == 0)
		{
			System.out.println("Command Line Parameter: Path to <project> folder");
			return;
		}
		String projectFolderPath = args[0];
		File projectFolder = new File(projectFolderPath);
		
		EclipseProjectCreator creator = new EclipseProjectCreator();
		creator.createEclipseProject(projectFolder);		
	}

	public void createEclipseProject(File projectFolder) throws Exception {
		File classpathFile = new File(projectFolder, ".classpath");
		File projectFile = new File(projectFolder, ".project");		

		if(classpathFile.exists())
		{
		    System.out.println("Skipping .classpath, file exists");
		} else
		{
		    System.out.println("Creating Classpath File: " + classpathFile.getAbsolutePath());
		    String cpContents = IOUtils.toString(getClass().getResourceAsStream("classpath.template.txt"));
		    FileOutputStream fos = new FileOutputStream(classpathFile);
		    IOUtils.write(cpContents, fos);
		    fos.close();
		    isNewProject = true;
		}
		
        if(projectFile.exists())
        {
            System.out.println("Skipping .project, file exists");
        } else
        {
            System.out.println("Creating Project File: " + projectFile.getAbsolutePath());
            String pContents = IOUtils.toString(getClass().getResourceAsStream("project.template.txt"));
            
            pContents = pContents.replaceAll("\\$project.name\\$", projectFolder.getName());
            FileOutputStream fos = new FileOutputStream(projectFile);
            IOUtils.write(pContents, fos);
            fos.close();
            isNewProject = true;
        }		
	}		
	
	public boolean isNewProject() {
	    return isNewProject;
	}
}
