package converter.common.project;

import java.io.File;

import converter.common.cache.CacheConfigurationConverter;

public class SetupNewProject
{
    /**
     * Pass in the path to the <project> folder will generate 
     * Eclipse Project and Classpath and Cache files
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        if(args.length == 0)
        {
            System.out.println("Command Line Parameter: Path to <project> folder");
            return;
        }
        String projectFolderPath = args[0];
        File projectFolder = new File(projectFolderPath);
        
        File cnfFolder = new File(projectFolder, "cnf");
        
        EclipseProjectCreator creator = new EclipseProjectCreator();
        creator.createEclipseProject(projectFolder);        
        
        CacheConfigurationConverter cacheConfigurationConverter = new CacheConfigurationConverter();
        cacheConfigurationConverter .convertProjectCacheFile(cnfFolder);
        
        if(creator.isNewProject())
        {
            System.out.println("Executing Source Conversion");
            File srcFolder = new File(projectFolder, "src");
            ProjectConverter projectConverter = new ProjectConverter();
            projectConverter.convertFiles(srcFolder.getAbsolutePath());
        } else 
        {
            System.out.println("Existing Project, skipping Source Conversion");
        }
    }
    

}
