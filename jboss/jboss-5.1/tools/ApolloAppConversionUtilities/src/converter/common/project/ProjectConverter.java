package converter.common.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import converter.common.FileUtil;
import converter.common.cache.CacheConfigurationConverter;


/**
 * Provide limited conversion of *.java files from Oracle to JBoss containers/parsing libraries.
 * Tool will provide a report/listing of files it is considering and whether it is has changed the file (with the number of changed lines).
 * All changed file must be reviewed for correctness against the original.
 * DO NOT RERUN this tool on the same file twice or on an already converted/partially converted file.
 * 
 * Changed files will replace the original so if not running on CVS/source code controlled files, make sure you have a back up.
 * 
 * Tool expects one argument, the base path to your source. I recommend the "src" directory of the module/project. 
 * It will filter out CVS directories and look for all java files.
 * @author chill
 *
 */
public class ProjectConverter {

	List <String> tokens = new ArrayList<String>();
	Map <String, String> handler2cache;
	private int totalChanges = 0;
	private int totalFilesChanged = 0;
	private Pattern pattern1, pattern2, pattern3;
	
	ProjectConverter () throws Exception {
		tokens = FileUtil.readFileLines(getClass().getResourceAsStream("conversionTokens.txt"));
		String regex = "(\\s*)(.*)\\.print\\((.*)";
		pattern1 = Pattern.compile(regex);

		regex = "(\\s*.*[= ])(.*).selectSingleNode\\((.*)";
		pattern2 = Pattern.compile(regex);
		regex = "(\\s*.*[= ])(\\w*).selectNodes\\((.*)";
		pattern3 = Pattern.compile(regex);

		getCacheInfo();
	}
		
	/**
	 * Appends to the conversion token's the Cache Handler Class Names to map from 
	 * the old cache to the new cache
	 */
	private void getCacheInfo()
    {
	    CacheConfigurationConverter cacheConverter = new CacheConfigurationConverter();
	    List<String> cacheTokens = cacheConverter.getHandlerNameConversionTokens("~");
	    tokens.addAll(cacheTokens);
	    handler2cache = cacheConverter.getHandlerName2CacheMap();
	    
    }

    /**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws Exception {
        if(args.length == 0)
        {
            System.out.println("Command Line Parameter: Path to project src folder");
            return;
        }

		ProjectConverter converter = new ProjectConverter();
		converter.convertFiles(args[0]);
		System.out.println("Conversion complete.");
		System.out.println("  Total Files Changed: " + converter.totalFilesChanged);
		System.out.println("  Total Lines Changed: " + converter.totalChanges);
		
	}
	
	public void convertFiles(String dirname) throws IOException {
		File dir = new File(dirname);
		//System.out.println("Found " + dirname);
		File files[] = dir.listFiles();
		if (files.length == 0)
			return;
		for (File f : files) {
			String fname = f.getName();
			if (!fname.equals("CVS")) {
				if (f.isDirectory())
					convertFiles(dirname + "/" + fname);
				else if (fname.endsWith(".java")) {
					System.out.println("Convert file: " + dirname + "/" + fname);
					convertFile(dirname + "/" + fname);
				}
			}
		}
	}
	
	/// Line by Line, apply various replacements on the line to migrate it forward
	private void convertFile(String fname) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fname));
		StringBuffer sb = new StringBuffer();
		String s, news;
		int changed = 0;
		while ((s = br.readLine()) != null) {
			news = swapToken(s);
			sb.append(swapToken(s)+"\r\n");
			if (!s.equals(news)) {
				changed++;
				//System.out.println(s);
			}
		}
		br.close();
		
		if (changed > 0) {
			System.out.println("Updating file " + fname + " with " + changed + " changes.\n");
			File f = new File(fname);
			f.setWritable(true);
			FileOutputStream fos = new FileOutputStream(fname);
			IOUtils.write(sb, fos);
			fos.close();
			totalFilesChanged ++;
		}
		else
			System.out.println("No changes to file " + fname + ".\n");

		
		this.totalChanges += changed;
	}
	
	// Apply configurable replacement from the file and hardcoded, more complex/regex replacements 
	private String swapToken(String line) {
		String newLine = line;
		String regexToken, newToken, token;
		Iterator <String>tI = tokens.iterator();
		
		// Configurable replacements
		while (tI.hasNext()) {
			token = tI.next();
			int ind = token.indexOf("~");
			regexToken = token.substring(0,ind);
			newToken = token.substring(ind+1);
			
			//if (line.indexOf(regexToken)!= -1) {
				newLine = line.replace(regexToken, newToken);
				line = newLine;
			//}
		}
		
		// Hard-coded regex replacements
		// doc.print(X) -> DOMUtil.print(doc, X)
		// 	doc.print(new StringWriter(xml));
		String regex = "(\\s*)(.*).print\\((.*)";

		String replace = "$1DOMUtil.print($2, $3";
		newLine = swapRegex(line, pattern1, replace);
		line = newLine;
		
		// doc.selectSingleNode(xml); -> DOMUtil.selectsingleNode(doc, xml);
		// n1 = doc.selectSingleNode(xml); -> n1 = DOMUtil.selectsingleNode(doc, xml);
		regex = "(\\s*.*[= ])(.*).selectSingleNode\\((.*)";
		replace = "$1DOMUtil.selectSingleNode($2, $3";
		newLine = swapRegex(line, pattern2, replace);

		line = newLine;
		
		// n1 = doc.selectNode(xml); -> n1 = DOMUtil.selectNodes(doc, xml);
		// doc.selectNode(xml); -> DOMUtil.selectNodes(doc, xml);
		regex = "(\\s*.*[= ])(\\w*).selectNodes\\((.*)";
		replace = "$1DOMUtil.selectNodes($2, $3";
		newLine = swapRegex(line, pattern3, replace);

		newLine = swapCacheNames(newLine);
		
		
		return newLine;
	}



    private String swapRegex(String line, Pattern pattern, String replace) {
		String newLine;
		Matcher match = pattern.matcher(line);
		newLine = match.replaceFirst(replace);
		return newLine;
	}

    
    /**
     * Replaces cache handler names referenced by "string" using the mappings from cache mapping
     * @param newLine
     * @return
     */
    private String swapCacheNames(String line)
    {
    	// AccountProgramMasterHandler programMasterHandler = (AccountProgramMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_ACCOUNT_PROGRAM_MASTER");
    	if (line.matches(".*\\(\\w+\\)\\s*CacheManager.getInstance\\(\\).getHandler.*")) {
    		// grab handler name and use it pull cache name from map
    		int cm = line.indexOf("CacheManager.getInstance().getHandler");
    		int lastLeft = line.lastIndexOf("(", cm);
    		int lastRight = line.lastIndexOf(")", cm);
    		String handlerName = line.substring(lastLeft+1, lastRight);
    		String cacheName = handler2cache.get(handlerName);
    		if (cacheName != null) {
    			String regex = "CacheManager.getInstance\\(\\).getHandler\\(\".*\"\\)"; /// move to constructor ///
    			Pattern pattern = Pattern.compile(regex);
    			String replace = "CacheManager.getInstance\\(\\).getHandler\\(\""+cacheName+"\"\\)";
    			line = swapRegex(line, pattern, replace);
    		}
    	}
        
        return line;
    }    
    
}
