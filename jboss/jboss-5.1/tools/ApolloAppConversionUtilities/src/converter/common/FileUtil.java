package converter.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class FileUtil {

	/**
	 * Reads all lines in the passed in stream
	 * @param is
	 * @return
	 */
	public static List<String> readFileLines(InputStream is) {
	    try 
	    {
	        return (List<String>) IOUtils.readLines(is);
	    } catch (Exception e)
	    {
	        throw new RuntimeException(e);
	    }
	}
	
	
	/**
	 * Creates a temp file with the passed in name, and InputStream contents
	 * @param inputStream
	 * @param name
	 * @return
	 */
	public static File createTempFile(InputStream inputStream, String name)
	{
		try {
			File tempFile = File.createTempFile(name, "");
			tempFile.deleteOnExit();
			
			BufferedInputStream bis = new BufferedInputStream(inputStream, 20000);
			
			// FileOutputStream fos = new FileOutputStream(tempFile);
			BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(tempFile), 20000);
			
			int next = bis.read();
			while(next >= 0)
			{
				fos.write(next);
				next = bis.read();
			}
			
			fos.close();
			return tempFile;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
