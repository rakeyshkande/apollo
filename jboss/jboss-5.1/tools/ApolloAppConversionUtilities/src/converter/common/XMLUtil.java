package converter.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

public class XMLUtil {

	public static Document getXMLDocument(InputStream inputStream)
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			return db.parse(inputStream);
		} catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	
	
	public static Element getElementByTagName(Element root, String tagName)
	{
	    List<Element> retVal = getElementsByTagName(root, tagName);
	    
	    if(retVal.size() > 0)
	    {
	        return retVal.get(0);
	    }
	    
	    return null;	    
	}


	public static List<Element> getElementsByTagName(Element rootElement, String tagName) {
		List<Element> retVal = new ArrayList<Element>();
		
		NodeList elements = rootElement.getElementsByTagName(tagName);
		for(int iLoop = 0; iLoop < elements.getLength(); iLoop ++)
		{
			retVal.add((Element) elements.item(iLoop));
		}
		return retVal;
	}


    public static String getElementTextByTagName(Element originalMDB, String tagName, String defValue)
    {
        Element el = getElementByTagName(originalMDB, tagName);
        
        if(el != null) 
        {
            return el.getTextContent();
        }
        
        return defValue;
        
    }
}
