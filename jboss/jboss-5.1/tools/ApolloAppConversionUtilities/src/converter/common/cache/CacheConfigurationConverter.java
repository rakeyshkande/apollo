package converter.common.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import converter.common.XMLUtil;

/**
 * Converts cache.xml to cacheMgr.xml based on a pre-defined mapping
 */
public class CacheConfigurationConverter {

	private static String TABSPACE = "    ";
	private static String TABSPACE2 = TABSPACE + TABSPACE ;
	private static String TABSPACE3 = TABSPACE2 + TABSPACE ;	
	
	Document cacheMappings = XMLUtil.getXMLDocument(getClass().getResourceAsStream("cachemapping.xml"));
	List<Element> mapping =  XMLUtil.getElementsByTagName(cacheMappings.getDocumentElement(), "cache");
	
	boolean hasUnknownCache = false;
	String unknownCaches = "";
	
	public CacheConfigurationConverter ()
	{
	}
	
	public String convert(InputStream source)
	{
		Document sourceDocument = XMLUtil.getXMLDocument(source);
		Element docRoot = sourceDocument.getDocumentElement();
		
		hasUnknownCache = false;
		unknownCaches = "";
		
		if("cache-config".equals(docRoot.getTagName()) == false)
		{
			throw new RuntimeException("Not a cache.xml file");
		}
		
		StringBuilder sb = new StringBuilder(16384);
		addCacheXMLHeader(sb, docRoot);
		List<Element> caches = XMLUtil.getElementsByTagName(docRoot, "cache");
		
		for(Element cache: caches)
		{
			sb.append("\n");			
			addCache(sb, cache);
			sb.append("\n");		
		}
		
		addCacheXMLFooter(sb);
		
		return sb.toString();
	}
	
	private void addCacheXMLHeader(StringBuilder sb, Element docRoot) {
		sb.append("<cache-config global-cache-name=");
		sb.append('"').append(docRoot.getAttribute("global-cache-name")).append('"');
		sb.append(" data-source=");
		sb.append('"').append(docRoot.getAttribute("data-source")).append('"');
		sb.append(" cache-admin-rate=");
		sb.append('"').append("5").append('"');		
		sb.append(">\n");
		sb.append(TABSPACE).append("<caches>");		
	}	

	private void addCacheXMLFooter(StringBuilder sb) {
		sb.append("\n");
		sb.append(TABSPACE).append("</caches>");
		sb.append("\n");		
		sb.append("</cache-config>");
	}

	private void addCache(StringBuilder sb, Element originalCache) {
		
		Element cacheMapping = getCacheMapping(originalCache);
		
		if(cacheMapping == null)
		{
			sb.append(TABSPACE2);
			sb.append("<!--     No Cache Mapping for Handler: ");
			sb.append(originalCache.getAttribute("cache-loader"));
			sb.append(" -->");
			hasUnknownCache = true;
			unknownCaches += TABSPACE + "UNKNOWN CACHE: " + originalCache.getAttribute("cache-loader") + "\n";
			return;
		}
		
		sb.append(TABSPACE2).append("<cache ");
		sb.append(" name=");
		sb.append('"').append(cacheMapping.getAttribute("name")).append('"');
		sb.append(" cache-type=");
		sb.append('"').append("Fixed").append('"');		
		sb.append("\n");
		sb.append(TABSPACE3);
		sb.append(" cache-loader=");
		sb.append('"').append(cacheMapping.getAttribute("newClass")).append('"');
		sb.append("\n");
		sb.append(TABSPACE3);
		sb.append(" refresh-rate=");
		sb.append('"').append(originalCache.getAttribute("refresh-rate")).append('"');		
		sb.append(" preload=");
		sb.append('"').append(originalCache.getAttribute("preload")).append('"');		
		sb.append(" />");		
	}

	private Element getCacheMapping(Element originalCache) {
		String originalCacheName = originalCache.getAttribute("cache-loader");

		for(Element newCache: mapping) {
			if(newCache.getAttribute("originalClass").equals(originalCacheName))
			{
				return newCache;
			}
		}
		
		return null;
	}
	
	


	public void convertProjectCacheFile(File cnfFolder)
	{
		File cacheconfig = new File(cnfFolder, "cacheconfig.xml");
		File cacheMgrConfig = new File(cnfFolder, "cacheMgrConfig.xml");
		convertProjectCacheFile(cacheconfig, cacheMgrConfig);
		
	}
	public void convertProjectCacheFile(File cacheconfig, File cacheMgrConfig)
	{
		try 
		{
			if(!cacheconfig.exists())
			{
				System.out.println("No Cache to convert");
				return;
			}
			System.out.println("Reading: " + cacheconfig.getAbsolutePath());
			System.out.println("Writing: " + cacheMgrConfig.getAbsolutePath());
			
			String output = convert(new FileInputStream(cacheconfig));		
			
			FileOutputStream fos = new FileOutputStream(cacheMgrConfig);
			fos.write(output.getBytes());
			fos.close();
			
			System.out.println("Completed: " + cacheMgrConfig.getAbsolutePath());
			
			if(hasUnknownCache)
			{
				System.out.println("WARNING: Generated Cache has Unknown Caches:");
				System.out.println(unknownCaches);
			}
			
		} catch (Exception e)
		{
			throw new RuntimeException(e);
		}		
	}
	
	
	
	/**
	 * Pass in the path to the <project>/cnf folder will read cacheconfig.xml and generate 
	 * cacheMgrConfig.xml
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if(args.length == 0)
		{
			System.out.println("Command Line Parameter: Path to <project>/cnf folder");
			return;
		}
		String cnfFolderPath = args[0];
		File cnfFolder = new File(cnfFolderPath);
		
		CacheConfigurationConverter converter = new CacheConfigurationConverter();
		converter.convertProjectCacheFile(cnfFolder);		
	}

    public String getHandlerNameForNewHandler(String handlerClassName)
    {
        for(Element newCache: mapping) {
            if(newCache.getAttribute("newClass").endsWith(handlerClassName))
            {
                return newCache.getAttribute("name");
            }
        }
        return null;
    }

    /**
     * Returns the original name, and the new name separated by the delimiter
     * @param delimiter
     * @return
     */
    public List<String> getHandlerNameConversionTokens(String delimiter)
    {
        List<String> retVal = new ArrayList<String>();
        
        for(Element newCache: mapping) {
            String oldClass = newCache.getAttribute("originalClass");
            String newClass = newCache.getAttribute("newClass");
            
            // Fully qualified by package (Handles Imports)
            retVal.add(oldClass + delimiter + newClass);
            
            // Just the class names (Handles code)
            oldClass = oldClass.substring(oldClass.lastIndexOf('.') + 1);
            newClass = newClass.substring(newClass.lastIndexOf('.') + 1);
            
            if(!oldClass.equals(newClass))
                retVal.add(oldClass + delimiter + newClass);
        }

        return retVal;
    }	

    /**
     * Returns a map of the (new) cache handler class name to cache name/constant mapping
     * @param delimiter
     * @return
     */
    public Map<String,String> getHandlerName2CacheMap()
    {
        Map<String,String> retVal = new HashMap<String,String>();
        
        for(Element newCache: mapping) {
            String cachename = newCache.getAttribute("name");
            String newClass = newCache.getAttribute("newClass");
                       
            // Just the class names (Handles code)
            newClass = newClass.substring(newClass.lastIndexOf('.') + 1);
            
            retVal.put(newClass, cachename);
        }

        return retVal;
    }	

}
