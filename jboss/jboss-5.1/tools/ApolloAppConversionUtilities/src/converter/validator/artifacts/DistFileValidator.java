package converter.validator.artifacts;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import converter.common.FileUtil;

/**
 * Validates the distribution for known problems
 */
public class DistFileValidator {

	File tempDir;

	List<String> invalidItemsInDist = new ArrayList<String>();

	List<String> invalidItemPatterns = FileUtil.readFileLines(getClass()
			.getResourceAsStream("invalidfilepatterns.txt"));

	public DistFileValidator() {
	}

	/**
	 * Recursively validates all files in the Dist Folder
	 * 
	 * @param distFolder
	 */
	public void validateDistArtifacts(File distFolder) {
		File[] artifacts = distFolder.listFiles();

		for (File artifact : artifacts) {
			validateArtifact(artifact);
		}
	}

	/**
	 * Validates the passed in artifact
	 * 
	 * @param artifact
	 */
	private void validateArtifact(File artifact) {
		System.out.println("Checking artifact: " + artifact.getAbsolutePath());
		if (artifact.getAbsolutePath().toLowerCase().endsWith(".ear")) {
			try {
				validateZipArchive(new ZipFile(artifact), artifact.getName());
			} catch (Exception e) {
				System.out.println("Failed to validate artifact: "
						+ e.toString());
			}
		}
	}

	private void validateZipArchive(ZipFile zipFile, String archiveName) throws Exception {
		try {
			Enumeration<? extends ZipEntry> zipItems = zipFile.entries();
			Set<String> duplicateChecks = new HashSet<String>();
			while (zipItems.hasMoreElements()) {
				ZipEntry item = zipItems.nextElement();
				
				if(item.isDirectory())
				{
					continue;
				}

				String itemName = item.getName();
				
				
				if(duplicateChecks.contains(itemName))
				{
	                System.out.println("Duplicate Item: " + archiveName + ":" + itemName);
	                invalidItemsInDist.add(itemName);				    
				}
				
				duplicateChecks.add(itemName);

				if (!validItemName(itemName, archiveName)) {
					continue;
				}

				// If this is an ear or war, unpack it and recurse
				if (itemName.toLowerCase().endsWith(".ear")
						|| itemName.toLowerCase().endsWith(".war")
						|| itemName.toLowerCase().endsWith(".jar")) {
					validateSubArchive(item, zipFile.getInputStream(item), archiveName);
				}
			}
		} finally {
			zipFile.close();
		}
	}

	private void validateSubArchive(ZipEntry item, InputStream inputStream, String archiveName)
			throws Exception {
		File fileName = new File(item.getName());

		File subArchiveFile = FileUtil.createTempFile(inputStream,
				fileName.getName());
		validateZipArchive(new ZipFile(subArchiveFile), archiveName + "/" + fileName.getName());
	}

	/**
	 * Checks the Item name against items that should not be supported
	 * 
	 * @param itemName
	 * @return
	 */
	private boolean validItemName(String itemName, String archiveName) {
		File fileName = new File(itemName);
		String checkItemName = fileName.getName();
		
		for (String invalidPattern : invalidItemPatterns) {
			if (checkItemName.matches(invalidPattern)) {
				System.out.println("Invalid Item: " + archiveName + ":" + itemName);
				invalidItemsInDist.add(itemName);
				return false;
			}
		}

		return true;
	}
	
	
	public static void main(String[] args) throws Exception
	{
		if(args.length == 0)
		{
			System.out.println("Command Line Parameter: Path to dist.jboss folder");
			return;
		}
		String distFolderPath = args[0];
		File distFolder = new File(distFolderPath);
		new DistFileValidator().validateDistArtifacts(distFolder);
	}
}
