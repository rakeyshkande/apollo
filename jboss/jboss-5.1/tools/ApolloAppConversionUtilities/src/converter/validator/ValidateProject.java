package converter.validator;

import java.io.File;

import converter.validator.artifacts.DistFileValidator;

public class ValidateProject
{
    /**
     * Pass in the path to the <project> folder will generate 
     * Eclipse Project and Classpath and Cache files
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        if(args.length == 0)
        {
            System.out.println("Command Line Parameter: Path to <project> folder");
            return;
        }
        String projectFolderPath = args[0];
        File projectFolder = new File(projectFolderPath);
        
        File srcFolder = new File(projectFolder, "src");
        
        System.out.println("Executing Source Validation");
        JavaFileValidator validator = new JavaFileValidator(srcFolder);
        validator.validateFiles(srcFolder);
        
        File webFolder = new File(projectFolder, "web");
        if(webFolder.exists())
        {
            System.out.println("Executing XSL Validation");
            XslFileValidator xslValidator = new XslFileValidator();
            xslValidator.validateXSLFiles(webFolder);
        }
        
        File distFolder = new File(projectFolder, "dist.jboss");
        if(distFolder.exists())
        {
            System.out.println("Executing Dist Validation");
            DistFileValidator distValidator = new DistFileValidator();
            distValidator.validateDistArtifacts(distFolder);
            
            
        }
        
        
    }
    

}
