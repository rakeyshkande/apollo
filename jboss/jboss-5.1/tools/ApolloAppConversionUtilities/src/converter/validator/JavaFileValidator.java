package converter.validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import converter.common.FileUtil;
import converter.common.cache.CacheConfigurationConverter;

public class JavaFileValidator
{
    CacheConfigurationConverter cacheConfigurationConverter = new CacheConfigurationConverter();
    
    File baseFolder= null;
    List<String> javaValidation = new ArrayList<String>();

    public JavaFileValidator(File srcFolder)
    {
        baseFolder = srcFolder.getParentFile();
        
        // Note: Most of these should have been handled by the Converter, but since we 
        // Also have a merge, revalidate
        List<String>  fileValidation= FileUtil.readFileLines(getClass().getResourceAsStream("java_validation.txt"));
        
        for(String item: fileValidation)
        {
            if(item.trim().length() == 0)
            {
                continue;
            }
         
            javaValidation.add(item);
            
        }
    }

    public static void main(String[] args) throws Exception
    {
        if(args.length == 0)
        {
            System.out.println("Command Line Parameter: Path to src folder");
            return;
        }
        String folderPath = args[0];
        File folder = new File(folderPath);
        
        if(!folder.exists())
        {
            System.out.println("Path does not exist: " + folder.getAbsolutePath());
            return;
        }
        
        new JavaFileValidator(folder).validateFiles(folder);
    }
    
    /**
     * Attempts to validate the passed in XSL file
     * @param target
     */
    public void validateFiles(File target) throws Exception
    {
        if(target.isFile())
        {
            checkFile(target);
        } else
        {
            if(target.getName().equals("CVS"))
            {
                return;
            }
            
            File[] children = target.listFiles();
            for(File child: children)
            {
                validateFiles(child);
            }
        }
    }

    private void checkFile(File target) throws Exception
    {
        if(!target.getName().toLowerCase().endsWith(".java"))
        {
            return;
        }        
        
        BufferedReader br = new BufferedReader(new FileReader(target));
        
        String nextLine = null;
        int iLineNumber = 1;
        while ((nextLine = br.readLine()) != null) {
            checkCache(nextLine, target, iLineNumber);
            checkClasses(nextLine, target, iLineNumber);
            iLineNumber++;
        }
    }


    /**
     * Checks if this is a Cache configuration
     * @param nextLine
     */
    
    static final String Cache_Mgr_Line = "CacheManager.getInstance().getHandler(";
    private void checkCache(String nextLine, File target, int lineNumber)
    {
        int iIndexCacheMgrLine = nextLine.indexOf(Cache_Mgr_Line);
        if(iIndexCacheMgrLine < 0)
        {
            return;
        }
        
        // Get the Handler Name
        int handlerNameStart = iIndexCacheMgrLine + Cache_Mgr_Line.length() + 1;
        int handlerNameEnd = nextLine.indexOf('"', handlerNameStart);
        
        if(handlerNameEnd < 0)
        {
            printLineMessage("Cannot Validate: " + nextLine.trim(), target, lineNumber);            
            return;
        }
        
        String handlerName = nextLine.substring(handlerNameStart, handlerNameEnd);
        
        
        // Handler Class
        handlerNameStart = nextLine.indexOf("(") + 1;
        handlerNameEnd = nextLine.indexOf(")", handlerNameStart);
        handlerNameStart = nextLine.lastIndexOf("(", handlerNameEnd) + 1;
        String handlerClassName = nextLine.substring(handlerNameStart, handlerNameEnd);
        
        String expectedHandlerName = cacheConfigurationConverter.getHandlerNameForNewHandler(handlerClassName);
        
        if(handlerName.equals(expectedHandlerName) == false)
        {
            printLineMessage("Cache Name Mismatch: Expected " + expectedHandlerName + " Found: " + handlerName, target, lineNumber);
            return;            
        }
    }    

    
    /**
     * Check for lines that should be reviewed
     * @param nextLine
     * @param name
     * @param iLineNumber
     */
    private void checkClasses(String nextLine, File target, int iLineNumber)
    { 
        // This might be right or wrong, and needs inspection
        if(nextLine.indexOf(" File(") >= 0 || nextLine.indexOf("java.io.File;") >= 0)
        {
            printLineMessage("Reference to File(). Confirm this is valid", target, iLineNumber);
        }
       
        //toJSONObject
        if(nextLine.indexOf("toJSONObject(") >= 0)
        {
            printLineMessage("Reference to toJSONObject(). Confirm this is valid", target, iLineNumber);
        }
        
        // These are definately wrong
        for(String item: javaValidation)
        {
            if(nextLine.indexOf(item) >= 0)
            {
                printLineMessage("Reference to " + item + ". This must be converted", target, iLineNumber);
            }
        }
    }    
    
    
    
    /**
     * Formats it such that Eclipse will find the file (usually) if it is in the workspace
     * @param message
     * @param target
     * @param iLineNumber
     */
    private void printLineMessage(String message, File target, int iLineNumber)
    {       
        String filePath = target.getAbsolutePath().substring(baseFolder.getAbsolutePath().length()).replaceAll("\\\\", "/");
        
        String fileLink = baseFolder.getName() + filePath;
        
        fileLink = target.getName();
        
        System.out.println(message + " at (" + fileLink + ":" + iLineNumber + ")" + ":Full Path=" + target.getAbsolutePath());
    }
}
