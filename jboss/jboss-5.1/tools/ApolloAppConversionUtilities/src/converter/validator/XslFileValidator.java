package converter.validator;

import java.io.File;

import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

/**
 * Validates the xsl files in the passed in folders. Logs 
 * the results to System.out.
 * 
 * This is because the Xalan parser is more strict than the
 * Oracle Parser
 */
public class XslFileValidator
{

    
    public static void main(String[] args) throws Exception
    {
        if(args.length == 0)
        {
            System.out.println("Command Line Parameter: Path to web folder");
            return;
        }
        String webFolderPath = args[0];
        File webFolder = new File(webFolderPath);
        
        if(!webFolder.exists())
        {
            System.out.println("Path does not exist: " + webFolder.getAbsolutePath());
            return;
        }
        
        new XslFileValidator().validateXSLFiles(webFolder);
    }

    /**
     * Attempts to validate the passed in XSL file
     * @param target
     */
    public void validateXSLFiles(File target)
    {
        if(target.isFile())
        {
            checkFile(target);
        } else
        {
            File[] children = target.listFiles();
            for(File child: children)
            {
                validateXSLFiles(child);
            }
        }
        
    }

    private void checkFile(File target)
    {
        if(!target.getName().toLowerCase().endsWith(".xsl"))
        {
            return;
        }
        
        
       
        TransformerFactory factory = TransformerFactory.newInstance();

        try
        {
            factory.newTemplates(new StreamSource(target));
            System.out.println("OkFile: " + target.getAbsolutePath());
        }
        catch (TransformerConfigurationException e)
        {
            System.out.println("WARNING - File: " + target.getAbsolutePath() + "->" + e.getMessage());
        }

        
    }    
    
    
}
