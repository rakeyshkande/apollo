package converter.validator;

import java.io.File;
import java.io.FileInputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import converter.common.XMLUtil;

public class EJBValidator
{

    
    public static void main(String[] args) throws Exception
    {
        if(args.length == 0)
        {
            System.out.println("Command Line Parameter: <Path to original ejb-jar.xml> <path to new ejb-jar.xml> [path jboss.xml] ");
            return;
        }
        
        String orginalPath = args[0];
        File originalFile = new File(orginalPath);
        
        String newPath = args[1];
        File newFile = new File(newPath);
        
        if(!originalFile.exists())
        {
            System.out.println("File does not exist: " + originalFile.getAbsolutePath());
            return;
        }
        
        if(!newFile.exists())
        {
            System.out.println("File does not exist: " + newFile.getAbsolutePath());
            return;
        }        

        File jbossFile = new File(newFile.getParentFile(), "jboss.xml");
        if(args.length > 2)
        {
            String jbossPath = args[2];
            jbossFile = new File(jbossPath);
        }
        
        if(!jbossFile.exists())
        {
            System.out.println("File does not exist: " + jbossFile.getAbsolutePath());
            return;
        }               
        
        new EJBValidator().validateEjb(originalFile, newFile, jbossFile);
    }

    
    private void validateEjb(File originalFile, File newFile, File jbossFile) throws Exception
    {
        Document originalEJBConfig = XMLUtil.getXMLDocument(new FileInputStream(originalFile));
        Document newEJBConfig = XMLUtil.getXMLDocument(new FileInputStream(newFile));
        Document jbossConfig = XMLUtil.getXMLDocument(new FileInputStream(jbossFile));
        
        checkMDBs(originalEJBConfig, newEJBConfig, jbossConfig);
    }


    private void checkMDBs(Document originalEJBConfig, Document newEJBConfig, Document jbossConfig)
    {
        // Check MDBs
        NodeList originalMDBs = originalEJBConfig.getElementsByTagName("message-driven");
        NodeList newMDBs = newEJBConfig.getElementsByTagName("message-driven");
        NodeList jbossMDBs = jbossConfig.getElementsByTagName("message-driven");
        
        if(originalMDBs.getLength() != newMDBs.getLength())
        {
            System.out.println("Number of MDBs do not match in ejb-jar.xml:" + originalMDBs.getLength() + " <> " +  newMDBs.getLength());
            return;
        }
        
        if(newMDBs.getLength() != jbossMDBs.getLength())
        {
            System.out.println("Number of MDBs do not match in jboss.xml" + "ejb-jar.xml: " + newMDBs.getLength() + ", jboss.xml: " + jbossMDBs.getLength());
            return;
        }        
        
        String[] attributesToCheck = new String[]
        {
          "display-name",
          "ejb-name",
          "ejb-class",
          "transaction-type"
        };
        
        for(int iLoop = 0; iLoop < originalMDBs.getLength(); iLoop ++)
        {
            Element originalMDB = (Element) originalMDBs.item(iLoop);
            Element newMDB = (Element) newMDBs.item(iLoop);
            Element jbossMDB = (Element) jbossMDBs.item(iLoop);
            
            String mdbName = XMLUtil.getElementByTagName(originalMDB, "ejb-name").getTextContent();
            
            // Attribute Check
            for(String attrName: attributesToCheck)
            {
                String attr1 = XMLUtil.getElementByTagName(originalMDB, attrName).getTextContent();
                String attr2 = XMLUtil.getElementByTagName(newMDB, attrName).getTextContent();
                if(attr1.equals(attr2) == false)
                {
                    System.out.println("Attribute Mismatch: " + mdbName + " Attr: "+ attrName + " Expected: " + attr1 + " Found: " + attr2);
                }                
            }
            
            String originalVal = "" + XMLUtil.getElementTextByTagName(originalMDB, "message-destination-link", null);
            String newVal = "" + getActivationConfig(newMDB, "DestinationJndiName", null);
            
            if(originalVal.equals(newVal) == false)
            {
                System.out.println("Mismatch in Queue: " + mdbName + " Expected: " + originalVal + " Found: " + newVal);
            }
            
            originalVal = "" + XMLUtil.getElementTextByTagName(originalMDB, "res-ref-name", null);
            newVal = "" + getActivationConfig(newMDB, "ConnectionFactoryJndiName", null);
            
            if(originalVal.equals(newVal.replace("java:", "")) == false)
            {
                System.out.println("Mismatch in CF: " + mdbName + " Expected: " + originalVal + " Found: " + newVal);
            }            

            originalVal = "" + getActivationConfig(originalMDB, "receiverThreads", "1");
            newVal = "" + getActivationConfig(newMDB, "MaxPoolSize", "1");
            
            if(originalVal.equals(newVal) == false)
            {
                System.out.println("Mismatch in MaxPoolSize: " + mdbName + " Expected: " + originalVal + " Found: " + newVal);
            }                   
            
            String jbossMDBName =  XMLUtil.getElementTextByTagName(jbossMDB, "ejb-name", null);
            
            if(mdbName.equals(jbossMDBName) == false)
            {
                System.out.println("Mismatch in JBOSS MDB: " + mdbName + " Expected: " + mdbName + " Found: " + jbossMDBName);
            }
        }        
        
        String jmxName = XMLUtil.getElementTextByTagName(jbossConfig.getDocumentElement(), "jmx-name", null);
        
        if(jmxName == null || jmxName.trim().length() == 0)
        {
            System.out.println("Missing/Invalid jmx-name element in jboss.xml, add with unique module name: <jmx-name>jboss.j2ee:service=EjbModule,module=????????</jmx-name>");
        }
    }    
    
    
    private String getActivationConfig(Element root, String configName, String defaultVal)
    {
        NodeList activationConfigs = root.getElementsByTagName("activation-config-property");
        
        for(int iLoop = 0; iLoop < activationConfigs.getLength(); iLoop ++)
        {
            Element actConfig = (Element) activationConfigs.item(iLoop);
            
            String attrName = XMLUtil.getElementByTagName(actConfig, "activation-config-property-name").getTextContent();
            
            if(attrName.equals(configName))
            {
                return XMLUtil.getElementByTagName(actConfig, "activation-config-property-value").getTextContent();
            }
        }
        
        return defaultVal;        
    }
    
}
