println "--Oracle AS -> JBOSS Conversion Utility--"

if (args) {
	println "+Attempting to convert ${args[0]}"
	
	def project = args[0]

	// the projects are located a few subdirectories below this one	
	new File("../../../../../.").eachDir{dir ->
		
		//find the project folder
		if (dir.getCanonicalPath().contains(args[0])) {
			println "+Located project ${args[0]}"
			println "${dir.getCanonicalPath()}"
			new File(dir.getCanonicalPath()).eachFileRecurse { file ->
				//println file.getName()
				if (file.isFile() && file.name.endsWith(".java")) {
					println file
					//println"+making file writable"
					file.setReadable true
					
					//search for some contents in the file
					List lines = file.readLines()
					println "lines: ${lines.size()}"
					
					
					//this is a buffer where we will store the contents of a file
					//with string replacements.  we will replace the original file with this when done
					def fileContents = new StringBuffer()
					
					//do some search and replace on each line
					lines.each {line -> 
						//println line
						
						//
						line = convertLine()

						fileBuffer.append(line + "\r\n")
						
						
					}
					//write the filebuffer to the file on which we are operating
					file.write(fileBuffer.toString());
				}
			}
							
		}
	}
	
} else {
	println "No module specified.  Please specify a module name."
	println "ex: >groovy convert.groovy customerordermanagement"
}

/**
 * run all of the string replacements to filter out Oracle specific code
 */
def String convertLine() {
	
	if (line.contains("XMLDocument")) {
		line = line.replaceAll "XMLDocument", "Document"
	}
	
	
}

