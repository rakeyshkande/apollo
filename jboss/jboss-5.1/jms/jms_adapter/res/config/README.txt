Approach for masking Passwords/Configurations in the XML Configurations:
http://docs.jboss.org/jbosssecurity/docs/6.0/security_guide/html/Masking_Passwords.html

Note: password_tool.bat is not in the 5.1 distro. Copy the file in this folder to jboss/bin
to use the instructions on Windows.
REfer to deploy/oracleaq-jboss-beans-secure-config.xml for an example of how this is used.

Using the masked password in a bean definition:
<annotation>@org.jboss.security.integration.password.Password(securityDomain=MASK_NAME,methodName=setPROPERTY_NAME)</annotation>

Example:
<annotation>@org.jboss.security.integration.password.Password(securityDomain="oracleaqpasswd",methodName="setPassword")</annotation>