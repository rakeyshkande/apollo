package com.ftd.jms.oracle;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.jms.Destination;

/**
 * Implements the Destination interface allowing instantiation as a bean.
 * This destination is converted to the target destination class prior 
 * to being sent to the target delegate. 
 */
public class DestinationImpl implements Destination, Serializable {
	private static final long serialVersionUID = 7193533766888151311L;
	
	private static final Logger logger = Logger.getLogger(DestinationImpl.class.getName());

	String owner;
	String name;
	
	/*
	 * The actual destination. This is so we only need to convert once
	 */
	Destination actualDestination;
	
	public void setOwner(String owner) {
		this.owner=owner;
		logger.finest("owner=" + owner);
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setName(String name) {
		this.name=name;
		logger.finest("name=" + name);
	}
	
	public String getName() {
		return name;
	}

    
	protected Destination getActualDestination()
    {
        return actualDestination;
    }

	protected void setActualDestination(Destination delegate)
    {
        this.actualDestination = delegate;
    }
	
}
