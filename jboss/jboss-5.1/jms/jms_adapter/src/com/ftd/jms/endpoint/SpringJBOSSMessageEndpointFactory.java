package com.ftd.jms.endpoint;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJBMetaData;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.InitialContext;
import javax.resource.spi.UnavailableException;
import javax.resource.spi.endpoint.MessageEndpoint;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.transaction.xa.XAResource;

import org.jboss.ejb.Container;
import org.jboss.ejb.EJBProxyFactory;
import org.jboss.ejb.EnterpriseContext;
import org.jboss.ejb.InstancePool;
import org.jboss.ejb.MessageDrivenContainer;
import org.jboss.ejb.plugins.inflow.JBossMessageEndpointFactoryMBean;
import org.jboss.invocation.Invocation;
import org.jboss.metadata.InvokerProxyBindingMetaData;
import org.jboss.metadata.MessageDrivenMetaData;
import org.jboss.system.ServiceMBeanSupport;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * This class implements the Messaging Endpoint Factory for JBOSS.
 * The specific implementation utilizes Spring to perform the 
 * Message Listening and delegating of the Messages to the MessageDrivenBeans.
 * 
 * The purpose for this class is to avoid the utilization of the
 * {@link javax.jms.Connection#createConnectionConsumer(Destination, String, javax.jms.ServerSessionPool, int)}
 * method, which is unsupported by the Oracle JMS Adapter as it is an optional API.
 * 
 * The Spring implementation relies on the MessageConsumer functionality, which is supported.
 * 
 * This Endping factory is a light wrapper between the JBOSS {@link org.jboss.ejb.MessageDrivenContainer} 
 * (which contains all the Message Driven Beans, and transaction management logic), and the Spring 
 * {@link rg.springframework.jms.listener.DefaultMessageListenerContainer}, which listens for messages.
 * The implementation utilizes the {@link MessageDrivenInvocationListener} to delegate messages from
 * Spring to the JBOSS MessageDrivenContainer.
 * 
 */
@SuppressWarnings("deprecation")
public class SpringJBOSSMessageEndpointFactory extends ServiceMBeanSupport
		implements EJBProxyFactory, MessageEndpointFactory,
		JBossMessageEndpointFactoryMBean {

	private static final Logger logger = Logger
			.getLogger(SpringJBOSSMessageEndpointFactory.class.getName());
	boolean deliveryActive = false;

	/** The invoker binding */
	protected String invokerBinding;

	/** The invoker meta data */
	protected InvokerProxyBindingMetaData invokerProxyBindingMetaData;

	/** Our meta data */
	protected EndpointMetaData endpointMetaData;

	/** The JBOSS Message Bean Container */
	protected MessageDrivenContainer container;

	/** The Spring Message Listener */
	DefaultMessageListenerContainer springContainer;

	public Object getEJBHome() {
		throw new Error("Not valid for MessageDriven beans");
	}

	public EJBMetaData getEJBMetaData() {
		throw new Error("Not valid for MessageDriven beans");
	}

	@SuppressWarnings("rawtypes")
	public Collection getEntityCollection(Collection arg0) {
		throw new Error("Not valid for MessageDriven beans");
	}

	public Object getEntityEJBObject(Object arg0) {
		throw new Error("Not valid for MessageDriven beans");
	}

	public Object getStatefulSessionEJBObject(Object arg0) {
		throw new Error("Not valid for MessageDriven beans");
	}

	public Object getStatelessSessionEJBObject() {
		throw new Error("Not valid for MessageDriven beans");
	}

	public boolean isIdentical(Container arg0, Invocation arg1) {
		throw new Error("Not valid for MessageDriven beans");
	}

	public void setContainer(final Container container) {
		logger.info("Setting the Container: " + container);
		this.container = (MessageDrivenContainer) container;
	}

	public String getConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean getDeliveryActive() {
		return deliveryActive;
	}

	public void startDelivery() throws Exception {
		deliveryActive = true;

	}

	public void stopDelivery() throws Exception {
		deliveryActive = false;
	}

	public void stopDelivery(boolean arg0) throws Exception {
		deliveryActive = false;
	}

	public MessageEndpoint createEndpoint(XAResource arg0)
			throws UnavailableException {

		logger.info("Creating Endpoint: " + arg0);
		return null;
	}

	public boolean isDeliveryTransacted(Method arg0)
			throws NoSuchMethodException {
		// TODO Auto-generated method stub
		return false;
	}

	public void setInvokerBinding(String invokerBinding) {
		logger.info("Set Invoker Binding: " + invokerBinding);
		this.invokerBinding = invokerBinding;
	}

	public void setInvokerMetaData(
			InvokerProxyBindingMetaData invokerProxyBindingMetaData) {
		logger.info("Set Invoker Meta Data: " + invokerProxyBindingMetaData);
		this.invokerProxyBindingMetaData = invokerProxyBindingMetaData;
	}

	protected void startService() throws Exception {
		logger.info("Starting Service");
		// Lets take a reference to our metadata
		endpointMetaData = new EndpointMetaData((MessageDrivenMetaData) container.getBeanMetaData());

		// Activate
		activate();
		
		logger.info("Service Started");
	}

	protected void stopService() throws Exception {
		// Deactivate
		// deactivate();
		logger.info("Stopping Service");
		springContainer.stop();
		logger.info("Service Stopped");
	}

	private void activate() throws Exception {
		logger.info("Activating Service for MDB: " + endpointMetaData.getEJBDisplayName());
		
		setupMDBContainer();
		
		Object messageListener = new MessageDrivenInvocationListener(container);
		
		ConnectionFactory connectionFactory = getJNDIItem(
				endpointMetaData.getConnectionFactoryJNDI(), ConnectionFactory.class);
		Destination destination = getJNDIItem(endpointMetaData.getDestinationJNDI(),
				Destination.class);
		
		springContainer = new DefaultMessageListenerContainer();
		springContainer.setConnectionFactory(connectionFactory);
		springContainer.setDestination(destination);
		springContainer.setMessageListener(messageListener);
		springContainer.setConcurrentConsumers(1);
		springContainer.setMaxConcurrentConsumers(endpointMetaData.getReceiverThreads());
		springContainer.setSessionTransacted(true);
		springContainer.setMaxMessagesPerTask(1);
		
		if(endpointMetaData.getAcknowledgeModeName() != null) 
		{
		    logger.info("Setting Acknowledge Mode: " + endpointMetaData.getAcknowledgeModeName());
		    springContainer.setSessionAcknowledgeModeName(endpointMetaData.getAcknowledgeModeName());
		}
		
		if(endpointMetaData.getMessageSelector() != null)
		{
		    logger.info("Configuring Message Selector: " + endpointMetaData.getMessageSelector());
		    springContainer.setMessageSelector(endpointMetaData.getMessageSelector());
		}
		
		springContainer.setRecoveryInterval(endpointMetaData.getRecoveryInterval());
		
		
		TaskExecutor executor = getTaskExecutor();
		springContainer.setTaskExecutor(executor);

		springContainer.afterPropertiesSet();
		springContainer.start();

		logger.info("Service Started for MDB: " + endpointMetaData.getEJBDisplayName());

	}

	/**
	 * @return A task executor for processing Messages
	 */
    private ThreadPoolTaskExecutor getTaskExecutor()
    {
        // Use the JBOSS Work Manager instead ?
        // That will require: JBossWorkManagerTaskExecutor

        /*
        SimpleAsyncTaskExecutor executor = new SimpleAsyncTaskExecutor();
        executor.setDaemon(true);
        */
        
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor(); 
		executor.setCorePoolSize(endpointMetaData.getReceiverThreads());
		executor.setMaxPoolSize(endpointMetaData.getReceiverThreads());
		executor.setDaemon(true);
		executor.afterPropertiesSet();
		executor.setThreadNamePrefix(endpointMetaData.getEJBDisplayName() + "-"); // Allow easier tracking of an EJB's Thread Log
        return executor;
    }

	/**
	 * Returns the Object references, case to the passed in return type
	 * @param <T>
	 * @param jndiName
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private <T> T getJNDIItem(String jndiName, Class<T> clazz) throws Exception {
		InitialContext context = new InitialContext();
		logger.info("Looking Up Object: " + jndiName);
		Object retVal = context.lookup(jndiName);
		logger.info("Found Object of type: " + retVal.getClass());

		return (T) retVal;
	}
	
	
    /**
     * Pre-fill the MDB Container with the MDB instances. Otherwise, the Container will
     * instantiate the Objects within the Transaction for the Message. Some of the MDBs do 
     * work in setMessageContext and expect not to run in a transactional environment.
     * We only want onMessage to be transactional.
     * 
     * This method pre-fills the InstancePool such that Number of Instances in the pool =
     * Number of threads for the MDB.
     */
    private void setupMDBContainer() throws Exception
    {
        InstancePool instancePool = container.getInstancePool();
        
        // Pre-fill the Container with the number of instances as receiver Threads
        int numInstances = endpointMetaData.getReceiverThreads();
        
        if(numInstances > instancePool.getMaxSize())
        {
            // We don't want to 'wait' for instances. Ensure pool has capacity for instance per thread
            throw new Exception("Container Pool Max Size less than MDB " + EndpointMetaData.AC_RECEIVER_THREADS + " Size: " + numInstances + ">" + instancePool.getMaxSize());
        }
        
        logger.info("Filling MDB Pool: " + endpointMetaData.getEJBDisplayName() + "->" + numInstances);
        
        //Retrieve the number of instances here for the number of threads
        List<EnterpriseContext> contexts = new LinkedList<EnterpriseContext>();
        for(int iLoop = 0; iLoop < numInstances; iLoop ++)
        {
            contexts.add(instancePool.get());
        }
        
        // Free the instances, this fills the pool
        for(EnterpriseContext next: contexts)
        {
            instancePool.free(next);
        }
        
        contexts.clear();
    }	
}
