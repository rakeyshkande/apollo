package com.ftd.jms.oracle;

import javax.jms.JMSException;
import javax.jms.Queue;

/**
 *
 */
public class QueueDestinationImpl extends DestinationImpl implements Queue {
	private static final long serialVersionUID = -4094771108172276817L;

	
	public String getQueueName() throws JMSException {
		return owner + "/" + name;
	}	
	
	public String toString() {
		return "QUEUE: "+ owner + "/" + name;
	}
}
