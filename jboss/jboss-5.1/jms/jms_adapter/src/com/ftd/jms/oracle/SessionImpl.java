package com.ftd.jms.oracle;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

import oracle.jms.AQjmsDestination;
import oracle.jms.AQjmsSession;

/**
 * Wraps the AQ JMS Sessions
 */
public class SessionImpl implements Session {
	private static final Logger logger = Logger.getLogger(SessionImpl.class.getName());
	
	
	AQjmsSession delegate;
	
	public SessionImpl(AQjmsSession delegate) {
		this.delegate = delegate;
	}

	public void close() throws JMSException {
		delegate.close();
	}

	public void commit() throws JMSException {
		delegate.commit();
	}

	public QueueBrowser createBrowser(Queue arg0) throws JMSException {
		return delegate.createBrowser(arg0);
	}

	public QueueBrowser createBrowser(Queue arg0, String arg1)
			throws JMSException {
		return delegate.createBrowser(arg0, arg1);
	}

	public BytesMessage createBytesMessage() throws JMSException {
		return delegate.createBytesMessage();
	}

	public MessageConsumer createConsumer(Destination arg0) throws JMSException {
		return delegate.createConsumer(convertDestination(arg0));
	}

	public MessageConsumer createConsumer(Destination arg0, String arg1)
			throws JMSException {
		
		return delegate.createConsumer(convertDestination(arg0), arg1);
	}

	public MessageConsumer createConsumer(Destination arg0, String arg1,
			boolean arg2) throws JMSException {
		return delegate.createConsumer(convertDestination(arg0), arg1, arg2);
	}

	public TopicSubscriber createDurableSubscriber(Topic arg0, String arg1)
			throws JMSException {
		return delegate.createDurableSubscriber(arg0, arg1);
	}

	public TopicSubscriber createDurableSubscriber(Topic arg0, String arg1,
			String arg2, boolean arg3) throws JMSException {
		
		return delegate.createDurableSubscriber(arg0, arg1, arg2, arg3);
	}

	public MapMessage createMapMessage() throws JMSException {
		return delegate.createMapMessage();
	}

	public Message createMessage() throws JMSException {
		return delegate.createMessage();
	}

	public ObjectMessage createObjectMessage() throws JMSException {
		return delegate.createObjectMessage();
	}

	public ObjectMessage createObjectMessage(Serializable arg0)
			throws JMSException {
		
		return delegate.createObjectMessage();
	}

	public Queue createQueue(String arg0) throws JMSException {
		return delegate.createQueue(arg0);
	}

	public StreamMessage createStreamMessage() throws JMSException {
		return delegate.createStreamMessage();
	}

	public TemporaryQueue createTemporaryQueue() throws JMSException {
		return delegate.createTemporaryQueue();
	}

	public TemporaryTopic createTemporaryTopic() throws JMSException {
		return delegate.createTemporaryTopic();
	}

	public TextMessage createTextMessage() throws JMSException {
		return delegate.createTextMessage();
	}

	public TextMessage createTextMessage(String arg0) throws JMSException {
		return delegate.createTextMessage(arg0);
	}

	public Topic createTopic(String arg0) throws JMSException {
		return delegate.createTopic(arg0); 
	}

	public int getAcknowledgeMode() throws JMSException {
		return delegate.getAcknowledgeMode();
	}

	public MessageListener getMessageListener() throws JMSException {
		return delegate.getMessageListener();
	}

	public boolean getTransacted() throws JMSException {
		return delegate.getTransacted();
	}

	public void recover() throws JMSException {
		delegate.recover();		
	}

	public void rollback() throws JMSException {
		delegate.rollback();
	}

	public void run() {
		delegate.run();
	}

	public void setMessageListener(MessageListener arg0) throws JMSException {
		delegate.setMessageListener(arg0);
	}

	public void unsubscribe(String arg0) throws JMSException {
		delegate.unsubscribe(arg0);
	}

	public MessageProducer createProducer(Destination arg0) throws JMSException {
		return delegate.createProducer(convertDestination(arg0));
	}
	
	/**
	 * Converts the destination to an Oracle Destination Implementation
	 * @param destination
	 * @return
	 * @throws JMSException
	 */
	protected Destination convertDestination(Destination destination) throws JMSException {
		
		if(logger.isLoggable(Level.FINEST)) {
			logger.finest("convertDestination( ):" + destination);
		}
		
		if(destination instanceof QueueDestinationImpl) {
			QueueDestinationImpl queue = (QueueDestinationImpl) destination;
			
			Destination actualDestination = queue.getActualDestination();
			if(actualDestination == null)
			{
			    actualDestination = delegate.getQueue(queue.getOwner(), queue.getName());
			    queue.setActualDestination(actualDestination); // Cache the conversion
			}
			
			return actualDestination;
		} else if(destination instanceof AQjmsDestination) {
			return destination;
		}
		
		logger.warning("This Destination Type is unsupported: " + destination);

		return destination;
	}
}
