package com.ftd.jms.oracle.xa;

import javax.jms.JMSException;
import javax.jms.QueueSession;
import javax.jms.XAQueueSession;

import oracle.jms.AQjmsXASession;

public class XAQueueSessionImpl extends XASessionImpl implements XAQueueSession {
	
	AQjmsXASession xadelegate;
	
	XAQueueSessionImpl(AQjmsXASession delegate) {
		super(delegate);
		this.xadelegate = delegate;
	}

    public QueueSession getQueueSession() throws JMSException
    {
        return xadelegate.getQueueSession();
    }


}
