package com.ftd.jms.oracle;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

import oracle.jms.AQjmsSession;


/**
 * This factory creates Oracle JMS destinations.
 * @author Arajoo
 *
 */
public class OracleAqDestinationFactory {

	private static final Logger logger = Logger.getLogger(OracleAqDestinationFactory.class.getName());

	/**
	 * Creates an Oracle Queue Destination
	 * @param connectionFactory
	 * @param queueName
	 * @param queueOwner
	 * @return
	 * @throws Exception
	 */
	public static Queue getQueueDestination(QueueConnectionFactory connectionFactory, String queueOwner, String queueName) throws Exception
	{
		if(logger.isLoggable(Level.FINEST)) {
			logger.finest("Creating Queue Destination: " + queueOwner + "." + queueName);
		}
		
		QueueConnection connection =null;
		AQjmsSession session = null;
		
		try
		{
			connection = connectionFactory.createQueueConnection();
			session = (AQjmsSession) connection.createQueueSession(true, 0);

			Queue retVal = session.getQueue(queueOwner, queueName);
			return retVal;
		} finally
		{
			closeQuietly(session);
			closeQuietly(connection);
		}
	}

	/**
	 * Closes the passed in connection, checking for nulls and exceptions.
	 * @param connection
	 */
	private static void closeQuietly(QueueConnection connection) {
		if(connection != null)
		{
			try
			{
				connection.close();
			} catch (Exception e)
			{
				logger.warning("Error closing connection: " + e.getMessage());
			}
		}
	}

	/**
	 * Closes the passed in session, checking for nulls and exceptions.
	 * @param session
	 */
	private static void closeQuietly(AQjmsSession session) {
		if(session != null)
		{
			try
			{
				session.close();
			} catch (Exception e)
			{
				logger.warning("Error closing session: " + e.getMessage());
			}
		}
	}
}
