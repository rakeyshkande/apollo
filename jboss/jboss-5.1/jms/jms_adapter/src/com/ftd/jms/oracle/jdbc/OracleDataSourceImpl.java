package com.ftd.jms.oracle.jdbc;

import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Provides a wrapper implementation for an Oracle Data source to 
 * provide unwrapping of the connections from the Wrapped JBOSS
 * connections
 */
public class OracleDataSourceImpl implements DataSource, Serializable
{
    private static final long serialVersionUID = 1884926894900738572L;

    private static final Logger logger = Logger.getLogger(OracleDataSourceImpl.class.getName());
        
    DataSource delegate;
    String dataSourceJNDI;
    
    public OracleDataSourceImpl()
    {
    }
    
    public void setDataSource(DataSource datasource)
    {
        this.delegate = datasource;
        logger.info("DataSource: " + delegate);
    }
    
    public void setDataSourceJNDI(String datasourceJNDI)
    {
        this.dataSourceJNDI = datasourceJNDI;
        logger.info("DataSource JNDI: " + this.dataSourceJNDI);
    }
    
    /**
     * Return the data source or looks up the JNDI Name and returns the
     * response. Throws an exception if the lookup fails
     * @return
     */
    private DataSource getDataSource() 
    {
        if(delegate != null)
        {
            return delegate;
        }
        
        // Lookup the Delegate
        try
        {
            InitialContext context = new InitialContext();
            DataSource theDS = (DataSource) context.lookup(dataSourceJNDI);
            setDataSource(theDS);
            return delegate;
        }
        catch (Throwable t)
        {
            throw new RuntimeException("Failed to retrieve DS: " + dataSourceJNDI + ": " + t.getMessage(), t);
        }
    }
    
    
    public PrintWriter getLogWriter() throws SQLException
    {
        return getDataSource().getLogWriter();
    }

    public void setLogWriter(PrintWriter out) throws SQLException
    {
        getDataSource().setLogWriter(out);
    }

    public void setLoginTimeout(int seconds) throws SQLException
    {
        getDataSource().setLoginTimeout(seconds);
    }

    public int getLoginTimeout() throws SQLException
    {
        return getDataSource().getLoginTimeout();
    }    

    public <T> T unwrap(Class<T> iface) throws SQLException
    {
        return null;
    }

    public boolean isWrapperFor(Class<?> iface) throws SQLException
    {
        return false;
    }    
    
    
    public Connection getConnection() throws SQLException
    {
        logger.finest("getConnection()");
        return new OracleConnectionImpl(getDataSource().getConnection());
    }

    public Connection getConnection(String username, String password) throws SQLException
    {
        if(logger.isLoggable(Level.FINEST))
            logger.finest("getConnection()" + username + ";" + password);
        
        return new OracleConnectionImpl(getDataSource().getConnection(username, password));
    }


}
