package com.ftd.jms.oracle;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.ConnectionConsumer;
import javax.jms.ConnectionMetaData;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.ServerSessionPool;
import javax.jms.Session;
import javax.jms.Topic;
import oracle.jms.AQjmsSession;

public class QueueConnectionImpl implements QueueConnection  {
	QueueConnection delegate;
	
	private static final Logger logger = Logger.getLogger(QueueConnectionImpl.class.getName());

	/**
	 * Constructor that takes in a delegate for the Queue Connection
	 * @param delegate
	 */
	protected QueueConnectionImpl(QueueConnection delegate) {
		this.delegate = delegate;		
	}
	
	protected QueueConnectionImpl(Connection delegate) {
		this.delegate = (QueueConnection) delegate;		
	}

	public void close() throws JMSException {
		delegate.close();
	}

	public ConnectionConsumer createConnectionConsumer(Destination destination,
			String messageSelector, ServerSessionPool serverSessionPool, int maxMessages) throws JMSException {
		
		if(logger.isLoggable(Level.FINEST)) {		
			logger.info("createConnectionConsumer(): " + destination);
		}
		
		// This method is unsupported by the Oracle Library
		return delegate.createConnectionConsumer(destination, messageSelector, serverSessionPool, maxMessages);
	}

	public ConnectionConsumer createDurableConnectionConsumer(Topic arg0,
			String arg1, String arg2, ServerSessionPool arg3, int arg4)
			throws JMSException {
		
		return delegate.createDurableConnectionConsumer(arg0, arg1, arg2, arg3, arg4);
	}

	public Session createSession(boolean arg0, int arg1) throws JMSException {
		
		if(logger.isLoggable(Level.FINEST)) {		
			logger.info("createSession(): " + arg0 + ":" + arg1);
		}
		
		PlatformTransactionManagerBinding.setupTransactionManagerBinding();

		return new SessionImpl((AQjmsSession) delegate.createSession(arg0, arg1));
	}

	public String getClientID() throws JMSException {
		return delegate.getClientID();
	}

	public ExceptionListener getExceptionListener() throws JMSException {
		return delegate.getExceptionListener();
	}

	public ConnectionMetaData getMetaData() throws JMSException {
		return delegate.getMetaData();
	}

	public void setClientID(String arg0) throws JMSException {
		delegate.setClientID(arg0);
	}

	public void setExceptionListener(ExceptionListener arg0)
			throws JMSException {
		delegate.setExceptionListener(arg0);
	}

	public void start() throws JMSException {
		delegate.start();
	}

	public void stop() throws JMSException {
		delegate.stop();
	}

	public ConnectionConsumer createConnectionConsumer(Queue arg0, String arg1,
			ServerSessionPool arg2, int arg3) throws JMSException {
		
		if(logger.isLoggable(Level.FINEST)) {		
			logger.info("createConnectionConsumer(): " + arg0);
		}
		
		return delegate.createConnectionConsumer(arg0, arg1, arg2, arg3);
	}

	public QueueSession createQueueSession(boolean arg0, int arg1)
			throws JMSException {
		if(logger.isLoggable(Level.FINEST)) {		
			logger.info("createQueueSession(): " + arg0 + ":" + arg1);
		}
		
		PlatformTransactionManagerBinding.setupTransactionManagerBinding();
		
		return new QueueSessionImpl((AQjmsSession) delegate.createQueueSession(arg0, arg1));
	}

}
