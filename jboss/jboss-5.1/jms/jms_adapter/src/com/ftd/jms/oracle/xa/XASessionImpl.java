package com.ftd.jms.oracle.xa;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.XASession;
import javax.transaction.xa.XAResource;

import oracle.jms.AQjmsSession;
import oracle.jms.AQjmsXAResource;
import oracle.jms.AQjmsXASession;

import com.ftd.jms.oracle.SessionImpl;

/**
 * Wraps the AQ JMS Sessions
 */
public class XASessionImpl extends SessionImpl implements XASession {
	
	AQjmsXASession xadelegate;
	
	XASessionImpl(AQjmsXASession delegate) {
	    super(delegate);
		this.xadelegate = delegate;
	}

    public Session getSession() throws JMSException
    {
        return new SessionImpl((AQjmsSession) xadelegate.getSession());
    }

    public XAResource getXAResource()
    {
        return new XAResourceImpl((AQjmsXAResource) xadelegate.getXAResource());
    }
	
	


}
