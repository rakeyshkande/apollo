package com.ftd.jms.endpoint;

import org.jboss.metadata.ActivationConfigPropertyMetaData;
import org.jboss.metadata.MessageDrivenMetaData;

/**
 * Wraps the passed in Meta data and returns specific information from it
 * @author Arajoo
 *
 */
@SuppressWarnings("deprecation")
public class EndpointMetaData {
	protected MessageDrivenMetaData messageDrivenMetaData;
	
	static final String AC_CONNECTION_FACTORY_JNDI="ConnectionFactoryJndiName";
	static final String AC_DESTINATION_JNDI="DestinationJndiName";
	static final String AC_RECEIVER_THREADS="MaxPoolSize";
	static final String AC_MESSAGE_SELECTOR = "MessageSelector";
	static final String AC_REDELIVERY_INTERVAL = "RedeliveryInterval";
	static final String AC_ACKNOWLEDGE_MODE = "AcknowledgeMode";	
	
	public EndpointMetaData(MessageDrivenMetaData metaData)
	{
		this.messageDrivenMetaData = metaData;
	}
	
	public String getEjbClass() 
	{
		return messageDrivenMetaData.getEjbClass();
	}
	
	public String getConnectionFactoryJNDI()
	{
		return getActivationConfig(AC_CONNECTION_FACTORY_JNDI, true);
	}
	
	public String getDestinationJNDI()
	{
		return getActivationConfig(AC_DESTINATION_JNDI, true);
	}
	
	public int getReceiverThreads() 
	{
		return getActivationConfigInt(AC_RECEIVER_THREADS, 1);
	}
		
	/**
	 * @return Null if not specified or is defined as an empty string
	 */
	public String getMessageSelector()
	{
	    String retVal = getActivationConfig(AC_MESSAGE_SELECTOR, false);
	    
	    // Make empty selector null
	    if(retVal != null && retVal.trim().length() == 0)
	    {
	        retVal = null;
	    }
	    
	    return retVal;
	}
	
	/**
	 * @return Null if not specified, or is defined as an empty String
	 */
    public String getAcknowledgeModeName()
    {
        String retVal = getActivationConfig(AC_ACKNOWLEDGE_MODE, false);
        
        // Make empty selector null
        if(retVal != null && retVal.trim().length() == 0)
        {
            retVal = null;
        }
        
        if("AutoAcknowledge".equals(retVal))
        {
            retVal = "AUTO_ACKNOWLEDGE";
        } else if("ClientAcknowledge".equals(retVal))
        {   
            retVal = "CLIENT_ACKNOWLEDGE";
        } else if("DupsOkAcknowledge".equals(retVal))
        {
            retVal = "DUPS_OK_ACKNOWLEDGE";
        } else 
        {
            retVal = "AUTO_ACKNOWLEDGE"; // Default
        }

        return retVal;
    }
	
	/**
	 * @return Recovery Interval in milliseconds. Default = 5000
	 */
    public long getRecoveryInterval()
    {
        // value is in seconds in ejb-jar.xml
        int interval = getActivationConfigInt(AC_REDELIVERY_INTERVAL, 5);
        if(interval < 5)
        {
            interval = 5;
        }

        return interval * 1000;
    }	
	
	
	/**
	 * Returns the requested Activation Configuration Value
	 * Throws an IllegalArgumentException if the value is not available and throwException is true.
	 * @param config
	 * @return
	 */
	private String getActivationConfig(String config, boolean throwException)
	{
		ActivationConfigPropertyMetaData item = messageDrivenMetaData.getActivationConfigProperties().get(config);
		
		if(item == null) 
		{
			if(throwException)
			{
				throw new IllegalArgumentException("Missing Activation Config: " + config);
			}
			
			return null;
		}
		
		return item.getValue().trim();
	}
	
	
	/**
	 * Returns the requested Activation Configuration Value
	 * Returns the default value if the config is not found
	 * @param config
	 * @return
	 */
	private int getActivationConfigInt(String config, int defaultValue)
	{	
		String val = getActivationConfig(config, false);
		
		if(val == null)
		{
			return defaultValue;
		}
		
		try
		{
			return Integer.parseInt(val);
		} catch (Exception e)
		{
			throw new IllegalArgumentException("Invalid Integer Value for Config: " + config + "=" + val);
		}
	}

	/**
	 * @return Returns the Messaging Type
	 */
	public String getMessagingType() 
	{
		return messageDrivenMetaData.getMessagingType();
	}
	
	
	/**
	 * Returns a Display Name for the EJB. If name is provided, returns that.
	 * Otherwise, returns the Class Name of the EJB class.
	 * @return
	 */
	public String getEJBDisplayName()
	{
	    String retVal = messageDrivenMetaData.getEjbName();
	    
	    if(retVal != null && retVal.trim().length() > 0)
	    {
	        return retVal;
	    }
	    
	    retVal = this.getEjbClass().trim();
	    
	    int nameIndex = retVal.lastIndexOf(".");
	    if(nameIndex < 0)
	    {
	        return retVal;
	    }
	    
	    return retVal.substring(nameIndex + 1);
	}


}
