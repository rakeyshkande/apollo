package com.ftd.jms.oracle;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

import oracle.jms.AQjmsSession;

public class QueueSessionImpl extends SessionImpl implements QueueSession {
	private static final Logger logger = Logger.getLogger(QueueSessionImpl.class.getName());
	
	QueueSessionImpl(AQjmsSession delegate) {
		super(delegate);
	}

	public QueueReceiver createReceiver(Queue arg0) throws JMSException {
		return delegate.createReceiver((Queue) convertDestination(arg0));
	}

	public QueueReceiver createReceiver(Queue arg0, String arg1)
			throws JMSException {
		return delegate.createReceiver((Queue) convertDestination(arg0), arg1);
	}

	public QueueSender createSender(Queue arg0) throws JMSException {
		if(logger.isLoggable(Level.FINEST)) {		
			logger.info("createSender(): " + arg0);
		}

		return new QueueSenderImpl(delegate.createSender((Queue) convertDestination(arg0)));
	}

}
