package com.ftd.jms.oracle.xa;

import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.XAQueueConnection;
import javax.jms.XAQueueSession;
import javax.jms.XASession;

import oracle.jms.AQjmsXASession;

import com.ftd.jms.oracle.PlatformTransactionManagerBinding;
import com.ftd.jms.oracle.QueueConnectionImpl;

public class XAQueueConnectionImpl extends QueueConnectionImpl implements XAQueueConnection  {
    
    private static final Logger logger = Logger.getLogger(XAQueueConnectionImpl.class.getName());
    
	XAQueueConnection xadelegate;
	
	/**
	 * Constructor that takes in a delegate for the Queue Connection
	 * @param delegate
	 */
	XAQueueConnectionImpl(XAQueueConnection delegate) {
		super(delegate);		
		xadelegate = delegate;
	}
	
	XAQueueConnectionImpl(Connection delegate) {
		this((XAQueueConnection) delegate);		
	}

    public XASession createXASession() throws JMSException
    {
        logger.info("createXASession()" );
        PlatformTransactionManagerBinding.setupTransactionManagerBinding();
        
        return new XASessionImpl((AQjmsXASession) xadelegate.createXASession());
    }

    public XAQueueSession createXAQueueSession() throws JMSException
    {
        logger.info("createXAQueueSession()" );
        
        PlatformTransactionManagerBinding.setupTransactionManagerBinding();
        
        return new XAQueueSessionImpl((AQjmsXASession) xadelegate.createXAQueueSession());
    }




}
