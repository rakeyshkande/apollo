package com.ftd.jms.oracle.xa;

import java.util.logging.Logger;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import oracle.jms.AQjmsXAResource;

public class XAResourceImpl implements XAResource
{
    AQjmsXAResource delegate;
    
    private static final Logger logger = Logger.getLogger(XAResourceImpl.class.getName());
    
    protected XAResourceImpl(AQjmsXAResource delegate)
    {
        this.delegate = delegate;
    }

    public void commit(Xid arg0, boolean arg1) throws XAException
    {
        delegate.commit(arg0, arg1);
    }

    public void end(Xid arg0, int arg1) throws XAException
    {
        delegate.end(arg0, arg1);

    }

    public void forget(Xid arg0) throws XAException
    {
        delegate.forget(arg0);
    }

    public int getTransactionTimeout() throws XAException
    {
        return delegate.getTransactionTimeout();
    }

    public boolean isSameRM(XAResource xares) throws XAException
    {
        boolean retVal = false;
        if(xares instanceof XAResourceImpl)
        {
            XAResourceImpl actualXares = (XAResourceImpl) xares;
            
            retVal = delegate.isSameRM(actualXares.delegate);
        } else 
        {
            retVal = delegate.isSameRM(xares);
        }
        
        logger.info("isSameRM: " + xares + ": " + retVal);
        
        return retVal;
    }

    public int prepare(Xid xid) throws XAException
    {
        return delegate.prepare(xid);
    }

    public Xid[] recover(int flag) throws XAException
    {
        return delegate.recover(flag);
    }

    public void rollback(Xid xid) throws XAException
    {
        delegate.rollback(xid);
    }

    public boolean setTransactionTimeout(int seconds) throws XAException
    {
        return delegate.setTransactionTimeout(seconds);
    }

    public void start(Xid xid, int flags) throws XAException
    {
        delegate.start(xid, flags);
    }

}
