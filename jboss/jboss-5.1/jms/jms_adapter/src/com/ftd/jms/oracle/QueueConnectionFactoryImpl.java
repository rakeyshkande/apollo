package com.ftd.jms.oracle;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

import oracle.jms.AQjmsQueueConnectionFactory;

/**
 * Wrapper Implementation for the Oracle Queue Connection Factory
 */
public class QueueConnectionFactoryImpl implements QueueConnectionFactory, Serializable{

	private static final long serialVersionUID = -6714309660601820700L;

	private static final Logger logger = Logger.getLogger(QueueConnectionFactoryImpl.class.getName());

	AQjmsQueueConnectionFactory delegate;
	
	/**
	 * Constructor that takes in a passed in factory as a delegate
	 * Defaults the bindTransactionManager flag to true
	 * @param delegate
	 */
	public QueueConnectionFactoryImpl(AQjmsQueueConnectionFactory delegate) {
	    this(delegate, Boolean.TRUE);
	}

	/**
	 * Constructor that takes in a passed in factory as a delegate and a flag indicating
	 * if the JBOSS TransactionManager should be exposed to the Oracle Queue API
	 * @param delegate
	 * @param bindTransactionManager
	 */
    public QueueConnectionFactoryImpl(AQjmsQueueConnectionFactory delegate, Boolean bindTransactionManager) {
        this.delegate = delegate;
        PlatformTransactionManagerBinding.setBindTransactionManager(bindTransactionManager.booleanValue());
    }	

	public Connection createConnection() throws JMSException {
		if(logger.isLoggable(Level.FINEST)) {
			logger.info("createConnection()" );
		}
				
		return new QueueConnectionImpl(delegate.createConnection());
	}

	public Connection createConnection(String arg0, String arg1)
			throws JMSException {

		if(logger.isLoggable(Level.FINEST)) {
			logger.info("createConnection(): " + arg0 + ": " + arg1);
		}
		
		return new QueueConnectionImpl(delegate.createConnection(arg0, arg1));
	}

	public QueueConnection createQueueConnection() throws JMSException {
		
		if(logger.isLoggable(Level.FINEST)) {
			logger.info("createQueueConnection()" );
		}	
		
		return new QueueConnectionImpl(delegate.createQueueConnection());
	}

	public QueueConnection createQueueConnection(String arg0, String arg1)
			throws JMSException {
		if(logger.isLoggable(Level.FINEST)) {
			logger.info("createQueueConnection(): " + arg0 + ": " + arg1);
		}
        
		return new QueueConnectionImpl(delegate.createQueueConnection());
	}

}
