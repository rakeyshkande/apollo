package com.ftd.jms.oracle.xa;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.XAConnection;
import javax.jms.XAQueueConnection;
import javax.jms.XAQueueConnectionFactory;

import oracle.jms.AQjmsXAQueueConnectionFactory;

import com.ftd.jms.oracle.QueueConnectionFactoryImpl;

/**
 * Wrapper Implementation for the Oracle Queue Connection Factory
 */
public class XAQueueConnectionFactoryImpl extends QueueConnectionFactoryImpl implements XAQueueConnectionFactory, Serializable{

	private static final long serialVersionUID = -6714309660601820700L;

	private static final Logger logger = Logger.getLogger(XAQueueConnectionFactoryImpl.class.getName());

	AQjmsXAQueueConnectionFactory xadelegate;
	
	/**
	 * Constructor that takes in a passed in factory as a delegate
	 * @param delegate
	 */
	public XAQueueConnectionFactoryImpl(AQjmsXAQueueConnectionFactory delegate) 
	{
		this(delegate, Boolean.FALSE);

	}
	
	public XAQueueConnectionFactoryImpl(AQjmsXAQueueConnectionFactory delegate, Boolean bindTransactionManager) 
	{
	    super(delegate, bindTransactionManager);
        xadelegate = delegate;
        
        if(logger.isLoggable(Level.FINEST))
            logger.info("XAQueueConnectionFactoryImpl:: " + delegate);	    
	}

    public XAConnection createXAConnection() throws JMSException
    {
        if(logger.isLoggable(Level.FINEST)) {
            logger.info("createXAConnection()" );
        }
        
        return new XAQueueConnectionImpl(xadelegate.createXAConnection());
    }

    public XAConnection createXAConnection(String arg0, String arg1) throws JMSException
    {
        if(logger.isLoggable(Level.FINEST)) {
            logger.info("createXAConnection(): " + arg0 + "," + arg1);
        }
        
        return new XAQueueConnectionImpl(xadelegate.createXAConnection(arg0, arg1));
    }

    public XAQueueConnection createXAQueueConnection() throws JMSException
    {
        if(logger.isLoggable(Level.FINEST)) {
            logger.info("createXAQueueConnection()" );
        }
               
        return new XAQueueConnectionImpl(xadelegate.createXAQueueConnection());
    }

    public XAQueueConnection createXAQueueConnection(String arg0, String arg1) throws JMSException
    {
        if(logger.isLoggable(Level.FINEST)) {
            logger.info("createXAQueueConnection(): " + arg0 + "," + arg1);
        }
        
        return new XAQueueConnectionImpl(xadelegate.createXAQueueConnection(arg0, arg1));
    }


}
