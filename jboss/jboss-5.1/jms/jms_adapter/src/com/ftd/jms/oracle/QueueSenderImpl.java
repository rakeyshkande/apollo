package com.ftd.jms.oracle;

import java.io.Serializable;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueSender;

public class QueueSenderImpl implements QueueSender, Serializable {
	
	private static final long serialVersionUID = 7087641808999716291L;
	QueueSender delegate;
	
	public QueueSenderImpl(QueueSender delegate) {
		this.delegate = delegate;
		
	}

	public void close() throws JMSException {
		delegate.close();
	}

	public int getDeliveryMode() throws JMSException {
		return delegate.getDeliveryMode();
	}

	public Destination getDestination() throws JMSException {
		return delegate.getDestination();
	}

	public boolean getDisableMessageID() throws JMSException {
		return delegate.getDisableMessageID();
	}

	public boolean getDisableMessageTimestamp() throws JMSException {
		return delegate.getDisableMessageTimestamp();
	}

	public int getPriority() throws JMSException {
		return delegate.getPriority();
	}

	public long getTimeToLive() throws JMSException {
		return delegate.getTimeToLive();
	}

	public void send(Destination arg0, Message arg1) throws JMSException {
		delegate.send(arg0, arg1);
	}

	public void send(Destination arg0, Message arg1, int arg2, int arg3,
			long arg4) throws JMSException {
		delegate.send(arg0, arg1, arg2, arg3, arg4);
	}

	public void setDeliveryMode(int arg0) throws JMSException {
		delegate.setDeliveryMode(arg0);
		
	}

	public void setDisableMessageID(boolean arg0) throws JMSException {
		delegate.setDisableMessageID(arg0);
	}

	public void setDisableMessageTimestamp(boolean arg0) throws JMSException {
		delegate.setDisableMessageTimestamp(arg0);
	}

	public void setPriority(int arg0) throws JMSException {
		delegate.setPriority(arg0);
	}

	public void setTimeToLive(long arg0) throws JMSException {
		delegate.setTimeToLive(arg0);
	}

	public Queue getQueue() throws JMSException {
		return delegate.getQueue();
	}

	public void send(Message arg0) throws JMSException {
		delegate.send(arg0);
	}

	public void send(Queue arg0, Message arg1) throws JMSException {
		delegate.send(arg0, arg1);
	}

	public void send(Message arg0, int arg1, int arg2, long arg3)
			throws JMSException {
		delegate.send(arg0, arg1, arg2, arg3);
	}

	public void send(Queue arg0, Message arg1, int arg2, int arg3, long arg4)
			throws JMSException {
		delegate.send(arg0, arg1, arg2, arg3, arg4);
	}
	

}
