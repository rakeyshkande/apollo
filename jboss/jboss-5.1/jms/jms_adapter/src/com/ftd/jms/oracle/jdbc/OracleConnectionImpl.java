package com.ftd.jms.oracle.jdbc;

import java.lang.reflect.Method;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.transaction.xa.XAResource;

import oracle.jdbc.OracleOCIFailover;
import oracle.jdbc.OracleSavepoint;
import oracle.jdbc.internal.OracleConnection;
import oracle.jdbc.internal.OracleStatement;
import oracle.jdbc.oracore.OracleTypeADT;
import oracle.jdbc.oracore.OracleTypeCLOB;
import oracle.jdbc.pool.OracleConnectionCacheCallback;
import oracle.jdbc.pool.OraclePooledConnection;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.BFILE;
import oracle.sql.BLOB;
import oracle.sql.BfileDBAccess;
import oracle.sql.BlobDBAccess;
import oracle.sql.CLOB;
import oracle.sql.ClobDBAccess;
import oracle.sql.CustomDatum;
import oracle.sql.Datum;
import oracle.sql.StructDescriptor;

/**
 * Wrapper class that provides unwrapping of connections provided by JBOSS to
 * retrieve the internal Oracle Connection.
 * Delegates all calls to the internal Oracle Connection, except those pertaining
 * to Connection close() and Transaction methods. These methods are delegated to the
 * JBOSS connection.
 */
public class OracleConnectionImpl implements OracleConnection
{
    private static final Logger logger = Logger.getLogger(OracleConnectionImpl.class.getName());
    
    Connection wrappedConnection;
    OracleConnection internalConnection;
    
    public OracleConnectionImpl(Connection wrappedConnection)
    {
        this.wrappedConnection = wrappedConnection;
        internalConnection = getOracleConnection(wrappedConnection);
    }
    
    /**
     * Unwraps an Oracle Connection from the passed in Connection which may be
     * a ConnectionWrapper from JBOSS
     * @param connection
     * @return
     */
    private OracleConnection getOracleConnection(Connection connection)
    {
        // If we are running in OC4J, this is already an OracleConnection
        if (connection == null || connection instanceof OracleConnection) {
            return (OracleConnection) connection;
        }         
        
        try
        {
            Method getUnderlyingConnectionMethod = connection.getClass().getMethod("getUnderlyingConnection", (Class[]) null);
            Connection retVal = (Connection) getUnderlyingConnectionMethod.invoke(connection, (Object[]) null);
            
            if(retVal instanceof OracleConnection)
            {
                return (OracleConnection) retVal;
            }
            
            logger.fine("Underlying connection not an Oracle connection: " + retVal.getClass().getName());
            
            // Recurse
            return getOracleConnection(retVal);
        }
        catch (Exception e)
        {
            logger.warning("Unable to unwrap connection. Not a JBOSS Wrapped Connection: " + e.getMessage());
        }
        
        return null;
    }    

    
    public Connection _getPC()
    {
        return internalConnection._getPC();
    }

    
    public void applyConnectionAttributes(Properties arg0) throws SQLException
    {
        internalConnection.applyConnectionAttributes(arg0);
    }

    
    public void archive(int arg0, int arg1, String arg2) throws SQLException
    {
        internalConnection.archive(arg0, arg1, arg2);
    }

    public void close() throws SQLException
    {
        logger.finest("close()");
        wrappedConnection.close();
    }

    
    public boolean isClosed() throws SQLException
    {
        if(logger.isLoggable(Level.FINEST))
        {
            logger.finest("isClosed: " + wrappedConnection.isClosed());
            
            if(wrappedConnection.isClosed())
            {
                logStackTrace("isClosed: ");
            }
        }
        
        return wrappedConnection.isClosed();
    }
    
    public void close(Properties arg0) throws SQLException
    {
        logger.warning("Close with Properties called,delegating to close(): " + arg0);
        close();
    }

    
    public void close(int arg0) throws SQLException
    {
        logger.warning("Close with arg called,delegating to close(): " + arg0);
        close();
    }

    
    public boolean getAutoClose() throws SQLException
    {
        if(logger.isLoggable(Level.FINEST))
        {
            logger.finest("getAutoClose: " + internalConnection.getAutoClose());
        }
        
        return internalConnection.getAutoClose();
    }

    
    public CallableStatement getCallWithKey(String arg0) throws SQLException
    {
        return internalConnection.getCallWithKey(arg0);
    }

    
    public Properties getConnectionAttributes() throws SQLException
    {
        return internalConnection.getConnectionAttributes();
    }

    
    public int getConnectionReleasePriority() throws SQLException
    {
        return internalConnection.getConnectionReleasePriority();
    }

    
    public boolean getCreateStatementAsRefCursor()
    {
        return internalConnection.getCreateStatementAsRefCursor();
    }

    
    public String getCurrentSchema() throws SQLException
    {
        return internalConnection.getCurrentSchema();
    }

    
    public int getDefaultExecuteBatch()
    {
        return internalConnection.getDefaultExecuteBatch();
    }

    
    public int getDefaultRowPrefetch()
    {
        return internalConnection.getDefaultRowPrefetch();
    }

    
    public Object getDescriptor(String arg0)
    {
        return internalConnection.getDescriptor(arg0);
    }

    
    public short getEndToEndECIDSequenceNumber() throws SQLException
    {
        return internalConnection.getEndToEndECIDSequenceNumber();
    }

    
    public String[] getEndToEndMetrics() throws SQLException
    {
        return internalConnection.getEndToEndMetrics();
    }

    
    public boolean getExplicitCachingEnabled() throws SQLException
    {
        return internalConnection.getExplicitCachingEnabled();
    }

    
    public boolean getImplicitCachingEnabled() throws SQLException
    {
        return internalConnection.getImplicitCachingEnabled();
    }

    
    public boolean getIncludeSynonyms()
    {
        return internalConnection.getIncludeSynonyms();
    }

    
    public Object getJavaObject(String arg0) throws SQLException
    {
        return internalConnection.getJavaObject(arg0);
    }

    
    public Properties getProperties()
    {
        return internalConnection.getProperties();
    }

    
    public boolean getRemarksReporting()
    {
        return internalConnection.getRemarksReporting();
    }

    
    public boolean getRestrictGetTables()
    {
        return internalConnection.getRestrictGetTables();
    }

    
    public String getSQLType(Object arg0) throws SQLException
    {
        return internalConnection.getSQLType(arg0);
    }

    
    public String getSessionTimeZone()
    {
        return internalConnection.getSessionTimeZone();
    }

    
    public int getStatementCacheSize() throws SQLException
    {
        return internalConnection.getStatementCacheSize();
    }

    
    public PreparedStatement getStatementWithKey(String arg0) throws SQLException
    {
        return internalConnection.getStatementWithKey(arg0);
    }

    
    public int getStmtCacheSize()
    {
        return internalConnection.getStmtCacheSize();
    }

    
    public short getStructAttrCsId() throws SQLException
    {
        return internalConnection.getStructAttrCsId();
    }

    
    public Properties getUnMatchedConnectionAttributes() throws SQLException
    {
        return internalConnection.getUnMatchedConnectionAttributes();
    }

    
    public String getUserName() throws SQLException
    {
        return internalConnection.getUserName();
    }

    
    public boolean getUsingXAFlag()
    {
        return internalConnection.getUsingXAFlag();
    }

    
    public boolean getXAErrorFlag()
    {
        return internalConnection.getXAErrorFlag();
    }

    
    public boolean isLogicalConnection()
    {
        return internalConnection.isLogicalConnection();
    }

    
    public boolean isProxySession()
    {
        return internalConnection.isProxySession();
    }

    
    public void openProxySession(int arg0, Properties arg1) throws SQLException
    {
        internalConnection.openProxySession(arg0, arg1);
    }

    
    public void oracleReleaseSavepoint(OracleSavepoint arg0) throws SQLException
    {
        internalConnection.oracleReleaseSavepoint(arg0);
    }

    
    public void oracleRollback(OracleSavepoint arg0) throws SQLException
    {
        internalConnection.oracleRollback(arg0);
    }

    
    public OracleSavepoint oracleSetSavepoint() throws SQLException
    {
        return internalConnection.oracleSetSavepoint();
    }

    
    public OracleSavepoint oracleSetSavepoint(String arg0) throws SQLException
    {
        return internalConnection.oracleSetSavepoint(arg0);
    }

    
    public OracleConnection physicalConnectionWithin()
    {
        return internalConnection.physicalConnectionWithin();
    }

    
    public int pingDatabase(int arg0) throws SQLException
    {
        return internalConnection.pingDatabase(arg0);
    }

    
    public CallableStatement prepareCallWithKey(String arg0) throws SQLException
    {
        return internalConnection.prepareCallWithKey(arg0);
    }

    
    public PreparedStatement prepareStatementWithKey(String arg0) throws SQLException
    {
        return internalConnection.prepareStatementWithKey(arg0);
    }

    
    public void purgeExplicitCache() throws SQLException
    {
        internalConnection.purgeExplicitCache();
    }

    
    public void purgeImplicitCache() throws SQLException
    {
        internalConnection.purgeImplicitCache();
    }

    
    public void putDescriptor(String arg0, Object arg1) throws SQLException
    {
        internalConnection.putDescriptor(arg0, arg1);
    }

    
    public void registerConnectionCacheCallback(OracleConnectionCacheCallback arg0, Object arg1, int arg2)
            throws SQLException
    {
        internalConnection.registerConnectionCacheCallback(arg0, arg1, arg2);
    }

    
    public void registerSQLType(String arg0, Class arg1) throws SQLException
    {
        internalConnection.registerSQLType(arg0, arg1);
    }

    
    public void registerSQLType(String arg0, String arg1) throws SQLException
    {
        internalConnection.registerSQLType(arg0, arg1);
    }

    
    public void registerTAFCallback(OracleOCIFailover arg0, Object arg1) throws SQLException
    {
        internalConnection.registerTAFCallback(arg0, arg1);
    }

    
    public void setAutoClose(boolean arg0) throws SQLException
    {
        logger.finest("Set Auto Close: " + arg0);
        internalConnection.setAutoClose(arg0);
    }

    
    public void setConnectionReleasePriority(int arg0) throws SQLException
    {
        internalConnection.setConnectionReleasePriority(arg0);
    }

    
    public void setCreateStatementAsRefCursor(boolean arg0)
    {
        internalConnection.setCreateStatementAsRefCursor(arg0);
    }

    
    public void setDefaultExecuteBatch(int arg0) throws SQLException
    {
        internalConnection.setDefaultExecuteBatch(arg0);
    }

    
    public void setDefaultRowPrefetch(int arg0) throws SQLException
    {
        internalConnection.setDefaultRowPrefetch(arg0);
    }

    
    public void setEndToEndMetrics(String[] arg0, short arg1) throws SQLException
    {
        internalConnection.setEndToEndMetrics(arg0, arg1);
    }

    
    public void setExplicitCachingEnabled(boolean arg0) throws SQLException
    {
        internalConnection.setExplicitCachingEnabled(arg0);
    }

    
    public void setImplicitCachingEnabled(boolean arg0) throws SQLException
    {
        internalConnection.setImplicitCachingEnabled(arg0);
    }

    
    public void setIncludeSynonyms(boolean arg0)
    {
        internalConnection.setIncludeSynonyms(arg0);
    }

    
    public void setPlsqlWarnings(String arg0) throws SQLException
    {
        internalConnection.setPlsqlWarnings(arg0);
    }

    
    public void setRemarksReporting(boolean arg0)
    {
        internalConnection.setRemarksReporting(arg0);
    }

    
    public void setRestrictGetTables(boolean arg0)
    {
        internalConnection.setRestrictGetTables(arg0);
    }

    
    public void setSessionTimeZone(String arg0) throws SQLException
    {
        internalConnection.setSessionTimeZone(arg0);
    }

    
    public void setStatementCacheSize(int arg0) throws SQLException
    {
        internalConnection.setStatementCacheSize(arg0);
    }

    
    public void setStmtCacheSize(int arg0) throws SQLException
    {
        internalConnection.setStmtCacheSize(arg0);
    }

    
    public void setStmtCacheSize(int arg0, boolean arg1) throws SQLException
    {
        internalConnection.setStmtCacheSize(arg0, arg1);
    }

    
    public void setUsingXAFlag(boolean arg0)
    {
        internalConnection.setUsingXAFlag(arg0);
    }

    
    public void setWrapper(oracle.jdbc.OracleConnection arg0)
    {
        internalConnection.setWrapper(arg0);
    }

    
    public void setXAErrorFlag(boolean arg0)
    {
        internalConnection.setXAErrorFlag(arg0);   
    }

    
    public void shutdown(int arg0) throws SQLException
    {
        internalConnection.shutdown(arg0);
    }

    
    public void startup(String arg0, int arg1) throws SQLException
    {
        internalConnection.startup(arg0, arg1);
    }

    
    public oracle.jdbc.OracleConnection unwrap()
    {
        return internalConnection.unwrap();
    }

    
    public Statement createStatement() throws SQLException
    {
        return internalConnection.createStatement();
    }

    
    public PreparedStatement prepareStatement(String sql) throws SQLException
    {
        return internalConnection.prepareStatement(sql);
    }

    
    public CallableStatement prepareCall(String sql) throws SQLException
    {
        return internalConnection.prepareCall(sql);
    }

    
    public String nativeSQL(String sql) throws SQLException
    {
        return internalConnection.nativeSQL(sql);
    }

    
    public void setAutoCommit(boolean autoCommit) throws SQLException
    {
        logger.finest("setAutoCommit: " + autoCommit);
        
        if(autoCommit == wrappedConnection.getAutoCommit())
        {
            return; // No Change
        }
        
        // If we are in CMT, this will cause issues. For now, assume above will address
        wrappedConnection.setAutoCommit(autoCommit);
    }

    
    public boolean getAutoCommit() throws SQLException
    {
        return wrappedConnection.getAutoCommit();
    }

    
    public void commit() throws SQLException
    {
        try 
        {
            logger.finest("Calling commit()");
            wrappedConnection.commit();
        } catch (Throwable t)
        {
            // During CMT, this will throw an exception
            logger.warning("Cannot force commit: " + t.getMessage());
            
            if(isClosed())
            {
                logger.warning("Connection is closed(). Due to commit error?");
            }
        }
    }

    
    public void rollback() throws SQLException
    {
        wrappedConnection.rollback();
    }
    
    public DatabaseMetaData getMetaData() throws SQLException
    {
        return internalConnection.getMetaData();
    }

    
    public void setReadOnly(boolean readOnly) throws SQLException
    {
        wrappedConnection.setReadOnly(readOnly);
    }

    
    public boolean isReadOnly() throws SQLException
    {
        return wrappedConnection.isReadOnly();
    }

    
    public void setCatalog(String catalog) throws SQLException
    {
        internalConnection.setCatalog(catalog);
    }

    
    public String getCatalog() throws SQLException
    {
        return internalConnection.getCatalog();
    }

    
    public void setTransactionIsolation(int level) throws SQLException
    {
        internalConnection.setTransactionIsolation(level);
    }

    
    public int getTransactionIsolation() throws SQLException
    {
        return internalConnection.getTransactionIsolation();
    }

    
    public SQLWarning getWarnings() throws SQLException
    {
        return internalConnection.getWarnings();
    }

    
    public void clearWarnings() throws SQLException
    {
        internalConnection.clearWarnings();
    }

    
    public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException
    {
        return internalConnection.createStatement(resultSetType, resultSetConcurrency);
    }

    
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
            throws SQLException
    {
        return internalConnection.prepareStatement(sql, resultSetType, resultSetConcurrency);
    }

    
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException
    {
        return internalConnection.prepareCall(sql, resultSetType, resultSetConcurrency);
    }
  
    public void setHoldability(int holdability) throws SQLException
    {
        internalConnection.setHoldability(holdability);
    }

    
    public int getHoldability() throws SQLException
    {
        return internalConnection.getHoldability();
    }

    
    public Savepoint setSavepoint() throws SQLException
    {
        return wrappedConnection.setSavepoint();
    }

    
    public Savepoint setSavepoint(String name) throws SQLException
    {
        return wrappedConnection.setSavepoint(name);
    }

    
    public void rollback(Savepoint savepoint) throws SQLException
    {
        wrappedConnection.rollback(savepoint);
    }

    
    public void releaseSavepoint(Savepoint savepoint) throws SQLException
    {
        wrappedConnection.releaseSavepoint(savepoint);
    }

    
    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
            throws SQLException
    {
        return internalConnection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
            int resultSetHoldability) throws SQLException
    {
        return internalConnection.prepareStatement(sql, resultSetType, resultSetConcurrency);
    }

    
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
            int resultSetHoldability) throws SQLException
    {
        return internalConnection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    
    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException
    {
        return internalConnection.prepareStatement(sql, autoGeneratedKeys);
    }

    
    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException
    {
        return internalConnection.prepareStatement(sql, columnIndexes);
    }

    
    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException
    {
        return internalConnection.prepareStatement(sql, columnNames);
    }

    
    public Clob createClob() throws SQLException
    {
        return internalConnection.createClob();
    }

    
    public Blob createBlob() throws SQLException
    {
        return internalConnection.createBlob();
    }

    
    public NClob createNClob() throws SQLException
    {
        return internalConnection.createNClob();
    }

    
    public SQLXML createSQLXML() throws SQLException
    {
        return internalConnection.createSQLXML();
    }

    
    public boolean isValid(int timeout) throws SQLException
    {
        return internalConnection.isValid(timeout);
    }

    
    public void setClientInfo(String name, String value) throws SQLClientInfoException
    {
        internalConnection.setClientInfo(name, value);
    }

    
    public void setClientInfo(Properties properties) throws SQLClientInfoException
    {
        internalConnection.setClientInfo(properties);
    }

    
    public String getClientInfo(String name) throws SQLException
    {
        return internalConnection.getClientInfo(name);
    }

    
    public Properties getClientInfo() throws SQLException
    {
        return internalConnection.getClientInfo();
    }

    
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException
    {
        return internalConnection.createArrayOf(typeName, elements);
    }

    
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException
    {
        return internalConnection.createStruct(typeName, attributes);
    }

    
    public <T> T unwrap(Class<T> iface) throws SQLException
    {
        return internalConnection.unwrap(iface);
    }

    
    public boolean isWrapperFor(Class<?> iface) throws SQLException
    {
        return internalConnection.isWrapperFor(iface);
    }

    
    public int CHARBytesToJavaChars(byte[] arg0, int arg1, char[] arg2) throws SQLException
    {
        return internalConnection.CHARBytesToJavaChars(arg0, arg1, arg2);
    }

    
    public boolean IsNCharFixedWith()
    {
        return internalConnection.IsNCharFixedWith();
    }

    
    public int NCHARBytesToJavaChars(byte[] arg0, int arg1, char[] arg2) throws SQLException
    {
        return internalConnection.NCHARBytesToJavaChars(arg0, arg1, arg2);
    }

    
    public void abort() throws SQLException
    {
        internalConnection.abort();
    }

    
    public void cancel() throws SQLException
    {
        internalConnection.cancel();
    }

    
    public Class classForNameAndSchema(String arg0, String arg1) throws ClassNotFoundException
    {
        return internalConnection.classForNameAndSchema(arg0, arg1);
    }

    
    public void cleanupAndClose(boolean arg0) throws SQLException
    {
        internalConnection.cleanupAndClose(arg0);
    }

    
    public void clearAllApplicationContext(String arg0) throws SQLException
    {
        internalConnection.clearAllApplicationContext(arg0);
    }

    
    public void closeInternal(boolean arg0) throws SQLException
    {
        internalConnection.closeInternal(arg0);
    }

    
    public BFILE createBfile(byte[] arg0) throws SQLException
    {
        return internalConnection.createBfile(arg0);
    }

    
    public BfileDBAccess createBfileDBAccess() throws SQLException
    {
        return internalConnection.createBfileDBAccess();
    }

    
    public BLOB createBlob(byte[] arg0) throws SQLException
    {
        return internalConnection.createBlob(arg0);
    }

    
    public BlobDBAccess createBlobDBAccess() throws SQLException
    {
        return internalConnection.createBlobDBAccess();
    }

    
    public BLOB createBlobWithUnpickledBytes(byte[] arg0) throws SQLException
    {
        return internalConnection.createBlobWithUnpickledBytes(arg0);
    }

    
    public CLOB createClob(byte[] arg0) throws SQLException
    {
        return internalConnection.createClob(arg0);
    }

    
    public CLOB createClob(byte[] arg0, short arg1) throws SQLException
    {
        return internalConnection.createClob(arg0, arg1);
    }

    
    public ClobDBAccess createClobDBAccess() throws SQLException
    {
        return internalConnection.createClobDBAccess();
    }

    
    public CLOB createClobWithUnpickledBytes(byte[] arg0) throws SQLException
    {
        return internalConnection.createClobWithUnpickledBytes(arg0);
    }

    
    public Enumeration descriptorCacheKeys()
    {
        return internalConnection.descriptorCacheKeys();
    }

    
    public boolean getBigEndian() throws SQLException
    {
        return internalConnection.getBigEndian();
    }

    
    public int getC2SNlsRatio()
    {
        return internalConnection.getC2SNlsRatio();
    }

    
    public int getConnectionCacheCallbackFlag() throws SQLException
    {
        return internalConnection.getConnectionCacheCallbackFlag();
    }

    
    public OracleConnectionCacheCallback getConnectionCacheCallbackObj() throws SQLException
    {
        return internalConnection.getConnectionCacheCallbackObj();
    }

    
    public Object getConnectionCacheCallbackPrivObj() throws SQLException
    {
        return internalConnection.getConnectionCacheCallbackPrivObj();
    }

    
    public Properties getDBAccessProperties() throws SQLException
    {
        return internalConnection.getDBAccessProperties();
    }

    
    public String getDatabaseProductVersion() throws SQLException
    {
        return internalConnection.getDatabaseProductVersion();
    }

    
    public short getDbCsId() throws SQLException
    {
        return internalConnection.getDbCsId();
    }

    
    public boolean getDefaultFixedString()
    {
        return internalConnection.getDefaultFixedString();
    }

    
    public String getDefaultSchemaNameForNamedTypes() throws SQLException
    {
        return internalConnection.getDefaultSchemaNameForNamedTypes();
    }

    
    public Object getDescriptor(byte[] arg0)
    {
        return internalConnection.getDescriptor(arg0);
    }

    
    public short getDriverCharSet()
    {
        return internalConnection.getDriverCharSet();
    }

    
    public byte[] getFDO(boolean arg0) throws SQLException
    {
        return internalConnection.getFDO(arg0);
    }

    
    public void getForm(OracleTypeADT arg0, OracleTypeCLOB arg1, int arg2) throws SQLException
    {
        internalConnection.getForm(arg0, arg1, arg2);
    }

    
    public int getHeapAllocSize() throws SQLException
    {
        return internalConnection.getHeapAllocSize();
    }

    
    public int getHeartbeatNoChangeCount() throws SQLException
    {
        return internalConnection.getHeartbeatNoChangeCount();
    }

    
    public Map getJavaObjectTypeMap()
    {
        return internalConnection.getJavaObjectTypeMap();
    }

    
    public short getJdbcCsId() throws SQLException
    {
        return internalConnection.getJdbcCsId();
    }

    
    public Connection getLogicalConnection(OraclePooledConnection arg0, boolean arg1) throws SQLException
    {
        return internalConnection.getLogicalConnection(arg0, arg1);
    }

    
    public int getMaxCharSize() throws SQLException
    {
        return internalConnection.getMaxCharSize();
    }

    
    public int getMaxCharbyteSize()
    {
        return internalConnection.getMaxCharbyteSize();
    }

    
    public int getMaxNCharbyteSize()
    {
        return internalConnection.getMaxNCharbyteSize();
    }

    
    public short getNCharSet()
    {
        return internalConnection.getNCharSet();
    }

    
    public int getOCIEnvHeapAllocSize() throws SQLException
    {
        return internalConnection.getOCIEnvHeapAllocSize();
    }

    
    public Properties getOCIHandles() throws SQLException
    {
        return internalConnection.getOCIHandles();
    }

    
    public OracleConnection getPhysicalConnection()
    {
        return internalConnection.getPhysicalConnection();
    }

    
    public void getPropertyForPooledConnection(OraclePooledConnection arg0) throws SQLException
    {
        internalConnection.getPropertyForPooledConnection(arg0);
    }

    
    public String getProtocolType()
    {
        return internalConnection.getProtocolType();
    }

    
    public Properties getServerSessionInfo() throws SQLException
    {
        return internalConnection.getServerSessionInfo();
    }

    
    public long getStartTime() throws SQLException
    {
        return internalConnection.getStartTime();
    }

    
    public short getStructAttrNCsId() throws SQLException
    {
        return internalConnection.getStructAttrNCsId();
    }

    
    public long getTdoCState(String arg0, String arg1) throws SQLException
    {
        return internalConnection.getTdoCState(arg0, arg1);
    }

    
    public int getTxnMode()
    {
        return internalConnection.getTxnMode();
    }

    
    public Map getTypeMap() throws SQLException
    {
        return internalConnection.getTypeMap();
    }

    
    public String getURL() throws SQLException
    {
        return internalConnection.getURL();
    }

    
    public short getVersionNumber() throws SQLException
    {
        return internalConnection.getVersionNumber();
    }

    
    public oracle.jdbc.OracleConnection getWrapper()
    {
        return internalConnection.getWrapper();
    }

    
    public XAResource getXAResource() throws SQLException
    {
        return internalConnection.getXAResource();
    }

    
    public boolean isCharSetMultibyte(short arg0)
    {
        return internalConnection.isCharSetMultibyte(arg0);
    }

    
    public boolean isDescriptorSharable(OracleConnection arg0) throws SQLException
    {
        return internalConnection.isDescriptorSharable(arg0);
    }

    
    public boolean isStatementCacheInitialized()
    {
        return internalConnection.isStatementCacheInitialized();
    }

    
    public boolean isV8Compatible() throws SQLException
    {
        return internalConnection.isV8Compatible();
    }

    
    public int javaCharsToCHARBytes(char[] arg0, int arg1, byte[] arg2) throws SQLException
    {
        return internalConnection.javaCharsToCHARBytes(arg0, arg1, arg2);
    }

    
    public int javaCharsToNCHARBytes(char[] arg0, int arg1, byte[] arg2) throws SQLException
    {
        return internalConnection.javaCharsToNCHARBytes(arg0, arg1, arg2);
    }

    
    public ResultSet newArrayDataResultSet(Datum[] arg0, long arg1, int arg2, Map arg3) throws SQLException
    {
        return internalConnection.newArrayDataResultSet(arg0, arg1, arg2, arg3);
    }

    
    public ResultSet newArrayDataResultSet(ARRAY arg0, long arg1, int arg2, Map arg3) throws SQLException
    {
        return internalConnection.newArrayDataResultSet(arg0, arg1, arg2, arg3);
    }

    
    public ResultSet newArrayLocatorResultSet(ArrayDescriptor arg0, byte[] arg1, long arg2, int arg3, Map arg4)
            throws SQLException
    {
        return internalConnection.newArrayLocatorResultSet(arg0, arg1, arg2, arg3, arg4);
    }

    
    public ResultSetMetaData newStructMetaData(StructDescriptor arg0) throws SQLException
    {
        return internalConnection.newStructMetaData(arg0);
    }

    
    public int numberOfDescriptorCacheEntries()
    {
        return internalConnection.numberOfDescriptorCacheEntries();
    }

    
    public void putDescriptor(byte[] arg0, Object arg1) throws SQLException
    {
        internalConnection.putDescriptor(arg0, arg1);
    }

    
    public OracleStatement refCursorCursorToStatement(int arg0) throws SQLException
    {
        return internalConnection.refCursorCursorToStatement(arg0);
    }

    
    public void removeAllDescriptor()
    {
        internalConnection.removeAllDescriptor();
    }

    
    public void removeDescriptor(String arg0)
    {
        internalConnection.removeAllDescriptor();
    }

    
    public void setAbandonedTimeoutEnabled(boolean arg0) throws SQLException
    {
        internalConnection.setAbandonedTimeoutEnabled(arg0);
    }

    
    public void setApplicationContext(String arg0, String arg1, String arg2) throws SQLException
    {
        internalConnection.setApplicationContext(arg0, arg1, arg2);
    }

    
    public void setDefaultFixedString(boolean arg0)
    {
        internalConnection.setDefaultFixedString(arg0);
    }

    
    public void setFDO(byte[] arg0) throws SQLException
    {
        internalConnection.setFDO(arg0);
    }

    
    public void setJavaObjectTypeMap(Map arg0)
    {
        internalConnection.setJavaObjectTypeMap(arg0);
    }

    
    public void setStartTime(long arg0) throws SQLException
    {
        internalConnection.setStartTime(arg0);
    }

    
    public void setTxnMode(int arg0)
    {
        internalConnection.setTxnMode(arg0);
    }

    
    public void setTypeMap(Map arg0) throws SQLException
    {
        internalConnection.setTypeMap(arg0);
    }

    
    public Datum toDatum(CustomDatum arg0) throws SQLException
    {
        return internalConnection.toDatum(arg0);
    }

    
    
    private void logStackTrace(String prefix)
    {
        if(!logger.isLoggable(Level.FINEST))
            return;
        
        StackTraceElement[] st = Thread.currentThread().getStackTrace();
        
        StringBuffer sb = new StringBuffer(prefix);
        
        for(StackTraceElement it: st)
        {
            sb.append("\n").append(it.toString());
        }

        logger.finest(sb.toString());
        
    }
}
