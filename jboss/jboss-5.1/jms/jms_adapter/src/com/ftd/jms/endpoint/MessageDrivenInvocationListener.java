package com.ftd.jms.endpoint;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.jboss.ejb.MessageDrivenContainer;
import org.jboss.invocation.Invocation;

/**
 * Delegate onMessage to the MessageDrivenContainer.
 * This is needed as the Container takes care of all the transaction processing etc.
 * @author Arajoo
 */
public class MessageDrivenInvocationListener implements MessageListener
{
    private static final Logger logger = Logger.getLogger(MessageDrivenInvocationListener.class.getName());
            
    MessageDrivenContainer container;
    public static final Method ONMESSAGE; 
    
    static
    {
       try
       {
          ONMESSAGE = MessageListener.class.getMethod("onMessage", new Class[] { Message.class });
       }
       catch (Exception e)
       {
          throw new RuntimeException(e);
       }
    }    
    
    public MessageDrivenInvocationListener(MessageDrivenContainer container)
    {
        this.container = container;
    }

    /**
     * Delecates to the container's Invocation
     */
    public void onMessage(Message message)
    {
        if(logger.isLoggable(Level.FINE)) 
        {
            logger.fine("Received Message: " + message);
        }
        
        try 
        {
            // Wrap the Message in an Invocation object
            // And delegate to the Container
            Invocation invocation = new Invocation();
            invocation.setArguments(new Object[] {message});
            invocation.setMethod(ONMESSAGE);
            container.invoke(invocation);
        } catch (Exception ex)
        {
            logger.severe("Exception on Handler: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

}
