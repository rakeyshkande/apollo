package com.ftd.jms.oracle;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.LinkRef;

/**
 * Binds the JBOSS Transaction Manager into the JNDI Space required by the Oracle aqapi.jar library - 
 * Look into: {@link oracle.jms.EmulatedXAHandler}
 * This is generally checked by the {@link oracle.jms.AQjmsSession} object on initialization.
 * 
 */
public class PlatformTransactionManagerBinding
{
    private static final Logger logger = Logger.getLogger(PlatformTransactionManagerBinding.class.getName());
        
    /**
     * Flag indicating if the JBOSS Transaction Manager at java:TransactionManager
     * should be bound to what the Oracle AQ Expects at: java:comp/pm/TransactionManager
     * If true, this is performed by the {@link #initEnvironment()} method
     */
    static boolean bindTransactionManager = false;
    
    /**
     * Private constructor. Utilize the Static methods
     */
    private PlatformTransactionManagerBinding()
    {
    }
    
    /**
     * Configures if the Transaction Manager is to be bound.
     */
    public static void setBindTransactionManager(boolean newVal)
    {
        bindTransactionManager = newVal;
        
        if(logger.isLoggable(Level.INFO)) {
            logger.info("Bind Transaction Manager to Oracle: " + bindTransactionManager);
        }        
    }
    
    /**
     * if {@link #bindTransactionManager} is true, ensures that the TransactionManager is 
     * exposed to the JNDI location expected by AQ.
     */
    public static void setupTransactionManagerBinding()
    {
        if(!bindTransactionManager)
        {
            return;
        }
        
        try
        {
            InitialContext context = new InitialContext();                    
            if(pmIsRegistered(context))
            {
                return;
            }
            
            registerPm(context);
        } catch (Throwable t)
        {
            logger.warning("Failed Initialization of Platform Binding: " + t.getMessage());
        }
     
    }

    /**
     * Checks to see if the TransactionManager is already bound to the location for AQ
     * @param context
     * @return
     */
    private static boolean pmIsRegistered(Context context)
    {
        try
        {
            Object item = context.lookup("java:comp/pm/TransactionManager");
            
            if(logger.isLoggable(Level.FINEST))
                logger.finest("Lookup of Transaction Manager: " + item.getClass());            
            
            return true;
        } catch (Throwable t)
        {
            if(logger.isLoggable(Level.FINEST))
                logger.finest("Transaction Manager not found at java:comp/pm/TransactionManager: " + t.getMessage());    
            return false;
        }
    }
    
    
    /**
     * Registers the TransactionManager into the AQ location. This method is synchronized to prevent
     * duplicate bindings into the same context environment
     * @param context
     * @return
     */    
    private static synchronized void registerPm(Context context)
    {
        // Check again, since we are in a synchronization block
        if(pmIsRegistered(context))
        {
            return;
        }
        
        // Set up the TransactionManager for OracleAQ
        try 
        {
            // Check to ensure the JBOSS TM is registered/available
            Object tm = context.lookup("java:TransactionManager");
            
            if(logger.isLoggable(Level.FINEST))
                logger.finest("Adding Link Ref for JBOSS Transaction Manager: " + tm.toString());            
            
            Context sub = (Context) context.lookup("java:comp");
            sub = sub.createSubcontext("pm");
            LinkRef link = new LinkRef("java:TransactionManager");
            sub.bind("TransactionManager", link);
            
            logger.finest("Bound transaction Manager to: java:comp/pm/TransactionManager");
            
            Object linkRef = context.lookup("java:comp/pm/TransactionManager");
            
            if(logger.isLoggable(Level.FINEST))
                logger.finest("Bound transaction Manager to: java:comp/pm/TransactionManager: " + linkRef.toString());
                
        } catch (Throwable t)
        { 
            logger.warning("Failed to link to JBOSS Transaction Manager: " + t.getMessage());
            throw new RuntimeException(t);
        }           
    }
}
