package test.jms.util;

import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;


/**
 * This utility sends Text messages to a JMS queue
 */
public class JMSQueueSenderUtil {
	
	public JMSQueueSenderUtil() 
	{
		
	}
	
	/*
	public void sendTextMessage(String connectionFactoryJNDI, String queueName, String textMessage)
	throws Exception
	{
		Context ctx = new InitialContext();
		
		QueueConnectionFactory connectionFactory = null;
		Connection connection = null;
		Session session = null;
		Queue queue = null;
		MessageProducer messageProducer = null;
		
		try 
		{
		connectionFactory = (QueueConnectionFactory) ctx.lookup(connectionFactoryJNDI);
		
		if(connectionFactory == null)
		{
			throw new Exception("No Connection Factory at: " + connectionFactoryJNDI);
		}
		
		connection = connectionFactory.createQueueConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		queue = (Queue) ctx.lookup(queueName);
		messageProducer = session.createProducer(queue);
		messageProducer.send(session.createTextMessage(textMessage));
		} finally
		{
			try 
			{
				if(session != null)
					session.close();
			} catch (Exception ex)
			{
			}
			
			try 
			{
				if(connection != null)
					connection.close();
			} catch (Exception ex)
			{
				
			}
		}
		
	}	
	*/

	public void sendTextMessage(String connectionFactoryJNDI, String queueName, String textMessage, Map<String, String> properties)
	throws Exception
	{
	    logDebug("Connection Factory: " + connectionFactoryJNDI);
	    logDebug("Queue Name: " + queueName);
        logDebug("Text Message: " + textMessage);	    
	    
		Context ctx = new InitialContext();
		
		ConnectionFactory connectionFactory = null;
		Connection connection = null;
		Session session = null;
		Queue queue = null;
		MessageProducer messageProducer = null;
		
		try 
		{
		connectionFactory = (ConnectionFactory) ctx.lookup(connectionFactoryJNDI);
		
		if(connectionFactory == null)
		{
			throw new Exception("No Connection Factory at: " + connectionFactoryJNDI);
		}
		
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		queue = (Queue) ctx.lookup(queueName);
		messageProducer = session.createProducer(queue);
		
		TextMessage message = session.createTextMessage(textMessage);
		
		for(String prop: properties.keySet())
		{
		    String val = properties.get(prop);
	        logDebug("Property: " + prop + "=" + val);       
		    message.setStringProperty(prop, val);
		}
		    
		messageProducer.send(message);
		} finally
		{
			try 
			{
				if(session != null)
					session.close();
			} catch (Exception ex)
			{
			}
			
			try 
			{
				if(connection != null)
					connection.close();
			} catch (Exception ex)
			{
				
			}
		}
	}
	
	
	private void logDebug(String message)
	{
	    System.out.println(message);
	}
}
