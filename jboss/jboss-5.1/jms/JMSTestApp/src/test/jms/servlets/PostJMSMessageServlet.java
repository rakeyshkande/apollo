package test.jms.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import test.jms.util.JMSQueueSenderUtil;

/**
 * Servlet implementation class PostJMSMessageServlet
 */
public class PostJMSMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostJMSMessageServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performAction(request, response);
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performAction(request, response);
	}

	private void performAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
		String connectionFactory = request.getParameter("connectionFactory");
		String queueName = request.getParameter("queueName");
		String textMessage = request.getParameter("textMessage");
		String messageProperties = request.getParameter("messageProperties");

		JMSQueueSenderUtil jmsQueueSenderUtil = new JMSQueueSenderUtil();
		
		try 
		{
			response.setContentType("text/plain");
			jmsQueueSenderUtil.sendTextMessage(connectionFactory, queueName, textMessage, parseMessageProperties(messageProperties));
			response.getWriter().print("OK");
			response.getWriter().close();			
		} catch (Exception e) 
		{
			System.out.println("Error Sending Message: " + e.getMessage());
			e.printStackTrace();
			
			response.getWriter().println("ERROR");
			response.getWriter().println("Factory: " + connectionFactory);
			response.getWriter().println("queueName: " + connectionFactory);	
			response.getWriter().println("Exception: " + e.getMessage());
			e.printStackTrace(response.getWriter());
			response.getWriter().close();	
		}
	}

    private Map<String, String> parseMessageProperties(String messageProperties)
    {
        Map<String, String> retVal = new HashMap<String, String>();
        if(messageProperties == null || messageProperties.trim().length() == 0)
        {
            return retVal;
        }
        
        messageProperties = messageProperties.replace("\r", "");
        
        String[] items = messageProperties.split("\n");
        
        for(String item: items)
        {
            String[] keyVal = item.split("=");
            
            if(keyVal.length != 2)
            {
                continue;
            }
            
            retVal.put(keyVal[0].trim(), keyVal[1].trim());
        }
        
        return retVal;
    }
}
