package com.ftd.osp.excepqmgr;
import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.BusinessConfig;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

public class ExceptionQBO implements IBusinessObject {
  private Logger logger;
  
  public ExceptionQBO() {
    logger = new Logger("com.ftd.osp.excepqmgr.ExceptionQBO");
  }

    /**
     * Consumes the JMS messages that reside in the Application Exception Queue.
     * It sets the status for that order to 1013.
     * 
     * Implements the execute method of the IBusinessObject interface
     * @param config The BusinessConfig object passed from the message
     *
     */
    public void execute(BusinessConfig config){
       String guid = null;
        try  {

           guid = (String)config.getInboundMessageToken().getMessage();
           String message = "Order " 
                             + guid 
                             + " consumed successfully from the Application Exception Queue."
                             + " Please verify that the order was re-instated correctly." ;          
           logger.debug(message);
           this.sendMessage(message, config.getConnection());
                         
        } catch (Throwable t)  {
          logger.error("Order " + guid + " could not be consumed from the Application Exception Queue");
          logger.error(t);
          MessageToken token = new MessageToken();
          token.setStatus("ROLLBACK");
          config.setOutboundMessageToken(token);
         } finally  {
        }


    }


    /*
 * This method sends a message to the System Messenger.
 *
 * @param String message
 * @returns String message id
 */
  private void sendMessage(String message, Connection connection) 
  {
      try
      {
      //build system vo
        SystemMessengerVO sysMessage = new SystemMessengerVO();
        sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        sysMessage.setSource("Application Exception Queue Manager");
        sysMessage.setType("ERROR");
        sysMessage.setMessage(message);
        SystemMessenger.getInstance().send(sysMessage, connection);
      }
      catch(Throwable t)
      {
        logger.error("Sending system message failed. Message=" + message);
        logger.error(t);
      }
  }
    
                

}