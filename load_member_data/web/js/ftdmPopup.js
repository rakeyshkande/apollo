/*
 * Constructor
 */
Popup = function(child){
   // Member variables
   this.popupContent = child;
   this.activeDiv = null;
   this.onClose = null;
   this.dragging = false;

   // HTML objets
   this.div = null;
}

Popup._P = null;
Popup.isIE = ( /msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent) );

window.popup = null;


/*
 * Static functions
 */
Popup.setup = function(params, popupContent){
   function param_default(pname, def) { if (typeof params[pname] == "undefined") { params[pname] = def; } };
   param_default("displayArea",   null);
   param_default("itemIndex",     null);
   param_default("prePopulate",   true);
   var tmp = ["displayArea"];
   for (var i in tmp) {
      if (typeof params[tmp[i]] == "string") {
         params[tmp[i]] = document.getElementById(params[tmp[i]]);
      }
   }

   return new Popup(popupContent);
};

Popup.createElement = function(type, parent) {
   var el = null;
   if (document.createElementNS) {
      // use the XHTML namespace; IE won't normally get here unless the DOM2 implementation is fixed
      el = document.createElementNS("http://www.w3.org/1999/xhtml", type);
   } else {
      el = document.createElement(type);
   }
   if (typeof parent != "undefined") {
      parent.appendChild(el);
   }
   return el;
};

Popup.getElement = function(ev) {
   if (Popup.isIE) {
      return window.event.srcElement;
   } else {
      return ev.currentTarget;
   }
};

Popup.getTargetElement = function(ev) {
   if (Popup.isIE) {
      return window.event.srcElement;
   } else {
      return ev.target;
   }
};

Popup.addEvent = function(el, evname, func) {
   if (el.attachEvent) { // IE
      el.attachEvent("on" + evname, func);
   } else if (el.addEventListener) { // Gecko / W3C
      el.addEventListener(evname, func, true);
   } else { // Opera (or old browsers)
      el["on" + evname] = func;
   }
};

Popup.removeEvent = function(el, evname, func) {
   if (el.detachEvent) { // IE
      el.detachEvent("on" + evname, func);
   } else if (el.removeEventListener) { // Gecko / W3C
      el.removeEventListener(evname, func, true);
   } else { // Opera (or old browsers)
      el["on" + evname] = null;
   }
};

Popup.checkPopup = function(ev) {
   if (!window.popup) {
      return false;
   }
   var el = Popup.isIE ? Popup.getElement(ev) : Popup.getTargetElement(ev);
   for (; el != null && el != popup.div; el = el.parentNode);
   if (el == null) {
      //Popup.hide();
      return Popup.stopEvent(ev);
   }
};

Popup.stopEvent = function(ev) {
   if (Popup.isIE) {
      window.event.cancelBubble = true;
      window.event.returnValue = false;
   } else {
      ev.preventDefault();
      ev.stopPropagation();
   }
   return false;
};

Popup.dragMouseDown = function(ev) {
   var el = Popup.getElement(ev);
   if (el.disabled) {
      return false;
   }

   var popup = el.popup;
   popup.activeDiv = el;
   popup.dragStart(ev);
   return Popup.stopEvent(ev);
};

Popup.dragIt = function (ev) {
   var pop = Popup._P;
   if (!(pop && pop.dragging)) {
       return false;
   }
   var posX, posY;
   if (Popup.isIE) {
      posY = window.event.clientY + document.body.scrollTop;
      posX = window.event.clientX + document.body.scrollLeft;
   } else {
      posX = ev.pageX;
      posY = ev.pageY;
   }
   pop.hideShowCovered();
   var st = pop.div.style;
   st.left = (posX - pop.xOffs) + "px";
   st.top = (posY - pop.yOffs) + "px";
   return Popup.stopEvent(ev);
};

Popup.dragEnd = function (ev) {
   var pop = Popup._P;
   if (!(pop)) {
       return false;
   }
   pop.dragging = false;

   with (Popup) {
      removeEvent(document, "mousemove", dragIt);
      removeEvent(document, "mouseover", stopEvent);
      removeEvent(document, "mouseup", dragEnd);
   }
   pop.hideShowCovered();
};

Popup.addDragListener = function(el) {
   with (Popup) {
      addEvent(el, "mousedown", dragMouseDown);
   }
};

Popup.getAbsolutePos = function(el) {
   var r = { x: el.offsetLeft, y: el.offsetTop };
   if (el.offsetParent) {
      var tmp = Popup.getAbsolutePos(el.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
   }
   return r;
};

Popup.show = function () {

   if (Popup._P.popupContent.setValues) {
      Popup._P.popupContent.setValues();
   }
   
   Popup.addEvent(document, "mousedown", Popup.checkPopup);
   
   Popup._P.div.style.display = "block";
   
   if (Popup._P.popupContent.setFocus) {
   
      Popup._P.popupContent.setFocus();
      
   }
   Popup._P.hideShowCovered();
   
};

Popup.hide = function () {

   Popup._P.div.style.display = "none";
   Popup.removeEvent(document, "mousedown", Popup.checkPopup);
   Popup._P.hideShowCovered();
   Popup.destroy();

};

Popup.pressHide = function () {
 
  if (window.event.keyCode == 13)
      Popup.hide();

};

Popup.destroy = function () {
   var el = Popup._P.div.parentNode;
   el.removeChild(Popup._P.div);
   Popup._P = null;
};




/*
 * Member functions
 */
Popup.prototype.create = function(){
   Popup._P = this;
      var parent = document.getElementsByTagName("body")[0];
      var div = Popup.createElement("div");
      this.div = div;
      div.className = "popup";
      div.style.position = "absolute";
      div.style.display = "none";
   
   if (this.popupContent.renderContent)
      this.popupContent.renderContent(div);

   parent.appendChild(div);

};

Popup.prototype.showAt = function (x, y) {
   var s = this.div.style;
   s.left = x + "px";
   s.top = y + "px";
   Popup.show();
};

Popup.prototype.showAtElement = function (el) {
   var p = Popup.getAbsolutePos(el);
   this.showAt(p.x, p.y + el.offsetHeight);
};

Popup.prototype.dragStart = function (ev) {
   if (this.dragging) {
      return;
   }
   this.dragging = true;
   var posX;
   var posY;
   if (Popup.isIE) {
      posY = window.event.clientY + document.body.scrollTop;
      posX = window.event.clientX + document.body.scrollLeft;
   } else {
      posY = ev.clientY + window.scrollY;
      posX = ev.clientX + window.scrollX;
   }
   var st = this.div.style;
   this.xOffs = posX - parseInt(st.left);
   this.yOffs = posY - parseInt(st.top);
   with (Popup) {
      addEvent(document, "mousemove", dragIt);
      addEvent(document, "mouseover", stopEvent);
      addEvent(document, "mouseup", dragEnd);
   }
};

Popup.prototype.hideShowCovered = function () {
   function getStyleProp(obj, style){
      var value = obj.style[style];
      if (!value) {
         if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") { // Gecko, W3C
            value = document.defaultView.
               getComputedStyle(obj, "").getPropertyValue(style);
         } else if (obj.currentStyle) { // IE
            value = obj.currentStyle[style];
         } else {
            value = obj.style[style];
         }
      }
      return value;
   };

   function contains(a, b) {
      while (b.parentNode)
         if ((b = b.parentNode) == a)
            return true;
      return false;
   };


   var el = this.div;

   var p = Popup.getAbsolutePos(el);
   var EX1 = p.x;
   var EX2 = el.offsetWidth + EX1;
   var EY1 = p.y;
   var EY2 = el.offsetHeight + EY1;

   var tagsToHide = new Array("select");
   for (var k = 0; k < tagsToHide.length; k++) {
      var tags = document.getElementsByTagName(tagsToHide[k]);
      var tag = null;

      for (var i = 0; i < tags.length; i++) {
         tag = tags[i];
         if (!contains(this.div, tag)){
            p = Popup.getAbsolutePos(tag);
            var CX1 = p.x;
            var CX2 = tag.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = tag.offsetHeight + CY1;

            if ( (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1) ) {
               if (!tag.saveVisibility) {
                  tag.saveVisibility = getStyleProp(tag, "visibility");
               }
               tag.style.visibility = tag.saveVisibility;
            } else {
               if (!tag.saveVisibility) {
                  tag.saveVisibility = getStyleProp(tag, "visibility");
               }
               tag.style.visibility = "hidden";
            }
         }
      }
   }
};