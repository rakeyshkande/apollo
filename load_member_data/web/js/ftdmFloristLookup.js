/*
 * Constructor
 */
FloristPopup = function(index) {
   this.focusObj = null;
   this.itemIndex = index;
};

FloristPopup._F = null;

/*
 * Class functions
 */
FloristPopup.setup = function(params) {

   window.popup = Popup.setup(params, new FloristPopup(params.itemIndex));

   window.popup.create();

   //window.popup.showAtElement(params.displayArea);
   window.popup.showAt(400,200);

};

FloristPopup.enterSearchFlorist = function(ev) {
   if (ev.keyCode == 13)
      FloristPopup.searchFlorist();
};

FloristPopup.searchFlorist = function(ev) {
   var form = document.forms[0];

   //First validate that the inputs are valid
   check = true;
   var business = stripWhitespace(document.getElementById("floristBusinessInput").value);
   var city = stripWhitespace(document.getElementById("floristCityInput").value);
   var state = stripWhitespace(document.getElementById("floristStateInput").value);
   var zip = stripWhitespace(document.getElementById("floristZipInput").value);
   var phone = stripWhitespace(document.getElementById("floristPhoneInput").value);

   //state is required if business is given
   if ((business.length > 0) && (state.length == 0)) {
      if (check == true) {
         document.getElementById("floristStateInput").focus();
         check = false;
      }
      document.getElementById("floristStateInput").style.backgroundColor = 'pink';
   }

   //city is required if no zip and phone are given
   if ((city.length == 0) && (zip.length == 0) && (phone.length == 0)) {
      if (check == true) {
         document.getElementById("floristCityInput").focus();
         check = false;
      }
      document.getElementById("floristCityInput").style.backgroundColor = 'pink';
   }

   //zip is required if no city, state, and phone are given
   if ((zip.length == 0) && (city.length == 0) && (state.length == 0) && (phone.length == 0)) {
      if (check == true) {
         document.getElementById("floristZipInput").focus();
         check = false;
      }
      document.getElementById("floristZipInput").style.backgroundColor = 'pink';
   }

   if (!check) {
      alert("Please correct the marked fields")
      return false;
   }

   //Now that everything is valid, open the popup
   var country = document.getElementById("recip_country" + FloristPopup._F.itemIndex);
   var url_source="FloristLookupServlet?" +
   "&institutionInput=" + document.getElementById("floristBusinessInput").value +
   "&phoneInput=" + document.getElementById("floristPhoneInput").value +
   "&addressInput=" + document.getElementById("floristAddressInput").value +
   "&cityInput=" + document.getElementById("floristCityInput").value +
   "&stateInput=" + document.getElementById("floristStateInput")[document.getElementById("floristStateInput").selectedIndex].value +
   "&zipCodeInput=" + document.getElementById("floristZipInput").value +
   "&countryInput=" + country.value +
   "&sc_mode=" + document.getElementById("sc_mode").value +
   "&productId=" + document.getElementById("productid" + FloristPopup._F.itemIndex).value;

   //Open the popup
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = window.showModalDialog(url_source,"", modal_dim);

   if (ret && ret != null && ret[0] != ""){
      document.getElementById("florist_number" + FloristPopup._F.itemIndex).value = ret[0];
      document.getElementById("florist_name" + FloristPopup._F.itemIndex).value = ret[1];
      document.getElementById("floristDisplay" + FloristPopup._F.itemIndex).innerHTML = ret[1];
      document.getElementById("floristNameDisplay").innerHTML = ret[1];
      document.getElementById("floristPhoneDisplay").innerHTML = ret[2];
      document.getElementById("floristIdDisplay").innerHTML = ret[0];
   }

   Popup.hide();
   FloristPopup._F = null;
};

/*
 * Member Functions
 */
FloristPopup.prototype.renderContent = function(div) {

   FloristPopup._F = this;
   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("Florist Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   // Name
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Name:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "floristBusinessInput");
   input.setAttribute("tabIndex", "88");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Phone
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Phone:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "floristPhoneInput");
   input.setAttribute("tabIndex", "89");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Address
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Address:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "floristAddressInput");
   input.setAttribute("tabIndex", "90");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // City
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("City:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "floristCityInput");
   input.setAttribute("tabIndex", "91");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // States
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("States:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("select", cell);
   input.setAttribute("id", "floristStateInput");
   input.setAttribute("tabIndex", "92");

   // Zip
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Zip/Postal Code:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "floristZipInput");
   input.setAttribute("tabIndex", "93");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Buttons
   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   image = Popup.createElement("img", cell);
   image.setAttribute("alt", "Search");
   image.setAttribute("id", "floristSearch");
   image.setAttribute("src", "../images/button_search.gif");
   image.setAttribute("tabIndex", "94");
   Popup.addEvent(image, "click", FloristPopup.searchFlorist);
   Popup.addEvent(image, "keydown", FloristPopup.enterSearchFlorist);

   image = Popup.createElement("img", cell);
   image.setAttribute("alt", "Close screen");
   image.setAttribute("id", "floristClose");
   image.setAttribute("src", "../images/button_close.gif");
   image.setAttribute("tabIndex", "95");
   Popup.addEvent(image, "click", Popup.hide);
   Popup.addEvent(image, "keydown", Popup.pressHide);  
};

FloristPopup.prototype.setFocus = function() {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

FloristPopup.prototype.setValues = function() {
   var state = document.getElementById("recip_state" + this.itemIndex);
   populateStates("recip_country" + this.itemIndex, "floristStateInput", state.value);
};