/*
 * Constructor
 */
CityPopup = function(index) {
   this.focusObj = null;
   this.itemIndex = index;
};

CityPopup._C = null;

/*
 * Class functions
 */
CityPopup.setup = function(params) {
   window.popup = Popup.setup(params, new CityPopup(params.itemIndex));
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

CityPopup.enterCitySearch = function(ev) {
   if (ev.keyCode == 13)
      CityPopup.searchCity(ev);
};

CityPopup.searchCity = function(ev) {
   //First validate the inputs
   var check = true;

   var state = document.getElementById("cityLookupState");
   var city = document.getElementById("cityLookupCity");
   var zip = document.getElementById("cityLookupZip");

   //State is required if zip is empty
   if((stripWhitespace(state.value).length == 0) && (stripWhitespace(zip.value).length == 0)) {
     if (check == true) {
       state.focus();
       check = false;
     }
     state.style.backgroundColor = 'pink';
   }

   //City is required if zip is empty
   if((stripWhitespace(city.value).length == 0) && (stripWhitespace(zip.value).length == 0)) {
     if (check == true) {
       city.focus();
       check = false;
     }
     city.style.backgroundColor = 'pink';
   }

   if (!check) {
     alert("Please correct the marked fields")
     return false;
   }

   //Now that everything is valid, open the popup after removing special characters
   var bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
   var cityVal = stripCharsInBag(city.value, bag2);
   var stateVal = stripCharsInBag(state.value, bag2);
   var zipVal = stripCharsInBag(zip.value, bag2);
   var countryInput = (CityPopup._C.itemIndex == "shoppingCart") ? document.getElementById("buyer_country").value : document.getElementById("recip_country" + CityPopup._C.itemIndex).value;

   var form = document.forms[0];
   var url_source="LocationLookupServlet?" +
   "&cityInput=" + cityVal +
   "&stateInput=" + stateVal +
   "&zipCodeInput=" + zipVal +
   "&countryInput=" + countryInput;

   //Open the window
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = window.showModalDialog(url_source, "", modal_dim);

   // Set the new city, state, and zip
   if (ret && ret != null && ret[0] != ""){
     if (CityPopup._C.itemIndex == "shoppingCart"){
       document.getElementById("buyer_city").value = ret[0];
       setSelectedState("buyer_state", ret[1]);
       document.getElementById("buyer_postal_code").value = ret[2];
     } else {
       document.getElementById("recip_city" + CityPopup._C.itemIndex).value = ret[0];
       setSelectedState("recip_state" + CityPopup._C.itemIndex, ret[1]);
       document.getElementById("recip_postal_code" + CityPopup._C.itemIndex).value = ret[2];
     }
   }
   Popup.hide();
   CityPopup._C = null;
};


/*
 * Member Functions
 */
CityPopup.prototype.renderContent = function(div) {
   CityPopup._C = this;
   
   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("City Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   // City
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("City:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "cityLookupCity");
   input.setAttribute("tabIndex", "96");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // State
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("State:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("select", cell);
   input.setAttribute("id", "cityLookupState");
   input.setAttribute("tabIndex", "97");

   // Zip
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Zip/Postal Code:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "cityLookupZip");
   input.setAttribute("tabIndex", "98");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Buttons
   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   image = Popup.createElement("img", cell);
   image.setAttribute("alt", "Search");
   image.setAttribute("id", "sourceCodeSearch");
   image.setAttribute("src", "../images/button_search.gif");
   image.setAttribute("tabIndex", "99");
   Popup.addEvent(image, "click", CityPopup.searchCity);
   Popup.addEvent(image, "keydown", CityPopup.enterCitySearch);

   image = Popup.createElement("img", cell);
   image.setAttribute("alt", "Close screen");
   image.setAttribute("id", "sourceCodeClose");
   image.setAttribute("src", "../images/button_close.gif");
   image.setAttribute("tabIndex", "100");
   Popup.addEvent(image, "click", Popup.hide);
   Popup.addEvent(image, "keydown", Popup.pressHide);
};

CityPopup.prototype.setFocus = function() {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

CityPopup.prototype.setValues = function() {
   var prefix, postfix;
   if (CityPopup._C.itemIndex == "shoppingCart"){
     prefix = "buyer_";
     postfix = "";
   } else {
     prefix = "recip_";
     postfix = CityPopup._C.itemIndex;
   }
   var sState = prefix + "state" + postfix
   document.getElementById("cityLookupCity").value = document.getElementById(prefix + "city" + postfix).value;
   document.getElementById("cityLookupZip").value = document.getElementById(prefix + "postal_code" + postfix).value;
   populateStates(prefix + "country" + postfix, "cityLookupState", document.getElementById(sState)[document.getElementById(sState).selectedIndex].value);
};