<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">

	<xsl:variable name="selectedCity" select="key('pageData', 'city')/value"/>
	
	<xsl:variable name="selectedCityTitle" select="'Florist Coverage in '"/>

	<html>
	<form name="listZips" method="post">
		<head>	
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
 			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
			<script type="text/javascript" src="../js/calendar.js" />
			<title>FTD</title>
      
<script type="text/javascript" language="javascript">

	function doCloseAction()
	{
		window.close();
    }			

	function doRefreshAction()
	{
		var ret = new Array();
		ret[0] = document.getElementById("popupdeliverydate").value;
		ret[1] = '<xsl:value-of select="$selectedCity"/>';
		window.returnValue = ret;
		window.close();
    }			

</script>
		</head>
		<body>
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="concat($selectedCityTitle,$selectedCity)" />
				<xsl:with-param name="showExitButton" select="false()"/>
				<xsl:with-param name="showTime" select="false()"/>
			</xsl:call-template>
			
      <center>
			<input type="text" name="popupdeliverydate" value="{key('pageData', 'deliverydate')/value}" size="10" disabled="true"/>
			&nbsp;
			<input type="image" tabindex="-1" id="deldatecalendar" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
<script><![CDATA[
Calendar.setup(
{
inputField  : "popupdeliverydate",  // ID of the input field
ifFormat    : "mm/dd/y",  // the date format
button      : "deldatecalendar"  // ID of the button
}
);
]]></script>
			&nbsp;&nbsp;
			<input type="button" value=" Refresh " onClick="doRefreshAction();"/>
      </center>
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td colspan="9" width="100%" align="center">
					<div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">

						<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
															<td width="10%" class="colHeaderCenter">Member #</td>
															<td width="30%" class="colHeaderCenter">Name</td>
															<td width="10%" class="colHeaderCenter">Florist Type</td>
															<td width="20%" class="colHeaderCenter">Current Status</td>
															<td width="10%" class="colHeaderCenter">Sunday</td>
															<td width="10%" class="colHeaderCenter">Mercury</td>
															<td width="10%" class="colHeaderCenter">GoTo</td>
															<td width="10%" class="colHeaderCenter">Weight</td>															
														</tr>
														
														<xsl:for-each select="FLORISTS/FLORIST">
														
														<tr>

														<td width="10%" align="center">
															<xsl:value-of select="florist_id" />
														</td>
														<td width="30%" align="center">
															<xsl:value-of select="florist_name" />
														</td>
														<td width="10%" align="center">
															<xsl:value-of select="record_type" />
														</td>
														<td width="20%" align="center">	
														    <xsl:value-of select="status" />
														</td>
														<td width="10%" align="center">
															<xsl:value-of select="sunday_delivery_flag" />
														</td>
														<td width="10%" align="center">
															<xsl:value-of select="mercury_flag" />
														</td>
														<td width="10%" align="center">
															<xsl:value-of select="super_florist_flag" />
														</td>
														<td width="10%" align="center">
															<xsl:value-of select="florist_weight" />
														</td>
														
														</tr>
														
														</xsl:for-each>
						 </table>
					 </div>
				</td>
			</tr>
			</table>
      <center>
			<input type="button" value=" Close " onClick="doCloseAction();"/>
      </center>
			<!--xsl:call-template name="footer"/-->
		</body>
		</form>
	</html>
	
	
	</xsl:template>
</xsl:stylesheet>