<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="codification">
	
	<script type="text/javascript" language="javascript">
	<![CDATA[
	
	
	function dChanges()
	{
	document.forms[0].saveNeeded.value = "yes";
	enSave();
	document.getElementById("dchange").value = "";
	}

    function blockTypeChanged()
    {
        // If the blockType is Opt Out, then disable end Stuff
        // If the blockType is S or H, enable the end stuff

        // Get the drop down selection
        var blockTypeElement = document.getElementById("codNewBlockType");
        var blockTypeValue = blockTypeElement.options[blockTypeElement.selectedIndex].value;


        var endDateInput = document.getElementById("codNewBlockEndDate");
        var endDateCalendar = document.getElementById("CalendarBlockEnd");

        if (blockTypeValue == "O")
        {
            endDateInput.disabled = true;
            endDateCalendar.disabled = true;
        }
        else
        {
            endDateInput.disabled = false;
            endDateCalendar.disabled = false;
        }

        // Default the start date if is blank
        var startDateInput = document.getElementById("codNewBlockStartDate");
        var startDateValue = startDateInput.value;
        if (blockTypeValue != "A" && startDateValue == "")
        {
            // Set the start date
		    var today = new Date();
            var todayString = today.getMonth()+1 + "/" + today.getDate() + "/" + today.getYear();
            startDateInput.value = todayString;
        }

        if (blockTypeValue != "A")
        {
            dChanges();
        }

    }
	
	
	function validateCodification()
	{
		var count = 0;
	
		document.getElementById("invalidSoft").style.display = "none";
		document.getElementById("invalidDate").style.display = "none";
		document.getElementById("invalidHard").style.display = "none";
		document.getElementById("errorDate").style.display = "none";
		document.getElementById("missingDate").style.display = "none";
		document.getElementById("errorUnblock").style.display = "none";
		document.getElementById("errorholdUnblock").style.display = "block";
		document.getElementById("blockOverlap").style.display = "none";
		document.getElementById("invalidOpt").style.display = "none";
		document.getElementById("invalidOptStart").style.display = "none";
		document.getElementById("invalidSoft30Perm").style.display = "none";
		document.getElementById("startBeforeEnd").style.display = "none";
		document.getElementById("endAfterToday").style.display = "none";
		
		var uDate;
		var dateArray = new Array();
		var today = new Date();
			
		for( var i=1; i <= floristMaint.totalcod.value; i++)
		{

			if(document.getElementById('codblock' + i) != null)
				if(document.getElementById('codunblockdate' + i) != null)
				{
					document.getElementById('codunblockdate' + i).style.backgroundColor = 'white';
					uDate = document.getElementById('codunblockdate' + i).value;
		
					if(uDate.length > 0)
					{	
						dateArray = uDate.split("/");
						
						if(dateArray.length == 2)
							document.getElementById('codunblockdate' + i).value += "/" + today.getYear();
					}
				}
			}
	
	
	
			// for the sunday field...		
			//document.getElementById('codsundayunblockdate').style.backgroundColor = 'white';
			
			//uDate = document.getElementById('codsundayunblockdate').value;

	//		if(uDate.length > 0)
	//		{	
	//			dateArray = uDate.split("/");
	//			if(dateArray.length == 2)
	//				document.getElementById('codsundayunblockdate').value += "/" + today.getYear();
	//		}
			
			
			

			
        // Get the drop down selection
        var blockTypeElement = document.getElementById("codNewBlockType");
        var blockTypeValue = blockTypeElement.options[blockTypeElement.selectedIndex].value;

        var blockStartDate = document.getElementById("codNewBlockStartDate").value;
        var blockEndDate = document.getElementById("codNewBlockEndDate").value;


        // Check to make sure dates are populated if Soft block or hard block is selected
        if (blockTypeValue == 'S' || blockTypeValue == 'H')
        {
            if (blockStartDate == "" || blockEndDate == "")
            {
                document.getElementById("missingDate").style.display = "block";
                count++;
            }
        }

		if(count > 0)
			return false;
			
		
        // Check to make sure the end date is a proper format
	
			uDate = blockEndDate
			dateArray = new Array();
			if(uDate.length > 0)
			{
				dateArray = uDate.split("/");
				if(dateArray.length == 2)
				{
					floristMaint.codNewBlockEndDate.value += "/" + today.getYear();
					uDate = floristMaint.codNewBlockEndDate.value;
				}
				
				dateArray = uDate.split("/")
				
				if(dateArray.length != 3)
				{
					document.getElementById("errorDate").style.display = "block";
					document.getElementById("invalidDate").style.display = "none";
					count++;
				}
				else
				{
					if(dateArray[2].length == 2)
					{
						dateArray[2] = "20" + dateArray[2];
						floristMaint.codNewBlockEndDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
					}
				}
				
				
				if(dateArray[2] != null)
				{
					if(dateArray[2].length != 2)
					{
						if(dateArray[2].length != 4)
						{
							document.getElementById("errorDate").style.display = "block";
							document.getElementById("invalidDate").style.display = "none";
				   			count++;
						}
						else
						{
							 if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
						     {
						     document.getElementById("invalidDate").style.display = "block";
						     document.getElementById("errorDate").style.display = "none";
					   		 count++;
						     }
					     }
					}
				}
				
			}

			var iEndDate = Date.parse(blockEndDate);

        // Check to make sure the start date is a proper format

			uDate = blockStartDate
			dateArray = new Array();
			if(uDate.length > 0)
			{
				dateArray = uDate.split("/");
				if(dateArray.length == 2)
				{
					floristMaint.codNewBlockStartDate.value += "/" + today.getYear();
					uDate = floristMaint.codNewBlockStartDate.value;
				}
				
				dateArray = uDate.split("/")
				
				if(dateArray.length != 3)
				{
					document.getElementById("errorDate").style.display = "block";
					document.getElementById("invalidDate").style.display = "none";
					count++;
				}
				else
				{
					if(dateArray[2].length == 2)
					{
						dateArray[2] = "20" + dateArray[2];
						floristMaint.codNewBlockStartDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
					}
				}
				
				
				if(dateArray[2] != null)
				{
					if(dateArray[2].length != 2)
					{
						if(dateArray[2].length != 4)
						{
							document.getElementById("errorDate").style.display = "block";
							document.getElementById("invalidDate").style.display = "none";
				   			count++;
						}
						else
						{
							 if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
						     {
						     document.getElementById("invalidDate").style.display = "block";
						     document.getElementById("errorDate").style.display = "none";
					   		 count++;
						     }
					     }
					}
				}
				
			}
			
			
			if(count > 0)
				return false;
	
	
			var iStartDate = Date.parse(blockStartDate);
			
			var todayDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());
	
	
			iEndDate = iEndDate / 86400000;
			iStartDate = iStartDate / 86400000;
			todayDate = todayDate / 86400000;
			
			var diffEnd = iEndDate - todayDate;
			var diffStart = iStartDate - todayDate;
			var diffEndStart = iEndDate - iStartDate;
			
            // Soft and Hard checks
            if (blockTypeValue == 'S' || blockTypeValue == 'H')
            {
                if (diffEndStart < 0)
                {
					document.getElementById("startBeforeEnd").style.display = "block";
					count++;
                }
            }

            // Check to make sure a soft block has the proper range of dates
			if(blockTypeValue == 'S')
			{
				if(diffEnd < 0)
				{
					document.getElementById("endAfterToday").style.display = "block";
					count++;
				}
				else if(diffStart < 0)
				{
					document.getElementById("startAfterToday").style.display = "block";
					count++;
				}
				else if(diffEndStart > 30)
				{
					document.getElementById("invalidSoft").style.display = "block";
					count++;
				}
			}
			
            // Check to make sure a hard block has the proper range of dates
			if(blockTypeValue == 'H')
			{
				if(diffEnd < 0)
				{
					document.getElementById("endAfterToday").style.display = "block";
					count++;
				}
				else if(diffEndStart < 30 || diffEndStart > 90)
				{
					document.getElementById("invalidHard").style.display = "block";
					count++;
				}
			}

			if(count > 0)
				return false;

        // Check for a soft block > 30 days in future that doesnt have hard or opt permissions
        if (blockTypeValue == "S")
        {
            if (diffEndStart > 30)
            {
                var hardBlockUpdateAllowed = document.getElementById("hardBlockUpdate").value;
                var optOutUpdateAllowed = document.getElementById("optOutBlockUpdate").value;

                if (hardBlockUpdateAllowed == "N" && optOutUpdateAllowed == "N")
                {
					document.getElementById("invalidSoft30Perm").style.display = "block";
					count++;
                }
            }
        }

			if(count > 0)
				return false;

        // Loop through the existing blocks, for cross validation
        var blockCount = 1;
        var loopDone = 0;
        var blockTypeExisting = document.getElementById("codblockingBlockType" + blockCount);
        while (blockTypeExisting != null)
        {
            // Have a block row, get the values
            var blockTypeRowValue = blockTypeExisting.value;
            var rowStartDate = document.getElementById("codblockingStartDate" + blockCount).value;
            var rowEndDate = document.getElementById("codblockingEndDate" + blockCount).value;

            var dateArray;

			dateArray = rowStartDate.split("/")
			if(dateArray.length != 3)
			{
				document.getElementById("errorDate").style.display = "block";
				document.getElementById("invalidDate").style.display = "none";
				count++;
			}
			else
			{
				if(dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					rowStartDate= dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
			}
			dateArray = rowEndDate.split("/")
            // this can be null for Opt Out, ignore errors, set to forever
			if(dateArray.length != 3)
			{
                rowEndDate = "12/31/2999";
			}
			else
			{
				if(dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					rowEndDate= dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
			}

  
			var rowStartDateValue = Date.parse(rowStartDate);
			var rowEndDateValue = Date.parse(rowEndDate);
			rowStartDateValue = rowStartDateValue / 86400000;
			rowEndDateValue = rowEndDateValue / 86400000;

            /*
            alert("row start date = " + rowStartDateValue + "\n" +
                  "row end date   = " + rowEndDateValue + "\n" +
                  "row type       = " + blockTypeRowValue + "\n" +
                  "new start date = " + iStartDate + "\n" +
                  "new end date   = " + iEndDate + "\n" +
                  "new type       = " + blockTypeValue);
                  */

            // Only allowed on Opt Out

            if (blockTypeRowValue == "O" && blockTypeValue == "O")
            {
                document.getElementById("invalidOpt").style.display = "block";
                count++;
            }

            // Opt Out can only be after all existing blocks
            if (blockTypeValue == "O")
            {
                if (iStartDate <= rowEndDateValue)
                {
                    document.getElementById("invalidOptStart").style.display = "block";
                    count++;
                }
            }

            // Check to make sure there are no overlapping dates
            if (blockTypeRowValue == "O")
            {
                if (rowStartDate <= iEndDate)
                {
                    document.getElementById("blockOverlap").style.display = "block";
                    count++;
                }
            }

            if (blockTypeValue == "S" || blockTypeValue == "H")
            {
                // Starts before, ends anytime after start
                if (iStartDate < rowStartDateValue && iEndDate >= rowStartDateValue)
                {
                    document.getElementById("blockOverlap").style.display = "block";
                    count++;
                }
                // Starts inside, ends inside
                if (iStartDate >= rowStartDateValue && iEndDate <= rowEndDateValue)
                {
                    document.getElementById("blockOverlap").style.display = "block";
                    count++;
                }
                // Starts inside, ends after
                if (iStartDate >= rowStartDateValue && iStartDate <= rowEndDateValue)
                {
                    document.getElementById("blockOverlap").style.display = "block";
                    count++;
                }
            }

            // Get the block type of the next row
            blockCount++;
            blockTypeExisting = document.getElementById("codblockingBlockType" + blockCount);
        }


			if(count > 0)
				return false;
		
	
			
		// check the unblock dates....
		
		
		for( var i=1; i <= floristMaint.totalcod.value; i++)
		{

		if(document.getElementById('codblock' + i) != null)
		{
			if(document.getElementById('codblock' + i).checked == true)
			{
				if(document.getElementById('codunblockdate' + i) != null)
				{
					if(document.getElementById('codunblockdate' + i).value == "")
					{
						document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
						document.getElementById("errorUnblock").style.display = "block";
						document.getElementById("errorholdUnblock").style.display = "none";
						count++;
					}
					else
					{
					
					
					dateArray = document.getElementById('codunblockdate' + i).value.split("/");

			   		
			   		

					if(dateArray.length != 3)
					{
							document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
							document.getElementById("errorUnblock").style.display = "block";
							document.getElementById("errorholdUnblock").style.display = "none";
							count++;
					}
					else
					{
						if(dateArray[2].length == 2)
						{
							dateArray[2] = "20" + dateArray[2];
							document.getElementById('codunblockdate' + i).value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
						}
					}
					if(dateArray[2] != null)
					{
						if(dateArray[2].length != 2)
						{
							if(dateArray[2].length != 4)
							{
							document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
							document.getElementById("errorUnblock").style.display = "block";
							document.getElementById("errorholdUnblock").style.display = "none";
							count++;
							}
							else
							{
								if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
							     {
							   			document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
										document.getElementById("errorUnblock").style.display = "block";
										document.getElementById("errorholdUnblock").style.display = "none";
										count++;
							     }
							}
						}
					}

					
					
						iDate = Date.parse(document.getElementById('codunblockdate' + i).value);
	
						todayDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());
	
						iDate = iDate / 86400000;
						todayDate = todayDate / 86400000;
	
						var diff = iDate - todayDate;
	
						if(diff < 0)
						{
							document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
							document.getElementById("errorUnblock").style.display = "block";
							document.getElementById("errorholdUnblock").style.display = "none";
							count++;
						}
						
					}
				}
			}
		}
		
		}
		
		
		if(count > 0)
			return false;
		else
			return true;
		
}

function doCodDropChanged()
{

if(floristMaint.codlist.value != "")
	floristMaint.CodAdd.disabled = false;
else
	floristMaint.CodAdd.disabled = true;
}

function permBlockCheck(tempNum) {
    if (document.getElementById('codpermblock' + tempNum).checked) {
        document.getElementById('codblock' + tempNum).checked = false;
    }
    enSave();
}

function tempBlockCheck(tempNum) {
    if (document.getElementById('codblock' + tempNum).checked) {
        document.getElementById('codpermblock' + tempNum).checked = false;
    }
    enSave();
}

]]>
	</script>
									<tr>
										<td>
											<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
												
												
														<tr>



													<td  width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>

													</td>
													<td width="10%"><input type="hidden" name="dchange" value="{floristData/generalData/floristUnblockDate}"/></td>


													
													<td width="40%">
													&nbsp; &nbsp; &nbsp;
													<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
													<xsl:if test="$goto = 'Y'">
													GoTo Florist
													</xsl:if> &nbsp;
													</td>

												</tr>
												
												
												
												<tr bordercolor="white">
													<td id="errorUnblock" align="center" width="50%" class="RequiredFieldTxt" style="display:none">
													The highlighted fields are required or invalid.
													</td>
													
													<td id="errorholdUnblock" align="center" width="50%" class="RequiredFieldTxt" style="display:block">
													
													</td>


													<td class="label" align="right" width="10%" id="codstatuslabel"> Status </td>
                                                    <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="tblheader" align="left"> Florist Blocking / Suspension </td>
									</tr>


                                    <xsl:variable name="hardBlockUpdate" select="key('securityPermissions', 'hardBlockUpdate')/value" />
                                    <xsl:variable name="optOutBlockUpdate" select="key('securityPermissions', 'optOutBlockUpdate')/value" />
                                    <input type="hidden" name="hardBlockUpdate" id="hardBlockUpdate" value="{key('securityPermissions', 'hardBlockUpdate')/value}" />
                                    <input type="hidden" name="optOutBlockUpdate" id="optOutBlockUpdate" value="{key('securityPermissions', 'optOutBlockUpdate')/value}" />
									<tr>
									  <td>

									    <table  border="0" align="left">

									    <!-- Active value means null block type so pass empty value if active selected -->
									

                                          <tr>
                                            <td class="tblheader" align="left" width="20%">Type</td>
                                            <td class="tblheader" align="left" width="20%">Start Date</td>
                                            <td class="tblheader" align="left" width="20%">End Date</td>
                                            <td class="tblheader" align="left" width="20%">User</td>
                                            <td class="tblheader" align="left" width="20%">Remove</td>
                                          </tr>
                                          <xsl:for-each select="floristData/floristBlockList/floristBlock">
                                          <tr>
                                            <input name="codBlockingBlockType{position()}" type="hidden" value="{blockType}"/>
                                            <input name="codBlockingStartDate{position()}" type="hidden" value="{blockStartDate}"/>
                                            <input name="codBlockingEndDate{position()}" type="hidden" value="{blockEndDate}"/>
                                            <input name="codBlocking{position()}" type="hidden" value="{blockStartDate}"/>
                                            <xsl:choose>
                                              <xsl:when test="blockType = 'S'">
                                                <td>Soft</td>
                                              </xsl:when>
                                              <xsl:when test="blockType = 'H'">
                                                <td>Hard</td>
                                              </xsl:when>
                                              <xsl:when test="blockType = 'O'">
                                                <td>Opt Out</td>
                                              </xsl:when>
                                            </xsl:choose>
                                            <td><xsl:value-of select="blockStartDate" /></td>
                                            <td><xsl:value-of select="blockEndDate" /></td>
                                            <td><xsl:value-of select="blockUser" /></td>
									        <xsl:choose>
									        <xsl:when test="$hardBlockUpdate = 'N' and blockType = 'H'">
                                              <td></td>
									        </xsl:when>
									        <xsl:otherwise>
										      <xsl:choose>
										      <xsl:when test="$optOutBlockUpdate = 'N' and blockType = 'O'">
                                                <td></td>
										      </xsl:when>
										      <xsl:otherwise>
                                                <td>
                                                    <input type="checkbox"  name="codBlockingRemove{position()}" onclick="dChanges();"/>
                                                </td>
										      </xsl:otherwise>
										    </xsl:choose>
									          </xsl:otherwise>
									        </xsl:choose>
                                          </tr>
                                          </xsl:for-each>
                                        
                                          <xsl:variable name="suspendType" select="floristData/generalData/suspendType" />
										  <xsl:if test="$suspendType != ''">
										    <tr>
										      <td>
										        <xsl:choose>
										          <xsl:when test="$suspendType = 'G'">
										            GoTo Suspend
										          </xsl:when>
										          <xsl:otherwise>
										            Mercury Suspend
										          </xsl:otherwise>
										        </xsl:choose>
										      </td>
										      <td>
										        <xsl:value-of select="floristData/generalData/suspendStartDate"/>
										      </td>
										      <td>
										        <xsl:value-of select="floristData/generalData/suspendEndDate"/>
										      </td>
										      <td>OSP</td>
										    </tr>
                                          </xsl:if>

									      <tr>
                                            <td>
                                              <select name="codNewBlockType" id="codNewBlockType" onblur="blockTypeChanged();">
                                                <option value="A">Select...</option>
                                                <option value="S">Soft</option>
                                                <xsl:if test="$hardBlockUpdate = 'Y'">
                                                <option value="H">Hard</option>
                                                </xsl:if>
                                                <xsl:if test="$optOutBlockUpdate = 'Y'">
                                                <option value="O">Opt Out</option>
                                                </xsl:if>
                                              </select>
                                            </td>
                                            <td>
                                              <input name="codNewBlockStartDate" type="text" maxlength="10" />&nbsp;&nbsp;
                                              <input type="image" id="calendarBlockStart" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>

                                              <script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "codNewBlockStartDate",  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendarBlockStart" // ID of the button
													    }
													  );

													]]></script>
                                            </td>
                                            <td>
                                              <input name="codNewBlockEndDate" type="text" maxlength="10" />&nbsp;&nbsp;
                                              <input type="image" id="calendarBlockEnd" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>

                                              <script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "codNewBlockEndDate",  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendarBlockEnd" // ID of the button
													    }
													  );

													]]></script>
                                            </td>
                                            <td colspan="2">
										      <table border="0" align="left">
										        <tr>
										          <td align="left" id="errorDate" class="RequiredFieldTxt" style="display:none">
												      Date must be greater than today's date.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="missingDate" class="RequiredFieldTxt" style="display:none">
												      Date is required.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="invalidDate" class="RequiredFieldTxt" style="display:none">
												      Invalid Date.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="startAfterToday" class="RequiredFieldTxt" style="display:none">
												      Start Date must be equal to or after the current date.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="startBeforeEnd" class="RequiredFieldTxt" style="display:none">
												      Start Date must be equal to or before End Date.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="endAfterToday" class="RequiredFieldTxt" style="display:none">
												      End Date must be equal to or after the current date.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="blockOverlap" class="RequiredFieldTxt" style="display:none">
												      Date range must not overlap with any existing blocks.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="invalidSoft" class="RequiredFieldTxt" style="display:none">
												      Date range must be less than or equal to 30 days.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="invalidHard" class="RequiredFieldTxt" style="display:none">
												      Date range must be greater than 30 days and less than or equal to 90 days.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="invalidOpt" class="RequiredFieldTxt" style="display:none">
												      Only one Opt Out type block can be added.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="invalidOptStart" class="RequiredFieldTxt" style="display:none">
												      Opt Out blocks cannot have a start date before existing blocks end.
					 					          </td>
                                                </tr>
										        <tr>
					 					          <td align="left" id="invalidSoft30Perm" class="RequiredFieldTxt" style="display:none">
												      You do not have the permissions to add a Soft block with a start date more than 30 days after the current date.
					 					          </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>

					 					
									<tr>
										<td class="tblheader" align="left"> Codification </td>
									</tr>
				

	
									<tr>
										<td>
													
												<input type="hidden" name="totalcod" value="{count(floristData/codifiedProductList/codifiedProduct/id)}"></input>
																						
																																										
											<select name="codlist" tabindex="4"  onclick="doCodDropChanged();">
													<option value=""> Select... </option>
													<xsl:for-each select="codificationList/codes">
													    <xsl:sort select="codification_id"/>
														<xsl:variable name="cid" select="codification_id" />
														<xsl:variable name="cdname" select="description" />
			
														
														<xsl:choose>
											              <xsl:when test="/root/floristData/codifiedProductList/codifiedProduct[id = $cid]/id">
											              	<!-- Skip this because it is already on this florist -->
											              </xsl:when>
											              <xsl:otherwise>
											                <option value="{codification_id}">
															<xsl:value-of select="description" />
															&nbsp;
															<xsl:value-of select="codification_id" />
														</option>
											              </xsl:otherwise>
											            </xsl:choose>
														
														
														
														
													</xsl:for-each>
											</select>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="button" tabindex="5" value="Add" name="CodAdd" onClick="doCodAddAction();" disabled="true"/>
										</td>
									</tr>


									<tr><td><table width="100%">
									
										<tr>
										<td class="tblheader" align="left" width="20%">Everyday</td>
										<td class="tblheader" align="center" width="8%">Permanent<br/>Block</td>
										<td class="tblheader" align="center" width="8%">Temporary<br/>Block</td>
										<td class="tblheader" align="center" width="14%">Unblock Date</td>
										<td class="tblheader" align="center" width="20%"></td>
										<td class="tblheader" align="center" width="8%">Permanent<br/>Block</td>
										<td class="tblheader" align="center" width="8%">Temporary<br/>Block</td>
										<td class="tblheader" align="center" width="14%">Unblock Date
										<input type="hidden" name="codChecksStart"/>
										</td>
										</tr>
										
										<!-- the everyday category -->
										<script><![CDATA[var count=0;]]> </script>
										<xsl:for-each select="floristData/codifiedProductList/codifiedProduct">
										            <xsl:sort select="id"/>
													<input name="codcat{position()}" type="hidden" value="{category}"/>

													<xsl:variable name="catg" select="category" />
													<xsl:if test="$catg = 'Everyday'">

													<script><![CDATA[count = count+1;]]> </script>

													<script><![CDATA[
													if((count % 2) != 0)
														document.write("<tr>");
													]]></script>

													<td>
														<xsl:value-of select="name" />
														&nbsp; &nbsp;
														<input name="codid{position()}" type="hidden" value="{id}" tabindex="6"/>
														<xsl:value-of select="id" />
													</td>

													<xsl:variable name="codorblock" select="blockedFlag" />

													<td align="center">
														<input type="checkbox" name="codpermblock{position()}" tabindex="9" onclick="permBlockCheck({position()});">
															<xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
															<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																<xsl:attribute name="DISABLED" />
															</xsl:if>
														</input>
													</td>

													<td align="center">
														<input type="checkbox" name="codblock{position()}" tabindex="9" onclick="tempBlockCheck({position()});">
															<xsl:if test="$codorblock = 'Y' and blockEndDate != ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
															<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																<xsl:attribute name="DISABLED" />
															</xsl:if>
														</input>
													</td>

													<td align="center">

													<input type="text" tabindex="9" size="10" name="codunblockdate{position()}" value="{blockEndDate}">
														<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
															<xsl:attribute name="DISABLED" />
														</xsl:if>
													</input>
													&nbsp; &nbsp;
													<input type="image" tabindex="9" id="calendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
														<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
															<xsl:attribute name="DISABLED" />
														</xsl:if>
													</input>
													<xsl:variable name="lock" select="position()"/>

													<script>
														var calCount =
														<xsl:value-of select="$lock"/>;
														calCount = calCount + "";
													</script>
													</td>

													<script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "codunblockdate"+calCount,  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendar"+calCount  // ID of the button
													    }
													  );

													if((count % 2) == 0)
													{
														document.write("</tr>");
													}
													]]></script>


													</xsl:if>
										</xsl:for-each>


										

									<tr></tr>

									
										<tr>
										<td class="tblheader" align="left">Holiday</td>
										<td class="tblheader" align="center">Permanent<br/>Block</td>
										<td class="tblheader" align="center">Temporary<br/>Block</td>
										<td class="tblheader" align="center">Unblock Date</td>
										<td class="tblheader" align="center"></td>
										<td class="tblheader" align="center">Permanent<br/>Block</td>
										<td class="tblheader" align="center">Temporary<br/>Block</td>
										<td class="tblheader" align="center">Unblock Date</td>
										</tr>

										<!-- the holiday category -->
										<script><![CDATA[var count=0;]]> </script>
										<xsl:for-each select="floristData/codifiedProductList/codifiedProduct">
                                                <xsl:sort select="id"/>

												<xsl:variable name="catg" select="category" />
													<xsl:if test="$catg = 'Holiday'">

													<script><![CDATA[count = count+1;]]> </script>

													<script><![CDATA[
													if((count % 2) != 0)
														document.write("<tr>");
													]]></script>

													<td>
														<xsl:value-of select="name" />
														&nbsp; &nbsp;
														<input name="codid{position()}" type="hidden" value="{id}" tabindex="7"/>
														<xsl:value-of select="id" />
													</td>

													<xsl:variable name="codorblock" select="blockedFlag" />

													<td align="center">
														<input type="checkbox" name="codpermblock{position()}" tabindex="9" onclick="permBlockCheck({position()});">
															<xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
															<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																<xsl:attribute name="DISABLED" />
															</xsl:if>
														</input>
													</td>

													<td align="center">
														<input type="checkbox" name="codblock{position()}" tabindex="9" onclick="tempBlockCheck({position()});">
															<xsl:if test="$codorblock = 'Y' and blockEndDate != ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
															<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																<xsl:attribute name="DISABLED" />
															</xsl:if>
														</input>
													</td>

													<td align="center">

													<input type="text" tabindex="9" size="10" name="codunblockdate{position()}" value="{blockEndDate}">
														<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
															<xsl:attribute name="DISABLED" />
														</xsl:if>
													</input>
													&nbsp; &nbsp;
													<input type="image" tabindex="9" id="calendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
														<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
															<xsl:attribute name="DISABLED" />
														</xsl:if>
													</input>
													<xsl:variable name="lock" select="position()"/>

													<script>
														var calCount =
														<xsl:value-of select="$lock"/>;
														calCount = calCount + "";
													</script>
													</td>

													<script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "codunblockdate"+calCount,  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendar"+calCount  // ID of the button
													    }
													  );

													if((count % 2) == 0)
													{
														document.write("</tr>");
													}
													]]></script>


													</xsl:if>
										</xsl:for-each>

										
									<tr><td><input type="hidden" name="codChecksEnd"/></td></tr>
										
									
										<tr>
										<td class="tblheader" align="left">Other</td>
										<td class="tblheader" align="center">Permanent<br/>Block</td>
										<td class="tblheader" align="center">Temporary<br/>Block</td>
										<td class="tblheader" align="center">Unblock Date</td>
										<td class="tblheader" align="center"></td>
										<td class="tblheader" align="center">Permanent<br/>Block</td>
										<td class="tblheader" align="center">Temporary<br/>Block</td>
										<td class="tblheader" align="center">Unblock Date</td>
										</tr>


										<!-- the Other category -->
										<script><![CDATA[var count=0;]]> </script>
										<xsl:for-each select="floristData/codifiedProductList/codifiedProduct">
                                                <xsl:sort select="id"/>

												<xsl:variable name="catg" select="category" />
													<xsl:if test="$catg = 'Other'">
													<script><![CDATA[count = count+1;]]> </script>

													<script><![CDATA[
													if((count % 2) != 0)
														document.write("<tr>");
													]]></script>

													<td>
														<xsl:value-of select="name" />
														&nbsp; &nbsp;
														<input name="codid{position()}" type="hidden" value="{id}"/>
														<xsl:value-of select="id" />
													</td>

													<xsl:variable name="codorblock" select="blockedFlag" />

													<td align="center">
														<input type="checkbox" name="codpermblock{position()}" tabindex="9" onclick="permBlockCheck({position()});">
															<xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
															<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																<xsl:attribute name="DISABLED" />
															</xsl:if>
														</input>
													</td>

													<td align="center">
														<input type="checkbox" name="codblock{position()}" tabindex="9" onclick="tempBlockCheck({position()});">
															<xsl:if test="$codorblock = 'Y' and blockEndDate != ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
															<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																<xsl:attribute name="DISABLED" />
															</xsl:if>
														</input>
													</td>

													<td align="center">

													<input type="text" tabindex="9" size="10" name="codunblockdate{position()}" value="{blockEndDate}">
														<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
															<xsl:attribute name="DISABLED" />
														</xsl:if>
													</input>
													&nbsp; &nbsp;
													<input type="image" tabindex="9" id="calendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
														<xsl:if test="$codorblock = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
															<xsl:attribute name="DISABLED" />
														</xsl:if>
													</input>
													<xsl:variable name="lock" select="position()"/>

													<script>
														var calCount =
														<xsl:value-of select="$lock"/>;
														calCount = calCount + "";
													</script>
													</td>

													<script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "codunblockdate"+calCount,  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendar"+calCount  // ID of the button
													    }
													  );

													if((count % 2) == 0)
													{
														document.write("</tr>");
													}
													]]></script>


													</xsl:if>
										</xsl:for-each>



									
										<tr>									
										<td class="tblheader" align="left" colspan="8">Mercury Flag</td>

										</tr>
									
													<tr>

													<td align="left" colspan="8">
													
													<xsl:variable name="mm" select="//mercuryMachine" />


													<input type="radio" name="mercuryflag" value="Y" id="mercYes"  tabindex="10" onclick="enSave();">
													<xsl:if test="$mm = 'Y'">
														<xsl:attribute name="CHECKED" />
													</xsl:if>
													Yes
													</input>


													<input type="radio" name="mercuryflag" value="N" id="mercNo" tabindex="10" onclick="enSave();">
													<xsl:if test="$mm != 'Y'">
														<xsl:attribute name="CHECKED" />
													</xsl:if>
													No
													</input>

													</td>



													</tr>


									
									<tr>


										<td colspan="8" class="tblheader" align="left">Half and Full Day Closures</td>

										
									</tr>
									
									<tr>
									<td colspan="8" align="left">
									
									<xsl:variable name="oveRideFlag" select="floristData/generalData/closureOverrideFlag"/>
									<table width="50%" border="0" cellpadding="2" cellspacing="2">
										<tr>
											<td width="30%" class="colHeaderCenter">Day</td>
											<td width="40%" class="colHeaderCenter">JDE Status</td>
											<td width="30%">
												<xsl:if test="$oveRideFlag = 'Y'">
													<a href="javascript:doShowClosureOverride();" tabindex="1">Closure Overrides</a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
												</xsl:if>
											</td>
										</tr>
														
										<xsl:for-each select="floristData/closureList/closure">
											<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">
										        	<tr class="resultsCellEven">
													<td width="30%" align="center">
														<xsl:value-of select="day" />
													</td>
													<td width="40%" align="center">
														<xsl:value-of select="jdestatus" />
													</td>
													<td class="resultsCellWhite" width="30%">
														 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
													</td>
											        </tr>
											</xsl:when>
											<xsl:otherwise>
											        <tr>
													<td width="30%" align="center">
														<xsl:value-of select="day" />
													</td>
													<td width="40%" align="center">
														<xsl:value-of select="jdestatus" />
													</td>
													<td width="30%">
														 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
													</td>
											        </tr>
											</xsl:otherwise>
											</xsl:choose>
										</xsl:for-each>	
									</table>
									</td>	
									</tr>
									<tr>


										<td colspan="8" class="tblheader" align="left">Minimum Order</td>


									</tr>
									<tr>
									<td colspan="8" align="left"><input type="text" align="center" value="{//minOrder}" name="minorderamount" size="10" tabindex="11"></input></td>
									</tr>
									
									</table></td></tr>

									<tr><td align="center"><input type="button" align="center" value="SAVE" name="savecod" size="10" tabindex="12" onClick="saveAction();"></input></td></tr>

	
	</xsl:template>
</xsl:stylesheet>
