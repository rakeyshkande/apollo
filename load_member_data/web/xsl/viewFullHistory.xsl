<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">

	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
 			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<title>FTD: History</title>
			</head>
			<script type="text/javascript" language="javascript">
			<![CDATA[
				function loadHistory(c) // to filter the table that shows the history of the florist.
{

var sel = fullHist.hisdis.selectedIndex;
var i=0;

if(sel == 0) // show all....
{
for(i = 1; i <= c; i++)
{
	if(document.getElementById("disAdjustments"+i) != null)
		document.getElementById("disAdjustments"+i).style.display = "block";
	if(document.getElementById("disBlocks"+i) != null)
		document.getElementById("disBlocks"+i).style.display = "block";
	if(document.getElementById("disCodification"+i) != null)
		document.getElementById("disCodification"+i).style.display = "block";
	if(document.getElementById("disCompliments"+i) != null)
		document.getElementById("disCompliments"+i).style.display = "block";
	if(document.getElementById("disProblems"+i) != null)
		document.getElementById("disProblems"+i).style.display = "block";
	if(document.getElementById("disProfile"+i) != null)
		document.getElementById("disProfile"+i).style.display = "block";
	if(document.getElementById("disWeight"+i) != null)
		document.getElementById("disWeight"+i).style.display = "block";
	if(document.getElementById("disZip Codes"+i) != null)
		document.getElementById("disZip Codes"+i).style.display = "block";
	if(document.getElementById("disCities"+i) != null)
		document.getElementById("disCities"+i).style.display = "block";
}
return;
}

// hide all....
for(i = 1; i <= c; i++)
{
	if(document.getElementById("disAdjustments"+i) != null)
		document.getElementById("disAdjustments"+i).style.display = "none";
	if(document.getElementById("disBlocks"+i) != null)
		document.getElementById("disBlocks"+i).style.display = "none";
	if(document.getElementById("disCodification"+i) != null)
		document.getElementById("disCodification"+i).style.display = "none";
	if(document.getElementById("disCompliments"+i) != null)
		document.getElementById("disCompliments"+i).style.display = "none";
	if(document.getElementById("disProblems"+i) != null)
		document.getElementById("disProblems"+i).style.display = "none";
	if(document.getElementById("disProfile"+i) != null)
		document.getElementById("disProfile"+i).style.display = "none";
	if(document.getElementById("disWeight"+i) != null)
		document.getElementById("disWeight"+i).style.display = "none";
	if(document.getElementById("disZip Codes"+i) != null)
		document.getElementById("disZip Codes"+i).style.display = "none";
	if(document.getElementById("disCities"+i) != null)
		document.getElementById("disCities"+i).style.display = "none";
}

if(sel == 1) // show adjustments
{
for(i = 1; i <= c; i++)
{
	if(document.getElementById("disAdjustments"+i) != null)
		document.getElementById("disAdjustments"+i).style.display = "block";
}
return;
}

if(sel == 2) // show blocks
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disBlocks"+i) != null)
			document.getElementById("disBlocks"+i).style.display = "block";
	}
	return;
}

if(sel == 3) // show Cities
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disCities"+i) != null)
			document.getElementById("disCities"+i).style.display = "block";
	}
	return;
}

if(sel == 4) // show Codification
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disCodification"+i) != null)
			document.getElementById("disCodification"+i).style.display = "block";
	}
	return;
}

if(sel == 5) // show Compliments
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disCompliments"+i) != null)
			document.getElementById("disCompliments"+i).style.display = "block";
	}
	return;
}


if(sel == 6) // show Problems
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disProblems"+i) != null)
			document.getElementById("disProblems"+i).style.display = "block";
	}
	return;
}

if(sel == 7) // show Profile
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disProfile"+i) != null)
			document.getElementById("disProfile"+i).style.display = "block";
	}
	return;
}

if(sel == 8) // show Weight
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disWeight"+i) != null)
			document.getElementById("disWeight"+i).style.display = "block";
	}
	return;
}

if(sel == 9) // show Zip Codes
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disZip Codes"+i) != null)
			document.getElementById("disZip Codes"+i).style.display = "block";
	}
	return;
}

}


	function doCloseAction()
	{
        	window.close();
   }
			]]>
			</script>

		<body>
		<form name="fullHist" method="post">
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="'Florist History'" />
				<xsl:with-param name="showExitButton" select="false()"/>
				<xsl:with-param name="showTime" select="false()"/>
			</xsl:call-template>

        <center>

			<input type="button" value=" Close " onClick="doCloseAction();"/>

      </center>
      <br></br>
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td  colspan="9" class="tblheader" align="left"> History </td>
			</tr>

			<tr>


				<td colspan="9" width="100%" align="center">

					<div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">

			


													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
																<td width="10%" class="colHeaderCenter">ID</td>
																<td width="10%" class="colHeaderCenter">Date</td>
																<td width="5%" class="colHeaderCenter">Disposition</td>
																<td width="70%" class="colHeaderCenter">Comments</td>
														</tr>

														<tr>
														<td width="10%"/>
														<td width="10%"/>
														<script> var nHistory="0";</script>
														<td width="5" align="center">
														<select name="hisdis" onchange="loadHistory(nHistory);" tabindex="2">
														<option value="All" selected="true">All</option>
														<option value="Adjustments">Adjustments</option>
														<option value="Blocks">Blocks</option>
														<option value="Cities">Cities</option>
														<option value="Codification">Codification</option>
														<option value="Compliments">Compliments</option>
														<option value="Problems">Problems</option>
														<option value="Profile">Profile</option>
														<option value="Weight">Weight</option>
														<option value="Zip Codes">Zip Codes</option>
														</select>
														</td>
														<td width="70%"/>
														</tr>


														<xsl:for-each select="HISTORY/COUNT">

														<xsl:variable name="isManager" select="manager_only_flag" />

														<xsl:choose>
													      <xsl:when test="$isManager = 'Y' and key('pageData', 'permit')/value = 'N'"></xsl:when>
									      				<xsl:otherwise>

														<tr id="dis{comment_type}{position()}" style="display:block">
														<script>nHistory++;</script>

														<td width="10%" align="center">
															<xsl:value-of select="created_by" />
														</td>

														<td width="10%" align="center">

															<xsl:value-of select="comment_date_char" />
														</td>

														<td width="5%" align="center">

														<xsl:value-of select="comment_type" />

														</td>

														<td width="70%" align="center">
															<xsl:value-of select="florist_comment" />
														</td>

														</tr>


													    </xsl:otherwise>
													    </xsl:choose>

														</xsl:for-each>
													</table>
													</div>
				</td>
			</tr>

			</table>
				<br></br>
			 <center>

			<input type="button" value=" Close " onClick="doCloseAction();"/>

      </center>

													</form>
													</body>
	</html>
													
</xsl:template>
</xsl:stylesheet>