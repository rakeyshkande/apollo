<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">
	
<html>
<head>
				
<script type="text/javascript" language="javascript">
<![CDATA[


function init()
{
	if(window.dialogArguments == "N")
	{
		document.getElementById("newmanagerflag").disabled = true;
	}
	
}

	function doCloseAction()
	{
		var response;
		
 		if(addHistory.newcomment.value != "")
 		{
			response = confirm("Do you want to close without saving the comments?");
			if (!response)
			{
				return;
			}
	 		else
	 		{
	        	window.close();
	        }
        }
        else
        	window.close();
 	}
 	
 	function doSaveAction()
 	{
 		document.getElementById("errorDisposition").style.display = "none";
 		
 		if(addHistory.dispositionDescription.value == "null")
 		{
 		document.getElementById("errorDisposition").style.display = "block";
 		return;
 		}
 	
 		if(addHistory.newcomment.value.length < 1)
 		{
 			alert("Comments can not be empty.");
 			addHistory.newcomment.focus();
 			return;
 		}
 	
  		var ret = new Array();
        ret[0] = addHistory.flnum.value;
        ret[1] = addHistory.dispositionDescription.value;
        ret[2] = addHistory.newcomment.value;
        ret[3] = addHistory.newmanagerflag.checked;
	

        window.returnValue = ret;
	 	window.close();
 	
 	}

]]>
</script>
				<title>FTD - Florist Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>

			</head>
			<body>
				<form name="addHistory">
					<input type="hidden" name="servlet_action"/>
					<input type="hidden" name="flnum" value="{key('pageData', 'florist_number')/value}"/>
					
					 <!-- Header Template -->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Add History / Comment'" />
						<xsl:with-param name="showExitButton" select="false()"/>
						<xsl:with-param name="showTime" select="false()"/>
					</xsl:call-template>
					
					<table align="center" width="40%">
					<tr>
						<td class="tblheader" align="left">Disposition</td>
					</tr>
					
					<tr>

					<td width="5" align="center">
					<select name="dispositionDescription" tabindex="1">
					<option value="null" selected="true">Select...</option>
					<option value="Adjustments">Adjustments</option>
					<option value="Blocks">Blocks</option>
					<option value="Cities">Cities</option>
					<option value="Codification">Codification</option>
					<option value="Compliments">Compliments</option>
					<option value="Problems">Problems</option>
					<option value="Profile">Profile</option>
					<option value="Weight">Weight</option>
					<option value="Zip Codes">Zip Codes</option>
					</select>
					</td>
					
					
											 	

					</tr>
					<tr>
					<td align="left" id="errorDisposition" class="RequiredFieldTxt" style="display:none">
																Disposition is required.
					</td>
					</tr>
					
					<tr><td> &nbsp; </td></tr>
					<tr>
					
					<tr>
						<td class="tblheader" align="left">Comments</td>
					</tr>
					
					<tr>
					<td>
					<textarea name="newcomment" wrap="physical" cols="60" rows="8" tabindex="2">
					</textarea>
					</td>
				
					</tr>
					
					<tr><td> &nbsp; </td></tr>
					
					<tr>
					<td>
					<input type="checkbox" id="newmanagerflag" tabindex="3"> Manager Comment </input>
					</td>
					</tr>
					
					<tr><td> &nbsp; </td></tr>
					<tr align="center">
					
					<td>
					
					<input type="button"  tabindex="4" value="  Save  " onClick="doSaveAction();"/>
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					<input type="button" tabindex="5" value=" Close " onClick="doCloseAction();"/>
					
					</td>
					
					</tr>
					
					</tr>
					
					</table>

					</form>
					<script>init();</script>
				</body>
			</html>
				</xsl:template>
</xsl:stylesheet>