
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	
	<!-- security parameters -->
	<xsl:param name="web-context-root"/>
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="adminAction"/>
	
	<!--  Keys -->
	<xsl:key name="security" match="/root/security/data" use="name" />
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name" />
	<xsl:key name="securityPermissions" match="/root/securityPermissions/permission" use="name" />
<xsl:output method="html" indent="yes"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Florist Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<script type="text/javascript" language="javascript" src="../js/util.js"/>
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
				<script type="text/javascript" language="javascript">

		var usStates = new Array(
					<xsl:for-each select="stateList/state[countrycode='']">
					["<xsl:value-of select="statemasterid"/>", " <xsl:value-of select="statename"/>"]
						<xsl:choose>
							<xsl:when test="position()!=last()">,</xsl:when>
						</xsl:choose>
					</xsl:for-each>);

        var flStatuses  = new Array( 
					<xsl:for-each select="statusList/status">
     				["<xsl:value-of select="status"/>"]
						<xsl:choose>
							<xsl:when test="position()!=last()">,</xsl:when>
						</xsl:choose>
					</xsl:for-each>);
		
<![CDATA[

	function doSearchAction()
	{
		resetErrors();
	
		if (window.event)
			if (window.event.keyCode)
				if (window.event.keyCode != 13)
					return false;
		var pass = validateForm();
		if(pass == false)
			return;
		performAction(document.forms[0].action);
	}

	function doClearAction()
	{
	    floristSearch.sc_floristnum.value = "";
	    floristSearch.sc_zipcode.value = "";
	    floristSearch.sc_city.value = "";
	    floristSearch.sc_state.selectedIndex = 0;
	    floristSearch.sc_statuslist.selectedIndex = 0;
	    floristSearch.sc_shopname.value = "";
	    floristSearch.sc_phonenumber.value = "";
	    floristSearch.sc_product.value = "";
	    document.getElementById("pcod").checked = true;
	    floristSearch.sc_floristnum.focus();
	}
	function performAction(url)
	{
		showWaitMessage("content", "wait", "Searching");
		document.forms[0].action = url;
		document.forms[0].submit();
	 }
	
	function init()
	{
		
		checkChange();
		
		resetErrors();
		
		if(floristSearch.permitAddNew.value == "N")
		{
			floristSearch.new1.disabled = true;
			floristSearch.new2.disabled = true;
		}
		
		if(	document.getElementById("headText").firstChild.data.length > 0)
		{
			document.getElementById("errorZipServer").style.display = "block";
			document.getElementById("errorZip").style.display = "none";
		}
		
		if(floristSearch.viewtype.value == "Y")
		{
		// diasble all fields...
		floristSearch.sc_zipcode.disabled = true;
		floristSearch.sc_zipcode.style.backgroundColor = 'silver';
		
		floristSearch.sc_city.disabled = true;
		floristSearch.sc_city.style.backgroundColor = 'silver';
		
		floristSearch.sc_state.disabled = true;
		floristSearch.sc_state.style.backgroundColor = 'silver';
		
		floristSearch.sc_shopname.disabled = true;
		floristSearch.sc_shopname.style.backgroundColor = 'silver';
		
		floristSearch.sc_phonenumber.disabled = true;
		floristSearch.sc_phonenumber.style.backgroundColor = 'silver';
		
		floristSearch.sc_statuslist.disabled = true;
		floristSearch.sc_statuslist.style.backgroundColor = 'silver';
		
		floristSearch.sc_product.disabled = true;
		floristSearch.sc_product.style.backgroundColor = 'silver';
		
		}
		
				
	}
	
	function doLast(nCount)
	{
		showWaitMessage("content", "wait", "Searching");
		document.forms[0].action = "FloristSearchServlet?action=last&uid=test&count="+nCount;
		document.forms[0].submit();
	}
	
	function resetErrors()
	{
		document.getElementById("errorMem").style.display = "none";
		document.getElementById("errorPhone").style.display = "none";
		document.getElementById("errorZipServer").style.display = "none";
		document.getElementById("errorZip").style.display = "none";
		document.getElementById("errorZipCanada").style.display = "none";
	}
	
	
	
	function validateForm()
	{
		resetErrors();
	
	    var vfloristnum = floristSearch.sc_floristnum.value;
	    var vzipcode = floristSearch.sc_zipcode.value;
	    var vcity = floristSearch.sc_city.value;
	    var vstate = floristSearch.sc_state.value;
	    var vshopname = floristSearch.sc_shopname.value;
	    var vphonenumber = floristSearch.sc_phonenumber.value;
		var count = 0;
		var len=0;
		var result = true;
	
		if(trim(vfloristnum).length == 0)
			floristSearch.sc_floristnum.value = "";
		if(trim(vzipcode).length == 0)
			floristSearch.sc_zipcode.value = "";
		if(trim(vcity).length == 0)
			floristSearch.sc_city.value = "";
		if(trim(vshopname).length == 0)
			floristSearch.sc_shopname.value = "";
		if(trim(vphonenumber).length == 0)
			floristSearch.sc_phonenumber.value = "";
			
		// validate florist number
		if(trim(vfloristnum).length > 0)
		{
			len = trim(vfloristnum).length;
		
			if( len < 6)
				count++;
				
			if(len == 7 || len == 9)
			{
				for(var i=0; i<7; i++)
				{
					if(i == 2)
					{
						if(vfloristnum.charAt(i) != "-")
							count++;
						continue;
					}
					if(!isDigit(vfloristnum.charAt(i)))
						count++;
				}
				
				if(len == 9)
				{
					if(!isLetter(vfloristnum.charAt(7)))
						count++;
					if(!isLetter(vfloristnum.charAt(8)))
						count++;
				}
			}
			
			if(len == 6 || len == 8)
			{
				for(var i=0; i<6; i++)
				{
					if(!isDigit(vfloristnum.charAt(i)))
						count++;
				}
				if(len == 8)
				{
					if(!isLetter(vfloristnum.charAt(6)))
							count++;
					if(!isLetter(vfloristnum.charAt(7)))
							count++;
				}
			}
			
			if(count > 0)
			{
				document.getElementById("errorMem").style.display = "block";
				result=false;
			}
		}
		
		// validate zip code...
		if(trim(vzipcode).length > 0)
		{
			count = 0;
			len = trim(vzipcode).length;
			
			
			if(isDigit(vzipcode.charAt(i)))
			{
				for(var i=1; i<len; i++)
				{
					if(!isDigit(vzipcode.charAt(i)))
					{
					document.getElementById("errorZip").style.display = "block";
					return false;
					}
				}
			}
			
			if(len < 3 || len > 7 || len==4 )
			{
				if(isDigit(vzipcode.charAt(0)))
					document.getElementById("errorZip").style.display = "block";
				else
					document.getElementById("errorZipCanada").style.display = "block";
				
				return false;
			}
			else if(len == 5)
			{
				for(var i=0; i<5; i++)
				{
					if(!isDigit(vzipcode.charAt(i)))
						count++;
				}
			}
		
			if(count > 0)
			{
				if(isDigit(vzipcode.charAt(0)))
					document.getElementById("errorZip").style.display = "block";
				else
					document.getElementById("errorZipCanada").style.display = "block";
					
				return false;
			}
		
			if(len < 4)
			{
				if(!isLetter(vzipcode.charAt(0)))
					count++;
				if(!isDigit(vzipcode.charAt(1)))
					count++;
				if(!isLetter(vzipcode.charAt(2)))
					count++;
		
				if(count > 0)
				{
					if(isDigit(vzipcode.charAt(0)))
						document.getElementById("errorZip").style.display = "block";
					else
						document.getElementById("errorZipCanada").style.display = "block";
					return false;
				}
			}
			
			// Validate Canadian ZipCode.
			if(len > 5)
			{
			
				if(isLetter(vzipcode.charAt(0)))
				{
			
				//if(!isLetter(vzipcode.charAt(0)))
				//	count++;
					
				if(!isDigit(vzipcode.charAt(1)))
					count++;
				if(!isLetter(vzipcode.charAt(2)))
					count++
				if(len == 7)
				{
					if(vzipcode.charAt(3) != "-")
						if(vzipcode.charAt(3) != " ")
							count++;
				}
				
				if(!isDigit(vzipcode.charAt(len-3)))
					count++;
				if(!isLetter(vzipcode.charAt(len-2)))
					count++
				if(!isDigit(vzipcode.charAt(len-1)))
					count++;
					
				if(count > 0)
				{
					if(isDigit(vzipcode.charAt(0)))
						document.getElementById("errorZip").style.display = "block";
					else
						document.getElementById("errorZipCanada").style.display = "block";
					return false;
				}
				
				}
				else if(isDigit(vzipcode.charAt(0)))
				{
						document.getElementById("errorZip").style.display = "block";
						return false;
				}
				
				
			}
		
		
		} // end of validate zipcode.
		
		// validate phone number...
		if(trim(vphonenumber).length > 0)
		{
			len = trim(vphonenumber).length;
			var normalizedPhone = stripCharsInBag(vphonenumber, phoneNumberDelimiters)
			if (!isUSPhoneNumber(normalizedPhone, false))
			{
				document.getElementById("errorPhone").style.display = "block";
				return false;
			}
		}
		
		// validate search criteria
		
		count = 0;
	
		if((trim(vfloristnum)).length > 0)
		count++;
	
		if((trim(vzipcode)).length > 0)
		count++;
	
		if((trim(vshopname)).length > 0)
		count++;
	
		if((trim(vphonenumber)).length > 0)
		count++;
		
		if((trim(vcity)).length > 0 && (trim(vstate)).length > 0 )
		count++;
		
		if(count == 0)
		{
			alert("Please enter additional search criteria to perform your search");
			return false;
		}
		
		return result;
	}

	function trim(str)
	{
        str=str.replace(/^\s*(.*)/, "$1");
        str=str.replace(/(.*?)\s*$/, "$1");
        return str;
	}

	// Checks if the 'continue' button should be enabled or not.
	function checkChange()
	{
	 	var vfloristnum = floristSearch.sc_floristnum.value;
	    var vzipcode = floristSearch.sc_zipcode.value;
	    var vcity = floristSearch.sc_city.value;
	    var vstate = floristSearch.sc_state.value;
	    var vshopname = floristSearch.sc_shopname.value;
	    var vphonenumber = floristSearch.sc_phonenumber.value;
	    var vproduct = floristSearch.sc_product.value;
		var result=0;
		
		
		
	 	result = trim(vfloristnum).length + trim(vzipcode).length + (trim(vcity).length * trim(vstate).length);
	 	result = result + trim(vshopname).length + trim(vphonenumber).length + trim(vproduct).length;
		
		if(result > 0)
		{
			floristSearch.con1.disabled=false;
			floristSearch.con2.disabled=false;
		}
		else
		{
			floristSearch.con1.disabled=true;
			floristSearch.con2.disabled=true;
		}
		
      }

  	function doExitAction()
    {
   		document.forms[0].action = "FloristSearchServlet?action=exitSystem";
		document.forms[0].submit();
   		
    }
    
    function contiCheck(inObject)
    {
   		checkChange();
 		document.getElementById(inObject).focus;
    }
    function doNewAction() // to add new florist
    {

		//document.forms[0].isNew.value = "yes";
		document.forms[0].action = "FloristMaintenanceServlet?action=loadNewFlorist";
	 	document.forms[0].submit();
    }
]]>

				</script>
			</head>
			<body>
				<form name="floristSearch" method="post" action="FloristSearchServlet?action=searchFlorist">
					<input type="hidden" name="servlet_action"/>
					
					<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
					<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
					<input type="hidden" name="context" value="{$context}"/>
					<input type="hidden" name="adminAction" value="{$adminAction}"/>
					<input type="hidden" name="permitAddNew" value="{key('securityPermissions', 'addNewView')/value}"/>					
					<input type="hidden" name="sc_includeVendor" value="{key('securityPermissions', 'vendorView')/value}"/>			
					<input type="hidden" name="viewtype" value="{key('securityPermissions', 'searchVendorView')/value}"/>	
					<!-- Header Template -->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Florist Maintenance - Search'" />
						<xsl:with-param name="showExitButton" select="true()"/>
						<xsl:with-param name="showTime" select="true()"/>
					</xsl:call-template>
					<!-- Content to hide once the search begins. -->
					<div id="content" style="display:block" >
						<table width="98%" border="0" align="center" cellpadding="10" cellspacing="1">
							<tr>
								<td id="noresults" width="30%" class="RequiredFieldTxt">
									<xsl:if test="key('pageData', 'message_display')/value != ''">
										<xsl:value-of select="key('pageData', 'message_display')/value" />
									</xsl:if>
								</td>
								<td align="center" width="40%">
									<input type="button" onclick="doLast(25);" value="  Last  " tabindex="14"></input>
										&nbsp; &nbsp; &nbsp;
									<input type="button" onclick="doLast(50);" value=" Last 50" tabindex="15"></input>
		 								&nbsp; &nbsp; &nbsp;
									<input type="button" onclick="javascript:doSearchAction();" value="Continue" disabled="true" name="con1" tabindex="16"></input>
		 								&nbsp; &nbsp; &nbsp;
									<input type="button" name="new1" onclick="javascript:doNewAction();" value="   New  " tabindex="17"></input>
									&nbsp; &nbsp; &nbsp;
									<input type="button" onclick="javascript:doClearAction();" value="  Clear " tabindex="17"></input>
									</td>
								<td width="30%"/>
							</tr>
						</table>
						<table id="" width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
							<tr>
								<td>
									<br></br>
									<table id="" align = "center" cellspacing="2" cellpadding="1">
										<tr>
											<td class="label">Florist #</td>
											<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
											<td>
												<input  onkeyup="javascript:contiCheck('sc_floristnum');" tabindex="1" onchange="checkChange();"  type="text" name="sc_floristnum" size="20" maxlength="9" value="{key('searchCriteria', 'sc_floristnum')/value}"/>
											</td>
											<td id="errorMem" class="RequiredFieldTxt" style="display:none">
												&nbsp; &nbsp; Invalid Format
											</td>
										</tr>
										<tr>
											<td class="label">Zip/Postal Code</td>
											<td>	</td>
											<td>
												<input  onkeyup="javascript:contiCheck('sc_zipcode');" onchange="checkChange();" type="text" name="sc_zipcode" size="20" maxlength="11" value="{key('searchCriteria', 'sc_zipcode')/value}" tabindex="2"></input>
											</td>
											<td id="errorZipServer" class="RequiredFieldTxt" style="display:block">
												&nbsp; &nbsp;
												<xsl:if test="key('pageData', 'zip_display')/value != ''">
													<xsl:value-of select="key('pageData', 'zip_display')/value" />
												</xsl:if>
											</td>
											<td id="errorZip" class="RequiredFieldTxt" style="display:none">
												&nbsp; &nbsp; Invalid Format 99999
											</td>
											<td id="errorZipCanada" class="RequiredFieldTxt" style="display:none">
												&nbsp; &nbsp; Invalid Format A9A or A9A-9A9
											</td>
										</tr>
										<tr>
											<td class="label">City</td>
											<td></td>
											<td>
												<input onkeyup="javascript:contiCheck('sc_city');" type="text" name="sc_city" size="20" maxlength="40" value="{key('searchCriteria', 'sc_city')/value}" tabindex="3"></input>
											</td>
											<td> </td>
											<td> </td>
											<td class="label"> State/Province </td>
											<td>
												<select name="sc_state" tabindex="4"  onclick="checkChange();" onchange="checkChange();">
													<option value="">Select...</option>
													<xsl:for-each select="stateList/state">
														<xsl:variable name="sid" select="statemasterid" />
														<option value="{statemasterid}">
															<xsl:if test="key('searchCriteria', 'sc_state')/value = $sid">
																<xsl:attribute name="SELECTED" />
															</xsl:if>
															<xsl:value-of select="statename" />
														</option>
													</xsl:for-each>
												</select>
											</td>
										</tr>
										<tr>
											<td class="label"> Shop Name </td>
											<td></td>
											<td>
												<input  onkeyup="javascript:contiCheck('sc_shopname');" onchange="checkChange();" type="text" name="sc_shopname" size="20" maxlength="45" tabindex="5" value="{key('searchCriteria', 'sc_shopname')/value}"></input>
											</td>
										</tr>
										<tr>
											<td class="label"> Phone Number </td>
											<td></td>
											<td>
												<input  onkeyup="javascript:contiCheck('sc_phonenumber');" tabindex="6" onchange="checkChange();" type="text" name="sc_phonenumber" size="20" value="{key('searchCriteria', 'sc_phonenumber')/value}"></input>
											</td>
											<td id="errorPhone" class="RequiredFieldTxt"  style="display:none">
												&nbsp; &nbsp; Invalid Format
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td class="label">**Status</td>
											<td></td>
											<td>
												<select name="sc_statuslist" tabindex="7">
													<option> Select... </option>
													<xsl:for-each select="statusList/status">
														<xsl:variable name="stat" select="status" />
														
														<xsl:if test="$stat != 'Inactive'">
														<option value="{status}">
															<xsl:if test="key('searchCriteria', 'sc_statuslist')/value = $stat">
																<xsl:attribute name="SELECTED" />
															</xsl:if>
															<xsl:value-of select="status" />
														</option>
														</xsl:if>
														
														<xsl:if test="$stat = 'Inactive'">
															<xsl:if test="key('securityPermissions', 'inactiveSearchView')/value = 'Y'">
															<option value="{status}">
																<xsl:if test="key('searchCriteria', 'sc_statuslist')/value = $stat">
																	<xsl:attribute name="SELECTED" />
																</xsl:if>
																<xsl:value-of select="status" />
															</option>
															</xsl:if>
														</xsl:if>

													</xsl:for-each>
												</select>
											</td>
										</tr>
										<tr>
											<td class="label"> **Product </td>
											<td></td>
											<td>
												<input onchange="" type="text" name="sc_product" size="20" tabindex="8" value="{key('searchCriteria', 'sc_product')/value}"></input>
											</td>
											<td>
												<input type="radio" id="pcod" name="sc_productType" tabindex="8" value="codification" checked="true">Codification Code</input>
											</td>
											<td>
												<input type="radio" id="psku" name="sc_productType" tabindex="8" value="sku">SKU</input>
											</td>
											<td>
												<input type="radio" id="pname" name="sc_productType" tabindex="8" value="productname" >Product Name</input>
											</td>
											
											<input type="hidden" name="saveType" value="{key('searchCriteria', 'sc_productType')/value}"/>
											
											<script>
												if(floristSearch.saveType.value == "codification")
													document.getElementById("pcod").checked = true;
												else if(floristSearch.saveType.value == "sku")
													document.getElementById("psku").checked = true;
												else if(floristSearch.saveType.value == "productname")
													document.getElementById("pname").checked = true;
											</script>
											
										</tr>
									</table>
									<br></br>
								</td>
							</tr>
						</table>
						<table width="98%" border="0" align="center" cellpadding="10" cellspacing="0">
							<td width="30%"/>
							<td align="center" width="40%">
								<input type="button" onclick="doLast(25);" value="  Last  " tabindex="9"></input>
									&nbsp; &nbsp; &nbsp;
								<input type="button" onclick="doLast(50);" value=" Last 50" tabindex="10"></input>
		 							&nbsp; &nbsp; &nbsp;
								<input type="button" value="Continue" name="con2" disabled="true" onclick="javascript:doSearchAction();" tabindex="11"></input>
									&nbsp; &nbsp; &nbsp;
								<input type="button" name="new2" onclick="javascript:doNewAction();" value="   New  " tabindex="12"></input>
								&nbsp; &nbsp; &nbsp;
									<input type="button" onclick="javascript:doClearAction();" value="  Clear " tabindex="12"></input>
							</td>
							<td width="30%"/>
						</table>
						<br></br>
						<p align="center" class="Label">** Designates Secondary Search Criteria</p>
						
					</div>

				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage" />
										<td id="waitTD" width="50%" class="waitMessage" />
									</tr>
								</table></td>
						</tr>
					</table>
				</div>
				<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
							<tr>
								<td align="right">
									<div align="right" class="button">
										<input type="button" value="Exit" tabindex="13" onclick="javascript:doExitAction();"></input>
									</div>
								</td>
							</tr>
				</table>
				<xsl:call-template name="footer"/>
			</form>
			</body>
			<script>
			init();
			javascript:checkChange();
			floristSearch.sc_floristnum.focus();
			</script>
		</html>
	</xsl:template>
</xsl:stylesheet>