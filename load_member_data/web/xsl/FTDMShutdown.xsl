<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <!-- External Templates -->
    <xsl:import href="header.xsl"/>
    <xsl:import href="footer.xsl"/>
    
    <!-- security parameters -->
	<xsl:param name="web-context-root"/>
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="adminAction"/>
    
    <!--  Keys -->
    <xsl:key name="security" match="/root/security/data" use="name" />
    <xsl:key name="pageData" match="/root/pageData/data" use="name" />
    <xsl:key name="validationField" match="/root/validation" use="name" />
    
    <!-- Internal Templates
      addHeader
      addNavLinks
    -->
    
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/root">
        <html>
            <head>
                <title>FTDM Shutdown</title>
                <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
                <link rel="stylesheet" type="text/css" href="../css/calendar.css"/>
                <script type="text/javascript" language="javascript" src="../js/util.js"/>
                <script type="text/javascript" language="javascript" src="../js/FormChek.js"/>
                <script type="text/javascript" language="javascript" src="../js/calendar.js"/>
                <script type="text/javascript" language="javascript">
                    <![CDATA[
                    
                    
                     // commit subcude - we need to have a commit so we can not have people changing //
 // subcode info on the fly //
                    /*
                     *  Global Variables
                     */
                    var  isModified=false;
                    
                    
                    /*
                     *  Init function
                     */
                    function init()
                    {
                    }//end function init()
                    
                    /*
                     * Date functions
                     */
                    function DateAdd(startDate, numDays, numMonths, numYears)
                    {
                        var returnDate = new Date(startDate.getTime());
                        var yearsToAdd = numYears;
                        
                        var month = returnDate.getMonth()	+ numMonths;
                        if (month > 11)
                        {
                            yearsToAdd = Math.floor((month+1)/12);
                            month -= 12*yearsToAdd;
                            yearsToAdd += numYears;
                            
                        }//end if
                        
                        returnDate.setMonth(month);
                        returnDate.setFullYear(returnDate.getFullYear()	+ yearsToAdd);
                        
                        returnDate.setTime(returnDate.getTime()+60000*60*24*numDays);
                        
                        return returnDate;
                        
                    }//end function DateAdd()


                    function dateFieldListener(date_box)
                    {
                        var input = event.keyCode;
                        
                        if(input == 46)
                        {
                            date_box.value = date_box.value + '/';
                            
                        }//end if ()
                        
                        if ( (input < 47 || input > 57) && !(input == 43 || input == 45) )
                          event.returnValue = false;
                          
                    }//end function


                    function dateMaker(dateBox)
                    {
                        var dateEntered = dateBox.value;
                        
                        if(dateEntered == "0")
                        {
                            dateBox.value= getFormatDateToString(new Date);
                            
                        }//end if
                        else
                        {
                            if(dateEntered.length > 0)
                            {
                                if(dateEntered.charAt(0) == "+" || dateEntered.charAt(0) == "-")
                                {
                                    var numChange = getDateChangeValue(dateEntered);
                                    var newDate = DateAdd(new Date(),numChange,0,0);
                                    var newDateString = getFormatDateToString(newDate);
                                    dateBox.value = newDateString;
                                    
                                }//end if plus or minus
                                
                            }//end if date length
                            
                        }//end else date nto zero
                        
                    }//end function
   
   
                    function getDateChangeValue(dateEntered)
                    {
                        var num = 0;
                        
                        for(i=0; i<dateEntered.length; i++)
                        {
                            if(dateEntered.charAt(i) == "+")
                            {
                                num++;
                                
                            }//end if
                            else if(dateEntered.charAt(i) == "-")
                            {
                                num--;
                            }//end else
                            
                        }//end for
                        
                        return num;
                        
                    }//end function
                    
 
                    function getFormatDateToString(dateUnformatted)
                    {
                        var theMonth = dateUnformatted.getMonth() + 1;
                        
                        if(theMonth.length < 2) { theMonth = "0" + theMonth; }
                        
                        var theDate = dateUnformatted.getDate();
                        
                        if(theMonth.length < 2) { theDate = "0" + theDate; }   	
                        var theYear = dateUnformatted.getYear();
                        if(theMonth.length < 2) { theYear = "0" + theYear; }   	
                        
                        var todayString = theMonth + "/" + theDate + "/" + theYear;
                        
                        return todayString;
                        
                    }//end function
                    
                    
                    /*
                     * Action functions
                     */
                    function doExitAction()
                    {
                        if (isModified)
                        {
                            var exit=window.confirm("Your changes have not been saved.  Are you sure you want to exit?");
                            if (exit)
                            {
                                performAction("FTDMShutdownServlet?FTDM_Action=exitFTDM");
                            }//end if
                            else 
                            {
                                document.forms[0].restart_date.focus();
                            }//end else
                            
                        }//end if
                        else
                        {
                            performAction("FTDMShutdownServlet?FTDM_Action=exitFTDM");
                        }//end else
                        
                    }//end function doPopupExit()
                    
                    
                    function doSubmitAction()
                    {
                        alert("A message will be sent to activate FTDM shutdown/restart. \n\nIt may take 5-30 minutes to process.  Check back to this screen to verify status.");
                        if (document.forms[0].FTDM_Action[0].checked || document.forms[0].FTDM_Action[1].checked)
                        {
                            if (document.forms[0].restart_date.value)
                            {
                                document.forms[0].saveButton.disabled = true;
                                return true;
                                
                            }//end if
                            else
                            {
                                document.getElementById("error_FTDM_Action").style.display = "none";
                                document.getElementById("error_restart_date_message").style.display="none";
                                document.getElementById("error_restart_date_mandatory").style.display = "block";
                                return false;
                                
                            }//else else
                            
                        }//end else if
                        else
                        {
                            document.getElementById("error_FTDM_Action").style.display = "block";
                            document.getElementById("error_restart_date_mandatory").style.display = "none";
                            return false;
                            
                        }//end else

                    }//end method doSubmitAction
                    
                    
                    function performAction(url)
                    {
                        document.forms[0].action = url;
                        document.forms[0].submit();
                    }//end method performAction(url)
                    
                    
                    /*
                     * Utility functions
                     */
                    function fieldChanged()
                    {
                        isModified=true;
                        
                    }//end method fieldChanged()
                    
                    
                    ]]>
                </script>
            </head>
        
            <body onload="javascript:init();">
                <form name="FTDMShutdown" method="post" action="FTDMShutdownServlet" onSubmit="return doSubmitAction();">
                
                	<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
					<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
					<input type="hidden" name="context" value="{$context}"/>
					<input type="hidden" name="adminAction" value="{$adminAction}"/>
              
                    <!-- Header Template -->
                    <!-- <xsl:call-template name="addHeader"/> -->
            
                    <xsl:call-template name="header">
                        <xsl:with-param name="headerName"  select="'FTDM Shutdown'" />
                        <xsl:with-param name="showExitButton" select="true()"/>
                        <xsl:with-param name="showTime" select="true()"/>
                    </xsl:call-template>
                    
                    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
                        <tr>
                            <td>
                                <br/>
                                <!-- FTDM Shutdown Status -->
                                <table width="100%" border="0" cellpadding="2" cellspacing="0" >
                                    <tr>
                                        <td></td>
                                        <td align="left" width="65" class="label">Status</td>
                                        <td align="left" width="150" ><xsl:value-of select="status_table/status"/></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td align="left" class="label" >Shutdown</td>
                                        <td ><xsl:value-of select="status_table/shutdownDate"/></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td align="left" class="label"  >Restart</td>
                                        <td ><xsl:value-of select="status_table/restartDate"/></td>
                                    </tr>
                                </table>
                                
                                <!-- Actions -->
                                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                                    <tr bordercolor="#3669AC">
                                        <td class="tblheader" width="20%"></td>
                                        <td class="tblheader" align="left" colspan="5">FTDM</td>
                                    </tr>
                                    <tr>
                                        <td width="20%"></td>
                                        <td width="40" valign="top" class="label">
                                            <input type="RADIO" name="FTDM_Action" value="shutdown" onchange="javascript:fieldChanged();">
                                                <xsl:if test="data_entry_fields/parmAction='shutdown'">
                                                    <xsl:attribute name="CHECKED"/>
                                                </xsl:if>
                                            </input>Shutdown
                                        </td>
                                        <td width="40" valign="top" class="label">
                                        </td>
                                        <td/>
                                    </tr>
                                    <tr>
                                        <td width="20%"></td>
                                        <td colspan="5" id="error_FTDM_Action" class="RequiredFieldTxt" style="display:none">
                                            &nbsp; &nbsp; An action is required
                                        </td>
                                    </tr>
                                </table>
                                
                                <br/>
                                <br/>
                                
                                <!-- Restart Date -->
                                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                                    <tr class="tblheader" >
                                        <td class="tblheader" width="20%"></td>
                                        <td class="tblheader" align="left" colspan="3" width="80%">Restart Date</td>
                                    </tr>
                                    <tr>
                                        <td width="20%"></td>
                                        <td align="left" width="100px" valign="top" class="label">Date</td>
                                        <td align="left" width="80%">
                                            <input name="restart_date" type="text" maxlength="10" size="12" value="{data_entry_fields/restartDate}" onblur="dateMaker(this)" onfocus="javascript:fieldFocus();" onkeypress="javascript:dateFieldListener(this);"/>&nbsp;&nbsp;
                                            <img id="calendar" src="../images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">&nbsp;</td>
                                        <td colspan="2" width="80%" id="error_restart_date_message" class="RequiredFieldTxt" style="display:block">
                                            <xsl:if test="validation/restartDate != ''">
                                                <xsl:value-of select="validation/restartDate" />
                                            </xsl:if>
                                        </td>
                                        <td colspan="2" width="80%" id="error_restart_date_mandatory" class="RequiredFieldTxt" style="display:none">
                                            A restart date is required
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"/>
                                        <td align="left" width="100px" valign="top" class="label">&nbsp;</td>
                                        <td width="80%" colspan="2" align="left">
                                            <input name="saveButton" type="submit" value="  Save  "/>
                                        </td>
                                    </tr>
                                </table>
                                
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                
                            </td>
                        </tr>
                    </table>
                    
                    <div align="right" class="button">
                        <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
                            <tr>
                                <td width="20%"/>
                                <td width="60%"/>
                                <td width="20%" align="right">
                                    <input type="button" name="exitButton" value="Exit" onclick="javascript:doPopupExit();"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                        
                        <!-- Footer Template -->
                        <xsl:call-template name="footer"/>

                </form>
            </body>
        </html>
        
        <script type="text/javascript">
          Calendar.setup(
            {
              inputField  : "restart_date",  // ID of the input field
              ifFormat    : "mm/dd/y",  // the date format
              button      : "calendar"  // ID of the button
            }
          );
        </script>
    </xsl:template>
</xsl:stylesheet>