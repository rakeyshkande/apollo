<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="viewQueueTabs">

  <table id="itemTabsTable" width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <div id="tabs">
          <!-- Shopping Cart Tab -->
          <script type="text/javascript" language="javascript">

          <![CDATA[

            document.write('<button id="messageQueue" tabindex="90" content="messageQdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(0);" style="font-size: 9pt;"><span style="text-decoration:underline;">M</span>essage</button>');
 			document.write('<button id="floristQueue" tabindex="90" content="floristQdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(1);" style="font-size: 9pt;">&nbsp;<span style="text-decoration:underline;">F</span>lorist/Product</button>');
  			document.write('<button id="zipQueue" tabindex="90"  content="zipQdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(2);" style="font-size: 9pt;"><span style="text-decoration:underline;">Z</span>ip</button>');
		]]>
		
		var tabsArray = new Array(document.getElementById("messageQueue"));
            tabsArray[1] = document.getElementById("floristQueue");
            tabsArray[2] = document.getElementById("zipQueue");
            
            var _tabCount = 3;
            
  		</script>
  		</div>
	</td>
	</tr>
	</table>
	
	  <script type="text/javascript" language="javascript"><![CDATA[

    var TAB_WIDTH = 92;
    var MAX_TABS = 3;
    var minItem = 0;
    var maxItem = (tabsArray.length > MAX_TABS) ? minItem+MAX_TABS : tabsArray.length;
    var itemTabsTableOffset = document.getElementById("itemTabsTable").getBoundingClientRect().left + document.body.scrollLeft;
    var currentTabIndex = 0;
    var currentTab;
    var currentContent;

    function tabMouseOut(tab){
    	if(tab.className != "selected")
        tab.className = "button";
    }

    function tabMouseOver(tab){
    	if(tab.className != "selected")
        tab.className = "hover";
    }

    function showTab(tab){
    	if(currentTab)
      	currentTab.className = "button";

    	tab.className = "selected";
    	currentTab = tab;
    	showTabContent(tab);
    }

    function showTabContent(tab){
    	if(currentContent)
      	currentContent.className = "hidden";

    	currentContent = document.getElementById(tab.content);
    	currentContent.className = "visible";

    	if(tab.content == "floristQdiv")
    	{
    		if( document.getElementById("codA") != null &&
				document.getElementById("codA").disabled == false ) {
				document.getElementById("codA").focus();
			}
    		
			document.getElementById("messageQueue").tabIndex = 90;
			document.getElementById("floristQueue").tabIndex = -1;
			document.getElementById("zipQueue").tabIndex = 90;
    		
    	}
    	else if(tab.content == "zipQdiv")
    	{
    		document.getElementById("editalllink").focus();
    		document.getElementById("zipQueue").tabIndex = -1;
  		  	document.getElementById("messageQueue").tabIndex = 90;
      		document.getElementById("floristQueue").tabIndex = 90;
    	}
    	else if(tab.content == "messageQdiv");
	    {
		      document.getElementById("messageQueue").tabIndex = -1;
		      document.getElementById("floristQueue").tabIndex = 90;
		      document.getElementById("zipQueue").tabIndex = 90;
	    }

    }

    function initializeTabs(tabToDisplay){
      
    	currentTab = document.getElementById(tabToDisplay);
    	currentTab.className = "selected";
    	 	
    	currentContent = document.getElementById(currentTab.content);
    	
    	currentContent.className = "visible";
    
      	displayItems();
      	
      	

      //checkPrevNextStatus();
    }

    function displayItems() {
 
      for (var i = minItem, k = 0; i < maxItem; i++, k++){
        tabsArray[i].style.left = (k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset;
        
        tabsArray[i].style.visibility = "visible";
      }
     // showHideMoreTab( ((k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset) , "visible");
    }

    function hideItems() {
      for (var i = minItem; i < maxItem; i++){
        tabsArray[i].style.left = 0;
        tabsArray[i].style.visibility = "hidden";
      }
      //showHideMoreTab(0, "hidden");
    }

    function showHideMoreTab(offset, visibility){
      var more = getStyleObject("moreItemsTab");
      more.left = offset;
      more.visibility = visibility;
    }

    function showNextItem(){
      currentTabIndex++;
      showItemAt(currentTabIndex);
    }

    function showPrevItem(){
      currentTabIndex--;
      showItemAt(currentTabIndex);
    }

    function showItemAt(index)
    {
    
	if(currentTabIndex == 1)
	{

		if(!validateFloristQueue())
			return;
	}
    
    var now = floristMaint.endTab.value;
    
	currentTabIndex = index;
	
      if(currentTabIndex == 0)
      {
      

		floristMaint.endTab.value = "messageQueue";
      }
      else if(currentTabIndex == 1)
      {
     
      	floristMaint.endTab.value = "floristQueue";
      }
      else if(currentTabIndex == 2)
      {
     
      	floristMaint.endTab.value = "zipQueue";
      }
	
		if(now == "zipQueue")
	   {
		   if(isZipPopNeeded())
			{
				doUnblockZipPopUp()
			}
	   }
	   
		showTab(tabsArray[currentTabIndex]);
		
    }

    ]]>
  </script>
  
  
</xsl:template>

</xsl:stylesheet>