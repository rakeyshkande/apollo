

<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="tabs.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="profile.xsl"/>
	<xsl:import href="zipCodes.xsl"/>
        <xsl:import href="zipCodesDirectoryCity.xsl"/>
	<xsl:import href="codification.xsl"/>
	<xsl:import href="history.xsl"/>
	<xsl:import href="association.xsl"/>
	<xsl:import href="weights.xsl"/>
	<xsl:import href="orders.xsl"/>
	<xsl:import href="cities.xsl"/>
  <xsl:import href="sourcecodes.xsl"/>

	<!-- security parameters -->
	<xsl:param name="web-context-root"/>
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="adminAction"/>

	<xsl:output method="html" indent="yes"/>
	<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name" />
	<xsl:key name="processFlow" match="/root/processFlow/flow" use="name" />
	<xsl:key name="linkFlow" match="/root/link/data" use="name" />
	<xsl:key name="securityPermissions" match="/root/securityPermissions/permission" use="name" />

	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Florist Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
				<script type="text/javascript" language="javascript" src="../js/util.js"/>
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
			 	<script type="text/javascript" src="../js/calendar.js" />
<script type="text/javascript" language="javascript">
<![CDATA[
var images = new Array("calendar");


function disSave()
{
		document.getElementById("savepro").style.color = 'white';
		document.getElementById("savezip1").style.color = 'white';
		document.getElementById("savezip2").style.color = 'white';
		document.getElementById("savecod").style.color = 'white';
		document.getElementById("saveasn").style.color = 'white';
		document.getElementById("saveasn2").style.color = 'white';
		document.getElementById("savehist").style.color = 'white';
		document.getElementById("saveWeight").style.color = 'white';
		document.getElementById("savecity1").style.color = 'white';
		document.getElementById("savecity2").style.color = 'white';
}

function enSave()
{
		document.getElementById("savepro").style.color = 'black';
		document.getElementById("savezip1").style.color = 'black';
		document.getElementById("savezip2").style.color = 'black';
		document.getElementById("savecod").style.color = 'black';
		document.getElementById("saveasn").style.color = 'black';
		document.getElementById("saveasn2").style.color = 'black';
		document.getElementById("savehist").style.color = 'black';
		document.getElementById("saveWeight").style.color = 'black';
		document.getElementById("savecity1").style.color = 'black';
		document.getElementById("savecity2").style.color = 'black';
}




function stripLeading(string,chr) {

   var finished = false;
   for (var i = 0; i < string.length && !finished; i++)
       if (string.substring(i,i+1) != chr) finished = true;
   if (finished) return string.substring(i-1); else return string;
  
}


function doFullHistory()
{
	var ran = Math.floor(Math.random()*50);
	var url_source = "FloristMaintenanceServlet?action=viewFullHistory&fid=" + floristMaint.memnum.value + "&rand=" +ran + "&count=5000" + "&permit=" + floristMaint.permitHisPop.value;
	var modal_dim = "dialogWidth:750px; dialogHeight:400px; center:yes; status=0; help:no; scroll:yes";
	var ret = window.showModalDialog(url_source, window , modal_dim);
}


function doShowClosureOverride()
{
	var ran = Math.floor(Math.random()*50);
	var url_source = "FloristMaintenanceServlet?action=viewClosureOverride&fid=" + floristMaint.memnum.value + "&rand=" +ran;
	var modal_dim = "dialogWidth:750px; dialogHeight:400px; center:yes; status=0; help:no; scroll:yes";
	var ret = window.showModalDialog(url_source, window , modal_dim);
}


function eChecks()
{
//if(floristMaint.sundaydeliveryflag != null)
//floristMaint.sundaydeliveryflag.disabled = false;
}


function popAssociation(memN)
{
	if( floristMaint.saveNeeded.value == "yes")
	{
		var response = confirm("Are you sure you want to exit without saving this Florist\'s profile?");
		if (!response)
		{
			return;
		}
	}

	floristMaint.saveNeeded.value = "no";
	floristMaint.secondaryAction.value = "";

	document.forms[0].action = "FloristMaintenanceServlet?action=loadAssociatedFlorist&previousFloristNumber=" + floristMaint.mNum.value + "&floristNumber=" + memN;
	floristMaint.endTab.value = "profile";
	showWaitMessage("outerContent", "wait", "Loading");
	eChecks();
 	document.forms[0].submit();
}

function zipChange()
{
floristMaint.zipvalneeded.value = "yes";
document.forms[0].saveNeeded.value = "yes";
enSave();
}

function idChange()
{
floristMaint.idvalneeded.value = "yes";
document.forms[0].saveNeeded.value = "yes";
enSave();
}

function doAddNew()
{

// make all of the addNew changes....

//if(floristMaint.secondaryAction.value == "nosave")
//	floristMaint.isNew.value = "Y";

if(floristMaint.isNew.value == "Y")
{

	floristMaint.history.disabled = "true";
	floristMaint.association.disabled = "true";
	floristMaint.orders.disabled = "true";
	floristMaint.cities.disabled = "true";
	
	floristMaint.zips.style.visibility = "hidden";
	floristMaint.ftdi.style.visibility = "hidden";
	floristMaint.weight.style.visibility = "hidden";
	
	floristMaint.prostatus.style.visibility = "hidden";
	document.getElementById("profileStatus").style.visibility = "hidden";
	
	floristMaint.lastupdated.style.visibility = "hidden";
	
	document.getElementById("profileWeight").style.visibility = "hidden";
	document.getElementById("profileZips").style.visibility = "hidden";
	document.getElementById("profileFtdi").style.visibility = "hidden";
	document.getElementById("profileLastUpdated").style.visibility = "hidden";
	
	
	floristMaint.memnum.readOnly = false;
	document.getElementById("memnum").tabIndex = 1;
	
	floristMaint.contact.readOnly = false;
	document.getElementById("contact").tabIndex = 11;
	
	floristMaint.territory.readOnly = false;
	document.getElementById("territory").tabIndex = 12;
	
	floristMaint.minorderamount.readOnly = false;
	
//	floristMaint.codsundayblocked.style.visibility = "hidden";
//	floristMaint.codsundayunblockdate.style.visibility = "hidden";
	
//	document.getElementById("calendarSunday").style.visibility = "hidden";
	
	//floristMaint.ms_override.value = "No";
	document.getElementById("returntoResults").style.visibility = "hidden";
	
	if(floristMaint.vendorflag.value == "Y")
	{
		floristMaint.zipcodes.disabled = "true";
		floristMaint.codification.disabled = "true";
		floristMaint.weights.disabled = "true";
	}

}

}

function orderSearchAction()
{
	
	if(!validateOrders())
		return;

	floristMaint.mNum.value = floristMaint.memnum.value;

	document.forms[0].action = "OrderSearchServlet?action=searchOrders";
	showWaitMessage("outerContent", "wait", "Searching");
	eChecks();
 	document.forms[0].submit();

}

function init1()
{

if(floristMaint.saveNeeded.value == "yes")
	enSave();
else
	disSave();

if(floristMaint.loadOrdersSearch.value == "Yes")
{
document.getElementById("orderResults").style.display = "block";
document.getElementById("orderSearch").style.display = "none";
}
else
{
document.getElementById("orderResults").style.display = "none";
document.getElementById("orderSearch").style.display = "block";
}

//if(floristMaint.adjusted_weight.selectedIndex == 0)
//floristMaint.adjusted_weight.selectedIndex = floristMaint.weightadj.value;

// apply permissions....

if(floristMaint.permitProfile.value == "N")
{
        var profileStartPos=0;
        var profileEndPos=0;
        
        for(var i=0; document.forms[0].elements.length; i++)
        {
            if(document.forms[0].elements[i].name == "prostatus")
            {
                profileStartPos = i;
            }
            if(document.forms[0].elements[i].name == "savezip1")
            {
                profileEndPos = i;
                break;
            }            
        }
        
        for (var i = profileStartPos; i < profileEndPos; i++)
	{
		document.forms[0].elements[i].readOnly = true;
		document.forms[0].elements[i].tabIndex = -1;
	}

	document.getElementById("state").disabled = true; // disable state dropdown.
}


if(requestType != "zipCodesDirectoryCity" && floristMaint.permitZips.value == "N")
{
    var zipCodesStartPos = 0;
    var zipCodesEndPos = 0;
    var cityStartPos = 0;
    var cityEndPos = 0;

        for(var i = 0; i <= document.forms[0].elements.length; i++)
        {
            if(document.forms[0].elements[i].name == "savezip1")
            {
                zipCodesStartPos = i;
            }

            if(document.forms[0].elements[i].name == "savezip2")
            {
                zipCodesEndPos = i;
                break;
            }            
        }

        for(var i = 0; i <= document.forms[0].elements.length; i++)
        {
            if(document.forms[0].elements[i].name == "savecity1")
            {
                cityStartPos = i;
            }

            if(document.forms[0].elements[i].name == "savecity2")
            {
                cityEndPos = i;
                break;
            }            
        }
        
        for (var i = zipCodesStartPos; i <= zipCodesEndPos; i++)
        {
		document.forms[0].elements[i].disabled = true;
		document.forms[0].elements[i].tabIndex = -1;
        }
        for (var i = cityStartPos; i <= cityEndPos; i++)
        {
		document.forms[0].elements[i].disabled = true;
		document.forms[0].elements[i].tabIndex = -1;
        }
}

if(floristMaint.permitCodAdd.value == "N")
{
	floristMaint.CodAdd.disabled = true;
	floristMaint.codlist.disabled = true;
}

if(floristMaint.permitCodSunday.value == "N")
{
	//floristMaint.sundaydeliveryflag.disabled = true;
	//floristMaint.codsundayblocked.disabled = true;
	//floristMaint.codsundayunblockdate.readOnly = true;
	//document.getElementById("calendarSunday").disabled = true;
}

if(floristMaint.permitCodMerc.value == "N")
{
	document.getElementById("mercYes").disabled = true;
	document.getElementById("mercNo").disabled = true;
}

if(floristMaint.permitCodMin.value == "N")
{
	floristMaint.minorderamount.readOnly = true;
}

if(floristMaint.permitWeights.value == "N")
{
floristMaint.weights.disabled = true;
}


if(floristMaint.permitSourceCodes.value == "N")
{
floristMaint.sourcecodes.disabled = true;
}


if(floristMaint.validProZip.value == "No" || floristMaint.validProNum.value == "No")
{

if(floristMaint.validProZip.value == "No")
{
	document.getElementById("errorServerZip").style.display = "block";
	floristMaint.zipvalneeded.value = "yes";
}

if(floristMaint.validProNum.value == "No")
{

	document.getElementById("errorServerId").style.display = "block";
	floristMaint.idvalneeded.value = "yes"
}
	
	
	if(floristMaint.secondaryAction.value == "addsave")
	{
	floristMaint.isNew.value = "Y";
	}
	
	doAddNew();

	for (var i = 0; i < document.forms[0].elements.length; i++)
	{
		if(document.forms[0].elements[i].readOnly == true)
			document.forms[0].elements[i].style.backgroundColor = 'silver';
	}


	floristMaint.endTab.value = "profile";
	document.getElementById("profile").tabIndex = -1;
	var tab = floristMaint.endTab.value + "";
	initializeTabs(tab);
	addImageCursorListener(images);

	return;
}

	// its not addNew....
	if(floristMaint.isVendor.value == "Y")
	{
		floristMaint.zipcodes.disabled = "true";
		floristMaint.codification.disabled = "true";
		floristMaint.weights.disabled = "true";
		floristMaint.association.disabled = "true";
		floristMaint.orders.disabled = "true";
		floristMaint.cities.disabled = "true";
	}

	for (var i = 0; i < document.forms[0].elements.length; i++)
	{
		if(document.forms[0].elements[i].readOnly == true)
			document.forms[0].elements[i].style.backgroundColor = 'silver';
	}

																	/*
																	var form = document.forms[0];


																	//if(floristMaint.isNew.value == "Y")
																	//{

																		if(floristMaint.secondaryAction.value ==)"save"
																		{
																			floristMaint.secondaryAction.value == "nosave"
																			floristMaint.mNum.value = floristMaint.memnum.value;

																			form.action = "FloristMaintenanceServlet?action=insertFlorist";
																			showWaitMessage("outerContent", "wait", "Processing");
																			form.submit();

																			//document.forms[0].floristNumber.value = inValue;

																			// once returned from the save...

																			return;
																		}
																	//}
																	*/
	// end of new added..
	
	if(floristMaint.isAdded.value == "Y")
	{
	
	document.forms[0].action = "FloristMaintenanceServlet?floristNumber=" + floristMaint.memnum.value + "&disp=y";
	showWaitMessage("outerContent", "wait", "Processing");
	eChecks();
	document.forms[0].submit();
	
	return;
	}

	if(floristMaint.endTab.value == "")
	floristMaint.endTab.value = "profile";
	document.getElementById("profile").tabIndex = -1;
	
	var tab = floristMaint.endTab.value + "";
	
	initializeTabs(tab);
	addImageCursorListener(images);
	var eName;

	for (var i = 0; i < document.forms[0].elements.length; i++)
	{
	
	eName = document.forms[0].elements[i].name;
	
	if(document.forms[0].elements[i].onchange == "")
		continue;

	if(eName == "hisdis")
		continue;

	if(eName == "zipcode")
		continue;
		
	if(eName == "memnum")
		continue;
	
	if(eName == "orderStartDate")
		continue;
		    
	if(eName == "orderEndDate")
		continue;
		
	if(eName == "orderMercury")
		continue;
		
	if(eName == "orderRecip")	
		continue;

	if(eName == "orderMercValue")
		continue;

	if(eName == "orderSku")
		continue;

	if(eName == "order_type")
		continue;
		
	if(eName == "reportTypeRadio")
		continue;
	
        document.forms[0].elements[i].onchange = function(){document.forms[0].saveNeeded.value = "yes"; enSave();}
	
	}
	
	if(document.getElementById("savezipNeeded").value == "yes")
	document.forms[0].saveNeeded.value = "yes";

	if(floristMaint.prostatus.value == "Inactive")
	{
		document.getElementById("ronly").style.display = "block";
		floristMaint.savepro.disabled = "true";
		floristMaint.savezip1.disabled = "true";
		floristMaint.savezip2.disabled = "true";
		floristMaint.savecod.disabled = "true";
		floristMaint.saveasn.disabled = "true";
		floristMaint.saveasn2.disabled = "true";
		floristMaint.histadd.disabled = "true";
		floristMaint.histadd2.disabled = "true";
		floristMaint.savehist.disabled = "true";
		floristMaint.saveWeight.disabled = "true";
		floristMaint.savecity1.disabled = "true";
		floristMaint.savecity2.disabled = "true";
	}
	else if(floristMaint.lockFlag.value == "N")
	{
		document.getElementById("lockedrecord").style.display = "block";
		floristMaint.savepro.disabled = "true";
		floristMaint.savezip1.disabled = "true";
		floristMaint.savezip2.disabled = "true";
		floristMaint.savecod.disabled = "true";
		floristMaint.saveasn.disabled = "true";
		floristMaint.saveasn2.disabled = "true";
		floristMaint.histadd.disabled = "true";
		floristMaint.histadd2.disabled = "true";
		floristMaint.savehist.disabled = "true";
		floristMaint.saveWeight.disabled = "true";
		floristMaint.savecity1.disabled = "true";
		floristMaint.savecity2.disabled = "true";
	}
	else
	{
		document.getElementById("ronly").style.display = "none";
		document.getElementById("lockedrecord").style.display = "none";
	}
	
	if(floristMaint.secondaryAction.value == "save" || floristMaint.secondaryAction.value == "addsave")
	{
		//document.forms[0].saveNeeded.value = "no";
		//floristMaint.secondaryAction.value = "";
		
	    if (!validateZipBlocks())
	        return;

		doAddCommentPopUp('Y');
	}

        if(document.getElementById("listExisting").value.length > 0)
        {
            document.getElementById("errorExistingZips").style.display = "block";
            document.getElementById("errorholdExistingZips").style.display = "none";
        }
        else
        {
            document.getElementById("errorExistingZips").style.display = "none";
            document.getElementById("errorholdExistingZips").style.display = "block";
        }
	
        if(document.getElementById("listInvalid").value.length > 0)
        {
            document.getElementById("errorInvalidZips").style.display = "block";
            document.getElementById("errorholdInvalidZips").style.display = "none";
        }
        else
        {
            document.getElementById("errorInvalidZips").style.display = "none";
            document.getElementById("errorholdInvalidZips").style.display = "block";
        }
        
        if(document.getElementById("listAssociated").value.length > 0)
        {
            document.getElementById("errorAscZips").style.display = "block";
            document.getElementById("errorholdAscZips").style.display = "none";
        }
        else
        {
            document.getElementById("errorAscZips").style.display = "none";
            document.getElementById("errorholdAscZips").style.display = "block";
        }        

        if(requestType != "zipCodesDirectoryCity")
        {
            if(document.getElementById("listInvalidPop").value.length > 0)
            {
                    document.getElementById("errorInvalidPopZips").style.display = "block";
                    document.getElementById("errorholdInvalidZips").style.display = "none";
            }
            else
            {
                    document.getElementById("errorInvalidPopZips").style.display = "none";
            }
	
            if(document.getElementById("listNonSavedPop").value.length > 0)
            {
                    document.getElementById("errorInvalidSavedZips").style.display = "block";
                    document.getElementById("errorholdSavedZips").style.display = "none";
            }
            else
            {
                    document.getElementById("errorInvalidSavedZips").style.display = "none";
            }
	
            floristMaint.newzipcodes.value = floristMaint.listInvalid.value;
	
            if(floristMaint.newzipcodes.value.length > 0)
            {
                    if(floristMaint.listExisting.value.length > 0)
                    {
                    floristMaint.newzipcodes.value += ", ";
                    }
            }
	
            floristMaint.newzipcodes.value += floristMaint.listExisting.value;
	
            if(floristMaint.newzipcodes.value.length > 0)
            {
                    if(floristMaint.listAssociated.value.length > 0)
                    {
                        floristMaint.newzipcodes.value += ", ";
                    }
            }
	
            floristMaint.newzipcodes.value += floristMaint.listAssociated.value;

            // check the delete flag and the save flag...

        }
        else
        {
            floristMaint.codification.style.visibility = "hidden";
            floristMaint.history.style.visibility = "hidden";
            floristMaint.association.style.visibility = "hidden";
            floristMaint.weights.style.visibility = "hidden";
            floristMaint.orders.style.visibility = "hidden";
            floristMaint.cities.style.visibility = "hidden";
            
            if(document.getElementById("listInvalidCity").value != null)
            {
                if(document.getElementById("listInvalidCity").value.length > 0)
                {
                    document.getElementById("errorInvalidCity").style.display = "block";
                    document.getElementById("errorholdInvalidZips").style.display = "none";
                }
                else
                {
                    document.getElementById("errorInvalidCity").style.display = "none";
                }            
            }
        }
}

        

function doShowFloristZipPopUp(zipcode)
{
	var deliverydate = document.getElementById("popupzipdeliverydate").value;
	if (deliverydate == '') {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 
		if(mm<10) {
			mm='0'+mm
		} 
		deliverydate = mm+'/'+dd+'/'+yyyy;
		document.getElementById("popupzipdeliverydate").value = deliverydate;
	}
	var url_source = "LoadFloristListServlet?action=loadFlorists&thezipcode=" + zipcode + "&deliverydate=" + deliverydate;
	var modal_dim = "dialogWidth:750px; dialogHeight:400px; center:yes; status=0; help:no; scroll:yes";
	var ret = window.showModalDialog(url_source, window , modal_dim);
	if (ret && ret != null && ret[0] != "") {
	    document.getElementById("popupzipdeliverydate").value = ret[0];
	    doShowFloristZipPopUp(zipcode);
	}
	// reset to today's date
	var resetToday = new Date();
	var resetdd = resetToday.getDate();
	var resetmm = resetToday.getMonth()+1; //January is 0!
	var resetyyyy = resetToday.getFullYear();
	if(resetdd<10) {
		resetdd='0'+resetdd
	} 
	if(resetmm<10) {
		resetmm='0'+resetmm
	} 
	deliverydate = resetmm+'/'+resetdd+'/'+resetyyyy;
	document.getElementById("popupzipdeliverydate").value = deliverydate;
}

function doShowFloristCityPopUp(floristId, city)
{
	var deliverydate = document.getElementById("popupcitydeliverydate").value;
	if (deliverydate == '') {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 
		if(mm<10) {
			mm='0'+mm
		} 
		deliverydate = mm+'/'+dd+'/'+yyyy;
		document.getElementById("popupcitydeliverydate").value = deliverydate;
	}
	var url_source = "LoadFloristCitiesServlet?action=loadFlorists&floristId=" + floristId +
	    "&deliverydate=" + deliverydate + "&city=" + city;
	var modal_dim = "dialogWidth:750px; dialogHeight:400px; center:yes; status=0; help:no; scroll:yes";
	var ret = window.showModalDialog(url_source, window , modal_dim);
	if (ret && ret != null && ret[0] != "") {
	    document.getElementById("popupcitydeliverydate").value = ret[0];
	    doShowFloristCityPopUp(floristId, ret[1]);
	}
	// reset to today's date
	var resetToday = new Date();
	var resetdd = resetToday.getDate();
	var resetmm = resetToday.getMonth()+1; //January is 0!
	var resetyyyy = resetToday.getFullYear();
	if(resetdd<10) {
		resetdd='0'+resetdd
	} 
	if(resetmm<10) {
		resetmm='0'+resetmm
	} 
	deliverydate = resetmm+'/'+resetdd+'/'+resetyyyy;
	document.getElementById("popupcitydeliverydate").value = deliverydate;
}

function doZipList(){

  var ran = Math.floor(Math.random()*50);
  var url_source = "ZipCodeServlet?action=listzips&mNum="+floristMaint.mNum.value+"&rand="+ran;
  var modal_dim = "dialogWidth:750px; dialogHeight:400px; center:yes; status=0; help:no; scroll:yes";
  var ret = window.showModalDialog(url_source, window , modal_dim);
}

function doReturnAction()
{
/*
	if(currentTabIndex == 0 )
		if(!validateProfile())
			return;
	else if(currentTabIndex == 1)
	{
            // Ripped out code that called validateZipCode()
 	}
	else if(currentTabIndex == 3)
		if(!validateCodification())
			return;
*/

	if( floristMaint.saveNeeded.value == "yes")
	{
		var response = confirm("Are you sure you want to exit without saving this Florist\'s profile?");
		if (!response)
		{
			return;
		}
	}
	else
	{
		if(floristMaint.endTab.value == "orders")
		{
			if(floristMaint.loadOrdersSearch.value == "Yes")
			{
				document.getElementById("orderResults").style.display = "none";
				document.getElementById("orderSearch").style.display = "block";
				floristMaint.loadOrdersSearch.value = "No";
				document.getElementById("orderStartDate").focus();
				return;
			}
		}

	}
	
	
		document.forms[0].action = "FloristSearchServlet?action=loadFloristSearch";
		showWaitMessage("outerContent", "wait", "Processing");
		eChecks();
	 	document.forms[0].submit();
}

function doReturnSearchAction()
{

	if( floristMaint.saveNeeded.value == "yes")
		{
			var response = confirm("Are you sure you want to exit without saving this Florist\'s profile?");
			if (!response)
			{
				return;
			}
		}

	if(floristMaint.count.value.length < 1)
	{
		document.forms[0].action = "FloristSearchServlet?action=searchFlorist";
		showWaitMessage("outerContent", "wait", "Processing");
		eChecks();
		document.forms[0].submit();
	}
	else
	{
		document.forms[0].action = "FloristSearchServlet?action=last&userid=" + floristMaint.uid.value + "&count=" + floristMaint.count.value;
		showWaitMessage("outerContent", "wait", "Processing");
		eChecks();
		document.forms[0].submit();
	}
}

function doExitAction()
{
	doReturnAction();
}

function trim(str)
{
       str=str.replace(/^\s*(.*)/, "$1");
       str=str.replace(/(.*?)\s*$/, "$1");
     
       return str;
}

//dislay phone and fax fields in a standard format.
function convertPhones()
{

	var vphonenumer;
	var normalizedPhone;
	var s;
	var resultString = "";
	phoneNumberDelimiters += ".";


	vphonenumber = floristMaint.phone.value;
	if(trim(vphonenumber).length > 0)
	{
		normalizedPhone = stripCharsInBag(vphonenumber, phoneNumberDelimiters);
		s = normalizedPhone + "";
		resultString = s.substring(0,3) + "-" + s.substring(3,6) + "-" + s.substring(6,10);
		floristMaint.phone.value = resultString;
	}

/*

	vphonenumber = floristMaint.altphone.value;
	if(trim(vphonenumber).length > 0)
	{
		normalizedPhone = stripCharsInBag(vphonenumber, phoneNumberDelimiters);
		s = normalizedPhone + "";
		resultString = s.substring(0,3) + "-" + s.substring(3,6) + "-" + s.substring(6,10);
		floristMaint.altphone.value = resultString;
	}

	vphonenumber = floristMaint.fax.value;
	if(trim(vphonenumber).length > 0)
	{
		vphonenumber = floristMaint.fax.value;
		normalizedPhone = stripCharsInBag(vphonenumber, phoneNumberDelimiters);
		s = normalizedPhone + "";
		resultString = s.substring(0,3) + "-" + s.substring(3,6) + "-" + s.substring(6,10);
		floristMaint.fax.value = resultString;
	}
*/

}

function addAction()
{
		// validate the zipcodes...
                if(requestType != "zipCodesDirectoryCity")
                {
                    if((!validateZipCode()) || (floristMaint.newzipcodes.value.length < 1))
                    {    
                        floristMaint.newzipcodes.focus();
                        return;
                    }
                    else
                    {
                        floristMaint.mNum.value = floristMaint.memnum.value;
                        document.forms[0].action = "ZipCodeServlet?action=addZipCode";
                        eChecks();
                        showWaitMessage("outerContent", "wait", "Processing");
                        document.forms[0].submit();
                    }
                }
                else
                {
                    if(document.getElementById("newcity").value.length < 1)
                    {
                        alert("Please enter a city.");
                        document.getElementById("newcity").focus();
                    
                    }
                    else 
                    {
                        if(document.getElementById("newstate").value.length < 1)
                        {
                            alert("Please select a state/pro.");
                            document.getElementById("newstate").focus();
                        }
                        else
                        {
                            floristMaint.mNum.value = floristMaint.memnum.value;
                            document.forms[0].action = "ZipCodeServlet?action=addZipCode";
                            eChecks();
                            showWaitMessage("outerContent", "wait", "Processing");
                            document.forms[0].submit();
                        }
                    }
                }
}


function doCodAddAction()
{
		floristMaint.mNum.value = floristMaint.memnum.value;

		document.forms[0].action = "CodificationServlet?action=addCodifiedProduct";
		eChecks();
		
		showWaitMessage("outerContent", "wait", "Processing");
	 	document.forms[0].submit();
}

function loadHistory(c) // to filter the table that shows the history of the florist.
{

var sel = floristMaint.hisdis.selectedIndex;
var i=0;

if(sel == 0) // show all....
{
for(i = 1; i <= c; i++)
{
	if(document.getElementById("disAdjustments"+i) != null)
		document.getElementById("disAdjustments"+i).style.display = "block";
	if(document.getElementById("disBlocks"+i) != null)
		document.getElementById("disBlocks"+i).style.display = "block";
	if(document.getElementById("disCodification"+i) != null)
		document.getElementById("disCodification"+i).style.display = "block";
	if(document.getElementById("disCompliments"+i) != null)
		document.getElementById("disCompliments"+i).style.display = "block";
	if(document.getElementById("disProblems"+i) != null)
		document.getElementById("disProblems"+i).style.display = "block";
	if(document.getElementById("disProfile"+i) != null)
		document.getElementById("disProfile"+i).style.display = "block";
	if(document.getElementById("disWeight"+i) != null)
		document.getElementById("disWeight"+i).style.display = "block";
	if(document.getElementById("disZip Codes"+i) != null)
		document.getElementById("disZip Codes"+i).style.display = "block";
	if(document.getElementById("disCities"+i) != null)
		document.getElementById("disCities"+i).style.display = "block";
}
return;
}

// hide all....
for(i = 1; i <= c; i++)
{
	if(document.getElementById("disAdjustments"+i) != null)
		document.getElementById("disAdjustments"+i).style.display = "none";
	if(document.getElementById("disBlocks"+i) != null)
		document.getElementById("disBlocks"+i).style.display = "none";
	if(document.getElementById("disCodification"+i) != null)
		document.getElementById("disCodification"+i).style.display = "none";
	if(document.getElementById("disCompliments"+i) != null)
		document.getElementById("disCompliments"+i).style.display = "none";
	if(document.getElementById("disProblems"+i) != null)
		document.getElementById("disProblems"+i).style.display = "none";
	if(document.getElementById("disProfile"+i) != null)
		document.getElementById("disProfile"+i).style.display = "none";
	if(document.getElementById("disWeight"+i) != null)
		document.getElementById("disWeight"+i).style.display = "none";
	if(document.getElementById("disZip Codes"+i) != null)
		document.getElementById("disZip Codes"+i).style.display = "none";
	if(document.getElementById("disCities"+i) != null)
		document.getElementById("disCities"+i).style.display = "none";
}

if(sel == 1) // show adjustments
{
for(i = 1; i <= c; i++)
{
	if(document.getElementById("disAdjustments"+i) != null)
		document.getElementById("disAdjustments"+i).style.display = "block";
}
return;
}

if(sel == 2) // show blocks
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disBlocks"+i) != null)
			document.getElementById("disBlocks"+i).style.display = "block";
	}
	return;
}

if(sel == 3) // show Cities
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disCities"+i) != null)
			document.getElementById("disCities"+i).style.display = "block";
	}
	return;
}

if(sel == 4) // show Codification
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disCodification"+i) != null)
			document.getElementById("disCodification"+i).style.display = "block";
	}
	return;
}

if(sel == 5) // show Compliments
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disCompliments"+i) != null)
			document.getElementById("disCompliments"+i).style.display = "block";
	}
	return;
}


if(sel == 6) // show Problems
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disProblems"+i) != null)
			document.getElementById("disProblems"+i).style.display = "block";
	}
	return;
}

if(sel == 7) // show Profile
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disProfile"+i) != null)
			document.getElementById("disProfile"+i).style.display = "block";
	}
	return;
}

if(sel == 8) // show Weight
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disWeight"+i) != null)
			document.getElementById("disWeight"+i).style.display = "block";
	}
	return;
}

if(sel == 9) // show Zip Codes
{
	for(i = 1; i <= c; i++)
	{
		if(document.getElementById("disZip Codes"+i) != null)
			document.getElementById("disZip Codes"+i).style.display = "block";
	}
	return;
}

}

function saveAction()
{

		
// validate all tabs....

	if(currentTabIndex == 0 )
	{
	resetProfileErrors();
	
		if(!validateProfile())
			return;

	}
	else if(currentTabIndex == 1)
	{
	    if (!validateZipBlocks())
	        return;
	}
	else if(currentTabIndex == 3)
	{
		if(!validateCodification())
			return;
	}
	else if(currentTabIndex == 6)
	{
		if(!validateWeights())
				return;
	}

	if(floristMaint.saveNeeded.value != "yes")
		return;


	if(floristMaint.isNew.value == "Y")
	{
		floristMaint.secondaryAction.value = "addsave";
	}
	else
	{
		floristMaint.secondaryAction.value = "save"
	}

//floristMaint.isNew.value = "N";
	
if((currentTabIndex == 0) && (floristMaint.zipvalneeded.value == "yes"))
{

validateProServer();
}
else if((currentTabIndex == 0) && (floristMaint.idvalneeded.value == "yes"))
{
validateProServer();
}
else
{
	//floristMaint.isNew.value = "N";
	
	if(isZipPopNeeded()) {
		doUnblockZipPopUp();
	} else {
	    if (isCityPopNeeded()) {
	        doUnblockCityPopUp();
	    } else {
		    init1();
		}
	}
}

// if it is add new... create the florist...
/*
var form = document.forms[0];
if(floristMaint.isNew.value == "Y")
{
	floristMaint.mNum.value = floristMaint.memnum.value;
	eChecks();
	form.action = "FloristMaintenanceServlet?action=insertFlorist";
	showWaitMessage("outerContent", "wait", "Processing");
	form.submit();
	
	//document.forms[0].floristNumber.value = inValue;
	
	// once returned from the save...
	
	return;
}
*/




//doAddCommentPopUp('Y');

/*
		document.forms[0].action = "FloristMaintenanceServlet?action=updateFlorist";
	 	document.forms[0].submit();
*/
}

function doUnblockZipPopUp()
{

var form = document.forms[0];
var outZips = new Array(150);

var i;
var zipCount = 0;

var blockEndDate = "zipunblockdate";
var temp = "0";

for(i = 1; i <= form.totalZipcodes.value; i++)
{
	temp = "zipYes" + i;
	
        if(document.getElementById(temp) != null)
        {
                if(zipCount >= 150)
                        break;

                if(document.getElementById(temp).checked == true)
                {
                        if(document.getElementById('zipunblockdate' + i).value == "")
                        {
                                outZips[zipCount] = document.getElementById('zipcode' + i).value;
                                zipCount++;
                        }
                }
        }
}

var zipOutString = "";

for(var j=1; j<=zipCount; j++)
{
zipOutString = zipOutString + "&z" + j + "=" + outZips[j-1];
}

var url_source = "ZipCodeServlet?action=blockZipCode" + zipOutString;
var modal_dim = "dialogWidth:500px; dialogHeight:450px; center:yes; status=0; help:no; scroll:no";
var ret = window.showModalDialog(url_source, window, modal_dim);

if (!ret || ret == null) {
	currentTabIndex = 1;
	floristMaint.endTab.value = "zipcodes";
	return;
}
if(ret && ret != null && ret[0] == "closed")
{
	currentTabIndex = 1;
	floristMaint.endTab.value = "zipcodes";
	return;
}

//document.forms[0].saveNeeded.value = "yes";

	if (ret && ret != null)
	{
        form.newzipunblockdate.value = ret[0];
 	}

       var form = document.forms[0];
       
       form.action = "FloristMaintenanceServlet?action=updateFloristNoSave";
       eChecks();
       showWaitMessage("outerContent", "wait", "Processing");
       form.submit();

		return;

}


function doAddCommentPopUp(saveFlag)
{

var form = document.forms[0];

var url_source = "CommentsServlet?action=loadComment&mNum="+floristMaint.mNum.value;
var modal_dim = "dialogWidth:700px; dialogHeight:400px; center:yes; status=0; help:no; scroll:no";
var ret = window.showModalDialog(url_source, floristMaint.permitMangComment.value, modal_dim);

	if (ret && ret != null)
	{
        form.flnum.value = ret[0];
        form.dispositionDescription.value = ret[1];
        form.newcomment.value = ret[2];
        form.newmanagerflag.value = ret[3];
        
        if(saveFlag == 'Y')
        {
        	floristMaint.isNew.value = "N";
        	document.forms[0].saveNeeded.value = "no";
	        if(floristMaint.secondaryAction.value == "addsave")
	        	form.action = "FloristMaintenanceServlet?action=insertFlorist";
	        else
	        	form.action = "FloristMaintenanceServlet?action=updateFlorist";
	        	
	        floristMaint.secondaryAction.value = "";
        }
        else
        {
	      //  document.forms[0].saveNeeded.value = "yes";
	      
	        form.action = "CommentsServlet?action=addComment";
        }

        var cityStartPos = 0;
        var cityEndPos = 0;

        for(var i = 0; i <= document.forms[0].elements.length; i++) {
            if(document.forms[0].elements[i].name == "savecity1") {
                cityStartPos = i;
            }

            if(document.forms[0].elements[i].name == "savecity2") {
                cityEndPos = i;
                break;
            }            
        }
        for (var i = cityStartPos; i <= cityEndPos; i++) {
            document.forms[0].elements[i].disabled = false;
        }

		for( var i=1; i <= floristMaint.totalcod.value; i++) {
			if(document.getElementById('codblock' + i) != null) {
			    document.getElementById('codblock' + i).disabled = false;
			    document.getElementById('codpermblock' + i).disabled = false;
			    document.getElementById('codunblockdate' + i).disabled = false;
			}
		}

        eChecks();
		showWaitMessage("outerContent", "wait", "Processing");
        form.submit();

      }

}

function isZipPopNeeded()
{
if(requestType != "zipCodesDirectoryCity")
{
        var form = document.forms[0];

        var i;

        var temp = "0";

        for(i = 1; i <= form.totalZipcodes.value; i++)
        {
                temp = "zipYes" + i;

                if(document.getElementById(temp) != null)
                {
                        if(document.getElementById(temp).checked == true)
                        {
                                if(document.getElementById('zipunblockdate' + i).value == "")
                                {
                                        return true;
                                }
                        }
                }
        }

        return false;
}
return false;
}


function doBlockZipPopUp()
{

var form = document.forms[0];

var url_source = "ZipCodeServlet?action=blockZipCode";
var modal_dim = "dialogWidth:700px; dialogHeight:400px; center:yes; status=0; help:no; scroll:no";
var ret = window.showModalDialog(url_source, window, modal_dim);

}

function isCityPopNeeded() {
 
    for(var i = 1; i <= allowedCities.length; i++) {
        var temp = "cityYes" + i;
        if(document.getElementById(temp) != null) {
            if(document.getElementById(temp).checked == true) {
                if(document.getElementById('cityunblockdate' + i).value == "") {
                    if(document.getElementById('citypermanentblockflag' + i).value != "Y") {
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

function doUnblockCityPopUp() {

    var cityOutString = "";
    var j=1;

    for(var i = 1; i <= allowedCities.length; i++) {
        var temp = "cityYes" + i;
        if(document.getElementById(temp) != null) {
            if(document.getElementById(temp).checked == true) {
                if (document.getElementById('blockAllCities').value == "yes" ||
                    (document.getElementById('cityunblockdate' + i).value == "" &&
                        document.getElementById('citypermanentblockflag' + i).value != "Y")) {
                        cityOutString = cityOutString + "&z" + j + "=" + allowedCities[i-1];
                        j++;
                }
            }
        }
    }

    var url_source = "ZipCodeServlet?action=blockCity" + cityOutString + "&permitFulfillment=" +
        document.getElementById('permitFulfillment').value;
    var modal_dim = "dialogWidth:500px; dialogHeight:450px; center:yes; status=0; help:no; scroll:no";
    var ret = window.showModalDialog(url_source, window, modal_dim);

    if (!ret || ret == null) {
	    currentTabIndex = 2;
	    floristMaint.endTab.value = "cities";
	    return;
    }
    if(ret && ret != null && ret[0] == "closed") {
	    currentTabIndex = 2;
	    floristMaint.endTab.value = "cities";
	    return;
    }

	if (ret && ret != null) {
        floristMaint.newcityunblockdate.value = ret[0];
 	}

    var cityStartPos = 0;
    var cityEndPos = 0;

    for(var i = 0; i <= document.forms[0].elements.length; i++) {
        if(document.forms[0].elements[i].name == "savecity1") {
            cityStartPos = i;
        }

        if(document.forms[0].elements[i].name == "savecity2") {
            cityEndPos = i;
            break;
        }            
    }
    for (var i = cityStartPos; i <= cityEndPos; i++) {
        document.forms[0].elements[i].disabled = false;
    }

    var form = document.forms[0];
       
    form.action = "FloristMaintenanceServlet?action=updateFloristNoSave";
    showWaitMessage("outerContent", "wait", "Processing");
    form.submit();
	
	return;

}

]]>
</script>
			</head>
			<body>
				<form name="floristMaint" method="post">

					<input type="hidden" name="servlet_action"/>
					
					<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
					<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
					<input type="hidden" name="context" value="{$context}"/>
					<input type="hidden" name="adminAction" value="{$adminAction}"/>
					
					<input type="hidden" name="sc_floristnum" value="{key('searchCriteria', 'sc_floristnum')/value}"/>
					<input type="hidden" name="mNum" value="{floristData/generalData/memberNumber}"/>
					<input type="hidden" name="sc_zipcode" value="{key('searchCriteria', 'sc_zipcode')/value}"/>
					<input type="hidden" name="sc_city" value="{key('searchCriteria', 'sc_city')/value}"/>
					<input type="hidden" name="sc_state" value="{key('searchCriteria', 'sc_state')/value}"/>
					<input type="hidden" name="sc_shopname" value="{key('searchCriteria', 'sc_shopname')/value}"/>
					<input type="hidden" name="sc_phonenumber" value="{key('searchCriteria', 'sc_phonenumber')/value}"/>
					<input type="hidden" name="sc_statuslist" value="{key('searchCriteria', 'sc_statuslist')/value}"/>
					<input type="hidden" name="sc_product" value="{key('searchCriteria', 'sc_product')/value}"/>
					<input type="hidden" name="sc_productType" value="{key('searchCriteria', 'sc_productType')/value}"/>
					<input type="hidden" name="uid" value="{key('searchCriteria', 'uid')/value}"/>
					<input type="hidden" name="count" value="{key('searchCriteria', 'count')/value}"/>
					
					<input type="hidden" name="permitProfile" value="{key('securityPermissions', 'profileUpdate')/value}"/>
					<input type="hidden" name="permitZips" value="{key('securityPermissions', 'zipCodesUpdate')/value}"/>					
					<input type="hidden" name="permitFulfillment" value="{key('securityPermissions', 'fulfillmentUpdate')/value}"/>					
					<input type="hidden" name="permitCodAdd" value="{key('securityPermissions', 'addCodification')/value}"/>					
					
					<input type="hidden" name="permitCodSunday" value="{key('securityPermissions', 'sundayDeliveryUpdate')/value}"/>					
					<input type="hidden" name="permitCodMerc" value="{key('securityPermissions', 'mercuryUpdate')/value}"/>					
					<input type="hidden" name="permitCodMin" value="{key('securityPermissions', 'minimumOrderUpdate')/value}"/>					
										
					<input type="hidden" name="permitMangComment" value="{key('securityPermissions', 'managerCommentUpdate')/value}"/>					
					<input type="hidden" name="permitWeights" value="{key('securityPermissions', 'weightsView')/value}"/>					
					<input type="hidden" name="permitPerformance" value="{key('securityPermissions', 'performanceView')/value}"/>			
          			<input type="hidden" name="permitSourceCodes" value="{key('securityPermissions', 'sourceCodeView')/value}"/>  

					<input type="hidden" name="isVendor" value="{floristData/generalData/vendorFlag}"/>
					<input type="hidden" name="vendorflag" value="{key('securityPermissions', 'vendorFlagUpdate')/value}"/>
					<input type="hidden" name="sc_includeVendor" value="{key('searchCriteria', 'sc_includeVendor')/value}"/>

			        <input type="hidden" name="flnum"/>
			        <input type="hidden" name="dispositionDescription"/>
			        <input type="hidden" name="newcomment"/>
			        <input type="hidden" name="newmanagerflag"/>

     				<input type="hidden" name="isNew" readonly="true" value="{key('processFlow', 'isNew')/value}"/>
     				
     				<script>
     				if(floristMaint.isNew.value == '')
     					floristMaint.isNew.value = "N";
     				</script>
     				
     				<input type="hidden" name="processType" value="{key('processFlow', 'processType')/value}"/>
	        		<input type="hidden" name="isAdded" value="{key('processFlow', 'isAdded')/value}"/>
       				<input type="hidden" name="validProZip" value="{profileFlow/flow/value}"/>	
       				<input type="hidden" name="validProNum" value="{idFlow/flow/value}"/>	
       				<input type="hidden" name="loadOrdersSearch" value="{orderFlow/flow/value}"/>	
					<input type="hidden" name="lockUser" value="{floristData/generalData/lockedByUser}"/>
					<input type="hidden" name="lockFlag" value="{floristData/generalData/lockedFlag}"/>
       				
       				<input type="hidden" name="weightadj" value="{floristData/generalData/adjustedWeight}"/>
					<input type="hidden" name="zipvalneeded" value="no"/>
					<input type="hidden" name="idvalneeded" value="no"/>
					<input type="hidden" name="popupzipdeliverydate" value=""/>
					<input type="hidden" name="popupcitydeliverydate" value=""/>
					
	        		<xsl:variable name="mn" select="key('processFlow', 'processType')/value"/>
	        		
        			<xsl:if test="$mn = 'AddNew'">
						<script>
						floristMaint.isNew.value = 'Y';
						</script>
					</xsl:if>

					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Florist Maintenance'" />
						<xsl:with-param name="showExitButton" select="true()"/>
						<xsl:with-param name="showTime" select="true()"/>
					</xsl:call-template>
					
					<!-- Outer div needed to hold any content that will be blocked when an action is performed -->
					<div id="outerContent" style="display:block">
						<table width="100%" border="0"  cellpadding="0" cellspacing="0">
							<tr>
								<td nowrap=""></td>
							</tr>
							<tr>
								<td nowrap="" width="20%" id="returntoResults"> &nbsp;
									<a id="rs" href="javascript:doReturnSearchAction();" tabindex="15">Return to Florist Search Results</a>
								</td>
								<td nowrap="" id="ronly" width="10%" align="left" class="RequiredFieldTxt" style="display:none"> Read Only</td>
								<td nowrap="" id="lockedrecord" width="70%" align="left" class="RequiredFieldTxt" style="display:none">
								 <xsl:value-of select="floristData/generalData/lockedByUser"/>&nbsp;is currently viewing this record.
								 </td>
							</tr>
							<tr>
								<td nowrap="" height="8"></td>
							</tr>
						</table>

						<!-- Tabs template -->
		
						<xsl:call-template name="tabs"/>
						<ul id="content">
						
							<div id="profilediv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
										<xsl:call-template name="profile"/>
									</table>
									
								</td></tr>
									<tr>
										<td align="right"><input type="button" name="profileExit2" tabindex="14" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
								
								<script language="javascript">convertPhones();</script>
							
							</div>
							
							<input type="hidden" value="no" name="savezipNeeded"></input>
								<div id="zipcodesdiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
                                                                                <script type="text/javascript" language="javascript">
                                                                                                <![CDATA[
                                                                                                var requestType = "";
                                                                                                ]]>
                                                                                </script>
                                                                                
                                                                                <xsl:choose>
                                                                                        <xsl:when test="key('securityPermissions', 'directoryCityUpdate')/value = 'Y'"> 
                                                                                                <xsl:call-template name="zipcodesdirectorycity"/>
                                                                                
                                                                                                <script type="text/javascript" language="javascript">
                                                                                                        <![CDATA[
                                                                                                        requestType = "zipCodesDirectoryCity";
                                                                                                        ]]>
                                                                                                </script>                                                                          
                                                                                        </xsl:when> 
                                                                                        <xsl:otherwise>  
                                                                                                <xsl:call-template name="zipcodes"/>
                                                                                                
                                                                                                <script type="text/javascript" language="javascript">
                                                                                                        <![CDATA[
                                                                                                        requestType = "zipCodes";
                                                                                                        ]]>
                                                                                                </script> 
                                                                                        </xsl:otherwise> 
                                                                                </xsl:choose>        
									</table>
								</td></tr>
								<tr>
										<td align="right"><input type="button" name="zipExit2" tabindex="10" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
								</div>

							<div id="codificationdiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
										<xsl:call-template name="codification"/>
									</table>
								</td></tr>
								<tr>
										<td align="right"><input type="button" tabindex="13" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
							
							</div>
							
							<div id="historydiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
										<xsl:call-template name="history"/>
									</table>
								</td></tr>
								<tr>
										<td align="right"><input type="button" tabindex="5" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>

							</div>
							
							<div id="associationdiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
										<xsl:call-template name="association"/>
									</table>
								</td></tr>
								<tr>
										<td align="right"><input type="button" name="ascexit" tabindex="4" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
							</div>
							
							<div id="weightsdiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
				
										<xsl:call-template name="weights"/>
										
									</table>
								</td></tr>
								<tr>
										<td align="right"><input type="button" tabindex="8" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
							</div>
						
							<div id="ordersdiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
										<xsl:call-template name="orders"/>
									</table>
								</td></tr>
								<tr>
								<td align="right"><input type="button" name="orderexit" tabindex="11" value="Exit" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
							</div>
							<div id="citiesdiv">
								<table width="100%" border="0">
								<tr><td>
									<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
										<xsl:call-template name="cities"/>
									</table>
								</td></tr>
								<tr>
										<td align="right"><input type="button" tabindex="2" value="Exit" accesskey="E" onclick="javascript:doExitAction();"></input></td>
									</tr>
									<tr><td><xsl:call-template name="footer"/></td></tr>
								</table>
							</div>
              <div id="sourcecodesdiv">
                <table width="100%" border="0">
                <tr><td>
                  <table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
                    <xsl:call-template name="sourcecodes"/>
                  </table>
                </td></tr>
                <tr>
                    <td align="right"><input type="button" tabindex="2" value="Exit" accesskey="E" onclick="javascript:doExitAction();"></input></td>
                  </tr>
                  <tr><td><xsl:call-template name="footer"/></td></tr>
                </table>
              </div>  

						</ul>
					</div>
					
					<input type="hidden" value="{key('processFlow', 'endTab')/value}" name="endTab" />
					<input type="hidden" value="{key('processFlow', 'secondaryAction')/value}" name="secondaryAction" />
					<input type="hidden" value="{key('processFlow', 'saveNeeded')/value}" name="saveNeeded"></input>
					
					
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage" />
										<td id="waitTD" width="50%" class="waitMessage" />
									</tr>
								</table></td>
						</tr>
					</table>
				</div>

				</form>
                        </body>
		</html>
<script type="text/javascript">
doAddNew();
init1();

var s = '<xsl:value-of select="key('linkFlow', 'display')/value"/>';
s = s + "";

if(s == "no")
{
document.getElementById("rs").firstChild.data  = "";
}
	
</script>
	</xsl:template>
</xsl:stylesheet>
