<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="zipcodesdirectorycity">
	<script type="text/javascript" language="javascript">
	
<![CDATA[
function  assignmentChange()
{
    if(document.getElementById("assignment").checked == true)
    {
        document.getElementById("assignmentType").value = "S";
    }
    else
    {
        document.getElementById("assignmentType").value = "T";
    }
}

function assignedChange(checkboxName, pos)
{
    if(checkboxName == "unassign")
    {
        if(document.getElementById(checkboxName + pos).checked == true) 
        {
            //document.getElementById("assignedStatus" + pos).value ="avail";
            document.getElementById("zipblock" + pos).value ="Y";
            document.getElementById("cutofftime" + pos).disabled = true;
        }
        else
        {
            document.getElementById("assignedStatus" + pos).value ="assigned";
            document.getElementById("zipblock" + pos).value ="N";
            document.getElementById("cutofftime" + pos).disabled = false;
        }
    }
    else
    {
        if(document.getElementById(checkboxName + pos).checked == true) 
        {
            document.getElementById("assignedStatus" + pos).value ="assigned";
            document.getElementById("cutofftime" + pos).disabled = false;
        }
        else
        {
            document.getElementById("assignedStatus" + pos).value ="avail";
            document.getElementById("cutofftime" + pos).disabled = true;
        } 
    }

    enSave();  
}

function enableInputs()
{
    if(document.getElementById("savezip1").style.color == "black" && document.getElementById("savezip2").style.color == "black")
    {
        for(pos = 1; pos <= document.getElementById("totalZipcodes").value; pos++)
        {
            if(document.getElementById("cutofftime" + pos) != null)
            {
                document.getElementById("cutofftime" + pos).disabled = false;
            }
        }
    }
}

function zipSaveAction()
{
    enableInputs();
    saveAction();
}
]]>
</script>

									<tr>
										<td colspan="5">
											<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
											<tr>
												<td width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>
												</td>
												<td width="10%"/>
												<td width="40%">
													&nbsp; &nbsp; &nbsp;
													<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
													<xsl:if test="$goto = 'Y'">
													GoTo Florist
													</xsl:if> &nbsp;
												</td>
											</tr>

												
                                                                                        <tr bordercolor="white">
												
   												<td id="errorExistingZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													The following Zip Codes already exist for this florist - <br></br>
                                                                                                        <input type="hidden" name="listExisting" readonly="true" size="20" value="{existingZips/ezips/value}"/>
                                                                                                        <xsl:value-of select="existingZips/ezips/value" /> <br></br>
												</td>

												<td id="errorholdExistingZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
												</td>
												
												<td width="10%"/>
												<td width="40%"/>
                                                                                        </tr>                                                                                            
												
                                                                                        <tr bordercolor="white">
												<td id="errorInvalidZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The following Zip/Postal Codes are invalid - <br></br>
													<input type="hidden" name="listInvalid" readonly="true" size="20" value="{invalidZips/izips/value}"/>
													<xsl:value-of select="invalidZips/izips/value" />
												</td>

                                                                                                <td id="errorInvalidCity" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The city entered was invalid <br></br>
													<input type="hidden" name="listInvalidCity" readonly="true" size="20" value="{pageData/data/value}"/>
												</td>

												<td id="errorholdInvalidZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
												</td>                                                                                                
                                                                                                
                                                                                                <td class="label" align="right" width="10%"/>
													
                                                                                        </tr>
												
                                                                                        <tr>
                                                                                                <td align="center" colspan="5">
                                                                                                        <input type="button" align="center" value="SAVE" name="savezip1" size="10" onClick="zipSaveAction();"></input>
                                                                                                        &nbsp; &nbsp; &nbsp;
                                                                                                </td>
                                                                                        </tr>
                                                                                        
                                                                                        <tr bordercolor="white">
                                                                                                <td id="errorAscZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The following Zip/Postal Codes already exist on an associated florist - <br></br>
													<input type="hidden" name="listAssociated" readonly="true" size="20" value="{associatedZips/azips/value}"/>
													<xsl:value-of select="associatedZips/azips/value" />
                                                                                                </td>

                                                                                                <td id="errorholdAscZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
                                                                                                </td>
													
                                                                                                <td width="10%" align="left"></td>
                                                                                                <td width="40%" align="left"></td>
                                                                                        </tr>                                                                                    
                                                                                        
											</table>
										</td>
									</tr>

									<tr>
										<td class="tblheader" align="left">
									 	Add City
                                                                                </td>
									</tr>
									<tr>
										<td>
											<table cellpadding="1" cellspacing="1" width="100%" border="0" >
											<tr align="center">
												<td class="label" align="right">City</td>
												<td>
													<input type="text" name="newcity" />
												</td>

                                                                                                <td align="left">
                                                                                                        <font class="label">&nbsp;&nbsp;&nbsp;State/Pro
                                                                                                        </font>
							
                                                                                                        <select name="newstate">
                                                                                                                <option value="">Select...</option>
								
                                                                                                                <xsl:for-each select="stateList/state">
                                                                                                                        <option value="{statemasterid}">
                                                                                                                                <xsl:value-of select="statemasterid" />
                                                                                                                        </option>
                                                                                                                </xsl:for-each>
                                                                                                        </select>
                                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                </td>

                                                                                                <td class="label" align="right">Permanent</td>

                                                                                                <td>
                                                                                                        <input type="checkbox" name="assignment" checked="on" onclick="assignmentChange();"/>
                                                                                                        <input type="hidden" name="assignmentType" value="S"/>
                                                                                                </td>

												<td class="label"> Cut Off Time
												&nbsp; &nbsp; &nbsp; &nbsp;
													<select name="newcutofftime">
													
													<xsl:for-each select="cutoffTimeList/cutoffTime">
														<xsl:variable name="cut" select="cutoffMilitary" />

														<option value="{cutoffMilitary}">
														<xsl:value-of select="cutoffDisplay" />
														</option>
													</xsl:for-each>

													</select>
													<script>floristMaint.newcutofftime.selectedIndex = 1;</script>
												</td>
										
                                                                                
                                                                                
												<td>
													<input type="button" align="center" value="Add" name="add" size="10" onClick="addAction();"></input>
												</td>

                                                                                
                                                                                
                                                                                
                                                                                </tr>
											</table>
										</td>
									</tr>
                                                                        
                                                                        <tr>
										<td class="tblheader" align="left">
									 	Manage Zip/Postal Codes 
									 	 </td>
									</tr>

                                                                        <tr>
                                                                                <td>
                                                                                        <table width="100%" border="0">	
                                                                                                <tr>
                                                                                                        <td width="38%" align="center"><b>Available Zip Codes</b></td>								
                                                                                                        <td width="38%" align="center"><b>Unassigned Zip Codes</b></td>
                                                                                                        <td width="24%" align="center"><b>Excluded Zip Codes</b></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                        <td align="center">&nbsp;</td>								
                                                                                                        <td align="center">Florist should not charge more than $7.00 for delivery to any zip code selected for service</td>
                                                                                                        <td align="center">Please let the florist know that they will be contacted by FTD.COM fulfillment</td>
                                                                                                </tr>
                                                                                        </table>
                                                                                </td>
									</tr>

									<tr>

									<td>
									
                                                                        <table width="100%" border="0">
										<tr>
                                                                                        <td colspan="9" width="100%" align="center">

                                                                                            <table width="100%" border="1">
												<tr>
                                                                                                    <td width="38%" valign="top">
                                                                                                        
                                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
                                                                                                                        <td width="8%" class="colHeaderCenter">
                                                                                                                        Unassign
                                                                                                                        </td>
                                                                                                                        
															<td width="16%" class="colHeaderCenter">Zip Code</td>
															<td width="47%" class="colHeaderCenter">City</td>
															<td class="colHeaderCenter">Cut Off Time</td>
														</tr>

														<tr>
															<td>
															<input type="hidden" name="totalZipcodes" value="{count(floristData/postalCodeList/postalCode/postalCode)}"></input>
                                                                                                                        <input type="hidden" name="newzipunblockdate" value="" />
															</td>
														</tr>
                                                                                                                
                                                                                                                <xsl:for-each select="floristData/postalCodeList/postalCode[assignedStatus = 'assigned']"> 
                                                                                                                
															<xsl:sort select="postalCode" data-type="number" />
															
                                                                                                                        <xsl:variable name="realPos">
                                                                                                                                <xsl:number />
                                                                                                                        </xsl:variable>
                                                                                                                        
															<tr id="zip_row{$realPos}" style="display:block">
                                                                                                                            
                                                                                                                                <td align="center">
                                                                                                                                        <input type="checkbox" name="unassign{$realPos}" onclick="assignedChange('unassign','{$realPos}');" />
                                                                                                                                </td> 
												
                                                                                                                                <td width="10%" align="center">
                                                                                                                                        <input name="deleteFlag{$realPos}" type="hidden" value="{deleteFlag}"/>
                                                                                                                                        <input name="zipcode{$realPos}" type="hidden" value="{postalCode}"/>

                                                                                                                                        <input name="zipblock{$realPos}" type="hidden" value="{blockedFlag}"/>
                                                                                                                                        <input name="zipunblockdate{$realPos}" type="hidden" value="{blockEndDate}"/>
                                                                                                                                        <input name="assignedStatus{$realPos}" type="hidden" value="{assignedStatus}"/>
                                                                                                                                        <input name="assignmentType{$realPos}" type="hidden" value="{assignmentType}"/>

                                                                                                                                        <xsl:value-of select="postalCode"/>
                                                                                                                                </td>

                                                                                                                                <td width="20%" align="center">
                                                                                                                                        <xsl:value-of select="city" />
                                                                                                                                </td>

                                                                                                                                <td align="center">

                                                                                                                                <select name="cutofftime{$realPos}" >
																	
                                                                                                                                        <xsl:variable name="cuttime" select="cutoffTime" />

                                                                                                                                        <xsl:for-each select="//cutoffTimeList/cutoffTime">
                                                                                                                                                <xsl:variable name="cut" select="cutoffMilitary" />
	
                                                                                                                                                <option value="{cutoffMilitary}">
	
                                                                                                                                                <xsl:if test="$cuttime = $cut">
                                                                                                                                                <xsl:attribute name="SELECTED" />
                                                                                                                                                </xsl:if>
	
                                                                                                                                                <xsl:value-of select="cutoffDisplay" />
                                                                                                                                                </option>
                                                                                                                                        </xsl:for-each>
                                                                                                                                </select>

                                                                                                                                </td>
    
                                                                                                                        </tr>
															
                                                                                                                        <xsl:variable name="savedzipStat" select="savedFlag" />
                                                                                                                        <xsl:if test="savedFlag = 'N'">
                                                                                                                            <script>
                                                                                                                                var zipR =  <xsl:value-of select="$realPos"/>;
                                                                                                                                zipR = zipR + "";
																document.getElementById("unassign" + zipR).disabled = true;
                                                                                                                            </script>
                                                                                                                        </xsl:if>
                                                                                                                
                                                                                                                </xsl:for-each>
													</table>
                                                                                                    </td>
                                                                                                        
                                                                                                    <td width="38%" valign="top">
                                                                                                            <table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
                                                                                                                        <td width="8%" class="colHeaderCenter">
                                                                                                                        Assign
                                                                                                                        </td>
                                                                                                                        
															<td width="16%" class="colHeaderCenter">Zip Code</td>
															<td width="47%" class="colHeaderCenter">City</td>                                                                                                                      
															<td class="colHeaderCenter">Cut Off Time</td>
														</tr>
                                                                                                                
														<xsl:for-each select="floristData/postalCodeList/postalCode[assignedStatus = 'avail']"> 
                                                                                                                        
                                                                                                                        <xsl:sort select="postalCode" data-type="number" />
                                                                                                                        
                                                                                                                        <xsl:variable name="realPos">
                                                                                                                                <xsl:number />
                                                                                                                        </xsl:variable>
															
															<tr id="zip_row{$realPos}" style="display:block">
                                                                                                                            
                                                                                                                                <td align="center">
                                                                                                                                        <input type="checkbox" name="assign{$realPos}" onclick="assignedChange('assign','{$realPos}');" />
                                                                                                                                </td> 
												
                                                                                                                                <td width="10%" align="center">
                                                                                                                                        <input name="deleteFlag{$realPos}" type="hidden" value="{deleteFlag}"/>
                                                                                                                                        <input name="zipcode{$realPos}" type="hidden" value="{postalCode}"/>

                                                                                                                                        <input name="zipblock{$realPos}" type="hidden" value="{blockedFlag}"/>
                                                                                                                                        <input name="zipunblockdate{$realPos}" type="hidden" value="{blockEndDate}"/>
                                                                                                                                        <input name="assignedStatus{$realPos}" type="hidden" value="{assignedStatus}"/>
                                                                                                                                        <input name="assignmentType{$realPos}" type="hidden" value="{assignmentType}"/>

                                                                                                                                        <xsl:value-of select="postalCode"/>
                                                                                                                                        
                                                                                                                                </td>

                                                                                                                                <td width="20%" align="center">
                                                                                                                                        <xsl:value-of select="city" />
                                                                                                                                </td>

                                                                                                                                <td align="center">

                                                                                                                                <select name="cutofftime{$realPos}" disabled="true" >
																	
                                                                                                                                        <xsl:variable name="cuttime" select="cutoffTime" />

                                                                                                                                        <xsl:for-each select="//cutoffTimeList/cutoffTime">
                                                                                                                                                <xsl:variable name="cut" select="cutoffMilitary" />
	
                                                                                                                                                <option value="{cutoffMilitary}">
	
                                                                                                                                                <xsl:if test="$cuttime = $cut">
                                                                                                                                                <xsl:attribute name="SELECTED" />
                                                                                                                                                </xsl:if>
	
                                                                                                                                                <xsl:value-of select="cutoffDisplay" />
                                                                                                                                                </option>
                                                                                                                                        </xsl:for-each>
                                                                                                                                </select>

                                                                                                                                </td>

															</tr>
															
														</xsl:for-each>

                                                                                                            </table>                                                                                                       
                                                                                                    </td>

                                                                                                    <td width="24%" valign="top">
                                                                                                            <table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
															<td class="colHeaderCenter">Zip Code</td>
														</tr>
    
														<xsl:for-each select="floristData/postalCodeList/postalCode[assignedStatus = 'unavail' or assignedStatus = 'assignedUnavail']">
															<xsl:sort select="postalCode" data-type="number" />
                                                                                                                        
                                                                                                                        <xsl:variable name="realPos">
                                                                                                                                <xsl:number />
                                                                                                                        </xsl:variable>
															
															<tr id="zip_row{$realPos}" style="display:block">
    
																<td align="center">
                                                                                                                                        <xsl:value-of select="postalCode"/>

                                                                                                                                        <input name="deleteFlag{$realPos}" type="hidden" value="{deleteFlag}"/>
                                                                                                                                        <input name="zipcode{$realPos}" type="hidden" value="{postalCode}"/>

                                                                                                                                        <input name="zipblock{$realPos}" type="hidden" value="{blockedFlag}"/>
                                                                                                                                        <input name="zipunblockdate{$realPos}" type="hidden" value="{blockEndDate}"/>
                                                                                                                                        <input name="assignedStatus{$realPos}" type="hidden" value="{assignedStatus}"/>
                                                                                                                                        <input name="cutofftime{$realPos}" type="hidden" value="{cutoffTime}"/>
                                                                                                                                        <input name="assignmentType{$realPos}" type="hidden" value="{assignmentType}"/>
                                                                                                                                </td>
                                        
                                                                                                                        </tr>
                                                                                                                </xsl:for-each>        
                                                                                                                            
                                                                                                            </table>                                                                                                       
                                                                                                    </td>                                                                                                    
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                </tr>

									</table>

									</td>
									</tr>

									<tr><td align="center"><input type="button" align="center" value="SAVE" name="savezip2" size="10" onClick="zipSaveAction();"></input></td></tr>
	</xsl:template>
</xsl:stylesheet>