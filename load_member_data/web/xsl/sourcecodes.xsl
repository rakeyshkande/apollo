<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="sourcecodes">

<tr bordercolor="white">
	  <td>
      <table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
        <tr>
          <td  width="48%" valign="top">
            <table width="100%" border="0" cellpadding="3" cellspacing="2" height="100%">
              <tr>
                <td align="right" width="22"><img src="../images/primarySource.gif" border="0"/></td>
                <td align="left" valign="middle" width="100%"><b>Primary florist on these source codes</b></td>
              </tr>
              <tr>
                <td colspan="2" style="border:1px solid black; height:100%;padding-left:3px;">
                  <xsl:call-template name="sourceCodeList">
                    <xsl:with-param name="sourceType" select="'primary'"/>
                  </xsl:call-template>
                </td>
              </tr>
              </table>
          </td>
          <td width="4%"></td>
          <td  width="48%" valign="top">
            <table width="100%" border="0" cellpadding="3" cellspacing="2" height="100%">
              <tr>
                <td align="right" width="22"><img src="../images/backupSource.gif" border="0"/></td>
                <td align="left" valign="middle" width="100%"><b>Backup florist on these source codes</b></td>
              </tr>
              <tr>
                <td colspan="2" style="border:1px solid black; height:100%;padding-left:3px;">
                  <xsl:call-template name="sourceCodeList">
                    <xsl:with-param name="sourceType" select="'backup'"/>
                  </xsl:call-template>
                </td>
              </tr>
            </table> 
          </td>
        </tr>
      </table>
    </td>
</tr>
</xsl:template>

<xsl:template name="sourceCodeList">
  <xsl:param name="sourceType"></xsl:param>
  
	<!-- Select all entries in source list filtered by the priority parameter -->
  
    <xsl:if test="$sourceType='primary'">
      <xsl:for-each select="//floristData/sourceList/source[priority=1]">
        <xsl:sort select="code"/>
        <xsl:sort select="name"/>
        <xsl:value-of select="code"/> - <xsl:value-of select="name"/><br/>
      </xsl:for-each>
    </xsl:if>
    <xsl:if test="$sourceType='backup'">
      <xsl:for-each select="//floristData/sourceList/source[priority>1]">
        <xsl:sort select="code"/>
        <xsl:sort select="name"/>
        <xsl:value-of select="code"/> - <xsl:value-of select="name"/><br/>
      </xsl:for-each>
    </xsl:if>
</xsl:template>


</xsl:stylesheet>