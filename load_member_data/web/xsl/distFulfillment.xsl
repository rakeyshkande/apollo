<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<!--  Keys -->
	<xsl:key name="security" match="/root/security/data" use="name" />
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name" />
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Distribution / Fulfillment</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<script type="text/javascript" language="javascript" src="../js/util.js"/>
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
				<script type="text/javascript" language="javascript">



<![CDATA[

function doExitAction()
{
	alert('exited.');
}

function doContAction()
{
	var selReport = distFul.selectReports.value;
	if(selReport != "none")
	{
		showWaitMessage("content", "wait", "Loading...");
		document.forms[0].action = "MemberDataLoadServlet?action=view-florist-audit-report&reportID=" + selReport;
		document.forms[0].submit();
	}
}

]]>


</script>
			</head>
			<body>
				<form name="distFul" method="post" action="FloristSearchServlet?action=searchFlorist">
					<input type="hidden" name="servlet_action"/>


					<!-- Header Template -->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Distribution / Fulfillment'" />
						<xsl:with-param name="showExitButton" select="true()"/>
						<xsl:with-param name="showTime" select="true()"/>
					</xsl:call-template>
					<div id="content" style="display:block" >
						<table id="" width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
							<tr bordercolor="white"><td><br></br></td></tr>
							<tr bordercolor="white">
								<td width="30%"></td>
								<td width="50%" class="label">Florist Audit Report</td>
								<td width="20%"/>
							</tr>
							
							<tr bordercolor="white">
							<td></td>
							<td>
								<select name="selectReports">
								<option value="none" selected="true">No Reports Selected &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
								<option value="1">Report 1</option>
								<option value="2">Report 2</option>
								<!-- add others -->
								
								</select>
							</td>
							</tr>
							
							<tr bordercolor="white"><td><br></br></td></tr>
							<tr bordercolor="white">
							<td></td>
							<td><input type="button" value="Continue" onclick="doContAction();"></input></td>
							</tr>
							
							<tr bordercolor="white"><td><br></br></td></tr>
							
						</table>
					</div>
					
					<div id="waitDiv" style="display:none">
						<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="waitMessage" />
											<td id="waitTD" width="50%" class="waitMessage" />
										</tr>
									</table></td>
							</tr>
						</table>
					</div>
				
						<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
							<tr>
								<td align="right">
									<div align="right" class="button">
										<input type="button" value="Exit" tabindex="13" onclick="javascript:doExitAction();"></input>
									</div>
								</td>
							</tr>
						</table>
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>



