<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="orders">
	<script type="text/javascript" language="javascript">
	<![CDATA[
	
	
	function checkOrderChange()
	{
		 	var vstart= floristMaint.orderStartDate.value;
		    var vend = floristMaint.orderEndDate.value;
		    var vmercury = floristMaint.orderMercury.value;
		
			var result=0;
	
		 	//result = (trim(vstart).length * trim(vend).length) + trim(vmercury).length;
		 	result = trim(vstart).length + trim(vmercury).length;
	
			if(result > 0)
			{
				floristMaint.searchOrders.disabled=false;
			}
			else
			{
				floristMaint.searchOrders.disabled=true;
			}
      }
	
  	function contiOrderCheck(inObject)
    {
   		checkOrderChange();
 		document.getElementById(inObject).focus;
    }
    
    
	function validateOrders()
	{
	var today = new Date();
	var todayDate = today.getDate();
	var eCount = 0;
	 	
	document.getElementById("invalidOrderStartDate").style.display = "none";
	document.getElementById("errorOrderStartDate").style.display = "none";
	
	document.getElementById("errorOrderMercury").style.display = "none";
	
	document.getElementById("invalidOrderEndDate").style.display = "none";
	//document.getElementById("errorOrderEndDate").style.display = "none";
	document.getElementById("biggerOrderEndDate").style.display = "none";
	
	document.getElementById("holdStartDate").style.display = "block";
	document.getElementById("holdEndDate").style.display = "block";
	document.getElementById("holdOrderMercury").style.display = "block";
	
	document.getElementById("errorOrderMercValue").style.display = "none";
	document.getElementById("holdOrderMercValue").style.display = "block";
				
		   		
	// validate start date...
	var uDate = floristMaint.orderStartDate.value;
	var dateArray = new Array();
		if(uDate.length > 0)
		{

			dateArray = uDate.split("/");

			if(dateArray.length == 2)
			{
		
			floristMaint.orderStartDate.value += "/" + today.getYear();
			uDate = floristMaint.orderStartDate.value;
			dateArray = uDate.split("/");
			}

	   		if(dateArray.length != 3)
			{
				document.getElementById("invalidOrderStartDate").style.display = "block";
				eCount++;
			}
			else
			{
				if(dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					floristMaint.orderStartDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
			}
			
			if(floristMaint.orderStartDate.value.length != 10)
			{
				document.getElementById("invalidOrderStartDate").style.display = "block";
	   			eCount++;
			}
			
			
	   		
	   		if(dateArray[2].length != 2)
			{
				if(dateArray[2].length != 4)
				{
					document.getElementById("invalidOrderStartDate").style.display = "block";
		   			eCount++;
				}
				else
				{
					if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
				     {
     					document.getElementById("invalidOrderStartDate").style.display = "block";
				   		document.getElementById("holdStartDate").style.display = "none";
				   		eCount++;
				     }
			     }
			}
		
		}
		
		uDate = floristMaint.orderEndDate.value;
		dateArray = new Array();
		if(uDate.length > 0)
		{

			dateArray = uDate.split("/");

			if(dateArray.length == 2)
			{
			floristMaint.orderEndDate.value += "/" + today.getYear();
			uDate = floristMaint.orderEndDate.value;
			dateArray = uDate.split("/");
			}

	   		if(dateArray.length != 3)
			{
				document.getElementById("invalidOrderEndDate").style.display = "block";
				eCount++;
			}
			else
			{
				if(dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					floristMaint.orderEndDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
			}
			
			if(floristMaint.orderEndDate.value.length != 10)
			{
				document.getElementById("invalidOrderEndDate").style.display = "block";
	   			eCount++;
			}

			
			if(dateArray[2].length != 2)
			{
				if(dateArray[2].length != 4)
				{
					document.getElementById("invalidOrderEndDate").style.display = "block";
		   			eCount++;
				}
				else
				{
					if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
     				{
	     				document.getElementById("invalidOrderEndDate").style.display = "block";
				   		document.getElementById("holdEndDate").style.display = "none";
				   		eCount++;
					}
		     	}
			}
			
		}
		
		var sDate = Date.parse(floristMaint.orderStartDate.value);
		var eDate;
		var today = new Date();
		
		if(trim(floristMaint.orderEndDate.value).length > 0)
			eDate = Date.parse(floristMaint.orderEndDate.value);
		else
			eDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());

		if(trim(floristMaint.orderStartDate.value).length > 0)
		{
			if(eCount < 1)
			{
				sDate = sDate / 86400000;
				eDate = eDate / 86400000;

				var diff = eDate - sDate;

				if(diff < 0)
				{
					document.getElementById("biggerOrderEndDate").style.display = "block";
					eCount++;
				}
			}
		}
		
		// validate mercury order #
		var vmercury = trim(floristMaint.orderMercury.value);
		var noMerc = 0;
		if(vmercury.length > 0)
		{
		
			if(vmercury.length != 11)
			{
				noMerc++;
				eCount++;
			}
			
			if(vmercury.charAt(6) != "-")
			{
				noMerc++;
				eCount++;
			}
			
			if( isLetter(vmercury.charAt(1)) || isLetter(vmercury.charAt(2)) || isLetter(vmercury.charAt(3)) || isLetter(vmercury.charAt(4)) )
			{
			noMerc++;
			eCount++;
			}
			
			if( isLetter(vmercury.charAt(7)) || isLetter(vmercury.charAt(8)) || isLetter(vmercury.charAt(9)) || isLetter(vmercury.charAt(10)) )
			{
			noMerc++;
			eCount++;
			}
			
			if( isDigit(vmercury.charAt(0)) || isDigit(vmercury.charAt(5)) )
			{
			noMerc++;
			eCount++;
			}

		}
		
		if(noMerc > 0)
		{
			document.getElementById("errorOrderMercury").style.display = "block";
			document.getElementById("holdOrderMercury").style.display = "none";
		}
		
	 	var vstart= floristMaint.orderStartDate.value;
	    var vend = floristMaint.orderEndDate.value;

/*
	 	if( trim(vstart).length > 0 || trim(vend).length > 0 )
	 	{
		 	if((trim(vstart).length * trim(vend).length) == 0)
		 	{
				if(trim(vstart).length == 0)
				{
					document.getElementById("errorOrderStartDate").style.display = "block";
					document.getElementById("holdStartDate").style.display = "none";
				}
				else
				{
					document.getElementById("errorOrderEndDate").style.display = "block";
					document.getElementById("holdEndDate").style.display = "none";
				}
				eCount++;

		 	}
	 	}
*/	 	
	 	// validate merc value...
	 	
	 	var vmercval = trim(floristMaint.orderMercValue.value);

	 	if(vmercval.length > 0)
		{
	
		var p=0;

			if(vmercval.length > 6)
			{
				document.getElementById("errorOrderMercValue").style.display = "block";
				document.getElementById("holdOrderMercValue").style.display = "none";
				eCount++;
			}
			else
			{
				for(var i=0; i<vmercval.length; i++)
				{
					if(vmercval.charAt(i) == ".")
					{
						p = i;
						continue;
					}	
				
					if(!isDigit(vmercval.charAt(i)))
					{
						document.getElementById("errorOrderMercValue").style.display = "block";
						document.getElementById("holdOrderMercValue").style.display = "none";
						eCount++;
						break;
					}
				}
				
				if( p != (vmercval.length - 3))
				{
					document.getElementById("errorOrderMercValue").style.display = "block";
					document.getElementById("holdOrderMercValue").style.display = "none";
					eCount++;
				}

			}
		}
		
		if(eCount > 0)
			return false;
		else
			return true;
		
	} // end of validate orders function
	
	
    function setScrollingDivOrderHeight()
    {
		var searchResultsDiv = document.getElementById("oResults");
		var newHeight = document.body.clientHeight - searchResultsDiv.getBoundingClientRect().top - 100;
		if (newHeight > 15)
		{
			searchResultsDiv.style.height = newHeight;
		}
		
    }
    	
    
	function initOrders()
	{
		setNavigationHandlers();
	    setScrollingDivOrderHeight();
	    window.onresize = setScrollingDivOrderHeight;
	    
	}
	
	]]>
	</script>
		<tr bordercolor="white">
			<td>
				<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1" bordercolor="white">
					<tr bordercolor="white">
						<td  width="50%" align="center">
						<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>

						</td>
						<td width="10%"/>

						<td width="40%">
						&nbsp; &nbsp; &nbsp;
						<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
						<xsl:if test="$goto = 'Y'">
						GoTo Florist
						</xsl:if> &nbsp;
						</td>
					</tr>

					<tr bordercolor="white">
						<td align="right" width="50%"></td>


						<td class="label" align="right" width="10%"> Status </td>
                                                <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
					</tr>
					<tr></tr>

				</table>

			</td>
			</tr>

			<tr bordercolor="white">
			<td id="orderSearch">

			<table  onmousemove="checkOrderChange();" id="" width="98%" border="0" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
							<tr bordercolor="white"><td><br></br></td></tr>
							<tr bordercolor="white">
								<td>
									<br></br>
									
									<table id="" align = "center" cellspacing="2" cellpadding="1" bordercolor="white">
										<tr bordercolor="white">
												<td class="label"> Date </td>
												
												<td>
												<input name="orderStartDate" tabindex="1" type="text" maxlength="10" onchange="javascript:contiOrderCheck('orderStartDate');" onmousemove="javascript:contiOrderCheck('orderStartDate');"/>
							
												&nbsp; &nbsp;
												<!-- <img id="calendarStart" src="../images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/> -->
												<input type="image" tabindex="2" id="calendarStart" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
												&nbsp; &nbsp; &nbsp; <font class="label">To &nbsp;</font>
												</td>
											
												
												 <script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "orderStartDate",  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendarStart" // ID of the button
													    }
													  );

												]]></script>
													
												<td>
												<input name="orderEndDate" tabindex="3" maxlength="10" type="text" onkeyup="javascript:contiOrderCheck('orderEndDate');"></input>
												&nbsp; &nbsp;
												<input type="image" tabindex="4" id="calendarEnd" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
												&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
												</td>

													
												 <script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "orderEndDate",  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendarEnd" // ID of the button
													    }
													  );

												]]></script>
												

												<td><input type="radio" name="order_type" tabindex="5" value="delivery" checked="true">Delivery</input></td>
												<td><input type="radio" name="order_type" tabindex="5" value="order">Order</input></td>
										</tr>
										<tr>
												<td></td>
										
											 	<td align="left" id="invalidOrderStartDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
											 	</td>
											 	<td id="holdStartDate" style="display:block">&nbsp;</td>

											 	<td align="left" id="errorOrderStartDate" class="RequiredFieldTxt" style="display:none">
																		Required Field
											 	</td>
											 	
											 	<td align="left" id="invalidOrderEndDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
											 	</td>

											 	<td align="left" id="biggerOrderEndDate" class="RequiredFieldTxt" style="display:none">
													End Date must be greater than or equal to the Start Date.
											 	</td>
											 	
										
												<td id="holdEndDate" style="display:block">&nbsp;</td>
										
										</tr>
										
										<tr>
												<td class="label"> Mercury Order # </td>
												<td><input type="text" name="orderMercury" tabindex="6" onkeyup="javascript:contiOrderCheck('orderMercury');"/></td>
												
												<td align="left" id="errorOrderMercury" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
											 	</td>

												<td id="holdOrderMercury" style="display:block">&nbsp;</td>
										
										
										</tr>
										<tr><td><br></br></td></tr>
										<tr>
										
												<td class="label" >**Recipient Name &nbsp; &nbsp; &nbsp;</td>
												<td><input tabindex="7" type="text" name="orderRecip"/></td>
							
										</tr>
										<tr><td><br></br></td></tr>
										<tr>
											<td class="label"> **MercValue </td>
											<td>
												<input tabindex="8" type="text" name="orderMercValue" size="20" maxlength="45"  value=""></input>
											</td>
											<td align="left" id="errorOrderMercValue" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
											 	</td>

												<td id="holdOrderMercValue" style="display:block">&nbsp;</td>
											
										</tr>
										<tr><td><br></br></td></tr>
										<tr>
											<td class="label"> **SKU </td>
											<td>
												<input tabindex="9"  type="text" name="orderSku" size="20"></input>
											</td>
											<td id="errorPhone" class="RequiredFieldTxt"  style="display:none">
												&nbsp; &nbsp; Invalid Format
											</td>
										</tr>
										<tr><td><br></br></td></tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>

									
									<br></br>
								</td>
							</tr>
							<tr><td align="center"><input type="button" disabled="true" align="center" tabindex="10" value="Search" name="searchOrders" size="10"  onClick="orderSearchAction();"></input></td></tr>

						</table>

						</td>
			</tr>





			<tr bordercolor="white">
			
			<td id="orderResults" >
			
			<table width="100%" border="0" align="center">
			
			<tr>
				<td colspan="11" align="center">

						<table width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
									<td width="10%" class="colHeaderCenter">Mercury #</td>
									<td width="10%" class="colHeaderCenter">Order #</td>
									<td width="10%" class="colHeaderCenter">Order Date</td>
									<td width="10%" class="colHeaderCenter">Delivery Date</td>
									<td width="10%" class="colHeaderCenter">Name</td>
									<td width="5%" class="colHeaderCenter">Status</td>
									<td width="10%" class="colHeaderCenter">MercValue</td>
									<td width="10%" class="colHeaderCenter">SKU</td>
									<td width="10%" class="colHeaderCenter">Sell Amount</td>
									<td width="5%" class="colHeaderCenter">Refund</td>
									<td width="10%" class="colHeaderCenter">Refund Disposition</td>
							</tr>
							
							<xsl:for-each select="order_result_list/order_result">

							<tr align="center">
								<td><xsl:value-of select="mercuryNumber"/></td>
								<td><xsl:value-of select="orderNumber"/></td>
								<td><xsl:value-of select="orderDate"/></td>
								<td><xsl:value-of select="deliveryDate"/></td>
								<td><xsl:value-of select="lastName"/></td>
								<td><xsl:value-of select="orderStatus"/></td>
								<td><xsl:value-of select="mercValue"/></td>
								<td><xsl:value-of select="orderSku"/></td>
							</tr>
							</xsl:for-each>
						</table>

				</td>
			</tr>
			</table>
		
			</td>

			</tr>
			
	</xsl:template>
</xsl:stylesheet>
