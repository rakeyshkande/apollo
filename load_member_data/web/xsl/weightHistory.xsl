<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">

	<xsl:variable name="selectedZip" select="key('pageData', 'zipcode')/value"/>
	<xsl:variable name="selectedZipTitle" select="'Florist Coverage in Zip Code '"/>

	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
 			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<title>FTD</title>

<script type="text/javascript" language="javascript">
<![CDATA[
	function doCloseAction()
	{
        	window.close();
   }
]]>
</script>
		</head>
		<body>
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="'Weight History'" />
				<xsl:with-param name="showExitButton" select="false()"/>
				<xsl:with-param name="showTime" select="false()"/>
			</xsl:call-template>

      <center>
			
			<input type="button" value=" Close " onClick="doCloseAction();"/>
			
      </center>
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td  colspan="9" class="tblheader" align="left"> Weight History </td>
			</tr>
			
			<tr>
			
					
				<td colspan="9" width="100%" align="center">
				
					<div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">

						<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
															<td width="20%" class="colHeaderCenter">Date</td>
															<td width="20%" class="colHeaderCenter">Initial</td>
															<td width="20%" class="colHeaderCenter">Final</td>
															<td width="20%" class="colHeaderCenter">Adjusted</td>
															<td width="20%" class="colHeaderCenter">User ID</td>
														</tr>

														<xsl:for-each select="WEIGHTS/WEIGHT">

														<tr>

														<td width="20%" align="center">
															<xsl:value-of select="history_date" />
														</td>
														<td width="20%" align="center">
															<xsl:value-of select="initial_weight" />
														</td>
														<td width="20%" align="center">
															<xsl:value-of select="florist_weight" />
														</td>
														<td width="20%" align="center">
                              								<xsl:value-of select="adjusted_weight"/>
														</td>
														<td width="20%" align="center">
															<xsl:value-of select="last_updated_by" />
														</td>
														
														</tr>

														</xsl:for-each>
						 </table>
					 </div>
				</td>
			</tr>
			</table>
      <center>
			<input type="button" value=" Close " onClick="doCloseAction();"/>
      </center>
			<xsl:call-template name="footer"/>
		</body>
	</html>

	</xsl:template>
</xsl:stylesheet>