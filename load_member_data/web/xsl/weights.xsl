<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="weights">
		
		
<script type="text/javascript" language="javascript">
<![CDATA[


function noEndDateAction()
{
enSave();

if(floristMaint.weightNoEndDate.checked == true)
	floristMaint.weightEndDate.value = "";
	
}

function endDateAction()
{

if(trim(floristMaint.weightEndDate.value).length > 0)
	floristMaint.weightNoEndDate.checked = false;

}

function doWeightHistoryPopUp()
{
  var url_source = "WeightServlet?action=loadWeightHistory&floristid="+floristMaint.memnum.value;
  var modal_dim = "dialogWidth:750px; dialogHeight:400px; center:yes; status=0; help:no; scroll:yes";
  var ret = window.showModalDialog(url_source, "", modal_dim);
}

function validateWeights()
{
var eCount=0;
document.getElementById("invalidWeightEndDate").style.display = "none";
document.getElementById("errorWeightEndDate").style.display = "none";
document.getElementById("biggerWeightEndDate").style.display = "none";
document.getElementById("invalidWeightStartDate").style.display = "none";
document.getElementById("invalidDateWeightStartDate").style.display = "none";
document.getElementById("invalidDateWeightEndDate").style.display = "none";
document.getElementById("invalidAdjustedWeight").style.display = "none";

		var today = new Date();
		
	 	var todayDate = (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear();
	
	 	if(trim(floristMaint.weightStartDate.value).length == 0)
	 	{
	 	//floristMaint.weightStartDate.value = todayDate;
	 	}
	 	
//function isDate (year, month, day)
//var re = isDate ("2003", "02", "28")


	 	var uDate = floristMaint.weightStartDate.value;
		var dateArray = new Array();
		if(uDate.length > 0)
		{
			dateArray = uDate.split("/");

			if(dateArray.length == 2)
			{
			floristMaint.weightStartDate.value += "/" +  today.getYear();
			uDate = floristMaint.weightStartDate.value;
			}

			dateArray = uDate.split("/")

			
			
/*
	    	if (!(isYear(dateArray[2], false) && isMonth(inMonth, false) && (dateArray[1] <= 31)))
	   		{
		   		document.getElementById("invalidWeightStartDate").style.display = "block";
		   		eCount++;
	   		}
*/
  		
			if(dateArray.length != 3)
			{
				document.getElementById("invalidWeightStartDate").style.display = "block";
				eCount++;
			}
			else
			{
				if(dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					floristMaint.weightStartDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
			}
			
			
			if(dateArray[2] != null)
			{
				if(dateArray[2].length != 2)
				{
					if(dateArray[2].length != 4)
					{
						document.getElementById("invalidWeightStartDate").style.display = "block";
			   			eCount++;
					}
					else
					{
					   if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0")))
					    {
					   		document.getElementById("invalidDateWeightStartDate").style.display = "block";
        					eCount++;
					    }
					}
				}
			}
			
			
			
			
			

		}
		
		if(trim(floristMaint.weightStartDate.value).length > 0)
		{
			if(floristMaint.isNew.value == "Y")
			{
				// check to see if its in the past..
				var iDate = Date.parse(floristMaint.weightStartDate.value);
		
				var todayDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());

				iDate = iDate / 86400000;
				todayDate = todayDate / 86400000;
		
				var diff = iDate - todayDate;
				
				if(diff < 0)
				{
					document.getElementById("invalidDateWeightStartDate").style.display = "block";
					eCount++;
				}
			}
		}
		
		uDate = floristMaint.weightEndDate.value;
		dateArray = new Array();
		if(uDate.length > 0)
		{
			dateArray = uDate.split("/");

			if(dateArray.length == 2)
			{
			floristMaint.weightEndDate.value += "/" + today.getYear();
			uDate = floristMaint.weightEndDate.value;
			}
			
			dateArray = uDate.split("/")


			
	   		
			if(dateArray.length != 3)
			{
				document.getElementById("invalidWeightEndDate").style.display = "block";
				eCount++;
			}
			else
			{
				if(dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					floristMaint.weightEndDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
			}
			
			if(dateArray[2] != null)
			{
				if(dateArray[2].length != 2)
				{
					if(dateArray[2].length != 4)
					{
						document.getElementById("invalidWeightEndDate").style.display = "block";
			   			eCount++;
					}
					else
					{
					   if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0")))
					    {
					   		document.getElementById("invalidDateWeightEndDate").style.display = "block";
        					eCount++;
					    }
					}
				}
			}
		}
		
		if((floristMaint.weightNoEndDate.checked == false) && (trim(floristMaint.weightEndDate.value).length == 0))
		{
			//document.getElementById("errorWeightEndDate").style.display = "block";
			//eCount++;
		}
		
		var eDate = Date.parse(floristMaint.weightEndDate.value);
		var sDate;
		var today = new Date();
		
		if(trim(floristMaint.weightStartDate.value).length > 0)
			sDate = Date.parse(floristMaint.weightStartDate.value);
		else
			sDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());
		
		if(trim(floristMaint.weightEndDate.value).length > 0)
		{

			if(eCount < 1)
			{
				sDate = sDate / 86400000;
				eDate = eDate / 86400000;
				var diff = eDate - sDate;
				if(diff < 0)
				{
					document.getElementById("biggerWeightEndDate").style.display = "block";
					eCount++;
				}
			}
		}
		
		if(floristMaint.isNew.value == "N")
		{
			
			if(trim(floristMaint.weightStartDate.value).length > 0 || trim(floristMaint.weightEndDate.value).length > 0)
			{
				if(document.getElementById("adjusted_weight").value.length < 1)
				{
				document.getElementById("invalidAdjustedWeight").style.display = "block";
				eCount++;
				}
			}
			
	
			if(floristMaint.weightNoEndDate.checked == true)
			{
				if(document.getElementById("adjusted_weight").value.length < 1)
				{
				document.getElementById("invalidAdjustedWeight").style.display = "block";
				eCount++;
				}
			}
			
			if(document.getElementById("adjusted_weight").value.length > 0)
			{
				if(!(trim(floristMaint.weightEndDate.value).length > 0 || floristMaint.weightNoEndDate.checked == true))
				{
					document.getElementById("errorWeightEndDate").style.display = "block";
					eCount++;
				}
			}
		
		}
		else
		{
			if(eCount == 0)
			{
			
			/*
				if(document.getElementById("adjusted_weight").value.length == 0)
				{
					if(floristMaint.adjusted_weight.selectedIndex == 0)
					floristMaint.adjusted_weight.selectedIndex = 12;
				}
				
				if(!(trim(floristMaint.weightEndDate.value).length > 0 || floristMaint.weightNoEndDate.checked == true))
				{
					document.getElementById("errorWeightEndDate").style.display = "block";
					eCount++;
				}
			*/
			
			}
		}
		


		if(eCount > 0)
			return false;
		else
			return true;

	
}

		]]>
</script>


<tr bordercolor="white">
			<td>
				<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">

					<tr>
						<td  width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>

													</td>
						<td align="right" width="10%"></td>
						<td width="40%">
						&nbsp; &nbsp; &nbsp;
						<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
						<xsl:if test="$goto = 'Y'">
						GoTo Florist
						</xsl:if> &nbsp;
						</td>
					</tr>

					<tr bordercolor="white">
						<td align="right" width="50%"></td>
						<td class="label" align="right" width="10%" id="weightstatuslabel"> Status </td>
                                                <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
					</tr>
					
					<tr></tr>
			
				</table>

			</td>
			</tr>


			<tr>
			<td width="100%" align="center" bordercolor="white">

				<table width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
									<td width="20%">FTD Orders Sent</td>
									<td width="20%"><input type="text" tabindex="-1" name="orders_sent" maxlength="9" readonly="true" value="{floristData/generalData/ordersSent}"/></td>
									<td width="60%"><a href="javascript:doWeightHistoryPopUp();" tabindex="1" name="weightHistoryLink">Weight History</a></td>
							</tr>
							<tr>
									<td width="20%">Initial Weight</td>
									<td width="20%"><input type="text" tabindex="-1" name="initial_weight" maxlength="2" readonly="true" value="{floristData/generalData/initialWeight}"/></td>
									<td width="60%"></td>
							</tr>
							
							<tr>
									<td width="20%">Final Weight</td>
									<td width="20%"><input type="text" tabindex="-1" name="final_weight" maxlength="2" readonly="true" value="{floristData/generalData/finalWeight}"/></td>
									<td width="60%"></td>
							</tr>
							
				</table>
			
			</td>
			</tr>
	
			
			<tr>
				<td class="tblheader" align="left" width="100%">Manage Weights</td>
			</tr>
			
		
			
			<tr>
			<td width="100%" align="center" bordercolor="white">

				<table width="100%" border="0" cellpadding="2" cellspacing="2" onmouseover="endDateAction();">
							<tr>
									<td width="20%">Go To Florist</td>
									<td>
									
										<xsl:variable name="isgo" select="floristData/generalData/gotoFlag"/>
										
										<input type="radio" name="go_to_florist" tabindex="2" value="Y" id="gotoyes" onclick="enSave();">
											<xsl:if test="$isgo = 'Y'">
												<xsl:attribute name="CHECKED" />
											</xsl:if>
											<xsl:if test="key('securityPermissions', 'suspendOverrideUpdate')/value != 'Y'">
												<xsl:attribute name="DISABLED" />
											</xsl:if>
											Yes
										</input>
										&nbsp; &nbsp; &nbsp;
								
										<input type="radio" name="go_to_florist" tabindex="2" value="N" id="gotono" onclick="enSave();">
											<xsl:if test="$isgo = 'N'">
												<xsl:attribute name="CHECKED" />
											</xsl:if>
											<xsl:if test="key('securityPermissions', 'suspendOverrideUpdate')/value != 'Y'">
												<xsl:attribute name="DISABLED" />
											</xsl:if>
											No
										</input>
										
									</td>
							</tr>
							<tr>
									<td width="20%">Adjusted Weight</td>
									<td width="20%">

									
									<select name="adjusted_weight" tabindex="3">
										<option value=""> Select... </option>
										<script><![CDATA[
											for(var i=1; i<=30; i++)
											{
												document.write('<option value=\"' + i + '\" >' + i + '</option>' );
											}
											]]>
										</script>
									</select>
									
									<script><![CDATA[
									if(floristMaint.adjusted_weight.selectedIndex == 0)
									floristMaint.adjusted_weight.selectedIndex = floristMaint.weightadj.value;
									]]></script>
										
									</td>
									<td align="left" width="60%" id="invalidAdjustedWeight" class="RequiredFieldTxt" style="display:none">
																		Required Field
							 	</td>

							</tr>

							<tr>
									<td width="20%">Start Date </td>
									<td  width="20%"><input type="text" name="weightStartDate" tabindex="4" maxlength="10" value="{floristData/generalData/adjustedWeightStartDate}"/>&nbsp; &nbsp;
									<input type="image" tabindex="4" id="calendarStartDate" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
									</td>
									
									<td align="left" id="invalidWeightStartDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
							 	</td>
							 		<td align="left" id="invalidDateWeightStartDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Date
							 	</td>
							 	
							 	
									
									<script><![CDATA[

									 Calendar.setup(
									    {
									      inputField  : "weightStartDate",  // ID of the input field
									      ifFormat    : "mm/dd/y",  // the date format
									      button      : "calendarStartDate" // ID of the button
									    }
									  );

									]]></script>

									<td width="60%"></td>
							</tr>
							
							<tr>
									<td width="20%">End Date</td>
									<td width="20%"><input type="text" name="weightEndDate" tabindex="5" maxlength="10" value="{floristData/generalData/adjustedWeightEndDate}"   onblur="endDateAction();"/> &nbsp; &nbsp;
									<input type="image" tabindex="5" id="calendarEndDate" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
									</td>
									
									<td align="left" id="invalidWeightEndDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
											 	</td>
											 	
							 		<td align="left" id="errorWeightEndDate" class="RequiredFieldTxt" style="display:none">
																		An end date is required.
							 		</td>
							 		
							 		<td align="left" id="invalidDateWeightEndDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Date
							 		</td>
							 		
							 		<td align="left" id="biggerWeightEndDate" class="RequiredFieldTxt" style="display:none">
													End Date must be greater than or equal to the Start Date.
											 	</td>
											 		
											 	
									
									<script><![CDATA[

									 Calendar.setup(
									    {
									      inputField  : "weightEndDate",  // ID of the input field
									      ifFormat    : "mm/dd/y",  // the date format
									      button      : "calendarEndDate" // ID of the button
									    }
									  );

									]]></script>
									
									<td width="60%"></td>
							</tr>

							<tr>
									<td width="20%">No End Date</td>
									<td width="20%" align="left">
									<xsl:variable name="permWeightflag" select="floristData/generalData/adjustedWeightPermFlag"/>
									<input type="checkbox" tabindex="6" onclick="noEndDateAction();" name="weightNoEndDate" >
											<xsl:if test="$permWeightflag = 'Y'">
												<xsl:attribute name="CHECKED" />
											</xsl:if>
									</input>
									</td>
									<td width="60%"></td>
							</tr>
				</table>

			</td>
			</tr>
			
			<tr>
					<td>
						<p align="center">
							<input type="button" align="center" value=" SAVE " name="saveWeight" size="10" tabindex="7" onClick="saveAction();"></input>
						</p>
		
					</td>
			</tr>
		
		</xsl:template>
</xsl:stylesheet>
