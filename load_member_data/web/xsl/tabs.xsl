<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="tabs">

  <table id="itemTabsTable" width="90%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <div id="tabs">
          <!-- Shopping Cart Tab -->
          <script type="text/javascript" language="javascript">
        
          <![CDATA[
          
            document.write('<button id="profile" tabindex="90" content="profilediv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(0);">Profile</button>');
 			document.write('<button id="zipcodes" tabindex="90" content="zipcodesdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(1);">Zip Codes</button>');
			document.write('<button id="cities" tabindex="90" content="citiesdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(2);">Cities</button>');
  			document.write('<button id="codification" tabindex="90" content="codificationdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(3);">Codification</button>');
			document.write('<button id="history" tabindex="90" content="historydiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(4);">History</button>');
			document.write('<button id="association" tabindex="90" content="associationdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(5);">Association</button>');
   			document.write('<button id="weights" tabindex="90" content="weightsdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(6);">Weights</button>');
			document.write('<button id="orders" tabindex="90" content="ordersdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(7);">Orders</button>');
			document.write('<button id="sourcecodes" tabindex="90" content="sourcecodesdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(8);">Source Codes</button>');
		]]>
			
    		var tabsArray = new Array(document.getElementById("profile"));
            tabsArray[1] = document.getElementById("zipcodes");
            tabsArray[2] = document.getElementById("cities");
            tabsArray[3] = document.getElementById("codification");
            tabsArray[4] = document.getElementById("history");
            tabsArray[5] = document.getElementById("association");
            tabsArray[6] = document.getElementById("weights");
            tabsArray[7] = document.getElementById("orders");
            tabsArray[8] = document.getElementById("sourcecodes");
            var _tabCount = 9;


          </script>
        </div>
      </td>
    </tr>
  </table>

  <script type="text/javascript" language="javascript"><![CDATA[

    var TAB_WIDTH = 92;
    var MAX_TABS = 8;
    var minItem = 0;
    var maxItem = (tabsArray.length > MAX_TABS) ? minItem+MAX_TABS : tabsArray.length;
    var itemTabsTableOffset = document.getElementById("itemTabsTable").getBoundingClientRect().left + document.body.scrollLeft;
    var currentTabIndex = 0;
    var currentTab;
    var currentContent;

    function tabMouseOut(tab){
    	if(tab.className != "selected")
        tab.className = "button";
    }

    function tabMouseOver(tab){
    	if(tab.className != "selected")
        tab.className = "hover";
    }

    function showTab(tab){
    	if(currentTab)
      	currentTab.className = "button";

    	tab.className = "selected";
    	currentTab = tab;
    	showTabContent(tab);
    }

    function showTabContent(tab){
    	if(currentContent)
      	currentContent.className = "hidden";

    	currentContent = document.getElementById(tab.content);
    	
    	currentContent.className = "visible";
    	
    	if(tab.content == "profilediv")
    	{
    		if(floristMaint.memnum.readOnly == false)
	    		floristMaint.memnum.focus();
	    	else if(floristMaint.name.readOnly == false)
	    		floristMaint.name.focus();
	    	else
	    		floristMaint.profileExit2.focus();

    		document.getElementById("profile").tabIndex = -1;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = 90;
			document.getElementById("sourcecodes").tabIndex = 90;
    	}
    	else if(tab.content == "zipcodesdiv")
    	{
    		document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = -1;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = 90;
			document.getElementById("sourcecodes").tabIndex = 90;
    	
    	}
    	else if(tab.content == "codificationdiv")
    	{
    		//if(document.getElementById("codNewBlockType").disabled == false)
    			//document.getElementById("codNewBlockType").focus();
    		//if(floristMaint.newBlockStartDate.readOnly == false)
	    		//floristMaint.newBlockStartDate.focus();

   			document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = -1;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = 90;
 			document.getElementById("sourcecodes").tabIndex = 90;
    	}
    	else if(tab.content == "historydiv")
    	{
    		if(floristMaint.histadd.disabled == false)
	    		floristMaint.histadd.focus();
	  
	    		
   			document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = -1;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = 90;
			document.getElementById("sourcecodes").tabIndex = 90;
    	}
    	else if(tab.content == "associationdiv")
    	{
    		if(document.getElementById("ascFlor1") != null)
    			document.getElementById("ascFlor1").focus();
    		else if(document.getElementById("saveasn").enabled)
    			document.getElementById("saveasn").focus();
    		else
    			document.getElementById("ascexit").focus();

			document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = -1;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = 90;
			document.getElementById("sourcecodes").tabIndex = 90;
    	}
    	else if(tab.content == "weightsdiv")
    	{
    		document.getElementById("weightHistoryLink").focus();
   			document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = -1;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = 90;
			document.getElementById("sourcecodes").tabIndex = 90;
    	}
    	else if(tab.content == "ordersdiv")
    	{

    		if(floristMaint.loadOrdersSearch.value == "Yes")
    		{
    			document.getElementById("orderexit").focus();
   			}
    		else
    		{
    			document.getElementById("orderStartDate").focus();
			}    			
			
			document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = -1;
			document.getElementById("cities").tabIndex = 90;
			document.getElementById("sourcecodes").tabIndex = 90;
    	}	
    	else if(tab.content == "citiesdiv")
    	{
   			document.getElementById("profile").tabIndex = 90;
			document.getElementById("zipcodes").tabIndex = 90;
			document.getElementById("codification").tabIndex = 90;
			document.getElementById("history").tabIndex = 90;
			document.getElementById("association").tabIndex = 90;
			document.getElementById("weights").tabIndex = 90;
			document.getElementById("orders").tabIndex = 90;
			document.getElementById("cities").tabIndex = -1;
			document.getElementById("sourcecodes").tabIndex = 90;
    	}
      else if(tab.content == "sourcecodesdiv")
      {
        document.getElementById("sourcecodes").focus();
        
        document.getElementById("profile").tabIndex = 90;
        document.getElementById("zipcodes").tabIndex = 90;
        document.getElementById("codification").tabIndex = 90;
        document.getElementById("history").tabIndex = 90;
        document.getElementById("association").tabIndex = 90;
        document.getElementById("weights").tabIndex = 90;
        document.getElementById("orders").tabIndex = 90;
        document.getElementById("cities").tabIndex = 90;
        document.getElementById("sourcecodes").tabIndex = -1;    	
      }  

    }

    function initializeTabs(tabToDisplay){
    	currentTab = document.getElementById(tabToDisplay);
    	currentTab.className = "selected";
    	currentContent = document.getElementById(currentTab.content);
    	currentContent.className = "visible";
      	displayItems();
      	
      	var toDisp;
		if(floristMaint.isNew.value == "Y")
			toDisp = "Add New";
		else
			toDisp = "Florist Maintenance"
	
	
      	if(floristMaint.endTab.value == "profile")
      		currentTabIndex = 0;
      	else if(floristMaint.endTab.value == "zipcodes")
      		currentTabIndex = 1;
      	else if(floristMaint.endTab.value == "cities")
      		currentTabIndex = 2;
      	else if(floristMaint.endTab.value =="codification")
      		currentTabIndex = 3;
      	else if(floristMaint.endTab.value == "history")
      		currentTabIndex = 4;
      	else if(floristMaint.endTab.value == "association")
      		currentTabIndex = 5;
      	else if(floristMaint.endTab.value == "weights")
      		currentTabIndex = 6;
      	else if(floristMaint.endTab.value == "orders")
      		currentTabIndex = 7;
        else if(floristMaint.endTab.value == "sourcecodes")
          currentTabIndex = 8;

      		
		if(currentTabIndex == 0)
		{
			document.getElementById("headText").firstChild.data = toDisp + " - Profile";
			if(floristMaint.memnum.readOnly == false)
	    		floristMaint.memnum.focus();
	    	else if(floristMaint.name.readOnly == false)
	    		floristMaint.name.focus();
	    	else
	    		floristMaint.profileExit2.focus();
		}
		else if(currentTabIndex == 1)
		{
			document.getElementById("headText").firstChild.data = toDisp + " - Zip Codes";

			if(requestType != "zipCodesDirectoryCity")
                        {
                            if(!floristMaint.newzipcodes.disabled)
                            {
				floristMaint.newzipcodes.focus();
                            }
                            else {
                                floristMaint.zipExit2.focus();
                            }
			}
                        else
                        {
                            floristMaint.newcity.focus();
                        }
				
		}
		else if(currentTabIndex == 2)
			document.getElementById("headText").firstChild.data = toDisp + " - Cities";
		else if(currentTabIndex == 3)
			document.getElementById("headText").firstChild.data = toDisp + " - Codification";
		else if(currentTabIndex == 4)
			document.getElementById("headText").firstChild.data = toDisp + " - History";
		else if(currentTabIndex == 5)
			document.getElementById("headText").firstChild.data = toDisp + " - Association";
		else if(currentTabIndex == 6)
			document.getElementById("headText").firstChild.data = toDisp + " - Weights";
		else if(currentTabIndex == 7)
			document.getElementById("headText").firstChild.data = toDisp + " - Orders";
    else if(currentTabIndex == 8)
      document.getElementById("headText").firstChild.data = toDisp + " - Source Codes";
 	
      //checkPrevNextStatus();
    }
  
    function displayItems() {
      for (var i = minItem, k = 0; i < maxItem; i++, k++){
        tabsArray[i].style.left = (k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset;
        tabsArray[i].style.visibility = "visible";
      }
      //showHideMoreTab( ((k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset) , "visible");
    }

    function hideItems() {
      for (var i = minItem; i < maxItem; i++){
        tabsArray[i].style.left = 0;
        tabsArray[i].style.visibility = "hidden";
      }
      //showHideMoreTab(0, "hidden");
    }

    function showHideMoreTab(offset, visibility){
      var more = getStyleObject("moreItemsTab");
      more.left = offset;
      more.visibility = visibility;
    }

    function showNextItem(){
      currentTabIndex++;
      showItemAt(currentTabIndex);
    }

    function showPrevItem(){
      currentTabIndex--;
      showItemAt(currentTabIndex);
    }

    function showItemAt(index){
	
	// validate...

	if(currentTabIndex == 0 )
	{
	resetProfileErrors();
		if(!validateProfile())
			return;
	}
	else if(currentTabIndex == 1)
	{
                if(requestType != "zipCodesDirectoryCity" && (!validateZipCode()))
		{
			return;
		}
		
	}
	else if(currentTabIndex == 3)
	{
		if(!validateCodification())
			return;
	}
	else if(currentTabIndex == 6)
	{
		if(!validateWeights())
			return;
	}
	
	var now = floristMaint.endTab.value;

   currentTabIndex = index;
   	
      if(currentTabIndex == 0)
      	floristMaint.endTab.value = "profile";
      else if(currentTabIndex == 1)
      	floristMaint.endTab.value = "zipcodes";
      else if(currentTabIndex == 2)
      	floristMaint.endTab.value = "cities";
      else if(currentTabIndex == 3)
      	floristMaint.endTab.value = "codification";
      else if(currentTabIndex == 4)
      	floristMaint.endTab.value = "history";
      else if(currentTabIndex == 5)
      	floristMaint.endTab.value = "association";
      else if(currentTabIndex == 6)
      	floristMaint.endTab.value = "weights";
      else if(currentTabIndex == 7)
      	floristMaint.endTab.value = "orders";
		  else if(currentTabIndex == 8)
        floristMaint.endTab.value = "sourcecodes";

	   if(now == "profile")
	   {
		   if(floristMaint.idvalneeded.value == "yes"  || floristMaint.zipvalneeded.value == "yes")
		   {
	   		validateProServer();
		   }
	   }
       
	   if(now == "zipcodes")
	   {
	   if(isZipPopNeeded())
		{
			doUnblockZipPopUp()
		}
	   }

	   if(now == "cities")
	   {
	   if(isCityPopNeeded())
		{
			doUnblockCityPopUp()
		}
	   }

	
      //checkPrevNextStatus();
		
		floristMaint.secondaryAction.value = "";
		
		showTab(tabsArray[currentTabIndex]);
		
		var toDisp;
		if(floristMaint.isNew.value == "Y")
			toDisp = "Add New";
		else
			toDisp = "Florist Maintenance"

		if(currentTabIndex == 0)
			document.getElementById("headText").firstChild.data = toDisp + " - Profile";
		else if(currentTabIndex == 1)
		{
			document.getElementById("headText").firstChild.data = toDisp + " - Zip Codes";

			if(requestType != "zipCodesDirectoryCity")
                        {
                            if(!floristMaint.newzipcodes.disabled)
                            {
				floristMaint.newzipcodes.focus();
                            }
                            else {
                                floristMaint.zipExit2.focus();
                            }
			}
                        else
                        {
                            floristMaint.newcity.focus();
                        }

		}
		else if(currentTabIndex == 2)
			document.getElementById("headText").firstChild.data = toDisp + " - Cities";
		else if(currentTabIndex == 3)
			document.getElementById("headText").firstChild.data = toDisp + " - Codification";
		else if(currentTabIndex == 4)
			document.getElementById("headText").firstChild.data = toDisp + " - History";
		else if(currentTabIndex == 5)
			document.getElementById("headText").firstChild.data = toDisp + " - Association";
		else if(currentTabIndex == 6)
			document.getElementById("headText").firstChild.data = toDisp + " - Weights";
		else if(currentTabIndex == 7)
			document.getElementById("headText").firstChild.data = toDisp + " - Orders";
    else if(currentTabIndex == 8)
      document.getElementById("headText").firstChild.data = toDisp + " - Source Codes";

    }

    ]]>
  </script>
</xsl:template>

</xsl:stylesheet>
