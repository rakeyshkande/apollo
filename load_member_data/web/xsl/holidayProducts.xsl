
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- External Templates -->
  <xsl:import href="footer.xsl"/>

  <xsl:param name="web-context-root"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:param name="adminAction"/>
  <xsl:param name="confirmation"/>
  <xsl:output method="html" indent="yes"/>
   <!-- Root template -->
   <xsl:template match="/">
      <html>
        <head>
          <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252"></meta>
          <title>Holiday Products Maintenance</title>
          <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
          <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
          <link rel="stylesheet" type="text/css" href="../css/calendar.css" />
          <script type="text/javascript" src="../js/calendar.js" />
          <script type="text/javascript" src="../js/clock.js"/>
          <script type="text/javascript" src="../js/FormChek.js"/>
        </head>
        <body>
        
        <script language="javascript">        
          function doShowDetail(deliveryDate) {
              var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
              var url = webContextRoot + '/servlet/HolidayProductsServlet?action=loadDetail';
              document.getElementById('delivery_date').value = deliveryDate;
              form1.action = url;
              form1.submit();
          }
          
          <![CDATA[
          function addNew() {
              if (document.getElementById('popupNewDate').value == '') {
                  alert('Date required');
                  return;
              }
              var today = new Date();
              newDate = document.getElementById('popupNewDate').value;
              dateArray = newDate.split("/");
              var isValid = true;
			  if(dateArray.length != 3) {
				  isValid = false;
			  } else {
				  if(dateArray[2] != null && dateArray[2].length == 2) {
					  dateArray[2] = "20" + dateArray[2];
					  newDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				  }
				
			          if(dateArray[2] > today.getYear())
				          isValid = true;
			          else if(dateArray[2] < today.getYear())
				          isValid = false;
			          else {
                          if(dateArray[0] > today.getMonth()+1)
					          isValid = true;
				          else if(dateArray[0] < today.getMonth()+1)
					          isValid = false;
				          else {
					          if(dateArray[1] >= today.getDate())
						          isValid = true;
					          else if(dateArray[1] < today.getDate())
						          isValid = false;
					          else
						          isValid = false;
				          }
			      }
			  }
              if (!isValid) {
                  alert('Invalid Date');
                  return;
              }
              doShowDetail(newDate);
          }
          ]]>
          
        </script>
          <!-- Header Template -->
          <!-- <xsl:call-template name="addHeader"/> -->      
 
          <form name="form1" method="post">
            <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
            <input type="hidden" name="context" value="{$context}"/>
            <input type="hidden" name="adminAction" value="{$adminAction}"/>
            <input type="hidden" name="delivery_date" value=""/>
            
            <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		      <tr>
		        <td width="20%" align="left"><img border="0" src="../images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
		        <td width="60%" align="center" colspan="1" class="header" id="headText">Holiday Products Maintenance</td>
		        <td width="20%" align="right" class="label">
		          <table width="100%" cellspacing="0" cellpadding="0">
		              <tr>
		                <td id="time" align="right" class="label"></td>
		                <script type="text/javascript">startClock();</script>
		              </tr>
		              <tr>
		                <td align="right">
		                  <input type="button" tabindex="99" name="exitButtonHeader" value="Exit" onclick="document.forms[0].action = 'FloristSearchServlet?action=exitSystem'; document.forms[0].submit();"/>
		                </td>
		              </tr>
		          </table>
		        </td>
		      </tr>
		      <tr>
		        <td colspan="3"><hr/></td>
		      </tr>

              <tr>
                <td colspan="3" align="center"> 
                  <table cellspacing="2" cellpadding="3" border="0" width="75%" align="center">
                    <tr>
                      <td width="50%" class="colHeaderCenter">Delivery Date</td>
                      <td width="50%" class="colHeaderCenter"># of Products</td> 
                    </tr>
                    <xsl:for-each select="root/holidayProducts/holidayProduct">
                      <tr>
                        <td align="center">
                          <a href="javascript:doShowDetail('{delivery_date}');" tabindex="-1">
                            <xsl:value-of select="delivery_date"/>
                          </a>
                        </td>
                        <td align="center"><xsl:value-of select="product_count"/></td>
                      </tr>
                    </xsl:for-each>
                
		            <tr>
    		          <td colspan="2"><hr/></td>
		            </tr>
                
                    <tr>
                      <td colspan="2" align="center">
                        <input name="popupNewDate" type="text" />
                        &nbsp;&nbsp;
                        <img id="calendar" src="../images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
                        &nbsp;&nbsp;
                        <input type="button" align="center" value="New Delivery Date" size="10" tabindex="9" onClick="addNew();"></input>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

		      <tr>
		        <td colspan="3"><hr/></td>
		      </tr>
                
              <tr>
                <td width="100%" align="center" colspan="4">
                  <xsl:call-template name="footer"/>
                </td>
              </tr>
                
            </table>
          </form>
        </body>
      </html>   
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "popupNewDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar"  // ID of the button
    }
  );
</script>

   </xsl:template>
</xsl:stylesheet>