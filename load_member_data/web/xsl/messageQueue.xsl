<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="messageQueue">
<xsl:variable name="codifiedId" select="viewQueueData/generalData/productCodifiedId" />


<tr><td>

				<table width="100%" >

					<tr>

						<td width="20%" align="right" class="label">Order Number</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/orderNumber}"/>
						</td>

						<td width="10%" >
						</td>
						<td width="10%" align="center">
						
						<button style="font-size: 8pt;" tabindex="91" name="mRemove1" onclick="javascript:doRemoveAction();">
						<span style="text-decoration:underline;">R</span>emove</button>&nbsp;&nbsp;

						<button style="font-size: 8pt;" tabindex="92" onclick="javascript:doNextAction();">&nbsp;&nbsp;
						<span style="text-decoration:underline;">N</span>ext&nbsp;&nbsp;</button>&nbsp;&nbsp;
						
						
						</td>

						<td width="10%" align="right" class="label">Count &nbsp;</td>
						<td width="10%" align="left">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/messageCount}"/>
						</td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
					</tr>

					<tr><td><br></br></td></tr>



					<tr>
						<td  colspan="9" class="tblheader" align="left"> Mercury Message </td>
					</tr>

					<tr><td><br></br></td></tr>

					<tr>

						<td width="20%" align="right" class="label">Delivery Date</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/deliveryDate}"/>
						</td>

						<td width="10%" align="right" class="label">Message Date / Time</td>
						<td width="10%" align="left">

						<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/messageDate}"/></td>

						
						<td width="10%" align="right" class="label"></td>
						
						
						<td width="10%" align="right"></td>
						
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
					</tr>

					<tr>
						<td width="20%" align="right" class="label">Mercury #</td>
						<td>
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/mercuryNumber}"/>
						</td>
					</tr>

					<tr>
						<td width="20%" align="right" class="label">Reason</td>
						<td colspan="4">
						<textarea tabindex="-1" readonly="true" name="newzipcodes" wrap="physical" cols="100" rows="3" >
						<xsl:value-of select="viewQueueData/generalData/messageReason"/>
					 	</textarea>
						</td>

					</tr>

					<tr><td><br></br></td></tr>

					<tr>
						<td  colspan="9" class="tblheader" align="left"> Florist Information </td>
					</tr>

					<tr><td><br></br></td></tr>


					<tr>
						<td width="20%" align="right" class="label">Florist #</td>
						<td width="10%"><input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristNumber}"/></td>

						<td width="10%" align="right" class="label"></td>
						<td width="10%" align="left"></td>

						<td width="10%" align="right" class="label"></td>
						<td width="10%" align="left"></td>

						<td width="10%"></td>
						<td width="10%">

						</td>
						<td></td>
						</tr>
					
					<tr>
						<td width="20%" align="right" class="label">Florist Name</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristName}"/>
						</td>

						<td width="10%" align="right" class="label">GoTo</td>
						<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristGotoFlag}"/></td>

						<td width="10%" align="right" class="label">Status</td>
						<td width="10%" align="left" colspan="3" rowspan="2">
						    <textarea tabindex="-1" readonly="true" wrap="physical" cols="75" rows="3" >
    	    					<xsl:value-of select="viewQueueData/generalData/floristStatus"/>
	    				 	</textarea>
						</td>
						<td></td>
						</tr>

				<tr >
						<td width="20%" align="right" class="label">City</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristCity}"/>
						</td>

						<td width="10%" align="right" class="label">State</td>
						<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristState}"/></td>

				</tr>

				

		

			<tr><td><br></br></td></tr>

					<tr>
						<td  colspan="9" class="tblheader" align="left"> Product Information </td>
					</tr>

					<tr><td><br></br></td></tr>

					<tr>

						<td width="20%" align="right" class="label">Product Id</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productId}"/>
						</td>
						
						<td width="10%" align="right" class="label">Blocked</td>
						<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productBlockFlag}"/></td>

						<td width="10%" align="right" class="label">Unblock Date</td>
						<xsl:choose>
						<xsl:when test="viewQueueData/generalData/productUnblockDate = ''">
							<td width="10%" align="left">
								<input type="text" readonly="true" tabindex="-1" value="N/A"/>
							</td>
	      				</xsl:when>
						<xsl:otherwise>
							<td width="10%" align="left">
								<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productUnblockDate}"/>
							</td>
	      				</xsl:otherwise>
						</xsl:choose>
						
					</tr>

					<tr>

						<td width="20%" align="right" class="label">Product Name</td>
						<td colspan="3">
							<input type="text" size="74" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productName}"/>
						</td>

						<td width="10%" align="right" class="label">Codified</td>

						<td width="10%" align="left">
							<xsl:choose>
							<xsl:when test="viewQueueData/generalData/productCodifiedId = ''">
								<input type="text" readonly="true" tabindex="-1" value="N/A"/>
		      				</xsl:when>
							<xsl:otherwise>
							
								<xsl:choose>
								<xsl:when test="viewQueueData/codifiedProductList/codifiedProduct[id = $codifiedId] != ''">
									<input type="text" readonly="true" tabindex="-1" value="Y"/>
			      				</xsl:when>
								<xsl:otherwise>
									<input type="text" readonly="true" tabindex="-1" value="N"/>
			      				</xsl:otherwise>
								</xsl:choose>	
							
		      				</xsl:otherwise>
							</xsl:choose>	
						</td>

					</tr>

					<tr><td><br></br></td></tr>


					<tr>
						<td  colspan="9" class="tblheader" align="left"> Zip / Postal Code Information </td>
					</tr>

					<tr><td><br></br></td></tr>

					<tr>
						<td width="20%" align="right" class="label">Delivery Address</td>
						<td width="10%" colspan="3">
							<input size="74" type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/deliveryAddress}"/>
						</td>

						

						<td width="10%" align="right" class="label"># Florists in Zip</td>
						<td width="10%" align="left">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/zipFloristCount}"/>
						</td>
					</tr>

				<tr>
						<td width="20%" align="right" class="label">Blocked</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/zipBlockFlag}"/>
						</td>

						<td width="10%" align="right" class="label">Unblock Date</td>						
						<xsl:choose>
						<xsl:when test="viewQueueData/generalData/zipUnblockDate = ''">
							<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="N/A"/></td>
	      				</xsl:when>
						<xsl:otherwise>
							<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/zipUnblockDate}"/></td>
	      				</xsl:otherwise>
						</xsl:choose>	
				</tr>

				<tr><td><br></br></td></tr>

					<tr>

						<td colspan="9" align="center">
						
						<button style="font-size: 8pt;" name="mRemove2" onclick="javascript:doRemoveAction();" tabindex="101">
						<span style="text-decoration:underline;">R</span>emove</button>&nbsp;&nbsp;

						<button style="font-size: 8pt;"  onclick="javascript:doNextAction();" tabindex="102">&nbsp;&nbsp;
						<span style="text-decoration:underline;">N</span>ext&nbsp;&nbsp;</button>&nbsp;&nbsp;
						
						
						</td>

					</tr>

			</table>
			
</td></tr>

				

	</xsl:template>
</xsl:stylesheet>