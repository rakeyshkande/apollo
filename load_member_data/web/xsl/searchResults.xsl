
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	
	<!-- security parameters -->
	<xsl:param name="web-context-root"/>
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="adminAction"/>
	
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name" />
	<xsl:key name="securityPermissions" match="/root/securityPermissions/permission" use="name" />
	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Florist Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<script type="text/javascript" language="javascript" src="../js/util.js"/>
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
				<script type="text/javascript" language="javascript">

        var usStates = new Array(
					<xsl:for-each select="stateList/state[countrycode='']">
     				["<xsl:value-of select="statemasterid"/>", "<xsl:value-of select="statename"/>"]
						<xsl:choose>
							<xsl:when test="position()!=last()">,</xsl:when>
						</xsl:choose>
					</xsl:for-each>);

<![CDATA[

	function doSearchAction()
    {
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;
      performAction(document.forms[0].action);
    }

	function viewProfile(inValue)
	{
		document.forms[0].floristNumber.value = inValue;
		document.forms[0].action = "FloristMaintenanceServlet";
		showWaitMessage("outerContent", "wait", "Processing");
	 	document.forms[0].submit();
 	}

    function doExitAction()
    {
    	
		document.forms[0].action = "FloristSearchServlet?action=loadFloristSearch";
		showWaitMessage("outerContent", "wait", "Processing");
	 	document.forms[0].submit();
    }

    function init()
    {
	   	setNavigationHandlers();
	    setScrollingDivHeight();
	    window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight()
    {
		var searchResultsDiv = document.getElementById("searchResults");
		var newHeight = document.body.clientHeight - searchResultsDiv.getBoundingClientRect().top - 100;
		if (newHeight > 15)
		{
			searchResultsDiv.style.height = newHeight;
		}
    }

]]>
				</script>
			</head>
			<body onload="javascript:init();">
				<form name="SearchResultsForm" method="post" action="">
					<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
					<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
					<input type="hidden" name="context" value="{$context}"/>
					<input type="hidden" name="adminAction" value="{$adminAction}"/>
					
					<input type="hidden" name="servlet_action"/>
					<input type="hidden" name="floristNumber"/>
					<input type="hidden" name="sc_floristnum" value="{key('searchCriteria', 'sc_floristnum')/value}"/>
					<input type="hidden" name="sc_zipcode" value="{key('searchCriteria', 'sc_zipcode')/value}"/>
					<input type="hidden" name="sc_city" value="{key('searchCriteria', 'sc_city')/value}"/>
					<input type="hidden" name="sc_state" value="{key('searchCriteria', 'sc_state')/value}"/>
					<input type="hidden" name="sc_shopname" value="{key('searchCriteria', 'sc_shopname')/value}"/>
					<input type="hidden" name="sc_phonenumber" value="{key('searchCriteria', 'sc_phonenumber')/value}"/>
					<input type="hidden" name="sc_statuslist" value="{key('searchCriteria', 'sc_statuslist')/value}"/>
					<input type="hidden" name="sc_product" value="{key('searchCriteria', 'sc_product')/value}"/>
					<input type="hidden" name="sc_productType" value="{key('searchCriteria', 'sc_productType')/value}"/>
					<input type="hidden" name="sc_includeVendor" value="{key('searchCriteria', 'sc_includeVendor')/value}"/>
					<input type="hidden" name="uid" value="{key('searchCriteria', 'uid')/value}"/>
					<input type="hidden" name="count" value="{key('searchCriteria', 'count')/value}"/>
					<!-- Header Template -->
					<!-- <xsl:call-template name="addHeader"/> -->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Florist Maintenance - Search Results'" />
						<xsl:with-param name="showExitButton" select="true()"/>
						<xsl:with-param name="showTime" select="true()"/>
					</xsl:call-template>
					<!-- Outter div needed to hold any content that will be blocked when an action is performed -->
					<div id="outerContent">
						<br></br>
						<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
							<tr>
								<td>
									<!--  Search Results column headers -->
									<table width="100%" border="0" cellpadding="2" cellspacing="2">
										<tr>
											<td colspan="9" width="100%" align="center">
												<!--  Scrolling div contiains search results -->
												<div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">
													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
															<td width="10%" class="colHeaderCenter">Member #</td>
															<td width="25%" class="colHeaderCenter">Name</td>
															<td width="10%" class="colHeaderCenter">Status</td>
															<td width="5%" class="colHeaderCenter">Zip/Postal</td>
															
															<td width="15%" class="colHeaderCenter">City,State</td>
															<td width="15%" class="colHeaderCenter">Phone</td>
															<td width="5%" class="colHeaderCenter"> Weight</td>
															<td width="5%" class="colHeaderCenter">Sunday</td>
															<td width="5%" class="colHeaderCenter">Mercury</td>
															<td width="5%" class="colHeaderCenter">GoTo</td>
														</tr>

														<xsl:for-each select="search_result_list/search_result">
															<tr>
																<td width="10%" align="center">
																	<a tabindex="1" class="link" href="javascript:viewProfile('{flornum}')">
																		<xsl:value-of select="flornum"/>
																	</a>
																</td>
																<td width="25%" align="left">
																	<xsl:value-of select="name" />
																</td>
																<td width="10%" align="center">
																	<xsl:value-of select="status" />
																</td>
																
																<td width="5%" align="center">
																	<xsl:value-of select="zip" />
																</td>
																<td width="15%" align="left">
																	<xsl:value-of select="city" />,
																	<xsl:value-of select="state" /> 
																</td>
																<td width="15%" align="center">
																	<xsl:variable name="st" select="phone" />
																	<script>
 																		var s = '<xsl:value-of select="$st"/>';
																		s = s + "";
	    																var resultString = "";

	    																if(s.length == 10)
	    																{
																			resultString = s.substring(0,3) + "-" + s.substring(3,6) + "-" + s.substring(6,10);
	 																	}
	 																	else
	 																	{
	 																		resultString = s;
	 																	}
	 																	
	 																	document.write(resultString);
																	</script>
																</td>
																<td width="5%" align="center" id="wt{position()}">
															<xsl:choose>
																<xsl:when test="key('securityPermissions', 'overrideWeightView')/value = 'Y'">
																	<xsl:choose>
																	<xsl:when test="string-length(adjustedWeight) &gt; 0">
																	
																		 <xsl:value-of select="adjustedWeight"/>
																	  </xsl:when>
																	  <xsl:otherwise>
																		  <xsl:value-of select="finalWeight"/>
																	  </xsl:otherwise>
																	</xsl:choose>
																</xsl:when>
																<xsl:otherwise>
																<script>
																
																var wgt = '<xsl:value-of select="finalWeight"/>';
																if(wgt.length > 0)
																{
																if(wgt > 12)
																{
																	document.write("12");
																}
																else
																	document.write(wgt);
																}
																</script>
																</xsl:otherwise>
															</xsl:choose>
																
																</td>
																<td width="5%" align="center">
																	<xsl:value-of select="sunday" />
																</td>
																<td width="5%" align="center">
																	<xsl:value-of select="mercury" />
																</td>
																
																
																<td width="5%" align="center">
																	<xsl:value-of select="go" />
																</td>
															</tr>
														</xsl:for-each>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br></br>
						<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
							<tr>
								<td align="right">
									<div align="right" class="button">
										<input type="button" tabindex="2" value="Exit" onclick="javascript:doExitAction();"></input>
									</div>
								</td>
							</tr>
						</table>
					</div>
					
					<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage" />
										<td id="waitTD" width="50%" class="waitMessage" />
									</tr>
								</table></td>
						</tr>
					</table>
				</div>
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>