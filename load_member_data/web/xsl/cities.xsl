<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="cities">
	<script type="text/javascript" language="javascript">
	
	<![CDATA[

var EXIT = "_EXIT_";

function checkAllCitiesActive() {
    for(var i=0; i<allowedCities.length; i++) {
        if (document.getElementById("cityNo" + (i+1)) != null) {
            document.getElementById("cityNo" + (i+1)).checked = true;
            document.getElementById("cityYes" + (i+1)).checked = false;
        }
    }
    enSave();
    document.forms[0].saveNeeded.value = "yes";
    document.getElementById("blockAllCities").value = "no";
}

function checkAllCitiesBlock() {
    for(var i=0; i<allowedCities.length; i++) {
        if (document.getElementById("CityNo" + (i+1)) != null) {
            document.getElementById("cityNo" + (i+1)).checked = false;
            document.getElementById("cityYes" + (i+1)).checked = true;
        }
    }
    enSave();
    document.forms[0].saveNeeded.value = "yes";
    document.getElementById("blockAllCities").value = "yes";
}

]]>
</script>
<input type="hidden" name="blockAllCities" value="no"/>

									<tr>
										<td>
											<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
											<tr>
												<td width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>
												</td>
												<td width="10%"/>
												<td width="40%">
													&nbsp; &nbsp; &nbsp;
													<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
													<xsl:if test="$goto = 'Y'">
													GoTo Florist
													</xsl:if> &nbsp;
												</td>
											</tr>
											
											<tr>
												<td width="50%"/>
												<td class="label" align="right" width="10%" id="zipstatuslabel"> Status </td>
                                                <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
											</tr>
											<tr>
											
												
												<td align="center" colspan="5">
													<input type="button" align="center" value="SAVE" name="savecity1" size="10" tabindex="102" onClick="saveAction();"></input>
												</td>
											</tr>
												
												
											</table>
										</td>
									</tr>
									<tr>
										<td class="tblheader" align="left">
									 	Manage Cities 
									 	 </td>
									</tr>
											
									<tr>

									<td>
									<table width="100%" border="0">
										<tr>
											<td colspan="9" width="100%" align="center">


													<table width="100%" border="0" cellpadding="2" cellspacing="0">
														<tr>

															<td width="10%" class="colHeaderCenter">Member #</td>
															<td width="15%" class="colHeaderCenter">Name</td>
															<td width="15%" class="colHeaderCenter">City</td>
															<td width="7%" class="colHeaderCenter">State/Pro</td>
															<td width="9%" class="colHeaderCenter">Florist Type</td>
															<td width="9%" class="colHeaderCenter"># of Florists</td>
															<td width="10%" class="colHeaderCenter">
															    Active
																<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value = 'Y'">
															        &nbsp;&nbsp;<a href="javascript:checkAllCitiesActive()"><img src="../images/all_check_blue.gif" border="0" align="absmiddle"/></a>
																</xsl:if>
															</td>
															<td width="10%" class="colHeaderCenter">
															    Block
																<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value = 'Y'">
															        &nbsp;&nbsp;<a href="javascript:checkAllCitiesBlock()"><img src="../images/all_check_blue.gif" border="0" align="absmiddle"/></a>
															    </xsl:if>
															</td>
															<td width="15%" class="colHeaderLeft">
															    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															    Unblock Date
															</td>

														</tr>

														<tr>
															<td>
															<input type="hidden" name="totalcitycodes" value="{count(floristData/cityList/city/floristId)}"></input>
															<input type="hidden" name="newcityunblockdate" value="" />
															</td>
														</tr>

                                                        <script>
                                                        var allowedCities = new Array();
                                                        var cnt = 0;
                                                        </script>
                                                        <xsl:for-each select="floristData/cityList/city">
															<xsl:sort select="sort1"/>
															<xsl:sort select="sort2"/>
															<xsl:sort select="floristId"/>
															
                                                            <script>
                                                            allowedCities[cnt++] = "<xsl:value-of select="city"/>";
                                                            </script>
                                                            <input name="cityfloristid{position()}" type="hidden" value="{floristId}"/>

															<xsl:variable name="bgcolor">
																<xsl:choose>
																	<xsl:when test="floristType = 'R'">#DDDDDD</xsl:when>
																	<xsl:otherwise>#FFFFFF</xsl:otherwise>
																</xsl:choose>
															</xsl:variable>

															<tr id="city_row{position()}" style="display:block;background-color:{$bgcolor}">
															
																<td align="center">
																    <xsl:value-of select="floristId" />
																</td>
																<td align="center">
																    <xsl:value-of select="floristName" />
																</td>
																<td align="center">
																    <xsl:value-of select="city" />
																</td>
																<td align="center">
																    <xsl:value-of select="state" />
																</td>
																<td align="center">
																    <xsl:value-of select="floristType" />
																</td>
																<td align='center'>
																<u>
																<a href="javascript:doShowFloristCityPopUp('{floristId}', '{city}');" id="cityhref{position()}" tabindex="-1">
																<xsl:value-of select="floristCount" /></a>
																</u>

																</td>

																<xsl:variable name="cityStat" select="blockedFlag" />

																<td align="center">
																	<input type="radio" value="N" name="cityblock{position()}" tabindex="-1" id="cityNo{position()}" onclick="enSave();">
																		<xsl:if test="$cityStat = 'N'">
																			<xsl:attribute name="CHECKED" />
																		</xsl:if>
																		<xsl:if test="$cityStat = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																			<xsl:attribute name="DISABLED" />
																		</xsl:if>
																	</input>
																</td>

																<td align="center">
																	<input type="radio" value="Y" name="cityblock{position()}"  tabindex="-1" id="cityYes{position()}" onclick="enSave();">
																		<xsl:if test="$cityStat = 'Y'">
																			<xsl:attribute name="CHECKED" />
																		</xsl:if>
																		<xsl:if test="$cityStat = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																			<xsl:attribute name="DISABLED" />
																		</xsl:if>
																	</input>
																</td>
																
																<td align="left">
																<input type="text" tabindex="-1" readonly="true" value="{blockEndDate}" name="cityunblockdate{position()}" size="10"/>
																<xsl:choose>
																<xsl:when test="$cityStat = 'Y'">
																    &nbsp;
																	<input type="image" tabindex="-1" id="citycalendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
																		<xsl:if test="key('securityPermissions', 'zipCodesUpdate')/value != 'Y'">
																			<xsl:attribute name="DISABLED" />
																		</xsl:if>
																		<xsl:if test="$cityStat = 'Y' and blockEndDate = '' and key('securityPermissions', 'fulfillmentUpdate')/value != 'Y'">
																			<xsl:attribute name="DISABLED" />
																		</xsl:if>
																	</input>
																	<xsl:variable name="unblockcitypos" select="position()"/>

																	<script>
																		var calCityCount = <xsl:value-of select="$unblockcitypos"/>;
																		calCityCount = calCityCount + "";
																	</script>
																	<script><![CDATA[

																	 Calendar.setup(
																	    {
																	      inputField  : "cityunblockdate"+calCityCount,  // ID of the input field
																	      ifFormat    : "mm/dd/y",  // the date format
													      				  button      : "citycalendar"+calCityCount  // ID of the button
													    				}
													  				);
																	]]></script>
																	<xsl:choose>
																	    <xsl:when test="blockEndDate = ''">
																	        <input type="hidden" name="citypermanentblockflag{position()}" value="Y"/>
																	    </xsl:when>
																	    <xsl:otherwise>
																	        <input type="hidden" name="citypermanentblockflag{position()}" value="N"/>
																	    </xsl:otherwise>
																	</xsl:choose>
																</xsl:when>
																<xsl:otherwise>
																    <input type="hidden" name="citypermanentblockflag{position()}" value="N"/>
																</xsl:otherwise>
																</xsl:choose>

																</td>

															</tr>
															
															
                                                        </xsl:for-each>
													</table>
											</td>
										</tr>
										
									
									</table>

									</td>
									</tr>
									
									<tr><td align="center"><input type="button" align="center" value="SAVE" name="savecity2" size="10" tabindex="9" onClick="saveAction();"></input></td></tr>

	</xsl:template>
</xsl:stylesheet>
