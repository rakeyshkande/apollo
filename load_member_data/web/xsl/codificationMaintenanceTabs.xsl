<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="codifiedProductsTabs">

  <table id="itemTabsTable" width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <div id="tabs">
          <!-- Shopping Cart Tab -->
          <script type="text/javascript" language="javascript">

          <![CDATA[
            document.write('<button id="productSetup" content="productSdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(0);" style="font-size: 8pt;">Product Setup</button>');
 			document.write('<button id="codificationSetup" content="codificationSdiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(1);" style="font-size: 8pt;">Codification <br/>Setup</button>');
		]]>

			var tabsArray = new Array(document.getElementById("productSetup"));
            tabsArray[1] = document.getElementById("codificationSetup");
            var _tabCount = 2;

  		</script>
  		</div>
	</td>
	</tr>
	</table>

	  <script type="text/javascript" language="javascript"><![CDATA[

    var TAB_WIDTH = 92;
    var MAX_TABS = 3;
    var minItem = 0;
    var maxItem = (tabsArray.length > MAX_TABS) ? minItem+MAX_TABS : tabsArray.length;
    var itemTabsTableOffset = document.getElementById("itemTabsTable").getBoundingClientRect().left + document.body.scrollLeft;
    var currentTabIndex = 0;
    var currentTab;
    var currentContent;

    function tabMouseOut(tab){
    	if(tab.className != "selected")
        tab.className = "button";
    }

    function tabMouseOver(tab){
    	if(tab.className != "selected")
        tab.className = "hover";
    }

    function showTab(tab){
    	if(currentTab)
      	currentTab.className = "button";

    	tab.className = "selected";
    	currentTab = tab;
    	showTabContent(tab);
    }

    function showTabContent(tab){
    	if(currentContent)
      	currentContent.className = "hidden";
    	currentContent = document.getElementById(tab.content);
    	currentContent.className = "visible";
    }

    function initializeTabs(tabToDisplay){

    	currentTab = document.getElementById(tabToDisplay);
    	currentTab.className = "selected";
		currentContent = document.getElementById(currentTab.content);
    	currentContent.className = "visible";
      	displayItems();
      	
      	
   		if(codMaint.currentTab.value == "product")
      		currentTabIndex = 0;
      	else
      		currentTabIndex = 1;
 	
 		if(currentTabIndex == 0)
		{
			document.getElementById("headText").firstChild.data = "Codified Products Setup";
			codMaint.newsku.focus();
		}
		else
		{
			document.getElementById("headText").firstChild.data = "Codification Setup";
			codMaint.newcodificationcode.focus();
		}

      //checkPrevNextStatus();
    }

    function displayItems() {

      for (var i = minItem, k = 0; i < maxItem; i++, k++){
        tabsArray[i].style.left = (k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset;

        tabsArray[i].style.visibility = "visible";
      }
     // showHideMoreTab( ((k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset) , "visible");
    }

    function hideItems() {
      for (var i = minItem; i < maxItem; i++){
        tabsArray[i].style.left = 0;
        tabsArray[i].style.visibility = "hidden";
      }
      //showHideMoreTab(0, "hidden");
    }

    function showHideMoreTab(offset, visibility){
      var more = getStyleObject("moreItemsTab");
      more.left = offset;
      more.visibility = visibility;
    }

    function showNextItem(){
      currentTabIndex++;
      showItemAt(currentTabIndex);
    }

    function showPrevItem(){
      currentTabIndex--;
      showItemAt(currentTabIndex);
    }

    function showItemAt(index)
    {
    
	currentTabIndex = index;

	if(currentTabIndex == 0)
	{

		codMaint.currentTab.value = "product";
	}
	else
	{	
		codMaint.currentTab.value = "cod";
	}
	
	currentTabIndex = index;
	
 	
		
	showTab(tabsArray[currentTabIndex]);
	
		if(currentTabIndex == 0)
		{
			document.getElementById("headText").firstChild.data = "Codified Products Setup";
			codMaint.newsku.focus();
		}
		else
		{
			document.getElementById("headText").firstChild.data = "Codification Setup";
			codMaint.newcodificationcode.focus();
		}

	
    }

    ]]>
  </script>


</xsl:template>

</xsl:stylesheet>