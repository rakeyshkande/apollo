<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">
    <xsl:variable name="zipcodes" select="key('pageData', 'zipcodes')/value"/>


	<html>
		<head>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
				<script type="text/javascript" language="javascript" src="../js/util.js"/>
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
				 <script type="text/javascript" src="../js/calendar.js" />
			<title>FTD</title>

<script type="text/javascript" language="javascript">
<![CDATA[


					var images = new Array("calendar");
					init();
					
					function stripLeading(string,chr) {
					   var finished = false;
					   for (var i = 0; i < string.length && !finished; i++)
					       if (string.substring(i,i+1) != chr) finished = true;
					   if (finished) return string.substring(i-1); else return string;
					}

					function validateEditPopUp()
					{
					var isValid = "true";

					document.getElementById("invalidDate").style.display = "none";
					document.getElementById("invalidDateZip").style.display = "none";
					document.getElementById("errorDate").style.display = "none";
					document.getElementById("requiredDate").style.display = "none";

					if(document.getElementById('rbBlock').checked == true)
					{
						if(multiform.editZipBlockDate.value.length < 1)
						{
							document.getElementById("requiredDate").style.display = "block";
							return false;
						}
					}



					var today = new Date();
				 	var todayDate = today.getDate();

					var uDate = multiform.editZipBlockDate.value;
					var dateArray = new Array();

					if(uDate.length > 0)
					{
						dateArray = uDate.split("/");

						if(dateArray.length == 2)
						{
						multiform.editZipBlockDate.value += "/" + today.getYear();
						uDate = multiform.editZipBlockDate.value;
						dateArray = uDate.split("/");
						}
						
						if(dateArray.length != 3)
						{
							document.getElementById("errorDate").style.display = "block";
							return false;
						}
						else
						{
							if(dateArray[2] != null && dateArray[2].length == 2)
							{
								dateArray[2] = "20" + dateArray[2];
								multiform.editZipBlockDate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
							}
			
							if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
						     {
				   		 		document.getElementById("invalidDateZip").style.display = "block";
						   		document.getElementById("invalidDate").style.display = "none";
								document.getElementById("errorDate").style.display = "none";
								document.getElementById("requiredDate").style.display = "none";
						   		return false;
						    }
			
						}

		
						if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0")))
	     				{
					   		document.getElementById("invalidDateZip").style.display = "block";
					   		document.getElementById("invalidDate").style.display = "none";
							document.getElementById("errorDate").style.display = "none";
							document.getElementById("requiredDate").style.display = "none";
					   		return false;
					    }

						if(dateArray[2] > today.getYear())
							isValid = "true";
						else if(dateArray[2] < today.getYear())
							isValid = "false";
						else
						{

							if(dateArray[0] > today.getMonth()+1)
								isValid = "true"
							else if(dateArray[0] < today.getMonth()+1)
								isValid = "false";
							else
							{
								if(dateArray[1] > today.getDate())
									isValid = "true";
								else if(dateArray[1] < today.getDate())
									isValid = "false";
								else
									isValid = "false";
							}

						}
						if(isValid == "false")
						{
							document.getElementById("invalidDate").style.display = "none";
							document.getElementById("requiredDate").style.display = "none";
							document.getElementById("errorDate").style.display = "block";
							
					   		return false;
						}
					}

					return true;


					} // end of validateEditPopUp

					function init()
					{
						addImageCursorListener(images);
					}


                    /*
                     * Date functions
                     */
                    function DateAdd(startDate, numDays, numMonths, numYears)
                    {
                        var returnDate = new Date(startDate.getTime());
                        var yearsToAdd = numYears;

                        var month = returnDate.getMonth()	+ numMonths;
                        if (month > 11)
                        {
                            yearsToAdd = Math.floor((month+1)/12);
                            month -= 12*yearsToAdd;
                            yearsToAdd += numYears;

                        }//end if

                        returnDate.setMonth(month);
                        returnDate.setFullYear(returnDate.getFullYear()	+ yearsToAdd);

                        returnDate.setTime(returnDate.getTime()+60000*60*24*numDays);

                        return returnDate;

                    }//end function DateAdd()


                    function dateFieldListener(date_box)
                    {
                        var input = event.keyCode;

                        if(input == 46)
                        {
                            date_box.value = date_box.value + '/';

                        }//end if ()

                        if ( (input < 47 || input > 57) && !(input == 43 || input == 45) )
                          event.returnValue = false;

                    }//end function


                    function dateMaker(dateBox)
                    {
                        var dateEntered = dateBox.value;

                        if(dateEntered == "0")
                        {
                            dateBox.value= getFormatDateToString(new Date);

                        }//end if
                        else
                        {
                            if(dateEntered.length > 0)
                            {
                                if(dateEntered.charAt(0) == "+" || dateEntered.charAt(0) == "-")
                                {
                                    var numChange = getDateChangeValue(dateEntered);
                                    var newDate = DateAdd(new Date(),numChange,0,0);
                                    var newDateString = getFormatDateToString(newDate);
                                    dateBox.value = newDateString;

                                }//end if plus or minus

                            }//end if date length

                        }//end else date nto zero

                    }//end function


                    function getDateChangeValue(dateEntered)
                    {
                        var num = 0;

                        for(i=0; i<dateEntered.length; i++)
                        {
                            if(dateEntered.charAt(i) == "+")
                            {
                                num++;

                            }//end if
                            else if(dateEntered.charAt(i) == "-")
                            {
                                num--;
                            }//end else

                        }//end for

                        return num;

                    }//end function


                    function getFormatDateToString(dateUnformatted)
                    {
                        var theMonth = dateUnformatted.getMonth() + 1;

                        if(theMonth.length < 2) { theMonth = "0" + theMonth; }

                        var theDate = dateUnformatted.getDate();

                        if(theMonth.length < 2) { theDate = "0" + theDate; }
                        var theYear = dateUnformatted.getYear();
                        if(theMonth.length < 2) { theYear = "0" + theYear; }

                        var todayString = theMonth + "/" + theDate + "/" + theYear;

                        return todayString;

                    }

	function doCloseAction()
	{
        window.returnValue = new Array("_EXIT_");
      	window.close();
   }

	function blockClick()
	{
		if(document.getElementById('editBlocksFlag').checked)
		{
			multiform.editZipBlockDate.disabled = false;
			document.getElementById('rbBlock').disabled = false;
			document.getElementById('rbActive').disabled = false;
			document.getElementById('calendar').disabled = false;
		}
		else
		{
			multiform.editZipBlockDate.disabled = true;
			document.getElementById('rbBlock').disabled = true;
			document.getElementById('rbActive').disabled = true;
			document.getElementById('calendar').disabled = true;
		}
	}
	
	function cutoffClick()
	{
		if(document.getElementById('editCutoffFlag').checked)
		{
			multiform.editCutoff.disabled = false;
		}
		else
		{
			multiform.editCutoff.disabled = true;
		}
	}
	
	function doSubmitAction()
	{
		if(multiform.editZipCodes.value.length < 1)
		{
			alert("No Zip Codes are entered.");
			multiform.editZipCodes.focus();
			return;
		}
		

		if(validateEditPopUp() == false)
			return;
		
		if(document.getElementById('editCutoffFlag').checked == false)
		{
			if(document.getElementById('editBlocksFlag').checked == false)
			{
				var st = "Are you sure you want to remove the following zip codes: " + document.getElementById('editZipCodes').value + " ?";
				var response = confirm(st);
				if (!response)
				{
					return;
				}
			}
		}

		var radiobtn = document.getElementById('editZipBlockFlag');

		//get radio button value
		var radioActive = document.getElementById('rbActive');
		var radioBlock = document.getElementById('rbBlock');
		var radioValue = "";
		if(radioActive.checked){
			radioValue = radioActive.value;
			}
		else if(radioBlock.checked){
			radioValue = radioBlock.value;
		}

		//edit cutoff check box
		var cutoffChkBox = "N";
		if(document.getElementById('editCutoffFlag').checked){
			cutoffChkBox = "Y";
		}

		//edit cutoff check box
		var blockChkBox = "N";
		if(document.getElementById('editBlocksFlag').checked){
			blockChkBox = "Y";
		}

        var ret = new Array();
        ret[0] = multiform.editZipCodes.value;
        ret[1] = cutoffChkBox;
        ret[2] = multiform.editCutoff.value;
        ret[3] = blockChkBox;
        ret[4] = radioValue;
        ret[5] = multiform.editZipBlockDate.value;

        window.returnValue = ret;

        window.close();

   }


]]>
</script>
		</head>
		<body>
			<form name="multiform">
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="'Edit Multiple Zip Codes'" />
				<xsl:with-param name="showExitButton" select="false()"/>
				<xsl:with-param name="showTime" select="false()"/>
			</xsl:call-template>


			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td colspan="9" width="100%" align="center">
				<table width="100%" border="0" cellpadding="1" cellspacing="1">
					<tr>
						<td align="left">
						  <textarea   
						    name="editZipCodes" 
						    wrap="physical" 
						    cols="40" 
						    rows="8" 
						    tabindex="1"
						    onBlur="this.value=this.value.toUpperCase();"><xsl:value-of select="$zipcodes"/>
						  </textarea>
						</td>
						<td>
							<table width="100%" border="0" cellpadding="1" cellspacing="1">
							<tr align="left"> <td></td><td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Unblock Date</td> </tr>
								<tr>
								
									<td><input type="checkbox" name="editBlocksFlag" onClick="blockClick();" >Block&nbsp;&nbsp;</input></td>
									<td>
										<table>
											<tr>
												<td><input type="radio"  value="Block" name="blockradio" id="rbBlock" disabled="true">Block</input></td>
												<td><input name="editZipBlockDate"   type="text" tabindex="3" disabled="true" maxlength="10" value="" onblur="dateMaker(this)" onfocus="javascript:fieldFocus();" onkeypress="javascript:dateFieldListener(this);"/>&nbsp;&nbsp;
												<input type="image" tabindex="2" disabled="true" id="calendar" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
												</td>
												<td  colspan="2" align="center" id="errorDate" class="RequiredFieldTxt" style="display:none">
												Date must be greater than today's date.
											 	</td>
											 	<td align="center" colspan="2" id="invalidDate" class="RequiredFieldTxt" style="display:none">
																		Invalid Format
											 	</td>
											 	<td align="center" colspan="2" id="requiredDate" class="RequiredFieldTxt" style="display:none">
																		Unblock Date is Required.
											 	</td>
											 	<td align="center" colspan="2" id="invalidDateZip" class="RequiredFieldTxt" style="display:none">
																		Invalid Date
											 	</td>
											 	
											 	
											</tr>
											<tr>
												<td><input type="radio" disabled="true" value="Active" name="blockradio" id="rbActive" checked="true">Unblock</input></td>
												<td>&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>

								<tr>
									<td><input type="checkbox" name="editCutoffFlag" onclick="cutoffClick();">Cutoff&nbsp;&nbsp;</input></td>
									<td>
										<table>
											<tr><td>

													<select disabled="true" name="editCutoff" tabindex="2">
													<xsl:for-each select="cutoffTimeList/cutoffTime">
														<xsl:variable name="cut" select="cutoffMilitary" />

														<option value="{cutoffMilitary}">
														<xsl:value-of select="cutoffDisplay" />
														</option>
													</xsl:for-each>

													</select>



											</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
      <center>
      		<br></br>
			<input type="button" value=" Submit " onClick="doSubmitAction();"/> &nbsp; &nbsp;
			<input type="button" value=" Close " onClick="doCloseAction();"/>
      </center>
			<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "editZipBlockDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar"  // ID of the button
    }
  );
</script>
	</xsl:template>
</xsl:stylesheet>