
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- @author Anshu Gaind -->
<!-- $Id: index.xsl,v 1.4.150.2 2015/01/26 20:20:26 gsergeycvs Exp $ -->
  <!-- External Templates -->
  <xsl:import href="footer.xsl"/>

  <xsl:param name="web-context-root"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:param name="adminAction"/>
  <xsl:param name="confirmation"/>
  <xsl:output method="html" indent="yes"/>
   <!-- Root template -->
   <xsl:template match="/">
      <html>
        <head>
          <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252"></meta>
          <title>Main</title>
          <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
          <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
          <script type="text/javascript" src="../js/clock.js"/>
        </head>
        <body>
        
        <script language="javascript">        
          function submitForm()
          {
            var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
            var message = 'Are you sure you want to ';
            if(document.getElementById("load-florist-data").checked == true)
            {              
              message = message + 'load florist data?';
              form1.action = webContextRoot + '/servlet/MemberDataLoadServlet?action=load-florist-data'
            }
            else if(document.getElementById("view-florist-audit-report").checked == true)
            {
              message = 'X';
              form1.action = webContextRoot + '/servlet/MemberDataLoadServlet?action=view-florist-audit-report-list'
            }
            else if(document.getElementById("load-florist-weight-data").checked == true)
            {
              message = message + 'load florist weight data?';
              form1.action = webContextRoot + '/servlet/MemberDataLoadServlet?action=load-florist-weight-data'
            }
            else if(document.getElementById("view-florist-weight-audit-report").checked == true)
            {            	
              message = 'X';
              form1.action = webContextRoot + '/servlet/MemberDataLoadServlet?action=view-florist-weight-audit-report-list'
            }
            
            if(message == 'X')
            {
              form1.submit();
            }
            else
            {
              if(confirm(message))
              {
                form1.submit();
              }
            }
            
          }
          
        </script>
          <!-- Header Template -->
          <!-- <xsl:call-template name="addHeader"/> -->      
 
          <form name="form1" method="post">
            <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
            <input type="hidden" name="context" value="{$context}"/>
            <input type="hidden" name="adminAction" value="{$adminAction}"/>
            
            <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		      <tr>
		        <td width="20%" align="left"><img border="0" src="../images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
		        <td width="60%" align="center" colspan="1" class="header" id="headText">Florist Maintenance - Main Page</td>
		        <td width="20%" align="right" class="label">
		          <table width="100%" cellspacing="0" cellpadding="0">
		              <tr>
		                <td id="time" align="right" class="label"></td>
		                <script type="text/javascript">startClock();</script>
		              </tr>
		              <tr>
		                <td align="right">
		                  <input type="button" tabindex="99" name="exitButtonHeader" value="Exit" onclick="document.forms[0].action = 'FloristSearchServlet?action=exitSystem'; document.forms[0].submit();"/>
		                </td>
		              </tr>
		          </table>
		        </td>
		      </tr>
		      <tr>
		        <td colspan="3"><hr/></td>
		      </tr>
		    </table>
 
            <table cellspacing="2" cellpadding="3" border="0" width="100%" align="left">
                <tr>
                  <td width="10%" align="left">                  
                    <input id="load-florist-data" type="radio" name="group" value="load-florist-data" checked="true"/>
                  </td>
                  <td width="90%" align="left">                  
                    Load Florist, Codifications and Linking Data
                  </td>
                </tr>
                <tr>
                  <td width="10%" align="left">                  
                    <input id="view-florist-audit-report" type="radio" name="group" value="view-florist-audit-report"/>
                  </td>
                  <td width="90%" align="left">                  
                    View Florist Audit Report
                  </td>
                </tr>
                <tr><td><br></br></td></tr>
                <tr>
                  <td width="10%" align="left">
                    <input id="load-florist-weight-data" type="radio" name="group" value="load-florist-weight-data"/>
                  </td>
                  <td width="90%" align="left">
                    Load Florist Weight Data
                  </td>
                </tr>
                
                
                 <tr>
                  <td width="10%" align="left">
                    <input id="view-florist-weight-audit-report" type="radio" name="group" value="view-florist-weight-audit-report"/>
                  </td>
                  <td width="90%" align="left">
                    View Florist Weight Audit Report
                  </td>
                </tr>
                
                <tr><td><br></br></td></tr>
                
                <tr>
                  <td width="10%" align="left">
                    &nbsp;                  
                  </td>
                  <td width="90%" align="left">                  
                    <input type="button" value="Submit" name="submit-button" onClick="submitForm();"/>
                  </td>
                </tr>
                <tr>
                  <td width="100%" align="left" colspan="2" class="Instruction">  
                    <xsl:value-of select="$confirmation" />
                  </td>
                </tr>
                <tr>
                  <td width="100%" align="center" colspan="4">
                    <xsl:call-template name="footer"/>
                  </td>
                </tr>
                
              </table>
          </form>
        </body>
      </html>   
   </xsl:template>
</xsl:stylesheet>