<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="association">
	
									<tr>
										<td width="100%">
											<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
											
													<tr>



													<td  width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>

													</td>
													<td width="10%"/>

													<td width="40%">
													&nbsp; &nbsp; &nbsp;
													<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
													<xsl:if test="$goto = 'Y'">
													GoTo Florist
													</xsl:if> &nbsp;
													</td>

												</tr>
												
												<tr bordercolor="white">
													<td align="right" width="50%">

													</td>
													<td class="label" align="right" width="10%"> Status </td>
                                                                                                                                                            <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
												</tr>
											</table>
										</td>
									</tr>
									
									<tr>
									<td colspan="4" align="center">
									<xsl:choose>
										<xsl:when test="key('securityPermissions', 'associationsUpdate')/value = 'Y'">
											<input type="button" align="center" value="SAVE" name="saveasn" size="10" tabindex="92" onClick="saveAction();"></input>
										</xsl:when>
										<xsl:otherwise>
											<input type="button"  disabled="true" align="center" value="SAVE" name="saveasn" size="10" tabindex="92" onClick="saveAction();"></input>
										</xsl:otherwise>
									</xsl:choose>
									</td>
									
									</tr>

										<tr>
										<td>
										<table width="100%" border="0" align="center">
										<tr>
											<td colspan="8" width="100%" align="center">


													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
																<td width="11%" class="colHeaderCenter">Florist #</td>
																<td width="25%" class="colHeaderCenter">Name</td>
																<td width="10%" class="colHeaderCenter">Zip/Postal</td>
																<td width="10%" class="colHeaderCenter">State/Pro</td>
																<td width="15%" class="colHeaderCenter">Phone</td>
																<td width="7%" class="colHeaderCenter">Sunday</td>
																<td width="7%" class="colHeaderCenter">Mercury</td>
																<td width="8%" class="colHeaderCenter">Status</td>
																<td width="12%" class="colHeaderCenter"># of Zips</td>
														</tr>
														
														<xsl:for-each select="floristData/associatedFloristList/associatedFlorist">
															
																<tr>
																<td><a href="javascript:popAssociation('{memberNumber}');" tabindex="1" name="ascFlor{position()}"><xsl:value-of select="memberNumber"/></a></td>
																<td><xsl:value-of select="name" /></td>
																<td align="center"><xsl:value-of select="postalCode" /></td>
																<td align="center"><xsl:value-of select="state" /></td>
																<td align="center"><xsl:value-of select="phone" /></td>
																<td align="center"><xsl:value-of select="sundayFlag" /></td>
																<td align="center"><xsl:value-of select="mercuryFlag" /></td>
																<td align="center"><xsl:value-of select="status" /></td>
																<td align="center"><xsl:value-of select="zipCount" /></td>
																</tr>
												
														</xsl:for-each>
													</table>
											</td>
										</tr>
										</table>
										</td>



										</tr>
										
										<tr>

										<td align="center" width="100%">
										<xsl:choose>
											<xsl:when test="key('securityPermissions', 'associationsUpdate')/value = 'Y'">
												<input type="button" align="center" value="SAVE" name="saveasn2" size="10" tabindex="2" onClick="saveAction();"></input>
											</xsl:when>
											<xsl:otherwise>
												<input type="button"  disabled="true" align="center" value="SAVE" name="saveasn2" size="10" tabindex="2" onClick="saveAction();"></input>
											</xsl:otherwise>
										</xsl:choose>
										
										</td>
										
										</tr>

	</xsl:template>
</xsl:stylesheet>
