<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="zipQueue">

	<script type="text/javascript" language="javascript">

	<![CDATA[

function isZipPopNeeded()
{

var form = document.forms[0];

var i;

var temp = "0";

for(i = 1; i <= form.totalZipcodes.value; i++)
{
	temp = "zipYes" + i;

	if(document.getElementById(temp).checked == true)
	{
		if(document.getElementById('zipunblockdate' + i).value == "")
		{
		return true;
		}
	}
}

return false;

}

function assignZipTabs()
{

var pos = 0;
for(var i=1; i<=floristMaint.totalZipcodes.value; i++)
{
	if(document.getElementById("zipNo" + i) != null)
	{
		if(!document.getElementById("zipNo" + i).disabled)
		{
			pos = i;
			break;
		}
	}
}


if(document.getElementById("ziphref" + pos) != null)
	document.getElementById("ziphref" + pos).tabIndex = 5;
if(document.getElementById("zipNo" + pos) != null)
	document.getElementById("zipNo" + pos).tabIndex = 6;
if(document.getElementById("zipYes" + pos) != null)
	document.getElementById("zipYes" + pos).tabIndex = 7;
if(document.getElementById("cutofftime" + pos) != null)
	document.getElementById("cutofftime" + pos).tabIndex = 8;

}


function doUnblockZipPopUp()
{

var form = document.forms[0];
var outZips = new Array(150);

var i;
var zipCount = 0;

var blockEndDate = "zipunblockdate";
var temp = "0";

for(i = 1; i <= form.totalZipcodes.value; i++)
{
	temp = "zipYes" + i;
	if(zipCount >= 150)
		break;

	if(document.getElementById(temp).checked == true)
	{
		if(document.getElementById('zipunblockdate' + i).value == "")
		{

		outZips[zipCount] = document.getElementById('zipcode' + i).value;

		zipCount++;
		}
	}
}

var zipOutString = "";

for(var j=1; j<=zipCount; j++)
{
zipOutString = zipOutString + "&z" + j + "=" + outZips[j-1];
}

var url_source = "ZipCodeServlet?action=blockZipCode" + zipOutString;
var modal_dim = "dialogWidth:500px; dialogHeight:450px; center:yes; status=0; help:no; scroll:no";
var ret = window.showModalDialog(url_source, "", modal_dim);

if(ret && ret != null && ret[0] == "closed")
{
	currentTabIndex = 2;
	floristMaint.endTab.value = "zipQueue";
	return false;
}

//document.forms[0].saveNeeded.value = "yes";

		if (ret && ret != null)
		{
	        form.newzipunblockdate.value = ret[0];
	 	}


       var form = document.forms[0];
       form.action = "ViewQueueServlet?action=updateFloristNoSave";
       showWaitMessage("outerContent", "wait", "Processing");
       form.submit();

		return true;

}



]]>
</script>
<input type="hidden" name="editZipCodes"/>
<input type="hidden" name="editCutoffFlag"/>
<input type="hidden" name="editCutoff"/>
<input type="hidden" name="editBlocksFlag"/>
<input type="hidden" name="editZipBlockDate"/>
<input type="hidden" name="editZipBlockFlag"/>

			<tr><td>

				<table width="100%" >

					<tr>

						<td width="20%" align="right" class="label">Order Number</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/orderNumber}"/>
						</td>

						<td width="10%" >
						</td>

						<td width="10%" align="center">
						
						<button style="font-size: 8pt;" name="zSave1" onclick="javascript:doSaveAction();" tabindex="91">&nbsp;&nbsp;
						<span style="text-decoration:underline;">S</span>ave&nbsp;&nbsp;</button>&nbsp;&nbsp;
						
						<button style="font-size: 8pt;" name="zRemove1" onclick="javascript:doRemoveAction();" tabindex="92">
						<span style="text-decoration:underline;">R</span>emove</button>&nbsp;&nbsp;

						<button style="font-size: 8pt;"  onclick="javascript:doNextAction();" tabindex="93">&nbsp;&nbsp;
						<span style="text-decoration:underline;">N</span>ext&nbsp;&nbsp;</button>&nbsp;&nbsp;
						
						</td>

						
						<td width="10%"></td>
						<td width="10%" align="right" class="label">Count</td>
						<td width="10%" align="left">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/messageCount}"/>
						</td>

						<td width="10%"></td>
						<td width="10%"></td>
					</tr>

					<tr><td><br></br></td></tr>


					<tr>
						<td  colspan="9" class="tblheader" align="left"> Zip / Postal Code Information </td>
					</tr>

					<tr>
						<td width="20%" align="right" class="label">Delivery Address</td>
						<td width="10%" colspan="3">
							<input size="74" type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/deliveryAddress}"/>
						</td>

						

						<td width="10%" align="right" class="label" colspan="2"># Florists in Zip</td>
						<td width="10%" align="left">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/zipFloristCount}"/>
						</td>
					</tr>

					<tr>
							<td width="20%" align="right" class="label">Blocked</td>
							<td width="10%">
								<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/zipBlockFlag}"/>
							</td>
	
							<td width="10%" align="right" class="label">Unblock Date</td>						
							<xsl:choose>
							<xsl:when test="viewQueueData/generalData/zipUnblockDate = ''">
								<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="N/A"/></td>
		      				</xsl:when>
							<xsl:otherwise>
								<td width="10%" align="left"><input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/zipUnblockDate}"/></td>
		      				</xsl:otherwise>
							</xsl:choose>	
					</tr>

					<tr>
						<td  colspan="9" class="tblheader" align="left"> Zip Code Blocking / Removal </td>
					</tr>

				</table>

				</td>
				</tr>

				<tr>
				<td>
									<table width="100%" border="0">
										<tr>
											<td colspan="5" width="100%" align="center">


													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>

															<td width="5%" class="colHeaderCenter">Zip Code</td>
															<td width="20%" class="colHeaderCenter">City</td>
															<td width="5%" class="colHeaderCenter"><span style="text-decoration:underline;">A</span>ctive</td>
															<td width="5%" class="colHeaderCenter">Block</td>
															<td width="30%" class="colHeaderCenter">Cut <span style="text-decoration:underline;">O</span>ff Time</td>
															<td width="30%" class="colHeaderCenter">Unblock Date</td>

														</tr>

														<tr>
															<td>
															<input type="hidden" name="totalZipcodes" value="{count(viewQueueData/postalCodeList/postalCode/postalCode)}"></input>
															<input type="hidden" name="newzipunblockdate" value="" />
															</td>
														</tr>
														
														
														
														
														
														
														
														
														
														
														
														
														<xsl:variable name="deliverZip" select="viewQueueData/generalData/deliveryZipCode" />
														
														<xsl:for-each select="viewQueueData/postalCodeList/postalCode">
														<xsl:sort select="postalCode" data-type="number" />
														
														<xsl:if test="postalCode = $deliverZip">
														
															<script>
																var calCount =
																	<xsl:value-of select="position()"/>;
																	calCount = calCount + "";
																</script>
																<script><![CDATA[
																if(floristMaint.pcode.value == "")
																{
																	floristMaint.pcode.value = calCount;
																}
																]]></script>
																
															
															<tr id="zip_row{position()}" style="display:block">
															
															
													

																<td width="10%" align="center">
																	<input name="deleteFlag{position()}" type="hidden" value="{deleteFlag}"/>

																	<input name="zipcode{position()}" type="hidden" value="{postalCode}"/>

																		<b><xsl:value-of select="postalCode"/></b>

																		<xsl:if test="deleteFlag != 'N'">
																			<xsl:if test="savedFlag = 'N'">
																				<script>
																				document.getElementById("savezipNeeded").value = "yes";
																				</script>
																			</xsl:if>
																		</xsl:if>

																</td>

																<td width="20%" align="center">
																	<b><xsl:value-of select="city" /></b>
																</td>


																<xsl:variable name="zipStat" select="blockedFlag" />


																<td align="center">

																	<input type="radio" value="N" name="zipblock{position()}" id="zipNo{position()}" tabindex="-1">
																		<xsl:if test="$zipStat = 'N'">
																		<xsl:attribute name="CHECKED" />
																		</xsl:if>
																	</input>


																</td>

																<td align="center">
																	<input type="radio" value="Y" name="zipblock{position()}" id="zipYes{position()}" tabindex="-1">
																		<xsl:if test="$zipStat = 'Y'">
																		<xsl:attribute name="CHECKED" />
																		</xsl:if>
																	</input>
																</td>

																<!--
																	<xsl:if test="$zipStat = 'Y'">
																		<input type="hidden" value="Y" name="zipblock{position()}"/>
																	</xsl:if>

																	<xsl:if test="$zipStat = 'N'">
																		<input type="hidden" value="N" name="zipblock{position()}"/>
																	</xsl:if>
																-->

																<td align="center">

																<select name="cutofftime{position()}" tabindex="-1">
																	<xsl:variable name="cuttime" select="cutoffTime" />

																	<xsl:for-each select="//cutoffTimeList/cutoffTime">
																	<xsl:variable name="cut" select="cutoffMilitary" />

																	<option value="{cutoffMilitary}">

																	<xsl:if test="$cuttime = $cut">
																	<xsl:attribute name="SELECTED" />
																	</xsl:if>

																	<xsl:value-of select="cutoffDisplay" />
																	</option>
																	</xsl:for-each>
																</select>



																</td>

																<td align="center">
																<input type="text" tabindex="-1" readonly="true" value="{blockEndDate}" name="zipunblockdate{position()}"/>


																</td>

															</tr>


															<xsl:if test="deleteFlag = 'Y'">
																<script>
																	var zipRowCount = <xsl:value-of select="position()"/>;
																	zipRowCount = zipRowCount + "";

																	document.getElementById("zip_row" + zipRowCount).style.display = "none";
																</script>
															</xsl:if>
														</xsl:if>
														</xsl:for-each>
														
														
														
														
												
														
														
														

														<xsl:for-each select="viewQueueData/postalCodeList/postalCode">
															<xsl:sort select="postalCode" data-type="number" />
															
															<xsl:if test="postalCode != $deliverZip">
																<script>
																var calCount =
																	<xsl:value-of select="position()"/>;
																	calCount = calCount + "";
																</script>
																<script><![CDATA[
																if(floristMaint.pcode.value == "")
																{
																	floristMaint.pcode.value = calCount;
																}
																]]></script>

															<tr id="zip_row{position()}" style="display:block">


																<td width="10%" align="center">
																	<input name="deleteFlag{position()}" type="hidden" value="{deleteFlag}"/>

																	<input name="zipcode{position()}" type="hidden" value="{postalCode}"/>

																		<xsl:value-of select="postalCode"/>

																		<xsl:if test="deleteFlag != 'N'">
																			<xsl:if test="savedFlag = 'N'">
																				<script>
																				document.getElementById("savezipNeeded").value = "yes";
																				</script>
																			</xsl:if>
																		</xsl:if>

																</td>

																<td width="20%" align="center">
																	<xsl:value-of select="city" />
																</td>


																<xsl:variable name="zipStat" select="blockedFlag" />


																<td align="center">

																	<input type="radio" value="N" name="zipblock{position()}" id="zipNo{position()}" tabindex="-1">
																		<xsl:if test="$zipStat = 'N'">
																		<xsl:attribute name="CHECKED" />
																		</xsl:if>
																	</input>


																</td>

																<td align="center">
																	<input type="radio" value="Y" name="zipblock{position()}" id="zipYes{position()}" tabindex="-1">
																		<xsl:if test="$zipStat = 'Y'">
																		<xsl:attribute name="CHECKED" />
																		</xsl:if>
																	</input>
																</td>

																<!--
																	<xsl:if test="$zipStat = 'Y'">
																		<input type="hidden" value="Y" name="zipblock{position()}"/>
																	</xsl:if>

																	<xsl:if test="$zipStat = 'N'">
																		<input type="hidden" value="N" name="zipblock{position()}"/>
																	</xsl:if>
																-->

																<td align="center">

																<select name="cutofftime{position()}" tabindex="-1">
																	<xsl:variable name="cuttime" select="cutoffTime" />

																	<xsl:for-each select="//cutoffTimeList/cutoffTime">
																	<xsl:variable name="cut" select="cutoffMilitary" />

																	<option value="{cutoffMilitary}">

																	<xsl:if test="$cuttime = $cut">
																	<xsl:attribute name="SELECTED" />
																	</xsl:if>

																	<xsl:value-of select="cutoffDisplay" />
																	</option>
																	</xsl:for-each>
																</select>



																</td>

																<td align="center">
																<input type="text" tabindex="-1" readonly="true" value="{blockEndDate}" name="zipunblockdate{position()}"/>


																</td>

															</tr>


															<xsl:if test="deleteFlag = 'Y'">
																<script>
																	var zipRowCount = <xsl:value-of select="position()"/>;
																	zipRowCount = zipRowCount + "";

																	document.getElementById("zip_row" + zipRowCount).style.display = "none";
																</script>
															</xsl:if>

														</xsl:if>

														</xsl:for-each>
													</table>
											</td>
										
										
										</tr>
										<tr>
										
											<td width="48%">
				                        <table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">				
				                           <tr>
				                              <td id="errorInvalidPopZips" align="left" width="70%" class="RequiredFieldTxt" style="display:none">
				                                 <br></br>The following Zip/Postal Codes can not be blocked or modified:<br></br>
				                                 <input type="hidden" name="listInvalidPop" readonly="true" size="20" value="{invalidPopZips/iPopzips/value}"/>
				                                 <xsl:value-of select="invalidPopZips/iPopzips/value" />
				                              </td>
				                              <td id="holderrorInvalidPopZips" align="left" width="70%" style="display:none"/>
				                              <td width="21%" align="right">
				                                 <input type="hidden" id="editalllink" name="editalllink" />
				                               </td>
				                               <td width="9%" align="right"></td>
				                           </tr>
				                        </table>
											</td>
											<td width="10%" align="right">
						
						<button style="font-size: 8pt;" name="zSave2" onclick="javascript:doSaveAction();" tabindex="9">&nbsp;&nbsp;
						<span style="text-decoration:underline;">S</span>ave&nbsp;&nbsp;</button>&nbsp;&nbsp;

						<button style="font-size: 8pt;" name="zRemove2" onclick="javascript:doRemoveAction();" tabindex="10">
						<span style="text-decoration:underline;">R</span>emove</button>&nbsp;&nbsp;

						<button style="font-size: 8pt;"  onclick="javascript:doNextAction();" tabindex="11">&nbsp;&nbsp;
						<span style="text-decoration:underline;">N</span>ext&nbsp;&nbsp;</button>&nbsp;&nbsp;
						
						
						<input type="hidden" value="viewQueue" name="pageLocation" />
						</td>
						<td ></td>
										
										</tr>


									</table>

					</td>
					</tr>

	</xsl:template>
</xsl:stylesheet>