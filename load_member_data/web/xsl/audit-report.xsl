<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- @author Anshu Gaind -->
<!-- $Id: audit-report.xsl,v 1.2 2006/05/26 15:49:23 mkruger Exp $ -->

  <!-- External Templates -->
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>

  <!-- parameters -->
  <xsl:param name="web-context-root"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:param name="adminAction"/>
  <xsl:param name="message"/>
  
  <!-- variables -->
  <xsl:variable name="new-status" select="'New'" />
  <xsl:variable name="approved-status" select="'Approved'" />
  <xsl:variable name="processed-status" select="'Processed'" />
  <xsl:variable name="rejected-status" select="'Rejected'" />

  <xsl:variable name="current-status" select="/audit-report/header/content/status" />
  <xsl:variable name="updated-date" select="//header/content/updated_date" />

  <xsl:output method="html" indent="yes"/>


  <!-- Root template -->
  <xsl:template match="/">
        <xsl:call-template name="AUDIT-REPORT-TEMPLATE" />
  </xsl:template>
  
  <xsl:template name="AUDIT-REPORT-TEMPLATE" >
    <html>
      <head>
        <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8"/>
        <title>Florist Audit Report</title>
        <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
      </head>
      <body>
      
        <script language="javascript">
          function submitForm(str)
          {          
            if (str == 'Approve') 
            {
                form1.action = '/<xsl:value-of select="$web-context-root"/>/servlet/MemberDataLoadServlet?action=approve-florist-audit-report';
                form1.submit();
            }
            else if (str == 'Reject') 
            {
                // open pop-up
                var url_source = '/<xsl:value-of select="$web-context-root"/>/html/comments.html';
                var modal_dim = "dialogWidth:650px; dialogHeight:400px; center:yes; status=0; help:no; scroll:no";
                var vDate = new Date();
                var vMonth = vDate.getMonth() + 1;
                var hr = vDate.getHours();
				var t = hr + "";
				if(t.length == 1)
					t = "0";
				else
					t = "";
                	
                var pas = 'REJECT-FLORIST-AUDIT-REPORT' 
                          + '^' + form1.rejectcomment.value 
                          + '^' + '<xsl:value-of select="//header/content/updated_by" />' 
                          + ' ' + vMonth + '/' + vDate.getDate() + '/' + vDate.getFullYear()+ ' ' + t +  hr + ":" + vDate.getMinutes();
                var ret = window.showModalDialog(url_source, pas, modal_dim);
               
               	if(ret == null)
               		return;
               
                if (ret.length != 0)  
                {
                    form1.comments.value = ret;
                    form1.action = '/<xsl:value-of select="$web-context-root"/>/servlet/MemberDataLoadServlet?action=reject-florist-audit-report';
                    form1.submit();
                }
             }
          }
          
          function trim(str)
          {
                str=str.replace(/^\s*(.*)/, "$1");
                str=str.replace(/(.*?)\s*$/, "$1");
                return str;
          }

          function doExitAction()
          {
              var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
              form1.action = webContextRoot + '/servlet/MemberDataLoadServlet?action=view-florist-audit-report-list';
              form1.submit();
          }
          
          function showComments()
          {
                var url_source = '/<xsl:value-of select="$web-context-root"/>/html/comments.html';
                var modal_dim = "dialogWidth:650px; dialogHeight:400px; center:yes; status=0; help:no; scroll:no";
                var vDate = '<xsl:value-of select="concat(substring($updated-date,6,2),'/',substring($updated-date,9,2),'/',substring($updated-date,1,4), substring($updated-date,11,6) )" />' ;
                var pas = "ASHOW-COMMENTS"  + '^' + form1.rejectcomment.value  + '^' + '<xsl:value-of select="//header/content/updated_by" />' + ' ' + vDate;
                window.showModalDialog(url_source, pas , modal_dim);
          }
        </script>
        <!-- Header Template -->
        <!-- <xsl:call-template name="addHeader"/> -->      
        <xsl:call-template name="header">
          <xsl:with-param name="headerName"  select="'Distribution / Fulfillment'" />
          <xsl:with-param name="showExitButton" select="true()"/>
          <xsl:with-param name="showTime" select="true()"/>
        </xsl:call-template>
        <form name="form1" method="POST">
          <input type="hidden" name="reportID" value="{/audit-report/header/content/report_id}"/>
          <input type="hidden" name="oldStatus" value="{/audit-report/header/content/status}"/>
          <input type="hidden" name="rejectcomment" value="{/audit-report/header/content/report_comment}"/>
          <input type="hidden" name="comments" value=""/>
          <input type="hidden" name="reportType" value="audit"/>
          
          <input type="hidden" name="message" value="{$message}"/>
          <input type="hidden" name="web-context-root" value="{$web-context-root}"/>
			    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
			    <input type="hidden" name="context" value="{$context}"/>
			    <input type="hidden" name="adminAction" value="{$adminAction}"/>
          
          
           <table cellspacing="2" cellpadding="3" border="0" bgcolor="white" width="100%" align="left">

          <tr>
              <td width="100%" align="center" colspan="4">
<!--
               <input type="button" value="Approve" name="approve-button-top" onClick="submitForm('Approve');"/>
               <input type="button" value="Reject" name="reject-button-top" onClick="submitForm('Reject');"/>
	-->
				       <xsl:choose>
                  <xsl:when test="contains($current-status, $new-status)" >
                   <!-- Both buttons clickable if status is new -->
                    <input type="button" tabindex="1" value="Approve" name="approve-button-top" onClick="submitForm('Approve');"/>
				&nbsp; &nbsp;
                    <input type="button" tabindex="2" value="&nbsp;&nbsp;Reject&nbsp;&nbsp;" name="reject-button-top" onClick="submitForm('Reject');"/>
                  </xsl:when>
                  <xsl:when test="contains($current-status, $approved-status) or contains($current-status, $rejected-status) or contains($current-status, $processed-status)" >
                    <!-- Both buttons disabled if status is approved or rejected or processed -->
                    <input type="button" tabindex="1" value="Approve" name="approve-button-top" onClick="submitForm('Approve');" disabled="disabled"/>
				&nbsp; &nbsp;
                     <input type="button" tabindex="2" value="&nbsp;&nbsp;Reject&nbsp;&nbsp;" name="reject-button-top" onClick="submitForm('Reject');" disabled="disabled"/>
                  </xsl:when>
                </xsl:choose>

              </td>
            </tr>
            <tr><td><br></br></td></tr>

            <tr align="center">

              <xsl:choose>
                <xsl:when test="string-length($message) &lt;= 0">
                  <td colspan="4" nowrap="nowrap" align="center" style="font-size: 10pt;">
                      <STRONG>Florist Audit Report</STRONG>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td colspan="4" nowrap="nowrap" align="center" style="font-size: 10pt;">
                      <STRONG>Florist Audit Report</STRONG>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
            
            
            <tr>
              <xsl:choose>
                <xsl:when test="string-length($message) &lt;= 0">
					        <td colspan="4" nowrap="nowrap" align="center" style="font-size: 10pt;"></td>
                </xsl:when>
                <xsl:otherwise>
       	 			     <td colspan="2" nowrap="nowrap" align="center" style="font-size: 10pt;"></td>
                  <xsl:choose>
                    <xsl:when test="starts-with($message, 'Approved')">
                      <td colspan="2" class="Instruction" nowrap="nowrap" align="left">
                          <xsl:value-of select="$message" />
                      </td>
                    </xsl:when>
                    <xsl:when test="starts-with($message, 'Rejected')">
                      <td colspan="2" class="RequiredFieldTxt" nowrap="nowrap" align="left">
                  		 <xsl:value-of select="$message" />
                      </td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td colspan="2" class="RequiredFieldTxt" nowrap="nowrap" align="left">
                      	 <xsl:value-of select="$message" />
                      </td>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>

              </xsl:choose>
             
            </tr>
             <tr>
            <xsl:choose>
                <xsl:when test="string-length($message) &lt;= 0">
					        <td colspan="4" nowrap="nowrap" align="center" style="font-size: 10pt;"></td>
                </xsl:when>
                <xsl:otherwise>
   	 			    <td colspan="2" nowrap="nowrap" align="center" style="font-size: 10pt;"></td>
                  <xsl:choose>
                    <xsl:when test="starts-with($message, 'Approved')">
                      <td colspan="2" class="Instruction" nowrap="nowrap" align="left">
                      </td>
                    </xsl:when>
                    <xsl:when test="starts-with($message, 'Rejected')">
                      <td colspan="2" class="RequiredFieldTxt" nowrap="nowrap" align="left">
                	     <input type="hidden"  value="{/audit-report/header/content/report_comment}"/>
                  		<a href="javascript:showComments();"><u>Comments</u></a>
                      </td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td colspan="2" class="RequiredFieldTxt" nowrap="nowrap" align="left">
                      	
                      </td>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>

              </xsl:choose>

            </tr>
            <tr>
           		<td width="40%" class="colheader" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Florists</td>
           		<td width="20%" class="colheader" align="left">&nbsp;R</td>
          		<td width="20%" class="colheader" align="left">&nbsp;S</td>
           		<td width="20%" class="colheader" align="left">&nbsp;E</td>
            </tr>
            <xsl:for-each select="/audit-report/details/content">
            <tr>
              <td width="40%" align="left">&nbsp;
                <xsl:value-of select="field_name"/>
              </td>
              <td width="20%" align="left">&nbsp;
                <xsl:value-of select="value_1"/>
              </td>
              <td width="20%" align="left">&nbsp;
                <xsl:value-of select="value_2"/>
              </td>
              <td width="20%" align="left">&nbsp;
                <xsl:value-of select="value_3"/>
              </td>
            </tr>
            </xsl:for-each>

            <tr>
              <td width="100%" align="center" colspan="4">
                <xsl:choose>
                  <xsl:when test="contains($current-status, $new-status)" >
                    <!-- Both buttons clickable if status is new -->
                    <input type="button" tabindex="3" value="Approve" name="approve-button-bottom" onClick="submitForm('Approve');"/>
					&nbsp;&nbsp;
                    <input type="button" tabindex="4" value="&nbsp;&nbsp;Reject&nbsp;&nbsp;" name="reject-button-bottom" onClick="submitForm('Reject');" />
                  </xsl:when>
                  <xsl:when test="contains($current-status, $approved-status) or contains($current-status, $rejected-status) or contains($current-status, $processed-status)" >
                    <!-- Both buttons disabled if status is approved or rejected or processed -->
                    <input type="button" tabindex="3" value="Approve" name="approve-button-bottom" onClick="submitForm('Approve');" disabled="disabled"/>
					&nbsp;&nbsp;
                    <input type="button" tabindex="4" value="&nbsp;&nbsp;Reject&nbsp;&nbsp;" name="reject-button-bottom" onClick="submitForm('Reject');" disabled="disabled"/>
                  </xsl:when>
                </xsl:choose>
              </td>
            </tr>
				
				<tr>
				<td align="right" colspan="6"><input type="button" tabindex="100" value="Exit" onclick="javascript:doExitAction();"></input></td>
	            </tr>

            <tr>
              <td width="100%" align="center" colspan="4">
                <xsl:call-template name="footer"/>
              </td>
            </tr>
            
 
          </table>
          
        </form>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>