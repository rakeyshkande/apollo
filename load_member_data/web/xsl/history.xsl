<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="history">
		
									<tr>
										<td>
											<input type="hidden" name="permitHisPop" value="{key('securityPermissions', 'managerCommentView')/value}"/>
										
											<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
												
												<tr>



													<td  width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>

													</td>


													<td align="right" width="10%">


													</td>

													<td width="40%">
													&nbsp; &nbsp; &nbsp;
													<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
													<xsl:if test="$goto = 'Y'">
													GoTo Florist
													</xsl:if> &nbsp;
													</td>

												</tr>
												
												<tr bordercolor="white">
													<td align="right" width="50%"></td>


													<td class="label" align="right" width="10%"> Status </td>
                                                                                                                                                                    <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
												</tr>

											</table>

										</td>
										</tr>
										
										<tr>
										<td colspan="4" class="label" align="center"> <input name="histadd" tabindex="1" type="button" value=" Add " onclick="doAddCommentPopUp();"></input></td>
										</tr>

										<tr>
										<td class="tblheader"  align="left" width="100%">History</td>
										</tr>
										
										<tr align="right">
										<td>
										<table>
										<tr>
										<td>
										<a href="javascript:doFullHistory();" tabindex="1">View Full History</a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
										</td>
										</tr>
										</table>
										</td>
										
										</tr>

										<tr>
										<td>
										<table width="100%" border="0" align="center" >
										<tr>
											<td colspan="5" width="100%" align="center">

													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
																<td width="10%" class="colHeaderCenter">ID</td>
																<td width="10%" class="colHeaderCenter">Date</td>
																<td width="5%" class="colHeaderCenter">Disposition</td>
																<td width="70%" class="colHeaderCenter">Comments</td>
														</tr>

														<tr>
														<td width="10%"/>
														<td width="10%"/>
														<script> var nHistory="0";</script>
														<td width="5" align="center">
														<select name="hisdis" onchange="loadHistory(nHistory);" tabindex="2">
														<option value="All" selected="true">All</option>
														<option value="Adjustments">Adjustments</option>
														<option value="Blocks">Blocks</option>
														<option value="Cities">Cities</option>
														<option value="Codification">Codification</option>
														<option value="Compliments">Compliments</option>
														<option value="Problems">Problems</option>
														<option value="Profile">Profile</option>
														<option value="Weight">Weight</option>
														<option value="Zip Codes">Zip Codes</option>
														</select>
														</td>
														<td width="70%"/>
														</tr>


														<xsl:for-each select="floristData/floristHistoryList/floristHistory">

														<xsl:variable name="isNew" select="newCommentFlag" />
														<xsl:variable name="isManager" select="managerFlag" />
														<xsl:if test="$isNew = 'Y'">

														<tr><td>
															<input type="hidden" name="disposition{position()}"  value="{dispositionDescription}" ></input>
															<input type="hidden" name="comment{position()}"  value="{comments}"></input>
															<input type="hidden" name="managerflag{position()}"  value="{managerFlag}"></input>
															<input type="hidden" name="newDate{position()}"  value="{commentDate}"></input>
														</td></tr>

														</xsl:if>
														
														<xsl:variable name="zipStat" select="blockedFlag" />
														
														 <xsl:choose>
													      <xsl:when test="$isManager = 'Y' and key('securityPermissions', 'managerCommentView')/value = 'N'">
													
													      </xsl:when>
													      
													      <xsl:otherwise>

														<tr id="dis{dispositionDescription}{position()}" style="display:block">
														<script>nHistory++;</script>

														<xsl:if test="$zipStat = 'N'">
														<input type="hidden" value="N" name="zipblocked{position()}"/>
														</xsl:if>

														<td width="10%" align="center">
															<xsl:value-of select="userId" />
														</td>

														<td width="10%" align="center">

															<xsl:value-of select="commentDate" />
														</td>

														<td width="5%" align="center">

														<xsl:value-of select="dispositionDescription" />

														</td>



														<td width="70%" align="left">

														&nbsp;&nbsp;&nbsp; <xsl:value-of select="comments" />


														</td>

														</tr>
														
														
													    </xsl:otherwise>
													    </xsl:choose>
									
												

														</xsl:for-each>

														<input type="hidden" name="totalHist" value="{count(floristData/floristHistoryList/floristHistory/comments)}"></input>


													</table>

											</td>
										</tr>
										<tr><td><br></br></td></tr>
										
										<tr>
											<td colspan="4" width="100%" class="label" align="center" > <input name="histadd2" tabindex="3" type="button" value=" Add " onclick="doAddCommentPopUp();"></input></td>
										</tr>
										<tr><td><br></br></td></tr>
										</table>
										</td>
	
										</tr>
										
									
										
										<tr><td align="center" width="100%"><input type="button" align="center" value="SAVE" name="savehist" size="10" tabindex="4" onClick="saveAction();"></input></td></tr>

	</xsl:template>
</xsl:stylesheet>
