<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- @author Metin Hamidof -->

  <!-- External Templates -->
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>

  <!-- parameters -->
  <xsl:param name="web-context-root"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:param name="adminAction"/>
  <xsl:param name="message"/>

  <xsl:output method="html" indent="yes"/>

  <!-- Root template -->
  <xsl:template match="/">
        <xsl:call-template name="ZIP-REMOVALS-REPORT-TEMPLATE" />
  </xsl:template>
  
  <xsl:template name="ZIP-REMOVALS-REPORT-TEMPLATE" >
        <html>
            <head>
                <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8"/>
                <title>Zip Removals Report</title>
                <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
                <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
            </head>
        
        <body>
        
        <form name="form1" method="post" action="FloristSearchServlet?action=exitSystem">
            <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
            <input type="hidden" name="context" value="{$context}"/>
            <input type="hidden" name="adminAction" value="{$adminAction}"/>
            
            <input type="hidden" name="servlet_action"/>    
            <input type="hidden" name="web-context-root" value="{$web-context-root}"/>


            <script language="javascript">
                <![CDATA[

                function doExitAction()
                {
                    document.forms[0].action = "FloristSearchServlet?action=exitSystem";
                    document.forms[0].submit();
                }
                ]]>
            </script>        
        
            <xsl:call-template name="header">
                <xsl:with-param name="headerName"  select="'Distribution / Fulfillment'" />
                <xsl:with-param name="showExitButton" select="true()"/>
                <xsl:with-param name="showTime" select="true()"/>
            </xsl:call-template>
            
            <table cellspacing="2" cellpadding="3" border="0" bgcolor="white" width="100%" align="left">
                <tr align="center">
                    <td colspan="4" nowrap="nowrap" align="center" style="font-size: 10pt;">
                        <STRONG>Zip Removals Report</STRONG>
                    </td>
                </tr>

                <tr>
                    <td width="50%" class="colheader" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Florist ID</td>
                    <td width="50%" class="colheader" align="left">&nbsp;Zip Code</td>
                </tr>

                <xsl:for-each select="/root/ZIPS/ZIP">
                <tr>
                <td width="50%" align="left">&nbsp;
                    <xsl:value-of select="top_level_florist_id"/>
                </td>
                <td width="50%" align="left">&nbsp;
                    <xsl:value-of select="zip_code"/>
                </td>
                </tr>
                </xsl:for-each>
                <tr>
                    <td align="right" colspan="6"><input type="button" tabindex="100" value="Exit" onclick="javascript:doExitAction();"></input></td>
                </tr>

                <tr>
                    <td width="100%" align="center" colspan="4">
                        <xsl:call-template name="footer"/>
                    </td>
                </tr>
            </table>
          </form>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>