<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="profile">
	
	<script type="text/javascript" language="javascript">

<![CDATA[

function resetProfileErrors()
{
	document.getElementById("errorMem").style.display = "none";
	document.getElementById("HolderrorMem").style.display = "block";
	document.getElementById("errorProMemNum").style.display = "none";
	
	document.getElementById("errorName").style.display = "none";	
	document.getElementById("HolderrorName").style.display = "block";
	
	document.getElementById("errorAddress").style.display = "none";	
	document.getElementById("HolderrorAddress").style.display = "block";
	
	document.getElementById("errorCity").style.display = "none";	
	document.getElementById("HolderrorCity").style.display = "block";
	
	document.getElementById("errorState").style.display = "none";
	document.getElementById("HolderrorState").style.display = "block";
	
	document.getElementById("errorZip").style.display = "none";
	document.getElementById("missingZip").style.display = "none";
	document.getElementById("errorServerZip").style.display = "none";
	document.getElementById("errorServerId").style.display = "none";
	document.getElementById("errorZipCanada").style.display = "none";
	document.getElementById("HolderrorZip").style.display = "block";
	
	document.getElementById("errorPhone").style.display = "none";
	document.getElementById("HolderrorPhone").style.display = "block";
	
	document.getElementById("errorAltphone").style.display = "none";
	document.getElementById("HolderrorAltphone").style.display = "block";
	
	document.getElementById("errorFax").style.display = "none";
	document.getElementById("HolderrorFax").style.display = "block";
	
	document.getElementById("errorEmail").style.display = "none";
	document.getElementById("HolderrorEmail").style.display = "block";
	
	document.getElementById("errorTerritory").style.display = "none";
	
}

function validateProServer()
{
		document.forms[0].action = "FloristMaintenanceServlet?action=validateProServer";
		showWaitMessage("outerContent", "wait", "Validating");
		document.forms[0].submit();
}

function validateProfile()
{
resetProfileErrors();

var eCount = 0;


var vfloristnum = floristMaint.memnum.value;
if(trim(vfloristnum).length == 0)
{
	document.getElementById("HolderrorMem").style.display = "none";
	document.getElementById("errorMem").style.display = "block";
	eCount++;
}

var vfloristname = floristMaint.name.value;
if(trim(vfloristname).length == 0)
{
	document.getElementById("HolderrorName").style.display = "none";
	document.getElementById("errorName").style.display = "block";
	eCount++;
}

var vfloristaddress = floristMaint.address.value;
if(trim(vfloristaddress).length == 0)
{
	document.getElementById("HolderrorAddress").style.display = "none";
	document.getElementById("errorAddress").style.display = "block";
	eCount++;
}

var vfloristcity = floristMaint.city.value;
if(trim(vfloristcity).length == 0)
{
	document.getElementById("HolderrorCity").style.display = "none";
	document.getElementById("errorCity").style.display = "block";
	eCount++;
	
}

var vfloriststate = floristMaint.state.value;
if(trim(vfloriststate).length == 0)
{
	document.getElementById("HolderrorState").style.display = "none";
	document.getElementById("errorState").style.display = "block";
	eCount++;
	
}

var vfloristzip = floristMaint.zipcode.value;
if((trim(vfloristzip).length == 0) && (vfloriststate != "NA"))
{
	document.getElementById("missingZip").style.display = "block";
	eCount++;
}

var count=0;
var len=0;

//3.	The format is 99-9999AA or 99-999999
// validate florist number
	if(trim(vfloristnum).length > 0)
	{
			count = 0;
			len = trim(vfloristnum).length;
			
			if(len != 9)
			{
				count++;
			}
			else
			{
				//validate the first 2...
				if(!isDigit(vfloristnum.charAt(0)))
						count++;
					
				if(!isDigit(vfloristnum.charAt(1)))
						count++;
			
				if(len == 9)
				{
					//99-9999AA 
					if(vfloristnum.charAt(2) != "-")
						count++;
					if(!isDigit(vfloristnum.charAt(3)))
						count++;
					if(!isDigit(vfloristnum.charAt(4)))
						count++;
					if(!isDigit(vfloristnum.charAt(5)))
						count++;
					if(!isDigit(vfloristnum.charAt(6)))
						count++;
					if(!isLetter(vfloristnum.charAt(7)))
						count++;
					if(!isLetter(vfloristnum.charAt(8)))
						count++;
				}
			} // end of else
		
		if(count > 0)
		{
		
			document.getElementById("errorProMemNum").style.display = "block";
			eCount++;
			count = 0;
		}
	}

var vzipcode = floristMaint.zipcode.value;
// validate zip code...
		if((trim(vzipcode).length > 0) && (vfloriststate != "NA"))
		{
		
			count = 0;
			len = trim(vzipcode).length;
			if(len == 10)
			{
				if(floristMaint.isNew.value == "Y")
				{
					for(var i=0; i<10; i++)
					{
						if(i==5)
						{
							if(vzipcode.charAt(i) != "-")
								count++;
							continue;
						}
						
						if(!isDigit(vzipcode.charAt(i)))
							count++;
					}
				}
				else
				{
					count++;
				}
			}
			else if(len < 3 || len > 7 || len==4 )
			{
				document.getElementById("errorZip").style.display = "block";
				count++;
			}
			else if(len == 5)
			{
				for(var i=0; i<5; i++)
				{
					if(!isDigit(vzipcode.charAt(i)))
						count++;
				}
			}

			if(count > 0)
			{
				document.getElementById("errorZip").style.display = "block";
				eCount++;
				count = 0;
			}
	
			if(len == 3)
			{
				if(!isLetter(vzipcode.charAt(0)))
					count++;
				if(!isDigit(vzipcode.charAt(1)))
					count++;
				if(!isLetter(vzipcode.charAt(2)))
					count++;

				if(count > 0)
				{
					document.getElementById("errorZipCanada").style.display = "block";
					eCount++;
					count = 0;
				}
			}

			// Validate Canadian ZipCode.
		
			if(len > 5 && len < 8)
			{
				if(!isLetter(vzipcode.charAt(0)))
					count++;
				if(!isDigit(vzipcode.charAt(1)))
					count++;
				if(!isLetter(vzipcode.charAt(2)))
					count++
				if(len == 7)
				{
					if(vzipcode.charAt(3) != "-")
						if(vzipcode.charAt(3) != " ")
							count++;
				}

				if(!isDigit(vzipcode.charAt(len-3)))
					count++;
				if(!isLetter(vzipcode.charAt(len-2)))
					count++
				if(!isDigit(vzipcode.charAt(len-1)))
					count++;
			}
		
			if(count > 0)
			{
				document.getElementById("errorZipCanada").style.display = "block";
				eCount++;
				count = 0;
			}
}

var vfloristphone = floristMaint.phone.value;
if(trim(vfloristphone).length == 0)
{
	document.getElementById("HolderrorPhone").style.display = "none";
	document.getElementById("errorPhone").style.display = "block";
	eCount++;

}


// validate phone number...
if(trim(vfloristphone).length > 0)
{
	len = trim(vfloristphone).length;
	var normalizedPhone = stripCharsInBag(vfloristphone, phoneNumberDelimiters)
	if (!isUSPhoneNumber(normalizedPhone, false))
	{
		document.getElementById("HolderrorPhone").style.display = "none";
		document.getElementById("errorPhone").style.display = "block";
		eCount++;
	}
}

/*
var vfloristaltphone = floristMaint.altphone.value;
if(trim(vfloristaltphone).length > 0)
{

	len = trim(vfloristaltphone).length;
	var normalizedPhone = stripCharsInBag(vfloristaltphone, phoneNumberDelimiters)
	if (!isUSPhoneNumber(normalizedPhone, false))
	{	
		document.getElementById("HolderrorAltphone").style.display = "none";
		document.getElementById("errorAltphone").style.display = "block";
		eCount++;
	}
}

var vfloristfax = floristMaint.fax.value;
if(trim(vfloristfax).length > 0)
{
	len = trim(vfloristfax).length;
	var normalizedPhone = stripCharsInBag(vfloristfax, phoneNumberDelimiters)
	if (!isUSPhoneNumber(normalizedPhone, false))
	{
		document.getElementById("HolderrorFax").style.display = "none";
		document.getElementById("errorFax").style.display = "block";
		eCount++;
	}
}
*/

// validate email
var vfloristemail = floristMaint.email.value;

if(trim(vfloristemail).length > 0)
{
var emailCheck = isEmail (vfloristemail);

if(!emailCheck)
{
document.getElementById("HolderrorEmail").style.display = "none";
document.getElementById("errorEmail").style.display = "block";
eCount++;
}

}

// validate territory...
var vfloristterritory = trim(floristMaint.territory.value);

if(vfloristterritory.length > 0)
{

if(vfloristterritory.length != 2)
{
document.getElementById("errorTerritory").style.display = "block";
eCount++;
}
else
{
	if(!isDigit(vfloristterritory.charAt(0)))
	{
		document.getElementById("errorTerritory").style.display = "block";
		eCount++;
	}
	if(!isDigit(vfloristterritory.charAt(1)))
	{
		document.getElementById("errorTerritory").style.display = "block";
		eCount++;
	}
}

}

if(eCount > 0)
	return false;
else
	return true;

} // end of the validateProfile() function.

 function CheckLength()
 {
	  if (floristMaint.address.value.length <= 60)
	  {
			floristMaint.address.focus();
	  }
	  else
	  {
	  document.getElementById("headText").focus();
	  }
 }
              

]]>
</script>
		<tr bordercolor="white">
			<td>
				<table cellspacing="2" cellpadding="1" border="0">
					<tr>
						<td class="label">
						<xsl:if test="floristData/generalData/vendorFlag and floristData/generalData/vendorFlag='Y'">
							Vendor
						</xsl:if>
						<xsl:if test="key('processFlow','processType')/value='AddNew' and key('securityPermissions', 'vendorFlagUpdate')/value='Y'">
							Vendor
						</xsl:if>
						</td>
						<td>
							<xsl:if test="floristData/generalData/vendorFlag and floristData/generalData/vendorFlag='Y'">
							<input type="checkbox" checked="true" disabled="true"/>
							</xsl:if>
							<xsl:if test="key('processFlow','processType')/value='AddNew' and key('securityPermissions', 'vendorFlagUpdate')/value='Y'">
							<input type="checkbox" checked="true" disabled="true"/>
							</xsl:if>
						</td>
						<td></td>
						<td></td>

						<td>
							<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
							<xsl:if test="$goto = 'Y'">
								GoTo Florist
							</xsl:if>
						</td>
					</tr>


					<tr >
						<td class="label"> Florist # </td>
						<td>
							<input type="text" name="memnum" tabindex="-1" onchange="idChange();" size="9"  maxlength="9"  readonly="true" value="{floristData/generalData/memberNumber}"/>
						</td>
						
						<td id="errorServerId" class="RequiredFieldTxt" style="display:none">
							&nbsp; &nbsp; Florist Number Exists.
						</td>
						
						<td align="left" id="errorMem" class="RequiredFieldTxt" style="display:none">
												Required Field
					 	</td>
					 	<td id="HolderrorMem" style="display:block">
												&nbsp; &nbsp;
					 	</td>
					
					 	<td id="errorProMemNum" class="RequiredFieldTxt" style="display:none">
							 Invalid Format
						</td>
						
                                                <xsl:choose>
                                                    <xsl:when test="key('securityPermissions', 'directoryCityUpdate')/value != 'Y'">
                                                        <td class="label" id="profileStatus" > Status </td>
                                                        <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" />
                                                        <input type="hidden" tabindex="-1" name="prostatus" size="16" readonly="true" value="{floristData/generalData/status}"></input>
                                                </td>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td class="label" id="profileStatus" colspan="2">
                                                                <xsl:choose>
                                                                        <xsl:when test="floristData/generalData/status = 'Blocked'">
                                                                                <input type="hidden" name="prostatus" value="{floristData/generalData/status} - {floristData/generalData/floristBlockUser} - {floristData/generalData/floristBlockStartDate}"></input>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                                <input type="hidden" name="prostatus" value="{floristData/generalData/status}"></input>
                                                                        </xsl:otherwise>
                                                                </xsl:choose>
                                                        </td>
                                                    </xsl:otherwise>
                                                </xsl:choose>
					</tr>
					<tr>
						<td class="label"> Name </td>
						<td>
							<input type="text" name="name" size="30" tabindex="2" maxlength="45" value="{floristData/generalData/name}"></input>
						</td>
	
						<td align="left" id="errorName" class="RequiredFieldTxt" style="display:none">
												Required Field 
					 	</td>
					 	<td id="HolderrorName" style="display:block">
												&nbsp; &nbsp;
					 	</td>
						<td class="label" id="profileLastUpdated"> Last Updated </td>
						<td>
							<input tabindex="-1" type="text" name="lastupdated" size="16" readonly="true"  maxlength="10" value="{floristData/generalData/lastUpdateDate}"></input>
						</td>
						
					</tr>
					<tr>
						<td class="label"> Address </td>
						<td>
							<textarea name="address" onkeypress="CheckLength();" wrap="physical" cols="32" rows="2" tabindex="3" >
								<xsl:value-of select="floristData/generalData/address"/>
							</textarea>
						</td>
						<td align="left" id="errorAddress" class="RequiredFieldTxt" style="display:none">
												Required Field
					 	</td>
					 	<td id="HolderrorAddress" style="display:block">
												&nbsp; &nbsp;
					 	</td>
					</tr>
					<tr>
						<td class="label"> City </td>
						<td>
							<input type="text" name="city" size="30" tabindex="4"  maxlength="45" value="{floristData/generalData/city}"/>
							
						</td>
						<td align="left">
						<font class="label">&nbsp;&nbsp;&nbsp;State/Pro
							</font>
							<xsl:variable name="scom" select="floristData/generalData/state"/>
							<select name="state" tabindex="5">
								<option value="">Select...</option>
								<xsl:for-each select="stateList/state">
									<xsl:variable name="sid" select="statemasterid" />
									<option value="{statemasterid}">
										<xsl:if test="$scom = $sid">
											<xsl:attribute name="SELECTED" />
										</xsl:if>
										<xsl:value-of select="statemasterid" />
									</option>
								</xsl:for-each>
							</select>
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						</td>
						<td class="label"> Zip </td>
						<td>
							<input type="text" name="zipcode" size="20" tabindex="6" onchange="zipChange();"  maxlength="11" value="{floristData/generalData/postalCode}"></input>
						</td>
			
						<td id="errorServerZip" class="RequiredFieldTxt" style="display:none">
							&nbsp; &nbsp; Zip Code is not valid
						</td>

					<td align="left" id="missingZip" class="RequiredFieldTxt" style="display:none">
												Required Field
					 	</td>
					 	
						<td id="errorZip" class="RequiredFieldTxt" style="display:none">
							&nbsp; &nbsp; Invalid Format
						</td>
						<td id="errorZipCanada" class="RequiredFieldTxt" style="display:none">
							&nbsp; &nbsp; Invalid Format A9A or A9A-9A9
						</td>
						<td id="HolderrorZip" style="display:block">
						&nbsp; &nbsp;
			 			</td>
						
					</tr>
					<tr>
					<td></td>
					<td align="left" id="errorCity" class="RequiredFieldTxt" style="display:none">
												Required Field
			 		</td>
			 		
			 		<td id="HolderrorCity" style="display:block">
												&nbsp; &nbsp;
			 		</td>
			 		
			 		<td align="center" id="errorState" class="RequiredFieldTxt" style="display:none">
												Required Field
				 	</td>
				 	
				 	<td id="HolderrorState" style="display:block">
												&nbsp; &nbsp;
			 		</td>
			 		
					
					</tr>
					<tr>
						<td class="label"> Phone </td>
						<td>
							<input type="text" name="phone" size="30" tabindex="7"  maxlength="20" value="{floristData/generalData/phone}"></input>
						</td>
						<td align="left" id="errorPhone" class="RequiredFieldTxt" style="display:none">
												Invalid Phone
				 		</td>

				 	<td id="HolderrorPhone" style="display:block">
												&nbsp; &nbsp;
			 		</td>
						<td class="label">Alt Phone</td>
						<td>
							<input type="text" name="altphone" size="20" tabindex="8"  maxlength="20" value="{floristData/generalData/altPhone}"></input>
						</td>
						
					<td align="left" id="errorAltphone" class="RequiredFieldTxt" style="display:none">
												Invalid Phone
				 	</td>

				 	<td id="HolderrorAltphone" style="display:block">
												&nbsp; &nbsp;
			 		</td>
			 		
					</tr>
					<tr>
						<td class="label"> Fax </td>
						<td>
							<input type="text" name="fax" size="30" tabindex="9"  maxlength="20" value="{floristData/generalData/fax}"></input>
						</td>
						
						<td align="left" id="errorFax" class="RequiredFieldTxt" style="display:none">
												Invalid Fax
				 		</td>

					 	<td id="HolderrorFax" style="display:block">
													&nbsp; &nbsp;
				 		</td>
					
						<td class="label"> Email </td>
						<td>
							<input type="text" name="email" size="20" tabindex="10"  maxlength="55" value="{floristData/generalData/emailAddress}"></input>
						</td>
						<td align="left" id="errorEmail" class="RequiredFieldTxt" style="display:none">
												Invalid Email
				 		</td>

					 	<td id="HolderrorEmail" style="display:block">
													&nbsp; &nbsp;
				 		</td>
					</tr>
					<tr></tr>
					<tr></tr>
					<tr></tr>
					<tr>
						<td class="label"> Contact </td>
						<td>
							<input type="text" tabindex="-1" name="contact" size="30" readonly="true"  maxlength="45" value="{floristData/generalData/contactName}"></input>
						</td>
						<td></td>
						<td class="label"> Territory </td>
						<td>
							<input type="text" tabindex="-1" name="territory" size="20" readonly="true"  maxlength="2" value="{floristData/generalData/territory}"></input>
						</td>
						<td align="left" id="errorTerritory" class="RequiredFieldTxt" style="display:none">
												Invalid Format
				 		</td>
						
					</tr>
					<tr></tr>
					<tr></tr>
					<tr></tr>
                                        <xsl:if test="key('securityPermissions', 'directoryCityUpdate')/value != 'Y'">
					<tr>
						<td class="label" id="profileWeight">Weight</td>
						<td>
						
						
					<xsl:choose>
						<xsl:when test="key('securityPermissions', 'overrideWeightView')/value = 'Y'">
							<xsl:choose>
							<xsl:when test="string-length(floristData/generalData/adjustedWeight) &gt; 0">
								<input type="text" tabindex="-1" name="weight" size="12" readonly="true"  maxlength="3" value="{floristData/generalData/adjustedWeight}"></input>
							  </xsl:when>
							  <xsl:otherwise>
								  <input type="text" tabindex="-1" name="weight" size="12" readonly="true"  maxlength="3" value="{floristData/generalData/finalWeight}"></input>
							  </xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
						<input type="text" tabindex="-1" id="wt" name="weight" size="12" readonly="true"  maxlength="3" value="{floristData/generalData/finalWeight}"></input>
						<script>
						var wgt = '<xsl:value-of select="floristData/generalData/finalWeight"/>';
						if(wgt.length > 0)
						{
						if(wgt > 12)
							document.getElementById("wt").value = 12;
						}
						</script>
						</xsl:otherwise>
					</xsl:choose>
						</td>
					</tr>
                                        </xsl:if>
					<tr>
						<td class="label" id="profileZips"> Zips </td>
						<td>
							<input type="text" tabindex="-1" name="zips" size="12" readonly="true"  maxlength="4" value="{floristData/generalData/postalCodeCount}"></input>
						</td>
					</tr>
                                        <xsl:if test="key('securityPermissions', 'directoryCityUpdate')/value != 'Y'">
					<tr>
						<td class="label" id="profileFtdi">FTDI Sending Rank </td>
						<td>
							<input tabindex="-1" type="text" name="ftdi" size="12" readonly="true"  maxlength="9"></input>
						</td>
					</tr>
                                        </xsl:if>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<p align="center">
					<input type="button" align="center" value=" SAVE " name="savepro" size="10" tabindex="13" onClick="saveAction();"></input>
				</p>
				
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
