
<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="floristQueue">
		<xsl:variable name="codifiedId" select="viewQueueData/generalData/productCodifiedId" />
		<xsl:variable name="floristGotoFlag" select="viewQueueData/generalData/floristGotoFlag" />
		<script type="text/javascript" language="javascript"><![CDATA[

	function dChanges()
	{
		document.forms[0].saveNeeded.value = "yes";
		document.getElementById("dchange").value = "";
	}
	
	
	function markProductReadonly(productIndex)
	{
		document.getElementById('codblock' + productIndex).disabled = true;
		if(document.getElementById('codunblockdate' + productIndex))
		{
			document.getElementById('codunblockdate' + productIndex).readOnly = true;
		}
		
		if(document.getElementById('calendar' + productIndex))
		{
			document.getElementById('calendar' + productIndex).disabled = true;
		}
	}

	function validateFloristQueue()
	{

		document.getElementById("errorDate").style.display = "none";
		document.getElementById("errorUnblock").style.display = "none";
		document.getElementById("missingReason").style.display = "none";
		document.getElementById("errorholdUnblock").style.display = "block";
			
		var uDate;
		var dateArray = new Array();
		var today = new Date();
		var count = 0;
		
		if(document.getElementById("codblocking").value != '') {
			if(floristMaint.reason_comment.value == "") {
				count++;
				document.getElementById("missingReason").style.display = "block";
			}
			if(floristMaint.codblockdate.value == "")	{
			    document.getElementById("errorDate").innerHTML = 'Start Date is required.';
				document.getElementById("errorDate").style.display = "block";
				count++;
			} else {
    			if(floristMaint.codunblockdate.value == "")	{
    			    document.getElementById("errorDate").innerHTML = 'End Date is required.';
	    			document.getElementById("errorDate").style.display = "block";
		    		count++;
			    }
			}
	
    		document.getElementById('codunblockdate').style.backgroundColor = 'white';

	    	if(count > 0)
		    	return false;
		
	    	if(document.getElementById("dchange").value != floristMaint.codunblockdate.value) {
		
				uDate = floristMaint.codblockdate.value;
				dateArray = new Array();
				if(uDate.length > 0) {
					dateArray = uDate.split("/");
	
					if(dateArray.length == 2)
					{
						floristMaint.codblockdate.value += "/" + today.getYear();
						uDate = floristMaint.codblockdate.value;
					}
	
					dateArray = uDate.split("/");
	
					if(dateArray.length != 3)
					{
						document.getElementById('codblockdate').style.backgroundColor = 'pink';
           			    document.getElementById("errorDate").innerHTML = 'Invalid Start Date.';
						document.getElementById("errorDate").style.display = "block";
						count++;
					}
					else
					{
						if(dateArray[2].length == 2)
						{
							dateArray[2] = "20" + dateArray[2];
							floristMaint.codblockdate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
						}
					}
							
					if(dateArray[2] != null)
					{
						if(dateArray[2].length != 2)
						{
							if(dateArray[2].length != 4)
							{
								document.getElementById('codblockdate').style.backgroundColor = 'pink';
                   			    document.getElementById("errorDate").innerHTML = 'Invalid Start Date.';
								document.getElementById("errorDate").style.display = "block";
								count++;
							}
							else
							{
							    if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
								{
								     document.getElementById('codblockdate').style.backgroundColor = 'pink';
                              	     document.getElementById("errorDate").innerHTML = 'Invalid Start Date.';
								     document.getElementById("errorDate").style.display = "block";
									 count++;
								}
							}
						}
					}
					
				}
						
				uDate = floristMaint.codunblockdate.value;
				dateArray = new Array();
				if(uDate.length > 0) {
					dateArray = uDate.split("/");
	
					if(dateArray.length == 2)
					{
						floristMaint.codunblockdate.value += "/" + today.getYear();
						uDate = floristMaint.codunblockdate.value;
					}
	
					dateArray = uDate.split("/");
	
					if(dateArray.length != 3)
					{
						document.getElementById('codunblockdate').style.backgroundColor = 'pink';
           			    document.getElementById("errorDate").innerHTML = 'Invalid End Date.';
						document.getElementById("errorDate").style.display = "block";
						count++;
					}
					else
					{
						if(dateArray[2].length == 2)
						{
							dateArray[2] = "20" + dateArray[2];
							floristMaint.codunblockdate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
						}
					}
							
					if(dateArray[2] != null)
					{
						if(dateArray[2].length != 2)
						{
							if(dateArray[2].length != 4)
							{
								document.getElementById('codunblockdate').style.backgroundColor = 'pink';
                   			    document.getElementById("errorDate").innerHTML = 'Invalid End Date.';
								document.getElementById("errorDate").style.display = "block";
								count++;
							}
							else
							{
							    if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
								{
								     document.getElementById('codunblockdate').style.backgroundColor = 'pink';
                              	     document.getElementById("errorDate").innerHTML = 'Invalid End Date.';
								     document.getElementById("errorDate").style.display = "block";
									 count++;
								}
							}
						}
					}
					
				}
						
    			if(count > 0)
	    			return false;
	
		    	var iStartDate = Date.parse(floristMaint.codblockdate.value);
			    var iEndDate = Date.parse(floristMaint.codunblockdate.value);
    			var todayDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());
	
	    		iStartDate = iStartDate / 86400000;
		    	iEndDate = iEndDate / 86400000;
			    todayDate = todayDate / 86400000;
	
    			var diff1 = iStartDate - todayDate;
	    		var diff2 = iEndDate - todayDate;
		    	var diff3 = iEndDate - iStartDate;
	
    	        var block_type = document.getElementById('codblocking').value;
	    		if(block_type == 'S') // soft...
		    	{
			    	if(diff1 < 0)
				    {
                        document.getElementById("errorDate").innerHTML = 'Start Date must be equal to or after the current date.';
    					document.getElementById("errorDate").style.display = "block";
	    				count++;
		    		}
			    	else if(diff2 < 0)
				    {
                        document.getElementById("errorDate").innerHTML = 'End Date must be equal to or after the current date.';
    					document.getElementById("errorDate").style.display = "block";
	    				count++;
		    		}
			    	else if(diff3 < 0)
				    {
                        document.getElementById("errorDate").innerHTML = 'Start Date must be equal to or before the End Date.';
    					document.getElementById("errorDate").style.display = "block";
	    				count++;
		    		}
			    	else if(diff3 > 30)
				    {
                        document.getElementById("errorDate").innerHTML = 'Date range must be less than or equal to 30 days.';
    					document.getElementById("errorDate").style.display = "block";
	    				count++;
		    		}
			    	else if(diff1 > 30)
				    {
                        document.getElementById("errorDate").innerHTML = 'Start Date must be less than or equal to 30 days after the current date.';
    					document.getElementById("errorDate").style.display = "block";
	    				count++;
		    		}
			    }
			    var blockLoop = document.getElementById('totalblocks').value;
			    for (var i=0; i<blockLoop; i++) {
			        blockStart = blockStartDates[i];
			        blockEnd = blockEndDates[i];
        			var checkStartDate = Date.parse(blockStart);
        			var checkEndDate = Date.parse(blockEnd);
	
			        checkStartDate = checkStartDate / 86400000;
			        checkEndDate = checkEndDate / 86400000;
	
    			    var diff1 = iStartDate - checkEndDate;
    	    		var diff2 = iEndDate - checkStartDate;
			        if (diff1 <= 0) {
			            if (diff2 >= 0) {
                            document.getElementById("errorDate").innerHTML = 'Date range must not overlap with any existing blocks.';
        					document.getElementById("errorDate").style.display = "block";
	        				count++;
	        			}
			        }
			    }
		    }
		}

        if(count > 0)
			return false;
		
	if(document.getElementById('product_blocked_flag') != null)
	{
		if(document.getElementById('product_blocked_flag').checked == true)
		{
				if(document.getElementById('product_unblock_date').value == "")
				{
					document.getElementById('product_unblock_date').style.backgroundColor = 'pink';
					document.getElementById("errorUnblock").style.display = "block";
					document.getElementById("errorholdUnblock").style.display = "none";
					count++;
				}
				
				else
				{

					
					var uDate = floristMaint.product_unblock_date.value;
	
					if(uDate.length > 0)
					{
						dateArray = uDate.split("/");
						if(dateArray.length == 2)
						{
							floristMaint.product_unblock_date.value += "/" + today.getYear();
							uDate = floristMaint.product_unblock_date.value;
						}
	
						dateArray = uDate.split("/");
	
						if(dateArray.length != 3)
						{
							document.getElementById('product_unblock_date').style.backgroundColor = 'pink';
							document.getElementById("errorUnblock").style.display = "block";
							document.getElementById("errorholdUnblock").style.display = "none";
							count++;
						}
						else
						{
							if(dateArray[2].length == 2)
							{
								dateArray[2] = "20" + dateArray[2];
								floristMaint.product_unblock_date.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
							}
						}
	
	
						if(dateArray[2] != null)
						{
							if(dateArray[2].length != 2)
							{
								if(dateArray[2].length != 4)
								{
									document.getElementById('product_unblock_date').style.backgroundColor = 'pink';
									document.getElementById("errorUnblock").style.display = "block";
									document.getElementById("errorholdUnblock").style.display = "none";
									count++;
								}
								else
								{
									if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
							     	{
								     	document.getElementById('product_unblock_date').style.backgroundColor = 'pink';
										document.getElementById("errorUnblock").style.display = "block";
										document.getElementById("errorholdUnblock").style.display = "none";
										count++;
					     			}
								}
	
							}
						}
	
						iDate = Date.parse(document.getElementById('product_unblock_date').value);
		
						todayDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());
		
						iDate = iDate / 86400000;
						todayDate = todayDate / 86400000;
		
						var diff = iDate - todayDate;
		
						if(diff <= 0)
						{
							document.getElementById('product_unblock_date').style.backgroundColor = 'pink';
							document.getElementById("errorUnblock").style.display = "block";
							document.getElementById("errorholdUnblock").style.display = "none";
							count++;
						}
						
					} //uDate.length
					
				} // else
				
			
			}
		
		}
		
		
		// check all of the category un-block dates...
		
		for( var i=1; i <= floristMaint.totalcod.value; i++)
		{

		if(document.getElementById('codblock' + i) != null) {
			if(document.getElementById('codunblockdate' + i) != null)
			{
				document.getElementById('codunblockdate' + i).style.backgroundColor = 'white';
				uDate = document.getElementById('codunblockdate' + i).value;

				if(uDate.length > 0)
				{
					dateArray = uDate.split("/");

					if(dateArray.length == 2)
						document.getElementById('codunblockdate' + i).value += "/" + today.getYear();
				}
			}
		}
		}
		
		for( var i=1; i <= floristMaint.totalcod.value; i++)
		{

			if(document.getElementById('codblock' + i) != null)
			{
				if(document.getElementById('codblock' + i).checked == true)
				{
					if(document.getElementById('codunblockdate' + i) != null)
					{
						if(document.getElementById('codunblockdate' + i).value == "")
						{
							document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
							document.getElementById("errorUnblock").style.display = "block";
							document.getElementById("errorholdUnblock").style.display = "none";
							count++;
						}
						else
						{
						
							dateArray = document.getElementById('codunblockdate' + i).value.split("/");
		
							var inMonth = stripCharsInBag (dateArray[0], "0");
		
							if(dateArray.length != 3)
							{
									document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
									document.getElementById("errorUnblock").style.display = "block";
									document.getElementById("errorholdUnblock").style.display = "none";
									count++;
							}
							else
							{
								if(dateArray[2].length == 2)
								{
									dateArray[2] = "20" + dateArray[2];
									document.getElementById('codunblockdate' + i).value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
								}
							}
							if(dateArray[2] != null)
							{
								if(dateArray[2].length != 2)
								{
									if(dateArray[2].length != 4)
									{
									document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
									document.getElementById("errorUnblock").style.display = "block";
									document.getElementById("errorholdUnblock").style.display = "none";
									count++;
									}
									else
									{
										if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
								     	{
									     	document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
											document.getElementById("errorUnblock").style.display = "block";
											document.getElementById("errorholdUnblock").style.display = "none";
											count++;
						     			}
									}
									
								}
							}
					
	
							iDate = Date.parse(document.getElementById('codunblockdate' + i).value);
	
							todayDate = Date.parse((today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear());
	
							iDate = iDate / 86400000;
							todayDate = todayDate / 86400000;
	
							var diff = iDate - todayDate;
	
							if(diff <= 0)
							{
								document.getElementById('codunblockdate' + i).style.backgroundColor = 'pink';
								document.getElementById("errorUnblock").style.display = "block";
								document.getElementById("errorholdUnblock").style.display = "none";
								count++;
							}
	
						}
					}
				}
			}
		} // end of for loop
		
		if(count > 0)
			return false;
		else
			return true;
		
	} // end of the validation of the Florist Queue Function
	
function permBlockCheck(tempNum) {
    if (document.getElementById('codpermblock' + tempNum).checked) {
        document.getElementById('codblock' + tempNum).checked = false;
    }
    // enSave();
}

function tempBlockCheck(tempNum) {
    if (document.getElementById('codblock' + tempNum).checked) {
        document.getElementById('codpermblock' + tempNum).checked = false;
    }
    // enSave();
}	
      
]]></script>
		<tr>
			<td>
				<table width="100%" >
					<tr>
						<td width="20%" align="right" class="label">Order Number</td>
						<td width="10%">
							<input type="text" readonly="true" value="{viewQueueData/generalData/orderNumber}" tabindex="-1"/>
						</td>
						<td width="10%" ></td>
						<td width="10%" align="center">
							<button style="font-size: 8pt;" name="fSave2" onclick="javascript:doSaveAction();" tabindex="91">&nbsp;&nbsp;
								<span style="text-decoration:underline;">S</span>ave&nbsp;&nbsp;
							</button>&nbsp;&nbsp;

							<button style="font-size: 8pt;" name="fRemove2" onclick="javascript:doRemoveAction();" tabindex="92">
								<span style="text-decoration:underline;">R</span>emove
							</button>&nbsp;&nbsp;

							<button style="font-size: 8pt;"  onclick="javascript:doNextAction();" tabindex="93">&nbsp;&nbsp;
								<span style="text-decoration:underline;">N</span>ext&nbsp;&nbsp;
							</button>&nbsp;&nbsp;
						
						
						</td>
						<td width="10%" align="right" class="label">Count &nbsp;</td>
						<td width="10%" align="left">
							<input type="text" readonly="true"  tabindex="-1" value="{viewQueueData/generalData/messageCount}"/>
						</td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
					</tr>
					<tr>
						<td>
							<br></br>
						</td>
					</tr>
					<tr>
						<td  colspan="9" class="tblheader" align="left"> Florist Information </td>
					</tr>
					<tr>
						<td>
							<br></br>
						</td>
					</tr>
					<tr>
						<td width="20%" align="right" class="label">Florist #</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristNumber}"/>
						</td>
						<td width="10%" align="right" class="label"></td>
						<td width="10%" align="left"></td>
						<td width="10%" align="right" class="label"></td>
						<td width="10%" align="left"></td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td></td>
					</tr>
					<tr>
						<td width="20%" align="right" class="label">Florist Name</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristName}"/>
						</td>
						<td width="10%" align="right" class="label">GoTo</td>
						<td width="10%" align="left">
							<input type="text" readonly="true" tabindex="-1" id="gotoid" value="{viewQueueData/generalData/floristGotoFlag}"/>
						</td>
						<td width="10%" align="right" class="label">Status</td>
						<td width="10%" align="left" colspan="3" rowspan="2">
						    <textarea tabindex="-1" readonly="true" wrap="physical" cols="75" rows="3" >
    	    					<xsl:value-of select="viewQueueData/generalData/floristStatus"/>
	    				 	</textarea>
						</td>
						<td></td>
					</tr>
					<tr>
						<td width="20%" align="right" class="label">City</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristCity}"/>
						</td>
						<td width="10%" align="right" class="label">State</td>
						<td width="10%" align="left">
							<input type="hidden" name="dchange" value="{viewQueueData/generalData/blockEndDate}"/>
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/floristState}"/>
						</td>
					</tr>
					<tr bordercolor="white">
						<td>
							<br></br>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td  colspan="9" class="tblheader" align="left"> Florist Blocking
				<xsl:if test="viewQueueData/generalData/floristGotoFlag = 'Y'">
						&nbsp; &nbsp; (blocking of goto florists is not allowed)
				</xsl:if>
			</td>
		</tr>
		<tr bordercolor="white">
			<td>
				<table  border="0" align="left" width="100%">
					<tr>
						<td width="15%" class="tblheader" align="center">Block Type</td>
						<td width="15%" class="tblheader" align="center">Start Date</td>
						<td width="15%" class="tblheader" align="center">End Date</td>
						<td width="25%" class="tblheader" align="center">Reason</td>
						<td width="15%" class="tblheader" align="center">User</td>
						<td width="15%" class="tblheader" align="center">Remove</td>
					</tr>
					<script>
                        var blockStartDates = new Array(<xsl:for-each select="viewQueueData/floristBlockList/floristBlock">
                            ["<xsl:value-of select="blockStartDate"/>"]
                            <xsl:choose>
                                <xsl:when test="position()!=last()">,</xsl:when>
                            </xsl:choose>
                        </xsl:for-each>);
                        var blockEndDates = new Array(<xsl:for-each select="viewQueueData/floristBlockList/floristBlock">
                            ["<xsl:value-of select="blockEndDate"/>"]
                            <xsl:choose>
                                <xsl:when test="position()!=last()">,</xsl:when>
                            </xsl:choose>
                        </xsl:for-each>);
					</script>
					<xsl:for-each select="viewQueueData/floristBlockList/floristBlock">
					    <tr>
					        <td align="center">
					            <xsl:choose>
					                <xsl:when test="blockType = 'S'">Soft</xsl:when>
					                <xsl:when test="blockType = 'H'">Hard</xsl:when>
					                <xsl:when test="blockType = 'O'">Opt Out</xsl:when>
					                <xsl:otherwise>Other</xsl:otherwise>
					            </xsl:choose>
					        </td>
					        <td align="center">
					            <xsl:value-of select="blockStartDate" />
					        </td>
					        <td align="center">
					            <xsl:value-of select="blockEndDate" />
					        </td>
					        <td align="center">
					            <xsl:value-of select="blockReason" />
					        </td>
					        <td align="center">
					            <xsl:value-of select="blockUser" />
					        </td>
					        <td align="center">
					            <xsl:if test="$floristGotoFlag != 'Y'">
					                <xsl:if test="blockType = 'S'">
    					                <input type="checkbox" name="removeblock{position()}"/>
	    				                <input type="hidden" name="removestartdate{position()}" value="{blockStartDate}" />
					                </xsl:if>
					            </xsl:if>
					        </td>
					    </tr>
					</xsl:for-each>
         			<input type="hidden" name="totalblocks" value="{count(viewQueueData/floristBlockList/floristBlock/blockType)}" />
         			<input type="hidden" name="gotofloristflag" value="{$floristGotoFlag}" />
    					<tr id="blockDiv">
	    					<td align="center">
							    <select name="codblocking" tabindex="1">
							        <option value="">Select...</option>
							        <option value="S">Soft</option>
							    </select>
    						</td>
    						<td align="center">
	    						<input name="codblockdate" type="text" tabindex="2" size="15" maxlength="10" value="{viewQueueData/generalData/blockStartDate}"/>
						    	&nbsp;&nbsp;
							    <input type="image" tabindex="3" id="calendarcod" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
				    		</td>
					    	<script><![CDATA[
										 Calendar.setup(
										    {
										      inputField  : "codblockdate",  // ID of the input field
										      ifFormat    : "mm/dd/y",  // the date format
										      button      : "calendarcod" // ID of the button
										    }
										  );
									]]></script>
    						<td align="center">
	    						<input name="codunblockdate" type="text" tabindex="2" size="15" maxlength="10" value="{viewQueueData/generalData/defaultUnblockDate}"/>
						    	&nbsp;&nbsp;
							    <input type="image" tabindex="3" id="calendarcodunblock" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
				    		</td>
					    	<script><![CDATA[
										 Calendar.setup(
										    {
										      inputField  : "codunblockdate",  // ID of the input field
										      ifFormat    : "mm/dd/y",  // the date format
										      button      : "calendarcodunblock" // ID of the button
										    }
										  );
									]]></script>
    						<td align="center">
    							<xsl:variable name="reason_text" select="viewQueueData/generalData/reasonComment"/>
	    						<input type="hidden" name="previous_reason_comment" value="{$reason_text}"/>
		    					<select name="reason_comment" tabindex="4">
			    					<option value="">Select...</option>
				    				<option value="Out of Product">
					    				<xsl:if test="$reason_text = 'Out of Product'">
						    				<xsl:attribute name="SELECTED" />
							    		</xsl:if>
										1 - Out of Product
    								</option>
	    							<option value="Not Taking Any More Orders">
		    							<xsl:if test="$reason_text = 'Not Taking Any More Orders'">
			    							<xsl:attribute name="SELECTED" />
				    					</xsl:if>
										2 - Not Taking Any More Orders
					    			</option>
						    		<option value="Mercury Not Responding">
							    		<xsl:if test="$reason_text = 'Mercury Not Responding'">
								    		<xsl:attribute name="SELECTED" />
									    </xsl:if>
										3 - Mercury Not Responding
    								</option>
	    							<option value="Other">
		    							<xsl:if test="$reason_text = 'Other'">
			    							<xsl:attribute name="SELECTED" />
				    					</xsl:if>
										4 - Other
					    			</option>
						    	</select>
						    </td>
    					</tr>
 					<tr>
  						<td colspan="3" align="left" id="errorDate" class="RequiredFieldTxt" style="display:none">
							Invalid date.
    					</td>
		    			<td align="left" id="missingReason" class="RequiredFieldTxt" style="display:none">
							Reason is required.
					    </td>
				    </tr>
				</table>
			</td>
		</tr>
		<tr  bordercolor="white">
			<td>
				<br></br>
			</td>
		</tr>
		<tr>
			<td>
				<table>
					<tr>
						<td  colspan="9" class="tblheader" align="left"> Product Information </td>
					</tr>
					<tr  bordercolor="white">
						<td>
							<br></br>
						</td>
					</tr>
					<tr>
						<td width="20%" align="right" class="label">Product Id</td>
						<td width="10%">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productId}"/>
						</td>
						<td width="10%" align="right" class="label">Blocked</td>
						<td width="10%" align="left">
							<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productBlockFlag}"/>
						</td>
						<td width="10%" align="right" class="label">Unblock Date</td>
						<xsl:choose>
							<xsl:when test="viewQueueData/generalData/productUnblockDate = ''">
								<td width="10%" align="left">
									<input type="text" readonly="true" tabindex="-1" value="N/A"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td width="10%" align="left">
									<input type="text" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productUnblockDate}"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</tr>
					<tr>
						<td width="20%" align="right" class="label">Product Name</td>
						<td width="10%" colspan="3">
							<input type="text" size="74" readonly="true" tabindex="-1" value="{viewQueueData/generalData/productName}"/>
						</td>
						<td width="10%" align="right" class="label">Codified</td>
						<td width="10%" align="left">
							<xsl:choose>
								<xsl:when test="viewQueueData/generalData/productCodifiedId = ''">
									<input type="text" readonly="true" tabindex="-1" value="N/A"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="viewQueueData/codifiedProductList/codifiedProduct[id = $codifiedId] != ''">
											<input type="text" readonly="true" tabindex="-1" value="Y"/>
										</xsl:when>
										<xsl:otherwise>
											<input type="text" readonly="true" tabindex="-1" value="N"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td width="10%"></td>
					</tr>
					<tr  bordercolor="white">
						<td>
							<br></br>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td id="errorUnblock" align="center" width="50%" class="RequiredFieldTxt" style="display:none">
		The highlighted fields are required or invalid.
			</td>
			<td id="errorholdUnblock" align="center" width="50%" class="RequiredFieldTxt" style="display:block"></td>
		</tr>
		<tr>
			<td>
				<table width="100%">
					<xsl:if test="$codifiedId != '' and viewQueueData/codifiedProductList/codifiedProduct[id = $codifiedId] != ''">
						<input name="product_codified_id" type="hidden" value="{$codifiedId}"/>
						<tr>
							<td class="tblheader" align="left">Product Blocking</td>
							<td class="tblheader" align="center">Block</td>
							<td class="tblheader" align="center">Unblock Date</td>
							<td class="tblheader" align="center"></td>
							<td class="tblheader" align="center" colspan="2"></td>
						</tr>
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td>														</td>
										<td width="10%"></td>
										<td>
											<xsl:value-of select="viewQueueData/codifiedProductList/codifiedProduct[id = $codifiedId]/name"/> &nbsp; | &nbsp;
											<xsl:value-of select="$codifiedId"/> &nbsp; | &nbsp;
											<xsl:value-of select="viewQueueData/generalData/productId"/>
										</td>
									</tr>
								</table>
							</td>
							<td align="center">
								<input type="checkbox" name="product_blocked_flag" tabindex="5">
									<xsl:if test="viewQueueData/codifiedProductList/codifiedProduct[id = $codifiedId]/blockedFlag = 'Y'">
										<xsl:attribute name="CHECKED" />
									</xsl:if>
								</input>
							</td>
							<td align="center">
								<input type="text" tabindex="5" size="10" name="product_unblock_date" value="{viewQueueData/codifiedProductList/codifiedProduct[id = $codifiedId]/blockEndDate}"/> &nbsp; &nbsp;
								<input type="image" tabindex="5" id="calendarSunday" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
							</td>
							<script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "product_unblock_date",  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendarSunday" // ID of the button
													    }
													  );

													]]></script>
							<td align="center">
								<b>
									<i>(Product on the order)</i>
								</b>
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td class="tblheader" align="left">Holiday</td>
                  <td class="tblheader" align="center">Permanent<br/>Block</td>
                  <td class="tblheader" align="center">Temporary<br/>Block</td>
                  <td class="tblheader" align="center">Unblock Date</td>
                  <td class="tblheader" align="center"></td>
                  <td class="tblheader" align="center">Permanent<br/>Block</td>
                  <td class="tblheader" align="center">Temporary<br/>Block</td>
                  <td class="tblheader" align="center">Unblock Date</td>
					</tr>
					<!-- the holiday category -->
					<script><![CDATA[var count=0;]]></script>
					<xsl:for-each select="viewQueueData/codifiedProductList/codifiedProduct">
					    <xsl:sort select="id"/>
						<xsl:variable name="catg" select="category" />
						<xsl:if test="$catg = 'Holiday'">
							<script><![CDATA[count = count+1;]]></script>
							<script><![CDATA[
													if((count % 2) != 0)
														document.write("<tr>");
													]]></script>
							<td>
								<xsl:value-of select="name" />
														&nbsp; &nbsp;
								<input name="codid{position()}" type="hidden" value="{id}"/>
								<xsl:value-of select="id" />
							</td>
							<xsl:variable name="codorblock" select="blockedFlag" />
													<td align="center">
                                          <input type="checkbox" name="codpermblock{position()}" tabindex="4" onclick="permBlockCheck({position()});">
                                             <xsl:attribute name="DISABLED" />
                                             <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
														</input>
													</td>
							<td align="center">
                          <input type="checkbox" name="codblock{position()}" tabindex="5" onclick="tempBlockCheck({position()});">
                          <xsl:if test="$codorblock = 'Y' and blockEndDate != ''">
										<xsl:attribute name="CHECKED" />
									</xsl:if>
	                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
	                          <xsl:attribute name="DISABLED" />
	                       </xsl:if>
								</input>
							</td>
                     <td align="center">

                     <input type="text" tabindex="5" size="10" name="codunblockdate{position()}" value="{blockEndDate}">
                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
                          <xsl:attribute name="DISABLED" />
                       </xsl:if>
                     </input>                                          
                     &nbsp; &nbsp;
                     
                     <input type="image" tabindex="5" id="calendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
                          <xsl:attribute name="DISABLED" />
                       </xsl:if>                                          
                     </input>
                     <xsl:variable name="lock" select="position()"/>

                     <script>
                        var calCount =
                        <xsl:value-of select="$lock"/>;
                        calCount = calCount + "";
                     </script>
                     </td>

                     <script><![CDATA[

                      Calendar.setup(
                         {
                           inputField  : "codunblockdate"+calCount,  // ID of the input field
                           ifFormat    : "mm/dd/y",  // the date format
                           button      : "calendar"+calCount  // ID of the button
                         }
                       );

                     if((count % 2) == 0)
                     {
                        document.write("</tr>");
                     }
                     ]]></script>
							<xsl:if test="$codifiedId = id">
								<script>
													var productCount = 
									<xsl:value-of select="position()"/>;
													productCount = productCount + "";
<![CDATA[markProductReadonly(productCount);]]>
								</script>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
					<tr></tr>
					
					
					<tr>
                  <td class="tblheader" align="left">Other</td>
                  <td class="tblheader" align="center">Permanent<br/>Block</td>
                  <td class="tblheader" align="center">Temporary<br/>Block</td>
                  <td class="tblheader" align="center">Unblock Date</td>
                  <td class="tblheader" align="center"></td>
                  <td class="tblheader" align="center">Permanent<br/>Block</td>
                  <td class="tblheader" align="center">Temporary<br/>Block</td>
                  <td class="tblheader" align="center">Unblock Date</td>
					</tr>
					<!-- the Other category -->
					<script><![CDATA[var count=0;]]></script>
					<xsl:for-each select="viewQueueData/codifiedProductList/codifiedProduct">
					    <xsl:sort select="id"/>
						<xsl:variable name="catg" select="category" />
						<xsl:if test="$catg = 'Other'">
							<script><![CDATA[count = count+1;]]></script>
							<script><![CDATA[
													if((count % 2) != 0)
														document.write("<tr>");
													]]></script>
							<td>
								<xsl:value-of select="name" />
														&nbsp; &nbsp;
								<input name="codid{position()}" type="hidden" value="{id}"/>
								<xsl:value-of select="id" />
							</td>
							<xsl:variable name="codorblock" select="blockedFlag" />
													<td align="center">
                                          <input type="checkbox" name="codpermblock{position()}" tabindex="4" onclick="permBlockCheck({position()});">
                                             <xsl:attribute name="DISABLED" />
                                             <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
														</input>
													</td>
							<td align="center">
                           <input type="checkbox" name="codblock{position()}" tabindex="5" onclick="tempBlockCheck({position()});">
                              <xsl:if test="$codorblock = 'Y' and blockEndDate != ''">
										<xsl:attribute name="CHECKED" />
									</xsl:if>
                           <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
                              <xsl:attribute name="DISABLED" />
                           </xsl:if>									
								</input>
							</td>
							<td align="center">
								<input type="text" size="10"  tabindex="5" name="codunblockdate{position()}" value="{blockEndDate}">
	                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
	                          <xsl:attribute name="DISABLED" />
	                       </xsl:if>                     
	                     </input>
								&nbsp; &nbsp;
								<input type="image"  tabindex="5" id="calendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
	                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
	                          <xsl:attribute name="DISABLED" />
	                       </xsl:if>                     								
	                     </input>
								<xsl:variable name="lock" select="position()"/>
								<script>
														var calCount =
									<xsl:value-of select="$lock"/>;
														calCount = calCount + "";
								</script>
							</td>
							<script><![CDATA[

													 Calendar.setup(
													    {
													      inputField  : "codunblockdate"+calCount,  // ID of the input field
													      ifFormat    : "mm/dd/y",  // the date format
													      button      : "calendar"+calCount  // ID of the button
													    }
													  );

													if((count % 2) == 0) 
													{
														document.write("</tr>");
													}
													]]></script>
													
							<xsl:if test="$codifiedId = id">
								<script>
													var productCount = 
									<xsl:value-of select="position()"/>;
													productCount = productCount + "";
<![CDATA[markProductReadonly(productCount);]]>
								</script>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
					<tr></tr>
					
					
					<tr>
                   <td class="tblheader" align="left" width="20%">Everyday</td>
                   <td class="tblheader" align="center" width="8%">Permanent<br/>Block</td>
                   <td class="tblheader" align="center" width="8%">Temporary<br/>Block</td>
                   <td class="tblheader" align="center" width="14%">Unblock Date</td>
                   <td class="tblheader" align="center" width="20%"></td>
                   <td class="tblheader" align="center" width="8%">Permanent<br/>Block</td>
                   <td class="tblheader" align="center" width="8%">Temporary<br/>Block</td>
                   <td class="tblheader" align="center" width="14%">Unblock Date</td>
					</tr>
					<!-- the everyday category -->
					<script><![CDATA[var count=0;]]></script>
					<xsl:for-each select="viewQueueData/codifiedProductList/codifiedProduct">
					    <xsl:sort select="id"/>
						<input name="codcat{position()}" type="hidden" value="{category}"/>
						<xsl:variable name="catg" select="category" />
						<xsl:if test="$catg = 'Everyday'">
							<script><![CDATA[count = count+1;]]></script>
							<script><![CDATA[
													if((count % 2) != 0)
														document.write("<tr>");
													]]></script>
							<td>
								<xsl:value-of select="name" />
														&nbsp; &nbsp;
								<input name="codid{position()}" type="hidden" value="{id}"/>
								<xsl:value-of select="id" />
							</td>
							<xsl:variable name="codorblock" select="blockedFlag" />
													<td align="center">
                                          <input type="checkbox" name="codpermblock{position()}" tabindex="4" onclick="permBlockCheck({position()});">
                                             <xsl:attribute name="DISABLED" />
                                             <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
																	<xsl:attribute name="CHECKED" />
															</xsl:if>
														</input>
													</td>
							<td align="center">
                           <input type="checkbox" name="codblock{position()}" tabindex="5" onclick="tempBlockCheck({position()});">
                              <xsl:if test="$codorblock = 'Y' and blockEndDate != ''">
										<xsl:attribute name="CHECKED" />
									</xsl:if>
                           <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
                             <xsl:attribute name="DISABLED" />
                           </xsl:if>								
								</input>
							</td>
							
							
                     <td align="center">

                     <input type="text" tabindex="5" size="10" name="codunblockdate{position()}" value="{blockEndDate}">
                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
                          <xsl:attribute name="DISABLED" />
                       </xsl:if>                     
                     </input>
                     &nbsp; &nbsp;
                     
                     <input type="image" tabindex="5" id="calendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
                       <xsl:if test="$codorblock = 'Y' and blockEndDate = ''">
                          <xsl:attribute name="DISABLED" />
                       </xsl:if>                                          
                     </input>
                     <xsl:variable name="lock" select="position()"/>

                     <script>
                        var calCount =
                        <xsl:value-of select="$lock"/>;
                        calCount = calCount + "";
                     </script>
                     </td>

                     <script><![CDATA[

                      Calendar.setup(
                         {
                           inputField  : "codunblockdate"+calCount,  // ID of the input field
                           ifFormat    : "mm/dd/y",  // the date format
                           button      : "calendar"+calCount  // ID of the button
                         }
                       );

                     if((count % 2) == 0)
                     {
                        document.write("</tr>");
                     }
                     ]]></script>
							<xsl:if test="$codifiedId = id">
								<script>
													var productCount = 
									<xsl:value-of select="position()"/>;
													productCount = productCount + "";
<![CDATA[markProductReadonly(productCount);]]>
								</script>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" >
					<tr>
						<td width="20%" align="right"></td>
						<td width="10%"></td>
						<td width="10%" ></td>
						<td width="10%" align="center">
							<button style="font-size: 8pt;" name="fSave1" onclick="javascript:doSaveAction();" tabindex="6">&nbsp;&nbsp;
								<span style="text-decoration:underline;">S</span>ave&nbsp;&nbsp;
							</button>&nbsp;&nbsp;

							<button style="font-size: 8pt;" name="fRemove1" onclick="javascript:doRemoveAction();" tabindex="7">
								<span style="text-decoration:underline;">R</span>emove
							</button>&nbsp;&nbsp;

							<button style="font-size: 8pt;"  onclick="javascript:doNextAction();" tabindex="8">&nbsp;&nbsp;
								<span style="text-decoration:underline;">N</span>ext&nbsp;&nbsp;
							</button>&nbsp;&nbsp;
						
						</td>
						<td width="10%" align="right" ></td>
						<td width="10%" align="left"></td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
					</tr>
				</table></td>
			<input type="hidden" name="totalcod" value="{count(viewQueueData/codifiedProductList/codifiedProduct/id)}"></input>
		</tr>
	</xsl:template>
</xsl:stylesheet>