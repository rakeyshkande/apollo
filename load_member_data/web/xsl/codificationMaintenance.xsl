<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External Templates -->


<xsl:import href="codificationMaintenanceTabs.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="header.xsl"/>

<!-- security parameters -->
<xsl:param name="web-context-root"/>
<xsl:param name="securitytoken"/>
<xsl:param name="context"/>
<xsl:param name="adminAction"/>

<xsl:output method="html" indent="yes"/>
<xsl:key name="processFlow" match="/root/processFlow/flow" use="name" />

<xsl:template match="/root">
	<html>
		<head>
			<title>FTD - Florist Maintenance</title>
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
			<script type="text/javascript" language="javascript" src="../js/util.js"/>
			<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
		 	<script type="text/javascript" src="../js/calendar.js" />

<script type="text/javascript" language="javascript">
<![CDATA[

/*
   Limits input to digits only.      
*/
function digitOnlyListener()
{
    onChange="enableAdd();"

    var input = event.keyCode;
    if (input < 48 || input > 57){
        event.returnValue = false;
    }
}

function enableAdd(){
	document.getElementById("addbutton").disabled=false;
}


function doExitAction()
{
	doReturnAction();
}
	
	
	
function doReturnAction()
{

if(codMaint.saveNeeded.value == "yes")
{
		var response = confirm("Your changes have not been saved. Are you sure you want to exit the screen?");
		if (!response)
		{
			return;
		}
}

  		document.forms[0].action = "CodifyMaintServlet?action=exitCodifyMaint";
  		showWaitMessage("outerContent", "wait", "Processing");
		document.forms[0].submit();
}


function enableAddproduct(){
	document.getElementById("addbuttonproduct").disabled=false;
}

function init()
{
var tab;

//disable add
document.getElementById("addbutton").disabled=true;
document.getElementById("addbuttonproduct").disabled=true;

if(codMaint.currentTab.value == "product")
	tab = "productSetup";
else
	tab = "codificationSetup";

initializeTabs(tab);

//	for (var i = 0; i < document.forms[0].elements.length; i++)
//	{
//		if(document.forms[0].elements[i].readOnly == true)
//			document.forms[0].elements[i].style.backgroundColor = 'silver';
//	}

}

function enableFields()
{
var f = showWait();

for (var i = 0; i < document.forms[0].elements.length; i++)
	document.forms[0].elements[i].disabled = false;
	
var s = hideWait();
}



function countCodCodes(code)
{

var result = 0;

for(var i=1; i<= codMaint.totalCodCodes.value; i++)
{
	if(code == document.getElementById("cCode" + i).value)
	{
	result++;
	}
}

return result;

}


function countProductCodes(code)
{

var result = 0;

for(var i=1; i<= codMaint.totalProSkus.value; i++)
{
	if(trim(code) == document.getElementById("pCode" + i).value)
	{
		if(document.getElementById("deleteproduct" + i).checked == false)
		{
			result++;
		}
	}
}

return result;

}



function addCodificationAction()
{			



var eCount = 0;

// validate the add fields...
	document.getElementById("errorCC").firstChild.data = " ";
	codMaint.newcodificationcode.style.backgroundColor = "white";
	if(trim(codMaint.newcodificationcode.value).length < 1)
	{
		document.getElementById("errorCC").firstChild.data = "Value is Required";
		codMaint.newcodificationcode.style.backgroundColor = "pink";
		eCount++;
	}
	
	document.getElementById("errorDesc").firstChild.data = " ";
	codMaint.newdescription.style.backgroundColor = "white";
	if(trim(codMaint.newdescription.value).length < 1)
	{
		document.getElementById("errorDesc").firstChild.data = "Value is Required";
		codMaint.newdescription.style.backgroundColor = "pink";
		eCount++;
	}
	
	document.getElementById("errorCCat").firstChild.data = " ";
	codMaint.newcodificationcategory.style.backgroundColor = "white";
	if(codMaint.newcodificationcategory.value == "")
	{
		document.getElementById("errorCCat").firstChild.data = "Value is Required";
		codMaint.newcodificationcategory.style.backgroundColor = "pink";
		eCount++;
	}
	
	document.getElementById("errorCAll").firstChild.data = " ";
	codMaint.newcodifyall.style.backgroundColor = "white";
	if(codMaint.newcodifyall.value == "")
	{
		document.getElementById("errorCAll").firstChild.data = "Value is Required";
		codMaint.newcodifyall.style.backgroundColor = "pink";
		eCount++;
	}
			
	if(trim(codMaint.newcodificationcode.value).length > 0)
	{
		for(var i=1; i<= codMaint.totalCodCodes.value; i++)
		{
			if(codMaint.newcodificationcode.value == document.getElementById("cCode" + i).value)
			{
			document.getElementById("errorCC").firstChild.data = "Codification Code Exists";
			codMaint.newcodificationcode.style.backgroundColor = "pink";
			eCount++;
			break;
			}
		}
	}
	
	if(eCount > 0)
	{
		var s = hideWait();
		return;
	}
		
	codMaint.saveNeeded.value = "yes";

	enableFields();
	
	
	document.forms[0].action = "CodifyMaintServlet?action=addCodification";
	showWaitMessage("outerContent", "wait", "Processing");
	document.forms[0].submit();

}

function trim(str)
{
	str=str.replace(/^\s*(.*)/, "$1");
	str=str.replace(/(.*?)\s*$/, "$1");
	return str;
}
	
function addProductAction()
{			
var eCount = 0;

// validate the add fields...
document.getElementById("errorSku").firstChild.data = " ";
codMaint.newsku.style.backgroundColor = "white";

document.getElementById("errorCode").firstChild.data = " ";
codMaint.newproductcodificationcode.style.backgroundColor = "white";


if(trim(codMaint.newsku.value).length < 1)
{
	document.getElementById("errorSku").firstChild.data = "Field is Required";
	codMaint.newsku.style.backgroundColor = "pink";
	eCount++;
}

if(codMaint.newproductcodificationcode.value == "")
{
	document.getElementById("errorCode").firstChild.data = "Field is Required";
	codMaint.newproductcodificationcode.style.backgroundColor = "pink";
	eCount++;
}

if(trim(codMaint.newsku.value).length > 0)
{
	for(var i=1; i<= codMaint.totalProSkus.value; i++)
	{
		if(codMaint.newsku.value == document.getElementById("pSku" + i).value)
		{
		document.getElementById("errorSku").firstChild.data = "Product SKU exists.";
		codMaint.newsku.style.backgroundColor = "pink";
		eCount++;
		break;
		}
	}
}

if(eCount > 0)
	return;


	codMaint.saveNeeded.value = "yes";
	enableFields();
	document.forms[0].action = "CodifyMaintServlet?action=addCodifiedProduct";
	showWaitMessage("outerContent", "wait", "Processing");
 	document.forms[0].submit();
	 	
}

function showWait()
{


   var content = document.getElementById("outerContent");
   var height = 500;
   var waitDiv = document.getElementById("waitDiv").style;
   var waitMessage = document.getElementById("waitMessage");
   content.style.display = "none";
   waitDiv.height = height;
   waitMessage.innerHTML = "Validating . . .";
   waitDiv.display = "block";
	return true;
}

function hideWait()
{
   var content = document.getElementById("outerContent").style.display = "block";
   var height = 500;
   var waitDiv = document.getElementById("waitDiv").style.display = "none";
   return true;
}



function saveAction()
{	

var f = showWait();

// reset all errors...
		for(var i=1; i<= codMaint.totalCodCodes.value; i++)
		{
			document.getElementById("lerrors" + i).style.display = "block";
			document.getElementById("lerrorCC" + i).firstChild.data = " ";
			document.getElementById("cCode" + i).style.backgroundColor = "white";
			document.getElementById("lerrorDesc" + i).firstChild.data = " ";
			document.getElementById("cDescription" + i).style.backgroundColor = "white";
			document.getElementById("lerrorCat" + i).firstChild.data = " ";
			document.getElementById("cCategory" + i).style.backgroundColor = "white";
			document.getElementById("lerrorFlorist" + i).firstChild.data = " ";
			document.getElementById("cAllFlorist" + i).style.backgroundColor = "white";
			document.getElementById("cLoad" + i).style.backgroundColor = "white";
			document.getElementById("lerrorRemove" + i).firstChild.data = " ";
			document.getElementById("cRemove" + i).style.backgroundColor = "white";
		}

			// check the removed codes on the product tab...
			var productBlocks = "";

			for(var i=1; i<= codMaint.totalProSkus.value; i++)
			{
				if(document.getElementById("deleteproduct" + i).checked == true)
				{
					if(productBlocks.length > 1)
						productBlocks = productBlocks + ", " + trim(document.getElementById("pSku" + i).value);
					else
						productBlocks = productBlocks + " " + trim(document.getElementById("pSku" + i).value);
				}
			}

			if(productBlocks.length > 1)
			{
				var agree = confirm("The following Products are being removed: " + productBlocks + ". Are you sure you want to remove these?");
				if (!agree)
				{
					var r = hideWait();
					return;
				}
			}
			

		var eCount = 0;
		var errs = 0;
		// check that no duplicates exists in the cod. code column..

		for(var i=1; i<= codMaint.totalCodCodes.value; i++)
		{
			errs = 0;
			errs = countCodCodes(document.getElementById("cCode" + i).value);
			if(errs > 1)
			{
				document.getElementById("lerrorCC" + i).firstChild.data = "Duplicate Code.";
				document.getElementById("cCode" + i).style.backgroundColor = "pink";
				document.getElementById("lerrors" + i).style.display = "block";
				eCount++;
			}
		}

		var coverArea = "";
		var cr=0;
			for(var i=1; i<= codMaint.totalCodCodes.value; i++)
			{
				if(document.getElementById("cRemove" + i).checked == true)
				{
					if( countProductCodes(document.getElementById("cCode" + i).value) > 0)
					{
						document.getElementById("lerrorRemove" + i).firstChild.data = "Code is in Use.";
						document.getElementById("cRemove" + i).style.backgroundColor = "pink";
						cr++;
					}
					else
					{
						if(coverArea.length > 1)
							coverArea = coverArea + ", " + trim(document.getElementById("cCode" + i).value);
						else
							coverArea = coverArea + " " + trim(document.getElementById("cCode" + i).value);
					}
				}
			}
			
			if(cr > 0)
			{
				var r = hideWait();
				return;
			}
			
			// check the removed codes on the codification tab...

			if(coverArea.length > 1)
			{
				var agree = confirm("Codification Code(s) " + coverArea + " is being removed. Are you sure?");
				if (!agree)
				{
					var r = hideWait();
					return;
				}
			}




		for(var i=1; i<= codMaint.totalCodCodes.value; i++)
		{
			errs = 0;

			if(document.getElementById("codChanged" + i).value == "no")
				continue;

			if(trim(document.getElementById("cCode" + i).value).length < 1)
			{
				document.getElementById("lerrorCC" + i).firstChild.data = "Value Required.";
				document.getElementById("cCode" + i).style.backgroundColor = "pink";
				errs++;
			}
			if(trim(document.getElementById("cDescription" + i).value).length < 1)
			{
				document.getElementById("lerrorDesc" + i).firstChild.data = "Value Required.";
				document.getElementById("cDescription" + i).style.backgroundColor = "pink";
				errs++;
			}
			if(trim(document.getElementById("cCategory" + i).value).length < 1)
			{
				document.getElementById("lerrorCat" + i).firstChild.data = "Value Required.";
				document.getElementById("cCategory" + i).style.backgroundColor = "pink";
				errs++;
			}
			if(trim(document.getElementById("cAllFlorist" + i).value).length < 1)
			{
				document.getElementById("lerrorFlorist" + i).firstChild.data = "Value Required.";
				document.getElementById("cAllFlorist" + i).style.backgroundColor = "pink";
				errs++;
			}
			
			if(document.getElementById("cLoad" + i).value == "Y")
			{
				if(document.getElementById("cAllFlorist" + i).value == "Y")
				{
					document.getElementById("lerrorFlorist" + i).firstChild.data = "Value cannot be set to yes if Load From File is also Yes.";
					document.getElementById("cAllFlorist" + i).style.backgroundColor = "pink";
					errs++;
				}
			}
			
			if(errs > 0)
			{
				document.getElementById("lerrors" + i).style.display = "block";
				eCount++;
			}
		}


		if(eCount > 0)
		{
		var r = hideWait();
		return;
		}
			
		enableFields();
		codMaint.saveNeeded.value = "no";
		document.forms[0].action = "CodifyMaintServlet?action=saveCodifications";
		showWaitMessage("outerContent", "wait", "Processing");
	 	document.forms[0].submit();
}


function doEditCod(pos)
{

if(!document.getElementById("editCod" + pos).checked)
{
	document.getElementById("cCode" + pos).disabled = true;
	document.getElementById("cDescription" + pos).disabled = true;
	document.getElementById("cCategory" + pos).disabled = true;
	document.getElementById("cAllFlorist" + pos).disabled = true;
	document.getElementById("cLoad" + pos).disabled = true;
    document.getElementById("cRequired" + pos).disabled = true;
}
else
{
	document.getElementById("cCode" + pos).disabled = false;
	document.getElementById("cDescription" + pos).disabled = false;
	document.getElementById("cCategory" + pos).disabled = false;
	document.getElementById("cAllFlorist" + pos).disabled = false;
	document.getElementById("cLoad" + pos).disabled = false;
    document.getElementById("cRequired" + pos).disabled = false;
}

document.getElementById("codChanged"+pos).value = "yes";

}

]]>
</script>
		</head>

		<body>
			<form name="codMaint" method="post">

				<xsl:call-template name="header">
					<xsl:with-param name="headerName"  select="'Codified Products Setup'" />
					<xsl:with-param name="showExitButton" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
				</xsl:call-template>
				
				<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
				<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
				<input type="hidden" name="context" value="{$context}"/>
				<input type="hidden" name="adminAction" value="{$adminAction}"/>

<!-- Outer div needed to hold any content that will be blocked when an action is performed -->
<div id="outerContent" style="display:block">

					<table width="100%">
							<tr>
								<td height="18"></td>
							</tr>
						</table>

<xsl:call-template name="codifiedProductsTabs"/>

<ul id="content">

<div id="productSdiv">

<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<input type="hidden" value="{key('processFlow', 'saveNeeded')/value}" name="saveNeeded"></input>

	<table width="100%">

		<tr>
			<td>
				<p align="center">
					<input type="button" align="center" value=" SAVE " name="savepro" size="10" tabindex="10" onClick="saveAction();"></input>
				</p>
			</td>
		</tr>

		<tr>
		<td class="tblheader" align="left">Create</td>
		</tr>

		<tr><td><br></br></td></tr>

		<tr>
		<td align="center">

		<table width="70%">


		<tr align="center">
			<td width="20%" class="label"></td>
			<td width="30%" class="label"> </td>
			<td width="15%" class="label" > Front End </td>
			<td width="15%"></td>
			<td width="10%" class="label"></td>
			<td width="10%" class="label"></td>
		</tr>

		<tr align="center">
			<td width="20%" class="label"> Product SKU</td>
			<td width="30%" class="label"> Codification Code - Name</td>
			<td width="15%" class="label"> Validation </td>
			<td width="15%"></td>
		</tr>

		<tr align="center">
			<td><input type="text" name="newsku" value="{FORMDATA/DATA[name='productSKU']/value}" maxlength="10" onkeypress="enableAddproduct();" /></td>

			<td>
			<xsl:variable name="cidselected" select="FORMDATA/DATA[name='codificationCode']/value" />

			<select name="newproductcodificationcode" onChange="enableAddproduct();">
			<option value="">Select...</option>
				<xsl:for-each select="codificationList/codes">
				<xsl:sort select="codification_id" data-type="text" />
						<xsl:variable name="cid2" select="codification_id" />
						<option value="{codification_id}">
						<xsl:if test="$cidselected = $cid2">
								<xsl:attribute name="SELECTED" />
							</xsl:if>
							<xsl:value-of select="codification_id" /> &nbsp;
							<xsl:value-of select="description" />
						</option>
				</xsl:for-each>
			</select>
			</td>

			<td>
			<select name="newfrontendvalidation" onChange="enableAddproduct();">
			<option value="Y" selected="true" >Yes</option>
			<option value="N">
				<xsl:if test="FORMDATA/DATA[name='frontEndValidation']/value = 'N'">
					<xsl:attribute name="selected"/>
				</xsl:if>No
			</option>
			</select>
			</td>

			<td><input type="button" name="addbuttonproduct" value=" Add " onClick="addProductAction();" /></td>

		</tr>
		
		<tr align="center">
			<td width="20%" id="errorSku" class="RequiredFieldTxt">&nbsp;<xsl:value-of select="ERRORS/ERROR[name='formError']/value"/></td>
			<td width="30%" id="errorCode" class="RequiredFieldTxt">&nbsp;</td>
		</tr>	
			

		</table>

		</td>
		</tr>

		<tr>
		<td class="tblheader" align="left">Manage</td>
		</tr>


		<tr>
			<td colspan="8" width="100%" align="center">
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>

						
						<td width="15%" class="colHeaderCenter">Product SKU</td>
						<td width="25%" class="colHeaderCenter" align="left">Product Name</td>
						<td width="5%" class="colHeaderCenter">Status</td>
						<td width="30%" class="colHeaderCenter">Codification Code - Name</td>
						<td width="10%" class="colHeaderCenter">Front End Validation</td>
						<td width="10%" class="colHeaderCenter">Sequence #</td>
						<td width="5%" class="colHeaderCenter">Remove</td>
					</tr>


						<xsl:for-each select="codifyData/codificationProduct/codification">
						<xsl:sort select="productId" data-type="text" />
						<tr align="center">
								
		
								<td width="15%">
								<xsl:value-of select="productId"/>
								
								
								<input type="hidden" name="pSku{position()}" value="{productId}"/>
								</td>
		
								<td width="25%" align="left">
									<xsl:value-of select="productName"/>
								</td>
								
								<td width="5%" >
									<xsl:value-of select="status"/>
								</td>
		
								<td width="30%" align="center">
								<input type="hidden" name="pCode{position()}" value="{codificationId}"/>
								
								<xsl:value-of select="codificationId"/> &nbsp; - &nbsp; <xsl:value-of select="description" />
								<!--
								<xsl:variable name="sid" select="codificationId" />
								<select name="pCodeName{position()}">
									<option>Select...</option>

									<xsl:for-each select="//codificationList/codes">
										<xsl:variable name="cid" select="codification_id" />
										<option value="{codification_id}">
											<xsl:if test="$sid = $cid">
												<xsl:attribute name="SELECTED" />
											</xsl:if>
											<xsl:value-of select="codification_id" /> &nbsp;
											<xsl:value-of select="description" />
										</option>
									</xsl:for-each>
									
								</select>	
								-->
									
								</td>
		
								<td width="10%" >
								<input type="hidden" name="pendValid{position()}" value="{checkOEflag}"/>
							
								<xsl:value-of select="checkOEflag"/>
								<!--
									<select name="endValid{position()}">
										<option selected="true">Yes</option>
										<option>
											<xsl:if test="checkOEflag = 'N'">
												<xsl:attribute name="selected"/>
											</xsl:if>
										No
										</option>
									</select>
								-->
								</td>
		
								<td width="10%">
								<input type="hidden" name="pNovator{position()}" value="{flagSequence}"/>
								<xsl:value-of select="flagSequence"/>
								<!--	<input type="text" name="pNovator{position()}" value="{flagSequence}"/> -->
								</td>
								<td width="5%" >
									<input type="checkbox" name="deleteproduct{position()}"/>
								</td>
						</tr>
				</xsl:for-each>


				</table>
					<input type="hidden" name="totalProSkus" value="{count(codifyData/codificationProduct/codification/productId)}"></input>
				</td>
			</tr>


		<tr>
			<td>
				<p align="center">
					<input type="button" align="center" value=" SAVE " name="savepro" size="10" tabindex="10" onClick="saveAction();"></input>
				</p>
			</td>
		</tr>
	</table>
</td></tr>
</table>

</div>

<div id="codificationSdiv">

<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">

	<tr><td>
		<table width="100%">
			<tr>
				<td>
					<p align="center">
						<input type="button" align="center" value=" SAVE " name="savepro" size="10" tabindex="10" onClick="saveAction();"></input>
					</p>
				</td>
			</tr>

			<tr>
				<td class="tblheader" align="left">Create</td>
			</tr>

			<tr><td><br></br></td></tr>

			<tr>
				<td align="center">

				<table width="100%">

	


				<tr align="center">
						<td width="5%"/>
						<td width="10%" class="label">Codification Code</td>
						<td width="30%" class="label">Description</td>
						<td width="15%" class="label">Codification Category</td>
						<td width="10%" class="label">Codify All Florist</td>
						<td width="10%" class="label">Codification Required</td>
                        <td width="10%"/>
                        <td width="10%"/>
					
					</tr>
					
					<tr align="center">
						<td width="5%"/>
						<td width="10%" class="label"><input type="text" name="newcodificationcode" maxlength="5" onkeypress="enableAdd();" /></td>
						<td width="30%" class="label"><input type="text" name="newdescription" maxlength="100" size="60" onkeypress="enableAdd();" /></td>
						<td width="15%" class="label">
						<select name="newcodificationcategory" onChange="enableAdd();">
						<option value="">Select...</option>
								<xsl:for-each select="//categoryList/category">
										<xsl:variable name="codsid" select="category_id" />
										<option value="{category_id}">
											<xsl:value-of select="category_id" />
										</option>
									</xsl:for-each>
							</select></td>
						<td width="10%" class="label">
							<select name="newcodifyall" onChange="enableAdd();">
								<option value="Y">Yes</option>
								<option value="N" selected="true">No</option>
							</select></td>
						<td width="10%" class="label">
							<select name="newrequired" onChange="enableAdd();">
								<option value="Y">Yes</option>
								<option value="N" selected="true">No</option>
							</select></td>
						<td width="10%"><input type="button" name="addbutton" value=" Add " onClick="addCodificationAction();"/></td>
                        <td width="10%"/>


					</tr>
					
					<tr align="center">
						<td width="5%"/>
						<td width="10%" id="errorCC" class="RequiredFieldTxt">&nbsp;</td>
						<td width="30%" id="errorDesc" class="RequiredFieldTxt">&nbsp;</td>
						<td width="15%" id="errorCCat" class="RequiredFieldTxt">&nbsp;</td>
						<td width="10%" id="errorCAll" class="RequiredFieldTxt">&nbsp;</td>
						<td width="10%"/>
						<td width="10%"/>
                        <td width="10%"/>
					</tr>
	

			<tr>
				<td class="tblheader" align="left" colspan="8">Manage</td>
			</tr>

	<tr><td><br></br></td></tr>
						<tr align="center">

							<td width="5%" class="colheader">Edit</td>
							<td width="10%" class="colheader">Codification Code</td>
							<td width="30%" class="colheader">Description</td>
							<td width="15%" class="colheader" >Codification Category</td>
							<td width="10%" class="colheader">Codify All Florist</td>
							<td width="10%" class="colheader">Codification Required</td>
							<td width="10%" class="colheader">Load from File</td>
							<td width="10%" class="colheader">Remove</td>
						</tr>

			<xsl:for-each select="codifyData/codificationMaster/codification">
			<xsl:sort select="codificationId" data-type="text" />
			
						<tr align="center">
							<td width="5%">
							<input type="checkbox" name="editCod{position()}" onclick="doEditCod('{position()}');"></input>
							<input type="hidden" name="previousCodification{position()}" value="{previousCodificationId}"/>
							<input type="hidden" name="codChanged{position()}" value="no"/>
							
							</td>
							<td width="10%"><input type="text" name="cCode{position()}" disabled="true" value="{codificationId}" /></td>
							<td width="30%"><input type="text" name="cDescription{position()}" disabled="true" value="{description}" size="75" /></td>
							<td width="15%">
							
							<xsl:variable name="codcid" select="categoryId" />
								<select name="cCategory{position()}" disabled="true" >
									<option>Select...</option>
										
									<xsl:for-each select="//categoryList/category">
										<xsl:variable name="codsid" select="category_id" />
										<option value="{category_id}">
											<xsl:if test="$codsid = $codcid">
												<xsl:attribute name="SELECTED" />
											</xsl:if>
											<xsl:value-of select="category_id" />
										</option>
									</xsl:for-each>
									
								</select>
							</td>
							<td width="10%" >
								<select name="cAllFlorist{position()}" disabled="true">
								<option selected="true" value="Y">Yes</option>
								<option value="N">
									<xsl:if test="codifyAllFloristFlag = 'N'">
										<xsl:attribute name="selected"/>
									</xsl:if>
								No
								</option>
								</select>
							</td>

							<td width="10%" >
								<select name="cRequired{position()}" disabled="true">
								<option selected="true" value="Y">Yes</option>
								<option value="N">
									<xsl:if test="codificationRequired = 'N'">
										<xsl:attribute name="selected"/>
									</xsl:if>
								No
								</option>
								</select>
							</td>

							<td width="10%">
                                <input type="hidden" name="cLoad{position()}" value="{loadFromFileFlag}" />
                                    <xsl:choose>
                                       <xsl:when test="loadFromFileFlag = 'Y'">
                                       Yes
                                       </xsl:when>
                                       <xsl:otherwise>
                                       No
                                       </xsl:otherwise>
                                    </xsl:choose>
							</td>
							<td width="10%">
								<input type="checkbox" name="cRemove{position()}" ></input>
								<!--
								<xsl:if test="codifyAllFloristFlag = 'Y'">
									<input type="checkbox" name="cGlobal{position()}" ></input>
								</xsl:if> -->
							</td>
							
						</tr>
						
						<!-- the error fields for each of the above data items in the table. -->
						<tr id="lerrors{position()}" style="display:none" align="center">
							<td width="5%" ></td>
							<td width="10%" id="lerrorCC{position()}" class="RequiredFieldTxt">&nbsp;</td>
							<td width="30%" id="lerrorDesc{position()}" class="RequiredFieldTxt">&nbsp;</td>
							<td width="15%" id="lerrorCat{position()}" class="RequiredFieldTxt">&nbsp;</td>
							<td width="10%" id="lerrorFlorist{position()}" class="RequiredFieldTxt">&nbsp;</td>
							<td width="10%" ></td>
							<td width="10%" ></td>
							<td width="10%" id="lerrorRemove{position()}" class="RequiredFieldTxt">&nbsp;</td>
						</tr>
					</xsl:for-each>

		</table>
		<input type="hidden" name="totalCodCodes" value="{count(codifyData/codificationMaster/codification/codificationId)}"></input>
		<input type="hidden" name="currentTab" value="{FORMDATA/DATA[name='currentTab']/value}" />
	</td>
	</tr>
		<tr><td><br></br></td></tr>


	</table>
	</td>
	</tr>

	<tr>
			<td>
				<p align="center">
					<input type="button" align="center" value=" SAVE " name="savepro" size="10" tabindex="10" onClick="saveAction();"></input>
				</p>
			</td>
	</tr>

</table>

</div>

</ul>


</div>

<div id="waitDiv" style="display:none">
	<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
		<tr>
			<td width="100%">
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td id="waitMessage" align="right" width="50%" class="waitMessage" />
						<td id="waitTD" width="50%" class="waitMessage" />
					</tr>
				</table></td>
		</tr>
	</table>
</div>

		</form>
	</body>
</html>

<script type="text/javascript">
init();
</script>

</xsl:template>
</xsl:stylesheet>