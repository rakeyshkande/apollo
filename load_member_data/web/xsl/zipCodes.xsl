<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="zipcodes">
	<script type="text/javascript" language="javascript">
	
	<![CDATA[

var EXIT = "_EXIT_";

function resetZipCodesErrors()
{
	document.getElementById("errorExistingZips").style.display = "none";
	document.getElementById("errorholdExistingZips").style.display = "block";
	
	document.getElementById("errorInvalidZips").style.display = "none";
	document.getElementById("errorInvalidPopZips").style.display = "none";
	document.getElementById("errorInvalidSavedZips").style.display = "none";
	
	
	document.getElementById("errorholdInvalidZips").style.display = "block";
	
	document.getElementById("errorAscZips").style.display = "none";
	document.getElementById("errorholdAscZips").style.display = "block";
}
	
	
function assignZipTabs()
{

var pos = 0;
for(var i=1; i<=floristMaint.totalZipcodes.value; i++)
{
	if(document.getElementById("zipNo" + i) != null)
	{
		if(!document.getElementById("zipNo" + i).disabled)
		{
			pos = i;
			break;
		}
	}
}


if(document.getElementById("ziphref" + pos) != null)
	document.getElementById("ziphref" + pos).tabIndex = 5;
if(document.getElementById("zipNo" + pos) != null)
	document.getElementById("zipNo" + pos).tabIndex = 6;
if(document.getElementById("zipYes" + pos) != null)
	document.getElementById("zipYes" + pos).tabIndex = 7;
if(document.getElementById("cutofftime" + pos) != null)
	document.getElementById("cutofftime" + pos).tabIndex = 8;

}


function validateZipCode()
{
    resetZipCodesErrors();

    var vzipcode = floristMaint.newzipcodes.value;
    var formattedZips = "";
    var validZipFlag = false;
    var invalidZips = new Array();
    var invalidZipCount = 0;
    
    vzipcode = trim(vzipcode);

    if(vzipcode.length > 0)
    {
        var zipArray=vzipcode.toUpperCase().split(",");

        for(var j=0; j<zipArray.length; j++)
        {
            zipArray[j] = trim(zipArray[j]);
            
            if(zipArray[j].length > 0)
            {
                if(formattedZips.length < 1)
                {
                    formattedZips = zipArray[j];
                }
                else
                {
                    formattedZips += "," + zipArray[j];
                }
                
                for(var i=0; i<allowedZips.length; i++) 
                {
                    if(zipArray[j] == allowedZips[i])
                    {
                        validZipFlag = true;
                        break;
                    }
                }
                if(!validZipFlag)
                {
                    invalidZips[invalidZipCount++] = zipArray[j];
                }
            }
            validZipFlag = false;
        }
        floristMaint.newzipcodes.value = formattedZips;
    }

    if(invalidZips.length > 0)
    {
        var invalidZipsString = "";
        
        for(var i=0; i<invalidZips.length; i++)
        {
            invalidZipsString += '\n' + '\t' + '\t' + '\t' + invalidZips[i];
        }
        
        var response = confirm("Would you like to override the zip code restriction for the following zip(s)? : " + invalidZipsString);
        return response;        
    }
    else
    {
        return true;  
    }
}
// end of validate zip code function

function checkAllActive() {
    for(var i=0; i<allowedZips.length; i++) {
        if (document.getElementById("zipNo" + (i+1)) != null) {
            //tempMsg = tempMsg + allowedZips[i] + ' ' + document.getElementById("zipNo" + (i+1)).checked + ' ' + document.getElementById("zipYes" + (i+1)).checked + '\n';
            document.getElementById("zipNo" + (i+1)).checked = true;
            document.getElementById("zipYes" + (i+1)).checked = false;
        }
    }
    enSave();
    document.forms[0].saveNeeded.value = "yes";
    document.getElementById("blockAllZips").value = "no";
}

function checkAllBlock() {
    for(var i=0; i<allowedZips.length; i++) {
        if (document.getElementById("zipNo" + (i+1)) != null) {
            //tempMsg = tempMsg + allowedZips[i] + '\n';
            document.getElementById("zipNo" + (i+1)).checked = false;
            document.getElementById("zipYes" + (i+1)).checked = true;
            if (document.getElementById('zipblockdate' + (i+1)).value == "") {
                document.getElementById('zipblockdate' + (i+1)).value = getTodayDate();
            }
            document.getElementById("zipblockcalendar" + (i+1)).style.visibility = "visible";
        }
    }
    enSave();
    document.forms[0].saveNeeded.value = "yes";
    document.getElementById("blockAllZips").value = "yes";
}

function blockChange() {
    enSave();
    document.forms[0].saveNeeded.value = "yes";
}

function unblockChange() {
    enSave();
    document.forms[0].saveNeeded.value = "yes";
}

function checkBlock(pos) {
    if (document.getElementById('zipblockdate' + pos).value == "") {
        document.getElementById('zipblockdate' + pos).value = getTodayDate();
    }
    document.getElementById("zipblockcalendar" + pos).style.visibility = "visible";
    enSave();
}

function getTodayDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
		dd='0'+dd
	} 
	if(mm<10) {
		mm='0'+mm
	} 
	returndate = mm+'/'+dd+'/'+yyyy;
	return returndate;
}

function validateZipBlocks() {
    var validFlag = true;;
	for(i = 1; i <= document.forms[0].totalZipcodes.value; i++) {
        temp = "zipYes" + i;
        if(document.getElementById(temp) != null) {
            if(document.getElementById(temp).checked == true) {
                tempStartDate = document.getElementById('zipblockdate' + i).value;
	            tempEndDate = document.getElementById('zipunblockdate' + i).value;
	            if (tempEndDate.length > 0) {
	                startList = tempStartDate.split("/");
	                endList = tempEndDate.split("/");
	                newStartDate = startList[2] + startList[0] + startList[1];
	                newEndDate = endList[2] + endList[0] + endList[1];
	                if (newStartDate > newEndDate) {
	                    validFlag = false;
	                    document.getElementById('zipblockdate' + i).style.backgroundColor = 'pink';
	                    document.getElementById('zipunblockdate' + i).style.backgroundColor = 'pink';
    	                //alert(tempStartDate + " - " + tempEndDate + "\n" + newStartDate + " - " + newEndDate);
	                } else {
	                    document.getElementById('zipblockdate' + i).style.backgroundColor = 'silver';
	                    document.getElementById('zipunblockdate' + i).style.backgroundColor = 'silver';
	                }
	            }
	        }	
        }
    }
    if (validFlag) {
        document.getElementById("errorInvalidZipBlocks").style.display = "none";
    } else {
        document.getElementById("errorInvalidZipBlocks").style.display = "block";
    }
    return validFlag;
}

]]>
</script>
<input type="hidden" name="editZipCodes"/>
<input type="hidden" name="editCutoffFlag"/>
<input type="hidden" name="editCutoff"/>
<input type="hidden" name="editBlocksFlag"/>
<input type="hidden" name="editZipBlockDate"/>
<input type="hidden" name="editZipBlockFlag"/>
<input type="hidden" name="blockAllZips" value="no"/>

									<tr>
										<td>
											<table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
											<tr>
												<td width="50%" align="center">
													<b>Florist # : &nbsp; <xsl:value-of select="floristData/generalData/memberNumber"/></b>
												</td>
												<td width="10%"/>
												<td width="40%">
													&nbsp; &nbsp; &nbsp;
													<xsl:variable name="goto" select="floristData/generalData/gotoFlag" />
													<xsl:if test="$goto = 'Y'">
													GoTo Florist
													</xsl:if> &nbsp;
												</td>
											</tr>
											
											<tr>
											
												<td id="errorExistingZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													The following Zip Codes already exist for this florist - <br></br>
												<input type="hidden" name="listExisting" readonly="true" size="20" value="{existingZips/ezips/value}"/>
												<xsl:value-of select="existingZips/ezips/value" /> <br></br>
												</td>

												<td id="errorInvalidZipBlocks" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													The Block Start Date cannot be after the Block End Date
												</td>

												<td id="errorholdExistingZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
												</td>
												
												<td width="10%"/>
												<td width="40%">
												</td>
												</tr>
												<tr bordercolor="white">

												<td id="errorInvalidZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The following Zip/Postal Codes are invalid - <br></br>
													<input type="hidden" name="listInvalid" readonly="true" size="20" value="{invalidZips/izips/value}"/>
													<xsl:value-of select="invalidZips/izips/value" />
												</td>
												
												<td id="errorInvalidPopZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The following Zip/Postal Codes can not be blocked or modified.<br></br>
													<input type="hidden" name="listInvalidPop" readonly="true" size="20" value="{invalidPopZips/iPopzips/value}"/>
													<xsl:value-of select="invalidPopZips/iPopzips/value" />
												</td>
												
												

												<td id="errorholdInvalidZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
												</td>
												
												<td class="label" align="right" width="10%" id="zipstatuslabel"> Status </td>
                                                <td width="40%">&nbsp; &nbsp; &nbsp; <xsl:value-of select="floristData/generalData/statusText" /></td>
													
												</tr>
												
												<tr>
													<td align="center" colspan="5">
																	<input type="button" align="center" value="SAVE" name="savezip1" size="10" tabindex="102" onClick="saveAction();"></input>
																	&nbsp; &nbsp; &nbsp;
													</td>
												</tr>
												
												
												<tr bordercolor="white">
													<td id="errorAscZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The following Zip/Postal Codes already exist on an associated florist - <br></br>
													<input type="hidden" name="listAssociated" readonly="true" size="20" value="{associatedZips/azips/value}"/>
													<xsl:value-of select="associatedZips/azips/value" />
													</td>

													<td id="errorholdAscZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
													</td>
													<td width="10%" align="left"></td>
													<td width="40%" align="left"></td>
												</tr>
												
												
												<tr bordercolor="white">
													<td id="errorInvalidSavedZips" align="left" width="50%" class="RequiredFieldTxt" style="display:none">
													<br></br>The following Zip/Postal Codes need to be saved before they can be modified.<br></br>
													<input type="hidden" name="listNonSavedPop" readonly="true" size="20" value="{invalidSavedZips/nonsavedzips/value}"/>
													<xsl:value-of select="invalidSavedZips/nonsavedzips/value" />
													</td>

													<td id="errorholdSavedZips" align="left" width="50%" class="RequiredFieldTxt" style="display:block">
													</td>
													<td width="10%" align="left"></td>
													<td width="40%" align="left"></td>
												</tr>
												
												
											</table>
										</td>
									</tr>
									<tr>
										<td class="tblheader" align="left">
									 	 Add Zip/Postal Codes
									 	 </td>
									</tr>
									<tr>
										<td>
											<table cellpadding="1" cellspacing="1" width="100%" border="0" >
											<tr align="center">
												<td class="label" align="right"> Zip Code </td>
												<td>
													<textarea tabindex="1" 
													          name="newzipcodes" 
													          wrap="physical" 
													          cols="32" 
													          rows="2" 
													          onBlur="this.value=this.value.toUpperCase();" >
													</textarea>
												</td>

												<td class="label"> Cut Off Time
												&nbsp; &nbsp; &nbsp; &nbsp;
													<select name="newcutofftime" tabindex="2">
													
													<xsl:for-each select="cutoffTimeList/cutoffTime">
														<xsl:variable name="cut" select="cutoffMilitary" />

														<option value="{cutoffMilitary}">
														<xsl:value-of select="cutoffDisplay" />
														</option>
													</xsl:for-each>

													</select>
													<script>floristMaint.newcutofftime.selectedIndex = 1;</script>
												</td>
												<td>
													<input type="button" align="center" value="Add" name="add" size="10" tabindex="3" onClick="addAction();"></input>
												<script>var invZip = "";</script>
												<xsl:for-each select="invalidZips/zips">
												  <xsl:variable name="iZip" select="value" />

												  <xsl:value-of select="$iZip"/>
												  
												  <script>
												  invZip = invZip + ", " +
														<xsl:value-of select="$iZip"/>;

														invZip = invZip + "";
														
													</script>	

											  	</xsl:for-each>
											  	
											  	<script>
											  		if(invZip.length > 1)
											  		alert("The following Zip/Postal Codes are invalid - " + invZip);
											  	</script>
												
												</td>
												
												
												
												
											</tr>
											</table>
										</td>
									</tr>

									<tr>
										<td class="tblheader" align="left">
									 	Manage Zip/Postal Codes 
									 	 </td>
									</tr>
											
									<tr>

									<td>
									<table width="100%" border="0">
										<tr>
											<td colspan="9" width="100%" align="center">


													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>

															<td width="10%" class="colHeaderCenter">
																<xsl:if test="key('securityPermissions', 'zipCodeListLinkView')/value = 'Y'">
																	<a href="javascript:doZipList({postalCode});" class="tableheaderlink" tabindex="4"><u>Zip Code</u></a>
																</xsl:if>
																<xsl:if test="key('securityPermissions', 'zipCodeListLinkView')/value != 'Y'">
																	Zip Code
																</xsl:if>
															</td>

															<td width="20%" class="colHeaderCenter">City</td>
															<td width="10%" class="colHeaderCenter"># of Florists</td>
															<td width="10%" class="colHeaderCenter">Cut Off Time</td>
															<td width="10%" class="colHeaderCenter">
															    Active
																<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value = 'Y'">
															        &nbsp;&nbsp;<a href="javascript:checkAllActive()"><img src="../images/all_check_blue.gif" border="0" align="absmiddle"/></a>
																</xsl:if>
															</td>
															<td width="10%" class="colHeaderCenter">
															    Block
																<xsl:if test="key('securityPermissions', 'fulfillmentUpdate')/value = 'Y'">
															        &nbsp;&nbsp;<a href="javascript:checkAllBlock()"><img src="../images/all_check_blue.gif" border="0" align="absmiddle"/></a>
															    </xsl:if>
															</td>
															<td width="15%" class="colHeaderLeft">
															    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															    Start Date
															</td>
															<td width="15%" class="colHeaderLeft">
															    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															    End Date
															</td>

														</tr>

														<tr>
															<td>
															<input type="hidden" name="totalZipcodes" value="{count(floristData/postalCodeList/postalCode/postalCode)}"></input>
															<input type="hidden" name="newzipunblockdate" value="" />
															</td>
														</tr>

                                                                                                                <script>
                                                                                                                        var allowedZips = new Array();
                                                                                                                        var cnt = 0;
                                                                                                                </script>
                                                                                                                <xsl:for-each select="floristData/postalCodeList/postalCode">
															<xsl:sort select="postalCode"/>
															
                                                                                                                        <script>
                                                                                                                                allowedZips[cnt++] = "<xsl:value-of select="postalCode"/>";
                                                                                                                        </script>
                                                                                                                        
                                                                                                                        <input name="deleteFlag{position()}" type="hidden" value="{deleteFlag}"/>
															<input name="zipcode{position()}" type="hidden" value="{postalCode}"/>
                                                                                                                        <input name="assignedStatus{position()}" type="hidden" value="{assignedStatus}"/>
                                                                                                                        <input name="assignmentType{position()}" type="hidden" value="{assignmentType}"/>
                                                                                                                                                                                                                                                
                                                                                                                        <xsl:choose>
                                                                                                                        <xsl:when test="assignedStatus = 'assigned' or assignedStatus = 'assignedUnavail'">
															
															<tr id="zip_row{position()}" style="display:block">
															
												
																<td width="10%" align="center">
																	
																		<xsl:value-of select="postalCode"/>
																		
																		<xsl:if test="deleteFlag != 'N'">
																			<xsl:if test="savedFlag = 'N'">
																				<script>
																				document.getElementById("savezipNeeded").value = "yes";
																				</script>
																			</xsl:if>
																		</xsl:if>

																</td>

																<td width="20%" align="center">
																	<xsl:value-of select="city" />
																</td>
																<td width='10%' align='center'>
								                                  <u>
								                                  
																<a href="javascript:doShowFloristZipPopUp('{postalCode}');" id="ziphref{position()}" tabindex="-1">
								                                  <xsl:value-of select="floristCount" /></a>

								                                  </u>

																</td>

																<td align="center">

																<select name="cutofftime{position()}" tabindex="-1">
																	
																	<xsl:variable name="cuttime" select="cutoffTime" />

																	<xsl:for-each select="//cutoffTimeList/cutoffTime">
																		<xsl:variable name="cut" select="cutoffMilitary" />
	
																		<option value="{cutoffMilitary}">
	
																		<xsl:if test="$cuttime = $cut">
																		<xsl:attribute name="SELECTED" />
																		</xsl:if>
	
																		<xsl:value-of select="cutoffDisplay" />
																		</option>
																	</xsl:for-each>
																</select>
																
																</td>

																<xsl:variable name="zipStat" select="blockedFlag" />
																<xsl:variable name="savedzipStat" select="savedFlag" />

																<td align="center">
																	<input type="radio" value="N" name="zipblock{position()}" tabindex="-1" id="zipNo{position()}" onclick="enSave();">
																		<xsl:if test="$zipStat = 'N'">
																			<xsl:attribute name="CHECKED" />
																		</xsl:if>
																	</input>
																</td>

																<td align="center">
																	<input type="radio" value="Y" name="zipblock{position()}"  tabindex="-1" id="zipYes{position()}" onclick="checkBlock({position()});">
																		<xsl:if test="$zipStat = 'Y'">
																			<xsl:attribute name="CHECKED" />
																		</xsl:if>
																	</input>
																</td>
																
																<xsl:if test="savedFlag = 'N'">
																<script>
																	var zipR = <xsl:value-of select="position()"/>;
																	zipR = zipR + "";
																	document.getElementById("zipNo" + zipR).disabled = true;
																	document.getElementById("zipYes" + zipR).disabled = true;
																</script>
																</xsl:if>

																<td align="left">
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="text" tabindex="-1" readonly="true" value="{blockStartDate}" name="zipblockdate{position()}" size="10" onchange="blockChange()"/>
																    &nbsp;
																	<input type="image" tabindex="-1" id="zipblockcalendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
																		<xsl:if test="key('securityPermissions', 'zipCodesUpdate')/value != 'Y'">
																			<xsl:attribute name="DISABLED" />
																		</xsl:if>
																		<xsl:choose>
																		    <xsl:when test="blockStartDate != ''">
																		        <xsl:attribute name="style">
																		            <xsl:value-of select="'visibility:visible;'"/>
																		        </xsl:attribute>
																		    </xsl:when>
																		    <xsl:otherwise>
																		        <xsl:attribute name="style">
																		            <xsl:value-of select="'visibility:hidden;'"/>
																		        </xsl:attribute>
																		    </xsl:otherwise>
																		</xsl:choose>
																	</input>
																	<xsl:variable name="blockpos" select="position()"/>

																	<script>
																		var calCount = <xsl:value-of select="$blockpos"/>;
																		calCount = calCount + "";
																	</script>
																	<script><![CDATA[

																	 Calendar.setup(
																	    {
																	      inputField  : "zipblockdate"+calCount,  // ID of the input field
																	      ifFormat    : "mm/dd/y",  // the date format
													      				  button      : "zipblockcalendar"+calCount  // ID of the button
													    				}
													  				);
																	]]></script>
																
																</td>

																<td align="left">
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="text" tabindex="-1" readonly="true" value="{blockEndDate}" name="zipunblockdate{position()}" size="10" onchange="unblockChange()"/>
																<xsl:if test="blockEndDate != ''">
																    &nbsp;
																	<input type="image" tabindex="-1" id="zipcalendar{position()}" src="../images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE">
																		<xsl:if test="key('securityPermissions', 'zipCodesUpdate')/value != 'Y'">
																			<xsl:attribute name="DISABLED" />
																		</xsl:if>
																	</input>
																	<xsl:variable name="unblockpos" select="position()"/>

																	<script>
																		var calCount = <xsl:value-of select="$unblockpos"/>;
																		calCount = calCount + "";
																	</script>
																	<script><![CDATA[

																	 Calendar.setup(
																	    {
																	      inputField  : "zipunblockdate"+calCount,  // ID of the input field
																	      ifFormat    : "mm/dd/y",  // the date format
													      				  button      : "zipcalendar"+calCount  // ID of the button
													    				}
													  				);
																	]]></script>
																
																</xsl:if>

																</td>

															</tr>
															
															
															<xsl:if test="deleteFlag = 'Y'">
																<script>
																	var zipRowCount = <xsl:value-of select="position()"/>;
																	zipRowCount = zipRowCount + "";
																
																	document.getElementById("zip_row" + zipRowCount).style.display = "none";
																</script>
															</xsl:if>
															
                                                                                                                </xsl:when>
                                                                                                                <xsl:otherwise>
                                                                                                                        <input name="zipblock{position()}" type="hidden" value="{blockedFlag}"/>
                                                                                                                        <input name="zipblockdate{position()}" type="hidden" value="{blockStartDate}"/>
                                                                                                                        <input name="zipunblockdate{position()}" type="hidden" value="{blockEndDate}"/>
                                                                                                                        <input name="cutofftime{position()}" type="hidden" value="{cutoffTime}"/>
                                                                                                                </xsl:otherwise>
                                                                                                                </xsl:choose>
                                                                                                                </xsl:for-each>
													</table>
											</td>
											<script>assignZipTabs();</script>
										</tr>
										
									
									</table>

									</td>
									</tr>
									
									<tr><td align="center"><input type="button" align="center" value="SAVE" name="savezip2" size="10" tabindex="9" onClick="saveAction();"></input></td></tr>

	</xsl:template>
</xsl:stylesheet>
