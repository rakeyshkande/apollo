<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="header">
<xsl:param name="headerName"/>
<xsl:param name="showTime"/>
<xsl:param name="showExitButton"/>

    <script type="text/javascript" language="javascript"><![CDATA[
      function doPopupExit(){
        if (window.doExitAction)
          doExitAction();
        else
          doReturnToMainMenuAction();
      }]]>
    </script>

    <script type="text/javascript" src="../js/clock.js"/>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="" width="20%" align="left"><img border="0" src="../images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td nowrap="" width="60%" align="center" colspan="1" class="header" id="headText"><xsl:value-of select="$headerName"/></td>
        <td nowrap="" width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <xsl:if test="$showTime">
              <tr>
                <td nowrap="" id="time" align="right" class="label"></td>
                <script type="text/javascript">startClock();</script>
              </tr>
            </xsl:if>
            <xsl:if test="$showExitButton">
              <tr>
                <td align="right">
                  <input type="button" tabindex="99" name="exitButtonHeader" id="exitButtonHeader" value="Exit" onclick="javascript:doPopupExit();"/>
                </td>
              </tr>
            </xsl:if>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>

</xsl:template>
</xsl:stylesheet>