<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External Templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="viewQueueTabs.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="messageQueue.xsl"/>
<xsl:import href="floristQueue.xsl"/>
<xsl:import href="zipQueue.xsl"/>

<!-- security parameters -->
<xsl:param name="web-context-root"/>
<xsl:param name="securitytoken"/>
<xsl:param name="context"/>
<xsl:param name="adminAction"/>

<xsl:output method="html" indent="yes"/>
<xsl:key name="processFlow" match="/root/processFlow/flow" use="name" />
<xsl:template match="/root">
	<html>
		<head>
			<title>FTD - Florist Maintenance</title>
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
			<script type="text/javascript" language="javascript" src="../js/util.js"/>
			<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
		 	<script type="text/javascript" src="../js/calendar.js" />
		 	<script type="text/javascript" src="../js/clock.js"/>

<script language="javascript1.2">
<![CDATA[


document.onkeypress=pressed;

function stripLeading(string,chr) {

   var finished = false;
   for (var i = 0; i < string.length && !finished; i++)
       if (string.substring(i,i+1) != chr) finished = true;
   if (finished) return string.substring(i-1); else return string;

}

function pressed(key)
{

	if (!event && window.event)
		event = window.event;
		
	if (event.ctrlKey)
	{
		if(event.keyCode != 17)
		{
			if(event.keyCode == 82) // remove action... r
			{
				if(floristMaint.fRemove2.disabled == false)
				{
					doRemoveAction();
					event.returnValue = false;
					event.keyCode = 550;
					return;
				}
			}
			else if(event.keyCode == 78) // next  n
			{
				doNextAction();
				event.returnValue = false;
				event.keyCode = 550;
				return;
			}
			else if(event.keyCode == 83) // save s
			{
				if(floristMaint.fSave2.disabled == false)
				{
				doSaveAction();
				event.returnValue = false;
				event.keyCode = 550;
				return;
				}
			}
			else if(event.keyCode == 69) // exit e
			{
				doExitAction();
				event.returnValue = false;
				event.keyCode = 550;
				return;
			}
			else if(event.keyCode == 13) // m
			{
					showItemAt(0);
					event.returnValue = false;
					event.keyCode = 550;
					return;
			}
			else if(event.keyCode == 70) // f
			{
					showItemAt(1);
					event.returnValue = false;
					event.keyCode = 550;
					return;
			}
			else if(event.keyCode == 90) // m
			{
					showItemAt(2);
					event.returnValue = false;
					event.keyCode = 550;
					return;
			}
			else if(event.keyCode == 65) // a
			{
				var pc = floristMaint.pcode.value;
			
				if(floristMaint.endTab.value == "zipQueue")
				{
					
					if(document.getElementById("zipNo" + pc) != null)
						document.getElementById("zipNo" + pc).focus();
					else
					{
						if(document.getElementById("zipNo1") != null)
							document.getElementById("zipNo1").focus();
					}
				}
				else if(floristMaint.endTab.value == "floristQueue")
				{
					//focus on the active radio button...
					if( document.getElementById("codA") != null &&
						document.getElementById("codA").disabled == false ) {
						document.getElementById("codA").focus();
					}
				}
				
				event.keyCode = 550;
				event.returnValue = false;
				return;
			}
			else if(event.keyCode == 79) // o
			{
			
				if(floristMaint.endTab.value == "zipQueue")
				{
					var pc = floristMaint.pcode.value;

					if(document.getElementById("cutofftime" + pc) != null)
					{
						document.getElementById("cutofftime" + pc).focus();
					}
					else
					{
						if(document.getElementById("cutofftime1") != null)
						document.getElementById("cutofftime1").focus();
					}
				}
				event.keyCode = 550;
				event.returnValue = false;
				return;
			}
			
		}
}

} // end of pressed function...

function init()
{

	document.getElementById("lockedrecord").style.display = "none";
	document.getElementById("ronly").style.display = "none";

	var eName;
	for (var i = 0; i < document.forms[0].elements.length; i++)
	{
		if(document.forms[0].elements[i].readOnly == true)
			document.forms[0].elements[i].style.backgroundColor = '#e0e0e2';

		eName = document.forms[0].elements[i].name;
		
		if(eName == "saveNeeded")
			continue;
			
		
		

		document.forms[0].elements[i].onchange = function(){  document.forms[0].saveNeeded.value = "yes";}

	}

	if(floristMaint.endTab.value == "")
		floristMaint.endTab.value = "messageQueue";

	var tab = floristMaint.endTab.value + "";
	initializeTabs(tab);
	
	if(document.getElementById("savezipNeeded").value == "yes")
	document.forms[0].saveNeeded.value = "yes";
	
	// inactive show/hide...
	
	
	if(floristMaint.lockFlag.value == "N")
		document.getElementById("lockedrecord").style.display = "block";
	else
		document.getElementById("lockedrecord").style.display = "none";
		
		
	if(floristMaint.statusActive.value == "Inactive")
		document.getElementById("ronly").style.display = "block";
	else
		document.getElementById("ronly").style.display = "none";
	
	if(floristMaint.lockFlag.value == "N" || floristMaint.statusActive.text == "Inactive") //  its locked or inactive...
	{
		floristMaint.fSave1.disabled = "true";
		floristMaint.fSave2.disabled = "true";
		floristMaint.fRemove1.disabled = "true";
		floristMaint.fRemove2.disabled = "true";
		floristMaint.zSave1.disabled = "true";
		floristMaint.zSave2.disabled = "true";
		floristMaint.zRemove1.disabled = "true";
		floristMaint.zRemove2.disabled = "true";
		floristMaint.mRemove1.disabled = "true";
		floristMaint.mRemove2.disabled = "true";
	}

	
	if(document.getElementById("listInvalidPop").value.length > 0)
	{
		document.getElementById("errorInvalidPopZips").style.display = "block";
		document.getElementById("holderrorInvalidPopZips").style.display = "none";
	}
	else
	{
		document.getElementById("errorInvalidPopZips").style.display = "none";
	}
		
    assignZipTabs();
    
    var today = new Date();
	todayDate = (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getYear();
	document.getElementById('codblockdate').value = todayDate;
	
	if (document.getElementById('gotofloristflag').value == 'Y') {
	    document.getElementById("blockDiv").style.display = "none";
	}
	
}

function doExitAction()
{
	doReturnAction();
}

function doReturnAction()
{

	if( floristMaint.saveNeeded.value == "yes")
	{
		var response = confirm("Changes have been made but not saved, are you sure you want to exit?");
		if (!response)
		{
			return;
		}
	}
	document.getElementById("lockedrecord").style.display = "none";
	document.getElementById("ronly").style.display = "none";
	
	showWaitMessage("outerContent", "wait", "Processing");
	document.forms[0].action = "ViewQueueServlet?action=exitViewQueue";
	document.forms[0].submit();
}

function doSaveAction()
{

if(!validateFloristQueue())
	return;
	
var result;

if(isZipPopNeeded())
{

result = doUnblockZipPopUp();

}
if(result == false)
{

currentTabIndex = 2;
floristMaint.endTab.value = "zipQueue";
showTab(tabsArray[2]);
return;	

}		
		
		document.getElementById("lockedrecord").style.display = "none";
		document.getElementById("ronly").style.display = "none";
		doSaveSubmit();
		
}

function doSaveSubmit()
{
	document.forms[0].action = "ViewQueueServlet?action=updateFlorist&qtyp=s";
	showWaitMessage("outerContent", "wait", "Processing");
	if (document.getElementById('totalcod') != null) {
		for( var i=1; i <= floristMaint.totalcod.value; i++) {
		   if(document.getElementById('codblock' + i) != null) {
		       document.getElementById('codblock' + i).disabled = false;
		       document.getElementById('codpermblock' + i).disabled = false;
		       document.getElementById('codunblockdate' + i).disabled = false;
		   }
		}
	}
	document.forms[0].submit();
}

function doRemoveAction()
{

if(!validateFloristQueue())
	return;
	
	if( floristMaint.saveNeeded.value == "yes")
	{
		var response = confirm("Changes have been made but not saved, message must be saved before it can be removed. Do you want to save the message?");
		if (!response)
		{
			return;
		}
		else
		{
			doSaveAction();
			return;
		}
	}
	else
	{
		var response = confirm("Are you sure you want to remove this message from the Queue?");
		if (!response)
		{
			return;
		}
	}
		document.getElementById("lockedrecord").style.display = "none";
		document.getElementById("ronly").style.display = "none";
		floristMaint.saveNeeded.value = "no";
		floristMaint.endTab.value = "messageQueue";
		showWaitMessage("outerContent", "wait", "Processing");
		document.forms[0].action = "ViewQueueServlet?action=removeMessage";
		document.forms[0].submit();
}



function doNextAction()
{
	if( floristMaint.saveNeeded.value == "yes")
	{
		var response = confirm("Changes have been made but not saved, are you sure you want to proceed to the next message?");
		if (!response)
		{
			return;
		}
	}
	
	// clear all errors...
	
		document.getElementById("lockedrecord").style.display = "none";
		
		document.getElementById("ronly").style.display = "none";
		floristMaint.saveNeeded.value = "no";
		floristMaint.endTab.value = "messageQueue";
		showWaitMessage("outerContent", "wait", "Processing");
		document.forms[0].action = "ViewQueueServlet?action=nextMessage";
		document.forms[0].submit();
}




]]>
</script>
	
		</head>
			
		<body onkeydown="pressed(event);">
			<form name="floristMaint" method="post">
			
			<input type="hidden" name="mNum" value="{viewQueueData/generalData/floristNumber}"/>
			<input type="hidden" name="mercuryId" value="{viewQueueData/generalData/mercuryId}"/>
			<input type="hidden" name="lockFlag" value="{viewQueueData/generalData/lockedFlag}"/>
   			<input type="hidden" name="blocktype" value="{viewQueueData/generalData/blockType}"/>
			<input type="hidden" name="statusActive" value="{viewQueueData/generalData/status}"/>
			<input type="hidden" name="pcode" value=""/>
			
						
			<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
			<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
			<input type="hidden" name="context" value="{$context}"/>
			<input type="hidden" name="adminAction" value="{$adminAction}"/>

    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="" width="20%"  align="left"><img border="0" src="../images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td nowrap="" width="60%"  align="center" colspan="1" class="header" id="headText">View Queue</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td nowrap="" id="time" align="right" class="label"></td>
                <script type="text/javascript">startClock();</script>
              </tr>
              <tr>
                <td align="right">
					<button style="font-size: 8pt;" onclick="javascript:doExitAction();" tabindex="99"><span style="text-decoration:underline;">E</span>xit</button>
                </td>
              </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>

			<table width="800">
							<tr>
								<td height="18" width="1%">
								<input type="hidden" name="saveNeeded" value="{key('processFlow', 'saveNeeded')/value}"></input>
								</td>
								<td id="ronly" width="10%" align="left" class="RequiredFieldTxt" style="display:none"> Read Only</td>
								<td id="lockedrecord" width="89%" align="left" class="RequiredFieldTxt" style="display:none"> This florist record is currently locked by user <xsl:value-of select="viewQueueData/generalData/lockedByUser"/>. You are unable to make changes to the record while it is locked.</td>
							</tr>
						</table>
<!-- Outer div needed to hold any content that will be blocked when an action is performed -->
<div id="outerContent" style="display:block">

						
					

<xsl:call-template name="viewQueueTabs"/>

<ul id="content">


<div id="messageQdiv">
	<table width="100%" border="0">
	<tr><td>
		<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<xsl:call-template name="messageQueue"/>
		</table>
	</td></tr>
		<tr>
		
			<td align="right">
			<button style="font-size: 8pt;" onclick="javascript:doExitAction();" tabindex="110"><span style="text-decoration:underline;">E</span>xit</button>
			</td>
		</tr>
		<tr><td><xsl:call-template name="footer"/></td></tr>
	</table>
</div>


<div id="floristQdiv">
	<table width="100%" border="0">
	<tr><td>
		<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<xsl:call-template name="floristQueue"/>
		</table>
	</td></tr>
		<tr>
			<td align="right">
			<button style="font-size: 8pt;" onclick="javascript:doExitAction();" tabindex="9"><span style="text-decoration:underline;">E</span>xit</button>
			</td>
		</tr>
		<tr><td><xsl:call-template name="footer"/></td></tr>
	</table>
</div>

<div id="zipQdiv">

	<table width="100%" border="0">
	<tr><td>
		<table id="" width="100%" border="4" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<xsl:call-template name="zipQueue"/>
		</table>
	</td></tr>
		<tr>
			<td align="right">
				<button style="font-size: 8pt;" onclick="javascript:doExitAction();" tabindex="12"><span style="text-decoration:underline;">E</span>xit</button>
			</td>
		</tr>
		<tr><td><xsl:call-template name="footer"/></td></tr>
	</table>

</div>


</ul>
</div>

<input type="hidden" value="{key('processFlow', 'endTab')/value}" name="endTab" />
<input type="hidden" value="no" name="savezipNeeded"></input>

<div id="waitDiv" style="display:none">
	<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
		<tr>
			<td width="100%">
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td id="waitMessage" align="right" width="50%" class="waitMessage" />
						<td id="waitTD" width="50%" class="waitMessage" />
					</tr>
				</table></td>
		</tr>
	</table>
</div>
		</form>
	</body>
</html>

<script type="text/javascript">

init();
</script>

</xsl:template>
</xsl:stylesheet>