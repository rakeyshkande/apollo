<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">
    <xsl:variable name="zipcodes" select="key('pageData', 'zipcodes')/value"/>


	<html>
		<head>	
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
 			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<title>FTD</title>
      
<script type="text/javascript" language="javascript">
<![CDATA[      
	function doCloseAction()
	{
        	window.close();
   }				
]]>
</script>
		</head>
		<body>
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="'Zip Code List'" />
				<xsl:with-param name="showExitButton" select="false()"/>
				<xsl:with-param name="showTime" select="false()"/>
			</xsl:call-template>
			
      <center>
			<input type="button" value=" Close " onClick="doCloseAction();"/>
      </center>
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td colspan="9" width="100%" align="center">
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td width="10%" class="colHeaderCenter">Zip/Postal Codes</td>
					</tr>
					<tr>
						<center>
						<td align="center"><textarea  readonly="true" name="zipcodes" wrap="physical" cols="80" rows="8" tabindex="1"><xsl:value-of select="$zipcodes"/></textarea></td>
						</center>
					</tr>
				</table>
				</td>
			</tr>
			</table>
      <center>
			<input type="button" value=" Close " onClick="doCloseAction();"/>
      </center>
			<xsl:call-template name="footer"/>
		</body>
	</html>
	
	</xsl:template>
</xsl:stylesheet>