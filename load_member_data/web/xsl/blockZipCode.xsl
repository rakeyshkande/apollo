<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">

<html>
<head>

<script type="text/javascript" language="javascript">
<![CDATA[
				function stripLeading(string,chr)
				{
					   var finished = false;
					   for (var i = 0; i < string.length && !finished; i++)
					       if (string.substring(i,i+1) != chr) finished = true;
					   if (finished) return string.substring(i-1); else return string;
				}
					
	function validateBlockZip()
	{

		var today = new Date();
	 	var todayDate = today.getDate();
		
		var uDate = blockZipCode.popupunblockdate.value;
		var dateArray = new Array();
	
		if(uDate.length > 0)
		{
			
			dateArray = uDate.split("/");
			
			if(dateArray.length == 2)
			{
			blockZipCode.popupunblockdate.value += "/" + today.getYear();
			uDate = blockZipCode.popupunblockdate.value;
			dateArray = uDate.split("/");
			}

			if(dateArray.length != 3)
			{
				document.getElementById("errorDate").style.display = "block";
				return false;
			}
			else
			{
				if(dateArray[2] != null && dateArray[2].length == 2)
				{
					dateArray[2] = "20" + dateArray[2];
					blockZipCode.popupunblockdate.value = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
				}
				
				if(! isDate(dateArray[2], stripLeading(dateArray[0], "0"), stripLeading(dateArray[1], "0") ))
			     {
			   		document.getElementById("invalidDateZip").style.display = "block";
			   		document.getElementById("invalidDate").style.display = "none";
					document.getElementById("errorDate").style.display = "none";
			   		return false;
			    }

			}

			var inMonth = stripCharsInBag (dateArray[0], "0")

			var isValid = "true";
			
			if(dateArray[2] > today.getYear())
				isValid = "true";
			else if(dateArray[2] < today.getYear())
				isValid = "false";
			else
			{
			
				if(dateArray[0] > today.getMonth()+1)
					isValid = "true"
				else if(dateArray[0] < today.getMonth()+1)
					isValid = "false";
				else
				{
					if(dateArray[1] >= today.getDate())
						isValid = "true";
					else if(dateArray[1] < today.getDate())
						isValid = "false";
					else
						isValid = "false";
				}
	
			}
			if(isValid == "false")
			{
				document.getElementById("invalidDate").style.display = "none";
				document.getElementById("invalidDateZip").style.display = "none";
				document.getElementById("errorDate").style.display = "block";
		   		return false;
			}
		}
	
	return true;
	
}

	function doCloseAction()
	{
	 	var ret = new Array();
	  	ret[0] = "closed";
		window.returnValue = ret;
		window.close();
 	}
 	
 	
 	function doSubmitAction()
 	{
	
 		document.getElementById("invalidDate").style.display = "none";
		document.getElementById("errorDate").style.display = "none";
		document.getElementById("invalidDateZip").style.display = "none";
 	
		if(!validateBlockZip())
			return;
			
		if(blockZipCode.popupunblockdate.value == "")
		{
			var agree = confirm("Are you sure you want to remove the zip codes?");
			if (!agree)
			{
			blockZipCode.popupunblockdate.focus();
			return;
			}
		}
			
  		var ret = new Array();
        ret[0] = blockZipCode.popupunblockdate.value;
		
        window.returnValue = ret;
	 	window.close();

 	}

]]>
</script>

				<title>FTD - Florist Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
				<script type="text/javascript" src="../js/calendar.js" />
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />

			</head>
			<body>
				<form name="blockZipCode">
					<input type="hidden" name="servlet_action"/>

					 <!-- Header Template -->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Block Zip Code'" />
						<xsl:with-param name="showExitButton" select="false()"/>
						<xsl:with-param name="showTime" select="true()"/>
					</xsl:call-template>

					<table align="center" width="65%">
					
					<tr>
						<td align="left" width="210">The following Zip Codes have been marked to be blocked:</td>
					</tr>
					
					<tr>
					
					<td align="left">
						<textarea name="newzipcodes" cols="36" rows="3" readonly="true">
						    <xsl:value-of select="key('pageData', 'zipcodes')/value" />
						</textarea>
					</td>
					
					</tr>
					
					

					<tr>
					<td>
						&nbsp;
					</td>
					</tr>
					
					<tr>
						<td align="left" width="200">
						Please enter a Block End Date and select Submit:
						</td>
					</tr>

					<tr>
					
						<td align="left">
						
						<!--<tabindex="3" maxlength="10" value="" onblur="dateMaker(this)" onfocus="javascript:fieldFocus();" onkeypress="javascript:dateFieldListener(this);"/> -->
						
							<input name="popupunblockdate" type="text" />
							  
							&nbsp;&nbsp;
  							<img id="calendar" src="../images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
						</td>

						<td width="500" align="left" id="errorDate" class="RequiredFieldTxt" style="display:none">
												Date must be greater than today's date.
					 	</td>
					 	<td align="left" id="invalidDate" class="RequiredFieldTxt" style="display:none">
												Invalid Format
					 	</td>
					 	<td align="left" id="invalidDateZip" class="RequiredFieldTxt" style="display:none">
												Invalid Date
					 	</td>
					 	
					<tr>
					
					
					<td> &nbsp; </td>
					
					</tr>

					<tr>
					<td width="200">
					Or if you wish to remove them, do not enter a date and select Submit
					</td>
					</tr>

					<tr><td> &nbsp; </td></tr>
					<tr align="center">

					<td>

					<input type="button" value=" Submit  " onClick="doSubmitAction();"/>

					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

					<input type="button" value=" Cancel " onClick="doCloseAction();"/>

					</td>

					</tr>

					</tr>

					</table>

					</form>
				</body>
			</html>
			<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "popupunblockdate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar"  // ID of the button
    }
  );
  
  blockZipCode.newzipcodes.style.backgroundColor = 'silver';
</script>
</xsl:template>
</xsl:stylesheet>