
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- External Templates -->
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>

  <xsl:param name="web-context-root"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:param name="adminAction"/>
  <xsl:param name="confirmation"/>
  <xsl:output method="html" indent="yes"/>
   <!-- Root template -->
   <xsl:template match="/">
      <html>
        <head>
          <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252"></meta>
          <meta http-equiv="X-UA-Compatible" content="IE=edge;" />
          <title>Exclusion Zone Maintenance</title>
          <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
          <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
          <link rel="stylesheet" type="text/css" href="../css/calendar.css" />
          <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
          <script type="text/javascript" src="../js/calendar.js" />
          <script type="text/javascript" src="../js/clock.js"/>
          <script type="text/javascript" src="../js/FormChek.js"/>
          <script type="text/javascript" src="../js/jquery-3.2.1.js"/>
          <script type="text/javascript" src="../js/jquery-ui.js"/>

          <style>
              button.ui-datepicker-current { display: none; }
              .ui-autocomplete.ui-widget {
                  font-size: 10px;
              }
              .ui-autocomplete {
                  max-height: 250px;
                  overflow-y: auto;   /* prevent horizontal scrollbar */
                  overflow-x: hidden; /* add padding to account for vertical scrollbar */
                  z-index:1000 !important;
              }
          </style>
          <script type="text/javascript">
              jQuery(document).ready(function () {
                  init();
              });

              function init() {
                  $('#submitButton').click(function () {
                    if (validatePage()) {
                      $(':button').prop('disabled', true);
                      $('#pleasewait').show();
                      var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
                      var url = webContextRoot + "/servlet/ExclusionZonesServlet";
                      var mySelections = [];
                      $('#blockStates :selected').each(function(i) {
                          mySelections.push(this.value);
                      });

		              jQuery.ajax({     
			              type: "POST",     
			              url: url,     
			              dataType: "text",
			              data: {
			                  action: "submit",
			                  securitytoken: $('#securitytoken').val(),
			                  context: $('#context').val(),
			                  description: $('#blockDescription').val(),
			                  startDate: $('#blockStartDate').val(),
			                  endDate: $('#blockEndDate').val(),
			                  blockCities: $('#blockCities').val(),
			                  blockStates: mySelections.join(","),
			                  blockZipCodes: $('#blockZipCodes').val()
			              },        
			              success: submitCallback,
                          error: function (xhr, status, error) {
                              $(':button').prop('disabled', false);
                              $('#pleasewait').hide();
                              alert('Error occurred: ' + status + ' || ' + error);
			              }                 
			          });
			        }
	              });
	              $('#addCity').click(function () {
                      var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
                      var url = webContextRoot + "/servlet/ExclusionZonesServlet";

		              jQuery.ajax({     
			              type: "POST",     
			              url: url,
			              data: {
			                  action: 'checkCity',
			                  securitytoken: $('#securitytoken').val(),
			                  context: $('#context').val(),
			                  cityName: $('#inputBlockCity').val()
			              },
			              dataType: "text",        
			              success: cityCallback,
                          error: function (xhr, status, error) {
                              alert('Error occurred: ' + status + ' || ' + error);
			              }                 
			          }); 
	              });
	              $('#inputBlockCity').autocomplete({
	                  source: function(request, response) {
                          var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
                          var url = webContextRoot + "/servlet/ExclusionZonesServlet";

    		              jQuery.ajax({     
			                  type: "POST",     
			                  url: url,
			                  data: "city=" + request.term,    
			                  data: {
			                      action: 'autoCity',
			                      securitytoken: $('#securitytoken').val(),
			                      context: $('#context').val(),
			                      city: request.term
			                  },
			                  dataType: "json",        
			                  success: function (data) {
			                      response(data);
			                  },
                              error: function (xhr, status, error) {
                                  alert('Error occurred: ' + status + ' || ' + error);
			                  }                 
			              }); 
	                  },
	                  minLength: 3,
	                  delay: 1000
	              });
	              
	              $('#exitButton, #exitButtonExisting').click(function () {
	                  doExitAction();
	              });

<![CDATA[
	              $('#blockCities').on('mouseup', function(eventData) {
				      if ($(this).val() != '') {
                          var scrollPosition = $(this).scrollTop();
                          var lineHeight = $(this).css("line-height");
                          lineHeight = parseInt(lineHeight.substring(0, lineHeight.length - 2));
                          var line = Math.floor((eventData.offsetY + scrollPosition) / lineHeight);
                          var tempArray = $(this).val().split('\n');
    					  if (tempArray.length > line) {
                              var value = $(this).val();
							  var replaceVal = tempArray[line];
							  if ( (line+1) < tempArray.length) {
							      replaceVal += '\n';
							  } else if (tempArray.length > 1) {
							      replaceVal = '\n' + replaceVal;
							  }
                              var newVal = value.replace(replaceVal, '');
                              $(this).val(newVal);
					      }
					  }
                  });
]]>
	              $('#blockStates').change(function () {
	                  showStateCount();
	              });
	              $('#addCity').button();
	              $('#submitButton').button();
	              $('#removeButton').button();
	              $('#exitButton').button();
	              $('#exitButtonHeader').button();
	              $('#exitButtonExisting').button();

                  $('#blockStartDate').datepicker({
                      showButtonPanel: true,
	                  showAnim: "slideDown",
	                  closeText : "Close"
                  });
                  $('#blockEndDate').datepicker({
                      showButtonPanel: true,
	                  showAnim: "slideDown",
	                  closeText : "Close"
                  });
	              
                  $("#submitDialog").dialog({
                      autoOpen: false,
                      modal: true,
                      resizable: false,
                      draggable: false,
                      buttons: {
                          "OK": function() {
                              $(this).dialog('close');
                          }
                      }
                  });
                  
                  $("#invalidCityDialog").dialog({
                      autoOpen: false,
                      modal: true,
                      resizable: false,
                      buttons: {
                          "OK": function() {
                              $(this).dialog('close');
                          }
                      }
                  });
                  
                  $("#showDetailsDialog").dialog({
                      autoOpen: false,
                      modal: true,
                      resizable: false,
                      maxHeight: 400,
                      buttons: {
                          "OK": function() {
                              $(this).dialog('close');
                          }
                      }
                  });
                  
                  $("#removeConfirm").dialog({
                      autoOpen: false,
                      resizable: false,
                      modal: true,
                      draggable: false,
                      buttons: {
                          "Yes": function() {
                              $( this ).dialog( "close" );
                              $(':button').prop('disabled', true);
                              var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
                              var url = webContextRoot + "/servlet/ExclusionZonesServlet?action=remove";
                              form1.action = url;
                              form1.submit();
                          },
                          "No": function() {
                              $( this ).dialog( "close" );
                          }
                      }
                  });

                  $('#removeButton').click(function () {
                      var checkedValues = $('input:checkbox:checked').map(function() {
                          return this.id;
                      }).get();
                      
                      if(checkedValues.length > 0) {
                          document.getElementById('removeIds').value = checkedValues.join(",");
                          $("#removeConfirm").dialog("open");
			          }
                  });

                  showStateCount();
                  $('#blockDescription').focus();

              }
              
              function showStateCount() {
	              var bcount = $('#blockStates :selected').length;
	              $("#stateCount").fadeOut(function () {
	                  $('#stateCount').text(bcount);
	              });
	              $("#stateCount").fadeIn();
              }
              
              function validatePage() {
                  $(':input')
                      .not(':button, :submit, :checkbox')
                      .css('background-color','white');
				  $('#submitErrors').html('');
                  var temp = $('#blockDescription');
                  if (temp.val() == '') {
                      $('#submitErrors').append("<li>Description is required</li>");
                      $('#blockDescription').css('background-color','pink');
                  }
                  var tempStartDate = $('#blockStartDate');
                  if (tempStartDate.val() == '') {
                      $('#submitErrors').append("<li>Block Start Date is required</li>");
					  $("#blockStartDate").css('background-color','pink');
                  }
                  var tempEndDate = $('#blockEndDate');
                  if (tempEndDate.val() == '') {
                      $('#submitErrors').append("<li>Block End Date is required</li>");
					  $("#blockEndDate").css('background-color','pink');
                  } else {
				      if (tempStartDate.val() != '') {
    	                  startList = tempStartDate.val().split("/");
	                      endList = tempEndDate.val().split("/");
	                      newStartDate = startList[2] + startList[0] + startList[1];
	                      newEndDate = endList[2] + endList[0] + endList[1];
	                      if (newStartDate > newEndDate) {
					          $('#submitErrors').append("<li>Block Start Date must be before Block End Date</li>");
        					  $("#blockStartDate").css('background-color','pink');
        					  $("#blockEndDate").css('background-color','pink');
				          } else {
						      var today = new Date();
							  var newToday = today.getFullYear() + '' + ('0' + (today.getMonth()+1)).substr(-2) + '' + ('0' + today.getDate()).substr(-2);
							  if (newToday > newEndDate) {
							      $('#submitErrors').append("<li>Block End Date cannot be before today's date</li>");
            					  $("#blockEndDate").css('background-color','pink');
							  }
				          }
				      }
				  }
				  var ccount = $("#blockCities").val().length;
				  var scount = $('#blockStates :selected').length;
				  var zcount = $("#blockZipCodes").val().length;
				  if ((ccount + scount + zcount) == 0) {
                      $('#submitErrors').append("<li>You need to block at least one city, state, or zip code</li>");
					  $("#blockCities").css('background-color','pink');
					  $("#blockStates").css('background-color','pink');
					  $("#blockZipCodes").css('background-color','pink');
				  }
				  var zipCodes = $("#blockZipCodes").val();
				  if (zipCodes.indexOf(',') >= 0 || zipCodes.indexOf(' ') >= 0) {
				      $('#submitErrors').append("<li>Zip Codes cannot contain commas or spaces</li>");
				      $("#blockZipCodes").css('background-color','pink');
				  }
				  
                  if ( $('#submitDialog ul li').length > 0 ) {
                      $("#submitDialog").dialog("open");
                      return false;
                  } else {
                      return true;
                  }
              }

              function cityCallback(data) {
                  if (data != 'NO') {
	                  var box = $("#blockCities");
                      box.val(box.val() + data + '\n');
                      $('#inputBlockCity').val('');
                  } else {
                      $("#invalidCityDialog").dialog("open");
                  }
                  $('#inputBlockCity').focus();
              }
              
              function submitCallback(data) {
                  if (data.length > 0) {
                      obj = JSON.parse(data);
                      $.each(obj.ezDetailList, function(newKey, newVal) {
                          var newRow = '';
                          newRow += '<tr style="background-color: #66CC66;">';
                          newRow += '<td>' + newVal.exclusion_description + '</td>';
                          newRow += '<td align="center">' + newVal.block_start_date + '</td>';
                          newRow += '<td align="center">' + newVal.block_end_date + '</td>';
                          if (newVal.city_cnt != '0') {
                              newRow += '<td align="center">';
                              newRow += '<a href="javascript:showDetails(';
                              newRow += newVal.exclusion_zone_header_id;
                              newRow += ', \'CITY\');">';
                              newRow += newVal.city_cnt
                              newRow += '</a>';
                              newRow += '</td>';
                          } else {
                              newRow += '<td align="center">0</td>';
                          }
                          if (newVal.state_cnt != '0') {
                              newRow += '<td align="center">';
                              newRow += '<a href="javascript:showDetails(';
                              newRow += newVal.exclusion_zone_header_id;
                              newRow += ', \'STATE\');">';
                              newRow += newVal.state_cnt
                              newRow += '</a>';
                              newRow += '</td>';
                          } else {
                              newRow += '<td align="center">0</td>';
                          }
                          if (newVal.zip_cnt != '0') {
                              newRow += '<td align="center">';
                              newRow += '<a href="javascript:showDetails(';
                              newRow += newVal.exclusion_zone_header_id;
                              newRow += ', \'ZIP\');">';
                              newRow += newVal.zip_cnt
                              newRow += '</a>';
                              newRow += '</td>';
                          } else {
                              newRow += '<td align="center">0</td>';
                          }
                          newRow += '<td align="center">';
                          newRow += '<input type="checkbox" id="' + newVal.exclusion_zone_header_id + '" style="zoom:0.75"/>';
                          newRow += '</td>';
                          newRow += '</tr>';
                          $('#existingTable tbody').prepend(newRow);
                      });
                  }
                  
                  $('#blockDescription, #blockStartDate, #blockEndDate, #blockCities, #blockStates, #blockZipCodes')
                      .val("");
                  $(':button').prop('disabled', false);
                  $('#pleasewait').hide();
                  showStateCount();
              }
              
        	  function doExitAction() {
   		          document.forms[0].action = "FloristSearchServlet?action=exitSystem";
		          document.forms[0].submit();
              }
              
              function showDetails(headerId, blockSource) {
                  var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
                  var url = webContextRoot + "/servlet/ExclusionZonesServlet";
		          jQuery.ajax({     
			          type: "POST",     
			          url: url,     
			          dataType: "text",
			          data: {
			              action: "showDetails",
			              securitytoken: $('#securitytoken').val(),
			              context: $('#context').val(),
			              headerId: headerId,
			              blockSource: blockSource
			          },        
			          success: showDetailsCallback,
                      error: function (xhr, status, error) {
                          $(':button').prop('disabled', false);
                          $('#pleasewait').hide();
                          alert('Error occurred: ' + status + ' || ' + error);
			          }                 
			      });
              }
              
              function showDetailsCallback(data) {
                  //alert(data);
                  $('#showDetailsList').html('');
                  if (data.length > 0) {
                      obj = JSON.parse(data);
                      $.each(obj, function(index, value) {
                          $('#showDetailsList').append("<li>" + value + "</li>");
                      });
                      $("#showDetailsDialog").dialog("open");
                  }
              }

          </script>
        </head>
        <body>
        
          <form name="form1" id="form1" method="post">
            <input type="hidden" name="securitytoken" id="securitytoken" value="{$securitytoken}"/>
            <input type="hidden" name="context" id="context" value="{$context}"/>
            <input type="hidden" name="adminAction" id="adminAction" value="{$adminAction}"/>
            <input type="hidden" name="removeIds" id="removeIds" value=""/>
            
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="'Exclusion Zone Maintenance'" />
				<xsl:with-param name="showExitButton" select="true()"/>
				<xsl:with-param name="showTime" select="true()"/>
			</xsl:call-template>

            <div id="submitDialog" title="Attention!" >            
                <ul id="submitErrors"></ul>                     
            </div>

            <div id="invalidCityDialog">            
                <p>Invalid City</p>                     
            </div>

            <div id="showDetailsDialog" title="" >            
                <ul id="showDetailsList"></ul>                     
            </div>

            <div id="removeConfirm" title="Confirmation">
                <p align="center">Are you sure?</p>
            </div>

            <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center"> 
   			      <table width="80%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
                    <tr>
                      <td>
                        <table width="90%" border="0" align="center" cellpadding="2" cellspacing="3">
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="label" align="left" width="40%">
                              Description:
                            </td>
                            <td class="label" align="left" width="30%">
                              Block Start Date:
                            </td>
                            <td class="label" align="left" width="30%">
                              Block End Date:
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <input type="text" id="blockDescription" size="40" maxlength="50" tabindex="100" />
                            </td>
                            <td>
                              <input type="text" id="blockStartDate" readonly="true" size="12" maxlength="10" tabindex="200" />
                            </td>
                            <td>
                              <input type="text" id="blockEndDate" readonly="true" size="12" maxlength="10" tabindex="300" />
                            </td>
                          </tr>
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="label" align="left">
                              Cities:
                              <div class="Instruction">Suggestions appear after entering at least three characters and one second of inactivity</div>
                            </td>
                            <td class="label" align="left">
                              States:
                              &nbsp;
                              <span id="stateCount" style="color:blue;font-weight:bolder;"></span>
                              <div class="Instruction">Use Ctrl key to select multiple states and to deselect a state</div>
                            </td>
                            <td class="label" align="left">
                              Zip Codes:
                              <div class="Instruction">One zip code per line, no commas or spaces</div>
                            </td>
                          </tr>
                          <tr>
                            <td valign="top">
                              <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
                                <tr>
                                  <td width="60%"> 
                                    <input type="text" id="inputBlockCity" size="35" maxlength="50" tabindex="400" />
                                  </td>
                                  <td width="40%" align="left">
                                    <input type="button" id="addCity" align="center" valign="top" value="Add City" tabindex="410" />
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <textarea rows="5" cols="50" id="blockCities" readonly="true" style="line-height: 10pt;" tabindex="-1"></textarea>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td valign="top">
                              <select id="blockStates" size="7" multiple="true" tabindex="500">
                                <xsl:for-each select="root/stateList/state">
                                  <option id="{statemasterid}">
                                    <xsl:value-of select="statename" />
                                  </option>
                                </xsl:for-each>
                              </select>
                            </td>
                            <td valign="top">
                               <textarea rows="7" cols="15" id="blockZipCodes" tabindex="600"></textarea>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="3" align="center">
                              <input type="button" id="submitButton" align="center" value="Submit" size="10" tabindex="900"></input>
                              &nbsp;&nbsp;&nbsp;
                              <input type="button" id="exitButton" align="center" value="Exit" size="10" tabindex="920"></input>
                              &nbsp;&nbsp;&nbsp;
                              <img src="../images/pleasewait.gif" id="pleasewait" border="0" hidden="true" align="middle"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table with="90%" border="0" align="center" cellpadding="2" cellspacing="2" id="existingTable">
                          <thead>
                          <tr>
                            <td class="header" colspan="7">Existing Exclusions</td>
                          </tr>
                          <tr>
                            <td width="20%" class="colHeaderLeft">Description</td>
                            <td width="15%" class="colHeaderCenter">Block Start Date</td>
                            <td width="15%" class="colHeaderCenter">Block End Date</td>
                            <td width="15%" class="colHeaderCenter">Blocked Cities</td>
                            <td width="15%" class="colHeaderCenter">Blocked States</td>
                            <td width="15%" class="colHeaderCenter">Blocked Zips</td>
                            <td width="5%" class="colheaderCenter">Remove</td>
                          </tr>
                          </thead>
                          <tbody>
                            <xsl:for-each select="root/ezDetailList/ezDetail">
                              <tr>
                                <td align="left">
                                  <xsl:value-of select="exclusion_description" />
                                </td>
                                <td align="center">
                                  <xsl:value-of select="block_start_date" />
                                </td>
                                <td align="center">
                                  <xsl:value-of select="block_end_date" />
                                </td>
                                <td align="center">
                                  <xsl:choose>
                                    <xsl:when test="city_cnt > 0">
                                      <a href="javascript:showDetails({exclusion_zone_header_id}, 'CITY');">
                                        <xsl:value-of select="city_cnt" />
                                      </a>
                                    </xsl:when>
                                    <xsl:otherwise>0</xsl:otherwise>
                                  </xsl:choose>
                                </td>
                                <td align="center">
                                  <xsl:choose>
                                    <xsl:when test="state_cnt > 0">
                                      <a href="javascript:showDetails({exclusion_zone_header_id}, 'STATE');">
                                        <xsl:value-of select="state_cnt" />
                                      </a>
                                    </xsl:when>
                                    <xsl:otherwise>0</xsl:otherwise>
                                  </xsl:choose>
                                </td>
                                <td align="center">
                                  <xsl:choose>
                                    <xsl:when test="zip_cnt > 0">
                                      <a href="javascript:showDetails({exclusion_zone_header_id}, 'ZIP');">
                                        <xsl:value-of select="zip_cnt" />
                                      </a>
                                    </xsl:when>
                                    <xsl:otherwise>0</xsl:otherwise>
                                  </xsl:choose>
                                </td>
                                <td align="center">
                                  <input type="checkbox" id="{exclusion_zone_header_id}" style="zoom:0.75">
								    <xsl:if test="remove_flag != 'Y'">
									  <xsl:attribute name="DISABLED" />
                                    </xsl:if>
                                  </input>
                                </td>
                              </tr>
                            </xsl:for-each>
                          </tbody>
                          <tfoot>
                          <tr>
                            <td colspan="7" align="center">
                              <input type="button" id="removeButton" align="center" value="Remove Selected Blocks" size="10" tabindex="940"></input>
                              &nbsp;&nbsp;&nbsp;
                              <input type="button" id="exitButtonExisting" align="center" value="Exit" size="10" tabindex="960"></input>
                            </td>
                          </tr>
                          </tfoot>
                        </table>
                      </td>
		            </tr>
                
                  </table>
                </td>
              </tr>

              <tr>
                <td width="100%" align="center" colspan="4">
                  <xsl:call-template name="footer"/>
                </td>
              </tr>
                
            </table>
          </form>
        </body>
      </html>   
   </xsl:template>
</xsl:stylesheet>