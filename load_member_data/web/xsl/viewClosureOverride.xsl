<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:template match="/root">

	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
 			<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
			<title>FTD: History</title>
			</head>
			<script type="text/javascript" language="javascript">
			<![CDATA[
				
	function doCloseAction()
	{
        	window.close();
   }
			]]>
			</script>

		<body>
		<form name="fullHist" method="post">
			<xsl:call-template name="header">
				<xsl:with-param name="headerName"  select="'Closure Override'" />
				<xsl:with-param name="showExitButton" select="false()"/>
				<xsl:with-param name="showTime" select="false()"/>
			</xsl:call-template>

        <center>

			<input type="button" value=" Close " onClick="doCloseAction();"/>

      </center>
      <br></br>
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td  colspan="9" class="tblheader" align="left"> Closure Override </td>
			</tr>

			<tr>


				<td colspan="9" width="100%" align="center">

					<div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">

			


													<table width="100%" border="0" cellpadding="2" cellspacing="2">
														<tr>
																<td width="50%" class="colHeaderCenter">Override Date</td>
																<td width="50%" class="colHeaderCenter">Created On</td>
														</tr>

														

														<xsl:for-each select="closureOverrideList/closureOverride">
													
									    				<xsl:choose>
										    				<xsl:when test="(position() mod 2) = 0">
										    				
															<tr class="resultsCellEven" style="display:block">
																<td width="50%" align="center">
																	<xsl:value-of select="day_of_week" />, <xsl:value-of select="override_date" />
																</td>
																<td width="50%" align="center">
																	<xsl:value-of select="created_on" />
																</td>
															</tr>
														
															</xsl:when>
															<xsl:otherwise>
										    				
															<tr style="display:block">
																<td width="50%" align="center">
																	<xsl:value-of select="day_of_week" />, <xsl:value-of select="override_date" />
																</td>
																<td width="50%" align="center">
																	<xsl:value-of select="created_on" />
																</td>
															</tr>
														
															</xsl:otherwise>
														</xsl:choose>


														</xsl:for-each>
													</table>
													</div>
				</td>
			</tr>

			</table>
				<br></br>
			 <center>

			<input type="button" value=" Close " onClick="doCloseAction();"/>

      </center>

													</form>
													</body>
	</html>
													
</xsl:template>
</xsl:stylesheet>