<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- External Templates -->
  <xsl:import href="footer.xsl"/>

  <xsl:param name="web-context-root"/>
  <xsl:param name="securitytoken"/>
  <xsl:param name="context"/>
  <xsl:param name="adminAction"/>
  <xsl:param name="confirmation"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:key name="pageData" match="/root/pageData/data" use="name" />
   <!-- Root template -->
   <xsl:template match="/">
      <html>
        <head>
          <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252"></meta>
          <title>Holiday Products Maintenance</title>
          <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
          <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
          <script type="text/javascript" src="../js/clock.js"/>
        
          <style type='text/css'>
            form{display:inline;}
            textarea{vertical-align:middle}
            input.button{vertical-align:middle}
          </style>

          <script type="text/javascript" language="javascript">
          <![CDATA[    
          function doRemove() {
              var hasRemoves = "N";
              for (var i=1; i<=prodCount; i++) {
                  if (document.getElementById("checkprod"+i).checked) {
                      hasRemoves = "Y";
                      break;
                  }
              }
              if (hasRemoves == "N") {
                  alert("Please select at least one product to be removed");
                  return;
              }
		      var response = confirm("Are you sure you want to remove these products?");
		      if (!response) {
			      return;
		      }
		      ]]>
              var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
              var url = webContextRoot + '/servlet/HolidayProductsServlet?action=removeProducts';
              document.getElementById('productCount').value = prodCount;
              form1.action = url;
              form1.submit();
          }
          
          function doAdd() {
              var strValues = document.getElementById('newproducts').value;
              if (strValues == '') {
                  alert('Please enter at least one product id');
                  return;
              }
              var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
              var url = webContextRoot + '/servlet/HolidayProductsServlet?action=addProducts';
              form1.action = url;
              form1.submit();
          }
          
          function doBack() {
              var webContextRoot = '/<xsl:value-of select="$web-context-root"/>';
              var url = webContextRoot + '/servlet/HolidayProductsServlet?action=load';
              form1.action = url;
              form1.submit();
          }
          
          <![CDATA[    
          function checkAll() {
              for (i = 1; i <= prodCount; i++) {
                  document.getElementById('checkprod'+i).checked = true;
              }
          }
          
          function uncheckAll() {
              for (i = 1; i <= prodCount; i++) {
                  document.getElementById('checkprod'+i).checked = false;
              }
          }
          
          function init() {
              //alert(form1.error_message.value);
              if (form1.error_message.value == '') {
                  document.getElementById("errorAddProducts").style.display = "none";
              } else {
                  document.getElementById("errorAddProducts").style.display = "block";
              }
          }
          
          ]]></script>
 
        </head>
        <body onload="javascript:init();">

          <form name="form1" method="post">
            <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
            <input type="hidden" name="context" value="{$context}"/>
            <input type="hidden" name="adminAction" value="{$adminAction}"/>
            <input type="hidden" name="delivery_date" value="{key('pageData','delivery_date')/value}"/>
            <input type="hidden" name="error_message" value="{key('pageData','error_message')/value}"/>
            <input type="hidden" name="productCount" value="0"/>
            
            <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		      <tr>
		        <td width="20%" align="left"><img border="0" src="../images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
		        <td width="60%" align="center" colspan="1" class="header" id="headText">
		            Holiday Products Maintenance - <xsl:value-of select="key('pageData','delivery_date')/value"/>
		        </td>
		        <td width="20%" align="right" class="label">
		          <table width="100%" cellspacing="0" cellpadding="0">
		              <tr>
		                <td id="time" align="right" class="label"></td>
		                <script type="text/javascript">startClock();</script>
		              </tr>
		              <tr>
		                <td align="right">
		                  <input type="button" tabindex="99" name="exitButtonHeader" value="Exit" onclick="document.forms[0].action = 'FloristSearchServlet?action=exitSystem'; document.forms[0].submit();"/>
		                </td>
		              </tr>
		          </table>
		        </td>
		      </tr>
		      <tr>
		        <td colspan="3"><hr/></td>
		      </tr>
		      
		      <tr>
                <td id="errorAddProducts" align="left" colspan="3" class="RequiredFieldTxt" style="display:none">
                  <xsl:value-of select="key('pageData','error_message')/value"/>
                </td>
		      </tr>

              <tr>
                <td colspan="3" align="center"> 
                  <table cellspacing="2" cellpadding="3" border="0" width="100%" align="center">
                    <tr>
                      <td width="20%" class="colHeaderCenter">Product Id</td>
                      <td width="20%" class="colHeaderCenter">Product Id</td> 
                      <td width="20%" class="colHeaderCenter">Product Id</td>
                      <td width="20%" class="colHeaderCenter">Product Id</td> 
                      <td width="20%" class="colHeaderCenter">Product Id</td> 
                    </tr>
                    <script><![CDATA[var prodCount=0;]]> </script>
                    <xsl:for-each select="root/holidayProducts/holidayProduct">
                      <script><![CDATA[
                        prodCount = prodCount + 1
                        if(((prodCount-1) % 5) == 0)
                        document.write("<tr>");
                      ]]></script>
                      <xsl:variable name="bgcolor">
                        <xsl:choose>
                          <xsl:when test="status = 'A'">#66cc66</xsl:when>
                          <xsl:otherwise>#FF6666</xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>
                      
                      <td align="left" style="display:block;background-color:{$bgcolor}">
                          <input type="checkbox" name="checkprod{position()}" tabindex="1"/>
                          &nbsp;
                          <xsl:value-of select="product_id"/>
                          <input type="hidden" name="productid{position()}" value="{product_id}"/>
                      </td>
                      <script><![CDATA[
                        if((prodCount % 5) == 0)
                        document.write("</tr>");
                      ]]></script>
                    </xsl:for-each>
                
		            <tr>
    		          <td colspan="5"><hr/></td>
		            </tr>
                
                    <tr>
                      <td colspan="2" align="center">
                        <a href="javascript:checkAll()"><img src="../images/all_check.gif" border="0" align="absmiddle"/></a>
                        <a href="javascript:uncheckAll()"><img src="../images/all_uncheck.gif" border="0" align="absmiddle"/></a>
                        &nbsp;&nbsp;
                        <input type="button" value="Remove Checked Products" size="10" tabindex="90" onClick="doRemove();"></input>
                      </td>
                      <td colspan="2" align="center" valign="middle">
                        <textarea name="newproducts" cols="36" rows="3" tabindex="92"/>
                        &nbsp;&nbsp;
                        <input type="button" value="Add New Products" size="10" tabindex="94" onClick="doAdd();"></input>
                      </td>
                      <td>
                        <input type="button" value="Back" size="10" tabindex="96" onClick="doBack();"></input>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

		      <tr>
		        <td colspan="3"><hr/></td>
		      </tr>
                
              <tr>
                <td width="100%" align="center" colspan="4">
                  <xsl:call-template name="footer"/>
                </td>
              </tr>
                
            </table>
          </form>
        </body>
      </html>   
   </xsl:template>
</xsl:stylesheet>