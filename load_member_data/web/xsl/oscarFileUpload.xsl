
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External Templates -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	
	<!-- security parameters -->
	<xsl:param name="web-context-root"/>
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>
	<xsl:param name="adminAction"/>
	
	<!--  Keys -->
	<xsl:key name="security" match="/root/security/data" use="name" />
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />		
    <xsl:output method="html" indent="yes"/>
	<xsl:template match="/root">
	<html>
		<head>
				<title>FTD - OSCAR File Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
				<script type="text/javascript" language="javascript" src="../js/util.js"/>
				<script type="text/javascript" language="javascript" src="../js/FormChek.js" />
				<script type="text/javascript" language="javascript">

<![CDATA[

    function validateForm(actionType)
    {  
      if (actionType == "upload") 
      { 
        var file_name = document.getElementById("OSCAR_file").value;
        if (file_name == "") {
          document.getElementById("error_OSCAR_file_mandatory").style.display = "block";
          return false;
        }      
      }
      return true;     
    }
    
	function doAction(actionType)
	{	    
	  if (validateForm(actionType))
	  {
	     if (actionType == "export") {
	       var winName = "win" + Math.round(1000*Math.random());
           window.open('',winName,'scrollbars=0,menubar=1,toolbar=0,location=0,status=0,resizable=1');           
           document.forms[0].target = winName;           
	     }
	     else
	     {
	       document.forms[0].target = "";
	       showWaitMessage("content", "wait", "Processing");		   
	     }
	     document.forms[0].actionType.value = actionType;			
		 document.forms[0].action = "OSCARFileUploadServlet";
		 document.forms[0].submit();
	  }
	}
	
  	function doExitAction()
    {
      document.forms[0].target = "";
      document.forms[0].adminAction.value = "distributionFulfillment";
      document.forms[0].actionType.value = "exitSystem";        
      document.forms[0].action = "OSCARFileUploadServlet";
	  document.forms[0].submit();   		
    }
    
]]>
	</script>
	</head>
	<body>
	  <form name="OSCARFileUploadForm" method="post" action="OSCARFileUploadServlet" enctype="multipart/form-data">
		<input type="hidden" name="web-context-root" value="{$web-context-root}"/>
		<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
		<input type="hidden" name="context" value="{$context}"/>
		<input type="hidden" name="adminAction" value="{$adminAction}"/>	
    	<input type="hidden" name="actionType" value="{key('pageData','actionType')/value}"/>	

		<!-- Header Template -->
		<xsl:call-template name="header">
		  <xsl:with-param name="headerName"  select="'OSCAR File Maintenance'" />
		  <xsl:with-param name="showExitButton" select="true()"/>
		  <xsl:with-param name="showTime" select="true()"/>
		</xsl:call-template>
		
		<div id="content" style="display:block">
          <table id="" width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
            <tr>
			  <td>
				<br></br>
			    <table width="100%" border="3" cellpadding="2" cellspacing="0">
			      <!--************* OSCAR AREA *************-->
				  <tr valign="top">
				    <table width="100%" border="0" cellpadding="2" cellspacing="0">
					  <!-- EXPORT AREA -->
					  <tr>
						<td class="label" style="font-size:12pt; font-weight: bold;" width="100%" align="left">
						               Current Status of Zip Codes and City/State Areas
						</td>
                      </tr>
					  <tr>
					    <td class="label" width="80%" align="left" valign="top">
                          <button type="button" id="buttonExport" class="bluebutton" tabindex="5" onclick="doAction('export');">Export</button>
 						           &nbsp;&nbsp;Click Export to export a file containing the current status of zip codes and city/state areas.
						</td>
					  </tr>								
                      <tr><td>&nbsp;</td></tr>
			          <tr><td>&nbsp;</td></tr>
					  <tr>
					    <td class="label" style="font-size:12pt; font-weight: bold;" width="100%" align="left">
						       OSCAR File Uploads
						</td>
					  </tr>
					  <tr>
					    <td class="label" width="100%" align="left">
						       Choose which file you want to upload:
						</td>
					  </tr>
					  <tr>
					    <td class="label" width="100%" align="left" valign="top">
						  <input type="radio" name="uploadType" id="uploadType" value="zipCode" checked="true" tabindex="5"/> Zip Code
						</td>
					  </tr>
					  <tr>
					    <td class="label" width="100%" align="left" valign="top">
						  <input type="radio" name="uploadType" id="uploadType" value="cityState" tabindex="5"/> City/State
						</td>
					  </tr>
					  
					  <!--************* File selector *************-->
					  <tr>
					    <!-- Input File (.Xls) -->
						<td class="label" width="100%" align="left" valign="top">
						  <input type="file" name="OSCAR_file" id="OSCAR_file" tabindex="5"/>
					    </td>
					  </tr>
					  <tr>
					    <td colspan="3" id="error_OSCAR_file_mandatory" class="RequiredFieldTxt" style="display:none">
						         &nbsp; &nbsp; A valid file name is required for uploading.
						</td>											
					  </tr>
					  <tr>
					    <td>
						  <button type="button" id="buttonUpload" class="bluebutton" tabindex="5" onclick="doAction('upload');">Upload</button>
							     &nbsp;&nbsp;&nbsp;
						  <button type="button" id="buttonExit" class="bluebutton" tabindex="5" onclick="doExitAction();">Exit</button>
						</td>
					  </tr>
                      <tr><td >&nbsp;</td></tr>
                      <tr><td >&nbsp;</td></tr>
	                  <tr>
	                    <td id="noresults" class="RequiredFieldTxt">								                           
		                  <xsl:value-of select="/root/errorMessage"/>									                       
			            </td>			            
			          </tr>
			          <tr>
			            <td id="results" class="Instruction">								                           
		                  <xsl:value-of select="/root/message"/>									                       
			            </td>
			          </tr>
			          <tr><td >&nbsp;</td></tr>
			        </table>
			      </tr>
	            </table>
	          </td>
			</tr>
		  </table>
		</div>

		<div id="waitDiv" style="display:none">
		  <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
			<tr>
			  <td width="100%">
				<table width="100%" cellspacing="0" cellpadding="0">
				  <tr>
					<td id="waitMessage" align="right" width="50%" class="waitMessage" />
					<td id="waitTD" width="50%" class="waitMessage" />
				  </tr>
				</table>
			  </td>
			</tr>
		  </table>
		</div>

		<xsl:call-template name="footer"/>
	  </form>
	</body>
 	</html>
	</xsl:template>
</xsl:stylesheet>