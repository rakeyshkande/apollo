
package com.ftd.eros.events.member.suspends.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.eros.events.member.suspends.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MemberSuspendsChanged_QNAME = new QName("http://eros.ftd.com/events/member/suspends/v1/", "memberSuspendsChanged");
    private final static QName _MemberSuspendsChangedResponse_QNAME = new QName("http://eros.ftd.com/events/member/suspends/v1/", "memberSuspendsChangedResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.eros.events.member.suspends.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Listings }
     * 
     */
    public Listings createListings() {
        return new Listings();
    }

    /**
     * Create an instance of {@link MemberSuspendsChanged }
     * 
     */
    public MemberSuspendsChanged createMemberSuspendsChanged() {
        return new MemberSuspendsChanged();
    }

    /**
     * Create an instance of {@link Listing }
     * 
     */
    public Listing createListing() {
        return new Listing();
    }

    /**
     * Create an instance of {@link Member }
     * 
     */
    public Member createMember() {
        return new Member();
    }

    /**
     * Create an instance of {@link MemberSuspendsChangedResponse }
     * 
     */
    public MemberSuspendsChangedResponse createMemberSuspendsChangedResponse() {
        return new MemberSuspendsChangedResponse();
    }

    /**
     * Create an instance of {@link Suspends }
     * 
     */
    public Suspends createSuspends() {
        return new Suspends();
    }

    /**
     * Create an instance of {@link Suspend }
     * 
     */
    public Suspend createSuspend() {
        return new Suspend();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberSuspendsChanged }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eros.ftd.com/events/member/suspends/v1/", name = "memberSuspendsChanged")
    public JAXBElement<MemberSuspendsChanged> createMemberSuspendsChanged(MemberSuspendsChanged value) {
        return new JAXBElement<MemberSuspendsChanged>(_MemberSuspendsChanged_QNAME, MemberSuspendsChanged.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberSuspendsChangedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eros.ftd.com/events/member/suspends/v1/", name = "memberSuspendsChangedResponse")
    public JAXBElement<MemberSuspendsChangedResponse> createMemberSuspendsChangedResponse(MemberSuspendsChangedResponse value) {
        return new JAXBElement<MemberSuspendsChangedResponse>(_MemberSuspendsChangedResponse_QNAME, MemberSuspendsChangedResponse.class, null, value);
    }

}
