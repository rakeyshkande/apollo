
package com.ftd.eros.events.member.suspends.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cityStateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="listingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="listingType" type="{http://eros.ftd.com/events/member/suspends/v1/}listingType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listing", propOrder = {
    "cityStateNumber",
    "listingCode",
    "listingType"
})
public class Listing {

    @XmlElement(required = true)
    protected String cityStateNumber;
    @XmlElement(required = true)
    protected String listingCode;
    @XmlElement(required = true)
    protected ListingType listingType;

    /**
     * Gets the value of the cityStateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityStateNumber() {
        return cityStateNumber;
    }

    /**
     * Sets the value of the cityStateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityStateNumber(String value) {
        this.cityStateNumber = value;
    }

    /**
     * Gets the value of the listingCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListingCode() {
        return listingCode;
    }

    /**
     * Sets the value of the listingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListingCode(String value) {
        this.listingCode = value;
    }

    /**
     * Gets the value of the listingType property.
     * 
     * @return
     *     possible object is
     *     {@link ListingType }
     *     
     */
    public ListingType getListingType() {
        return listingType;
    }

    /**
     * Sets the value of the listingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListingType }
     *     
     */
    public void setListingType(ListingType value) {
        this.listingType = value;
    }

}
