
package com.ftd.eros.events.member.suspends.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for memberClassification.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="memberClassification">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SYSTEM"/>
 *     &lt;enumeration value="FLORIST"/>
 *     &lt;enumeration value="FAH"/>
 *     &lt;enumeration value="FOL"/>
 *     &lt;enumeration value="WEBGIFTS"/>
 *     &lt;enumeration value="RETRANS"/>
 *     &lt;enumeration value="FTD_COM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "memberClassification")
@XmlEnum
public enum MemberClassification {

    SYSTEM,
    FLORIST,
    FAH,
    FOL,
    WEBGIFTS,
    RETRANS,
    FTD_COM;

    public String value() {
        return name();
    }

    public static MemberClassification fromValue(String v) {
        return valueOf(v);
    }

}
