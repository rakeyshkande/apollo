
package com.ftd.eros.events.member.close.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebService(name = "MemberDayClosedOverrideEvents", targetNamespace = "http://eros.ftd.com/events/member/close/v1/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface MemberDayClosedOverrideEvents {


    /**
     * 
     * @param member
     * @param generationDate
     * @param dayClosedOverrideInfo
     */
    @WebMethod
    @RequestWrapper(localName = "memberDayClosedOverride", targetNamespace = "http://eros.ftd.com/events/member/close/v1/", className = "com.ftd.eros.events.member.close.v1.MemberDayClosedOverride")
    @ResponseWrapper(localName = "memberDayClosedOverrideResponse", targetNamespace = "http://eros.ftd.com/events/member/close/v1/", className = "com.ftd.eros.events.member.close.v1.MemberDayClosedOverrideResponse")
    public void memberDayClosedOverride(
        @WebParam(name = "generationDate", targetNamespace = "http://eros.ftd.com/events/member/close/v1/")
        XMLGregorianCalendar generationDate,
        @WebParam(name = "member", targetNamespace = "http://eros.ftd.com/events/member/close/v1/")
        Member member,
        @WebParam(name = "dayClosedOverrideInfo", targetNamespace = "http://eros.ftd.com/events/member/close/v1/")
        DayClosedOverrideInfo dayClosedOverrideInfo);

}
