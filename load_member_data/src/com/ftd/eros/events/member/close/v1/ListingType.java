
package com.ftd.eros.events.member.close.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listingType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="listingType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALSO_SERVE"/>
 *     &lt;enumeration value="EXTRA_LISTING"/>
 *     &lt;enumeration value="OWS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "listingType")
@XmlEnum
public enum ListingType {

    ALSO_SERVE,
    EXTRA_LISTING,
    OWS;

    public String value() {
        return name();
    }

    public static ListingType fromValue(String v) {
        return valueOf(v);
    }

}
