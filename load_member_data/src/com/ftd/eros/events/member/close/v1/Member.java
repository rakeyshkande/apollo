
package com.ftd.eros.events.member.close.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="businessName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cityStateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="memberClassification" type="{http://eros.ftd.com/events/member/close/v1/}memberClassification"/>
 *         &lt;element name="memberCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="memberType" type="{http://eros.ftd.com/events/member/close/v1/}memberType"/>
 *         &lt;element name="listings" type="{http://eros.ftd.com/events/member/close/v1/}listings"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "member", propOrder = {
    "businessName",
    "cityStateNumber",
    "memberClassification",
    "memberCode",
    "memberType",
    "listings"
})
public class Member {

    protected String businessName;
    @XmlElement(required = true)
    protected String cityStateNumber;
    @XmlElement(required = true)
    protected MemberClassification memberClassification;
    @XmlElement(required = true)
    protected String memberCode;
    @XmlElement(required = true)
    protected MemberType memberType;
    @XmlElement(required = true)
    protected Listings listings;

    /**
     * Gets the value of the businessName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * Sets the value of the businessName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessName(String value) {
        this.businessName = value;
    }

    /**
     * Gets the value of the cityStateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityStateNumber() {
        return cityStateNumber;
    }

    /**
     * Sets the value of the cityStateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityStateNumber(String value) {
        this.cityStateNumber = value;
    }

    /**
     * Gets the value of the memberClassification property.
     * 
     * @return
     *     possible object is
     *     {@link MemberClassification }
     *     
     */
    public MemberClassification getMemberClassification() {
        return memberClassification;
    }

    /**
     * Sets the value of the memberClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link MemberClassification }
     *     
     */
    public void setMemberClassification(MemberClassification value) {
        this.memberClassification = value;
    }

    /**
     * Gets the value of the memberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberCode() {
        return memberCode;
    }

    /**
     * Sets the value of the memberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberCode(String value) {
        this.memberCode = value;
    }

    /**
     * Gets the value of the memberType property.
     * 
     * @return
     *     possible object is
     *     {@link MemberType }
     *     
     */
    public MemberType getMemberType() {
        return memberType;
    }

    /**
     * Sets the value of the memberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MemberType }
     *     
     */
    public void setMemberType(MemberType value) {
        this.memberType = value;
    }

    /**
     * Gets the value of the listings property.
     * 
     * @return
     *     possible object is
     *     {@link Listings }
     *     
     */
    public Listings getListings() {
        return listings;
    }

    /**
     * Sets the value of the listings property.
     * 
     * @param value
     *     allowed object is
     *     {@link Listings }
     *     
     */
    public void setListings(Listings value) {
        this.listings = value;
    }

}
