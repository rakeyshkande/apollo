
package com.ftd.eros.events.member.close.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.eros.events.member.close.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MemberDayClosedOverrideResponse_QNAME = new QName("http://eros.ftd.com/events/member/close/v1/", "memberDayClosedOverrideResponse");
    private final static QName _MemberDayClosedOverride_QNAME = new QName("http://eros.ftd.com/events/member/close/v1/", "memberDayClosedOverride");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.eros.events.member.close.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Listing }
     * 
     */
    public Listing createListing() {
        return new Listing();
    }

    /**
     * Create an instance of {@link DayClosedOverrideInfo }
     * 
     */
    public DayClosedOverrideInfo createDayClosedOverrideInfo() {
        return new DayClosedOverrideInfo();
    }

    /**
     * Create an instance of {@link Member }
     * 
     */
    public Member createMember() {
        return new Member();
    }

    /**
     * Create an instance of {@link MemberDayClosedOverrideResponse }
     * 
     */
    public MemberDayClosedOverrideResponse createMemberDayClosedOverrideResponse() {
        return new MemberDayClosedOverrideResponse();
    }

    /**
     * Create an instance of {@link Listings }
     * 
     */
    public Listings createListings() {
        return new Listings();
    }

    /**
     * Create an instance of {@link MemberDayClosedOverride }
     * 
     */
    public MemberDayClosedOverride createMemberDayClosedOverride() {
        return new MemberDayClosedOverride();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberDayClosedOverrideResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eros.ftd.com/events/member/close/v1/", name = "memberDayClosedOverrideResponse")
    public JAXBElement<MemberDayClosedOverrideResponse> createMemberDayClosedOverrideResponse(MemberDayClosedOverrideResponse value) {
        return new JAXBElement<MemberDayClosedOverrideResponse>(_MemberDayClosedOverrideResponse_QNAME, MemberDayClosedOverrideResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberDayClosedOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eros.ftd.com/events/member/close/v1/", name = "memberDayClosedOverride")
    public JAXBElement<MemberDayClosedOverride> createMemberDayClosedOverride(MemberDayClosedOverride value) {
        return new JAXBElement<MemberDayClosedOverride>(_MemberDayClosedOverride_QNAME, MemberDayClosedOverride.class, null, value);
    }

}
