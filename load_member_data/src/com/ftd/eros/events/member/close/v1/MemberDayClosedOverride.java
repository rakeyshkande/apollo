
package com.ftd.eros.events.member.close.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for memberDayClosedOverride complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="memberDayClosedOverride">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="generationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="member" type="{http://eros.ftd.com/events/member/close/v1/}member"/>
 *         &lt;element name="dayClosedOverrideInfo" type="{http://eros.ftd.com/events/member/close/v1/}dayClosedOverrideInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "memberDayClosedOverride", propOrder = {
    "generationDate",
    "member",
    "dayClosedOverrideInfo"
})
public class MemberDayClosedOverride {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar generationDate;
    @XmlElement(required = true)
    protected Member member;
    @XmlElement(required = true)
    protected DayClosedOverrideInfo dayClosedOverrideInfo;

    /**
     * Gets the value of the generationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGenerationDate() {
        return generationDate;
    }

    /**
     * Sets the value of the generationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGenerationDate(XMLGregorianCalendar value) {
        this.generationDate = value;
    }

    /**
     * Gets the value of the member property.
     * 
     * @return
     *     possible object is
     *     {@link Member }
     *     
     */
    public Member getMember() {
        return member;
    }

    /**
     * Sets the value of the member property.
     * 
     * @param value
     *     allowed object is
     *     {@link Member }
     *     
     */
    public void setMember(Member value) {
        this.member = value;
    }

    /**
     * Gets the value of the dayClosedOverrideInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DayClosedOverrideInfo }
     *     
     */
    public DayClosedOverrideInfo getDayClosedOverrideInfo() {
        return dayClosedOverrideInfo;
    }

    /**
     * Sets the value of the dayClosedOverrideInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DayClosedOverrideInfo }
     *     
     */
    public void setDayClosedOverrideInfo(DayClosedOverrideInfo value) {
        this.dayClosedOverrideInfo = value;
    }

}
