
package com.ftd.eros.events.member.availability.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for memberType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="memberType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="STAND_ALONE"/>
 *     &lt;enumeration value="MAIN"/>
 *     &lt;enumeration value="BRANCH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "memberType")
@XmlEnum
public enum MemberType {

    STAND_ALONE,
    MAIN,
    BRANCH;

    public String value() {
        return name();
    }

    public static MemberType fromValue(String v) {
        return valueOf(v);
    }

}
