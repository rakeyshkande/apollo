
package com.ftd.eros.events.member.availability.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ftd.eros.events.member.availability.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MemberCommunicationStatusChanged_QNAME = new QName("http://eros.ftd.com/events/member/availability/v1/", "memberCommunicationStatusChanged");
    private final static QName _MemberCommunicationStatusChangedResponse_QNAME = new QName("http://eros.ftd.com/events/member/availability/v1/", "memberCommunicationStatusChangedResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ftd.eros.events.member.availability.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Listings }
     * 
     */
    public Listings createListings() {
        return new Listings();
    }

    /**
     * Create an instance of {@link MemberCommunicationStatusChanged }
     * 
     */
    public MemberCommunicationStatusChanged createMemberCommunicationStatusChanged() {
        return new MemberCommunicationStatusChanged();
    }

    /**
     * Create an instance of {@link Member }
     * 
     */
    public Member createMember() {
        return new Member();
    }

    /**
     * Create an instance of {@link MemberCommunicationStatusChangedResponse }
     * 
     */
    public MemberCommunicationStatusChangedResponse createMemberCommunicationStatusChangedResponse() {
        return new MemberCommunicationStatusChangedResponse();
    }

    /**
     * Create an instance of {@link Listing }
     * 
     */
    public Listing createListing() {
        return new Listing();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberCommunicationStatusChanged }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eros.ftd.com/events/member/availability/v1/", name = "memberCommunicationStatusChanged")
    public JAXBElement<MemberCommunicationStatusChanged> createMemberCommunicationStatusChanged(MemberCommunicationStatusChanged value) {
        return new JAXBElement<MemberCommunicationStatusChanged>(_MemberCommunicationStatusChanged_QNAME, MemberCommunicationStatusChanged.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberCommunicationStatusChangedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eros.ftd.com/events/member/availability/v1/", name = "memberCommunicationStatusChangedResponse")
    public JAXBElement<MemberCommunicationStatusChangedResponse> createMemberCommunicationStatusChangedResponse(MemberCommunicationStatusChangedResponse value) {
        return new JAXBElement<MemberCommunicationStatusChangedResponse>(_MemberCommunicationStatusChangedResponse_QNAME, MemberCommunicationStatusChangedResponse.class, null, value);
    }

}
