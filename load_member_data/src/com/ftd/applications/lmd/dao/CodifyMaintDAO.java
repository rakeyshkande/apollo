package com.ftd.applications.lmd.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;

import org.w3c.dom.Document;

import com.ftd.applications.lmd.vo.CodificationMaintenanceVO;
import com.ftd.applications.lmd.vo.CodificationMasterVO;
import com.ftd.applications.lmd.vo.CodificationProductsVO;
import com.ftd.applications.lmd.vo.CodificationVO;
import com.ftd.applications.lmd.vo.ProductVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/*
 * DAO Used on Codified Maint pages.
 * 
 * @author Ed Mueller
 */
public class CodifyMaintDAO extends HttpServlet 
{

     private static final String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.dao.CodifyMaintDAO";
    private Logger logger;
    private Connection connection;
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE";    
    
    private static final String VIEW_CODIFICATION_INFO = "VIEW_CODIFIED_INFORMATION";
    private static final String CODIFICATION_MASTER_MAP = "CODE_MASTER_CUR";
    private static final String CODIFICATION_PRODUCT_MAP = "CODE_PROD_CUR";
    
    /**
     * Constructor
     * @param conn
     */
    public CodifyMaintDAO(Connection conn)
    {
        super();
        logger = new Logger(LOGGER_CATEGORY);   
        this.connection = conn;
    }
    
    /**
     * Retrive all Codified Product and Codified Master rows and put them into the 
     * CodifiedMaintanence VO
     * @return CodifiedMaintanenceVO
     * @throws java.lang.Exception
     */
    public CodificationMaintenanceVO getCodificationMaintenance() throws Exception
    {
        CodificationMaintenanceVO maintVO = new CodificationMaintenanceVO();;
    
        long time = System.currentTimeMillis();
        Map maintMap = getMaintMap();
        logger.debug("Codification Maintenance Load: " + (System.currentTimeMillis() - time));
        
        if(maintMap != null)
        {            
            maintVO.setCodificationMasterMap(getMasterMap(maintMap));
            maintVO.setCodificationProductMap(getProductMap(maintMap));
            
            // Set the changed flags to false
            maintVO.setChangeFlags(false);
        }
        
        return maintVO;        
    }
    
    /*
     * Get the Codification Mater Data
     * @return 
     * @throws java.lang.Exception
     */
    private Map getMaintMap() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_CODIFICATION_INFO);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Map)dataAccessUtil.execute(dataRequest);
    }    
    
    /*
     * Get Codification Master Data
     * @param maintMap
     * @return 
     * @throws java.lang.Exception
     */
    private Map getMasterMap(Map maintMap) throws Exception
    {
        HashMap masterMap = new HashMap();
        CodificationMasterVO masterVO = null;
    
        CachedResultSet rs = (CachedResultSet) maintMap.get(CODIFICATION_MASTER_MAP);
        while(rs != null && rs.next())
        {
            masterVO = new CodificationMasterVO();
            masterVO.setCodificationID((String)rs.getString("CODIFICATION_ID"));
            masterVO.setPreviousCodificationID((String)rs.getString("CODIFICATION_ID"));
            masterVO.setDescription((String)rs.getString("DESCRIPTION"));
            masterVO.setCategory_id((String)rs.getString("CATEGORY_ID"));
            masterVO.setCodifyAllFloristFlag((String)rs.getString("CODIFY_ALL_FLORIST_FLAG"));
            masterVO.setLoadFromFileFlag((String)rs.getString("LOAD_FROM_FILE")); 
            masterVO.setCodificationRequired((String)rs.getString("CODIFICATION_REQUIRED"));

            masterMap.put(masterVO.getCodificationID(), masterVO);
        }
        
        return masterMap;
    }
    
    /*
     * Get Codification Product Map
     * @param maintMap
     * @return 
     * @throws java.lang.Exception
     */
    private Map getProductMap(Map maintMap) throws Exception
    {
        HashMap productMap = new HashMap();
        CodificationProductsVO productVO = null;
        
        CachedResultSet rs = (CachedResultSet) maintMap.get(CODIFICATION_PRODUCT_MAP);
        while(rs != null && rs.next())
        {

            productVO = new CodificationProductsVO();
            productVO.setProductID((String)rs.getObject(1));
            //productVO.setNovatorProductID((String)rs.getObject(2));
            productVO.setProductName((String)rs.getObject(3));
            productVO.setStatus((String)rs.getObject(4));
            productVO.setCodificationID((String)rs.getObject(5));
            productVO.setDescription((String)rs.getObject(6));
            productVO.setCheckOEflag((String)rs.getObject(7));
            
            //check for null since this is anumber datatype
            if(rs.getObject(8) != null)
            {
                productVO.setFlagSequence(new Integer(((BigDecimal)rs.getObject(8)).intValue()));
            }            
            
            productMap.put(productVO.getProductID(), productVO);
        }
        
        return productMap;
    }    
    
    /*
     * Get Codification Product Map
     * @param maintMap
     * @return 
     * @throws java.lang.Exception
     */
    public ProductVO getProduct(String novatorId) throws Exception
    {
        HashMap productMap = new HashMap();
        ProductVO productVO = null;

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PRODUCT_BY_PRODUCT_ID");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        HashMap paramMap = new HashMap();
        paramMap.put("IN_PRODUCT_ID",novatorId); 
        dataRequest.setInputParams(paramMap);
        
        CachedResultSet rs =  (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if(rs != null && rs.next())
        {
            productVO = new ProductVO();
            productVO.setFtdID((String)rs.getObject(1));
            productVO.setName((String)rs.getObject(2));
            productVO.setStatus((String)rs.getObject(3));
        }
        
        return productVO;
    }        
    
    /**
     * Update the CodifiedProducts and CodificationMaster table
     * @param vo
     * @throws java.lang.Exception
     */
    public void updateCodificationMaintenance(CodificationMaintenanceVO vo) throws Exception
    {
        //update Codified Products
        updateCodifiedProducts(vo);

        //update Codification Master
        udpateCodificationMaster(vo);

    }
    
    /*
     * 
     * @param maintVO
     * @throws java.lang.Exception
     */
      private void udpateCodificationMaster(CodificationMaintenanceVO maintVO) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          List deleteList = new ArrayList();
          List changedList = new ArrayList();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;
          
          //loop through each zip
          Map masterMap = maintVO.getCodificationMasterMap();
          Iterator i = masterMap.entrySet().iterator();    
          while(i.hasNext()){
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              CodificationMasterVO masterVO = (CodificationMasterVO)masterMap.get(e.getKey());
              
              if(masterVO != null && masterVO.isChanged())
              {
                  //check if the codification changed
                  if(masterVO.getDeleteFlag() != null && masterVO.getDeleteFlag().equals("Y"))
                  {
                      dataRequest.setStatementID("DELETE_CODIFICATION_MASTER");
                      Map paramMap = new HashMap();
                      paramMap.put("IN_CODIFICATION_ID",masterVO.getCodificationID()); 
                      paramMap.put("IN_USER",maintVO.getLastUpdateUser()); 
                      dataRequest.setInputParams(paramMap);
                      outputs = (Map) dataAccessUtil.execute(dataRequest);
                      
                      deleteList.add(masterVO.getCodificationID());
                  }
                  else if(!masterVO.getCodificationID().equals(masterVO.getPreviousCodificationID()))
                  {
                      //codification id change
                	  logger.debug(masterVO.getPreviousCodificationID() + " changed to " +
                			  masterVO.getCodificationID());
                      dataRequest.setStatementID("CHANGE_CODIFICATION_ID");
                      Map paramMap = new HashMap();
                      paramMap.put("IN_PREVIOUS_CODIFICATION_ID",masterVO.getPreviousCodificationID());
                      paramMap.put("IN_CODIFICATION_ID",masterVO.getCodificationID()); 
                      paramMap.put("IN_CATEGORY_ID",masterVO.getCategory_id()); 
                      paramMap.put("IN_DESCRIPTION",masterVO.getDescription());
                      paramMap.put("IN_LOAD_FROM_FILE",masterVO.getLoadFromFileFlag()); 
                      paramMap.put("IN_CODIFY_ALL_FLORISTS_FLAG",masterVO.getCodifyAllFloristFlag()); 
                      paramMap.put("IN_CODIFICATION_REQUIRED",masterVO.getCodificationRequired()); 

                      dataRequest.setInputParams(paramMap);
                      outputs = (Map) dataAccessUtil.execute(dataRequest);     
                      
                      changedList.add(masterVO.getPreviousCodificationID());
                  }
                  else
                  {
                          dataRequest.setStatementID("UPDATE_CODIFICATION_MASTER");
                          Map paramMap = new HashMap();
                          paramMap.put("IN_CODIFICATION_ID",masterVO.getCodificationID()); 
                          paramMap.put("IN_CATEGORY_ID",masterVO.getCategory_id()); 
                          paramMap.put("IN_DESCRIPTION",masterVO.getDescription());
                          paramMap.put("IN_LOAD_FROM_FILE",masterVO.getLoadFromFileFlag()); 
                          paramMap.put("IN_CODIFY_ALL_FLORISTS_FLAG",masterVO.getCodifyAllFloristFlag()); 
                          paramMap.put("IN_CODIFICATION_REQUIRED",masterVO.getCodificationRequired());
                          dataRequest.setInputParams(paramMap);
                          outputs = (Map) dataAccessUtil.execute(dataRequest);
                  }
                  
                  String status = (String) outputs.get(STATUS_PARAM);
                  if(status != null && status.equals("N"))
                  {
                      String message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }   
                  
                  //check if global unblock nees to be done
                  if(masterVO.getGlobalUnblockFlag().equals("Y"))
                  {
                      globalUnblock(masterVO.getCodificationID());
                  }
                  
                  //check if a codify or uncodify needs to be done
                  if(masterVO.isCodifyAllFloristFlagChanged())
                  {
                      if(masterVO.getCodifyAllFloristFlag().equals("Y"))
                      {
                          codifyAllProducts(masterVO.getCodificationID(), maintVO.getLastUpdateUser());
                      }
                      else
                      {
                          uncodifyAllProducts(masterVO.getCodificationID(),maintVO.getLastUpdateUser());
                      }
                  }
              }
          }//end while
          
          // clear deletes from memory
          for(int j=0; j < deleteList.size(); j++)
          {
              maintVO.getCodificationMasterMap().remove(deleteList.get(j));
              logger.debug(deleteList.get(j) + " removed from Codification List");
          }

          // clear changes from memory
          for(int j=0; j < changedList.size(); j++)
          {
              maintVO.getCodificationMasterMap().remove(changedList.get(j));
              logger.debug(changedList.get(j) + " removed from Codification List");
          }
      }
      
      
    /*
     * 
     * @param maintVO
     * @throws java.lang.Exception
     */
      private void updateCodifiedProducts(CodificationMaintenanceVO maintVO) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          List deleteList = new ArrayList();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;
          
          //loop through each zip
          Map postalMap = maintVO.getCodificationProductMap();        
          Iterator i = postalMap.entrySet().iterator();    
          while(i.hasNext())
          {
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              CodificationProductsVO productVO = (CodificationProductsVO)postalMap.get(e.getKey());
              
              if(productVO != null && productVO.isChanged())
              {
                  if(productVO.getDeleteFlag() != null && productVO.getDeleteFlag().equals("Y"))
                  {
                      dataRequest.setStatementID("DELETE_CODIFIED_PRODUCT");
                      Map paramMap = new HashMap();
                      paramMap.put("IN_PRODUCT_ID",productVO.getProductID()); 
                      dataRequest.setInputParams(paramMap);
                      outputs = (Map) dataAccessUtil.execute(dataRequest);
                      
                      deleteList.add(productVO.getProductID());
                  }
                  else
                  {
                          // Update newly codified products
                          dataRequest.setStatementID("UPDATE_CODIFIED_PRODUCTS");
                          Map paramMap = new HashMap();
                          paramMap.put("IN_PRODUCT_ID",productVO.getProductID()); 
                          paramMap.put("IN_CHECK_OE_FLAG",productVO.getCheckOEflag());
						  
						  
						  if(productVO.getCheckOEflag().equals("Y") && productVO.getFlagSequence() == null)
						  {
							productVO.setFlagSequence(this.getNextSequence());  
						  }
						  
                          paramMap.put("IN_FLAG_SEQUENCE",productVO.getFlagSequence());
                          paramMap.put("IN_CODIFICATION_ID",productVO.getCodificationID());
                          dataRequest.setInputParams(paramMap);
                          outputs = (Map) dataAccessUtil.execute(dataRequest);
                  }
              }
          }//end while
                    
          // clear deletes from memory
          for(int j=0; j < deleteList.size(); j++)
          {
              maintVO.getCodificationProductMap().remove(deleteList.get(j));
          }
      }      
    
    
    

    /**
     * Codify all florists
     * @param maintVO
     * @throws java.lang.Exception
     */
      private void codifyAllProducts(String codificationID, String userID) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;
          
          dataRequest.setStatementID("CODIFY_ALL_FLORISTS");
          Map paramMap = new HashMap();
          paramMap.put("IN_CODIFICATION_ID",codificationID); 
          paramMap.put("IN_USER",userID); 
          dataRequest.setInputParams(paramMap);
          outputs = (Map) dataAccessUtil.execute(dataRequest);                      
                  
          String status = (String) outputs.get(STATUS_PARAM);
          if(status != null && status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }    

      }      
    
    /**
     * uncodify all florists
     * @param maintVO
     * @throws java.lang.Exception
     */
      private void uncodifyAllProducts(String codificationID, String userID) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;
          
          dataRequest.setStatementID("UNCODIFY_ALL_FLORISTS");
          Map paramMap = new HashMap();
          paramMap.put("IN_CODIFICATION_ID",codificationID); 
          paramMap.put("IN_USER",userID); 
          dataRequest.setInputParams(paramMap);
          outputs = (Map) dataAccessUtil.execute(dataRequest);                      
                  
          String status = (String) outputs.get(STATUS_PARAM);
          if(status != null && status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }    

      }    
    
    
    private boolean getNovatorIDExists(String novatorID) throws Exception
    {
    
        boolean productFound = false;
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PRODUCT_BY_NOVATOR_ID");
        dataRequest.addInputParam("NOVATOR_ID", novatorID);
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        List zipList = new ArrayList();
        
        CachedResultSet rs =  (CachedResultSet)dataAccessUtil.execute(dataRequest);    
        if(rs != null && rs.next())
        {            
            productFound = true;
        }
        
        return productFound;        
    }
    
    
    public CodificationVO getCodification(String codificationID) throws Exception
    {
    
        boolean found = false;
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("VIEW_CODIFICATIONS_BY_ID");
        dataRequest.addInputParam("CODIFIED_ID", codificationID);
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        List zipList = new ArrayList();
        
        CachedResultSet rs =  (CachedResultSet)dataAccessUtil.execute(dataRequest);    
        CodificationVO vo = null;
        if(rs != null && rs.next())
        {            
            vo = new CodificationVO();
            vo.setCodificationId((String)rs.getObject(1));
            vo.setCategoryId((String)rs.getObject(2));            
            vo.setDescription((String)rs.getObject(3));
        }
        
        return vo;        
    }        
    
    

    
    
   /** This method converts an XML document to a String. 
     * @param Document document to convert 
     * @return String version of XML */
    public static String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";       
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }          
        
   private void globalUnblock(String codificationId)
   {
       
   }
 
 
    /*
     * Get Codification Product Map
     * @param maintMap
     * @return 
     * @throws java.lang.Exception
     */
    public Integer getNextSequence() throws Exception
    {
        HashMap productMap = new HashMap();
        ProductVO productVO = null;

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_NEXT_AVAILBLE_SEQUENCE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        BigDecimal bd =  (BigDecimal)dataAccessUtil.execute(dataRequest);
        

        
        return new Integer(bd.toString());
    }         
      
}
