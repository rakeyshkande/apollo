/*
 * @(#) ManualMercuryFloristDAO.java      1.1.2.3     2004/09/30
 * 
 * 
 */

package com.ftd.applications.lmd.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.vo.ManualMercuryFloristVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/09/09  Initial Release.(RFL)
 * 1.1.2.2  2004/09/21  Updated functionality.(RFL)
 * 1.1.2.3  2004/09/30  Modified functionality to accomdate immediate occurance
 *                      of restart.(RFL)
 *                      
 * 
 */
public class ManualMercuryFloristDAO  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.S";
    private static final int SHUTDOWN_FLAG = 1;
    private static final int SHUTDOWN_BY = 2;
    private static final int SHUTDOWN_DATE = 3;
    private static final int RESTART_DATE = 4;
    private static final int RESTARTED_BY = 5;
    private static final int LAST_UPDATE_DATE = 6;
    
    private static final String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.dao.ManualMercuryFloristDAO";
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Logger logger;

    


    public ManualMercuryFloristDAO() 
    {
        super();
        logger = new Logger(LOGGER_CATEGORY);
    }//end method
    
    
    public ManualMercuryFloristVO getCurrentStatus() 
                                        throws DataAccessException
    {
        NodeList nl = null;
        Document xmlDoc = null;
        ManualMercuryFloristVO returnValue = new ManualMercuryFloristVO();
    
        xmlDoc = (Document)this.executeDataQuery("VIEW_FTDM_SHUTDOWN");
        
        String xpath = "STATUSES/STATUS";
        try
        {
            nl = DOMUtil.selectNodes(xmlDoc, xpath);
        }//end try
        catch (XPathException xsle) 
        {
            logger.error("Exception thrown while selecting nodes", xsle);
            throw new DataAccessException("Unable to access data.");
        }//end catch ()
        
        if(nl != null && nl.getLength() > 0)
        {
            Element node = (Element)nl.item(nl.getLength() - 1);
            
            String field = node.getAttribute("restart_date");
            if (field != null && field.length() > 0) 
            {
                try
                {
                    returnValue.setCurrentRestartDate(
                                            formatStringToUtilDate(field));
                }//end try
                catch (ParseException pe) 
                {
                    logger.error("Exception thrown parsing restart date",
                                 pe);
                    throw new DataAccessException("Unable to parse data.");
                }//end catch (ParseException)
            }//end if ()
            
            returnValue.setCurrentRestartRequestor(
                    node.getAttribute("restarted_by"));
                    
            field = node.getAttribute("shutdown_date");
            if (field != null && field.length() > 0)
            {
                try
                {
                    returnValue.setCurrentShutdownDate(
                        formatStringToUtilDate(field));
                }//end try
                catch (ParseException pe) 
                {
                    logger.error("Exception thrown parsing shutdown date", pe);
                    throw new DataAccessException("Unable to parse data.");
                }//end catch (ParseException)
            }//end if
            
            returnValue.setCurrentShutdownRequestor(
                    node.getAttribute("shutdown_by"));

        }//end if
    
        return returnValue;
        
    }//end method getCurrentStatus()
    
    
    public void restartManualMercuryFlorist(String csrID) throws DataAccessException 
    {
        logger.debug("restartManualMercuryFlorist(String csrID)");
        HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;

        parameters.put("IN_LAST_UPDATED_BY", csrID);
        resultSet = this.executeDataQuery("RESTART_FTDM", parameters);
        
        if (resultSet == null) 
        {
            logger.error("missing output parameters during save FTDM restart.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if ()
        
        outputParameters = (Map)resultSet;
        
        if (outputParameters == null) 
        {
            logger.error("missing output parameters during save FTDM restart.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if 
        else
        {
            String status = (String) outputParameters.get("OUT_STATUS");
            String message = (String) outputParameters.get("OUT_MESSAGE");
            logger.debug("status:: " + status);
            logger.debug("message:: " + message);
            if (status != null) 
            {
                status = status.trim();
                if (status.equalsIgnoreCase("N"))  
                {
                    logger.error("Failure saving FTDM restart - " + message);
                    throw new DataAccessException("Unable to access "
                                                  + "datastore.");
                    
                }//end if
                
            }//end if
            
        }//end else
    }//end method
    
    
    /**
     * 
     * @param restartDate
     * @param csrID
     * @throws DataAccessException
     */
    public void saveRestart(Date restartDate, String csrID, boolean enqueue) 
                                            throws DataAccessException
    {
        logger.debug("saveRestart(Date restartDate, String csrID, boolean enqueue) ");
        HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;
        
        //set Parameters
        parameters.put("RESTART_DATE", this.getTimestamp(restartDate));
        parameters.put("RESTARTED_BY", csrID);
        parameters.put("IN_LAST_UPDATED_BY", csrID);
        if (enqueue){
            parameters.put("IN_ENQUEUE", 'Y');
        }
        resultSet = this.executeDataQuery("UPDATE_FTDM_SHUTDOWN_RESTART", parameters);
        
        if (resultSet == null) 
        {
            logger.error("missing output parameters during save FTDM restart.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if ()
        
        outputParameters = (Map)resultSet;
        
        if (outputParameters == null) 
        {
            logger.error("missing output parameters during save FTDM restart.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if 
        else
        {
            String status = (String) outputParameters.get("OUT_STATUS");
            String message = (String) outputParameters.get("OUT_MESSAGE");
            logger.debug("status:: " + status);
            logger.debug("message:: " + message);
            if (status != null) 
            {
                status = status.trim();
                if (status.equalsIgnoreCase("N"))  
                {
                    logger.error("Failure saving FTDM restart - " + message);
                    throw new DataAccessException("Unable to access "
                                                  + "datastore.");
                    
                }//end if
            }//end if
            
        }//end else
        
    }//end method saveRestart
    
    
    public void saveShutdown(Date restartDate, String csrID) throws DataAccessException
    {
        logger.debug("saveShutdown(Date restartDate, String csrID) ");
        HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;
        
        if (isShutdown()) 
        {
            //Empty!
            
        }//end if
        else
        {
            //set Parameters
            parameters.put("SHUTDOWN_BY", csrID);
            parameters.put("RESTART_DATE", this.getTimestamp(restartDate));
            resultSet = this.executeDataQuery("INITIATE_FTDM_SHUTDOWN", parameters);
            
            if (resultSet == null) 
            {
                logger.error("missing output parameters during save FTDM shutdown");
                throw new DataAccessException("Unable to access datastore.");
                
            }//end if ()
            
            outputParameters = (Map)resultSet;
            
            if (outputParameters == null) 
            {
                logger.error("missing output parameters during save FTDM shutdown");
                throw new DataAccessException("Unable to access datastore.");
                
            }//end if 
            else
            {
                String status = (String) outputParameters.get("OUT_STATUS");
                String message = (String) outputParameters.get("OUT_MESSAGE");
                logger.debug("status:: " + status);
                logger.debug("message:: " + message);
                if (status != null) 
                {
                    status = status.trim();
                    if (status.equalsIgnoreCase("N"))  
                    {
                        logger.error("Failure saving FTDM shutdown - " + message);
                        throw new DataAccessException("Unable to access datastore");
                        
                    }//end if
                    
                }//end if
                
            }//end else
            
        }//end else
    }//end method


    /*
     * 
     */
    private Date formatStringToUtilDate(String string) throws ParseException
    {
        SimpleDateFormat formatter = 
                                    new SimpleDateFormat(this.DATE_TIME_FORMAT);
        
        return formatter.parse(string);
        
    }//end method formatStringToUtilDate(String)
    
    
    /*
     * 
     */
    private Object executeDataQuery(DataRequest dataRequest) 
                                                    throws DataAccessException
    {
        DataAccessUtil dataAccessUtil = null;
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        try 
        {
            dataAccessUtil = DataAccessUtil.getInstance();
            
        }//end
        catch (IOException ioe) 
        {
            logger.error("unable to get instance of DataAccessUtil", ioe);
            throw new DataAccessException("Unable to access datastore");
                                                    
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("unable to get instance of DataAccessUtil", pce);
            throw new DataAccessException("Unable to access datastore");
            
        }
        catch (SAXException saxe) 
        {
            logger.error("unable to get instance of DataAccessUtil", saxe);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch
        
        if (dataAccessUtil == null) 
        {
            logger.error("DataAccessUtil is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        try 
        {
            return dataAccessUtil.execute(dataRequest);
        }
        catch (IOException ioe) 
        {
            logger.error("unable to execute DataRequest", ioe);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("unable to execute DataRequest", pce);
            throw new DataAccessException("Unable to access datastore");
            
        }
        catch (SQLException sqle) 
        {
            logger.error("unable to execute DataRequest", sqle);
            throw new DataAccessException("Unable to access datastore");
            
        }
    
    }//end method executeDataQuery()
    
    
    /*
     * 
     */
    private Object executeDataQuery(String storedProcedureName) 
                                                    throws DataAccessException
    {
        return this.executeDataQuery(storedProcedureName, new HashMap());
        
    }//end method
    
    
    /*
     * 
     */
    private Object executeDataQuery(String storedProcedureName,
                                    HashMap parameters) 
                                                    throws DataAccessException
    {
        DataRequest dataRequest = null;
        DataAccessUtil dataAccessUtil = null;
        
        dataRequest = this.getDataRequest();
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        dataRequest.reset();
        dataRequest.setStatementID(storedProcedureName);
        dataRequest.setInputParams(parameters);
        
        try 
        {
            return this.executeDataQuery(dataRequest);
        }
        catch (Exception e) 
        {
            logger.error("unable to execute DataRequest", e);
            throw new DataAccessException("Unable to access datastore");
            
        }
        finally 
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                try
                {
                    dataRequest.getConnection().close();
                }//end try
                catch (SQLException sqle) 
                {
                    logger.error("Exception thrown while closing DataRequest "
                                 + "connection.", sqle);
                    throw new DataAccessException("Unable to process "
                                                        + "datastore record.");
                    
                }//end catch
                
            }//end if ()     
            
        }//end finally
    
    }//end method executeDataQuery()
    
    
    /*
     * 
     */
    private DataRequest getDataRequest() throws DataAccessException
    {
        try
        {
            return DataRequestHelper.getInstance().getDataRequest();
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to get data request helper.", e);
            throw new DataAccessException("Unable to access datastore");
        }//end catch(Exception)

    }//end method
    
    /*
     * 
     */
    private Date getDate(String value, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date date = null;
        
        if (value != null) 
        {
            date = formatter.parse(value);
            
        }//end method
        
        return date;
    }//end method


    /*
     * 
     */
    private Timestamp getTimestamp(java.util.Date value)
    {
        java.sql.Timestamp sqlDate = null;
        
        if(value != null)
        {
            sqlDate = new java.sql.Timestamp(value.getTime());
        }//end if
        
        return sqlDate;
        
    }//end method

    
    /*
     * 
     */
    private boolean isShutdown() throws DataAccessException 
    {
        Object resultSet = null;
        String shutdownStatus = null;
        
        //set Parameters
        shutdownStatus = (String)this.executeDataQuery("IS_FTDM_SHUTDOWN");
        
        if (shutdownStatus == null) 
        {
            logger.error("missing output parameters during IS FTDM shutdown");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if ()

        return (shutdownStatus.equalsIgnoreCase("Y") ? true : false);
        
    }//end method isShutdown()
    
}//end class
