/**
 * com.ftd.applications.lmd.dao.ZipCodeDAO.java
 */
package com.ftd.applications.lmd.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.vo.ZipCodeVO;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class ZipCodeDAO 
{
    private static final String STORED_PROCEDURE_UPDATE_OSCAR_ZIP_FLAG = "SET_OSCAR_FLAG_BY_ZIP_CODE";
    private static final String IN_ZIP_CODE_LIST = "IN_ZIP_CODE_LIST";
    private static final String IN_OSCAR_ZIP = "IN_OSCAR_FLAG";
    private static final String IN_UPDATED_BY = "IN_UPDATED_BY";

    private static final String STORED_PROCEDURE_UPDATE_OSCAR_CITY_STATE_FLAG = "SET_OSCAR_FLAG_BY_CITY_STATE";
    private static final String IN_CITY_STATE_LIST = "IN_CITY_STATE_LIST";
    private static final String STORED_PROCEDURE_QUERY_ALL_ZIP_CODES = "GET_OSCAR_ZIP_CODE";
    private static final String COLUMN_ZIP_CODE = "zip_code_id";
    private static final String COLUMN_CITY = "city";
    private static final String COLUMN_STATE = "state_id";
    private static final String COLUMN_OSCAR_ZIP_FLAG = "oscar_zip_flag";
    private static final String COLUMN_OSCAR_CITY_STATE_FLAG = "oscar_city_state_flag";
    private static final String UPDATED_BY = "OSCAR_MAINT";

  	
    private Logger logger = new Logger(ZipCodeDAO.class.getName());

    
    
    public ZipCodeDAO()
    {
    	super();
    	logger = new Logger(ZipCodeDAO.class.getName());
    }
    
    public List<ZipCodeVO> queryAll() throws DataAccessException
    {
        Object resultSet = null;
        CachedResultSet rs = null;
        List<ZipCodeVO> list = new ArrayList<ZipCodeVO>();
        ZipCodeVO vo;
        resultSet = this.executeDataQuery(STORED_PROCEDURE_QUERY_ALL_ZIP_CODES);
        
        rs = (CachedResultSet)resultSet;
        
        while (rs != null && rs.next()) 
        {
            vo = new ZipCodeVO();
            vo.setZipCode((String)rs.getString(COLUMN_ZIP_CODE));
    	    vo.setCity((String)rs.getString(COLUMN_CITY));
    	    vo.setStateId((String)rs.getString(COLUMN_STATE));
    	    vo.setOscarZipFlag((String)rs.getString(COLUMN_OSCAR_ZIP_FLAG));
    	    vo.setOscarCityStateFlag((String)rs.getString(COLUMN_OSCAR_CITY_STATE_FLAG));
        
    	    list.add(vo);
    	    
        }//end if
        
        return list;
        
    }
    
    /**
     * Updates the OSCAR_CITY_STATE_FLAG, on the ZIP_CODE table, to the provided value(oscarFlag) for all the supplied City/States(list).
     * 
     * 
     * @param list  list of City/States to be updated
     * @param oscarFlag  String parameter, Y or N
     * @throws DataAccessException
     */
    public void updateOscarCityStateFlag(List<ZipCodeVO> list, String oscarFlag) throws DataAccessException, Exception
    {
    	int numberOfIterations;
    	String stringOfCityStates;

    	HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;

        int CITY_STATE_GOVERNOR = Integer.parseInt(CacheUtil.getInstance().getGlobalParm("OSCAR", "CITY_STATE_GOVERNOR"));
    	logger.info("CITY_STATE_GOVERNOR: " + CITY_STATE_GOVERNOR);

       	//Don't enter if list is empty
       	if (null != list && list.size() > 0)
       	{
       		//Determine the number of times to call the stored procedure
           	numberOfIterations = list.size() / CITY_STATE_GOVERNOR;
           	if (list.size()%CITY_STATE_GOVERNOR != 0)
           	{
           		numberOfIterations++;
           	}
            	
           	for (int i = 0; i < numberOfIterations; i++)
           	{
           		int fromIndex = i * CITY_STATE_GOVERNOR;
           		int toIndex = (i+1)*CITY_STATE_GOVERNOR > list.size()? list.size():(i+1)*CITY_STATE_GOVERNOR;
           		stringOfCityStates = this.buildCityStateList(list.subList(fromIndex, toIndex));            		

            	if (StringUtils.isNotBlank(stringOfCityStates)){
            		logger.debug("CityState List: " + stringOfCityStates);
            	    //set Parameters
				    parameters.put(IN_CITY_STATE_LIST, stringOfCityStates);
				    parameters.put(IN_OSCAR_ZIP, oscarFlag);
				    parameters.put(IN_UPDATED_BY, UPDATED_BY);
				    resultSet = this.executeDataQuery(STORED_PROCEDURE_UPDATE_OSCAR_CITY_STATE_FLAG, parameters);
			        
			        if (resultSet == null) 
			        {
			            logger.error("missing output parameters during update Oscar city state flag.");
			            throw new DataAccessException("Unable to access datastore.");
				            
			        }//end if ()
				        
			        outputParameters = (Map)resultSet;
				        
			        if (outputParameters == null) 
			        {
			            logger.error("missing output parameters during update Oscar city state flag.");
			            throw new DataAccessException("Unable to access datastore.");
			            
			        }//end if 
			        else
			        {
			            String status = (String) outputParameters.get("OUT_STATUS");
			            String message = (String) outputParameters.get("OUT_MESSAGE");
			            logger.debug("status:: " + status);
			            logger.debug("message:: " + message);
			            if (status != null) 
			            {
			                status = status.trim();
			                if (status.equalsIgnoreCase("N"))  
			                {
			                    logger.error("Failure during update Oscar city state flag - " + message);
			                    throw new DataAccessException("Unable to access datastore.");
			                    
			                }//end if
			                
			            }//end if
				            
			        }//end else
				        
           		}
           	}
       		
       	}
    		
   	}
    	
    
    /**
     * Updates the OSCAR_ZIP_FLAG, on the ZIP_CODE table, to the provided value(oscarFlag) for all the supplied zip codes(list).
     * 
     * 
     * @param list  list of zip codes to be updated
     * @param oscarFlag  String parameter, Y or N
     * @throws DataAccessException
     */
    public void updateOscarZipFlag(List<ZipCodeVO> list, String zipFlag) throws DataAccessException, Exception
    {
    	int numberOfIterations;
    	String stringOfZips;

    	HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;
        
        int ZIP_CODE_GOVERNOR = Integer.parseInt(CacheUtil.getInstance().getGlobalParm("OSCAR", "ZIP_CODE_GOVERNOR"));
    	logger.info("ZIP_CODE_GOVERNOR: " + ZIP_CODE_GOVERNOR);
        	
      	//Don't enter if list is empty
       	if (null != list && list.size() > 0)
       	{
           	numberOfIterations = list.size() / ZIP_CODE_GOVERNOR;
           	if (list.size()%ZIP_CODE_GOVERNOR != 0)
           	{
           		numberOfIterations++;
           	}
          	
           	for (int i = 0; i < numberOfIterations; i++)
           	{
           		int fromIndex = i * ZIP_CODE_GOVERNOR;
           		int toIndex = (i+1)*ZIP_CODE_GOVERNOR > list.size()? list.size():(i+1)*ZIP_CODE_GOVERNOR;
           		stringOfZips = this.buildZipCodeList(list.subList(fromIndex, toIndex));
           		
           		if (StringUtils.isNotBlank(stringOfZips)){
           			//set Parameters
			        parameters.put(IN_ZIP_CODE_LIST, stringOfZips);
			        parameters.put(IN_OSCAR_ZIP, zipFlag);
			        parameters.put(IN_UPDATED_BY, UPDATED_BY);
			        resultSet = this.executeDataQuery(STORED_PROCEDURE_UPDATE_OSCAR_ZIP_FLAG, parameters);
			        
			        if (resultSet == null) 
			        {
			            logger.error("missing output parameters during update Oscar zip flag.");
			            throw new DataAccessException("Unable to access datastore.");
			            
			        }//end if ()
			        
			        outputParameters = (Map)resultSet;
			        
			        if (outputParameters == null) 
			        {
			            logger.error("missing output parameters during update Oscar zip flag.");
			            throw new DataAccessException("Unable to access datastore.");
			            
			        }//end if 
			        else
			        {
			            String status = (String) outputParameters.get("OUT_STATUS");
			            String message = (String) outputParameters.get("OUT_MESSAGE");
			            logger.debug("status:: " + status);
			            logger.debug("message:: " + message);
			            if (status != null) 
			            {
			                status = status.trim();
			                if (status.equalsIgnoreCase("N"))  
			                {
			                    logger.error("Failure during update Oscar zip flag - " + message);
			                    throw new DataAccessException("Unable to access datastore.");
			                    
			                }//end if
			                
			            }//end if
			            
			        }//end else
				}
           	}
        		
       	}
    		
   	}    	

    /*
     ***************************************************************************************************************************************
     *		Private Methods
     ***************************************************************************************************************************************
     */
    
    private String buildCityStateList(List<ZipCodeVO> list)
    {
    	List<String> cityStateList;
    	Set<String> cityStateSet = new HashSet<String>();  //Set Interface removes duplicates
    	StringBuffer key = new StringBuffer();
    	StringBuffer sb = new StringBuffer();
    	
    	for (ZipCodeVO vo:list)
    	{
    		key.append(vo.getCity().toUpperCase()).append(",").append(vo.getStateId().toUpperCase());
    		if (cityStateSet.add(key.toString()))
    		{
    			sb.append(key.toString()).append(";");
    		}
    		key.setLength(0);
    	}
    	//trim extraneous semicolon from string
    	if (sb.length() > 0) {sb.setLength(sb.length() - 1);}
    	
    	return sb.toString();
    	
    }
    
    private String buildZipCodeList(List<ZipCodeVO> list)
    {
    	List<String> zipCodeList;
    	Set<String> zipCodeSet = new HashSet<String>();  //Set Interface removes duplicates
    	StringBuffer sb = new StringBuffer();
    	
    	for (ZipCodeVO vo:list)
    	{
    		if (zipCodeSet.add(vo.getZipCode().toUpperCase()))
    		{
    			sb.append(vo.getZipCode().toUpperCase()).append(",");
    		}
    	}
    	//trim extraneous comma from string
    	if (sb.length() > 0) {sb.setLength(sb.length() - 1);}
    	
    	return sb.toString();
    	
    }
    
    /*
     * 
     */
    private Object executeDataQuery(DataRequest dataRequest) throws DataAccessException
    {
        DataAccessUtil dataAccessUtil = null;
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        try 
        {
            dataAccessUtil = DataAccessUtil.getInstance();
            
        }//end
        catch (IOException ioe) 
        {
            logger.error("unable to get instance of DataAccessUtil", ioe);
            throw new DataAccessException("Unable to access datastore");
                                                    
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("unable to get instance of DataAccessUtil", pce);
            throw new DataAccessException("Unable to access datastore");
            
        }
        catch (SAXException saxe) 
        {
            logger.error("unable to get instance of DataAccessUtil", saxe);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch
        
        if (dataAccessUtil == null) 
        {
            logger.error("DataAccessUtil is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        try 
        {
            return dataAccessUtil.execute(dataRequest);
        }
        catch (IOException ioe) 
        {
            logger.error("unable to execute DataRequest", ioe);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("unable to execute DataRequest", pce);
            throw new DataAccessException("Unable to access datastore");
            
        }
        catch (SQLException sqle) 
        {
            logger.error("unable to execute DataRequest", sqle);
            throw new DataAccessException("Unable to access datastore");
            
        }
    
    }//end method executeDataQuery()
    
    
    /*
     * 
     */
    private Object executeDataQuery(String storedProcedureName) throws DataAccessException
    {
        DataRequest dataRequest = null;
        DataAccessUtil dataAccessUtil = null;
        
        dataRequest = this.getDataRequest();
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        dataRequest.reset();
        dataRequest.setStatementID(storedProcedureName);
        
        try 
        {
            return this.executeDataQuery(dataRequest);
        }
        catch (Exception e) 
        {
            logger.error("unable to execute DataRequest", e);
            throw new DataAccessException("Unable to access datastore");
            
        }
        finally 
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                try
                {
                    dataRequest.getConnection().close();
                }//end try
                catch (SQLException sqle) 
                {
                    logger.error("Exception thrown while closing DataRequest "
                                 + "connection.", sqle);
                    throw new DataAccessException("Unable to process "
                                                        + "datastore record.");
                    
                }//end catch
                
            }//end if ()     
            
        }//end finally
    
    }//end method executeDataQuery()
    
    /*
     * 
     */
    private Object executeDataQuery(String storedProcedureName, HashMap parameters) throws DataAccessException
    {
        DataRequest dataRequest = null;
        DataAccessUtil dataAccessUtil = null;
        
        dataRequest = this.getDataRequest();
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        dataRequest.reset();
        dataRequest.setStatementID(storedProcedureName);
        dataRequest.setInputParams(parameters);
        
        try 
        {
            return this.executeDataQuery(dataRequest);
        }
        catch (Exception e) 
        {
            logger.error("unable to execute DataRequest", e);
            throw new DataAccessException("Unable to access datastore");
            
        }
        finally 
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                try
                {
                    dataRequest.getConnection().close();
                }//end try
                catch (SQLException sqle) 
                {
                    logger.error("Exception thrown while closing DataRequest "
                                 + "connection.", sqle);
                    throw new DataAccessException("Unable to process "
                                                        + "datastore record.");
                    
                }//end catch
                
            }//end if ()     
            
        }//end finally
    
    }//end method executeDataQuery()
    
    /*
     * 
     */
    private DataRequest getDataRequest() throws DataAccessException
    {
        try
        {
            return DataRequestHelper.getInstance().getDataRequest();
            
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to get data request helper.", e);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch(Exception)

    }//end method
    
    
}

