package com.ftd.applications.lmd.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.mail.Session;
import javax.naming.InitialContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.memberObjects.FTD.Member;
import com.ftd.applications.lmd.memberObjects.FTD.Member.BranchShopAsStandalone;
import com.ftd.applications.lmd.memberObjects.FTD.Member.CityListings;
import com.ftd.applications.lmd.memberObjects.FTD.Member.CityListings.Listing;
import com.ftd.applications.lmd.memberObjects.FTD.Member.CityListings.Listing.SpecialListings;
import com.ftd.applications.lmd.memberObjects.FTD.Member.Codifications;
import com.ftd.applications.lmd.memberObjects.FTD.Member.Codifications.CodifiedProduct;
import com.ftd.applications.lmd.memberObjects.FTD.Member.Codifications.CodifiedProduct.MinimumOrderAmount;
import com.ftd.applications.lmd.memberObjects.FTD.Member.HoursOfOperation;
import com.ftd.applications.lmd.memberObjects.FTD.Member.PhoneNumbers;
import com.ftd.applications.lmd.memberObjects.FTD.Member.PhoneNumbers.Phone;
import com.ftd.applications.lmd.memberObjects.FTD.Member.ShopType;
import com.ftd.applications.lmd.productObjects.FTD.Products.Product;
import com.ftd.applications.lmd.productObjects.FTD.Products.Product.ActiveFor.System;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.applications.lmd.vo.FloristBlockVO;
import com.ftd.applications.lmd.vo.FloristCodifiedProductVO;
import com.ftd.applications.lmd.vo.FloristSuspendResumeTimeVO;
import com.ftd.applications.lmd.vo.FloristWeightCalcVO;
import com.ftd.applications.lmd.vo.LSTReciprocityVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * DAO
 *
 * @author Anshu Gaind
 */

public class FloristDAO 
{
  private static Logger logger  = new Logger("com.ftd.applications.lmd.dao.FloristDAO");
  private static final String CODIFICATION_ID_LOW = "LOW";

  public FloristDAO()
  {
  }
  
  /**
   * Saves data into the following tables:
   *   1. FLORIST_MASTER_STAGE
   *   2. FLORIST_CODIFICATIONS_STAGE
   *   3. EFOS_CITY_STATE_STAGE
   *   4. CODIFICATION_MASTER_STAGE
   *   
   * @param floristsXMLList
   * @param citiesMap
   * @param productsXMLList
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void saveFloristData(List<Member> floristsXMLList, Map citiesMap, List<Product> productsXMLList)
  throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
{
  Connection con = null;

  try
  {

    String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
    con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");            

    // remove old staging data, independent of the following transaction
    this.resetFloristDataStagingTables(con);      
    this.saveFloristMasterXMLStagingData(floristsXMLList, con);      
    this.saveEFOSCityStateStagingData(citiesMap, con);
    this.saveProductXMLStagingData(productsXMLList, con);
    
  } finally 
  {
    if (con != null && (! con.isClosed() )) 
    {
        con.close();
    }
  }
}

  /**
   * Resets the florist data staging tables
   * 
   * @param con
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private void resetFloristDataStagingTables(Connection con) 
    throws SQLException, IOException, ParserConfigurationException, SAXException, Exception
  {
    logger.debug("Begin resetting florist data staging data");
    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();      
    dataRequest.setConnection(con); 
    logger.debug(con.getMetaData().getURL());
    dataRequest.setStatementID("RESET_FLORIST_DATA_STAGE");
      
    // no input parameters
      
    Map result = (Map)dau.execute(dataRequest);
    String status = (String) result.get("OUT_STATUS");
    String message = (String)  result.get("OUT_MESSAGE");
    
    if (status.equalsIgnoreCase("N")) 
    {
      logger.error(message);
      throw new Exception(message);
    }
    logger.debug("End resetting florist data staging data");
                
  }
  
  /**
   * Saves data into the following tables:
   *   1. FLORIST_MASTER_STAGE
   *   2. FLORIST_CODIFICATIONS_STAGE
   * 
   * @param floristsXMLList
   * @param con
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  private void saveFloristMasterXMLStagingData(List<Member> floristsXMLList, Connection con) throws Exception {    
    logger.debug("Begin saving florist master staging data");
    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();      
    logger.debug(con.getMetaData().getURL());

    ConfigurationUtil cu = ConfigurationUtil.getInstance();
    String tempMinOrderAmt = cu.getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, "LOW_CODIFICATION_MIN_ORDER_AMOUNT");
    int lowMinOrderAmt = 30;
    try {
    	logger.debug("lowMinOrderAmt: " + tempMinOrderAmt);
    	lowMinOrderAmt = Integer.parseInt(tempMinOrderAmt);
    } catch (Exception e) {
    	logger.error("Could not convert " + tempMinOrderAmt + " to an integer, using default of 30");
    }

    int ctr = 0;

    for (int a=0; a<floristsXMLList.size(); a++) {
        Member member = floristsXMLList.get(a);
        if (member.getSendOnly() == null || !member.getSendOnly().toString().equalsIgnoreCase("true")) {
		    ShopType shop = member.getShopType();
		    String phoneNumber = null;
		    String faxNumber = null;
		    PhoneNumbers phoneNumbers = member.getPhoneNumbers();
		    if (phoneNumbers != null) {
		        List<Phone> phoneList = phoneNumbers.getPhone();
		        for (int i=0; i<phoneList.size(); i++) {
		    	    Phone phone = phoneList.get(i);
		    	    com.ftd.applications.lmd.memberObjects.PhoneNumbers phoneType = phone.getType();
		    	    if (phoneType != null) {
		    		    if (phoneType.equals(com.ftd.applications.lmd.memberObjects.PhoneNumbers.SHOP)) {
		    			    phoneNumber = phone.getValue();
		    			    phoneNumber = phoneNumber.replace("-", "");
		    		    } else if (phoneType.equals(com.ftd.applications.lmd.memberObjects.PhoneNumbers.FAX)) {
		    			    faxNumber = phone.getValue();
		    		    }
		    	    }
	    	    }
		    }
	    	HoursOfOperation hoo = member.getHoursOfOperation();

        	List floristCodifiedProductVOList = new ArrayList();
    	    Codifications codify = member.getCodifications();
    	    if (codify != null) {
	    	    List<CodifiedProduct> codifiedProducts = codify.getCodifiedProduct();
    	        for (int b=0; b<codifiedProducts.size(); b++) {
	        	    CodifiedProduct cp = codifiedProducts.get(b);
	        	    FloristCodifiedProductVO fcpVO = new FloristCodifiedProductVO();

	                String codificationId = cp.getCode();
	        	    fcpVO.setCodifiedID(codificationId);
	        	
	        	    MinimumOrderAmount minOrderAmt = cp.getMinimumOrderAmount();
	        	    if (minOrderAmt.getHolidayException() != null && minOrderAmt.getHolidayException().toString().equalsIgnoreCase("true")) {
	        		    fcpVO.setHolidayPriceFlag("H");
	        	    } else {
	        		    fcpVO.setHolidayPriceFlag(null);
	        	    }
	        	    fcpVO.setMinPrice(minOrderAmt.getValue());
	        	    fcpVO.setHasMinimum(minOrderAmt.getHasMinimum().toString());
	        	    floristCodifiedProductVOList.add(fcpVO);
    	        }
	        }

    	    com.ftd.applications.lmd.memberObjects.FTD.Member.MinimumOrderAmount minOrderAmt = member.getMinimumOrderAmount();
            int minimumOrderAmt = 0;
            if (minOrderAmt != null) {
            	BigDecimal tempMin = minOrderAmt.getValue();
            	minimumOrderAmt = tempMin.intValue();
            }
            if (minimumOrderAmt <= lowMinOrderAmt) {
            	FloristCodifiedProductVO fcpVO = new FloristCodifiedProductVO();
            	fcpVO.setCodifiedID(CODIFICATION_ID_LOW);
            	fcpVO.setMinPrice(Integer.toString(minimumOrderAmt));
            	fcpVO.setHasMinimum("true");
            	floristCodifiedProductVOList.add(fcpVO);
            }

    	    CityListings list = member.getCityListings();
    	    if (list != null) {
                List<Listing> cityList = list.getListing();
	    	    for (int i=0; i<cityList.size(); i++) {
		    	    Listing city = cityList.get(i);

	                dataRequest.reset();
	                dataRequest.setConnection(con);
	                dataRequest.setStatementID("INSERT_FLORIST_MASTER_STG");
	        
	                HashMap inputParameters = new HashMap();
	                inputParameters.put("IN_FLORIST_ID", city.getCode());
	                inputParameters.put("IN_FLORIST_NAME", member.getBusinessName());
	                inputParameters.put("IN_ADDRESS", member.getStreetAddress());
	                inputParameters.put("IN_CITY", city.getCityName());
	                inputParameters.put("IN_STATE", city.getState().getValue());
	                inputParameters.put("IN_PHONE_NUMBER", phoneNumber);
	                inputParameters.put("IN_ZIP_CODE", member.getZip());
	                String cityCode = city.getCityCode();
	                cityCode = "000000" + cityCode;
	                cityCode = cityCode.substring(cityCode.length() - 6);
	                inputParameters.put("IN_CITY_STATE_NUMBER", cityCode);
	                inputParameters.put("IN_RECORD_TYPE", city.getType());
	                String mercuryFlag = null;
	                if (member.getTechnologyTerminal() != null && member.getTechnologyTerminal().toString().equalsIgnoreCase("true")) {
	        	        mercuryFlag = "M";
	                }
	                inputParameters.put("IN_MERCURY_FLAG", mercuryFlag);
	                inputParameters.put("IN_OWNERS_NAME", member.getOwnerName());

	                inputParameters.put("IN_MINIMUM_ORDER_AMOUNT", minimumOrderAmt);
	                inputParameters.put("IN_FLORIST_WEIGHT", 0);
	                inputParameters.put("IN_INITIAL_WEIGHT", 0);

                    // If this flag is true, then member should be treated as standalone even if it is a branch.
	                // It it typically set for grocery florists.
	                BranchShopAsStandalone bsas = member.getBranchShopAsStandalone();
	                boolean treatAsStandalone = false;
	                if (bsas != null && (bsas.getValue()).equals(com.ftd.applications.lmd.memberObjects.Boolean.TRUE)) {
	                    treatAsStandalone = true;
	                }

                    String memberCode = member.getCode();
	                String shopType = shop.getValue();
	                if (shopType != null && shopType.equalsIgnoreCase("branch") && !treatAsStandalone) {
		                inputParameters.put("IN_TOP_LEVEL_FLORIST_ID", shop.getMainMemberCode());
	                } else {
	        	        inputParameters.put("IN_TOP_LEVEL_FLORIST_ID", memberCode);
	                }
	                if (memberCode != null && !memberCode.equals(city.getCode())) {
	                    inputParameters.put("IN_PARENT_FLORIST_ID", memberCode);
	                } else if (shopType != null && shopType.equalsIgnoreCase("branch") && !treatAsStandalone) {
	                    inputParameters.put("IN_PARENT_FLORIST_ID", shop.getMainMemberCode());
	                }
	        
	                SpecialListings special = city.getSpecialListings();
	                String specialFax = null;
	                if (special != null) {
	            	    if (special.getFax() != null) {
	            		    specialFax = special.getFax();
	            	    }
	                    inputParameters.put("IN_ALT_PHONE_NUMBER", special.getTollFree());
	                }
	                if (specialFax != null) {
	                    inputParameters.put("IN_FAX_NUMBER", special.getFax());
	                } else if (faxNumber != null) {
	                    inputParameters.put("IN_FAX_NUMBER", faxNumber);
	                }
	                
	                String internalLink = Long.toString(member.getInternalLink());
	                if (internalLink != null && internalLink.equals("0")) {
	                	internalLink = null;
	                }
		            if (StringUtils.isNotBlank(internalLink) && internalLink.length() < 8) {
		            	internalLink = StringUtils.leftPad(internalLink, 8, '0');
		            }
	                inputParameters.put("IN_INTERNAL_LINK_NUMBER", internalLink);
	                
	                inputParameters.put("IN_TERRITORY", member.getTerritory());
	                dataRequest.setInputParams(inputParameters);
	        
	                Map result = (Map)dau.execute(dataRequest);
	                String status = (String) result.get("OUT_STATUS");
	                String message = (String)  result.get("OUT_MESSAGE");
	        
	                if (status.equalsIgnoreCase("N")) {
	                    logger.error(dataRequest.getStatementID() + "::" + city.getCode() + " failed");
	                    throw new Exception(message);
	                }
	        
	                FloristCodifiedProductVO fcpVO = null;
	                for (Iterator floristCodifiedProductVOListIter = floristCodifiedProductVOList.iterator(); floristCodifiedProductVOListIter.hasNext();) {        
	                    fcpVO = (FloristCodifiedProductVO) floristCodifiedProductVOListIter.next();
	          
	                    inputParameters = new HashMap();
	                    inputParameters.put("IN_FLORIST_ID", city.getCode());
	                    inputParameters.put("IN_CODIFIED_ID", fcpVO.getCodifiedID());
	                    inputParameters.put("IN_HOLIDAY_PRICE_FLAG", fcpVO.getHolidayPriceFlag());
	                
	                    if (fcpVO.getHasMinimum() != null && fcpVO.getHasMinimum().equalsIgnoreCase("true")) {
		                    int codifyMinOrderAmt = 0;
		                    if (fcpVO.getMinPrice() != null && !fcpVO.getMinPrice().equals("")) {
		        	            String tempMin = fcpVO.getMinPrice();
		                        double tempDouble = Double.parseDouble(tempMin);
		                        codifyMinOrderAmt = (int) tempDouble;
		                    }
	                        inputParameters.put("IN_MIN_PRICE", codifyMinOrderAmt);
	                    }
	          
	                    dataRequest.setInputParams(inputParameters);
	                    dataRequest.setStatementID("INSERT_FLORIST_CODIFIED_STG");
	          	    
	                    result = (Map)dau.execute(dataRequest);
	                    status = (String) result.get("OUT_STATUS");
	                    message = (String)  result.get("OUT_MESSAGE");
	          
	                    if (status.equalsIgnoreCase("N")) {
	                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::" + fcpVO.getCodifiedID() + " failed");
	                        throw new Exception(message);
	                    }
	                }
	                
	    	        String openCloseCode = null;
	    	        String openTime = null;
	    	        String closeTime = null;
	    	    
                    openCloseCode = hoo.getMonday().getCode().toString();
                    openCloseCode = openCloseCode.replace("1", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "MONDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getMonday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getMonday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::MONDAY failed");
                        throw new Exception(message);
                    }
                
                    openCloseCode = hoo.getTuesday().getCode().toString();
                    openCloseCode = openCloseCode.replace("2", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "TUESDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getTuesday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getTuesday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::TUESDAY failed");
                        throw new Exception(message);
                    }

                    openCloseCode = hoo.getWednesday().getCode().toString();
                    openCloseCode = openCloseCode.replace("3", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "WEDNESDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getWednesday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getWednesday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::WEDNESDAY failed");
                        throw new Exception(message);
                    }

                    openCloseCode = hoo.getThursday().getCode().toString();
                    openCloseCode = openCloseCode.replace("4", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "THURSDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getThursday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getThursday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::THURSDAY failed");
                        throw new Exception(message);
                    }

                    openCloseCode = hoo.getFriday().getCode().toString();
                    openCloseCode = openCloseCode.replace("5", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "FRIDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getFriday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getFriday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::FRIDAY failed");
                        throw new Exception(message);
                    }

                    openCloseCode = hoo.getSaturday().getCode().toString();
                    openCloseCode = openCloseCode.replace("6", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "SATURDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getSaturday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getSaturday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::SATURDAY failed");
                        throw new Exception(message);
                    }

                    openCloseCode = hoo.getSunday().getCode().toString();
                    openCloseCode = openCloseCode.replace("7", "");
                    inputParameters = new HashMap();
                    inputParameters.put("IN_FLORIST_ID", city.getCode());
                    inputParameters.put("IN_DAY_OF_WEEK", "SUNDAY");
                    inputParameters.put("IN_OPEN_CLOSE_CODE", openCloseCode);
                    openTime = hoo.getSunday().getOpenTime().toString();
                    inputParameters.put("IN_OPEN_TIME", openTime);
                    closeTime = hoo.getSunday().getCloseTime().toString();
                    inputParameters.put("IN_CLOSE_TIME", closeTime);
                    dataRequest.reset();
                    dataRequest.setConnection(con);
                    dataRequest.setInputParams(inputParameters);
                    dataRequest.setStatementID("INSERT_FLORIST_HOURS_STG");
                    result = (Map)dau.execute(dataRequest);
                    status = (String) result.get("OUT_STATUS");
                    message = (String)  result.get("OUT_MESSAGE");
                    if (status.equalsIgnoreCase("N")) {
                        logger.error(dataRequest.getStatementID() + "::" + city.getCode() +  "::SUNDAY failed");
                        throw new Exception(message);
                    }

                    if (++ctr%threshold == 0) logger.debug(ctr + " records processed");

    	        }
    	    
	    	}
	    	
    	}
	}

	logger.debug(ctr + " records processed");  
	logger.debug("End saving florist master staging data");
  }
  
  private void saveProductXMLStagingData(List<Product> productsXMLList, Connection con) throws Exception {
	  logger.debug("Begin saving product staging data");
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      
      logger.debug(con.getMetaData().getURL());

      int ctr = 0;

      for (int a=0; a<productsXMLList.size(); a++) {
    	  Product product = productsXMLList.get(a);

          dataRequest.reset();
          dataRequest.setConnection(con);
          dataRequest.setStatementID("INSERT_CODIFICATION_MASTER_STG");
      
          HashMap inputParameters = new HashMap();
          String codificationId = product.getCode();
          inputParameters.put("IN_CODIFICATION_ID", codificationId);
          inputParameters.put("IN_CATEGORY_ID", product.getCategory().getValue().toString());
          inputParameters.put("IN_DESCRIPTION", product.getDescription());
          inputParameters.put("IN_CATEGORY_TYPE", product.getCategory().getType().toString());
          String activeForDotCom = "false";
		  List<System> activeList = product.getActiveFor().getSystem();
		  for (int j=0; j<activeList.size(); j++) {
			  System system = (System) activeList.get(j);
        	  if (system.getValue() != null && system.getValue().toString().equalsIgnoreCase("DOT_COM")) {
        		  activeForDotCom = "true";
        		  break;
        	  }
          }
          inputParameters.put("IN_ACTIVE_FOR_DOTCOM", activeForDotCom);
          dataRequest.setInputParams(inputParameters);
	        
          Map result = (Map)dau.execute(dataRequest);
          String status = (String) result.get("OUT_STATUS");
          String message = (String)  result.get("OUT_MESSAGE");
      
          if (status.equalsIgnoreCase("N")) {
              logger.error(dataRequest.getStatementID() + "::" + product.getCode2() + " failed");
              throw new Exception(message);
          }
      }
      
      ConfigurationUtil cu = ConfigurationUtil.getInstance();
      String lowDescription = cu.getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, "LOW_CODIFICATION_DESCRIPTION");
      String lowCategory = cu.getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, "LOW_CODIFICATION_CATEGORY");

      dataRequest.reset();
      dataRequest.setConnection(con);
      dataRequest.setStatementID("INSERT_CODIFICATION_MASTER_STG");
  
      HashMap inputParameters = new HashMap();
      inputParameters.put("IN_CODIFICATION_ID", CODIFICATION_ID_LOW);
      inputParameters.put("IN_CATEGORY_ID", lowCategory);
      inputParameters.put("IN_DESCRIPTION", lowDescription);
      inputParameters.put("IN_CATEGORY_TYPE", "FTD");
      inputParameters.put("IN_ACTIVE_FOR_DOTCOM", "true");
      dataRequest.setInputParams(inputParameters);
        
      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
  
      if (status.equalsIgnoreCase("N")) {
          logger.error(dataRequest.getStatementID() + "::" + CODIFICATION_ID_LOW + " failed");
          throw new Exception(message);
      }

  }

  
  /**
   * Saves data into the following table:
   *   3. EFOS_CITY_STATE_STAGE
   * 
   * @param citiesMap
   * @param con
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  private void saveEFOSCityStateStagingData(Map citiesMap, Connection con)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {    
    logger.debug("Begin saving EFOS city state staging data");
    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();      
    logger.debug(con.getMetaData().getURL());
    
    String cityStateNumber, cityName;

    int ctr = 0;
    
    for (Iterator iter = citiesMap.keySet().iterator(); iter.hasNext() ; ) 
    {
      cityStateNumber = (String) iter.next();
      cityName = (String) citiesMap.get(cityStateNumber);
      dataRequest.reset();
      
      dataRequest.setConnection(con);
      
      dataRequest.setStatementID("INSERT_EFOS_CITY_STATE_STG");
      
      HashMap inputParameters = new HashMap();
      inputParameters.put("IN_CITY_STATE_NUMBER", cityStateNumber);
      inputParameters.put("IN_CITY_NAME", cityName);
      
      dataRequest.setInputParams(inputParameters);
      
      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(dataRequest.getStatementID() + "::" + cityStateNumber + "::" + cityName + " failed");
        throw new Exception(message);
      }

      if (++ctr%threshold == 0) logger.debug(ctr + " records processed");

    } // end for loop
    
    logger.debug(ctr + " records processed");  
    logger.debug("End saving EFOS city state staging data");
      
  }
  
  
  /**
   * Indicates whether or not an audit approval of florist staging data was
   * performed
   * 
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public boolean isAuditApproved()
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
  
    Connection con = null;
    String isAuditApproved;
    try 
    {

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("IS_AUDIT_APPROVED");

      
      isAuditApproved = (String)dau.execute(dataRequest);
      
    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
    return (isAuditApproved != null && isAuditApproved.equals("Y"))?true:false;
  }

  /**
   * Returns an audit report document
   * 
   * @param reportID
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Document getFloristAuditReport(String reportID)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    return this.getAuditReport(reportID);   
  }

  /**
   * Returns a document containing florist audit reports
   * 
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Document getFloristAuditReportNames()
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    Document reportNamesDoc = null;
    
    try 
    {
      logger.debug("Begin getting florist audit report names");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("VIEW_FLORIST_AUDIT_RPT_NAMES");
      
      reportNamesDoc = (Document)dau.execute(dataRequest);    
      
      logger.debug("End getting florist audit report names");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
    return reportNamesDoc;
    
  }
    
  
  /**
   * Updates the status of the florist audit report
   * 
   * @param reportID
   * @param oldStatus
   * @param newStatus
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristAuditReport(String reportID, String oldStatus, String newStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    this.updateAuditReport(reportID, oldStatus, newStatus, updatedBy);
  }

  /**
   * Updates the florist audit report comments
   * 
   * @param reportID
   * @param reportComments
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristAuditReportComments(String reportID, String reportComments, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    this.updateAuditReportComments(reportID, reportComments, updatedBy);
  }
  
  
  /**
   * Builds the florist audit report
   * 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void buildFloristAuditReport()
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    
    try 
    {
      logger.debug("Begin building florist audit report");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("BUILD_FLORIST_AUDIT");

      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(message);
        throw new Exception(message);
      }
     
      logger.debug("End building florist audit report");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
  }


  /**
   * Builds the florist weight calculation audit report
   * 
   * @param processingDate
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void buildFloristWeightCalcAuditReport(GregorianCalendar processingDate)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    
    try 
    {
      logger.debug("Begin building florist weight calculation audit report");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("BUILD_FLORIST_WEIGHT_AUDIT");

      HashMap inputParameters = new HashMap();
      
      inputParameters.put("IN_CALC_DATE", new Date(processingDate.getTimeInMillis()));
      dataRequest.setInputParams(inputParameters);

      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(message);
        throw new Exception(message);
      }
     
      logger.debug("End building florist weight calculation audit report");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
  }
  
  /**
   * Sends notifications to all users in the role provided
   * 
   * @param role
   * @param context
   * @param fromAddress
   * @param subject
   * @param notificationContent
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void sendNotification(String recipConfigName, String fromAddress, String subject, String notificationContent)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    InitialContext initContext = null;
    Session mailSession = null;
    
    try 
    {
      
      logger.debug("Begin sending notifications");
      
      if(recipConfigName == null || recipConfigName.length() == 0) {
            logger.warn("Recipient to send notification not specified.");
            return;
      }
    
      NotificationVO notificationVO = null;
      NotificationUtil notificationUtil = NotificationUtil.getInstance();
      initContext = new InitialContext();
      mailSession = (Session)initContext.lookup("mail/load_florist_data_notification_session");
            
      ConfigurationUtil cu = ConfigurationUtil.getInstance();
      String recipList = cu.getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT,recipConfigName);
       
      StringTokenizer st = new StringTokenizer(recipList, ",");
      String toAddress = null;
      
      while (st.hasMoreTokens()) 
      {
        toAddress = st.nextToken();
        notificationVO = new NotificationVO();
        
        notificationVO.setMessageTOAddress(toAddress);
        notificationVO.setMessageFromAddress(fromAddress);
        notificationVO.setMessageSubject(subject);
        
        notificationVO.setMessageContent(notificationContent);
        try 
        {
          notificationUtil.notify(mailSession, notificationVO);
        } catch (Exception ex) 
        {
          // do not throw the exception back to the calling block
          logger.error("Failed to send notification to user " + toAddress);
          logger.error(ex);
        } 
        logger.debug("Notification(s) sent:" + toAddress);
      }
      
      
      logger.debug("End  sending notifications");

    } finally 
    {
      if (initContext != null ) 
      {
          initContext.close();
      }

    }
    
  }

  /**
   * Loads florist data to production
   * 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void loadFloristDataToProduction()
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    
    try 
    {
      logger.debug("Begin loading florist data to production");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("LOAD_FLORIST_DATA");

      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(message);
        throw new Exception(message);
      }
     
      logger.debug("End loading florist data to production");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
  }


  /**
   * Loads florist weight calculation data to production
   * 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void loadFloristWeightCalcDataToProduction(GregorianCalendar processingDate)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    
    try 
    {
      logger.debug("Begin loading florist weight calculation data to production");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("LOAD_FLORIST_WEIGHT_DATA");
      HashMap inputParameters = new HashMap();
      
      inputParameters.put("IN_CALC_DATE", new Date(processingDate.getTimeInMillis()));
      dataRequest.setInputParams(inputParameters);

      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(message);
        throw new Exception(message);
      }
     
      logger.debug("End loading florist weight calculation data to production");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
  }
    
  
  @SuppressWarnings("unchecked")
	public void saveFloristSuspendResumeData(FloristSuspendResumeTimeVO floristSuspendResumeTimeVO,
			Connection con) throws Exception 
	{
		logger.debug("Saving Florist Suspend/Resume Data for FloristId:"+floristSuspendResumeTimeVO.getFloristID());
		DataAccessUtil dau = DataAccessUtil.getInstance();
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(con);
		dataRequest.setStatementID("UPDATE_FLORIST_MERCURY_SUSPEND");

		Map inputParameters = new HashMap();
		inputParameters.put("IN_FLORIST_ID",floristSuspendResumeTimeVO.getFloristID());
		inputParameters.put("IN_SUSPEND_START_DATE", 
				(floristSuspendResumeTimeVO.getSuspendTime() != null) 
				? new Timestamp(floristSuspendResumeTimeVO.getSuspendTime().getTime()) : null);
		inputParameters.put("IN_SUSPEND_END_DATE", 
				(floristSuspendResumeTimeVO.getResumeTime() != null) ? new Timestamp(
						floristSuspendResumeTimeVO.getResumeTime().getTime()) : null);
		inputParameters.put("IN_TIMEZONE_OFFSET_IN_MINUTES", new Integer(
				floristSuspendResumeTimeVO.getTimezoneOffsetInMinutes()));
		
		dataRequest.setInputParams(inputParameters);

		Map result = (Map) dau.execute(dataRequest);
		String status = (String) result.get("OUT_STATUS");
		String message = (String) result.get("OUT_MESSAGE");

		if (status.equalsIgnoreCase("N")) 
		{
			String logMessage = "Loading Suspend and Resume data for the following florist failed."
					+ "\n"
					+ "Message from database is"
					+ "\n"
					+ message
					+ "\n" + floristSuspendResumeTimeVO.toXML();
			logger.error(logMessage);
			
			SystemMessage.send(logMessage, "FLORIST SUSPEND SERVICE",
					SystemMessage.LEVEL_PRODUCTION, "ERROR", con);
		}

	}


  /**
   * Saves the florist weight calculation data
   * 
   * @param map
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void saveFloristWeightCalcData(Map map)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {    
    Connection con = null;
    try 
    {
      logger.debug("Begin saving florist weight staging data");
      int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      
      logger.debug(con.getMetaData().getURL());
      
      FloristWeightCalcVO floristWeightCalcVO = null;
      int ctr = 0;
      
      for (Iterator iter = map.keySet().iterator(); iter.hasNext() ; ) 
      {
        floristWeightCalcVO = (FloristWeightCalcVO) map.get(iter.next());
        dataRequest.reset();
        
        dataRequest.setConnection(con);
        
        dataRequest.setStatementID("UPDATE_FLORIST_WEIGHT_CALC");

        HashMap inputParameters = new HashMap();
        inputParameters.put("IN_PARENT_FLORIST_ID", floristWeightCalcVO.getFloristID());
        inputParameters.put("IN_CALC_DATE", new Date(floristWeightCalcVO.getDate().getTimeInMillis()) );        
        inputParameters.put("IN_BASE_WEIGHT", new Integer(floristWeightCalcVO.getBaseWeight()) );
        inputParameters.put("IN_LOW_SENDING_THRESHOLD_WGHT", new Integer(floristWeightCalcVO.getLowSendingThresholdWeight()) );
        inputParameters.put("IN_RECIPROCITY_WEIGHT", new Integer(floristWeightCalcVO.getReciprocityWeight()));
        inputParameters.put("IN_FINAL_WEIGHT", new Integer(floristWeightCalcVO.getFinalWeight()));
        inputParameters.put("IN_TOTAL_REVENUE", new Double(floristWeightCalcVO.getTotalRevenue().doubleValue()));
        inputParameters.put("IN_REBATE_AMOUNT", new Double(floristWeightCalcVO.getRebate().doubleValue()));
        inputParameters.put("IN_TOTAL_CLEARED_ORDERS", new Integer(floristWeightCalcVO.getTotalClearedOrders()));
        inputParameters.put("IN_ORDERS_SENT_FROM_FTD", new Integer(floristWeightCalcVO.getOrdersSentFromFTD()));
        inputParameters.put("IN_LOW_SENDING_THRESHOLD_VALUE", new Integer(floristWeightCalcVO.getLowSendingThresholdValue()));
        inputParameters.put("IN_RECIPROCITY_RATIO", new Integer(floristWeightCalcVO.getReciprocityRatio()));
        inputParameters.put("IN_GROCERY_STORE_FLAG", floristWeightCalcVO.getGroceryStoreFlag());
        inputParameters.put("IN_ORDER_GATHERER_FLAG", floristWeightCalcVO.getOrderGathererFlag());
        inputParameters.put("IN_SEND_ONLY_FLAG", floristWeightCalcVO.getSendOnlyFlag());
        inputParameters.put("IN_GROCERY_STORE_WEIGHT", new Integer(floristWeightCalcVO.getGroceryStoreWeight()) );
        inputParameters.put("IN_REVENUE_LINKING_KEY", floristWeightCalcVO.getTopLevelFloristKey() );
        dataRequest.setInputParams(inputParameters);
        
        Map result = (Map)dau.execute(dataRequest);
        String status = (String) result.get("OUT_STATUS");
        String message = (String)  result.get("OUT_MESSAGE");
        
        if (status.equalsIgnoreCase("N")) 
        {
          String logMessage = "Loading weight data for the following florist failed." 
                                + "\n" 
                                + "Message from database is"
                                + "\n" 
                                + message
                                + "\n" 
                                + floristWeightCalcVO.toXML();
          logger.error(logMessage);
          throw new Exception(logMessage);
        }

        if (++ctr%threshold == 0) logger.debug(ctr + " records processed");
        
      } // end for loop
      
      logger.debug(ctr + " records processed");  
      logger.debug("End saving florist weight staging data");      
    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
  }

  /**
   * Returns a set of florists, specified earlier, alongwith the average revenue
   * and rebate data for the specified date range.
   * 
   * @param currentDate
   * @param beginDate
   * @param endDate
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Map getFloristAvgRevRebData(GregorianCalendar currentDate,
                                      GregorianCalendar beginDate, 
                                      GregorianCalendar endDate)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
   {
      Connection con = null;
      Map map = new HashMap();
      
      try 
      {
        logger.debug("Begin getting florists' average rebate and revenue data");
        int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
  
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
  
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
        dataRequest.setConnection(con); 
        logger.debug(con.getMetaData().getURL());
        dataRequest.setStatementID("GET_FLORIST_REVENUE_REBATE");
          
        // input parameters  
        HashMap parameters = new HashMap();
        parameters.put("IN_CALC_DATE", new Date(currentDate.getTimeInMillis()));
        parameters.put("IN_DATE_RANGE_START", new Date(beginDate.getTimeInMillis()));
        parameters.put("IN_DATE_RANGE_END", new Date(endDate.getTimeInMillis()));
        dataRequest.setInputParams(parameters);
        
        FloristWeightCalcVO floristWeightCalcVO = null;
        
        int ctr = 0;
        
        CachedResultSet crs = (CachedResultSet)dau.execute(dataRequest);
        while (crs.next()) 
        {
          try 
          {
            floristWeightCalcVO = new FloristWeightCalcVO();
            floristWeightCalcVO.setFloristID((String)crs.getObject(1));
            floristWeightCalcVO.setAverageRevenue( (BigDecimal)crs.getObject(2) );
            floristWeightCalcVO.setAverageRebate( (BigDecimal)crs.getObject(3));
            map.put(floristWeightCalcVO.getFloristID(), floristWeightCalcVO);
            
          } catch (Throwable ex) 
          {
            logger.error(ex);
            logger.error(floristWeightCalcVO.toXML());
          } 
          
          
          if (++ctr%threshold == 0) logger.debug(ctr + " records processed");
        }
        
        logger.debug(ctr + " records processed");  
        logger.debug("End getting florists' average rebate and revenue data");

      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
      
      return map;
  }
  
  /**
   * Returns the orders sent from FTD figure for the set of florists
   * , specified earlier, for the specified date range.
   * 
   * Returns a map of maps, where the key corresponds to the 
   * florist id and the map contains the history.
   * 
   * @param currentDate
   * @param beginDate
   * @param endDate
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Map getLSTHistory(GregorianCalendar currentDate,
                                      GregorianCalendar beginDate, 
                                      GregorianCalendar endDate)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
   {
      Connection con = null;
      Map map = new HashMap();
      
      try 
      {
        logger.debug("Begin getting florists' lst history");
        int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
  
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
  
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
        dataRequest.setConnection(con); 
        logger.debug(con.getMetaData().getURL());
        dataRequest.setStatementID("GET_FLORIST_LOW_SENDING");
          
        // input parameters  
        HashMap parameters = new HashMap();
        parameters.put("IN_CALC_DATE", new Date(currentDate.getTimeInMillis()));
        parameters.put("IN_DATE_RANGE_START", new Date(beginDate.getTimeInMillis()));
        parameters.put("IN_DATE_RANGE_END", new Date(endDate.getTimeInMillis()));
        dataRequest.setInputParams(parameters);
        
        FloristWeightCalcVO floristWeightCalcVO = null;
        
        CachedResultSet crs = (CachedResultSet)dau.execute(dataRequest);
        GregorianCalendar date = null;
        int ctr = 0;
        LSTReciprocityVO _LSTReciprocityVO = null;
        String timeStamp = null;
        HashMap history = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateKey = null;
        HashSet dateSet = new HashSet();
        
        while (crs.next()) 
        {
          try 
          {
            _LSTReciprocityVO = new LSTReciprocityVO();
            _LSTReciprocityVO.setFloristID((String)crs.getObject(1));
            date = new GregorianCalendar();
            timeStamp = (crs.getObject(2)).toString();
            date.setTime(sdf.parse(timeStamp));
            _LSTReciprocityVO.setDate( date );
            _LSTReciprocityVO.setTotalClearedOrders( ((BigDecimal)crs.getObject(3)).intValue());
            _LSTReciprocityVO.setLowSendingThresholdValue( ((BigDecimal)crs.getObject(4)).intValue());
            
            if ( ! map.containsKey(_LSTReciprocityVO.getFloristID())) 
            {
                map.put(_LSTReciprocityVO.getFloristID(), new HashMap());
            }
            else 
            {
              history = (HashMap) map.get(_LSTReciprocityVO.getFloristID());
              dateKey = sdf.format( _LSTReciprocityVO.getDate().getTime());
              dateSet.add(dateKey);
              history.put(dateKey, _LSTReciprocityVO);
            }

          } catch (Throwable ex) 
          {
            logger.error(ex);
            logger.error(_LSTReciprocityVO.toXML());
          } 


          if (++ctr%threshold == 0) logger.debug(ctr + " records processed");

        }
        
        logger.debug("Data was obtained for the following dates");
        for(Iterator iter = dateSet.iterator(); iter.hasNext();)
        {
          logger.debug((String)iter.next());
        }

        logger.debug(ctr + " records processed");  
        logger.debug("End getting florists' lst history");

      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
      
      return map;
  }

  /**
   * Returns the orders sent from FTD figure for the set of florists
   * , specified earlier, for the specified date range.
   * 
   * Returns a map of maps, where the key corresponds to the 
   * florist id and the map contains the history.
   *
   * @param currentDate
   * @param beginDate
   * @param endDate
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Map getReciprocityHistory(GregorianCalendar currentDate,
                                      GregorianCalendar beginDate, 
                                      GregorianCalendar endDate)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
   {
      Connection con = null;
      Map map = new HashMap();
      
      try 
      {
        logger.debug("Begin getting florists' reciprocity history");
        int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
  
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
  
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
        dataRequest.setConnection(con); 
        logger.debug(con.getMetaData().getURL());
        dataRequest.setStatementID("GET_FLORIST_RECIPROCITY");
          
        // input parameters  
        HashMap parameters = new HashMap();
        parameters.put("IN_CALC_DATE", new Date(currentDate.getTimeInMillis()));
        parameters.put("IN_DATE_RANGE_START", new Date(beginDate.getTimeInMillis()));
        parameters.put("IN_DATE_RANGE_END", new Date(endDate.getTimeInMillis()));
        dataRequest.setInputParams(parameters);
        
        FloristWeightCalcVO floristWeightCalcVO = null;
        
        CachedResultSet crs = (CachedResultSet)dau.execute(dataRequest);
        GregorianCalendar date = null;
        int ctr = 0;
        LSTReciprocityVO _LSTReciprocityVO = null;
        String timeStamp = null;
        HashMap history = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateKey = null;
        HashSet dateSet = new HashSet();
        
        while (crs.next()) 
        {
          try 
          {
            _LSTReciprocityVO = new LSTReciprocityVO();
            _LSTReciprocityVO.setFloristID((String)crs.getObject(1));
  
            date = new GregorianCalendar();
            timeStamp = (crs.getObject(2)).toString();
            date.setTime(sdf.parse(timeStamp));
            _LSTReciprocityVO.setDate( date );
  
            _LSTReciprocityVO.setTotalClearedOrders( ((BigDecimal)crs.getObject(3)).intValue());
            _LSTReciprocityVO.setReciprocityRatio( ((BigDecimal)crs.getObject(4)).intValue());
            _LSTReciprocityVO.setOrdersSentFromFTD( ((BigDecimal)crs.getObject(6)).intValue());
  
            if ( ! map.containsKey(_LSTReciprocityVO.getFloristID())) 
            {
                map.put(_LSTReciprocityVO.getFloristID(), new HashMap());
            }
            else 
            {
              history = (HashMap) map.get(_LSTReciprocityVO.getFloristID());
              dateKey = sdf.format( _LSTReciprocityVO.getDate().getTime());
              dateSet.add(dateKey);
              history.put(dateKey, _LSTReciprocityVO);
            }
            
          } catch (Throwable ex) 
          {
            logger.error(ex);
            logger.error(_LSTReciprocityVO.toXML());
          }           

          if (++ctr%threshold == 0) logger.debug(ctr + " records processed");

        }
        
        logger.debug("Data was obtained for the following dates");
        for(Iterator iter = dateSet.iterator(); iter.hasNext();)
        {
          logger.debug((String)iter.next());
        }
        
        logger.debug(ctr + " records processed");  
        logger.debug("End getting florists' reciprocity history");

      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
      
      return map;
  }
  
  /**
   * Returns florist weight calculation process control date
   * 
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public GregorianCalendar getFloristWeightCalcControlDate()
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
   {
      Connection con = null;
      GregorianCalendar controlDate = new GregorianCalendar();
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      
      try 
      {
        logger.debug("Begin getting florists' weight calc control date");  
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
  
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
        dataRequest.setConnection(con); 
        logger.debug(con.getMetaData().getURL());
        dataRequest.setStatementID("GET_GLOBAL_PARAM");

        // input parameters  
        HashMap parameters = new HashMap();
        parameters.put("IN_CONTEXT", configUtil.getProperty("florist_weight_calc_config.xml", "FLORIST_WEIGHT_CALC_GLOBAL_PARAM_CONTEXT"));
        parameters.put("IN_PARAM", configUtil.getProperty("florist_weight_calc_config.xml", "FLORIST_WEIGHT_CALC_GLOBAL_PARAM_NAME"));
        dataRequest.setInputParams(parameters);
        SimpleDateFormat sdf =  new SimpleDateFormat("dd/MMM/yy");
        java.util.Date date = sdf.parse((String)dau.execute(dataRequest));
        controlDate.setTime(date);
                
        logger.debug("End getting florists' weight calc control date");  

      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
      
      return controlDate;
  }
  

  /**
   * Updates florist weight calculation process control date
   * 
   * @param controlDate
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristWeightCalcControlDate(GregorianCalendar controlDate)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
   {
      Connection con = null;
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      
      try 
      {
        logger.debug("Begin updating florists' weight calc control date");  
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
  
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
        dataRequest.setConnection(con); 
        logger.debug(con.getMetaData().getURL());
        dataRequest.setStatementID("UPDATE_FLORIST_WEIGHT_CALCULATION_PROCESS_CONTROL_DATE");

        // input parameters  
        HashMap parameters = new HashMap();
        parameters.put("IN_CONTEXT", configUtil.getProperty("florist_weight_calc_config.xml", "FLORIST_WEIGHT_CALC_GLOBAL_PARAM_CONTEXT"));
        parameters.put("IN_PARAM", configUtil.getProperty("florist_weight_calc_config.xml", "FLORIST_WEIGHT_CALC_GLOBAL_PARAM_NAME"));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy");
        String date = sdf.format(new Date(controlDate.getTimeInMillis()));
        parameters.put("IN_VALUE", date );
        parameters.put("IN_UPDATED_BY", "LOAD_MEMBER_DATA");
        dataRequest.setInputParams(parameters);
        
                  
        Map result = (Map)dau.execute(dataRequest);
        String status = (String) result.get("OUT_STATUS");
        String message = (String)  result.get("OUT_MESSAGE");
        if (status.equalsIgnoreCase("N")) 
        {
          logger.error(message);
          throw new Exception(message);
        }
                
        logger.debug("End updating florists' weight calc control date");  

      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
  }

  /**
   * Updates florist weight calculation process control date
   * 
   * @param queueName
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void deleteJMSMessages(String queueName)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
   {
      Connection con = null;
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      
      try 
      {
        logger.debug("Begin deleting messages");  
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
  
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
        dataRequest.setConnection(con); 
        logger.debug(con.getMetaData().getURL());
        dataRequest.setStatementID("DELETE_MESSAGES");

        // input parameters  
        HashMap parameters = new HashMap();
        parameters.put("IN_QUEUE_NAME", queueName);
        dataRequest.setInputParams(parameters);
        
        logger.debug("Deleting all messages from " + queueName);   
        
        Map result = (Map)dau.execute(dataRequest);
        String status = (String) result.get("OUT_STATUS");
        String message = (String)  result.get("OUT_MESSAGE");
        if (status.equalsIgnoreCase("N")) 
        {
          logger.error(message);
          throw new Exception(message);
        }
                
        logger.debug("End deleting messages");  

      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
  }



  /**
   * Returns a florist weight calculation audit report document
   * 
   * @param reportID
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Document getFloristWeightCalcAuditReport(String reportID)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    return this.getAuditReport(reportID);   
  }

  /**
   * Returns a document containing florist weight calculation audit reports
   * 
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public Document getFloristWeightCalcAuditReportNames()
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    Document reportNamesDoc = null;
    
    try 
    {
      logger.debug("Begin getting florist weight calculation audit report names");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("VIEW_FLORIST_WEIGHT_RPT_NAMES");
      
      reportNamesDoc = (Document)dau.execute(dataRequest);    
      
      logger.debug("End getting florist weight calculation audit report names");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
    return reportNamesDoc;
    
  }
 

  /**
   * Updates the status of the florist weight calculation audit report
   * 
   * @param reportID
   * @param oldStatus
   * @param newStatus
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristWeightCalcAuditReport(String reportID, String oldStatus, String newStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    this.updateAuditReport(reportID, oldStatus, newStatus, updatedBy);
  }

  /**
   * Updates the florist weight calculation audit report comments
   * 
   * @param reportID
   * @param reportComments
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristWeightCalcAuditReportComments(String reportID, String reportComments, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    this.updateAuditReportComments(reportID, reportComments, updatedBy);
  }
  /*****************************************************************************/
  /**
   * Returns an audit report document
   * 
   * @param reportID
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  private Document getAuditReport(String reportID)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;
    Document auditReport = null;
    Element headerContent = null, detailsContent = null;
    
    try 
    {
      logger.debug("Begin getting audit report data");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("VIEW_AUDIT_REPORT");

      HashMap inputParameters = new HashMap();
      inputParameters.put("IN_REPORT_ID", reportID);
      dataRequest.setInputParams(inputParameters);
      
      Map result = (Map)dau.execute(dataRequest);
      Document reportHeader = (Document)result.get("OUT_HEADER_CUR");    
      Document reportDetails = (Document)result.get("OUT_DETAIL_CUR");  
      
      auditReport = DOMUtil.getDefaultDocument();
      Element root = auditReport.createElement("audit-report");
      auditReport.appendChild(root);
      
      Element header = auditReport.createElement("header");
      NodeList reportHeaderNL = DOMUtil.selectNodes(reportHeader, "//content");
      for (int i = 0; i < reportHeaderNL.getLength(); i++) 
      {
        headerContent = (Element)reportHeaderNL.item(i);
        if (headerContent != null) 
        {
          header.appendChild(auditReport.importNode(headerContent, true));                    
        }
      }
      root.appendChild(header);
      
      Element details = auditReport.createElement("details");
      NodeList reportDetailsNL = DOMUtil.selectNodes(reportDetails, "//content");
      for (int i = 0; i < reportDetailsNL.getLength(); i++) 
      {
        detailsContent = (Element) reportDetailsNL.item(i);
        if (detailsContent != null) 
        {
          details.appendChild(auditReport.importNode(detailsContent, true));                    
        }
      }
      root.appendChild(details);      
      logger.debug("End getting audit report data");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }
    
    return auditReport;
  }

  /**
   * Updates the status of an audit report
   * 
   * @param reportID
   * @param oldStatus
   * @param newStatus
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  private void updateAuditReport(String reportID, String oldStatus, String newStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;    
    try 
    {
      logger.debug("Begin updating audit report data");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("UPDATE_AUDIT_REPORT_STATUS");

      HashMap inputParameters = new HashMap();
      inputParameters.put("IN_REPORT_ID", reportID);
      inputParameters.put("IN_OLD_STATUS", oldStatus);
      inputParameters.put("IN_NEW_STATUS", newStatus);
      inputParameters.put("IN_UPDATED_BY", updatedBy);
      dataRequest.setInputParams(inputParameters);

      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(message);
        throw new Exception(message);
      }        
     
      logger.debug("End updating audit report data");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }    
  }


  /**
   * Updates report comments
   * 
   * @param reportID
   * @param reportComments
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  private void updateAuditReportComments(String reportID, String reportComments, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    Connection con = null;    
    try 
    {
      logger.debug("Begin updating audit report comments");

      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
            
      logger.debug(con.getMetaData().getURL());
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();      

      dataRequest.setConnection(con);      
      dataRequest.setStatementID("UPDATE_AUDIT_REPORT_COMMENT");

      HashMap inputParameters = new HashMap();
      inputParameters.put("IN_REPORT_ID", reportID);
      inputParameters.put("IN_REPORT_COMMENT", reportComments);
      inputParameters.put("IN_UPDATED_BY", updatedBy);
      dataRequest.setInputParams(inputParameters);

      Map result = (Map)dau.execute(dataRequest);
      String status = (String) result.get("OUT_STATUS");
      String message = (String)  result.get("OUT_MESSAGE");
      
      if (status.equalsIgnoreCase("N")) 
      {
        logger.error(message);
        throw new Exception(message);
      }        
     
      logger.debug("End updating audit report comments");

    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
    }    
  } 

    
    /**
     *  This procedure is responsible for deleting any florist zip information
     *  for florists that are no longer "signed up" for particular zips.
     *  This is run after a member load and linking.
     */
    public void deleteNonMemberFloristZips()
      throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
     {
        Connection con = null;
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    
        try
        {
          logger.debug("Begin deleting non-member florist zips");
          String useJDBCConnectionUtil = ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
          con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
    
          DataAccessUtil dau = DataAccessUtil.getInstance();
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(con);
          logger.debug(con.getMetaData().getURL());
          dataRequest.setStatementID("DELETE_NON_MEMBER_FLORIST_ZIPS");
    
          Map result = (Map)dau.execute(dataRequest);
          String status = (String) result.get("OUT_STATUS");
          String message = (String)  result.get("OUT_MESSAGE");
          if (status.equalsIgnoreCase("N"))
          {
            logger.error(message);
            throw new Exception(message);
          }
          logger.debug("End deleting non-member florist zips");
    
        } finally
        {
          if (con != null && (! con.isClosed() ))
          {
              con.close();
          }
        }
    }
    
    public List getFloristBlocks(String floristId, Connection con) throws SQLException, Exception {

    	List blockList = new ArrayList();

    	try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("VIEW_FLORIST_BLOCKS");
            Map paramMap = new HashMap();
            paramMap.put("IN_FLORIST_ID", floristId); 
            dataRequest.setInputParams(paramMap);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
            while (crs.next()) {
        	    FloristBlockVO blockVO = new FloristBlockVO();
            	blockVO.setBlockType(crs.getString("block_type"));
            	blockVO.setBlockStartDate(crs.getDate("block_start_date"));
            	blockVO.setBlockEndDate(crs.getDate("block_end_date"));
        	    blockVO.setBlockUser(crs.getString("blocked_by_user_id"));
            	blockVO.setBlockFlag(crs.getString("blocked_flag"));
            	blockVO.setBlockReason(crs.getString("block_reason"));
            	blockList.add(blockVO);
            }
        
    	} catch (Exception e) {
    		logger.error(e);
    	}

    	return blockList;
    }

	public void processFloristSuspend(Connection con, String floristId) throws Exception
	{
		logger.debug("Begin FloristDAO.processFloristSuspend()");
		DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("FLORIST_SUSPEND_RESUME");
        
        Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put("IN_FLORIST_ID", floristId);
        paramMap.put("IN_SUSPEND_TYPE", "M");//insignificant...this value will be retrieved from DB
        paramMap.put("IN_SUSPEND_INDICATOR", "N");//insignificant...this value will be retrieved from DB
        
        dataRequest.setInputParams(paramMap);
        
        Map result = (Map)dau.execute(dataRequest);
        String status = (String) result.get("OUT_STATUS");
        String message = (String)  result.get("OUT_MESSAGE");
        if (status.equalsIgnoreCase("N"))
        {
          logger.error(message);
          throw new Exception(message);
        }
        logger.debug("End.");
	}
    
    public void insertErosAvailabilityHistory(String floristId, Date generationDate, Date expirationDate,
    		String onlineFlag, String callForwardingFlag, Connection con) throws Exception {
    	
    	try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("INSERT_EROS_AVAIL_HISTORY");
            Map paramMap = new HashMap();
            paramMap.put("IN_FLORIST_ID", floristId); 
            paramMap.put("IN_GENERATION_DATE", generationDate); 
            paramMap.put("IN_EXPIRATION_DATE", expirationDate); 
            paramMap.put("IN_ONLINE_FLAG", onlineFlag); 
            paramMap.put("IN_CALL_FORWARDING_FLAG", callForwardingFlag); 
            dataRequest.setInputParams(paramMap);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    		Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            
            String status = (String) outputs.get("OUT_STATUS");
            if(status != null && status.equalsIgnoreCase("N")) {
                String message = (String) outputs.get("OUT_MESSAGE");
                logger.error(message);
                throw new Exception(message);
            }
    		
    	} catch (Exception e) {
    		logger.error(e);
    	}
    }
    
	@SuppressWarnings("unchecked")
	public void postOJMSMessage(Connection conn, String queueName, 
						 String correlationId, String payload, 
						 long delaySeconds ) throws Exception 
	{
        
        DataRequest dataRequest = new DataRequest();
        
        Map<String, Object> inputParams = new HashMap<String, Object>();
        inputParams.put("IN_QUEUE_NAME",queueName);
        inputParams.put("IN_CORRELATION_ID",correlationId);
        inputParams.put("IN_PAYLOAD",payload);
        inputParams.put("IN_DELAY_SECONDS",new Long(delaySeconds));
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("POST_OJMS_MESSAGE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
	

	/** Updates the florist Lat Long information into florist_master.
	 * @param conn
	 * @param floristId
	 * @param latLong
	 * @param updatedBy
	 * @throws Exception
	 */
	public boolean updateFloristLatLong(Connection conn, String floristId, double[] latLong, String updatedBy) throws Exception {
		try{
			logger.info("Entering updateFloristLatLong()");
	        DataRequest dataRequest = new DataRequest();        
	        Map<String, Object> inputParams = new HashMap<String, Object>();
	        inputParams.put("IN_FLORIST_ID",floristId);
	        inputParams.put("IN_LATITUDE",new Double(latLong[0]));        
	        inputParams.put("IN_LONGITUDE",new Double(latLong[1]));
	        inputParams.put("IN_UPDATED_BY",updatedBy);
	        dataRequest.setInputParams(inputParams);
	        dataRequest.setConnection(conn);
	        dataRequest.setStatementID("UPDATE_FLORIST_LATLONG");
	        
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
	        
			String status = (String) outputs.get("OUT_STATUS");
			
			if (status != null && status.equalsIgnoreCase("N")) {				
				throw new Exception("Error updating lat long: out Message:"	+ (String) outputs.get("OUT_MESSAGE"));
			}
			
			return true;
		} catch (Exception e) {
			throw new Exception("Error updating lat long: " + e.getMessage());
		}
	}
	
	public boolean insertFloristSuspendHistory(Connection conn, FloristSuspendResumeTimeVO vo) throws Exception {
		
		boolean result = true;
		logger.info("insertFloristSuspendHistory()");
		
		try {
	        DataRequest dataRequest = new DataRequest();        
	        Map<String, Object> inputParams = new HashMap<String, Object>();
	        inputParams.put("IN_FLORIST_ID", vo.getFloristID());
	        inputParams.put("IN_SUSPEND_START_DATE", vo.getSuspendTime() == null ? null : new Timestamp(vo.getSuspendTime().getTime()));        
	        inputParams.put("IN_SUSPEND_END_DATE", vo.getResumeTime() == null ? null : new Timestamp(vo.getResumeTime().getTime()));
	        dataRequest.setInputParams(inputParams);
	        dataRequest.setConnection(conn);
	        dataRequest.setStatementID("INSERT_FLORIST_SUSPEND_HISTORY");
	        
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
	        
			String status = (String) outputs.get("OUT_STATUS");
			
			if (status != null && status.equalsIgnoreCase("N")) {				
				logger.error("Error inserting history: " + (String) outputs.get("OUT_MESSAGE"));
				result = false;
			}
			
		} catch (Exception e) {
			logger.error(e);
		}
		
		return result;
	}
	
	/**
	 * @param conn
	 * @param member
	 * @param dayClosedOverrideInfo
	 * @return Boolean
	 */
	public boolean insertFlorisHoursOverride(Connection conn,String memberCode,	java.sql.Date overrideDate) throws Exception
	{
		DataRequest dataRequest = null;
		Map<String, Object> inputParameters = null;
		DataAccessUtil dataAccessUtil = null;
		Map<String, Object> outputs = null;
		boolean result = true;

		logger.info(" In insertFlorisHoursOverride() ");

		try {

			dataRequest = new DataRequest();
			dataAccessUtil = DataAccessUtil.getInstance();

			inputParameters = new HashMap<String, Object>();
			inputParameters.put("IN_FLORIST_ID", memberCode);
			inputParameters.put("IN_OVERRIDE_DATE",overrideDate);

			dataRequest.setInputParams(inputParameters);
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("INSERT_FLORIST_HOURS_OVERRIDE");
			outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
			String status = (String) outputs.get("OUT_STATUS");

			if (status != null && status.equalsIgnoreCase("N")) {
				logger.error("Error inserting florist Overrides : "	+ (String) outputs.get("OUT_MESSAGE"));
				result = false;
			}

		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}
	
	
	/**
	 * @param conn
	 * @param memberCode
	 * @param overrideDate
	 * @return Boolean
	 */
	public boolean deleteFlorisHoursOverride(Connection conn,String memberCode,	java.sql.Date overrideDate) throws Exception
	{
		DataRequest dataRequest = null;
		Map<String, Object> inputParameters = null;
		DataAccessUtil dataAccessUtil = null;
		Map<String, Object> outputs = null;
		boolean result = true;

		logger.info(" In deleteFlorisHoursOverride() ");

		try {

			dataRequest = new DataRequest();
			dataAccessUtil = DataAccessUtil.getInstance();

			inputParameters = new HashMap<String, Object>();
			inputParameters.put("IN_FLORIST_ID", memberCode);
			inputParameters.put("IN_OVERRIDE_DATE",overrideDate);

			dataRequest.setInputParams(inputParameters);
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("DELETE_FLORIST_HOURS_OVERRIDE");
			outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
			String status = (String) outputs.get("OUT_STATUS");
			
			if (status != null && status.equalsIgnoreCase("N")) {
				logger.error("Error Deleting florist Overrides : "	+ (String) outputs.get("OUT_MESSAGE"));
				result = false;
			}

		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}
	
	/**
	 * @param conn
	 * @param memberCode
	 * @param overrideDate
	 * @return Boolean
	 */
	public boolean updateFlorisHoursOverride(Connection conn,String memberCode,	java.sql.Date overrideDate) throws Exception
	{
		DataRequest dataRequest = null;
		Map<String, Object> inputParameters = null;
		DataAccessUtil dataAccessUtil = null;
		Map<String, Object> outputs = null;
		boolean result = true;

		logger.info(" In updateFlorisHoursOverride() ");

		try {

			dataRequest = new DataRequest();
			dataAccessUtil = DataAccessUtil.getInstance();

			inputParameters = new HashMap<String, Object>();
			inputParameters.put("IN_FLORIST_ID", memberCode);
			inputParameters.put("IN_OVERRIDE_DATE",overrideDate);

			dataRequest.setInputParams(inputParameters);
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("UPDATE_FLORIST_HOURS_OVERRIDE");
			outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
			String status = (String) outputs.get("OUT_STATUS");
			
			if (status != null && status.equalsIgnoreCase("N")) {
				logger.error("Error Updating florist Overrides : "	+ (String) outputs.get("OUT_MESSAGE"));
				result = false;
			}

		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}
}

