package com.ftd.applications.lmd.dao;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.applications.lmd.vo.*;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class ViewQueueDAO  
{

     private static final String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.dao.ViewQueueDAO";

    private Logger logger;
    private Connection connection;

    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE";
    private static final String SYS_MESSAGE_TYPE_ERROR = "ERROR";
    private static final String SYS_MESSAGE_SOURCE = "NOPAGE View Queue Processing";
    private static final String NO_DATA_FOUND_SQL_CODE = "ORA-01403";

    public ViewQueueDAO(Connection conn)
    {
        this.connection = conn;
        logger = new Logger(LOGGER_CATEGORY);    
    }
    
    public void updateViewQueue(ViewQueueVO vo) throws Exception
    {
        long time = System.currentTimeMillis();
    
        time = System.currentTimeMillis();
        updateFloristBlocks(vo);
        logger.debug("View Queue-Florist Block Update: " + (System.currentTimeMillis() - time));
        
        time = System.currentTimeMillis();
        updateFloristCodifications(vo);
        logger.debug("View Queue-Florist Codifications Update: " + (System.currentTimeMillis() - time));        
        
        time = System.currentTimeMillis();
        updateFloristZips(vo);
        logger.debug("View Queue-Florist Zips Update: " + (System.currentTimeMillis() - time));        
    }

      /*
       * Update florist block information
       */
      private void updateFloristBlocks(ViewQueueVO viewQueueVO) throws Exception
      {
      
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy"); 
          
          FloristBlockVO floristBlockVO = viewQueueVO.getNewFloristBlock();
          logger.info("floristId: " + viewQueueVO.getFloristNumber());
          logger.info("blockType: " + floristBlockVO.getBlockType());
          logger.info("blockIndicator: " + floristBlockVO.getBlockFlag());
          logger.info("reason: " + floristBlockVO.getBlockReason());
          logger.info("changed: " + floristBlockVO.isChanged());

          if(floristBlockVO != null && floristBlockVO.getBlockFlag() != null && floristBlockVO.getBlockFlag().equalsIgnoreCase("Y")) {
              dataRequest.setStatementID("INSERT_FLORIST_BLOCKS");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",viewQueueVO.getFloristNumber());
              paramMap.put("IN_BLOCK_TYPE",floristBlockVO.getBlockType());
              paramMap.put("IN_BLOCK_INDICATOR",floristBlockVO.getBlockFlag());
              paramMap.put("IN_BLOCK_START_DATE",convertDate(floristBlockVO.getBlockStartDate()));
              paramMap.put("IN_BLOCK_END_DATE",convertDate(floristBlockVO.getBlockEndDate()));
              paramMap.put("IN_BLOCKED_BY_USER_ID",viewQueueVO.getLastUpdateUser());

              String additionalComment = "from View Queue because " +
                  floristBlockVO.getBlockReason() + " (" + viewQueueVO.getOrderNumber() + ")";
              paramMap.put("IN_ADDITIONAL_COMMENT",additionalComment);
              paramMap.put("IN_ENQUEUE_FLAG", "Y");
              paramMap.put("IN_BLOCK_REASON", floristBlockVO.getBlockReason());
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);

              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
          
          if (viewQueueVO.getRemoveBlockList() != null && viewQueueVO.getRemoveBlockList().size() > 0) {
        	  List removeList = viewQueueVO.getRemoveBlockList();
        	  for (int i=0; i<removeList.size(); i++) {
        		  Date removeDate = (Date) removeList.get(i);
        		  logger.info("removeDate: " + sdf.format(removeDate) + " " + viewQueueVO.getLastUpdateUser());
        		  
                  dataRequest.setStatementID("REMOVE_FLORIST_BLOCKS");
                  Map paramMap = new HashMap();
                  paramMap.put("IN_FLORIST_ID", viewQueueVO.getFloristNumber());
                  paramMap.put("IN_BLOCK_START_DATE", convertDate(removeDate));
                  paramMap.put("IN_BLOCKED_BY_USER_ID",viewQueueVO.getLastUpdateUser());
                  String additionalComment = "from View Queue " + " (" + viewQueueVO.getOrderNumber() + ")";
                  paramMap.put("IN_ADDITIONAL_COMMENT",additionalComment);
                  paramMap.put("IN_ENQUEUE_FLAG", "Y");
                  dataRequest.setInputParams(paramMap);
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);

                  String status = (String) outputs.get(STATUS_PARAM);
                  if(status != null && status.equals("N"))
                  {
                      String message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }
        	  }
          }
      }

      /*
       * Update/Insert the product codifications
       */
      private void updateFloristCodifications(ViewQueueVO viewQueueVO) throws Exception
      {        

          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
         
          //loop through each codification
          Map codifiedMap = viewQueueVO.getCodifiedProductMap();          
          Iterator i = codifiedMap.entrySet().iterator();    
          while(i.hasNext()){
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              CodifiedProductVO codifiedVO = (CodifiedProductVO)codifiedMap.get(e.getKey());        
          
              if(codifiedVO != null && codifiedVO.isChanged())
              {
                dataRequest.setStatementID("UPDATE_FLORIST_CODIFICATIONS");
                Map paramMap = new HashMap();
                paramMap.put("IN_FLORIST_ID",viewQueueVO.getFloristNumber()); 
                paramMap.put("IN_CODIFICATION_ID",codifiedVO.getId()); 
                paramMap.put("IN_BLOCK_INDICATOR",codifiedVO.getBlockedFlag()); 
                paramMap.put("IN_BLOCK_END_DATE",convertDate(codifiedVO.getBlockEndDate())); 
                paramMap.put("IN_BLOCKED_BY_USER_ID",viewQueueVO.getLastUpdateUser());
                paramMap.put("IN_ADDITIONAL_COMMENT",ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.VIEW_QUEUE_ADDITIONAL_COMMENT) + " (" + viewQueueVO.getOrderNumber() + ")"); 
                
                dataRequest.setInputParams(paramMap);
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                
                String status = (String) outputs.get(STATUS_PARAM);
                String message = (String) outputs.get(MESSAGE_PARAM);
                if(status != null && status.equals("N"))
                {
                  throw new Exception(message);
                }
              }

          }//end while                      
      }
    

      private void updateFloristZips(ViewQueueVO viewQueueVO) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          List deleteList = new ArrayList();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;
          
          //loop through each zip
          Map postalMap = viewQueueVO.getPostalCodeMap();        
          Iterator i = postalMap.entrySet().iterator();    
          while(i.hasNext()){
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              PostalCodeVO postalCodeVO = (PostalCodeVO)postalMap.get(e.getKey());
              
              if(postalCodeVO != null && postalCodeVO.isChanged())
              {
                  if(postalCodeVO.getDeleteFlag() != null && postalCodeVO.getDeleteFlag().equals("Y"))
                  {
                      dataRequest.setStatementID("DELETE_FLORIST_ZIPS");
                      Map paramMap = new HashMap();
                      paramMap.put("IN_FLORIST_ID",viewQueueVO.getFloristNumber()); 
                      paramMap.put("IN_ZIP_CODE",postalCodeVO.getPostalCode());
                      dataRequest.setInputParams(paramMap);
                      outputs = (Map) dataAccessUtil.execute(dataRequest);
                      
                      deleteList.add(postalCodeVO.getPostalCode());
                  }
                  else
                  {
                          dataRequest.setStatementID("UPDATE_FLORIST_ZIPS");
                          Map paramMap = new HashMap();
                          paramMap.put("IN_FLORIST_ID",viewQueueVO.getFloristNumber()); 
                          paramMap.put("IN_ZIP_CODE",postalCodeVO.getPostalCode());
                          paramMap.put("IN_BLOCK_INDICATOR",postalCodeVO.getBlockedFlag());
                          paramMap.put("IN_BLOCK_END_DATE",convertDate(postalCodeVO.getBlockEndDate()));
                          paramMap.put("IN_CUTOFF_TIME",postalCodeVO.getCutoffTime());
                          paramMap.put("IN_UPDATED_BY",viewQueueVO.getLastUpdateUser()); 
                          paramMap.put("IN_ADDITIONAL_COMMENT",ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.VIEW_QUEUE_ADDITIONAL_COMMENT)  + " (" + viewQueueVO.getOrderNumber() + ")"); 
                          dataRequest.setInputParams(paramMap);
                          outputs = (Map) dataAccessUtil.execute(dataRequest);
                  }
                  
                  String status = (String) outputs.get(STATUS_PARAM);
                  if(status != null && status.equals("N"))
                  {
                      String message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }    
              }
          }//end while
          
          // clear deletes from memory
          for(int j=0; j < deleteList.size(); j++)
          {
              viewQueueVO.getPostalCodeMap().remove(deleteList.get(j));
          }
      }
      
    
    public ViewQueueVO getViewQueue(String messageId) throws Exception
    {
        ViewQueueVO viewQueue = null;
        Map dataMap = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("VIEW_VIEW_QUEUE");
        dataRequest.addInputParam("IN_MERCURY_ID", messageId);
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();                
        try {
        	dataMap =  (Map)dataAccessUtil.execute(dataRequest);    
          if(dataMap != null)
          {
              CachedResultSet rs = (CachedResultSet) dataMap.get("FloristCursor");
              
              while(rs != null && rs.next())
              {            
                  viewQueue = new ViewQueueVO();
                  
                  viewQueue.setMercuryId(messageId);
                  viewQueue.setDeliveryDate(rs.getDate("delivery_date"));
                  viewQueue.setFloristCity(rs.getString("city"));
                  viewQueue.setFloristGotoFlag(rs.getString("super_florist_flag"));
                  viewQueue.setFloristName(rs.getString("florist_name"));
                  viewQueue.setFloristNumber(rs.getString("florist_id"));
                  viewQueue.setFloristState(rs.getString("state"));
                  viewQueue.setDeliveryAddress(rs.getString("city_state_zip"));
                  viewQueue.setFloristStatus(rs.getString("status"));
                  viewQueue.setMercuryNumber(rs.getString("mercury_message_number"));
                  viewQueue.setDeliveryZipCode(rs.getString("mercury_zip"));
  				viewQueue.setMessageDate(rs.getString("created_on"));
                  viewQueue.setMessageReason(rs.getString("comments"));
                  viewQueue.setOrderNumber(rs.getString("order_number"));
                  viewQueue.setProductBlockFlag(rs.getString("codify_blocked"));
                  viewQueue.setProductId(rs.getString("product_id"));
                  viewQueue.setProductName(rs.getString("product_name"));
                  viewQueue.setProductUnblockDate(rs.getDate("codify_block_end_date"));
                  viewQueue.setProductCodifiedId(rs.getString("codification_id"));
                  viewQueue.setZipBlockFlag(rs.getString("zip_blocked"));
                  viewQueue.setZipUnblockDate(rs.getDate("zip_block_end_date"));    

                  viewQueue.setZipFloristCount(rs.getInt("active_florists_count"));
                  viewQueue.setMessageCount(rs.getInt("total_view_queue_messages"));
                  viewQueue.setDefaultUnblockDate(rs.getString("default_unblock_date"));

                  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                  logger.info(messageId + " " + viewQueue.getFloristNumber() + 
                  		" zipBlock: " + viewQueue.getZipBlockFlag() +
                  		" goto: " + viewQueue.getFloristGotoFlag());
                  if (viewQueue.getZipUnblockDate() != null) {
                  	logger.info(sdf.format(viewQueue.getZipUnblockDate()));
                  }
      
                  FloristSuspendVO floristSuspend = new FloristSuspendVO();
                  floristSuspend.setSuspendType(rs.getString("suspend_type"));
                  floristSuspend.setSuspendStartDate(rs.getDate("suspend_start_date"));
                  floristSuspend.setSuspendEndDate(rs.getDate("suspend_end_date"));
                  floristSuspend.setSuspendFlag(rs.getString("suspend_flag"));
                  viewQueue.setFloristSuspend(floristSuspend);

                  viewQueue.setPostalCodeMap(getPostalCodes(viewQueue.getFloristNumber(),dataMap));
                  viewQueue.setCodifiedProductMap(getCodifiedProducts(viewQueue.getFloristNumber(),dataMap));

                  List blockList = new ArrayList();
                  FloristBlockVO floristBlockVO = null;
                  String statusString = "";

              	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
              	Date today = sdfDate.parse(sdfDate.format(new Date())); 

                  CachedResultSet rs1 = (CachedResultSet) dataMap.get("FloristBlockCursor");
                  while(rs1 != null && rs1.next())
                  {
                      floristBlockVO = new FloristBlockVO();

                      floristBlockVO.setBlockType(rs1.getString("block_type"));
                      floristBlockVO.setBlockStartDate(rs1.getDate("block_start_date"));
                      floristBlockVO.setBlockEndDate(rs1.getDate("block_end_date"));
                      floristBlockVO.setBlockUser(rs1.getString("blocked_by_user_id"));
                      floristBlockVO.setBlockFlag(rs1.getString("blocked_flag"));
                      floristBlockVO.setBlockReason(rs1.getString("block_reason"));

                      if (floristBlockVO.getBlockType() != null && floristBlockVO.getBlockType().equals("O")) {
                      	long dayDiffStart = (floristBlockVO.getBlockStartDate().getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
                      	logger.info("block: " + floristBlockVO.getBlockType() +
                      			" " + sdf.format(floristBlockVO.getBlockStartDate()) + " " + dayDiffStart);
                      	if (dayDiffStart <= 30) {
                      	    blockList.add(floristBlockVO);
                      	    statusString += "Opt Out - " + sdfDate.format(floristBlockVO.getBlockStartDate());
                      	    statusString += " ";
                      	}
                      } else {
                      	long dayDiffStart = (floristBlockVO.getBlockStartDate().getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
                      	long dayDiffEnd = (floristBlockVO.getBlockEndDate().getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
                      	logger.info("block: " + floristBlockVO.getBlockType() +
                  			" " + sdf.format(floristBlockVO.getBlockStartDate()) +
                  			" " + sdf.format(floristBlockVO.getBlockEndDate()) +
                  			" " + dayDiffStart + " " + dayDiffEnd);

                      	if ((dayDiffStart <= 30)) {
                      	    blockList.add(floristBlockVO);
              	    		statusString += "Blocked - " + sdfDate.format(floristBlockVO.getBlockStartDate()) +
              	    				" - " + sdfDate.format(floristBlockVO.getBlockEndDate());
              	    	    statusString += " ";
                          }
                      }
                  }
                  viewQueue.setFloristBlockList(blockList);
                  if (!statusString.equals("")) {
                  	viewQueue.setFloristStatus(statusString);
                  }

                  // Set the changed flags to false
                  viewQueue.setChangeFlags(false);
              }
              
          }	
        }
        catch (SQLException sqe) {
        	logger.error("Exception caught while trying to get the View Queue message from DB", sqe);
        	logger.error("Checking if the exception cause is due to NO DATA FOUND sql exception or not");
        	if (sqe.getMessage() != null && sqe.getMessage().contains(NO_DATA_FOUND_SQL_CODE)) {
        		try {
          		this.removeMessageFromQueue(messageId);	
          	}
          	catch (Exception e) {
          		StringBuffer errorString = new StringBuffer("Failed to set View Queue flag to N for Mercury Id: ");
          		errorString.append(messageId).append(". Exception message: ").append(e.getMessage());
          		errorString.append(messageId).append(". If this alert is triggered again then DEV team need to manually update the view queue flag to N").append(e.getMessage());
          		logger.error(errorString.toString(), e);
          		logger.error("Re-throwing the exception will cause Blank Screen issue in View Queue scree. So suppressing this exception and sending a system message");
          		SystemMessage.send(errorString.toString(), SYS_MESSAGE_SOURCE, SystemMessage.LEVEL_PRODUCTION, SYS_MESSAGE_TYPE_ERROR, connection);
          	}
        	}
        }
        catch (Exception e) {
        	StringBuffer errorString = new StringBuffer("Unable to get the View Queue message from DB for Mercury Id: ");
      		errorString.append(messageId).append(". Exception message: ").append(e.getMessage());
      		logger.error(errorString.toString(), e);
      		logger.error("Re-throwing the exception will cause Blank Screen issue in View Queue scree. So suppressing this exception and sending a system message");
        	SystemMessage.send(errorString.toString(), SYS_MESSAGE_SOURCE, SystemMessage.LEVEL_PRODUCTION, SYS_MESSAGE_TYPE_ERROR, connection);
        }
        
        return viewQueue;        
    }

    private Map getPostalCodes(String floristId, Map floristMap) throws Exception
    {
        HashMap postalCodeMap = new HashMap();
        PostalCodeVO postalCode = null;
        
        CachedResultSet rs = (CachedResultSet) floristMap.get("PostalCodeCursor");
        while(rs != null && rs.next())
        {
            postalCode = new PostalCodeVO();
            postalCode.setPostalCode((String)rs.getObject(2));
            postalCode.setBlockStartDate((Date)rs.getObject(3));
            postalCode.setBlockEndDate((Date)rs.getObject(4));
            postalCode.setBlockedFlag((String)rs.getObject(5));
            postalCode.setCutoffTime((String)rs.getObject(6));
            if(rs.getObject(7) != null)
            {
                postalCode.setFloristCount(new Integer(((BigDecimal)rs.getObject(7)).intValue()));
            }
            postalCode.setCity((String)rs.getObject(8));
            
            postalCodeMap.put(postalCode.getPostalCode(), postalCode);
        }
        
        return postalCodeMap;
    }    

    private Map getCodifiedProducts(String floristId, Map floristMap) throws Exception
    {
        HashMap codifiedProductMap = new HashMap();
        CodifiedProductVO codifiedProduct = null;
        
        CachedResultSet rs = (CachedResultSet) floristMap.get("FloristCodificationsCursor");
        while(rs != null && rs.next())
        {
            codifiedProduct = new CodifiedProductVO();
            codifiedProduct.setId((String)rs.getObject(2));
            codifiedProduct.setBlockStartDate((Date)rs.getObject(3));
            codifiedProduct.setBlockEndDate((Date)rs.getObject(4));
            codifiedProduct.setBlockedFlag((String)rs.getObject(5));
            codifiedProduct.setName((String)rs.getObject(6));
            codifiedProduct.setCategory((String)rs.getObject(7));
            
            codifiedProductMap.put(codifiedProduct.getId(), codifiedProduct);
        }
        
        return codifiedProductMap;
    }
        
 
     /*
     * 
     */
    private java.sql.Date convertDate(java.util.Date value)
    {
        java.sql.Date sqlDate = null;
        
        if(value != null)
        {
            sqlDate = new java.sql.Date(value.getTime());
        }//end if
        
        return sqlDate;
        
    }//end method
    
    public void removeMessageFromQueue(String mercuryId) throws Exception
    {
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_MERCURY_VIEW_QUEUE");
        dataRequest.addInputParam("IN_MERCURY_ID", mercuryId);
        dataRequest.addInputParam("IN_VIEW_QUEUE", "N");
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
     
    
}