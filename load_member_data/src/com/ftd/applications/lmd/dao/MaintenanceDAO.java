package com.ftd.applications.lmd.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ftd.applications.lmd.vo.*;

import org.w3c.dom.Document;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This is the DAO which handles the loads, inserts, and updates for the florist 
 * maintenance functionality.
 *
 * @author Brian Munter 
 */

public class MaintenanceDAO 
{
    private Connection connection;
    private Logger logger;

    private static final String FLORIST_ID = "FLORIST_ID";
    private static final String USER_ID = "USER_ID";
    private static final String COMMENT_COUNT = "COMMENT_COUNT";
    private static final String VIEW_FLORIST_INFO = "VIEW_FLORIST_INFO";
    private static final String VIEW_FLORISTS_BY_ZIPS = "VIEW_FLORISTS_BY_ZIPS";
    private static final String VIEW_FLORISTS_BY_CITY = "VIEW_FLORISTS_BY_CITY";
    private static final String VIEW_FLORIST_WEIGHT_HISTORY = "VIEW_FLORIST_WEIGHT_HISTORY";
    private static final String VIEW_FLORIST_ZIPS = "VIEW_FLORIST_ZIPS";
    private static final String VIEW_FLORIST_ZIPS_DEAD = "VIEW_FLORIST_ZIPS_DEAD";
    private static final String VIEW_HOLIDAY_PRODUCTS = "VIEW_HOLIDAY_PRODUCTS";
    private static final String VIEW_HOLIDAY_PRODUCTS_DETAIL = "VIEW_HOLIDAY_PRODUCTS_DETAIL";
    
    private static final String FLORIST_MASTER_CURSOR = "FloristMasterCursor";
    private static final String FLORIST_CODIFICATIONS_CURSOR = "FloristCodificationsCursor";
    private static final String ASSOCIATED_FLORISTS_CURSOR = "AssociatedFloristsCursor";
    private static final String POSTAL_CODE_CURSOR = "PostalCodeCursor";
    private static final String FLORIST_HISTORY_CURSOR = "FloristHistoryCursor";
    private static final String ZIPS_AVAIL_CURSOR = "ZipsAvailCursor";
    private static final String ZIPS_ASSIGNED_CURSOR = "ZipsAssignedCursor";
    private static final String FLORIST_BLOCK_CURSOR = "FloristBlockCursor";
    private static final String FLORIST_SUSPEND_CURSOR = "FloristSuspendCursor";
    private static final String FLORIST_CITIES_CURSOR = "FloristCitiesCursor";

    // These represent zip assignedStatus which is used by front-end
    private static final String ZIP_AVAIL   = "avail";
    private static final String ZIP_UNAVAIL = "unavail";
    private static final String ZIP_ASSIGNED = "assigned";
    private static final String ZIP_ASSIGNED_UNAVAIL = "assignedUnavail";

    private static final String ZIP_IS_BLOCKED = "Y";
    
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE";    
    
    
    private static final String  GET_FLORIST_OVERRIDE = "GET_FLORIST_OVERRIDE";
    private static final String   IN_FLORIST_ID="IN_FLORIST_ID";
    private static final String  IN_OVERRIDE_DATE ="IN_OVERRIDE_DATE";
    
    
    private boolean returnStatus;

    /** 
     * Constructor
     * 
     * @param connection Connection
     */
    public MaintenanceDAO(Connection connection)
    {
        this.connection = connection;
        this.logger = new Logger("com.ftd.applications.lmd.dao.MaintenanceDAO");
        this.returnStatus = true;
    }
    
    /**
     * This method is used to facilitate loading the value objects for the florist maintence
     * functionality.  All of the table loads are accomplished through one stored 
     * procedure which calls multple procedures within it.  The procedure then returns
     * multiple result sets which are pushed into the value object structure.
     *   
     * @param floristId String
     * @return floristValueObject FloristVO
     * @throws java.lang.Exception
     */
    public FloristVO load(String floristId, String userId) throws Exception
    {
        FloristVO florist = null;
    
        long time = System.currentTimeMillis();
        Map floristMap = getFloristMap(floristId, userId);
        logger.debug("Load floristMap time (ms): " + (System.currentTimeMillis() - time) + 
                    " for florist: " + floristId);
        
        if(floristMap != null)
        {
            florist = getGeneralFloristData(floristId, floristMap);
            florist.setCodifiedProductMap(getCodifiedProducts(floristId, floristMap));
            florist.setAssociatedFloristList(getAssociatedFlorists(floristId, floristMap));
            florist.setFloristHistoryList(getFloristHistory(floristId, floristMap));
            florist.setPostalCodeMap(getPostalCodes(floristId, floristMap));
            florist.setPerformanceSingleList(getPerformanceSingleData(floristId));
            florist.setPerformanceAssociatedList(getPerformanceAssociatedData(floristId));
            florist.setSourceFloristList(getSourceFloristList(floristMap));
            florist.setFloristBlockList(getFloristBlocks(floristMap));
            florist.setFloristSuspend(getFloristSuspend(floristMap));
            florist.setFloristClosureVO(getFloristClosureData(floristId));
            florist.setCitiesMap(getCities(floristId, floristMap));

            // Set the changed flags to false
            florist.setChangeFlags(false);
        }
        
        return florist;
    }

    /**
     * This method updates all the data found within the Florist VO.
     * 
     * @param floristVO Florist data
     */
     public void updateFlorist(FloristVO floristVO) throws Exception
     {
        long time = System.currentTimeMillis();
        
        // Update profile only for vendors
        if(floristVO.getVendorFlag() != null && floristVO.getVendorFlag().equals("Y"))
        {
            updateFloristProfile(floristVO);
            logger.debug("Vendor Update: " + (System.currentTimeMillis() - time));
        }
        // Normal florist update
        else
        {
            updateFloristProfile(floristVO);
            logger.debug("Profile Update: " + (System.currentTimeMillis() - time));
        
            time = System.currentTimeMillis();
            updateFloristWeights(floristVO);
            logger.debug("Florist Weights Update: " + (System.currentTimeMillis() - time));
        
            time = System.currentTimeMillis();
            updateFloristBlocks(floristVO);
            logger.debug("Florist Block Update: " + (System.currentTimeMillis() - time));
            
//            time = System.currentTimeMillis();
//            updateSundayBlock(floristVO);
//            logger.debug("Sunday Block Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristCodifications(floristVO);
            logger.debug("Florist Codifications Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristCodifiedMisc(floristVO);
            logger.debug("Codified Misc Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristZips(floristVO);
            logger.debug("Florist Zips Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristCities(floristVO);
            logger.debug("Florist Cities Update: " + (System.currentTimeMillis() - time));
        }
     }
     
     /**
     * This method inserts all the data found within the Florist VO.
     * 
     * @param floristVO Florist data
     */
     public void insertFlorist(FloristVO floristVO) throws Exception
     {
        long time = System.currentTimeMillis();
        
        // Insert for vendors
        if(floristVO.getVendorFlag() != null && floristVO.getVendorFlag().equals("Y"))
        {
            insertVendor(floristVO);
            logger.debug("Vendor Insert: " + (System.currentTimeMillis() - time));
        }
        // Insert for regular florists
        else
        {
            insertFloristProfile(floristVO);
            logger.debug("Profile Insert: " + (System.currentTimeMillis() - time));
        
            time = System.currentTimeMillis();
            updateFloristWeights(floristVO);
            logger.debug("Florist Weights Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristBlocks(floristVO);
            logger.debug("Florist Block Update: " + (System.currentTimeMillis() - time));
            
//            time = System.currentTimeMillis();
//            updateSundayBlock(floristVO);
//            logger.debug("Sunday Block Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristCodifications(floristVO);
            logger.debug("Florist Codifications Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristCodifiedMisc(floristVO);
            logger.debug("Codified Misc Update: " + (System.currentTimeMillis() - time));
            
            time = System.currentTimeMillis();
            updateFloristZips(floristVO);
            logger.debug("Florist Zips Update: " + (System.currentTimeMillis() - time));
        }
     }
     
      /*
       * Insert florist information
       */
      private void insertFloristProfile(FloristVO floristVO) throws Exception
      {
      
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          
          if(floristVO != null && floristVO.isChanged())
          {
              dataRequest.setStatementID("INSERT_FLORIST_PROFILE");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
              paramMap.put("IN_FLORIST_NAME",floristVO.getName());
              paramMap.put("IN_ADDRESS",floristVO.getAddress());
              paramMap.put("IN_CITY",floristVO.getCity());
              paramMap.put("IN_STATE",floristVO.getState());
              paramMap.put("IN_ZIP_CODE",floristVO.getPostalCode());
              paramMap.put("IN_PHONE_NUMBER",FieldUtils.stripNonNumeric(floristVO.getPhone()));
              paramMap.put("IN_FAX_NUMBER",floristVO.getFax());
              paramMap.put("IN_EMAIL_ADDRESS",floristVO.getEmailAddress());
              paramMap.put("IN_LAST_UDPATED_BY",floristVO.getLastUpdateUser());
              paramMap.put("IN_ALT_PHONE_NUMBER",floristVO.getAltPhone());
              paramMap.put("IN_TERRITORY",floristVO.getTerritory());
              paramMap.put("IN_OWNERS_NAME",floristVO.getContactName());
              paramMap.put("IN_RECORD_TYPE",floristVO.getRecordType());
              paramMap.put("IN_STATUS",floristVO.getStatus());
              paramMap.put("IN_VENDOR_FLAG",floristVO.getVendorFlag());
              paramMap.put("IN_MINIMUM_ORDER_AMOUNT",getDoubleValue(floristVO.getMinimumOrderAmount()));
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
      }
      
      
      /*
       * Insert vendor information
       */
      private void insertVendor(FloristVO floristVO) throws Exception
      {
      
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          
          if(floristVO != null && floristVO.isChanged())
          {
              dataRequest.setStatementID("INSERT_FLORIST_PROFILE");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
              paramMap.put("IN_FLORIST_NAME",floristVO.getName());
              paramMap.put("IN_ADDRESS",floristVO.getAddress());
              paramMap.put("IN_CITY",floristVO.getCity());
              paramMap.put("IN_STATE",floristVO.getState());
              paramMap.put("IN_ZIP_CODE",floristVO.getPostalCode());
              paramMap.put("IN_PHONE_NUMBER",FieldUtils.stripNonNumeric(floristVO.getPhone()));
              paramMap.put("IN_FAX_NUMBER",floristVO.getFax());
              paramMap.put("IN_EMAIL_ADDRESS",floristVO.getEmailAddress());
              paramMap.put("IN_LAST_UDPATED_BY",floristVO.getLastUpdateUser());
              paramMap.put("IN_ALT_PHONE_NUMBER",floristVO.getAltPhone());
              paramMap.put("IN_TERRITORY",floristVO.getTerritory());
              paramMap.put("IN_OWNERS_NAME",floristVO.getContactName());
              paramMap.put("IN_RECORD_TYPE",floristVO.getRecordType());
              paramMap.put("IN_STATUS",floristVO.getStatus());
              paramMap.put("IN_VENDOR_FLAG",floristVO.getVendorFlag());
              paramMap.put("IN_MINIMUM_ORDER_AMOUNT", null);
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
              
              // Default the final weight for the vendor
              dataRequest.reset();
              dataRequest.setStatementID("UPDATE_VENDOR_WEIGHT");
              paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber());
              paramMap.put("IN_FLORIST_WEIGHT",floristVO.getFloristWeight().getFinalWeight());
              dataRequest.setInputParams(paramMap);
              dataAccessUtil = DataAccessUtil.getInstance();
              outputs = (Map) dataAccessUtil.execute(dataRequest);
        
              status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
      }

      /*
       * Update florist information
       */
      private void updateFloristProfile(FloristVO floristVO) throws Exception
      {
      
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          
          if(floristVO != null && floristVO.isChanged())
          {
              dataRequest.setStatementID("UPDATE_FLORIST_PROFILE");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
              paramMap.put("IN_FLORIST_NAME",floristVO.getName());
              paramMap.put("IN_ADDRESS",floristVO.getAddress());
              paramMap.put("IN_CITY",floristVO.getCity());
              paramMap.put("IN_STATE",floristVO.getState());
              paramMap.put("IN_ZIP_CODE",floristVO.getPostalCode());
              paramMap.put("IN_PHONE_NUMBER",FieldUtils.stripNonNumeric(floristVO.getPhone()));
              paramMap.put("IN_FAX_NUMBER",floristVO.getFax());
              paramMap.put("IN_EMAIL_ADDRESS",floristVO.getEmailAddress());
              paramMap.put("IN_LAST_UDPATED_BY",floristVO.getLastUpdateUser());
              paramMap.put("IN_ALT_PHONE_NUMBER",floristVO.getAltPhone());
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
              
              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
      }

      /*
       * Update florist block information
       */
      public void updateFloristBlocks(FloristVO floristVO) throws Exception
      {
      
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();

          logger.debug("updateFloristBlocks called");
          List<FloristBlockVO> floristBlockList = floristVO.getFloristBlockList();
          for (int i=0; i < floristBlockList.size();i++)
          {
              FloristBlockVO floristBlockVO = floristBlockList.get(i);
              logger.debug("processing block " + floristBlockVO.getBlockType() + " " + floristBlockVO.getBlockStartDate());
              if(floristBlockVO != null)
              {
                  if(floristBlockVO.isBlockNew())
                  {
                      logger.debug("Block is NEW");
                      insertFloristBlock(floristVO.getMemberNumber(),floristBlockVO, floristVO.getLastUpdateUser());
                  }
                  else if(floristBlockVO.isBlockRemoved())
                  {
                      logger.debug("Block is REMOVED");
                      removeFloristBlock(floristVO.getMemberNumber(),floristBlockVO, floristVO.getLastUpdateUser());

                  }
                  else
                  {
                      logger.debug("Block is unchanged");
                  }
              }
          }
      }
      public void insertFloristBlock(String memberNumber, FloristBlockVO floristBlockVO, String userID) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);

          dataRequest.setStatementID("INSERT_FLORIST_BLOCKS");
          Map paramMap = new HashMap();
          paramMap.put("IN_FLORIST_ID",memberNumber);
          paramMap.put("IN_BLOCK_TYPE",floristBlockVO.getBlockType());
          paramMap.put("IN_BLOCK_INDICATOR",floristBlockVO.getBlockFlag());
          paramMap.put("IN_BLOCK_START_DATE",convertDate(floristBlockVO.getBlockStartDate()));
          paramMap.put("IN_BLOCK_END_DATE",convertDate(floristBlockVO.getBlockEndDate()));
          paramMap.put("IN_BLOCKED_BY_USER_ID",userID);
          paramMap.put("IN_ADDITIONAL_COMMENT",null);
          paramMap.put("IN_ENQUEUE_FLAG","Y");
          paramMap.put("IN_BLOCK_REASON", null);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          String status = (String) outputs.get(STATUS_PARAM);
          if(status != null && status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }

      }

      public void removeFloristBlock(String memberNumber, FloristBlockVO floristBlockVO, String userID) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);

          dataRequest.setStatementID("REMOVE_FLORIST_BLOCKS");
          Map paramMap = new HashMap();
          paramMap.put("IN_FLORIST_ID",memberNumber);
          paramMap.put("IN_BLOCK_TYPE",floristBlockVO.getBlockType());
          paramMap.put("IN_BLOCK_INDICATOR",floristBlockVO.getBlockFlag());
          paramMap.put("IN_BLOCK_START_DATE",convertDate(floristBlockVO.getBlockStartDate()));
          paramMap.put("IN_BLOCK_END_DATE",convertDate(floristBlockVO.getBlockEndDate()));
          paramMap.put("IN_BLOCKED_BY_USER_ID",userID);
          paramMap.put("IN_ADDITIONAL_COMMENT",null);
          paramMap.put("IN_ENQUEUE_FLAG","Y");
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);

          String status = (String) outputs.get(STATUS_PARAM);
          if(status != null && status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }

      }
      
     /**
      * Update florist block information
      * 
      * @param floristID
      * @param floristBlockVO
      * @throws Exception
      */
      public void updateFloristBlocks(String floristID, FloristBlockVO floristBlockVO) throws Exception
      {
      
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          
          if(floristBlockVO != null && floristBlockVO.isChanged())
          {
              dataRequest.setStatementID("UPDATE_FLORIST_BLOCKS");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID", floristID); 
              paramMap.put("IN_BLOCK_TYPE", floristBlockVO.getBlockType()); 
              paramMap.put("IN_BLOCK_INDICATOR", floristBlockVO.getBlockFlag()); 
              paramMap.put("IN_BLOCK_START_DATE", convertDate(floristBlockVO.getBlockStartDate())); 
              paramMap.put("IN_BLOCK_END_DATE", convertDate(floristBlockVO.getBlockEndDate())); 
              paramMap.put("IN_BLOCKED_BY_USER_ID", floristBlockVO.getBlockUser()); 
              paramMap.put("IN_ADDITIONAL_COMMENT", null); 
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
              
              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
      }

      /*
       * Update florist weight information
       */
      private void updateFloristWeights(FloristVO floristVO) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          
          FloristWeightVO floristWeightVO = floristVO.getFloristWeight();
          if(floristWeightVO != null && floristWeightVO.isChanged())
          {
              dataRequest.setStatementID("UPDATE_FLORIST_WEIGHTS");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
              paramMap.put("IN_SUPER_FLORIST_FLAG",floristWeightVO.getGotoFlag()); 
              paramMap.put("IN_ADJUSTED_WEIGHT",floristWeightVO.getAdjustedWeight()); 
              paramMap.put("IN_ADJUSTED_WEIGHT_START_DATE",convertDate(floristWeightVO.getAdjustedWeightStartDate())); 
              paramMap.put("IN_ADJUSTED_WEIGHT_END_DATE",convertDate(floristWeightVO.getAdjustedWeightEndDate())); 
              paramMap.put("IN_ADJUSTED_WEIGHT_PERM_FLAG",floristWeightVO.getAdjustedWeightPermFlag()); 
              paramMap.put("IN_LAST_UPDATED_BY",floristVO.getLastUpdateUser()); 
              paramMap.put("IN_SUSPEND_OVERRIDE_FLAG",floristWeightVO.getMercurySuspendOverride()); 
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
              
              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
      }
      
      /*
       * Update/Insert the product codifications
       */
      private void updateFloristCodifications(FloristVO floristVO) throws Exception
      {        

          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
         
          //loop through each codification
          Map codifiedMap = floristVO.getCodifiedProductMap();          
          Iterator i = codifiedMap.entrySet().iterator();    
          while(i.hasNext()){
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              CodifiedProductVO codifiedVO = (CodifiedProductVO)codifiedMap.get(e.getKey());        
          
              if(codifiedVO != null && codifiedVO.isChanged())
              {
                dataRequest.setStatementID("UPDATE_FLORIST_CODIFICATIONS");
                Map paramMap = new HashMap();
                paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
                paramMap.put("IN_CODIFICATION_ID",codifiedVO.getId()); 
                paramMap.put("IN_BLOCK_INDICATOR",codifiedVO.getBlockedFlag()); 
                paramMap.put("IN_BLOCK_END_DATE",convertDate(codifiedVO.getBlockEndDate())); 
                paramMap.put("IN_BLOCKED_BY_USER_ID",floristVO.getLastUpdateUser());
                paramMap.put("IN_ADDITIONAL_COMMENT",null); 
                
                dataRequest.setInputParams(paramMap);
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                
                String status = (String) outputs.get(STATUS_PARAM);
                String message = (String) outputs.get(MESSAGE_PARAM);
                if(status != null && status.equals("N"))
                {
                  throw new Exception(message);
                }
              }

          }//end while                      
      }
      
      private void updateFloristCodifiedMisc(FloristVO floristVO) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();

          if(floristVO != null && floristVO.isChanged())
          {
              dataRequest.setStatementID("UPDATE_FLORIST_CODIFIED_MISC");
              Map paramMap = new HashMap();
              paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
              paramMap.put("IN_MERCURY_FLAG",floristVO.getMercuryMachine());
              paramMap.put("IN_MINIMUM_ORDER_AMOUNT",getDoubleValue(floristVO.getMinimumOrderAmount()));
              paramMap.put("IN_LAST_UPDATED_BY",floristVO.getLastUpdateUser());
              dataRequest.setInputParams(paramMap);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
              
              String status = (String) outputs.get(STATUS_PARAM);
              if(status != null && status.equals("N"))
              {
                  String message = (String) outputs.get(MESSAGE_PARAM);
                  throw new Exception(message);
              }
          }
                              
      }
      
      private void updateFloristZips(FloristVO floristVO) throws Exception
      {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          List deleteList = new ArrayList();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;
          
          //loop through each zip
          Map postalMap = floristVO.getPostalCodeMap();        
          Iterator i = postalMap.entrySet().iterator();    
          while(i.hasNext()){
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              PostalCodeVO postalCodeVO = (PostalCodeVO)postalMap.get(e.getKey());
              
              // If any zip info changed (and zip is assigned to this florist)
              // then delete/insert/update accordingly
              //
              if(postalCodeVO != null && postalCodeVO.isChanged() &&
                 (ZIP_ASSIGNED.equals(postalCodeVO.getAssignedStatus()) ||
                  ZIP_ASSIGNED_UNAVAIL.equals(postalCodeVO.getAssignedStatus())))
              {
                  if(postalCodeVO.getDeleteFlag() != null && postalCodeVO.getDeleteFlag().equals("Y"))
                  {
                      dataRequest.setStatementID("DELETE_FLORIST_ZIPS");
                      Map paramMap = new HashMap();
                      paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
                      paramMap.put("IN_ZIP_CODE",postalCodeVO.getPostalCode());
                      dataRequest.setInputParams(paramMap);
                      outputs = (Map) dataAccessUtil.execute(dataRequest);
                      
                      deleteList.add(postalCodeVO.getPostalCode());
                  }
                  else
                  {
                          dataRequest.setStatementID("UPDATE_FLORIST_ZIPS");
                          Map paramMap = new HashMap();
                          paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
                          paramMap.put("IN_ZIP_CODE",postalCodeVO.getPostalCode());
                          paramMap.put("IN_BLOCK_INDICATOR",postalCodeVO.getBlockedFlag());
                          paramMap.put("IN_BLOCK_START_DATE",convertDate(postalCodeVO.getBlockStartDate()));
                          paramMap.put("IN_BLOCK_END_DATE",convertDate(postalCodeVO.getBlockEndDate()));
                          paramMap.put("IN_CUTOFF_TIME",postalCodeVO.getCutoffTime());
                          paramMap.put("IN_UPDATED_BY",floristVO.getLastUpdateUser());
                          paramMap.put("IN_ADDITIONAL_COMMENT",null); 
                          paramMap.put("IN_ASSIGNMENT_TYPE",postalCodeVO.getAssignmentType());
                          dataRequest.setInputParams(paramMap);
                          outputs = (Map) dataAccessUtil.execute(dataRequest);
                  }
                  
                  String status = (String) outputs.get(STATUS_PARAM);
                  if(status != null && status.equals("N"))
                  {
                      String message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }    
              }
          }//end while
          
          // clear deletes from memory
          for(int j=0; j < deleteList.size(); j++)
          {
              floristVO.getPostalCodeMap().remove(deleteList.get(j));
          }
      }
      
      private void updateFloristCities(FloristVO floristVO) throws Exception {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = null;

          Map citiesMap = floristVO.getCitiesMap();
          Iterator i = citiesMap.entrySet().iterator();    
          while(i.hasNext()) {
              //get New VO
              Map.Entry e = (Map.Entry) i.next();
              CityVO cityVO = (CityVO) citiesMap.get(e.getKey());
              if (cityVO.isChanged()) {
                  if(cityVO.getBlockStartDate() == null) {
                      dataRequest.setStatementID("DELETE_FLORIST_CITY_BLOCKS");
                      Map paramMap = new HashMap();
                      paramMap.put("IN_FLORIST_ID", cityVO.getFloristId());
                	  paramMap.put("IN_UPDATED_BY", floristVO.getLastUpdateUser());
                      dataRequest.setInputParams(paramMap);
                      outputs = (Map) dataAccessUtil.execute(dataRequest);
                  } else {
                	  dataRequest.setStatementID("UPDATE_FLORIST_CITY_BLOCKS");
                	  Map paramMap = new HashMap();
                	  paramMap.put("IN_FLORIST_ID", cityVO.getFloristId()); 
                	  paramMap.put("IN_BLOCK_START_DATE", convertDate(cityVO.getBlockStartDate()));
                	  paramMap.put("IN_BLOCK_END_DATE", convertDate(cityVO.getBlockEndDate()));
                	  paramMap.put("IN_BLOCK_COMMENTS", null);
                	  paramMap.put("IN_UPDATED_BY", floristVO.getLastUpdateUser());
                	  dataRequest.setInputParams(paramMap);
                	  outputs = (Map) dataAccessUtil.execute(dataRequest);
                  }
                  
            	  String status = (String) outputs.get(STATUS_PARAM);
            	  if(status != null && status.equals("N")) {
            		  String message = (String) outputs.get(MESSAGE_PARAM);
            		  throw new Exception(message);
            	  }
              }
          }
      }
      
      private void updateFloristSuspend(FloristVO floristVO) throws Exception
      {
          /*
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          List resultList = new ArrayList();
          
          dataRequest.setStatementID("UPDATE_FLORIST_SUSPEND");
          Map paramMap = new HashMap();
          paramMap.put("IN_FLORIST_ID",floristVO.getMemberNumber()); 
          paramMap.put("IN_SUSPEND_TYPE",floristVO.getSuspendType());
          paramMap.put("IN_BLOCK_INDICATOR",floristVO.getSuspendFlag());
          paramMap.put("IN_SUSPEND_END_DATE",convertDate(floristVO.getSuspendEndDate()));
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
          String status = (String) outputs.get(STATUS_PARAM);
          if(status != null && status.equals("N"))
          {
              String message = (String) outputs.get(MESSAGE_PARAM);
              throw new Exception(message);
          }
          */
    }

    public Document getFloristByZip(String zipcode, Date deliveryDate) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_FLORISTS_BY_ZIPS);
        dataRequest.addInputParam("IN_ZIP_CODE", zipcode);
        dataRequest.addInputParam("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }

    public Document getZipRemovals() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_FLORIST_ZIPS_DEAD);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }

    public Document getWeights(String memid) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_FLORIST_WEIGHT_HISTORY);
        dataRequest.addInputParam("IN_FLORIST_ID", memid);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }



    private FloristVO getGeneralFloristData(String floristId, Map floristMap) throws Exception
    {
        FloristVO florist = new FloristVO();
        
        CachedResultSet rs = (CachedResultSet) floristMap.get(FLORIST_MASTER_CURSOR);
        while(rs != null && rs.next())
        {
            florist.setMemberNumber((String)rs.getObject(1));
            florist.setName((String)rs.getObject(2));
            florist.setAddress((String)rs.getObject(3));
            florist.setCity((String)rs.getObject(4));
            florist.setState((String)rs.getObject(5));
            florist.setPostalCode((String)rs.getObject(6));
            florist.setPhone((String)rs.getObject(7));
            florist.setContactName((String)rs.getObject(10));
            florist.setStatus((String)rs.getObject(11));
            florist.setFax((String)rs.getObject(12));
            florist.setLastUpdateUser((String)rs.getObject(17));
            florist.setLastUpdateDate((String)rs.getObject(18));
            florist.setAltPhone((String)rs.getObject(19));
			if(rs.getObject(29) != null)
			{
				florist.setMinimumOrderAmount(new Double(((BigDecimal)rs.getObject(29)).doubleValue()));
            }
			if(rs.getObject(20) != null)
            {
                florist.setSendingRank(new Integer(((BigDecimal)rs.getObject(20)).intValue()));
            }
            if(rs.getObject(21) != null)
            {
                florist.setOrdersSent(new Long(((BigDecimal)rs.getObject(21)).longValue()));
            }
            if(rs.getObject(22) != null)
            {
                florist.setPostalCodeCount(new Integer(((BigDecimal)rs.getObject(22)).intValue()));
            }
            florist.setMercuryMachine((String)rs.getObject(25));
            florist.setSundayDeliveryFlag((String)rs.getObject(26));
            florist.setTerritory((String)rs.getObject(30));   
            florist.setEmailAddress((String)rs.getObject(31));
            florist.setVendorFlag((String)rs.getObject(36));
            
            // Set weights on florist
            FloristWeightVO floristWeight = new FloristWeightVO();
            if(rs.getObject(13) != null)
            {
                floristWeight.setInitialWeight(new Integer(((BigDecimal)rs.getObject(13)).intValue()));
            }
            if(rs.getObject(14) != null)
            {
                floristWeight.setAdjustedWeight(new Integer(((BigDecimal)rs.getObject(14)).intValue()));
            }
            if(rs.getObject(8) != null)
            {
                floristWeight.setFinalWeight(new Integer(((BigDecimal)rs.getObject(8)).intValue()));
            }
            floristWeight.setGotoFlag((String)rs.getObject(9));
            floristWeight.setAdjustedWeightStartDate((Date)rs.getObject(15));
            floristWeight.setAdjustedWeightEndDate((Date)rs.getObject(16));
            floristWeight.setAdjustedWeightPermFlag((String)rs.getObject(32));
            floristWeight.setMercurySuspendOverride((String)rs.getObject(37));
            florist.setFloristWeight(floristWeight);
        }
        
        return florist;
    }
    
    private Map getCodifiedProducts(String floristId, Map floristMap) throws Exception
    {
        HashMap codifiedProductMap = new HashMap();
        CodifiedProductVO codifiedProduct = null;
        
        CachedResultSet rs = (CachedResultSet) floristMap.get(FLORIST_CODIFICATIONS_CURSOR);
        while(rs != null && rs.next())
        {
            codifiedProduct = new CodifiedProductVO();
            codifiedProduct.setId((String)rs.getObject(2));
            codifiedProduct.setBlockStartDate((Date)rs.getObject(3));
            codifiedProduct.setBlockEndDate((Date)rs.getObject(4));
            codifiedProduct.setBlockedFlag((String)rs.getObject(5));
            codifiedProduct.setName((String)rs.getObject(6));
            codifiedProduct.setCategory((String)rs.getObject(7));
            
            codifiedProductMap.put(codifiedProduct.getId(), codifiedProduct);
        }
        
        return codifiedProductMap;
    }
    
    private List getAssociatedFlorists(String floristId, Map floristMap) throws Exception
    {
        ArrayList associatedFloristsList = new ArrayList();
        AssociatedFloristVO associatedFlorist = null;
        
        CachedResultSet rs = (CachedResultSet) floristMap.get(ASSOCIATED_FLORISTS_CURSOR);
        while(rs != null && rs.next())
        {
            associatedFlorist = new AssociatedFloristVO();
            associatedFlorist.setMemberNumber((String)rs.getObject(1));
            associatedFlorist.setName((String)rs.getObject(2));
            associatedFlorist.setState((String)rs.getObject(3));
            associatedFlorist.setPostalCode((String)rs.getObject(4));
            associatedFlorist.setPhone((String)rs.getObject(5));
            associatedFlorist.setMercuryFlag((String)rs.getObject(6));
            associatedFlorist.setSundayFlag((String)rs.getObject(7));
            associatedFlorist.setStatus((String)rs.getObject(8));
            if(rs.getObject(9) != null)
            {
                associatedFlorist.setPostalCodeCount(new Integer(((BigDecimal)rs.getObject(9)).intValue()));
            }
            
            associatedFloristsList.add(associatedFlorist);
        }
        
        return associatedFloristsList;
    }
    
	private List getPerformanceSingleData(String floristId) throws Exception
    {
		ArrayList performanceList = new ArrayList();
        FloristPerformanceVO performance = null;
		DecimalFormat df = new DecimalFormat("#.##");
		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
		dataRequest.setStatementID("VIEW_FLORIST_PERFORMANCE");
		dataRequest.addInputParam("IN_FLORIST_ID", floristId);
		dataRequest.addInputParam("IN_ASSOCIATED_FLORIST_FLAG", "N");
		BigDecimal sentNum;
		BigDecimal result;
		double a;

		CachedResultSet prs= (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
		   
		int i=0;
        while(prs != null && prs.next())
        {
            performance = new FloristPerformanceVO();
			
			if(((String)prs.getObject(1)).equals("Fiscal Year") == true)
			{
				i++;
				performance.setNum(""+i);
				sentNum = ((BigDecimal)prs.getObject(3));
				performance.setOSent(sentNum);
				performance.setOSales((BigDecimal)prs.getObject(4));
				
				performance.setOFilled((BigDecimal)prs.getObject(5));			
				result = ((BigDecimal)prs.getObject(5));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPFilled(df.format(a));
				
				performance.setPSales((BigDecimal)prs.getObject(6));
				
				performance.setOForward((BigDecimal)prs.getObject(7));
				result = ((BigDecimal)prs.getObject(7));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPForward(df.format(a));
				
				performance.setOReject((BigDecimal)prs.getObject(8));
				result = ((BigDecimal)prs.getObject(8));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPReject(df.format(a));

				performance.setOCancel((BigDecimal)prs.getObject(9));
				result = ((BigDecimal)prs.getObject(9));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPCancel(df.format(a));				
			}
			
			performanceList.add(performance);
        }
        
        return performanceList;
	}
	
	private List getPerformanceAssociatedData(String floristId) throws Exception
    {
		ArrayList performanceList = new ArrayList();
        FloristPerformanceVO performance = null;

		DecimalFormat df = new DecimalFormat("#.##");
		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
		dataRequest.setStatementID("VIEW_FLORIST_PERFORMANCE");
		dataRequest.addInputParam("IN_FLORIST_ID", floristId);
		dataRequest.addInputParam("IN_ASSOCIATED_FLORIST_FLAG", "Y");
		BigDecimal sentNum;
		BigDecimal result;
		double a;

		CachedResultSet prs= (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
		   
		int i=0;
        while(prs != null && prs.next())
        {
            performance = new FloristPerformanceVO();
			
			if(((String)prs.getObject(1)).equals("Fiscal Year") == true)
			{
				i++;
				performance.setNum(""+i);
				sentNum = ((BigDecimal)prs.getObject(3));
				performance.setOSent(sentNum);
				performance.setOSales((BigDecimal)prs.getObject(4));
				
				performance.setOFilled((BigDecimal)prs.getObject(5));
				result = ((BigDecimal)prs.getObject(5));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPFilled(df.format(a));
				
				performance.setPSales((BigDecimal)prs.getObject(6));
				
				performance.setOForward((BigDecimal)prs.getObject(7));
				result = ((BigDecimal)prs.getObject(7));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPForward(df.format(a));
				
				performance.setOReject((BigDecimal)prs.getObject(8));
				result = ((BigDecimal)prs.getObject(8));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPReject(df.format(a));

				performance.setOCancel((BigDecimal)prs.getObject(9));
				result = ((BigDecimal)prs.getObject(9));
				a = result.doubleValue();
				a = (a / sentNum.doubleValue()) * 100;
				performance.setPCancel(df.format(a));				
			}
			performanceList.add(performance);
        }
        
        return performanceList;
	}

	
	
	private FloristClosureVO getFloristClosureData(String floristId) throws Exception
    {
		
	//	logger.info("getFloristClosureData for the florist id:"+floristId);
		ArrayList performanceList = new ArrayList();
		FloristClosureVO floristClosureVO = new FloristClosureVO();

		
		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
		dataRequest.setStatementID("GET_DAY_CLOSURE");
		dataRequest.addInputParam("IN_FLORIST_ID", floristId);

		CachedResultSet prs= (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
		   
		
        while(prs != null && prs.next())
        {
        	DayClosureVO closureVO=new DayClosureVO();
        	closureVO.setDay((String)prs.getObject(1));
        	closureVO.setJdeStatus((String)prs.getObject(2));
        	floristClosureVO.getFloristDayclosureLst().add(closureVO);
        	if( floristClosureVO.getClosureOverrideFlag()==null && (new Integer(((BigDecimal)prs.getObject(3)).intValue())>0)){
        		floristClosureVO.setClosureOverrideFlag("Y");
        	}
        }
        
        return floristClosureVO;
    }
	
    private Map getPostalCodes(String floristId, Map floristMap) throws Exception
    {
        HashMap zipsAvailMap = new HashMap();
        HashMap zipsAssignedMap = new HashMap();
        HashMap zipCityMap = new HashMap();
        HashMap postalCodeMap = new HashMap();
        PostalCodeVO postalCode = null;
        String zipId;
        String assignmentType;

        // This result set represents EFOS based zips for all florists associated 
        // with this one (via top florist), so create local (zipsAvailMap) hash 
        // with value indicating zip is available.  
        //
        // Note we also get city name here since florist signed up for that
        // particular city.  Since a zip may represent multiple cities, this allows
        // us to diplay proper city name.
        //
        CachedResultSet rsZipsAvail = (CachedResultSet) floristMap.get(ZIPS_AVAIL_CURSOR);
        while(rsZipsAvail != null && rsZipsAvail.next())
        {
            zipId = (String)rsZipsAvail.getObject(1);
            zipsAvailMap.put(zipId, ZIP_AVAIL);
            zipCityMap.put(zipId, (String)rsZipsAvail.getObject(2));
        }
        
        // This result set represents all zips assigned to other florists 
        // associated with this one (via top florist), so modify zipsAvailMap hash
        // to flag these as currently "unavailable" for assignment to this florist.
        // We bother to include these since Member Directory Services zip page 
        // wants to show which are unavailable.
        //
        CachedResultSet rsZipsAssigned = (CachedResultSet) floristMap.get(ZIPS_ASSIGNED_CURSOR);
        while(rsZipsAssigned != null && rsZipsAssigned.next())
        {
            zipId = (String)rsZipsAssigned.getObject(1);
            assignmentType = (String)rsZipsAssigned.getObject(2);
            zipsAvailMap.put(zipId, ZIP_UNAVAIL);
            if (!zipsAvailMap.containsKey(zipId) && (assignmentType == null)) 
            {
                // Zip was not in our list of EFOS zips nor was it a manual
                // override of zip restrictions (assignmentType would be set).
                logger.warn("Zip not EFOS nor manually assigned is associated with florist: " + zipId);
            }
        }
        
        // This result set represents zips currently assigned to this florist,
        // so create the postalCodeMap containing these zips, using zipsAvailMap hash
        // to determine appropriate status for front-end.
        //
        CachedResultSet rs = (CachedResultSet) floristMap.get(POSTAL_CODE_CURSOR);
        while(rs != null && rs.next())
        {
            postalCode = new PostalCodeVO();
            zipId = (String)rs.getObject(2);
            postalCode.setPostalCode(zipId);
            postalCode.setBlockStartDate((Date)rs.getObject(3));
            postalCode.setBlockEndDate((Date)rs.getObject(4));
            postalCode.setBlockedFlag((String)rs.getObject(5));
            postalCode.setCutoffTime((String)rs.getObject(6));
            if(rs.getObject(7) != null)
            {
                postalCode.setFloristCount(new Integer(((BigDecimal)rs.getObject(7)).intValue()));
            }
            if (zipCityMap.containsKey(zipId)) {
                postalCode.setCity((String)zipCityMap.get(zipId));
            } else {
                postalCode.setCity((String)rs.getObject(8));
            }
            assignmentType = (String)rs.getObject(9);
            postalCode.setAssignmentType(assignmentType);
            
            // Since these are all zips assigned to this florist set status as such.
            // If zip is blocked, then still assigned but only Fulfillment has access
            // so flag it so Member Directory Services can't modify it.
            //
            String zipStatus;
            if (ZIP_IS_BLOCKED.equals(postalCode.getBlockedFlag())) 
            {
                zipStatus = ZIP_ASSIGNED_UNAVAIL;  
            } else { 

                zipStatus = ZIP_ASSIGNED;  // Assume normal zip assignment
                
                // Is zip absent from hash or not available?
                if (!ZIP_AVAIL.equals(zipsAvailMap.get(zipId))) 
                {
                    // Yup, so unless it was manual override of zip restriction,
                    // it should be unavail to Member Directory Services.
                    if (assignmentType == null)
                    {
                        zipStatus = ZIP_ASSIGNED_UNAVAIL;
                        logger.warn("Zip not EFOS nor manually assigned is assigned to florist: " + zipId);
                    }
                }
            }
            postalCode.setAssignedStatus(zipStatus);
            postalCodeMap.put(zipId, postalCode);

            // Remove zip from hash since we have just added it
            if (zipsAvailMap.containsKey(zipId)) 
            {
                zipsAvailMap.remove(zipId);
            }
        }
        
        // Now loop over zipsAvailMap and add remaining zips to postalCodeMap
        //
        Iterator i = zipsAvailMap.entrySet().iterator();    
        while (i.hasNext()) 
        {
            Map.Entry e = (Map.Entry) i.next();
            zipId = (String)e.getKey();
            postalCode = new PostalCodeVO();
            postalCode.setPostalCode(zipId);
            postalCode.setFloristCount(new Integer(0));  // ??? Do we really need this?
            // Note that city will be null for unavailable zips, but front-end 
            // doesn't display city for unavail anyhow.
            postalCode.setCity((String)zipCityMap.get(zipId));
            postalCode.setAssignedStatus((String)zipsAvailMap.get(zipId));
            // Everything else should be null in postalCode (and that's what we want)
            postalCodeMap.put(zipId, postalCode);
        }
        
        return postalCodeMap;
    }

    
    private Map getCities(String floristId, Map floristMap) throws Exception
    {
        HashMap cityMap = new HashMap();
        CityVO city = null;
        
        CachedResultSet rsCity = (CachedResultSet) floristMap.get(FLORIST_CITIES_CURSOR);
        while(rsCity != null && rsCity.next()) {
        	city = new CityVO();
        	city.setFloristId(rsCity.getString("florist_id"));
        	city.setFloristName(rsCity.getString("florist_name"));
        	city.setFloristCity(rsCity.getString("city"));
        	city.setFloristState(rsCity.getString("state"));
        	city.setFloristType(rsCity.getString("record_type"));
        	city.setFloristCount(new Integer(((BigDecimal)rsCity.getObject("florist_count")).intValue()));
        	city.setBlockStartDate(rsCity.getDate("block_start_date"));
        	city.setBlockEndDate(rsCity.getDate("block_end_date"));
        	city.setSort1(rsCity.getString("sort1"));
        	city.setSort2(rsCity.getString("sort2"));
        	
        	cityMap.put(city.getFloristId(), city);
        }
        
        return cityMap;
        
    }
    
    public List getFloristZipCodes(String floristId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_FLORIST_ZIPS);
        dataRequest.addInputParam(FLORIST_ID, floristId);
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        List zipList = new ArrayList();
        
        PostalCodeVO postalCode = null;
        CachedResultSet rs =  (CachedResultSet)dataAccessUtil.execute(dataRequest);    
        while(rs != null && rs.next())
        {            
            postalCode = new PostalCodeVO();
            postalCode.setPostalCode((String)rs.getObject(2));
            postalCode.setBlockStartDate((Date)rs.getObject(3));
            postalCode.setBlockEndDate((Date)rs.getObject(4));
            postalCode.setBlockedFlag((String)rs.getObject(5));
            postalCode.setCutoffTime((String)rs.getObject(6));
            zipList.add(postalCode);
        }
        
        return zipList;
    }
    
    private List getFloristHistory(String floristId, Map floristMap) throws Exception
    {
        ArrayList floristHistoryList = new ArrayList();
        FloristHistoryVO floristHistory = null;
        
        CachedResultSet rs = (CachedResultSet) floristMap.get(FLORIST_HISTORY_CURSOR);
        while(rs != null && rs.next())
        {
            floristHistory = new FloristHistoryVO();
            floristHistory.setCommentDate((String)rs.getObject(2));
            floristHistory.setCommentType((String)rs.getObject(3));
            floristHistory.setComments((String)rs.getObject(4));
            floristHistory.setManagerFlag((String)rs.getObject(5));
            floristHistory.setUserId((String)rs.getObject(6));
            
            floristHistoryList.add(floristHistory);
        }
        
        return floristHistoryList;
    }
    
    private Map getFloristMap(String floristId, String userId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_FLORIST_INFO);
        dataRequest.addInputParam(FLORIST_ID, floristId);
        dataRequest.addInputParam(USER_ID, userId);
        dataRequest.addInputParam(COMMENT_COUNT, new Integer(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.COMMENTS_DISPLAY_MAX)));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Map)dataAccessUtil.execute(dataRequest);
    }
  
 
     /*
     * 
     */
    private java.sql.Date convertDate(java.util.Date value)
    {
        java.sql.Date sqlDate = null;
        
        if(value != null)
        {
            sqlDate = new java.sql.Date(value.getTime());
        }//end if
        
        return sqlDate;
        
    }//end method
    
    private String getDoubleValue(Double dbl)
    {
      return dbl == null ? null : dbl.toString();
    }
    
    private List getSourceFloristList(Map floristMap) throws Exception
    {
      ArrayList sourceFloristList = new ArrayList();
      SourceFloristVO sourceFloristVO = null;
      
      CachedResultSet rs = (CachedResultSet) floristMap.get("SourceInfoCursor");
      while(rs != null && rs.next())
      {
        sourceFloristVO = new SourceFloristVO ();
        sourceFloristVO.setSourceCode((String)rs.getObject(1));
        sourceFloristVO.setDescription((String)rs.getObject(2));
        sourceFloristVO.setPriority(((BigDecimal)rs.getObject(3)).intValue());
        sourceFloristList.add(sourceFloristVO);
      }
      return sourceFloristList;
    }

    private List<FloristBlockVO> getFloristBlocks(Map floristMap) throws Exception
    {
        List<FloristBlockVO> blockList = new ArrayList<FloristBlockVO>();
        FloristBlockVO floristBlockVO = null;

        CachedResultSet rs = (CachedResultSet) floristMap.get(FLORIST_BLOCK_CURSOR);
        while(rs != null && rs.next())
        {
            floristBlockVO = new FloristBlockVO();
            floristBlockVO.setBlockType((String)rs.getObject(1));
            floristBlockVO.setBlockStartDate((Date)rs.getObject(2));
            floristBlockVO.setBlockEndDate((Date)rs.getObject(3));
            floristBlockVO.setBlockUser((String)rs.getObject(4));
            floristBlockVO.setBlockFlag((String)rs.getObject(5));
            blockList.add(floristBlockVO);
        }
        return blockList;
    }

    private FloristSuspendVO getFloristSuspend(Map floristMap) throws Exception
    {
        FloristSuspendVO floristSuspendVO = null;

        CachedResultSet rs = (CachedResultSet) floristMap.get(FLORIST_SUSPEND_CURSOR);
        if(rs != null && rs.next())
        {
            floristSuspendVO = new FloristSuspendVO();
            floristSuspendVO.setSuspendType((String) rs.getObject(1));
            floristSuspendVO.setSuspendStartDate((Date) rs.getObject(2));
            floristSuspendVO.setSuspendEndDate((Date) rs.getObject(3));
            floristSuspendVO.setSuspendFlag((String) rs.getObject(4));
        }
        return floristSuspendVO;
    }

    public boolean isFloristSuspendExists(String floristId, String userId) throws Exception
    {
    	Map floristMap = this.getFloristMap(floristId, userId);
    	if(floristMap != null)
        {
	    	FloristSuspendVO floristSuspend = this.getFloristSuspend(floristMap);
	    	if (floristSuspend != null && floristSuspend.getSuspendEndDate() != null) {
	    	    return true;
	    	}
        }
    	return false;
    }
    
    public boolean isFloristOverrideExists(String floristId, java.sql.Date date) throws Exception
    {
    	boolean isExists=false;
    	
    	 DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(connection);
         
         dataRequest.setStatementID(GET_FLORIST_OVERRIDE);
         dataRequest.addInputParam(IN_FLORIST_ID, floristId);
         dataRequest.addInputParam(IN_OVERRIDE_DATE, date);
         
         CachedResultSet rs= (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
         while(rs != null && rs.next()) 
         {
        	 isExists = true;
         } 
    	
     	 return isExists;
    	
    }
    
    public Document getFloristByCity(String floristId, Date deliveryDate) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_FLORISTS_BY_CITY);
        dataRequest.addInputParam("IN_FLORIST_ID", floristId);
        dataRequest.addInputParam("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }
  
    public Document getAllHolidayProducts() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_HOLIDAY_PRODUCTS);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }
  
    public Document getHolidayProductsDetail(Date deliveryDate) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_HOLIDAY_PRODUCTS_DETAIL);
        dataRequest.addInputParam("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }
  
    public Map insertHolidayProducts(String productId, Date deliveryDate) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("INSERT_HOLIDAY_PRODUCTS");
        Map paramMap = new HashMap();
        paramMap.put("IN_PRODUCT_ID", productId);
        paramMap.put("IN_DELIVERY_DATE", convertDate(deliveryDate));
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        return outputs;
    }

    public void deleteHolidayProducts(String productId, Date deliveryDate) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("DELETE_HOLIDAY_PRODUCTS");
        Map paramMap = new HashMap();
        paramMap.put("IN_PRODUCT_ID", productId);
        paramMap.put("IN_DELIVERY_DATE", convertDate(deliveryDate));
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get(STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }

    }

    public Document getPasStateList() throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PAS_STATES");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }
 
    public ArrayList<String> getErosCities(String city) throws Exception {

    	ArrayList<String> results = new ArrayList<String>();
    	
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_EROS_CITIES");
        dataRequest.addInputParam("IN_CITY_NAME", city);
    	
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs= (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (rs != null && rs.next()) {
        	results.add(rs.getString("city_name") + ", " + rs.getString("state_id"));
        }
        
        return results;
    }

    public Map verifyErosCity(String city, String state) throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("VERIFY_EROS_CITY");
        dataRequest.addInputParam("IN_CITY_NAME", city);
        dataRequest.addInputParam("IN_STATE_CODE", state);
    	
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        return outputs;
    }
    
    public Map saveExclusionZone(ExclusionZoneVO ezVO) throws Exception {
    	
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_EXCLUSION_ZONE_HEADER");
        dataRequest.addInputParam("IN_DESCRIPTION", ezVO.getDescription());
        dataRequest.addInputParam("IN_BLOCK_START_DATE", convertDate(ezVO.getBlockStartDate()));
        dataRequest.addInputParam("IN_BLOCK_END_DATE", convertDate(ezVO.getBlockEndDate()));
        dataRequest.addInputParam("IN_CREATED_BY", ezVO.getUserId());
    	
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if (status != null && status.equals("Y")) {
        	String headerId = (String) outputs.get("OUT_HEADER_ID");
        	logger.info("headerId: " + headerId);
        	outputs.put("headerId", headerId);
        	List tempList = ezVO.getBlockCities();
        	if (tempList != null && tempList.size() > 0) {
        		for (int a=0; a<tempList.size(); a++) {
        			String tempKey = (String) tempList.get(a);
        			logger.info("city tempKey: " + tempKey);
        			saveExclusionZoneDetail("CITY", tempKey, headerId, ezVO.getUserId());
        		}
        	}
        	tempList = ezVO.getBlockStates();
        	if (tempList != null && tempList.size() > 0) {
        		for (int a=0; a<tempList.size(); a++) {
        			String tempKey = (String) tempList.get(a);
        			logger.info("state tempKey: " + tempKey);
        			saveExclusionZoneDetail("STATE", tempKey, headerId, ezVO.getUserId());
        		}
        	}
        	tempList = ezVO.getBlockZipCodes();
        	if (tempList != null && tempList.size() > 0) {
        		for (int a=0; a<tempList.size(); a++) {
        			String tempKey = (String) tempList.get(a);
        			logger.info("zipCode tempKey: " + tempKey);
        			saveExclusionZoneDetail("ZIP", tempKey, headerId, ezVO.getUserId());
        		}
        	}
        } else {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }

        return outputs;
    }
    
    private void saveExclusionZoneDetail(String blockSource, String blockSourceKey, String headerId, String userId) throws Exception {
    	
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_EXCLUSION_ZONE_DETAIL");
        dataRequest.addInputParam("IN_HEADER_ID", headerId);
        dataRequest.addInputParam("IN_BLOCK_SOURCE", blockSource);
        dataRequest.addInputParam("IN_BLOCK_SOURCE_KEY", blockSourceKey);
        dataRequest.addInputParam("IN_CREATED_BY", userId);
    	
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if (status != null && status.equals("Y")) {
        	String addedFlag = (String) outputs.get("OUT_ADDED_FLAG");
        	logger.info("added: " + addedFlag);
        } else {
        	String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    public Document getExclusionZoneDetailList() throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_EXCLUSION_ZONE_DETAIL_LIST");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }
 
    public Document getExclusionZoneById(String headerId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.addInputParam("IN_HEADER_ID", headerId);
        dataRequest.setStatementID("GET_EXCLUSION_ZONE_BY_ID");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document)dataAccessUtil.execute(dataRequest);      
    }
 
    public ArrayList<String> getExclusionZoneDetails(String headerId, String blockSource) throws Exception {

    	ArrayList<String> results = new ArrayList<String>();

    	DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.addInputParam("IN_HEADER_ID", headerId);
        dataRequest.addInputParam("IN_BLOCK_SOURCE", blockSource);
        dataRequest.setStatementID("GET_EXCLUSION_ZONE_DETAILS");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs= (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (rs != null && rs.next()) {
        	results.add(rs.getString("block_source_key"));
        }
        
        return results;
    }
 
    public Map removeExclusionZone(String headerId, String userId) throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("REMOVE_EXCLUSION_ZONE");
        dataRequest.addInputParam("IN_HEADER_ID", headerId);
        dataRequest.addInputParam("IN_USER_ID", userId);
    	
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        return outputs;
    }
    
}
