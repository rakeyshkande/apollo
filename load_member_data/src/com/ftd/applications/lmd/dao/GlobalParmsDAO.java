/*
 * @(#) GlobalParmsDAO.java      1.1.2.2     2004/09/29
 * 
 * 
 */

package com.ftd.applications.lmd.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.vo.GlobalParmsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.1.2.2      2004/09/29  Initial Release.(RFL)
 * 
 * 
 */
public class GlobalParmsDAO  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final int GNADD_LEVEL = 1;
    private static final int GNADD_DATE = 2;
    private static final String GLOBAL_PARMS_STORED_PROC = "GET_GLOBAL_PARMS";
    private static final String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.dao.GlobalParmsDAO";
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Logger logger;

    


    public GlobalParmsDAO() 
    {
        super();
        logger = new Logger(LOGGER_CATEGORY);
        
    }//end method
    
    
    /**
     * Retrieves GlobalParms from datastore.
     * 
     * @return GlobalParmsVO
     * @throws DataAccessException
     */
    public GlobalParmsVO getGlobalParms() throws DataAccessException
    {
        Object resultSet = null;
        GlobalParmsVO vo = null;
        CachedResultSet rs = null;
    
        resultSet = this.executeDataQuery(this.GLOBAL_PARMS_STORED_PROC);
        
        if (resultSet == null) 
        {
            return null;
            
        }//end if ()
        
        rs = (CachedResultSet)resultSet;
        
        if (rs != null && rs.next()) 
        {
            vo = new GlobalParmsVO();
            vo.setGNADDState(((BigDecimal)rs.getObject(GNADD_LEVEL)).intValue());
            
            try
            {
                vo.setGNADDDate(this.getDate((String)rs.getObject(GNADD_DATE), 
                                              DATE_FORMAT));
            }//end try
            catch (ParseException pe) 
            {
                logger.error("unable to parse GNADD date", pe);
                throw new DataAccessException();
            }//end catch
        
        }//end if
        
        return vo;
        
    }//end method getGlobalParms()
    
    
    /**
     * 
     * @param GNADDDate
     * @throws DataAccessException
     */
    public void saveGNADDDate(Date GNADDDate, String userId) throws DataAccessException 
    {
        HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;
        
        //set Parameters
        parameters.put("IN_GNADD_DATE", this.getTimestamp(GNADDDate));
        parameters.put("IN_UPDATED_BY", userId);
        resultSet = this.executeDataQuery("UPDATE_GNADD_DATE", parameters);
        
        if (resultSet == null) 
        {
            logger.error("missing output parameters during save GNADD date.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if ()
        
        outputParameters = (Map)resultSet;
        
        if (outputParameters == null) 
        {
            logger.error("missing output parameters during save GNADD date.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if 
        else
        {
            String status = (String) outputParameters.get("OUT_STATUS");
            String message = (String) outputParameters.get("OUT_MESSAGE");
            logger.debug("status:: " + status);
            logger.debug("message:: " + message);
            if (status != null) 
            {
                status = status.trim();
                if (status.equalsIgnoreCase("N"))  
                {
                    logger.error("Failure saving GNADD Date - " + message);
                    throw new DataAccessException("Unable to access "
                                                  + "datastore.");
                    
                }//end if
                
            }//end if
            
        }//end else
        
    }//end method saveGNADDDate
    
    
    /**
     * 
     * @param level
     * @throws DataAccessException
     */
    public void saveGNADDLevel(int level, String userId) throws DataAccessException
    {
        HashMap parameters = new HashMap();
        Object resultSet = null;
        Map outputParameters = null;
        
        //set Parameters
        parameters.put("IN_GNADD_LEVEL", new Integer(level));
        parameters.put("IN_UPDATED_BY", userId);
        resultSet = this.executeDataQuery("UPDATE_GNADD_LEVEL", parameters);
        
        if (resultSet == null) 
        {
            logger.error("missing output parameters during save GNADD level.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if ()
        
        outputParameters = (Map)resultSet;
        
        if (outputParameters == null) 
        {
            logger.error("missing output parameters during save GNADD level.");
            throw new DataAccessException("Unable to access datastore.");
            
        }//end if 
        else
        {
            String status = (String) outputParameters.get("OUT_STATUS");
            String message = (String) outputParameters.get("OUT_MESSAGE");
            logger.debug("status:: " + status);
            logger.debug("message:: " + message);
            if (status != null) 
            {
                status = status.trim();
                if (status.equalsIgnoreCase("N"))  
                {
                    logger.error("Failure saving status of GNADD level - "
                                 + message);
                    throw new DataAccessException("Unable to access "
                                                  + "datastore.");
                    
                }//end if
                
            }//end if
            
        }//end else
        
    }//end method saveGNADDLevel
    
    
    /**
     * 
     * @param GNADDDate
     * @throws DataAccessException
     */
    public void saveOn(Date GNADDDate, String userId) throws DataAccessException
    {
        try 
        {
            
            //status
            this.saveGNADDLevel(GlobalParmsVO.GNADD_STATE_ON, userId);
            
            //date
            this.saveGNADDDate(GNADDDate, userId);
            
            
        }//end try
        catch (Throwable t) 
        {
            logger.error("Throwable thrown during SaveOn.", t);
            throw new DataAccessException("Unable to save GNADD On.");
            
        }//end catch (Exception)
        
    }

    
    /*
     * 
     */
    private Object executeDataQuery(DataRequest dataRequest) 
                                                    throws DataAccessException
    {
        DataAccessUtil dataAccessUtil = null;
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        try 
        {
            dataAccessUtil = DataAccessUtil.getInstance();
            
        }//end
        catch (IOException ioe) 
        {
            logger.error("unable to get instance of DataAccessUtil", ioe);
            throw new DataAccessException("Unable to access datastore");
                                                    
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("unable to get instance of DataAccessUtil", pce);
            throw new DataAccessException("Unable to access datastore");
            
        }
        catch (SAXException saxe) 
        {
            logger.error("unable to get instance of DataAccessUtil", saxe);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch
        
        if (dataAccessUtil == null) 
        {
            logger.error("DataAccessUtil is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        try 
        {
            return dataAccessUtil.execute(dataRequest);
        }
        catch (IOException ioe) 
        {
            logger.error("unable to execute DataRequest", ioe);
            throw new DataAccessException("Unable to access datastore");
            
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("unable to execute DataRequest", pce);
            throw new DataAccessException("Unable to access datastore");
            
        }
        catch (SQLException sqle) 
        {
            logger.error("unable to execute DataRequest", sqle);
            throw new DataAccessException("Unable to access datastore");
            
        }
    
    }//end method executeDataQuery()
    
    
    /*
     * 
     */
    public Object executeDataQuery(String storedProcedureName) 
                                                    throws DataAccessException
    {
        return this.executeDataQuery(storedProcedureName, new HashMap());
        
    }//end method
    
    
    /*
     * 
     */
    private Object executeDataQuery(String storedProcedureName,
                                    HashMap parameters) 
                                                    throws DataAccessException
    {
        DataRequest dataRequest = null;
        DataAccessUtil dataAccessUtil = null;
        
        dataRequest = this.getDataRequest();
        
        if (dataRequest == null) 
        {
            logger.error("DataRequest is null");
            throw new DataAccessException("Unable to access datastore");
            
        }//end if
        
        dataRequest.reset();
        dataRequest.setStatementID(storedProcedureName);
        dataRequest.setInputParams(parameters);
        
        try 
        {
            return this.executeDataQuery(dataRequest);
        }
        catch (Exception e) 
        {
            logger.error("unable to execute DataRequest", e);
            throw new DataAccessException("Unable to access datastore");
            
        }
        finally 
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                try
                {
                    dataRequest.getConnection().close();
                }//end try
                catch (SQLException sqle) 
                {
                    logger.error("Exception thrown while closing DataRequest "
                                 + "connection.", sqle);
                    throw new DataAccessException("Unable to process "
                                                        + "datastore record.");
                    
                }//end catch
                
            }//end if ()     
            
        }//end finally
    
    }//end method executeDataQuery()
    
    
    /*
     * 
     */
    private DataRequest getDataRequest() throws DataAccessException
    {
        try
        {
            return DataRequestHelper.getInstance().getDataRequest();
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to get data request helper.", e);
            throw new DataAccessException("Unable to access datastore");
        }//end catch(Exception)

    }//end method
    
    /*
     * 
     */
    private Date getDate(String value, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date date = null;
        
        if (value != null) 
        {
            date = formatter.parse(value);
            
        }//end method
        
        return date;
    }//end method


    /*
     * 
     */
    private String getProperty(String configFile, String property)
                                                    throws DataAccessException
    {
        try
        {
            return ConfigurationUtil.getInstance().getProperty(configFile, 
                                                               property);
        }//end catch ()
        catch (ParserConfigurationException pce) 
        {
            logger.error("Unable to parse configuration.", pce);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (ParserConfigurationException)
        catch (SAXException saxe) 
        {
            logger.error("Unable to SAX.", saxe);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (SAXException)
        catch (IOException ioe) 
        {
            logger.error("Unable to IO.", ioe);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (IOException)
        catch (TransformerException te) 
        {
            logger.error("Unable to Transform.", te);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (TransformerException)
        
    }//end method

    /*
     * 
     */
    private Timestamp getTimestamp(java.util.Date value)
    {
        java.sql.Timestamp sqlDate = null;
        
        if(value != null)
        {
            sqlDate = new java.sql.Timestamp(value.getTime());
        }//end if
        
        return sqlDate;
        
    }//end method
    
    
    /*
     * 
     */
    private UserTransaction getUserTransaction() throws DataAccessException 
    {
        InitialContext context = null;
        String jndiTransactionEntry = null;
        
        // Retrieve a user transaction
        try 
        {
            context = new InitialContext();
        }//end try
        catch (NamingException ne)
        {
            logger.error("unable to obtain InitialContext.", ne);
            throw new DataAccessException("Unable to access context.");
            
        }//end catch ()
        
        try
        {
            jndiTransactionEntry = ConfigurationUtil.getInstance().
                    getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.JNDI_USERTRANSACTION_ENTRY);
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Unable to parse configuration.", pce);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (ParserConfigurationException)
        catch (SAXException saxe) 
        {
            logger.error("Unable to SAX.", saxe);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (SAXException)
        catch (IOException ioe) 
        {
            logger.error("Unable to IO.", ioe);
            
            throw new DataAccessException("Unable to access configuration.");
        }//end catch (IOException)
        catch (TransformerException te) 
        {
            logger.error("Unable to Transform.", te);
            throw new DataAccessException("Unable to access configuration.");
            
        }//end catch (TransformerException)
        
        try
        {
            return (UserTransaction)context.lookup(jndiTransactionEntry);
        }//end try
        catch (NamingException ne) 
        {
            logger.error("Unable to Name.", ne);
            throw new DataAccessException("Unable to access transaction.");
            
        }//end catch (NamingException)
        
    }//end method getUserTransaction()
    
    
    /*
     * 
     */
    private int getUserTransactionTimeout() throws DataAccessException
    {
        return Integer.parseInt(this.getProperty(
                        ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                        ConfigurationConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
        
    }//end method
    
}//end class
