/**
 * 
 */
package com.ftd.applications.lmd.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.applications.lmd.constants.StoreFeedConstants;
import com.ftd.applications.lmd.vo.PriceCodeFeedVO;
import com.ftd.applications.lmd.vo.PriceHeaderVO;
import com.ftd.applications.lmd.vo.ShippingKeyFeed;
import com.ftd.applications.lmd.vo.ShippingKeyFeed.ShippingKeyDetails.ShippingKeyDetail;
import com.ftd.applications.lmd.vo.ShippingKeyFeed.ShippingKeyDetails.ShippingKeyDetail.ShippingMethodCost;
import com.ftd.applications.lmd.vo.SourceFeed;
import com.ftd.applications.lmd.vo.SourceFeed.SourcePrograms.Program;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 * 
 */
public class StoreFeedsDAO {

	private Connection conn;
	private static Logger logger;

	public StoreFeedsDAO(Connection conn) {
		logger = new Logger(StoreFeedsDAO.class.getName());
		this.conn = conn;
	}

	/**
	 * Gets the default source code detail.
	 * 
	 * @param sourceCode
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws Exception
	 */
	public SourceFeed getSourceCodeDetail(String sourceCode)
			throws IOException, ParserConfigurationException, SAXException, Exception {
		if(logger.isDebugEnabled()) {
			logger.info("**********StoreFeedsDAO.getSourceCodeDetail()*************");
		}

		Map<String, String> inputParams = new HashMap<String, String>();
		inputParams.put(StoreFeedConstants.IN_SOURCE_CODE, sourceCode);
		SourceFeed sourceVO = null;
		try {
			CachedResultSet rs = (CachedResultSet) submitDBRequest(conn, StoreFeedConstants.GET_SOURCE_CODE_DETAIL, inputParams);
			
			// source master data, thought multiple rows are returned we can consider on first row.
			if (rs.next()) {				
				sourceVO = new SourceFeed();				
				populateSourceCodeDetail(rs, sourceVO);
			}
			
			//  source program and email programs, every source code has set of source programs, each source program has set of email program Ids.
			rs.reset();
			sourceVO.getSourcePrograms().getProgram().addAll(getSourceProgramRef(rs));

			sourceVO.getApeBaseSourceCodes().getApeSourceCode().addAll(getApeBaseSourceCodes(sourceCode));
			sourceVO.getMpMemberLevel().getMpMemberLevelId().addAll(getMpMemberLevelInfo(sourceCode));
			populatePartnerDetils(sourceVO,sourceCode);
						
		} catch (Exception e) {
			String errorMsg = "StoreFeedDAO.getSourceCodeDetail: Could not retrieve source code detail from database for source code: "	+ sourceCode;
			logger.error(errorMsg, e);
			throw new Exception(errorMsg, e);
		}
		return sourceVO;
	}
	
	private void populatePartnerDetils(SourceFeed sourceVO, String sourceCode) throws SAXException, ParserConfigurationException, IOException, SQLException {
		
		Map<String, String> inputParams = new HashMap<String, String>();
		inputParams.put(StoreFeedConstants.IN_SOURCE_CODE, sourceCode);
		CachedResultSet cs = (CachedResultSet) submitDBRequest(conn, StoreFeedConstants.GET_PARTNER_MAPPING_DETAILS, inputParams);
		if(cs.next())
		{
			sourceVO.setPartnerId(cs.getString("PARTNER_ID"));
			sourceVO.setPartnerName(cs.getString("PARTNER_NAME"));
		}
	}

	private Collection<? extends Program> getSourceProgramRef(CachedResultSet result) {
		Map<String,Program> programs = new HashMap<String, SourceFeed.SourcePrograms.Program>();
		while (result.next()) {
			Program programRefVO = null;
			if(!programs.containsKey(result.getString("SOURCE_PROGRAM_REF_ID"))) {
				programRefVO = new Program();
				programRefVO.setProgramRefId(result.getString("SOURCE_PROGRAM_REF_ID"));
				programRefVO.setProgramName(result.getString("PROGRAM_NAME"));
				programRefVO.setStartDate(result.getString("START_DATE"));
				programs.put(result.getString("SOURCE_PROGRAM_REF_ID"), programRefVO);
			} else {
				programRefVO = programs.get(result.getString("SOURCE_PROGRAM_REF_ID"));
			}
			
			if(!programRefVO.getEmailProgram().getEmailProgramId().contains(result.getString("EMAIL_PROGRAM_ID"))) {
				programRefVO.getEmailProgram().getEmailProgramId().add(result.getString("EMAIL_PROGRAM_ID"));			
			}
		}
		
		return programs.values();
	}
	
	/**Get APE base source codes associated with the given source code.
	 * @param sourceCode
	 * @return
	 * @throws Exception
	 */
	public Collection<? extends String> getApeBaseSourceCodes(String sourceCode) throws Exception {   
    	if(logger.isDebugEnabled()) {
			logger.info("**********StoreFeedsDAO.getApeBaseSourceCodes()*************");
		}
	    Map<String, String> inputParams = new HashMap<String, String>();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    ArrayList<String> baseSourceCodesList = new ArrayList<String>();
	    try {
	      CachedResultSet result = (CachedResultSet) submitDBRequest(conn, StoreFeedConstants.GET_APE_BASE_SOURCE_CODES,  inputParams);
	      
	      while(result!=null && result.next()) {
	    	  baseSourceCodesList.add(result.getString("BASE_SOURCE_CODE"));
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "StoreFeedDAO.getApeBaseSourceCodes: Could not retrieve APE base source codes from database for source code: " + sourceCode;
	        logger.error(errorMsg, e);
	        throw new Exception(errorMsg, e);
	    }	
	    return baseSourceCodesList;
    }

	/** Get Miles point Member Level Info for a given source code.
	 * @param sourceCode
	 * @return
	 * @throws Exception
	 */
	public Collection<? extends String> getMpMemberLevelInfo(String sourceCode) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.info("**********StoreFeedsDAO.getMpMemberLevelInfo()*************");
		}
		Map<String, String> inputParams = new HashMap<String, String>();
	    inputParams.put(StoreFeedConstants.IN_SOURCE_CODE, sourceCode);
	    ArrayList<String> mpMemberLevelList = new ArrayList<String>();
	    try {
	      CachedResultSet result = (CachedResultSet) submitDBRequest(conn, StoreFeedConstants.GET_SRC_MP_MEMBER_LVL_INFO,  inputParams);
	      
	      while(result!=null && result.next()) {
	    	  mpMemberLevelList.add(result.getString("MP_MEMBER_LEVEL_ID"));
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "StoreFeedDAO.getMpMemberLevelInfo: Could not retrieve MPMemberLevel list from database for source code: " + sourceCode;
	        logger.error(errorMsg, e);
	        throw new Exception(errorMsg, e);
	    }	
	    return mpMemberLevelList;
	}

	/** Submit DB request for given details.
	 * @param conn
	 * @param statementId
	 * @param inputParams
	 * @return
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SQLException
	 */
	@SuppressWarnings("rawtypes")
	private Object submitDBRequest(Connection conn, String statementId,
			Map inputParams) throws SAXException, ParserConfigurationException,	IOException, SQLException {
		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setInputParams(inputParams);
		request.setStatementID(statementId);
		DataAccessUtil dau = DataAccessUtil.getInstance();
		return dau.execute(request);
	}
	
	/** Populates Source feed using cursor returned.
	 * @param result
	 * @param sourceVO
	 */
	private static void populateSourceCodeDetail(CachedResultSet result, SourceFeed sourceVO) {		
		sourceVO.setSourceCode(result.getString("SOURCE_CODE")); 
		sourceVO.setDescription(result.getString("DESCRIPTION"));                                                                      
		sourceVO.setSnhId(result.getString("SNH_ID")); 
		sourceVO.setPriceHeaderId(result.getString("PRICE_HEADER_ID")); 
		sourceVO.setSendToScrub(result.getString("SEND_TO_SCRUB")); 
		sourceVO.setFraudFlag(result.getString("FRAUD_FLAG")); 
		sourceVO.setEnableLpProcessing(result.getString("FRAUD_FLAG"));
		sourceVO.setEmergencyTextFlag(result.getString("EMERGENCY_TEXT_FLAG")); 
		sourceVO.setRequiresDeliveryConfirmation(result.getString("REQUIRES_DELIVERY_CONFIRMATION")); 
		if(result.getDate("UPDATED_ON") != null) {
			sourceVO.setUpdatedOnEnd(result.getString("UPDATED_ON"));
		}
		sourceVO.setSourceType(result.getString("SOURCE_TYPE"));
		sourceVO.setStartDate(result.getString("START_DATE")); 
		sourceVO.setEndDate(result.getString("END_DATE"));  
		sourceVO.setPaymentMethodId(result.getString("PAYMENT_METHOD_ID"));
		sourceVO.setDefaultSourceCodeFlag(result.getString("DEFAULT_SOURCE_CODE_FLAG")); 
		sourceVO.setHighlightDescriptionFlag(result.getString("HIGHLIGHT_DESCRIPTION_FLAG")); 
		sourceVO.setDiscountAllowedFlag(result.getString("DISCOUNT_ALLOWED_FLAG")); 
		sourceVO.setBinNumberCheckFlag(result.getString("BIN_NUMBER_CHECK_FLAG")); 
		sourceVO.setOrderSource(result.getString("ORDER_SOURCE")); 
		sourceVO.setCompanyId(result.getString("COMPANY_ID"));   
		sourceVO.setPromotionCode(result.getString("PROMOTION_CODE")); 
		sourceVO.setBonusPromotionCode(result.getString("BONUS_PROMOTION_CODE")); 
		sourceVO.setRequestedBy(result.getString("REQUESTED_BY")); 
		sourceVO.setRelatedSourceCode(result.getString("RELATED_SOURCE_CODE")); 
		sourceVO.setProgramWebsiteUrl(result.getString("PROGRAM_WEBSITE_URL"));
		sourceVO.setCommentText(result.getString("COMMENT_TEXT"));           
		sourceVO.setPartnerBankId(result.getString("PARTNER_BANK_ID"));        
		sourceVO.setWebloyaltyFlag(result.getString("WEBLOYALTY_FLAG")); 
		sourceVO.setIotwFlag(result.getString("IOTW_FLAG"));
		sourceVO.setInvoicePassword(result.getString("INVOICE_PASSWORD"));
		sourceVO.setMpRedemptionRateId(result.getString("MP_REDEMPTION_RATE_ID")); 
		sourceVO.setAddOnFreeId(result.getString("ADD_ON_FREE_ID")); 
		sourceVO.setDisplayServiceFeeCode(result.getString("DISPLAY_SERVICE_FEE_CODE"));
		sourceVO.setDisplayShippingFeeCode(result.getString("DISPLAY_SHIPPING_FEE_CODE"));
		sourceVO.setRandomWeightedFlag(result.getString("PRIMARY_BACKUP_RWD_FLAG"));     
		sourceVO.setSurchargeDescription(result.getString("SURCHARGE_DESCRIPTION"));
		sourceVO.setDisplaySurcharge(result.getString("DISPLAY_SURCHARGE_FLAG")); 
		sourceVO.setSurchargeAmount(result.getDouble("SURCHARGE_AMOUNT"));       
		sourceVO.setApplySurchargeCode(result.getString("APPLY_SURCHARGE_CODE")); 
		sourceVO.setAllowFreeShippingFlag(result.getString("ALLOW_FREE_SHIPPING_FLAG")); 
		sourceVO.setSameDayUpcharge(result.getString("SAME_DAY_UPCHARGE")); 
		sourceVO.setDisplaySameDayUpcharge(result.getString("DISPLAY_SAME_DAY_UPCHARGE")); 
		sourceVO.setSameDayUpchargeFS(result.getString("SAME_DAY_UPCHARGE_FS"));
		sourceVO.setMorningDeliveryFlag(result.getString("MORNING_DELIVERY_FLAG")); 
		sourceVO.setMorningDeliveryToFSMembers(result.getString("MORNING_DELIVERY_FREE_SHIPPING")); 
		sourceVO.setDeliveryFeeId(result.getString("DELIVERY_FEE_ID")); 
		sourceVO.setAutomatedPromotionEngineFlag(result.getString("AUTO_PROMOTION_ENGINE"));		
		sourceVO.setOscarSelectionEnabledFlag(result.getString("OSCAR_SELECTION_ENABLED_FLAG"));		
		sourceVO.setGiftCertificateFlag(result.getString("GIFT_CERTIFICATE_FLAG"));
		sourceVO.setBillingInfoFlag(result.getString("BILLING_INFO_FLAG"));
		sourceVO.setEnableLpProcessing(result.getString("ENABLE_LP_PROCESSING"));
		sourceVO.setCalculateTaxFlag(result.getString("CALCULATE_TAX_FLAG"));
		sourceVO.setCustomShippingCarrier(result.getString("CUSTOM_SHIPPING_CARRIER"));
	}
	
   	/** return a ShippingKeyVO for a given shippingID	  
	 * @param conn database connection
	 * @param shippingID the selected id for the shipping key.
	 * @return ShippingKeyVO
	 * @throws Exception
	 **/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ShippingKeyFeed getShippingKeyDetail(String shippingKeyID, String action) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.info("**********StoreFeedsDAO.getShippingKeyDetail()*************");
		}
		
		ShippingKeyFeed shippingKeyFeed = new ShippingKeyFeed();
		DataRequest dataRequest = new DataRequest();
		
		CachedResultSet shippingKeyResult = null;
		CachedResultSet shippingDetailResult = null;

		try {
			HashMap output = new HashMap();
			HashMap inputParams = new HashMap();
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			
			dataRequest.setConnection(conn);					
			inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyID);
			dataRequest.setInputParams(inputParams);

			// Get Shipping Key info
			dataRequest.setStatementID(StoreFeedConstants.GET_SHIPPING_KEYS);	
			output = (HashMap) dataAccessUtil.execute(dataRequest);
			
			String status = (String) output.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {
				throw new Exception("StoreFeedsDAO.getShippingKey: " + (String) output.get("OUT_MESSAGE"));
			}

			shippingKeyResult = (CachedResultSet) output.get("OUT_CUR");
			if (shippingKeyResult.next()) {
				shippingKeyFeed.setShippingKeyId(shippingKeyResult.getString("shipping_key_id"));
				shippingKeyFeed.setDescription(shippingKeyResult.getString("shipping_key_description"));
				shippingKeyFeed.setShipper(shippingKeyResult.getString("shipper"));
				shippingKeyFeed.setAction(action);
			}

			// Get Shipping key details
			inputParams = new HashMap();			
			dataRequest.reset();
			dataRequest.setConnection(conn);
			
			inputParams.put("IN_SHIPPING_KEY_ID", shippingKeyID);
			dataRequest.setInputParams(inputParams);
			
			dataRequest.setStatementID(StoreFeedConstants.GET_SHIPPING_KEY_DETAIL_COST);			
			output = (HashMap) dataAccessUtil.execute(dataRequest);
			
			status = (String) output.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {
				throw new Exception("getShippingKeyDetail.GET_SHIPPING_KEY_DETAILS: " + (String) output.get("OUT_MESSAGE"));
			}
			shippingDetailResult = (CachedResultSet) output.get("OUT_CUR");

			Map<String, ShippingKeyDetail> shippingKeyDetailMap = new HashMap<String, ShippingKeyDetail>();
			
			while (shippingDetailResult.next()) {
				ShippingKeyDetail shippingkeyDetail;
				
				if(shippingKeyDetailMap.containsKey(shippingDetailResult.getString("shipping_detail_id"))) {
					shippingkeyDetail = shippingKeyDetailMap.get(shippingDetailResult.getString("shipping_detail_id"));
				} else {
					shippingkeyDetail = new ShippingKeyDetail();
					shippingkeyDetail.setMinPrice(new Double(shippingDetailResult.getDouble("min_price")));
					shippingkeyDetail.setMaxPrice(new Double(shippingDetailResult.getDouble("max_price")));
					shippingKeyDetailMap.put(shippingDetailResult.getString("shipping_detail_id"), shippingkeyDetail);
				}
				ShippingMethodCost shippingMethodCost = new ShippingMethodCost();
				shippingMethodCost.setShippingMethodId(shippingDetailResult.getString("shipping_method_id"));
				shippingMethodCost.setShippingCost(shippingDetailResult.getDouble("shipping_cost"));
				
				shippingkeyDetail.getShippingMethodCost().add(shippingMethodCost);
			}
			
			shippingKeyFeed.getShippingKeyDetails().getShippingKeyDetail().addAll(shippingKeyDetailMap.values());
			
			return shippingKeyFeed;
		
		} catch (Exception e) {
			logger.error("Error caught in getShippingKeyDetail()" + e);
			throw new Exception("Unable to get the shipping detail", e);
		}
	}

	/**
	 * @param priceCode
	 * @return
	 * @throws Exception
	 */
	public PriceCodeFeedVO getPriceCodeDetail(String priceCode) throws Exception {

		if(logger.isDebugEnabled()) {
			logger.info("**********StoreFeedsDAO.getPriceCodeDetail()*************");
		}
		Map<String, String> inputParams = new HashMap<String, String>();
		inputParams.put(StoreFeedConstants.IN_PRICE_HEADER_ID, priceCode);
		PriceCodeFeedVO priceCodefeed = new PriceCodeFeedVO();
		BigDecimal zeroAmt = new BigDecimal(0);
		PriceHeaderVO leastHeaderVO = null;
		BigDecimal currentDiscAmt = null;
        BigDecimal smallestDiscAmt = null;
		
		try {
			CachedResultSet rs = (CachedResultSet) submitDBRequest(conn, StoreFeedConstants.GET_PRICING_CODE_DETAILS, inputParams);
			String description = null;
			// Find the price header with least discount
			while(rs.next())
			{
				PriceHeaderVO currentHeaderVO = new PriceHeaderVO();
				currentHeaderVO.setDiscountAmt(rs.getString("discount_amt"));
				currentHeaderVO.setDiscountType(rs.getString("discount_type"));
				currentHeaderVO.setPriceHeaderId(rs.getString("price_header_id"));
				currentDiscAmt = new BigDecimal(currentHeaderVO.getDiscountAmt());
				if(leastHeaderVO != null) {
					smallestDiscAmt = new BigDecimal(leastHeaderVO.getDiscountAmt());
	                if(currentDiscAmt.compareTo(smallestDiscAmt) < 0) {
	                	leastHeaderVO = currentHeaderVO;      
	                }
					
				} else if(currentDiscAmt.compareTo(zeroAmt) >= 0) {
					leastHeaderVO = currentHeaderVO;
				}				
				description = rs.getString("description");
			}
		
			
			if(leastHeaderVO == null) {
				logger.debug("The discount is not valid. The disocunt amount may be null. No need to send the detail.");
				return null;
			}
			setStartDate(priceCodefeed);
			setEndDate(priceCodefeed);	
			priceCodefeed.setPriceHeaderId(leastHeaderVO.getPriceHeaderId());
			priceCodefeed.setMinDiscountAmt(Double.parseDouble(leastHeaderVO.getDiscountAmt()));
			priceCodefeed.setDiscountType(leastHeaderVO.getDiscountType());
			priceCodefeed.setDescription(description);
		
						
		} catch (Exception e) {
			String errorMsg = "StoreFeedDAO.getPriceCodeDetail: Could not retrieve price code detail from database for PRICE code: "	+ priceCode;
			logger.error(errorMsg, e);
			throw new Exception(errorMsg, e);
		}
		return priceCodefeed;
		
	}

	
	/**
	 * Sets the end date to 31- dec 2050
	 * @param priceCodefeed
	 */
	private void setEndDate(PriceCodeFeedVO priceCodefeed) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar endDate = Calendar.getInstance();
		endDate.set(Calendar.YEAR,2050);
		endDate.set(Calendar.MONTH,11);
		endDate.set(Calendar.DAY_OF_MONTH,31);
		priceCodefeed.setEndDate(sdf.format(endDate.getTime()));	
		
	}

	/**
	 * sets the start date to first day of the year.
	 * @param priceCodefeed
	 */
	private void setStartDate(PriceCodeFeedVO priceCodefeed) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar startDate = Calendar.getInstance();
		startDate.set(Calendar.YEAR,startDate.get(Calendar.YEAR));
		startDate.set(Calendar.MONTH,0);
		startDate.set(Calendar.DAY_OF_MONTH,1);
		priceCodefeed.setStartDate(sdf.format(startDate.getTime()));	
	}
}
