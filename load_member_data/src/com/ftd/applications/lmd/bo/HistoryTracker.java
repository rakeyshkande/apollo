package com.ftd.applications.lmd.bo;

import com.ftd.applications.lmd.vo.*;
import com.ftd.ftdutilities.FieldUtils;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * This object is used to track the history of the FloristVO.  This object
 * contains method used to determine what properties of the VO have changed (if
 * any)
 *
 * @author Ed Mueller
 */
public class HistoryTracker
{

  //constants
  public static final String NO_CHANGE = "NO CHANGE";
  public static final String NEW_VALUE = "NEW";
  public static final String CHANGED_VALUE = "CHANGED";
  public static final String DELETED_VALUE = "DELETED";

  // dispostion constants
  public static final String PROFILE = "Profile";
  public static final String ZIP_CODES = "Zip Codes";
  public static final String CODIFICATION = "Codification";
  public static final String WEIGHTS = "Weight";

  /**
   * public constructor
   */
  public HistoryTracker()
  {
  }

  /**
   * This makes takes in two florist VOs and returns a list contain all the
   * difference between the two VOs.
   *
   * @param oldVO Old\Existing version of Florist VO
   * @param newVO New version of Florist VO
   * @returns List of HistorVO objects
   */
  public List getMaintenanceHistory(FloristVO oldVO,FloristVO newVO)
  {
    //List containing changes
    List changes = new ArrayList();

    //Check fields in the Florist VO for changes
    checkField(changes,"Name",PROFILE,oldVO.getName(),newVO.getName());
    checkField(changes,"Address",PROFILE,oldVO.getAddress(),newVO.getAddress());
    checkField(changes,"City",PROFILE,oldVO.getCity(),newVO.getCity());
    checkField(changes,"State",PROFILE,oldVO.getState(),newVO.getState());
    checkField(changes,"Postal Code",PROFILE,oldVO.getPostalCode(),newVO.getPostalCode());
    checkField(changes,"Phone",PROFILE,FieldUtils.stripNonNumeric(oldVO.getPhone()),FieldUtils.stripNonNumeric(newVO.getPhone()));
    checkField(changes,"Alt Phone",PROFILE,FieldUtils.stripNonNumeric(oldVO.getAltPhone()),FieldUtils.stripNonNumeric(newVO.getAltPhone()));
    checkField(changes,"Fax",PROFILE,oldVO.getFax(),newVO.getFax());
    checkField(changes,"Email Address",PROFILE,oldVO.getEmailAddress(),newVO.getEmailAddress());
    checkField(changes,"Mercury Machine",CODIFICATION,oldVO.getMercuryMachine(),newVO.getMercuryMachine());
    //checkField(changes,"Block Type",PROFILE,oldVO.getFloristBlock().getBlockType(),newVO.getFloristBlock().getBlockType());
    //checkField(changes,"Initial Weight",WEIGHTS,oldVO.getFloristWeight().getInitialWeight(),newVO.getFloristWeight().getInitialWeight());
    //checkField(changes,"Final Weight",WEIGHTS,oldVO.getFloristWeight().getFinalWeight(),newVO.getFloristWeight().getFinalWeight());
    //checkField(changes,"Adjusted Weight",WEIGHTS,oldVO.getFloristWeight().getAdjustedWeight(),newVO.getFloristWeight().getAdjustedWeight());
    //checkField(changes,"Adjusted Weight Start Date",WEIGHTS,oldVO.getFloristWeight().getAdjustedWeightStartDate(),newVO.getFloristWeight().getAdjustedWeightStartDate());
    //checkField(changes,"Adjusted Weight End Date",WEIGHTS,oldVO.getFloristWeight().getAdjustedWeightEndDate(),newVO.getFloristWeight().getAdjustedWeightEndDate());
    //checkField(changes,"Adjusted Weight Perm Flag",WEIGHTS,oldVO.getFloristWeight().getAdjustedWeightPermFlag(),newVO.getFloristWeight().getAdjustedWeightPermFlag());

    //check postal code list for changes
    checkMaintenancePostalCodeChanges(changes,oldVO,newVO);

    //check codified produst list for changes
    checkMaintenanceCodifiedProductChanges(changes,oldVO,newVO);

    //checkBlockChanges(changes, oldVO, newVO);


    //return list of changes
    return changes;
  }

  public List getViewQueueHistory(ViewQueueVO oldVO, ViewQueueVO newVO)
  {
    //List containing changes
    List changes = new ArrayList();

    //check postal code list for changes
    checkViewQueuePostalCodeChanges(changes,oldVO,newVO);

    //check codified produst list for changes
    checkViewQueueCodifiedProductChanges(changes,oldVO,newVO);

    //return list of changes
    return changes;
  }

  /*
   * This method check the list of Codified Products for changes.  It will check
   * for items added and removed from the list.  It will also check if any properties
   * of objects within the list have changed.
   *
   * @param List of changes
   * @param FloristVO Old version of florist VO
   * @param FloristVO New version of florist VO
   */
  private void checkMaintenanceCodifiedProductChanges(List changes, FloristVO oldVO, FloristVO newVO)
  {

     Map newProductMap = newVO.getCodifiedProductMap();
     Map oldProductMap = oldVO.getCodifiedProductMap();

     //take care of null maps
     if(newProductMap == null)
     {
       newProductMap = new HashMap();
     }
     if(oldProductMap == null)
     {
       oldProductMap = new HashMap();
     }

    //check for deleted and changed Products
    Iterator i = oldProductMap.entrySet().iterator();

    //loop through items in old product map
    while(i.hasNext()){
        //get Old VO
        Map.Entry e = (Map.Entry) i.next();
        CodifiedProductVO oldProduct = (CodifiedProductVO)oldProductMap.get(e.getKey());

        //get New VO
        CodifiedProductVO newProduct = (CodifiedProductVO)newProductMap.get(oldProduct.getId());

        //if product not found in new VO then it has been deleted
        if(newProduct == null)
        {
          HistoryVO historyVO = new HistoryVO();
          historyVO.setField("Codified Product");
          historyVO.setChangeType(DELETED_VALUE);
          historyVO.setOldValue(oldProduct.getId());
          historyVO.setDispositionId(CODIFICATION);
          changes.add(historyVO);
        }
        //else check for changes in the VO
        else
        {
          checkMaintenanceAdditionialField(changes,"Codified Product","Name",CODIFICATION,oldProduct.getId(),oldProduct.getName(),newProduct.getName());
          checkMaintenanceAdditionialField(changes,"Codified Product","Category",CODIFICATION,oldProduct.getId(),oldProduct.getCategory(),newProduct.getCategory());
          // sp checkAdditionialField(changes,"Codified Product","Blocked Flag",CODIFICATION,oldProduct.getId(),oldProduct.getBlockedFlag(),newProduct.getBlockedFlag());
          checkMaintenanceAdditionialField(changes,"Codified Product","Block Start Date",CODIFICATION,oldProduct.getId(),oldProduct.getBlockStartDate(),newProduct.getBlockStartDate());
          checkMaintenanceAdditionialField(changes,"Codified Product","Block End Date",CODIFICATION,oldProduct.getId(),oldProduct.getBlockEndDate(),newProduct.getBlockEndDate());
        }//end else
    }//end while loop - check for changed and deleted codes

    //check for new and changed products
    i = newProductMap.entrySet().iterator();
    while(i.hasNext()){
        //get New VO
        Map.Entry e = (Map.Entry) i.next();
        CodifiedProductVO newProduct = (CodifiedProductVO)newProductMap.get(e.getKey());

        //get Old VO
        CodifiedProductVO oldProduct = (CodifiedProductVO)oldProductMap.get(newProduct.getId());

        //if Product not find in old VO then it has been added
        if(oldProduct == null)
        {
          HistoryVO historyVO = new HistoryVO();
          historyVO.setField("Codified Product");
          historyVO.setChangeType(NEW_VALUE);
          historyVO.setNewValue(newProduct.getId());
          historyVO.setDispositionId(CODIFICATION);
          changes.add(historyVO);
        }//end if
   }//end while loop - check for new codes

  }//end method


  /*
   * This method check the list of Codified Products for changes.  It will check
   * for items added and removed from the list.  It will also check if any properties
   * of objects within the list have changed.
   *
   * @param List of changes
   * @param FloristVO Old version of florist VO
   * @param FloristVO New version of florist VO
   */
  private void checkViewQueueCodifiedProductChanges(List changes, ViewQueueVO oldVO, ViewQueueVO newVO)
  {

     Map newProductMap = newVO.getCodifiedProductMap();
     Map oldProductMap = oldVO.getCodifiedProductMap();

     //take care of null maps
     if(newProductMap == null)
     {
       newProductMap = new HashMap();
     }
     if(oldProductMap == null)
     {
       oldProductMap = new HashMap();
     }

    //check for deleted and changed Products
    Iterator i = oldProductMap.entrySet().iterator();

    //loop through items in old product map
    while(i.hasNext())
    {
        //get Old VO
        Map.Entry e = (Map.Entry) i.next();
        CodifiedProductVO oldProduct = (CodifiedProductVO)oldProductMap.get(e.getKey());

        //get New VO
        CodifiedProductVO newProduct = (CodifiedProductVO)newProductMap.get(oldProduct.getId());

        //if product not found in new VO then it has been deleted
        if(newProduct == null)
        {
          HistoryVO historyVO = new HistoryVO();
          historyVO.setField("Codified Product");
          historyVO.setChangeType(DELETED_VALUE);
          historyVO.setOldValue(oldProduct.getId());
          historyVO.setDispositionId(CODIFICATION);
          changes.add(historyVO);
        }
        //else check for changes in the VO
        else
        {
          checkMaintenanceAdditionialField(changes,"Codified Product","Name",CODIFICATION,oldProduct.getId(),oldProduct.getName(),newProduct.getName());
          checkMaintenanceAdditionialField(changes,"Codified Product","Category",CODIFICATION,oldProduct.getId(),oldProduct.getCategory(),newProduct.getCategory());
          // sp checkAdditionialField(changes,"Codified Product","Blocked Flag",CODIFICATION,oldProduct.getId(),oldProduct.getBlockedFlag(),newProduct.getBlockedFlag());
          checkMaintenanceAdditionialField(changes,"Codified Product","Block Start Date",CODIFICATION,oldProduct.getId(),oldProduct.getBlockStartDate(),newProduct.getBlockStartDate());
          checkMaintenanceAdditionialField(changes,"Codified Product","Block End Date",CODIFICATION,oldProduct.getId(),oldProduct.getBlockEndDate(),newProduct.getBlockEndDate());
        }//end else
    }//end while loop - check for changed and deleted codes

  }//end method


  /*
   * This method check the list of Postal Codes for changes.  It will check
   * for items added and removed from the list.  It will also check if any properties
   * of objects within the list have changed.
   *
   * @param List of changes
   * @param FloristVO Old version of florist VO
   * @param FloristVO New version of florist VO
   */
  private void checkMaintenancePostalCodeChanges(List changes, FloristVO oldVO, FloristVO newVO)
  {

     Map newPostalMap = newVO.getPostalCodeMap();
     Map oldPostalMap = oldVO.getPostalCodeMap();

     //take care of null maps
     if(newPostalMap == null)
     {
       newPostalMap = new HashMap();
     }
     if(oldPostalMap == null)
     {
       oldPostalMap = new HashMap();
     }

    //check for deleted and changed postal codes
    Iterator i = oldVO.getPostalCodeMap().entrySet().iterator();

    //loop through postal code map
    while(i.hasNext()){
        //get Old VO
        Map.Entry e = (Map.Entry) i.next();
        PostalCodeVO oldPostalCode = (PostalCodeVO)oldPostalMap.get(e.getKey());

        //get New VO
        PostalCodeVO newPostalCode = (PostalCodeVO)newPostalMap.get(oldPostalCode.getPostalCode());

        //if postal not find in new VO then it has been delted
        if(newPostalCode == null)
        {
          HistoryVO historyVO = new HistoryVO();
          historyVO.setField("Postal Code");
          historyVO.setChangeType(DELETED_VALUE);
          historyVO.setOldValue(oldPostalCode.getPostalCode());
          historyVO.setDispositionId(ZIP_CODES);
          changes.add(historyVO);
        }
        //else check for changes in the VO
        else
        {
          checkMaintenanceAdditionialField(changes,"Postal Code","City",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getCity(),newPostalCode.getCity());
          checkMaintenanceAdditionialField(changes,"Postal Code","Cutoff",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getCutoffTime(),newPostalCode.getCutoffTime());
          checkMaintenanceAdditionialField(changes,"Postal Code","Florist Count",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getFloristCount(),newPostalCode.getFloristCount());
          // checkAdditionialField(changes,"Postal Code","Blocked Flag",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getBlockedFlag(),newPostalCode.getBlockedFlag());
          checkMaintenanceAdditionialField(changes,"Postal Code","Block Start Date",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getBlockStartDate(),newPostalCode.getBlockStartDate());
          checkMaintenanceAdditionialField(changes,"Postal Code","Block End Date",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getBlockEndDate(),newPostalCode.getBlockEndDate());
        }//end else
    }//end while loop - check for changed and deleted codes

    //check for new and changed postal codes
    i = newVO.getPostalCodeMap().entrySet().iterator();
    while(i.hasNext()){
        //get New VO
        Map.Entry e = (Map.Entry) i.next();
        PostalCodeVO newPostalCode = (PostalCodeVO)newPostalMap.get(e.getKey());

        //get Old VO
        PostalCodeVO oldPostalCode = (PostalCodeVO)oldPostalMap.get(newPostalCode.getPostalCode());

        //if postal not find in old VO then it has been added
        if(oldPostalCode == null)
        {
          HistoryVO historyVO = new HistoryVO();
          historyVO.setField("Postal Code");
          historyVO.setChangeType(NEW_VALUE);
          historyVO.setNewValue(newPostalCode.getPostalCode());
          historyVO.setDispositionId(ZIP_CODES);
          changes.add(historyVO);
        }//end if
   }//end while loop - check for new codes

  }

  /*
   * This method check the list of Postal Codes for changes.  It will check
   * for items added and removed from the list.  It will also check if any properties
   * of objects within the list have changed.
   *
   * @param List of changes
   * @param FloristVO Old version of florist VO
   * @param FloristVO New version of florist VO
   */
  private void checkViewQueuePostalCodeChanges(List changes, ViewQueueVO oldVO, ViewQueueVO newVO)
  {

     Map newPostalMap = newVO.getPostalCodeMap();
     Map oldPostalMap = oldVO.getPostalCodeMap();

     //take care of null maps
     if(newPostalMap == null)
     {
       newPostalMap = new HashMap();
     }
     if(oldPostalMap == null)
     {
       oldPostalMap = new HashMap();
     }

    //check for deleted and changed postal codes
    Iterator i = oldVO.getPostalCodeMap().entrySet().iterator();

    //loop through postal code map
    while(i.hasNext()){
        //get Old VO
        Map.Entry e = (Map.Entry) i.next();
        PostalCodeVO oldPostalCode = (PostalCodeVO)oldPostalMap.get(e.getKey());

        //get New VO
        PostalCodeVO newPostalCode = (PostalCodeVO)newPostalMap.get(oldPostalCode.getPostalCode());

        //if postal not find in new VO then it has been deleted
        if(newPostalCode == null)
        {
          HistoryVO historyVO = new HistoryVO();
          historyVO.setField("Postal Code");
          historyVO.setChangeType(DELETED_VALUE);
          historyVO.setOldValue(oldPostalCode.getPostalCode());
          historyVO.setDispositionId(ZIP_CODES);
          changes.add(historyVO);
        }
        //else check for changes in the VO
        else
        {
          checkMaintenanceAdditionialField(changes,"Postal Code","City",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getCity(),newPostalCode.getCity());
          checkMaintenanceAdditionialField(changes,"Postal Code","Cutoff",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getCutoffTime(),newPostalCode.getCutoffTime());
          checkMaintenanceAdditionialField(changes,"Postal Code","Florist Count",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getFloristCount(),newPostalCode.getFloristCount());
          // checkAdditionialField(changes,"Postal Code","Blocked Flag",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getBlockedFlag(),newPostalCode.getBlockedFlag());
          checkMaintenanceAdditionialField(changes,"Postal Code","Block Start Date",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getBlockStartDate(),newPostalCode.getBlockStartDate());
          checkMaintenanceAdditionialField(changes,"Postal Code","Block End Date",ZIP_CODES,oldPostalCode.getPostalCode(),oldPostalCode.getBlockEndDate(),newPostalCode.getBlockEndDate());
        }//end else
    }//end while loop - check for changed and deleted codes
  }

  /**
   * This method checks for changes to the blocks
   * @param changes
   * @param oldVO
   * @param newVO
   */
  private void checkBlockChanges(List changes, FloristVO oldVO, FloristVO newVO)
  {
    //checkField(changes,"Block Start Date",CODIFICATION,oldVO.getFloristBlock().getBlockStartDate(),newVO.getFloristBlock().getBlockStartDate());
    //checkField(changes,"Block End Date",CODIFICATION,oldVO.getFloristBlock().getBlockEndDate(),newVO.getFloristBlock().getBlockEndDate());

    List newBlockList = newVO.getFloristBlockList();
    List oldBlockList = oldVO.getFloristBlockList();

        //take care of null lists
    if(newBlockList == null)
    {
      newBlockList = new ArrayList();
    }
    if(oldBlockList == null)
    {
      oldBlockList = new ArrayList();
    }

        //check for deleted and changed Products
        Iterator i = oldBlockList.iterator();

        //loop through items in old block list
        while(i.hasNext())
        {
            FloristBlockVO oldBlockVO = (FloristBlockVO) i.next();

            //get New VO.  Loop throught he list looking for the one with the same block start date
            FloristBlockVO newBlockVO = null;
            for (int j=0; j < newBlockList.size();j++)
            {
              FloristBlockVO potentialNewBlock = (FloristBlockVO) newBlockList.get(j);
              if (potentialNewBlock.getBlockStartDate().equals(oldBlockVO.getBlockStartDate()))
              {
                newBlockVO = potentialNewBlock;
              }

            }

            //if product not found in new VO then it has been deleted
            if(newBlockVO == null)
            {
                HistoryVO historyVO = new HistoryVO();
                historyVO.setField("Block");
                historyVO.setChangeType(DELETED_VALUE);
                //TODO - fix historyVO
                historyVO.setOldValue(oldBlockVO.getBlockType());
                historyVO.setDispositionId(CODIFICATION);
                changes.add(historyVO);
            }
            //else check for changes in the VO
            else
            {
                checkMaintenanceAdditionialField(changes,"Block","Block Start Date",CODIFICATION,oldBlockVO.getBlockType(),oldBlockVO.getBlockStartDate(),newBlockVO.getBlockStartDate());
                checkMaintenanceAdditionialField(changes,"Block","Block End Date",CODIFICATION,oldBlockVO.getBlockType(),oldBlockVO.getBlockEndDate(),newBlockVO.getBlockEndDate());
            }//end else
        }//end while loop - check for changed and deleted codes

        //check for new and changed products
        i = newBlockList.iterator();
        while(i.hasNext()){
            //get New VO
            FloristBlockVO newBlockVO = (FloristBlockVO) i.next();

            //get New VO.  Loop throught he list looking for the one with the same block start date
            FloristBlockVO oldBlockVO = null;
            for (int j=0; j < oldBlockList.size();j++)
            {
                FloristBlockVO potentialOldBlock = (FloristBlockVO) oldBlockList.get(j);
                if (potentialOldBlock.getBlockStartDate().equals(newBlockVO.getBlockStartDate()))
                {
                    oldBlockVO = potentialOldBlock;
                }

            }

            //if Product not find in old VO then it has been added
            if(oldBlockVO == null)
            {
                HistoryVO historyVO = new HistoryVO();
                historyVO.setField("Block");
                historyVO.setChangeType(NEW_VALUE);
                //TODO - fix historyVO
                historyVO.setNewValue(newBlockVO.getBlockType());
                historyVO.setDispositionId(CODIFICATION);
                changes.add(historyVO);
            }//end if
        }//end while loop - check for new codes
    }//end method

  /*
   * This method will check if the Additionial Field has changed.  An additionial
   * field is considered any field that does NOT exist on the FloristVO, but it exists
   * in a List/Map found within the FloristVO.  For example all the fields in the
   * CodifiedProductVO are considered Additionial Fields because the Florist VO
   * contains a list of CodifiedProductVOs.
   *
   * @param List history of changes list
   * @String Additionial Field Name
   * @String Field Name
   * @String Additionial Data
   * @Object Object from old VO
   * @Object Object from new VO
   */
    private void checkMaintenanceAdditionialField(List historyList,String addFieldName,String fieldName,String disposition,String addData, Object oldValue, Object newValue)
    {
      //check if field changed
      String changeStatus = getChangeStatus(oldValue,newValue);
      if(!changeStatus.equals(NO_CHANGE))
      {
        HistoryVO historyVO = new HistoryVO();
        historyVO.setChangeType(changeStatus);
        historyVO.setOldValue(displayValue(oldValue));
        historyVO.setNewValue(displayValue(newValue));
        historyVO.setField(fieldName);
        historyVO.setDispositionId(disposition);
        historyVO.setAdditionialFieldName(addFieldName);
        historyVO.setAdditionialData(addData);
        historyList.add(historyVO);
      }
    }

  /*
   * This method will check if a field in the VO has changed.  This methods takes
   * in types of Objects so that the method will work with Strings, Longs, Integers,
   * and Dates.  If the field has changed then History info will be added to the past
   * in list.
   *
   * @parm List History of changes
   * @parm String The name of the field that is being checked
   * @param Object Old object from originally VO
   * @param Object New object from new VO
   */
    private void checkField(List historyList,String fieldName,String disposition,Object oldValue, Object newValue)
    {
      //check if field changed
      String changeStatus = getChangeStatus(oldValue,newValue);
      if(!changeStatus.equals(NO_CHANGE))
      {
        HistoryVO historyVO = new HistoryVO();
        historyVO.setChangeType(changeStatus);
        historyVO.setOldValue(displayValue(oldValue));
        historyVO.setNewValue(displayValue(newValue));
        historyVO.setField(fieldName);
        historyVO.setDispositionId(disposition);
        historyList.add(historyVO);
      }
    }


    /*
     * This method formats how the new value and old value
     * will be put into the VO.  This affects how the data
     * will be displayed to the user.
     *
     * @parm Object
     * @returns String representation of object, or empty string if the value is null
     */
      private String displayValue(Object obj)
      {
        String returnValue = null;

        if(obj != null)
        {
            // instance of didn't work here :)
            if(obj.getClass().getName().equals("java.util.Date"))
            {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
                returnValue = sdf.format((java.util.Date) obj);
            }
            else if(obj.getClass().getName().equals("java.sql.Date"))
            {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
                returnValue = sdf.format((java.sql.Date) obj);
            }
            else
            {
                returnValue = obj.toString();
            }
        }

        return returnValue;
      }


    /*
     * This method determines if an object value has changed.
     *
     * Note: This method uses the 'equals' method to determine if two objects
     * contain the same value.  This will work for the properties currently in
     * the VO (String, Integer, Long, Date) but it may not work if any new objects
     * are introducted which do not overwrite the 'equals' method.
     *
     * @param Object oldValue The Old Value
     * @param Object newValue The New Value
     * @String Text that indicates if the value has been changed, deleted, or created
     */
      private String getChangeStatus(Object oldValue, Object newValue)
      {
          String changeStatus = null;

          //if old an new value empty
          if(isEmpty(oldValue) && isEmpty(newValue))
          {
            changeStatus = NO_CHANGE;
          }
          //else, if new value added
          else if(isEmpty(oldValue) && !isEmpty(newValue))
          {
            changeStatus = NEW_VALUE;
          }
          else if(!isEmpty(oldValue) && isEmpty(newValue))
          {
            changeStatus = DELETED_VALUE;
          }
          else if(oldValue.equals(newValue))
          {
            changeStatus = NO_CHANGE;
          }
          else
          {
            changeStatus = CHANGED_VALUE;
          }

          return changeStatus;

      }

    /*
     * This method check is the passed in object is empty.
     * Empty is defined as being null or containing a toString length
     * of zero.
     *
     * @param Object value
     * @returns boolean true=empty
     */
    private boolean isEmpty(Object value)
    {
      return value == null || value.toString().length() == 0 ? true : false;
    }

  // THE FOLLOWING WAS USED FOR TESTING AND CAN BE MADE PART OF A JUNIT TEST IN THE FUTURE
 public static void main(String args[])
 {

   FloristVO voOld = new FloristVO();
   voOld.setAddress("111");   voOld.setName("ED");
   Map oldZipMap = new HashMap();
   PostalCodeVO po1 = new PostalCodeVO();
   po1.setPostalCode("11111");
   po1.setCity("here");
   po1.setCutoffTime("1");
   PostalCodeVO po2 = new PostalCodeVO();
   po2.setPostalCode("22222");
   po2.setCity("here");
   po2.setCutoffTime("2");
   oldZipMap.put(po1.getPostalCode(),po1);
   oldZipMap.put(po2.getPostalCode(),po2);
   voOld.setPostalCodeMap(oldZipMap);



   FloristVO voNew = new FloristVO();
   voNew.setAddress("222");
   voNew.setAltPhone("phone");
   voNew.setName(null);
   Map newZipMap = new HashMap();
   PostalCodeVO po3 = new PostalCodeVO();
   po3.setPostalCode("11111");
   po3.setCity("there");
   PostalCodeVO po4 = new PostalCodeVO();
   po4.setPostalCode("33333");
   po4.setCity("here");
   po4.setCutoffTime("3");
   newZipMap.put(po3.getPostalCode(),po3);
   newZipMap.put(po4.getPostalCode(),po4);
   voNew.setPostalCodeMap(newZipMap);

   HistoryTracker me = new HistoryTracker();
   List historyList = me.getMaintenanceHistory(voOld,voNew);

 }


}