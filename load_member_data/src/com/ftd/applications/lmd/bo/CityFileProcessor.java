package com.ftd.applications.lmd.bo;
import com.ftd.applications.lmd.util.FTPHelper;
import com.ftd.applications.lmd.util.FieldProcessor;
import com.ftd.applications.lmd.util.ParseConfig;
import com.ftd.applications.lmd.vo.ParseFieldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;



import org.xml.sax.SAXException;


/**
 * Uses the ParseConfig utility to pull the necessary data from the member, city,
 * and link files and set the data to value objects representing the file data.
 *
 * @author Anshu Gaind
 * @version $Id: CityFileProcessor.java,v 1.3 2011/06/30 15:14:14 gsergeycvs Exp $
 */

public class CityFileProcessor 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.CityFileProcessor");
  public CityFileProcessor()
  {
  }

  /**
   * Process city file
   *
   * @param fileName
   * @return
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.sql.SQLException
 * @throws XPathExpressionException 
   */
  public Map process(String fileName)
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, XPathExpressionException
  {
    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    HashMap map =new HashMap(50000, 0.9f);
    Map citiesFileConfigMap = ParseConfig.getInstance().getCitiesFileConfigMap();
    // load the link files from their local ftp location
    BufferedReader br = new BufferedReader(new FTPHelper().getFile("MemberData", fileName) );
    logger.debug("Processing " + fileName);

    try
    {

      String record, cityStateNumber, cityName;
      ParseFieldVO cityStateNumberFieldVO = (ParseFieldVO) citiesFileConfigMap.get("city-state-number")
                  ,cityNameFieldVO = (ParseFieldVO) citiesFileConfigMap.get("city-name");

      int ctr = 0;

      while ((record = br.readLine()) != null)
      {
        if ( record != null && (! record.equals("")) )
        {
          cityStateNumber = FieldProcessor.getFieldValue( cityStateNumberFieldVO, record);
          cityName = FieldProcessor.getFieldValue( cityNameFieldVO, record);
          map.put(cityStateNumber, cityName);

          if (++ctr%threshold == 0) logger.debug(ctr + " records processed");

        }
      }

    } finally
    {
      if (br != null)
      {
          br.close();
          logger.debug("Processed a total of " + map.size() + " records from " + fileName);
      }
    }

    return map;
  }

}
