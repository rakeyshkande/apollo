package com.ftd.applications.lmd.bo;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.ejb.MessageDispatcherSessionEJBDelegate;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.w3c.dom.Document;

import org.w3c.dom.NamedNodeMap;

/**
 * Loads florist zips data 
 */

public class LoadFloristZipsDataHandler extends EventHandler 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.LoadFloristZipsDataHandler");

  public LoadFloristZipsDataHandler()
  {
  }

  /**
   * Invoke stored proc to force removal of any zips (from florist_zips table) from 
   * florists that are no longer signed-up for those zips (based on EFOS city associations).
   *
   * @payload payload
   * @throws java.lang.Throwable
   */
  public void invoke(Object payload) throws Throwable
  {
    Document doc = null;
    
    try 
    {
        MessageToken messageToken = (MessageToken) payload;
        doc = (Document) DOMUtil.getDocument((String) messageToken.getMessage());

        FloristDAO floristDAO = new FloristDAO();
        floristDAO.deleteNonMemberFloristZips();
      
    } catch (Throwable t) 
    {
      // send the system message
      this.sendSystemMessage(t);
      throw t;
    } finally 
    {
    }
    
    
  }


    /**
     * Sends a system message
     * 
     * @param t
     */
    private void sendSystemMessage(Throwable t)
    {
      Connection con = null;
      try 
      {
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
        // write the stack trace to the log_message variable
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        String logMessage = "Failed to load florist zip data " + "\n" + sw.toString();
        pw.close();        
        
        SystemMessage.send(logMessage, "LOAD FLORIST DATA SERVICE", SystemMessage.LEVEL_PRODUCTION, "ERROR", con);
      } catch (Exception ex) 
      {
        logger.error(ex);
      } finally 
      {
        try 
        {
          if (con != null && (! con.isClosed() )) 
          {
              con.close();
          }
          
        } catch (Exception ex2) 
        {
          logger.error(ex2);
        }      
      }
      
    }
    

}
