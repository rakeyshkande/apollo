/**
 * 
 */
package com.ftd.applications.lmd.bo;

import java.io.StringWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.ftd.applications.lmd.constants.StoreFeedConstants;
import com.ftd.applications.lmd.dao.StoreFeedsDAO;
import com.ftd.applications.lmd.vo.PriceCodeFeedVO;
import com.ftd.applications.lmd.vo.ShippingKeyFeed;
import com.ftd.applications.lmd.vo.SourceFeed;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.StoreFeedUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class StoreFeedsBO {
	
	private static Logger logger;
	
	public StoreFeedsBO() {		
		logger = new Logger(StoreFeedsBO.class.getName());		
	}

	/**
	 * get the source code detail for a given source code populate into source
	 * feed marshal as XML store to Apollo table store_feeds
	 * 
	 * @param sourceCode
	 * @throws Exception
	 */
	public void processSourceFeed(String sourceCode) {
		if(logger.isDebugEnabled()) {
			logger.debug("**********StoreFeedsBO.saveSourceFeed() for source code, " + sourceCode + " ***************");
		}		
		Connection conn = null;		
		try {
			conn = getConnection();				
			SourceFeed sourceFeed =  new StoreFeedsDAO(conn).getSourceCodeDetail(sourceCode);			
			if(sourceFeed == null) {
				throw new Exception("Unable to populate source feed for source code, " + sourceCode);
			}			
			generateAndSaveStoreFeed(sourceFeed, StoreFeedConstants.SOURCE_FEED);			
		} catch (Exception e) {
			logger.error("Error caught processing SourceFeed - " + e.getMessage());
			StoreFeedUtil.sendSystemMessage("Error caught processing SourceFeed - " + e.getMessage());
		} finally {
			StoreFeedUtil.closeConnection(conn);
		}		
	}
	
	/**
	 * get the shipping key detail for a given shipping key id populate into
	 * Shipping key feed marshal as xml store to apollo table store_feeds
	 * 
	 * @param shippingKeyId
	 * @throws Exception
	 */
	public void processShippingKeyFeed(String msgText) throws Exception {
		
		String[] messageContent = msgText.split(StoreFeedConstants.EMPTY_STR);
		String shippingKeyId = messageContent[1] ;
		String action = "";
		if (messageContent.length > 2) {
			action = messageContent[2];
		} 

		if(logger.isDebugEnabled()) {
			logger.debug("**********StoreFeedsBO.saveShippingKeyFeed() for shipping key, " + shippingKeyId + " ***************");
		}		
		Connection conn = null;		
		try {
			conn = getConnection();	
			ShippingKeyFeed shippingKeyFeed;
			if(action != null && action.equalsIgnoreCase("Delete"))
			{
				shippingKeyFeed = new ShippingKeyFeed();
				shippingKeyFeed.setAction(action);
				shippingKeyFeed.setShippingKeyId(shippingKeyId);
			}
			else
			{
				shippingKeyFeed =  new StoreFeedsDAO(conn).getShippingKeyDetail(shippingKeyId,action);
			}

			if(shippingKeyFeed == null) {
				throw new Exception("Unable to create shippingKey feed forshippingKey Id, " + shippingKeyId);
			}
			
			generateAndSaveStoreFeed(shippingKeyFeed, StoreFeedConstants.SHIPPING_KEY_FEED);			
		} catch (Exception e) {	
			logger.error("Error caught processing shippingKey Feed - " + e.getMessage());
			StoreFeedUtil.sendSystemMessage("Error caught processing shippingKey feed - " + e.getMessage());
		} finally {
			StoreFeedUtil.closeConnection(conn);
		}		
	}
	
	/**Get the connection for the data source Load Member/ Order Scrub.
	 * @return
	 * @throws Exception
	 */
	private Connection getConnection() throws Exception {
		Connection connection = null;
		String useJDBCConnectionUtil = ConfigurationUtil.getInstance()
				.getProperty(StoreFeedConstants.LOAD_MEM_CONFIG_FILE, StoreFeedConstants.USE_JDBCCONNECTION_UTIL);
		connection = (useJDBCConnectionUtil != null && useJDBCConnectionUtil
				.equals("Y")) ? JDBCConnectionUtil.getInstance().getConnection(StoreFeedConstants.LOAD_MEM_DS) : 
					DataSourceUtil.getInstance().getConnection(StoreFeedConstants.ORD_SCRUB_DS);
		return connection;
	}
	
	/** Marshal the feed VO to XML String.
	 * save the store feed to Apollo DB.
	 * @param storeFeed
	 * @param feedType
	 * @throws Exception
	 */
	public static void generateAndSaveStoreFeed(Object storeFeed, String feedType) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("**********StoreFeedsBO.generateAndSaveStoreFeed() for feed type, " + feedType + " ***************");
		}
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		String feedId = new StoreFeedUtil().saveStoreFeed(marshalAsXML(storeFeed, props), feedType);
		
		if(feedId == null) {
			throw new Exception("Unable to save the feed of type, " + feedType);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("Succesfully saved the feed with id : " + feedId);
		}
	}
	
	/** Marshal the Object passed in as XML.
	 * @param object
	 * @return
	 * @throws Exception 
	 */
	public static String marshalAsXML(Object object, Map<String, Object> props) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("**********StoreFeedsBO.marshalAsXML() ***************");
		}
		String storeFeed = null;		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = jaxbContext.createMarshaller();
			if (props != null && props.size() > 0) {
				Set<String> keySet = props.keySet();
				for (String key : keySet) {
					marshaller.setProperty(key, props.get(key));
				}
			}
			StringWriter sw = new StringWriter();
			marshaller.marshal(object, sw);
			storeFeed = sw.toString();

			if (logger.isDebugEnabled()) {
				logger.debug("Store Feed: \n " + storeFeed);
			}

		} catch (JAXBException e) {
			logger.error("Error caught marshaling the XML, " + e.toString());
			throw new Exception(e);
		}
		return storeFeed;
	}

	public void processPriceCodeFeed(String priceCode) {
		if(logger.isDebugEnabled()) {
			logger.debug("**********StoreFeedsBO.processPriceCodeFeed() for price code, " + priceCode + " ***************");
		}		
		Connection conn = null;		
		try {
			conn = getConnection();				
			PriceCodeFeedVO priceCodeFeed =  new StoreFeedsDAO(conn).getPriceCodeDetail(priceCode);			
			if(priceCodeFeed == null) {
				throw new Exception("Unable to populate priceCode feed for price code, " + priceCode);
			}			
			generateAndSaveStoreFeed(priceCodeFeed, StoreFeedConstants.PRICE_CODE_FEED);			
		} catch (Exception e) {
			logger.error("Error caught processing priceCodeFeed - " + e.getMessage());
			StoreFeedUtil.sendSystemMessage("Error caught processing priceCodeFeed - " + e.getMessage());
		} finally {
			StoreFeedUtil.closeConnection(conn);
		}		
		
	}	
}
