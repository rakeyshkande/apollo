/**
 * com.ftd.applications.lmd.bo.OscarFileUploadBO.java
 */
package com.ftd.applications.lmd.bo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ftd.applications.lmd.dao.ZipCodeDAO;
import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.vo.OscarExportSpreadsheetRowVO;
import com.ftd.applications.lmd.vo.ZipCodeVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Business Object servicing OSCAR data requests
 * 
 * @author rlarson
 *
 */
public class OscarFileUploadBO 
{
	private static final String Y = "Y";
	private static final String N = "N";
	
	private Logger logger;
	

	public OscarFileUploadBO()
	{
    	super();
    	logger = new Logger(OscarFileUploadBO.class.getName());
		
	}
	
	/**
	 * Saves the OSCAR City/State value for the provided list of ZipCodeSpreadSheetRowVOs
	 * 
	 * @param spreadsheetRowList
	 * @throws DataAccessException
	 */
	public void saveCityStateFlagUpdates(List<ZipCodeVO> spreadsheetRowList) throws DataAccessException, Exception

	{
		List<ZipCodeVO> yesList = new ArrayList<ZipCodeVO>();
		List<ZipCodeVO> noList = new ArrayList<ZipCodeVO>();
		ZipCodeDAO dao = new ZipCodeDAO();

		if (null != spreadsheetRowList && spreadsheetRowList.size() > 0)
		{
			for (ZipCodeVO row:spreadsheetRowList)
			{
				if ( StringUtils.isNotBlank(row.getCity())
						&& StringUtils.isNotBlank(row.getOscarCityStateFlag())
						&& StringUtils.isNotBlank(row.getStateId()) )
				{
					String cityStateFlag = row.getOscarCityStateFlag();
				
					if (Y.equals(cityStateFlag))
					{
					    yesList.add(row);
					}
					if (N.equals(cityStateFlag))
					{
					    noList.add(row);
					}
					
				}
				
			}
			
			
			if (yesList.size() > 0)
			{
				dao.updateOscarCityStateFlag(yesList, Y);
			}
			if (noList.size() > 0)
			{
				dao.updateOscarCityStateFlag(noList, N);
				
			}
			
		}
		
	}
	
	/**
	 * Saves the OSCAR Zip Code value for the provided list of ZipCodeSpreadSheetRowVOs
	 * @param spreadsheetRowList
	 * @throws Exception
	 */
	public void saveZipCodeFlagUpdates(List<ZipCodeVO> spreadsheetRowList) throws DataAccessException, Exception
	{
		List<ZipCodeVO> yesList = new ArrayList<ZipCodeVO>();
		List<ZipCodeVO> noList = new ArrayList<ZipCodeVO>();
		ZipCodeDAO dao = new ZipCodeDAO();
		
		if (null != spreadsheetRowList && spreadsheetRowList.size() > 0)
		{
			for (ZipCodeVO row:spreadsheetRowList)
			{
				if ( StringUtils.isNotBlank(row.getZipCode())
						&& StringUtils.isNotBlank(row.getOscarZipFlag()) )
				{	
					String zipFlag = row.getOscarZipFlag();
					if (Y.equals(zipFlag))
					{
						yesList.add(row);
					}
					else if (N.equals(zipFlag))
					{
						noList.add(row);
					}
					
				}				
			}
			
			if (yesList.size() > 0)
			{
				dao.updateOscarZipFlag(yesList, Y);
			}
			if (noList.size() > 0)
			{
				dao.updateOscarZipFlag(noList, N);
				
			}
			
		}
		
	}
	
}

/*
 * MAINTENANCE HISTORY 
 * $Log: OscarFileUploadBO.java,v $
 * Revision 1.2  2012/12/07 18:39:44  gsergeycvs
 * #0000
 * Merge to trunk from 5.11.1.5
 *
 * Revision 1.1.4.5  2012/11/15 20:25:55  mavinenicvs
 * #13513 24344
 *
 * Revision 1.1.4.4  2012/11/15 16:20:01  rlazukcvs
 * #13513
 *
 * Revision 1.1.4.3  2012/11/10 22:48:16  mavinenicvs
 * #13513 24345
 *
 * Revision 1.1.4.2  2012/11/08 22:56:03  rlarsoncvs
 * #13513 24344 24345
 * Moved from BIRCHBOB branch
 *
 * Revision 1.1.2.3  2012/11/08 22:33:47  rlarsoncvs
 * #13513 24344 24345
 * Updates
 *
 * Revision 1.1.2.2  2012/11/07 22:58:49  rlarsoncvs
 * #13513 24344 24345
 * Expanded SaveCityState method
 *
 * Revision 1.1.2.1  2012/11/01 22:54:47  rlarsoncvs
 * #13513 24344
 * Initial version
 *
 */