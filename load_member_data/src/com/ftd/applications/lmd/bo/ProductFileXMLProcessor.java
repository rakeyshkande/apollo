package com.ftd.applications.lmd.bo;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.productObjects.FTD;
import com.ftd.applications.lmd.productObjects.FTD.Products;
import com.ftd.applications.lmd.productObjects.FTD.Products.Product;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;


public class ProductFileXMLProcessor {

    private Logger logger  = new Logger("com.ftd.applications.lmd.bo.ProductFileXMLProcessor");

    public List<Product> process(String fileName) throws JAXBException, Exception {
        
		Object object = null;
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String saveToLocation = cu.getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, "SAVE_TO_LOCATION");

		String xmlFile = saveToLocation + "/" + fileName;
		logger.info("fileName: " + xmlFile);

		List<Product> products = null;
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(FTD.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			object = unmarshaller.unmarshal(new File(xmlFile));
			FTD ftd = (FTD) object;
			Products p = ftd.getProducts();
			products = p.getProduct();
			logger.info("productList size = " + products.size());
		} catch (JAXBException e) {
            logger.error("JAXB error: " + e);
        } catch (Exception e) {
            logger.error("error: " + e);
		}
        
        return products;

    }

}
