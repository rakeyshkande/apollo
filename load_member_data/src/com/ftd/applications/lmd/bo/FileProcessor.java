package com.ftd.applications.lmd.bo;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.SQLException;

import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;



import org.xml.sax.SAXException;

import com.ftd.applications.lmd.memberObjects.FTD.Member;
import com.ftd.applications.lmd.productObjects.FTD.Products.Product;
import com.ftd.applications.lmd.vo.FloristWeightCalcVO;


/**
 * Uses the ParseConfig utility to pull the necessary data from the member, city,
 * and link files and set the data to value objects representing the file data.
 *
 * @author Anshu Gaind
 */

public class FileProcessor
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.FileProcessor");
  private CityFileProcessor cityFileProcessor;
  private MemberFileProcessor memberFileProcessor;
  private MemberFileXMLProcessor memberFileXMLProcessor;
  private ProductFileXMLProcessor productFileXMLProcessor;
  private FloristRevenueFileProcessor floristRevenueFileProcessor;
  private FloristLSTReciprocityFileProcessor floristLSTReciprocityFileProcessor;
  /**
   * Constructor
   */
  public FileProcessor()
  {
  }

  /**
   * Process city file
   *
   * @param fileName
   * @return
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws java.sql.SQLException
 * @throws XPathExpressionException 
   */
  public Map processCityFile(String fileName)
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, XPathExpressionException
  {
    if (this.cityFileProcessor == null) 
    {
        this.cityFileProcessor = new CityFileProcessor();
    }
    return this.cityFileProcessor.process(fileName);
  }

  /**
   * Process member file
   *
   * @param fileName
   * @return
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Map processMemberFile(String fileName, Map citiesMap)
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
  {
    if (this.memberFileProcessor == null) 
    {
        this.memberFileProcessor = new MemberFileProcessor();
    }
    return this.memberFileProcessor.process(fileName, citiesMap);
  }

  public List<Member> processMemberXMLFile(String fileName, Map citiesMap)
  throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
  {
    if (this.memberFileXMLProcessor == null) 
    {
        this.memberFileXMLProcessor = new MemberFileXMLProcessor();
    }
    return this.memberFileXMLProcessor.process(fileName);
  }

  public List<Product> processProductXMLFile(String fileName)
  throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
  {
    if (this.productFileXMLProcessor == null) 
    {
        this.productFileXMLProcessor = new ProductFileXMLProcessor();
    }
    return this.productFileXMLProcessor.process(fileName);
  }

  /**
   * Processes florist revenue file.
   * 
   * @param fileName
   * @param controlDate
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map processFloristRevenueFile(String fileName, GregorianCalendar controlDate)
      throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
  {
    if (this.floristRevenueFileProcessor == null) 
    {
        this.floristRevenueFileProcessor = new FloristRevenueFileProcessor();
    }
    
    return this.floristRevenueFileProcessor.process(fileName, controlDate);
    
  }

  /**
   * Processes low sending threshold and recprocity data file.
   * 
   * @param fileName
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws XPathExpressionException 
   */
  public Map processLSTReciprocityFile(String fileName)
      throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException 
  {
    if (this.floristLSTReciprocityFileProcessor == null) 
    {
        this.floristLSTReciprocityFileProcessor = new FloristLSTReciprocityFileProcessor();
    }
    
    return this.floristLSTReciprocityFileProcessor.process(fileName);
    
  }

}//~
