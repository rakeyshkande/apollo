package com.ftd.applications.lmd.bo;
import com.ftd.applications.lmd.util.FTPHelper;
import com.ftd.applications.lmd.util.FieldProcessor;
import com.ftd.applications.lmd.util.ParseConfig;
import com.ftd.applications.lmd.util.StateCodesHandler;
import com.ftd.applications.lmd.vo.FloristCodifiedProductVO;
import com.ftd.applications.lmd.vo.FloristMasterVO;
import com.ftd.applications.lmd.vo.ParseFieldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;

/**
 * Uses the ParseConfig utility to pull the necessary data from the member, city,
 * and link files and set the data to value objects representing the file data.
 *
 * @author Anshu Gaind
 * @version $Id: MemberFileProcessor.java,v 1.8 2011/06/30 15:14:15 gsergeycvs Exp $
 */
public class MemberFileProcessor
{
                                 // Florists with minimum_order_amounts <= this value will be codified as .LOW
  private static final int   LOW_CODIFICATION_MIN_ORDER_AMOUNT = 30;
                                 // .LOW codification ID
  private static final String CODIFICATION_ID_LOW = ".LOW";
                                // Number of holiday codification fields in member file
  private static final int   NUM_HOLIDAY_CODIFICATION_FIELDS = 50;

  private static final int PAD_RIGHT = 0;
  private static final int PAD_LEFT = 1;

  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.MemberFileProcessor");

  public MemberFileProcessor()
  {
  }

  /**
   * Process member file
   *
   * @param fileName
   * @return
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Map process(String fileName, Map citiesMap)
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
  {
    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    HashMap map = new HashMap(100000, 0.9f);
    Map membersFileConfigMap = ParseConfig.getInstance().getMembersFileConfigMap();
    Map codifiedMasterConfigMap = ParseConfig.getInstance().getCodifiedMasterConfigMap();

    // load the link files from their local ftp location
    BufferedReader br = new BufferedReader(new FTPHelper().getFile("MemberData", fileName) );
    int ctr = 0;
    logger.debug("Processing " + fileName);

    try
    {

      FloristMasterVO floristMasterVO = null;
      FloristCodifiedProductVO floristCodifiedProductVO = null;
      String record, state, stateCode, codifiedData, holidayFlagIndicator;
      ParseFieldVO codifiedDataParseFieldVO = null;

      String minimumOrderAmount = null
            , suffix = null
            , floristMemberNumber = null
            , zipCode = null
            , phoneNumber = null
            , specialListingInformation = null
            , tollFreeNumber = null
            , faxNumber = null
            , parentFloristID = null
            , topLevelFloristID = null;

	    byte[]  specialListingInformationByteArray, byteTollFree, byteFax;
      byte byteCurrent = 0;
      int j = 0;

      StateCodesHandler stateCodesHandler = (StateCodesHandler)CacheManager.getInstance().getHandler("STATE_CODES");
      //A non-digit: [^0-9]
      Pattern pattern = Pattern.compile("\\D");
      Matcher matcher;

      while ((record = br.readLine()) != null)
      {
        if ( record != null && (! record.equals("")) )
        {
            // The powers that be have mandated that florist suffixes starting with Z
            // are sending-only, so don't include them.
            suffix = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("suffix"), record);
            floristMemberNumber = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("florist-member-number"), record);
            if (suffix != null && suffix.startsWith("Z")) {
                logger.debug("Florist with Z suffix ignored: " + floristMemberNumber + suffix);
                continue;
            }

           // set the attributes on the florist master vo
           floristMasterVO = new FloristMasterVO();
           floristMasterVO.setListingType( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("listing-type"), record) );
           floristMasterVO.setFloristID( floristMemberNumber + suffix );

           floristMasterVO.setFloristName( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("florist-name"), record) );
           floristMasterVO.setAddress( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("address"), record) );
           // city set under load city data

           // get state code from record
           stateCode = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("state"), record);
           // lookup state for that state code, from the cache
           state = stateCodesHandler.getState(stateCode);
           // set that state on florist master vo
           floristMasterVO.setState(state);

           // remove leading 'Z' for all US zipcodes
           zipCode = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("zip-code"), record);
           floristMasterVO.setZipCode( (zipCode.charAt(0) == 'Z')?zipCode.substring(1):zipCode );

           // extract the numbers from phone number
           phoneNumber = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("phone-number"), record);
           matcher = pattern.matcher(phoneNumber.subSequence(0,phoneNumber.length()));
           floristMasterVO.setPhoneNumber( matcher.replaceAll("") );

           floristMasterVO.setCityStateNumber( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("city-state-number"), record) );

           // load city data
           if ( floristMasterVO.getListingType().equals("E") || floristMasterVO.getListingType().equals("S"))
           {
              floristMasterVO.setCity( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("city"), record) );
           }
           else
           {
              floristMasterVO.setCity((String) citiesMap.get(floristMasterVO.getCityStateNumber()) );
           }

           floristMasterVO.setMercuryFlag( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("mercury-flag"), record) );
           floristMasterVO.setInternalLinkNumber( FieldProcessor.getFieldValue ( (ParseFieldVO) membersFileConfigMap.get("internal-link-number"), record) );

           if (floristMasterVO.getListingType().equals("R"))
           {
              floristMasterVO.setOwnersName( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("owners-name"), record) );
           }

           minimumOrderAmount = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("minimum-order-amount"), record).substring(1,4);

           // If minimum order amount is below specific value, then florist should be codified as LOW.
           try {
             int minimumOrderAmountInt = Integer.valueOf(minimumOrderAmount).intValue();
             if ( minimumOrderAmountInt <= LOW_CODIFICATION_MIN_ORDER_AMOUNT)
             {
                floristCodifiedProductVO = new FloristCodifiedProductVO();
                floristCodifiedProductVO.setCodifiedID(CODIFICATION_ID_LOW);
                floristCodifiedProductVO.setMinPrice(minimumOrderAmount);
                floristMasterVO.setFloristCodifiedProductVO(floristCodifiedProductVO);
             }
           } catch (Exception e) {
             // If minimumOrderAmount was empty (or non-numeric), then florist should still be codified as LOW.
             floristCodifiedProductVO = new FloristCodifiedProductVO();
             floristCodifiedProductVO.setCodifiedID(CODIFICATION_ID_LOW);
             floristCodifiedProductVO.setMinPrice("0");
             floristMasterVO.setFloristCodifiedProductVO(floristCodifiedProductVO);
           }
           floristMasterVO.setMinimumOrderAmount( minimumOrderAmount );
           floristMasterVO.setFtoFlag( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("fto-flag"), record) );
           floristMasterVO.setFloristWeight( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("florist-weight"), record) );
           floristMasterVO.setInitialWeight( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("initial-weight"), record) );

           // load codified product data
           try {
               for (Iterator iter = codifiedMasterConfigMap.keySet().iterator(); iter.hasNext() ;)
               {
                  codifiedDataParseFieldVO = (ParseFieldVO) codifiedMasterConfigMap.get((String) iter.next());
                  codifiedData = FieldProcessor.getFieldValue( codifiedDataParseFieldVO, record);
                  // If codification data in florist_master for this codification_id, then add
                  // entry in florist_codifications
                  if (codifiedData != null && (!codifiedData.trim().equals("")) )
                  {
                    floristCodifiedProductVO = new FloristCodifiedProductVO();
                    floristCodifiedProductVO.setCodifiedID(codifiedDataParseFieldVO.getName());
                    holidayFlagIndicator = codifiedData.substring(0,1);
                    if (holidayFlagIndicator.equals("H"))
                    {
                        floristCodifiedProductVO.setHolidayPriceFlag(holidayFlagIndicator);
                    }
                    if(codifiedData.length() < Integer.parseInt(codifiedDataParseFieldVO.getLength())) {
                        codifiedData = FieldProcessor.pad(codifiedData, PAD_RIGHT, " ", Integer.parseInt(codifiedDataParseFieldVO.getLength()));
                    }
                    floristCodifiedProductVO.setMinPrice(codifiedData.substring(1,4));

                    floristMasterVO.setFloristCodifiedProductVO(floristCodifiedProductVO);
                  }
               }//~ end for loop
           } catch (Exception e) {
               logger.error("File processing failed on line number " + (ctr+1));
               if(codifiedDataParseFieldVO != null) {
                   logger.error("Error processing non-holiday codifications. Name=" +
                        codifiedDataParseFieldVO.getName() + ";position=" +
                        codifiedDataParseFieldVO.getStartPosition());
               }
               throw e;
           }

           // Process Holiday Codifications.  The codification IDs (if any) will be in one or more
           // of the 50 Holiday Code fields.  Since the codification IDs are not necessarily
           // in same code field per florist, we can't process as was done for other codified
           // product data (in above loop).
           String hCodeValue = null;
           int holidayCode = 0;
           try {
               for (holidayCode = 0; (holidayCode < NUM_HOLIDAY_CODIFICATION_FIELDS); holidayCode++)
               {
                  hCodeValue = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("holiday-code-" + (holidayCode+1)), record);
                  if ((hCodeValue != null) && (!hCodeValue.trim().equals("")))
                  {
                    floristCodifiedProductVO = new FloristCodifiedProductVO();
                    floristCodifiedProductVO.setCodifiedID(hCodeValue);
                    // This codification ID should not have been added via above codification
                    // loop so no check is necessary - just blindly add it.
                    floristMasterVO.setFloristCodifiedProductVO(floristCodifiedProductVO);
                    if (codifiedMasterConfigMap.containsKey(hCodeValue.trim()))
                    {
                       logger.error("Holiday codification '" + hCodeValue + "' referenced from florist_id '" + floristMasterVO.getFloristID() +
                                    "' was not found in codification_master. ");
                    }
                  } else {
                    // Assumption here is holiday codes are contiguous, i.e., once a blank code is
                    // encountered, there won't be any more (so we can safely exit this loop).
                    break;
                  }
               }
           } catch (Exception e) {
               logger.error("File processing failed on line number " + (ctr+1));
               logger.error("Error processing holiday codifications. Holiday code=" +
                   holidayCode + ";holiday code value=" + hCodeValue);
               throw e;
           }


           // process special listing information to extract toll free number and fax number
           // toll free number is followed by a hex value of 1
           // fax number is followed by a hex value of 2
           // store hours is followed by a hex value of 3
           int i = 0;
           specialListingInformationByteArray = null;
           try {
               specialListingInformation = FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("special-listing-information"), record);

               if (specialListingInformation != null )
               {
                  specialListingInformationByteArray = specialListingInformation.getBytes();
                  byteTollFree = new byte[1024];
                  byteFax = new byte[1024];

                  for (i = 0; i < specialListingInformationByteArray.length; i++)
                  {

                    if (specialListingInformationByteArray[i] == 63 || specialListingInformationByteArray[i] == 159 ) {
                        break;
                    }
                    if (specialListingInformationByteArray[i] < 10)
                    {
                       byteCurrent = specialListingInformationByteArray[i];
                       j = 0;
                    }
                    if (byteCurrent == 1)
                    {
                      byteTollFree[j] = specialListingInformationByteArray[i];
                      j++;
                    }
                    else
                    {
                      if (byteCurrent == 2	)
                      {
                        byteFax[j] = specialListingInformationByteArray[i];
                        j++;
                      }
                      else
                      {
                        j = 0;
                      }
                    }

                  }
                  tollFreeNumber = new String(byteTollFree);
                  faxNumber = new String(byteFax);
                  floristMasterVO.setTollFreeNumber( tollFreeNumber );
                  floristMasterVO.setFaxNumber( faxNumber );
               }

               // add territory
               floristMasterVO.setTerritory( FieldProcessor.getFieldValue( (ParseFieldVO) membersFileConfigMap.get("territory"), record) );

           } catch (Exception e) {
               logger.error("File processing failed on line number " + (ctr+1));
               logger.error("Error processing special list information. i=" + i + ";j=" + j);
               logger.error("specialListingInformation is:" + specialListingInformation);
               logger.error("specialListingInformationByteArray[0] is:" + specialListingInformationByteArray[0]);
               logger.error("Current char is: " + specialListingInformationByteArray[i]);
               throw e;
           }
           // add florist to the collection
           map.put(floristMasterVO.getFloristID(), floristMasterVO);
           if (++ctr%threshold == 0) logger.debug(ctr + " records processed");

        }//~ end if

      }//~ end while loop

    } catch (Exception e) {
        logger.error("File processing failed on line number " + (ctr+1));
        logger.error(e);
        throw e;
    }
    finally
    {
      if (br != null)
      {
          br.close();
          logger.debug("Processed a total of " + map.size() + " records from " + fileName);
      }
    }

    return map;
  }

}
