/*
 * @(#) ManualMercuryFloristBusinessDelegate.java      1.1.2.3     2004/09/30
 * 
 * 
 */

package com.ftd.applications.lmd.bo;

import com.ftd.applications.lmd.dao.ManualMercuryFloristDAO;
import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.exception.ManualMercuryFloristException;
import com.ftd.applications.lmd.util.LoadMemberDataUtil;
import com.ftd.applications.lmd.vo.ManualMercuryFloristVO;

import com.ftd.osp.utilities.plugins.Logger;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/09/14  Initial Release.(RFL)
 * 1.2      2004/09/21  Added and enhanced functionality.(RFL)
 * 1.1.2.3  2004/09/30  Correction mulitple saves.(RFL)
 * 
 * 
 */
 
public class ManualMercuryFloristBusinessDelegate  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static final String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.dao.ManualMercuryFloristDAO";
    private static final int RESTART_TIME = 6;
    private static final int HOURS_PER_DAY = 24;
    private static final long MILLISECONDS_PER_HOUR = 60 * 60 * 1000;
    private static final int HOURS_REMAINING = HOURS_PER_DAY - RESTART_TIME;
    private static final long MILLISECONDS_REMAINING = 
                                        HOURS_REMAINING * MILLISECONDS_PER_HOUR;
    //EVENTS INFO//
     private final String QUEUE = "FTDM_SHUTDOWN";
    private final String CONTEXT = "LOAD_MEMBER_DATA";
    private final String EVENT = "FTDM_SHUTDOWN";
    public static final String SHUTDOWN= "SHUTDOWN";
    public static final String UPDATE= "UPDATE";
    public static final String UPDATE_RESTART= "UPDATE_RESTART";

    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Logger logger;

    


    public ManualMercuryFloristBusinessDelegate() 
    {
        super();
        logger = new Logger(LOGGER_CATEGORY);
        
    }//end method
    
    
    public ManualMercuryFloristVO getCurrentStatus() 
                                            throws DataAccessException
    {
        return new ManualMercuryFloristDAO().getCurrentStatus();
        
    }//end method getCurrentStatus()
    
    
    /**
     * Business rules require that all future date restarts are scheduled for
     * 6:00 am.
     * 
     * @param   restartDate <code>Date</code>
     * @param   csrID       <code>String</code> of CSR's id
     * 
     * @throws  ManualMercuryFloristException
     */
    public void restart(Date restartDate, String csrID) 
                                                    throws DataAccessException, Exception
    {
        Date now = new GregorianCalendar().getTime();
        ManualMercuryFloristDAO dao = new ManualMercuryFloristDAO();
        
            Date correctedDate = this.amendTimeToDate(restartDate,
                                                      this.RESTART_TIME,
                                                      0,
                                                      0,
                                                      0);
                                                      
        if (correctedDate.before(now)) 
        {
            LoadMemberDataUtil loadMemberDataUtil= new LoadMemberDataUtil();
            String date = this.buildRestartDateString(now);
            String payload = UPDATE_RESTART +","+csrID+","+date;
            loadMemberDataUtil.queueToEventsQueue(CONTEXT, EVENT, "0", payload,QUEUE);
            
        }//end if
        else 
        {
            LoadMemberDataUtil loadMemberDataUtil= new LoadMemberDataUtil();
            String date = this.buildRestartDateString(correctedDate);
            String payload = UPDATE +","+csrID+","+date;
            loadMemberDataUtil.queueToEventsQueue(CONTEXT, EVENT, "0", payload,QUEUE);
        
        }//end else
    }//end method restart()
    
    
    public void shutdown(Date restartDate, String csrID) throws DataAccessException, Exception
    {     
        Date correctedDate = this.amendTimeToDate(restartDate,this.RESTART_TIME, 0, 0, 0);
        LoadMemberDataUtil loadMemberDataUtil= new LoadMemberDataUtil();
        String date = this.buildRestartDateString(correctedDate);
        String payload = SHUTDOWN +","+csrID+","+date;
        loadMemberDataUtil.queueToEventsQueue(CONTEXT, EVENT, "0", payload, QUEUE);
        //new ManualMercuryFloristDAO().saveShutdown(correctedDate, csrID);
        
    }//end method shutdown()
    
    
    /*
     * 
     */
    private Date amendTimeToDate(Date date, 
                                 int hour, 
                                 int minute, 
                                 int second, 
                                 int millisecond) 
    {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, millisecond);
        return cal.getTime();
        
    }//end method
    
    
    /*
     * 
     */
    private boolean isToday(Date date) 
    {
        Calendar cal = new GregorianCalendar();
        
        cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, RESTART_TIME);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        Date six = cal.getTime();
        
        long diff = date.getTime() - six.getTime();
        
        if (diff > MILLISECONDS_REMAINING) 
        {
            return false;
            
        }//end if
        else 
        {
            return true;
            
        }//end else 
        
    }//end method

    private String buildRestartDateString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(date);
    }
   

}//end class