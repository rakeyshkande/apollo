package com.ftd.applications.lmd.bo;
import com.enterprisedt.net.ftp.FTPException;

import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.util.FTPHelper;
import com.ftd.applications.lmd.util.FieldProcessor;
import com.ftd.applications.lmd.util.ParseConfig;
import com.ftd.applications.lmd.vo.FloristWeightCalcVO;
import com.ftd.applications.lmd.vo.ParseFieldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

/**
 * Processes Florist revenue file
 * 
 * @author Anshu Gaind
 */

public class FloristRevenueFileProcessor 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.FloristRevenueFileProcessor");

  /**
   * Constructor
   */
  public FloristRevenueFileProcessor()
  {
  }

  /**
   * Processes florist revenue data file
   * 
   * @param fileName
   * @param processingDate
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map process(String fileName, GregorianCalendar processingDate)
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
    {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    int maximumWeight = Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "MAXIMUM_POINTS"));
    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    HashMap map = new HashMap();
   
    Map revenueDataConfigMap = ParseConfig.getInstance().getFloristRevenueDataConfigMap();

    // load the link files from their local ftp location
    BufferedReader br = new BufferedReader(new FTPHelper().getFile("FloristRevenueData", fileName) );
    logger.debug("Processing " + fileName);


    try
    {

      FloristWeightCalcVO floristWeightCalcVO = null;
      String record, totalRevenue, rebate,_FTDOrders;
      ParseFieldVO codifiedDataParseFieldVO;      

      int ctr = 0, ordersSentFromFTD = 0;

      String processFTDOrdersFromRevenueFile = (String)ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "PROCESS_FTD_ORDERS_FROM_REVENUE_FILE");
      Map _FTDOrdersMap = null;
      
      /** Apply the short-term solution where ftd orders are provided in a separate file **/
      if (processFTDOrdersFromRevenueFile.equalsIgnoreCase("false")) 
      {
        String _fileName = (String)ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "FTD_ORDERS_DATA_FILE");
        _FTDOrdersMap = new FloristFTDOrdersFileProcessor().process(_fileName, processingDate);          
      }
      

      BigDecimal dTotalRevenue = null;
      BigDecimal dRebates = null;
      while ((record = br.readLine()) != null)
      {
        if ( record != null && (! record.equals("")) )
        {
           try 
           {
             floristWeightCalcVO = new FloristWeightCalcVO();
             floristWeightCalcVO.setDate(processingDate);
             floristWeightCalcVO.setMaximumWeight(maximumWeight);
             floristWeightCalcVO.setFloristID( FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("member-long-key"), record) );
             floristWeightCalcVO.setTopLevelFloristKey( FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("top-level-florist-key"), record) );
             
             totalRevenue = FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("total-revenue"), record);             
             totalRevenue = FieldProcessor.decodeCharacterMapping(totalRevenue);
             // length 11, scale of 2
             floristWeightCalcVO.setTotalRevenue( (totalRevenue != null && (!totalRevenue.equals("")))?new BigDecimal( new BigInteger(totalRevenue) , 2 ):null );
             
             rebate = FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("rebate-revenue"), record);             
             rebate = FieldProcessor.decodeCharacterMapping(rebate);
             // length 15, scale of 2
             floristWeightCalcVO.setRebate( (rebate != null && (!rebate.equals("")))?new BigDecimal( new BigInteger(rebate) , 2 ):null );
             
             //Now back out the rebates from total revenue (defect 1067)
             dTotalRevenue = floristWeightCalcVO.getTotalRevenue();
             dRebates = floristWeightCalcVO.getRebate();
             if( dTotalRevenue!=null && dRebates!=null ) 
             {
               floristWeightCalcVO.setTotalRevenue(dTotalRevenue.subtract(dRebates));
             }
             
             
             floristWeightCalcVO.setTotalClearedOrders( Integer.parseInt(FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("cleared-order-number"), record)) );
             /******************************************************************/
              if (processFTDOrdersFromRevenueFile.equalsIgnoreCase("false")) 
              {
               // get the value of the orders sent from ftd from the ftd orders map
               _FTDOrders = (String) _FTDOrdersMap.get(floristWeightCalcVO.getFloristID());
               ordersSentFromFTD = ((_FTDOrders != null) && (!_FTDOrders.equals("")))?Integer.parseInt(_FTDOrders):0;
               floristWeightCalcVO.setOrdersSentFromFTD(ordersSentFromFTD);                   
              }
              else 
              {
                floristWeightCalcVO.setOrdersSentFromFTD( Integer.parseInt(FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("ftd-orders"), record)) );
              }
             /******************************************************************/

             floristWeightCalcVO.setGroceryStoreFlag( FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("grocery-store-flag"), record) );
             floristWeightCalcVO.setOrderGathererFlag( FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("order-gatherer-flag"), record) );
             floristWeightCalcVO.setSendOnlyFlag( FieldProcessor.getFieldValue( (ParseFieldVO) revenueDataConfigMap.get("send-only-flag"), record) );
  
             // add florist to the collection
             map.put(floristWeightCalcVO.getFloristID(), floristWeightCalcVO);
             if (++ctr%threshold == 0) logger.debug(ctr + " records processed");
            
           } catch (Exception ex) 
           {
            logger.error("Skipping record..." + floristWeightCalcVO.toXML());
            logger.error(ex);
           } 

        }//~ end if

      }//~ end while loop

    } finally
    {
      if (br != null)
      {
          br.close();
          logger.debug("Processed a total of " + map.size() + " records from " + fileName);
      }
    }

    return map;
    
  }
  
  
  /****************************************************************************/
  
  /**
   * This class was created as part of a short-term solution, where ftd orders
   * are being processed from an external file. Eventually all this functionality
   * will be deprecated in favor of that data being a part of the revenue file.
   * 
   * @author Anshu Gaind
   */
class FloristFTDOrdersFileProcessor 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.FloristFTDOrdersFileProcessor");

  /**
   * Constructor
   */
  public FloristFTDOrdersFileProcessor()
  {
  }

  /**
   * Processes florist revenue data file
   * 
   * @param fileName
   * @param processingDate
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map process(String fileName, GregorianCalendar processingDate)
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
    {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    HashMap map = new HashMap();
   
    String _FTDOrdersDataFileParsingLogic = (String)ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "FTD_ORDERS_DATA_FILE_PARSING_LOGIC");
    String _XPath = (String)ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "FTD_ORDERS_DATA_FILE_PARSING_LOGIC_XPATH");
    
    // load the config information
    Document doc = ((Document)DOMUtil.getDocument(_FTDOrdersDataFileParsingLogic));        
    
    Map _FTDOrdersDataConfigMap = this.loadConfig(doc, _XPath);

    BufferedReader br = null;


    try
    {

      FloristWeightCalcVO floristWeightCalcVO = null;
      String record, totalRevenue, rebate;
      ParseFieldVO codifiedDataParseFieldVO;      
      String floristID = null, ftdOrders = null, yearMonth = null;
      
      String processingYear = String.valueOf(processingDate.get(Calendar.YEAR));
      String processingMonth = ( (processingDate.get(Calendar.MONTH)+1) < 10)?("0"+(processingDate.get(Calendar.MONTH)+1)):(String.valueOf(processingDate.get(Calendar.MONTH)+1));
      String processingYearMonth = processingYear + processingMonth;
      
      int ctr = 0;

      // load the link files from their local ftp location
      ArrayList remoteFile = new ArrayList();
      remoteFile.add(fileName);
      FTPHelper ftpHelper = new FTPHelper();
      
      /**
       * This was done to maintain the signature of the method. The FTPException
       * is caught, logged, and thrown back as an IOException. As the constructor
       * of IOException does not allow nesting other exceptions, the entire
       * stack trace of the FTPException is used as the detailed message.
       * 
       * The process does not create any lock files
       * 
       * @author Anshu Gaind
       */
      try 
      {
        ftpHelper.login("FloristRevenueData");
        ftpHelper.getFileFromFTPServer(remoteFile);        
      } catch (FTPException ex) 
      {
        // write the stack trace to the log_message variable
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        ex.printStackTrace(pw);
        String log_message = sw.toString();
        pw.close();        
        
        logger.error(ex);
        
        throw new IOException(log_message);
      } finally
      {
        if (ftpHelper.isLoggedIn()) 
        { 
          try 
          {
            ftpHelper.logout();  
          } catch (Exception ex) 
          {
            logger.error(ex);
          }    
        }            

      }
     
      br = new BufferedReader(new FTPHelper().getFile("FloristRevenueData", fileName) );
      logger.debug("Processing " + fileName);

      while ((record = br.readLine()) != null)
      {
        if ( record != null && (! record.equals("")) )
        {
           try 
           {
              yearMonth = FieldProcessor.getFieldValue( (ParseFieldVO) _FTDOrdersDataConfigMap.get("year-month"), record);
              // collect data only for the processing month and year
              if (processingYearMonth.equals(yearMonth)) 
              {
                floristID = FieldProcessor.getFieldValue( (ParseFieldVO) _FTDOrdersDataConfigMap.get("florist-id"), record);;
                ftdOrders = FieldProcessor.getFieldValue( (ParseFieldVO) _FTDOrdersDataConfigMap.get("ftd-orders"), record);
                
                // the key is the florist id and the value is ftd orders
                map.put(floristID, ftdOrders);                  
              }
              
              if (++ctr%threshold == 0) logger.debug(ctr + " records processed");
           } catch (Exception ex) 
           {
            logger.error("Skipping record..." + FieldProcessor.getFieldValue( (ParseFieldVO) _FTDOrdersDataConfigMap.get("florist-id"), record ) );
            logger.error(ex);
           } 

        }//~ end if

      }//~ end while loop

      // archive the file
      ftpHelper.archiveFile("FloristRevenueData",remoteFile);
      
    } finally
    {
      if (br != null)
      {
          br.close();
          logger.debug("Processed a total of " + map.size() + " records from " + fileName);
      }
    }

    return map;
    
  }


  /**
   * Loads configuration data
   * 
   * @param doc
   * @param XPath
   * @return 
 * @throws XPathExpressionException 
   */
  private Map loadConfig(Document doc, String XPath) throws XPathExpressionException
  {
    HashMap map = new HashMap();
    
    NodeList nodeList = DOMUtil.selectNodes(doc, XPath);    
    Node node = null;
    ParseFieldVO fieldVO = null;

    if (nodeList != null) 
    {
      for (int i = 0; i < nodeList.getLength(); i++) 
      {
        node = (Node) nodeList.item(i);
        if (node != null) 
        {
          fieldVO = new ParseFieldVO();
          fieldVO.setName(DOMUtil.selectSingleNode(node,"name/text()").getNodeValue());
          fieldVO.setStartPosition(DOMUtil.selectSingleNode(node,"start-position/text()").getNodeValue());
          fieldVO.setLength(DOMUtil.selectSingleNode(node,"length/text()").getNodeValue());
          // add to the collection
          map.put(fieldVO.getName(), fieldVO);
        }      
      }      
    }
    return map;
  }

  
}  
  
}//:~)
