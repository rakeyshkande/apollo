package com.ftd.applications.lmd.bo;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.ejb.MessageDispatcherSessionEJBDelegate;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.w3c.dom.Document;

import org.w3c.dom.NamedNodeMap;

/**
 * Loads florist data from staging to production 
 * 
 * @author Anshu Gaind
 * @version $Id: LoadFloristDataToProductionHandler.java,v 1.5 2011/06/30 15:14:15 gsergeycvs Exp $
 */

public class LoadFloristDataToProductionHandler extends EventHandler 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.LoadFloristDataToProductionHandler");

  public LoadFloristDataToProductionHandler()
  {
  }

  /**
   * Invoke the event handler
   *
   * @payload payload
   * @throws java.lang.Throwable
   */
  public void invoke(Object payload) throws Throwable
  {
    FloristDAO floristDAO = new FloristDAO();
    Document doc = null;
    
    try 
    {
      MessageToken messageToken = (MessageToken) payload;
      doc = (Document) DOMUtil.getDocument((String) messageToken.getMessage());

      
      // load florist data to production 
      floristDAO.loadFloristDataToProduction();
      
      NamedNodeMap attributes = DOMUtil.selectSingleNode(doc, "//notification").getAttributes();
      String recipConfigName = ConfigurationConstants.EMAIL_NOTIFY_RECIP_FLORIST_DATA;
      String _fromAddress = attributes.getNamedItem("from-address").getNodeValue();
      String _subject = DOMUtil.selectSingleNode(doc, "//notification/subject/text()").getNodeValue();
      String _notificationContent = DOMUtil.selectSingleNode(doc, "//notification/notification-content/text()").getNodeValue();

      // send notifications
      floristDAO.sendNotification(recipConfigName, _fromAddress, _subject, _notificationContent);
      
    } catch (Throwable t) 
    {
      NamedNodeMap attributes = DOMUtil.selectSingleNode(doc, "//audit-report").getAttributes();
      String _reportID = attributes.getNamedItem("report-id").getNodeValue();
      String _oldStatus = attributes.getNamedItem("old-status").getNodeValue();
      String _newStatus = attributes.getNamedItem("new-status").getNodeValue();
      String _updatedBy = attributes.getNamedItem("updated-by").getNodeValue();

      // change the status of the report back to the original status
      InitialContext initContext = new InitialContext();
      Context myEnv = (Context) initContext.lookup("java:comp/env");
      boolean convertToBool = false;
      if (myEnv != null) 
      {
        String useLocalInterface = (String) myEnv.lookup("Use MessageDispatcherSessionEJB Local Interface");
        convertToBool = (useLocalInterface != null && useLocalInterface.equals("true"))?true:false;          
      }
      new MessageDispatcherSessionEJBDelegate(convertToBool).updateFloristAuditReport(_reportID, _newStatus, _oldStatus, _updatedBy);
      logger.debug("Changed the status of audit report " + _reportID + " from " + _oldStatus + " to " + _newStatus);

      // send the system message
      this.sendSystemMessage(t);
      throw t;
    } finally 
    {
    }
    
    
  }

  /**
   * Sends a system message
   * 
   * @param t
   */
  private void sendSystemMessage(Throwable t)
  {
    Connection con = null;
    try 
    {
      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
      // write the stack trace to the log_message variable
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw, true);
      t.printStackTrace(pw);
      String logMessage = "Failed to load florist data to the production tables" + "\n" + sw.toString();
      pw.close();        
      
      SystemMessage.send(logMessage, "LOAD FLORIST DATA SERVICE", SystemMessage.LEVEL_PRODUCTION, "ERROR", con);
    } catch (Exception ex) 
    {
      logger.error(ex);
    } finally 
    {
      try 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
        
      } catch (Exception ex2) 
      {
        logger.error(ex2);
      }      
    }
    
  }

}
