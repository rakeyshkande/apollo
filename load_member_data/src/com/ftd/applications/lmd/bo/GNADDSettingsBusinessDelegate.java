/*
 * @(#) GNADDSettingsBusinessDelegate.java      1.1.2.2     2004/09/29
 * 
 * 
 */

package com.ftd.applications.lmd.bo;

import com.ftd.applications.lmd.dao.GlobalParmsDAO;
import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.vo.GlobalParmsVO;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.1.2.2      2004/09/29  Initial Release.(RFL)
 * 
 * 
 */
 
public class GNADDSettingsBusinessDelegate  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static final String LOGGER_CATEGORY = 
                    "com.ftd.applications.lmd.bo.GNADDSettingsBusinessDelegate";
    private static final int TRIGGER_TIME = 6;
    private static final int HOURS_PER_DAY = 24;
    private static final long MILLISECONDS_PER_HOUR = 60 * 60 * 1000;
    private static final int HOURS_REMAINING = HOURS_PER_DAY - TRIGGER_TIME;
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Logger logger;

    


    public GNADDSettingsBusinessDelegate() 
    {
        super();
        logger = new Logger(LOGGER_CATEGORY);
        
    }//end method
    
    
    /**
     * 
     * @returns GlobalParmsVO
     * @throws  DataAccessException
     */
    public GlobalParmsVO getSettings() throws DataAccessException
    {
        return new GlobalParmsDAO().getGlobalParms();
        
    }//end method getCurrentStatus()
    
    
    /**
     * Business rules require that all future date turn ons are scheduled for
     * 6:00 am.
     * 
     * @param   shutoffDate <code>Date</code>
     * 
     * @throws  DataAccessException
     */
    public void turnOn(Date GNADDDate, String userId) throws DataAccessException
    {
        GlobalParmsDAO dao = new GlobalParmsDAO();
        
        Date amendedTimeDate = this.amendTimeToDate(GNADDDate);
        
        dao.saveOn(GNADDDate, userId);
        
    }//end method restart()
    
    public void updateDate(Date GNADDDate, String userId) throws DataAccessException
    {
        GlobalParmsDAO dao = new GlobalParmsDAO();
        Date amendedTimeDate = this.amendTimeToDate(GNADDDate);
        dao.saveGNADDDate(GNADDDate, userId);
    }
    
    /**
     * 
     * @throws DataAccessException
     */
    public void turnOff(String userId) throws DataAccessException
    {
        GlobalParmsDAO dao = new GlobalParmsDAO();
        
        //save status
        dao.saveGNADDLevel(GlobalParmsVO.GNADD_STATE_OFF, userId);
        
    }//end method shutdown()
    
    
    /*
     * 
     */
    private Date amendTimeToDate(Date date) 
    {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, TRIGGER_TIME);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
        
    }//end method
    
    
}//end class