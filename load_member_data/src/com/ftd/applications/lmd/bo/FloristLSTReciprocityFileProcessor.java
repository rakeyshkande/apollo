package com.ftd.applications.lmd.bo;
import com.ftd.applications.lmd.util.FTPHelper;
import com.ftd.applications.lmd.util.FieldProcessor;
import com.ftd.applications.lmd.util.ParseConfig;
import com.ftd.applications.lmd.vo.FloristWeightCalcVO;
import com.ftd.applications.lmd.vo.LSTReciprocityVO;
import com.ftd.applications.lmd.vo.ParseFieldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.IOException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;



import org.xml.sax.SAXException;

/**
 * Processes low sending threshold and recprocity data file.
 * 
 * @author Anshu Gaind
 * @version $Id: FloristLSTReciprocityFileProcessor.java,v 1.3 2011/06/30 15:14:14 gsergeycvs Exp $
 */

public class FloristLSTReciprocityFileProcessor 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.FloristLSTReciprocityFileProcessor");

  /**
   * Constructor
   */
  public FloristLSTReciprocityFileProcessor()
  {
  }

  /**
   * Processes low sending threshold and recprocity data file.
   * 
   * @param fileName
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws XPathExpressionException 
   */
  public Map process(String fileName)
    throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException 
    {
    int threshold = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "PROGRESS_MESSAGE_THRESHOLD"));
    HashMap map = new HashMap();
   
    Map _LSTReciprocityDataConfigMap = ParseConfig.getInstance().getLSTReciprocityDataConfigMap();

    // load the link files from their local ftp location
    BufferedReader br = new BufferedReader(new FTPHelper().getFile("FloristRevenueData", fileName) );
    logger.debug("Processing " + fileName);

    try
    {
      LSTReciprocityVO _LSTReciprocityVO = null;
      String record, currencyCode, _LST, reciprocity;
      ParseFieldVO codifiedDataParseFieldVO;

      int ctr = 0;

      while ((record = br.readLine()) != null)
      {
        if ( record != null && (! record.equals("")) )
        {
           _LSTReciprocityVO = new LSTReciprocityVO();
           
           currencyCode = FieldProcessor.getFieldValue( (ParseFieldVO) _LSTReciprocityDataConfigMap.get("currency-code"), record);
           _LST = FieldProcessor.getFieldValue( (ParseFieldVO) _LSTReciprocityDataConfigMap.get("LST"), record);
           reciprocity = FieldProcessor.getFieldValue( (ParseFieldVO) _LSTReciprocityDataConfigMap.get("reciprocity"), record);
           
           _LSTReciprocityVO.setCurrencyCode(currencyCode);
           _LSTReciprocityVO.setLowSendingThresholdValue( Integer.parseInt(_LST) );
           _LSTReciprocityVO.setReciprocityRatio( Integer.parseInt(reciprocity) );

           // add to the collection
           map.put(_LSTReciprocityVO.getCurrencyCode(), _LSTReciprocityVO);
           
           if (++ctr%threshold == 0) logger.debug(ctr + " records processed");
            
        }//~ end if

      }//~ end while loop

    } finally
    {
      if (br != null)
      {
          br.close();
          logger.debug("Processed a total of " + map.size() + " records from " + fileName);
      }
    }

    return map;
    
  }
  
}//:~)
