package com.ftd.applications.lmd.bo;
import com.enterprisedt.net.ftp.FTPException;

import com.ftd.applications.lmd.bo.FileProcessor;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.memberObjects.FTD.Member;
import com.ftd.applications.lmd.productObjects.FTD.Products.Product;
import com.ftd.applications.lmd.util.FTPHelper;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;



import org.xml.sax.SAXException;

/**
 * Loads Florist Data
 * 
 * @author Anshu Gaind
 * @version $Id: LoadFloristDataHandler.java,v 1.4.82.1 2015/01/27 16:16:52 tschmigcvs Exp $
 */

public class LoadFloristDataHandler extends EventHandler 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.bo.LoadFloristDataHandler");
  private static String LOCKFILE = "LD_FL_DATA.LOCKFILE";

  public LoadFloristDataHandler()
  {
  }
  
  /**
   * Invoke the event handler
   *
   * @payload payload
   * @throws java.lang.Throwable
   */
  public void invoke(Object payload) throws Throwable
  {
    FTPHelper ftpHelper = null;
    boolean processing = false;

    try
    {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      List remoteFiles = new ArrayList();
      String remoteFile1 = configUtil.getProperty("filenames.xml", "city-file");
      String remoteFile2 = configUtil.getProperty("filenames.xml", "member-file");
      String remoteFile3 = configUtil.getProperty("filenames.xml", "product-file");
      remoteFiles.add(remoteFile1);
      remoteFiles.add(remoteFile2);
      remoteFiles.add(remoteFile3);
      
      ftpHelper = new FTPHelper();
      ftpHelper.login("MemberData");
      String[] fileNames = ftpHelper.list();
      
      List filesOnServer = Arrays.asList(fileNames);
    
    
      if (filesOnServer.contains(remoteFile1) && filesOnServer.contains(remoteFile2)) 
      {    
        // check to see if the files are not locked
        if ( ! ftpHelper.isLocked(LOCKFILE)) 
        {
          processing = true;
          
          // lock files
          ftpHelper.lock(LOCKFILE);
      
          ftpHelper.getFileFromFTPServer(remoteFiles);
          // log out to avoid a possible timeout situation while processing
          ftpHelper.logout();
              
          this.loadFloristData();
          this.archiveFloristDataFiles(remoteFiles);
          this.buildFloristAuditReport();
          this.notifyFloristAuditReportReady();        
        }
        else
        {
          logger.error("Files are locked on the FTP server. Skip processing...");
        }          
      }
      else
      {
        logger.error(remoteFile1 + " or " + remoteFile2 + " are not available on the FTP server. Skip processing...");
      }
    } catch(Throwable t)
    {
      // send the system message
      this.sendSystemMessage(t);
      throw t;      
    } finally 
    {
      if (processing) 
      {
        // log back in to unlock files, and logout
        ftpHelper.login("MemberData");
        if (ftpHelper.isLoggedIn()) 
        {                  
          if (ftpHelper.isLocked(LOCKFILE)) 
          {
            ftpHelper.unlock(LOCKFILE); 
          }
          ftpHelper.logout();          
        }            
      }      
      else 
      {
        if (ftpHelper.isLoggedIn()) 
        {                  
          ftpHelper.logout();          
        }                    
      }
    }              
  }
    

  /**
   * Sends a system message
   * 
   * @param t
   */
  private void sendSystemMessage(Throwable t)
  {
    Connection con = null;
    try 
    {
      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
      // write the stack trace to the log_message variable
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw, true);
      t.printStackTrace(pw);
      String logMessage = "Failed to load florist data to the staging tables" + "\n" + sw.toString();
      pw.close();        
      
      SystemMessage.send(logMessage, "LOAD FLORIST DATA SERVICE", SystemMessage.LEVEL_PRODUCTION, "ERROR", con);
    } catch (Exception ex) 
    {
      logger.error(ex);
    } finally 
    {
      try 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
        
      } catch (Exception ex2) 
      {
        logger.error(ex2);
      }      
    }
    
  }
 
  /**
   * Loads the data in the florist data files, into the database.
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.lang.Exception
   */
  private void loadFloristData() 
    throws IOException, SAXException, ParserConfigurationException, TransformerException ,  Exception
  {
    Map citiesMap = null;
    List<Member> floristsXMLList = null;
    List<Product> productsXMLList = null;

    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    FileProcessor fileProcessor = new FileProcessor();    

    
    // process city file
    logger.debug("Begin processing city file");
    citiesMap = fileProcessor.processCityFile(configUtil.getProperty("filenames.xml", "city-file"));
    logger.debug("End processing city file");
    
    // process product file
    logger.debug("Begin processing product file");
    productsXMLList = fileProcessor.processProductXMLFile( configUtil.getProperty("filenames.xml", "product-file"));
    logger.debug("End processing product file");
    
    // process member file
    logger.debug("Begin processing member file");
    floristsXMLList = fileProcessor.processMemberXMLFile( configUtil.getProperty("filenames.xml", "member-file"), citiesMap );
    logger.debug("End processing member file");
    
    // store file data to staging
    FloristDAO floristDAO = new FloristDAO();
    floristDAO.saveFloristData(floristsXMLList, citiesMap, productsXMLList);
    
    // New LMD changes will be added here

  }
 

  /**
   * Archives the Florist Data files
   * 
   * @param fileNames
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws XPathExpressionException 
   */
  private void archiveFloristDataFiles(List fileNames)
    throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException
  {
    // archive files
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    logger.debug("Archiving files");
    new FTPHelper().archiveFile("MemberData",fileNames);
    
  }

  /**
   * Builds the florist audit report
   * 
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private void buildFloristAuditReport()
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
    FloristDAO floristDAO = new FloristDAO();
    floristDAO.buildFloristAuditReport();
  }
  
  /**
   * Notifies that the florist audit report is ready
   * 
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private void notifyFloristAuditReportReady()
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
    ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
    
    String recipConfigName = ConfigurationConstants.EMAIL_NOTIFY_RECIP_FLORIST_DATA;
    String fromAddress = configurationUtil.getProperty("notification_config.xml", "FROM_ADDRESS");
    String subject = configurationUtil.getProperty("notification_config.xml", "FLORIST_AUDIT_REPORT_AVAILABLE_SUBJECT");
    String notificationContent = configurationUtil.getProperty("notification_config.xml", "FLORIST_AUDIT_REPORT_AVAILABLE_MESSAGE");
    
    FloristDAO floristDAO = new FloristDAO();
    floristDAO.sendNotification(recipConfigName, fromAddress, subject, notificationContent);
  }
}
