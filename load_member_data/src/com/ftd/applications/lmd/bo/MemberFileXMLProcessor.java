package com.ftd.applications.lmd.bo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.memberObjects.FTD;
import com.ftd.applications.lmd.memberObjects.FTD.Member;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


public class MemberFileXMLProcessor {

    private Logger logger  = new Logger("com.ftd.applications.lmd.bo.MemberFileXMLProcessor");

    public List<Member> process(String fileName) throws JAXBException, Exception {
        
		Object object = null;
		ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String saveToLocation = cu.getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, "SAVE_TO_LOCATION");

		String xmlFile = saveToLocation + "/" + fileName;
		logger.info("fileName: " + xmlFile);
		
		List<Member> memberList = null;
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(FTD.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			object = unmarshaller.unmarshal(new File(xmlFile));
			FTD ftd = (FTD) object;
			memberList = ftd.getMember();
			logger.info("memberList size = " + memberList.size());

		} catch (JAXBException e) {
            logger.error("JAXB error: " + e);
        } catch (Exception e) {
            logger.error("error: " + e);
		}

		return memberList;
    }

}
