package com.ftd.applications.lmd.bo;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.util.FTPHelper;
import com.ftd.applications.lmd.util.FloristTotalRevenueComparator;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.applications.lmd.vo.FloristWeightCalcVO;
import com.ftd.applications.lmd.vo.LSTReciprocityVO;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Calculates florist weights
 * 
 * @author Anshu Gaind
 */

public class FloristWeightCalcHandler extends EventHandler 
{
  private Logger logger= new Logger("com.ftd.applications.lmd.bo.FloristWeightCalcHandler");
  private static String LOCKFILE = "FL_WGT_CAL.LOCKFILE";
  private LSTReciprocityVO _LSTReciprocityVO;
  
  private FloristDAO floristDAO = new FloristDAO();
  
  
  public FloristWeightCalcHandler()
  {
  }


  /**
   * Invoke the event handler
   *
   * @payload payload
   * @throws java.lang.Throwable
   */
  public void invoke(Object payload) throws Throwable
  {
    FTPHelper ftpHelper = null;
    boolean processing = false;
    
    try 
    {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      
      List remoteFiles = new ArrayList();
      String remoteFile1 = configUtil.getProperty("filenames.xml", "florist-revenue-data-file");
      String remoteFile2 = configUtil.getProperty("filenames.xml", "LST-reciprocity-data-file");
      remoteFiles.add(remoteFile1);
      remoteFiles.add(remoteFile2);
      
      ftpHelper = new FTPHelper();
      ftpHelper.login("FloristRevenueData");

      String[] fileNames = ftpHelper.list();

      List filesOnServer = Arrays.asList(fileNames);
 
      
      if (filesOnServer.contains(remoteFile1) && filesOnServer.contains(remoteFile2)) 
      {    
          // check to see if the files are not locked
          if ( ! ftpHelper.isLocked(LOCKFILE)) 
          {
            processing = true;
            
            // lock files
            ftpHelper.lock(LOCKFILE);                        
            ftpHelper.getFileFromFTPServer(remoteFiles);
            // log out to avoid a possible timeout situation while processing
            ftpHelper.logout();
            

            GregorianCalendar controlDate = floristDAO.getFloristWeightCalcControlDate();
            logger.debug("The control date is " + controlDate.getTime() );
            GregorianCalendar processingDate = new GregorianCalendar();
            processingDate.setTimeInMillis(controlDate.getTimeInMillis());
            //rollback date to the beginning of the previous month    
            processingDate.add(Calendar.MONTH, -1);
            processingDate.roll(Calendar.DATE, (1- processingDate.get(Calendar.DATE)));
            logger.debug("The processing date is " + processingDate.getTime() );

            Map floristsMap = this.loadFloristRevenueData(processingDate);

            _LSTReciprocityVO = this.load_LST_and_Reciprocity(floristsMap);
            
            /* 
             * Insert data for the current month of processing.
             * This florist id dataset is then used for all subsequent calls to extract 
             * data for the florist weight calculation process.
             */ 
            floristDAO.saveFloristWeightCalcData(floristsMap);

            // load  rolling avg for revenue and rebate, for the last x months
            this.addFloristAvgRevRebData(floristsMap, processingDate);

            // load LST and reciprocity information for the last x months
            this.addLSTReciprocityHistory(floristsMap, processingDate);
            
            /*
             * In order to roll up to the top level florist, create a 
             * representative vo for each unique linking key.
             * This vo has the cumulative figures for all florists that have the
             * same linking key
             */ 
            Map topLevelFloristsMap = this.rollUpToTopLevelFlorist(floristsMap);
            /*
             * The following are first calculated for top level florists, on 
             * the cumulative figures. The weights are then propagated to all 
             * other parent florists, which have the same linking key.
             * 
             * Also, these weights do not apply to florists that are Send-Only,
             * Order Gatherers, and Grocery Stores. These are already excluded 
             * in the top level florist list.
             */
            this.calculateFloristBaseWeights(topLevelFloristsMap);            
            this.calculateFloristLSTReciprocityWeights(topLevelFloristsMap);
            this.propagateFloristWeights(floristsMap, topLevelFloristsMap);
            
            this.assignWeightToGroceryStoresOrderGatherersSendOnly(floristsMap);
            
            // final save
            floristDAO.saveFloristWeightCalcData(floristsMap);
            this.archiveFloristRevenueDataFiles();
            
            this.buildFloristWeightCalcAuditReport(processingDate);
            this.notifyFloristWeightCalcAuditReportReady();
          }
          else
          {
            logger.error("Files are locked on the FTP server. Skip processing...");
          }          
      }
      else
      {
        logger.error(remoteFile1 + " or " + remoteFile2 + " are not available on the FTP server. Skip processing...");
      }          
      
    } catch(Throwable t)
    {
      // send the system message
      this.sendSystemMessage(t);
      throw t;      
    } finally 
    {
      if (processing) 
      {
        // log back in to unlock files, and logout
        ftpHelper.login("FloristRevenueData");
        if (ftpHelper.isLoggedIn()) 
        {                  
          if (ftpHelper.isLocked(LOCKFILE)) 
          {
            ftpHelper.unlock(LOCKFILE); 
          }
          ftpHelper.logout();          
        }            
      }      
      else 
      {
        if (ftpHelper.isLoggedIn()) 
        {                  
          ftpHelper.logout();          
        }                    
      }
    }              
    
  }
  
  /**
   * Sends a system message
   * 
   * @param t
   */
  private void sendSystemMessage(Throwable t)
  {
    Connection con = null;
    try 
    {
      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
      // write the stack trace to the log_message variable
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw, true);
      t.printStackTrace(pw);
      String logMessage = "Failed to load florist weight calculation data to the staging tables" + "\n" + sw.toString();
      pw.close();        
      
      SystemMessage.send(logMessage, "FLORIST WEIGHT CALCULATION SERVICE", SystemMessage.LEVEL_PRODUCTION, "ERROR", con);
    } catch (Exception ex) 
    {
      logger.error(ex);
    } finally 
    {
      try 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
        
      } catch (Exception ex2) 
      {
        logger.error(ex2);
      }      
    }
    
  }
   

  /**
   * Loads low sending threshold and recprocity data file.
   * It also sets the current month's information on each florist, in the collection of florists.
   * 
   * @param floristsMap
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws XPathExpressionException 
   */
  private LSTReciprocityVO load_LST_and_Reciprocity(Map floristsMap)
    throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException 
  {

    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    FileProcessor fileProcessor = new FileProcessor();    
        
    // process member file
    logger.debug("Begin processing florist revenue data file");
    Map map = fileProcessor.processLSTReciprocityFile( configUtil.getProperty("filenames.xml", "LST-reciprocity-data-file") );
    logger.debug("End processing florist revenue data file");
    
    // get the values for USD
    LSTReciprocityVO _LSTReciprocityVO = (LSTReciprocityVO)map.get("USD");
    
    FloristWeightCalcVO floristWeightCalcVO = null;
    
    // load the current month's data on the florist map
    for(Iterator iter = floristsMap.keySet().iterator(); iter.hasNext();)
    {
      floristWeightCalcVO = (FloristWeightCalcVO) floristsMap.get(iter.next());
      floristWeightCalcVO.setLowSendingThresholdValue(_LSTReciprocityVO.getLowSendingThresholdValue());
      floristWeightCalcVO.setReciprocityRatio(_LSTReciprocityVO.getReciprocityRatio());
    }
    
    return _LSTReciprocityVO;
  }
 
  /**
   * Loads the data in the florist revenue data files.
   * 
   * @return 
   * @param processingDate
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  private Map loadFloristRevenueData(GregorianCalendar processingDate) 
    throws IOException, SAXException, ParserConfigurationException, TransformerException , SQLException, Exception
  {
    Map map;

    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    FileProcessor fileProcessor = new FileProcessor();    
        
    // process member file
    logger.debug("Begin processing florist revenue data file");
    map = fileProcessor.processFloristRevenueFile( configUtil.getProperty("filenames.xml", "florist-revenue-data-file"), processingDate );
    logger.debug("End processing florist revenue data file");

    return map;
  }
 
  /**
   * In order to roll up to the top level florist, create a 
   * representative vo for each unique linking key.
   * This vo has the cumulative figures for all florists that have the
   * same linking key
   * 
   * It excludes Send-Only, Order Gatherers, and Grocery Stores in the
   * list that is returned
   * 
   * @param floristsMap
   * @return 
   */
  private Map rollUpToTopLevelFlorist(Map floristsMap)
    throws Exception
  {
    HashMap topLevelFloristsMap = new HashMap();
    
    ObjectCopyUtil objectCopyUtil = new ObjectCopyUtil();
    
    FloristWeightCalcVO vo = null, topLevelFloristVO = null;
    BigDecimal averageRevenue = null, averageRebate = null, _cumTotalRevenue = null, _cumTotalRebate = null;
    BigDecimal numberOfMonths = new BigDecimal(Double.parseDouble( ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "REVENUE_REBATE_AVERAGE_NUMBER_OF_MONTHS") ));
    int totalClearedOrders = 0, ordersSentFromFTD = 0;
    
    for(Iterator iter = floristsMap.keySet().iterator(); iter.hasNext();)
    {
      vo = (FloristWeightCalcVO) floristsMap.get((String)iter.next());
      
      //(excluding Send-Only, Order Gathers, and Grocery Stores) 
      if (vo.getGroceryStoreFlag().equals("N")
          && vo.getOrderGathererFlag().equals("N")
          && vo.getSendOnlyFlag().equals("N")) 
      {
        if (! topLevelFloristsMap.containsKey(vo.getTopLevelFloristKey()) ) 
        {
            // clone a deep copy
            topLevelFloristsMap.put(vo.getTopLevelFloristKey(), (FloristWeightCalcVO) objectCopyUtil.deepCopy(vo));
        }
       /*
        * add the following figures to the representative toplevelvo for that key,
        * to arrive at a cumulative figure for all florists with the same
        * linking key.
        */
        topLevelFloristVO = (FloristWeightCalcVO) topLevelFloristsMap.get(vo.getTopLevelFloristKey());

        // total revenue
        topLevelFloristVO.setTotalRevenue( (topLevelFloristVO.getTotalRevenue() == null)?vo.getTotalRevenue():topLevelFloristVO.getTotalRevenue().add(vo.getTotalRevenue()));
        // rebate
        topLevelFloristVO.setRebate( (topLevelFloristVO.getRebate() == null)?vo.getRebate():topLevelFloristVO.getRebate().add(vo.getRebate()));
        // total cleared orders
        totalClearedOrders = topLevelFloristVO.getTotalClearedOrders() + vo.getTotalClearedOrders();
        topLevelFloristVO.setTotalClearedOrders(totalClearedOrders);
        // orders sent from ftd
        ordersSentFromFTD = topLevelFloristVO.getOrdersSentFromFTD() + vo.getOrdersSentFromFTD();
        topLevelFloristVO.setOrdersSentFromFTD(ordersSentFromFTD);
        
        // rework average revenue
        averageRevenue = topLevelFloristVO.getAverageRevenue();
        _cumTotalRevenue = (averageRevenue == null)?(vo.getAverageRevenue().multiply(numberOfMonths)):(averageRevenue.multiply(numberOfMonths)).add(vo.getAverageRevenue().multiply(numberOfMonths));
        averageRevenue = _cumTotalRevenue.divide(numberOfMonths, BigDecimal.ROUND_DOWN);
        topLevelFloristVO.setAverageRevenue(averageRevenue);
        
        // rework average rebate
        averageRebate = topLevelFloristVO.getAverageRebate();
        _cumTotalRebate = (averageRebate == null)?(vo.getAverageRebate().multiply(numberOfMonths)):(averageRebate.multiply(numberOfMonths)).add(vo.getAverageRebate().multiply(numberOfMonths));
        averageRebate = _cumTotalRebate.divide(numberOfMonths, BigDecimal.ROUND_DOWN);
        topLevelFloristVO.setAverageRebate(averageRebate);
        
        
        /*
        * LST and Reciprocity History; 
        * rollup total cleared orders and orders sent from ftd, 
        * for each month to the top level florist
        */
        
        Map topLevelLSTReciprocityHistory = topLevelFloristVO.getLSTReciprocityHistory();
        Map voLSTReciprocityHistory = vo.getLSTReciprocityHistory();
        
        if (topLevelLSTReciprocityHistory == null) 
        {
            topLevelFloristVO.setLSTReciprocityHistory(voLSTReciprocityHistory);
        }
        else 
        {
          String dateKey = null;
          LSTReciprocityVO topLevelLSTRecVO = null, _LSTRecVO = null;
          
          for(Iterator iter2 = topLevelLSTReciprocityHistory.keySet().iterator(); iter2.hasNext();)
          {
            // for each month of history
            dateKey = (String)iter2.next();
            
            topLevelLSTRecVO = topLevelFloristVO.getLSTReciprocityHistory(dateKey);
            _LSTRecVO = vo.getLSTReciprocityHistory(dateKey);
            
            // if the parent florist has data for that date
            if (_LSTRecVO != null) 
            {
              // rollup total cleared orders for that month
              totalClearedOrders = topLevelLSTRecVO.getTotalClearedOrders() + _LSTRecVO.getTotalClearedOrders();
              topLevelLSTRecVO.setTotalClearedOrders(totalClearedOrders);
              // rollup orders sent from ftd for that month
              ordersSentFromFTD = topLevelLSTRecVO.getOrdersSentFromFTD() + _LSTRecVO.getOrdersSentFromFTD();
              topLevelLSTRecVO.setOrdersSentFromFTD(ordersSentFromFTD);                    
            }
          }
        }
        
        
      }
    } // end for
    logger.debug("Rolled up all parent florists to " + topLevelFloristsMap.size() + " top level florists" );
    return topLevelFloristsMap;
  }
  
  
  /**
   * Calculates florists base weights, which is based on ranking net revenue.
   * 
   * @param topLevelFloristsMap
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.transform.TransformerException
   */
  private void calculateFloristBaseWeights(Map topLevelFloristsMap)
    throws ParserConfigurationException , IOException, SAXException, TransformerException
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    
    Collection values = topLevelFloristsMap.values();    
    
    Object[] topLevelFloristsArray = values.toArray();
    // sort based on the average net revenue
    Arrays.sort(topLevelFloristsArray, new FloristTotalRevenueComparator());
    
    // assign percentile
    double percentile;
    FloristWeightCalcVO topLevelFloristVO = null;
        
    for (int i = 0; i < topLevelFloristsArray.length; i++) 
    {
      topLevelFloristVO = (FloristWeightCalcVO) topLevelFloristsArray[i];
      
      float step_1 = topLevelFloristsArray.length - i;
      float step_2 = step_1 / topLevelFloristsArray.length;
      float step_3 = step_2 * 100;
      percentile = Math.floor(step_3); // rounded
      topLevelFloristVO.setPercentile(percentile);
      
      //assign base weight, based on percentile
      if (percentile > 90) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_ABOVE_90")) );
      }
      else if (percentile > 80 && percentile < 91) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_80_AND_90")) );
      }
      else if (percentile > 70 && percentile < 81) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_70_AND_80")) );
      }
      else if (percentile > 60 && percentile < 71) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_60_AND_70")) );
      }
      else if (percentile > 50 && percentile < 61) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_50_AND_60")) );
      }
      else if (percentile > 40 && percentile < 51) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_40_AND_50")) );
      }
      else if (percentile > 30 && percentile < 41) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_30_AND_40")) );
      }
      else if (percentile > 20 && percentile < 31) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_20_AND_30")) );
      }
      else if (percentile > 10 && percentile < 21) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BETWEEN_10_AND_20")) );
      }
      else if ( percentile < 11) 
      {
          topLevelFloristVO.setBaseWeight( Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_PERCENTILE_BELOW_10")) );
      }
          
    }// end for
    
  }
  
  /**
   * Calculates florist low sending threshold and reciprocity weights
   * 
   * @param topLevelFloristsMap
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.transform.TransformerException
   */
  private void calculateFloristLSTReciprocityWeights(Map topLevelFloristsMap)
    throws ParserConfigurationException , IOException, SAXException, TransformerException
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    FloristWeightCalcVO topLevelFloristVO = null;
    LSTReciprocityVO _LSTReciprocityVO = null;
    Map _LSTReciprocityHistoryMap = null;
    String dateKey = null;
    
    int maximumWeight = Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "MAXIMUM_POINTS"));
    int _LSTAvgMonths = Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "LST_AVERAGE_NUMBER_OF_MONTHS"));
    int reciprocityAvgMonths = Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "RECIPROCITY_AVERAGE_NUMBER_OF_MONTHS"));
    int _LSTWeight = Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_MEETING_LST_CRITERIA"));          
    int reciprocityWeight = Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_MEETING_RECIPROCITY_CRITERIA"));
    String topLevelFloristKey = null;
    
    // top level florist map is organized by the linking key
    for(Iterator iter = topLevelFloristsMap.keySet().iterator(); iter.hasNext();)
    {
      topLevelFloristKey = (String)iter.next();      
      topLevelFloristVO = (FloristWeightCalcVO) topLevelFloristsMap.get(topLevelFloristKey);
      
      // get the history for each florist
      _LSTReciprocityHistoryMap = topLevelFloristVO.getLSTReciprocityHistory();
      
      // assume true; negated below
      boolean assignLSTWeight = true;
      boolean assignReciprocityWeight = true;
      // variable to count the number of months of data processed for a florist
      int monthCount = 0;
      float reciprocityCutOff = 0f, ordersSentFromFTD = 0f, reciprocityRatio = 0f;
      
      // loop through historical LST and reciprocity data, organized by months
      for(Iterator iter2 = _LSTReciprocityHistoryMap.keySet().iterator(); iter2.hasNext();monthCount++)
      {
        dateKey = (String)iter2.next();
        
        // for each month of history
        _LSTReciprocityVO = (LSTReciprocityVO) _LSTReciprocityHistoryMap.get(dateKey);
        
        /******************BEGIN LST********************************************/
        /*
         * if the criteria was not met for any single month then don't bother 
         * to check for the remaining months. The florist will not qualify
         * for the kicker anyway
         */ 
        if (assignLSTWeight) 
        {
          // check to see whether low sending threshold criteria was not met
          if ( _LSTReciprocityVO.getTotalClearedOrders() < _LSTReciprocityVO.getLowSendingThresholdValue() ) 
          {
            /*
             * The criteria should have been met for every single year to qualify. 
             * Flag it to indicate to not assign any LST weight.
             */
             assignLSTWeight = false;          
          }
        }
        /******************END LST**********************************************/
        
        /******************BEGIN RECIPROCITY************************************/
        /*
         * if the criteria was not met for any single month then don't bother 
         * to check for the remaining months. The florist will not qualify
         * for the kicker anyway
         */ 
        if (assignReciprocityWeight) 
        {
          // if both values are zero, then they get no reciprocity weight
          if (   _LSTReciprocityVO.getTotalClearedOrders() == 0 
              && _LSTReciprocityVO.getOrdersSentFromFTD() == 0)
          {
              /*
               * The criteria should have been met for every single year to qualify. 
               * Flag it to indicate to not assign any reciprocity weight.
               */
               assignReciprocityWeight = false;            
          }
          // if no orders were generated by them
          else if (_LSTReciprocityVO.getTotalClearedOrders() == 0) 
          {
              /*
               * The criteria should have been met for every single year to qualify. 
               * Flag it to indicate to not assign any reciprocity weight.
               */
               assignReciprocityWeight = false;
          }
          /*
           * if both values are greater than zero then check to see whether 
           * reciprocity ratio criteria  was not met
           */
          else if (   _LSTReciprocityVO.getTotalClearedOrders() > 0  
                   && _LSTReciprocityVO.getOrdersSentFromFTD() > 0) 
          {
              /*
               * For every x orders there should be 1 order           
               *                  OR
               * totalClearedOrders >= ordersSentFromFTD * 1/reciprocityRatio
               */
              ordersSentFromFTD = _LSTReciprocityVO.getOrdersSentFromFTD();
              reciprocityRatio = _LSTReciprocityVO.getReciprocityRatio();
              reciprocityCutOff =   ordersSentFromFTD/ reciprocityRatio;
              if(  _LSTReciprocityVO.getTotalClearedOrders() < reciprocityCutOff )
              {
                /*
                 * The criteria should have been met for every single year to qualify. 
                 * Flag it to indicate to not assign any reciprocity weight.
                 */
                 assignReciprocityWeight = false;
              }              
          } 
            
        }
        /********************END RECIPROCITY************************************/
        
      }// end inner for loop
      
      /*** assign weights if atleast x months of data was processed ***/
      if (monthCount < _LSTAvgMonths) 
      {
          assignLSTWeight = false;
      }
      
      if (monthCount < reciprocityAvgMonths) 
      {
          assignReciprocityWeight = false;
      }
      
      
        /******************ASSIGN WEIGHTS**************************************/

      // assign weights based on the 2 flags
      if (assignLSTWeight ) 
      {
          // assign LST weight
          topLevelFloristVO.setLowSendingThresholdWeight(_LSTWeight);
      }
      if (assignReciprocityWeight) 
      {
          // assign reciprocity weight
          topLevelFloristVO.setReciprocityWeight(reciprocityWeight);
      }
      
    } // end outer for loop
    
  }
  
  /**
   * Propagates the weights that were assigned to the top level florists, to all
   * the parent florists connected by the linking key.
   * 
   * @param floristsMap
   * @param topLevelFloristsMap
   */
  private void propagateFloristWeights(Map floristsMap, Map topLevelFloristsMap)
  {
    logger.debug("Begin propagating florist weights");
    FloristWeightCalcVO topLevelFloristVO = null, parentFloristVO = null;
    HashMap newFloristsMap = new HashMap();
    
    // create a new collection of all florists organized by the linking key
    for(Iterator iter = floristsMap.keySet().iterator(); iter.hasNext();)
    {
      parentFloristVO = (FloristWeightCalcVO) floristsMap.get((String)iter.next()); 
        if (! newFloristsMap.containsKey(parentFloristVO.getTopLevelFloristKey())) 
        {
            ArrayList list = new ArrayList();
            list.add(parentFloristVO);
            newFloristsMap.put(parentFloristVO.getTopLevelFloristKey(), list);
        }
        else
        {
            ArrayList list = (ArrayList) newFloristsMap.get(parentFloristVO.getTopLevelFloristKey());
            list.add(parentFloristVO);             
        }
    }// end for
    
    int florists = 0;
    // top level florist map is organized by the linking key
    for(Iterator iter = topLevelFloristsMap.keySet().iterator(); iter.hasNext();)
    {
      topLevelFloristVO = (FloristWeightCalcVO) topLevelFloristsMap.get((String)iter.next());
      // From the newly created collection, extract the list of florists with the same linking key
      ArrayList list = (ArrayList)newFloristsMap.get(topLevelFloristVO.getTopLevelFloristKey());
      for(Iterator iter2 = list.iterator(); iter2.hasNext();)
      {          
        parentFloristVO = (FloristWeightCalcVO) iter2.next();
        //(excluding Send-Only, Order Gathers, and Grocery Stores) 
        if (parentFloristVO.getGroceryStoreFlag().equals("N")
            && parentFloristVO.getOrderGathererFlag().equals("N")
            && parentFloristVO.getSendOnlyFlag().equals("N")) 
        {
          parentFloristVO.setPercentile(topLevelFloristVO.getPercentile());          
          parentFloristVO.setBaseWeight(topLevelFloristVO.getBaseWeight());
          parentFloristVO.setLowSendingThresholdWeight(topLevelFloristVO.getLowSendingThresholdWeight());
          parentFloristVO.setReciprocityWeight(topLevelFloristVO.getReciprocityWeight());
          florists++;
        }
      }// end inner for loop
    }// end outer for loop
    logger.debug("Propagated weights to " + florists + " florists");
    logger.debug("End propagating florist weights");
  }
  
  /**
   * Assigns weights to florists flagged as grocery stores, order gatherers, and send only
   * 
   * @param floristsMap
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.transform.TransformerException
   */
  private void assignWeightToGroceryStoresOrderGatherersSendOnly(Map floristsMap)
    throws ParserConfigurationException , IOException, SAXException, TransformerException
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    FloristWeightCalcVO parentFloristVO = null;
    int groceryStoreCount = 0, orderGathererCount = 0, sendOnlyCount = 0;
    
    for(Iterator iter = floristsMap.keySet().iterator(); iter.hasNext();)
    {
      parentFloristVO = (FloristWeightCalcVO) floristsMap.get((String)iter.next()); 
      
      if (parentFloristVO.getGroceryStoreFlag().equals("Y")) 
      {
          parentFloristVO.setGroceryStoreWeight(Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_GROCERY_STORE")) );
          groceryStoreCount++;
      }
      else if (parentFloristVO.getOrderGathererFlag().equals("Y")) 
      {
          parentFloristVO.setOrderGathererWeight(Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_ORDER_GATHERER")) );
          orderGathererCount++;
      }
      else if (parentFloristVO.getSendOnlyFlag().equals("Y")) 
      {
          parentFloristVO.setSendOnlyWeight(Integer.parseInt(configUtil.getProperty("florist_weight_calc_config.xml", "POINTS_FOR_SEND_ONLY")) );
          sendOnlyCount++;
      }
      
    } // end for loop
    logger.debug("Assigned weights to " 
                  + groceryStoreCount + " grocery stores ");
    
  }
  
  
   /**
   * Loads the average revenue and rebate data for the parent florists, specified
   * earlier.
   * The current rebate and revenue data is included in the average
   * 
   * @param floristsMap
   * @param processingDate
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.lang.Exception
   */
  private void addFloristAvgRevRebData(Map floristsMap, GregorianCalendar processingDate)
    throws SQLException, IOException, SAXException, ParserConfigurationException, TransformerException , Exception
  {
    GregorianCalendar beginDate = new GregorianCalendar();
    beginDate.setTimeInMillis(processingDate.getTimeInMillis());
    GregorianCalendar endDate = new GregorianCalendar();
    
    //rollback beginDate to the beginning of the Month, x months ago
    beginDate.add(Calendar.MONTH, Integer.parseInt( "-" + ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "REVENUE_REBATE_AVERAGE_NUMBER_OF_MONTHS")));
    beginDate.roll(Calendar.DATE, (1- beginDate.get(Calendar.DATE)));
    
    /*
     * The end date is the same as the value of previous month
     * This is done to include the data for the month of processing
     */ 
   endDate.setTimeInMillis(processingDate.getTimeInMillis());

    logger.debug("Begin date:: " + beginDate.getTime() );
    logger.debug("End date:: " + endDate.getTime() );

    Map map = floristDAO.getFloristAvgRevRebData(processingDate, beginDate, endDate);  
    FloristWeightCalcVO floristWeightCalcVO = null, revRebInfoVO;
    
    // load the data to the florist map
    for(Iterator iter = map.keySet().iterator(); iter.hasNext();)
    {
      revRebInfoVO = (FloristWeightCalcVO) map.get((String)iter.next());
      floristWeightCalcVO = (FloristWeightCalcVO) floristsMap.get(revRebInfoVO.getFloristID());
      if (floristWeightCalcVO != null) 
      {
        floristWeightCalcVO.setAverageRebate(revRebInfoVO.getAverageRebate());
        floristWeightCalcVO.setAverageRevenue(revRebInfoVO.getAverageRevenue());          
      }
    }            
  }

   /**
   * Adds low sending threshold and reciprocity history information to all 
   * florists in the collection.
   * 
   * The history is added as a Map, where the key is the date formatted as 
   * dd/MMM/yy. The key is of type java.lang.String
   * 
   * @param floristsMap
   * @param processingDate
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.lang.Exception
   */
  private void addLSTReciprocityHistory(Map floristsMap, GregorianCalendar processingDate)
    throws SQLException, IOException, SAXException, ParserConfigurationException, TransformerException , Exception
  {
    GregorianCalendar beginDate = new GregorianCalendar();
    beginDate.setTimeInMillis(processingDate.getTimeInMillis());
    GregorianCalendar endDate = new GregorianCalendar();
     
    //rollback beginDate to the beginning of the Month, x months ago
    beginDate.add(Calendar.MONTH, Integer.parseInt( "-" + ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "LST_AVERAGE_NUMBER_OF_MONTHS")));
    beginDate.roll(Calendar.DATE, (1- beginDate.get(Calendar.DATE)));
    
    /*
     * The end date is the same as the value of previous month
     * This is done to include the data for the month of processing
     */ 
    endDate.setTimeInMillis(processingDate.getTimeInMillis());

    logger.debug("Begin date:: " + beginDate.getTime() );
    logger.debug("End date:: " + endDate.getTime() );
    
    Map _LSTReciprocityMap = new HashMap(), _LSTMap = new HashMap(), reciprocityMap = new HashMap();
    FloristWeightCalcVO floristWeightCalcVO = null;
    LSTReciprocityVO _LSTReciprocityVO = null, reciprocityVO = null;
    
    /**********************LST*************************************************/
    /*
     * Returns a map of maps, where the key corresponds to the 
     * florist id and the map contains the history.
     */
    _LSTMap = floristDAO.getLSTHistory(processingDate, beginDate, endDate);        
    
    /***********************RECIPROCITY****************************************/
    // reset begin date
    beginDate.setTimeInMillis(processingDate.getTimeInMillis());
    //rollback beginDate to the beginning of the Month, x months ago
    beginDate.add(Calendar.MONTH, Integer.parseInt("-" + ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "RECIPROCITY_AVERAGE_NUMBER_OF_MONTHS")));
    beginDate.roll(Calendar.DATE, (1- beginDate.get(Calendar.DATE)));


    logger.debug("Begin date:: " + beginDate.getTime() );
    logger.debug("End date:: " + endDate.getTime() );
   
    /*
     * Returns a map of maps, where the key corresponds to the 
     * florist id and the map contains the history.
     */
    reciprocityMap = floristDAO.getReciprocityHistory(processingDate, beginDate, endDate);  
    /***************************************************************************/

    String floristIDkey = null;
    /*
     * combine both maps into one
     */
    // 1. load the LST History data to _LSTReciprocityMap
    for(Iterator iter = _LSTMap.keySet().iterator(); iter.hasNext();)
    {      
      floristIDkey = (String)iter.next();
      _LSTReciprocityMap.put(floristIDkey, _LSTMap.get(floristIDkey));
    }
    
    HashMap _LSTReciprocityHistory = null, reciprocityHistory = null;
    String dateKey = null;
    
    // 2. update the reciprocity data in _LSTReciprocityMap
    // for each florist id
    for(Iterator iter = reciprocityMap.keySet().iterator(); iter.hasNext();)
    {
      floristIDkey = (String)iter.next();
      reciprocityHistory = (HashMap)reciprocityMap.get(floristIDkey);
      _LSTReciprocityHistory = (HashMap) _LSTReciprocityMap.get(floristIDkey);
      
      // for each month of history, obtain data from reciprocity map and update it in the LSTReciprocity map
      for(Iterator iter2 = _LSTReciprocityHistory.keySet().iterator(); iter2.hasNext();)
      {
        dateKey = (String) iter2.next();
        _LSTReciprocityVO = (LSTReciprocityVO) _LSTReciprocityHistory.get(dateKey);
        
        if (_LSTReciprocityVO != null) 
        {
          reciprocityVO = (LSTReciprocityVO) reciprocityHistory.get(dateKey);
          
          if (reciprocityVO != null) 
          {
            _LSTReciprocityVO.setOrdersSentFromFTD(reciprocityVO.getOrdersSentFromFTD());
            _LSTReciprocityVO.setReciprocityRatio(reciprocityVO.getReciprocityRatio());                        
          }          
        }
      }
    }
    
    // load the reciprocity and lst data history to the florist map
    for(Iterator iter = _LSTReciprocityMap.keySet().iterator(); iter.hasNext();)
    {
      floristIDkey = (String)iter.next();
      // for each florist
      floristWeightCalcVO = (FloristWeightCalcVO)floristsMap.get(floristIDkey);
      
      if (floristWeightCalcVO != null) 
      {
        // load the history map
        floristWeightCalcVO.setLSTReciprocityHistory((Map)_LSTReciprocityMap.get(floristIDkey));          
      }
    }
    
  }


  /**
   * Archives the Florist Revenue Data files
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  private void archiveFloristRevenueDataFiles()
    throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException
  {
    // archive files
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    logger.debug("Archiving files");
    
    ArrayList fileNames = new ArrayList();
    fileNames.add(configUtil.getProperty("filenames.xml", "florist-revenue-data-file"));
    fileNames.add(configUtil.getProperty("filenames.xml", "LST-reciprocity-data-file"));
    
    new FTPHelper().archiveFile("FloristRevenueData",fileNames);
    
  }

  /**
   * Builds the florist weights calculation audit report
   * 
   * @param processingDate
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private void buildFloristWeightCalcAuditReport(GregorianCalendar processingDate)
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
    floristDAO.buildFloristWeightCalcAuditReport(processingDate);
  }
  
  /**
   * Notifies that the florist weight calculation audit report is ready
   * 
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private void notifyFloristWeightCalcAuditReportReady()
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
    ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
    
    String recipConfigName = ConfigurationConstants.EMAIL_NOTIFY_RECIP_FLORIST_WEIGHT;
    String fromAddress = configurationUtil.getProperty("notification_config.xml", "FROM_ADDRESS");
    String subject = configurationUtil.getProperty("notification_config.xml", "FLORIST_WEIGHT_CALC_AUDIT_REPORT_AVAILABLE_SUBJECT");
    String notificationContent = configurationUtil.getProperty("notification_config.xml", "FLORIST_WEIGHT_CALC_AUDIT_REPORT_AVAILABLE_MESSAGE");
    
    floristDAO.sendNotification(recipConfigName, fromAddress, subject, notificationContent);
  }
  
  
}
