
package com.ftd.applications.lmd.tests;
import com.ftd.applications.lmd.util.HttpHelper;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * @author Anshu Gaind
 * @version $Id: HttpHelperTest.java,v 1.3 2012/09/18 13:32:26 gsergeycvs Exp $
 */

public class HttpHelperTest extends TestCase 
{
  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public HttpHelperTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }
   
   public void testHttpHelper()
   {
     try 
     {
      HttpHelper httpHelper = new HttpHelper();
      
      
     } catch (Exception ex) 
     {
      ex.printStackTrace();
     } finally 
     {
     }
     
   }
  
   
  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    suite.addTest(new HttpHelperTest("testHttpHelper"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
   
}