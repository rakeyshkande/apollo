package com.ftd.applications.lmd.tests;
import com.ftd.applications.lmd.bo.FloristWeightCalcHandler;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * @author Anshu Gaind
 * @version $Id: FloristWeightCalcHandlerTest.java,v 1.2 2006/05/26 15:49:22 mkruger Exp $
 */

public class FloristWeightCalcHandlerTest extends TestCase 
{
  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public FloristWeightCalcHandlerTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }
   
         
   
   public void testInvoke()
   {
     try 
     {
      new FloristWeightCalcHandler().invoke("");
      
     } catch (Throwable ex) 
     {
      ex.printStackTrace();
     } finally 
     {
     }
     
   }
   
   
  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    suite.addTest(new FloristWeightCalcHandlerTest("testInvoke"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
   
}