package com.ftd.applications.lmd.vo;

import java.io.Serializable;

/**
 * @author kkeyan
 *
 */
public class ClosureOverrideVO implements Serializable {

	private String overrideDate;
	private String createdOn;
	
	public String getOverrideDate() {
		return overrideDate;
	}
	public void setOverrideDate(String overrideDate) {
		this.overrideDate = overrideDate;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	
	@Override
	public String toString() {
		return "ClosureOverrideVO [overrideDate=" + overrideDate + ", createdOn=" + createdOn + "]";
	}
	
	
	
	
}
