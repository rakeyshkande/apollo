package com.ftd.applications.lmd.vo;


/**
 * This class is an object representation of the field node in the 
 * members-file-parsing-logic XML configuration file.  
 * 
 * @author Anshu Gaind
 * @version $Id: ParseFieldVO.java,v 1.2 2006/05/26 15:49:22 mkruger Exp $
 */

public class ParseFieldVO 
{
  private String name;
  private String startPosition;
  private String length;

  public ParseFieldVO()
  {
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getStartPosition()
  {
    return startPosition;
  }

  public void setStartPosition(String startPosition)
  {
    this.startPosition = startPosition;
  }

  public String getLength()
  {
    return length;
  }

  public void setLength(String length)
  {
    this.length = length;
  }
}