/**
 * 
 */
package com.ftd.applications.lmd.vo;

import com.ftd.security.cache.vo.Field;
import com.ftd.security.cache.vo.LineItemVO;

/**
 * @author rlarson
 *
 */
public class OscarUploadCityStateSpreadsheetRowVO extends LineItemVO {

	public static final String FIELD_NAME_CITY = "CITY";
	public static final String FIELD_NAME_STATE_ID = "STATE_ID";
	public static final String FIELD_NAME_OSCAR_CITY_STATE = "OSCAR_CITY_STATE";
	
    private static final Integer COLUMNS = new Integer(3); 

    static {
    	OscarUploadCityStateSpreadsheetRowVO.setColumnSize(COLUMNS.intValue());
    }

    
	/**
	 * 
	 */
	public OscarUploadCityStateSpreadsheetRowVO() {
        // create fieldList if it's null.
        super();
        fieldList.add(new Field(FIELD_NAME_CITY, Field.TYPE_STRING, 0, 40, true));
        fieldList.add(new Field(FIELD_NAME_STATE_ID, Field.TYPE_STRING, 1, 2, true));
        fieldList.add(new Field(FIELD_NAME_OSCAR_CITY_STATE, Field.TYPE_STRING, 2, 1, true));
        
	}

	@Override
	public void validate() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
