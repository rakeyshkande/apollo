package com.ftd.applications.lmd.vo;
import java.util.Date;

public class FloristHistoryVO extends BaseVO
{
    private String userId;
    private String commentDate;
    private String commentType;
    private String comments;
    private String managerFlag;
    private String newCommentFlag;
    
    public FloristHistoryVO()
    {
    }
    
    public String getUserId()
    {
        return userId;
    }
    
    public void setUserId(String userId)
    {
        if(valueChanged(this.userId, userId))
        {
            setChanged(true);
        } 
        
        this.userId = userId;
    }
    
    public String getCommentDate()
    {
        return commentDate;
    }
    
    public void setCommentDate(String commentDate)
    {
        if(valueChanged(this.commentDate, commentDate))
        {
            setChanged(true);
        } 
        
        this.commentDate = commentDate;
    }
    
    public String getComments()
    {
        return comments;
    }
    
    public void setComments(String comments)
    {
        if(valueChanged(this.comments, comments))
        {
            setChanged(true);
        } 
        
        this.comments = comments;
    }

    public String getManagerFlag()
    {
        return managerFlag;
    }

    public void setManagerFlag(String managerFlag)
    {
        if(valueChanged(this.managerFlag, managerFlag))
        {
            setChanged(true);
        } 
        
        this.managerFlag = "N";
        if(managerFlag != null && managerFlag.equals("true")) 
        {
          this.managerFlag = "Y";          
        }
        if(managerFlag != null && managerFlag.equals("Y")) 
        {
          this.managerFlag = "Y";          
        }
    }

    public String getCommentType()
    {
        return commentType;
    }

    public void setCommentType(String commentType)
    {
        if(valueChanged(this.commentType, commentType))
        {
            setChanged(true);
        } 
        
        this.commentType = commentType;
    }

    public String getNewCommentFlag()
    {
        return newCommentFlag;
    }

    public void setNewCommentFlag(String newCommentFlag)
    {
        if(valueChanged(this.newCommentFlag, newCommentFlag))
        {
            setChanged(true);
        } 
        
        this.newCommentFlag = newCommentFlag;
    }
}