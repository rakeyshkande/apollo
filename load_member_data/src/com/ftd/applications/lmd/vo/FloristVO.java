package com.ftd.applications.lmd.vo;

import com.ftd.ftdutilities.FieldUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Date;

public class FloristVO extends BaseVO
{
    private String memberNumber;
    private String name;
    private String address;
    private String city;
    private String state;
    private String postalCode;
    private String phone;
    private String altPhone;
    private String fax;
    private String contactName;
    private String status;
    private Integer postalCodeCount;
    private String territory;
    private Integer sendingRank;
    private String lastUpdateDate;
    private Long ordersSent;
    private String lastUpdateUser;
    private String mercuryMachine;
    private String sundayDeliveryFlag;
    private String lockedFlag;
    private String lockedByUser;
    private String emailAddress;
    private Double minimumOrderAmount;
    private String recordType;
    private String vendorFlag;
    private List associatedFloristList  = new ArrayList();
    private Map codifiedProductMap = new HashMap();
	private List performanceSingleList = new ArrayList();
    private List performanceAssociatedList = new ArrayList();
    private List floristHistoryList   = new ArrayList();
    private Map postalCodeMap = new HashMap();
    private List<FloristBlockVO> floristBlockList = new ArrayList<FloristBlockVO>();
    private FloristWeightVO floristWeight = new FloristWeightVO();
    private List sourceFloristList = new ArrayList();
    private FloristSuspendVO floristSuspend = new FloristSuspendVO();
    private FloristClosureVO floristClosureVO= new FloristClosureVO();
    private Map citiesMap = new HashMap();

    public FloristVO()
    {
    }

    public String getMemberNumber()
    {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber)
    {
        if(valueChanged(this.memberNumber, memberNumber))
        {
            setChanged(true);
        }

        this.memberNumber = memberNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        if(valueChanged(this.name, name))
        {
            setChanged(true);
        }

        this.name = name;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        if(valueChanged(this.address, address))
        {
            setChanged(true);
        }

        this.address = address;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        if(valueChanged(this.city, city))
        {
            setChanged(true);
        }

        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        if(valueChanged(this.state, state))
        {
            setChanged(true);
        }

        this.state = state;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        if(valueChanged(this.postalCode, postalCode))
        {
            setChanged(true);
        }

        this.postalCode = postalCode;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        if(valueChanged(FieldUtils.stripNonNumeric(this.phone), FieldUtils.stripNonNumeric(phone)))
        {
            setChanged(true);
        }

        this.phone = phone;
    }

    public String getAltPhone()
    {
        return altPhone;
    }

    public void setAltPhone(String altPhone)
    {
        if(valueChanged(this.altPhone, altPhone))
        {
            setChanged(true);
        }

        this.altPhone = altPhone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        if(valueChanged(this.fax, fax))
        {
            setChanged(true);
        }

        this.fax = fax;
    }

    public String getContactName()
    {
        return contactName;
    }

    public void setContactName(String contactName)
    {
        if(valueChanged(this.contactName, contactName))
        {
            setChanged(true);
        }

        this.contactName = contactName;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        if(valueChanged(this.status, status))
        {
            setChanged(true);
        }

        this.status = status;
    }

    public Integer getPostalCodeCount()
    {
        return postalCodeCount;
    }

    public void setPostalCodeCount(Integer postalCodeCount)
    {
        if(valueChanged(this.postalCodeCount, postalCodeCount))
        {
            setChanged(true);
        }

        this.postalCodeCount = postalCodeCount;
    }

    public String getTerritory()
    {
        return territory;
    }

    public void setTerritory(String territory)
    {
        if(valueChanged(this.territory, territory))
        {
            setChanged(true);
        }

        this.territory = territory;
    }

    public Integer getSendingRank()
    {
        return sendingRank;
    }

    public void setSendingRank(Integer sendingRank)
    {
        if(valueChanged(this.sendingRank, sendingRank))
        {
            setChanged(true);
        }

        this.sendingRank = sendingRank;
    }

    public String getLastUpdateDate()
    {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate)
    {
        if(valueChanged(this.lastUpdateDate, lastUpdateDate))
        {
            setChanged(true);
        }

        this.lastUpdateDate = lastUpdateDate;
    }

    public Long getOrdersSent()
    {
        return ordersSent;
    }

    public void setOrdersSent(Long ordersSent)
    {
        if(valueChanged(this.ordersSent, ordersSent))
        {
            setChanged(true);
        }

        this.ordersSent = ordersSent;
    }

    public List getAssociatedFloristList()
    {
        return associatedFloristList;
    }

    public void setAssociatedFloristList(List associatedFloristList)
    {
        this.associatedFloristList = associatedFloristList;
    }

    public Map getCodifiedProductMap()
    {
        return codifiedProductMap;
    }

    public void setCodifiedProductMap(Map codifiedProductMap)
    {
        this.codifiedProductMap = codifiedProductMap;
    }

	public List getPerformanceSingleList()
    {
        return performanceSingleList;
    }

    public void setPerformanceSingleList(List performanceSingleList)
    {
        this.performanceSingleList = performanceSingleList;
    }

    public List getPerformanceAssociatedList()
    {
        return performanceAssociatedList;
    }

    public void setPerformanceAssociatedList(List performanceAssociatedList)
    {
        this.performanceAssociatedList = performanceAssociatedList;
    }

    public List getFloristHistoryList()
    {
        return floristHistoryList;
    }

    public void setFloristHistoryList(List floristHistoryList)
    {
        this.floristHistoryList = floristHistoryList;
    }

    public Map getPostalCodeMap()
    {
        return postalCodeMap;
    }

    public void setPostalCodeMap(Map postalCodeMap)
    {
        this.postalCodeMap = postalCodeMap;
    }

    public String getLastUpdateUser()
    {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser)
    {
        if(valueChanged(this.lastUpdateUser, lastUpdateUser))
        {
            setChanged(true);
        }

        this.lastUpdateUser = lastUpdateUser;
    }

    public String getMercuryMachine()
    {
        return mercuryMachine;
    }

    public void setMercuryMachine(String mercuryMachine)
    {
        if(valueChanged(this.mercuryMachine, mercuryMachine))
        {
            setChanged(true);
        }

        this.mercuryMachine = mercuryMachine;
    }

    public String getSundayDeliveryFlag()
    {
        return sundayDeliveryFlag;
    }

    public void setSundayDeliveryFlag(String sundayDeliveryFlag)
    {
        if(valueChanged(this.sundayDeliveryFlag, sundayDeliveryFlag))
        {
            setChanged(true);
        }

        this.sundayDeliveryFlag = sundayDeliveryFlag;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        if(valueChanged(this.emailAddress, emailAddress))
        {
            setChanged(true);
        }

        this.emailAddress = emailAddress;
    }

    public Double getMinimumOrderAmount()
    {
        return minimumOrderAmount;
    }

    public void setMinimumOrderAmount(Double minimumOrderAmount)
    {
        if(valueChanged(this.minimumOrderAmount, minimumOrderAmount))
        {
            setChanged(true);
        }

        this.minimumOrderAmount = minimumOrderAmount;
    }

    public List<FloristBlockVO> getFloristBlockList()
    {
        return floristBlockList;
    }

    public void setFloristBlockList(List<FloristBlockVO> floristBlockList)
    {
        this.floristBlockList = floristBlockList;
    }

    public FloristWeightVO getFloristWeight()
    {
        return floristWeight;
    }

    public void setFloristWeight(FloristWeightVO floristWeight)
    {
        this.floristWeight = floristWeight;
    }

    public String getRecordType()
    {
        return recordType;
    }

    public void setRecordType(String recordType)
    {
        this.recordType = recordType;
    }

    public void setChangeFlags(boolean flag)
    {
        this.setChanged(flag);

        List blockList = this.getFloristBlockList();
        if(blockList != null)
        {
            Iterator blockIterator = blockList.iterator();
            while(blockIterator.hasNext())
            {
                FloristBlockVO floristBlock = (FloristBlockVO) blockIterator.next();
                floristBlock.setChanged(flag);
            }
        }



        if(this.getFloristWeight() != null)
        {
            this.getFloristWeight().setChanged(flag);
        }

        Map codifiedMap = this.getCodifiedProductMap();
        Iterator codifiedIter = codifiedMap.entrySet().iterator();
        while(codifiedIter.hasNext())
        {
              Map.Entry e = (Map.Entry) codifiedIter.next();
              CodifiedProductVO codifiedVO = (CodifiedProductVO)codifiedMap.get(e.getKey());
              codifiedVO.setChanged(flag);
        }

        Map zipMap = this.getPostalCodeMap();
        Iterator zipIter = zipMap.entrySet().iterator();
        while(zipIter.hasNext())
        {
              Map.Entry e = (Map.Entry) zipIter.next();
              PostalCodeVO postalCode = (PostalCodeVO)zipMap.get(e.getKey());
              postalCode.setChanged(flag);
        }

        List historyList = this.getFloristHistoryList();
        if(historyList != null)
        {
            Iterator historyIter = historyList.iterator();
            while(historyIter.hasNext())
            {
                FloristHistoryVO floristHistory = (FloristHistoryVO) historyIter.next();
                floristHistory.setChanged(flag);
            }
        }

        List associatedFloristList = this.getAssociatedFloristList();
        if(associatedFloristList != null)
        {
            Iterator associatedFloristIter = associatedFloristList.iterator();
            while(associatedFloristIter.hasNext())
            {
                AssociatedFloristVO associatedFlorist = (AssociatedFloristVO) associatedFloristIter.next();
                associatedFlorist.setChanged(flag);
            }
        }

        Map cityMap = this.getCitiesMap();
        Iterator cityIter = cityMap.entrySet().iterator();
        while(cityIter.hasNext())
        {
              Map.Entry e = (Map.Entry) cityIter.next();
              CityVO cityVO = (CityVO) cityMap.get(e.getKey());
              cityVO.setChanged(flag);
        }

    }

    public String getLockedFlag()
    {
        return lockedFlag;
    }

    public void setLockedFlag(String lockedFlag)
    {
        this.lockedFlag = lockedFlag;
    }

    public String getLockedByUser()
    {
        return lockedByUser;
    }

    public void setLockedByUser(String lockedByUser)
    {
        this.lockedByUser = lockedByUser;
    }

    public String getVendorFlag()
    {
        return vendorFlag;
    }

    public void setVendorFlag(String vendorFlag)
    {
        if(valueChanged(this.vendorFlag, vendorFlag))
        {
            setChanged(true);
        }

        this.vendorFlag = vendorFlag;
    }

  public void setSourceFloristList(List sourceFloristList)
  {
    this.sourceFloristList = sourceFloristList;
  }

  public List getSourceFloristList()
  {
    return sourceFloristList;
  }

    public FloristSuspendVO getFloristSuspend()
    {
        return floristSuspend;
    }

    public void setFloristSuspend(FloristSuspendVO floristSuspend)
    {
        this.floristSuspend = floristSuspend;
    }

	public FloristClosureVO getFloristClosureVO() {
		return floristClosureVO;
	}

	public void setFloristClosureVO(FloristClosureVO floristClosureVO) {
		this.floristClosureVO = floristClosureVO;
	}

	public Map getCitiesMap() {
		return citiesMap;
	}

	public void setCitiesMap(Map citiesMap) {
		this.citiesMap = citiesMap;
	}
}
