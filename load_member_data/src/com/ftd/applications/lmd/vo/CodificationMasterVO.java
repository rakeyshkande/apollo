package com.ftd.applications.lmd.vo;
import javax.servlet.http.HttpServlet;

public class CodificationMasterVO  extends BaseVO
{

    private String previousCodificationID;
    private String codificationID;
    private String description;
    private String category_id;
    private String codifyAllFloristFlag;
    private String loadFromFileFlag;
    private String deleteFlag = "N";
    private String globalUnBlockFlag;
    private String codificationRequired = "N";
    private boolean codifyAllFloristFlagChanged = false;

    public CodificationMasterVO()
    {
    }

    public boolean isCodifyAllFloristFlagChanged()
    {
        return codifyAllFloristFlagChanged;
    }
    public void setCodifyAllFloristFlagChanged(boolean value)
    {
        codifyAllFloristFlagChanged = value;
    }


    public String getGlobalUnblockFlag()
    {
        return globalUnBlockFlag;
    }
    public void setGlobalUnblockFlag(String value)
    {
        globalUnBlockFlag = value;
    }


    public String getCodificationID()
    {
        return codificationID;
    }
    public void setCodificationID(String value)
    {
        if(valueChanged(this.codificationID, value))
        {
            setChanged(true);
        }     
        codificationID = value;
    }

    public String getPreviousCodificationID()
    {
        return previousCodificationID;
    }
    public void setPreviousCodificationID(String value)
    {
        if(valueChanged(this.previousCodificationID, value))
        {
            setChanged(true);
        }     
        previousCodificationID = value;
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String value)
    {
        if(valueChanged(this.description, value))
        {
            setChanged(true);
        }         
        description = value;
    }
    
    
    public String getCategory_id()
    {
        return category_id;
    }
    public void setCategory_id(String value)
    {
        if(valueChanged(this.category_id, value))
        {
            setChanged(true);
        }         
        category_id = value;        
    }
    
    
    public String getCodifyAllFloristFlag()
    {
        return codifyAllFloristFlag;
    }
    public void setCodifyAllFloristFlag(String value)
    {
        if(valueChanged(this.codifyAllFloristFlag, value))
        {
            setChanged(true);
        }         
        codifyAllFloristFlag = value;
    }
    
    
    public String getLoadFromFileFlag()
    {
        return loadFromFileFlag;
    }
    public void setLoadFromFileFlag(String value)
    {
        if(valueChanged(this.loadFromFileFlag, value))
        {
            setChanged(true);
        }         
        loadFromFileFlag = value;
    }
    
        
    public String getDeleteFlag()
    {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag)
    {
        if(valueChanged(this.deleteFlag, deleteFlag))
        {
            setChanged(true);
        } 
    
        this.deleteFlag = deleteFlag;
    }    
    
    public String getCodificationRequired()
    {
        return codificationRequired;
    }

    public void setCodificationRequired(String codReq)
    {
        if(valueChanged(this.codificationRequired, codReq))
        {
            setChanged(true);
        } 
    
        this.codificationRequired = codReq;
    }    
}