/**
 * 
 */
package com.ftd.applications.lmd.vo;

import com.ftd.security.cache.vo.Field;
import com.ftd.security.cache.vo.LineItemVO;

/**
 * @author rlarson
 *
 */
public class OscarExportSpreadsheetRowVO extends LineItemVO {

	public static final String FIELD_NAME_ZIP_CODE = "ZIP_CODE_ID";
	public static final String FIELD_NAME_CITY = "CITY";
	public static final String FIELD_NAME_STATE_ID = "STATE_ID";
	public static final String FIELD_NAME_OSCAR_ZIP = "OSCAR_ZIP";
	public static final String FIELD_NAME_OSCAR_CITY_ZIP = "OSCAR_CITY_STATE";
	
    private static final Integer COLUMNS = new Integer(5); 

    static {
    	OscarExportSpreadsheetRowVO.setColumnSize(COLUMNS.intValue());
    }

    
	/**
	 * 
	 */
	public OscarExportSpreadsheetRowVO() {
        // create fieldList if it's null.
        super();
        fieldList.add(new Field(FIELD_NAME_ZIP_CODE, Field.TYPE_STRING, 0, 10, true));
        fieldList.add(new Field(FIELD_NAME_CITY, Field.TYPE_STRING, 1, 40, true));
        fieldList.add(new Field(FIELD_NAME_STATE_ID, Field.TYPE_STRING, 2, 2, true));
        fieldList.add(new Field(FIELD_NAME_OSCAR_ZIP, Field.TYPE_STRING, 3, 1, true));
        fieldList.add(new Field(FIELD_NAME_OSCAR_CITY_ZIP, Field.TYPE_STRING, 4, 1, true));
        
	}

	@Override
	public void validate() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
