package com.ftd.applications.lmd.vo;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Date;

public class FloristSuspendResumeTimeVO 
{
  private String floristID;
  private Date suspendTime;
  private Date resumeTime;
  private int timezoneOffsetInMinutes;

  public FloristSuspendResumeTimeVO()
  {
  }

  public String getFloristID()
  {
    return floristID;
  }

  public void setFloristID(String floristID)
  {
    this.floristID = trim(floristID);
  }

  public Date getSuspendTime()
  {
    return suspendTime;
  }

  public void setSuspendTime(Date suspendTime)
  {
    this.suspendTime = suspendTime;
  }

  public Date getResumeTime()
  {
    return resumeTime;
  }

  public void setResumeTime(Date resumeTime)
  {
    this.resumeTime = resumeTime;
  }

  public int getTimezoneOffsetInMinutes()
  {
    return timezoneOffsetInMinutes;
  }

  public void setTimezoneOffsetInMinutes(int timezoneOffsetInMinutes)
  {
    this.timezoneOffsetInMinutes = timezoneOffsetInMinutes;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try 
    {
      sb.append("<florist-suspend-resume-time-vo>");
      
      Field[] fields = this.getClass().getDeclaredFields();
      
      for (int i = 0; i < fields.length; i++) 
      {        
        sb.append("<" + fields[i].getName() + ">");
        sb.append(fields[i].get(this));
        sb.append("</" + fields[i].getName() + ">");
      }
      sb.append("</florist-suspend-resume-time-vo>");
      
    } catch (Exception ex) 
    {
      // do nothing     
    }
    
    return sb.toString();
  }

}