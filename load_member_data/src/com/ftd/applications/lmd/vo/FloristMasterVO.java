package com.ftd.applications.lmd.vo;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


/**
 * @author Anshu Gaind
 * @version $Id: FloristMasterVO.java,v 1.2 2006/05/26 15:49:22 mkruger Exp $
 */

public class FloristMasterVO 
{
  private String floristID;
  private String mercuryFlag;
  private String topLevelFloristId;
  private String parentFloristId;
  private String phoneNumber;
  private String floristName;
  private String address;
  private String zipCode;
  private String cityStateNumber;
  private String listingType;
  private String ownersName;
  private String minimumOrderAmount;
  private String ftoFlag;
  private String floristWeight;
  private String initialWeight;
  private String city;
  private String state;
  private String internalLinkNumber;
  
  
  private List floristCodifiedProductVOList;
  private String tollFreeNumber;
  private String faxNumber;
  private String territory;
  
  public FloristMasterVO()
  {
    floristCodifiedProductVOList = new ArrayList();
  }

  public String getFloristID()
  {
    return floristID;
  }

  public void setFloristID(String floristID)
  {
    this.floristID = trim(floristID);
  }

  public String getMercuryFlag()
  {
    return mercuryFlag;
  }

  public void setMercuryFlag(String mercuryFlag)
  {
    this.mercuryFlag = trim(mercuryFlag);
  }

  public String getTopLevelFloristId()
  {
    return topLevelFloristId;
  }

  public void setTopLevelFloristId(String topLevelFloristId)
  {
    this.topLevelFloristId = trim(topLevelFloristId);
  }

  public String getParentFloristId()
  {
    return parentFloristId;
  }

  public void setParentFloristId(String parentFloristId)
  {
    this.parentFloristId = trim(parentFloristId);
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = trim(phoneNumber);
  }

  public String getFloristName()
  {
    return floristName;
  }

  public void setFloristName(String floristName)
  {
    this.floristName = trim(floristName);
  }

  public String getAddress()
  {
    return address;
  }

  public void setAddress(String address)
  {
    this.address = trim(address);
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = trim(zipCode);
  }

  public String getCityStateNumber()
  {
    return cityStateNumber;
  }

  public void setCityStateNumber(String cityStateNumber)
  {
    this.cityStateNumber = trim(cityStateNumber);
  }

  public String getListingType()
  {
    return listingType;
  }

  public void setListingType(String listingType)
  {
    this.listingType = trim(listingType);
  }

  public String getOwnersName()
  {
    return ownersName;
  }

  public void setOwnersName(String ownersName)
  {
    this.ownersName = trim(ownersName);
  }

  public String getMinimumOrderAmount()
  {
    return minimumOrderAmount;
  }

  public void setMinimumOrderAmount(String minimumOrderAmount)
  {
    this.minimumOrderAmount = trim(minimumOrderAmount);
  }

  public String getFtoFlag()
  {
    return ftoFlag;
  }

  public void setFtoFlag(String ftoFlag)
  {
    this.ftoFlag = trim(ftoFlag);
  }

  public String getFloristWeight()
  {
    return floristWeight;
  }

  public void setFloristWeight(String floristWeight)
  {
    this.floristWeight = trim(floristWeight);
  }

  public String getInitialWeight()
  {
    return initialWeight;
  }

  public void setInitialWeight(String initialWeight)
  {
    this.initialWeight = trim(initialWeight);
  }

  public String getCity()
  {
    return city;
  }

  public void setCity(String city)
  {
    this.city = trim(city);
  }

  public String getState()
  {
    return state;
  }

  public void setState(String state)
  {
    this.state = trim(state);
  }

  public List getFloristCodifiedProductVOList()
  {
    return floristCodifiedProductVOList;
  }
  
  public void setFloristCodifiedProductVO(FloristCodifiedProductVO floristCodifiedProductVO)
  {
    floristCodifiedProductVOList.add(floristCodifiedProductVO);
  }

  public String getTollFreeNumber()
  {
    return tollFreeNumber;
  }

  public void setTollFreeNumber(String tollFreeNumber)
  {
    this.tollFreeNumber = trim(tollFreeNumber);
  }

  public String getFaxNumber()
  {
    return faxNumber;
  }

  public void setFaxNumber(String faxNumber)
  {
    this.faxNumber = trim(faxNumber);
  }

  public String getInternalLinkNumber()
  {
    return internalLinkNumber;
  }

  public void setInternalLinkNumber(String intLinkNum)
  {
    this.internalLinkNumber = trim(intLinkNum);
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

  public String getTerritory()
  {
    return territory;
  }

  public void setTerritory(String territory)
  {
    this.territory = trim(territory);
  }


  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try 
    {
      sb.append("<florist-master-vo>");
      
      Field[] fields = this.getClass().getDeclaredFields();
      
      for (int i = 0; i < fields.length; i++) 
      {
        if (fields[i].getName().equals("floristCodifiedProductVOList")) 
        {
            sb.append("<florist-codified-product-vo-list>");
            
            FloristCodifiedProductVO floristCodifiedProductVO = null;
            
            for (Iterator iter = floristCodifiedProductVOList.iterator(); iter.hasNext();) 
            {
              floristCodifiedProductVO = (FloristCodifiedProductVO) iter.next();
              sb.append(floristCodifiedProductVO.toXML());
            }
            sb.append("</florist-codified-product-vo-list>");
        } else 
        {
          sb.append("<" + fields[i].getName() + ">");
          sb.append(fields[i].get(this));
          sb.append("</" + fields[i].getName() + ">");          
        }
        
      }
      sb.append("</florist-master-vo>");
      
    } catch (Exception ex) 
    {
      // do nothing
    }
    
    return sb.toString();
  }



}