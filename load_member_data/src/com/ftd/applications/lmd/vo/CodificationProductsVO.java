package com.ftd.applications.lmd.vo;
import javax.servlet.http.HttpServlet;

public class CodificationProductsVO  extends BaseVO
{


    private String productID;
    private String productName;
    private String status;
    private String codificationID;
    private String description;
    private String checkOEflag;
    private Integer flagSequence;
    private String deleteFlag = "N";

    
    
    public CodificationProductsVO()
    {
    }

    public String getProductID()
    {
        return productID;
    }
    public void setProductID(String value)
    {
        if(valueChanged(this.productID, value))
        {
            setChanged(true);
        }             
       productID = value;
    }




    public String getProductName()
    {
        return productName;
    }
    public void setProductName(String value)
    {
        if(valueChanged(this.productName, value))
        {
            setChanged(true);
        }             
       productName = value;
    }
    
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String value)
    {
        if(valueChanged(this.status, value))
        {
            setChanged(true);
        }             
      status  = value;
    }
    
    public String getCodificationID()
    {
        return codificationID;
    }
    public void setCodificationID(String value)
    {
        if(valueChanged(this.codificationID, value))
        {
            setChanged(true);
        }             
       codificationID = value;
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String value)
    {
        if(valueChanged(this.description, value))
        {
            setChanged(true);
        }             
       description = value;
    }
    
    public String getCheckOEflag()
    {
        return checkOEflag;
    }
    public void setCheckOEflag(String value)
    {
        if(valueChanged(this.checkOEflag, value))
        {
            setChanged(true);
        }             
       checkOEflag = value;
    }
    
    public Integer getFlagSequence()
    {
        return flagSequence;
    }
    public void setFlagSequence(Integer value)
    {
        if(valueChanged(this.flagSequence, value))
        {
            setChanged(true);
        }             
       flagSequence = value;
    }    

    public String getDeleteFlag()
    {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag)
    {
        if(valueChanged(this.deleteFlag, deleteFlag))
        {
            setChanged(true);
        } 
    
        this.deleteFlag = deleteFlag;
    }

}