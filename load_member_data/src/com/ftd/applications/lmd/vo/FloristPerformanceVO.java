package com.ftd.applications.lmd.vo;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.math.BigDecimal;



public class FloristPerformanceVO extends BaseVO
{
	private BigDecimal oSent;
	private BigDecimal oSales;
	private BigDecimal oFilled;
	private String pFilled;
	private BigDecimal pSales;
	private BigDecimal oForward;
	private String pForward;
	private BigDecimal oReject;
	private String pReject;
	private BigDecimal oCancel;
	private String pCancel;
	private String num;
	
	public FloristPerformanceVO()
	{
		
		
		
	}

	public BigDecimal getOSent()
	{
		return oSent;
	}

	public void setOSent(BigDecimal oSent)
	{
		this.oSent = oSent;
	}

	public BigDecimal getOSales()
	{
		return oSales;
	}

	public void setOSales(BigDecimal oSales)
	{
		this.oSales = oSales;
	}

	public BigDecimal getOFilled()
	{
		return oFilled;
	}

	public void setOFilled(BigDecimal oFilled)
	{
		this.oFilled = oFilled;
	}

	public String getPFilled()
	{
		return pFilled;
	}

	public void setPFilled(String pFilled)
	{
		this.pFilled = pFilled;
	}

	public BigDecimal getPSales()
	{
		return pSales;
	}

	public void setPSales(BigDecimal pSales)
	{
		this.pSales = pSales;
	}

	public BigDecimal getOForward()
	{
		return oForward;
	}

	public void setOForward(BigDecimal oForward)
	{
		this.oForward = oForward;
	}

	public String getPForward()
	{
		return pForward;
	}

	public void setPForward(String pForward)
	{
		this.pForward = pForward;
	}

	public BigDecimal getOReject()
	{
		return oReject;
	}

	public void setOReject(BigDecimal oReject)
	{
		this.oReject = oReject;
	}

	public String getPReject()
	{
		return pReject;
	}

	public void setPReject(String pReject)
	{
		this.pReject = pReject;
	}

	public BigDecimal getOCancel()
	{
		return oCancel;
	}

	public void setOCancel(BigDecimal oCancel)
	{
		this.oCancel = oCancel;
	}

	public String getPCancel()
	{
		return pCancel;
	}

	public void setPCancel(String pCancel)
	{
		this.pCancel = pCancel;
	}

	public String getNum()
	{
		return num;
	}

	public void setNum(String num)
	{
		this.num = num;
	}





}