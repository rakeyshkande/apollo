package com.ftd.applications.lmd.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kkeyan
 *
 */
public class FloristClosureVO implements Serializable{
	
	private String closureOverrideFlag;
	private List<DayClosureVO> floristDayclosureLst=new ArrayList<DayClosureVO>();
	private List<ClosureOverrideVO> closureOverrideVOLst=new ArrayList<ClosureOverrideVO>();
	
	
	public List<DayClosureVO> getFloristDayclosureLst() {
		return floristDayclosureLst;
	}
	public void setFloristDayclosureLst(List<DayClosureVO> floristDayclosureLst) {
		this.floristDayclosureLst = floristDayclosureLst;
	}
	public List<ClosureOverrideVO> getClosureOverrideVOLst() {
		return closureOverrideVOLst;
	}
	public void setClosureOverrideVOLst(List<ClosureOverrideVO> closureOverrideVOLst) {
		this.closureOverrideVOLst = closureOverrideVOLst;
	}
	public String getClosureOverrideFlag() {
		return closureOverrideFlag;
	}
	public void setClosureOverrideFlag(String closureOverrideFlag) {
		this.closureOverrideFlag = closureOverrideFlag;
	}
	
	
	

}
