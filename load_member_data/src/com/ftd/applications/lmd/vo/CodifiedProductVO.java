package com.ftd.applications.lmd.vo;

import java.util.Date;

public class CodifiedProductVO extends BaseVO
{
    private String id;
    private String name;
    private String category;
    private String blockedFlag;
    private Date blockStartDate;
    private Date blockEndDate;
    private String savedFlag;

    public CodifiedProductVO()
    {
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        if(valueChanged(this.id, id))
        {
            setChanged(true);
        }  
    
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        if(valueChanged(this.name, name))
        {
            setChanged(true);
        }
        
        this.name = name;
    }
    
    public String getBlockedFlag()
    {
        return blockedFlag;
    }
    
    public void setBlockedFlag(String blockedFlag)
    {
        if(valueChanged(this.blockedFlag, blockedFlag))
        {
            setChanged(true);
        }
        
        this.blockedFlag = blockedFlag;
    }
    
    public Date getBlockStartDate()
    {
        return blockStartDate;
    }
    
    public void setBlockStartDate(Date blockStartDate)
    {
        if(valueChanged(this.blockStartDate, blockStartDate))
        {
            setChanged(true);
        }
        
        this.blockStartDate = blockStartDate;
    }
    
    public Date getBlockEndDate()
    {
        return blockEndDate;
    }
    
    public void setBlockEndDate(Date blockEndDate)
    {
        if(valueChanged(this.blockEndDate, blockEndDate))
        {
            setChanged(true);
        }
        
        this.blockEndDate = blockEndDate;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        if(valueChanged(this.category, category))
        {
            setChanged(true);
        }
        
        this.category = category;
    }

    public String getSavedFlag()
    {
        return savedFlag;
    }

    public void setSavedFlag(String savedFlag)
    {
        if(valueChanged(this.savedFlag, savedFlag))
        {
            setChanged(true);
        }
        
        this.savedFlag = savedFlag;
    }



}