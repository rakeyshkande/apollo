package com.ftd.applications.lmd.vo;

import java.util.*;

public class ViewQueueVO extends BaseVO
{
    private String mercuryNumber;
    private String orderNumber;
    private Integer messageCount;
    private Date deliveryDate;
	private String messageDate;
    private String messageReason;
    private String floristName;
    private String floristGotoFlag;
    private String floristStatus;
    private String floristCity;
    private String floristState;
    private String floristNumber;
    private String productBlockFlag;
    private Date productUnblockDate;
    private String productName;
    private String productId;
    private String productCodifiedId;
    private String zipBlockFlag;
    private Date zipUnblockDate;
    private Integer zipFloristCount;
    private FloristBlockVO newFloristBlock = new FloristBlockVO();
    private FloristSuspendVO floristSuspend = new FloristSuspendVO();
    private Map postalCodeMap = new HashMap();
    private Map codifiedProductMap = new HashMap();
    private String lastUpdateUser;
    private String deliveryAddress; 
    private String lockedFlag;
    private String lockedByUser;
    private String mercuryId;
    private String reasonComment;
    private String deliveryZipCode;
    private List floristBlockList = new ArrayList();
    private String defaultUnblockDate;
    private List removeBlockList = new ArrayList();
    
    public ViewQueueVO() 
    {
    }
    
    public String getLastUpdateUser()
    {
        return lastUpdateUser;
    }
    
    public void setLastUpdateUser(String lastUpdateUser)
    {
        if(valueChanged(this.lastUpdateUser, lastUpdateUser))
        {
            setChanged(true);
        } 
        
        this.lastUpdateUser = lastUpdateUser;
    }
    
    public FloristBlockVO getNewFloristBlock()
    {
        return newFloristBlock;
    }
    
    public void setNewFloristBlock(FloristBlockVO newFloristBlock)
    {        
        this.newFloristBlock = newFloristBlock;
    }
    
    public Map getCodifiedProductMap()
    {
        return codifiedProductMap;
    }
    
    public void setCodifiedProductMap(Map codifiedProductMap)
    {      
        this.codifiedProductMap = codifiedProductMap;
    }
    
    public Map getPostalCodeMap()
    {
        return postalCodeMap;
    }
    
    public void setPostalCodeMap(Map postalCodeMap)
    {
        this.postalCodeMap = postalCodeMap;
    }
    
    public String getMercuryNumber()
    {
        return mercuryNumber;
    }
    
    public void setMercuryNumber(String mercuryNumber)
    {
        if(valueChanged(this.mercuryNumber, mercuryNumber))
        {
            setChanged(true);
        } 
        
        this.mercuryNumber = mercuryNumber;
    }
    
    public String getOrderNumber()
    {
        return orderNumber;
    }
    
    public void setOrderNumber(String orderNumber)
    {
        if(valueChanged(this.orderNumber, orderNumber))
        {
            setChanged(true);
        } 
        
        this.orderNumber = orderNumber;
    }
    
    public Integer getMessageCount()
    {
        return messageCount;
    }
    
    public void setMessageCount(Integer messageCount)
    {
        if(valueChanged(this.messageCount, messageCount))
        {
            setChanged(true);
        } 
        
        this.messageCount = messageCount;
    }
    
    public Date getDeliveryDate()
    {
        return deliveryDate;
    }
    
    public void setDeliveryDate(Date deliveryDate)
    {
        if(valueChanged(this.deliveryDate, deliveryDate))
        {
            setChanged(true);
        } 
        
        this.deliveryDate = deliveryDate;
    }
    
    public String getMessageDate()
    {
        return messageDate;
    }
    
    public void setMessageDate(String messageDate)
    {
        if(valueChanged(this.messageDate, messageDate))
        {
            setChanged(true);
        } 
        
        this.messageDate = messageDate;
    }
    
    public String getMessageReason()
    {
        return messageReason;
    }
    
    public void setMessageReason(String messageReason)
    {
        if(valueChanged(this.messageReason, messageReason))
        {
            setChanged(true);
        } 
        
        this.messageReason = messageReason;
    }
    
    public String getFloristName()
    {
        return floristName;
    }
    
    public void setFloristName(String floristName)
    {
        if(valueChanged(this.floristName, floristName))
        {
            setChanged(true);
        } 
        
        this.floristName = floristName;
    }
    
    public String getFloristGotoFlag()
    {
        return floristGotoFlag;
    }
    
    public void setFloristGotoFlag(String floristGotoFlag)
    {
        if(valueChanged(this.floristGotoFlag, floristGotoFlag))
        {
            setChanged(true);
        } 
        
        this.floristGotoFlag = floristGotoFlag;
    }
    
    public String getFloristStatus()
    {
        return floristStatus;
    }
    
    public void setFloristStatus(String floristStatus)
    {
        if(valueChanged(this.floristStatus, floristStatus))
        {
            setChanged(true);
        }      
    
        this.floristStatus = floristStatus;
    }
    
    public String getFloristCity()
    {
        return floristCity;
    }
    
    public void setFloristCity(String floristCity)
    {
        if(valueChanged(this.floristCity, floristCity))
        {
            setChanged(true);
        }
        
        this.floristCity = floristCity;
    }
    
    public String getFloristState()
    {
        return floristState;
    }
    
    public void setFloristState(String floristState)
    {
        if(valueChanged(this.floristState, floristState))
        {
            setChanged(true);
        }
        
        this.floristState = floristState;
    }
    
    public String getFloristNumber()
    {
        return floristNumber;
    }
    
    public void setFloristNumber(String floristNumber)
    {
        if(valueChanged(this.floristNumber, floristNumber))
        {
            setChanged(true);
        }
        
        this.floristNumber = floristNumber;
    }
    
    public String getProductBlockFlag()
    {
        return productBlockFlag;
    }
    
    public void setProductBlockFlag(String productBlockFlag)
    {
        if(valueChanged(this.productBlockFlag, productBlockFlag))
        {
            setChanged(true);
        }
        
        this.productBlockFlag = productBlockFlag;
    }
    
    public Date getProductUnblockDate()
    {
        return productUnblockDate;
    }
    
    public void setProductUnblockDate(Date productUnblockDate)
    {
        if(valueChanged(this.productUnblockDate, productUnblockDate))
        {
            setChanged(true);
        }
        
        this.productUnblockDate = productUnblockDate;
    }
    
    public String getProductName()
    {
        return productName;
    }
    
    public void setProductName(String productName)
    {
        if(valueChanged(this.productName, productName))
        {
            setChanged(true);
        }
        
        this.productName = productName;
    }
    
    public String getProductId()
    {
        return productId;
    }
    
    public void setProductId(String productId)
    {
        if(valueChanged(this.productId, productId))
        {
            setChanged(true);
        }
        
        this.productId = productId;
    }
    
    public String getProductCodifiedId()
    {
        return productCodifiedId;
    }
    
    public void setProductCodifiedId(String productCodifiedId)
    {
        if(valueChanged(this.productCodifiedId, productCodifiedId))
        {
            setChanged(true);
        }
        
        this.productCodifiedId = productCodifiedId;
    }
    
    public String getZipBlockFlag()
    {
        return zipBlockFlag;
    }
    
    public void setZipBlockFlag(String zipBlockFlag)
    {
        if(valueChanged(this.zipBlockFlag, zipBlockFlag))
        {
            setChanged(true);
        }
        
        this.zipBlockFlag = zipBlockFlag;
    }
    
    public Date getZipUnblockDate()
    {
        return zipUnblockDate;
    }
    
    public void setZipUnblockDate(Date zipUnblockDate)
    {
        if(valueChanged(this.zipUnblockDate, zipUnblockDate))
        {
            setChanged(true);
        }
        
        this.zipUnblockDate = zipUnblockDate;
    }
    
    public Integer getZipFloristCount()
    {
        return zipFloristCount;
    }
    
    public void setZipFloristCount(Integer zipFloristCount)
    {
        if(valueChanged(this.zipFloristCount, zipFloristCount))
        {
            setChanged(true);
        }
        
        this.zipFloristCount = zipFloristCount;
    }
  
    public String getDeliveryAddress()
    {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress)
    {
        if(valueChanged(this.deliveryAddress, deliveryAddress))
        {
            setChanged(true);
        }
        
        this.deliveryAddress = deliveryAddress;
    }

    public String getLockedFlag()
    {
        return lockedFlag;
    }

    public void setLockedFlag(String lockedFlag)
    {
        if(valueChanged(this.lockedFlag, lockedFlag))
        {
            setChanged(true);
        }
        
        this.lockedFlag = lockedFlag;
    }

    public String getLockedByUser()
    {
        return lockedByUser;
    }

    public void setLockedByUser(String lockedByUser)
    {
        if(valueChanged(this.lockedByUser, lockedByUser))
        {
            setChanged(true);
        }
        
        this.lockedByUser = lockedByUser;
    }

    public String getMercuryId()
    {
        return mercuryId;
    }

    public void setMercuryId(String mercuryId)
    {
        if(valueChanged(this.mercuryId, mercuryId))
        {
            setChanged(true);
        }
        
        this.mercuryId = mercuryId;
    }

    public String getReasonComment()
    {
        return reasonComment;
    }

    public void setReasonComment(String reasonComment)
    {
        if(valueChanged(this.reasonComment, reasonComment))
        {
            setChanged(true);
        }
        
        this.reasonComment = reasonComment;
    }
    
    public void setChangeFlags(boolean flag)
    {
        this.setChanged(flag);
        
        if(this.getFloristSuspend() != null) {
            this.getFloristSuspend().setChanged(flag);
        }

        if(this.getNewFloristBlock() != null) {
            this.getNewFloristBlock().setChanged(flag);
        }

        Iterator blockIterator = floristBlockList.iterator();
        while(blockIterator.hasNext())
        {
            FloristBlockVO floristBlockVO = (FloristBlockVO) blockIterator.next();
            floristBlockVO.setChanged(flag);
        }
       
        Iterator zipIter = postalCodeMap.entrySet().iterator();    
        while(zipIter.hasNext())
        {
              Map.Entry e = (Map.Entry) zipIter.next();
              PostalCodeVO vo = (PostalCodeVO)postalCodeMap.get(e.getKey());
              vo.setChanged(flag);
        }
        
        Iterator codifiedIter = codifiedProductMap.entrySet().iterator();    
        while(codifiedIter.hasNext())
        {
              Map.Entry e = (Map.Entry) codifiedIter.next();
              CodifiedProductVO codifiedVO = (CodifiedProductVO)codifiedProductMap.get(e.getKey());
              codifiedVO.setChanged(flag);
        }
    }

    public String getDeliveryZipCode()
    {
        return deliveryZipCode;
    }

    public void setDeliveryZipCode(String deliveryZipCode)
    {
        this.deliveryZipCode = deliveryZipCode;
    }

    public FloristSuspendVO getFloristSuspend()
    {
        return floristSuspend;
    }

    public void setFloristSuspend(FloristSuspendVO floristSuspend)
    {
        this.floristSuspend = floristSuspend;
    }

    public List getFloristBlockList()
    {
        return floristBlockList;
    }

    public void setFloristBlockList(List floristBlockList)
    {
        this.floristBlockList = floristBlockList;
    }

	public void setDefaultUnblockDate(String defaultUnblockDate) {
		this.defaultUnblockDate = defaultUnblockDate;
	}

	public String getDefaultUnblockDate() {
		return defaultUnblockDate;
	}

	public void setRemoveBlockList(List removeBlockList) {
		this.removeBlockList = removeBlockList;
	}

	public List getRemoveBlockList() {
		return removeBlockList;
	}
}
