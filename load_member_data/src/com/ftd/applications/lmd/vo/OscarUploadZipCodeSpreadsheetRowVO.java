/**
 * com.ftd.applications.lmd.vo.OscarUploadZipCodeSpreadsheetRowVO.java
 */
package com.ftd.applications.lmd.vo;

import com.ftd.security.cache.vo.Field;
import com.ftd.security.cache.vo.LineItemVO;

/**
 * @author rlarson
 *
 */
public class OscarUploadZipCodeSpreadsheetRowVO extends LineItemVO {

	public static final String FIELD_NAME_ZIP_CODE = "ZIP_CODE_ID";
	public static final String FIELD_NAME_CITY = "CITY";
	public static final String FIELD_NAME_STATE_ID = "STATE_ID";
	public static final String FIELD_NAME_OSCAR_ZIP = "OSCAR_ZIP";
	public static final String FIELD_NAME_OSCAR_CITY_STATE = "OSCAR_CITY_STATE";
	
    private static final Integer COLUMNS = new Integer(5); 

    static {
    	OscarUploadZipCodeSpreadsheetRowVO.setColumnSize(COLUMNS.intValue());
    }

    
	/**
	 * 
	 */
	public OscarUploadZipCodeSpreadsheetRowVO() {
        // create fieldList if it's null.
        super();
        fieldList.add(new Field(FIELD_NAME_ZIP_CODE, Field.TYPE_STRING, 0, 10, true));
        fieldList.add(new Field(FIELD_NAME_CITY, Field.TYPE_STRING, 1, 40, true));
        fieldList.add(new Field(FIELD_NAME_STATE_ID, Field.TYPE_STRING, 2, 2, true));
        fieldList.add(new Field(FIELD_NAME_OSCAR_ZIP, Field.TYPE_STRING, 3, 1, true));
        fieldList.add(new Field(FIELD_NAME_OSCAR_CITY_STATE, Field.TYPE_STRING, 4, 1, true));        
	}

	@Override
	public void validate() throws Exception {
		// TODO Auto-generated method stub
		
	}

}

/*
 * MAINTENANCE HISTORY 
 * $Log: OscarUploadZipCodeSpreadsheetRowVO.java,v $
 * Revision 1.2  2012/12/07 18:39:44  gsergeycvs
 * #0000
 * Merge to trunk from 5.11.1.5
 *
 * Revision 1.1.4.3  2012/11/10 22:49:50  mavinenicvs
 * #13513 24345
 *
 * Revision 1.1.4.2  2012/11/08 22:57:05  rlarsoncvs
 * #13513 24344 24345
 * Moved from BIRCHBOB branch
 *
 * Revision 1.1.2.2  2012/11/08 22:34:09  rlarsoncvs
 * #13513 24344 24345
 * Updates
 *
 *
 */