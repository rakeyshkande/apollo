/*
 * @(#) ManualMercuryFloristVO.java      1.1.2.2     2004/09/21
 * 
 * 
 */

package com.ftd.applications.lmd.vo;
import com.ftd.osp.utilities.plugins.Logger;
import java.util.Date;

/**
 * Value Object for Manual Mercury Florist data.
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/09/13  Initial Release.(RFL)
 * 1.1.2.2. 2004/09/21  Corrected setRestartDate() method.(RFL)
 * 
 */
public class ManualMercuryFloristVO  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    public static final String STATUS_ON = "ON";
    public static final String STATUS_SHUTDOWN = "SHUTDOWN";
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Date currentShutdownDate = null;
    private String currentShutdownRequestor = null;
    private Date currentRestartDate = null;
    private String currentRestartRequestor = null;
    


    public ManualMercuryFloristVO() 
    {
        super();
    }//end method
    
    
    /**
     * 
     */
    public Date getCurrentRestartDate() 
    {
        return currentRestartDate;
    }//end method
    
    
    /**
     * 
     */
    public String getCurrentRestartRequestor() 
    {
        return currentRestartRequestor;
    }//end method
    
    
    /**
     * 
     */
    public Date getCurrentShutdownDate() 
    {
        return currentShutdownDate;
    }//end method
    
    
    /**
     * 
     */
    public String getCurrentShutdownRequestor() 
    {
        return currentShutdownRequestor;
    }//end method
    
    
    /**
     * 
     */
    public String getCurrentStatus() 
    {
        if (this.getCurrentShutdownDate() == null)
        {
            return this.STATUS_ON;
        }//end if
        else 
        {
            return this.STATUS_SHUTDOWN;
        }//end else
        
    }//end method
    
    
    /**
     * 
     */
    public void setCurrentRestartDate(Date restartDate) 
    {
        currentRestartDate = restartDate;
        
    }//end method
    
    
    /**
     * 
     */
    public void setCurrentRestartRequestor(String requestor) 
    {
        currentRestartRequestor = requestor;
        
    }//end method
    
    
    /**
     * 
     */
    public void setCurrentShutdownDate(Date shutdownDate) 
    {
        currentShutdownDate = shutdownDate;
        
    }//end method
    
    
    /**
     * 
     */
    public void setCurrentShutdownRequestor(String requestor)
    {
        currentShutdownRequestor = requestor;
        
    }//end method
    
    
}//end class