package com.ftd.applications.lmd.vo;

import java.util.Date;

public class FloristSuspendVO extends BaseVO
{
    private String suspendType;
    private Date suspendStartDate;
    private Date suspendEndDate;
    private String suspendFlag;
    private String blockUser;

    public FloristSuspendVO()
    {
    }
    

    public String getSuspendType()
    {
        return suspendType;
    }

    public void setSuspendType(String suspendType)
    {
        if(valueChanged(this.suspendType, suspendType))
        {
            setChanged(true);
        } 
        
        this.suspendType = suspendType;
    }

    public Date getSuspendStartDate()
    {
        return suspendStartDate;
    }

    public void setSuspendStartDate(Date suspendStartDate)
    {
        if(valueChanged(this.suspendStartDate, suspendStartDate))
        {
            setChanged(true);
        } 
        
        this.suspendStartDate = suspendStartDate;
    }

    public Date getSuspendEndDate()
    {
        return suspendEndDate;
    }

    public void setSuspendEndDate(Date suspendEndDate)
    {
        if(valueChanged(this.suspendEndDate, suspendEndDate))
        {
            setChanged(true);
        } 
        
        this.suspendEndDate = suspendEndDate;
    }


    public String getSuspendFlag()
    {
        return suspendFlag;
    }

    public void setSuspendFlag(String suspendFlag)
    {
        if(valueChanged(this.suspendFlag, suspendFlag))
        {
            setChanged(true);
        } 
        
        this.suspendFlag = suspendFlag;
    }

    public String getBlockUser()
    {
        return blockUser;
    }

    public void setBlockUser(String blockUser)
    {
        if(valueChanged(this.blockUser, blockUser))
        {
            setChanged(true);
        } 
        
        this.blockUser = blockUser;
    }
}