package com.ftd.applications.lmd.vo;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.GregorianCalendar;

/**
 * @author Anshu Gaind
 * @version $Id: LSTReciprocityVO.java,v 1.2 2006/05/26 15:49:22 mkruger Exp $
 */

public class LSTReciprocityVO implements Serializable
{
  private String currencyCode;
  private int lowSendingThresholdValue;
  private int reciprocityRatio;
  private GregorianCalendar date;
  private int totalClearedOrders;
  private int ordersSentFromFTD;
  private String floristID;
  
  public LSTReciprocityVO()
  {
  }

  
  public String getCurrencyCode()
  {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode)
  {
    this.currencyCode = currencyCode;
  }

  public int getLowSendingThresholdValue()
  {
    return lowSendingThresholdValue;
  }

  public void setLowSendingThresholdValue(int lowSendingThresholdValue)
  {
    this.lowSendingThresholdValue = lowSendingThresholdValue;
  }

  public int getReciprocityRatio()
  {
    return reciprocityRatio;
  }

  public void setReciprocityRatio(int reciprocityRatio)
  {
    this.reciprocityRatio = reciprocityRatio;
  }

  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try 
    {
      sb.append("<LST-reciprocity-vo>");
      
      Field[] fields = this.getClass().getDeclaredFields();
      
      for (int i = 0; i < fields.length; i++) 
      {        
        sb.append("<" + fields[i].getName() + ">");
        if (fields[i].getName().equals("date")) 
        {
            sb.append(((GregorianCalendar)fields[i].get(this)).getTime());
        }
        else
        {
          sb.append(fields[i].get(this));
        }
        sb.append("</" + fields[i].getName() + ">");
      }
      sb.append("</LST-reciprocity-vo>");
      
    } catch (Exception ex) 
    {
      // do nothing     
    }
    
    return sb.toString();
  }

  public GregorianCalendar getDate()
  {
    return date;
  }

  public void setDate(GregorianCalendar date)
  {
    this.date = date;
  }

  public int getTotalClearedOrders()
  {
    return totalClearedOrders;
  }

  public void setTotalClearedOrders(int totalClearedOrders)
  {
    this.totalClearedOrders = totalClearedOrders;
  }

  public int getOrdersSentFromFTD()
  {
    return ordersSentFromFTD;
  }

  public void setOrdersSentFromFTD(int ordersSentFromFTD)
  {
    this.ordersSentFromFTD = ordersSentFromFTD;
  }

  public String getFloristID()
  {
    return floristID;
  }

  public void setFloristID(String floristID)
  {
    this.floristID = floristID;
  }

}