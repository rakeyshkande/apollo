package com.ftd.applications.lmd.vo;

/**
 * This class contains field names of the result list of the Florist Search and
 * the corresponding getter and setter methods.
 *
 * @author Mehul Patel
 */
 
public class SearchResultVO 
{
	private String flornum;
	private String name;
	private String zip;
    private String state;
    private String phone;
    private String sunday;
    private String mercury;
    private String status;
    private String go;
	private Integer finalWeight;
	private Integer adjustedWeight;
	private String city;


	public SearchResultVO()
	{

	}

    
	public String getFlornum()
	{
	    return flornum;
	}

	public void setFlornum(String sflornum)
	{
	    this.flornum = sflornum;
	}


    public void setName(String sname)
    {
        this.name = sname;
    }


    public String getName()
    {
        return name;
    }


    public void setZip(String szip)
    {
        this.zip = szip;
    }


    public String getZip()
    {
        return zip;
    }


    public void setState(String sstate)
    {
        this.state = sstate;
    }


    public String getState()
    {
        return state;
    }


    public void setPhone(String sphone)
    {
        this.phone = sphone;
    }


    public String getPhone()
    {
        return phone;
    }


    public void setSunday(String ssunday)
    {
        this.sunday = ssunday;
    }


    public String getSunday()
    {
        return sunday;
    }


    public void setMercury(String smercury)
    {
        this.mercury = smercury;
    }


    public String getMercury()
    {
        return mercury;
    }


    public void setStatus(String sstatus)
    {
        this.status = sstatus;
    }


    public String getStatus()
    {
        return status;
    }


    public void setGo(String sgo)
    {
        this.go = sgo;
    }


    public String getGo()
    {
        return go;
    }

	public Integer getFinalWeight()
	{
		return finalWeight;
	}

	public void setFinalWeight(Integer finalWeight)
	{
		this.finalWeight = finalWeight;
	}

	public Integer getAdjustedWeight()
	{
		return adjustedWeight;
	}

	public void setAdjustedWeight(Integer adjustedWeight)
	{
		this.adjustedWeight = adjustedWeight;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}



}