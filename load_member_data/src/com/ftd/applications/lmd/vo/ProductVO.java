package com.ftd.applications.lmd.vo;
import javax.servlet.http.HttpServlet;

public class ProductVO 
{
    private String novatorID;
    private String ftdID;
    private String name;
    private String status;

    public ProductVO()
    {
    }
    
    public String getNovatorID()
    {
        return novatorID;
    }
    public void setNovatorID(String value)
    {
        novatorID = value;
    }

    public String getStatus()
    {
        return status;
    }
    public void setStatus(String value)
    {
        status = value;
    }

    public String getFtdID()
    {
        return ftdID;
    }
    public void setFtdID(String value)
    {
        ftdID = value;
    }


    public String getName()
    {
        return name;
    }
    public void setName(String value)
    {
        name = value;
    }
    
}