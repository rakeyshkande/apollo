/*
 * @(#) GlobalParmsVO.java      1.1     2004/09/29
 * 
 * 
 */

package com.ftd.applications.lmd.vo;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.Date;

/**
 * Value Object for Manual Mercury Florist data.
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.1      2004/09/29  Initial Release.(RFL)
 * 
 * 
 */
public class GlobalParmsVO  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    public static final int GNADD_STATE_ON = 1;
    public static final int GNADD_STATE_OFF = 0;
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Date GNADDDate = null;
    private int GNADDLevel = 0;
    


    public GlobalParmsVO() 
    {
        super();
    }//end method
    
    
    /**
     * 
     */
    public Date getGNADDDate() 
    {
        return GNADDDate;
    }//end method
    
    
    /**
     * 
     */
    public int getGNADDState() 
    {
        return GNADDLevel;
    }//end method
    
    
    /**
     * 
     */
    public void setGNADDDate(Date newDate) 
    {
        GNADDDate = newDate;
        
    }//end method
    
    
    /**
     * 
     */
    public void setGNADDState(int GNADDState) 
    {
        GNADDLevel = GNADDState;
        
    }//end method
    
    
}//end class