package com.ftd.applications.lmd.vo;

import java.util.Date;

public class CityVO extends BaseVO
{
    private String floristId;
    private String floristName;
    private String floristCity;
    private String floristState;
    private String floristType;
    private Integer floristCount;
    private Date blockStartDate;
    private Date blockEndDate;
    private String sort1;
    private String sort2;

    public CityVO() {
    }

	public String getFloristId() {
		return floristId;
	}

	public void setFloristId(String floristId) {
		this.floristId = floristId;
	}

	public String getFloristName() {
		return floristName;
	}

	public void setFloristName(String floristName) {
		this.floristName = floristName;
	}

	public String getFloristCity() {
		return floristCity;
	}

	public void setFloristCity(String floristCity) {
		this.floristCity = floristCity;
	}

	public String getFloristState() {
		return floristState;
	}

	public void setFloristState(String floristState) {
		this.floristState = floristState;
	}

	public String getFloristType() {
		return floristType;
	}

	public void setFloristType(String floristType) {
		this.floristType = floristType;
	}

	public Integer getFloristCount() {
		return floristCount;
	}

	public void setFloristCount(Integer floristCount) {
		this.floristCount = floristCount;
	}

	public Date getBlockStartDate() {
		return blockStartDate;
	}

	public void setBlockStartDate(Date blockStartDate) {
        if(valueChanged(this.blockStartDate, blockStartDate)) {
            setChanged(true);
        } 
		this.blockStartDate = blockStartDate;
	}

	public Date getBlockEndDate() {
		return blockEndDate;
	}

	public void setBlockEndDate(Date blockEndDate) {
        if(valueChanged(this.blockEndDate, blockEndDate)) {
            setChanged(true);
        } 
		this.blockEndDate = blockEndDate;
	}

	public String getSort1() {
		return sort1;
	}

	public void setSort1(String sort1) {
		this.sort1 = sort1;
	}

	public String getSort2() {
		return sort2;
	}

	public void setSort2(String sort2) {
		this.sort2 = sort2;
	}

}