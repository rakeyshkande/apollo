/**
 * 
 */
package com.ftd.applications.lmd.vo;

/**
 * @author rlarson
 *
 */
public class ZipCodeVO 
{
	public static final String FIELD_NAME_ZIP_CODE = "ZIP_CODE_ID";
	public static final String FIELD_NAME_CITY = "CITY";
	public static final String FIELD_NAME_STATE_ID = "STATE";
	public static final String FIELD_NAME_OSCAR_ZIP = "OSCAR_ZIP_FLAG";
	public static final String FIELD_NAME_OSCAR_CITY_STATE = "OSCAR_CITY_STATE_FLAG";
	
	private String zipCode;
	private String city;
	private String stateId;
	private String oscarZipFlag;
	private String oscarCityStateFlag;
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the stateId
	 */
	public String getStateId() {
		return stateId;
	}
	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	/**
	 * @return the oscarZipFlag
	 */
	public String getOscarZipFlag() {
		return oscarZipFlag;
	}
	/**
	 * @param oscarZipFlag the oscarZipFlag to set
	 */
	public void setOscarZipFlag(String oscarZipFlag) {
		this.oscarZipFlag = oscarZipFlag;
	}
	/**
	 * @return the oscarCityStateFlag
	 */
	public String getOscarCityStateFlag() {
		return oscarCityStateFlag;
	}
	/**
	 * @param oscarCityStateFlag the oscarCityStateFlag to set
	 */
	public void setOscarCityStateFlag(String oscarCityStateFlag) {
		this.oscarCityStateFlag = oscarCityStateFlag;
	}

}