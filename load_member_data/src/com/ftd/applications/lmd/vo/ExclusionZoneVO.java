package com.ftd.applications.lmd.vo;

import java.util.Date;
import java.util.List;

public class ExclusionZoneVO {
	
	private String description;
	private Date blockStartDate;
	private Date blockEndDate;
	private List<String> blockCities;
	private List<String> blockStates;
	private List<String> blockZipCodes;
	private String userId;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getBlockStartDate() {
		return blockStartDate;
	}
	public void setBlockStartDate(Date blockStartDate) {
		this.blockStartDate = blockStartDate;
	}
	
	public Date getBlockEndDate() {
		return blockEndDate;
	}
	public void setBlockEndDate(Date blockEndDate) {
		this.blockEndDate = blockEndDate;
	}
	
	public List<String> getBlockCities() {
		return blockCities;
	}
	public void setBlockCities(List<String> blockCities) {
		this.blockCities = blockCities;
	}
	
	public List<String> getBlockStates() {
		return blockStates;
	}
	public void setBlockStates(List<String> blockStates) {
		this.blockStates = blockStates;
	}
	
	public List<String> getBlockZipCodes() {
		return blockZipCodes;
	}
	public void setBlockZipCodes(List<String> blockZipCodes) {
		this.blockZipCodes = blockZipCodes;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

}
