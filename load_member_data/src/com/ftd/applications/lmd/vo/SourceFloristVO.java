package com.ftd.applications.lmd.vo;

public class SourceFloristVO
  extends BaseVO
{
  private String sourceCode;
  private String description;
  private int priority;
  
  public SourceFloristVO()
  {
  }

  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = sourceCode;
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setPriority(int priority)
  {
    this.priority = priority;
  }

  public int getPriority()
  {
    return priority;
  }
}
