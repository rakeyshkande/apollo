package com.ftd.applications.lmd.vo;

public class HistoryVO 
{

    private String changeType;
    private String oldValue;
    private String newValue;
    private String field;
    private String additionialData;
    private String additionialFieldName;  
    private String dispositionId;
    
    public HistoryVO()
    {
    }
  
    public String getChangeType()
    {
        return changeType;
    }
    
    public void setChangeType(String value)
    {
        this.changeType = value;
    }  

    public String getAdditionialData()
    {
        return additionialData;
    }
    
    public void setAdditionialData(String value)
    {
        this.additionialData = value;
    }  

    public String getAdditionialFieldName()
    {
        return additionialFieldName;
    }
    
    public void setAdditionialFieldName(String value)
    {
        this.additionialFieldName = value;
    }  

    public String getField()
    {
        return field;
    }
    
    public void setField(String value)
    {
        this.field = value;
    }  


    public String getOldValue()
    {
        return oldValue;
    }
    
    public void setOldValue(String value)
    {
        this.oldValue = value;
    }  
    
    public String getNewValue()
    {
        return newValue;
    }
    
    public void setNewValue(String value)
    {
        this.newValue = value;
    }      

    public String getDispositionId()
    {
        return dispositionId;
    }

    public void setDispositionId(String dispositionId)
    {
        this.dispositionId = dispositionId;
    }
  
}