package com.ftd.applications.lmd.vo;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServlet;


public class CodificationMaintenanceVO  extends BaseVO
{

    private Map codificationProductMap;
    private Map codificationMasterMap;
    private String lastUpdateUser;
    

    public CodificationMaintenanceVO()
    {
    }
    
  public String getLastUpdateUser()
  {
    return lastUpdateUser;
  }

  public void setLastUpdateUser(String lastUpdateUser)
  {
    this.lastUpdateUser = lastUpdateUser;
  }    
    
    public Map getCodificationProductMap()
    {
        return codificationProductMap;
    }
    
    public void setCodificationProductMap(Map value)
    {
        codificationProductMap = value;
    }    

    public Map getCodificationMasterMap()
    {
        return codificationMasterMap;
    }
    
    public void setCodificationMasterMap(Map value)
    {
        codificationMasterMap = value;
    }    
    
    public void setChangeFlags(boolean flag)
    {
        this.setChanged(flag);
    
        Iterator codifiedIter = codificationProductMap.entrySet().iterator();    
        while(codifiedIter.hasNext())
        {
              Map.Entry e = (Map.Entry) codifiedIter.next();
              CodificationProductsVO codifiedVO = (CodificationProductsVO)codificationProductMap.get(e.getKey());
              codifiedVO.setChanged(flag);
        }     

        Iterator masterIter = codificationMasterMap.entrySet().iterator();    
        while(masterIter.hasNext())
        {
              Map.Entry e = (Map.Entry) masterIter.next();
              CodificationMasterVO masterVO = (CodificationMasterVO)codificationMasterMap.get(e.getKey());
              masterVO.setChanged(flag);
        }     

    }    

}