package com.ftd.applications.lmd.vo;

import java.util.Date;

public class PostalCodeVO extends BaseVO
{
    private String postalCode;
    private String city;
    private String cutoffTime;
    private Integer floristCount;
    private Date blockStartDate;
    private Date blockEndDate;
    private String blockedFlag;
    private String deleteFlag;
    private String savedFlag;
    private String assignedStatus;
    private String assignmentType;

    public PostalCodeVO()
    {
    }
    
    public String getPostalCode()
    {
        return postalCode;
    }
    
    public void setPostalCode(String postalCode)
    {
        if(valueChanged(this.postalCode, postalCode))
        {
            setChanged(true);
        } 
    
        this.postalCode = postalCode;
    }
    
    public String getCity()
    {
        return city;
    }
    
    public void setCity(String city)
    {
        if(valueChanged(this.city, city))
        {
            setChanged(true);
        } 
    
        this.city = city;
    }
    
    public String getCutoffTime()
    {
        return cutoffTime;
    }
    
    public void setCutoffTime(String cutoffTime)
    {
        if(valueChanged(this.cutoffTime, cutoffTime))
        {
            setChanged(true);
        } 
    
        this.cutoffTime = cutoffTime;
    }
    
    public Integer getFloristCount()
    {
        return floristCount;
    }
    
    public void setFloristCount(Integer floristCount)
    {
        if(valueChanged(this.floristCount, floristCount))
        {
            setChanged(true);
        } 
    
        this.floristCount = floristCount;
    }
    
    public Date getBlockStartDate()
    {
        return blockStartDate;
    }
    
    public void setBlockStartDate(Date blockStartDate)
    {
        if(valueChanged(this.blockStartDate, blockStartDate))
        {
            setChanged(true);
        } 
    
        this.blockStartDate = blockStartDate;
    }
    
    public Date getBlockEndDate()
    {
        return blockEndDate;
    }
    
    public void setBlockEndDate(Date blockEndDate)
    {
        if(valueChanged(this.blockEndDate, blockEndDate))
        {
            setChanged(true);
        } 
    
        this.blockEndDate = blockEndDate;
    }
    
    public String getBlockedFlag()
    {
        return blockedFlag;
    }
    
    public void setBlockedFlag(String blockedFlag)
    {
        if(valueChanged(this.blockedFlag, blockedFlag))
        {
            setChanged(true);
        } 
    
        this.blockedFlag = blockedFlag;
    }

    public String getDeleteFlag()
    {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag)
    {
        if(valueChanged(this.deleteFlag, deleteFlag))
        {
            setChanged(true);
        } 
    
        this.deleteFlag = deleteFlag;
    }

    public String getSavedFlag()
    {
        return savedFlag;
    }

    public void setSavedFlag(String savedFlag)
    {
        if(valueChanged(this.savedFlag, savedFlag))
        {
            setChanged(true);
        } 
    
        this.savedFlag = savedFlag;
    }
    
    public String getAssignedStatus()
    {
        return assignedStatus;
    }

    public void setAssignedStatus(String assStatus)
    {
        if(valueChanged(this.assignedStatus, assStatus))
        {
            setChanged(true);
        } 
    
        this.assignedStatus = assStatus;
    }
    
    public String getAssignmentType()
    {
        return assignmentType;
    }

    public void setAssignmentType(String assType)
    {
        if(valueChanged(this.assignmentType, assType))
        {
            setChanged(true);
        } 
    
        this.assignmentType = assType;
    }    
}