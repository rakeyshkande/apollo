package com.ftd.applications.lmd.vo;
import java.lang.reflect.Field;
import java.util.Iterator;


/**
 * @author Anshu Gaind
 * @version $Id: FloristCodifiedProductVO.java,v 1.2.160.1 2015/01/27 17:57:40 tschmigcvs Exp $
 */

public class FloristCodifiedProductVO 
{
  private String codifiedID;
  private String holidayPriceFlag;
  private String minPrice;
  private String hasMinimum;

  public FloristCodifiedProductVO()
  {
  }

  public String getCodifiedID()
  {
    return codifiedID;
  }

  public void setCodifiedID(String codifiedID)
  {
    this.codifiedID = trim(codifiedID);
  }

  public String getHolidayPriceFlag()
  {
    return holidayPriceFlag;
  }

  public void setHolidayPriceFlag(String holidayPriceFlag)
  {
    this.holidayPriceFlag = trim(holidayPriceFlag);
  }

  public String getMinPrice()
  {
    return minPrice;
  }

  public void setMinPrice(String minPrice)
  {
    this.minPrice = trim(minPrice);
  }

  public String getHasMinimum() {
	return hasMinimum;
}

public void setHasMinimum(String hasMinimum) {
	this.hasMinimum = hasMinimum;
}

private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try 
    {
      sb.append("<florist-codified-product-vo>");
      
      Field[] fields = this.getClass().getDeclaredFields();
      
      for (int i = 0; i < fields.length; i++) 
      {        
        sb.append("<" + fields[i].getName() + ">");
        sb.append(fields[i].get(this));
        sb.append("</" + fields[i].getName() + ">");
      }
      sb.append("</florist-codified-product-vo>");
      
    } catch (Exception ex) 
    {
      // do nothing     
    }
    
    return sb.toString();
  }


}