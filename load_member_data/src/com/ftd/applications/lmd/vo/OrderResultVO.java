package com.ftd.applications.lmd.vo;

/**
 * This class contains field names of the result list of the Florist Search and
 * the corresponding getter and setter methods.
 *
 * @author Mehul Patel
 */
 
public class OrderResultVO 
{
	private String mercNumber;
	private String orderNumber;
	private String orderDate;
	private String lastName;
	private String orderStatus;
	private String mercValue;
	private String sku;
	private String deliveryDate;

	public String getMercNumber()
	{
		return mercNumber;
	}

	public void setMercNumber(String mercNumber)
	{
		this.mercNumber = mercNumber;
	}

	public String getOrderNumber()
	{
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	public String getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(String orderDate)
	{
		this.orderDate = orderDate;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getOrderStatus()
	{
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus)
	{
		this.orderStatus = orderStatus;
	}

	public String getMercValue()
	{
		return mercValue;
	}

	public void setMercValue(String mercValue)
	{
		this.mercValue = mercValue;
	}

	public String getSku()
	{
		return sku;
	}

	public void setSku(String sku)
	{
		this.sku = sku;
	}

	public String getDeliveryDate()
	{
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}



}