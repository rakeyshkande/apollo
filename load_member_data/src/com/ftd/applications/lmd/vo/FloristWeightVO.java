package com.ftd.applications.lmd.vo;

import java.util.Date;

public class FloristWeightVO extends BaseVO
{
    private Integer initialWeight;
    private Integer finalWeight;
    private Integer adjustedWeight;
    private Date adjustedWeightStartDate;
    private Date adjustedWeightEndDate;
    private String gotoFlag;
    private String adjustedWeightPermFlag;
    private String mercurySuspendOverride;
    
    public FloristWeightVO()
    {
    }
    
    public String getGotoFlag()
    {
        return gotoFlag;
    }
    
    public void setGotoFlag(String gotoFlag)
    {
        if(valueChanged(this.gotoFlag, gotoFlag))
        {
            setChanged(true);
        } 
        
        this.gotoFlag = gotoFlag;
    }
    
    public Integer getInitialWeight()
    {
        return initialWeight;
    }
    
    public void setInitialWeight(Integer initialWeight)
    {
        if(valueChanged(this.initialWeight, initialWeight))
        {
            setChanged(true);
        } 
        
        this.initialWeight = initialWeight;
    }
    
    public Integer getFinalWeight()
    {
        return finalWeight;
    }
    
    public void setFinalWeight(Integer finalWeight)
    {
        if(valueChanged(this.finalWeight, finalWeight))
        {
            setChanged(true);
        } 
        
        this.finalWeight = finalWeight;
    }
    
    public Integer getAdjustedWeight()
    {
        return adjustedWeight;
    }
    
    public void setAdjustedWeight(Integer adjustedWeight)
    {
        if(valueChanged(this.adjustedWeight, adjustedWeight))
        {
            setChanged(true);
        } 
        
        this.adjustedWeight = adjustedWeight;
    }
    
    public Date getAdjustedWeightStartDate()
    {
        return adjustedWeightStartDate;
    }
    
    public void setAdjustedWeightStartDate(Date adjustedWeightStartDate)
    {
        if(valueChanged(this.adjustedWeightStartDate, adjustedWeightStartDate))
        {
            setChanged(true);
        } 
        
        this.adjustedWeightStartDate = adjustedWeightStartDate;
    }
    
    public Date getAdjustedWeightEndDate()
    {
        return adjustedWeightEndDate;
    }
    
    public void setAdjustedWeightEndDate(Date adjustedWeightEndDate)
    {
        if(valueChanged(this.adjustedWeightEndDate, adjustedWeightEndDate))
        {
            setChanged(true);
        } 
        
        this.adjustedWeightEndDate = adjustedWeightEndDate;
    }
    
    public String getMercurySuspendOverride()
    {
        return mercurySuspendOverride;
    }
    
    public void setMercurySuspendOverride(String mercurySuspendOverride)
    {
        if(valueChanged(this.mercurySuspendOverride, mercurySuspendOverride))
        {
            setChanged(true);
        } 
        
        this.mercurySuspendOverride = mercurySuspendOverride;
    }
    
    public String getAdjustedWeightPermFlag()
    {
        return adjustedWeightPermFlag;
    }

    public void setAdjustedWeightPermFlag(String adjustedWeightPermFlag)
    {
        if(valueChanged(this.adjustedWeightPermFlag, adjustedWeightPermFlag))
        {
            setChanged(true);
        } 
        
        this.adjustedWeightPermFlag = adjustedWeightPermFlag;
    }
}