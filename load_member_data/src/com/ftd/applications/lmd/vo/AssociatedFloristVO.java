package com.ftd.applications.lmd.vo;

public class AssociatedFloristVO extends BaseVO
{
    private String memberNumber;
    private String name;
    private String postalCode;
    private String state;
    private String phone;
    private String sundayFlag;
    private String mercuryFlag;
    private String status;
    private Integer postalCodeCount;
    
    public AssociatedFloristVO()
    {
    }

    public String getMemberNumber()
    {
        return memberNumber;
    }
    
    public void setMemberNumber(String memberNumber)
    {
        if(valueChanged(this.memberNumber, memberNumber))
        {
            setChanged(true);
        }  

        this.memberNumber = memberNumber;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        if(valueChanged(this.name, name))
        {
            setChanged(true);
        }  

        this.name = name;
    }
    
    public String getPostalCode()
    {
        return postalCode;
    }
    
    public void setPostalCode(String postalCode)
    {
        if(valueChanged(this.postalCode, postalCode))
        {
            setChanged(true);
        }  

        this.postalCode = postalCode;
    }
    
    public String getState()
    {
        return state;
    }
    
    public void setState(String state)
    {
        if(valueChanged(this.state, state))
        {
            setChanged(true);
        }  

        this.state = state;
    }
    
    public String getPhone()
    {
        return phone;
    }
    
    public void setPhone(String phone)
    {
        if(valueChanged(this.phone, phone))
        {
            setChanged(true);
        }  

        this.phone = phone;
    }
    
    public String getSundayFlag()
    {
        return sundayFlag;
    }
    
    public void setSundayFlag(String sundayFlag)
    {
        if(valueChanged(this.sundayFlag, sundayFlag))
        {
            setChanged(true);
        }  

        this.sundayFlag = sundayFlag;
    }
    
    public String getMercuryFlag()
    {
        return mercuryFlag;
    }
    
    public void setMercuryFlag(String mercuryFlag)
    {
        if(valueChanged(this.mercuryFlag, mercuryFlag))
        {
            setChanged(true);
        }  

        this.mercuryFlag = mercuryFlag;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public void setStatus(String status)
    {
        if(valueChanged(this.status, status))
        {
            setChanged(true);
        }  

        this.status = status;
    }

    public Integer getPostalCodeCount()
    {
        return postalCodeCount;
    }

    public void setPostalCodeCount(Integer postalCodeCount)
    {
        if(valueChanged(this.postalCodeCount, postalCodeCount))
        {
            setChanged(true);
        }  
        
        this.postalCodeCount = postalCodeCount;
    }
}