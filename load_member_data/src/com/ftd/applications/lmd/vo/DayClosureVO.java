package com.ftd.applications.lmd.vo;

import java.io.Serializable;

/**
 * @author kkeyan
 *
 */
public class DayClosureVO implements Serializable {
	private String day;
	private String jdeStatus;
	
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getJdeStatus() {
		return jdeStatus;
	}
	public void setJdeStatus(String jdeStatus) {
		this.jdeStatus = jdeStatus;
	}
	
	
	@Override
	public String toString() {
		return "DayClosureVO [day=" + day + ", jdeStatus=" + jdeStatus + "]";
	}

}
