package com.ftd.applications.lmd.vo;
import java.io.Serializable;

import java.lang.reflect.Field;

import java.math.BigDecimal;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Anshu Gaind
 * @version $Id: FloristWeightCalcVO.java,v 1.2 2006/05/26 15:49:22 mkruger Exp $
 */

public class FloristWeightCalcVO implements Serializable
{
  private String floristID;
  private String groceryStoreFlag;
  private String orderGathererFlag;
  private String sendOnlyFlag;
  private double percentile;
  private int baseWeight;
  private int lowSendingThresholdWeight;
  private int reciprocityWeight;
  private String topLevelFloristKey;
  private int totalClearedOrders;
  private int ordersSentFromFTD;
  private int lowSendingThresholdValue;
  private int reciprocityRatio;
  private GregorianCalendar date;
  private int groceryStoreWeight;
  private int maximumWeight;
  private int orderGathererWeight;
  private int sendOnlyWeight;
  
  /** transient variable will not be deep copied **/
  private transient BigDecimal totalRevenue;
  private transient BigDecimal rebate;
  private transient BigDecimal averageRevenue;
  private transient BigDecimal averageRebate;
  private transient Map LSTReciprocityHistoryMap;

  public FloristWeightCalcVO()
  {
  }

  public String getFloristID()
  {
    return floristID;
  }

  public void setFloristID(String floristID)
  {
    this.floristID = trim(floristID);
  }

  public BigDecimal getTotalRevenue()
  {
    return totalRevenue;
  }

  public void setTotalRevenue(BigDecimal totalRevenue)
  {
    this.totalRevenue = totalRevenue;
  }

  /**
   * Returns the lesser of the true final weight or the maximum weight.
   * 
   * @return 
   */
  public int getFinalWeight()
  {
    int finalWeight = this.getBaseWeight() 
                      + this.getLowSendingThresholdWeight() 
                      + this.getReciprocityWeight() 
                      + this.getGroceryStoreWeight()
                      + this.getOrderGathererWeight()
                      + this.getSendOnlyWeight();
    return finalWeight = (finalWeight > this.getMaximumWeight())?this.getMaximumWeight():finalWeight;    
  }



  public String getGroceryStoreFlag()
  {
    return groceryStoreFlag;
  }

  public void setGroceryStoreFlag(String groceryStoreFlag)
  {
    this.groceryStoreFlag = trim(groceryStoreFlag);
    // default to N
    this.groceryStoreFlag = (this.groceryStoreFlag.equals(""))?"N":this.groceryStoreFlag;
  }

  public String getOrderGathererFlag()
  {
    return orderGathererFlag;
  }

  public void setOrderGathererFlag(String orderGathererFlag)
  {
    this.orderGathererFlag = trim(orderGathererFlag);
    // default to N
    this.orderGathererFlag = (this.orderGathererFlag.equals(""))?"N":this.orderGathererFlag;
  }

  public String getSendOnlyFlag()
  {
    return sendOnlyFlag;
  }

  public void setSendOnlyFlag(String sendOnlyFlag)
  {
    this.sendOnlyFlag = trim(sendOnlyFlag);
    // default to N
    this.sendOnlyFlag = (this.sendOnlyFlag.equals(""))?"N":this.sendOnlyFlag;
  }

  public double getPercentile()
  {
    return percentile;
  }

  public void setPercentile(double percentile)
  {
    this.percentile = percentile;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try 
    {
      sb.append("<florist-weight-calc-vo>");
      
      Field[] fields = this.getClass().getDeclaredFields();
      
      for (int i = 0; i < fields.length; i++) 
      {        
        sb.append("<" + fields[i].getName() + ">");
        if (fields[i].getName().equals("date")) 
        {
            sb.append(((GregorianCalendar)fields[i].get(this)).getTime());
        }
        else
        {
          sb.append(fields[i].get(this));
        }
        sb.append("</" + fields[i].getName() + ">");
      }
      sb.append("</florist-weight-calc-vo>");
      
    } catch (Exception ex) 
    {
      // do nothing     
    }
    
    return sb.toString();
  }

  public int getBaseWeight()
  {
    return baseWeight;
  }

  public void setBaseWeight(int baseWeight)
  {
    this.baseWeight = baseWeight;
  }

  public int getLowSendingThresholdWeight()
  {
    return lowSendingThresholdWeight;
  }

  public void setLowSendingThresholdWeight(int lowSendingThresholdWeight)
  {
    this.lowSendingThresholdWeight = lowSendingThresholdWeight;
  }

  public int getReciprocityWeight()
  {
    return reciprocityWeight;
  }

  public void setReciprocityWeight(int reciprocityWeight)
  {
    this.reciprocityWeight = reciprocityWeight;
  }

  public String getTopLevelFloristKey()
  {
    return topLevelFloristKey;
  }

  public void setTopLevelFloristKey(String topLevelFloristKey)
  {
    this.topLevelFloristKey = topLevelFloristKey;
  }

  public int getTotalClearedOrders()
  {
    return totalClearedOrders;
  }

  public void setTotalClearedOrders(int totalClearedOrders)
  {
    this.totalClearedOrders = totalClearedOrders;
  }

  public int getOrdersSentFromFTD()
  {
    return ordersSentFromFTD;
  }

  public void setOrdersSentFromFTD(int ordersSentFromFTD)
  {
    this.ordersSentFromFTD = ordersSentFromFTD;
  }

  public BigDecimal getRebate()
  {
    return rebate;
  }

  public void setRebate(BigDecimal rebate)
  {
    this.rebate = rebate;
  }

  public BigDecimal getAverageRevenue()
  {
    return averageRevenue;
  }

  public void setAverageRevenue(BigDecimal averageRevenue)
  {
    this.averageRevenue = averageRevenue;
  }

  public BigDecimal getAverageRebate()
  {
    return averageRebate;
  }

  public void setAverageRebate(BigDecimal averageRebate)
  {
    this.averageRebate = averageRebate;
  }

  public Map getLSTReciprocityHistory()
  {
    return this.LSTReciprocityHistoryMap;
  }

  /**
   * The key is a date 
   * 
   * @param key
   * @return 
   */
  public LSTReciprocityVO getLSTReciprocityHistory(Object key)
  {
    return (LSTReciprocityVO) this.LSTReciprocityHistoryMap.get(key);
  }
  
  /**
   * The history is added as a Map, where the key is a date 
   * 
   * @param key
   * @param LSTReciprocity
   */
  public void setLSTReciprocityHistory(Object key, LSTReciprocityVO LSTReciprocity)
  {
    if (this.LSTReciprocityHistoryMap == null) 
    {
        this.LSTReciprocityHistoryMap = new HashMap();
    }
    this.LSTReciprocityHistoryMap.put(key, LSTReciprocity);
  }

  public void setLSTReciprocityHistory(Map _LSTReciprocityHistoryMap)
  {
    this.LSTReciprocityHistoryMap = _LSTReciprocityHistoryMap;
  }
  
  public int getLowSendingThresholdValue()
  {
    return lowSendingThresholdValue;
  }

  public void setLowSendingThresholdValue(int lowSendingThresholdValue)
  {
    this.lowSendingThresholdValue = lowSendingThresholdValue;
  }

  public int getReciprocityRatio()
  {
    return reciprocityRatio;
  }

  public void setReciprocityRatio(int reciprocityRatio)
  {
    this.reciprocityRatio = reciprocityRatio;
  }

  public GregorianCalendar getDate()
  {
    return date;
  }

  public void setDate(GregorianCalendar date)
  {
    this.date = date;
  }

  public int getGroceryStoreWeight()
  {
    return groceryStoreWeight;
  }

  public void setGroceryStoreWeight(int groceryStoreWeight)
  {
    this.groceryStoreWeight = groceryStoreWeight;
  }

  public int getMaximumWeight()
  {
    return maximumWeight;
  }

  public void setMaximumWeight(int maximumWeight)
  {
    this.maximumWeight = maximumWeight;
  }

  public int getOrderGathererWeight()
  {
    return orderGathererWeight;
  }

  public void setOrderGathererWeight(int orderGathererWeight)
  {
    this.orderGathererWeight = orderGathererWeight;
  }

  public int getSendOnlyWeight()
  {
    return sendOnlyWeight;
  }

  public void setSendOnlyWeight(int sendOnlyWeight)
  {
    this.sendOnlyWeight = sendOnlyWeight;
  }
  
}