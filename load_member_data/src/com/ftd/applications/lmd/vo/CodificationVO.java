package com.ftd.applications.lmd.vo;
import javax.servlet.http.HttpServlet;

public class CodificationVO 
{

    private String codificationId;
    private String categoryId;
    private String description;

    public CodificationVO()
    {
    }
    
    public String getCodificationId()
    {
        return codificationId;
    }
    public void setCodificationId(String value)
    {
       codificationId = value;
    }    


    public String getCategoryId()
    {
        return categoryId;
    }
    public void setCategoryId(String value)
    {
       categoryId = value;
    }    
    
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String value)
    {
       description = value;
    }        
}