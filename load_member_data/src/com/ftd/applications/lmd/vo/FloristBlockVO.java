package com.ftd.applications.lmd.vo;

import java.util.Date;

public class FloristBlockVO extends BaseVO
{
    private String blockType;
    private Date blockStartDate;
    private Date blockEndDate;
    private String blockFlag;
    private String blockUser;
    private boolean blockRemoved;
    private boolean blockNew;
    private String blockReason;

    public FloristBlockVO()
    {
    }
    
        public String getBlockType()
    {
        return blockType;
    }

    public void setBlockType(String blockType)
    {
        if(valueChanged(this.blockType, blockType))
        {
            setChanged(true);
        } 
        
        this.blockType = blockType;
    }

    public Date getBlockStartDate()
    {
        return blockStartDate;
    }

    public void setBlockStartDate(Date blockStartDate)
    {
        if(valueChanged(this.blockStartDate, blockStartDate))
        {
            setChanged(true);
        } 
        
        this.blockStartDate = blockStartDate;
    }

    public Date getBlockEndDate()
    {
        return blockEndDate;
    }

    public void setBlockEndDate(Date blockEndDate)
    {
        if(valueChanged(this.blockEndDate, blockEndDate))
        {
            setChanged(true);
        } 
        
        this.blockEndDate = blockEndDate;
    }

    public String getBlockFlag()
    {
        return blockFlag;
    }

    public void setBlockFlag(String blockFlag)
    {
        if(valueChanged(this.blockFlag, blockFlag))
        {
            setChanged(true);
        } 
        
        this.blockFlag = blockFlag;
    }

    public String getBlockUser()
    {
        return blockUser;
    }

    public void setBlockUser(String blockUser)
    {
        if(valueChanged(this.blockUser, blockUser))
        {
            setChanged(true);
        } 
        
        this.blockUser = blockUser;
    }

    public boolean isBlockRemoved()
    {
        return blockRemoved;
    }

    public void setBlockRemoved(boolean blockRemoved)
    {
        this.blockRemoved = blockRemoved;
    }

    public boolean isBlockNew()
    {
        return blockNew;
    }

    public void setBlockNew(boolean blockNew)
    {
        this.blockNew = blockNew;
    }

	public void setBlockReason(String blockReason) {
		this.blockReason = blockReason;
	}

	public String getBlockReason() {
		return blockReason;
	}
}