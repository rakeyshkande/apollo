/*
 * @(#) DataAccessException.java      1.1.2.1     2004/09/29
 * 
 * 
 */

package com.ftd.applications.lmd.exception;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.1.2.1      2004/09/29  Initial Release.(RFL)
 * 
 */
public class DataAccessException extends Exception  
{


    /**
     * 
     */
    public DataAccessException() 
    {
        super();
    }//end method
    
    
    /**
     * 
     * @param string
     */
    public DataAccessException(String string) 
    {
        super(string);
    }//end method
    
    
}//end class