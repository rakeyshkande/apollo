/*
 * @(#) ManualMercuryFloristException.java      1.0     2004/09/14
 * 
 * 
 */

package com.ftd.applications.lmd.exception;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/09/14  Initial Release.(RFL)
 * 
 */
public class ManualMercuryFloristException extends Exception  
{


    /**
     * 
     */
    public ManualMercuryFloristException() 
    {
        super();
    }//end method
    
    
    /**
     * 
     * @param string
     */
    public ManualMercuryFloristException(String string) 
    {
        super(string);
    }//end method
    
    
}//end class