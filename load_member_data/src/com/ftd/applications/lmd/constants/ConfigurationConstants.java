package com.ftd.applications.lmd.constants;

public interface ConfigurationConstants 
{
    public static final String LOAD_MEMBER_CONFIG_FILE = "load_member_config.xml";
    public static final String LOAD_MEMBER_CONFIG_CONTEXT = "LOAD_MEMBER_DATA_CONFIG";
    
    public static final String STANDALONE_FLAG = "STANDALONE_FLAG";
    public static final String SECURITY_APP_CONTEXT = "SECURITY_APP_CONTEXT";
    public static final String ERROR_PAGE_LOCATION = "ERROR_PAGE_LOCATION";
    public static final String SECURITY_LOCATION = "SECURITY_LOCATION";
    public static final String SECURITY_REDIRECT = "SECURITY_REDIRECT";
    public static final String SECURITY_DEFAULT_LOCATION = "SECURITY_DEFAULT_LOCATION";
    public static final String SECURITY_ADMIN_REDIRECT = "SECURITY_ADMIN_REDIRECT";
    public static final String DATASOURCE_NAME = "DATASOURCE_NAME";
    public static final String DATASOURCE_LOCATION = "DATASOURCE_LOCATION";
	public static final String JNDI_TRANSACTION_ENTRY = "JNDI_TRANSACTION_ENTRY";
    public static final String JNDI_USERTRANSACTION_ENTRY = "JNDI_USERTRANSACTION_ENTRY";
    public static final String TRANSACTION_TIMEOUT_IN_SECONDS = "TRANSACTION_TIMEOUT_IN_SECONDS";
    public static final String OVERRIDE_SECURITY_CHECK = "OVERRIDE_SECURITY_CHECK";
    public static final String OVERRIDE_SECURITY_USER = "OVERRIDE_SECURITY_USER";
    public static final String VIEW_QUEUE_ADDITIONAL_COMMENT = "VIEW_QUEUE_ADDITIONAL_COMMENT";
    public static final String COMMENTS_DISPLAY_MAX = "COMMENTS_DISPLAY_MAX";
    public static final String ENQUEUE_OFFSET_MINUTES = "ENQUEUE_OFFSET_MINUTES";
    public static final String EMAIL_NOTIFY_RECIP_FLORIST_WEIGHT = "FloristWeight_NotifyToAddress";
    public static final String EMAIL_NOTIFY_RECIP_FLORIST_DATA = "FloristData_NotifyToAddress";
}