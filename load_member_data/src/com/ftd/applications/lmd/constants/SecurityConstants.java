package com.ftd.applications.lmd.constants;

public interface SecurityConstants 
{
    public static final String INACTIVE_SEARCH = "FM Inactive Search";
    public static final String ADD_NEW = "FM Add New";
    public static final String PROFILE = "FM Profile";
    public static final String OVERRIDE_WEIGHT = "FM Override Weight";
    public static final String ZIP_CODES = "FM Zip Codes";
    public static final String ZIP_CODES_LIST_LINK = "FM Zip Codes List Link";
    public static final String OPTOUT_BLOCK = "FM Florist OptOut Block";
    public static final String HARD_BLOCK = "FM Florist Hard Block";
    public static final String CODIFY_PRODUCT_BLOCK = "FM Codify Product Block";
    public static final String ADD_CODIFICATION = "FM Add Codification";
    public static final String SUNDAY_DELIVERY = "FM Sunday Delivery";
    public static final String MERCURY = "FM Mercury";
    public static final String MINIMUM_ORDER = "FM Minimum Order";
    public static final String MANAGER_COMMENT = "FM Manager Comment";  // view and update
    public static final String ASSOCIATIONS = "FM Associations";
    public static final String WEIGHTS = "FM Weights";
    public static final String PERFORMANCE = "FM Performance";
    public static final String VENDOR = "FM Vendor";
    public static final String SEARCH_VENDOR = "FM Search Vendor";
    public static final String SUSPEND_OVERRIDE = "FM Suspend Override";
    public static final String DIRECTORY_CITY = "FM Directory City";
    public static final String SOURCE_FLORIST_PRIORITY= "SourceFloristPriority";
    public static final String FULFILLMENT= "FM Fulfillment";
    
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String VIEW = "View";
    public static final String UPDATE = "Update";
}