/**
 * 
 */
package com.ftd.applications.lmd.constants;

/**
 * @author smeka
 * 
 */
public class StoreFeedConstants {
	public static final String EMPTY_STR = " ";
	
	public static final String SOURCE_FEED = "SOURCE_FEED";	
	public static final String SHIPPING_KEY_FEED = "SHIPPING_KEY_FEED";
	public static final String PRICE_CODE_FEED = "PRICE_CODE_FEED";	
	
	public static final String USE_JDBCCONNECTION_UTIL = "USE_JDBCCONNECTION_UTIL";	
	public static final String LOAD_MEM_CONFIG_FILE = "load_member_config.xml";	
	public static final String LOAD_MEM_DS = "LOAD MEMBER";	
	public static final String ORD_SCRUB_DS = "ORDER SCRUB";
	
	public static final String GET_SOURCE_CODE_DETAIL = "GET_SOURCE_CODE_DETAIL";
	public static final String GET_PRICING_CODE_DETAILS = "GET_PRICING_CODE_DETAILS";	
	public static final String GET_SRC_MP_MEMBER_LVL_INFO = "GET_SRC_MP_MEMBER_LVL_INFO";	
	public static final String GET_APE_BASE_SOURCE_CODES = "GET_APE_BASE_SOURCE_CODES";
	public static final String GET_SHIPPING_KEYS = "GET_SHIPPING_KEYS";
	public static final String GET_SHIPPING_KEY_DETAIL_COST = "GET_SHIPPING_KEY_DETAIL_COST";
	
	public static final String IN_SOURCE_CODE = "IN_SOURCE_CODE";
	public static final String IN_PRICE_HEADER_ID = "IN_PRICE_HEADER_ID";
	public static final String GET_PARTNER_MAPPING_DETAILS = "GET_PARTNER_MAPPING_DETAILS";
}
