package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.FloristUpdater;
import com.ftd.applications.lmd.util.HistoryHelper;
import com.ftd.applications.lmd.util.MaintenanceXAO;
import com.ftd.applications.lmd.util.SearchManager;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.CodifiedProductVO;
import com.ftd.applications.lmd.vo.FloristHistoryVO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.applications.lmd.vo.SearchResultVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;
import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.StringTokenizer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * 
 *
 * @author 
 */

public class CommentsServlet extends HttpServlet 
{    

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.CommentsServlet";
    private final static String XSL_FLORIST_MAINTENANCE = "/xsl/floristMaintenance.xsl";
	private final static String XSL_ADD_HISTORY = "/xsl/addHistory.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.
     * 
     * 1. loadSearch: This action is called to load the main Florist Search Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        String action = request.getParameter("action");
    
        if(action != null && action.equals("addComment")) 
        {
		    this.addComment(request, response);
        }
		
		if(action != null && action.equals("loadComment"))
		{
			this.loadComment(request, response);
		}

    }
	
	private void loadComment(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	
        try
        {
            File xslFile = new File(getServletContext().getRealPath(XSL_ADD_HISTORY));
            Document responseDocument = DOMUtil.getDocument();
            
            HashMap pageData = new HashMap();
            pageData.put("florist_number", request.getParameter("mNum"));
            
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, ServletHelper.getParameterMap(request));
        }
        catch(Exception e)
        {
            logger.error(e);
        }
	
	}
	
	
    private void addComment(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        
        try
        {
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            if(request.getParameter("memnum") != null && request.getParameter("memnum").length() > 0)
            {
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
                FloristVO florist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                florist = floristUpdater.updateFloristFromRequest(florist, request);

                // Florist History - add new comment
                if(request.getParameter("newcomment") != null && request.getParameter("newcomment").length() > 0)
                {
                    FloristHistoryVO floristHistory = new FloristHistoryVO();
            
                    floristHistory.setComments(request.getParameter("newcomment")); // the textarea
        
                    floristHistory.setCommentDate(formatCommentDate(new Date()));
                    floristHistory.setCommentType(request.getParameter("dispositionDescription"));
                    
                    if(request.getParameter("newmanagerflag") != null && request.getParameter("newmanagerflag").equals("true"))
                    {                   
                        floristHistory.setManagerFlag("Y");
                    }
                    else
                    {
                        floristHistory.setManagerFlag("N");
                    }
                    
                    floristHistory.setUserId(florist.getLastUpdateUser());
                    florist.getFloristHistoryList().add(0, floristHistory);
                    
                    // Persist comment to database immediatly
                    HistoryHelper historyHelper = new HistoryHelper(dataRequest);
                    historyHelper.persistHistory(florist.getMemberNumber(), floristHistory);
                }
                
                MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes()); 
				
                BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
			
                File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
            
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
    private String formatCommentDate(Date date)
    {
        String formattedDate = "";
        
        if(date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
            formattedDate = sdf.format(date);
        }
        
        return formattedDate;
    }

}
