package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.FloristUpdater;
import com.ftd.applications.lmd.util.MaintenanceXAO;
import com.ftd.applications.lmd.util.SearchManager;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.util.LockHelper;
import com.ftd.applications.lmd.vo.CodifiedProductVO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.applications.lmd.vo.SearchResultVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.StringTokenizer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * 
 *
 * @author 
 */

public class CodificationServlet extends HttpServlet 
{    

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.CodificationServlet";
    private final static String XSL_FLORIST_MAINTENANCE = "/xsl/floristMaintenance.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.
     * 
     * 1. loadSearch: This action is called to load the main Florist Search Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        
        try
        {
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            { 
                if(action != null && action.equals("addCodifiedProduct")) 
                {
                    this.addCodifiedProduct(request, response);
                }
                else
                {
                    throw new ServletException("Action Not Supported");
                }
            }
        } 
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 

    }
	
    private void addCodifiedProduct(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        
        try
        {
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
         
            if(request.getParameter("memnum") != null && request.getParameter("memnum").length() > 0)
            {
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
                FloristVO florist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                florist = floristUpdater.updateFloristFromRequest(florist, request);
                
                // Update lock for user
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestMaintenanceLock(florist, florist.getLastUpdateUser());
                
                // Add new codified product to florist
                if(request.getParameter("codlist") != null && request.getParameter("codlist").length() > 0)
                {
                    CodifiedProductVO codifiedProduct = new CodifiedProductVO();
                    codifiedProduct.setId(request.getParameter("codlist"));
                    
                    codifiedProduct.setBlockedFlag("N");
                        
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    dataRequest.setStatementID("VIEW_CODIFICATIONS_BY_ID");
                    dataRequest.addInputParam("CODIFIED_ID", codifiedProduct.getId());
                    
                    CachedResultSet codifiedResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                    if(codifiedResultSet != null && codifiedResultSet.getRowCount() > 0)
                    {            
                        while(codifiedResultSet.next())
                        {
                            codifiedProduct.setCategory((String) codifiedResultSet.getObject(2));
                            codifiedProduct.setName((String) codifiedResultSet.getObject(3));
                        }
                    }
                    
                    florist.getCodifiedProductMap().put(codifiedProduct.getId(), codifiedProduct);
                }
                
                MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes()); 
                
                BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
        
				String isN;
				if(request.getParameter("isNew") != null && (request.getParameter("isNew") == "Y"))
				{
				HashMap flowMap = new HashMap();
                flowMap.put("processType", "AddNew");
                DOMUtil.addSection(responseDocument, "processFlow", "flow", flowMap, true);
				}
			
                File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
            
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    
    }

}
