package com.ftd.applications.lmd.servlets.actions;

import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.dao.GlobalParmsDAO;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.transaction.UserTransaction;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;


import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;


/**
 * Handles approval of florist audit report
 *
 * @author Anshu Gaind
 * @version $Id: ApproveFloristAuditReportAction.java,v 1.4 2011/06/30 15:14:16 gsergeycvs Exp $
 */

public class ApproveFloristAuditReportAction 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.actions.ApproveFloristAuditReport");

  public ApproveFloristAuditReportAction()
  {
  }
  
  /**
   * Approve an audit report
   * 
   * @param request
   * @param response
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  public void execute(HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
      String reportID = request.getParameter("reportID");
      String oldStatus = request.getParameter("oldStatus");
      String newStatus = (String)request.getAttribute("NEW_STATUS");
      String updatedBy = ServletHelper.getUserID(request);
      
      
      ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
      int transactionTimeout = Integer.parseInt((String)configurationUtil.getProperty("load_member_config.xml", "TRANSACTION_TIMEOUT_IN_SECONDS"));      
      String role = configurationUtil.getProperty("notification_config.xml", "ROLE");
      String context = configurationUtil.getProperty("load_member_config.xml", "SECURITY_MANAGER_CONTEXT");
      String fromAddress = configurationUtil.getProperty("notification_config.xml", "FROM_ADDRESS");
      
      String notificationContent = null, message = null, subject = null;
      
      InitialContext initialContext = null;
      UserTransaction userTransaction = null;
      
      TraxUtil traxUtil = TraxUtil.getInstance();
      Document auditReport = null;
      String status, userID, updatedDate;
      HashMap parameters = null;
      File auditReportXSL = null;
      
      FloristDAO floristDAO = new FloristDAO();
  
  
      try 
      {
        // get the initial context
        initialContext = new InitialContext();
  
        // Retrieve the UserTransaction object
        String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "JNDI_USERTRANSACTION_ENTRY");
        userTransaction = (UserTransaction)  initialContext.lookup(jndi_usertransaction_entry);
        logger.debug("User transaction obtained");
  
        userTransaction.setTransactionTimeout(transactionTimeout);
        logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");
        
        // Start the transaction with the begin method
        userTransaction.begin();
        logger.debug("********************BEGIN TRANSACTION***********************");
  
        // update the status of the report
        floristDAO.updateFloristAuditReport(reportID, oldStatus, newStatus, updatedBy);
        
        // if the update was successful, get a fresh audit report
        auditReport = (Document)floristDAO.getFloristAuditReport(reportID);
        
        Node statusNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/status/text()");
        Node userIDNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_by/text()");
        Node updatedDateNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_date/text()");
         
        status = (statusNode != null)?statusNode.getNodeValue():"";
        userID = (userIDNode != null)?userIDNode.getNodeValue():"";
        updatedDate = (updatedDateNode != null)?updatedDateNode.getNodeValue():"";

        // format date
        if (!updatedDate.equals("")){
          SimpleDateFormat dfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
          Date date = dfIn.parse(updatedDate);
          SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
          updatedDate = dfOut.format(date);
        }
        
        // c.	If the report has not been approved or rejected previously, change the 
        //    status to Approved and refresh the page with the updated status.
        message = "Approved - "  + updatedDate  + " - " + userID;
        notificationContent = configurationUtil.getProperty("notification_config.xml", "APPROVED_MESSAGE_1")
                              + "\"" + userID + "\"" + " on " + "\"" + updatedDate + "\"";
        subject = configurationUtil.getProperty("notification_config.xml", "APPROVED_MESSAGE_SUBJECT");
        
        /***********************************************************************/
        Document payloadDoc = DOMUtil.getDefaultDocument();
        Element payloadNode = (Element) payloadDoc.createElement("payload");
        payloadDoc.appendChild(payloadNode);
        Element auditReportNode = (Element) payloadDoc.createElement("audit-report");
        auditReportNode.setAttribute("report-id", reportID);
        auditReportNode.setAttribute("old-status", oldStatus);
        auditReportNode.setAttribute("new-status", newStatus);
        auditReportNode.setAttribute("updated-by", updatedBy);
        payloadNode.appendChild(auditReportNode);
        
        Element notificationNode = (Element) payloadDoc.createElement("notification");
        notificationNode.setAttribute("role", role);
        notificationNode.setAttribute("context", context);
        notificationNode.setAttribute("from-address", fromAddress);
        payloadNode.appendChild(notificationNode);

        Element subjectNode = (Element) payloadDoc.createElement("subject");
        subjectNode.appendChild(payloadDoc.createTextNode(subject));
        notificationNode.appendChild(subjectNode);

        Element notificationContentNode = (Element) payloadDoc.createElement("notification-content");
        notificationContentNode.appendChild(payloadDoc.createTextNode(notificationContent));
        notificationNode.appendChild(notificationContentNode);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true); 
        DOMUtil.print(payloadDoc, pw);
        String payload = sw.toString();
        pw.close();
        /***********************************************************************/
        
        // send a JMS message to load florist data to production
        Dispatcher dispatcher = Dispatcher.getInstance();
        MessageToken messageToken = new MessageToken();
        messageToken.setStatus("DISPATCH EVENT");
        messageToken.setMessage(ServletHelper.createDispatchEventTextMessage(configurationUtil.getProperty("load_member_config.xml", "LOAD_FLORIST_DATA_TO_PRODUCTION_EVENT_NAME"), payload));
        messageToken.setJMSCorrelationID(configurationUtil.getProperty("load_member_config.xml", "CONTEXT_NAME") + "::" + configurationUtil.getProperty("load_member_config.xml", "LOAD_FLORIST_DATA_TO_PRODUCTION_EVENT_NAME") );
        dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
        
        //Assuming everything went well, commit the transaction.
        userTransaction.commit();
        logger.debug("User transaction committed");
        logger.debug("********************END TRANSACTION***********************");
        
      } catch (Throwable t) 
      {
        logger.error(t);
        //if the transaction failed
        ServletHelper.rollback(userTransaction);
        // get a fresh audit report
        auditReport = (Document)floristDAO.getFloristAuditReport(reportID);

        Node statusNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/status/text()");
        Node userIDNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_by/text()");
        Node updatedDateNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_date/text()");
         
        status = (statusNode != null)?statusNode.getNodeValue():"";
        userID = (userIDNode != null)?userIDNode.getNodeValue():"";
        updatedDate = (updatedDateNode != null)?updatedDateNode.getNodeValue():"";

        // format date
        if (!updatedDate.equals("")){
          SimpleDateFormat dfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
          Date date = dfIn.parse(updatedDate);
          SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
          updatedDate = dfOut.format(date);
        }
                  
        parameters = ServletHelper.getParameterMap(request);
        auditReportXSL = new File(request.getSession().getServletContext().getRealPath("/xsl/audit-report.xsl"));
  
        // the following condition indicates a logical error with updating status
        if ( (t.getMessage() != null) && (t.getMessage().startsWith("RSNU")) ) 
        {              
            // a.	If the report was approved 
            if (status.equals(configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_APPROVED"))) 
            {
              message = "This report was approved by " + userID + " on " + updatedDate;
            }
            
            // b.	If the report was rejected
            else if (status.equals(configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_REJECTED"))) 
            {
              message = "This report was rejected by  " + userID + " on " + updatedDate;
            }
        }
        // failed for other reasons within the data tier, display the report with a std error message
        else 
        {
            message = t.getMessage() == null?"Failed to approve florist audit report. Please check logs.":t.getMessage();            
        }
  
        // add the message to the parameters map
        parameters.put("message", (message == null)?"":message);
        traxUtil.transform(request, response, auditReport, auditReportXSL, parameters);                                        
        
        // walk out of the execution block
        return;
      } finally
      {          
         // close initial context
         if(initialContext != null)
         {
             initialContext.close();
         }            
      }
    
      // if the transaction committed successfully, get a fresh audit report  
      // c.	If the report has not been approved or rejected previously, change the 
      //    status to Approved and refresh the page with the updated status.
      parameters = ServletHelper.getParameterMap(request);
      auditReportXSL = new File(request.getSession().getServletContext().getRealPath("/xsl/audit-report.xsl"));  
      parameters.put("message", (message == null)?"":message );
      traxUtil.transform(request, response, auditReport, auditReportXSL, parameters);                                        
    
  }
  
}
