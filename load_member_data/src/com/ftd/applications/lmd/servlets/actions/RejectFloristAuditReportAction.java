package com.ftd.applications.lmd.servlets.actions;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.transaction.UserTransaction;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;


import org.xml.sax.SAXException;

/**
 * Handles rejection of florist audit report
 * 
 * @author Anshu Gaind
 * @version $Id: RejectFloristAuditReportAction.java,v 1.4 2011/06/30 15:14:16 gsergeycvs Exp $
 */

public class RejectFloristAuditReportAction 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.actions.RejectFloristAuditReport");

  public RejectFloristAuditReportAction()
  {
  }
  
  /**
   * Rejects an audit report
   * 
   * @param request
   * @param response
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  public void execute(HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
      String reportID = request.getParameter("reportID");
      String oldStatus = request.getParameter("oldStatus");
      String newStatus = (String)request.getAttribute("NEW_STATUS");
      String updatedBy = ServletHelper.getUserID(request);
      
      String comments = request.getParameter("comments");
      
      ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
      int transactionTimeout = Integer.parseInt((String)configurationUtil.getProperty("load_member_config.xml", "TRANSACTION_TIMEOUT_IN_SECONDS"));      
      String recipConfigName = ConfigurationConstants.EMAIL_NOTIFY_RECIP_FLORIST_DATA;
      String fromAddress = configurationUtil.getProperty("notification_config.xml", "FROM_ADDRESS");
      
      String notificationContent = null, message = null, subject = null;
      
      InitialContext initialContext = null;
      UserTransaction userTransaction = null;
      
      TraxUtil traxUtil = TraxUtil.getInstance();
      Document auditReport = null;
      String status, userID, updatedDate;
      HashMap parameters = null;
      File auditReportXSL = null;
      
      FloristDAO floristDAO = new FloristDAO();
  
  
      try 
      {
        // get the initial context
        initialContext = new InitialContext();
  
        // Retrieve the UserTransaction object
        String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "JNDI_USERTRANSACTION_ENTRY");
        userTransaction = (UserTransaction)  initialContext.lookup(jndi_usertransaction_entry);
        logger.debug("User transaction obtained");
  
        userTransaction.setTransactionTimeout(transactionTimeout);
        logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");
        
        // Start the transaction with the begin method
        userTransaction.begin();
        logger.debug("********************BEGIN TRANSACTION***********************");
  
        // update the status of the report
        floristDAO.updateFloristAuditReport(reportID, oldStatus, newStatus, updatedBy);
        
        // if rejected insert comments
        floristDAO.updateFloristAuditReportComments(reportID, comments, updatedBy);

        // if the update was successful, get a fresh audit report
        auditReport = (Document)floristDAO.getFloristAuditReport(reportID);
        
        status = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/status/text()").getNodeValue();
        userID = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_by/text()").getNodeValue();
        updatedDate = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_date/text()").getNodeValue();
        // format date
        {
          SimpleDateFormat dfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
          Date date = dfIn.parse(updatedDate);
          SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
          updatedDate = dfOut.format(date);
        }
        
        // c.	If the report has not been approved or rejected previously, change the 
        //    status to Approved and refresh the page with the updated status.
        message = "Rejected - "  + updatedDate  + " - " + userID;
        notificationContent = configurationUtil.getProperty("notification_config.xml", "REJECT_MESSAGE_1")
                              + comments
                              + configurationUtil.getProperty("notification_config.xml", "REJECT_MESSAGE_2");
        subject = configurationUtil.getProperty("notification_config.xml", "REJECT_MESSAGE_SUBJECT");
        
        // send notifications
        floristDAO.sendNotification(recipConfigName, fromAddress, subject, notificationContent);
        
        //Assuming everything went well, commit the transaction.
        userTransaction.commit();
        logger.debug("User transaction committed");
        logger.debug("********************END TRANSACTION***********************");
      } catch (Throwable t) 
      {
        logger.error(t);
        //if the transaction failed
        ServletHelper.rollback(userTransaction);
        // get a fresh audit report
        auditReport = (Document)floristDAO.getFloristAuditReport(reportID);
        status = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/status/text()").getNodeValue();
        userID = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_by/text()").getNodeValue();
        updatedDate = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_date/text()").getNodeValue();
        // format date
        {
          SimpleDateFormat dfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
          Date date = dfIn.parse(updatedDate);
          SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
          updatedDate = dfOut.format(date);
        }
                  
        parameters = ServletHelper.getParameterMap(request);
        auditReportXSL = new File(request.getSession().getServletContext().getRealPath("/xsl/audit-report.xsl"));
  
        // the following condition indicates a logical error with updating status
        if ( (t.getMessage() != null) && (t.getMessage().startsWith("RSNU")) ) 
        {              
            // a.	If the report was approved 
            if (status.equals(configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_APPROVED"))) 
            {
              message = "This report was approved by " + userID + " on " + updatedDate;
            }
            
            // b.	If the report was rejected
            else if (status.equals(configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_REJECTED"))) 
            {
              message = "This report was rejected by  " + userID + " on " + updatedDate;
            }
        }
        // failed for other reasons within the data tier, display the report with a std error message
        else 
        {
            message = t.getMessage() == null?"Failed to reject florist audit report. Please check logs.":t.getMessage();            
        }
  
        // add the message to the parameters map
        parameters.put("message", message);
        traxUtil.transform(request, response, auditReport, auditReportXSL, parameters);                                        
        
        // walk out of the execution block
        return;
      } finally
      {          
         // close initial context
         if(initialContext != null)
         {
             initialContext.close();
         }            
      }
    
      // if the transaction committed successfully, get a fresh audit report  
      // c.	If the report has not been approved or rejected previously, change the 
      //    status to Approved and refresh the page with the updated status.
      parameters = ServletHelper.getParameterMap(request);
      auditReportXSL = new File(request.getSession().getServletContext().getRealPath("/xsl/audit-report.xsl"));  
      parameters.put("message", message);
      traxUtil.transform(request, response, auditReport, auditReportXSL, parameters);                                        
    
  }
  
}
