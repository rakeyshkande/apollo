package com.ftd.applications.lmd.servlets.actions;

import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;



/**
 * Handles display of zip removals report
 * 
 * @author Metin Hamidof
 * @version
 */

public class ViewZipRemovalsReportAction 
{
    
    private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.actions.ViewZipRemovalsReportAction");
    private final static String XSL_FLORIST_ZIPS_DEAD = "/xsl/zipRemovalsReport.xsl";
    
    public ViewZipRemovalsReportAction() {
    }

    /**
     * Displays zip removal report
     * 
     * @param request
     * @param response
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) 
    {
        DataRequest dataRequest = null;
        
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();    
        
            dataRequest = DataRequestHelper.getInstance().getDataRequest();    
            MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
        
            File xslFile = new File(request.getSession().getServletContext().getRealPath(XSL_FLORIST_ZIPS_DEAD));
            Document floristZipsDead = maintDao.getZipRemovals();
            
            DOMUtil.addSection(responseDocument, floristZipsDead.getChildNodes());    
            TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, ServletHelper.getParameterMap(request));
            
        }       
        catch(Exception e)
        {
             logger.error(e);
        }
        finally
        {
          if(dataRequest.getConnection() != null)
          {
            try{
              dataRequest.getConnection().close();
            }
            catch(Exception e){ }
          }
        }
    }
 }
