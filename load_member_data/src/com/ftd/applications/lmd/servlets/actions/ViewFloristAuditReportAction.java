package com.ftd.applications.lmd.servlets.actions;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;


import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Handles display of florist audit report
 * 
 * @author Anshu Gaind
 * @version $Id: ViewFloristAuditReportAction.java,v 1.3 2011/06/30 15:14:16 gsergeycvs Exp $
 */

public class ViewFloristAuditReportAction 
{
  private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.actions.ViewFloristAuditReport");

  public ViewFloristAuditReportAction()
  {
  }

  /**
   * Displays an audit report
   * 
   * @param request
   * @param response
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  public void execute(HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
    String reportID = request.getParameter("reportID");
    FloristDAO floristDAO = new FloristDAO();
    Document auditReport = (Document)floristDAO.getFloristAuditReport(reportID);
    Node statusNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/status/text()");
    Node userIDNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_by/text()");
    Node updatedDateNode = DOMUtil.selectSingleNode(auditReport, "/audit-report/header/content/updated_date/text()");
     
    String status = (statusNode != null)?statusNode.getNodeValue():"";
    String userID = (userIDNode != null)?userIDNode.getNodeValue():"";
    String updatedDate = (updatedDateNode != null)?updatedDateNode.getNodeValue():"";

    // format date
    if (!updatedDate.equals("")){
      SimpleDateFormat dfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
      Date date = dfIn.parse(updatedDate);
      SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
      updatedDate = dfOut.format(date);
    }
    
    String message = status + " - "  + updatedDate  + " - " + userID;
    
    HashMap parameters = ServletHelper.getParameterMap(request);
    parameters.put("message", (message == null)?"":message);
    
    TraxUtil traxUtil = TraxUtil.getInstance();
    File auditReportXSL = new File(request.getSession().getServletContext().getRealPath("/xsl/audit-report.xsl"));
    traxUtil.transform(request, response, auditReport, auditReportXSL, parameters);
  }
 
}
