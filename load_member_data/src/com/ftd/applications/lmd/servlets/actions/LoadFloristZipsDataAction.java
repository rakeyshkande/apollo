package com.ftd.applications.lmd.servlets.actions;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;

import javax.jms.JMSException;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;

/**
 * Handles loading of florist zip data (by removing any unassociated zips)
 * 
 */

public class LoadFloristZipsDataAction 
{
    private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.actions.LoadFloristZipsDataAction");

  public LoadFloristZipsDataAction()
  {
  }

  /**
   * Sends event to trigger removal of any zips (from florist_zips table) from 
   * florists that are no longer signed-up for those zips (based on EFOS city associations).
   * 
   * @param request
   * @param response
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws javax.jms.JMSException
   * @throws java.lang.Exception
   */
  public void execute(HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException, IOException, SAXException , TransformerException, JMSException, Exception
  {
    logger.debug("Sending dispatch event to trigger loading of florist zip data (i.e., zip removals)");
    ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
    Dispatcher dispatcher = Dispatcher.getInstance();
    MessageToken token = new MessageToken();
    token.setStatus("DISPATCH EVENT");
    token.setMessage(ServletHelper.createDispatchEventTextMessage(
                     configurationUtil.getProperty("load_member_config.xml", "LOAD_FLORIST_ZIP_DATA_EVENT_NAME"), ""));
    token.setJMSCorrelationID(configurationUtil.getProperty("load_member_config.xml", "CONTEXT_NAME") + "::" + configurationUtil.getProperty("load_member_config.xml", "LOAD_FLORIST_ZIP_DATA_EVENT_NAME") );
    dispatcher.dispatchTextMessage(new InitialContext(), token);
  }
}
