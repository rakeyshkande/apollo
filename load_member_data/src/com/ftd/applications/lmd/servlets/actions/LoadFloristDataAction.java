package com.ftd.applications.lmd.servlets.actions;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;

import javax.jms.JMSException;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;

/**
 * Handles loading of florist data
 * 
 * @author Anshu Gaind
 * @version $Id: LoadFloristDataAction.java,v 1.3 2011/06/30 15:14:16 gsergeycvs Exp $
 */

public class LoadFloristDataAction 
{
    private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.actions.LoadFloristDataAction");

  public LoadFloristDataAction()
  {
  }

  /**
   * Loads florist data
   * 
   * @param request
   * @param response
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws javax.jms.JMSException
   * @throws java.lang.Exception
   */
  public void execute(HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException, IOException, SAXException , TransformerException, JMSException, Exception
  {
    ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
    Dispatcher dispatcher = Dispatcher.getInstance();
    MessageToken messageToken = new MessageToken();
    messageToken.setStatus("DISPATCH EVENT");
    messageToken.setMessage(ServletHelper.createDispatchEventTextMessage(configurationUtil.getProperty("load_member_config.xml", "LOAD_FLORIST_DATA_EVENT_NAME"), ""));
    messageToken.setJMSCorrelationID(configurationUtil.getProperty("load_member_config.xml", "CONTEXT_NAME") + "::" + configurationUtil.getProperty("load_member_config.xml", "LOAD_FLORIST_DATA_EVENT_NAME") );
    dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
    
  }
 
}
