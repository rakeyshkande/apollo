/*
 * @(#) GNADDSettingServlet.java      1.1.2.1     2004/09/29
 * 
 * 
 */

package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.bo.GNADDSettingsBusinessDelegate;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.GlobalParmsDAO;
import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.GlobalParmsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.1.2.1      2004/09/29  Initial Release.(RFL)
 * 
 * 
 */
 
public class GNADDSettingServlet extends HttpServlet  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static final String CONTENT_TYPE = 
                        "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.servlets.GNADDSettingServlet";
    private final static String XSL_GNADD_SETTING = "/xsl/GNADDSetting.xsl";
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final String DATE_TIME_FORMAT = "MM/dd/yyyy '-' hh:mm a";
    private static final String DATE_DELIMITER = "/";
    private static final String ACTION_ON = "On";
    private static final String ACTION_OFF = "Off";
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Logger logger;

    

    public GNADDSettingServlet() 
    {
    }//end method GNADDSettingServlet()
    
    
    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }


    /**
     * 
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, 
                         HttpServletResponse response) throws ServletException,
                                                              IOException
    {
    
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        
        try
        {
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            { 
                response.setContentType(CONTENT_TYPE);

                try
                {
                    this.createResponse(request, response, null, null);
                }
                catch (Exception e) 
                {
                    logger.error("Exception thrown during doGet.", e);
                    throw e;
                }
            }
        }
        catch (Throwable t) 
        {
            try
            {
                request.getRequestDispatcher(
                        ConfigurationUtil.getInstance().getProperty(
                                ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.ERROR_PAGE_LOCATION)).
                                                    forward(request, response);
                
            }//end try
            catch (Exception e) 
            {
                logger.error("Exception thrown accessing error page.", e);
                throw new ServletException("Unexpected server exception.");
                
            }//end catch
            finally 
            {
                logger.error("Error thrown in doGet().", t);
                
            }//end finally
        
        }//end catch 
        
    }//end method doGet()
    
    
    /**
     * 
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, 
                          HttpServletResponse response) throws ServletException,
                                                              IOException
    {
        try
        {
            response.setContentType(CONTENT_TYPE);
            
            String action = request.getParameter("GNADD_Action");
            String context = request.getParameter("context");
            String securityToken = request.getParameter("securitytoken");
            
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            {
                if(action != null && action.equals(ACTION_ON)) 
                {
                    try
                    {
                        this.saveOn(request, response);
                    }//end try
                    catch (Exception e) 
                    {
                        logger.error("Exception thrown while saving on.", e);
                        throw e;
                        
                    }//end catch
                }//end if
                else if(action != null && action.equals(ACTION_OFF)) 
                {
                    try
                    {
                        this.saveOff(request, response);
                    }//end try
                    catch (Exception e) 
                    {
                        logger.error("Exception thrown while saving off.", e);
                        throw e;
    
                    }//end catch
                    
                }//end else if
                else if(action != null && action.equals("exitGNADD")) 
                {
                    // Redirect to menu
                    this.redirectToMenu(request, response);
                }
                else 
                {
                    logger.error("Invalid action type(" + action + ").");
                    throw new Exception("Invalid submit action type.");
                    
                }//end else
            }
            
        }//end try
        catch (Throwable t) 
        {
            try
            {
                request.getRequestDispatcher(
                        ConfigurationUtil.getInstance().getProperty(
                                ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.ERROR_PAGE_LOCATION)).
                                                    forward(request, response);
                
            }//end try
            catch (Exception e) 
            {
                logger.error("Exception thrown accessing error page.", e);
                throw new ServletException("Unexpected server exception.");
                
            }//end catch
            finally 
            {
                logger.error("Error thrown in doPost().", t);
                
            }//end finally
        
        }//end catch 
        
    }//end method doPost()
    

    /*
     * 
     */
    private Document createBaseDocument(GlobalParmsVO vo)
                                            throws ParserConfigurationException
    {
        Document document = null;
        
        try 
        {
            document = DOMUtil.getDefaultDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Exception thrown while getting default DOM document");
            throw pce;
            
        }//end catch
        
        if (vo != null)
        {
            //Create display data root
            Element viewElement = document.createElement("statusTable");
            document.appendChild(viewElement);
            
            //Create status node
            Element statusElement = document.createElement("status");
            switch (vo.getGNADDState()) 
            {
                case GlobalParmsVO.GNADD_STATE_ON:
                    statusElement.appendChild(
                                        document.createTextNode(this.ACTION_ON));
                    break;
                    
                case GlobalParmsVO.GNADD_STATE_OFF:
                    statusElement.appendChild(
                                        document.createTextNode(this.ACTION_OFF));
                    break;
                    
                default:
                    statusElement.appendChild(
                                            document.createTextNode("Unknown"));
                    break;
                    
            }//end switch
                
            viewElement.appendChild(statusElement);
            
            //create Shutdown date node
            Element GNADDDateElement = document.createElement("GNADDDate");
            if (vo.getGNADDDate() != null) 
            {
                GNADDDateElement.appendChild(document.createTextNode(
                                            this.formatDate(vo.getGNADDDate(),
                                                            this.DATE_FORMAT)));
                
            }//end if
            viewElement.appendChild(GNADDDateElement);
        
        }//end if
        
        return document;
        
    }//end method createBaseDocument()
    
    
    /*
     * 
     */
    private Document createDataEntryDocument(HttpServletRequest request)
                                            throws ParserConfigurationException
    {
        Document document = null;
        
        try 
        {
            document = DOMUtil.getDefaultDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Exception thrown while getting default DOM document");
            throw pce;
            
        }//end catch
        
        //Create screen field root
        Element fieldsElement = document.createElement("data_entry_fields");
        document.appendChild(fieldsElement);
        
        //Create  node
        Element actionElement = document.createElement("parmAction");
        
        String action = request.getParameter("FTDM_Action");
        if(action != null) 
        {
            actionElement.appendChild(document.createTextNode(action));
            
        }//end if
        fieldsElement.appendChild(actionElement);
        
        //create restart date
        Element restartDateElement = document.createElement("restartDate");
        
        String restartDate = request.getParameter("restart_date");
        if (restartDate != null) 
        {
            restartDateElement.appendChild(document.createTextNode(
                                                                restartDate));
            
        }//end if
        fieldsElement.appendChild(restartDateElement);
        
        return document;
        
    }//end method createScreenDocument
    
    
    /*
     * 
     */
    private void createResponse(HttpServletRequest request, 
                                HttpServletResponse response,
                                Document screenDataDocument,
                                Document validationDocument) throws Exception
    {
        
        Document responseDocument = null;
        
        // Create the initial document
        try 
        {
            responseDocument = DOMUtil.getDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Unable to get response document.", pce);
            throw pce;
        }//end catch
        
        //Base data
        Document viewDocument = 
                        this.createBaseDocument(this.getGNADDSettings());
        DOMUtil.addSection(responseDocument, viewDocument.getChildNodes());
        
        //Screen data
        if (screenDataDocument != null) 
        {
            DOMUtil.addSection(responseDocument, 
                               screenDataDocument.getChildNodes());
            
        }//end if
        
        //Screen validation 
        if (validationDocument != null) 
        {
            DOMUtil.addSection(responseDocument, 
                               validationDocument.getChildNodes());
            
        }//end if
        
        //Security data
        HashMap securityData = new HashMap();
        securityData.put("context", request.getParameter("context"));
        securityData.put("securitytoken", request.getParameter("securitytoken"));
        DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

        //Get xsl file for page
        File xslFile = 
            new File(getServletContext().getRealPath(XSL_GNADD_SETTING));
        
        try
        {
            TraxUtil.getInstance().transform(request, 
                                             response, 
                                             responseDocument, 
                                             xslFile, 
                                             ServletHelper.getParameterMap(request));
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to transform response document.", e);
            throw e;
        }//end catch (Exception)
        
    }//end method createResponse
    
    
    /*
     * 
     */
    private Document createValidationDocument() 
                                            throws ParserConfigurationException
    {
        Document document = null;
        
        try 
        {
            document = DOMUtil.getDefaultDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Exception thrown while getting default DOM document");
            throw pce;
            
        }//end catch
        
        //Create screen field root
        Element validationElement = document.createElement("validation");
        document.appendChild(validationElement);
        
        //Create  node
        Element restartDateElement = document.createElement("restartDate");
        restartDateElement.appendChild(document.createTextNode("Invalid date format"));
        validationElement.appendChild(restartDateElement);
        
        return document;
        
    }//end method createValidationDocument()
    
    
    /*
     * 
     */
    private String formatDate(Date date, String dateFormat)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        
        return formatter.format(date);
        
    }//end method
    
    
    /*
     * 
     */
    private GlobalParmsVO getGNADDSettings() throws DataAccessException
    {
        return new GNADDSettingsBusinessDelegate().getSettings();
        
    }//end method getManualMercuryFloristStatus()
    
    
    /*
     * Remember, Calendar.getTime() sets the date/time on the calendar.
     */
    private Date parseDate(String dateParameter) throws Exception
    {
        StringBuffer date = new StringBuffer();
        SimpleDateFormat formatter = new SimpleDateFormat(this.DATE_FORMAT);

        StringTokenizer st = new StringTokenizer(dateParameter, 
                                                 this.DATE_DELIMITER);
        int count = st.countTokens();
        
        GregorianCalendar cal = new GregorianCalendar();
        cal.getTime();
        String currentYear = String.valueOf(cal.get(Calendar.YEAR));
        
        if ((count < 2) || (count > 3)) 
        {
            throw new Exception("Invalid date format.");
        }//end if
        
        //Month
        date.append(st.nextToken());
        date.append(this.DATE_DELIMITER);
        //date
        date.append(st.nextToken());
        date.append(this.DATE_DELIMITER);
        //year
        if (st.hasMoreTokens()) 
        {
            String year = st.nextToken();
            if (year.length() == 2) 
            {
                date.append(currentYear.substring(0, currentYear.length() - 2));
                date.append(year);
            }
            else 
            {
                date.append(year);
            }//end else
            
        }//end if
        else 
        {
            date.append(currentYear);
        }//end if
        
        //parse the date
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(formatter.parse(date.toString()));
        cal2.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
        cal2.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
        cal2.set(Calendar.SECOND, cal.get(Calendar.SECOND));
        cal2.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));
        
        return cal2.getTime();
        
    }//end method parseDate
    
    
    /*
     * 
     */
    private void saveOn(HttpServletRequest request, 
                             HttpServletResponse response) throws Exception
    {
        Document screenDocument = null;
        Document validationDocument = null;
        Date GNADDDate = null;
        
        String dateParameter = request.getParameter("GNADD_date");
        String action = request.getParameter("GNADD_Action");
        String currentSetting = request.getParameter("current_setting");

        if(dateParameter == null || dateParameter.equals(""))
        {
            //send missing date validation to screen
            screenDocument = this.createDataEntryDocument(request);
            validationDocument = this.createValidationDocument();
            
        }//end if
        else
        {
            try
            {
                GNADDDate = this.parseDate(dateParameter);
            }//end 
            catch (Exception e) 
            {
                screenDocument = this.createDataEntryDocument(request);
                validationDocument = this.createValidationDocument();
            }
            
            if (GNADDDate != null) 
            {
                GNADDSettingsBusinessDelegate gnaddBusinessDelegate = new GNADDSettingsBusinessDelegate();
                
                if(currentSetting == null || !action.equals(currentSetting))
                {
                    gnaddBusinessDelegate.turnOn(GNADDDate, ServletHelper.getUserID(request));
                }
                else
                {
                    gnaddBusinessDelegate.updateDate(GNADDDate, ServletHelper.getUserID(request));
                }
            }//end if
            else 
            {
                screenDocument = this.createDataEntryDocument(request);
                validationDocument = this.createValidationDocument();
            }//end else
                    
        }//end else
        
        try {
            this.createResponse(request, 
                                response, 
                                screenDocument, 
                                validationDocument);
        }//end try
        catch (Exception e) 
        {
            throw e;
        }//end catch


    }//end method saveRestart()
    
    
    /*
     * 
     */
    private void saveOff(HttpServletRequest request, 
                              HttpServletResponse response) throws Exception
    {
    
        String action = request.getParameter("GNADD_Action");
        String currentSetting = request.getParameter("current_setting");

        if(currentSetting == null || !action.equals(currentSetting))
        {
            new GNADDSettingsBusinessDelegate().turnOff(ServletHelper.getUserID(request));
        }
        
        try
        {
            this.createResponse(request, response, null, null);
        }//end try
        catch (Exception e) 
        {
            throw e;
            
        }//end catch
        
    }//end method saveON()
	
	private void redirectToMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

        String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
        String adminAction = (String) request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {
            mainExitURL = mainExitURL + adminAction;
        }
        else
        {
            mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }

        response.sendRedirect(mainExitURL + securityParams);
    }
    
}//end class
