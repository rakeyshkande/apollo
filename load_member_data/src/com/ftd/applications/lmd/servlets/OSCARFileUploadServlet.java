/**
 * com.ftd.applications.lmd.servlets.OSCARFileUploadServlet.java
 */
package com.ftd.applications.lmd.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.applications.lmd.bo.OscarFileUploadBO;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.ZipCodeDAO;
import com.ftd.applications.lmd.util.LMDSpreadsheetUtil;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.ZipCodeVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityFilter;
import com.ftd.security.util.FileProcessor;



/**
 * A concrete implementation of HttpServlet to service requests to upload OSCAR zip code and city/state data.  This class also services
 * requests to export the current OSCAR data.
 * 
 * @author rlarson
 *
 */
public class OSCARFileUploadServlet extends HttpServlet 
{
	private static final String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.OSCARFileUploadServlet";
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private static final String XSL_OSCAR_FILE_UPLOAD = "/xsl/oscarFileUpload.xsl";
    public static final String ACTION = "servlet_action";
	public static final String ACTION_FILE_UPLOAD = "upload";
	public static final String ACTION_EXPORT = "export";
	public static final String ACTION_EXIT = "exitSystem";
	public static final String UPLOAD_TYPE = "uploadType";
	public static final String UPLOAD_TYPE_ZIP_CODE = "zipCode";
	public static final String UPLOAD_TYPE_CITY_STATE = "cityState";
	public static final String FILE_ITEM = "file_item";
	private static final String DOM_MESSAGE = "message";
	private static final String DOM_ERROR_MESSAGE = "errorMessage";
	private static final String MESSAGE_SUCCESS = "Your file has been processed.  Click Export above to view current status.";

    private Logger logger;  

    
    /**
     * Default constructor
     */
    public OSCARFileUploadServlet() 
    {
    	super();
    }//end method OSCARFileUploadServlet()    
   
    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger(LOGGER_CATEGORY);
    }

    /**
     * Services HTTP GET requests to display default OSCAR Upload File html page
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
        	HashMap parmMap;
            // Get parameters from HttpServletRequest
            if (SecurityFilter.isMultipartFormData(request))
            {
                parmMap = com.ftd.security.util.ServletHelper.getMultipartParam(request);
            }
            else
            {
                parmMap = this.getCleanHttpParameters(request);
            }

            String context = (String)parmMap.get("context");
            String securityToken = (String)parmMap.get("securitytoken");
            
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            {
                response.setContentType(CONTENT_TYPE); 
                try
                {
                    this.createResponse(request, response, null, null, parmMap);
                }
                catch (Exception e) 
                {
                    logger.error("Exception thrown during doGet.", e);
                    throw e;
                }
            }
        }
        catch (Throwable t) 
        {
            try
            {
                request.getRequestDispatcher(
                        ConfigurationUtil.getInstance().getProperty(
                                ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.ERROR_PAGE_LOCATION)).
                                                    forward(request, response);
                
            }//end try
            catch (Exception e) 
            {
                logger.error("Exception thrown accessing error page.", e);
                throw new ServletException("Unexpected server exception.");
                
            }//end catch
            finally 
            {
                logger.error("Error thrown in doGet().", t);
                
            }//end finally
        
        }//end catch 
        
    }//end method doGet()    
    
    /**
     * Services HTTP POST requests to process OSCAR file uploads and OSCAR data exports
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {    	
    	OscarFileUploadBO bo = new OscarFileUploadBO();
    	List<ZipCodeVO> zipCodeSpreadsheetRowList;
    	String message = null;
    	String errorMessage = null;
    	
        try
        {
            response.setContentType(CONTENT_TYPE);  
 
            HashMap parmMap;
            // Get parameters from HttpServletRequest
            if (SecurityFilter.isMultipartFormData(request))
            {
                parmMap = com.ftd.security.util.ServletHelper.getMultipartParam(request);
            }
            else
            {
                parmMap = this.getCleanHttpParameters(request);
            }

            String context = (String)parmMap.get("context");
            String securityToken = (String)parmMap.get("securitytoken");
            
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            {
                String action = (String)parmMap.get("actionType");
                
                if (ACTION_EXIT.equals(action))
                {
                    // Redirect to menu
                    this.redirectToMenu(parmMap, response);
                    this.createResponse(request, response, message, errorMessage, parmMap);
                }   
                else if (ACTION_EXPORT.equals(action))
                {
                	response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                	response.setHeader("Content-Disposition","attachment; filename=OscarExport" + ".xlsx");

                	XSSFWorkbook workBook = new XSSFWorkbook();
                    XSSFSheet sheet = workBook.createSheet();               
                    int noRow =0;
                    XSSFRow headerRow =  sheet.createRow(noRow);
                    
                    XSSFCell headerCell0=headerRow.createCell(0);
                    headerCell0.setCellValue(ZipCodeVO.FIELD_NAME_ZIP_CODE);
                    XSSFCell headerCell1=headerRow.createCell(1);
                    headerCell1.setCellValue(ZipCodeVO.FIELD_NAME_CITY);
                    XSSFCell headerCell2 = headerRow.createCell(2);
                    headerCell2.setCellValue(ZipCodeVO.FIELD_NAME_STATE_ID);
                    XSSFCell headerCell3=headerRow.createCell(3);
                    headerCell3.setCellValue(ZipCodeVO.FIELD_NAME_OSCAR_ZIP);
                    XSSFCell headerCell4=headerRow.createCell(4);
                    headerCell4.setCellValue(ZipCodeVO.FIELD_NAME_OSCAR_CITY_STATE);                    
                    
                    ZipCodeDAO zipCodeDAO = new ZipCodeDAO();
                    List<ZipCodeVO> list = zipCodeDAO.queryAll();
                    
                    Iterator<ZipCodeVO> it = list.iterator();
                    
                    while(it.hasNext()) {
                    	ZipCodeVO zipCodeVO = (ZipCodeVO)it.next();
                    	XSSFRow datarow =  sheet.createRow(++noRow); 
                    	
                    	XSSFCell dataCell0=datarow.createCell(0);
                    	dataCell0.setCellType(Cell.CELL_TYPE_STRING);
                    	dataCell0.setCellValue((String)zipCodeVO.getZipCode());                    	
                        XSSFCell dataCell1=datarow.createCell(1);
                        dataCell1.setCellType(Cell.CELL_TYPE_STRING);
                        dataCell1.setCellValue((String)zipCodeVO.getCity());
                        XSSFCell dataCell2=datarow.createCell(2);
                        dataCell2.setCellType(Cell.CELL_TYPE_STRING);
                        dataCell2.setCellValue((String)zipCodeVO.getStateId());
                        XSSFCell dataCell3=datarow.createCell(3);
                        dataCell3.setCellType(Cell.CELL_TYPE_STRING);
                        dataCell3.setCellValue((String)zipCodeVO.getOscarZipFlag());
                        XSSFCell dataCell4=datarow.createCell(4);
                        dataCell4.setCellType(Cell.CELL_TYPE_STRING);
                        dataCell4.setCellValue((String)zipCodeVO.getOscarCityStateFlag());                        
                    }
                   
                    ServletOutputStream out = response.getOutputStream();                  
                    try {   
                        workBook.write(out);                        
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (out != null) {
                            try {
                                out.flush();
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }                
                }
                else if (ACTION_FILE_UPLOAD.equals(action))
                {
                    String uploadType = (String)parmMap.get(UPLOAD_TYPE);
                        
                    if (UPLOAD_TYPE_ZIP_CODE.equals(uploadType))
                    {
                       	try
                       	{                        	
                           	//Retrieve file
                           	zipCodeSpreadsheetRowList = getOscarFileUploadZipCodeSpreadsheetRows((FileItem)parmMap.get(FILE_ITEM));
                           	
                		    bo.saveZipCodeFlagUpdates(zipCodeSpreadsheetRowList);
                			    
                		    message = MESSAGE_SUCCESS;
                       	}
                       	catch (Throwable t)
                       	{
                       	    logger.error("Exception thrown while Updating OSCAR Zip Code Flag", t);
                       	    errorMessage = "Bad data.  " + t.getLocalizedMessage();
                      	}                    	
                    }
                    else if (UPLOAD_TYPE_CITY_STATE.equals(uploadType))
                    {
                		try
                		{
                    		//Retrieve file
                			zipCodeSpreadsheetRowList = getOscarFileUploadCityStateSpreadsheetRows((FileItem)parmMap.get(FILE_ITEM));
               			    bo.saveCityStateFlagUpdates(zipCodeSpreadsheetRowList);
                			    
               			    message = MESSAGE_SUCCESS;
                       	}
                       	catch (Throwable t)
                       	{
                       	    logger.error("Exception thrown while Updating OSCAR City/State Flag", t);
                       	    errorMessage = "Bad data.  " + t.getLocalizedMessage();
                       	}                   		
                   	}
                    else
                    {
                        errorMessage = "Invalid upload type.";
                    }          
                    
                    this.createResponse(request, response, message, errorMessage, parmMap);
                }//end if(ACTION = UPLOAD)
                else
                {
                   	errorMessage = "Invalid action type provided.";
                }                
            }//end else for (ACTION = EXIT)
        }        
        catch (Throwable t) 
        {
            try
            {
                request.getRequestDispatcher(
                        ConfigurationUtil.getInstance().getProperty(
                                ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.ERROR_PAGE_LOCATION)).
                                                    forward(request, response);
                
            }//end try
            catch (Exception e) 
            {
                logger.error("Exception thrown accessing error page.", e);
                throw new ServletException("Unexpected server exception.");
                
            }//end catch
            finally 
            {
                logger.error("Error thrown in doPost().", t);
                
            }//end finally
        
        }//end catch 
        
    }//end method doPost()
    

    /**
     * Builds a HashMap of the request parameters contained in the provided HttpServletRequest
     * 
     * @param request
     * @return HashMap containing the request parameters, keyed by parameter name
     */
    protected HashMap getCleanHttpParameters(HttpServletRequest request)
    {
        HashMap result = new HashMap();
        
        for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); )
        {
	        String key = (String) i.nextElement();
	        String value = request.getParameter(key);
	
	        if ((value != null) && (value.trim().length() > 0))
	        {
	          result.put(key, value.trim());
	        }
        }

        return result;
    }
    

    /*
     * Updates HttpServletResponse with default response document
     */
    private void createResponse(HttpServletRequest request, 
                                HttpServletResponse response,
                                String message,
                                String errorMessage,
                                HashMap map) throws Exception
    {
        
        Document responseDocument = JAXPUtil.createDocument();
        Element root = responseDocument.createElement("root");
        responseDocument.appendChild(root);        
  
        // Message
        if ((message!=null) && (message!=""))
        {
        	Element messageElement = responseDocument.createElement(DOM_MESSAGE);
            root.appendChild(messageElement);
            messageElement.appendChild(responseDocument.createTextNode(message));    
        }
        // Error Message
        if ((errorMessage!=null) && (errorMessage!=""))
        {
            Element errorMessageElement = responseDocument.createElement(DOM_ERROR_MESSAGE);
            root.appendChild(errorMessageElement);
            errorMessageElement.appendChild(responseDocument.createTextNode(errorMessage));            
        }
        
         //Get xsl file for page
        File xslFile = new File(getServletContext().getRealPath(XSL_OSCAR_FILE_UPLOAD));        
              
        try
        {
        	//remove the uploaded file
        	if (map.containsKey(FILE_ITEM))
        	    map.remove(FILE_ITEM);
            TraxUtil.getInstance().transform(request, 
                                             response, 
                                             responseDocument, 
                                             xslFile, 
                                             map);
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to transform response document.", e);
            throw e;
        }//end catch (Exception)
        
    }//end method createResponse
    
    
    /*
     * Updates HttpServletResponse to redirect response to Distribution/Fulfillment menu
     */
	private void redirectToMenu(HashMap map, HttpServletResponse response) throws Exception
    {
        try
        {
            // Load return page
            String securityParams = "&securitytoken=" + map.get("securitytoken") + "&context=" + map.get("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

            String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
            String adminAction = (String) map.get("adminAction");
            if(adminAction != null && adminAction.length() > 0)
            {
                mainExitURL = mainExitURL + adminAction;
            }
            else
            {
                mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
            }

            response.sendRedirect(mainExitURL + securityParams);
        }
        catch(Exception e)
        {
            logger.error(e);
        }
    }
    
	/*
	 * Reads a FileItem, and returns the File contents as a List of ZipCodeVOs.
	 */
    private List<ZipCodeVO> getOscarFileUploadCityStateSpreadsheetRows(FileItem fileItem) throws Exception
    {
        logger.debug("Entering getOscarFileUploadCityStateSpreadsheetRows...");

        if (fileItem == null)
        {
            throw new RuntimeException("getOscarFileUploadCityStateSpreadsheetRows: Upload data file can not be null.");
        }
        else
        {
             return LMDSpreadsheetUtil.getInstance().processSpreadsheetFile(fileItem.getInputStream());
        }

    }

	/*
	 * Reads a FileItem, and returns the File contents as a List of ZipCodeVOs.
	 */
    private List<ZipCodeVO> getOscarFileUploadZipCodeSpreadsheetRows(FileItem fileItem) throws Exception
    {
        logger.debug("Entering getOscarFileUploadZipCodeSpreadsheetRows...");

        if (fileItem == null)
        {
            throw new RuntimeException("getOscarFileUploadZipCodeSpreadsheetRows: Upload data file can not be null.");
        }
        else
        {
            return LMDSpreadsheetUtil.getInstance().processSpreadsheetFile(fileItem.getInputStream());
        }

    }
}

/*
 * MAINTENANCE HISTORY 
 * $Log: OSCARFileUploadServlet.java,v $
 * Revision 1.2  2012/12/07 18:39:44  gsergeycvs
 * #0000
 * Merge to trunk from 5.11.1.5
 *
 * Revision 1.1.4.8  2012/11/20 18:51:48  mavinenicvs
 * #13513 14264
 *
 * Revision 1.1.4.7  2012/11/20 17:52:27  mavinenicvs
 * #13513 14264
 *
 * Revision 1.1.4.6  2012/11/15 20:27:04  mavinenicvs
 * #13513 24344
 *
 * Revision 1.1.4.5  2012/11/10 22:49:14  mavinenicvs
 * #13513 24345
 *
 * Revision 1.1.4.4  2012/11/10 00:41:28  mavinenicvs
 * #13513 24345
 *
 * Revision 1.1.4.3  2012/11/09 23:08:51  mavinenicvs
 * #13513 24345
 *
 * Revision 1.1.4.2  2012/11/08 22:56:45  rlarsoncvs
 * #13513 24344 24345
 * Moved from BIRCHBOB branch
 *
 * Revision 1.1.2.4  2012/11/08 22:33:37  rlarsoncvs
 * #13513 24344 24345
 * Updates
 *
 *
 */