package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.ViewQueueDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.HistoryHelper;
import com.ftd.applications.lmd.util.LockHelper;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.util.ViewQueueUpdater;
import com.ftd.applications.lmd.util.ViewQueueXAO;
import com.ftd.applications.lmd.vo.FloristBlockVO;
import com.ftd.applications.lmd.vo.ViewQueueVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;


/**
 * This servlet is used to populate fields in the Florist Maintenance Tabs.
 * The data is obtained from the DAO object, a proxy, and is loaded
 * by the FloristMaintenance tabs.
 *
 * @author Mehul Patel
 */
 
 
public class ViewQueueServlet extends HttpServlet 
{    
	private Logger logger;
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
	private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.ViewQueueServlet";
	private final static String XSL_VIEW_QUEUE = "/xsl/viewQueue.xsl";

    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }
    
	/**
     * Calls the function to load the Florist Maintenance Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
		
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
    
        try
        {
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            { 
                if(action != null && action.equals("loadQueue")) 
                {
                   this.loadQueue(request, response);
                }
                else if(action != null && action.equals("nextMessage")) 
                {
                   this.nextMessage(request, response);
                }
                else if(action != null && action.equals("updateFlorist")) 
                {
                    this.updateFlorist(request, response);
                }
                else if(action != null && action.equals("updateFloristNoSave")) 
                {
                    this.updateFloristNoSave(request, response);
                }
                else if(action != null && action.equals("removeMessage")) 
                {
                    this.removeMessage(request, response);
                }
                else if(action != null && action.equals("exitViewQueue")) 
                {
                    this.exitViewQueue(request, response);
                }
            }
        } 
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 
    }
    
	
	/**
     * This function initializes and loads the first available message within
     * the View Queue.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void loadQueue(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            // Pull user currently logged on
            String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
            
            // Clear previous views fro the user
            this.clearPreviousViews(userId, dataRequest);
            
            /*
            // Load the next mercury id available in the queue
            String mercuryId = this.loadNextMercuryId(userId, dataRequest);
            
            logger.debug("Retrieved mercuryID = " + mercuryId);            
            
            // Load the data for the mercury id
            if(mercuryId != null && mercuryId.length() > 0)
            {
                // Load data for queue fromd database
                ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
                ViewQueueVO viewQueue = viewQueueDAO.getViewQueue(mercuryId);
            
                if (viewQueue == null)
                {
                    logger.error("viewQueue object for " + mercuryId + " is null");
                    this.redirectToMenu(request,response);
                }
             */
             ViewQueueVO viewQueue = getNextMessage(dataRequest,userId);
             if (viewQueue != null)
             {
                // Check and update lock on florist and set lock on queue data
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestViewQueueLock(viewQueue, userId);
                
                // Convert data to XML
                ViewQueueXAO viewQueueXAO = new ViewQueueXAO();
                DOMUtil.addSection(responseDocument, ((Document) viewQueueXAO.generateXML(viewQueue)).getChildNodes()); 
                    
                // Generate base page data
                BaseDataBuilder data = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, data.loadViewQueueGenericData(dataRequest, request));
                    
                // Transform for presentation
                File xslFile = new File(getServletContext().getRealPath(XSL_VIEW_QUEUE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
                
            }
            else
            {
                // Redirect to menu
                this.redirectToMenu(request, response);
            }

		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    
    /**
     * This function sends the user to the next available message within
     * the View Queue.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void nextMessage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            LockHelper lockHelper = new LockHelper(dataRequest);
            
            // Pull user currently logged on
            String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
            
            // Release lock on florist
            lockHelper.releaseViewQueueLock(request.getParameter("mNum"), userId);
            
            /*
            // Load the next mercury id available in the queue
            String mercuryId = this.loadNextMercuryId(userId, dataRequest);
		    logger.debug("Retrieved mercuryID = " + mercuryId);            
            
            // Load the data for the mercury id
            if(mercuryId != null && mercuryId.length() > 0)
            {
                // Load data for queue from database
                ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
                ViewQueueVO viewQueue = viewQueueDAO.getViewQueue(mercuryId);

                if (viewQueue == null)
                {
                    logger.error("viewQueue object for " + mercuryId + " is null");
                    this.redirectToMenu(request,response);
                }
                */
            ViewQueueVO viewQueue = getNextMessage(dataRequest,userId);
            if (viewQueue != null)
            {
            
                // Check and update lock on florist and set lock on queue data
                lockHelper.requestViewQueueLock(viewQueue, userId);
                
                // Convert data to XML
                ViewQueueXAO viewQueueXAO = new ViewQueueXAO();
                DOMUtil.addSection(responseDocument, ((Document) viewQueueXAO.generateXML(viewQueue)).getChildNodes()); 
                
                // Genrate base page data
                BaseDataBuilder data = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, data.loadViewQueueGenericData(dataRequest, request));
            
                // Transform for presentation
                File xslFile = new File(getServletContext().getRealPath(XSL_VIEW_QUEUE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
                
            }
            else
            {
                // Redirect to menu
                this.redirectToMenu(request, response);
            }

		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    
    /**
     * This function sends the user to the next available message within
     * the View Queue.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void exitViewQueue(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            LockHelper lockHelper = new LockHelper(dataRequest);
            
            // Pull user currently logged on
            String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
            
            // Release lock on florist
            lockHelper.releaseViewQueueLock(request.getParameter("mNum"), userId);
            
            // Redirect to menu
            this.redirectToMenu(request, response);
        
		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    
    /**
     * This function updates the florist information with the data returned
     * from the View Queue interface which the user entered.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void updateFlorist(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
            ObjectCopyUtil objectCopyUtil = new ObjectCopyUtil();
            
            // Pull user currently logged on
            String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
            
            if(request.getParameter("mercuryId") != null && request.getParameter("mercuryId").length() > 0)
            {
                // Load the current queue from the database
                ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
                ViewQueueVO currentViewQueue = viewQueueDAO.getViewQueue(request.getParameter("mercuryId"));
                ViewQueueVO previousViewQueue = (ViewQueueVO) objectCopyUtil.deepCopy(currentViewQueue);
                
                // Refresh lock on queue data to get new timeout
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestViewQueueLock(currentViewQueue, userId);
                
                // Update the blocks and associated fields
                ViewQueueUpdater viewQueueUpdater = new ViewQueueUpdater(dataRequest);
                currentViewQueue = viewQueueUpdater.updateFloristFromRequest(currentViewQueue, request);
    
            	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
            	FloristBlockVO newfbVO = currentViewQueue.getNewFloristBlock();
            	if (newfbVO != null && newfbVO.getBlockType() != null && !newfbVO.getBlockType().equals("")) {
                	logger.debug("newBlock: " + newfbVO.getBlockType() + " " + 
            			sdfDate.format(newfbVO.getBlockStartDate()) + " - " +
            			sdfDate.format(newfbVO.getBlockEndDate()));
            	
                	Date today = sdfDate.parse(sdfDate.format(new Date())); 
            	    List floristBlocks = currentViewQueue.getFloristBlockList();
                    for (int i=0; i<floristBlocks.size(); i++) {
                	    FloristBlockVO fbVO = (FloristBlockVO) floristBlocks.get(i);
                	    String blockType = fbVO.getBlockType();
                	    if (blockType.equalsIgnoreCase("O")) {
                        	long dayDiffStart = (fbVO.getBlockStartDate().getTime() - newfbVO.getBlockStartDate().getTime()) / (1000 * 60 * 60 * 24);
                        	long dayDiffEnd = (fbVO.getBlockStartDate().getTime() - newfbVO.getBlockEndDate().getTime()) / (1000 * 60 * 60 * 24);
                    	    logger.debug("blockCheck: " + fbVO.getBlockType() + " " + 
                        			sdfDate.format(fbVO.getBlockStartDate()) + " " +
                        			dayDiffStart + " " + dayDiffEnd);
                    	    if (dayDiffStart <= 0 || dayDiffEnd <= 0) {
                    	    	logger.debug("block overlap");
                    	    	newfbVO.setBlockFlag("N");
                    	    }
                	    } else {
                        	long dayDiffStart = (fbVO.getBlockStartDate().getTime() - newfbVO.getBlockStartDate().getTime()) / (1000 * 60 * 60 * 24);
                        	long dayDiffEnd = (fbVO.getBlockEndDate().getTime() - newfbVO.getBlockEndDate().getTime()) / (1000 * 60 * 60 * 24);
                    	    logger.debug("blockCheck: " + fbVO.getBlockType() + " " + 
                        			sdfDate.format(fbVO.getBlockStartDate()) + " " +
                        			sdfDate.format(fbVO.getBlockEndDate()) + " " +
                        			dayDiffStart + " " + dayDiffEnd);
                    	    if ((dayDiffStart >= 0 && dayDiffEnd <= 0) ||
                    	    		(dayDiffStart <= 0 && dayDiffEnd >= 0) ) {
                    	    	logger.debug("block overlap");
                    	    	newfbVO.setBlockFlag("N");
                    	    }
                	    }
                    }
		        }
                
                // Persist data to the database through DAO
                viewQueueDAO.updateViewQueue(currentViewQueue);
                
                // Track florist history and persist to database
                HistoryHelper historyHelper = new HistoryHelper(dataRequest);
                List historyList = historyHelper.generateViewQueueHistory(previousViewQueue, currentViewQueue);
                
                // Reload from database to retrieve any database logic updates from the save  
                currentViewQueue = viewQueueDAO.getViewQueue(request.getParameter("mercuryId"));
                        
                // Convert data to XML
                ViewQueueXAO viewQueueXAO = new ViewQueueXAO();
                DOMUtil.addSection(responseDocument, ((Document) viewQueueXAO.generateXML(currentViewQueue)).getChildNodes()); 
                    
                // Genrate base page data
                BaseDataBuilder data = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, data.loadViewQueueGenericData(dataRequest, request));
            
                // Transform for presentation
                File xslFile = new File(getServletContext().getRealPath(XSL_VIEW_QUEUE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    /**
     * This function updates the florist information with the data returned
     * from the View Queue interface which the user entered.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void removeMessage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
			ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
            
            LockHelper lockHelper = new LockHelper(dataRequest);
            
            // Pull user currently logged on
            String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
            
            // Execute remove procedure to remove this message from queue
            viewQueueDAO.removeMessageFromQueue(request.getParameter("mercuryId"));
            
            // Release lock on florist
            lockHelper.releaseViewQueueLock(request.getParameter("mNum"), userId);
            
            /*
            // Load the next mercury id available in the queue
            String mercuryId = this.loadNextMercuryId(userId, dataRequest);
            
            // Load the data for the mercury id
            if(mercuryId != null && mercuryId.length() > 0)
            {
                // Load data for queue from database
                ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
                ViewQueueVO viewQueue = viewQueueDAO.getViewQueue(mercuryId);
                */            
            ViewQueueVO viewQueue = getNextMessage(dataRequest,userId);
            if (viewQueue != null)
            {
                // Check and update lock on florist and set lock on queue data
                lockHelper.requestViewQueueLock(viewQueue, userId);
                
                // Convert data to XML
                ViewQueueXAO viewQueueXAO = new ViewQueueXAO();
                DOMUtil.addSection(responseDocument, ((Document) viewQueueXAO.generateXML(viewQueue)).getChildNodes()); 
                    
                // Generate base page data
                BaseDataBuilder data = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, data.loadViewQueueGenericData(dataRequest, request));
            
                // Transform for presentation
                File xslFile = new File(getServletContext().getRealPath(XSL_VIEW_QUEUE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
                
            }
            else
            {
                // Redirect to menu
                this.redirectToMenu(request, response);
            }
		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    private void updateFloristNoSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            // Pull user currently logged on
            String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
            
            // Load the current queue from the database
            ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
            ViewQueueVO viewQueue = viewQueueDAO.getViewQueue(request.getParameter("mercuryId"));
            
            // Refresh lock on queue data to get new timeout
            LockHelper lockHelper = new LockHelper(dataRequest);
            lockHelper.requestViewQueueLock(viewQueue, userId);
            
            // Update the blocks and associated fields
            ViewQueueUpdater viewQueueUpdater = new ViewQueueUpdater(dataRequest);
            viewQueue = viewQueueUpdater.updateFloristFromRequest(viewQueue, request);
           
            // Convert data to XML
            ViewQueueXAO viewQueueXAO = new ViewQueueXAO();
            DOMUtil.addSection(responseDocument, ((Document) viewQueueXAO.generateXML(viewQueue)).getChildNodes()); 
                
            // Genrate base page data
            BaseDataBuilder data = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument, data.loadViewQueueGenericData(dataRequest, request));
        
            // Transform for presentation
            File xslFile = new File(getServletContext().getRealPath(XSL_VIEW_QUEUE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
                
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
    }
    
    private void clearPreviousViews(String userId, DataRequest dataRequest) throws Exception
    {
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        dataRequest.reset();
        dataRequest.setStatementID("DELETE_CSR_VIEWED_IDS");
        dataRequest.addInputParam("IN_CSR_ID", userId);
        dataRequest.addInputParam("IN_ENTITY_TYPE", "MERCURY");
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
    }
       
    private String loadNextMercuryId(String userId, DataRequest dataRequest) throws Exception
    {
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        dataRequest.reset();
        dataRequest.setStatementID("GET_VIEW_QUEUE_MERCURY_ID");
        dataRequest.addInputParam("IN_CSR_ID", userId);
        String mercuryId = (String) dataAccessUtil.execute(dataRequest);
        
        return mercuryId;
    }
    
    private String pullCurrentUser(String securityToken) throws Exception
    {
        String userId = null;
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            userId = securityManager.getUserInfo(securityToken).getUserID();
        }
        else
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
        
        return userId;
    }
    
    private void redirectToMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

        String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
        String adminAction = (String) request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {
            mainExitURL = mainExitURL + adminAction;
        }
        else
        {
            mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }

        response.sendRedirect(mainExitURL + securityParams);
    }
    
    private ViewQueueVO getNextMessage(DataRequest dataRequest, String userId) throws Exception
    {
        // Load the next mercury id available in the queue
        ViewQueueVO viewQueue = null;
        String mercuryId = this.loadNextMercuryId(userId, dataRequest);

        while (mercuryId != null && mercuryId.length() > 0 && viewQueue == null)    
        {
            logger.debug("Retrieved mercuryID = " + mercuryId);            
            // Load the data for the mercury id
            if(mercuryId != null && mercuryId.length() > 0)
            {
                // Load data for queue fromd database
                ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
                viewQueue = viewQueueDAO.getViewQueue(mercuryId);
            }
            if (viewQueue == null)
            {
                mercuryId = this.loadNextMercuryId(userId, dataRequest);
            }
        }
        return viewQueue;
    }
    
} // end of the servlet class.
