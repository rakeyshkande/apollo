package com.ftd.applications.lmd.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.vo.FloristBlockVO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Servlet to process external block requests.
 * 
 */

public class FloristBlockRequestServlet extends HttpServlet 
{    

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.FloristBlockRequestServlet";

    
    public static final String BLOCK_PARM_FLORISTID = "florist_id";
    public static final String BLOCK_PARM_BLOCKTYPE = "block_type";
    public static final String BLOCK_PARM_CODE = "block_unblock_code";
    public static final String BLOCK_PARM_START_DATE = "block_start_date";
    public static final String BLOCK_PARM_END_DATE = "block_end_date";
    public static final String BLOCK_PARM_USER = "blocked_by";
    public static final String BLOCK_PARM_COMMENT = "comment";
    public static final String BLOCK_PARM_VALIDATION_CODE = "VALIDATION_CODE";
    public static final String BLOCK_PARM_DATE_FORMAT = "MM/dd/yyyy";
    
    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * Process the block/unblock request with validation along the way.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException
    {  		
        response.setContentType(CONTENT_TYPE);
        
        // get and validate the block/unblock and validation codes
        String blockCode = request.getParameter(BLOCK_PARM_CODE);
        String blockType = request.getParameter(BLOCK_PARM_BLOCKTYPE);
        String incomingValCode = request.getParameter(BLOCK_PARM_VALIDATION_CODE);
        String localValCode = null;
        try
        {
            localValCode = ConfigurationUtil.getInstance().getFrpGlobalParm(
                    ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, 
                    BLOCK_PARM_VALIDATION_CODE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e);
            throw new ServletException(e.getMessage());
        }
        
        // verify that the incoming validation code matches the one in global parms, like a password.
        if (incomingValCode == null || !incomingValCode.equals(localValCode))
        {
            logger.error("Invalid validation code");
            throw new ServletException("Invalid validation code");
        }
        
        // only support S blocks for now
        if (!"S".equals(blockType))
        {
            logger.error("Invalid block type: " + blockType);
        	throw new ServletException("Invalid block type: " + blockType);
        }
         
        try
        {
            // call correct action
            if ("BLOCK".equals(blockCode))
                blockRequest(request, response);
            else if ("UNBLOCK".equals(blockCode))
                unblockRequest(request, response);
            else
                throw new ServletException("Invalid block code: " + blockCode);

            createSuccessResponse(response);
        } 
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 

    }
	
    /**
     * Create the block request specified in the request parameters. 
     * Partially stubbed out until other block related changes are made.
     * @param request
     * @param response
     * @throws Exception
     */
    private void blockRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        DataRequest dataRequest = null;

        try
        {
            dataRequest = DataRequestHelper.getInstance().getDataRequest();         
            FloristBlockVO vo = populateFloristBlockVO(request);

            String floristID = request.getParameter(BLOCK_PARM_FLORISTID);

            if (!hasExistingBlock(floristID, vo.getBlockStartDate(), dataRequest))
            {
                MaintenanceDAO mdao = new MaintenanceDAO(dataRequest.getConnection());
                mdao.insertFloristBlock(floristID, vo, vo.getBlockUser());
            }

        }
        catch(Exception e)
        {
            logger.error(e);
            throw e;
        }
        finally
        {
            try 
            {
                if(dataRequest != null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
                throw se;
            }
        }    
    }


    /**
     * STUBBED OUT METHOD
     * @param request
     * @param response
     * @throws Exception
     */
    private void unblockRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        DataRequest dataRequest = null;

        try
        {
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            FloristBlockVO vo = populateFloristBlockVO(request);

            MaintenanceDAO mdao = new MaintenanceDAO(dataRequest.getConnection());
            String floristID = request.getParameter(BLOCK_PARM_FLORISTID);

            mdao.removeFloristBlock(floristID, vo, vo.getBlockUser());
        }
        catch(Exception e)
        {
            logger.error(e);
            throw e;
        }
        finally
        {
            try 
            {
                if(dataRequest != null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
                throw se;
            }
        }
    }

    
    /**
     * Populate a floristBlockVO from a request object
     * @param request
     * @return
     * @throws Exception
     */
    private FloristBlockVO populateFloristBlockVO(HttpServletRequest request) throws Exception
    {
        Date startDate;
        Date endDate;
        startDate = FieldUtils.formatStringToUtilDate(request.getParameter(BLOCK_PARM_START_DATE));
        endDate = FieldUtils.formatStringToUtilDate(request.getParameter(BLOCK_PARM_END_DATE));
        
        FloristBlockVO vo = new FloristBlockVO();
        vo.setBlockEndDate(startDate);
        vo.setBlockFlag(request.getParameter(BLOCK_PARM_CODE));
        vo.setBlockStartDate(endDate);
        vo.setBlockType(request.getParameter(BLOCK_PARM_BLOCKTYPE));
        vo.setBlockUser(request.getParameter(BLOCK_PARM_USER));
        return vo;
    }

    /**
     * Create a SUCCESS response.
     * @param response
     * @throws IOException
     */
    private void createSuccessResponse(HttpServletResponse response) throws IOException
    {
        PrintWriter out = response.getWriter();
        out.println("SUCCESS");
        out.close();
    }

    private boolean hasExistingBlock(String floristID, Date blockStartDate, DataRequest dataRequest)
            throws Exception
    {
        boolean hasExistingBlock = false;
        FloristDAO floristDAO = new FloristDAO();

        List floristBlockList = floristDAO.getFloristBlocks(floristID, dataRequest.getConnection());

        for (int i=0; i < floristBlockList.size();i++)
        {
            FloristBlockVO floristBlockVO = (FloristBlockVO) floristBlockList.get(i);

            // If the block date is within an existing block range

            logger.debug("Request Block Date: " + blockStartDate);
            logger.debug("Florist Block: " + floristBlockVO.getBlockType() + " " + floristBlockVO.getBlockStartDate() + " " + floristBlockVO.getBlockEndDate());
            if (floristBlockVO.getBlockType().equals("O"))
            {
                if ((blockStartDate.compareTo(floristBlockVO.getBlockStartDate()) >= 0))
                {
                    hasExistingBlock = true;
                }
            }
            else
            {
                if ((blockStartDate.compareTo(floristBlockVO.getBlockStartDate()) >= 0) &&
                    (blockStartDate.compareTo(floristBlockVO.getBlockEndDate()) <= 0)
                   )
                {
                    hasExistingBlock = true;
                }
            }
            logger.debug("Has Existing Block: " + hasExistingBlock);
        }


        return hasExistingBlock;
    }

}
