package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;
import com.ftd.osp.utilities.plugins.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class LoadFloristListServlet extends HttpServlet 
{

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.LoadFloristListServlet";
    private final static String XSL_FLORIST_LIST = "/xsl/zipFloristList.xsl";
    private final static String FLORIST_XPATH = "FLORISTS/FLORIST";

 
    
    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }    
    
    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }  
  
    /**
     * Performs multiple pieces of functionality based on the type of action.
     * 
     * 1. loadFlorists: This action is called to load the Florist list page
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        String action = request.getParameter("action");
    
        if(action != null && action.equals("loadFlorists")) 
        {
          this.listFlorists(request, response);
        }		

    }  
  
  
	private void listFlorists(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	
    DataRequest dataRequest = null;
  
	  try
	  {
        String zipcode = request.getParameter("thezipcode");
        String deliverydate = request.getParameter("deliverydate");
        logger.info("zipcode: " + zipcode + " deliverydate: " + deliverydate);
		
		zipcode = zipcode.replaceAll("-", " ");
		zipcode = zipcode.trim();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date deliveryDate = sdf.parse(deliverydate);
		
  			// Create the initial document
    	Document responseDocument = DOMUtil.getDocument();    
    
        dataRequest = DataRequestHelper.getInstance().getDataRequest();    
        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
    
        File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_LIST));
        Document floristZips = maintDao.getFloristByZip(zipcode, deliveryDate);
        
        NodeList nl = DOMUtil.selectNodes(floristZips, FLORIST_XPATH);
        for (int i = 0; i < nl.getLength() ; i++)
        {
  
            //get element
            Element data = (Element) nl.item(i);
    
            //format the phone number            
            String value = data.getAttribute("phone_number");
            String formatted = formatPhoneNumber(value);
            data.setAttribute("phone_number",formatted);   
            
        
        }
        
        
        DOMUtil.addSection(responseDocument, floristZips.getChildNodes());        
		
    		HashMap pageData = new HashMap();
        pageData.put("zipcode", zipcode);
        pageData.put("deliverydate", deliverydate);
		
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
        TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, null);
        
	  }	  
	  catch(Exception e)
	  {
        logger.error(e);
    }
    finally
    {
      if(dataRequest.getConnection() != null)
      {
        try{
          dataRequest.getConnection().close();
        }
        catch(Exception e){ }
      }
    }
 
	
	}  
  
  private String formatPhoneNumber(String phoneNumber)
  {
    //if number is not null
    if(phoneNumber != null)
    {
      //if phone number is not already formatted with dashes
      if(phoneNumber.indexOf("-") < 0)
      {
        //if length of phone number is 10 (area code + number)
        if(phoneNumber.length() == 10)
        {
          phoneNumber = phoneNumber.substring(0,3) + "-" + phoneNumber.substring(3,6) + "-" + phoneNumber.substring(6);
        }//end if length = 10
      }//end if formatted
    }//end if null

   return phoneNumber;

  }//end method
  

  
}
