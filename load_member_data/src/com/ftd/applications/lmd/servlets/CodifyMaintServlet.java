package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.CodifyMaintDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.CodifyMaintUpdater;
import com.ftd.applications.lmd.util.CodifyMaintXAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.CodificationMaintenanceVO;
import com.ftd.applications.lmd.vo.CodificationMasterVO;
import com.ftd.applications.lmd.vo.CodificationProductsVO;
import com.ftd.applications.lmd.vo.CodificationVO;
import com.ftd.applications.lmd.vo.ProductVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;
import com.ftd.security.SecurityManager;

public class CodifyMaintServlet extends HttpServlet 
{

	private Logger logger;
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
	private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.CodifyMaintServlet";
	private final static String XSL_CODIFICATION_MAINTENANCE = "/xsl/codificationMaintenance.xsl";

    private final static String INVALID_SKU = "Entered SKU does not exist in database.";
    
    private final static String CURRENT_TAG_PARAM = "currentTab";
    private final static String PRODUCT_TAB = "product";
    private final static String CODIFICATION_TAB = "cod";

    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    
    }
    
    /**
     * Delegates to the doPost method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }
    
	/**
     * Calls the function to load the Florist Maintenance Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
		
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
    
        try
        {
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            { 
                if(action != null && action.equals("addCodifiedProduct")) 
                {
                   this.addCodifiedProduct(request, response);
                }
                else if(action != null && action.equals("addCodification")) 
                {
                   this.addCodification(request, response);
                }
                else if(action != null && action.equals("saveCodifications")) 
                {
                   this.saveCodifications(request, response);
                }
                else if(action != null && action.equals("exitCodifyMaint")) 
                {
                    this.exitCodifyMaint(request, response);
                }
                else
                {
                    this.loadpage(request,response);
                }
            }
        } 
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 
    }
    
    private void loadpage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		DataRequest dataRequest = null;
		
		try
		{
			HashMap pageData = new HashMap();
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

            CodifyMaintDAO maintDAO = new CodifyMaintDAO(dataRequest.getConnection());
            CodificationMaintenanceVO codificationMaintVO = maintDAO.getCodificationMaintenance();
            CodifyMaintXAO maintXAO = new CodifyMaintXAO();
            Document doc = (Document) maintXAO.generateXML(codificationMaintVO);
            
            DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(codificationMaintVO)).getChildNodes()); 
            
            //get data for drop downs
            BaseDataBuilder data = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument,data.loadCodificationGenericDataData(null, dataRequest, request, false));
            
            //add what tabpage to go to
            HashMap formdata = new HashMap();
            formdata.put(CURRENT_TAG_PARAM,PRODUCT_TAB);   
            DOMUtil.addSection(responseDocument,"FORMDATA","DATA",formdata,true);
            
            logger.debug(convertDocToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_CODIFICATION_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}//end try		
		catch(Exception e)
		{
			logger.error(e);
		}//end catch
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}//end finally
	}//end load page
        
        
    private void saveCodifications(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		DataRequest dataRequest = null;
		
		try
		{
			HashMap pageData = new HashMap();
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

            //get data from database
            CodifyMaintDAO maintDAO = new CodifyMaintDAO(dataRequest.getConnection());
            CodificationMaintenanceVO codificationMaintVO = maintDAO.getCodificationMaintenance();

            //combine data from DB with that of the request object
            CodifyMaintUpdater updater = new CodifyMaintUpdater(dataRequest);
            codificationMaintVO = updater.updateFloristFromRequest(dataRequest.getConnection(),codificationMaintVO,request);

            //save changes
            maintDAO.updateCodificationMaintenance(codificationMaintVO);

            //generate XML
            CodifyMaintXAO maintXAO = new CodifyMaintXAO();
            Document doc = (Document) maintXAO.generateXML(codificationMaintVO);
            
            //add xml to document
            DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(codificationMaintVO)).getChildNodes()); 
            
            //get data for drop downs
            BaseDataBuilder data = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument,data.loadCodificationGenericDataData(null, dataRequest, request, false));
            
            //add what tabpage to go to
            String tabpage = request.getParameter(CURRENT_TAG_PARAM);
            HashMap formdata = new HashMap();
            formdata.put(CURRENT_TAG_PARAM,tabpage);            
            DOMUtil.addSection(responseDocument,"FORMDATA","DATA",formdata,true);
            
            logger.debug(convertDocToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_CODIFICATION_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}//end try		
		catch(Exception e)
		{
			logger.error(e);
		}//end catch
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}//end finally             
    }

    private void addCodifiedProduct(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		DataRequest dataRequest = null;
        boolean valid = true;
        String errorText = "";
		
		try
		{
			HashMap pageData = new HashMap();
            
             CodificationProductsVO productVO = new CodificationProductsVO();
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

            //get data from database
            CodifyMaintDAO maintDAO = new CodifyMaintDAO(dataRequest.getConnection());
            CodificationMaintenanceVO codificationMaintVO = maintDAO.getCodificationMaintenance();

            //combine data from DB with that of the request object
            CodifyMaintUpdater updater = new CodifyMaintUpdater(dataRequest);
            codificationMaintVO = updater.updateFloristFromRequest(dataRequest.getConnection(),codificationMaintVO,request);

            //add new codification master to VO
            String productId = request.getParameter("newsku");
            if(productId != null)
            {         
                //populate with values from request           
                productVO.setProductID(productId);                
                productVO.setCheckOEflag(request.getParameter("newfrontendvalidation"));
                productVO.setCodificationID(request.getParameter("newproductcodificationcode"));                
    
                //retrieve product from database..this gets us the ftd product id and the product name
                ProductVO productMasterVO = maintDAO.getProduct(productId);   
                
                //if product not found then flag user with error
                if(productMasterVO == null)
                {
                    //error
                    valid = false;
                    errorText = INVALID_SKU;
                }
                else{
                    productVO.setProductName(productMasterVO.getName());
                    productVO.setProductID(productMasterVO.getFtdID());
                    productVO.setStatus(productMasterVO.getStatus());
        
                    //populate with values from request           
                    productVO.setProductID(productId);                
                    productVO.setCheckOEflag(request.getParameter("newfrontendvalidation"));
                    productVO.setCodificationID(request.getParameter("newproductcodificationcode"));                
                    //productVO.setDescription(request.getParameter("codifiedunblockdate"));
					
					//if(request.getParameter("newfrontendvalidation").equals("Y"))
                   // productVO.setFlagSequence(maintDAO.getNextSequence());                
                    
					//productVO.setStatus(request.getParameter("codifiedunblockdate"));
                    productVO.setDeleteFlag("N");
    
                    //get codification description
                    CodificationVO codification = maintDAO.getCodification(productVO.getCodificationID());
                    if(codification != null){
                        productVO.setDescription(codification.getDescription());
                    }
    
                    //add it to the map
                    codificationMaintVO.getCodificationProductMap().put(productVO.getProductID(), productVO);
                }
            }


            //generate XML
            CodifyMaintXAO maintXAO = new CodifyMaintXAO();
            Document doc = (Document) maintXAO.generateXML(codificationMaintVO);
            
            //add xml to document
            DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(codificationMaintVO)).getChildNodes()); 
            
            //get data for drop downs
            BaseDataBuilder data = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument,data.loadCodificationGenericDataData(null, dataRequest, request, false));
			
            //add error if needed
            HashMap formdata = new HashMap();
            if(!valid){            
                HashMap errorMap = new HashMap();
                errorMap.put("formError",errorText);
                DOMUtil.addSection(responseDocument,"ERRORS","ERROR",errorMap,true);               

                //add user entered data
                formdata.put("productSKU",productVO.getProductID());
                formdata.put("codificationCode",productVO.getCodificationID());
                formdata.put("frontEndValidation",productVO.getCheckOEflag());                
                
            }
            
            //add what tabpage to go to
            formdata.put(CURRENT_TAG_PARAM,PRODUCT_TAB);
            DOMUtil.addSection(responseDocument,"FORMDATA","DATA",formdata,true);
            
            logger.debug(convertDocToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_CODIFICATION_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}//end try		
		catch(Exception e)
		{
			logger.error(e);
		}//end catch
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}//end finally                
    }
    
    /**
     * 
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void exitCodifyMaint(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			// Redirect to menu
            this.redirectToMenu(request, response);
		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    private void addCodification(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		DataRequest dataRequest = null;
		
		try
		{
			HashMap pageData = new HashMap();
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

            //get data from database
            CodifyMaintDAO maintDAO = new CodifyMaintDAO(dataRequest.getConnection());
            CodificationMaintenanceVO codificationMaintVO = maintDAO.getCodificationMaintenance();

            //combine data from DB with that of the request object
            CodifyMaintUpdater updater = new CodifyMaintUpdater(dataRequest);
            codificationMaintVO = updater.updateFloristFromRequest(dataRequest.getConnection(),codificationMaintVO,request);

            //get codification from request
            String codificationId = request.getParameter("newcodificationcode");

            //add new codification master to VO            
            if(codificationId != null)
            {
                CodificationMasterVO masterVO = new CodificationMasterVO();
                //populate with values from request
                masterVO.setCodificationID(codificationId);
                masterVO.setPreviousCodificationID(codificationId);
                masterVO.setCategory_id(request.getParameter("newcodificationcategory"));
                masterVO.setCodifyAllFloristFlag(request.getParameter("newcodifyall"));                
                masterVO.setCodificationRequired(request.getParameter("newrequired"));                
                masterVO.setDescription(request.getParameter("newdescription"));
                masterVO.setLoadFromFileFlag("N");   // For manually added codifications this is always N

                //add it to the map
                codificationMaintVO.getCodificationMasterMap().put(masterVO.getCodificationID(), masterVO);
            }
            

            //generate XML
            CodifyMaintXAO maintXAO = new CodifyMaintXAO();
            Document doc = (Document) maintXAO.generateXML(codificationMaintVO);
            
            //add xml to document
            DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(codificationMaintVO)).getChildNodes()); 
            
            //get data for drop downs
            BaseDataBuilder data = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument,data.loadCodificationGenericDataData(null, dataRequest, request, false));
            
            //add what tabpage to go to
            HashMap formdata = new HashMap();
            formdata.put(CURRENT_TAG_PARAM,CODIFICATION_TAB);
            DOMUtil.addSection(responseDocument,"FORMDATA","DATA",formdata,true);            
            
            logger.debug(convertDocToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_CODIFICATION_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}//end try		
		catch(Exception e)
		{
			logger.error(e);
		}//end catch
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}//end finally        
    }    
 
         private Integer getIntegerValue(String value)
        {
            return value == null || value.trim().length() == 0? null : new Integer(value);
        }
        private Long getLongValue(String value)
        {
            return value == null || value.trim().length() == 0? null : new Long(value);
        }
 
        
    /** This method converts an XML document to a String. 
     * @param Document document to convert 
     * @return String version of XML */
    private static String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";       
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
    }      
    
    private String pullCurrentUser(String securityToken) throws Exception
    {
        String userId = null;
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            userId = securityManager.getUserInfo(securityToken).getUserID();
        }
        else
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
        
        return userId;
    }
    
    private void redirectToMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

        String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
        String adminAction = (String) request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {
            mainExitURL = mainExitURL + adminAction;
        }
        else
        {
            mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }

        response.sendRedirect(mainExitURL + securityParams);
    }
   
    
}
