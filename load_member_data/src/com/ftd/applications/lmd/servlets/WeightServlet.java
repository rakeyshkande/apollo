package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;
import com.ftd.osp.utilities.plugins.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class WeightServlet extends HttpServlet 
{

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.WeightServlet";
    private final static String XSL_FLORIST_LIST = "/xsl/weightHistory.xsl";
    private final static String FLORIST_XPATH = "WEIGHTS/WEIGHT";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }    
    
    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }  
  
    /**
     * Performs multiple pieces of functionality based on the type of action.
     * 
     * 1. loadFlorists: This action is called to load the Florist list page
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        String action = request.getParameter("action");
    
        if(action != null && action.equals("loadWeightHistory")) 
        {
          this.listWeightHistory(request, response);
        }		

    }  
  
  
	private void listWeightHistory(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	
    DataRequest dataRequest = null;
  
	  try
	  {
        String floristid = request.getParameter("floristid");
    
  		// Create the initial document
    	Document responseDocument = DOMUtil.getDocument();    
    
        dataRequest = DataRequestHelper.getInstance().getDataRequest();    
        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
    
        File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_LIST));
        Document floristWeights = maintDao.getWeights(floristid);
        
        DOMUtil.addSection(responseDocument, floristWeights.getChildNodes());    
        TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, null);
        
	  }	  
	  catch(Exception e)
	  {
        logger.error(e);
    }
    finally
    {
      if(dataRequest.getConnection() != null)
      {
        try{
          dataRequest.getConnection().close();
        }
        catch(Exception e){ }
      }
    }

	}  
  
}
