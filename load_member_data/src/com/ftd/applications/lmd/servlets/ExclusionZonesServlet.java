package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.ExclusionZoneVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;

public class ExclusionZonesServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
	private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.ExclusionZonesServlet";
	private final static String XSL_EXCLUSION_ZONES = "/xsl/exclusionZones.xsl";
	private Logger logger;

    public void init(ServletConfig config) throws ServletException {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    
    }
    
    /**
     * Delegates to the doPost method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	logger.info("doGet()");
        this.doPost(request, response);
    }
    
	/**
     * Calls the function to load the Florist Maintenance Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
		
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        logger.info("action: " + action);
    
        try {
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) { 
                if(action != null && action.equals("load")) {
                   this.loadMainPage(request, response);
                } else if(action != null && action.equals("submit")) {
                    this.submitPage(request, response);
                } else if(action != null && action.equals("checkCity")) {
                    this.checkCity(request, response);
                } else if(action != null && action.equals("autoCity")) {
                    this.autoCity(request, response);
                } else if(action != null && action.equals("showDetails")) {
                    this.showDetails(request, response);
                } else if(action != null && action.equals("remove")) {
                    this.remove(request, response);
                } else {
                    logger.error("Invalid action: " + action);
                    this.redirectToMenu(request, response);
                }
            }
        } catch (Exception e) {
            logger.error(e);
            throw new ServletException(e);
        } 
    }
    
    private void loadMainPage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DataRequest dataRequest = null;
		
		try {
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();

			dataRequest = DataRequestHelper.getInstance().getDataRequest();			
	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
	        Document stateList = maintDao.getPasStateList();
	        DOMUtil.addSection(responseDocument, stateList.getChildNodes());

	        Document detailList = maintDao.getExclusionZoneDetailList();
	        DOMUtil.addSection(responseDocument, detailList.getChildNodes());

            File xslFile = new File(getServletContext().getRealPath(XSL_EXCLUSION_ZONES));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    @SuppressWarnings("rawtypes")
	private void submitPage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DataRequest dataRequest = null;
		
		try {
		    
			ExclusionZoneVO ezVO = new ExclusionZoneVO();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
			String description = request.getParameter("description");
			logger.info(description);
			ezVO.setDescription(description);
			String blockStartDate = request.getParameter("startDate");
			logger.info(blockStartDate);
			ezVO.setBlockStartDate(sdf.parse(blockStartDate));
			String blockEndDate = request.getParameter("endDate");
			logger.info(blockEndDate);
			ezVO.setBlockEndDate(sdf.parse(blockEndDate));
			String blockCities = request.getParameter("blockCities");
			String blockStates = request.getParameter("blockStates");
			String blockZipCodes = request.getParameter("blockZipCodes");
			
			if (blockCities != null && blockCities.length() > 0) {
			    String[] cityList = blockCities.split("\\n");
			    logger.info("cityList: " + cityList.length);
			    ezVO.setBlockCities(Arrays.asList(cityList));
			}
			
			if (blockStates != null && blockStates.length() > 0) {
			    String[] stateList = blockStates.split(",");
			    logger.info("stateList: " + stateList.length);
			    ezVO.setBlockStates(Arrays.asList(stateList));
			}
			
			if (blockZipCodes != null && blockZipCodes.length() > 0) {
			    String[] zipList = blockZipCodes.split("\\n");
			    logger.info("zipList: " + zipList.length);
			    ezVO.setBlockZipCodes(Arrays.asList(zipList));
			}
			
	        SecurityManager securityManager = SecurityManager.getInstance();
	        String userId = securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID();
	        ezVO.setUserId(userId);
	        logger.info(userId);
	        
	        dataRequest = DataRequestHelper.getInstance().getDataRequest();			
	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
			Map outputs = maintDao.saveExclusionZone(ezVO);

			String headerId = (String) outputs.get("headerId");
			logger.info("headerId: " + headerId);
			
			String status = (String) outputs.get("OUT_STATUS");
			Document ajaxResponse = DOMUtil.getDocument();
	        if(status != null && status.equals("Y")) {
				ajaxResponse = maintDao.getExclusionZoneById(headerId);
	        } else {
				//ajaxResponse = "NO";
				logger.error("didn't work");
	        }
			
			JSONObject json = XML.toJSONObject(DOMUtil.convertToString(ajaxResponse));
			logger.info(json.toString());
			byte[] byteData = json.toString().getBytes();
			response.getOutputStream().write(byteData);

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    @SuppressWarnings("rawtypes")
	private void checkCity(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		DataRequest dataRequest = null;

		try {
		    
			String ajaxResponse = "";
			String cityName = request.getParameter("cityName");
			logger.info("cityName: " + cityName);
			if (cityName == null || cityName.equals("") || cityName.length() < 4) {
				ajaxResponse = "NO";
			} else {
				cityName = cityName.toUpperCase();
                String city = cityName.substring(0, cityName.length() - 4);
                String state = cityName.substring(cityName.length() - 2);
                logger.info("<" + city + "> <" + state + ">");
				
    			dataRequest = DataRequestHelper.getInstance().getDataRequest();			
    	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
				Map outputs = maintDao.verifyErosCity(city, state);
		        String status = (String) outputs.get("OUT_STATUS");
		        if(status != null && status.equals("N")) {
					ajaxResponse = "NO";
		        } else {
					ajaxResponse = cityName;
		        }
			}
			
			byte[] byteData = ajaxResponse.getBytes();
			response.getOutputStream().write(byteData);

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void autoCity(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		DataRequest dataRequest = null;

		try {
		    
		    String tempCity = request.getParameter("city").toUpperCase();
		    logger.info("city: " + tempCity);

			dataRequest = DataRequestHelper.getInstance().getDataRequest();			
	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
	        ArrayList<String> results = maintDao.getErosCities(tempCity);

		    JSONArray jsonArray = new JSONArray(results);
		    logger.info(jsonArray.toString());
		    
			byte[] byteData = jsonArray.toString().getBytes();
			response.getOutputStream().write(byteData);

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void showDetails(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		DataRequest dataRequest = null;

		try {
		    
		    String headerId = request.getParameter("headerId");
		    logger.info("headerId: " + headerId);
		    String blockSource = request.getParameter("blockSource");
		    logger.info("blockSource: " + blockSource);

			dataRequest = DataRequestHelper.getInstance().getDataRequest();			
	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
	        ArrayList<String> results = maintDao.getExclusionZoneDetails(headerId, blockSource);

		    JSONArray jsonArray = new JSONArray(results);
		    logger.info(jsonArray.toString());
		    
			byte[] byteData = jsonArray.toString().getBytes();
			response.getOutputStream().write(byteData);

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void remove(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		DataRequest dataRequest = null;

		try {
		    
		    String removeIds = request.getParameter("removeIds");
		    logger.info("removeIds: " + removeIds);
		    
	        SecurityManager securityManager = SecurityManager.getInstance();
	        String userId = securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID();
	        logger.info(userId);

	        dataRequest = DataRequestHelper.getInstance().getDataRequest();			
	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());

	        if (removeIds != null && removeIds.length() > 0) {
			    String[] headerList = removeIds.split(",");
			    for (int i=0; i<headerList.length; i++) {
			    	String headerId = headerList[i];
			    	logger.info("headerId: " + headerId);
			    	Map outputs = maintDao.removeExclusionZone(headerId, userId);
			        String status = (String) outputs.get("OUT_STATUS");
			        if(status == null || status.equals("N")) {
			        	String message = (String) outputs.get("OUT_MESSAGE");
			        	logger.error(message);
			        }
			    }

		    }

			Document responseDocument = DOMUtil.getDocument();

	        Document stateList = maintDao.getPasStateList();
	        DOMUtil.addSection(responseDocument, stateList.getChildNodes());

	        Document detailList = maintDao.getExclusionZoneDetailList();
	        DOMUtil.addSection(responseDocument, detailList.getChildNodes());

            File xslFile = new File(getServletContext().getRealPath(XSL_EXCLUSION_ZONES));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void redirectToMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

        String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
        String adminAction = (String) request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {
            mainExitURL = mainExitURL + adminAction;
        }
        else
        {
            mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }

        response.sendRedirect(mainExitURL + securityParams);
    }
   
}
