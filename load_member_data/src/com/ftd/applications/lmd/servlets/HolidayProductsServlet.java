package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Document;

public class HolidayProductsServlet extends HttpServlet {

	private Logger logger;
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
	private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.HolidayProductsServlet";
	private final static String XSL_HOLIDAY_PRODUCTS = "/xsl/holidayProducts.xsl";
	private final static String XSL_HOLIDAY_PRODUCTS_DETAIL = "/xsl/holidayProductsDetail.xsl";

    public void init(ServletConfig config) throws ServletException {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    
    }
    
    /**
     * Delegates to the doPost method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    
	/**
     * Calls the function to load the Florist Maintenance Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
		
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        logger.info("action: " + action);
    
        try {
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) { 
                if(action != null && action.equals("load")) {
                   this.loadMainPage(request, response);
                } else if(action != null && action.equals("loadDetail")) {
                   this.loadDetailPage(request, response);
                } else if(action != null && action.equals("addProducts")) {
                    this.addProducts(request, response);
                } else if(action != null && action.equals("removeProducts")) {
                    this.removeProducts(request, response);
                } else {
                    logger.error("Invalid action: " + action);
                    this.redirectToMenu(request, response);
                }
            }
        } catch (Exception e) {
            logger.error(e);
            throw new ServletException(e);
        } 
    }
    
    private void loadMainPage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DataRequest dataRequest = null;
		
		try {
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
	        Document productList = maintDao.getAllHolidayProducts();
	        DOMUtil.addSection(responseDocument, productList.getChildNodes());
	        logger.info(DOMUtil.convertToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_HOLIDAY_PRODUCTS));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void loadDetailPage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DataRequest dataRequest = null;
		
		try {
			String deliveryDate = request.getParameter("delivery_date");
			logger.info("deliveryDate: " + deliveryDate);
			Date dDate = null;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				dDate = sdf.parse(deliveryDate);
				deliveryDate = sdf.format(dDate);
			} catch (Exception e) {
				logger.error("Invalid date format");
				this.loadMainPage(request, response);
				return;
			}
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
	        Document productList = maintDao.getHolidayProductsDetail(dDate);
	        DOMUtil.addSection(responseDocument, productList.getChildNodes());
	        
            HashMap pageData = new HashMap();
            pageData.put("delivery_date", deliveryDate);
 			DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
	        logger.info(DOMUtil.convertToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_HOLIDAY_PRODUCTS_DETAIL));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void addProducts(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DataRequest dataRequest = null;
		String deliveryDate = request.getParameter("delivery_date");
		String newProducts = request.getParameter("newproducts");
		logger.info("deliveryDate: " + deliveryDate);
		String tempProducts = newProducts.replaceAll(",", " ");
		//tempProducts = tempProducts.replaceAll("\n", " ");
		//tempProducts = tempProducts.trim().replaceAll(" +", " ");
		tempProducts = tempProducts.replaceAll("\\s{2,}", " ");
		String[] prodList = tempProducts.split(" ");
		
		try {
			Date dDate = null;
			String jmsDate = "";
			Date maxPASDate = this.getMaxPASDate();
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat osdf = new SimpleDateFormat("MMddyyyy");
				dDate = sdf.parse(deliveryDate);
				jmsDate = osdf.format(dDate);
				logger.info("maxPASDate: " + sdf.format(maxPASDate));
			} catch (Exception e) {
				logger.error("Invalid date format");
				this.loadMainPage(request, response);
				return;
			}
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

			String errorMessage = null;
			boolean somethingAdded = false;
	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
			for (int i=0; i<prodList.length; i++) {
				if (prodList[i] != null && prodList[i].length() > 0) {
					logger.info(i + " " + prodList[i]);
					Map outputs = maintDao.insertHolidayProducts(prodList[i], dDate);
			        String status = (String) outputs.get("OUT_STATUS");
			        if(status != null && status.equals("N")) {
			            String message = (String) outputs.get("OUT_MESSAGE");
			            if (errorMessage == null) {
			            	errorMessage = message;
			            } else {
			            	errorMessage += ", " + message;
			            }
			        } else {
			        	somethingAdded = true;
			        }
				}
			}
			if (somethingAdded) {
				if (!dDate.after(maxPASDate)) {
				    logger.info("Queueing JMS message");
    			    FloristDAO floristDao = new FloristDAO();
	    		    floristDao.postOJMSMessage(dataRequest.getConnection(),
					    "OJMS.PAS_COMMAND", "ADD_PRODUCT_DATE " + jmsDate + " ALL",
					    "ADD_PRODUCT_DATE " + jmsDate + " ALL", 3);
				}
			}

			Document productList = maintDao.getHolidayProductsDetail(dDate);
	        DOMUtil.addSection(responseDocument, productList.getChildNodes());
	        
            HashMap pageData = new HashMap();
            pageData.put("delivery_date", deliveryDate);
            pageData.put("error_message", errorMessage);
 			DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
	        //logger.info(DOMUtil.convertToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_HOLIDAY_PRODUCTS_DETAIL));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void removeProducts(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DataRequest dataRequest = null;
		String deliveryDate = request.getParameter("delivery_date");
		String productCount = request.getParameter("productCount");
		logger.info("deliveryDate: " + deliveryDate + " " + productCount);
		
		try {
			Date dDate = null;
			int prodCount = 0;
			String jmsDate = "";
			Date maxPASDate = this.getMaxPASDate();
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat osdf = new SimpleDateFormat("MMddyyyy");
				dDate = sdf.parse(deliveryDate);
				jmsDate = osdf.format(dDate);
				prodCount = Integer.parseInt(productCount);
				logger.info("maxPASDate: " + sdf.format(maxPASDate));
			} catch (Exception e) {
				logger.error("Invalid date format");
				this.loadMainPage(request, response);
				return;
			}
			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			

	        MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
	        for (int i=1; i<=prodCount; i++) {
	        	String productId = request.getParameter("productid" + i);
	        	String removeFlag = request.getParameter("checkprod" + i);
	        	if (removeFlag != null && removeFlag.equals("on")) {
		        	logger.info(i + " " + productId + " " + removeFlag);
	        	    maintDao.deleteHolidayProducts(productId, dDate);
	        	}
	        }
	        
			if (!dDate.after(maxPASDate)) {
			    logger.info("Queueing JMS message");
			    FloristDAO floristDao = new FloristDAO();
			    floristDao.postOJMSMessage(dataRequest.getConnection(),
					"OJMS.PAS_COMMAND", "ADD_PRODUCT_DATE " + jmsDate + " ALL",
					"ADD_PRODUCT_DATE " + jmsDate + " ALL", 3);
			}

	        Document productList = maintDao.getHolidayProductsDetail(dDate);
	        DOMUtil.addSection(responseDocument, productList.getChildNodes());
	        
            HashMap pageData = new HashMap();
            pageData.put("delivery_date", deliveryDate);
 			DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
	        //logger.info(DOMUtil.convertToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_HOLIDAY_PRODUCTS_DETAIL));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		} catch(Exception e) {
			logger.error(e);
		} finally {
            try {
                if(dataRequest !=null && dataRequest.getConnection() != null) {
                    dataRequest.getConnection().close();
                }
            } catch(SQLException se) {
                logger.error(se);
            }
		}
	}
        
    private void redirectToMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

        String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
        String adminAction = (String) request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {
            mainExitURL = mainExitURL + adminAction;
        }
        else
        {
            mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }

        response.sendRedirect(mainExitURL + securityParams);
    }
   
    private Date getMaxPASDate() throws Exception {
    	Date maxDate = new Date();
        String maxDays = ConfigurationUtil.getInstance().getFrpGlobalParm("PAS_CONFIG", "MAX_DAYS");
        int maxDaysInt = Integer.parseInt(maxDays);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.add(Calendar.DATE, maxDaysInt);
        maxDate = cal.getTime();

    	return maxDate;
    }
}
