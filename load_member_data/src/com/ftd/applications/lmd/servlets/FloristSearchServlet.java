package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.constants.SecurityConstants;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.LockHelper;
import com.ftd.applications.lmd.util.MaintenanceXAO;
import com.ftd.applications.lmd.util.SearchManager;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.SearchResultVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * This servlet provides the functionality to search florists and display the results.
 * Please reference SearchManager classe for more detail on search 
 * business logic.
 *
 * @author Mehul Patel
 */

public class FloristSearchServlet extends HttpServlet 
{    

    private Logger logger;
    BaseDataBuilder data;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.FloristSearchServlet";
    private final static String XSL_FLORIST_SEARCH = "/xsl/floristSearch.xsl";
    private final static String XSL_SEARCH_RESULT_LIST = "/xsl/searchResults.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
		data = new BaseDataBuilder();
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.
     * 
     * 1. loadSearch: This action is called to load the main Florist Search Page.
     * 2. searchFlorist: This action is called to search for florists and displaying them.
     * 3. last: This action is called in displaying the 'last' or the 'last 50' viewed florists.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        
        try
        {        
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            { 
                if(action != null && action.equals("loadFloristSearch")) 
                {
                    this.loadSearch(request, response, null);
                }
                else if(action != null && action.equals("searchFlorist")) 
                {
                    this.submitSearch(request, response);
                }
                else if(action != null && action.equals("last"))
                {
                    this.last(request, response);
                }
                else if(action != null && action.equals("exitSystem"))
                {
                    this.exitSystem(request, response);
                }
                else
                {
                    throw new ServletException("Action Not Supported");
                }
            }
        }
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 

    }

	/**
	* Gets and displays the last specified number of florists that the user has viewed.
    * 
    * @param request HttpServletRequest
    * @param response HttpServletResponse
    * 
    * @exception ServletException
	* @exception IOException
	*/
    private void last(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        
        try
        {
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            // Release lock if coming from a profile
            String floristId = request.getParameter("mNum");
            if(floristId != null && floristId.trim().length() > 0)
            {
                SecurityManager securityManager = SecurityManager.getInstance();
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.releaseMaintenanceLock(floristId, securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
            }  
         
            SearchManager searchManager = new SearchManager();
            ArrayList resultList = searchManager.retrieveLastMap(request, dataRequest);
            
            if(resultList != null && resultList.size() > 0)
            {
                this.appendResultList(resultList, responseDocument);
                DOMUtil.addSection(responseDocument, "securityPermissions", "permission", data.generateSecurityData(request, dataRequest), true);
                // Load search criteria
                DOMUtil.addSection(responseDocument, "searchCriteria", "criteria", data.generateSearchCriteria(request), true);
                File xslFile = new File(getServletContext().getRealPath(XSL_SEARCH_RESULT_LIST));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
            else
            {
                // No search results found with the specified search criteria
                this.loadSearch(request, response, "No florist found for search criteria entered.");
            }
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
    * Submits the search criteria and invokes the search results page.
    * 
    * @param request HttpServletRequest
    * @param response HttpServletResponse
    * 
    * @exception ServletException
    * @exception IOException
    */
    
    private void submitSearch(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
    
        try
        {
            // Initialize data request
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            // Release lock if coming from a profile
            String floristId = request.getParameter("mNum");
            if(floristId != null && floristId.trim().length() > 0)
            {
                SecurityManager securityManager = SecurityManager.getInstance();
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.releaseMaintenanceLock(floristId, securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
            }  
            
            // Retrieve sorted search result list and add to xml
            ArrayList resultList = null;    
            
            if(request.getParameter("sc_zipcode") != null && request.getParameter("sc_zipcode").length() > 0)
            {
        
                dataRequest.reset();
                dataRequest.setStatementID("GET_ZIP_DETAILS");
                
                dataRequest.addInputParam("ZIP_CODE", request.getParameter("sc_zipcode"));
                String zipCodeExists = (String)dataAccessUtil.execute(dataRequest);
                
                if(!zipCodeExists.equals("Y"))
                {
                this.loadSearch(request, response, "Zip Code does not exist.");    
                return;
                }
            }
    
            // Retrieve sorted search result list and add to xml
            SearchManager searchManager = new SearchManager();
            
            resultList = searchManager.retrieveResultMap(request, dataRequest);
            
			if(resultList != null && resultList.size() == 1)
			{
			// direct to maintenanace page...	
			
			
			FloristMaintenanceServlet f = new FloristMaintenanceServlet();
			SearchResultVO v = (SearchResultVO) resultList.get(0);
			String fnum = v.getFlornum();
			
			SecurityManager securityManager = SecurityManager.getInstance();
		
			ServletHelper servletHelper = new ServletHelper();
		
			MaintenanceDAO maintDAO = new MaintenanceDAO(dataRequest.getConnection());
			FloristVO florist = maintDAO.load(fnum, servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
			
			// Update lock for user
			LockHelper lockHelper = new LockHelper(dataRequest);
			lockHelper.requestMaintenanceLock(florist, securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
			
			// Convert to XML
			MaintenanceXAO maintXAO = new MaintenanceXAO();
			DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(florist)).getChildNodes()); 
			// Genrate base page data
			BaseDataBuilder data = new BaseDataBuilder();
			DOMUtil.addSection(responseDocument,data.loadGenericDataData(null, dataRequest, request, false));

			HashMap pageData = new HashMap();
            pageData.put("display", "no");
			DOMUtil.addSection(responseDocument, "link", "data", pageData, true);

            File xslFile = new File(getServletContext().getRealPath("/xsl/floristMaintenance.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
				
			}
            else if(resultList != null && resultList.size() > 0)
            {
                this.appendResultList(resultList, responseDocument);
				  // Load security permissions        
				DOMUtil.addSection(responseDocument, "securityPermissions", "permission", data.generateSecurityData(request, dataRequest), true);
			
                DOMUtil.addSection(responseDocument,data.loadGenericDataData(null, dataRequest, request, false));
                
				File xslFile = new File(getServletContext().getRealPath(XSL_SEARCH_RESULT_LIST));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
            else
            {
                    // No search results found with the specified search criteria
                    this.loadSearch(request, response, "No florist found for search criteria entered.");
            }
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
    * Makes calls to obtain the states and the status lists and loads the search page.
    * 
    * @param request HttpServletRequest
    * @param response HttpServletResponse
    * @param messageDisplay String
    * 
    * @exception ServletException
    * @exception IOException
    */
    private void loadSearch(HttpServletRequest request, HttpServletResponse response, String messageDisplay) throws IOException, ServletException
    {
        DataRequest dataRequest = null;

        try
        {
			// Create the initial document
            Document responseDocument = DOMUtil.getDocument();
        
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            // Release lock if coming from a profile
            String floristId = request.getParameter("mNum");
            if(floristId != null && floristId.trim().length() > 0)
            {
                SecurityManager securityManager = SecurityManager.getInstance();
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.releaseMaintenanceLock(floristId, securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
            }  

			// load and output the Status List
			dataRequest.reset();
			dataRequest.setStatementID("STATUS_LIST_LOOKUP");
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  
        
			// load and output the State List
			dataRequest.reset();
			dataRequest.setStatementID("STATE_LIST_LOOKUP");
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  
        
			// Create page data
            HashMap pageData = new HashMap();
			
			// Load pageData fields.
            if(messageDisplay != null)
            {
                if(messageDisplay == "Zip Code does not exist.")
                    pageData.put("zip_display", messageDisplay);
                else
                    pageData.put("message_display", messageDisplay);
            }
    
            DOMUtil.addSection(responseDocument, "securityPermissions", "permission", this.loadSecurityPermissions(request), true);
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            DOMUtil.addSection(responseDocument, "searchCriteria", "criteria", data.generateSearchCriteria(request), true);
             
			 
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_SEARCH));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
        }
    }
    private void exitSystem(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {        
        try
        {
            // Load return page
            String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

            String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
            String adminAction = (String) request.getParameter("adminAction");
            if(adminAction != null && adminAction.length() > 0)
            {
                mainExitURL = mainExitURL + adminAction;
            }
            else
            {
                mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
            }

            response.sendRedirect(mainExitURL + securityParams);
        }
        catch(Exception e)
        {
            logger.error(e);
        }
    }

    /**
    * For every record that resulted in the resultList,
    * appends the responseDocument with result elements.
    * 
    * @param resultList ArrayList
    * @param responseDocument Document
    * 
    * @exception IOException
    */
    private void appendResultList(ArrayList resultList, Document responseDocument) throws Exception
    {
        SearchResultVO searchResult = null;

        Element resultElement = null;

        Element flornumElement = null;
        Element nameElement = null;
        Element zipElement = null;
        Element stateElement = null;
        Element phoneElement = null;
        Element sundayElement = null;
        Element mercuryElement = null;
        Element statusElement = null;
        Element goElement = null;
		Element finalWeightElement = null;
        Element adjustedWeightElement = null;
		Element cityElement = null;
		
        Element resultListElement = responseDocument.createElement("search_result_list");

        if(resultList != null && resultList.size() > 0)
        {
            for(int i = 0; i < resultList.size(); i++)
            {
                searchResult = (SearchResultVO) resultList.get(i);

                resultElement = responseDocument.createElement("search_result");
           
                flornumElement = responseDocument.createElement("flornum");
                if(searchResult.getFlornum()!= null)
                {
                    flornumElement.appendChild(responseDocument.createTextNode(searchResult.getFlornum()));
                    resultElement.appendChild(flornumElement);
                }

                nameElement = responseDocument.createElement("name");
                if(searchResult.getName() != null)
                {
                    nameElement.appendChild(responseDocument.createTextNode(searchResult.getName()));
                    resultElement.appendChild(nameElement);
                }

                zipElement = responseDocument.createElement("zip");
                if(searchResult.getZip() != null)
                {
                    zipElement.appendChild(responseDocument.createTextNode(searchResult.getZip()));
                    resultElement.appendChild(zipElement);
                }

                stateElement = responseDocument.createElement("state");
                if(searchResult.getState() != null)
                {
                    stateElement.appendChild(responseDocument.createTextNode(searchResult.getState()));
                    resultElement.appendChild(stateElement);
                }

                phoneElement = responseDocument.createElement("phone");
                if(searchResult.getPhone() != null)
                {
                    phoneElement.appendChild(responseDocument.createTextNode(searchResult.getPhone()));
                    resultElement.appendChild(phoneElement);
                }

                sundayElement = responseDocument.createElement("sunday");
                if(searchResult.getSunday() != null)
                {
                    sundayElement.appendChild(responseDocument.createTextNode(searchResult.getSunday()));
                }
				else
				{
					sundayElement.appendChild(responseDocument.createTextNode("N"));
				}
				resultElement.appendChild(sundayElement);
				
                mercuryElement = responseDocument.createElement("mercury");
                if(searchResult.getMercury() != null)
				{
                    mercuryElement.appendChild(responseDocument.createTextNode(searchResult.getMercury()));
				}
                else
				{
                    mercuryElement.appendChild(responseDocument.createTextNode("N"));
				}
                resultElement.appendChild(mercuryElement); 
    
                statusElement = responseDocument.createElement("status");
                if(searchResult.getStatus() != null)
                {
                    statusElement.appendChild(responseDocument.createTextNode(searchResult.getStatus()));
                    resultElement.appendChild(statusElement);
                }

                goElement = responseDocument.createElement("go");
                if(searchResult.getGo() != null)
                {
                    goElement.appendChild(responseDocument.createTextNode(searchResult.getGo()));
                    resultElement.appendChild(goElement);
                }
				
				finalWeightElement = responseDocument.createElement("finalWeight");
				if(searchResult.getFinalWeight() != null)
                {
                    finalWeightElement.appendChild(responseDocument.createTextNode(searchResult.getFinalWeight().toString()));
                    resultElement.appendChild(finalWeightElement);
                }
				
				adjustedWeightElement = responseDocument.createElement("adjustedWeight");
				if(searchResult.getAdjustedWeight() != null)
                {
                    adjustedWeightElement.appendChild(responseDocument.createTextNode(searchResult.getAdjustedWeight().toString()));
                    resultElement.appendChild(adjustedWeightElement);
                }
				
				cityElement = responseDocument.createElement("city");
				if(searchResult.getCity() != null)
                {
                    cityElement.appendChild(responseDocument.createTextNode(searchResult.getCity().toString()));
                    resultElement.appendChild(cityElement);
                }

				// add the element as a record in the resultListElement
                resultListElement.appendChild(resultElement);
            }
        }
		// appends the responseDocument with the resultListElement
        responseDocument.getFirstChild().appendChild(resultListElement);
    }


    private HashMap loadSecurityPermissions(HttpServletRequest request) throws Exception
    {
        HashMap securityMap = new HashMap();
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            String securityToken = request.getParameter("securitytoken");
            String context = request.getParameter("context");
        
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.INACTIVE_SEARCH, SecurityConstants.VIEW))
            {
                securityMap.put("inactiveSearchView", "Y");
            }
            else
            {
                securityMap.put("inactiveSearchView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.ADD_NEW, SecurityConstants.YES))
            {
                securityMap.put("addNewView", "Y");
            }
            else
            {
                securityMap.put("addNewView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.VENDOR, SecurityConstants.VIEW))
            {
                securityMap.put("vendorView", "Y");
            }
            else
            {
                securityMap.put("vendorView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.SEARCH_VENDOR, SecurityConstants.VIEW))
            {
                securityMap.put("searchVendorView", "Y");
            }
            else
            {
                securityMap.put("searchVendorView", "N");
            }
        }
        else
        {
            securityMap.put("inactiveSearchView", "Y");
            securityMap.put("addNewView", "Y");
            securityMap.put("vendorView", "N");
        }
        
        return securityMap;
    }
}
