package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.dao.ViewQueueDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.FloristUpdater;
import com.ftd.applications.lmd.util.MaintenanceXAO;
import com.ftd.applications.lmd.util.SearchManager;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.util.LockHelper;
import com.ftd.applications.lmd.util.ViewQueueUpdater;
import com.ftd.applications.lmd.util.ViewQueueXAO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.applications.lmd.vo.SearchResultVO;
import com.ftd.applications.lmd.vo.ViewQueueVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * 
 *
 * @author 
 */

public class ZipCodeServlet extends HttpServlet 
{    

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.ZipCodeServlet";
    private final static String XSL_FLORIST_MAINTENANCE = "/xsl/floristMaintenance.xsl";
    private final static String XSL_BLOCK_ZIPCODE = "/xsl/blockZipCode.xsl";
    private final static String XSL_ZIP_LIST = "/xsl/zipCodePopup.xsl";
    private final static String XSL_MULTI_POPUP = "/xsl/zipCodeMulitEdit.xsl";
    private final static String XSL_VIEW_QUEUE = "/xsl/viewQueue.xsl";
    private final static String XSL_BLOCK_CITY = "/xsl/blockCity.xsl";
    
    private static final String ZIP_ASSIGNMENT_TYPE_OVERRIDE = "S";
    private static final String ZIP_ASSIGNMENT_TYPE_TEMPORARY_OVERRIDE = "T";
    private static final String ZIP_ASSIGNED = "assigned";
    private static final String ZIP_ASSIGNED_UNAVAIL = "assignedUnavail";
  
	
    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost Method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.
     * 
     * 1. loadSearch: This action is called to load the main Florist Search Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        
        try
        {
            if(action != null && action.equals("addZipCode")) 
            {
				if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
				{ 
                   this.addZipCode(request, response);
				}
            }
            else if(action != null && action.equals("updateZips"))
            {
				if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
				{ 
					this.updateZips(request, response);
				}
            }
            else if(action != null && action.equals("blockZipCode"))
            {
                this.blockZipCode(request, response);
            }
            else if(action != null && action.equals("blockCity"))
            {
                this.blockCity(request, response);
            }
            else if(action != null && action.equals("listzips"))
            {
                this.listzips(request, response);
            }
            else if(action != null && action.equals("displayMultiZip"))
            {
                this.displayMultiZip(request, response);
            }
            else if(action != null && action.equals("displayAllMultiZip"))
            {
                this.displayAllMultiZip(request, response);

            }
        } 
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 
    }
	
	
	
	private void blockZipCode(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        
        try
        {
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
			
			String zipData = null;
			for(int j=1; j<=150; j++) {
				if(request.getParameter("z"+j) != null) {
					if (zipData == null) {
						zipData = request.getParameter("z"+j);
				    } else {
				    	zipData = zipData + ", " + request.getParameter("z"+j);
				    }
				}
			}
            HashMap pageData = new HashMap();
            pageData.put("zipcodes", zipData);
 			DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
			
			File xslFile = new File(getServletContext().getRealPath(XSL_BLOCK_ZIPCODE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}
		catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
		
	}
    
	private void blockCity(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        
        try
        {
            Document responseDocument = DOMUtil.getDocument();
			
			String cityData = null;
			for(int j=1; j<=150; j++) {
				if(request.getParameter("z"+j) != null) {
					if (cityData == null) {
						cityData = request.getParameter("z"+j);
				    } else {
					    cityData = cityData + ", " + request.getParameter("z"+j);
				    }
				}
			}
            HashMap pageData = new HashMap();
            pageData.put("cities", cityData);
 			pageData.put("permitFulfillment", request.getParameter("permitFulfillment"));
 			DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
			
			File xslFile = new File(getServletContext().getRealPath(XSL_BLOCK_CITY));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}
		catch(Exception e)
        {
            logger.error(e);
        }
		
	}
    
    /*
     * Used for popup that displays a list of zipcodes.
     */
    private void displayMultiZip(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
      
        DataRequest dataRequest = null;
      
        try{
      
  			// Create the initial document
    		Document responseDocument = DOMUtil.getDocument();    
            dataRequest = DataRequestHelper.getInstance().getDataRequest();   
            BaseDataBuilder dataBuilder = new BaseDataBuilder();
            Element cutoffElement = dataBuilder.loadCutoffDataData(dataRequest,responseDocument);
            
            DOMUtil.addSection(responseDocument, cutoffElement.getChildNodes()); 
            
            File xslFile = new File(getServletContext().getRealPath(XSL_MULTI_POPUP));
            
            //get florist number from request
            String mNum = request.getParameter("mNum");            
           
            //add member number to xml
            HashMap pageData = new HashMap();
            pageData.put("mNum", mNum);           

            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, ServletHelper.getParameterMap(request));        
        
        }
        catch(Exception e)
        {
          logger.error(e);
        }
        finally
        {
            if(dataRequest.getConnection() != null)
            {
                try
                {
                    dataRequest.getConnection().close();
                }
                catch(Exception e) { }
            }
        }
      
    }    
    
    private void displayAllMultiZip(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
      
        DataRequest dataRequest = null;
      
        try{
      
  			// Create the initial document
    		Document responseDocument = DOMUtil.getDocument();    
            dataRequest = DataRequestHelper.getInstance().getDataRequest();   
            BaseDataBuilder dataBuilder = new BaseDataBuilder();
            Element cutoffElement = dataBuilder.loadCutoffDataData(dataRequest,responseDocument);
            
            DOMUtil.addSection(responseDocument, cutoffElement.getChildNodes()); 
            
            File xslFile = new File(getServletContext().getRealPath(XSL_MULTI_POPUP));
            
            //get florist number from request
            String mNum = request.getParameter("mNum");      
            
            //retreive all zip codes for florist
            dataRequest = DataRequestHelper.getInstance().getDataRequest();    
            MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
            List zipList = maintDao.getFloristZipCodes(mNum);

            //make a comma delimited string with all the zips
            Iterator iter = zipList.iterator();
            String zipString = "";
            PostalCodeVO zip = null;
            
            while(iter.hasNext())
            {
                zip = (PostalCodeVO)iter.next();
                
                //add comma if needed
               
                
                //append zip if active
                if( zip != null && zip.getPostalCode() != null)
                {
				
				//(zip.getBlockedFlag() == null || zip.getBlockedFlag().equals("N")))
					if(zipString.length() > 0)
					{
						zipString = zipString + ",";
					}
				
                    zipString = zipString + zip.getPostalCode();
                }
            }
            
            //add zipcode string to XML
            HashMap pageData = new HashMap();
            pageData.put("mNum", mNum); 
            pageData.put("zipcodes", zipString);
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, ServletHelper.getParameterMap(request));        
        
        }
        catch(Exception e)
        {
          logger.error(e);
        }
        finally
        {
            if(dataRequest.getConnection() != null)
            {
                try
                {
                    dataRequest.getConnection().close();
                }
                catch(Exception e) { }
            }
        }
      
    } 
    
    /*
     * Used for popup that displays a list of zipcodes.
     */
    private void listzips(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
      
        DataRequest dataRequest = null;
      
        try{
      
  			// Create the initial document
    		Document responseDocument = DOMUtil.getDocument();    
        
            File xslFile = new File(getServletContext().getRealPath(XSL_ZIP_LIST));
            
            //get florist number from request
            String floristId = request.getParameter("mNum");
            
            //retreive all zip codes for florist
            dataRequest = DataRequestHelper.getInstance().getDataRequest();    
            MaintenanceDAO maintDao =  new MaintenanceDAO(dataRequest.getConnection());
            List zipList = maintDao.getFloristZipCodes(floristId);

            //make a comma delimited string with all the zips
            Iterator iter = zipList.iterator();
            String zipString = "";
            PostalCodeVO zip = null;
            
            while(iter.hasNext())
            {
                zip = (PostalCodeVO)iter.next();
                
                //add comma if needed
                if(zipString.length() > 0)
                {
                    zipString = zipString + ",";
                }
                
                //append zip if active
                if( zip != null && 
                    zip.getPostalCode() != null && 
                    (zip.getBlockedFlag() == null ||
                    zip.getBlockedFlag().equals("N")))
                {
                    zipString = zipString + zip.getPostalCode();
                }
            }
            
            //add zipcode string to XML
            HashMap pageData = new HashMap();
            pageData.put("zipcodes", zipString);
            
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            TraxUtil.getInstance().transform(request, response,responseDocument, xslFile, ServletHelper.getParameterMap(request));        
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
          //close the connection
          if(dataRequest.getConnection() != null)
          {
            try
            {
              dataRequest.getConnection().close();
            }
            catch(Exception e) 
            {}
          }
        }
      
    }
  
    private void addZipCode(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        ServletHelper servletHelper = new ServletHelper();
        
        try
        {
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
         
            if(request.getParameter("memnum") != null && request.getParameter("memnum").length() > 0)
            {  
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
                FloristVO florist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                florist = floristUpdater.updateFloristFromRequest(florist, request);
                
                // Update lock for user
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestMaintenanceLock(florist, florist.getLastUpdateUser());
                
                HashMap invCodes = new HashMap();
                HashMap existCodes = new HashMap();
                HashMap ascCodes = new HashMap();
        
                String inZip= null;
                int count = 0;
                
                String izips = "";
                String ezips = "";
                String azips = "";
                
                int invalid = 0;
                int alreadyExists = 0;

                String newzipcodes = request.getParameter("newzipcodes");
                String newcity     = request.getParameter("newcity");
                boolean noCityFound = true;
                
                // Add new zip code to florist
                if((newzipcodes != null && newzipcodes.length() > 0) ||
                   (newcity != null && newcity.length() > 0))
                {
				
                    // check if covered by the associated florists....
                    HashMap testCodes = new HashMap();
                    
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    dataRequest.reset();
                    dataRequest.setStatementID("VIEW_COVERED_ZIPS");
                    dataRequest.addInputParam("IN_FLORIST_ID", request.getParameter("memnum"));
            
                    CachedResultSet azipResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                    if(azipResultSet != null && azipResultSet.getRowCount() > 0)
                    {            
                    
                            while(azipResultSet.next())
                            {
                                    testCodes.put((String) azipResultSet.getObject(1), "Y");
                            }
                    }
				
                    PostalCodeVO postalCode = null;
                    StringTokenizer st = null;
                    CachedResultSet zipCityRs = null;
                    
                    if (newcity != null && newcity.length() > 0) {
                        dataRequest.reset();
                        dataRequest.setStatementID("VIEW_CITY_ZIPS");
                        dataRequest.addInputParam("IN_CITY", request.getParameter("newcity"));
                        dataRequest.addInputParam("IN_STATE", request.getParameter("newstate"));
                        zipCityRs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                        if ((zipCityRs != null) && (zipCityRs.getRowCount() > 0)) {
                            inZip = "";  // Get into loop
                            noCityFound = false; 
                        }
                    } else {
                        st = new StringTokenizer(request.getParameter("newzipcodes"), ",");
                        if (st.hasMoreTokens()) {
                            inZip = "";  // Get into loop
                        }
                    }
outer:              while (inZip != null) 
                    {
                        if ((zipCityRs != null) && (zipCityRs.next())) {
                            inZip = (String) zipCityRs.getObject(1);
                        } else if ((st != null) && (st.hasMoreTokens())) {
                            inZip = st.nextToken();
                        } else {
                            inZip = null;
                            continue outer;
                        }

                        String zipRestrictionOverride = null;
                
                        // validate the US or the Canadian ZipCode...
                        // validate zip code...
                        count=0;
                        inZip = inZip.trim();
                        int len = inZip.trim().length();

                        if(len < 3 || len > 7 || len==4 )
                        {
                                count++;
                        }
                        else if(len == 5)
                        {
                                for(int d=0; d<5; d++)
                                {
                                        if(!Character.isDigit(inZip.charAt(d)))
                                                count++;
                                }
                        }

                        if(len == 3)
                        {
                                if(!Character.isLetter(inZip.charAt(0)))
                                        count++;
                                if(!Character.isDigit(inZip.charAt(1)))
                                        count++;
                                if(!Character.isLetter(inZip.charAt(2)))
                                        count++;
                        }

                        // Validate Canadian ZipCode. // strip the first 3
                        if(len > 5)
                        {

                                inZip = inZip.substring(0,3);
                        
                                if(!Character.isLetter(inZip.charAt(0)))
                                        count++;
                                if(!Character.isDigit(inZip.charAt(1)))
                                        count++;
                                if(!Character.isLetter(inZip.charAt(2)))
                                        count++;
                        }

                        if(count > 0) // if not valid...
                        {
                                //invalid++;
                                //invCodes.put("invalidZip"+invalid, inZip);
                                
                                if(izips.length() > 0)
                                        izips = izips + ", " + inZip;
                                else
                                        izips = izips + inZip;
                                
                                continue outer;

                        }
                        else 
                        {
                                // check if it is a valid zip in the zip database...
                                

                                // validate the zipcode...
                                dataRequest.reset();
                                dataRequest.setStatementID("GET_ZIP_DETAILS");
                                dataRequest.addInputParam("ZIP_CODE", inZip);
                                String notCodeExists = (String)dataAccessUtil.execute(dataRequest);
                                
                                if(!notCodeExists.equals("Y"))
                                {
                                //invalid++;
                                //invCodes.put("invalidZip"+invalid, inZip);
                                if(izips.length() > 0)
                                        izips = izips + ", " + inZip;
                                else
                                        izips = izips + inZip;
                                
                                continue outer;
                                }
                        }
                        
                        
                        // the zip is valid and exists in the database....                                                                                                
                        // check if it's already assigned to florist

                        if(florist.getPostalCodeMap() != null && florist.getPostalCodeMap().size() > 0)
                        {
                            PostalCodeVO pCode = null;
                            Iterator postalCodeIterator = florist.getPostalCodeMap().values().iterator();
                            
                            boolean zipInList = false;
                            while(postalCodeIterator.hasNext())
                            {
                                pCode = (PostalCodeVO) postalCodeIterator.next();
                                //postalCodesElement.appendChild(floristDocument.importNode(generatePostalCodeXML(postalCode), true));
                                if(inZip.equals(pCode.getPostalCode()))
                                {
                                    // it exists...add to the alreadyExists...
                                    //	alreadyExists++;
                                    //	existCodes.put("existCodes"+alreadyExists, inZip);
                                    //	continue outer;
                                    zipInList = true;    
                                    String assignedStatus = pCode.getAssignedStatus();
                                    if (ZIP_ASSIGNED.equals(assignedStatus) ||
                                        ZIP_ASSIGNED_UNAVAIL.equals(assignedStatus))
                                    {
                                        if(ezips.length() > 0)
                                                ezips = ezips + ", " + inZip;
                                        else
                                                ezips = ezips + inZip;
                                        continue outer;
                                    }
                                }
                            }
                            if (zipInList == false) {
                                // Zip is nowhere in list, so we are overriding any zip restrictions
                                if(ZIP_ASSIGNMENT_TYPE_TEMPORARY_OVERRIDE.equalsIgnoreCase(request.getParameter("assignmentType"))) {
                                    zipRestrictionOverride = ZIP_ASSIGNMENT_TYPE_TEMPORARY_OVERRIDE;  // Temporary override
                                } else {
                                    zipRestrictionOverride = ZIP_ASSIGNMENT_TYPE_OVERRIDE;            // Default is static override
                                }
                            }
                        }

                        // check if in the associated zip list...
                        if(testCodes.get(inZip) != null)
                        {
                                // it is covered by an associated florist...
                                
                                        if(azips.length() > 0)
                                                azips = azips + ", " + inZip;
                                        else
                                                azips = azips + inZip;
                                        
                                        continue outer;
                        }


                        // a valid zip, add the zip code 
                        postalCode = new PostalCodeVO();
                        postalCode.setPostalCode(inZip);
                        postalCode.setCutoffTime(request.getParameter("newcutofftime"));
                        postalCode.setBlockedFlag("N");
                        postalCode.setSavedFlag("N");
                        postalCode.setAssignedStatus("assigned");
                        postalCode.setAssignmentType(zipRestrictionOverride);
                        logger.debug("New zip requested to be added: " + inZip + 
                                     " assignmentType: " + zipRestrictionOverride);
                        dataRequest.reset();
                        dataRequest.setStatementID("VIEW_ZIP_CITY_FLORIST_COUNT");
                        dataRequest.addInputParam("ZIP", postalCode.getPostalCode());
                        
						
                        CachedResultSet countResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                        if(countResultSet != null && countResultSet.getRowCount() > 0)
                        {            
                            while(countResultSet.next())
                            {
                                postalCode.setFloristCount(new Integer(((BigDecimal) countResultSet.getObject(1)).intValue()));
                                postalCode.setCity((String) countResultSet.getObject(2));
                            }
                        }
                        
                        florist.getPostalCodeMap().put(postalCode.getPostalCode(), postalCode);
						
                    } // end of the while loop
                } //end of if
                
                
                MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes()); 
				
                BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));

                if (noCityFound) {
                    HashMap pageData = new HashMap();
                    pageData.put("noCityFound", "true");
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                }
			
                invCodes.put("invalZips", izips);
                existCodes.put("existZips", ezips);
                ascCodes.put("asscZips", azips);

                DOMUtil.addSection(responseDocument, "invalidZips", "izips", invCodes, true);
                DOMUtil.addSection(responseDocument, "existingZips", "ezips", existCodes, true);
                DOMUtil.addSection(responseDocument, "associatedZips", "azips", ascCodes, true);
                
                String isN;
                if(request.getParameter("isNew") != null && (request.getParameter("isNew") == "Y"))
                {
                    HashMap flowMap = new HashMap();
                    flowMap.put("processType", "AddNew");
                    DOMUtil.addSection(responseDocument, "processFlow", "flow", flowMap, true);
                }
				
                File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
			}// end of if
				
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
    private void updateZips(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        
		HashMap invCodes = new HashMap();
		HashMap nsCodes = new HashMap();
		String izips = "";
		String inZip= "";
		String nszips = "";
		int invalid = 0;
		
        try
        {
            if(request.getParameter("pageLocation") != null && request.getParameter("pageLocation").equals("viewQueue"))
            {
                Document responseDocument = DOMUtil.getDocument();
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
             
                if(request.getParameter("mercuryId") != null && request.getParameter("mercuryId").length() > 0)
                {
                    String userId = this.pullCurrentUser(request.getParameter("securitytoken"));
                    
                    ViewQueueDAO viewQueueDAO = new ViewQueueDAO(dataRequest.getConnection());
                    ViewQueueVO viewQueue = viewQueueDAO.getViewQueue(request.getParameter("mercuryId"));
                    
                    ViewQueueUpdater viewQueueUpdater = new ViewQueueUpdater(dataRequest);
                    viewQueue = viewQueueUpdater.updateFloristFromRequest(viewQueue, request);
                    
                    // Update lock for user
                    LockHelper lockHelper = new LockHelper(dataRequest);
                    lockHelper.requestViewQueueLock(viewQueue, userId);
                    
                    // Add new zip code to florist
                    if(request.getParameter("editZipCodes") != null && request.getParameter("editZipCodes").length() > 0)
                    {
                        PostalCodeVO postalCode = null;
                        
                        StringTokenizer st = new StringTokenizer(request.getParameter("editZipCodes"), ",");
                        while (st.hasMoreTokens()) 
                        {
							inZip = st.nextToken();
							inZip = inZip.trim();
                            postalCode = (PostalCodeVO) viewQueue.getPostalCodeMap().get(inZip);
                        
                            if(postalCode != null)
                            {
									if(postalCode.getSavedFlag() != null && postalCode.getSavedFlag().equals("N"))
									{
										 // Invalid postal code entered
										if(izips.length() > 0)
											izips = izips + ", " + inZip;
										else
											izips = izips + inZip;
									}
									else
									{
									
										if(request.getParameter("editCutoffFlag") != null && request.getParameter("editCutoffFlag").equals("Y"))
										{
											postalCode.setCutoffTime(request.getParameter("editCutoff"));
										}
                                
										if(request.getParameter("editBlocksFlag") != null && request.getParameter("editBlocksFlag").equals("Y"))
										{
											if(request.getParameter("editZipBlockFlag") != null && request.getParameter("editZipBlockFlag").equals("Block"))
											{
												if(request.getParameter("editZipBlockDate") != null && request.getParameter("editZipBlockDate").length() > 0)
												{
													postalCode.setBlockedFlag("Y");
													postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("editZipBlockDate")));
												}
												else
												{
													postalCode.setDeleteFlag("Y");
													postalCode.setBlockEndDate(null);
												}
												
											}
											else
											{
												postalCode.setBlockedFlag("N");
												postalCode.setBlockEndDate(null);
											}
										}
										else if(request.getParameter("editCutoffFlag") != null && request.getParameter("editCutoffFlag").equals("N"))
										{
											postalCode.setDeleteFlag("Y");
											postalCode.setBlockEndDate(null);
										}
									}
							}
                            else
                            {
                                // Invalid postal code entered
								
								if(izips.length() > 0)
									izips = izips + ", " + inZip;
								else
									izips = izips + inZip;
                            }
							
                        }
                    }
                    
                    ViewQueueXAO viewQueueXAO = new ViewQueueXAO();
                    DOMUtil.addSection(responseDocument, ((Document) viewQueueXAO.generateXML(viewQueue)).getChildNodes()); 
                  
                    BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                    DOMUtil.addSection(responseDocument, baseDataBuilder.loadViewQueueGenericData(dataRequest, request));

					invCodes.put("invalPopZips", izips);
					DOMUtil.addSection(responseDocument, "invalidPopZips", "iPopzips", invCodes, true);

					
                    File xslFile = new File(getServletContext().getRealPath(XSL_VIEW_QUEUE));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
                }
            }
            else
            {
                ServletHelper servletHelper = new ServletHelper();
                Document responseDocument = DOMUtil.getDocument();
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
             
                if(request.getParameter("mNum") != null && request.getParameter("mNum").length() > 0)
                {
                    MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
                    FloristVO florist = maintenanceDAO.load(request.getParameter("mNum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                    
                    FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                    florist = floristUpdater.updateFloristFromRequest(florist, request);
                    
                    // Update lock for user
                    LockHelper lockHelper = new LockHelper(dataRequest);
                    lockHelper.requestMaintenanceLock(florist, florist.getLastUpdateUser());
                    
                    // Add new zip code to florist
                    if(request.getParameter("editZipCodes") != null && request.getParameter("editZipCodes").length() > 0)
                    {
                        PostalCodeVO postalCode = null;
                        
                        StringTokenizer st = new StringTokenizer(request.getParameter("editZipCodes"), ",");
                        while (st.hasMoreTokens()) 
                        {
							inZip = st.nextToken();
							inZip = inZip.trim();
                            postalCode = (PostalCodeVO) florist.getPostalCodeMap().get(inZip);
							
                            if(postalCode != null)
                            {
							
								if(postalCode.getSavedFlag() != null && postalCode.getSavedFlag().equals("N"))
								{
									 // non-saved ones
									if(izips.length() > 0)
										nszips = nszips + ", " + inZip;
									else
										nszips = nszips + inZip;
								}
								else
								{
									if(request.getParameter("editCutoffFlag") != null && request.getParameter("editCutoffFlag").equals("Y"))
									{
										postalCode.setCutoffTime(request.getParameter("editCutoff"));
									}
									
									if(request.getParameter("editBlocksFlag") != null && request.getParameter("editBlocksFlag").equals("Y"))
									{
										if(request.getParameter("editZipBlockFlag") != null && request.getParameter("editZipBlockFlag").equals("Block"))
										{
											if(request.getParameter("editZipBlockDate") != null && request.getParameter("editZipBlockDate").length() > 0)
											{
												postalCode.setBlockedFlag("Y");
												postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("editZipBlockDate")));
											}
											else
											{
												postalCode.setDeleteFlag("Y");
												postalCode.setBlockEndDate(null);
											}
											
										}
										else
										{
											postalCode.setBlockedFlag("N");
											postalCode.setBlockEndDate(null);
										}
										
									}
								}
                            }
                            else
                            {
                                // Invalid postal code entered
								if(izips.length() > 0)
									izips = izips + ", " + inZip;
								else
									izips = izips + inZip;
                            }
                        }
                    }
                    
                    MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                    DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes()); 
                  
                    BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                    DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));

					invCodes.put("invalPopZips", izips);
					DOMUtil.addSection(responseDocument, "invalidPopZips", "iPopzips", invCodes, true);
					
					nsCodes.put("nonSavedZips", nszips);
					DOMUtil.addSection(responseDocument, "invalidSavedZips", "nonsavedzips", nsCodes, true);
					
                    File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
                }
            }
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    
    }
    
    private String pullCurrentUser(String securityToken) throws Exception
    {
        String userId = null;
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            userId = securityManager.getUserInfo(securityToken).getUserID();
        }
        else
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
        
        return userId;
    }

}
