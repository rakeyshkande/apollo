package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.FloristUpdater;
import com.ftd.applications.lmd.util.MaintenanceXAO;
import com.ftd.applications.lmd.util.SearchManager;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.OrderResultVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.applications.lmd.vo.SearchResultVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 *
 *
 * @author
 */

public class OrderSearchServlet extends HttpServlet
{

    private Logger logger;
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.OrderSearchServlet";
    private final static String XSL_FLORIST_MAINTENANCE = "/xsl/floristMaintenance.xsl";
    private final static String XSL_BLOCK_ZIPCODE = "/xsl/blockZipCode.xsl";
    private final static String XSL_ZIP_LIST = "/xsl/zipCodePopup.xsl";
    private final static String XSL_MULTI_POPUP = "/xsl/zipCodeMulitEdit.xsl";


    /**
     * Initializes servlet.
     *
     * @param config ServletConfig
     *
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost Method.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     *
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.
     *
     * 1. loadSearch: This action is called to load the main Florist Search Page.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     *
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        String action = request.getParameter("action");
		try
		{
			if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
			{ 
				if(action != null && action.equals("searchOrders"))
				{
				   this.searchOrders(request, response);
				}
			}
		}
	   catch (Exception e) 
		{
			logger.error(e);
			throw new ServletException(e);
		} 
		
	}

/**
* For every record that resulted in the resultList for the order search,
* appends the responseDocument with result elements.
* 
* @param resultList ArrayList
* @param responseDocument Document
* 
* @exception IOException
*/
    private void appendResultList(ArrayList resultList, Document responseDocument) throws Exception
    {
        OrderResultVO orderResult = null;

        Element resultElement = null;

        Element MercNumElement = null;
        Element OrderNumElement = null;
        Element OrderDateElement = null;
        Element DeliveryDateElement = null;
        Element LastNameElement = null;
        Element OrderStatusElement = null;
        Element MercValueElement = null;
        Element SkuElement = null;
		
        Element resultListElement = responseDocument.createElement("order_result_list");

        if(resultList != null && resultList.size() > 0)
        {
            for(int i = 0; i < resultList.size(); i++)
            {
                orderResult = (OrderResultVO) resultList.get(i);

                resultElement = responseDocument.createElement("order_result");
           
                MercNumElement = responseDocument.createElement("mercuryNumber");
                if(orderResult.getMercNumber() != null)
                {
                    MercNumElement.appendChild(responseDocument.createTextNode(orderResult.getMercNumber()));
                    resultElement.appendChild(MercNumElement);
                }

                OrderNumElement = responseDocument.createElement("orderNumber");
				if(orderResult.getOrderNumber() != null)
                {
                    OrderNumElement.appendChild(responseDocument.createTextNode(orderResult.getOrderNumber()));
                    resultElement.appendChild(OrderNumElement);
                }
				
				OrderDateElement = responseDocument.createElement("orderDate");
				if(orderResult.getOrderDate() != null)
                {
                    OrderDateElement.appendChild(responseDocument.createTextNode(orderResult.getOrderDate()));
                    resultElement.appendChild(OrderDateElement);
                }
				
				DeliveryDateElement = responseDocument.createElement("deliveryDate");
				if(orderResult.getDeliveryDate() != null)
                {
                    DeliveryDateElement.appendChild(responseDocument.createTextNode(orderResult.getDeliveryDate()));
                    resultElement.appendChild(DeliveryDateElement);
                }
				
				LastNameElement = responseDocument.createElement("lastName");
				if(orderResult.getLastName() != null)
                {
                    LastNameElement.appendChild(responseDocument.createTextNode(orderResult.getLastName()));
                    resultElement.appendChild(LastNameElement);
                }
				
				OrderStatusElement = responseDocument.createElement("orderStatus");
				if(orderResult.getOrderStatus() != null)
                {
                    OrderStatusElement.appendChild(responseDocument.createTextNode(orderResult.getOrderStatus()));
                    resultElement.appendChild(OrderStatusElement);
                }
				
				MercValueElement = responseDocument.createElement("mercValue");
				if(orderResult.getMercValue() != null)
                {
                    MercValueElement.appendChild(responseDocument.createTextNode(orderResult.getMercValue()));
                    resultElement.appendChild(MercValueElement);
                }
				
				SkuElement = responseDocument.createElement("orderSku");
				if(orderResult.getSku() != null)
                {
                    SkuElement.appendChild(responseDocument.createTextNode(orderResult.getSku()));
                    resultElement.appendChild(SkuElement);
                }
				
				// add the element as a record in the resultListElement
                resultListElement.appendChild(resultElement);
            }
        }
		// appends the responseDocument with the resultListElement
        responseDocument.getFirstChild().appendChild(resultListElement);
    }


	private void searchOrders(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

	    DataRequest dataRequest = null;

        try
        {
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			HashMap flowMap = new HashMap();
            flowMap.put("loadOrdersSearch", "Yes");
            DOMUtil.addSection(responseDocument, "orderFlow", "flow", flowMap, true);
	
	
			SearchManager searchManager = new SearchManager();
			ArrayList resultList = searchManager.retrieveOrderMap(request, dataRequest);
			
			if(resultList != null && resultList.size() > 0)
			{
            this.appendResultList(resultList, responseDocument);
			}
			
			MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
			FloristVO florist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));

			FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
			florist = floristUpdater.updateFloristFromRequest(florist, request);

			MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
			DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes());

			BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));

			// append the search results...
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}
		catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se)
            {
                logger.error(se);
            }
        }

	}
}

	
