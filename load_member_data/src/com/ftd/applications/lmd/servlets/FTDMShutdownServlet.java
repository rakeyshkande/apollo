/*
 * @(#) FTDMShutdownServlet.java      1.1.2.3     2004/09/30
 * 
 * 
 */

package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.bo.ManualMercuryFloristBusinessDelegate;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.exception.DataAccessException;
import com.ftd.applications.lmd.exception.ManualMercuryFloristException;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.ManualMercuryFloristVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/09/13  Initial Release.(RFL)
 * 1.1.2.2  2004/09/21  Updated functionality.(RFL)
 * 1.1.2.3  2004/09/30  Corrected for immediate restarts.(RFL)
 * 
 * 
 */
public class FTDMShutdownServlet extends HttpServlet  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static final String CONTENT_TYPE = 
                        "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.servlets.FTDMShutdownServlet";
    private final static String XSL_FTDM_SHUTDOWN = "/xsl/FTDMShutdown.xsl";
    private static final String DATE_FORMAT = "MM/dd/yy";
    private static final String DATE_TIME_FORMAT = "MM/dd/yyyy '-' hh:mm a";
    private static final String DATE_DELIMITER = "/";
    private static final String ACTION_SHUTDOWN = "shutdown";
    private static final String ACTION_RESTART = "restart";
    
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private Logger logger;

    

    public FTDMShutdownServlet() 
    {
        
    }//end method FTDMShutdownServlet()
    
    
    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
        
    }//end method


    /**
     * 
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, 
                         HttpServletResponse response) throws ServletException,
                                                              IOException
    {
        try
        {
            response.setContentType(CONTENT_TYPE);
            
            String context = request.getParameter("context");
            String securityToken = request.getParameter("securitytoken");

            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            {
                this.createResponse(request, response, null, null);
            }

        }//end try
        catch (Throwable t) 
        {
            try
            {
                request.getRequestDispatcher(
                        ConfigurationUtil.getInstance().getProperty(
                                ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.ERROR_PAGE_LOCATION)).
                                                    forward(request, response);
                
            }//end try
            catch (Exception e) 
            {
                logger.error("Exception thrown accessing error page.", e);
                throw new ServletException("Unexpected server exception.");
                
            }//end catch
            finally 
            {
                logger.error("Error thrown in doGet().", t);
                
            }//end finally
        
        }//end catch 

    }//end method doGet()
    
    
    /**
     * 
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, 
                          HttpServletResponse response) throws ServletException,
                                                              IOException
    {
        try
        {
            response.setContentType(CONTENT_TYPE);
            
            String action = request.getParameter("FTDM_Action");
            String context = request.getParameter("context");
            String securityToken = request.getParameter("securitytoken");
    
            if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
            {
                if(action != null && action.equals(ACTION_SHUTDOWN)) 
                {
                    try
                    {
                        this.saveShutdown(request, response);
                    }//end try
                    catch (Exception e) 
                    {
                        logger.error("Exception thrown while saving shutdown.", e);
                        throw new ServletException("Shutdown was unsuccessful.");
                    }//end catch
        
                    
                }//end if
                else if(action != null && action.equals(ACTION_RESTART)) 
                {
                    try
                    {
                        this.saveRestart(request, response);
                    }//end try
                    catch (Exception e) 
                    {
                        logger.error("Exception thrown while saving restart.", e);
                        throw new ServletException("Restart was unsuccessful.");
                    }//end catch
                    
                }//end else if
                else if(action != null && action.equals("exitFTDM")) 
                {
                    // Redirect to menu
                    this.redirectToMenu(request, response);
                }
                else 
                {
                    logger.error("Invalid action type(" + action + ")."); 
                    throw new ServletException("Invalid action type.");
                    
                }//end else
            }
            
        }//end try
        catch (Throwable t) 
        {
            try
            {
                request.getRequestDispatcher(
                        ConfigurationUtil.getInstance().getProperty(
                                ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, 
                                ConfigurationConstants.ERROR_PAGE_LOCATION)).
                                                    forward(request, response);
                
            }//end try
            catch (Exception e) 
            {
                logger.error("Exception thrown accessing error page.", e);
                throw new ServletException("Unexpected server exception.");
                
            }//end catch
            finally 
            {
                logger.error("Error thrown in doGet().", t);
                
            }//end finally
        
        }//end catch 
        
    }//end method doPost()
    

    /*
     * 
     */
    private Document createBaseDocument(ManualMercuryFloristVO status)
                                            throws ParserConfigurationException
    {
        Document document = null;
        
        try 
        {
            document = DOMUtil.getDefaultDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Exception thrown while getting default DOM document");
            throw pce;
            
        }//end catch
        
        //Create display data root
        Element viewElement = document.createElement("status_table");
        document.appendChild(viewElement);
        
        //Create status node
        Element statusElement = document.createElement("status");
        if (status.getCurrentStatus() != null)
        {
            statusElement.appendChild(
                            document.createTextNode(status.getCurrentStatus()));
        }//end if
        viewElement.appendChild(statusElement);
        
        //create Shutdown date node
        Element shutdownElement = document.createElement("shutdownDate");
        if (status.getCurrentShutdownDate() != null) 
        {
            shutdownElement.appendChild(document.createTextNode(
                                this.formatDate(status.getCurrentShutdownDate(),
                                                this.DATE_FORMAT)));
            
        }//end if
        viewElement.appendChild(shutdownElement);
        
        //create restart date node
        Element restartElement = document.createElement("restartDate");
        if (status.getCurrentRestartDate() != null) 
        {
            restartElement.appendChild(document.createTextNode(
                                this.formatDate(status.getCurrentRestartDate(),
                                                this.DATE_TIME_FORMAT)));
            
        }//end if
        viewElement.appendChild(restartElement);
        
        return document;
        
    }//end method createBaseDocument()
    
    
    /*
     * 
     */
    private Document createDataEntryDocument(HttpServletRequest request)
                                            throws ParserConfigurationException
    {
        Document document = null;
        
        try 
        {
            document = DOMUtil.getDefaultDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Exception thrown while getting default DOM document");
            throw pce;
            
        }//end catch
        
        //Create screen field root
        Element fieldsElement = document.createElement("data_entry_fields");
        document.appendChild(fieldsElement);
        
        //Create  node
        Element actionElement = document.createElement("parmAction");
        
        String action = request.getParameter("FTDM_Action");
        if(action != null) 
        {
            actionElement.appendChild(document.createTextNode(action));
            
        }//end if
        fieldsElement.appendChild(actionElement);
        
        //create restart date
        Element restartDateElement = document.createElement("restartDate");
        
        String restartDate = request.getParameter("restart_date");
        if (restartDate != null) 
        {
            restartDateElement.appendChild(document.createTextNode(
                                                                restartDate));
            
        }//end if
        fieldsElement.appendChild(restartDateElement);
        
        return document;
        
    }//end method createScreenDocument
    
    
    /*
     * 
     */
    private void createResponse(HttpServletRequest request, 
                                HttpServletResponse response,
                                Document screenDataDocument,
                                Document validationDocument) throws Exception
    {
        
        Document responseDocument = null;
        
        // Create the initial document
        try 
        {
            responseDocument = DOMUtil.getDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Unable to get response document.", pce);
            throw pce;
        }//end catch
        
        //Base data
        Document viewDocument = 
                this.createBaseDocument(this.getManualMercuryFloristStatus());
        DOMUtil.addSection(responseDocument, viewDocument.getChildNodes());
        
        //Screen data
        if (screenDataDocument != null) 
        {
            DOMUtil.addSection(responseDocument, 
                               screenDataDocument.getChildNodes());
            
        }//end if
        
        //Screen validation 
        if (validationDocument != null) 
        {
            DOMUtil.addSection(responseDocument, 
                               validationDocument.getChildNodes());
            
        }//end if

        //Get xsl file for page
        File xslFile = 
            new File(getServletContext().getRealPath(XSL_FTDM_SHUTDOWN));
        
        try
        {
            TraxUtil.getInstance().transform(request, 
                                             response, 
                                             responseDocument, 
                                             xslFile, 
                                             ServletHelper.getParameterMap(request));
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to transform response document.", e);
            throw e;
        }//end catch (Exception)
        
    }//end method createResponse
    
     private Document createValidationDocument() 
                                             throws ParserConfigurationException{
        return this.createValidationDocument("Invalid date format");                                      
    }

    /*
     * 
     */
    private Document createValidationDocument(String error) 
                                            throws ParserConfigurationException
    {
        Document document = null;
        
        try 
        {
            document = DOMUtil.getDefaultDocument();
        }//end try
        catch (ParserConfigurationException pce) 
        {
            logger.error("Exception thrown while getting default DOM document");
            throw pce;
            
        }//end catch
        
        //Create screen field root
        Element validationElement = document.createElement("validation");
        document.appendChild(validationElement);
        
        //Create  node
        Element restartDateElement = document.createElement("restartDate");
        restartDateElement.appendChild(document.createTextNode(error));
        validationElement.appendChild(restartDateElement);
        
        return document;
        
    }//end method createValidationDocument()
    
    
    /*
     * 
     */
    private String formatDate(Date date, String dateFormat)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        
        return formatter.format(date);
        
    }//end method
    
    
    /*
     * 
     */
    private ManualMercuryFloristVO getManualMercuryFloristStatus()
                                            throws DataAccessException
    {
        return new ManualMercuryFloristBusinessDelegate().getCurrentStatus();
        
    }//end method getManualMercuryFloristStatus()
    
    
    /*
     * Remember, Calendar.getTime() sets the date/time on the calendar.
     */
    private Date parseDate(String dateParameter) throws Exception
    {
        StringBuffer date = new StringBuffer();
        SimpleDateFormat formatter = new SimpleDateFormat(this.DATE_FORMAT);

        StringTokenizer st = new StringTokenizer(dateParameter, 
                                                 this.DATE_DELIMITER);
        int count = st.countTokens();
        
        GregorianCalendar cal = new GregorianCalendar();
        cal.getTime();
        String currentYear = String.valueOf(cal.get(Calendar.YEAR));
        
        if ((count < 2) || (count > 3)) 
        {
            throw new Exception("Invalid date format.");
        }//end if
        
        //Month
        date.append(st.nextToken());
        date.append(this.DATE_DELIMITER);
        //date
        date.append(st.nextToken());
        date.append(this.DATE_DELIMITER);
        //year
        if (st.hasMoreTokens()) 
        {
            date.append(st.nextToken());
            
        }//end if
        else 
        {
            date.append(currentYear);
        }//end if
        
        
        //parse the date
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(formatter.parse(date.toString()));
        cal2.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
        cal2.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
        cal2.set(Calendar.SECOND, cal.get(Calendar.SECOND));
        cal2.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));
        
        GregorianCalendar cal3 = new GregorianCalendar();
        cal3.add(Calendar.DATE, 30);
        if (cal2.after(cal3)){
            throw new Exception("Restart date cannot be more than 30 days after today.");
        }
        
        return cal2.getTime();
        
    }//end method parseDate
    
    
    /*
     * 
     */
    private void saveRestart(HttpServletRequest request, 
                             HttpServletResponse response) throws Exception
    {
        Document screenDocument = null;
        Document validationDocument = null;
        Date restartDate = null;
        
        String restartDateParameter = request.getParameter("restart_date");

        if(restartDateParameter == null)
        {
            //send missing date validation to screen
            screenDocument = this.createDataEntryDocument(request);
            validationDocument = this.createValidationDocument();
            
        }//end if
        else
        {
            try
            {
                restartDate = this.parseDate(restartDateParameter);
            }//end 
            catch (Exception e) 
            {
                screenDocument = this.createDataEntryDocument(request);
                validationDocument = this.createValidationDocument(e.getMessage());
            }
            
            if (restartDate != null) 
            {
                ServletHelper servletHelper = new ServletHelper();
                String userId = servletHelper.pullCurrentUser(request.getParameter("securitytoken"));
        
                new ManualMercuryFloristBusinessDelegate().restart(restartDate, userId);
            }
                    
        }//end else
        
        try {
            this.createResponse(request, 
                                response, 
                                screenDocument, 
                                validationDocument);
        }//end try
        catch (Exception e) 
        {
            throw e;
        }//end catch


    }//end method saveRestart()
    
    
    /*
     * 
     */
    private void saveShutdown(HttpServletRequest request, 
                              HttpServletResponse response) throws Exception
    {
        Document screenDocument = null;
        Document validationDocument = null;
        Date restartDate = null;
        
        String restartDateParameter = request.getParameter("restart_date");

        if(restartDateParameter == null)
        {
            //send missing date validation to screen
            screenDocument = this.createDataEntryDocument(request);
            validationDocument = this.createValidationDocument();
            
        }//end if
        else
        {
            try
            {
                restartDate = this.parseDate(restartDateParameter);
            }//end 
            catch (Exception e) 
            {
                screenDocument = this.createDataEntryDocument(request);
                validationDocument = this.createValidationDocument(e.getMessage());
            }
            
            if (restartDate != null) 
            {
                ServletHelper servletHelper = new ServletHelper();
                String userId = servletHelper.pullCurrentUser(request.getParameter("securitytoken"));
                new ManualMercuryFloristBusinessDelegate().shutdown(restartDate, userId);
            }
                    
        }//end else
        
        try {
            this.createResponse(request, 
                                response, 
                                screenDocument, 
                                validationDocument);
        }//end try
        catch (Exception e) 
        {
            throw e;
        }//end catch

    }//end method saveShutdown()
    
    private void redirectToMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

        String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.LOAD_MEMBER_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT);
        String adminAction = (String) request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {
            mainExitURL = mainExitURL + adminAction;
        }
        else
        {
            mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }

        response.sendRedirect(mainExitURL + securityParams);
    }
    
    
}//end class
