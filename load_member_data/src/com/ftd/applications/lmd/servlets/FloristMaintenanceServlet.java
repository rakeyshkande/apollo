package com.ftd.applications.lmd.servlets;

import com.ftd.applications.lmd.bo.HistoryTracker;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.MaintenanceDAO;
import com.ftd.applications.lmd.util.BaseDataBuilder;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.util.FloristUpdater;
import com.ftd.applications.lmd.util.HistoryHelper;
import com.ftd.applications.lmd.util.LockHelper;
import com.ftd.applications.lmd.util.MaintenanceXAO;
import com.ftd.applications.lmd.util.SearchManager;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.applications.lmd.vo.FloristHistoryVO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;
import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


/**
 * This servlet is used to populate fields in the Florist Maintenance Tabs.
 * The data is obtained from the DAO object, a proxy, and is loaded
 * by the FloristMaintenance tabs.
 *
 * @author Mehul Patel
 */
 
 
public class FloristMaintenanceServlet extends HttpServlet 
{    
	private Logger logger;
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
	private final static String LOGGER_CATEGORY = "com.ftd.applications.lmd.servlets.FloristMaintenanceServlet";
	private final static String XSL_FLORIST_MAINTENANCE = "/xsl/floristMaintenance.xsl";
	private final static String XSL_FULL_HISTORY = "/xsl/viewFullHistory.xsl";
	private final static String XSL_CLOSURE_OVERRIDE = "/xsl/viewClosureOverride.xsl";
	
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Delegates to the doPost method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }
    
	/**
     * Calls the function to load the Florist Maintenance Page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
		
        String action = request.getParameter("action");
        String context = request.getParameter("context");
        String securityToken = request.getParameter("securitytoken");
        
        try
        {
            // Unsecure popup actions
            if(action != null && action.equals("viewFullHistory")) 
            {
               this.viewFullHistory(request, response);
            }
            
            if(action != null && action.equals("viewClosureOverride")) 
            {
               this.viewClosureOverrides(request, response);
            }
            // Secure web actions
            else
            {
                if (ServletHelper.isWebActionSecure(context, securityToken, request, response)) 
                { 
                    if(action != null && action.equals("updateFlorist")) 
                    {
                       this.updateFlorist(request, response);
                    }
                    else if(action != null && action.equals("updateFloristNoSave")) 
                    {
                       this.updateFloristNoSave(request, response);
                    }
                    else if(action != null && action.equals("loadNewFlorist")) 
                    {
                       this.loadNewFlorist(request, response);
                    }
                    else if(action != null && action.equals("insertFlorist")) 
                    {
                       this.insertFlorist(request, response);
                    }
                    else if(action != null && action.equals("validateProServer")) 
                    {
                       this.validateProServer(request, response);
                    }	
                    else if(action != null && action.equals("loadAssociatedFlorist")) 
                    {
                       this.loadAssociatedFlorist(request, response);
                    }
                    else
                    {
                        this.loadMaintenance(request, response);
                    }
                }
            }
        } 
        catch (Exception e) 
        {
            logger.error(e);
            throw new ServletException(e);
        } 
    }
		
	
	private void viewFullHistory(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		
		   DataRequest dataRequest = null;
	  
		  try
		  {
			
			Document responseDocument = DOMUtil.getDocument();    
    
			dataRequest = DataRequestHelper.getInstance().getDataRequest();  
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			
			dataRequest.reset();
			dataRequest.setStatementID("VIEW_FLORIST_COMMENTS");
         
			Integer a = Integer.valueOf(request.getParameter("count"));
			dataRequest.addInputParam("IN_FLORIST_ID", request.getParameter("fid"));
			dataRequest.addInputParam("IN_COMMENT_COUNT", a);
			
			//CachedResultSet histResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  
			
			HashMap pageData = new HashMap();
            pageData.put("permit", request.getParameter("permit"));           
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
			
			File xslFile = new File(getServletContext().getRealPath(XSL_FULL_HISTORY));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
		
		  }
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
		  if(dataRequest.getConnection() != null)
		  {
			try{
			  dataRequest.getConnection().close();
			}
			catch(Exception e){ }
		  }
		}
	
	}
	
	
	
	
	private void viewClosureOverrides(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		
		   DataRequest dataRequest = null;
	  
		  try
		  {
			
			Document responseDocument = DOMUtil.getDocument();    
    
			dataRequest = DataRequestHelper.getInstance().getDataRequest(); 
			
			dataRequest.reset();			
			dataRequest.setStatementID("GET_CLOSURE_OVERRIDE");	
			dataRequest.addInputParam("IN_FLORIST_ID", request.getParameter("fid"));
			
			//CachedResultSet histResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  
			
			HashMap pageData = new HashMap();
            pageData.put("permit", request.getParameter("permit"));           
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
			
			File xslFile = new File(getServletContext().getRealPath(XSL_CLOSURE_OVERRIDE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
		
		  }
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
		  if(dataRequest.getConnection() != null)
		  {
			try{
			  dataRequest.getConnection().close();
			}
			catch(Exception e){ }
		  }
		}
	
	}
	
	
	private void validateProServer(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	
	    DataRequest dataRequest = null;
        
        try
        {
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
        
			if(request.getParameter("idvalneeded").equals("yes"))
			{
                String inId = request.getParameter("memnum");
                dataRequest.reset();
                dataRequest.setStatementID("IS_ID_UNIQUE");
                dataRequest.addInputParam("IN_FLORIST_ID", inId);
                String isUnique = (String)dataAccessUtil.execute(dataRequest);
                
                if(isUnique.equals("Y"))
                {
                    HashMap flowMap = new HashMap();
                    flowMap.put("validID", "No");
                    DOMUtil.addSection(responseDocument, "idFlow", "flow", flowMap, true);
                }
                else
                {
                    HashMap flowMap = new HashMap();
                    flowMap.put("validID", "Yes");
                    DOMUtil.addSection(responseDocument, "idFlow", "flow", flowMap, true);
                }
			}
            
			String inZip = request.getParameter("zipcode");
			String inState = request.getParameter("state");
			if (! "NA".equalsIgnoreCase(inState)) {   
			    // Do zip check
    			dataRequest.reset();
    			dataRequest.setStatementID("GET_ZIP_DETAILS");
    			dataRequest.addInputParam("ZIP_CODE", inZip);
    			String notCodeExists = (String)dataAccessUtil.execute(dataRequest);
    			
    			if(!notCodeExists.equals("Y"))
    			{
    				HashMap flowMap = new HashMap();
                    flowMap.put("validProZip", "No");
                    DOMUtil.addSection(responseDocument, "profileFlow", "flow", flowMap, true);
    			}
    			else
    			{
    				HashMap flowMap = new HashMap();
                    flowMap.put("validProZip", "Yes");
                    DOMUtil.addSection(responseDocument, "profileFlow", "flow", flowMap, true);
    			}
    			
			} else {
                // Skip zip check for international florist (state is NA) 
                HashMap flowMap = new HashMap();
                flowMap.put("validProZip", "Yes");
                DOMUtil.addSection(responseDocument, "profileFlow", "flow", flowMap, true);			    
			}
			
            FloristVO florist = null;
            String isNewFlag = request.getParameter("isNew");
            
			if(isNewFlag != null && isNewFlag.equals("Y"))
            {
                florist = new FloristVO();
            }
            else
            {
                MaintenanceDAO maintDAO = new MaintenanceDAO(dataRequest.getConnection());
                florist = maintDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
            }
              
            FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
            florist = floristUpdater.updateFloristFromRequest(florist, request);  
                
			MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
			DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes());
				
			BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
                
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
			
		}
		catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
	
	}
	
	
	/**
     * This function loads the page by populating status and state lists.
	 * Data is fetched from the DAO object and is used to load up tab pages.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void loadMaintenance(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			HashMap pageData = new HashMap();
            SecurityManager securityManager = SecurityManager.getInstance();
			
            ServletHelper servletHelper = new ServletHelper();
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
		
           // SearchManager searchManager = new SearchManager();
			
            if(request.getParameter("floristNumber") != null && !request.getParameter("floristNumber").equals(""))
            {
                // Load florist
                MaintenanceDAO maintDAO = new MaintenanceDAO(dataRequest.getConnection());
                FloristVO florist = maintDAO.load(request.getParameter("floristNumber"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                // Update lock for user
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestMaintenanceLock(florist, securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
                
                // Convert to XML
                MaintenanceXAO maintXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(florist)).getChildNodes()); 
                // Genrate base page data
                BaseDataBuilder data = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument,data.loadGenericDataData(null, dataRequest, request, false));
            }
            
			if(request.getParameter("disp") != null)
			{
			
				pageData.put("display", "no");
				DOMUtil.addSection(responseDocument, "link", "data", pageData, true);

			}

            logger.debug(DOMUtil.convertToString(responseDocument));

            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    /**
     * This function loads the page by populating status and state lists.  All 
     * florist data is empty.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
	private void loadNewFlorist(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{			
			// Create the initial document
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            BaseDataBuilder data = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument,data.loadGenericDataData(null, dataRequest, request, false));

            HashMap flowMap = new HashMap();
            flowMap.put("processType", "AddNew");
            DOMUtil.addSection(responseDocument, "processFlow", "flow", flowMap, true);
				
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
		
        }
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    private void insertFlorist(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            if(request.getParameter("memnum") != null && !request.getParameter("memnum").equals(""))
            {
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
               
                // Update florist from request
                FloristVO newFlorist = new FloristVO();
                FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                newFlorist = floristUpdater.updateFloristFromRequest(newFlorist, request);
                
                // Set default values for new florist
                newFlorist.setMemberNumber(request.getParameter("memnum"));
                newFlorist.setRecordType("R");
                newFlorist.setStatus("Active");
                
                // Vendor defaults
                if(request.getParameter("vendorflag") != null && request.getParameter("vendorflag").equals("Y"))
                {
                    newFlorist.setVendorFlag("Y");
                    newFlorist.getFloristWeight().setFinalWeight(new Integer(1));
                }
                // Florist defaults
                else
                {
                    // Normal florist
                    newFlorist.setVendorFlag("N");
                    
                    // Default adjusted weight to the following
                    if(request.getParameter("adjustedweight") == null || request.getParameter("adjustedweight").trim().length() == 0)
                    {
                        newFlorist.getFloristWeight().setAdjustedWeight(new Integer(12));
                    }
                    
                    // Default adjusted weight end date to the following
                    if(request.getParameter("adjustedenddate") == null || request.getParameter("adjustedenddate").trim().length() == 0)
                    {
                        Calendar endDate = Calendar.getInstance();
                        endDate.add(Calendar.DATE, 90);
                        
                        newFlorist.getFloristWeight().setAdjustedWeightEndDate(endDate.getTime());
                    } 
                    
                    // Add zip code of residence
                    newFlorist.getPostalCodeMap().put(newFlorist.getPostalCode(), this.loadZipCode(newFlorist, dataRequest));
                }

                // Save florist to database
                maintenanceDAO.insertFlorist(newFlorist);
                
                // Persist update comment to database
                this.persistUpdateComment(request, newFlorist.getLastUpdateUser(), dataRequest);
                    
                // Reload from database to retrieve any database logic updates from the save
                newFlorist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                // Update lock for user
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestMaintenanceLock(newFlorist, newFlorist.getLastUpdateUser());
    
				HashMap flowMap = new HashMap();
                flowMap.put("isAdded", "Y");
                DOMUtil.addSection(responseDocument, "processFlow", "flow", flowMap, true);

                // Transform presentation
                MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(newFlorist)).getChildNodes()); 
            }
            else
            {
                // Create page data for error message
                HashMap pageData = new HashMap();
                pageData.put("member_number_error", "Member number is not unique");
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                
                // Override process flow to stay in Add New
                HashMap flowMap = new HashMap();
                flowMap.put("processType", "AddNew");
                DOMUtil.addSection(responseDocument, "processFlow", "flow", flowMap, true);

            }
            
            BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
			
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    private void updateFlorist(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            if(request.getParameter("memnum") != null && request.getParameter("memnum").length() > 0)
            {
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
                ObjectCopyUtil objectCopyUtil = new ObjectCopyUtil();
               
                // Pull two florists from database
                FloristVO currentFlorist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                FloristVO previousFlorist = (FloristVO) objectCopyUtil.deepCopy(currentFlorist);
                
                // Update florist from request
                FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                currentFlorist = floristUpdater.updateFloristFromRequest(currentFlorist, request);
                
                // Update lock for user
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestMaintenanceLock(currentFlorist, currentFlorist.getLastUpdateUser());
                
                // Save florist to database if lock is still present
                if(currentFlorist.getLockedFlag() != null && currentFlorist.getLockedFlag().equals("Y"))
                {
                    maintenanceDAO.updateFlorist(currentFlorist);
                    
                    // Track auto florist history and persist to database
                    HistoryHelper historyHelper = new HistoryHelper(dataRequest);
                    List historyList = historyHelper.generateMaintenanceHistory(previousFlorist, currentFlorist);
                    
                    // Persist update comment to databse
                    this.persistUpdateComment(request, currentFlorist.getLastUpdateUser(), dataRequest);
                    
                    // Reload from database to retrieve any database logic updates from the save
                    currentFlorist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                }
                
                // Transform presentation
                MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(currentFlorist)).getChildNodes()); 
       
                BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
                
                File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
            
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    private void updateFloristNoSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
        
        try
        {
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
		
            if(request.getParameter("memnum") != null && request.getParameter("memnum").length() > 0)
            {
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
                FloristVO florist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
                florist = floristUpdater.updateFloristFromRequest(florist, request); 
                
                // Update lock for user
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.requestMaintenanceLock(florist, florist.getLastUpdateUser());
				
                MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes()); 
				
                BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
			
                File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
            }
            
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
    private void loadAssociatedFlorist(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		DataRequest dataRequest = null;
		
		try
		{
			HashMap pageData = new HashMap();
            SecurityManager securityManager = SecurityManager.getInstance();
			
            ServletHelper servletHelper = new ServletHelper();
			Document responseDocument = DOMUtil.getDocument();
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
		
            SearchManager searchManager = new SearchManager();
            
            if(request.getParameter("floristNumber") != null && !request.getParameter("floristNumber").equals(""))
            {
                // Release lock on current florist
                String userId = securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID();
                LockHelper lockHelper = new LockHelper(dataRequest);
                lockHelper.releaseMaintenanceLock(request.getParameter("previousFloristNumber"), userId);
            
                // Load associated florist
                MaintenanceDAO maintDAO = new MaintenanceDAO(dataRequest.getConnection());
                FloristVO florist = maintDAO.load(request.getParameter("floristNumber"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
                
                // Update lock for user
                lockHelper.requestMaintenanceLock(florist, securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
                
                // Convert to XML
                MaintenanceXAO maintXAO = new MaintenanceXAO();
                DOMUtil.addSection(responseDocument, ((Document) maintXAO.generateXML(florist)).getChildNodes()); 
                
                // Genrate base page data
                BaseDataBuilder data = new BaseDataBuilder();
                DOMUtil.addSection(responseDocument,data.loadGenericDataData(null, dataRequest, request, false));
            }
            
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));

		}
		
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
		}
	}
    
    private PostalCodeVO loadZipCode(FloristVO newFlorist, DataRequest dataRequest) throws Exception
    {
        PostalCodeVO postalCode = new PostalCodeVO();
        postalCode.setPostalCode(newFlorist.getPostalCode());
        
        // Unsaved postal codes can not be blocked
        postalCode.setBlockedFlag("N");
        postalCode.setSavedFlag("N");
        
        // Default cutoff time
        postalCode.setCutoffTime("1400");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("VIEW_ZIP_CITY_FLORIST_COUNT");
		
		if(postalCode.getPostalCode() != null && postalCode.getPostalCode().length() > 5)
		{
			postalCode.setPostalCode(postalCode.getPostalCode().substring(0,3));
		}
		
        dataRequest.addInputParam("ZIP", postalCode.getPostalCode());
        
        CachedResultSet countResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
        if(countResultSet != null && countResultSet.getRowCount() > 0)
        {            
            while(countResultSet.next())
            {
                postalCode.setFloristCount(new Integer(((BigDecimal) countResultSet.getObject(1)).intValue()));
                postalCode.setCity((String) countResultSet.getObject(2));
            }
        }
        
        return postalCode;
    }
    
    private void validateFloristID(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	    DataRequest dataRequest = null;
        
        try
        {
            ServletHelper servletHelper = new ServletHelper();
            Document responseDocument = DOMUtil.getDocument();
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
 
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			String inId = request.getParameter("memnum");
			dataRequest.reset();
			dataRequest.setStatementID("IS_ID_UNIQUE");
			dataRequest.addInputParam("IN_FLORIST_ID", inId);
			String isUnique = (String)dataAccessUtil.execute(dataRequest);
			
			if(isUnique.equals("Y"))
			{
				HashMap flowMap = new HashMap();
                flowMap.put("validID", "No");
                DOMUtil.addSection(responseDocument, "idFlow", "flow", flowMap, true);
			}
			else
			{
				HashMap flowMap = new HashMap();
                flowMap.put("validID", "Yes");
                DOMUtil.addSection(responseDocument, "idFlow", "flow", flowMap, true);
			}
			
			MaintenanceDAO maintenanceDAO = new MaintenanceDAO(dataRequest.getConnection());
			FloristVO florist = maintenanceDAO.load(request.getParameter("memnum"), servletHelper.pullCurrentUser(request.getParameter("securitytoken")));
			
			FloristUpdater floristUpdater = new FloristUpdater(dataRequest);
			florist = floristUpdater.updateFloristFromRequest(florist, request);   
            
            // Update lock for user
            LockHelper lockHelper = new LockHelper(dataRequest);
            lockHelper.requestMaintenanceLock(florist, florist.getLastUpdateUser());
                
			MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
			DOMUtil.addSection(responseDocument, ((Document) maintenanceXAO.generateXML(florist)).getChildNodes());
				
			BaseDataBuilder baseDataBuilder = new BaseDataBuilder();
            DOMUtil.addSection(responseDocument, baseDataBuilder.loadGenericDataData(null, dataRequest, request, false));
				
            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_MAINTENANCE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
			
		}
		catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
	
	}
    
    private void persistUpdateComment(HttpServletRequest request, String userId, DataRequest dataRequest) throws Exception
    {
        if(request.getParameter("newcomment") != null && request.getParameter("newcomment").length() > 0)
        {
            FloristHistoryVO floristHistory = new FloristHistoryVO();
    
            floristHistory.setComments(request.getParameter("newcomment")); // the textarea

            floristHistory.setCommentDate(formatCommentDate(new Date()));
            floristHistory.setCommentType(request.getParameter("dispositionDescription"));
            floristHistory.setManagerFlag(request.getParameter("newmanagerflag"));
            floristHistory.setUserId(userId);
            floristHistory.setNewCommentFlag("Y");
            
            // Persist comment to database immediatly
            HistoryHelper historyHelper = new HistoryHelper(dataRequest);
            historyHelper.persistHistory(request.getParameter("memnum"), floristHistory);
        }
    }
    
    private String formatCommentDate(Date date)
    {
        String formattedDate = "";
        
        if(date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
            formattedDate = sdf.format(date);
        }
        
        return formattedDate;
    }
    
} // end of the servlet class.
