
package com.ftd.applications.lmd.servlets;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.servlets.actions.ApproveFloristAuditReportAction;
import com.ftd.applications.lmd.servlets.actions.ApproveFloristWeightAuditReportAction;
import com.ftd.applications.lmd.servlets.actions.LoadFloristDataAction;
import com.ftd.applications.lmd.servlets.actions.LoadFloristZipsDataAction;
import com.ftd.applications.lmd.servlets.actions.LoadFloristWeightDataAction;
import com.ftd.applications.lmd.servlets.actions.RejectFloristAuditReportAction;
import com.ftd.applications.lmd.servlets.actions.RejectFloristWeightAuditReportAction;
import com.ftd.applications.lmd.servlets.actions.ViewFloristAuditReportAction;
import com.ftd.applications.lmd.servlets.actions.ViewFloristWeightAuditReportAction;
import com.ftd.applications.lmd.servlets.actions.ViewZipRemovalsReportAction;
import com.ftd.applications.lmd.util.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;


import org.xml.sax.SAXException;


/**
 * The Load Member Data process involves the receipt of florist related 
 * information from FTDI and the processing of that data to be used by FTD.COM�s
 * applications.
 * 
 * The files will be pulled from the server to our order processing systems via
 * FTP.  The process will be executed from an administration page <func id> 
 * that calls this servlet, which contains the logic to execute the FTP process.
 * 
 * @author Anshu Gaind
 * @version $Id: MemberDataLoadServlet.java,v 1.4.82.1 2015/01/23 00:17:57 gsergeycvs Exp $
 */
public class MemberDataLoadServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private ServletConfig servletConfig;
  private Logger logger  = new Logger("com.ftd.applications.lmd.servlets.MemberDataLoadServlet");
  
  /**
   * Initialize the servlet
   * @param config
   * @throws javax.servlet.ServletException
   */
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    this.servletConfig = config;
  }

  /**
   * 
   * @param request
   * @param response
   * @throws javax.servlet.ServletException
   * @throws java.io.IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    String context = request.getParameter("context");
    String securityToken = request.getParameter("securitytoken");
    String action = request.getParameter("action");
    
    response.setContentType(CONTENT_TYPE);

    try 
    {      
      ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
      
      if (ServletHelper.isValidToken(context, securityToken)) 
      {          
          if (action != null && action.equals("load-florist-data")) 
          {
            logger.debug(action);
            new LoadFloristDataAction().execute(request, response);
            request.setAttribute("confirmation", "Request to load florist data has been submitted");
            this.displayIndexPage(request, response);
          } 
          else if (action != null && action.equals("view-florist-audit-report")) 
          {
            logger.debug(action);
            new ViewFloristAuditReportAction().execute(request, response);
          } 
          else if (action != null && action.equals("view-zip-removals-report")) 
          {
            logger.debug(action);
            new ViewZipRemovalsReportAction().execute(request, response);
          }
          else if (action != null && action.equals("approve-florist-audit-report")) 
          {
            logger.debug(action);            
            request.setAttribute("NEW_STATUS", configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_APPROVED"));
            new ApproveFloristAuditReportAction().execute(request, response);
          } 
          else if (action != null && action.equals("reject-florist-audit-report")) 
          {
            logger.debug(action);
            request.setAttribute("NEW_STATUS", configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_REJECTED"));
            new RejectFloristAuditReportAction().execute(request, response);
          }
          else if(action != null && action.equals("view-florist-audit-report-list")) // added..
          {
            Document responseDocument = (Document)new FloristDAO().getFloristAuditReportNames();
            File xslFile = new File(getServletContext().getRealPath("/xsl/audit-report-list.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
          }
          else if (action != null && action.equals("load-florist-weight-data")) 
          {
            logger.debug(action);
            new LoadFloristWeightDataAction().execute(request, response);
            request.setAttribute("confirmation", "Request to load weight data has been submitted");
            this.displayIndexPage(request, response);
          } 
          else if (action != null && action.equals("view-florist-weight-audit-report")) 
          {
            logger.debug(action);
            new ViewFloristWeightAuditReportAction().execute(request, response);
          } 
          else if (action != null && action.equals("approve-florist-weight-audit-report")) 
          {
            logger.debug(action);            
            request.setAttribute("NEW_STATUS", configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_APPROVED"));
            new ApproveFloristWeightAuditReportAction().execute(request, response);
          } 
          else if (action != null && action.equals("reject-florist-weight-audit-report")) 
          {
            logger.debug(action);
            request.setAttribute("NEW_STATUS", configurationUtil.getProperty("load_member_config.xml", "AUDIT_REPORT_STATUS_REJECTED"));
            new RejectFloristWeightAuditReportAction().execute(request, response);
          }
          else if(action != null && action.equals("view-florist-weight-audit-report-list")) // added..
          {
            Document responseDocument = (Document)new FloristDAO().getFloristWeightCalcAuditReportNames();
            File xslFile = new File(getServletContext().getRealPath("/xsl/weight-audit-report-list.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, ServletHelper.getParameterMap(request));
          }
          else if(action != null && action.equals("load-florist-zip-data")) 
          {
              logger.debug(action);
              new LoadFloristZipsDataAction().execute(request, response);
              request.setAttribute("confirmation", "Request to load florist zip data has been submitted");
              this.displayIndexPage(request, response);
          }
          else 
          {
            this.displayIndexPage(request, response);
          }
      }// end if
    } 
    catch (Throwable t) 
    {
      logger.error(t);
      throw new ServletException(t);
    } 

  }

  /**
   * 
   * @param request
   * @param response
   * @throws javax.servlet.ServletException
   * @throws java.io.IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    this.doGet(request, response);
  }
  
  /**
   * Displays the index page
   * 
   * @param request
   * @param response
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private void displayIndexPage(HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException , IOException, SQLException, SAXException, Exception
  {
    // display index
    TraxUtil traxUtil = TraxUtil.getInstance();
    File indexXSL = new File(getServletContext().getRealPath("/xsl/index.xsl"));
    HashMap parameters = ServletHelper.getParameterMap(request);
    String confirmation = (String)request.getAttribute("confirmation");
    parameters.put("confirmation", confirmation == null? "" : confirmation);
    logger.debug("Setting confirmation in parameter map:" + confirmation);
    traxUtil.transform(request, response, DOMUtil.getDefaultDocument(), indexXSL, parameters);
    
  }
  
 
  
 

  
    

}//~
