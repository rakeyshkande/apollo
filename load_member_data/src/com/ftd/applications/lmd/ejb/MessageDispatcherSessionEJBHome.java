package com.ftd.applications.lmd.ejb;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface MessageDispatcherSessionEJBHome extends EJBHome
{
  public MessageDispatcherSessionEJB create() throws RemoteException, CreateException;
}