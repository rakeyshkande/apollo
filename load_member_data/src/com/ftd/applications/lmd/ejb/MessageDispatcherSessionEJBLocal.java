package com.ftd.applications.lmd.ejb;
import java.io.IOException;
import java.sql.SQLException;
import javax.ejb.EJBLocalObject;
import com.ftd.osp.utilities.vo.MessageToken;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public interface MessageDispatcherSessionEJBLocal extends EJBLocalObject 
{

  public void dispatchTextMessage(MessageToken messageToken);

  public void updateFloristWeightCalcAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception;

  public void updateFloristAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy) 
    throws IOException, Exception, SQLException, ParserConfigurationException, TransformerException , SAXException;

}
