package com.ftd.applications.lmd.ejb;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.w3c.dom.Document;

import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.avs.webservice.Address;
import com.ftd.avs.webservice.AddressVerificationService;
import com.ftd.avs.webservice.AddressVerificationServiceBingImplService;
import com.ftd.avs.webservice.Exception_Exception;
import com.ftd.avs.webservice.VerificationRequest;
import com.ftd.avs.webservice.VerificationResponse;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * @author smeka
 * 
 */
public class FloristLatLongUpdateMDB implements MessageDrivenBean,
		MessageListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MessageDrivenContext context;
	
	private static Logger logger = new Logger("com.ftd.applications.lmd.ejb.FloristLatLongUpdateMDB");
	
	private static AddressVerificationService addressVerificationSerivce;
	
	private static String avsURL;
	
	private static String DEFAULT_COUNTRY = "US";
	
	private static final String LAT_LONG_UPDATED_BY = "FLORIST_LATLONG_UPDATE_MDB";

	/**
	 * 
	 */
	public void ejbCreate() {
	}

	/**
	 * Receives messages to update the florist lat and long periodically.
	 * 
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message msg) {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering FloristLatLongUpdateMDB onMessage()");
		}
		try {
			processUpdateFloristLatLongReq(msg);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting FloristLatLongUpdateMDB onMessage()");
			}			
			if (context.getRollbackOnly()) {
				throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
			}
		}
	}

	/**
	 * Process Update Florist Longitude and Latitude
	 * 
	 * @param msg
	 * @throws Exception
	 */
	private void processUpdateFloristLatLongReq(Message msg) throws Exception {
		Connection conn = null;
		boolean success = false;
		
		try {
			FloristVO florist = getFloristFromJMSMsg(((TextMessage) msg).getText());

			if (!isValidFloristData(florist)) {
				throw new Exception("Florist detail is insifficient to update lat and long.");
			}

			double[] latLong = getLatLong(florist);

			if (latLong == null) {
				StringBuffer address = new StringBuffer("\n FloristId : "
						+ florist.getMemberNumber() + "\n florist address : "
						+ florist.getAddress() + "\n florist city : "
						+ florist.getCity() + "\n florist state : "
						+ florist.getState() + "\n florist zip : "
						+ florist.getPostalCode());
				throw new Exception("Unable to calculate latitude and longitude for a given florist address -" + address);
			}

			conn = DataRequestHelper.getConnection();						
			success = new FloristDAO().updateFloristLatLong(conn, florist.getMemberNumber(), latLong, LAT_LONG_UPDATED_BY);
			
			if(success) {
				logger.info("FloristLatLongUpdateMDB - Succesfullt updated lat long for florist : " + florist.getMemberNumber());
			}

		} catch (Throwable e) {
			logger.error("Error caught processing FloristLatLongUpdateMDB request: " + e.getMessage());

		} finally {
			if (!success) {
				logger.error("Cannot process the request to FloristLatLongUpdateMDB. Rollback transaction");
				this.context.setRollbackOnly();
			}
			if (conn != null && !conn.isClosed()) {
				logger.debug("Closing connection.");
				conn.close();
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Exiting processUpdateFloristLatLongReq().");
			}
		}
	}

	/**
	 * Constructs FloristVO using JMS message. 
	 * @param textMessage
	 * @return
	 * @throws Exception
	 */
	private FloristVO getFloristFromJMSMsg(String textMessage) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("******** getFloristFromJMSMsg()******** Extracting florist detail from JMS message \n" + textMessage);
		}
		try {
			Document jmsMessage = DOMUtil.getDocument(textMessage);
			FloristVO florist = new FloristVO();
			
			florist.setMemberNumber(DOMUtil.selectSingleNode(jmsMessage,
					"/FLORIST_LATLONG_UPDATE/FLORIST_ID/text()").getNodeValue());
			
			florist.setAddress(DOMUtil.selectSingleNode(jmsMessage,
					"/FLORIST_LATLONG_UPDATE/ADDRESS/text()").getNodeValue());
			
			florist.setCity(DOMUtil.selectSingleNode(jmsMessage,
					"/FLORIST_LATLONG_UPDATE/CITY/text()").getNodeValue());
			
			florist.setState(DOMUtil.selectSingleNode(jmsMessage,
					"/FLORIST_LATLONG_UPDATE/STATE/text()").getNodeValue());
			
			florist.setPostalCode(DOMUtil.selectSingleNode(jmsMessage,
					"/FLORIST_LATLONG_UPDATE/ZIP_CODE/text()").getNodeValue());
			return florist;
		} catch (Exception e) {
			throw new Exception(
					"Error caught extracting florist detail from JMS message - "
							+ e.getMessage());
		}
	}

	/**
	 * Validate the Florist bean.
	 * 
	 * @param florist
	 * @return
	 */
	private boolean isValidFloristData(FloristVO florist) {
		if (florist.getMemberNumber() == null
				|| florist.getMemberNumber().length() <= 0) {
			return false;
		}
		if (florist.getCity() == null || florist.getCity().length() <= 0) {
			return false;
		}
		if (florist.getPostalCode() == null
				|| florist.getPostalCode().length() <= 0) {
			return false;
		}

		return true;
	}

	/**
	 * Get the Latitude and Longitude for florist address interacting with Bing
	 * Service.
	 * 
	 * @param florist
	 * @return
	 * @throws Exception
	 */
	private double[] getLatLong(FloristVO florist) throws Exception {

		Address address = new Address();
		address.setStreet1(florist.getAddress());
		address.setCity(florist.getCity());
		address.setState(florist.getState());
		address.setZip(florist.getPostalCode());
		address.setCountry(DEFAULT_COUNTRY);

		VerificationRequest verificationRequest = new VerificationRequest();
		verificationRequest.setAddress(address);

		try {
			AddressVerificationService service = getAddressVerificationService();
			VerificationResponse resp = service.verifyAddress(verificationRequest);

			if (resp != null && resp.getVerifiedAddresses() != null	&& resp.getVerifiedAddresses().size() > 0) {
				if (resp.getVerifiedAddresses().get(0).getLongitutde() != null && resp.getVerifiedAddresses().get(0).getLatitude() != null) {
					logger.info("Verified Address succesfully : " + resp.getVerifiedAddresses().size());
					logger.info("latitude : " + resp.getVerifiedAddresses().get(0).getLatitude() + ", longitude: " + resp.getVerifiedAddresses().get(0).getLongitutde());
					double[] latLong = {Double.valueOf(resp.getVerifiedAddresses().get(0).getLatitude()), 
										Double.valueOf(resp.getVerifiedAddresses().get(0).getLongitutde())};
					return latLong;
				}
			} else {
				logger.info("Unable to get the verified address for florist Id:" + florist.getMemberNumber());
			}
		} catch (Exception_Exception e) {
			throw new Exception("Error caught sending addres verification request to AVS : " + e.getMessage());
		}
		return null;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see javax.ejb.MessageDrivenBean#ejbRemove()
	 */
	public void ejbRemove() {
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see javax.ejb.MessageDrivenBean#setMessageDrivenContext(javax.ejb.MessageDrivenContext)
	 */
	public void setMessageDrivenContext(MessageDrivenContext ctx) {
		this.context = ctx;
	}
	
    /** Get the Address Verification service instance.
     * @return
     * @throws CacheException
     * @throws Exception
     */
    private static AddressVerificationService getAddressVerificationService() throws CacheException, Exception {
    	GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
    	if (addressVerificationSerivce == null || 
				(avsURL != null && !avsURL.equals(gph.getFrpGlobalParm("SERVICE", "ADDRESS_VERIFICATION_SERVICE_URL")) )) {
			
			avsURL = gph.getFrpGlobalParm("SERVICE", "ADDRESS_VERIFICATION_SERVICE_URL");
			logger.warn("Creating Address Verification Service reference from wsdl: " + avsURL);
			try {
				URL avsServiceURL = new URL(avsURL);
				AddressVerificationServiceBingImplService avs = new AddressVerificationServiceBingImplService(avsServiceURL);
				addressVerificationSerivce = avs.getAddressVerificationServiceBingImplPort();
			} catch (MalformedURLException e) {
				logger.error("Error when getting bing web service", e);
				e.printStackTrace();
			}
		}
		
		return addressVerificationSerivce;
	}
}