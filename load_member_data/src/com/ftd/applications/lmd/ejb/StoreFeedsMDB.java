/**
 * 
 */
package com.ftd.applications.lmd.ejb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.applications.lmd.bo.StoreFeedsBO;
import com.ftd.applications.lmd.constants.StoreFeedConstants;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class StoreFeedsMDB implements MessageDrivenBean, MessageListener {
	
	private static final long serialVersionUID = 1L;	
	private MessageDrivenContext context;
	private Logger logger;
	
	@Override
	public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
		this.context = ctx;
		logger = new Logger(StoreFeedsMDB.class.getName());
		if(logger.isDebugEnabled()) {
			logger.debug("MessageDrivenContext initialized :" + this.context);
		}
	}
	
	@Override
	public void onMessage(Message  msg) {
		if (logger.isDebugEnabled()) {
			logger.debug("**********StoreFeedsMDB.onMessage()***************");
		}
		try {
			
			TextMessage textMessage = (TextMessage) msg;
			String msgText = textMessage.getText();
						
			if(msgText == null || msgText.equals("")) {
				throw new Exception("Invalid request. message text cannot be empty.");
			}
			
			String[] messageContent = msgText.split(StoreFeedConstants.EMPTY_STR);

			if ((messageContent[0] == null || messageContent[0].equals("")) 
					|| (messageContent[1] == null || (messageContent[1].equals("")))) {
				throw new Exception("Invalid action/data: message text cannot be empty.");
			} 
			
			if(logger.isDebugEnabled()) {
				logger.debug("***************PROCESS_" + messageContent[0] + "****************");
			}
			
			StoreFeedsBO storeFeedsBO = null;
			
			if (StoreFeedConstants.SOURCE_FEED.equalsIgnoreCase(messageContent[0])) {				
				storeFeedsBO = new StoreFeedsBO();
				storeFeedsBO.processSourceFeed(messageContent[1]);
			} else if (StoreFeedConstants.SHIPPING_KEY_FEED.equalsIgnoreCase(messageContent[0])) {				
				storeFeedsBO = new StoreFeedsBO();
				storeFeedsBO.processShippingKeyFeed(msgText);
			}else if (StoreFeedConstants.PRICE_CODE_FEED.equalsIgnoreCase(messageContent[0])) {				
				storeFeedsBO = new StoreFeedsBO();
				storeFeedsBO.processPriceCodeFeed(messageContent[1]);
			} 
			else {
				logger.error("Invalid action: no process is defined for action, " + messageContent[0]);				
			}
			
			logger.info("Finished StoreFeedsMDB");
			
		} catch (Exception e) {
			logger.error("Error caught: " + e.getMessage());
		} catch (Throwable t) {
			logger.error("Thrown error: " + t);
		}		
	}

	@Override
	public void ejbRemove() throws EJBException {	}
	
	public void ejbCreate() {	}

}
