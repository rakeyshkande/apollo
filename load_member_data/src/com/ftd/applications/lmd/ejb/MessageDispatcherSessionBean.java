package com.ftd.applications.lmd.ejb;
import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.SQLException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import javax.naming.InitialContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * Dispatches the JMS message back to the queue, as an independent transaction
 * 
 * @author Anshu Gaind
 * @version $Id: MessageDispatcherSessionBean.java,v 1.3 2011/06/30 15:14:15 gsergeycvs Exp $
 */

public class MessageDispatcherSessionBean implements SessionBean 
{
  private Logger logger = new Logger("com.ftd.applications.lmd.ejb.MessageDispatcherSessionBean");
  private SessionContext sessionContext;
  private FloristDAO floristDAO;
  
  public void ejbCreate()
  {
  }

  public void ejbActivate()
  {
  }

  public void ejbPassivate()
  {
  }

  public void ejbRemove()
  {
  }

  public void setSessionContext(SessionContext ctx)
  {
    this.sessionContext = ctx;
  }


  /**
   * Dispatches the text message, as part of a new transaction
   * 
   * @param messageToken
   */
  public void dispatchTextMessage(MessageToken messageToken)
  {
    try 
    {
      Dispatcher.getInstance().dispatchTextMessage(new InitialContext(), messageToken);      
    } catch (Exception ex) 
    {
        logger.error(ex);
        // write the stack trace to the log_message variable
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        ex.printStackTrace(pw);
        pw.close();        
      // log exception
      throw new EJBException(sw.toString());
    } 
  }

  /**
   * Updates the florist weight calculation audit report
   * 
   * @param reportID
   * @param newStatus
   * @param oldStatus
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristWeightCalcAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, Exception
  {
    if (floristDAO == null) 
    {
        floristDAO = new FloristDAO();
    }
    
    floristDAO.updateFloristWeightCalcAuditReport(reportID, newStatus, oldStatus, updatedBy);
  }

  /**
   * Updates florist audit report
   * 
   * @param reportID
   * @param newStatus
   * @param oldStatus
   * @param updatedBy
   * @throws java.io.IOException
   * @throws java.lang.Exception
   * @throws java.sql.SQLException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws org.xml.sax.SAXException
   */
  public void updateFloristAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy) throws IOException, Exception, SQLException, ParserConfigurationException, TransformerException , SAXException
  {
    if (floristDAO == null) 
    {
        floristDAO = new FloristDAO();
    }
    
    floristDAO.updateFloristAuditReport(reportID, newStatus, oldStatus, updatedBy);
  }
  
  


}
