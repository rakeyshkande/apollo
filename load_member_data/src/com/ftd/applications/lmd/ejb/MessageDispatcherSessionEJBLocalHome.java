package com.ftd.applications.lmd.ejb;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface MessageDispatcherSessionEJBLocalHome extends EJBLocalHome 
{
  public MessageDispatcherSessionEJBLocal create() throws CreateException;
}