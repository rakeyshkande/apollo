package com.ftd.applications.lmd.ejb;

import com.ftd.applications.lmd.dao.ManualMercuryFloristDAO;

import com.ftd.applications.lmd.util.SystemMessage;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.StringTokenizer;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.w3c.dom.Document;

public class FTDMShutdownMDBBean implements MessageDrivenBean, 
                                            MessageListener {
    private Logger logger;
    private static final String LOGGER_CATEGORY = 
                        "com.ftd.applications.lmd.ejb.FTDShutdownMDBBean";
    private MessageDrivenContext _context;
    // FTD SHUTDOWN TYPES
    public static final String SHUTDOWN= "SHUTDOWN";
    public static final String UPDATE= "UPDATE";
    public static final String UPDATE_RESTART= "UPDATE_RESTART";


    public void ejbCreate() {
    }

    public void setMessageDrivenContext(MessageDrivenContext context) throws EJBException {
        logger = new Logger("com.ftd.applications.lmd.ejb.FTDShutdownMDBBean");
        _context = context;
    }

    public void ejbRemove() throws EJBException {
    }

    public void onMessage(Message message) {
        try {
        //get text that is part of jms message
         String textMessage = ((TextMessage) message).getText();      
         logger.debug("message: "+textMessage);
         Document doc = (Document) DOMUtil.getDocument(textMessage);  
         String payload = DOMUtil.selectSingleNode(doc, "/event/payload/text()").getNodeValue();
         
        //message token format - TYPE,CSR,RESTART_DATE
        StringTokenizer tokenizer = new StringTokenizer(payload,",");

        String messageFlag = tokenizer.nextToken();
        //get the csr id, if one exists
        String csr = null;            
        if(tokenizer.hasMoreTokens())
        {
            csr = tokenizer.nextToken();            
        }
        else
        {
          throw new Exception("CSR id does not exist in token.");
        }
        
        //get the csr id, if one exists
        String restartDateStr = null;            
        Date restartDate = null;
        if(tokenizer.hasMoreTokens())
        {
          restartDateStr = tokenizer.nextToken();            
          SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
          restartDate = sdf.parse(restartDateStr);
         
        }
        else
        {
          throw new Exception("Restart Date does not exist in token.");
        }
        
        ManualMercuryFloristDAO dao = new ManualMercuryFloristDAO();

        if(messageFlag.equals(SHUTDOWN))
        {
            dao.saveShutdown(restartDate, csr);            
        }
        else if(messageFlag.equals(UPDATE))
        {
            dao.saveRestart(restartDate, csr, true);            
        }
        else if(messageFlag.equals(UPDATE_RESTART))
        {
            dao.saveRestart(restartDate, csr, false);
            dao.restartManualMercuryFlorist(csr);
        }
        } catch (Throwable t){
            
            logger.debug(t);
            this.sendSystemMessage(t);
        }
    }
    /**
     * Sends a system message
     * 
     * @param t
     */
    private void sendSystemMessage(Throwable t)
    {
      Connection con = null;
      try 
      {
        String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
        // write the stack trace to the log_message variable
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        String logMessage = "FTDM Shutdown failure: " + "\n" + sw.toString();
        pw.close();        
        
        SystemMessage.send(logMessage, "FTDM SHUTDOWN", SystemMessage.LEVEL_PRODUCTION, "ERROR", con);
      } catch (Exception ex) 
      {
        logger.error(ex);
      } finally 
      {
        try 
        {
          if (con != null && (! con.isClosed() )) 
          {
              con.close();
          }
          
        } catch (Exception ex2) 
        {
          logger.error(ex2);
        }      
      }
    }
}
