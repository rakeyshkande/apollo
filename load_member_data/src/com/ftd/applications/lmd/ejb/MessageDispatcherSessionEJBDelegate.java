package com.ftd.applications.lmd.ejb;
import com.ftd.osp.utilities.vo.MessageToken;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * Delegate to the MessageDispatcherSessionEJB
 * 
 * @author Anshu Gaind
 * @version $Id: MessageDispatcherSessionEJBDelegate.java,v 1.3 2011/06/30 15:14:15 gsergeycvs Exp $
 */

public class MessageDispatcherSessionEJBDelegate 
{
  private MessageDispatcherSessionEJBLocal local;
  private MessageDispatcherSessionEJB remote;
  private boolean useLocalInterface;
  
  /**
   * Constructor
   * 
   * @param useLocalInterface
   * @throws javax.naming.NamingException
   * @throws javax.ejb.CreateException
   * @throws java.rmi.RemoteException
   */
  public MessageDispatcherSessionEJBDelegate(boolean useLocalInterface)
  throws NamingException, CreateException, RemoteException
  {
    super();
    this.useLocalInterface = useLocalInterface;
    this.init();
  }
  
  /**
   * Initializer
   * 
   * @throws javax.naming.NamingException
   * @throws javax.ejb.CreateException
   * @throws java.rmi.RemoteException
   */
  private void init() throws NamingException, CreateException, RemoteException
  {
    if (this.useLocalInterface) 
    {
        if (local == null) 
        {
            this.local = this.getMessageDispatcherSessionEJBLocal();
        }        
    }
    else 
    {
        if (remote == null) 
        {
            this.remote = this.getMessageDispatcherSessionEJB();
        }        
    }
    
        
    
  }
 /**
   * Returns a reference to the local interface of MessageDispatcherSessionEJB
   * 
   * Returns a null if not found
   * 
   * @return 
   * @throws javax.naming.NamingException
   * @throws javax.ejb.CreateException
   */
  private MessageDispatcherSessionEJBLocal getMessageDispatcherSessionEJBLocal() throws NamingException, CreateException
  {
      InitialContext context = new InitialContext();
      MessageDispatcherSessionEJBLocalHome home = (MessageDispatcherSessionEJBLocalHome)context.lookup("java:comp/env/ejb/local/MessageDispatcherSessionEJB");
      return (home != null)?(MessageDispatcherSessionEJBLocal) home.create():null;                    
  }


  /**
   * Returns a reference to the remote interface of MessageDispatcherSessionEJB
   * 
   * Returns a null if not found
   * 
   * @return 
   * @throws javax.naming.NamingException
   * @throws javax.ejb.CreateException
   */
  private MessageDispatcherSessionEJB getMessageDispatcherSessionEJB() throws NamingException, CreateException, RemoteException
  {
      InitialContext context = new InitialContext();
      MessageDispatcherSessionEJBHome home = (MessageDispatcherSessionEJBHome)PortableRemoteObject.narrow(context.lookup("java:comp/env/ejb/MessageDispatcherSessionEJB"), MessageDispatcherSessionEJBHome.class);
      return (home != null)?(MessageDispatcherSessionEJB) home.create():null;                    
  }

  /**
   * Dispatches the text message, as part of a new transaction
   * 
   * @param messageToken
   * @throws java.rmi.RemoteException
   */
  public void dispatchTextMessage(MessageToken messageToken) throws RemoteException
  {
    if (this.useLocalInterface) 
    {
        local.dispatchTextMessage(messageToken);
    }
    else
    {
      remote.dispatchTextMessage(messageToken);
    }
  }
  

  /**
   * Updates the florist weight calculation audit report
   * 
   * @param reportID
   * @param newStatus
   * @param oldStatus
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristWeightCalcAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, RemoteException, Exception
  {
    if (this.useLocalInterface) 
    {
        local.updateFloristWeightCalcAuditReport(reportID, newStatus, oldStatus, updatedBy);
    }
    else
    {
        remote.updateFloristWeightCalcAuditReport(reportID, newStatus, oldStatus, updatedBy);
    }
  }
  

  /**
   * Updates florist audit report
   * 
   * @param reportID
   * @param newStatus
   * @param oldStatus
   * @param updatedBy
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public void updateFloristAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy)
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, RemoteException, Exception
  {
    if (this.useLocalInterface) 
    {
        local.updateFloristAuditReport(reportID, newStatus, oldStatus, updatedBy);
    }
    else
    {
        remote.updateFloristAuditReport(reportID, newStatus, oldStatus, updatedBy);
    }
  }


}
