package com.ftd.applications.lmd.ejb;

import com.ftd.eventhandling.eventmanager.EventManagerBean;
import com.ftd.eventhandling.eventmanager.ResourceNameRegister;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.net.URL;

import javax.ejb.MessageDrivenContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;
import javax.xml.transform.stream.StreamSource;

public class EventManagerMDB extends EventManagerBean
{
    /**
       * Set the associated message-driven context. The container calls this method after the instance creation.
       * The enterprise Bean instance should store the reference to the context object in an instance variable.
       * This method is called with no transaction context.
       *
       * @param ctx A MessageDrivenContext interface for the instance.
       * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
       */
    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.messageDrivenContext = ctx;
        InitialContext initContext = null;

        try
        {
            logger = new Logger("com.ftd.eventhandling.eventmanager.EventManager");
            initContext = new InitialContext();
            Context myenv = (Context) initContext.lookup("");
            logger.debug("Begin Registering Component");
            eventsLogDataSourceName = "jdbc/AQDS";
            ResourceNameRegister.register("Events Log Data Source Name", eventsLogDataSourceName);
            logger.debug("Events Log Data Source Name :: " + eventsLogDataSourceName);
            eventsLogDataSource = (DataSource) myenv.lookup(eventsLogDataSourceName);
            String eventsSchemaName = "event.xsd";
            ResourceNameRegister.register("Events Schema Name", eventsSchemaName);    

            schema = DOMUtil.getSchema(new StreamSource(ResourceUtil.getInstance().getResourceFileAsStream(eventsSchemaName)));
            logger.debug("Created events schema :: " + eventsSchemaName );
            logger.debug("End Registering Component");

        } catch (Exception ex)
        {
            logger.error(ex);
        } finally
        {
            try
            {
                initContext.close();
            } catch (Exception ex)
            {
                logger.error(ex);
            } finally
            {
            }
        }

    }
}
