package com.ftd.applications.lmd.ejb;
import java.io.IOException;
import java.sql.SQLException;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;
import com.ftd.osp.utilities.vo.MessageToken;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public interface MessageDispatcherSessionEJB extends EJBObject
{

  public void dispatchTextMessage(MessageToken messageToken) throws RemoteException;

  public void updateFloristWeightCalcAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy) 
    throws SQLException, IOException, ParserConfigurationException, SAXException , TransformerException, RemoteException, Exception;

  public void updateFloristAuditReport(String reportID, String newStatus, String oldStatus, String updatedBy) 
    throws RemoteException, IOException, Exception, SQLException, ParserConfigurationException, TransformerException , SAXException;

}
