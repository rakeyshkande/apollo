package com.ftd.applications.lmd.ejb;

import java.sql.Connection;
import java.util.Date;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.applications.lmd.dao.FloristDAO;
import com.ftd.applications.lmd.util.DataRequestHelper;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author skatam
 *
 */
public class FloristFutureSuspendMDB implements MessageDrivenBean, MessageListener 
{
	private static final long serialVersionUID = 1L;
	private Logger logger = new Logger("com.ftd.applications.lmd.ejb.FloristFutureSuspendMDB");
	//private MessageDrivenContext _context;
	
	@Override
	public void onMessage(Message message) 
	{
		try 
		{
			String floristId = ((TextMessage) message).getText();
			logger.debug("FloristFutureSuspendMDB received message at "+new Date());
			logger.debug("Message(FloristId) : "+floristId);
			this.processFloristSuspends(floristId);
		} 
		catch (JMSException e) 
		{
			logger.error(e);
		}      
        
	}
	
	private void processFloristSuspends(String floristId)
	{
		Connection con = null;
		try 
		{
			con = DataRequestHelper.getConnection();
			new FloristDAO().processFloristSuspend(con, floristId);
		} catch (Exception e) 
		{
			logger.error(e);
		}
		finally
		{
			DataRequestHelper.closeConnection(con);
		}
	}
	
	public void ejbCreate() {
    }
	
	@Override
	public void ejbRemove() throws EJBException 
	{
	}
	
	@Override
	public void setMessageDrivenContext(MessageDrivenContext context)
			throws EJBException 
	{
		//_context = context;
	}
	
}
