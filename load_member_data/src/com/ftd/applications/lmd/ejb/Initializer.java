package com.ftd.applications.lmd.ejb;
import java.util.Hashtable;

/**
 *
 * @author Anshu Gaind
 * @version $Id: Initializer.java,v 1.2 2006/05/26 15:49:21 mkruger Exp $
 */

public class Initializer 
{
  private static Initializer INITIALIZER = new Initializer();
  private static Hashtable stateMap = new Hashtable();
  
  /**
   * Private Constructor
   */
  private Initializer()
  {
  }
  
  /**
   * Returns a reference to the singleton
   * 
   * @return 
   */
  public static Initializer getInstance()
  {
    return INITIALIZER;
  }
  
  /**
   * Marks the object as initialized
   * 
   * @param objectName
   */
  public void initialize(String objectName)
  {
    if (! stateMap.containsKey(objectName)) 
    {
        stateMap.put(objectName, "INITIALIZED");
    }
  }

  /**
   * Inidicates whether or not a given object has already been marked as initialized
   * 
   * @param objectName
   * @return 
   */
  public boolean isInitialized(String objectName)
  {
    if (stateMap.containsKey(objectName)) 
    {
        return true;
    }
    return false;
    
  }
}