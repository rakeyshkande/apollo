package com.ftd.applications.lmd.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import com.ftd.applications.lmd.vo.*;
import com.ftd.osp.utilities.plugins.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This is the XAO which handles the conversion of the florist maintenance
 * object structure to the XML which the user interface layer will use.
 *
 * @author Brian Munter
 */

public class MaintenanceXAO 
{
    private static final String BASIC_DATE = "MM/dd/yy";
    private static final String TIME_DATE = "MM/dd/yy hh:mm a";
    private static final String YEAR_DATE = "MM/dd/yyyy";
    private Logger logger = new Logger("com.ftd.applications.lmd.dao.MaintenanceXAO");


    public MaintenanceXAO()
    {
    }

    /**
     * This method is the primary method used to convert the florist maintenace 
     * value object structure into XML.
     *   
     * @param florist FloristVO
     * @return maintenanceDocument Document
     * @throws java.lang.Exception
     */
    public Document generateXML(FloristVO florist) throws Exception
    {
        Document floristDocument = DOMUtil.getDefaultDocument();
        Element tagElement = null;
      
        // Create florist root
        Element floristElement = floristDocument.createElement("floristData");
        floristDocument.appendChild(floristElement);
        
        // Add general florist data to document
        floristElement.appendChild(floristDocument.importNode(generateGeneralDataXML(florist), true));
        
        // Add associated florists to the document
        Element associatedFloristsElement = floristDocument.createElement("associatedFloristList");
        floristElement.appendChild(associatedFloristsElement);
            
        if(florist.getAssociatedFloristList() != null && florist.getAssociatedFloristList().size() > 0)
        {
            AssociatedFloristVO associatedFlorist = null;
            Iterator associatedFloristIterator = florist.getAssociatedFloristList().iterator();
                
            while(associatedFloristIterator.hasNext())
            {
                associatedFlorist = (AssociatedFloristVO) associatedFloristIterator.next();
                associatedFloristsElement.appendChild(floristDocument.importNode(generateAssociatedFloristXML(associatedFlorist), true));
            }
        }
        
        // Add codified products to the document
        Element codifiedProductsElement = floristDocument.createElement("codifiedProductList");
        floristElement.appendChild(codifiedProductsElement);
            
        if(florist.getCodifiedProductMap() != null && florist.getCodifiedProductMap().size() > 0)
        {
            CodifiedProductVO codifiedProduct = null;
            Iterator codifiedProductIterator = florist.getCodifiedProductMap().values().iterator();
                
            while(codifiedProductIterator.hasNext())
            {
                codifiedProduct = (CodifiedProductVO) codifiedProductIterator.next();
                codifiedProductsElement.appendChild(floristDocument.importNode(generateCodifiedProductXML(codifiedProduct), true));
            }
        }

        // Add florist blocks to the document
        Element floristBlocksElement = floristDocument.createElement("floristBlockList");
        floristElement.appendChild(floristBlocksElement);

        if(florist.getFloristBlockList() != null && florist.getFloristBlockList().size() > 0)
        {
            FloristBlockVO floristBlockVO = null;
            Iterator floristBlockIterator = florist.getFloristBlockList().iterator();

            while(floristBlockIterator.hasNext())
            {
                floristBlockVO = (FloristBlockVO) floristBlockIterator.next();
                floristBlocksElement.appendChild(floristDocument.importNode(generateFloristBlocksXML(floristBlockVO), true));
            }
        }

		// Add florist single performance data to the document.
        Element performanceSingleElement = floristDocument.createElement("performanceSingleList");
        floristElement.appendChild(performanceSingleElement);
            
        if(florist.getPerformanceSingleList() != null && florist.getPerformanceSingleList().size() > 0)
        {
            FloristPerformanceVO singlePerformance = null;
            Iterator performanceIterator = florist.getPerformanceSingleList().iterator();
            
            while(performanceIterator.hasNext())
            {
                singlePerformance = (FloristPerformanceVO) performanceIterator.next();
                performanceSingleElement.appendChild(floristDocument.importNode(generateSinglePerformanceXML(singlePerformance), true));
            }
        }
        
        // Add florist associated performance data to the document.
        Element performanceAssociatedElement = floristDocument.createElement("performanceAssociatedList");
        floristElement.appendChild(performanceAssociatedElement);
            
        if(florist.getPerformanceSingleList() != null && florist.getPerformanceSingleList().size() > 0)
        {
            FloristPerformanceVO associatedPerformance = null;
            Iterator performanceIterator = florist.getPerformanceSingleList().iterator();
            
            while(performanceIterator.hasNext())
            {
                associatedPerformance = (FloristPerformanceVO) performanceIterator.next();
                performanceAssociatedElement.appendChild(floristDocument.importNode(generateAssociatedPerformanceXML(associatedPerformance), true));
            }
        }
		
        // Add postal codes to the document
        Element postalCodesElement = floristDocument.createElement("postalCodeList");
        floristElement.appendChild(postalCodesElement);
            
        if(florist.getPostalCodeMap() != null && florist.getPostalCodeMap().size() > 0)
        {
            PostalCodeVO postalCode = null;
            Iterator postalCodeIterator = florist.getPostalCodeMap().values().iterator();
                
            while(postalCodeIterator.hasNext())
            {
                postalCode = (PostalCodeVO) postalCodeIterator.next();
                postalCodesElement.appendChild(floristDocument.importNode(generatePostalCodeXML(postalCode), true));
            }
        }
        
        Element cityListElement = floristDocument.createElement("cityList");
        floristElement.appendChild(cityListElement);
        
        if (florist.getCitiesMap() != null && florist.getCitiesMap().size() > 0) {
        	CityVO city = null;
        	Iterator cityIterator = florist.getCitiesMap().values().iterator();
        	
        	while (cityIterator.hasNext()) {
        		city = (CityVO) cityIterator.next();
        		cityListElement.appendChild(floristDocument.importNode(generateCityXML(city), true));
        	}
        }
        
        // Add florist history to the document
        Element floristHistoryElement = floristDocument.createElement("floristHistoryList");
        floristElement.appendChild(floristHistoryElement);
            
        if(florist.getFloristHistoryList() != null && florist.getFloristHistoryList().size() > 0)
        {
            FloristHistoryVO floristHistory = null;
            Iterator floristHistoryIterator = florist.getFloristHistoryList().iterator();
                
            while(floristHistoryIterator.hasNext())
            {
                floristHistory = (FloristHistoryVO) floristHistoryIterator.next();
                floristHistoryElement.appendChild(floristDocument.importNode(generateFloristHistoryXML(floristHistory), true));
            }
        }
        
        // Add florist source info to the document
        Element floristSourceListElement = floristDocument.createElement("sourceList");
        floristElement.appendChild(floristSourceListElement);
            
        if(florist.getSourceFloristList() != null && florist.getSourceFloristList().size() > 0)
        {
            SourceFloristVO floristSource = null;
            Iterator floristSourceIterator = florist.getSourceFloristList().iterator();
                
            while(floristSourceIterator.hasNext())
            {
                floristSource = (SourceFloristVO) floristSourceIterator.next();
                floristSourceListElement.appendChild(floristDocument.importNode( generateSourceXML(floristSource), true));
            }
        }

        
     // Add florist halfdayfullday closure info to the document
        Element floristClosureListElement = floristDocument.createElement("closureList");
        floristElement.appendChild(floristClosureListElement);
        
        FloristClosureVO floristClosureVO=florist.getFloristClosureVO();
        if(floristClosureVO !=null && floristClosureVO.getFloristDayclosureLst() !=null && floristClosureVO.getFloristDayclosureLst().size()>0){
        	
        	Iterator floristClosureIterator = floristClosureVO.getFloristDayclosureLst().iterator();
        	
        	DayClosureVO dayClosureVO = null;
        	while(floristClosureIterator.hasNext())
            {
                dayClosureVO = (DayClosureVO) floristClosureIterator.next();
                floristClosureListElement.appendChild(floristDocument.importNode( generateClosureXML(dayClosureVO), true));
            }
        }
        
        
        
        // Add florist halfdayfullday closure info to the document
        Element floristClosureOverrideListElement = floristDocument.createElement("closureOverrideList");
        floristElement.appendChild(floristClosureOverrideListElement);
        
        
        if(floristClosureVO !=null && floristClosureVO.getClosureOverrideVOLst() !=null && floristClosureVO.getClosureOverrideVOLst().size()>0){
        	
        	Iterator floristClosureIterator = floristClosureVO.getClosureOverrideVOLst().iterator();
        	
        	ClosureOverrideVO dayClosureVO = null;
        	while(floristClosureIterator.hasNext())
            {
                dayClosureVO = (ClosureOverrideVO) floristClosureIterator.next();
                floristClosureOverrideListElement.appendChild(floristDocument.importNode( generateClosureOverrideXML(dayClosureVO), true));
            }
        }
        
        
        
        return floristDocument;
    }
    
    private Element generateGeneralDataXML(FloristVO florist) throws ParserConfigurationException
    {
        Document generalDataDocument = DOMUtil.getDefaultDocument();
        Element generalDataElement = generalDataDocument.createElement("generalData");
        
        generalDataElement.appendChild(this.createTextNode("memberNumber", florist.getMemberNumber(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("lockedFlag", florist.getLockedFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("lockedByUser", florist.getLockedByUser(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("name", florist.getName(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("address", florist.getAddress(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("city", florist.getCity(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("state", florist.getState(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("postalCode", florist.getPostalCode(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("phone", florist.getPhone(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("altPhone", florist.getAltPhone(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("fax", florist.getFax(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("contactName", florist.getContactName(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("status", florist.getStatus(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("postalCodeCount", florist.getPostalCodeCount(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("territory", florist.getTerritory(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("gotoFlag", florist.getFloristWeight().getGotoFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("sendingRank", florist.getSendingRank(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("lastUpdateDate", florist.getLastUpdateDate(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("lastUpdateUser", florist.getLastUpdateUser(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("ordersSent", florist.getOrdersSent(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("initialWeight", florist.getFloristWeight().getInitialWeight(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("finalWeight", florist.getFloristWeight().getFinalWeight(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("adjustedWeight", florist.getFloristWeight().getAdjustedWeight(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("adjustedWeightStartDate", this.formatDate(florist.getFloristWeight().getAdjustedWeightStartDate(), BASIC_DATE), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("adjustedWeightEndDate", this.formatDate(florist.getFloristWeight().getAdjustedWeightEndDate(), BASIC_DATE), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("mercuryMachine", florist.getMercuryMachine(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("sundayDeliveryFlag", florist.getSundayDeliveryFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("emailAddress", florist.getEmailAddress(), generalDataDocument));        
        generalDataElement.appendChild(this.createTextNode("closureOverrideFlag", florist.getFloristClosureVO().getClosureOverrideFlag(), generalDataDocument));
        
        generalDataElement.appendChild(this.createTextNode("mercurySuspend", florist.getFloristWeight().getMercurySuspendOverride(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("adjustedWeightPermFlag", florist.getFloristWeight().getAdjustedWeightPermFlag(), generalDataDocument));

        generalDataElement.appendChild(this.createTextNode("adjustedWeightPermFlag", florist.getFloristWeight().getAdjustedWeightPermFlag(), generalDataDocument));

        if (florist.getFloristSuspend() != null) {
            generalDataElement.appendChild(this.createTextNode("suspendType", florist.getFloristSuspend().getSuspendType(), generalDataDocument));
            generalDataElement.appendChild(this.createTextNode("suspendFlag", florist.getFloristSuspend().getSuspendFlag(), generalDataDocument));
            generalDataElement.appendChild(this.createTextNode("suspendEndDate", this.formatDate(florist.getFloristSuspend().getSuspendEndDate(), TIME_DATE), generalDataDocument));
            generalDataElement.appendChild(this.createTextNode("suspendStartDate", this.formatDate(florist.getFloristSuspend().getSuspendStartDate(), TIME_DATE), generalDataDocument));
        }

        // Build a Status object for display on the tabs
        String statusString = buildStatusString(florist);
        generalDataElement.appendChild(this.createTextNode("statusText", statusString, generalDataDocument));


        if(florist.getMinimumOrderAmount() != null)
        {
            generalDataElement.appendChild(this.createTextNode("minOrder", new BigDecimal(florist.getMinimumOrderAmount().doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString(), generalDataDocument));
        }
        
        generalDataElement.appendChild(this.createTextNode("vendorFlag", florist.getVendorFlag(), generalDataDocument));
        
        return generalDataElement;
    }

    private String buildStatusString(FloristVO florist)
    {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");

        if (florist.getStatus() != null && florist.getStatus().equals("Blocked"))
        {
            List floristBlocks = florist.getFloristBlockList();
            for (int i=0; i < floristBlocks.size();i++)
            {
                FloristBlockVO floristBlock = (FloristBlockVO) floristBlocks.get(i);
                if (i != 0)
                {
                    sb.append(", ");
                }
                if (floristBlock.getBlockType().equals("O"))
                {
                    sb.append("Opt Out - ");
                }
                else
                {
                    sb.append("Blocked - ");
                }
                sb.append(floristBlock.getBlockUser());
                sb.append(" ");
                sb.append(sdf.format(floristBlock.getBlockStartDate()));
                // Opt out just has a start date
                if (!floristBlock.getBlockType().equals("O"))
                {
                    sb.append("-");
                    sb.append(sdf.format(floristBlock.getBlockEndDate()));
                }
            }
        }
        else
        {
            sb.append(florist.getStatus());
        }

        logger.debug("Florist Status = " + sb.toString());
        return sb.toString();
    }
    
    private Element generateAssociatedFloristXML(AssociatedFloristVO associatedFlorist) throws ParserConfigurationException
    {
        Document associatedFloristDocument = DOMUtil.getDefaultDocument();
        Element associatedFloristElement = associatedFloristDocument.createElement("associatedFlorist");
        
        associatedFloristElement.appendChild(this.createTextNode("memberNumber", associatedFlorist.getMemberNumber(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("name", associatedFlorist.getName(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("mercuryFlag", associatedFlorist.getMercuryFlag(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("phone", this.formatPhoneNumber(associatedFlorist.getPhone()), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("postalCode", associatedFlorist.getPostalCode(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("state", associatedFlorist.getState(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("status", associatedFlorist.getStatus(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("sundayFlag", associatedFlorist.getSundayFlag(), associatedFloristDocument));
        associatedFloristElement.appendChild(this.createTextNode("zipCount", associatedFlorist.getPostalCodeCount(), associatedFloristDocument));
        
        return associatedFloristElement;
    }
    
	private Element generateSinglePerformanceXML(FloristPerformanceVO performance) throws ParserConfigurationException
    {
        Document performanceDocument = DOMUtil.getDefaultDocument();
        Element performanceElement = performanceDocument.createElement("FloristSinglePerformance");
        
		
	    performanceElement.appendChild(this.createTextNode("ordersSent", performance.getOSent(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("yearIndex", performance.getNum(), performanceDocument));
	    performanceElement.appendChild(this.createTextNode("orderSales", performance.getOSales(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("orderFilled", performance.getOFilled(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentFilled", performance.getPFilled(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("sales", performance.getPSales(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("orderForward", performance.getOForward(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentForward", performance.getPForward(), performanceDocument));
		performanceElement.appendChild(this.createTextNode("orderReject", performance.getOReject(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentReject", performance.getPReject(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("orderCancel", performance.getOCancel(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentCancel", performance.getPCancel(), performanceDocument));
        
        return performanceElement;
    }
    
    private Element generateAssociatedPerformanceXML(FloristPerformanceVO performance) throws ParserConfigurationException
    {
        Document performanceDocument = DOMUtil.getDefaultDocument();
        Element performanceElement = performanceDocument.createElement("FloristAssociatedPerformance");
        
		
	    performanceElement.appendChild(this.createTextNode("ordersSent", performance.getOSent(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("yearIndex", performance.getNum(), performanceDocument));
	    performanceElement.appendChild(this.createTextNode("orderSales", performance.getOSales(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("orderFilled", performance.getOFilled(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentFilled", performance.getPFilled(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("sales", performance.getPSales(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("orderForward", performance.getOForward(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentForward", performance.getPForward(), performanceDocument));
		performanceElement.appendChild(this.createTextNode("orderReject", performance.getOReject(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentReject", performance.getPReject(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("orderCancel", performance.getOCancel(), performanceDocument));
        performanceElement.appendChild(this.createTextNode("percentCancel", performance.getPCancel(), performanceDocument));
        
        return performanceElement;
    }
	
    private Element generateCodifiedProductXML(CodifiedProductVO codifiedProduct) throws ParserConfigurationException
    {
        Document codifiedProductDocument = DOMUtil.getDefaultDocument();
        Element codifiedProductElement = codifiedProductDocument.createElement("codifiedProduct");
        
        codifiedProductElement.appendChild(this.createTextNode("id", codifiedProduct.getId(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("name", codifiedProduct.getName(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("blockedFlag", codifiedProduct.getBlockedFlag(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(codifiedProduct.getBlockStartDate(), BASIC_DATE), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(codifiedProduct.getBlockEndDate(), YEAR_DATE), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("category", codifiedProduct.getCategory(), codifiedProductDocument));
        
        return codifiedProductElement;
    }

    private Element generateFloristBlocksXML(FloristBlockVO floristBlockVO) throws ParserConfigurationException
    {
        Document floristBlockDocument = DOMUtil.getDefaultDocument();
        Element floristBlockElement = floristBlockDocument.createElement("floristBlock");

        floristBlockElement.appendChild(this.createTextNode("blockType", floristBlockVO.getBlockType(), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockFlag", floristBlockVO.getBlockFlag(), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockUser", floristBlockVO.getBlockUser(), floristBlockDocument));

        floristBlockElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(floristBlockVO.getBlockStartDate(), BASIC_DATE), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(floristBlockVO.getBlockEndDate(), BASIC_DATE), floristBlockDocument));


        return floristBlockElement;
    }

    private Element generatePostalCodeXML(PostalCodeVO postalCode) throws ParserConfigurationException
    {
        Document postalCodeDocument = DOMUtil.getDefaultDocument();
        Element postalCodeElement = postalCodeDocument.createElement("postalCode");
        
        postalCodeElement.appendChild(this.createTextNode("postalCode", postalCode.getPostalCode(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("city", postalCode.getCity(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("cutoffTime", postalCode.getCutoffTime(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("floristCount", postalCode.getFloristCount(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("blockedFlag", postalCode.getBlockedFlag(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(postalCode.getBlockStartDate(), YEAR_DATE), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(postalCode.getBlockEndDate(), YEAR_DATE), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("deleteFlag", postalCode.getDeleteFlag(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("savedFlag", postalCode.getSavedFlag(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("assignedStatus", postalCode.getAssignedStatus(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("assignmentType", postalCode.getAssignmentType(), postalCodeDocument));
        
        return postalCodeElement;
    }
    
    private Element generateCityXML(CityVO city) throws ParserConfigurationException
    {
        Document cityDocument = DOMUtil.getDefaultDocument();
        Element cityElement = cityDocument.createElement("city");
        
        cityElement.appendChild(this.createTextNode("floristId", city.getFloristId(), cityDocument));
        cityElement.appendChild(this.createTextNode("floristName", city.getFloristName(), cityDocument));
        cityElement.appendChild(this.createTextNode("city", city.getFloristCity(), cityDocument));
        cityElement.appendChild(this.createTextNode("state", city.getFloristState(), cityDocument));
        cityElement.appendChild(this.createTextNode("floristType", city.getFloristType(), cityDocument));
        cityElement.appendChild(this.createTextNode("floristCount", city.getFloristCount(), cityDocument));
        if (city.getBlockStartDate() != null) {
        	cityElement.appendChild(this.createTextNode("blockedFlag", "Y", cityDocument));
        } else {
        	cityElement.appendChild(this.createTextNode("blockedFlag", "N", cityDocument));
        }
        cityElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(city.getBlockStartDate(), BASIC_DATE), cityDocument));
        cityElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(city.getBlockEndDate(), YEAR_DATE), cityDocument));
        cityElement.appendChild(this.createTextNode("sort1", city.getSort1(), cityDocument));
        cityElement.appendChild(this.createTextNode("sort2", city.getSort2(), cityDocument));
        
        return cityElement;
    }
    
    private Element generateFloristHistoryXML(FloristHistoryVO floristHistory) throws ParserConfigurationException
    {
        Document floristHistoryDocument = DOMUtil.getDefaultDocument();
        Element floristHistoryElement = floristHistoryDocument.createElement("floristHistory");
        
        floristHistoryElement.appendChild(this.createTextNode("comments", floristHistory.getComments(), floristHistoryDocument));
        floristHistoryElement.appendChild(this.createTextNode("commentDate", floristHistory.getCommentDate(), floristHistoryDocument));
        floristHistoryElement.appendChild(this.createTextNode("dispositionDescription", floristHistory.getCommentType(), floristHistoryDocument));
        floristHistoryElement.appendChild(this.createTextNode("managerFlag", floristHistory.getManagerFlag(), floristHistoryDocument));
        floristHistoryElement.appendChild(this.createTextNode("userId", floristHistory.getUserId(), floristHistoryDocument));
        floristHistoryElement.appendChild(this.createTextNode("newCommentFlag", floristHistory.getNewCommentFlag(), floristHistoryDocument));
        
        return floristHistoryElement;
    }
    
    private Element generateSourceXML(SourceFloristVO sourceFloristVO)  throws ParserConfigurationException
    {
        Document sourceDocument = DOMUtil.getDefaultDocument();
        Element sourceElement = sourceDocument.createElement("source");
        
        sourceElement.appendChild(this.createTextNode("code", sourceFloristVO.getSourceCode(), sourceDocument));
        sourceElement.appendChild(this.createTextNode("name", sourceFloristVO.getDescription(), sourceDocument));
        sourceElement.appendChild(this.createTextNode("priority", String.valueOf(sourceFloristVO.getPriority()), sourceDocument));
        
        return sourceElement;
    }
    
    
    private Element generateClosureXML(DayClosureVO dayClosureVO)  throws ParserConfigurationException
    {
        Document sourceDocument = DOMUtil.getDefaultDocument();
        Element sourceElement = sourceDocument.createElement("closure");
        
        sourceElement.appendChild(this.createTextNode("day", dayClosureVO.getDay(), sourceDocument));
        sourceElement.appendChild(this.createTextNode("jdestatus", dayClosureVO.getJdeStatus(), sourceDocument));
        
        return sourceElement;
    }
    
    private Element generateClosureOverrideXML(ClosureOverrideVO dayClosureVO)  throws ParserConfigurationException
    {
        Document sourceDocument = DOMUtil.getDefaultDocument();
        Element sourceElement = sourceDocument.createElement("closureOverride");
        
        sourceElement.appendChild(this.createTextNode("overridedate", dayClosureVO.getOverrideDate(), sourceDocument));
        sourceElement.appendChild(this.createTextNode("createdon", dayClosureVO.getCreatedOn(), sourceDocument));
        
        return sourceElement;
    }
    
    
    
    
    
    
    private Element createTextNode(String elementName, Object elementValue, Document document)
    {
        Element tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue.toString()));
        }

        return tagElement;
    }
    
    public static void main(String[] args)
    {
        MaintenanceXAO maintenanceXAO = new MaintenanceXAO();
    }
    
    private String formatDate(Date date, String dateFormat)
    {
        String formattedDate = "";
        
        if(date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            formattedDate = sdf.format(date);
        }
        
        return formattedDate;
    }
    
    private String formatPhoneNumber(String phoneNumber)
    {
        //if number is not null
        if(phoneNumber != null)
        {
            //if phone number is not already formatted with dashes
            if(phoneNumber.indexOf("-") < 0)
            {
                //if length of phone number is 10 (area code + number)
                if(phoneNumber.length() == 10)
                {   
                    phoneNumber = phoneNumber.substring(0,3) + "-" + phoneNumber.substring(3,6) + "-" + phoneNumber.substring(6);
                }//end if length = 10
            }//end if formatted
        }//end if null
        
        return phoneNumber;
    
    }//end method
}
