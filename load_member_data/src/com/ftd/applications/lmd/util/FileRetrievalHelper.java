package com.ftd.applications.lmd.util;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


import org.w3c.dom.NamedNodeMap;

import org.xml.sax.SAXException;

/**
 * @author Anshu Gaind
 */
public class FileRetrievalHelper 
{
  private Node ftpLocationNode;
  private boolean isLoggedIn;
  private FTPClient ftpClient;
  
  private static Logger logger  = new Logger("com.ftd.applications.lmd.util.FileRetrievalHelper");
  private static String ftpLocationsFileName = "ftp-locations.xml";
  private static Document ftpLocationsDoc;
  private static String LOCKFILE = "LOCKFILE.LOCKFILE";
  
  // Secure Configuration 
  private static final String SECURE_CONFIG_CONTEXT = "load_member_data";
  private static final String LMD_CONFIG_CONTEXT = "LOAD_MEMBER_DATA_CONFIG";
  private static final String FTP_USER = "_FtpUser";
  private static final String FTP_PSWD = "_FtpPswd";
  private static final String FTP_SERVER = "_server";
  private static final String FTP_DIR = "_dir";
  
  
  /**
   * Constructor
   */
   public FileRetrievalHelper()
    throws IOException, SAXException, ParserConfigurationException
   {
     init();
   }
  
  /**
   * Loads the XML document
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  private void init()
    throws IOException, SAXException, ParserConfigurationException
  {      
      URL url = ResourceUtil.getInstance().getResource(ftpLocationsFileName);
      if (url != null)
      {
        File ftpInfoFile = new File(new URLDecoder().decode(url.getFile()));
        this.ftpLocationsDoc = (Document)DOMUtil.getDocument(ftpInfoFile);
      }
      else
      {
        throw new IOException(ftpLocationsFileName +" file could not be found.");
      }                  
  }
  
  
  
  /**
   * Logs in to the FTP Location, changes directory to the target directory, 
   * and sets the transfer type
   * 
   * @param ftpLocation
   * @throws java.io.IOException
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.transform.TransformerException
   */
  public void login(String ftpLocation)
    throws IOException, FTPException, ParserConfigurationException , SAXException, TransformerException, SQLException, Exception
  {
    String server = null;
    String targetDirectory, transferType, userName, password, saveToLocation;

    this.ftpLocationNode = (Node) DOMUtil.selectSingleNode(this.ftpLocationsDoc, "/ftp-locations/ftp-location[@id='" + ftpLocation + "']");
    if (ftpLocationNode == null) 
    {
      throw new IOException("Configuration for "+ ftpLocation + " could not be found in " + ftpLocationsFileName);          
    }
    
    NamedNodeMap nnm = ftpLocationNode.getAttributes();
    transferType = nnm.getNamedItem("transfer-type").getNodeValue();

    // Obtain Secure username and password
    userName = ConfigurationUtil.getInstance().
      getSecureProperty(SECURE_CONFIG_CONTEXT, ftpLocation + FTP_USER);
    password = ConfigurationUtil.getInstance().
      getSecureProperty(SECURE_CONFIG_CONTEXT, ftpLocation + FTP_PSWD);
    server = ConfigurationUtil.getInstance().
      getFrpGlobalParm(LMD_CONFIG_CONTEXT, ftpLocation + FTP_SERVER);
    targetDirectory = ConfigurationUtil.getInstance().
      getFrpGlobalParm(LMD_CONFIG_CONTEXT, ftpLocation + FTP_DIR);
    
    logger.debug("Opening FTP connection to " + server + " as user " + userName );
    this.ftpClient = new FTPClient(server);
    
    this.ftpClient.login(userName, password);
    
    this.ftpClient.setConnectMode(FTPConnectMode.PASV);
    
    if (targetDirectory != null || (! targetDirectory.equals(""))) 
    {
        logger.debug("Changing directory to " + targetDirectory);
        this.ftpClient.chdir(targetDirectory);
    }
    if (transferType != null && transferType.equals("ASCII")) 
    {
      logger.debug("Setting transfer type as ASCII");
      this.ftpClient.setType(FTPTransferType.ASCII);                  
    } else 
    {
      logger.debug("Setting transfer type as BINARY");
      this.ftpClient.setType(FTPTransferType.BINARY);        
    }
    
    // set state
    this.isLoggedIn = true;
    
  }
  
  /**
   * Logs out from the FTP Location
   * 
   * Throws an exception if not logged in.
   * 
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws java.io.IOException
   */
  public void logout()
    throws FTPException, IOException
  {
    if (this.isLoggedIn) 
    {
      this.ftpClient.quit();
    }
    else
    {
      throw new FTPException("Login Required");
    }
    
  }
  
  /**
   * Indicates whether or not an FTP session has been established
   * 
   * @return 
   */
  public boolean isLoggedIn()
  {
    return this.isLoggedIn;
  }
  
  
  /**
   * Indicates whether or not a lock has been established.
   * 
   * Throws an exception if not logged in.
   * 
   * @return 
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws java.io.IOException
   */
  public boolean isLocked()
    throws FTPException, IOException
  {
    if (this.isLoggedIn) 
    {
      if (existsOnFTPServer(LOCKFILE) ) 
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      throw new FTPException("Login Required");
    }
  }
  
  
  /**
   * Creates a lock file in the target directory. Returns false if it finds
   * an existing lock file
   * 
   * Throws an exception if not logged in.
   * 
   * @return 
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws java.io.IOException
   */
  public boolean lock()
    throws FTPException, IOException
  {
      // process only if the lock file does not exist on the FTP server
      if (! isLocked() ) 
      {          
        String lockFilePath = System.getProperty("java.io.tmpdir") + LOCKFILE;
        File lockFile = new File(lockFilePath);
        if (! lockFile.exists()) 
        {
            FileOutputStream fos = new FileOutputStream(lockFile);
            fos.write(1);
            fos.close();
        }
        
        FileInputStream fis = new FileInputStream(lockFile);
        this.ftpClient.put(fis, LOCKFILE);
        fis.close();
        return true;
      } 
      else
      {
         return false;
      }
        
  }
  
  
  
  /**
   * Retrieves the file from the ftp server based on the configuration in the
   * configuration document.
   * 
   * Throws an exception if not logged in.
   * 
   * @param remoteFiles
   * @throws java.io.IOException
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.transform.TransformerException
   */
  public void getFileFromFTPServer(List remoteFiles)
    throws IOException, FTPException, ParserConfigurationException , SAXException, TransformerException
  {
    if (this.isLoggedIn()) 
    {
      String server = null;
      String targetDirectory, transferType, userName, password, saveToLocation;
  
      NamedNodeMap nnm = this.ftpLocationNode.getAttributes();
      saveToLocation = nnm.getNamedItem("save-to-location").getNodeValue();
      
      String fileName;
      
      for (Iterator iter = remoteFiles.iterator(); iter.hasNext() ; ) 
      {
        fileName = (String)iter.next();
        logger.debug("Downloading file " + fileName + " to " + saveToLocation);
        this.ftpClient.get(saveToLocation + File.separator + fileName, fileName);
      }
    }
    else
    {
      throw new FTPException("Login Required");
    }
  }
  

  /**
   * Removes the lock file in the target directory. Returns false if it cannot find
   * an existing lock file
   * 
   * Throws an exception if not logged in.
   * 
   * @return 
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws java.io.IOException
   */
  public boolean unlock()
    throws IOException, FTPException
  {
      // process only if the lock file exists on the FTP server
      if (isLocked()) 
      {
        this.ftpClient.delete(LOCKFILE);
        return true;
      }    
      else 
      {
        return false;
      }
  }
  
  /**
   * List current directory's contents as an array of strings of filenames.
   *  
   * Throws an exception if not logged in.
   * 
   * @return 
   * @throws java.io.IOException
   * @throws com.enterprisedt.net.ftp.FTPException
   */
  public String[] list()
    throws IOException, FTPException
  {
    if (this.isLoggedIn()) 
    {
      return this.ftpClient.dir();
    }
    else
    {
      throw new FTPException("Login Required");
    }    
  }
  

  /**
   * List a directory's contents as an array of strings of filenames.
   * 
   * Throws an exception if not logged in.
   * 
   * @param dirName
   * @return 
   * @throws java.io.IOException
   * @throws com.enterprisedt.net.ftp.FTPException
   */
  public String[] list(String dirName)
    throws IOException, FTPException
  {
    if (this.isLoggedIn()) 
    {
      return this.ftpClient.dir(dirName);
    }
    else
    {
      throw new FTPException("Login Required");
    }
  }

  /**
   * Deletes the specified list of files on the FTP server location
   * 
   * Throws an exception if not logged in.
   * 
   * @param remoteFiles
   * @throws java.io.IOException
   * @throws com.enterprisedt.net.ftp.FTPException
   */
  public void deleteOnFTPServer(List remoteFiles)
    throws IOException, FTPException
  {
    if (this.isLoggedIn()) 
    {
      for (Iterator iter = remoteFiles.iterator(); iter.hasNext() ;) 
      {
        this.ftpClient.delete((String)iter.next());
      }
    }  
    else
    {
      throw new FTPException("Login Required");
    }
    
  }
  
  /**
   * Returns the file as an InputStream, from the location specified in the
   * configuration document
   * 
   * @param ftpLocation
   * @param fileName
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws XPathExpressionException 
   */
  public FileReader getFile(String ftpLocation, String fileName)
    throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException 
  {
    Node ftpLocationNode = (Node) DOMUtil.selectSingleNode(this.ftpLocationsDoc, "/ftp-locations/ftp-location[@id='" + ftpLocation + "']");
    if (ftpLocationNode == null) 
    {
      throw new IOException("Configuration for "+ ftpLocation + " could not be found in " + ftpLocationsFileName);          
    }
    NamedNodeMap nnm = ftpLocationNode.getAttributes();
    String saveToLocation = nnm.getNamedItem("save-to-location").getNodeValue();
    FileReader fr = new FileReader(saveToLocation + File.separator + fileName);
    
    return fr;
  }
  
  /**
   * Archives the file to an archival directory, in the location specified in 
   * the configuration document.
   * 
   * @param ftpLocation
   * @param fileNames
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws XPathExpressionException 
   */
  public void archiveFile(String ftpLocation, List fileNames)
   throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException 
  {    
    Node ftpLocationNode = (Node) DOMUtil.selectSingleNode(this.ftpLocationsDoc, "/ftp-locations/ftp-location[@id='" + ftpLocation + "']");
    if (ftpLocationNode == null) 
    {
      throw new IOException("Configuration for "+ ftpLocation + " could not be found in " + ftpLocationsFileName);          
    }
    NamedNodeMap nnm = ftpLocationNode.getAttributes();
    String fileArchiveLocation = nnm.getNamedItem("file-archive-location").getNodeValue();
    String fromLocation = nnm.getNamedItem("save-to-location").getNodeValue();
    
    GregorianCalendar calendar = new GregorianCalendar();
    
    String newDir =  calendar.get(Calendar.YEAR) 
                      + "_" 
                      + calendar.get(Calendar.MONTH) 
                      + "_" 
                      + calendar.get(Calendar.DATE) 
                      + "_" 
                      + calendar.get(Calendar.HOUR_OF_DAY) 
                      + "_" 
                      + calendar.get(Calendar.MINUTE) 
                      + "_" 
                      + calendar.get(Calendar.SECOND);
    
    // create an archival directory
    File archiveDirectory = new File(fileArchiveLocation + File.separator + newDir);
    archiveDirectory.mkdir();
    String archiveDirectoryLocation = archiveDirectory.toString() + File.separator;
    
    String fileName, targetFileLocation, archiveFileLocation;
    
    for (Iterator iter = fileNames.iterator(); iter.hasNext() ; ) 
    {
      fileName = (String) iter.next();
      targetFileLocation = fromLocation + File.separator + fileName;
      archiveFileLocation = archiveDirectoryLocation + fileName;
      
      logger.debug("Archiving " + targetFileLocation + " to " + archiveDirectory);
      
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(targetFileLocation));
      
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(archiveDirectoryLocation + fileName));
      
      try 
      {
        for (int i = bis.read(); i != -1; i = bis.read()) 
        {
          bos.write(i);
        }
        
        bos.flush();
                      
      } finally 
      {
        if (bis != null) 
        {
            bis.close();
            // remove the file from its original location
            new File(targetFileLocation).delete();
        }
        
        if (bos != null) 
        {
            bos.close();
        }
      }
      
    }
  }

 

  /**
   * Indicates whether or not a file exists on the FTP Server.
   * 
   * @param fileName
   * @param ftpClient
   * @return 
   */
  private boolean existsOnFTPServer(String fileName)
    throws FTPException, IOException
  {
    String[] files = this.ftpClient.dir();
    
    for (int i = 0; i < files.length; i++) 
    {
      if (files[i].equals(fileName)) 
      {
          return true;
      }
    }
    
    return false;
  }
  
  
//  public static void main(String[] args)
//  {
//    try 
//    {
//    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
//    Object array[] = configUtil.getProperties("filenames.xml").values().toArray();
//    List remoteFiles = new ArrayList();
//    
//    remoteFiles.add(configUtil.getProperty("filenames.xml", "city-file"));
//    
//    FileRetrievalHelper fileRetrievalHelper = new FileRetrievalHelper();
//    fileRetrievalHelper.login("as400");
//    
//    if (fileRetrievalHelper.lock()) 
//    {
//      fileRetrievalHelper.getFileFromFTPServer( remoteFiles);
//      fileRetrievalHelper.unlock();  
//    }
//    else 
//    {
//      logger.error("Files are locked on the FTP server. Skip processing...");
//    }
//    
//    fileRetrievalHelper.logout();
//      
//    } catch (Exception ex) 
//    {
//      ex.printStackTrace();
//    } finally 
//    {
//    }
//    
//  }
  
  
}//~
