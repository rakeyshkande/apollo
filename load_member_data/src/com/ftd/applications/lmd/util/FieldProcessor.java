package com.ftd.applications.lmd.util;
import com.ftd.applications.lmd.vo.ParseFieldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;


/**
 * Takes in a ParseFieldVO object and the source type(member, link, codified, 
 * etc) and returns a string value from the file utilizing the start position 
 * and field length.
 * 
 * @author Anshu Gaind
 */

public class FieldProcessor 
{
  private static Logger logger  = new Logger("com.ftd.applications.lmd.util.FieldProcessor");
    private static final int PAD_RIGHT = 0; 
    private static final int PAD_LEFT = 1; 
  /**
   * Returns a null if either of the input parameters are null
   * 
   * @param fieldVO
   * @param record
   * @return 
   */
  public static String getFieldValue(ParseFieldVO fieldVO, String record)
  {
    String fieldValue = null;
    
    if ( fieldVO != null && record != null ) 
    {
      if (fieldVO.getStartPosition()!= null 
            && (!fieldVO.getStartPosition().equals("")) 
            && fieldVO.getLength()!= null 
            && (!fieldVO.getLength().equals(""))  ) 
      {      
        int startPosition = Integer.parseInt(fieldVO.getStartPosition()) - 1;
        int length = Integer.parseInt(fieldVO.getLength());
        
        if (record.length() >= (startPosition + length)) 
        {
          try 
          {
            fieldValue = record.substring( startPosition,  startPosition + length);
          } catch (Throwable t) 
          {
            logger.error(record);
            logger.error(t);
          } 
            
        }// end if
        else 
        {
            if (record.length() >= startPosition) 
            {
              try 
              {
                fieldValue = record.substring( startPosition);
              } catch (Throwable t) 
              {
                logger.error(record);
                logger.error(t);
              } 
            }
        }
      }// end if
      
    }// end if
        
    return fieldValue;
  }
  
  /**
   * Decodes the string based on the character mappings specified in the 
   * character mappings document.
   * 
   * @param str
   * @return 
   * @throws javax.xml.transform.TransformerException
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public static String decodeCharacterMapping(String str)
    throws TransformerException, IOException, SAXException, ParserConfigurationException 
  {
    StringBuffer sb = new StringBuffer(str);
    char c = sb.charAt(sb.length() - 1);
    String replacement = ConfigurationUtil.getInstance().getProperty("character_mapping_config.xml", String.valueOf(c));
    
    if (replacement == null) 
    {
        return str;
    }
    if (replacement.equals("")) 
    {
        return str;
    }
    
    if (replacement.startsWith("-")) 
    {
        sb.replace(sb.length()-1, sb.length(), replacement.substring(replacement.indexOf("-") + 1, replacement.length()));
        sb.insert(0,"-");
    }
    else
    {
        sb.replace(sb.length()-1, sb.length(), replacement);      
    }

    return sb.toString();
  }
  
    /**
     * This method pads extra chars either left or right for the size specified
     * @param value current value
     * @param padSide specifies whether to pad left or right
     * @param padChar  the char to pad
     */
    public static String pad(String value,int padSide, String padChar, int size)
    {
     //null check
     if(value == null)
        value = "";
       
     String padded = value;       
    
     if(value.length() > size) {
        if(padSide == PAD_RIGHT) {
          padded = value.substring(0,size);
        }
        else {
          padded = value.substring(padded.length()-size);
        }
     }
     else
     {
       for(int i=value.length();i<size;i++)
       {
         if(padSide == PAD_LEFT)
            padded = padChar + padded;
         else
            padded = padded + padChar;
       }//end for loop
     }//end else less then max size
    
     return padded;
    }  
  
//  public static void main(String[] args)
//  {
//    try 
//    {
//      java.math.BigDecimal bd = new java.math.BigDecimal( new java.math.BigInteger(FieldProcessor.decodeCharacterMapping("1234")));
//      System.out.println(bd.floatValue());
//    } catch (Exception ex) 
//    {
//      ex.printStackTrace();
//    } finally 
//    {
//    }
//    
//  }
  
  
}//~
