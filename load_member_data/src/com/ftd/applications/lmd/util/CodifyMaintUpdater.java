package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.dao.CodifyMaintDAO;
import com.ftd.applications.lmd.vo.CodificationVO;
import com.ftd.applications.lmd.vo.ProductVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import java.sql.Connection;
import com.ftd.applications.lmd.vo.CodificationMaintenanceVO;
import com.ftd.applications.lmd.vo.CodificationMasterVO;
import com.ftd.applications.lmd.vo.CodificationProductsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import com.ftd.security.SecurityManager;

public class CodifyMaintUpdater extends HttpServlet 
{
    private DataRequest dataRequest;
    private Logger logger;    
    private final static String EMPTY_STRING = "";
 
    public CodifyMaintUpdater(DataRequest dataRequest)
    {
        this.dataRequest = dataRequest;
        this.logger = new Logger("com.ftd.applications.lmd.util.FloristUpdater");   
    }
    public CodificationMaintenanceVO updateFloristFromRequest(Connection conn,CodificationMaintenanceVO maintVO, HttpServletRequest request) throws Exception
    {
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            maintVO.setLastUpdateUser(securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
        }
        else
        {
            maintVO.setLastUpdateUser(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER));
        }

        // Codification Master
        String codifiedId = null;
        CodificationMasterVO masterVO = null;
        String codifyFlagFromRequest = null;
        
        int counter = 1;
        
        //loop through all the codication master rows in the request
        boolean endOfList = false;
        while(!endOfList)
        {
            //get codification row
            codifiedId = request.getParameter("cCode" + counter);
            codifyFlagFromRequest = request.getParameter("cAllFlorist" + counter);                
            
            String deleteFlag = request.getParameter("cRemove" + counter);
            if(deleteFlag == null)
            {
                deleteFlag = "N";
            }

            
            if(codifiedId != null){

                //get the codification master record from the map retrieved from the DB
                masterVO = (CodificationMasterVO) maintVO.getCodificationMasterMap().get(codifiedId);
            
                if(masterVO == null)
                {
                    //item was added new in memory and is not in the database yet
                    masterVO = new CodificationMasterVO();
                    masterVO.setCodificationID(codifiedId);                   
                    
                    //if codify flag is Y then set the codify changed flag so that codification is done
                    if(codifyFlagFromRequest.equals("Y"))
                    {
                        masterVO.setCodifyAllFloristFlagChanged(true);
                    }
                    
                    //add vo to the map    
                    if(!(deleteFlag.equalsIgnoreCase("Y") || deleteFlag.equalsIgnoreCase("on"))){
                        maintVO.getCodificationMasterMap().put(masterVO.getCodificationID(), masterVO);
                    }
                }else
                {

                    //check if the codify flag has change
                    if(!codifyFlagFromRequest.equals(masterVO.getCodifyAllFloristFlag()))
                    {
                        masterVO.setCodifyAllFloristFlagChanged(true);
                    }

                }
                    
                    //populate with values from request                    
                    masterVO.setPreviousCodificationID(request.getParameter("previousCodification" + counter));
                    masterVO.setCategory_id(request.getParameter("cCategory" + counter));
                    masterVO.setCodifyAllFloristFlag(request.getParameter("cAllFlorist" + counter));                
                    masterVO.setDescription(request.getParameter("cDescription" + counter));
                    masterVO.setCodificationRequired(request.getParameter("cRequired" + counter));
                    masterVO.setLoadFromFileFlag(request.getParameter("cLoad" + counter));                
                    
                    if(deleteFlag != null && deleteFlag.equals("on"))
                    { 
                        masterVO.setDeleteFlag("Y");
                    }
                    else
                    {
                        masterVO.setDeleteFlag("N");
                    }  
                    String globalFlag = request.getParameter("cGlobal" + counter);
                    if(globalFlag != null && globalFlag.equals("on"))
                    { 
                        masterVO.setGlobalUnblockFlag("Y");
                    }
                    else
                    {
                        masterVO.setGlobalUnblockFlag("N");
                    }               
    
                    //increment counter                
                    counter++;
            }
            else
            {
                //stop parsing through request, we got to the end
                endOfList = true;
            }
        }//end while loop
        
        
        
        //loop through all the codication product rows in the request
        counter = 1;
        endOfList = false;
        String productId = null;
        CodificationProductsVO productVO = null;
        while(!endOfList)
        {
            //get product row
            productId = request.getParameter("pSku" + counter);
            if(productId != null)
            {
                //get the product record from the map retrieved from the DB
                productVO = (CodificationProductsVO) maintVO.getCodificationProductMap().get(productId);            
           
                //if codification master was not found in the DB map
                if(productVO == null)
                {
                    //item was added new in memory and is not in the database yet
                    productVO = new CodificationProductsVO();
                    productVO.setProductID(productId);                   
                    
                    //add vo to the map    
                    maintVO.getCodificationProductMap().put(productVO.getProductID(), productVO);
                }
                
                //populate with values from request
                productVO.setCheckOEflag(request.getParameter("pendValid" + counter));
                productVO.setCodificationID(request.getParameter("pCode" + counter));                                
                //productVO.setDescription(request.getParameter("codifiedunblockdate" + counter));
                productVO.setFlagSequence(getIntegerValue(request.getParameter("pNovator" + counter)));
                //productVO.setProductName(request.getParameter("codifiedunblockdate" + counter));
                //productVO.setStatus(request.getParameter("codifiedunblockdate" + counter));
                String deleteFlag  =request.getParameter("deleteproduct" + counter);
                if(deleteFlag != null && deleteFlag.equals("on"))
                { 
                    productVO.setDeleteFlag("Y");
                }
                else
                {
                    productVO.setDeleteFlag("N");
                }           
                
                //if product name or codification description is null then get it from DB
                CodifyMaintDAO dao = new CodifyMaintDAO(conn);
                if(productVO.getProductName() == null)
                {
                    ProductVO productMaster = dao.getProduct(productVO.getProductID());
                    if(productMaster != null)
                    {
                        productVO.setProductName(productMaster.getName());
                    }                   
                }
                if(productVO.getDescription() == null)
                {
                    CodificationVO codification = dao.getCodification(productVO.getCodificationID());
                    if(codification != null)
                    {
                        productVO.setDescription(codification.getDescription());
                    }
                }

                //increment counter                
                counter++;
            }
            else
            {
                //stop parsing through request, we got to the end
                endOfList = true;
            }
        }//end while loop        
        
        return maintVO;
        
    }//end proc
        
        private Integer getIntegerValue(String value)
        {
            return value == null || value.trim().length() == 0 ? null : new Integer(value);
        }
        private Long getLongValue(String value)
        {
            return value == null || value.trim().length() == 0 ? null : new Long(value);
        }
        
    }
     
