package com.ftd.applications.lmd.util;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.applications.lmd.vo.ParseFieldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class will manage the reads from the XML parsing file for general 
 * florist data and also load the parsing logic from the database table 
 * CODIFIED_MASTER.  The XML file should only be read initially when the 
 * application server starts and whenever the file is changed (based on 
 * timestamp).  The codified data will be read from database each time 
 * requested.  The object will follow the singleton pattern and the instance 
 * will represent the data contained in the florist_parse_config.xml file 
 * and CODIFIED_MASTER table.  The data from the reads will be stored in Maps
 * within this class.
 * 
 * @author Anshu Gaind
 * @version $Id: ParseConfig.java,v 1.3 2011/06/30 15:14:17 gsergeycvs Exp $
 */
public class ParseConfig 
{
   private static Logger logger  = new Logger("com.ftd.applications.lmd.util.ParseConfig");
   private static ParseConfig PARSECONFIG = new ParseConfig();
   private static boolean SINGLETON_INITIALIZED;
   private static final String configFile = "file-parsing-logic.xml";
   private static Map membersFileConfig;
   private static Map citiesFileConfig;
   private static Map linkFileConfig;
   private static Map floristResumeSuspendDataConfig;
   private static Map floristRevenueDataConfig;
   private static Map _LSTReciprocityDataConfigMap;
   
   private static File file;
   private static long fileLastModified;
   
  /**
   * The private Constructor
   **/
  private ParseConfig()
  {
  }


 /**
  * The static initializer for the  util. It ensures that
  * at a given time there is only a single instance
  *
  * @return the  util
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
  **/
  public static ParseConfig getInstance()  
   throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
    if (! SINGLETON_INITIALIZED ){
      initConfig();
    }
    return PARSECONFIG;
  }


  /**
   * Initialize the configuration
   *
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   **/
  private synchronized static void initConfig()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  {
    if (! SINGLETON_INITIALIZED)
    {
      URL url = ResourceUtil.getInstance().getResource(configFile);
      if (url != null)
      {
        file = new File(url.getFile());
        fileLastModified = file.lastModified();
        
        // load the config information
        Document doc =  DOMUtil.getDocument(ResourceUtil.getInstance().getResourceAsStream(configFile));        
        
        membersFileConfig = loadConfig(doc, "//members-file/field");
        citiesFileConfig = loadConfig(doc,"//cities-file/field" );
        linkFileConfig = loadConfig(doc, "//link-file/field");
        floristResumeSuspendDataConfig = loadConfig(doc, "//florist-suspend-resume-data-file/field");
        floristRevenueDataConfig = loadConfig(doc, "//florist-revenue-data-file/field");
        _LSTReciprocityDataConfigMap = loadConfig(doc, "//LST-reciprocity-data-file/field");
        
        SINGLETON_INITIALIZED = true;
      }
      else
      {
        throw new IOException("The configuration file was not found.");
      }
    }

  }

  /**
   * Loads configuration data
   * 
   * @param doc
   * @param XPath
   * @return 
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  private static Map loadConfig(Document doc, String XPath) throws XPathExpressionException
  {
    HashMap map = new HashMap();
    
    NodeList nodeList = DOMUtil.selectNodes(doc, XPath);    
    Node node = null;
    ParseFieldVO fieldVO = null;

    if (nodeList != null) 
    {
      for (int i = 0; i < nodeList.getLength(); i++) 
      {
        node = (Node) nodeList.item(i);
        if (node != null) 
        {
          fieldVO = new ParseFieldVO();
          fieldVO.setName(DOMUtil.selectSingleNode(node,"name/text()").getNodeValue());
          fieldVO.setStartPosition(DOMUtil.selectSingleNode(node,"start-position/text()").getNodeValue());
          fieldVO.setLength(DOMUtil.selectSingleNode(node,"length/text()").getNodeValue());
          // add to the collection
          map.put(fieldVO.getName(), fieldVO);
        }      
      }      
    }
    return map;
  }



  /**
   * Load the codified master data from the codified_master table. This method
   * is executed each time files are processed, to ensure that the parsing logic
   * is uptodate.
   * 
   * The information is then loaded into a Map of ParseFieldVO objects.
   * 
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  private static Map loadCodifiedMasterData()
    throws SQLException, IOException, ParserConfigurationException, SAXException, Exception
  {
    Connection con = null;
    HashMap map = new HashMap();
    try 
    {
      logger.debug("BEGIN LOADING CODIFIED MASTER DATA...");
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      String useJDBCConnectionUtil =ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
      con = (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER"):DataSourceUtil.getInstance().getConnection("LOAD MEMBER");            
      dataRequest.setConnection(con);
      dataRequest.setStatementID("VIEW_CODIFIED_MASTER_FROM_FILE");
      CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
      
      String codificationID, filePosition, fileLength; 
      ParseFieldVO fieldVO;
      while(rs.next())
      {
          codificationID = (String)rs.getObject(1);
          filePosition = (rs.getObject(2) != null)?String.valueOf( ((BigDecimal)rs.getObject(2)).longValue() ):null;
          fileLength = (rs.getObject(3) != null)?String.valueOf( ((BigDecimal)rs.getObject(3)).longValue() ):null;
          fieldVO = new ParseFieldVO();
          fieldVO.setName(codificationID);
          fieldVO.setStartPosition(filePosition);
          fieldVO.setLength(fileLength);
          // add to the collection
          map.put(fieldVO.getName(), fieldVO);
      }  
      logger.debug("END LOADING CODIFIED MASTER DATA...");
      
    } finally 
    {
      if (con != null && (! con.isClosed() )) 
      {
          con.close();
      }
      
    }
    return map;
  }


  /**
   * Indicates whether or not the configuration is current
   * 
   * @return 
   */
  private boolean isConfigUpToDate()
  {
    return (this.fileLastModified == this.file.lastModified())?true:false; 
  }
  
  /**
   * Reloads the configuration file
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  private void reloadConfig()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  
  {
    this.SINGLETON_INITIALIZED = false;
    this.initConfig();
  }

  /**
   * Returns the members configuration map
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map getMembersFileConfigMap()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this.membersFileConfig;
  }
  
  /**
   * Returns the cities file configuration map
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map getCitiesFileConfigMap()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException 
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this.citiesFileConfig;
  }

  /**
   * Returns the link file configuration map
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map getLinkFileConfigMap()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this.linkFileConfig;
  }
  
  /**
   * Returns the codified master data configuration map. This data is loaded
   * from the codified_master table, each time this method is called. 
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
  public Map getCodifiedMasterConfigMap()
        throws IOException, SAXException, ParserConfigurationException , SQLException, Exception 
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this.loadCodifiedMasterData();   
  }
  
  /**
   * Returns the florist suspend resume data configuration file
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map getFloristSuspendResumeDataConfigMap()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this.floristResumeSuspendDataConfig;
  }
  
  /**
   *  Returns the florist revenue data configuration file
   *  
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map getFloristRevenueDataConfigMap()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this.floristRevenueDataConfig;
  }
  
  /**
   * Returns LST anf reciprocity data configuration file
   *  
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
 * @throws XPathExpressionException 
   * @throws oracle.xml.parser.v2.XSLException
   */
  public Map getLSTReciprocityDataConfigMap()
        throws IOException, SAXException, ParserConfigurationException, XPathExpressionException  
  {
    if (! this.isConfigUpToDate()) 
    {
        this.reloadConfig();
    }
    return this._LSTReciprocityDataConfigMap;
  }  
}//~
