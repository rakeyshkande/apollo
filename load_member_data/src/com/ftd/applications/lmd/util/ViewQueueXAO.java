package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.vo.*;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This is the XAO which handles the conversion of the florist maintenance
 * object structure to the XML which the user interface layer will use.
 *
 * @author Brian Munter
 */

public class ViewQueueXAO 
{
    private static final String BASIC_DATE = "MM/dd/yyyy";

    public void ViewQueueXAO()
    {
    }

    /**
     * This method is the primary method used to convert the florist maintenace 
     * value object structure into XML.
     *   
     * @param florist FloristVO
     * @return maintenanceDocument Document
     * @throws java.lang.Exception
     */
    public Document generateXML(ViewQueueVO viewQueue) throws Exception
    {
/*
  private FloristBlockVO floristBlock = new FloristBlockVO();
*/
        Document floristDocument = DOMUtil.getDefaultDocument();
        Element tagElement = null;
      
        // Create view queue root
        Element floristElement = floristDocument.createElement("viewQueueData");
        floristDocument.appendChild(floristElement);
        
        // Add general view queue data to document
        floristElement.appendChild(floristDocument.importNode(generateGeneralDataXML(viewQueue), true));
        
        // Add associated florists to the document
        Element associatedFloristsElement = floristDocument.createElement("associatedFloristList");
        floristElement.appendChild(associatedFloristsElement);           
        
        // Add codified products to the document
        Element codifiedProductsElement = floristDocument.createElement("codifiedProductList");
        floristElement.appendChild(codifiedProductsElement);
            
        if(viewQueue.getCodifiedProductMap() != null && viewQueue.getCodifiedProductMap().size() > 0)
        {
            CodifiedProductVO codifiedProduct = null;
            Iterator codifiedProductIterator = viewQueue.getCodifiedProductMap().values().iterator();
                
            while(codifiedProductIterator.hasNext())
            {
                codifiedProduct = (CodifiedProductVO) codifiedProductIterator.next();
                codifiedProductsElement.appendChild(floristDocument.importNode(generateCodifiedProductXML(codifiedProduct), true));
            }
        }
        
        // Add postal codes to the document
        Element postalCodesElement = floristDocument.createElement("postalCodeList");
        floristElement.appendChild(postalCodesElement);
            
        if(viewQueue.getPostalCodeMap() != null && viewQueue.getPostalCodeMap().size() > 0)
        {
            PostalCodeVO postalCode = null;
            Iterator postalCodeIterator = viewQueue.getPostalCodeMap().values().iterator();
                
            while(postalCodeIterator.hasNext())
            {
                postalCode = (PostalCodeVO) postalCodeIterator.next();
                postalCodesElement.appendChild(floristDocument.importNode(generatePostalCodeXML(postalCode), true));
            }
        }

        // Add florist blocks to the document
        Element floristBlocksElement = floristDocument.createElement("floristBlockList");
        floristElement.appendChild(floristBlocksElement);

        if(viewQueue.getFloristBlockList() != null && viewQueue.getFloristBlockList().size() > 0)
        {
            FloristBlockVO floristBlockVO = null;
            Iterator floristBlockIterator = viewQueue.getFloristBlockList().iterator();

            while(floristBlockIterator.hasNext())
            {
                floristBlockVO = (FloristBlockVO) floristBlockIterator.next();
                floristBlocksElement.appendChild(floristDocument.importNode(generateFloristBlocksXML(floristBlockVO), true));
            }
        }

        return floristDocument;
    }
    
    private Element generateGeneralDataXML(ViewQueueVO viewQueue) throws ParserConfigurationException
    {
        Document generalDataDocument = DOMUtil.getDefaultDocument();
        Element generalDataElement = generalDataDocument.createElement("generalData");
 
        generalDataElement.appendChild(this.createTextNode("mercuryId", viewQueue.getMercuryId(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("floristNumber", viewQueue.getFloristNumber(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("mercuryNumber", viewQueue.getMercuryNumber(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("orderNumber", viewQueue.getOrderNumber(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("messageCount", viewQueue.getMessageCount(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("deliveryDate", this.formatDate(viewQueue.getDeliveryDate(), BASIC_DATE), generalDataDocument));
        //generalDataElement.appendChild(this.createTextNode("messageDate", this.formatDate(viewQueue.getMessageDate(), BASIC_DATE), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("messageDate", viewQueue.getMessageDate(), generalDataDocument));
		generalDataElement.appendChild(this.createTextNode("messageReason", viewQueue.getMessageReason(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("floristName", viewQueue.getFloristName(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("floristGotoFlag", viewQueue.getFloristGotoFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("floristStatus", viewQueue.getFloristStatus(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("floristCity", viewQueue.getFloristCity(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("floristState", viewQueue.getFloristState(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("productBlockFlag", viewQueue.getProductBlockFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("productUnblockDate", this.formatDate(viewQueue.getProductUnblockDate(), BASIC_DATE), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("productName", viewQueue.getProductName(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("productId", viewQueue.getProductId(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("productCodifiedId", viewQueue.getProductCodifiedId(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("deliveryAddress", viewQueue.getDeliveryAddress(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("deliveryZipCode", viewQueue.getDeliveryZipCode(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("zipBlockFlag", viewQueue.getZipBlockFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("zipUnblockDate", viewQueue.getZipUnblockDate(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("zipFloristCount", viewQueue.getZipFloristCount(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("lockedFlag", viewQueue.getLockedFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("lockedByUser", viewQueue.getLockedByUser(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("reasonComment", viewQueue.getReasonComment(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("defaultUnblockDate", viewQueue.getDefaultUnblockDate(), generalDataDocument));

        FloristSuspendVO florstSuspend = viewQueue.getFloristSuspend();
        generalDataElement.appendChild(this.createTextNode("suspendType", florstSuspend.getSuspendFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("suspendFlag", florstSuspend.getSuspendFlag(), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("suspendStartDate", this.formatDate(florstSuspend.getSuspendStartDate(), BASIC_DATE), generalDataDocument));
        generalDataElement.appendChild(this.createTextNode("suspendEndDate", this.formatDate(florstSuspend.getSuspendEndDate(), BASIC_DATE), generalDataDocument));

        return generalDataElement;
    }
    
    private Element generateFloristBlocksXML(FloristBlockVO floristBlockVO) throws ParserConfigurationException
    {
        Document floristBlockDocument = DOMUtil.getDefaultDocument();
        Element floristBlockElement = floristBlockDocument.createElement("floristBlock");

        floristBlockElement.appendChild(this.createTextNode("blockType", floristBlockVO.getBlockType(), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockFlag", floristBlockVO.getBlockFlag(), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockUser", floristBlockVO.getBlockUser(), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockReason", floristBlockVO.getBlockReason(), floristBlockDocument));

        floristBlockElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(floristBlockVO.getBlockStartDate(), BASIC_DATE), floristBlockDocument));
        floristBlockElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(floristBlockVO.getBlockEndDate(), BASIC_DATE), floristBlockDocument));

        return floristBlockElement;
    }


    
    private Element generateCodifiedProductXML(CodifiedProductVO codifiedProduct) throws ParserConfigurationException
    {
        Document codifiedProductDocument = DOMUtil.getDefaultDocument();
        Element codifiedProductElement = codifiedProductDocument.createElement("codifiedProduct");
        
        codifiedProductElement.appendChild(this.createTextNode("id", codifiedProduct.getId(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("name", codifiedProduct.getName(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("blockedFlag", codifiedProduct.getBlockedFlag(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(codifiedProduct.getBlockStartDate(), BASIC_DATE), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(codifiedProduct.getBlockEndDate(), BASIC_DATE), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("category", codifiedProduct.getCategory(), codifiedProductDocument));
        
        return codifiedProductElement;
    }
    
    private Element generatePostalCodeXML(PostalCodeVO postalCode) throws ParserConfigurationException
    {
        Document postalCodeDocument = DOMUtil.getDefaultDocument();
        Element postalCodeElement = postalCodeDocument.createElement("postalCode");
        
        postalCodeElement.appendChild(this.createTextNode("postalCode", postalCode.getPostalCode(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("city", postalCode.getCity(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("cutoffTime", postalCode.getCutoffTime(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("floristCount", postalCode.getFloristCount(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("blockedFlag", postalCode.getBlockedFlag(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("blockStartDate", this.formatDate(postalCode.getBlockStartDate(), BASIC_DATE), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("blockEndDate", this.formatDate(postalCode.getBlockEndDate(), BASIC_DATE), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("deleteFlag", postalCode.getDeleteFlag(), postalCodeDocument));
        postalCodeElement.appendChild(this.createTextNode("savedFlag", postalCode.getSavedFlag(), postalCodeDocument));
        
        return postalCodeElement;
    }
    
    private Element createTextNode(String elementName, Object elementValue, Document document)
    {
        Element tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue.toString()));
        }

        return tagElement;
    }
    

    
    private String formatDate(Date date, String dateFormat)
    {
        String formattedDate = "";
        
        if(date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            formattedDate = sdf.format(date);
        }
        
        return formattedDate;
    }
    
    private String formatPhoneNumber(String phoneNumber)
    {
        //if number is not null
        if(phoneNumber != null)
        {
            //if phone number is not already formatted with dashes
            if(phoneNumber.indexOf("-") < 0)
            {
                //if length of phone number is 10 (area code + number)
                if(phoneNumber.length() == 10)
                {   
                    phoneNumber = phoneNumber.substring(0,3) + "-" + phoneNumber.substring(3,6) + "-" + phoneNumber.substring(6);
                }//end if length = 10
            }//end if formatted
        }//end if null
        
        return phoneNumber;
    
    }//end method
    
    public static void main(String[] args)
    {
        Document doc = null;
    
        try{
            ViewQueueVO vo = new ViewQueueVO();
            vo.setMercuryNumber("111");
            ViewQueueXAO xao = new ViewQueueXAO();
            doc = (Document)xao.generateXML(vo);
        }
        catch(Exception e){ }
    }
    
}
