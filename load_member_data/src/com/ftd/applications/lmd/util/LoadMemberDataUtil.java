package com.ftd.applications.lmd.util;

import com.ftd.osp.framework.dispatcher.Dispatcher;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.StringWriter;

import javax.naming.InitialContext;

import org.jdom.CDATA;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * Manages the state of and access to an HTTP connection
 *
 * @author Anshu Gaind
 * @version $Id: LoadMemberDataUtil.java,v 1.2 2007/01/16 16:23:32 mcampbelcvs Exp $
 */

public class LoadMemberDataUtil 
{
  
  private Logger logger  = new Logger("com.ftd.applications.lmd.util.LoadMemberDataUtil");

    public LoadMemberDataUtil (){
    }

   /**
    * Sends a payload off into FTD JMS queue land
    * @param eventHandler 
    * @param eventName event name
    * @param payload xml document that will be put into the payload.
    * @throws com.ftd.amazon.api.AmazonException
    */
    
   public static void queueToEventsQueue(String contextName, String eventName, String delay, String payload, String pipeline) throws Exception
   { 
       try  {
         org.jdom.Element root = new Element("event");
         Namespace xsi = Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance");
         root.setAttribute("noNamespaceSchemaLocation","event.xsd",xsi);
         root.addContent(new Element("event-name").setText(eventName));
         root.addContent(new Element("context").setText(contextName));
         root.addContent(new Element("payload").setText(payload));

         XMLOutputter xml;
         Format format;
         StringWriter sw;
         org.jdom.Document doc = new org.jdom.Document(root);
         
         xml = new XMLOutputter();
         format = Format.getCompactFormat();
         format.setOmitDeclaration(true);
         format.setOmitEncoding(true);
         xml.setFormat(format);
         sw = new StringWriter();
         xml.output(doc,sw);
         
         MessageToken messageToken = new MessageToken();
         // name of the pipeline
         messageToken.setStatus(pipeline);
         messageToken.setMessage(sw.toString());
         messageToken.setProperty("CONTEXT", contextName, "java.lang.String");
         messageToken.setProperty("JMS_OracleDelay", delay, "long");
         //messageToken.setJMSCorrelationID(contextName + " " + eventName);
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
       } catch (Exception ex)  {
             throw new Exception("enqueueJmsMessage caught exception: " + ex);
       }  
   }
 
}