package com.ftd.applications.lmd.util;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;

/**
 * @author Anshu Gaind
 * @version $Id: FTPHelperTest.java,v 1.3 2011/06/30 15:14:17 gsergeycvs Exp $
 */
public class FTPHelperTest 
{
  private static Logger logger  = new Logger("com.ftd.applications.lmd.util.FileRetrievalHelper");
  
  /**
   * Retrieves the file from the ftp server based on the configuration in the
   * ftp-config.xml document.
   * 
   * @param remoteFiles
   * @throws java.io.IOException
   * @throws com.enterprisedt.net.ftp.FTPException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.transform.TransformerException
   */
  public static void getFileFromFTPServer(List remoteFiles)
    throws IOException, FTPException, ParserConfigurationException , SAXException, TransformerException
  {
    String server = null;
    String targetDirectory, transferType, userName, password, saveToLocation;
    FTPClient ftpClient = null;
    try 
    {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      server = (String) configUtil.getProperty("ftp-config.xml", "server");
      targetDirectory = (String) configUtil.getProperty("ftp-config.xml", "target-directory");
      transferType = (String) configUtil.getProperty("ftp-config.xml", "transfer-type");
      userName = (String) configUtil.getProperty("ftp-config.xml", "user-name");
      password = (String) configUtil.getProperty("ftp-config.xml", "password");
      saveToLocation = (String) configUtil.getProperty("ftp-config.xml", "save-to-location");
      
      logger.debug("Opening FTP connection to " + server + " as user " + userName );
      ftpClient = new FTPClient(server);
      
      ftpClient.login(userName, password);
            
      ftpClient.setConnectMode(FTPConnectMode.PASV);
      
      if (targetDirectory != null || (! targetDirectory.equals(""))) 
      {
          logger.debug("Changing directory to " + targetDirectory);
          ftpClient.chdir(targetDirectory);
      }
URL url = ResourceUtil.getInstance().getResource("LOCKFILE");      
FileInputStream fis = new FileInputStream(new File(new URLDecoder().decode(url.getFile()))); 
ftpClient.put(fis, "LOCKFILE");     
fis.close();
      if (transferType != null && transferType.equals("ASCII")) 
      {
        logger.debug("Setting transfer type as ASCII");
        ftpClient.setType(FTPTransferType.ASCII);                  
      } else 
      {
        logger.debug("Setting transfer type as BINARY");
        ftpClient.setType(FTPTransferType.BINARY);        
      }

      String fileName;
      
      for (Iterator iter = remoteFiles.iterator(); iter.hasNext() ; ) 
      {
        fileName = (String)iter.next();
        logger.debug("Downloading file " + fileName + " to " + saveToLocation);
        ftpClient.get(saveToLocation + File.separator + fileName, fileName);
      }

    } finally 
    {
      if (ftpClient != null) 
      {
        ftpClient.delete("LOCKFILE");
        ftpClient.quit();
        logger.debug("FTP connection to " + server + " closed");
      }
      
    }
  }
  
  /**
   * Returns the file as an InputStream, from the location specified in the
   * ftp-config.xml document
   * 
   * @param fileName
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public static FileReader getFile(String fileName)
    throws IOException, SAXException, ParserConfigurationException, TransformerException 
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String saveToLocation = (String) configUtil.getProperty("ftp-config.xml", "save-to-location");
    FileReader fr = new FileReader(saveToLocation + File.separator + fileName);
    
    return fr;
  }
  
  /**
   * Archives the file to an archival directory, in the location specified in 
   * the ftp-config.xml document.
   * 
   * @param fileNames
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public static void archiveFile(List fileNames)
   throws IOException, SAXException, ParserConfigurationException, TransformerException 
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String fileArchiveLocation = (String) configUtil.getProperty("ftp-config.xml", "file-archive-location");
    String fromLocation = (String) configUtil.getProperty("ftp-config.xml", "save-to-location");
    
    GregorianCalendar calendar = new GregorianCalendar();
    
    String newDir =  calendar.get(Calendar.YEAR) 
                      + "_" 
                      + calendar.get(Calendar.MONTH) 
                      + "_" 
                      + calendar.get(Calendar.DATE) 
                      + "_" 
                      + calendar.get(Calendar.HOUR_OF_DAY) 
                      + "_" 
                      + calendar.get(Calendar.MINUTE) 
                      + "_" 
                      + calendar.get(Calendar.SECOND);
    
    // create an archival directory
    File archiveDirectory = new File(fileArchiveLocation + File.separator + newDir);
    archiveDirectory.mkdir();
    String archiveDirectoryLocation = archiveDirectory.toString() + File.separator;
    
    String fileName, targetFileLocation, archiveFileLocation;
    
    for (Iterator iter = fileNames.iterator(); iter.hasNext() ; ) 
    {
      fileName = (String) iter.next();
      targetFileLocation = fromLocation + File.separator + fileName;
      archiveFileLocation = archiveDirectoryLocation + fileName;
      
      logger.debug("Archiving " + targetFileLocation + " to " + archiveDirectory);
      
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(targetFileLocation));
      
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(archiveDirectoryLocation + fileName));
      
      try 
      {
        for (int i = bis.read(); i != -1; i = bis.read()) 
        {
          bos.write(i);
        }
        
        bos.flush();
                      
      } finally 
      {
        if (bis != null) 
        {
            bis.close();
            // remove the file from its original location
            new File(targetFileLocation).delete();
        }
        
        if (bos != null) 
        {
            bos.close();
        }
      }
      
    }
    

    
  }


public static void main(String[] args)
{
  try 
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    Object array[] = configUtil.getProperties("filenames.xml").values().toArray();
    List remoteFiles = new ArrayList();
    
    remoteFiles.add(configUtil.getProperty("filenames.xml", "city-file"));
    
    com.ftd.applications.lmd.util.FTPHelperTest.getFileFromFTPServer(remoteFiles);

  } catch (Exception ex) 
  {
    ex.printStackTrace();
  } finally 
  {
  }
  
}
  
}//~
