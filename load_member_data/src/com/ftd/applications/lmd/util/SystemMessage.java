package com.ftd.applications.lmd.util;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
/**
 * Sends system messages to the system messages table
 * 
 * @author Anshu Gaind
 * @version $Id: SystemMessage.java,v 1.2 2006/05/26 15:49:22 mkruger Exp $
 */

public class SystemMessage 
{
    public static final int LEVEL_DEBUG = SystemMessengerVO.LEVEL_DEBUG;
    public static final int LEVEL_PRODUCTION = SystemMessengerVO.LEVEL_PRODUCTION;

 /**
   * Logs a message in the system messages table
   * 
   * @param message
   * @param source
   * @param level
   * @param type
   * @param connection
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
  public static void send(String message
                          , String source
                          , int level
                          , String type
                          , Connection connection) 
      throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException
  {
      SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
      systemMessengerVO.setLevel(level);
      systemMessengerVO.setSource(source);
      systemMessengerVO.setType(type);
      systemMessengerVO.setMessage(message);
      SystemMessenger.getInstance().send(systemMessengerVO, connection, false);
  }

}