package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.vo.OrderResultVO;
import com.ftd.applications.lmd.vo.SearchResultVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.applications.lmd.constants.ConfigurationConstants;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletRequest;

/**
 * This class contains methods to pass in search parameters and obtain the results.
 *
 * @author Mehul Patel
 */

public class SearchManager 
{
    private Logger logger;

    /** 
     * Constructor
     */
    public SearchManager()
    {
       logger = new Logger("com.ftd.applications.lmd.util.SearchManager");
    }

    public ArrayList retrieveOrderMap(HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        dataRequest.reset();
        dataRequest.setStatementID("SEARCH_MERCURY_ORDERS");
        //SearchManager searchManager = new SearchManager();
if(request.getParameter("orderStartDate").toString().length() > 0)
{
		if((request.getParameter("order_type").toString()).equals("delivery"))
		{
			dataRequest.addInputParam("IN_DELIVERY_START_DATE", FieldUtils.formatStringToSQLDate(request.getParameter("orderStartDate")));
			if(request.getParameter("orderEndDate") != null && request.getParameter("orderEndDate").length() > 0)
				dataRequest.addInputParam("IN_DELIVERY_END_DATE", FieldUtils.formatStringToSQLDate(request.getParameter("orderEndDate")));
			else
			{
				java.util.Date today = new java.util.Date();
				String t = FieldUtils.formatUtilDateToString(today);
				dataRequest.addInputParam("IN_DELIVERY_END_DATE", FieldUtils.formatStringToSQLDate(t));
			}
			dataRequest.addInputParam("IN_ORDER_START_DATE", null);
			dataRequest.addInputParam("IN_ORDER_END_DATE", null);
		}
		else
		{
			dataRequest.addInputParam("IN_DELIVERY_START_DATE", null);
			dataRequest.addInputParam("IN_DELIVERY_END_DATE", null);
			
			dataRequest.addInputParam("IN_ORDER_START_DATE", FieldUtils.formatStringToSQLDate(request.getParameter("orderStartDate")));
			if(request.getParameter("orderEndDate") != null && request.getParameter("orderEndDate").length() > 0)
				dataRequest.addInputParam("IN_ORDER_END_DATE", FieldUtils.formatStringToSQLDate(request.getParameter("orderEndDate")));
			else
			{
				java.util.Date today = new java.util.Date();
				String t = FieldUtils.formatUtilDateToString(today);
				dataRequest.addInputParam("IN_ORDER_END_DATE", FieldUtils.formatStringToSQLDate(t));
			}
		}
}
else
{
	dataRequest.addInputParam("IN_ORDER_START_DATE", null);
	dataRequest.addInputParam("IN_ORDER_END_DATE", null);
	dataRequest.addInputParam("IN_DELIVERY_START_DATE", null);
	dataRequest.addInputParam("IN_DELIVERY_END_DATE", null);
}

		dataRequest.addInputParam("IN_MERCURY_ORDER_NUMBER", request.getParameter("orderMercury"));
        dataRequest.addInputParam("IN_RECIPIENT", request.getParameter("orderRecip"));
		dataRequest.addInputParam("IN_PRICE", request.getParameter("orderMercValue"));
        dataRequest.addInputParam("IN_PRODUCT_ID", request.getParameter("orderSku"));
		dataRequest.addInputParam("IN_RESPONSIBLE_FLORIST", request.getParameter("memnum"));

        CachedResultSet orderResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
     
        ArrayList sortedResults = new ArrayList();
        OrderResultVO orderResult = null;
        if(orderResultSet != null && orderResultSet.getRowCount() > 0)
        {            
            while(orderResultSet.next())
            {
                // Result list display fields
                orderResult = new OrderResultVO();
				
				orderResult.setMercNumber((String) orderResultSet.getObject(1));
				orderResult.setOrderNumber((String) orderResultSet.getObject(2));
				orderResult.setOrderDate((String)FieldUtils.formatSQLDateToString((Date)orderResultSet.getObject(3)) );
				orderResult.setDeliveryDate((String)FieldUtils.formatSQLDateToString((Date)orderResultSet.getObject(4)) );
				orderResult.setLastName((String) orderResultSet.getObject(5));
				orderResult.setOrderStatus((String) orderResultSet.getObject(6));
				if(orderResultSet.getObject(7) != null)
					orderResult.setMercValue(orderResultSet.getObject(7).toString());
				orderResult.setSku((String) orderResultSet.getObject(8));
                sortedResults.add(orderResult);
            }
        }
        
        return sortedResults;
    }


	/**
	* Retrieves the resultset containing Florists that were viewed recently.
	* 
	* @param request HttpServletRequest
	* @param dataRequest DataRequest
	*
	*/

	private String pullCurrentUser(String securityToken) throws Exception
    {
        String userId = null;
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            userId = securityManager.getUserInfo(securityToken).getUserID();
        }
        else
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
        
        return userId;
    }
	
    public ArrayList retrieveLastMap(HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        
        dataRequest.reset();
        dataRequest.setStatementID("GET_FLORIST_LAST");
        SearchManager searchManager = new SearchManager();
        
       // dataRequest.addInputParam("csrid", request.getParameter("uid"));
	   
	    dataRequest.addInputParam("csrid", pullCurrentUser(request.getParameter("securitytoken")));
        dataRequest.addInputParam("count", request.getParameter("count"));
        
        CachedResultSet searchResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
     
        // HashMap resultMap = new HashMap();
        ArrayList sortedResults = new ArrayList();
        SearchResultVO searchResult = null;
        if(searchResultSet != null && searchResultSet.getRowCount() > 0)
        {            
            while(searchResultSet.next())
            {
                // Result list display fields
                searchResult = new SearchResultVO();
                searchResult.setFlornum((String) searchResultSet.getObject(1));
                searchResult.setName((String) searchResultSet.getObject(2));
                searchResult.setZip((String) searchResultSet.getObject(3));
                searchResult.setState((String) searchResultSet.getObject(4));
                searchResult.setPhone((String) searchResultSet.getObject(5));
                searchResult.setSunday((String) searchResultSet.getObject(6));
                searchResult.setMercury((String) searchResultSet.getObject(7));
                searchResult.setStatus((String) searchResultSet.getObject(8));
                searchResult.setGo((String) searchResultSet.getObject(9));
				
				if(searchResultSet.getObject(10) != null)
					searchResult.setFinalWeight(((new Integer(((BigDecimal)searchResultSet.getObject(10)).intValue()))));
				else
					searchResult.setFinalWeight(null);
					
				if(searchResultSet.getObject(11) != null)
					searchResult.setAdjustedWeight(((new Integer(((BigDecimal)searchResultSet.getObject(11)).intValue()))));
				else
					searchResult.setAdjustedWeight(null);
					
				 searchResult.setCity((String) searchResultSet.getObject(12));
				
                sortedResults.add(searchResult);
            }
        }
        
        return sortedResults;
    }

	/**
	* Fetches parameters, builds a search query and executes it.
	* 
	* @param request HttpServletRequest
	* @param dataRequest DataRequest
	*
	*/
    public ArrayList retrieveResultMap(HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        // Load list of data
        
        dataRequest.reset();
        dataRequest.setStatementID("GET_FLORIST_SEARCH_RESULT");
        dataRequest.addInputParam("floristid", request.getParameter("sc_floristnum"));
        
        dataRequest.addInputParam("city", request.getParameter("sc_city"));
        if(request.getParameter("sc_state") != null && !request.getParameter("sc_state").equals("Select..."))
        {
            dataRequest.addInputParam("state", request.getParameter("sc_state"));
        }
        else
        {
            dataRequest.addInputParam("state", null);
        }
        dataRequest.addInputParam("zip_code", request.getParameter("sc_zipcode"));
        dataRequest.addInputParam("phone_number", request.getParameter("sc_phonenumber"));
        dataRequest.addInputParam("florist_name", request.getParameter("sc_shopname"));
        if(request.getParameter("sc_statuslist") != null && !request.getParameter("sc_statuslist").equals("Select..."))
        {
            dataRequest.addInputParam("status", request.getParameter("sc_statuslist"));
        }
        else
        {
            dataRequest.addInputParam("status", null);
        }
        
        dataRequest.addInputParam("product", request.getParameter("sc_product"));
        if(request.getParameter("sc_product") != null && request.getParameter("sc_product").length() > 0)
            dataRequest.addInputParam("product_search_type", request.getParameter("sc_productType"));
        else
            dataRequest.addInputParam("product_search_type", null);
    
        if(request.getParameter("sc_includeVendor") != null && request.getParameter("sc_includeVendor").length() > 0)
            dataRequest.addInputParam("vendor_flag", request.getParameter("sc_includeVendor"));
        else
            dataRequest.addInputParam("vendor_flag", "N");
    
        CachedResultSet searchResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        ArrayList sortedResults = new ArrayList();
        SearchResultVO searchResult = null;
        if(searchResultSet != null && searchResultSet.getRowCount() > 0)
        {            
            while(searchResultSet.next())
            {
                // Result list display fields
                searchResult = new SearchResultVO();
                searchResult.setFlornum((String) searchResultSet.getObject(1));
                searchResult.setName((String) searchResultSet.getObject(2));
                searchResult.setZip((String) searchResultSet.getObject(3));
                searchResult.setState((String) searchResultSet.getObject(4));
                searchResult.setPhone((String) searchResultSet.getObject(5));
                searchResult.setSunday((String) searchResultSet.getObject(6));
                searchResult.setMercury((String) searchResultSet.getObject(7));
                searchResult.setStatus((String) searchResultSet.getObject(8));
                searchResult.setGo((String) searchResultSet.getObject(9));
				
				if(searchResultSet.getObject(10) != null)
					searchResult.setFinalWeight(((new Integer(((BigDecimal)searchResultSet.getObject(10)).intValue()))));
				else
					searchResult.setFinalWeight(null);
					
				if(searchResultSet.getObject(11) != null)
					searchResult.setAdjustedWeight(((new Integer(((BigDecimal)searchResultSet.getObject(11)).intValue()))));
				else
					searchResult.setAdjustedWeight(null);
					
				searchResult.setCity((String) searchResultSet.getObject(12));

	            logger.debug(searchResult.getFlornum() + " " + searchResult.getStatus());
	            if (searchResult.getStatus() != null && searchResult.getStatus().equalsIgnoreCase("Blocked")) {
    	            DataRequest blockRequest = DataRequestHelper.getInstance().getDataRequest();
	                //blockRequest.setConnection(conn);
	                blockRequest.setStatementID("VIEW_FLORIST_BLOCKS");
	                blockRequest.addInputParam("IN_FLORIST_ID", searchResult.getFlornum());

	                DataAccessUtil blockAccessUtil = DataAccessUtil.getInstance();
    	            CachedResultSet cr = (CachedResultSet)blockAccessUtil.execute(blockRequest);

	                if (cr.next()) {
	                	String blockType = cr.getString("block_type");
	                	Date blockStartDate = cr.getDate("block_start_date");
	                	Date blockEndDate = cr.getDate("block_end_date");
	                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
    	                if (blockType != null) {
	                    	if (blockType.equalsIgnoreCase("O")) {
	                    		searchResult.setStatus("Opt Out - " + sdf.format(blockStartDate));
	                    	} else {
	                    		searchResult.setStatus("Blocked - " + sdf.format(blockStartDate) + "-" + sdf.format(blockEndDate));
	                    	}
	                    }
	                }
    	            logger.debug("New status: " + searchResult.getFlornum() + " " + searchResult.getStatus());
	                if(blockRequest !=null && blockRequest.getConnection() != null) {
                        blockRequest.getConnection().close();
                    }
	            }

                sortedResults.add(searchResult);
            }
        }
        return sortedResults;
    }

}
