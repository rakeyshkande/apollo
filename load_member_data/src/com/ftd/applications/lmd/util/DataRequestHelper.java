package com.ftd.applications.lmd.util;

import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.JDBCConnectionUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.applications.lmd.constants.ConfigurationConstants;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class is a singleton helper utility for generating DataRequest objects.
 *
 * @author Brian Munter
 */

public class DataRequestHelper 
{
    private static DataRequestHelper DATA_REQUEST_HELPER;

    /**
     * Returns an instance of DataRequestHelper.
     * 
     * @exception Exception
     */
    public static synchronized DataRequestHelper getInstance() throws Exception
    {
        if (DATA_REQUEST_HELPER == null)
        {
            DATA_REQUEST_HELPER = new DataRequestHelper();
            return DATA_REQUEST_HELPER;
        }
        else 
        {
            return DATA_REQUEST_HELPER;
        }
    }

    /**
     * Retrieves a DataRequest object based on connection properties specified 
     * in the Scrub configuration file.
     * 
     * @exception Exception
     */
    public DataRequest getDataRequest() throws Exception
    {
        String dataSourceName = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.DATASOURCE_NAME);
        // String dataSourceLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.DATASOURCE_LOCATION);
    
        Connection connection =  DataSourceUtil.getInstance().getConnection(dataSourceName);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        return dataRequest;
    }
    
    public static Connection getConnection() throws Exception
	{
		String useJDBCConnectionUtil =ConfigurationUtil.getInstance()
			.getProperty("load_member_config.xml", "USE_JDBCCONNECTION_UTIL");
        return (useJDBCConnectionUtil != null && useJDBCConnectionUtil.equals("Y"))?
        		JDBCConnectionUtil.getInstance().getConnection("LOAD MEMBER")
        		:DataSourceUtil.getInstance().getConnection("LOAD MEMBER");
	}
	
    public static void closeConnection(Connection con)
	{
		try 
		 {
			if (con != null && (! con.isClosed() )) { 
				con.close(); 
			}
		 }
		 catch (SQLException e) 
		 { }
	}
}