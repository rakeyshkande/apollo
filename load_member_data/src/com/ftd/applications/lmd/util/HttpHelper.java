package com.ftd.applications.lmd.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * Manages the state of and access to an HTTP connection
 * 
 * @author Anshu Gaind
 * @version $Id: HttpHelper.java,v 1.4 2011/06/30 15:14:17 gsergeycvs Exp $
 */

public class HttpHelper 
{
  private static String FLORIST_DATA_CONFIG_CONTEXT = "LOAD_MEMBER_DATA_CONFIG";
  private static HashMap httpLocations;
  
  private Logger logger  = new Logger("com.ftd.applications.lmd.util.HttpHelper");
  private Map requestParameters;
  
  /**
   * Constructor
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  public HttpHelper()
    throws Exception
  {
    if (this.httpLocations == null) 
    {
        this.init();
        this.requestParameters = new HashMap();
    }
  }

  /**
   * Loads the XML configuration document
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  private synchronized void init()
    throws Exception
  {      
    // double check
    if (this.httpLocations == null) 
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        
        String novator = cu.getFrpGlobalParm(FLORIST_DATA_CONFIG_CONTEXT,"http_location_novator");
        
        httpLocations = new HashMap();
        httpLocations.put("novator",novator);
    }
  }
  
  
  /**
   * Posts the parameters to the specified HTTP location
   * 
   * @param httpLocation
   * @param parameters
   * @return 
   * @throws java.io.IOException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public String doPost(String httpLocation, Map parameters)
    throws IOException 
  {
    String urlString, response;
    int result;
    
    urlString = (String) httpLocations.get(httpLocation);

    PostMethod post = new PostMethod(urlString);
    NameValuePair nvPair = null;
    NameValuePair[] nvPairArray = new NameValuePair[parameters.size()];
    int i = 0;
    String name, value;
    
    for(Iterator iter = parameters.keySet().iterator(); iter.hasNext();i++)
    {
      name = (String) iter.next();
      value = (String) parameters.get(name);
      nvPair = new NameValuePair(name, value);
      nvPairArray[i] = nvPair;
    }

    post.setRequestBody(nvPairArray);
    
    HttpClient httpclient = new HttpClient();
    // Execute request
    try {
        result = httpclient.executeMethod(post);
        response = post.getResponseBodyAsString();
    } finally {
        // Release current connection to the connection pool once you are done
        post.releaseConnection();
    }
    String message = "Http Response Code is " + result;
    logger.debug(message);
    
    if ( result != HttpURLConnection.HTTP_OK ) 
    {
      throw new IOException(message);
    }    
    
    return response;
  }
  
}
