package com.ftd.applications.lmd.util;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import com.ftd.security.SecurityManager;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * Servlet helper class 
 * 
 * @author Anshu Gaind
 */

public class ServletHelper 
{
    private static Logger logger = new Logger("com.ftd.applications.lmd.util.ServletHelper");

  /**
   * Indicates whether or not the security token is a valid.
   * It checks the validity of the security token.
   * 
   * 
   * @param context
   * @param securityToken
   * @return 
   * @throws com.ftd.security.exceptions.ExpiredIdentityException
   * @throws com.ftd.security.exceptions.ExpiredSessionException
   * @throws com.ftd.security.exceptions.InvalidSessionException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
    public static boolean isValidToken(String context, String securityToken) 
      throws ExpiredIdentityException, 
             ExpiredSessionException, 
             InvalidSessionException, 
             SAXException, 
             ParserConfigurationException,
             IOException,
             SQLException,
             TransformerException,
             Exception
             {

      
      if (ServletHelper.isSecurityCheckOverridden()) 
      {
          logger.error("Security check has been overridden.");
          return true;
      }
      
      if (securityToken == null || (securityToken.equals("")))  {
          logger.error("No Security Token found");
          return false;
      } 
      
      logger.debug("authenticating security token");
      com.ftd.security.SecurityManager securityManager = com.ftd.security.SecurityManager.getInstance();
      return securityManager.authenticateSecurityToken(context, getUnitID(), securityToken);
  }
  
  
  /**
   * Used as a helper method to isValidToken to handle the redirect for 
   * security exceptions through web applications.
   * 
   * 
   * @param context
   * @param securityToken
   * @return boolean
   * @throws com.ftd.security.exceptions.ExpiredIdentityException
   * @throws com.ftd.security.exceptions.ExpiredSessionException
   * @throws com.ftd.security.exceptions.InvalidSessionException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public static boolean isWebActionSecure(String context, String securityToken, HttpServletRequest request, HttpServletResponse response) 
      throws ExpiredIdentityException, 
             ExpiredSessionException, 
             InvalidSessionException, 
             SAXException, 
             ParserConfigurationException,
             IOException,
             SQLException,
             TransformerException,
             Exception
             {

        boolean isSecure = false;
      
        try
        {
            isSecure = ServletHelper.isValidToken(context, securityToken);

            if(!isSecure)
            {
                // Redirect to login page
                logger.info("** Login Error  **");
//                response.sendRedirect(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_LOCATION) + 
//                ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT) + 
//                ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + "&securitytoken=" +  
//                securityToken + "&context=" + context + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT));
                com.ftd.security.util.ServletHelper.redirectToLogin(request, response,
                securityToken, "context=" + context + "&applicationcontext=" + 
                ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT));
            }
        }
        catch(Exception e)
        {
            // Redirect to login page
            logger.info("** Login Expired **");
//            response.sendRedirect(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_LOCATION) + 
//            ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_ADMIN_REDIRECT) + 
//            ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + "&securitytoken=" +  
//            securityToken + "&context=" + context + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT));
            com.ftd.security.util.ServletHelper.redirectToLogin(request, response,
                securityToken, "context=" + context + "&applicationcontext=" + 
                ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT));
        }
        
        return isSecure;
  }


  /**
   * Looks up the unit ID required for authentication.
   * 
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
   public static String getUnitID() 
    throws IOException, SAXException, ParserConfigurationException, TransformerException 
   {
        return ConfigurationUtil.getInstance().getProperty
            ("load_member_config.xml", "SECURITY_MANAGER_UNIT_ID");               
   }


  /**
   * Returns a HashMap with context, securitytoken, and application context from request.
   * 
   * @param request
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public static HashMap getParameterMap(HttpServletRequest request)  
    throws IOException, SAXException, ParserConfigurationException, TransformerException 
  {

      HashMap parameters = new HashMap();
      String context, securityToken, adminAction;
      String webContextRoot = ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "WEB_CONTEXT_ROOT");
      
      if(isMultipartFormData(request)) {
          context = (String)request.getAttribute("context");
          securityToken = (String)request.getAttribute("securitytoken");       
          adminAction = (String)request.getAttribute("adminAction");       
      } else {
          context = request.getParameter("context");
          securityToken = request.getParameter("securitytoken");
          adminAction = request.getParameter("adminAction");       
      }
      
      parameters.put("context", context == null? "" : context);
      parameters.put("securitytoken", securityToken == null? "" : securityToken);
      parameters.put("web-context-root", webContextRoot == null? "" : webContextRoot);
      parameters.put("adminAction", adminAction == null? "" : adminAction);
      
      logger.debug("Setting context in parameter map: " + context);
      logger.debug("Setting securityToken in parameter map: " + securityToken);
      logger.debug("Setting web context root in parameter map: " + webContextRoot);
      logger.debug("Setting adminAction in parameter map: " + adminAction);
      
      return parameters;
  }


  /**
   * Returns true if request content type is multipart/form-data.
   * @param request HttpServlet request to be analyzed
   */
   public static boolean isMultipartFormData(HttpServletRequest request)
   {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader("CONTENT-TYPE");
        if(headerContentType != null && headerContentType.startsWith("multipart/form-data")) {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
   }

  /**
   * Returns a user id. It will return a null if no user information can be obtained
   * 
   * @param request
   * @return 
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.lang.Exception
   */
  public static String getUserID(HttpServletRequest request)
    throws SQLException, IOException, ParserConfigurationException, SAXException, Exception  
  {
    SecurityManager securityManager = SecurityManager.getInstance();
    
    String securityToken = request.getParameter("securitytoken");
    if (ServletHelper.isSecurityCheckOverridden())  {
        logger.error("Security check has been overridden.");
        return ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "DEFAULT_USER_ID");
    } 
    if (securityToken == null || (securityToken.equals("")))  {
        logger.error("No Security Token found");
        return ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "DEFAULT_USER_ID");
    }
    
    return ( securityManager.getUserInfo(securityToken) != null)?(securityManager.getUserInfo(securityToken)).getUserID():null;
  }
  
  /**
   * Indicates whether or not security check has been overridden
   * 
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
  public static boolean isSecurityCheckOverridden()
    throws   SAXException, 
             ParserConfigurationException,
             IOException,
             SQLException,
             TransformerException,
             Exception
  
  {
      
      return (ConfigurationUtil.getInstance().getProperty("load_member_config.xml", "OVERRIDE_SECURITY_CHECK").equals("Y"))?true:false; 
    
  }

  /**
   * Creates the XML string for the event manager
   * 
   * @param eventName
   * @param payload
   * @return 
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  public static String createDispatchEventTextMessage(String eventName, String payload)
    throws ParserConfigurationException, IOException, SAXException , TransformerException
  {
      Document messageDoc = DOMUtil.getDefaultDocument();
      Element event = (Element) messageDoc.createElement("event");
      event.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
      event.setAttribute("xsi:noNamespaceSchemaLocation", "event.xsd");
      messageDoc.appendChild(event);
      
      Element eventNameNode = (Element) messageDoc.createElement("event-name");
      eventNameNode.appendChild(messageDoc.createTextNode(eventName));
      event.appendChild(eventNameNode);
      
      Element contextNode = (Element) messageDoc.createElement("context");
      ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
      String context = configurationUtil.getProperty("load_member_config.xml", "CONTEXT_NAME");
      contextNode.appendChild(messageDoc.createTextNode(context));
      event.appendChild(contextNode);

      Element payloadNode =  (Element) messageDoc.createElement("payload"); 
      payloadNode.appendChild(messageDoc.createCDATASection(payload));
      event.appendChild(payloadNode);
      
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw, true); 
      try {
		DOMUtil.print(messageDoc, pw);
      } catch (Exception e) {
		e.printStackTrace();
	  }
      String xmlString = sw.toString();
      pw.close();
      logger.debug(xmlString);
      return xmlString;
  }
  
  
  
    public String pullCurrentUser(String securityToken) throws Exception
    {
        String userId = null;
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            userId = securityManager.getUserInfo(securityToken).getUserID();
        }
        else
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
        
        return userId;
    }
  
    /**
     * Rollback the user transaction
     * 
     * @param userTransaction
     */
    public static void rollback(UserTransaction userTransaction)
      throws SystemException
    {
      if (userTransaction != null)  
      {
        // rollback the user transaction
        userTransaction.rollback();
        logger.error("User transaction rolled back");
      }
    }
    

}
