package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.vo.CityVO;
import com.ftd.applications.lmd.vo.CodifiedProductVO;
import com.ftd.applications.lmd.vo.FloristBlockVO;
import com.ftd.applications.lmd.vo.FloristHistoryVO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.FloristWeightVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;

public class FloristUpdater
{
    private DataRequest dataRequest;
    private Logger logger;

    private final static String EMPTY_STRING = "";

    // These represent zip assignedStatus which is used by front-end
    private static final String ZIP_ASSIGNED = "assigned";
    private static final String ZIP_ASSIGNED_UNAVAIL = "assignedUnavail";

    public FloristUpdater(DataRequest dataRequest)
    {
        this.dataRequest = dataRequest;
        this.logger = new Logger("com.ftd.applications.lmd.util.FloristUpdater");
    }

    public FloristVO updateFloristFromRequest(FloristVO florist, HttpServletRequest request) throws Exception
    {
        SecurityManager securityManager = SecurityManager.getInstance();

        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            florist.setLastUpdateUser(securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
        }
        else
        {
            florist.setLastUpdateUser(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER));
        }

        // General Data
        florist.setMemberNumber(this.getValue(request.getParameter("memnum"), florist.getMemberNumber()));
        florist.setName(this.getValue(request.getParameter("name"), florist.getName()));
        florist.setAddress(this.getValue(request.getParameter("address"), florist.getAddress()));
        florist.setCity(this.getValue(request.getParameter("city"), florist.getCity()));
        florist.setState(this.getValue(request.getParameter("state"), florist.getState()));
        florist.setPostalCode(this.getValue(request.getParameter("zipcode"), florist.getPostalCode()));
        florist.setPhone(this.getValue(request.getParameter("phone"), florist.getPhone()));
        florist.setAltPhone(this.getValue(request.getParameter("altphone"), florist.getAltPhone()));
        florist.setFax(this.getValue(request.getParameter("fax"), florist.getFax()));
		florist.setContactName(this.getValue(request.getParameter("contact"), florist.getContactName()));
        florist.setEmailAddress(this.getValue(request.getParameter("email"), florist.getEmailAddress()));
        florist.setMercuryMachine(this.getValue(request.getParameter("mercuryflag"), florist.getMercuryMachine()));
        florist.setTerritory(this.getValue(request.getParameter("territory"), florist.getMercuryMachine()));
        florist.setVendorFlag(this.getValue(request.getParameter("isVendor"), florist.getVendorFlag()));

        if(request.getParameter("minorderamount") != null && request.getParameter("minorderamount").length() > 0)
        {
            florist.setMinimumOrderAmount(new Double(request.getParameter("minorderamount")));
        }

        if(request.getParameter("ms_override_flag") != null && (request.getParameter("ms_override_flag").equals("on") || request.getParameter("ms_override_flag").equals("Y")))
        {
            florist.getFloristWeight().setMercurySuspendOverride("Y");
        }
        else
        {
            florist.getFloristWeight().setMercurySuspendOverride("N");
        }

        // Zip Codes
        String postalCodeNumber = null;
        String postalCodeRequestedAssignedStatus = null;
        String postalCodeCurrentAssignedStatus = null;
        PostalCodeVO postalCode = null;
        String postalCodeCurrentCity = null;
        boolean zipAlreadyAssignedToFlorist = false;
        logger.info("blockAllZips: " + request.getParameter("blockAllZips"));

        int postalCodeCounter = 0;  // Count will be properly set to 1 at first increment

        // Update current zip codes for florist
        while(true)
        {
            postalCodeCounter++;
            postalCodeNumber = request.getParameter("zipcode" + postalCodeCounter);
            postalCodeCurrentCity = null;
            postalCodeRequestedAssignedStatus = request.getParameter("assignedStatus" + postalCodeCounter);
            if(postalCodeNumber != null)
            {
                // We only want to process zips that are currently assigned to this florist (or desire to be)
                if (!ZIP_ASSIGNED.equals(postalCodeRequestedAssignedStatus) &&
                    !ZIP_ASSIGNED_UNAVAIL.equals(postalCodeRequestedAssignedStatus))
                {
                    continue;  // Skip this one
                }

                // Note that postalCodeMap represents zips currently assigned to this florist
                // as well as zips available for assignment (and even zips assigned to related florists)
                // so we weed out if it's really assigned here
                postalCode = (PostalCodeVO) florist.getPostalCodeMap().get(postalCodeNumber);
                if (postalCode == null) {
                    // Zip not currently assigned to this florist (or associated florists)
                    zipAlreadyAssignedToFlorist = false;
                } else {
                    // Zip is related to this florist...
                    postalCodeCurrentAssignedStatus = postalCode.getAssignedStatus();
                    postalCodeCurrentCity = postalCode.getCity();
                    if (ZIP_ASSIGNED.equals(postalCodeCurrentAssignedStatus) ||
                        ZIP_ASSIGNED_UNAVAIL.equals(postalCodeCurrentAssignedStatus))
                    {
                        // ...it's currently assigned to this florist
                        zipAlreadyAssignedToFlorist = true;
                    } else {
                        // ...it's not currently assigned to this florist
                        zipAlreadyAssignedToFlorist = false;
                    }
                }

                // Zipcode was added (to request) and is not in the database yet
                if(zipAlreadyAssignedToFlorist == false)
                {
                    postalCode = new PostalCodeVO();
                    postalCode.setPostalCode(postalCodeNumber);

                    // Unsaved postal codes can not be blocked
                    postalCode.setBlockedFlag("N");
                    postalCode.setSavedFlag("N");

                    postalCode.setCutoffTime(this.getValue(request.getParameter("cutofftime" + postalCodeCounter), postalCode.getCutoffTime()));

                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    dataRequest.setStatementID("VIEW_ZIP_CITY_FLORIST_COUNT");
                    dataRequest.addInputParam("ZIP", postalCode.getPostalCode());

                    CachedResultSet countResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                    if(countResultSet != null && countResultSet.getRowCount() > 0)
                    {
                        while(countResultSet.next())
                        {
                            postalCode.setFloristCount(new Integer(((BigDecimal) countResultSet.getObject(1)).intValue()));
                            postalCode.setCity((String) countResultSet.getObject(2));
                        }
                    }
                    if (postalCodeCurrentCity != null) {
                        postalCode.setCity(postalCodeCurrentCity); // Use existing city since there was one
                    }
                    postalCode.setAssignedStatus(ZIP_ASSIGNED);  // We want new zip assigned
                    postalCode.setAssignmentType(this.getValue(request.getParameter("assignmentType" + postalCodeCounter),
                                                 postalCode.getAssignmentType()));

                    florist.getPostalCodeMap().put(postalCodeNumber, postalCode);
                }
                // Zipcode was pulled from database and can be blocked
                else if(postalCode.getSavedFlag() == null || !postalCode.getSavedFlag().equals("N"))
                {
                    postalCode.setCutoffTime(this.getValue(request.getParameter("cutofftime" + postalCodeCounter), postalCode.getCutoffTime()));
                    postalCode.setDeleteFlag(this.getValue(request.getParameter("deleteFlag" + postalCodeCounter), postalCode.getDeleteFlag()));

                    logger.info(postalCodeNumber + " block: " + request.getParameter("zipblock" + postalCodeCounter) +
                    		" unblockdate: " + request.getParameter("zipunblockdate" + postalCodeCounter) +
                    		" newunblockdate: " + request.getParameter("newzipunblockdate") +
                    		" blockdate: " + request.getParameter("zipblockdate" + postalCodeCounter));
                    
                    if(request.getParameter("zipblock" + postalCodeCounter) != null && request.getParameter("zipblock" + postalCodeCounter).equals("Y"))
                    {
                    	if (request.getParameter("blockAllZips") != null && request.getParameter("blockAllZips").equalsIgnoreCase("yes"))
                    	{
                            postalCode.setBlockedFlag("Y");
                            // Update unblock date
                            postalCode.setBlockStartDate(FieldUtils.formatStringToUtilDate(request.getParameter("zipblockdate" + postalCodeCounter)));
                            postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("newzipunblockdate")));
                        // Currently blocked zip code
                    	} else if(request.getParameter("zipunblockdate" + postalCodeCounter) != null && request.getParameter("zipunblockdate" + postalCodeCounter).length() > 0)
                        {
                            postalCode.setBlockedFlag("Y");

                            //Date currentEndDate = FieldUtils.formatStringToUtilDate(request.getParameter("zipunblockdate" + postalCodeCounter));
                            //if(postalCode.getBlockEndDate() == null || currentEndDate.compareTo(postalCode.getBlockEndDate()) != 0)
                            //{
                            postalCode.setBlockStartDate(FieldUtils.formatStringToUtilDate(request.getParameter("zipblockdate" + postalCodeCounter)));
                            // Update unblock date
                            postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("zipunblockdate" + postalCodeCounter)));
                            //}
                        }
                        // New block
                        else if(request.getParameter("newzipunblockdate") != null && request.getParameter("newzipunblockdate").length() > 0)
                        {
                            postalCode.setBlockedFlag("Y");

                            //Date currentEndDate = FieldUtils.formatStringToUtilDate(request.getParameter("newzipunblockdate"));
                            //if(postalCode.getBlockEndDate() == null || currentEndDate.compareTo(postalCode.getBlockEndDate()) != 0)
                            //{
                            postalCode.setBlockStartDate(FieldUtils.formatStringToUtilDate(request.getParameter("zipblockdate" + postalCodeCounter)));
                            // Update unblock date
                            postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("newzipunblockdate")));
                            //}
                        }
                        // Remove zip if no unblock date specified
                        else
                        {
                            postalCode.setDeleteFlag("Y");
                            postalCode.setBlockEndDate(null);
                        }
                    }
                    else
                    {
                        postalCode.setBlockedFlag("N");
                        postalCode.setBlockEndDate(null);
                    }
                }
            }
            else
            {
                break;
            }
        }

        // Cities
        String cityFloristId = null;
        CityVO cityVO = null;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        logger.info("blockAllCities: " + request.getParameter("blockAllCities"));

        int cityCounter = 0;  // Count will be properly set to 1 at first increment

        // Update current cities for florist
        while(true) {
            cityCounter++;
            cityFloristId = request.getParameter("cityfloristid" + cityCounter);
            String cityBlock = request.getParameter("cityblock" + cityCounter);
            String cityUnblockBlock = request.getParameter("cityunblockdate" + cityCounter);
            if(cityFloristId != null) {
            	cityVO = (CityVO) florist.getCitiesMap().get(cityFloristId);
                if (cityVO != null) {
                    logger.info(cityVO.getFloristCity() + " " + cityVO.getFloristId() +
                    		" block: " + request.getParameter("cityblock" + cityCounter) +
                    		" unblockdate: " + request.getParameter("cityunblockdate" + cityCounter) +
                    		" newunblockdate: " + request.getParameter("newcityunblockdate") +
                    		" citypermanentblockflag: " + request.getParameter("citypermanentblockflag" + cityCounter) +
                    		" cityVO: " + ((cityVO.getBlockEndDate() == null) ? "null" : sdf.format(cityVO.getBlockEndDate())) +
                    	    " changed: " + cityVO.isChanged());
                    
                    boolean newBlock = false;
                    if(request.getParameter("cityblock" + cityCounter) != null && request.getParameter("cityblock" + cityCounter).equals("Y")) {
                    	if (cityVO.getBlockStartDate() == null) {
                		    cityVO.setBlockStartDate(sdf.parse(sdf.format(new Date())));
                		    newBlock = true;
                    	}
                    	if (request.getParameter("blockAllCities") != null && request.getParameter("blockAllCities").equalsIgnoreCase("yes")) {
                    		// block all cities
                    		cityVO.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("newcityunblockdate")));
                    	} else if(request.getParameter("cityunblockdate" + cityCounter) != null && request.getParameter("cityunblockdate" + cityCounter).length() > 0) {
                    		Date tempUnblockDate = FieldUtils.formatStringToUtilDate(request.getParameter("cityunblockdate" + cityCounter));
                    		if (tempUnblockDate != null && 
                    				(cityVO.getBlockEndDate() != null && tempUnblockDate.compareTo(cityVO.getBlockEndDate()) != 0 ||
                    				cityVO.getBlockEndDate() == null)) {
                    			// Update unblock date
                    			cityVO.setBlockEndDate(tempUnblockDate);
                    		}
                        } else if(request.getParameter("newcityunblockdate") != null && request.getParameter("newcityunblockdate").length() > 0) {
                        	if (!request.getParameter("citypermanentblockflag" + cityCounter).equalsIgnoreCase("Y")) {
                                // New block
                        	    cityVO.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("newcityunblockdate")));
                        	}
                        } else {
                        	if (newBlock || request.getParameter("citypermanentblockflag" + cityCounter).equalsIgnoreCase("Y")) {
                                // Permanent block, no end date
                        	    cityVO.setBlockEndDate(null);
                        	}
                        }
                    } else {
                    	cityVO.setBlockStartDate(null);
                    	cityVO.setBlockEndDate(null);
                    }
                }
            } else {
            	break;
            }
        }

        // Blocking - grab any new blocks first
        String newBlockType = request.getParameter("codNewBlockType");
        if(newBlockType != null && !newBlockType.equals("A"))
        {
            FloristBlockVO floristBlock = new FloristBlockVO();
            floristBlock.setBlockNew(true);
            floristBlock.setBlockType(newBlockType);
            floristBlock.setBlockFlag("Y");
            floristBlock.setBlockStartDate(FieldUtils.formatStringToUtilDate(request.getParameter("codNewBlockStartDate")));
            floristBlock.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("codNewBlockEndDate")));

            logger.debug("Added a new block of " + floristBlock.getBlockType() + " " + floristBlock.getBlockStartDate());
            florist.getFloristBlockList().add(floristBlock);
        }

        // Now lets grab any removes
        int blockCounter = 1;
        while (true)
        {
            String blockRemoveCheckbox = request.getParameter("codBlockingRemove" + blockCounter);
            String blockRemoveStartDate = request.getParameter("codBlocking" + blockCounter);

            logger.debug("Removing Block " + blockRemoveCheckbox + " " + blockRemoveStartDate);
            if(blockRemoveStartDate != null)
            {
                logger.debug("Testing Block " + blockRemoveCheckbox + " " + blockRemoveStartDate);
                if (blockRemoveCheckbox != null && blockRemoveCheckbox.equals("on"))
                {
                    logger.debug("Removing Block " + blockRemoveCheckbox + " " + blockRemoveStartDate);

                    FloristBlockVO floristBlock = new FloristBlockVO();
                    floristBlock.setBlockRemoved(true);
                    Date removeBlockStartDate = FieldUtils.formatStringToUtilDate(blockRemoveStartDate);

                    // find the florist block in the florist block list and flag it removed
                    List floristBlocks = florist.getFloristBlockList();
                    for (int i=0; i < floristBlocks.size();i++)
                    {
                        FloristBlockVO oldFloristBlock = (FloristBlockVO) floristBlocks.get(i);
                        // Compare the start dates, it should be unique
                        if (removeBlockStartDate.equals(oldFloristBlock.getBlockStartDate()))
                        {
                            logger.debug(removeBlockStartDate + " -= " + oldFloristBlock.getBlockStartDate());
                            oldFloristBlock.setBlockRemoved(true);
                        }
                        else
                        {
                            logger.debug(removeBlockStartDate + " != " + oldFloristBlock.getBlockStartDate());
                        }
                    }

                }

                blockCounter++;
            }
            else
            {
                break;
            }

        }


//        SundayBlockVO sundayBlock = florist.getSundayBlock();
//        if(request.getParameter("codsundayblocked") != null && request.getParameter("codsundayblocked").equals("on"))
//        {
//            sundayBlock.setSundayDeliveryBlockFlag("Y");
//        }
//        else
//        {
//            sundayBlock.setSundayDeliveryBlockFlag("N");
//        }
//
//        if(request.getParameter("codsundayunblockdate") != null && request.getParameter("codsundayunblockdate").length() > 0)
//        {
//            sundayBlock.setSundayDeliveryBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("codsundayunblockdate")));
//        }
//        else
//        {
//            sundayBlock.setSundayDeliveryBlockEndDate(null);
//        }

        // Codification
        String codifiedId = null;
        CodifiedProductVO codifiedProduct = null;
        String previousBlockFlag = null;

        int codifiedCounter = 1;

        while(true)
        {
            codifiedId = request.getParameter("codid" + codifiedCounter);
            if(codifiedId != null)
            {
                codifiedProduct = (CodifiedProductVO) florist.getCodifiedProductMap().get(codifiedId);

                if(codifiedProduct == null)
                {
                    // Codified product was added new in memory and is not in the database yet
                    codifiedProduct = new CodifiedProductVO();
                    codifiedProduct.setId(codifiedId);

                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                    dataRequest.setStatementID("VIEW_CODIFICATIONS_BY_ID");
                    dataRequest.addInputParam("CODIFIED_ID", codifiedProduct.getId());

                    CachedResultSet codifiedResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                    if(codifiedResultSet != null && codifiedResultSet.getRowCount() > 0)
                    {
                        while(codifiedResultSet.next())
                        {
                            codifiedProduct.setCategory((String) codifiedResultSet.getObject(2));
                            codifiedProduct.setName((String) codifiedResultSet.getObject(3));
                        }
                    }

                    florist.getCodifiedProductMap().put(codifiedProduct.getId(), codifiedProduct);
                }

                if(request.getParameter("codblock" + codifiedCounter) != null && request.getParameter("codblock" + codifiedCounter).equals("on"))
                {
                    previousBlockFlag = codifiedProduct.getBlockedFlag();
                    codifiedProduct.setBlockedFlag("Y");

                    if(request.getParameter("codunblockdate" + codifiedCounter) != null && request.getParameter("codunblockdate" + codifiedCounter).length() > 0)
                    {
                        Date currentEndDate = FieldUtils.formatStringToUtilDate(request.getParameter("codunblockdate" + codifiedCounter));
                        if(codifiedProduct.getBlockEndDate() == null || currentEndDate.compareTo(codifiedProduct.getBlockEndDate()) != 0)
                        {
                            // Update unblock date
                            codifiedProduct.setBlockEndDate(currentEndDate);
                        }
                    }
                } else if(request.getParameter("codpermblock" + codifiedCounter) != null && request.getParameter("codpermblock" + codifiedCounter).equals("on")) {
                	codifiedProduct.setBlockedFlag("Y");
                	codifiedProduct.setBlockEndDate(null);
                }
                else
                {
                    codifiedProduct.setBlockedFlag("N");
                    codifiedProduct.setBlockEndDate(null);
                }

                codifiedCounter++;
            }
            else
            {
                break;
            }
        }

        // Maintain unsaved history
        /*
        int historyTotal = 0;

        if(request.getParameter("totalHist") != null)
        {
            historyTotal = Integer.parseInt(request.getParameter("totalHist"));
        }

        for(int i = 0; i <= historyTotal; i++)
        {
            if(request.getParameter("comment" + i) != null)
            {
                FloristHistoryVO floristHistory = new FloristHistoryVO();

                floristHistory.setComments(request.getParameter("comment" + i));
                floristHistory.setCommentType(request.getParameter("disposition" + i));
                floristHistory.setManagerFlag(request.getParameter("managerflag" + i));
                floristHistory.setCommentDate(request.getParameter("newDate" + i));
                floristHistory.setUserId(florist.getLastUpdateUser());
                floristHistory.setNewCommentFlag("Y");

                florist.getFloristHistoryList().add(0, floristHistory);
            }
        }
        */

        /*
        // Florist History - add new comment
        if(request.getParameter("newcomment") != null && request.getParameter("newcomment").length() > 0)
        {
            FloristHistoryVO floristHistory = new FloristHistoryVO();

            floristHistory.setComments(request.getParameter("newcomment")); // the textarea

            floristHistory.setCommentDate(formatCommentDate(new Date()));
            floristHistory.setCommentType(request.getParameter("dispositionDescription"));
            floristHistory.setManagerFlag(request.getParameter("newmanagerflag"));
            floristHistory.setUserId(florist.getLastUpdateUser());
            floristHistory.setNewCommentFlag("Y");

            florist.getFloristHistoryList().add(0, floristHistory);

            // Persist comment to database immediatly
            HistoryHelper historyHelper = new HistoryHelper();
            historyHelper.persistHistory(florist.getMemberNumber(), floristHistory);
        }
        */

        // Weighting
        FloristWeightVO floristWeight = florist.getFloristWeight();

        floristWeight.setGotoFlag(this.getValue(request.getParameter("go_to_florist"), floristWeight.getGotoFlag()));

        if(request.getParameter("adjusted_weight") != null && request.getParameter("adjusted_weight").length() > 0)
        {
            floristWeight.setAdjustedWeight(new Integer(request.getParameter("adjusted_weight")));
        }
		else
		{
			 floristWeight.setAdjustedWeight(null);
		}

        if(request.getParameter("weightStartDate") != null && request.getParameter("weightStartDate").length() > 0)
        {
			floristWeight.setAdjustedWeightStartDate((FieldUtils.formatStringToUtilDate(request.getParameter("weightStartDate"))));
        }
		else
		{
			if(request.getParameter("adjusted_weight") != null && request.getParameter("adjusted_weight").length() > 0)
			{
				floristWeight.setAdjustedWeightStartDate(new Date());
			}
			else
			{
				floristWeight.setAdjustedWeightStartDate(null);
			}
		}

        if(request.getParameter("weightEndDate") != null && request.getParameter("weightEndDate").length() > 0)
        {
            floristWeight.setAdjustedWeightEndDate((FieldUtils.formatStringToUtilDate(request.getParameter("weightEndDate"))));
        }

        if(request.getParameter("weightNoEndDate") != null && request.getParameter("weightNoEndDate").equals("on"))
        {
            floristWeight.setAdjustedWeightPermFlag("Y");
        }
        else
        {
            floristWeight.setAdjustedWeightPermFlag("N");
        }

        return florist;
    }

    private String getValue(String requestValue, String currentValue)
    {
        if(requestValue != null)
        {
            return requestValue;
        }
        else
        {
            return currentValue;
        }
    }

    private String formatCommentDate(Date date)
    {
        String formattedDate = "";

        if(date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
            formattedDate = sdf.format(date);
        }

        return formattedDate;
    }

}