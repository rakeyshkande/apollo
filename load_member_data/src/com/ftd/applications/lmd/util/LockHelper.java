package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.ViewQueueVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;

public class LockHelper 
{
    private DataRequest dataRequest;
    private Logger logger;
    
    public LockHelper(DataRequest dataRequest)
    {
        this.dataRequest = dataRequest;
        this.logger = new Logger("com.ftd.applications.lmd.util.LockHelper");
    }
    
    public void requestMaintenanceLock(FloristVO florist, String userId) throws Exception
    {
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("Y"))
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("UPDATE_FLORIST_LOCK");
        dataRequest.addInputParam("IN_FLORIST_ID", florist.getMemberNumber());
        dataRequest.addInputParam("IN_LOCK_INDICATOR", "Y");
        dataRequest.addInputParam("IN_LOCKED_BY", userId);
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("Y"))
        {
            // Record updated and available
            florist.setLockedByUser(userId);
            florist.setLockedFlag("Y");
        }
        else
        {
            // Record already locked by another user
            String lockedByUser = (String) outputs.get("OUT_LOCKED_BY");
            florist.setLockedByUser(lockedByUser);
            florist.setLockedFlag("N");
        }
    }
    
    public void releaseMaintenanceLock(String floristId, String userId) throws Exception
    {
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("Y"))
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("UPDATE_FLORIST_LOCK");
        dataRequest.addInputParam("IN_FLORIST_ID", floristId);
        dataRequest.addInputParam("IN_LOCK_INDICATOR", "N");
        dataRequest.addInputParam("IN_LOCKED_BY", userId);
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    }
    
    public void requestViewQueueLock(ViewQueueVO viewQueue, String userId) throws Exception
    {
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("Y"))
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("UPDATE_FLORIST_LOCK");
        dataRequest.addInputParam("IN_FLORIST_ID", viewQueue.getFloristNumber());
        dataRequest.addInputParam("IN_LOCK_INDICATOR", "Y");
        dataRequest.addInputParam("IN_LOCKED_BY", userId);
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("Y"))
        {
            // Record updated and available
            viewQueue.setLockedByUser(userId);
            viewQueue.setLockedFlag("Y");
        }
        else
        {
            // Record already locked by another user
            String lockedByUser = (String) outputs.get("OUT_LOCKED_BY");
            viewQueue.setLockedByUser(lockedByUser);
            viewQueue.setLockedFlag("N");
        }
    }
    
    public void releaseViewQueueLock(String floristId, String userId) throws Exception
    {
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("Y"))
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER);
        }
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("UPDATE_FLORIST_LOCK");
        dataRequest.addInputParam("IN_FLORIST_ID", floristId);
        dataRequest.addInputParam("IN_LOCK_INDICATOR", "N");
        dataRequest.addInputParam("IN_LOCKED_BY", userId);
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    }
}