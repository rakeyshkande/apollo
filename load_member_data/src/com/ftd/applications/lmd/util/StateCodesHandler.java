package com.ftd.applications.lmd.util;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 * @author Anshu Gaind
 * @version $Id: StateCodesHandler.java,v 1.3 2011/06/30 15:14:17 gsergeycvs Exp $
 */

public class StateCodesHandler extends CacheHandlerBase
{
  private static Logger logger  = new Logger("com.ftd.applications.lmd.util.StateCodesHandler");
  // set by the setCachedObject method
  private Map stateCodes;
  
  public StateCodesHandler()
  {
  }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
      Map map = new HashMap();
      try
      {
        logger.debug("BEGIN LOADING STATE CODES HANDLER...");
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_EFOS_STATE_CODES");
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        
        String stateCode, stateID;                 
        while(rs.next())
        {
            stateCode = (String)rs.getObject(1);
            stateID = (String)rs.getObject(2);
            map.put(stateCode, stateID);
        }  
        logger.debug( rs.getRowCount() + " records loaded");
        logger.debug("END LOADING STATE CODES HANDLER...");
      } catch(Exception e)
      {
          logger.error(e);
          throw new CacheException("Could not load state codes data.");
      }
      
      return map;      
    }
    
  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.stateCodes = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  
  /**
   * Returns the state for the state code
   * 
   * @param stateCode
   * @return 
   */
  public String getState(String stateCode)
  {
    return (String) this.stateCodes.get(stateCode);
  }

}//~
