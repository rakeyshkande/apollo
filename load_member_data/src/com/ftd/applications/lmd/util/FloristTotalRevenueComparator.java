package com.ftd.applications.lmd.util;
import com.ftd.applications.lmd.vo.FloristWeightCalcVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Comparator;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * Compares florist's total revenue
 * 
 * @author Anshu Gaind
 * @version $Id: FloristTotalRevenueComparator.java,v 1.3 2011/06/30 15:14:17 gsergeycvs Exp $
 */

public class FloristTotalRevenueComparator implements Comparator
{
  private BigDecimal numberOfMonths;
  
  /**
   * Constuctor
   */
  public FloristTotalRevenueComparator()
    throws IOException, SAXException, ParserConfigurationException, TransformerException 
  {
    this.init();
  }
  
  /**
   * Initialize the comparator
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws javax.xml.transform.TransformerException
   * @throws oracle.xml.parser.v2.XSLException
   */
  private void init() throws IOException, SAXException, ParserConfigurationException, TransformerException 
  {
    numberOfMonths = new BigDecimal(Double.parseDouble( ConfigurationUtil.getInstance().getProperty("florist_weight_calc_config.xml", "REVENUE_REBATE_AVERAGE_NUMBER_OF_MONTHS") ));
  }
  
  /**
   * Compares the values of total revenues 
   * 
   * Compares its two arguments for order. Returns a negative integer, zero, 
   * or a positive integer as the first argument is less than, equal to, 
   * or greater than the second.
   * 
   * @param o1
   * @param o2
   * @return 
   */
  public int compare(Object o1, Object o2)
  {
    FloristWeightCalcVO vo1 = (FloristWeightCalcVO) o1;
    FloristWeightCalcVO vo2 = (FloristWeightCalcVO) o2;
    
    BigDecimal totalRevenue1 = vo1.getAverageRevenue().multiply(numberOfMonths);
    
    BigDecimal totalRevenue2 = vo2.getAverageRevenue().multiply(numberOfMonths);
    /*
     * The total revenue is net of rebate
     */
    return totalRevenue2.compareTo( totalRevenue1 );    
  }
  
  /**
   * Indicates whether some other object is "equal to" this Comparator. 
   * 
   * @param obj
   * @return 
   */
  public boolean equals(Object obj)
  {
    if (obj instanceof FloristTotalRevenueComparator) 
    {
        return true;
    }
    else
    {
      return false;
    }
    
  }
}
