package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.vo.CodifiedProductVO;
import com.ftd.applications.lmd.vo.FloristBlockVO;
import com.ftd.applications.lmd.vo.PostalCodeVO;
import com.ftd.applications.lmd.vo.ViewQueueVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class ViewQueueUpdater 
{
    private DataRequest dataRequest;
    private Logger logger;
    
    private final static String EMPTY_STRING = "";
    
    public ViewQueueUpdater(DataRequest dataRequest)
    {
        this.dataRequest = dataRequest;
        this.logger = new Logger("com.ftd.applications.lmd.util.ViewQueueUpdater");
    }
    
    public ViewQueueVO updateFloristFromRequest(ViewQueueVO viewQueue, HttpServletRequest request) throws Exception
    {
        SecurityManager securityManager = SecurityManager.getInstance();
        
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
        if(securityOverride != null && securityOverride.equals("N"))
        {
            viewQueue.setLastUpdateUser(securityManager.getUserInfo(request.getParameter("securitytoken")).getUserID());
        }
        else
        {
            viewQueue.setLastUpdateUser(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_USER));
        }
        
        // Zip Codes
        String postalCodeNumber = null;
        PostalCodeVO postalCode = null;
        
        int postalCodeCounter = 1;
        
        // Update current zip codes for florist
        while(true)
        {
            postalCodeNumber = request.getParameter("zipcode" + postalCodeCounter);
            if(postalCodeNumber != null)
            {
                postalCode = (PostalCodeVO) viewQueue.getPostalCodeMap().get(postalCodeNumber);
                
                // Zipcode was pulled from database and can be blocked
                if(postalCode != null)
                {
                    postalCode.setCutoffTime(this.getValue(request.getParameter("cutofftime" + postalCodeCounter), postalCode.getCutoffTime()));
                    postalCode.setDeleteFlag(this.getValue(request.getParameter("deleteFlag" + postalCodeCounter), postalCode.getDeleteFlag()));
                    
                    if(request.getParameter("zipblock" + postalCodeCounter) != null && request.getParameter("zipblock" + postalCodeCounter).equals("Y"))
                    {
                        // Currently blocked zip code
                        if(request.getParameter("zipunblockdate" + postalCodeCounter) != null && request.getParameter("zipunblockdate" + postalCodeCounter).length() > 0)
                        {
                            postalCode.setBlockedFlag("Y");
                            postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("zipunblockdate" + postalCodeCounter)));
                        }
                        // New block
                        else if(request.getParameter("newzipunblockdate") != null && request.getParameter("newzipunblockdate").length() > 0)
                        {
                            postalCode.setBlockedFlag("Y");
                            postalCode.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("newzipunblockdate")));
                        }
                        // Remove zip if no unblock date specified
                        else
                        {
                            postalCode.setDeleteFlag("Y");
                            postalCode.setBlockEndDate(null);
                        }
                    }
                    else
                    {
                        postalCode.setBlockedFlag("N");
                        postalCode.setBlockEndDate(null);                
                    }
                }
                
                postalCodeCounter++;
            }
            else
            {
                break; 
            }
        }
        
        // Blocking
        logger.info("codblocking: " + request.getParameter("codblocking"));
        logger.info("codblockdate: " + request.getParameter("codblockdate"));
        logger.info("codunblockdate: " + request.getParameter("codunblockdate"));
        
        try {
            logger.info("totalblocks: " + request.getParameter("totalblocks"));
        	String totalblocks = request.getParameter("totalblocks");
        	if (totalblocks != null) {
        		int blockCount = Integer.parseInt(totalblocks);
        		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        		List removeList = new ArrayList();
        		for (int x=0; x<blockCount; x++) {
        			String removeblock = request.getParameter("removeblock" + (x+1));
        			String removestartdate = request.getParameter("removestartdate" + (x+1));
        			logger.info("removeblock " + removeblock + " " + removestartdate);
        			if (removeblock != null && removeblock.equalsIgnoreCase("on")) {
        			    removeList.add(sdf.parseObject(removestartdate));
        			}
        		}
        		viewQueue.setRemoveBlockList(removeList);
        	}
        } catch (Exception e) {
        	logger.error(e);
        }
        
        FloristBlockVO floristBlock = viewQueue.getNewFloristBlock();
        floristBlock.setBlockType(this.getValue(request.getParameter("codblocking"), floristBlock.getBlockType()));
        if(floristBlock.getBlockType() != null && floristBlock.getBlockType().trim().length() > 0 && !floristBlock.getBlockType().equals("A"))
        {
            floristBlock.setBlockFlag("Y");
            floristBlock.setBlockStartDate(FieldUtils.formatStringToUtilDate(request.getParameter("codblockdate")));
            floristBlock.setBlockEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("codunblockdate")));
        
            // Update the reason comment
            if(request.getParameter("reason_comment") != null && request.getParameter("reason_comment").length() > 0)
            {
            	floristBlock.setBlockReason(request.getParameter("reason_comment"));
                logger.info("reason: " + floristBlock.getBlockReason());
            }
        }
        else
        {
            floristBlock.setBlockFlag("N");
            floristBlock.setBlockEndDate(null);
        }
        
        // Codification
        String codifiedId = null;
        String directEditCodifiedId = null;
        CodifiedProductVO codifiedProduct = null;
        
        // Direct edit on product for message
        directEditCodifiedId = request.getParameter("product_codified_id");
        if(directEditCodifiedId != null && directEditCodifiedId.length() > 0)
        {
            codifiedProduct = (CodifiedProductVO) viewQueue.getCodifiedProductMap().get(directEditCodifiedId);
            
            if(request.getParameter("product_blocked_flag") != null && request.getParameter("product_blocked_flag").equals("on"))
            {
                codifiedProduct.setBlockedFlag("Y");
                    
                if(request.getParameter("product_unblock_date") != null && request.getParameter("product_unblock_date").length() > 0)
                {
                    Date currentEndDate = FieldUtils.formatStringToUtilDate(request.getParameter("product_unblock_date"));
                    if(codifiedProduct.getBlockEndDate() == null || currentEndDate.compareTo(codifiedProduct.getBlockEndDate()) != 0)
                    {
                        // Update unblock date
                        codifiedProduct.setBlockEndDate(currentEndDate);
                    }
                }
            }
            else
            {
                codifiedProduct.setBlockedFlag("N");
                codifiedProduct.setBlockEndDate(null);
            }
        }
        
        // Cycle through rest of codified products
        int codifiedCounter = 1;
        while(true)
        {
            codifiedId = request.getParameter("codid" + codifiedCounter);
            if(codifiedId != null)
            {
                // Do not process if already processed above
                if(directEditCodifiedId == null || !directEditCodifiedId.equals(codifiedId))
                {
                    codifiedProduct = (CodifiedProductVO) viewQueue.getCodifiedProductMap().get(codifiedId);
                    
                    if(codifiedProduct != null && request.getParameter("codblock" + codifiedCounter) != null && request.getParameter("codblock" + codifiedCounter).equals("on"))
                    {
                        codifiedProduct.setBlockedFlag("Y");
                            
                        if(request.getParameter("codunblockdate" + codifiedCounter) != null && request.getParameter("codunblockdate" + codifiedCounter).length() > 0)
                        {
                            Date currentEndDate = FieldUtils.formatStringToUtilDate(request.getParameter("codunblockdate" + codifiedCounter));
                            if(codifiedProduct.getBlockEndDate() == null || currentEndDate.compareTo(codifiedProduct.getBlockEndDate()) != 0)
                            {
                                // Update unblock date
                                codifiedProduct.setBlockEndDate(currentEndDate);
                            }
                        }
                    } else if(request.getParameter("codpermblock" + codifiedCounter) != null && request.getParameter("codpermblock" + codifiedCounter).equals("on")) {
                       codifiedProduct.setBlockedFlag("Y");
                       codifiedProduct.setBlockEndDate(null);
                    }
                    else
                    {
                        codifiedProduct.setBlockedFlag("N");
                        codifiedProduct.setBlockEndDate(null);
                    }
                }
                
                codifiedCounter++;
            }
            else
            {
                break; 
            }
        }
        
        return viewQueue;
    }
    
    private String getValue(String requestValue, String currentValue)
    {
        if(requestValue != null)
        {
            return requestValue;
        }
        else
        {
            return currentValue;
        }
    }
    
    private String formatCommentDate(Date date)
    {
        String formattedDate = "";
        
        if(date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
            formattedDate = sdf.format(date);
        }
        
        return formattedDate;
    }
    
}