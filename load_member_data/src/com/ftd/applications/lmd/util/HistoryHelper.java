package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.bo.HistoryTracker;
import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.vo.FloristHistoryVO;
import com.ftd.applications.lmd.vo.FloristVO;
import com.ftd.applications.lmd.vo.HistoryVO;
import com.ftd.applications.lmd.vo.ViewQueueVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HistoryHelper 
{
    private DataRequest dataRequest;
    private Logger logger;
    
    private final static String EMPTY_STRING = "";
    
    public HistoryHelper(DataRequest dataRequest)
    {
        this.dataRequest = dataRequest;
        this.logger = new Logger("com.ftd.applications.lmd.util.HistoryHelper");
    }
    
    public List generateMaintenanceHistory(FloristVO oldVO, FloristVO newVO) throws Exception
    {
        List historyList = new ArrayList();
        
        HistoryTracker historyTracker = new HistoryTracker();
        
        HistoryVO history = null;
        FloristHistoryVO floristHistory = null;
        
        // Create florist history and insert into database if updated
        if(oldVO != null)
        {
            List historyTrackerList = historyTracker.getMaintenanceHistory(oldVO, newVO);
        
            Iterator autoHistoryIterator = historyTrackerList.iterator();
            while(autoHistoryIterator.hasNext())
            {
                // Create object and add to list
                history = (HistoryVO) autoHistoryIterator.next();
                
                StringBuffer comment = new StringBuffer();
                floristHistory = new FloristHistoryVO();
                floristHistory.setComments(this.getPhrase(history));
                floristHistory.setCommentType(history.getDispositionId());
                floristHistory.setManagerFlag("N");
                floristHistory.setUserId(newVO.getLastUpdateUser());
                historyList.add(floristHistory);
                
                this.persistHistory(newVO.getMemberNumber(), floristHistory);
            }
        }
        
        return historyList;
    }
    
    public List generateViewQueueHistory(ViewQueueVO oldViewQueue, ViewQueueVO newViewQueue) throws Exception
    {
        List historyList = new ArrayList();
        
        HistoryTracker historyTracker = new HistoryTracker();
        
        HistoryVO history = null;
        FloristHistoryVO floristHistory = null;
        
        // Create florist history and insert into database if updated
        if(oldViewQueue != null)
        {
            List historyTrackerList = historyTracker.getViewQueueHistory(oldViewQueue, newViewQueue);
        
            Iterator autoHistoryIterator = historyTrackerList.iterator();
            while(autoHistoryIterator.hasNext())
            {
                // Create object and add to list
                history = (HistoryVO) autoHistoryIterator.next();
                
                StringBuffer comment = new StringBuffer();
                floristHistory = new FloristHistoryVO();
                floristHistory.setComments(this.getPhrase(history) + " " + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.VIEW_QUEUE_ADDITIONAL_COMMENT) + " (" + newViewQueue.getOrderNumber() + ")");
                floristHistory.setCommentType(history.getDispositionId());
                floristHistory.setManagerFlag("N");
                floristHistory.setUserId(newViewQueue.getLastUpdateUser());
                historyList.add(floristHistory);
                
                this.persistHistory(newViewQueue.getFloristNumber(), floristHistory);
            }
        }
        
        return historyList;
    }
    
    public void persistHistory(String floristId, FloristHistoryVO floristHistory) throws Exception
    {        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
        // Save comment to database
        dataRequest.setStatementID("INSERT_FLORIST_COMMENTS");
        dataRequest.addInputParam("IN_FLORIST_ID", floristId);
        dataRequest.addInputParam("IN_COMMENT_TYPE", floristHistory.getCommentType());
        dataRequest.addInputParam("IN_COMMENT", floristHistory.getComments());
        dataRequest.addInputParam("IN_MANAGER_FLAG", floristHistory.getManagerFlag());
        dataRequest.addInputParam("IN_USER", floristHistory.getUserId());
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if(status.equals("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
    }
    
    private String getPhrase(HistoryVO history)
    {
        String phrase = "";
        
        if(history.getChangeType() != null)
        {
            if(history.getChangeType().equals(HistoryTracker.NEW_VALUE))
            {
                phrase = history.getField() + " " + history.getNewValue() + " was added";
            }
            else if(history.getChangeType().equals(HistoryTracker.DELETED_VALUE))
            {
                phrase = history.getField() + " " + history.getOldValue() + " was deleted";
            }
            else if(history.getChangeType().equals(HistoryTracker.CHANGED_VALUE))
            {
                if(history.getAdditionialFieldName() != null)
                {
                    phrase = history.getField() + " was changed from " + history.getOldValue() + " to " + history.getNewValue() + " for " + history.getAdditionialFieldName() + " " + history.getAdditionialData() + "";
                }
                else
                {
                    phrase = history.getField() + " was changed from " + history.getOldValue() + " to " + history.getNewValue() + "";
                }
            }
        }
        
        return phrase;
    }
}