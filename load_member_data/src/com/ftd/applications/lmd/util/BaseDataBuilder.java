package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.constants.ConfigurationConstants;
import com.ftd.applications.lmd.constants.SecurityConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * This class provides the shared function calls utilized by other servlets
 * such as generating the search criteria.
 *
 * @author Mehul Patel
 */
public class BaseDataBuilder 
{
    private Logger logger;

    /** 
     * Constructor
     */
    public BaseDataBuilder()
    {
       logger = new Logger("com.ftd.applications.lmd.util.BaseDataBuilder");
    }

	/**
	* Fetches the specified parameters and generates the search criteria.
	* 
	* @param request HttpServletRequest
	*
	*/
	public HashMap generateSearchCriteria(HttpServletRequest request)
    {
        HashMap searchCriteriaMap = new HashMap();

        searchCriteriaMap.put("sc_floristnum", request.getParameter("sc_floristnum"));
        searchCriteriaMap.put("sc_zipcode", request.getParameter("sc_zipcode"));
        searchCriteriaMap.put("sc_city", request.getParameter("sc_city"));
        searchCriteriaMap.put("sc_state", request.getParameter("sc_state"));
        searchCriteriaMap.put("sc_shopname", request.getParameter("sc_shopname"));
        searchCriteriaMap.put("sc_phonenumber", request.getParameter("sc_phonenumber"));
        searchCriteriaMap.put("sc_statuslist", request.getParameter("sc_statuslist"));
        searchCriteriaMap.put("sc_product", request.getParameter("sc_product"));
        searchCriteriaMap.put("sc_productType", request.getParameter("sc_productType"));
        searchCriteriaMap.put("sc_includeVendor", request.getParameter("sc_includeVendor"));
		searchCriteriaMap.put("uid", request.getParameter("uid"));
        searchCriteriaMap.put("count", request.getParameter("count"));
		
        return searchCriteriaMap;
    }


    /**
     * Get drop down data for codifications page
     * @param order
     * @param dataRequest
     * @param request
     * @param DataLevel
     * @return 
     * @throws java.lang.Exception
     */
	public NodeList loadCodificationGenericDataData(OrderVO order, DataRequest dataRequest, HttpServletRequest request, boolean DataLevel) throws Exception
	{
			Document responseDocument = DOMUtil.getDocument();

			dataRequest.reset();
			dataRequest.setStatementID("VIEW_CODIFICATIONS");
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  

			dataRequest.reset();
			dataRequest.setStatementID("VIEW_CODIFICATIONS_CAT");
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  
			
			
			DOMUtil.addSection(responseDocument, "processFlow", "flow", this.generateProcessFlow(request), true);
			
            return responseDocument.getFirstChild().getChildNodes();
    }
    
    /**
     * Get drop down data for view queue page
     * @param order
     * @param dataRequest
     * @param request
     * @param DataLevel
     * @return 
     * @throws java.lang.Exception
     */
	public NodeList loadViewQueueGenericData(DataRequest dataRequest, HttpServletRequest request) throws Exception
	{
			Document responseDocument = DOMUtil.getDocument();
            
            // Load web process flow        
			DOMUtil.addSection(responseDocument, "processFlow", "flow", this.generateProcessFlow(request), true);
            
            // Load cutoff times
			dataRequest.reset();
			dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
			dataRequest.addInputParam("IN_CONTEXT", new String("FLORIST_MAINTENANCE"));
			dataRequest.addInputParam("IN_NAME", new String("CUTOFF_TIMES"));
			String cutoffs = (String) DataAccessUtil.getInstance().execute(dataRequest);
			
			String[] cutArray = cutoffs.split(",");
            DOMUtil.addSection(responseDocument, generateCutoffElement(responseDocument, cutArray).getChildNodes()); 
                        
            return responseDocument.getFirstChild().getChildNodes();
    }


	/**
	* Ouputs the generated criteria onto the response document.
	* 
	* @param order OrderVO
	* @param dataRequest DataRequest
	* @param request HttpServletRequest
	* @param DataLevel Boolean // not in use at this point
	*
	*/
	public NodeList loadGenericDataData(OrderVO order, DataRequest dataRequest, HttpServletRequest request, boolean DataLevel) throws Exception
	{
			Document responseDocument = DOMUtil.getDocument();
			
			// Load search criteria        
			DOMUtil.addSection(responseDocument, "searchCriteria", "criteria", this.generateSearchCriteria(request), true);
			
            // Load web process flow        
			DOMUtil.addSection(responseDocument, "processFlow", "flow", this.generateProcessFlow(request), true);
            
            // Load security permissions        
			DOMUtil.addSection(responseDocument, "securityPermissions", "permission", this.generateSecurityData(request, dataRequest), true);
			
			dataRequest.reset();
			dataRequest.setStatementID("VIEW_CODIFICATIONS");
			DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());  
			
			dataRequest.reset();
            dataRequest.setStatementID("STATE_LIST_LOOKUP");
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
        
			dataRequest.reset();
			dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
			dataRequest.addInputParam("IN_CONTEXT", new String("FLORIST_MAINTENANCE"));
			dataRequest.addInputParam("IN_NAME", new String("CUTOFF_TIMES"));
			String cutoffs = (String) DataAccessUtil.getInstance().execute(dataRequest);
			
			String[] cutArray = cutoffs.split(",");
            DOMUtil.addSection(responseDocument, generateCutoffElement(responseDocument, cutArray).getChildNodes()); 
                        
			return responseDocument.getFirstChild().getChildNodes();
	}


	/**
	* Ouputs the generated criteria onto the response document.
	* 
	* @param order OrderVO
	* @param dataRequest DataRequest
	* @param request HttpServletRequest
	* @param DataLevel Boolean // not in use at this point
	*
	*/
	public Element loadCutoffDataData(DataRequest dataRequest,Document responseDocument) throws Exception
	{
        dataRequest.reset();
        dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
        dataRequest.addInputParam("IN_CONTEXT", new String("FLORIST_MAINTENANCE"));
        dataRequest.addInputParam("IN_NAME", new String("CUTOFF_TIMES"));
        String cutoffs = (String) DataAccessUtil.getInstance().execute(dataRequest);
        
        String[] cutArray = cutoffs.split(",");
        
        return generateCutoffElement(responseDocument, cutArray);
	}

    private Element generateCutoffElement(Document responseDocument, String[] cutArray) throws Exception
    {
        String militaryTime = null;
        String displayTime = null;
    
        Element cutoffTimeRootElement = responseDocument.createElement("cutoffTimeRoot");
        Element cutoffTimeListElement = responseDocument.createElement("cutoffTimeList");
        Element cutoffTimeElement = null;
        Element cutoffDisplayElement = null;
        Element cutoffMilitaryElement = null;
        
        cutoffTimeRootElement.appendChild(cutoffTimeListElement);
        
        for(int i=0; i < cutArray.length; i++)
        {
            cutoffTimeElement = responseDocument.createElement("cutoffTime");
            cutoffDisplayElement = responseDocument.createElement("cutoffDisplay");
            cutoffMilitaryElement = responseDocument.createElement("cutoffMilitary");
        
            militaryTime = convertToMilitary(cutArray[i]);
            displayTime = cutArray[i];
        
            cutoffDisplayElement.appendChild(responseDocument.createTextNode(displayTime));
            cutoffMilitaryElement.appendChild(responseDocument.createTextNode(militaryTime));
    
            cutoffTimeElement.appendChild(cutoffDisplayElement);
            cutoffTimeElement.appendChild(cutoffMilitaryElement);
            cutoffTimeListElement.appendChild(cutoffTimeElement);
        }

        return cutoffTimeRootElement;
    }
    
    private HashMap generateProcessFlow(HttpServletRequest request)
    {
        HashMap flowMap = new HashMap();

        flowMap.put("endTab", request.getParameter("endTab"));
        flowMap.put("secondaryAction", request.getParameter("secondaryAction"));
		if(request.getParameter("qtyp") != null)
			flowMap.put("saveNeeded", "no");
		else
			flowMap.put("saveNeeded", request.getParameter("saveNeeded"));
        flowMap.put("processType", request.getParameter("processType"));
		flowMap.put("isNew", request.getParameter("isNew"));
		
        return flowMap;
    }
    
    private String convertToMilitary(String time) throws Exception
    {
        String formattedDate = "";
        
        SimpleDateFormat sdfInput = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdfOutput = new SimpleDateFormat("HHmm");
        formattedDate = sdfOutput.format(sdfInput.parse(time));
        
        return formattedDate;
    }
    
    public HashMap generateSecurityData(HttpServletRequest request, DataRequest dataRequest) throws Exception
	{
        SecurityManager securityManager = SecurityManager.getInstance();
        HashMap securityMap = new HashMap();
        
        String securityOverride = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.LOAD_MEMBER_CONFIG_FILE, ConfigurationConstants.OVERRIDE_SECURITY_CHECK);
    
        if(securityOverride != null && securityOverride.equals("N"))
        {
            String securityToken = request.getParameter("securitytoken");
            String context = request.getParameter("context");
        
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.PROFILE, SecurityConstants.UPDATE))
            {
                securityMap.put("profileUpdate", "Y");
            }
            else
            {
                securityMap.put("profileUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.OVERRIDE_WEIGHT, SecurityConstants.VIEW))
            {
                securityMap.put("overrideWeightView", "Y");
            }
            else
            {
                securityMap.put("overrideWeightView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.SUSPEND_OVERRIDE, SecurityConstants.UPDATE))
            {
                securityMap.put("suspendOverrideUpdate", "Y");
            }
            else
            {
                securityMap.put("suspendOverrideUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.ZIP_CODES, SecurityConstants.UPDATE))
            {
                securityMap.put("zipCodesUpdate", "Y");
            }
            else
            {
                securityMap.put("zipCodesUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.VENDOR, SecurityConstants.UPDATE))
            {
                securityMap.put("vendorFlagUpdate", "Y");
            }
            else
            {
                securityMap.put("vendorFlagUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.ZIP_CODES_LIST_LINK, SecurityConstants.VIEW))
            {
                securityMap.put("zipCodeListLinkView", "Y");
            }
            else
            {
                securityMap.put("zipCodeListLinkView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.ADD_CODIFICATION, SecurityConstants.YES))
            {
                securityMap.put("addCodification", "Y");
            }
            else
            {
                securityMap.put("addCodification", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.SUNDAY_DELIVERY, SecurityConstants.UPDATE))
            {
                securityMap.put("sundayDeliveryUpdate", "Y");
            }
            else
            {
                securityMap.put("sundayDeliveryUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.MERCURY, SecurityConstants.UPDATE))
            {
                securityMap.put("mercuryUpdate", "Y");
            }
            else
            {
                securityMap.put("mercuryUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.MINIMUM_ORDER, SecurityConstants.UPDATE))
            {
                securityMap.put("minimumOrderUpdate", "Y");
            }
            else
            {
                securityMap.put("minimumOrderUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.HARD_BLOCK, SecurityConstants.UPDATE))
            {
                securityMap.put("hardBlockUpdate", "Y");
            }
            else
            {
                securityMap.put("hardBlockUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.OPTOUT_BLOCK, SecurityConstants.UPDATE))
            {
                securityMap.put("optOutBlockUpdate", "Y");
            }
            else
            {
                securityMap.put("optOutBlockUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.MANAGER_COMMENT, SecurityConstants.VIEW))
            {
                securityMap.put("managerCommentView", "Y");
            }
            else
            {
                securityMap.put("managerCommentView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.MANAGER_COMMENT, SecurityConstants.UPDATE))
            {
                securityMap.put("managerCommentUpdate", "Y");
            }
            else
            {
                securityMap.put("managerCommentUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.ASSOCIATIONS, SecurityConstants.UPDATE))
            {
                securityMap.put("associationsUpdate", "Y");
            }
            else
            {
                securityMap.put("associationsUpdate", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.WEIGHTS, SecurityConstants.VIEW))
            {
                securityMap.put("weightsView", "Y");
            }
            else
            {
                securityMap.put("weightsView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.PERFORMANCE, SecurityConstants.VIEW))
            {
                securityMap.put("performanceView", "Y");
            }
            else
            {
                securityMap.put("performanceView", "N");
            }
            
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.DIRECTORY_CITY, SecurityConstants.UPDATE))
            {
                securityMap.put("directoryCityUpdate", "Y");
            }
            else
            {
                securityMap.put("directoryCityUpdate", "N");
            }
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.SOURCE_FLORIST_PRIORITY, SecurityConstants.VIEW)) {
                securityMap.put("sourceCodeView", "Y");
            }
            else
            {
                securityMap.put("sourceCodeView", "N");
            }
            if (securityManager.assertPermission(context, securityToken, SecurityConstants.FULFILLMENT, SecurityConstants.UPDATE)) {
                securityMap.put("fulfillmentUpdate", "Y");
            }
            else
            {
                securityMap.put("fulfillmentUpdate", "N");
            }

        }
        else
        {
            securityMap.put("profileUpdate", "Y");
            securityMap.put("overrideWeightView", "Y");
            securityMap.put("zipCodesUpdate", "Y");
            securityMap.put("zipCodeListLinkView", "Y");
            securityMap.put("addCodification", "Y");
            securityMap.put("sundayDeliveryUpdate", "Y");
            securityMap.put("mercuryUpdate", "Y");
            securityMap.put("minimumOrderUpdate", "Y");
            securityMap.put("hardBlockUpdate", "Y");
            securityMap.put("optOutBlockUpdate", "Y");
            securityMap.put("managerCommentView", "Y");
            securityMap.put("managerCommentUpdate", "Y");
            securityMap.put("associationsUpdate", "Y");
            securityMap.put("weightsView", "Y");
            securityMap.put("performanceView", "Y");
            securityMap.put("venderFlagUpdate", "N");
            securityMap.put("suspendOverrideUpdate", "Y");
            securityMap.put("directoryCityUpdate", "N");
            securityMap.put("sourceCodeView", "Y");
            securityMap.put("fulfillmentUpdate", "Y");
        }
        
        return securityMap;
	}


}
