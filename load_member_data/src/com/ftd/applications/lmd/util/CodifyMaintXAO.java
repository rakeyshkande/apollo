package com.ftd.applications.lmd.util;
import com.ftd.applications.lmd.vo.CodificationMaintenanceVO;
import com.ftd.applications.lmd.vo.CodificationMasterVO;
import com.ftd.applications.lmd.vo.CodificationProductsVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.util.Iterator;
import javax.servlet.http.HttpServlet;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CodifyMaintXAO extends HttpServlet 
{
    public CodifyMaintXAO()
    {
    }
    
    
    /**
     * This method is the primary method used to convert the florist maintenace 
     * value object structure into XML.
     *   
     * @param florist FloristVO
     * @return maintenanceDocument Document
     * @throws java.lang.Exception
     */
    public Document generateXML(CodificationMaintenanceVO maintVO) throws Exception
    {
        Document codifyDocument = DOMUtil.getDefaultDocument();
        Element tagElement = null;
      
        // Create  root
        Element codifyElement = codifyDocument.createElement("codifyData");
        codifyDocument.appendChild(codifyElement);
        
        // Add general data to document
        codifyElement.appendChild(codifyDocument.importNode(generateGeneralDataXML(maintVO), true));      
        
        // Add codified master to doc
        Element codifiedMasterElement = codifyDocument.createElement("codificationMaster");
        codifyElement.appendChild(codifiedMasterElement);
            
        if(maintVO.getCodificationMasterMap() != null && maintVO.getCodificationMasterMap().size() > 0)
        {
            CodificationMasterVO masterVO = null;
            Iterator codifiedMasterIterator = maintVO.getCodificationMasterMap().values().iterator();
                
            while(codifiedMasterIterator.hasNext())
            {
                masterVO = (CodificationMasterVO) codifiedMasterIterator.next();
                codifiedMasterElement.appendChild(codifyDocument.importNode(generateCodificationMasterXML(masterVO), true));
            }
        }
        
        // Add postal codes to the document
        Element productElement = codifyDocument.createElement("codificationProduct");
        codifyElement.appendChild(productElement);
            
        if(maintVO.getCodificationProductMap() != null && maintVO.getCodificationProductMap().size() > 0)
        {
            CodificationProductsVO product = null;
            Iterator productIterator = maintVO.getCodificationProductMap().values().iterator();
                
            while(productIterator.hasNext())
            {
                product = (CodificationProductsVO) productIterator.next();
                productElement.appendChild(codifyDocument.importNode(generateCodificationProductXML(product), true));
            }
        }
        
        
        return codifyDocument;
    }   
    
    
    
    private Element generateGeneralDataXML(CodificationMaintenanceVO maintVO) throws ParserConfigurationException
    {
        Document generalDataDocument = DOMUtil.getDefaultDocument();
        Element generalDataElement = generalDataDocument.createElement("generalData");
        
        generalDataElement.appendChild(this.createTextNode("lastUpdateUser", maintVO.getLastUpdateUser(), generalDataDocument));

        return generalDataElement;
    }
    
    
    private Element generateCodificationMasterXML(CodificationMasterVO masterVO) throws ParserConfigurationException
    {
        Document codifiedMasterDocument = DOMUtil.getDefaultDocument();
        Element codifiedMasterElement = codifiedMasterDocument.createElement("codification");
        
        codifiedMasterElement.appendChild(this.createTextNode("categoryId", masterVO.getCategory_id(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("codificationId", masterVO.getCodificationID(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("codifyAllFloristFlag", masterVO.getCodifyAllFloristFlag(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("description", masterVO.getDescription(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("loadFromFileFlag", masterVO.getLoadFromFileFlag(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("deleteFlag", masterVO.getDeleteFlag(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("previousCodificationId", masterVO.getPreviousCodificationID(), codifiedMasterDocument));
        codifiedMasterElement.appendChild(this.createTextNode("codificationRequired", masterVO.getCodificationRequired(), codifiedMasterDocument));
        
        return codifiedMasterElement;
    }    
    
    private Element generateCodificationProductXML(CodificationProductsVO productVO) throws ParserConfigurationException
    {
        Document codifiedProductDocument = DOMUtil.getDefaultDocument();
        Element codifiedProductElement = codifiedProductDocument.createElement("codification");
        
        codifiedProductElement.appendChild(this.createTextNode("checkOEflag", productVO.getCheckOEflag(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("codificationId", productVO.getCodificationID(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("deleteFlag", productVO.getDeleteFlag(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("description", productVO.getDescription(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("flagSequence", productVO.getFlagSequence(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("productId", productVO.getProductID(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("productName", productVO.getProductName(), codifiedProductDocument));
        codifiedProductElement.appendChild(this.createTextNode("status", productVO.getStatus(), codifiedProductDocument));

        
        return codifiedProductElement;
    }        
    
    private Element createTextNode(String elementName, Object elementValue, Document document)
    {
        Element tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue.toString()));
        }

        return tagElement;
    }    
}
