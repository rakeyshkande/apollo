package com.ftd.applications.lmd.util;

import com.ftd.applications.lmd.vo.ZipCodeVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.exceptions.ExcelProcessingException;

import java.io.InputStream;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.fileupload.FileItem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;



import org.xml.sax.SAXException;


/** This class contains utility methods used to work with an Excel spreadsheet - XSSF version.
 */
public class LMDSpreadsheetUtil
{
    private static Logger logger = new Logger("com.ftd.applications.lmd.util.LMDSpreadsheetUtil");
    private static LMDSpreadsheetUtil spreadsheetUtil;  
    private List<ZipCodeVO> oscarRowListVO;
   
    /** Private constructor.  
     */
    private LMDSpreadsheetUtil() {
    }

    public static LMDSpreadsheetUtil getInstance() 
    {
       if(spreadsheetUtil == null ){
          spreadsheetUtil = new LMDSpreadsheetUtil();
       }
       return spreadsheetUtil;
    }
  
    public List<ZipCodeVO> processSpreadsheetFile(InputStream uploadedStream)
           throws ExcelProcessingException, Exception
   {
        logger.debug("Entering processSpreadsheetFile...");        
   
        OPCPackage pkg = OPCPackage.open(uploadedStream);
		XSSFReader r = new XSSFReader( pkg );
		SharedStringsTable sst = r.getSharedStringsTable();

		XMLReader parser = fetchSheetParser(sst);

		// rId2 found by processing the Workbook
		// Seems to either be rId# or rSheet#
		Iterator<InputStream> sheets = r.getSheetsData();
		
		InputStream sheet = sheets.next();

		InputSource sheetSource = new InputSource(sheet);
	
		parser.parse(sheetSource);		
	
		sheet.close();
		logger.debug("Processing List size: " + oscarRowListVO.size());
		return oscarRowListVO;
   }
    
    private XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException {
		XMLReader parser =
			XMLReaderFactory.createXMLReader(
					"org.apache.xerces.parsers.SAXParser"
			);
		ContentHandler handler = new SheetHandler(sst);
		parser.setContentHandler(handler);
		return parser;
	}

	/** 
	 * See org.xml.sax.helpers.DefaultHandler javadocs 
	 */
	private static class SheetHandler extends DefaultHandler {
		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;
		private ZipCodeVO oscarRowVO;
		
		private String colRowLabel;// = attributes.getValue("r");
		private String fieldName = "";// = false;
		//end add required variables
		
		private SheetHandler(SharedStringsTable sst) {
			this.sst = sst;
		}		
		
		public void startDocument() throws SAXException 
		{
			LMDSpreadsheetUtil.getInstance().oscarRowListVO = new ArrayList<ZipCodeVO>();
			oscarRowVO = new ZipCodeVO();
		}
		
		public void endDocument() throws SAXException 
		{
			//last row need to be added still.
			LMDSpreadsheetUtil.getInstance().oscarRowListVO.add(oscarRowVO);
			logger.debug("No of rows processed: " + LMDSpreadsheetUtil.getInstance().oscarRowListVO.size());
		}

		public void startElement(String uri, String localName, String name,
				Attributes attributes) throws SAXException {
			
			if(name.equals("row")) {
				if ( (oscarRowVO != null) && ((oscarRowVO.getZipCode() != null) || 
						                       ((oscarRowVO.getCity() != null) && (oscarRowVO.getStateId() != null))) ) 
				{
			        LMDSpreadsheetUtil.getInstance().oscarRowListVO.add(oscarRowVO);
			        oscarRowVO = new ZipCodeVO();
				}				
			}
			
			// c => cell
			if(name.equals("c")) {
				// Print the cell reference
				
				/*start additional code*/								
				colRowLabel = attributes.getValue("r");
				
				fieldName = isRequiredField(colRowLabel);				
		
				
				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if( (cellType != null && cellType.equals("s")) && fieldName != "") {					
					nextIsString = true;
				} else if ((cellType == null) && ( fieldName.equals(ZipCodeVO.FIELD_NAME_ZIP_CODE) || 
						                           fieldName.equals(ZipCodeVO.FIELD_NAME_CITY) || 
						                           fieldName.equals(ZipCodeVO.FIELD_NAME_STATE_ID) ||
						                           fieldName.equals(ZipCodeVO.FIELD_NAME_OSCAR_ZIP) ||
						                           fieldName.equals(ZipCodeVO.FIELD_NAME_OSCAR_CITY_STATE) )) {				
					nextIsString = true;
				}
				else {				
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}
		
		public void endElement(String uri, String localName, String name)
				throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if(nextIsString) {
				int idx = 0;
				try{
					 idx = Integer.parseInt(lastContents);					 
					 lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
				}catch(NumberFormatException nfe){
					
				}catch(IndexOutOfBoundsException ioobe){
					
				}
			}
			    // v => contents of a cell
			    // Output after we've seen the string contents
			if(name.equals("v") && nextIsString) {
			    if(fieldName.equals(ZipCodeVO.FIELD_NAME_ZIP_CODE) && nextIsString) {				    
				    oscarRowVO.setZipCode(lastContents);
			    }
			    else if(fieldName.equals(ZipCodeVO.FIELD_NAME_CITY) && nextIsString) {				
				    oscarRowVO.setCity(lastContents);
			    }
			    else if(fieldName.equals(ZipCodeVO.FIELD_NAME_STATE_ID) && nextIsString) {				
				    oscarRowVO.setStateId(lastContents);
			    }
			    else if(fieldName.equals(ZipCodeVO.FIELD_NAME_OSCAR_ZIP) && nextIsString) {				
				    oscarRowVO.setOscarZipFlag(lastContents);
			    }
			    else if(fieldName.equals(ZipCodeVO.FIELD_NAME_OSCAR_CITY_STATE) && nextIsString) {				
				    oscarRowVO.setOscarCityStateFlag(lastContents);
			    }	
			}
		}

		public void characters(char[] ch, int start, int length)
				throws SAXException {
			lastContents += new String(ch, start, length);
		}
	}
    
	private static String isRequiredField(String colRowPosition){
		
		String firstLetter = "";
		String secondLetter = "";
		String fieldName = "";
		
		firstLetter = colRowPosition.substring(0, 1);
		secondLetter = colRowPosition.substring(1);
		
		try{
			if( (firstLetter.equalsIgnoreCase("A") ||
					firstLetter.equalsIgnoreCase("B") ||
					firstLetter.equalsIgnoreCase("C") ||
					firstLetter.equalsIgnoreCase("D") ||
					firstLetter.equalsIgnoreCase("E") )
					&& 
					Integer.parseInt(secondLetter)>1 ){

				
			
			    if(firstLetter.equalsIgnoreCase("A")){ // column A is zip code
			    	fieldName = ZipCodeVO.FIELD_NAME_ZIP_CODE;
			    }
			    if(firstLetter.equalsIgnoreCase("B")){ // column B is city
			    	fieldName = ZipCodeVO.FIELD_NAME_CITY;
			    }
			    if(firstLetter.equalsIgnoreCase("C")){ // column C is state
			    	fieldName = ZipCodeVO.FIELD_NAME_STATE_ID;
			    }
			    if(firstLetter.equalsIgnoreCase("D")){ // column D is zip flag
			    	fieldName = ZipCodeVO.FIELD_NAME_OSCAR_ZIP;
			    }
			    if(firstLetter.equalsIgnoreCase("E")){ // column E is city/state flag
			    	fieldName = ZipCodeVO.FIELD_NAME_OSCAR_CITY_STATE;
			    }
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		return fieldName;		
	}
}
