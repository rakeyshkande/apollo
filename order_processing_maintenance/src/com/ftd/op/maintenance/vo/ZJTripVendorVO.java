package com.ftd.op.maintenance.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class ZJTripVendorVO extends BaseVO implements XMLInterface
{
  private  long       trip_id;
  private  String     vendor_id;
  private  String     vendor_name;
  private  String     city;
  private  String     address1;
  private  String     state;
  private  String     original_trip_vendor_status_code;
  private  String     new_trip_vendor_status_code;
  private  String     original_order_cutoff_time;
  private  String     new_order_cutoff_time;
  private  double     plt_cubic_inch_allowed_qty;
  private  long       full_plt_qty;
  private  long       full_plt_order_qty;
  private  String     prtl_plt_in_use_flag;
  private  long       prtl_plt_order_qty;
  private  double     prtl_plt_cubic_in_used_qty;
  private  String     created_by;
  private  Date       created_on;
  private  String     updated_by;
  private  Date       updated_on;
  private  String     tvo_reject_printed_exist;

  private  List       tripVendorOrderVOList;

  public ZJTripVendorVO()
  {
    tripVendorOrderVOList   = new ArrayList();
  }

  public void setTripId(long trip_id)
  {
    this.trip_id = trip_id;
  }

  public long getTripId()
  {
    return trip_id;
  }

  public void setVendorId(String vendor_id)
  {
    this.vendor_id = vendor_id;
  }

  public String getVendorId()
  {
    return vendor_id;
  }

  public void setVendorName(String vendor_name)
  {
    this.vendor_name = vendor_name;
  }

  public String getVendorName()
  {
    return vendor_name;
  }

  public void setCity(String city)
  {
    this.city = city;
  }

  public String getCity()
  {
    return city;
  }

  public void setAddress1(String address1)
  {
    this.address1 = address1;
  }

  public String getAddress1()
  {
    return address1;
  }

  public void setState(String state)
  {
    this.state = state;
  }

  public String getState()
  {
    return state;
  }

  public void setOriginalTripVendorStatusCode(String original_trip_vendor_status_code)
  {
    this.original_trip_vendor_status_code = original_trip_vendor_status_code;
  }

  public String getOriginalTripVendorStatusCode()
  {
    return original_trip_vendor_status_code;
  }

  public void setNewTripVendorStatusCode(String new_trip_vendor_status_code)
  {
    this.new_trip_vendor_status_code = new_trip_vendor_status_code;
  }

  public String getNewTripVendorStatusCode()
  {
    return new_trip_vendor_status_code;
  }

  public void setOriginalOrderCutoffTime(String original_order_cutoff_time)
  {
    this.original_order_cutoff_time = original_order_cutoff_time;
  }

  public String getOriginalOrderCutoffTime()
  {
    return original_order_cutoff_time;
  }

  public void setNewOrderCutoffTime(String new_order_cutoff_time)
  {
    this.new_order_cutoff_time = new_order_cutoff_time;
  }

  public String getNewOrderCutoffTime()
  {
    return new_order_cutoff_time;
  }

  public void setPltCubicInchAllowedQty(double plt_cubic_inch_allowed_qty)
  {
    this.plt_cubic_inch_allowed_qty = plt_cubic_inch_allowed_qty;
  }

  public double getPltCubicInchAllowedQty()
  {
    return plt_cubic_inch_allowed_qty;
  }

  public void setFullPltQty(long full_plt_qty)
  {
    this.full_plt_qty = full_plt_qty;
  }

  public long getFullPltQty()
  {
    return full_plt_qty;
  }

  public void setFullPltOrderQty(long full_plt_order_qty)
  {
    this.full_plt_order_qty = full_plt_order_qty;
  }

  public long getFullPltOrderQty()
  {
    return full_plt_order_qty;
  }

  public void setPrtlPltInUseFlag(String prtl_plt_in_use_flag)
  {
    this.prtl_plt_in_use_flag = prtl_plt_in_use_flag;
  }

  public String getPrtlPltInUseFlag()
  {
    return prtl_plt_in_use_flag;
  }

  public void setPrtlPltOrderQty(long prtl_plt_order_qty)
  {
    this.prtl_plt_order_qty = prtl_plt_order_qty;
  }

  public long getPrtlPltOrderQty()
  {
    return prtl_plt_order_qty;
  }

  public void setPrtlPltCubicInUsedQty(double prtl_plt_cubic_in_used_qty)
  {
    this.prtl_plt_cubic_in_used_qty = prtl_plt_cubic_in_used_qty;
  }

  public double getPrtlPltCubicInUsedQty()
  {
    return prtl_plt_cubic_in_used_qty;
  }

  public void setCreatedBy(String created_by)
  {
    this.created_by = created_by;
  }

  public String getCreatedBy()
  {
    return created_by;
  }

  public void setCreatedOn(Date created_on)
  {
    this.created_on = created_on;
  }

  public Date getCreatedOn()
  {
    return created_on;
  }

  public void setUpdatedBy(String updated_by)
  {
    this.updated_by = updated_by;
  }

  public String getUpdatedBy()
  {
    return updated_by;
  }

  public void setUpdatedOn(Date updated_on)
  {
    this.updated_on = updated_on;
  }

  public Date getUpdatedOn()
  {
    return updated_on;
  }

  public void setTripVendorOrderVOList(List tripVendorOrderVOList)
  {
    this.tripVendorOrderVOList = tripVendorOrderVOList;
  }

  public List getTripVendorOrderVOList()
  {
    return tripVendorOrderVOList;
  }

  public void setTVORejectPrintedExist(String tvo_reject_printed_exist)
  {
    this.tvo_reject_printed_exist = tvo_reject_printed_exist;
  }

  public String getTVORejectPrintedExist()
  {
    return tvo_reject_printed_exist;
  }




  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  count
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<TRIP_VENDOR>");
      }
      else
      {
        sb.append("<TRIP_VENDOR num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            if (xmlInt != null)
            {
              int k = i + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</TRIP_VENDOR>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


}
