package com.ftd.op.maintenance.vo;

public class SecondChoiceVO 
{
    private String mercuryDescription;
    private String mercuryHolidayDescription;
    private String oeDescription;
    private String productSecondChoice;
    private String oeHolidayDescription;
    private String updated;

    public SecondChoiceVO()
    {
    }

    public String getMercuryDescription()
    {
        return mercuryDescription;
    }

    public void setMercuryDescription(String mercuryDescription)
    {
        this.mercuryDescription = mercuryDescription;
    }

    public String getMercuryHolidayDescription()
    {
        return mercuryHolidayDescription;
    }

    public void setMercuryHolidayDescription(String mercuryHolidayDescription)
    {
        this.mercuryHolidayDescription = mercuryHolidayDescription;
    }

    public String getOeDescription()
    {
        return oeDescription;
    }

    public void setOeDescription(String oeDescription)
    {
        this.oeDescription = oeDescription;
    }

    public String getProductSecondChoice()
    {
        return productSecondChoice;
    }

    public void setProductSecondChoice(String productSecondChoice)
    {
        this.productSecondChoice = productSecondChoice;
    }



    public String getOeHolidayDescription()
    {
        return oeHolidayDescription;
    }

    public void setOeHolidayDescription(String oeHolidayDescription)
    {
        this.oeHolidayDescription = oeHolidayDescription;
    }

    public String getUpdated()
    {
        return updated;
    }

    public void setUpdated(String updated)
    {
        this.updated = updated;
    }
}