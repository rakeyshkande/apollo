package com.ftd.op.maintenance.vo;


import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.List;
import java.util.Date;

public class CompanyVO
{
  private String  companyId;
  private String  companyName;
  private String  internetOrigin;
  private long    defaultProgramId;
  private String  phoneNumber;
  private String  clearingMemberNumber;
  private String  logoFileName;
  private String  enableLpProcessing;
    private String url;
    
  public CompanyVO()
  {
  }
  
  public void setCompanyId(String companyId)
  {

    this.companyId = trim(companyId);
  }
  
  public String getCompanyId()
  {
    return companyId;
  }
 
  public void setCompanyName(String companyName)
  {

    this.companyName = trim(companyName);
  }
  
  public String getCompanyName()
  {
    return companyName;
  }
  
  public void setInternetOrigin(String internetOrigin)
  {

    this.internetOrigin = trim(internetOrigin);
  }
  
  public String getInternetOrigin()
  {
    return internetOrigin;
  }
  
  public void setDefaultProgramId(long defaultProgramId)
  {

    this.defaultProgramId = defaultProgramId;
  }
  
  public long getDefaultProgramId()
  {
    return defaultProgramId;
  }
  
  public void setPhoneNumber(String phoneNumber)
  {

    this.phoneNumber = phoneNumber;
  }
  
  public String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public void setClearingMemberNumber(String clearingMemberNumber)
  {

    this.clearingMemberNumber = clearingMemberNumber;
  }
  
  public String getClearingMemberNumber()
  {
    return clearingMemberNumber;
  }
  
  public void setLogoFileName(String logoFileName)
  {

    this.logoFileName = logoFileName;
  }
  
  public String getLogoFileName()
  {
    return logoFileName;
  }
  
  public void setEnableLpProcessing(String enableLpProcessing)
  {

    this.enableLpProcessing = trim(enableLpProcessing);
  }
  
  public String getEnableLpProcessing()
  {
    return enableLpProcessing;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public String getURL()
    {
        return url;
    }

    public void setURL(String newUrl)
    {
        this.url = newUrl;
    }
  
}
