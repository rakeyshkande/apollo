package com.ftd.op.maintenance.vo;

public class CustomerVO extends BaseVO
{
    private String customerId;
    private String customerIdHoldFlag;
    private String firstName;
    private String lastName;
    private String nameHoldFlag;
    private String address1;
    private String address2;
    private String city;
    private String zipCode;
    private String state;
    private String country;
    private String addressHoldFlag;

    public CustomerVO()
    {
        nodeName = "customer";
    }

    public String getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(String customerId)
    {
        this.customerId = customerId;
    }

    public String getCustomerIdHoldFlag()
    {
        return customerIdHoldFlag;
    }

    public void setCustomerIdHoldFlag(String customerIdHoldFlag)
    {
        this.customerIdHoldFlag = customerIdHoldFlag;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getNameHoldFlag()
    {
        return nameHoldFlag;
    }

    public void setNameHoldFlag(String nameHoldFlag)
    {
        this.nameHoldFlag = nameHoldFlag;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getAddressHoldFlag()
    {
        return addressHoldFlag;
    }

    public void setAddressHoldFlag(String addressHoldFlag)
    {
        this.addressHoldFlag = addressHoldFlag;
    }
}