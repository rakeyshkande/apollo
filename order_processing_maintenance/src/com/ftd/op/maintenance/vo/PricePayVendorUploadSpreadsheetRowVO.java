package com.ftd.op.maintenance.vo;

import com.ftd.security.cache.vo.*;
import com.ftd.security.exceptions.ExcelProcessingException;

public class PricePayVendorUploadSpreadsheetRowVO extends LineItemVO {

    // attributes common to all instances.
    private static final int COLUMNS = 8;
    private static final String ERROR_REQUIRED_FIELD_MISSING = "Required Field Missing";
    private static final String ERROR_DATA_TOO_LARGE = "Data Too Large";
    private static final String ERROR_INVALID_NUMERIC = "Invalid Numeric Value";

    public static final String FIELD_NAME_PRODUCT_ID = "PRODUCT_ID";
    public static final String FIELD_NAME_VENDOR_NAME = "VENDOR_NAME";
    public static final String FIELD_NAME_VENDOR_ID = "VENDOR_ID";
    public static final String FIELD_NAME_PRODUCT_DESCRIPTION = "PRODUCT_DESCRIPTION";
    public static final String FIELD_NAME_PAY_VENDOR_AMT = "PAY_VENDOR_AMT";
    public static final String FIELD_NAME_STANDARD_PRICE = "STANDARD_PRICE";
    public static final String FIELD_NAME_DELUXE_PRICE = "DELUXE_PRICE";
    public static final String FIELD_NAME_PREMIUM_PRICE = "PREMIUM_PRICE";

  static {
      PricePayVendorUploadSpreadsheetRowVO.setColumnSize(COLUMNS);
  }

  /**
     * Initialize the list of Field. Specifies the Field name, type,
     * column position, max size, and if it's a required field.
     */
  public PricePayVendorUploadSpreadsheetRowVO()
  {
      // create fieldList if it's null.
      super();

      //note that while some of these are required fields, validation will not be done at this point.  
      //this is because the GUI has a textarea for errors for each record, and doing a validation at this 
      //point will result in a USE
      fieldList.add(new Field(FIELD_NAME_PRODUCT_ID, Field.TYPE_STRING, 0, 100, false));
      fieldList.add(new Field(FIELD_NAME_VENDOR_NAME, Field.TYPE_STRING, 1, 100, false));
      fieldList.add(new Field(FIELD_NAME_VENDOR_ID, Field.TYPE_STRING, 2, 100, false));
      fieldList.add(new Field(FIELD_NAME_PRODUCT_DESCRIPTION, Field.TYPE_STRING, 3, 100, false));
      fieldList.add(new Field(FIELD_NAME_PAY_VENDOR_AMT, Field.TYPE_STRING, 4, 100, false));
      fieldList.add(new Field(FIELD_NAME_STANDARD_PRICE, Field.TYPE_STRING, 5, 100, false));
      fieldList.add(new Field(FIELD_NAME_DELUXE_PRICE, Field.TYPE_STRING, 6, 100, false));
      fieldList.add(new Field(FIELD_NAME_PREMIUM_PRICE, Field.TYPE_STRING, 7, 100, false));

  }

  public void validate() throws Exception
  {

      // Get list of all fields.
      for (int i = 0; i < fieldList.size(); i++)
      {
          Field field = (Field) fieldList.get(i);
          boolean isRequired = field.isRequired();
          int maxSize = field.getMaxSize();
          String value = field.getValue();
          String type = field.getType();
          String columnName = field.getName();

          // For each field, validate the following:
          // If the field is required, it's value cannot be null.
          if (isRequired && (value == null || value.length() == 0)) {
              throw new ExcelProcessingException(ERROR_REQUIRED_FIELD_MISSING, getLineNumber() + 1, columnName);
          }
          // Field size cannot be greater than field max size.
          if (value != null && value.length() > maxSize) {
              throw new ExcelProcessingException(ERROR_DATA_TOO_LARGE, getLineNumber() + 1, columnName);
          }
          if (type.equals(Field.TYPE_LONG) && value != null && value.length() > 0) {
              try {
                  Long.parseLong(value);
              } catch (Throwable e) {
                  throw new ExcelProcessingException(ERROR_INVALID_NUMERIC, getLineNumber() + 1, columnName);
              }
          }

      }
  }


}
