package com.ftd.op.maintenance.vo;



public interface XMLInterface 
{

  public String toXML();

  public String toXML(int count);

}