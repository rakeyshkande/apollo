package com.ftd.op.maintenance.vo;

import java.util.Date;

public class VendorProductVO extends BaseVO implements XMLInterface {
	String vendorId;
	String productSubcodeId;
	String vendorSku;
	double vendorCost;
	String available;
	String removed;
	String createdBy;
	Date createdOn;
	String updatedBy;
	Date updatedOn;
	Date vendorCostEffDate;
	String invTrkId;
	String invStatus;
	String isAvailable;	
	String	prodType;

	public VendorProductVO() {
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setProductSubcodeId(String productSubcodeId) {
		this.productSubcodeId = productSubcodeId;
	}

	public String getProductSubcodeId() {
		return productSubcodeId;
	}

	public void setVendorSku(String vendorSku) {
		this.vendorSku = vendorSku;
	}

	public String getVendorSku() {
		return vendorSku;
	}

	public void setVendorCost(double vendorCost) {
		this.vendorCost = vendorCost;
	}

	public double getVendorCost() {
		return vendorCost;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getAvailable() {
		return available;
	}

	public void setRemoved(String removed) {
		this.removed = removed;
	}

	public String getRemoved() {
		return removed;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setVendorCostEffDate(Date vendorCostEffDate) {
		this.vendorCostEffDate = vendorCostEffDate;
	}

	public Date getVendorCostEffDate() {
		return vendorCostEffDate;
	}

	public String getInvTrkId() {
		return invTrkId;
	}

	public void setInvTrkId(String invTrkId) {
		this.invTrkId = invTrkId;
	}

	public String getInvStatus() {
		return invStatus;
	}

	public void setInvStatus(String invStatus) {
		this.invStatus = invStatus;
	}

	public String getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	
	
}
