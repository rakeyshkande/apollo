package com.ftd.op.maintenance.vo;
import java.util.Date;
import java.util.List;

public class InventoryVO 
{
    private String productId;
    private String productName;
    private String productStatus;
    private String memberNumber;
    private String vendorId;
    private String vendorName;
    private Date startDate;
    private Date endDate;
    private String contactNumber;
    private String contactExt;
    private Integer inventoryLevel;
    private Integer inventoryThreshold;
    private List notifications;

    public InventoryVO()
    {
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductStatus()
    {
        return productStatus;
    }

    public void setProductStatus(String productStatus)
    {
        this.productStatus = productStatus;
    }

    public String getMemberNumber()
    {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber)
    {
        this.memberNumber = memberNumber;
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setVendorId(String vendorId)
    {
        this.vendorId = vendorId;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public String getContactNumber()
    {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber)
    {
        this.contactNumber = contactNumber;
    }

    public String getContactExt()
    {
        return contactExt;
    }

    public void setContactExt(String contactExt)
    {
        this.contactExt = contactExt;
    }

    public Integer getInventoryLevel()
    {
        return inventoryLevel;
    }

    public void setInventoryLevel(Integer inventoryLevel)
    {
        this.inventoryLevel = inventoryLevel;
    }

    public Integer getInventoryThreshold()
    {
        return inventoryThreshold;
    }

    public void setInventoryThreshold(Integer inventoryThreshold)
    {
        this.inventoryThreshold = inventoryThreshold;
    }

    public List getNotifications()
    {
        return notifications;
    }

    public void setNotifications(List notifications)
    {
        this.notifications = notifications;
    }


  public void setVendorName(String vendorName)
  {
    this.vendorName = vendorName;
  }


  public String getVendorName()
  {
    return vendorName;
  }



}