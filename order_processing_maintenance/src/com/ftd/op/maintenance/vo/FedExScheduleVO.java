package com.ftd.op.maintenance.vo;

/**
 * The purpose of this class is to be a placeholder for records from
 * table VENUS.FEDEX_GND_SCHEDULE
 */
public class FedExScheduleVO extends BaseVO 
{

  private static final String START_SCHEDULE_XML = "<FE>";
  private static final String END_SCHEDULE_XML = "</FE>";
  private static final String START_FILE_LINE_XML = "<fl>";
  private static final String END_FILE_LINE_XML = "</fl>";
  private static final String START_ORIGIN_ZIP_XML = "<oz>";
  private static final String END_ORIGIN_ZIP_XML = "</oz>";
  private static final String START_ORIGIN_TERM_XML = "<otm>";
  private static final String END_ORIGIN_TERM_XML = "</otm>";
  private static final String START_DEST_ZIP_XML = "<dz>";
  private static final String END_DEST_ZIP_XML = "</dz>";
  private static final String START_DEST_TERM_XML = "<dt>";
  private static final String END_DEST_TERM_XML = "</dt>";
  private static final String START_SERVICE_DAYS_XML = "<sd>";
  private static final String END_SERVICE_DAYS_XML = "</sd>";
  private static final String START_ON_TIME_SERVICE_XML = "<ot>";
  private static final String END_ON_TIME_SERVICE_XML = "</ot>";
  private static final String START_SERVICE_TYPE_XML = "<st>";
  private static final String END_SERVICE_TYPE_XML = "</st>";

  private String originZip;
  private int originTerminal;
  private String destZip;
  private int destTerminal;
  private int serviceDays;
  private double onTimeService;
  private String serviceType;
  private int fileLine;
  
  /**
   * Default contsructor
   */
  public FedExScheduleVO()
  {
  }

  /**
   * Setter for the member variable originZip
   * @param originZip
   */
  public void setOriginZip(String originZip)
  {
    this.originZip = originZip;
  }


  /**
   * Getter for the member variable originZip
   * @return 
   */
  public String getOriginZip()
  {
    return originZip;
  }
  
  /**
   * Setter for the member variable serviceType
   * @param serviceType
   */
  public void setServiceType(String serviceType)
  {
    this.serviceType = serviceType;
  }


  /**
   * Getter for the member variable serviceType
   * @return 
   */
  public String getServiceType()
  {
    return serviceType;
  }


  /**
   * Setter for the member variable originTerminal
   * @param originTerminal
   */
  public void setOriginTerminal(int originTerminal)
  {
    this.originTerminal = originTerminal;
  }


  /**
   * Getter for the member variable originTerminal
   * @return 
   */
  public int getOriginTerminal()
  {
    return originTerminal;
  }


  /**
   * Setter for the member variable destZip
   * @param destZip
   */
  public void setDestZip(String destZip)
  {
    this.destZip = destZip;
  }


  /**
   * Getter for the member variable destZip
   * @return 
   */
  public String getDestZip()
  {
    return destZip;
  }


  /**
   * Setter for the member variable destTerminal
   * @param destTerminal
   */
  public void setDestTerminal(int destTerminal)
  {
    this.destTerminal = destTerminal;
  }


  /**
   * Getter for the member variable destTerminal
   * @return 
   */
  public int getDestTerminal()
  {
    return destTerminal;
  }


  /**
   * Setter for the member variable serviceDays
   * @param serviceDays
   */
  public void setServiceDays(int serviceDays)
  {
    this.serviceDays = serviceDays;
  }


  /**
   * Getter for the member variable serviceDays
   * @return 
   */
  public int getServiceDays()
  {
    return serviceDays;
  }


  /**
   * Setter for the member variable onTimeService
   * @param onTimeService
   */
  public void setOnTimeService(double onTimeService)
  {
    this.onTimeService = onTimeService;
  }


  /**
   * Getter for the member variable onTimeService
   * @return 
   */
  public double getOnTimeService()
  {
    return onTimeService;
  }
  
  /**
   * Setter for the member variable onTimeService
   * @param fileLine
   */
  public void setFileLine(int fileLine)
  {
    this.fileLine = fileLine;
  }


  /**
   * Getter for the member variable fileLine
   * @return 
   */
  public double getFileLine()
  {
    return fileLine;
  }
  
  
  public String toXML()
  {
      StringBuffer xmlString = new StringBuffer();
      xmlString.append(START_SCHEDULE_XML);
      xmlString.append(START_FILE_LINE_XML + fileLine + END_FILE_LINE_XML);
      xmlString.append(START_ORIGIN_ZIP_XML + originZip + END_ORIGIN_ZIP_XML);
      xmlString.append(START_ORIGIN_TERM_XML + originTerminal + END_ORIGIN_TERM_XML);
      xmlString.append(START_DEST_ZIP_XML + destZip + END_DEST_ZIP_XML);
      xmlString.append(START_DEST_TERM_XML + destTerminal + END_DEST_TERM_XML);
      xmlString.append(START_SERVICE_DAYS_XML + serviceDays + END_SERVICE_DAYS_XML);
      xmlString.append(START_ON_TIME_SERVICE_XML + onTimeService + END_ON_TIME_SERVICE_XML);
      xmlString.append(START_SERVICE_TYPE_XML + serviceType + END_SERVICE_TYPE_XML);
      xmlString.append(END_SCHEDULE_XML);
      
      return xmlString.toString();
  }
}