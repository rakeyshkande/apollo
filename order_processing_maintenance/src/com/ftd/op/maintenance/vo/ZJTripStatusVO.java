package com.ftd.op.maintenance.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ZJTripStatusVO extends BaseVO implements XMLInterface, Cloneable
{
  private  String             trip_status_code;
  private  String             trip_status_desc;
  private  String             created_by;
  private  Date               created_on;
  private  String             updated_by;
  private  Date               updated_on;


  public ZJTripStatusVO()
  {
  }

  public Object clone() throws CloneNotSupportedException
  {
      return super.clone();
  }

  public void setTripStatusCode(String trip_status_code)
  {
    this.trip_status_code = trip_status_code;
  }

  public String getTripStatusCode()
  {
    return trip_status_code;
  }

  public void setTripStatusDescription(String trip_status_desc)
  {
    this.trip_status_desc = trip_status_desc;
  }

  public String getTripStatusDescription()
  {
    return trip_status_desc;
  }

  public void setCreatedBy(String created_by)
  {
    this.created_by = created_by;
  }

  public String getCreatedBy()
  {
    return created_by;
  }

  public void setCreatedOn(Date created_on)
  {
    this.created_on = created_on;
  }

  public Date getCreatedOn()
  {
    return created_on;
  }

  public void setUpdatedBy(String updated_by)
  {
    this.updated_by = updated_by;
  }

  public String getUpdatedBy()
  {
    return updated_by;
  }

  public void setUpdatedOn(Date updated_on)
  {
    this.updated_on = updated_on;
  }

  public Date getUpdatedOn()
  {
    return updated_on;
  }

  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  count
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<TRIP_STATUS>");
      }
      else
      {
        sb.append("<TRIP_STATUS num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            if (xmlInt != null)
            {
              int k = i + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</TRIP_STATUS>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


}

