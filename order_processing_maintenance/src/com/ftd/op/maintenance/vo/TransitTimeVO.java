package com.ftd.op.maintenance.vo;

public class TransitTimeVO 
{
    private String shipMethod;
    private int transitTime;

    public TransitTimeVO()
    {
    }

    public String getShipMethod()
    {
        return shipMethod;
    }

    public void setShipMethod(String shipMethod)
    {
        this.shipMethod = shipMethod;
    }

    public int getTransitTime()
    {
        return transitTime;
    }

    public void setTransitTime(int transitTime)
    {
        this.transitTime = transitTime;
    }
}