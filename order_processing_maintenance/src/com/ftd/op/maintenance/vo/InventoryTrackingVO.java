package com.ftd.op.maintenance.vo;

import java.util.Date;

public class InventoryTrackingVO
{
  private long inventoryTrackingId;
  private String productId;
  private String vendorId;
  private Date startDate;
  private Date endDate;
  private long forecastQty;
  private long revisedForecastQty;
  private long warningThresholdQty;
  private long shutdownThresholdQty;
  private String availabilityOptionCode;
  private String comments;
  private String createdBy;
  private Date createdOn;
  private String updatedBy;
  private Date updatedOn;


  public InventoryTrackingVO()
  {
  }


  public void setInventoryTrackingId(long inventoryTrackingId)
  {
    this.inventoryTrackingId = inventoryTrackingId;
  }

  public long getInventoryTrackingId()
  {
    return inventoryTrackingId;
  }

  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  public String getProductId()
  {
    return productId;
  }

  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }

  public String getVendorId()
  {
    return vendorId;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }

  public Date getStartDate()
  {
    return startDate;
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }

  public Date getEndDate()
  {
    return endDate;
  }

  public void setForecastQty(long forecastQty)
  {
    this.forecastQty = forecastQty;
  }

  public long getForecastQty()
  {
    return forecastQty;
  }

  public void setRevisedForecastQty(long revisedForecastQty)
  {
    this.revisedForecastQty = revisedForecastQty;
  }

  public long getRevisedForecastQty()
  {
    return revisedForecastQty;
  }

  public void setWarningThresholdQty(long warningThresholdQty)
  {
    this.warningThresholdQty = warningThresholdQty;
  }

  public long getWarningThresholdQty()
  {
    return warningThresholdQty;
  }

  public void setShutdownThresholdQty(long shutdownThresholdQty)
  {
    this.shutdownThresholdQty = shutdownThresholdQty;
  }

  public long getShutdownThresholdQty()
  {
    return shutdownThresholdQty;
  }

  public void setAvailabilityOptionCode(String availabilityOptionCode)
  {
    this.availabilityOptionCode = availabilityOptionCode;
  }

  public String getAvailabilityOptionCode()
  {
    return availabilityOptionCode;
  }

  public void setComments(String comments)
  {
    this.comments = comments;
  }

  public String getComments()
  {
    return comments;
  }

  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }

  public String getCreatedBy()
  {
    return createdBy;
  }

  public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }

  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedBy()
  {
    return updatedBy;
  }

  public void setUpdatedOn(Date updatedOn)
  {
    this.updatedOn = updatedOn;
  }

  public Date getUpdatedOn()
  {
    return updatedOn;
  }
}
