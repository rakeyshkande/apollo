package com.ftd.op.maintenance.vo;

import java.util.Date;

public class AddonVO {
	private long addOnId;
	private long orderDetailId;
	private String addOnCode;
	private long addOnQuantity;
	private String desciption;
	private Double price;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;

	public AddonVO() {
	}

	public long getAddOnId() {
		return addOnId;
	}

	public void setAddOnId(long addOnId) {
		this.addOnId = addOnId;
	}

	public long getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public String getAddOnCode() {
		return addOnCode;
	}

	public void setAddOnCode(String addOnCode) {
		this.addOnCode = addOnCode;
	}

	public long getAddOnQuantity() {
		return addOnQuantity;
	}

	public void setAddOnQuantity(long addOnQuantity) {
		this.addOnQuantity = addOnQuantity;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
