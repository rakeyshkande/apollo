package com.ftd.op.maintenance.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class ZJInjectionHubVO extends BaseVO implements XMLInterface
{
  private  long       injection_hub_id;
  private  String     city_name;
  private  String     state_master_id;
  private  String     carrier_id;
  private  String     address_desc;
  private  String     zip_code;
  private  String     sds_account_num;
  private  String     ship_point_id;
  private  String     created_by;
  private  Date       created_on;
  private  String     updated_by;
  private  Date       updated_on;

  public ZJInjectionHubVO()
  {
  }

  public void setInjectionHubId(long injection_hub_id)
  {
    this.injection_hub_id = injection_hub_id;
  }

  public long getInjectionHubId()
  {
    return injection_hub_id;
  }

  public void setCityName(String city_name)
  {
    this.city_name = city_name;
  }

  public String getCityName()
  {
    return city_name;
  }

  public void setStateMasterId(String state_master_id)
  {
    this.state_master_id = state_master_id;
  }

  public String getStateMasterId()
  {
    return state_master_id;
  }

  public void setCarrierId(String carrier_id)
  {
    this.carrier_id = carrier_id;
  }

  public String getCarrierId()
  {
    return carrier_id;
  }

  public void setAddressDesc(String address_desc)
  {
    this.address_desc = address_desc;
  }

  public String getAddressDesc()
  {
    return address_desc;
  }

  public void setZipCode(String zip_code)
  {
    this.zip_code = zip_code;
  }

  public String getZipCode()
  {
    return zip_code;
  }

  public void setSdsAccountNum(String sds_account_num)
  {
    this.sds_account_num = sds_account_num;
  }

  public String getSdsAccountNum()
  {
    return sds_account_num;
  }

  public void setShipPointId(String ship_point_id)
  {
    this.ship_point_id = ship_point_id;
  }

  public String getShipPointId()
  {
    return ship_point_id;
  }

  public void setCreatedBy(String created_by)
  {
    this.created_by = created_by;
  }

  public String getCreatedBy()
  {
    return created_by;
  }

  public void setCreatedOn(Date created_on)
  {
    this.created_on = created_on;
  }

  public Date getCreatedOn()
  {
    return created_on;
  }

  public void setUpdatedBy(String updated_by)
  {
    this.updated_by = updated_by;
  }

  public String getUpdatedBy()
  {
    return updated_by;
  }

  public void setUpdatedOn(Date updated_on)
  {
    this.updated_on = updated_on;
  }

  public Date getUpdatedOn()
  {
    return updated_on;
  }

  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  count
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<INJECTION_HUB>");
      }
      else
      {
        sb.append("<INJECTION_HUB num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            if (xmlInt != null)
            {
              int k = i + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</INJECTION_HUB>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


}
