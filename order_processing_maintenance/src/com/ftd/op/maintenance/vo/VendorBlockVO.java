package com.ftd.op.maintenance.vo;
import java.util.Date;

public class VendorBlockVO extends BaseVO
{
    private String vendorId;
    private Date startDate;
    private Date endDate;
    private String globalFlag;
    private String blockType;

    public VendorBlockVO()
    {
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setVendorId(String vendorId)
    {
        this.vendorId = vendorId;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public String getGlobalFlag()
    {
        return globalFlag;
    }

    public void setGlobalFlag(String globalFlag)
    {
        this.globalFlag = globalFlag;
    }

    public String getBlockType()
    {
        return blockType;
    }

    public void setBlockType(String blockType)
    {
        this.blockType = blockType;
    }
}