package com.ftd.op.maintenance.vo;


public class MaintenanceCommentsVO extends BaseVO implements XMLInterface
{

  private String commentType; // String constant that indicates type.  Example: "VENDOR_MAINT" //
  private String commentTypeId; // String that indicates id.  Example : vendor_id //
  private String description;  // the comment //
  public static String VENDOR_MAINT_COMMENT_TYPE = "VENDOR_MAINT";
    private String createdBy;
  
  public MaintenanceCommentsVO()
  {
  }


  public void setCommentType(String commentType)
  {
    this.commentType = commentType;
  }


  public String getCommentType()
  {
    return commentType;
  }


  public void setCommentTypeId(String commentTypeId)
  {
    this.commentTypeId = commentTypeId;
  }


  public String getCommentTypeId()
  {
    return commentTypeId;
  }


  public void setDescription(String description)
  {
    this.description = description;
  }


  public String getDescription()
  {
    return description;
  }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }



}