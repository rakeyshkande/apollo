package com.ftd.op.maintenance.vo;

public class PricePayVendorVO {

  private int batchId;
  private int spreadsheetRowNum;
  private String productId;
  private String vendorId;
  private String vendorCost;
  private String standardPrice;
  private String deluxePrice;
  private String premiumPrice;
  private String processedFlag;
  private String errorMessage;
  private double vendorCostVariancePct;
  private double standardPriceVariancePct;
  private double deluxePriceVariancePct;
  private double premiumPriceVariancePct;
  private String userId;

  public PricePayVendorVO()
  {
  }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    public int getBatchId() {
        return batchId;
    }

    public void setSpreadsheetRowNum(int spreadsheetRowNum) {
        this.spreadsheetRowNum = spreadsheetRowNum;
    }

    public int getSpreadsheetRowNum() {
        return spreadsheetRowNum;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorCost(String vendorCost) {
        this.vendorCost = vendorCost;
    }

    public String getVendorCost() {
        return vendorCost;
    }

    public void setStandardPrice(String standardPrice) {
        this.standardPrice = standardPrice;
    }

    public String getStandardPrice() {
        return standardPrice;
    }

    public void setDeluxePrice(String deluxePrice) {
        this.deluxePrice = deluxePrice;
    }

    public String getDeluxePrice() {
        return deluxePrice;
    }

    public void setPremiumPrice(String premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    public String getPremiumPrice() {
        return premiumPrice;
    }

    public void setProcessedFlag(String processedFlag) {
        this.processedFlag = processedFlag;
    }

    public String getProcessedFlag() {
        return processedFlag;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setVendorCostVariancePct(double vendorCostVariancePct) {
        this.vendorCostVariancePct = vendorCostVariancePct;
    }

    public double getVendorCostVariancePct() {
        return vendorCostVariancePct;
    }

    public void setStandardPriceVariancePct(double standardPriceVariancePct) {
        this.standardPriceVariancePct = standardPriceVariancePct;
    }

    public double getStandardPriceVariancePct() {
        return standardPriceVariancePct;
    }

    public void setDeluxePriceVariancePct(double deluxePriceVariancePct) {
        this.deluxePriceVariancePct = deluxePriceVariancePct;
    }

    public double getDeluxePriceVariancePct() {
        return deluxePriceVariancePct;
    }

    public void setPremiumPriceVariancePct(double premiumPriceVariancePct) {
        this.premiumPriceVariancePct = premiumPriceVariancePct;
    }

    public double getPremiumPriceVariancePct() {
        return premiumPriceVariancePct;
    }
}
