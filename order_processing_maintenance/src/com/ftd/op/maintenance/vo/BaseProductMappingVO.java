package com.ftd.op.maintenance.vo;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class BaseProductMappingVO implements  Comparable<BaseProductMappingVO>{
	
	private String productPricePointName;
	private String productPricePoint;
    private String vendorSKUs;
    private String pricePointNameAndPrice;
    private boolean isProductAddon;

    public String getPricePointNameAndPrice() 
    {	
    	if(productPricePoint != null)
    	{
    		if(isProductAddon)
    			pricePointNameAndPrice = this.productPricePoint;
    		else
    			pricePointNameAndPrice = this.productPricePointName + " - " + this.productPricePoint;
    	}
		return pricePointNameAndPrice;
	}

    public String getProductPricePointName() {
        return productPricePointName;
    }

    public void setProductPricePointName(String productPricePointName) {
        this.productPricePointName = productPricePointName;
    }

	public String getVendorSKUs() {
		return vendorSKUs;
	}

	public void setVendorSKUs(String vendorSKUs) {
		this.vendorSKUs = vendorSKUs;
	}

    public String getProductPricePoint() {
        return productPricePoint;
    }

    public void setProductPricePoint(String productPricePoint) {
        this.productPricePoint = productPricePoint;
    }    

    public boolean getIsProductAddon() {
        return isProductAddon;
    }

    public void setIsProductAddon(boolean isProductAddon) {
        this.isProductAddon = isProductAddon;
    }
    
   	public int compareTo(BaseProductMappingVO compare1)
   	{
    		List<String> definedOrder = // define your custom order
    			Arrays.asList("BEST","BETTER","GOOD");

    		return Integer.valueOf(
    				definedOrder.indexOf(compare1.getProductPricePointName()))
    				.compareTo(
    						Integer.valueOf(
    								definedOrder.indexOf(this.productPricePointName)));

   	}		

}
