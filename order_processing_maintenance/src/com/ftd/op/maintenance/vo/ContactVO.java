package com.ftd.op.maintenance.vo;

public class ContactVO 
{
    private String vendorId;
    private String name;
    private String phone;
    private String phoneExt;
    private String fax;
    private String faxExt;
    private String email;
    private String primaryContact; // Y or N

    public ContactVO()
    {
    }

    public void setVendorId(String vendorId)
    {
      this.vendorId = vendorId;
    }
  
  
    public String getVendorId()
    {
      return vendorId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhoneExt()
    {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt)
    {
        this.phoneExt = phoneExt;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getFaxExt()
    {
        return faxExt;
    }

    public void setFaxExt(String faxExt)
    {
        this.faxExt = faxExt;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }


  public void setPrimaryContact(String primaryContact)
  {
    this.primaryContact = primaryContact;
  }


  public String getPrimaryContact()
  {
    return primaryContact;
  }


}