package com.ftd.op.maintenance.vo;

public class OccasionVO 
{
    private String description;
    private String displayOrder;
    private String occasionId;
    String active;
    String updated;

    public OccasionVO()
    {
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(String displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public String getOccasionId()
    {
        return occasionId;
    }

    public void setOccasionId(String occasionId)
    {
        this.occasionId = occasionId;
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public String getUpdated()
    {
        return updated;
    }

    public void setUpdated(String updated)
    {
        this.updated = updated;
    }
}