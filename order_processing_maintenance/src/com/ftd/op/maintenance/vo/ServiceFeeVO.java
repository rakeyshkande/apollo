package com.ftd.op.maintenance.vo;

public class ServiceFeeVO 
{
    private String serviceFeeId;
    private String description;
    private double domesticFee;
    private double sameDayUpcharge;
    private double internationalFee;
    private String updatedBy;
    private String requestedBy;
    private double vendorCharge;
    private double vendorSatUpcharge;
    private double vendorSunUpcharge;
    private double vendorMonUpcharge;
    private double sameDayUpchargeFS;
    
    
    public ServiceFeeVO()
    {
    }

    public String getServiceFeeId()
    {
        return serviceFeeId;
    }

    public void setServiceFeeId(String serviceFeeId)
    {
        this.serviceFeeId = serviceFeeId;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public double getDomesticFee()
    {
        return domesticFee;
    }

    public void setDomesticFee(double domesticFee)
    {
        this.domesticFee = domesticFee;
    }

    public double getInternationalFee()
    {
        return internationalFee;
    }

    public void setInternationalFee(double internationalFee)
    {
        this.internationalFee = internationalFee;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRequestedBy() {
        return requestedBy;
    }
    
    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public void setVendorCharge(double vendorCharge) {
        this.vendorCharge = vendorCharge;
    }

    public double getVendorCharge() {
        return vendorCharge;
    }

    public void setVendorSatUpcharge(double vendorSatUpcharge) {
        this.vendorSatUpcharge = vendorSatUpcharge;
    }

    public double getVendorSatUpcharge() {
        return vendorSatUpcharge;
    }

	public void setSameDayUpcharge(double sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}

	public double getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public double getVendorSunUpcharge() {
		return vendorSunUpcharge;
	}

	public void setVendorSunUpcharge(double vendorSunUpcharge) {
		this.vendorSunUpcharge = vendorSunUpcharge;
	}

	public double getVendorMonUpcharge() {
		return vendorMonUpcharge;
	}

	public void setVendorMonUpcharge(double vendorMonUpcharge) {
		this.vendorMonUpcharge = vendorMonUpcharge;
	}	
	
	public void setSameDayUpchargeFS(double sameDayUpchargeFS) {
		this.sameDayUpchargeFS = sameDayUpchargeFS;
	}

	public double getSameDayUpchargeFS() {
		return sameDayUpchargeFS;
	}
	
}
