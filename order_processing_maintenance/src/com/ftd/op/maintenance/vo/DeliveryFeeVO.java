package com.ftd.op.maintenance.vo;

/**
 * @author smeka
 *
 */
public class DeliveryFeeVO {
	
	private String id;
	
	private String description;
	
	private String requestedBy;
	
	private double deliveryFee;
	
	private String isDeleted;
	
	private String updatedBy;	

	private String createdBy;
	
	private int deliveryFeeType;	

	/** Returns delivery fee id
	 * @return
	 */
	public String getId() {
		return id;
	}

	/** Sets the delivery fee Id
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/** Returns the delivery fee description
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/** Sets the delivery fee description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/** Returns the delivery fee Requested by info
	 * @return
	 */
	public String getRequestedBy() {
		return requestedBy;
	}

	/** Sets the delivery fee Requested by info
	 * @param requestedBy
	 */
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	/** Returns the delivery fee value for a given delivery fee type.
	 * @return
	 */
	public double getDeliveryFee() {
		return deliveryFee;
	}

	/** Sets the delivery fee value for a given delivery fee type.
	 * @param deliveryFee
	 */
	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	/** Returns String [Y/N] to indicate that the delivery fee id deleted.
	 * @return
	 */
	public String getIsDeleted() {
		return isDeleted;
	}
	
	/** Sets a string to indicate if the delivery fee is deleted
	 * @param isDeleted
	 */
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	/** Returns the userId who updated the delivery fee info
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/** Sets the userId who is updating the delivery fee info
	 * @param updatedBy
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/** Returns the userId who created the delivery fee info
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/** Sets the userId who is creating the delivery fee info
	 * @param createdBy
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	/** Returns the delivery fee type. Delivery fee type Constants are maintained in OPMConstants.
	 * @return
	 */
	public int getDeliveryFeeType() {
		return deliveryFeeType;
	}

	/** Sets the delivery fee type.
	 * @param deliveryFeeType
	 */
	public void setDeliveryFeeType(int deliveryFeeType) {
		this.deliveryFeeType = deliveryFeeType;
	}
}
