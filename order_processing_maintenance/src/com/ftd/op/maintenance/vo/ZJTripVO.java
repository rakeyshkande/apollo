package com.ftd.op.maintenance.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class ZJTripVO extends BaseVO implements XMLInterface, Cloneable
{
  private  long               trip_id;
  private  String             trip_origination_desc;
  private  long               injection_hub_id;
  private  String             delivery_date;
  private  String             departure_date;
  private  String             actual_trip_status_code;
  private  String             virtual_trip_status_code;
  private  long               sort_code_seq;
  private  String             sort_code_state_master_id;
  private  String             sort_code_date_mmdd;
  private  String             sort_code;
  private  long               total_plt_qty;
  private  long               ftd_plt_qty;
  private  long               third_party_plt_qty;
  private  long               ftd_plt_used_qty;
  private  String             created_by;
  private  Date               created_on;
  private  String             updated_by;
  private  Date               updated_on;
  private  String             tv_reject_printed_exist;
  private  String             tv_printed_exist;
  private  String             max_order_cutoff_time;
  private  long               total_order_on_truck;
  private  long               days_in_transit_qty;

  private  ZJInjectionHubVO   injectionHubVO;
  private  List               tripVendorVOList;


  public ZJTripVO()
  {
    tripVendorVOList   = new ArrayList();
  }

  public Object clone() throws CloneNotSupportedException
  {
      return super.clone();
  }

  public void setTripId(long trip_id)
  {
    this.trip_id = trip_id;
  }

  public long getTripId()
  {
    return trip_id;
  }

  public void setTripOriginationDesc(String trip_origination_desc)
  {
    this.trip_origination_desc = trip_origination_desc;
  }

  public String getTripOriginationDesc()
  {
    return trip_origination_desc;
  }

  public void setInjectionHubId(long injection_hub_id)
  {
    this.injection_hub_id = injection_hub_id;
  }

  public long getInjectionHubId()
  {
    return injection_hub_id;
  }

  public void setDeliveryDate(String delivery_date)
  {
    this.delivery_date = delivery_date;
  }

  public String getDeliveryDate()
  {
    return delivery_date;
  }

  public void setDepartureDate(String departure_date)
  {
    this.departure_date = departure_date;
  }

  public String getDepartureDate()
  {
    return departure_date;
  }
  
  public void setActualTripStatusCode(String actual_trip_status_code)
  {
    this.actual_trip_status_code = actual_trip_status_code;
  }

  public String getActualTripStatusCode()
  {
    return actual_trip_status_code;
  }

  public void setVirtualTripStatusCode(String virtual_trip_status_code)
  {
    this.virtual_trip_status_code = virtual_trip_status_code;
  }

  public String getVirtualTripStatusCode()
  {
    return virtual_trip_status_code;
  }

  public void setSortCodeSeq(long sort_code_seq)
  {
    this.sort_code_seq = sort_code_seq;
  }

  public long getSortCodeSeq()
  {
    return sort_code_seq;
  }

  public void setSortCodeStateMasterId(String sort_code_state_master_id)
  {
    this.sort_code_state_master_id = sort_code_state_master_id;
  }

  public String getSortCodeStateMasterId()
  {
    return sort_code_state_master_id;
  }

  public void setSortCodeDateMMDD(String sort_code_date_mmdd)
  {
    this.sort_code_date_mmdd = sort_code_date_mmdd;
  }

  public String getSortCodeDateMMDD()
  {
    return sort_code_date_mmdd;
  }

  public void setSortCode(String sort_code)
  {
    this.sort_code = sort_code;
  }

  public String getSortCode()
  {
    return sort_code;
  }
  public void setTotalPltQty(long total_plt_qty)
  {
    this.total_plt_qty = total_plt_qty;
  }

  public long getTotalPltQty()
  {
    return total_plt_qty;
  }

  public void setDaysInTransitQty(long days_in_transit_qty)
  {
    this.days_in_transit_qty = days_in_transit_qty;
  }

  public long getDaysInTransitQty()
  {
    return days_in_transit_qty;
  }

  public void setFtdPltQty(long ftd_plt_qty)
  {
    this.ftd_plt_qty = ftd_plt_qty;
  }

  public long getFtdPltQty()
  {
    return ftd_plt_qty;
  }

  public void setThirdPartyPltQty(long third_party_plt_qty)
  {
    this.third_party_plt_qty = third_party_plt_qty;
  }

  public long getThirdPartyPltQty()
  {
    return third_party_plt_qty;
  }

  public void setFtdPltUsedQty(long ftd_plt_used_qty)
  {
    this.ftd_plt_used_qty = ftd_plt_used_qty;
  }

  public long getFtdPltUsedQty()
  {
    return ftd_plt_used_qty;
  }

  public void setCreatedBy(String created_by)
  {
    this.created_by = created_by;
  }

  public String getCreatedBy()
  {
    return created_by;
  }

  public void setCreatedOn(Date created_on)
  {
    this.created_on = created_on;
  }

  public Date getCreatedOn()
  {
    return created_on;
  }

  public void setUpdatedBy(String updated_by)
  {
    this.updated_by = updated_by;
  }

  public String getUpdatedBy()
  {
    return updated_by;
  }

  public void setUpdatedOn(Date updated_on)
  {
    this.updated_on = updated_on;
  }

  public Date getUpdatedOn()
  {
    return updated_on;
  }

  public void setTripVendorVOList(List tripVendorVOList)
  {
    this.tripVendorVOList = tripVendorVOList;
  }

  public List getTripVendorVOList()
  {
    return tripVendorVOList;
  }

  public void setTVRejectPrintedExist(String tv_reject_printed_exist)
  {
    this.tv_reject_printed_exist = tv_reject_printed_exist;
  }

  public String getTVRejectPrintedExist()
  {
    return tv_reject_printed_exist;
  }

  public void setTVPrintedExist(String tv_printed_exist)
  {
    this.tv_printed_exist = tv_printed_exist;
  }

  public String getTVPrintedExist()
  {
    return tv_printed_exist;
  }

  public void setInjectionHubVO(ZJInjectionHubVO injectionHubVO)
  {
    this.injectionHubVO = injectionHubVO;
  }

  public ZJInjectionHubVO getInjectionHubVO()
  {
    return injectionHubVO;
  }

  public void setMaxOrderCutoffTime(String max_order_cutoff_time)
  {
    this.max_order_cutoff_time = max_order_cutoff_time;
  }

  public String getMaxOrderCutoffTime()
  {
    return max_order_cutoff_time;
  }

  public void setTotalOrderOnTruck(long total_order_on_truck)
  {
    this.total_order_on_truck = total_order_on_truck;
  }

  public long getTotalOrderOnTruck()
  {
    return total_order_on_truck;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  count
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<TRIP>");
      }
      else
      {
        sb.append("<TRIP num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            if (xmlInt != null)
            {
              int k = i + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</TRIP>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


}
