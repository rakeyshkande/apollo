package com.ftd.op.maintenance.vo;

import com.ftd.osp.utilities.vo.AddOnVO;

import java.util.List;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;

public class VendorMasterVO extends BaseVO implements XMLInterface
{
    private String VendorID;
    private String VendorCode;
    private String VendorName;
    private String Cutoff;
    private String Address1;
    private String Address2;
    private String City;
    private String State;
    private String ShipVia;
    private String LeadDays;
    private String CompanyCode;
    private String VendorType;
    private String zipCode;
    private String generalInfo;
    private String accountsPayableId;
    private String glAccountNumber;
    private ContactVO mainContact;
    private ContactVO altContact;
    private List partnerShippingAccounts;
    private String zoneJumpEligibleFlag;
    private String phone;
    private String email;
    private String daysBlocked;
    private String newShippingSystem;
    private HashSet <String> vendorAddOnAssignedSet = new HashSet <String> ();
    private HashSet <String> vendorAddOnDisabledSet = new HashSet <String> ();
    private HashSet <String> vendorAddOnChangedSet = new HashSet <String> ();
    private HashMap <String, ArrayList<AddOnVO>> addOnMap;

    public VendorMasterVO()
    {
    }

    public String getVendorID()
    {
        return VendorID;
    }

    public void setVendorID(String VendorID)
    {
        this.VendorID = VendorID;
    }

    public String getVendorCode()
    {
        return VendorCode;
    }

    public void setVendorCode(String VendorCode)
    {
        this.VendorCode = VendorCode;
    }

    public String getVendorName()
    {
        return VendorName;
    }

    public void setVendorName(String VendorName)
    {
        this.VendorName = VendorName;
    }


    public String getCutoff()
    {
        return Cutoff;
    }

    public void setCutoff(String Cutoff)
    {
        this.Cutoff = Cutoff;
    }

    public String getAddress1()
    {
        return Address1;
    }

    public void setAddress1(String Address1)
    {
        this.Address1 = Address1;
    }

    public String getAddress2()
    {
        return Address2;
    }

    public void setAddress2(String Address2)
    {
        this.Address2 = Address2;
    }



    public String getCity()
    {
        return City;
    }

    public void setCity(String City)
    {
        this.City = City;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String State)
    {
        this.State = State;
    }

    public String getShipVia()
    {
        return ShipVia;
    }

    public void setShipVia(String ShipVia)
    {
        this.ShipVia = ShipVia;
    }

    public String getLeadDays()
    {
        return LeadDays;
    }

    public void setLeadDays(String LeadDays)
    {
        this.LeadDays = LeadDays;
    }

    public String getCompanyCode()
    {
        return CompanyCode;
    }

    public void setCompanyCode(String CompanyCode)
    {
        this.CompanyCode = CompanyCode;
    }

    public String getVendorType()
    {
        return VendorType;
    }

    public void setVendorType(String VendorType)
    {
        this.VendorType = VendorType;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getGeneralInfo()
    {
        return generalInfo;
    }

    public void setGeneralInfo(String generalInfo)
    {
        this.generalInfo = generalInfo;
    }

    public ContactVO getMainContact()
    {
        return mainContact;
    }

    public void setMainContact(ContactVO mainContact)
    {
        this.mainContact = mainContact;
    }

    public ContactVO getAltContact()
    {
        return altContact;
    }

    public void setAltContact(ContactVO altContact)
    {
        this.altContact = altContact;
    }


  public void setAccountsPayableId(String accountsPayableId)
  {
    this.accountsPayableId = accountsPayableId;
  }


  public String getAccountsPayableId()
  {
    return accountsPayableId;
  }


  public void setGlAccountNumber(String glAccountNumber)
  {
    this.glAccountNumber = glAccountNumber;
  }


  public String getGlAccountNumber()
  {
    return glAccountNumber;
  }

    public void setPartnerShippingAccounts(List partnerShippingAccounts) {
        this.partnerShippingAccounts = partnerShippingAccounts;
    }

    public List getPartnerShippingAccounts() {
        return partnerShippingAccounts;
    }
    
    public void setZoneJumpEligibleFlag(String zoneJumpEligibleFlag)
    {
      this.zoneJumpEligibleFlag = zoneJumpEligibleFlag;
    }


    public String getZoneJumpEligibleFlag()
    {
      return zoneJumpEligibleFlag;
    }

  public void setPhone(String phone)
  {
    this.phone = phone;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getEmail()
  {
    return email;
  }

  public void setDaysBlocked(String daysBlocked)
  {
    this.daysBlocked = daysBlocked;
  }

  public String getDaysBlocked()
  {
    return daysBlocked;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  count
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<VendorVO>");
      }
      else
      {
        sb.append("<VendorVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            if (xmlInt != null)
            {
              int k = i + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</VendorVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }
    public HashMap<String, ArrayList<AddOnVO>> getAddOnMap()
    {
        return addOnMap;
    }

    public void setAddOnMap(HashMap<String, ArrayList<AddOnVO>> addOnMap)
    {
        this.addOnMap = addOnMap;
    }

    public HashSet<String> getVendorAddOnAssignedSet()
    {
        return vendorAddOnAssignedSet;
    }

    public void setVendorAddOnAssignedSet(HashSet<String> vendorAddOnAssignedSet)
    {
        this.vendorAddOnAssignedSet = vendorAddOnAssignedSet;
    }

    public HashSet<String> getVendorAddOnDisabledSet()
    {
        return vendorAddOnDisabledSet;
    }

    public void setVendorAddOnDisabledSet(HashSet<String> vendorAddOnDisabledSet)
    {
        this.vendorAddOnDisabledSet = vendorAddOnDisabledSet;
    }

    public HashSet<String> getVendorAddOnChangedSet()
    {
        return vendorAddOnChangedSet;
    }

    public void setVendorAddOnChangedSet(HashSet<String> vendorAddOnChangedSet)
    {
        this.vendorAddOnChangedSet = vendorAddOnChangedSet;
    }

	public String getNewShippingSystem() {
		return newShippingSystem;
	}

	public void setNewShippingSystem(String newShippingSystem) {
		this.newShippingSystem = newShippingSystem;
	}
}
