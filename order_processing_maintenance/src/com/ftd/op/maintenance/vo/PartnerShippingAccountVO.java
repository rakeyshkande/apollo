package com.ftd.op.maintenance.vo;

public class PartnerShippingAccountVO extends BaseVO
{
    String vendorId;
    String carrierId;
    String partnerId;
    String accountNum;
    String validationRegex;

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setValidationRegex(String validationRegex) {
        this.validationRegex = validationRegex;
    }

    public String getValidationRegex() {
        return validationRegex;
    }
}
