package com.ftd.op.maintenance.vo;

import java.util.Date;

public class ServiceFeeOverrideVO 
{
    private String serviceFeeId;
    private Date overrideDate;
    private double domesticFee;
    private double sameDayUpcharge;
    private double internationalFee;
    private String requestedBy;
    private String updatedBy;
    private double vendorCharge;
    private double vendorSatUpcharge;    
    private String description; // for feed
    private double vendorSunUpcharge;
    private double vendorMonUpcharge;
    private double sameDayUpchargeFS;

    public ServiceFeeOverrideVO()
    {
    }

    public String getServiceFeeId()
    {
        return serviceFeeId;
    }

    public void setServiceFeeId(String serviceFeeId)
    {
        this.serviceFeeId = serviceFeeId;
    }
    
    public Date getOverrideDate() {
        return overrideDate;
    }

    public void setOverrideDate(Date overrideDate) {
        this.overrideDate = overrideDate;
    }
    
    public double getDomesticFee()
    {
        return domesticFee;
    }

    public void setDomesticFee(double domesticFee)
    {
        this.domesticFee = domesticFee;
    }

    public double getInternationalFee()
    {
        return internationalFee;
    }

    public void setInternationalFee(double internationalFee)
    {
        this.internationalFee = internationalFee;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setVendorCharge(double vendorCharge) {
        this.vendorCharge = vendorCharge;
    }

    public double getVendorCharge() {
        return vendorCharge;
    }

    public void setVendorSatUpcharge(double vendorSatUpcharge) {
        this.vendorSatUpcharge = vendorSatUpcharge;
    }

    public double getVendorSatUpcharge() {
        return vendorSatUpcharge;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

	public void setSameDayUpcharge(double sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}

	public double getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public double getVendorSunUpcharge() {
		return vendorSunUpcharge;
	}

	public void setVendorSunUpcharge(double vendorSunUpcharge) {
		this.vendorSunUpcharge = vendorSunUpcharge;
	}

	public double getVendorMonUpcharge() {
		return vendorMonUpcharge;
	}

	public void setVendorMonUpcharge(double vendorMonUpcharge) {
		this.vendorMonUpcharge = vendorMonUpcharge;
	}	
	public void setSameDayUpchargeFS(double sameDayUpchargeFS) {
		this.sameDayUpchargeFS = sameDayUpchargeFS;
	}

	public double getSameDayUpchargeFS() {
		return sameDayUpchargeFS;
	}
}
