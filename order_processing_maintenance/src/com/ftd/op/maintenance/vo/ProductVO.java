package com.ftd.op.maintenance.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class ProductVO extends BaseVO
{
  private String productId;
  private String novatorId;
  private String productName;
  private String novatorName;
  private String status;
  private String deliveryType;
  private String category;
  private String productType;
  private String productSubType;
  private String colorSizeFlag;
  private double standardPrice;
  private double deluxePrice;
  private double premiumPrice;
  private double preferredPricePoint;
  private double variablePriceMax;
  private String shortDescription;
  private String longDescription;
  private String floristReferenceNumber;
  private String mercuryDescription;
  private String itemComments;
  private String addOnBalloonsFlag;
  private String addOnBearsFlag;
  private String addOnCardsFlag;
  private String addOnFuneralFlag;
  private String codifiedFlag;
  private String exceptionCode;
  private Date exceptionStartDate; 
  private Date exceptionEndDate;
  private String exceptionMessage;
  private String vendorId;
  private double vendorCost;
  private String vendorSku;
  private String secondChoiceCode;
  private String holidaySecondChoiceCode;
  private String dropshipCode;
  private String discountAllowedFlag;
  private String deliveryIncludedFlag;
  private String taxFlag;
  private String serviceFeeFlag;
  private String exoticFlag;
  private String egiftFlag;
  private String countryId;
  private String arrangementSize;
  private String arrangementColors;
  private String dominantFlowers;
  private String searchPriority;
  private String recipe;
  private String subcodeFlag;
  private String dimWeight;
  private String nextDayUpgradeFlag;
  private String corporateSite;
  private String unspscCode;
  private String priceRank1;
  private String priceRank2;
  private String priceRank3;
  private String shipMethodCarrier;
  private String shipMethodFlorist;
  private String shippingKey;
  private String variablePriceFlag;
  private String holidaySku;
  private double holidayPrice;
  private String catalogFlag;
  private double holidayDeluxePrice;
  private double holidayPremiumPrice;
  private Date holidayStartDate;
  private Date holidayEndDate;
  private String holdUntilAvailable;
  private String mondayDeliveryFreshcut;
  private String twoDaySatFreshcut;
  
  //????
  private String companyId;
  private String defaultCarrier;
  private long inventoryLevel;
  private int vendorType;
  private String personalizationTemplateOrder;
  private String updatedBy; 
   
  public ProductVO()
  {
  }
  
  public void setProductId(String productId)
  {

    this.productId = productId;
  }


  public String getProductId()
  {
    return productId;
  }


  public void setNovatorId(String novatorId)
  {

    this.novatorId = novatorId;
  }


  public String getNovatorId()
  {
    return novatorId;
  }


  public void setProductName(String productName)
  {

    this.productName = productName;
  }


  public String getProductName()
  {
    return productName;
  }


  public void setNovatorName(String novatorName)
  {

    this.novatorName = novatorName;
  }


  public String getNovatorName()
  {
    return novatorName;
  }


  public void setStatus(String status)
  {

    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setDeliveryType(String deliveryType)
  {

    this.deliveryType = deliveryType;
  }


  public String getDeliveryType()
  {
    return deliveryType;
  }


  public void setCategory(String category)
  {

    this.category = category;
  }


  public String getCategory()
  {
    return category;
  }


  public void setProductType(String productType)
  {

    this.productType = productType;
  }


  public String getProductType()
  {
    return productType;
  }


  public void setProductSubType(String productSubType)
  {

    this.productSubType = productSubType;
  }


  public String getProductSubType()
  {
    return productSubType;
  }


  public void setColorSizeFlag(String colorSizeFlag)
  {

    this.colorSizeFlag = colorSizeFlag;
  }


  public String getColorSizeFlag()
  {
    return colorSizeFlag;
  }


  public void setStandardPrice(double standardPrice)
  {
    this.standardPrice = standardPrice;
  }


  public double getStandardPrice()
  {
    return standardPrice;
  }


  public void setDeluxePrice(double deluxePrice)
  {
    this.deluxePrice = deluxePrice;
  }


  public double getDeluxePrice()
  {
    return deluxePrice;
  }


  public void setPremiumPrice(double premiumPrice)
  {
    this.premiumPrice = premiumPrice;
  }


  public double getPremiumPrice()
  {
    return premiumPrice;
  }


  public void setPreferredPricePoint(double preferredPricePoint)
  {
    this.preferredPricePoint = preferredPricePoint;
  }


  public double getPreferredPricePoint()
  {
    return preferredPricePoint;
  }


  public void setVariablePriceMax(double variablePriceMax)
  {
    this.variablePriceMax = variablePriceMax;
  }


  public double getVariablePriceMax()
  {
    return variablePriceMax;
  }


  public void setShortDescription(String shortDescription)
  {

    this.shortDescription = shortDescription;
  }


  public String getShortDescription()
  {
    return shortDescription;
  }


  public void setLongDescription(String longDescription)
  {

    this.longDescription = longDescription;
  }


  public String getLongDescription()
  {
    return longDescription;
  }


  public void setFloristReferenceNumber(String floristReferenceNumber)
  {

    this.floristReferenceNumber = floristReferenceNumber;
  }


  public String getFloristReferenceNumber()
  {
    return floristReferenceNumber;
  }


  public void setMercuryDescription(String mercuryDescription)
  {

    this.mercuryDescription = mercuryDescription;
  }


  public String getMercuryDescription()
  {
    return mercuryDescription;
  }


  public void setItemComments(String itemComments)
  {

    this.itemComments = itemComments;
  }


  public String getItemComments()
  {
    return itemComments;
  }


  public void setAddOnBalloonsFlag(String addOnBalloonsFlag)
  {

    this.addOnBalloonsFlag = addOnBalloonsFlag;
  }


  public String getAddOnBalloonsFlag()
  {
    return addOnBalloonsFlag;
  }


  public void setAddOnBearsFlag(String addOnBearsFlag)
  {

    this.addOnBearsFlag = addOnBearsFlag;
  }


  public String getAddOnBearsFlag()
  {
    return addOnBearsFlag;
  }


  public void setAddOnCardsFlag(String addOnCardsFlag)
  {

    this.addOnCardsFlag = addOnCardsFlag;
  }


  public String getAddOnCardsFlag()
  {
    return addOnCardsFlag;
  }


  public void setAddOnFuneralFlag(String addOnFuneralFlag)
  {

    this.addOnFuneralFlag = addOnFuneralFlag;
  }


  public String getAddOnFuneralFlag()
  {
    return addOnFuneralFlag;
  }


  public void setCodifiedFlag(String codifiedFlag)
  {

    this.codifiedFlag = codifiedFlag;
  }


  public String getCodifiedFlag()
  {
    return codifiedFlag;
  }


  public void setExceptionCode(String exceptionCode)
  {

    this.exceptionCode = exceptionCode;
  }


  public String getExceptionCode()
  {
    return exceptionCode;
  }


  public void setExceptionStartDate(Date exceptionStartDate)
  {

    this.exceptionStartDate = exceptionStartDate;
  }


  public Date getExceptionStartDate()
  {
    return exceptionStartDate;
  }


  public void setExceptionEndDate(Date exceptionEndDate)
  {
    this.exceptionEndDate = exceptionEndDate;
  }


  public Date getExceptionEndDate()
  {
    return exceptionEndDate;
  }


  public void setExceptionMessage(String exceptionMessage)
  {

    this.exceptionMessage = exceptionMessage;
  }


  public String getExceptionMessage()
  {
    return exceptionMessage;
  }


  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }


  public String getVendorId()
  {
    return vendorId;
  }


  public void setVendorCost(double vendorCost)
  {
    this.vendorCost = vendorCost;
  }


  public double getVendorCost()
  {
    return vendorCost;
  }


  public void setVendorSku(String vendorSku)
  {

    this.vendorSku = vendorSku;
  }


  public String getVendorSku()
  {
    return vendorSku;
  }


  public void setSecondChoiceCode(String secondChoiceCode)
  {

    this.secondChoiceCode = secondChoiceCode;
  }


  public String getSecondChoiceCode()
  {
    return secondChoiceCode;
  }


  public void setDropshipCode(String dropshipCode)
  {

    this.dropshipCode = dropshipCode;
  }


  public String getDropshipCode()
  {
    return dropshipCode;
  }


  public void setDiscountAllowedFlag(String discountAllowedFlag)
  {

    this.discountAllowedFlag = discountAllowedFlag;
  }


  public String getDiscountAllowedFlag()
  {
    return discountAllowedFlag;
  }


  public void setDeliveryIncludedFlag(String deliveryIncludedFlag)
  {

    this.deliveryIncludedFlag = deliveryIncludedFlag;
  }


  public String getDeliveryIncludedFlag()
  {
    return deliveryIncludedFlag;
  }


  public void setServiceFeeFlag(String serviceFeeFlag)
  {

    this.serviceFeeFlag = serviceFeeFlag;
  }


  public String getServiceFeeFlag()
  {
    return serviceFeeFlag;
  }


  public void setExoticFlag(String exoticFlag)
  {
    this.exoticFlag = exoticFlag;
  }


  public String getExoticFlag()
  {
    return exoticFlag;
  }


  public void setCountryId(String countryId)
  {

    this.countryId = countryId;
  }


  public String getCountryId()
  {
    return countryId;
  }


  public void setArrangementSize(String arrangementSize)
  {

    this.arrangementSize = arrangementSize;
  }


  public String getArrangementSize()
  {
    return arrangementSize;
  }


  public void setArrangementColors(String arrangementColors)
  {

    this.arrangementColors = arrangementColors;
  }


  public String getArrangementColors()
  {
    return arrangementColors;
  }


  public void setDominantFlowers(String dominantFlowers)
  {

    this.dominantFlowers = dominantFlowers;
  }


  public String getDominantFlowers()
  {
    return dominantFlowers;
  }


  public void setSearchPriority(String searchPriority)
  {
    this.searchPriority = searchPriority;
  }


  public String getSearchPriority()
  {
    return searchPriority;
  }


  public void setRecipe(String recipe)
  {

    this.recipe = recipe;
  }


  public String getRecipe()
  {
    return recipe;
  }


  public void setSubcodeFlag(String subcodeFlag)
  {

    this.subcodeFlag = subcodeFlag;
  }


  public String getSubcodeFlag()
  {
    return subcodeFlag;
  }


  public void setDimWeight(String dimWeight)
  {

    this.dimWeight = dimWeight;
  }


  public String getDimWeight()
  {
    return dimWeight;
  }


  public void setNextDayUpgradeFlag(String nextDayUpgradeFlag)
  {

    this.nextDayUpgradeFlag = nextDayUpgradeFlag;
  }


  public String getNextDayUpgradeFlag()
  {
    return nextDayUpgradeFlag;
  }


  public void setCorporateSite(String corporateSite)
  {

    this.corporateSite = corporateSite;
  }


  public String getCorporateSite()
  {
    return corporateSite;
  }


  public void setUnspscCode(String unspscCode)
  {

    this.unspscCode = unspscCode;
  }


  public String getUnspscCode()
  {
    return unspscCode;
  }


  public void setPriceRank1(String priceRank1)
  {

    this.priceRank1 = priceRank1;
  }


  public String getPriceRank1()
  {
    return priceRank1;
  }


  public void setPriceRank2(String priceRank2)
  {

    this.priceRank2 = priceRank2;
  }


  public String getPriceRank2()
  {
    return priceRank2;
  }


  public void setPriceRank3(String priceRank3)
  {

    this.priceRank3 = priceRank3;
  }


  public String getPriceRank3()
  {
    return priceRank3;
  }


  public void setShipMethodCarrier(String shipMethodCarrier)
  {

    this.shipMethodCarrier = shipMethodCarrier;
  }


  public String getShipMethodCarrier()
  {
    return shipMethodCarrier;
  }


  public void setShipMethodFlorist(String shipMethodFlorist)
  {

    this.shipMethodFlorist = shipMethodFlorist;
  }


  public String getShipMethodFlorist()
  {
    return shipMethodFlorist;
  }


  public void setShippingKey(String shippingKey)
  {

    this.shippingKey = shippingKey;
  }


  public String getShippingKey()
  {
    return shippingKey;
  }


  public void setCompanyId(String companyId)
  {

    this.companyId = companyId;
  }


  public String getCompanyId()
  {
    return companyId;
  }


  public void setDefaultCarrier(String defaultCarrier)
  {

    this.defaultCarrier = defaultCarrier;
  }


  public String getDefaultCarrier()
  {
    return defaultCarrier;
  }


  public void setHoldUntilAvailable(String holdUntilAvailable)
  {

    this.holdUntilAvailable = holdUntilAvailable;
  }


  public String getHoldUntilAvailable()
  {
    return holdUntilAvailable;
  }


  public void setMondayDeliveryFreshcut(String mondayDeliveryFreshcut)
  {

    this.mondayDeliveryFreshcut = mondayDeliveryFreshcut;
  }


  public String getMondayDeliveryFreshcut()
  {
    return mondayDeliveryFreshcut;
  }


  public void setTwoDaySatFreshcut(String twoDaySatFreshcut)
  {

    this.twoDaySatFreshcut = twoDaySatFreshcut;
  }


  public String getTwoDaySatFreshcut()
  {
    return twoDaySatFreshcut;
  }


  public void setInventoryLevel(long inventoryLevel)
  {

    this.inventoryLevel = inventoryLevel;
  }


  public long getInventoryLevel()
  {
    return inventoryLevel;
  }
  
  public void setVendorType(int vendorType)
  {

    this.vendorType = vendorType;
  }


  public int getVendorType()
  {
    return vendorType;
  }
  
  public void setHolidaySecondChoiceCode(String holidaySecondChoiceCode)
  {

    this.holidaySecondChoiceCode = holidaySecondChoiceCode;
  }


  public String getHolidaySecondChoiceCode()
  {
    return holidaySecondChoiceCode;
  }
  
  public void setTaxFlag(String taxFlag)
  {

    this.taxFlag = taxFlag;
  }


  public String getTaxFlag()
  {
    return taxFlag;
  }
  
  public void setEgiftFlag(String egiftFlag)
  {

    this.egiftFlag = egiftFlag;
  }


  public String getEgiftFlag()
  {
    return egiftFlag;
  }
  
  public void setVariablePriceFlag(String variablePriceFlag)
  {

    this.variablePriceFlag = variablePriceFlag;
  }


  public String getVariablePriceFlag()
  {
    return variablePriceFlag;
  }
  
  public void setHolidaySku(String holidaySku)
  {

    this.holidaySku = holidaySku;
  }


  public String getHolidaySku()
  {
    return holidaySku;
  }
  
  public void setHolidayPrice(double holidayPrice)
  {
    this.holidayPrice = holidayPrice;
  }


  public double getHolidayPrice()
  {
    return holidayPrice;
  }
  
  public void setCatalogFlag(String catalogFlag)
  {

    this.catalogFlag = catalogFlag;
  }


  public String getCatalogFlag()
  {
    return catalogFlag;
  }
  
  public void setHolidayDeluxePrice(double holidayDeluxePrice)
  {
    this.holidayDeluxePrice = holidayDeluxePrice;
  }


  public double getHolidayDeluxePrice()
  {
    return holidayDeluxePrice;
  }
  
  public void setHolidayPremiumPrice(double holidayPremiumPrice)
  {
    this.holidayPremiumPrice = holidayPremiumPrice;
  }


  public double getHolidayPremiumPrice()
  {
    return holidayPremiumPrice;
  }
  
  public void setHolidayStartDate(Date holidayStartDate)
  {

    this.holidayStartDate = holidayStartDate;
  }


  public Date getHolidayStartDate()
  {
    return holidayStartDate;
  }
  
  public void setHolidayEndDate(Date holidayEndDate)
  {

    this.holidayEndDate = holidayEndDate;
  }


  public Date getHolidayEndDate()
  {
    return holidayEndDate;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public void setPersonalizationTemplateOrder(String personalizationTemplateOrder) {
        this.personalizationTemplateOrder = personalizationTemplateOrder;
    }

    public String getPersonalizationTemplateOrder() {
        return personalizationTemplateOrder;
    }

  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedBy()
  {
    return updatedBy;
  }
}
