package com.ftd.op.maintenance.vo;

public class BlockFlagVO 
{
    private boolean blockedForDelivery;
    private boolean blockedForShipping;

    public BlockFlagVO()
    {
    }

    public boolean isBlockedForDelivery()
    {
        return blockedForDelivery;
    }

    public void setBlockedForDelivery(boolean blockedForDelivery)
    {
        this.blockedForDelivery = blockedForDelivery;
    }

    public boolean isBlockedForShipping()
    {
        return blockedForShipping;
    }

    public void setBlockedForShipping(boolean blockedForShipping)
    {
        this.blockedForShipping = blockedForShipping;
    }
}