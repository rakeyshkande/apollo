package com.ftd.op.maintenance.vo;

import com.ftd.op.maintenance.vo.BaseVO;
import com.ftd.op.maintenance.vo.XMLInterface;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class MessageVO extends BaseVO implements XMLInterface, Cloneable
{

    String   businessName = null;
    String   cardMessage = null;
    String   comments = null;
    String   customerId= null;
    Date     deliveryDate = null;
    Date     departureDate = null;
    String   emailFlag = null;
    String   externalOrderNumber = null;
    String   fillingVendor = null;
    String   letterFlag = null;
    String   masterOrderNumber= null;
    Date     orderDate = null;
    String   orderDetailId = null;
    String   orderGUID= null;
    double   price = 0;
    boolean  printed;
    String   productId = null;
    String   recipientAddress1 = null;
    String   recipientCity = null;
    String   recipientCountry = null;
    String   recipientName = null;
    String   recipientPhoneNumber = null;
    String   recipientState = null;
    String   recipientZip = null;
    Date     shipDate = null;
    String   shipMethod = null;
    String   sourceCode = null;
    String   vendorSku;
    String   venusId = null;
    String   venusOrderNumber = null;
    boolean  zoneJumpFlag;
    Date     zoneJumpLabelDate;

  public MessageVO()
  {
  }


  public void setBusinessName(String businessName)
  {
    this.businessName = businessName;
  }

  public String getBusinessName()
  {
    return businessName;
  }

  public void setCardMessage(String cardMessage)
  {
    this.cardMessage = cardMessage;
  }

  public String getCardMessage()
  {
    return cardMessage;
  }

  public void setComments(String comments)
  {
    this.comments = comments;
  }

  public String getComments()
  {
    return comments;
  }

  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }

  public String getCustomerId()
  {
    return customerId;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }

  public void setDepartureDate(Date departureDate)
  {
    this.departureDate = departureDate;
  }

  public Date getDepartureDate()
  {
    return departureDate;
  }

  public void setEmailFlag(String emailFlag)
  {
    this.emailFlag = emailFlag;
  }

  public String getEmailFlag()
  {
    return emailFlag;
  }

  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = externalOrderNumber;
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

  public void setFillingVendor(String fillingVendor)
  {
    this.fillingVendor = fillingVendor;
  }

  public String getFillingVendor()
  {
    return fillingVendor;
  }

  public void setLetterFlag(String letterFlag)
  {
    this.letterFlag = letterFlag;
  }

  public String getLetterFlag()
  {
    return letterFlag;
  }

  public void setMasterOrderNumber(String masterOrderNumber)
  {
    this.masterOrderNumber = masterOrderNumber;
  }

  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }

  public void setOrderDate(Date orderDate)
  {
    this.orderDate = orderDate;
  }

  public Date getOrderDate()
  {
    return orderDate;
  }

  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }

  public String getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setOrderGUID(String orderGUID)
  {
    this.orderGUID = orderGUID;
  }

  public String getOrderGUID()
  {
    return orderGUID;
  }

  public void setPrice(double price)
  {
    this.price = price;
  }

  public double getPrice()
  {
    return price;
  }

  public void setPrinted(boolean printed)
  {
    this.printed = printed;
  }

  public boolean isPrinted()
  {
    return printed;
  }

  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  public String getProductId()
  {
    return productId;
  }

  public void setRecipientAddress1(String recipientAddress1)
  {
    this.recipientAddress1 = recipientAddress1;
  }

  public String getRecipientAddress1()
  {
    return recipientAddress1;
  }

  public void setRecipientCity(String recipientCity)
  {
    this.recipientCity = recipientCity;
  }

  public String getRecipientCity()
  {
    return recipientCity;
  }

  public void setRecipientCountry(String recipientCountry)
  {
    this.recipientCountry = recipientCountry;
  }

  public String getRecipientCountry()
  {
    return recipientCountry;
  }

  public void setRecipientName(String recipientName)
  {
    this.recipientName = recipientName;
  }

  public String getRecipientName()
  {
    return recipientName;
  }

  public void setRecipientPhoneNumber(String recipientPhoneNumber)
  {
    this.recipientPhoneNumber = recipientPhoneNumber;
  }

  public String getRecipientPhoneNumber()
  {
    return recipientPhoneNumber;
  }

  public void setRecipientState(String recipientState)
  {
    this.recipientState = recipientState;
  }

  public String getRecipientState()
  {
    return recipientState;
  }

  public void setRecipientZip(String recipientZip)
  {
    this.recipientZip = recipientZip;
  }

  public String getRecipientZip()
  {
    return recipientZip;
  }

  public void setShipDate(Date shipDate)
  {
    this.shipDate = shipDate;
  }

  public Date getShipDate()
  {
    return shipDate;
  }

  public void setShipMethod(String shipMethod)
  {
    this.shipMethod = shipMethod;
  }

  public String getShipMethod()
  {
    return shipMethod;
  }

  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = sourceCode;
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setVendorSku(String vendorSku)
  {
    this.vendorSku = vendorSku;
  }

  public String getVendorSku()
  {
    return vendorSku;
  }

  public void setVenusId(String venusId)
  {
    this.venusId = venusId;
  }

  public String getVenusId()
  {
    return venusId;
  }

  public void setVenusOrderNumber(String venusOrderNumber)
  {
    this.venusOrderNumber = venusOrderNumber;
  }

  public String getVenusOrderNumber()
  {
    return venusOrderNumber;
  }

  public void setZoneJumpFlag(boolean zoneJumpFlag)
  {
    this.zoneJumpFlag = zoneJumpFlag;
  }

  public boolean isZoneJumpFlag()
  {
    return zoneJumpFlag;
  }

  public void setZoneJumpLabelDate(Date zoneJumpLabelDate)
  {
    this.zoneJumpLabelDate = zoneJumpLabelDate;
  }

  public Date getZoneJumpLabelDate()
  {
    return zoneJumpLabelDate;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  count
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<MESSAGE>");
      }
      else
      {
        sb.append("<MESSAGE num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            if (xmlInt != null)
            {
              int k = i + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</MESSAGE>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }





}
