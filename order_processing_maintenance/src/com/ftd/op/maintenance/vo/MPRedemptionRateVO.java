package com.ftd.op.maintenance.vo;

import java.math.BigDecimal;

import java.util.Date;

public class MPRedemptionRateVO 
{
    private String redemptionRateId;
    private String description;
    private String paymentMethodId;
    private String redemptionRateAmt;
    private String requestedBy;
    private String operation;
    private Date updatedOn;
    private String updatedBy;

    public MPRedemptionRateVO()
    {
    }


    public void setRedemptionRateId(String redemptionRateId) {
        this.redemptionRateId = redemptionRateId;
    }

    public String getRedemptionRateId() {
        return redemptionRateId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setRedemptionRateAmt(String redemptionRateAmt) {
        this.redemptionRateAmt = redemptionRateAmt;
    }

    public String getRedemptionRateAmt() {
        return redemptionRateAmt;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }
}
