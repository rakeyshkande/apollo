package com.ftd.op.maintenance.vo;

//Response obtained from Novator
public class NovatorResponseVO extends BaseVO
{

    //Indicates if Novator responsed with a success or failure
    private boolean Success;

    //indicates if the Novator response could be determined
    private boolean Parseable;

    //Description received from Novator
    private String Description;
    
    public NovatorResponseVO() 
    {
    }

    
    public boolean isSuccess()
    {
        return Success;
    }

    public void setSuccess(boolean newSuccess)
    {
        Success = newSuccess;
    }

    public boolean isParseable()
    {
        return Parseable;
    }

    public void setParseable(boolean newCouldParse)
    {
        Parseable = newCouldParse;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String newDescription)
    {
        Description = newDescription;
    }

 
}