package com.ftd.op.maintenance.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class EmailVO
{
  private long      emailId;
  private long      customerId;
  private String    companyId;
  private String    emailAddress;
  private char      activeIndicator;
  private String    subscribeStatus;
  private Date  subscribeDate;
  private Date  createdOn;
  private String    createdBy;
  private Date  updatedOn;
  private String    updatedBy;


  public EmailVO()
  {
  }

  public void setCustomerId(long customerId)
  {

    this.customerId = customerId;
  }

  public long getCustomerId()
  {
    return customerId;
  }

  public void setCompanyId(String companyId)
  {
    this.companyId = trim(companyId);
  }

  public String getCompanyId()
  {
    return companyId;
  }

  public void setEmailId(long emailId)
  {

    this.emailId = emailId;
  }


  public long getEmailId()
  {
    return emailId;
  }


  public void setEmailAddress(String emailAddress)
  {

    this.emailAddress = trim(emailAddress);
  }


  public String getEmailAddress()
  {
    return emailAddress;
  }


  public void setActiveIndicator(char activeIndicator)
  {

    this.activeIndicator = activeIndicator;
  }


  public char getActiveIndicator()
  {
    return activeIndicator;
  }


  public void setSubscribeStatus(String subscribeStatus)
  {
    this.subscribeStatus = trim(subscribeStatus);
  }


  public String getSubscribeStatus()
  {
    return subscribeStatus;
  }


  public void setSubscribeDate(Date subscribeDate)
  {
    this.subscribeDate = subscribeDate;
  }


  public Date getSubscribeDate()
  {
    return subscribeDate;
  }

   public void setCreatedOn(Date createdOn)
  {

    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {


    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {

    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }
  

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}
