package com.ftd.op.maintenance.vo;

import java.util.Date;

public class ComponentSkuVO 
{
    private String componentSkuId;
    private String componentSkuName;
    private double baseCost;
    private Date baseCostEffectiveDate;
    private double qaCost;
    private Date qaCostEffectiveDate;
    private double freightInCost;
    private Date freightInCostEffectiveDate;
    private String updatedBy;
    private String availableFlag;
    private String commentText;

    public ComponentSkuVO()
    {
    }

    public void setComponentSkuId(String componentSkuId) {
        this.componentSkuId = componentSkuId;
    }

    public String getComponentSkuId() {
        return componentSkuId;
    }

    public void setComponentSkuName(String componentSkuName) {
        this.componentSkuName = componentSkuName;
    }

    public String getComponentSkuName() {
        return componentSkuName;
    }

    public void setBaseCost(double baseCost) {
        this.baseCost = baseCost;
    }

    public double getBaseCost() {
        return baseCost;
    }

    public void setBaseCostEffectiveDate(Date baseCostEffectiveDate) {
        this.baseCostEffectiveDate = baseCostEffectiveDate;
    }

    public Date getBaseCostEffectiveDate() {
        return baseCostEffectiveDate;
    }

    public void setQaCost(double qaCost) {
        this.qaCost = qaCost;
    }

    public double getQaCost() {
        return qaCost;
    }

    public void setQaCostEffectiveDate(Date qaCostEffectiveDate) {
        this.qaCostEffectiveDate = qaCostEffectiveDate;
    }

    public Date getQaCostEffectiveDate() {
        return qaCostEffectiveDate;
    }

    public void setFreightInCost(double freightInCost) {
        this.freightInCost = freightInCost;
    }

    public double getFreightInCost() {
        return freightInCost;
    }

    public void setFreightInCostEffectiveDate(Date freightInCostEffectiveDate) {
        this.freightInCostEffectiveDate = freightInCostEffectiveDate;
    }

    public Date getFreightInCostEffectiveDate() {
        return freightInCostEffectiveDate;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setAvailableFlag(String availableFlag) {
        this.availableFlag = availableFlag;
    }

    public String getAvailableFlag() {
        return availableFlag;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getCommentText() {
        return commentText;
    }
}
