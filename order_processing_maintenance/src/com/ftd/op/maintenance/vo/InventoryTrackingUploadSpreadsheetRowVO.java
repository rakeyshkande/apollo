package com.ftd.op.maintenance.vo;

import com.ftd.util.fileprocessing.*;

import com.ftd.security.exceptions.ExcelProcessingException;


/**
 * Representation of a line item to be uploaded from Excel spreadsheet.
 * @author Ali Lakhani
 */
public class InventoryTrackingUploadSpreadsheetRowVO
  extends FileLineItem
{
  // attributes common to all instances.
  private static final int COLUMNS = 11;
  private static final String ERROR_REQUIRED_FIELD_MISSING = "Required Field Missing";
  private static final String ERROR_DATA_TOO_LARGE = "Data Too Large";
  private static final String ERROR_INVALID_NUMERIC = "Invalid Numeric Value";

  public static final String FIELD_NAME_PRODUCT_ID = "PRODUCT_ID";
  public static final String FIELD_NAME_PRODUCT_NAME = "PRODUCT_NAME";
  public static final String FIELD_NAME_VENDOR_ID = "VENDOR_ID";
  public static final String FIELD_NAME_VENDOR_NAME = "VENDOR_NAME";
  public static final String FIELD_NAME_START_DATE = "START_DATE";
  public static final String FIELD_NAME_END_DATE = "END_DATE";
  public static final String FIELD_NAME_FORECAST_QTY = "FORECAST_QTY";
  public static final String FIELD_NAME_WARNING_THRESHOLD = "WARNING_THRESHOLD";
  public static final String FIELD_NAME_SHUTDOWN_THRESHOLD = "SHUTDOWN_THRESHOLD";
  public static final String FIELD_NAME_AVAILABILITY_OPTION = "AVAILABILITY_OPTION";
  public static final String FIELD_NAME_COMMENTS = "COMMENTS";


  static {
    InventoryTrackingUploadSpreadsheetRowVO.setColumnSize(COLUMNS);
  }

  /**
     * Initialize the list of Field. Specifies the Field name, type,
     * column position, max size, and if it's a required field.
     */
  public InventoryTrackingUploadSpreadsheetRowVO()
  {
    // create fieldList if it's null.
    super();

    //note that while some of these are required fields, validation will not be done at this point.  
    //this is because the GUI has a textarea for errors for each record, and doing a validation at this 
    //point will result in a USE
    fieldList.add(new Field(FIELD_NAME_PRODUCT_ID, Field.TYPE_STRING, 0, 10, false));
    fieldList.add(new Field(FIELD_NAME_PRODUCT_NAME, Field.TYPE_STRING, 1, 100, false));
    fieldList.add(new Field(FIELD_NAME_VENDOR_ID, Field.TYPE_STRING, 2, 5, false));
    fieldList.add(new Field(FIELD_NAME_VENDOR_NAME, Field.TYPE_STRING, 3, 50, false));
    fieldList.add(new Field(FIELD_NAME_START_DATE, Field.TYPE_STRING, 4, 10, false));
    fieldList.add(new Field(FIELD_NAME_END_DATE, Field.TYPE_STRING, 5, 10, false));
    fieldList.add(new Field(FIELD_NAME_FORECAST_QTY, Field.TYPE_STRING, 6, 10, false));
    fieldList.add(new Field(FIELD_NAME_WARNING_THRESHOLD, Field.TYPE_STRING, 7, 10, false));
    fieldList.add(new Field(FIELD_NAME_SHUTDOWN_THRESHOLD, Field.TYPE_STRING, 8, 10, false));
    fieldList.add(new Field(FIELD_NAME_AVAILABILITY_OPTION, Field.TYPE_STRING, 9, 5, false));
    fieldList.add(new Field(FIELD_NAME_COMMENTS, Field.TYPE_STRING, 10, 1000, false));

  }

  public void validate() throws Exception
  {

    // Get list of all fields.
    for (int i = 0; i < fieldList.size(); i++)
    {
      Field field = (Field) fieldList.get(i);
      boolean isRequired = field.isRequired();
      int maxSize = field.getMaxSize();
      String value = field.getValue();
      String type = field.getType();
      String columnName = field.getName();


      // For each field, validate the following:
      // If the field is required, it's value cannot be null.
      if (isRequired && (value == null || value.length() == 0))
      {
        throw new ExcelProcessingException(ERROR_REQUIRED_FIELD_MISSING, getLineNumber() + 1, columnName);
      }
      // Field size cannot be greater than field max size.
      if (value != null && value.length() > maxSize)
      {
        throw new ExcelProcessingException(ERROR_DATA_TOO_LARGE, getLineNumber() + 1, columnName);
      }
      if (type.equals(Field.TYPE_LONG))
      {
        try
        {
          Long.parseLong(value);
        }
        catch (Throwable e)
        {
          throw new ExcelProcessingException(ERROR_INVALID_NUMERIC, getLineNumber() + 1, columnName);
        }
      }

    }
  }


}