/**
 * 
 */
package com.ftd.op.maintenance.vo;

import java.sql.Date;
import java.util.List;

import com.ftd.util.pagination.PaginationVO;

/**
 * @author smeka
 * 
 */
public class InventorySearchCriteriaVO {

	private List<String> productIds;
	private List<String> vendorIds;
	private String productIdsStr;
	private String vendorIdsStr;
	private Date cStartDate;
	private Date cEndDate;
	private String productStatus;
	private String vendorStatus;
	private String vendorProductStatus;
	private String onHand;
	private PaginationVO pageDetail;

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}

	public List<String> getVendorIds() {
		return vendorIds;
	}

	public void setVendorIds(List<String> vendorIds) {
		this.vendorIds = vendorIds;
	}

	public String getProductIdsStr() {
		return productIdsStr;
	}

	public void setProductIdsStr(String productIdsStr) {
		this.productIdsStr = productIdsStr;
	}

	public String getVendorIdsStr() {
		return vendorIdsStr;
	}

	public void setVendorIdsStr(String vendorIdsStr) {
		this.vendorIdsStr = vendorIdsStr;
	}

	public Date getcStartDate() {
		return cStartDate;
	}

	public void setcStartDate(Date date) {
		this.cStartDate = date;
	}

	public Date getcEndDate() {
		return cEndDate;
	}

	public void setcEndDate(Date cEndDate) {
		this.cEndDate = cEndDate;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public String getVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

	public String getOnHand() {
		return onHand;
	}

	public void setOnHand(String onHand) {
		this.onHand = onHand;
	}

	public PaginationVO getPageDetail() {
		return pageDetail;
	}

	public void setPageDetail(PaginationVO pageDetail) {
		this.pageDetail = pageDetail;
	}

	public String getVendorProductStatus() {
		return vendorProductStatus;
	}

	public void setVendorProductStatus(String vendorProductStatus) {
		this.vendorProductStatus = vendorProductStatus;
	}	
	
}
