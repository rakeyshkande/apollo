package com.ftd.op.maintenance.filter;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class DataFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.op.maintenance.filter.DataFilter");

    private static final String ADMIN_ACTION = "adminaction";
    private static final String SITE_NAME = "sitename";

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";

    private static final String PROPERTY_FILE = "security-config.xml";
    private static final String OPM_CONFIG_CONTEXT = "ORDER_PROCESSING_MAINT_CONFIG";

    private static final String ERROR_PAGE = "error_page";

    //Header Data
    public static final String H_BRAND_NAME                 = "call_brand_name";
    public static final String H_CUSTOMER_ORDER_INDICATOR   = "call_type_flag";
    public static final String H_CUSTOMER_SERVICE_NUMBER    = "call_cs_number";
    public static final String H_DNIS_ID                    = "call_dnis";    
    public static final String H_CALL_LOG_ID                = "call_log_id";    

    //Timer Filter
    public static final String TIMER_CALL_LOG_ID            = "t_call_log_id";
    public static final String TIMER_COMMENT_ORIGIN_TYPE    = "t_comment_origin_type";
    public static final String TIMER_ENTITY_HISTORY_ID      = "t_entity_history_id";

    private static final String SC_CC_NUMBER                 = "sc_cc_number";
    private static final String SC_CUST_IND                  = "sc_cust_ind";
    private static final String SC_CUST_NUMBER               = "sc_cust_number";
    private static final String SC_EMAIL_ADDRESS             = "sc_email_address";
    private static final String SC_LAST_NAME                 = "sc_last_name";
    private static final String SC_MEMBERSHIP_NUMBER         = "sc_rewards_number";
    private static final String SC_ORDER_NUMBER              = "sc_order_number";
    private static final String SC_PHONE_NUMBER              = "sc_phone_number";
    private static final String SC_RECIP_IND                 = "sc_recip_ind";
    private static final String SC_TRACKING_NUMBER           = "sc_tracking_number";
    private static final String SC_ZIP_CODE                  = "sc_zip_code";

    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     * 
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String adminAction = null,
               siteName = null;


        try
        {
            HashMap parameters  = new HashMap();
            
            if(request.getAttribute(OPMConstants.CONS_APP_PARAMETERS)!=null)
            {
              parameters=(HashMap)request.getAttribute(OPMConstants.CONS_APP_PARAMETERS);
            }
            

            if(isMultipartFormData(((HttpServletRequest)request )))
            {
                //populate admin and site info only if multiple forms were found
                parameters.put(adminAction, ((request.getParameter(ADMIN_ACTION))                               !=null?request.getParameter(ADMIN_ACTION).toString().trim():""));
                parameters.put(siteName, ((request.getParameter(SITE_NAME))                                  !=null?request.getParameter(SITE_NAME).toString().trim():""));
                
                //Timer Filter
                populateTimerInfo(request, parameters);
                
                //Header Info
                populateHeaderInfo(request, parameters);

                //Search Criteria
                populateSearchCriteria(request, parameters);
                
				//Start Origin
				populateStartOriginInfo(request, parameters);
               
            }
            else
            {
                //Timer Filter
                populateTimerInfo(request, parameters);
                
                //Header Info
                populateHeaderInfo(request, parameters);

                //Search Criteria
                populateSearchCriteria(request, parameters);
               
				//Start Origin
				populateStartOriginInfo(request, parameters);


            }
            request.setAttribute(OPMConstants.CONS_APP_PARAMETERS, parameters);
            chain.doFilter(request, response);
        } 
        catch (Exception ex)
        {
            logger.error(ex);
            try
            {
                redirectToError(response);
            }
            catch (Exception e)
            {
                logger.error(e);
            }
        }
    }


    /**
     * Redirect csr to the error page.
     * 
     * @param ServletResponse
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private void redirectToError(ServletResponse response)
        throws Exception
    {
        ((HttpServletResponse) response).sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(OPM_CONFIG_CONTEXT,ERROR_PAGE));
    }


    /**
     * Returns true if request content type is multipart/form-data.
     * 
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
     public static boolean isMultipartFormData(HttpServletRequest request)
        throws Exception
     {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
       }


    /**
     * Populate Start Origin data
     * 
     */
    private void populateStartOriginInfo(ServletRequest request, HashMap parameters)
    {
    
      parameters.put(OPMConstants.START_ORIGIN, ((request.getParameter(OPMConstants.START_ORIGIN))!=null?request.getParameter(OPMConstants.START_ORIGIN).toString().trim():""));

    }


    /**
     * Populate Timer data
     * 
     */
    private void populateTimerInfo(ServletRequest request, HashMap parameters)
    {
    
      parameters.put(TIMER_CALL_LOG_ID,              ((request.getParameter(TIMER_CALL_LOG_ID))             !=null?request.getParameter(TIMER_CALL_LOG_ID).toString().trim():""));
      parameters.put(TIMER_COMMENT_ORIGIN_TYPE,      ((request.getParameter(TIMER_COMMENT_ORIGIN_TYPE))     !=null?request.getParameter(TIMER_COMMENT_ORIGIN_TYPE).toString().trim():""));
      parameters.put(TIMER_ENTITY_HISTORY_ID,        ((request.getParameter(TIMER_ENTITY_HISTORY_ID))       !=null?request.getParameter(TIMER_ENTITY_HISTORY_ID).toString().trim():""));

    }


    /**
     * Populate Header data
     * 
     */
    private void populateHeaderInfo(ServletRequest request, HashMap parameters)
    {
      parameters.put(H_BRAND_NAME,                   ((request.getParameter(H_BRAND_NAME))                  !=null?request.getParameter(H_BRAND_NAME).toString().trim():""));
      parameters.put(H_CUSTOMER_ORDER_INDICATOR,     ((request.getParameter(H_CUSTOMER_ORDER_INDICATOR))    !=null?request.getParameter(H_CUSTOMER_ORDER_INDICATOR).toString().trim():""));
      parameters.put(H_CUSTOMER_SERVICE_NUMBER,      ((request.getParameter(H_CUSTOMER_SERVICE_NUMBER))     !=null?request.getParameter(H_CUSTOMER_SERVICE_NUMBER).toString().trim():""));
      parameters.put(H_DNIS_ID,                      ((request.getParameter(H_DNIS_ID))                     !=null?request.getParameter(H_DNIS_ID).toString().trim():""));

    }


    /**
     * Populate Search Criteria
     * 
     */
    private void populateSearchCriteria(ServletRequest request, HashMap parameters)
    {
      parameters.put(SC_ORDER_NUMBER,                ((request.getParameter(SC_ORDER_NUMBER))               !=null?request.getParameter(SC_ORDER_NUMBER).toString().trim():""));
      parameters.put(SC_LAST_NAME,                   ((request.getParameter(SC_LAST_NAME))                  !=null?request.getParameter(SC_LAST_NAME).toString().trim():""));
      parameters.put(SC_PHONE_NUMBER,                ((request.getParameter(SC_PHONE_NUMBER))               !=null?request.getParameter(SC_PHONE_NUMBER).toString().trim():""));
      parameters.put(SC_EMAIL_ADDRESS,               ((request.getParameter(SC_EMAIL_ADDRESS))              !=null?request.getParameter(SC_EMAIL_ADDRESS).toString().trim():""));
      parameters.put(SC_ZIP_CODE,                    ((request.getParameter(SC_ZIP_CODE))                   !=null?request.getParameter(SC_ZIP_CODE).toString().trim():""));
      parameters.put(SC_CC_NUMBER,                   ((request.getParameter(SC_CC_NUMBER))                  !=null?request.getParameter(SC_CC_NUMBER).toString().trim():""));
      parameters.put(SC_CUST_NUMBER,                 ((request.getParameter(SC_CUST_NUMBER))                !=null?request.getParameter(SC_CUST_NUMBER).toString().trim():""));
      parameters.put(SC_TRACKING_NUMBER,             ((request.getParameter(SC_TRACKING_NUMBER))            !=null?request.getParameter(SC_TRACKING_NUMBER).toString().trim():""));
      parameters.put(SC_MEMBERSHIP_NUMBER,           ((request.getParameter(SC_MEMBERSHIP_NUMBER))          !=null?request.getParameter(SC_MEMBERSHIP_NUMBER).toString().trim():""));

      if ((request.getParameter(SC_CUST_IND)) !=null)
      {
        if (!((request.getParameter(SC_CUST_IND)).equalsIgnoreCase("") )  )
          parameters.put(SC_CUST_IND, "Y");
        else
          parameters.put(SC_CUST_IND, "N");
      }
      else
      {
        parameters.put(SC_CUST_IND, "N");
      }
            
      if ((request.getParameter(SC_RECIP_IND)) !=null)
      {
        if (!((request.getParameter(SC_RECIP_IND)).equalsIgnoreCase("") )  )
          parameters.put(SC_RECIP_IND, "Y");
        else
          parameters.put(SC_RECIP_IND, "N");
      }
      else
      {
        parameters.put(SC_RECIP_IND, "N");
      }
    
    }
    
    
    
  


}