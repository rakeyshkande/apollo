package com.ftd.op.maintenance.dao;

import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.ContactVO;
import com.ftd.op.maintenance.vo.GlobalParameterVO;
import com.ftd.op.maintenance.vo.InventoryVO;
import com.ftd.op.maintenance.vo.NotificationVO;
import com.ftd.op.maintenance.vo.PartnerShippingAccountVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.VendorAddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class VendorDAO
{
    private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.VendorDAO";
    private Connection conn;
    private Logger logger;


    public VendorDAO(Connection conn)
    {
        this.conn = conn;
        logger = new Logger(LOGGER_CATEGORY);
    }

    public boolean existsVendor(String vendorCode) throws Exception
    {
      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();

      HashMap inputParams = new HashMap();
      inputParams.put("FLORIST_ID", vendorCode);
      dataRequest.setInputParams(inputParams);

      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("VIEW_FLORIST_MASTER");
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if(rs != null && rs.next())
      {
        return true;
      } else
      {
        return false;
      }
    }
    /**
     * Get list of all available vendors in the DB
     * @return
     * @throws java.lang.Exception
     */
    public Document getVendorList() throws Exception
    {
      logger.debug("getVendorList():: Document");
      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_VENDOR_MAINT_LIST");
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);
    }


    /**
     * Get list of all available vendors in the DB
     * @return
     * @throws java.lang.Exception
     */
    public CachedResultSet getVendorListCRS() throws Exception
    {
      DataRequest dataRequest = new DataRequest();

      HashMap inputParams = new HashMap();
      inputParams.put("IN_SORT_BY", "vm.vendor_name");
      dataRequest.setInputParams(inputParams);

      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_VENDOR_MAINT_LIST_SORTED_CRS");
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      return crs; 
      
    }


    public boolean inventoryRecordExists(String productId) throws Exception
    {

      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();

      HashMap inputParams = new HashMap();
      inputParams.put("IN_PRDOUCT_ID", productId);
      dataRequest.setInputParams(inputParams);

      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_INVENTORY_CONTROL_RECORD_SET");
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      return rs == null || rs.getRowCount() == 0 ? false : true;
    }


    /**
     * @return
     * @throws java.lang.Exception
     */
    public Document getInventoryRecord(String productId, String vendorId) throws Exception
    {
        Document data = DOMUtil.getDefaultDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_INVENTORY_DATA");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap map = (HashMap)dataAccessUtil.execute(dataRequest);

        Document mainDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element mainRoot = (Element) mainDoc.createElement("inventorydata");
        mainDoc.appendChild(mainRoot);

        Document inventoryDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element invRoot = (Element) inventoryDoc.createElement("inventory");
        inventoryDoc.appendChild(invRoot);

        Document emailDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element emailRoot = (Element) emailDoc.createElement("notifications");
        emailDoc.appendChild(emailRoot);

        DOMUtil.addSection(inventoryDoc,((Document)map.get("OUT_INVENTORY_CURSOR")).getChildNodes());
        DOMUtil.addSection(emailDoc,((Document)map.get("OUT_INV_NOTIFICATIONS_CURSOR")).getChildNodes());
        DOMUtil.addSection(mainDoc,inventoryDoc.getChildNodes());
        DOMUtil.addSection(mainDoc,emailDoc.getChildNodes());

        return (Document)mainDoc;
    }


    public Document getVendorTypeList() throws Exception
    {
      logger.debug("getVendorTypeList() :: Document");
      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_VENDOR_TYPES_LIST");
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);

    }

    /**
     * Get list of all available vendors in the DB
     * @return
     * @throws java.lang.Exception
     */
    public Document getGlobalBlocksXML() throws Exception
    {
      logger.debug("getGlobalBlocksXML():: Document");
      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_GLOBAL_BLOCKS_XML");
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);

    }

    /**
     * Get list of all available vendors in the DB
     * @return
     * @throws java.lang.Exception
     */
    public Document getProductList(String vendorId) throws Exception
    {
      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_VENDOR_PRODUCTS");

      HashMap inputParams = new HashMap();
      inputParams.put("IN_VENDOR_ID", vendorId);
      dataRequest.setInputParams(inputParams);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);

    }


    /**
     * @return
     * @throws java.lang.Exception
     */
    public Document getVendorBlocksXML(String vendorId) throws Exception
    {
      logger.debug("getVendorBlocksXML(String vendorid)");
      Document data = DOMUtil.getDefaultDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("GET_VENDOR_BLOCKS_XML");
      HashMap inputParams = new HashMap();
      inputParams.put("IN_VENDOR_ID", vendorId);
      dataRequest.setInputParams(inputParams);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);
    }

    /**
     * Gets inventory data based on the passed product and vendor ids
     * @param productId filter
     * @param vendorId filter
     * @return
     * @throws Exception
     */
    public Document getInventoryReport(String productId, String vendorId) throws Exception {
        logger.debug("getVendorBlocksXML(String vendorid)");
        Document data = DOMUtil.getDefaultDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("INVENTORY_CONTROL_REPORT");
        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap map = (HashMap)dataAccessUtil.execute(dataRequest);

        Document xmlDoc = (Document)DOMUtil.getDocumentBuilder().newDocument();
        Element root = (Element)xmlDoc.createElement("report");
        xmlDoc.appendChild(root);


        Document parms = (Document)map.get("OUT_HEADING_CUR");
        Document report = (Document)map.get("OUT_COUNT_CUR");

        DOMUtil.addSection(xmlDoc, parms.getChildNodes());
        DOMUtil.addSection(xmlDoc, report.getChildNodes());

        return xmlDoc;


    }


    /**
     * Obtain all non-global vendor specific blocks
     * @return
     * @throws java.lang.Exception
     */
    public List getVendorBlocksList(String vendorId) throws Exception
    {
        List vendorBlocks = new ArrayList();
        logger.debug("getVendorBlocksXML(String vendorid)");
        Document data = DOMUtil.getDefaultDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_VENDOR_BLOCKS");
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);
        CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
          while ( results.next() )
          {
             VendorBlockVO blockVO = new VendorBlockVO();
             blockVO.setVendorId(vendorId);
             blockVO.setGlobalFlag("N");
             blockVO.setBlockType(results.getString("BLOCK_TYPE"));
             blockVO.setEndDate(results.getDate("END_DATE"));
             blockVO.setStartDate(results.getDate("START_DATE"));
             vendorBlocks.add(blockVO);
          }
        }
        return vendorBlocks;
    }

    /**
     * Obtain all global vendor specific blocks
     * @return
     * @throws java.lang.Exception
     */
    public List getGlobalVendorBlocksList(String vendorId) throws Exception
    {
        List vendorBlocks = new ArrayList();
        logger.debug("getGlobalVendorBlocksList(String vendorid)");
        Document data = DOMUtil.getDefaultDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_GLOBAL_VENDOR_BLOCKS");
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);
        CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
          while ( results.next() )
          {
             VendorBlockVO blockVO = new VendorBlockVO();
             blockVO.setVendorId(vendorId);
             blockVO.setGlobalFlag("Y");
             blockVO.setBlockType(results.getString("BLOCK_TYPE"));
             blockVO.setEndDate(results.getDate("END_DATE"));
             blockVO.setStartDate(results.getDate("START_DATE"));
             vendorBlocks.add(blockVO);
          }
        }
        return vendorBlocks;
    }


  /**
   * This method is a wrapper for the SP_GET_VENDOR SP.
   * It populates a vendor VO based on the returned record set.
   *
   * @param String - vendorId
   * @return Document
   */
    public Document getVendor(String vendorId) throws Exception
    {
        logger.debug("getVendor (String vendorId) :: Document");
        Document vendorDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element vendorXML = (Element) vendorDoc.createElement("vendor_details");
        vendorDoc.appendChild(vendorXML);

        Document data = (Document) vendorDoc;

        Map outputs = null;
        DataRequest dataRequest = new DataRequest();
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);

         /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_VENDOR_MASTER");
        dataRequest.setInputParams(inputParams);


        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap cursors = (HashMap) dataAccessUtil.execute(dataRequest);

        Document vendorMasterXML = CommonUtil.buildXML(cursors,"OUT_VENDOR_MASTER_CURSOR","vendors","vendor");
        Document primaryContactXML = CommonUtil.buildXML(cursors,"OUT_PRIMARY_CONTACT_CURSOR","maincontacts","maincontact");
        Document secondaryContactXML = CommonUtil.buildXML(cursors,"OUT_ALTERNATE_CONTACT_CURSOR","altcontacts","altcontact");
        DOMUtil.addSection(data,vendorMasterXML.getChildNodes());
        DOMUtil.addSection(data,primaryContactXML.getChildNodes());
        DOMUtil.addSection(data,secondaryContactXML.getChildNodes());

        return data;
    }

    public String getNextVendorId() throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      logger.debug("getNextVendorId() :: String ");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_NEXT_VENDOR_ID");

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return ((String) dataAccessUtil.execute(dataRequest));
    }


    public Document getVendorIdForProduct(String productId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      logger.debug("getVendorIdForProduct() :: String ");

      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_VENDOR_ID_FOR_PRODUCT");
      dataRequest.addInputParam("IN_PRODUCT_ID", productId);

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return ((Document) dataAccessUtil.execute(dataRequest));
    }


    /**
     * Get list of all available vendors in the DB
     * @return
     * @throws java.lang.Exception
     */
    public List getGlobalBlocksList() throws Exception
    {
        List blockList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_GLOBAL_BLOCKS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
          while ( results.next() )
          {
             VendorBlockVO blockVO = new VendorBlockVO();
             blockVO.setBlockType(results.getString("BLOCK_TYPE"));
             blockVO.setEndDate(results.getDate("END_DATE"));
             blockVO.setGlobalFlag(results.getString("GLOBAL_FLAG"));
             blockVO.setStartDate(results.getDate("START_DATE"));
             blockList.add(blockVO);
          }
        }
       return blockList;
    }

    public void insertGlobalBlock(VendorBlockVO blockVO) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);

        dataRequest.addInputParam("IN_BLOCK_DATE", blockVO.getStartDate() == null ? null : new java.sql.Date(blockVO.getStartDate().getTime()));

        //The statment that is executed depends on the block type
        if(blockVO.getBlockType().equalsIgnoreCase("Shipping"))
        {
            dataRequest.setStatementID("INSERT_GLOBAL_SHIPPING_BLOCK");
        }
        else if(blockVO.getBlockType().equalsIgnoreCase("Delivery"))
        {
            dataRequest.setStatementID("INSERT_GLOBAL_DELIVERY_BLOCK");
        }
        else
        {
            throw new Exception("Unknown block type:" + blockVO.getBlockType());
        }

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

    }


    public void insertVendorBlock(VendorBlockVO blockVO) throws Exception
    {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_BLOCK_TYPE", blockVO.getBlockType());
        dataRequest.addInputParam("IN_VENDOR_ID", blockVO.getVendorId());
        dataRequest.addInputParam("IN_START_DATE", blockVO.getStartDate() == null ? null : new java.sql.Date(blockVO.getStartDate().getTime()));
        dataRequest.addInputParam("IN_END_DATE", blockVO.getEndDate() == null ? null : new java.sql.Date(blockVO.getEndDate().getTime()));
        dataRequest.addInputParam("IN_GLOBAL_FLAG", blockVO.getGlobalFlag());
        dataRequest.setStatementID("INSERT_VENDOR_RESTRICTIONS");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

    }

    /**
     * Delete a global block
     */
     public void deleteGlobalBlock(String blockType, Date blockDate) throws Exception
     {
        logger.debug("deleteGlobalBlock(String blockType("+blockType+"), Date blockDate)");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_GLOBAL_BLOCKS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_BLOCK_TYPE", blockType);
        inputParams.put("IN_BLOCK_DATE", blockDate == null ? null : new java.sql.Date(blockDate.getTime()));
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }

    public void deleteVendorBlock(VendorBlockVO blockVO) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_BLOCK_TYPE", blockVO.getBlockType());
        dataRequest.addInputParam("IN_VENDOR_ID", blockVO.getVendorId());
        dataRequest.addInputParam("IN_START_DATE", blockVO.getStartDate() == null ? null : new java.sql.Date(blockVO.getStartDate().getTime()));
        dataRequest.addInputParam("IN_GLOBAL_FLAG", blockVO.getGlobalFlag());
        dataRequest.setStatementID("DELETE_VENDOR_RESTRICTIONS");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

    }

    /**
     *
     */
     public void insertVendor(VendorMasterVO vendorVO, String cSRId) throws Exception
     {
        logger.debug("insertVendor(VendorMasterVO vendorVO)");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_VENDOR_MASTER");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorVO.getVendorID());
        inputParams.put("IN_VENDOR_NAME", vendorVO.getVendorName());
        inputParams.put("IN_DEFAULT_CARRIER", vendorVO.getShipVia());
        inputParams.put("IN_MEMBER_NUMBER", vendorVO.getVendorCode());
        inputParams.put("IN_VENDOR_TYPE", vendorVO.getVendorType());
        inputParams.put("IN_ACTIVE", "Y");
        inputParams.put("IN_ADDRESS1", vendorVO.getAddress1());
        inputParams.put("IN_ADDRESS2", vendorVO.getAddress2());
        inputParams.put("IN_CITY", vendorVO.getCity());
        inputParams.put("IN_STATE", vendorVO.getState());
        inputParams.put("IN_ZIP_CODE", vendorVO.getZipCode());
        inputParams.put("IN_LEAD_DAYS", vendorVO.getLeadDays());
        inputParams.put("IN_COMPANY", vendorVO.getCompanyCode());
        inputParams.put("IN_GENERAL_INFO", vendorVO.getGeneralInfo());
        inputParams.put("IN_CUTOFF_TIME", vendorVO.getCutoff());
        inputParams.put("IN_ACCOUNTS_PAYABLE_ID", vendorVO.getAccountsPayableId());
        inputParams.put("IN_GL_ACCOUNT_NUMBER", vendorVO.getGlAccountNumber());
        inputParams.put("IN_ZONE_JUMP_ELIGIBLE_FLAG", vendorVO.getZoneJumpEligibleFlag());
        inputParams.put("IN_NEW_SHIPPING_SYSTEM", vendorVO.getNewShippingSystem());

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
        
        dataRequest.setStatementID("UPDATE_VENDOR_ADDON");
       
         if (vendorVO.getAddOnMap() != null) 
         {
             for (String key : vendorVO.getAddOnMap().keySet())
             {
                 for (int i = 0; i< vendorVO.getAddOnMap().get(key).size(); i++)
                 {
                     AddOnVO addOnVO = vendorVO.getAddOnMap().get(key).get(i);
                     if (!vendorVO.getVendorAddOnDisabledSet().contains(addOnVO.getAddOnId()))
                     {
                         VendorAddOnVO vendorAddOnVO = addOnVO.getVendorCostsMap().get("NEW_VENDOR");
                         inputParams = new HashMap();
                         inputParams.put("IN_VENDOR_ID",vendorVO.getVendorID());
                         inputParams.put("IN_ADDON_ID",vendorAddOnVO.getAddOnId());
                         inputParams.put("IN_ACTIVE_FLAG", vendorVO.getVendorAddOnAssignedSet().contains(addOnVO.getAddOnId()) ? "Y" : "N");
                         inputParams.put("IN_VENDOR_SKU",vendorAddOnVO.getSKU());
                         inputParams.put("IN_VENDOR_COST",vendorAddOnVO.getCost());
                         
                         inputParams.put("IN_UPDATED_BY", cSRId);
                         
                         dataRequest.setInputParams(inputParams);
                         outputs = (Map) dataAccessUtil.execute(dataRequest);
                         
                         status = (String) outputs.get("OUT_STATUS");
                         if(status != null && status.equalsIgnoreCase("N"))
                         {
                             String message = (String) outputs.get("OUT_MESSAGE");
                             throw new Exception(message);
                         }
                     }   
                 } 
             }
          }
     }


    /**
     *
     */
     public void updateInventory(InventoryVO invVO) throws Exception
     {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_INVENTORY_CONTROL");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRDOUCT_ID", invVO.getProductId());
        inputParams.put("IN_VENDOR_ID", invVO.getVendorId());
        inputParams.put("IN_CONTACT_NUMBER", invVO.getContactNumber());
        inputParams.put("IN_INVENTORY_LEVEL", invVO.getInventoryLevel());
        inputParams.put("IN_NOTICE_THRESHOLD", invVO.getInventoryThreshold());
        inputParams.put("IN_START_DATE", getSQLDate(invVO.getStartDate()));
        inputParams.put("IN_END_DATE", getSQLDate(invVO.getEndDate()));
        inputParams.put("IN_CONTACT_EXTENSION", invVO.getContactExt());

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }

    /**
     *
     */
     public void deleteInventory(String productId) throws Exception
     {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_INVENTORY_CONTROL");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRDOUCT_ID", productId);

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }


    /**
     *
     */
     public void insertInventory(InventoryVO invVO) throws Exception
     {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_INVENTORY_CONTROL");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRDOUCT_ID", invVO.getProductId());
        inputParams.put("IN_VENDOR_ID", invVO.getVendorId());
        inputParams.put("IN_CONTACT_NUMBER", invVO.getContactNumber());
        inputParams.put("IN_INVENTORY_LEVEL", invVO.getInventoryLevel());
        inputParams.put("IN_NOTICE_THRESHOLD", invVO.getInventoryThreshold());
        inputParams.put("IN_START_DATE", getSQLDate(invVO.getStartDate()));
        inputParams.put("IN_END_DATE", getSQLDate(invVO.getEndDate()));
        inputParams.put("IN_CONTACT_EXTENSION", invVO.getContactExt());

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }


     public void insertInventoryNotification(NotificationVO notifyVO) throws Exception
     {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_INV_CTRL_NOTIFICATIONS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", notifyVO.getProductId());
        inputParams.put("IN_VENDOR_ID", notifyVO.getVendorId());
        inputParams.put("IN_EMAIL_ADDRESS", notifyVO.getEmailAddress());

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }

     public void deleteInventoryNotification(String productId) throws Exception
     {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_ALL_INV_CTRL_NOTIFIS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRDOUCT_ID", productId);

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }


    /**
   * Wrapper for FTD_APPS.VENDOR_PKG.UPDATE_VENDOR_MASTER
   * @throws java.lang.Exception
   * @param vendorVO
   */
     public void updateVendor(VendorMasterVO vendorVO, String cSRId) throws Exception
     {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_VENDOR_MASTER");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorVO.getVendorID());
        inputParams.put("IN_VENDOR_NAME", vendorVO.getVendorName());
        inputParams.put("IN_DEFAULT_CARRIER", vendorVO.getShipVia());
        inputParams.put("IN_MEMBER_NUMBER", vendorVO.getVendorCode());
        inputParams.put("IN_VENDOR_TYPE", vendorVO.getVendorType());
        inputParams.put("IN_ACTIVE", "Y");
        inputParams.put("IN_ADDRESS1", vendorVO.getAddress1());
        inputParams.put("IN_ADDRESS2", vendorVO.getAddress2());
        inputParams.put("IN_CITY", vendorVO.getCity());
        inputParams.put("IN_STATE", vendorVO.getState());
        inputParams.put("IN_ZIP_CODE", vendorVO.getZipCode());
        inputParams.put("IN_LEAD_DAYS", vendorVO.getLeadDays());
        inputParams.put("IN_COMPANY", vendorVO.getCompanyCode());
        inputParams.put("IN_GENERAL_INFO", vendorVO.getGeneralInfo());
        inputParams.put("IN_CUTOFF_TIME", vendorVO.getCutoff());
        inputParams.put("IN_ACCOUNTS_PAYABLE_ID", vendorVO.getAccountsPayableId());
        inputParams.put("IN_GL_ACCOUNT_NUMBER", vendorVO.getGlAccountNumber());
        inputParams.put("IN_ZONE_JUMP_ELIGIBLE_FLAG", vendorVO.getZoneJumpEligibleFlag());
        inputParams.put("IN_NEW_SHIPPING_SYSTEM", vendorVO.getNewShippingSystem());
         
        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

        dataRequest.setStatementID("UPDATE_VENDOR_ADDON");
         
        if (vendorVO.getAddOnMap() != null) 
        {
            for (String key : vendorVO.getAddOnMap().keySet())
            {
                for (int i = 0; i< vendorVO.getAddOnMap().get(key).size(); i++)
                {
                    AddOnVO addOnVO = vendorVO.getAddOnMap().get(key).get(i);
                    if (!vendorVO.getVendorAddOnDisabledSet().contains(addOnVO.getAddOnId()) && vendorVO.getVendorAddOnChangedSet().contains(addOnVO.getAddOnId()))
                    {
                        VendorAddOnVO vendorAddOnVO = addOnVO.getVendorCostsMap().get(vendorVO.getVendorID());
                        inputParams = new HashMap();
                        inputParams.put("IN_VENDOR_ID",vendorVO.getVendorID());
                        inputParams.put("IN_ADDON_ID",vendorAddOnVO.getAddOnId());
                        inputParams.put("IN_ACTIVE_FLAG", vendorVO.getVendorAddOnAssignedSet().contains(addOnVO.getAddOnId()) ? "Y" : "N");
                        inputParams.put("IN_VENDOR_SKU",vendorAddOnVO.getSKU());
                        inputParams.put("IN_VENDOR_COST",vendorAddOnVO.getCost());
                        
                        inputParams.put("IN_UPDATED_BY", cSRId);
                        
                        dataRequest.setInputParams(inputParams);
                        outputs = (Map) dataAccessUtil.execute(dataRequest);
                        
                        status = (String) outputs.get("OUT_STATUS");
                        if(status != null && status.equalsIgnoreCase("N"))
                        {
                            String message = (String) outputs.get("OUT_MESSAGE");
                            throw new Exception(message);
                        }
                    }   
                } 
            }
         }
     }

    /**
     * This method is responsible for updating the shipping system for all
     * the products associated with a given vendor if the vendor type was
     * switched to SDS.
     * It is only a one time thing for each vendor to switch the products to the
     * SDS/Argo shipping system. Once all the vendors have been moved to SDS/Argo,
     * this method is no longer needed.
     *
     * Wrapper for FTD_APPS.VENDOR_PKG.UPDATE_SET_PRODUCT_SS
     * @throws java.lang.Exception
     * @param vendorVO
     */
     public String updateVendorProductsForSDS(VendorMasterVO vendorVO) throws Exception
     {
        String message = null;

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_SET_PRODUCT_SS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorVO.getVendorID());

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          message = (String) outputs.get("OUT_MESSAGE");
          if(message != null && message.startsWith("WARNING:"))
            message = message.substring(8);
          else
            throw new Exception(message);
        }

        return message;
     }


    /**
   * Wrapper for FTD_APPS.VENDOR_PKG.INSERT_VENDOR_CONTACT
   * @throws java.lang.Exception
   * @param contactVO
   */
     public void insertContact(ContactVO contactVO) throws Exception
     {
        logger.debug("insertContact(ContactVO contactVO)");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_VENDOR_CONTACT");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", contactVO.getVendorId());
        inputParams.put("IN_CONTACT_NAME", contactVO.getName());
        inputParams.put("IN_PRIMARY", contactVO.getPrimaryContact());
        inputParams.put("IN_PHONE", contactVO.getPhone());
        inputParams.put("IN_PHONE_EXTENSION", contactVO.getPhoneExt());
        inputParams.put("IN_FAX", contactVO.getFax());
        inputParams.put("IN_FAX_EXTENSION", contactVO.getFaxExt());
        inputParams.put("IN_EMAIL", contactVO.getEmail());

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
     }

    /**
   * Wrapper for FTD_APPS.VENDOR_PKG.DELETE_ALL_VENDOR_CONTACTS
   * @throws java.lang.Exception
   * @param vendorId
   */
     public void deleteAllContacts(String vendorId) throws Exception
     {
        logger.debug("deleteAllContacts(String vendorId ("+vendorId+"))");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_ALL_VENDOR_CONTACTS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          logger.debug(message);
          // we don't want to throw this because mostly it's just a warning //
        }

     }

    /**
     *
     */
     public void deleteVendor(String vendorId) throws Exception
     {
        logger.debug("deleteVendor(String vendorId ("+vendorId+"))");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INACTIVATE_VENDOR");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
     }


    private java.sql.Date getSQLDate(java.util.Date utilDate)
    {
        return utilDate == null ? null : new java.sql.Date(utilDate.getTime());
    }


  /**
   * Populates a VendorMasterVO with values from the FTD_APPS_VENDOR_MASTER table
   *
   * @param String - vendorId
   * @return VendorMasterVO
   */
    public VendorMasterVO getVendorMaster(String vendorId) throws Exception
    {
        logger.debug("getVendorMaster(String vendorId)");
        DataRequest dataRequest = new DataRequest();
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);

         /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_VENDOR_MASTER_ONLY");
        dataRequest.setInputParams(inputParams);


        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        VendorMasterVO vmVO = new VendorMasterVO();

        if(crs.next())
        {
        	vmVO.setVendorID(crs.getString("vendor_id"));
        	vmVO.setVendorName(crs.getString("vendor_name"));
          vmVO.setCutoff(crs.getString("cutoff_time"));
        }
        else
        {
        	throw new Exception("Could not find vendorId " + vendorId);
        }
        
        AddOnUtility addOnUtility = new AddOnUtility();
        HashMap <String, ArrayList <AddOnVO>> addOnMap = new HashMap <String, ArrayList <AddOnVO>> ();
        addOnMap = addOnUtility.getAssignedAddonListByVendor(vendorId,this.conn);
        vmVO.setAddOnMap(addOnMap);
 
        return vmVO;
    }


    /** Obtains all vendor ids
     * @return
     * @throws java.lang.Exception
     */
    public List getVendorIds() throws Exception
    {
        List vendorIds = new ArrayList();
        logger.debug("getVendorIds()");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_VENDOR_IDS");
        CachedResultSet results = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null )
        {
          while ( results.next() )
          {
            vendorIds.add(results.getString("vendor_id"));
          }
        }
        return vendorIds;
    }


     /**
     * Resets a vendor cutoff time
     * @param vendorId vendor id to update
     * @param cutoffTime cutoff time
     */
    public void insertVendorCutoff(String vendorId, String cutoffTime) throws Exception
    {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_VENDOR_CUTOFF");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        inputParams.put("IN_CUTOFF_TIME", cutoffTime);

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }

     }


    /**
    * This method is a wrapper for the FRP.MISC_PKG.UPDATE_GLOBAL_PARAMETER.
    * Save a global parameter.
    *
    * @param context parameter context
    * @param name parameter name
    * @param value parameter value
    */
    public void setGlobalParameter(String context, String name, String value, String updatedBy) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        try
        {
            logger.debug("setGlobalParameter(String context ("+context+"), String name("+name+"), String value("+value+"))");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CONTEXT", context);
            inputParams.put("IN_NAME", name);
            inputParams.put("IN_VALUE", value);
            inputParams.put("IN_UPDATED_BY", updatedBy);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("UPDATE_GLOBAL_PARMS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map) dataAccessUtil.execute(dataRequest);

            /* read store prodcedure output parameters to determine
            * if the procedure executed successfully */
            String status = (String) outputs.get("OUT_STATUS");
            if(status != null && status.equalsIgnoreCase("N"))
            {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
            }
        }
        catch (Exception e)
        {
            logger.error(e);
            throw e;
        }
    }


    /**
    * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.
    * It populates a global parameter VO based on the returned record set.
    *
    * @param context filter for context
    * @param name filter for parameter name
    * @return GlobalParameterVO
    */
    public GlobalParameterVO getGlobalParameter(String context, String name) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        GlobalParameterVO param = new GlobalParameterVO();
        boolean isEmpty = true;
        String outputs = null;

    try
    {
        logger.debug("getGlobalParameter (String context, String name) :: GlobalParameterVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CONTEXT", context);
        inputParams.put("IN_NAME", name);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (String) dataAccessUtil.execute(dataRequest);
        if (outputs != null)
            isEmpty = false;
        /* populate object */
        param.setName(name);
        param.setValue(outputs);
    }
    catch (Exception e)
    {
        logger.error(e);
        throw e;
    }
    if (isEmpty)
        return null;
    else
        return param;
    }


    /**
     * Deletes the inventory control record for the passed product/vendor
     * @param productId filter
     * @param vendorId filter
     */
    public void deleteInventory(String productId, String vendorId) throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_INVENTORY_CONTROL");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRDOUCT_ID", productId);
        inputParams.put("IN_VENDOR_ID", vendorId);

        dataRequest.setInputParams(inputParams);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }


    /**
     * Determine if an inventory record exists for the passed product/vendor
     * @param productId filter
     * @param vendorId fileter
     * @return true if found else false
     * @throws Exception
     */
    public boolean inventoryRecordExists(String productId, String vendorId) throws Exception {

        Document data = DOMUtil.getDefaultDocument();
        DataRequest dataRequest = new DataRequest();

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRDOUCT_ID", productId);
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_INVENTORY_CONTROL_RECORD_SET");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet rs =
            (CachedResultSet)dataAccessUtil.execute(dataRequest);

        return rs == null || rs.getRowCount() == 0 ? false : true;
    }

    /**
     * This method gets the vendor/partner/shipping/carrier/account information
     * @param vendorId
     * @return Document containing partner shipping accounts information
     * @throws Exception
     */
    public Document getPartnerShippingAccounts(String vendorId) throws Exception
    {
        logger.debug("getPartnerShippingAccounts(String vendorId): "+vendorId);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_PARTNER_SHIPPING_ACCOUNTS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Document doc =
            (Document) dataAccessUtil.execute(dataRequest);
        return doc;
    }

    /**
     * This method gets the partner/shipping/carrier/account information for 
     * adding a new vendor.
     * @return Document containing default shipping accounts information
     * @throws Exception
     */
    public Document getDefaultShippingAccounts() throws Exception
    {
        logger.debug("getDefaultShippingAccounts(): ");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_DEFAULT_SHIPPING_ACCOUNTS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Document doc =
            (Document) dataAccessUtil.execute(dataRequest);
        return doc;
    }


    /**
     * This method insert/updates to the partner_shipping_account table
     * @param vendorVO
     * @return
     * @throws Exception
     */
        public void updatePartnerShippingAccounts(VendorMasterVO vendorVO) throws Exception {

            DataRequest dataRequest = new DataRequest();
            List partnerShippingAccounts = vendorVO.getPartnerShippingAccounts();
            for (int i = 0; i< partnerShippingAccounts.size(); i++){
                PartnerShippingAccountVO  vo = (PartnerShippingAccountVO) partnerShippingAccounts.get(i);
                HashMap inputParams = new HashMap();
                inputParams.put("IN_VENDOR_ID", vendorVO.getVendorID());
                inputParams.put("IN_CARRIER_ID", vo.getCarrierId());
                inputParams.put("IN_PARTNER_ID", vo.getPartnerId());
                inputParams.put("IN_ACCOUNT_NUM", vo.getAccountNum());
                inputParams.put("IN_VALIDATION_REGEX", vo.getValidationRegex());
                dataRequest.setInputParams(inputParams);

                dataRequest.setConnection(this.conn);
                dataRequest.setStatementID("UPDATE_PARTNER_SHIPPING_ACCNTS");
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
                String status = (String) outputs.get("OUT_STATUS");
                if(status != null && status.equalsIgnoreCase("N"))
                {
                  String message = (String) outputs.get("OUT_MESSAGE");
                  throw new Exception(message);
                }

            }

        }
        
        /**
         * Retrieve list of vendors associated with active Zone Jump
         * Trip
         * @return
         * @throws java.lang.Exception
         */
        public List zoneJumpTripExists(String vendorId, String blockDate, String blockType) throws Exception
        {
            logger.debug("zoneJumpTripExists(String vendorId ("+vendorId+"))");

            List zoneJumpTrips = new ArrayList();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("ZJ_TRIP_EXISTS");
            dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
            dataRequest.addInputParam("IN_BLOCK_DATE", blockDate);
            dataRequest.addInputParam("IN_BLOCK_TYPE", blockType);
 
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            if ( rs != null )
            {
              while ( rs.next() )
              {
                  zoneJumpTrips.add(rs.getString("vendor_id"));
              }
            }
            return zoneJumpTrips;
        }
    
  /**
   * @return Document containing the values from the cursor
   *
   * @throws java.lang.Exception
   */
  public CachedResultSet getProductVendor(String sProductId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
  
    inputParams.put("IN_PRODUCT_ID", sProductId);
    dataRequest.setInputParams(inputParams);

    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_PRODUCT_AND_VENDOR_INFO_CRS");
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet results = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    return results; 

  }
  
  /**
     * Returns an Document with the addon and vendor information for a specific add-on
     * @param vendorId The vendorId to search on
     * @return
     * @throws Exception
     */
  public Document getVendorAddon(String vendorId) throws Exception
  {
      HashMap <String, ArrayList <AddOnVO>> addOnMap = new HashMap <String, ArrayList <AddOnVO>> ();
      AddOnUtility addOnUtility = new AddOnUtility();
      addOnMap = addOnUtility.getAllAddonListByVendor(vendorId,this.conn);
      Document addOnXML = addOnUtility.convertAddOnMapToXML(addOnMap);
      return addOnXML;
  }
}
