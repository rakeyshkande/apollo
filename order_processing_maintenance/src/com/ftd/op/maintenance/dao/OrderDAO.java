package com.ftd.op.maintenance.dao;
import com.ftd.op.maintenance.constants.OrderConstants;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.CompanyVO;
import com.ftd.op.maintenance.vo.CustomerVO;
import com.ftd.op.maintenance.vo.EmailVO;
import com.ftd.op.maintenance.vo.OrderDetailVO;
import com.ftd.op.maintenance.vo.OrderVO;
import com.ftd.op.maintenance.vo.VenusMessageVO;
import com.ftd.op.maintenance.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;

import org.xml.sax.SAXException;

public class OrderDAO
{
  private Connection conn;
  private Logger logger;

  /**
     * Constructor
     *
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
  public OrderDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.op.maintenance.dao.OrderDAO");
  }


  /**
   * This method is a wrapper for the GET_ORDER_DETAILS SP.
   * It populates an Order Detail VO based on the returned record set.
   *
   * @param String - order detail id
   * @return OrderDetailVO
   */
  public OrderDetailVO getOrderDetail(String orderDetailID)throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    OrderDetailVO orderDetail = new OrderDetailVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

    try
    {
      logger.debug("getOrderDetail (String orderDetailID("+orderDetailID+")) :: OrderDetailVO");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("ORDER_DETAIL_ID", new Long(orderDetailID));

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_ORDER_DETAILS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        orderDetail.setOrderDetailId(outputs.getLong("ORDER_DETAIL_ID"));

        orderDetail.setDeliveryDate(outputs.getString("delivery_date")==null?null:df.parse(outputs.getString("delivery_date")));

        orderDetail.setRecipientId(outputs.getLong("RECIPIENT_ID"));

        orderDetail.setProductId(outputs.getString("PRODUCT_ID"));
        orderDetail.setQuantity(outputs.getLong("QUANTITY"));
        orderDetail.setExternalOrderNumber(outputs.getString("EXTERNAL_ORDER_NUMBER"));
        orderDetail.setColor1(outputs.getString("COLOR_1"));
        orderDetail.setColor2(outputs.getString("COLOR_2"));
        orderDetail.setSubstitutionIndicator(outputs.getString("SUBSTITUTION_INDICATOR"));
        orderDetail.setSameDayGift(outputs.getString("SAME_DAY_GIFT"));
        orderDetail.setOccasion(outputs.getString("OCCASION"));
        orderDetail.setCardMessage(outputs.getString("CARD_MESSAGE"));
        orderDetail.setCardSignature(outputs.getString("CARD_SIGNATURE"));
        orderDetail.setSpecialInstructions(outputs.getString("SPECIAL_INSTRUCTIONS"));
        orderDetail.setReleaseInfoIndicator(outputs.getString("RELEASE_INFO_INDICATOR"));
        orderDetail.setFloristId(outputs.getString("FLORIST_ID"));
        orderDetail.setShipMethod(outputs.getString("SHIP_METHOD"));
        orderDetail.setShipDate(outputs.getDate("SHIP_DATE"));
        orderDetail.setOrderDispCode(outputs.getString("ORDER_DISP_CODE"));
        orderDetail.setScrubbedOn(outputs.getDate("SCRUBBED_ON_DATE"));
        orderDetail.setScrubbedBy(outputs.getString("USER_ID"));
        orderDetail.setOrderGuid(outputs.getString("ORDER_GUID"));
        orderDetail.setSourceCode(outputs.getString("SOURCE_CODE"));
        orderDetail.setSecondChoiceProduct(outputs.getString("SECOND_CHOICE_PRODUCT"));
        orderDetail.setRejectRetryCount(outputs.getLong("REJECT_RETRY_COUNT"));
        orderDetail.setSizeIndicator(outputs.getString("SIZE_INDICATOR"));
        orderDetail.setSubcode(outputs.getString("SUBCODE"));
        orderDetail.setOpStatus(outputs.getString("OP_STATUS"));
        orderDetail.setCarrierDelivery(outputs.getString("CARRIER_DELIVERY"));
        orderDetail.setCarrierId(outputs.getString("CARRIER_ID"));
        orderDetail.setVenusMethodOfPayment(outputs.getString("METHOD_OF_PAYMENT"));
        orderDetail.setOrderGuid(outputs.getString("ORDER_GUID"));
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw e;
    }
    if (isEmpty)
      return null;
    else
      return orderDetail;
  }


  /**
   * findOrderNumber
   *
   * @param a_orderNum
   * @throws java.lang.Exception
   */
  public HashMap findOrderNumber(String a_orderNum) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("FIND_ORDER_NUMBER");
    dataRequest.addInputParam("IN_ORDER_NUMBER", a_orderNum);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    HashMap searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }

  /**
   * This method is a wrapper for the SP_GET_ORDER SP.
   * It populates a Order VO based on the returned record set.
   *
   * @param String - orderGuid
   * @return OrderVO
   */
  public OrderVO getOrder(String orderGuid) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    OrderVO order = new OrderVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;

    try
    {
      logger.debug("getOrder (String orderGuid) :: OrderVO");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("ORDER_GUID", orderGuid);

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_ORDER_BY_GUID");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        order.setMasterOrderNumber(outputs.getString("MASTER_ORDER_NUMBER"));
        order.setCustomerId(outputs.getLong("CUSTOMER_ID"));
        order.setMembershipId(outputs.getLong("MEMBERSHIP_ID"));
        order.setCompanyId(outputs.getString("COMPANY_ID"));
        order.setSourceCode(outputs.getString("SOURCE_CODE"));
        order.setOriginId(outputs.getString("ORIGIN_ID"));
        order.setOrderDate(outputs.getDate("ORDER_DATE"));
        order.setOrderTotal(outputs.getDouble("ORDER_TOTAL"));
        order.setProductTotal(outputs.getDouble("PRODUCT_TOTAL"));
        order.setAddOnTotal(outputs.getDouble("ADD_ON_TOTAL"));
        order.setServiceFeeTotal(outputs.getDouble("SERVICE_FEE_TOTAL"));
        order.setShippingFeeTotal(outputs.getDouble("SHIPPING_FEE_TOTAL"));
        order.setDiscountTotal(outputs.getDouble("DISCOUNT_TOTAL"));
        order.setTaxTotal(outputs.getDouble("TAX_TOTAL"));
        order.setLossPreventionIndicator(outputs.getString("LOSS_PREVENTION_INDICATOR"));
        order.setLanguageId(outputs.getString("LANGUAGE_ID"));
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw e;
    }
    if (isEmpty)
      return null;
    else
      return order;
  }

  /**
   * This method is a wrapper for the SP_GET_CUSTOMER SP.
   * It populates a customer VO based on the returned record set.
   *
   * @param String - customerId
   * @return CustomerVO
   */
  public CustomerVO getCustomer(long customerId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    CustomerVO customer = new CustomerVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    CachedResultSet outputs2 = null;

    try
    {
      logger.debug("getCustomer (long customerId ("+customerId+")) :: CustomerVO");

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("CUSTOMER_ID", new Long(customerId));

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_CUSTOMER");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        customer.setFirstName(outputs.getString("CUSTOMER_FIRST_NAME"));
        customer.setLastName(outputs.getString("CUSTOMER_LAST_NAME"));
        customer.setAddress1(outputs.getString("CUSTOMER_ADDRESS_1"));
        customer.setAddress2(outputs.getString("CUSTOMER_ADDRESS_2"));
        customer.setCity(outputs.getString("CUSTOMER_CITY"));
        customer.setState(outputs.getString("CUSTOMER_STATE"));

        // fixed to deal with international zip_code of N/A

        String zip_code = outputs.getString("CUSTOMER_ZIP_CODE");
        if(zip_code != null) {
          zip_code = zip_code.substring(0, zip_code.length() >= 5 ? 5 : zip_code.length());
          customer.setZipCode(zip_code);
        }

        customer.setCountry(outputs.getString("CUSTOMER_COUNTRY"));
      }

      /* setup store procedure input parameters */
      HashMap inputParams2 = new HashMap();
      inputParams2.put("CUSTOMER_ID", new Long(customerId).toString());



    }
    catch (Exception e)
    {
        logger.error(e);
        throw e;
    }
    if (isEmpty)
      return null;
    else
      return customer;
  }


  /**
   * This method is a wrapper for the SP_UPDATE_ORDER_DETAIL SP.
   * It updates an order detail record to match the passed in VO.
   *
   * This method varies from updateOrder in that numeric values in the
   * VO are treated as null.  Also this uses a different statement
   * because the one used in updateOrder will not work when you if any
   * of the numbers are null.
   *
   * Note: Passing a null to the stored proc will prevent the stored proc
   * from updating the field.
   *
   * @param OrderDetailVO
   * @return none
   */
  public void updateOrderDetail(OrderDetailVO orderDtl, String updatedBy) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;


      logger.debug("updateOrder (OrderDetailVO orderDtl ("+orderDtl.getOrderDetailId()+")) :: void" + orderDtl);
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(OrderConstants.ORDER_DETAIL_ID, new Long(orderDtl.getOrderDetailId()));
      inputParams.put(OrderConstants.DELIVERY_DATE, orderDtl.getDeliveryDate()==null?null:new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
      inputParams.put(OrderConstants.RECIPIENT_ID, new Integer(java.sql.Types.INTEGER));
      inputParams.put(OrderConstants.PRODUCT_ID, orderDtl.getProductId());
      inputParams.put(OrderConstants.QUANTITY, new Long(orderDtl.getQuantity()));
      inputParams.put(OrderConstants.COLOR_1, orderDtl.getColor1());
      inputParams.put(OrderConstants.COLOR_2, orderDtl.getColor2());
      inputParams.put(OrderConstants.SUBSTITUTION_INDICATOR, orderDtl.getSubstitutionIndicator());
      inputParams.put(OrderConstants.SAME_DAY_GIFT, orderDtl.getSameDayGift());
      inputParams.put(OrderConstants.OCCASION, orderDtl.getOccasion());
      inputParams.put(OrderConstants.CARD_MESSAGE, orderDtl.getCardMessage());
      inputParams.put(OrderConstants.CARD_SIGNATURE, orderDtl.getCardSignature());
      inputParams.put(OrderConstants.SPECIAL_INSTRUCTIONS, orderDtl.getSpecialInstructions());
      inputParams.put(OrderConstants.RELEASE_INFO_INDICATOR,orderDtl.getReleaseInfoIndicator());
      inputParams.put(OrderConstants.FLORIST_ID, orderDtl.getFloristId());
      inputParams.put(OrderConstants.SHIP_METHOD, orderDtl.getShipMethod());
      inputParams.put(OrderConstants.SHIP_DATE, orderDtl.getShipDate()==null?null:new java.sql.Date(orderDtl.getShipDate().getTime()));
      inputParams.put(OrderConstants.ORDER_DISP_CODE, orderDtl.getOrderDispCode());
      inputParams.put(OrderConstants.SECOND_CHOICE_PRODUCT, orderDtl.getSecondChoiceProduct());
      inputParams.put(OrderConstants.ZIP_QUEUE_COUNT, new Long(orderDtl.getZipQueueCount()));
      inputParams.put("REJECT_RETRY_COUNT", orderDtl.getRejectRetryCount() + "");
      inputParams.put(OrderConstants.UPDATED_BY, updatedBy);
      inputParams.put(OrderConstants.DELIVERY_DATE_RANGE_END, orderDtl.getDeliveryDateRangeEnd()==null?null:new java.sql.Date(orderDtl.getDeliveryDateRangeEnd().getTime()));
      inputParams.put(OrderConstants.SCRUBBED_ON, orderDtl.getScrubbedOn() == null ? null : new java.sql.Timestamp(orderDtl.getScrubbedOn().getTime()));
      inputParams.put(OrderConstants.SCRUBBED_BY, orderDtl.getScrubbedBy());
      inputParams.put(OrderConstants.ARIBA_UNSPSC_CODE, orderDtl.getAribaUnspscCode());
      inputParams.put(OrderConstants.ARIBA_PO_NUMBER, orderDtl.getAribaPoNumber());
      inputParams.put(OrderConstants.ARIBA_AMS_PROJECT_CODE, orderDtl.getAribaAmsProjectCode());
      inputParams.put(OrderConstants.ARIBA_COST_CENTER, orderDtl.getAribaCostCenter());
      inputParams.put(OrderConstants.SIZE_INDICATOR, orderDtl.getSizeIndicator());
      inputParams.put(OrderConstants.MILES_POINTS, null); // never updated by OP
      inputParams.put(OrderConstants.SUBCODE, orderDtl.getSubcode());
      inputParams.put(OrderConstants.SOURCE_CODE, orderDtl.getSourceCode());
      inputParams.put("OP_STATUS", orderDtl.getOpStatus());


      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("UPDATE_ORDER_DETAILS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String) outputs.get(OrderConstants.OUT_STATUS_PARAM);
        if(status != null && status.equalsIgnoreCase(OrderConstants.VL_NO))
        {
          String message = (String) outputs.get(OrderConstants.OUT_MESSAGE_PARAM);
          throw new Exception(message);
        }
  }

   /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_MESSAGE_BY_VENUS_ID
   * stored procedure.
   *
   * @param String String venusId
   * @return VenusMessageVO
   * @throws IOException, ParserConfigurationException, SQLException, SAXException
   */

  public VenusMessageVO getVenusMessageById(String venusId) throws IOException, ParserConfigurationException, SQLException, SAXException
  {

    CachedResultSet crs = null;

    VenusMessageVO msgVO = null;

    logger.debug("VenusDAO.getVenusMessage(String venusId (" +venusId+ " )");
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_VENUS_MESSAGE_BY_VENUS_ID");
    dataRequest.addInputParam("IN_VENUS_ID", venusId);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      while ( crs.next() )
      {
        msgVO = new VenusMessageVO();
        msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
        msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
        msgVO.setVenusStatus((String)crs.getObject("VENUS_STATUS"));
        msgVO.setMsgType((String)crs.getObject("MSG_TYPE"));
        msgVO.setSendingVendor((String)crs.getObject("SENDING_VENDOR"));
        msgVO.setFillingVendor((String)crs.getObject("FILLING_VENDOR"));
        msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
        msgVO.setRecipient((String)crs.getObject("RECIPIENT"));
        msgVO.setBusinessName((String)crs.getObject("BUSINESS_NAME"));
        msgVO.setAddress1((String)crs.getObject("ADDRESS_1"));
        msgVO.setAddress2((String)crs.getObject("ADDRESS_2"));
        msgVO.setCity((String)crs.getObject("CITY"));
        msgVO.setState((String)crs.getObject("STATE"));
        msgVO.setZip((String)crs.getObject("ZIP"));
        msgVO.setCountry((String)crs.getObject("COUNTRY"));
        msgVO.setPhoneNumber((String)crs.getObject("PHONE_NUMBER"));
        msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
        msgVO.setFirstChoice((String)crs.getObject("FIRST_CHOICE"));
        msgVO.setPrice(new Double(crs.getDouble("PRICE")));
        msgVO.setCardMessage((String)crs.getObject("CARD_MESSAGE"));
        msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
        msgVO.setOperator((String)crs.getObject("OPERATOR"));
        msgVO.setComments((String)crs.getObject("COMMENTS"));
        msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
        msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
        msgVO.setProductID((String)crs.getObject("PRODUCT_ID"));
        msgVO.setCombinedReportNumber((String)crs.getObject("COMBINED_REPORT_NUMBER"));
        msgVO.setRofNumber((String)crs.getObject("ROF_NUMBER"));
        msgVO.setAdjReasonCode((String)crs.getObject("ADJ_REASON_CODE"));
        msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
        msgVO.setProcessedIndicator((String)crs.getObject("PROCESSED_INDICATOR"));
        msgVO.setMessageText((String)crs.getObject("MESSAGE_TEXT"));
        msgVO.setSendToVenus((String)crs.getObject("SEND_TO_VENUS"));
        msgVO.setOrderPrinted((String)crs.getObject("ORDER_PRINTED"));
        msgVO.setSubType((String)crs.getObject("SUB_TYPE"));
        msgVO.setErrorDescription((String)crs.getObject("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode((String)crs.getObject("CANCEL_REASON_CODE"));
        msgVO.setCompOrder((String)crs.getObject("COMP_ORDER"));
        msgVO.setOldPrice(new Double(crs.getDouble("OLD_PRICE")));
        msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
        msgVO.setVendorSKU(crs.getString("VENDOR_SKU"));
        msgVO.setProductDescription(crs.getString("PRODUCT_DESCRIPTION"));
        msgVO.setProductWeight(crs.getString("PRODUCT_WEIGHT"));
      }
    }
    return msgVO;
  }

    /**
   * This is a wrapper for CLEAN.CUSTOMER_QUERY_PKG.GET_CUSTOMER_EMAIL_INFO.
   * @throws java.lang.Exception
   * @return EmailVO
   * @param companyId
   * @param customerId
   */
     public EmailVO getCustomerEmailInfo(long customerId, String companyId) throws Exception
    {
         CachedResultSet results = null;
         EmailVO email = null;

         DataRequest dataRequest = new DataRequest();
         HashMap inParms = new HashMap();

         inParms.put("IN_CUSTOMER_ID", new Long(customerId));
         inParms.put("IN_COMPANY_ID", companyId);

         dataRequest.setConnection(this.conn);
         dataRequest.setStatementID( "GET_CUSTOMER_EMAIL_INFO" );
         dataRequest.setInputParams(inParms);

         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

         results = (CachedResultSet) dataAccessUtil.execute(dataRequest);
         if( results.next() )
         {
              if (results.getObject(3) != null)
              {
                email = new EmailVO();
                email.setEmailAddress(results.getObject(3).toString());
              }
         }
         return email;
    }

  /**
   * This method is a wrapper for the SP_GET_COMPANY SP.
   * It populates a company VO based on the returned record set.
   *
   * @param String - companyId
   * @return CompanyVO
   */
  public CompanyVO getCompany(String companyId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    CompanyVO company = new CompanyVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;

    logger.debug("getCompany (String companyId ("+companyId+")) :: CompanyVO");
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("COMPANY_ID", companyId);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_COMPANY");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    /* populate object */
    while (outputs.next())
    {
      isEmpty = false;
      company.setCompanyId(companyId);
      company.setCompanyName(outputs.getString("COMPANY_NAME"));
      company.setInternetOrigin(outputs.getString("INTERNET_ORIGIN"));
      company.setDefaultProgramId(outputs.getLong("DEFAULT_PROGRAM_ID"));
      company.setPhoneNumber(outputs.getString("PHONE_NUMBER"));
      company.setClearingMemberNumber(outputs.getString("CLEARING_MEMBER_NUMBER"));
      company.setLogoFileName(outputs.getString("LOGO_FILE_NAME"));
      company.setEnableLpProcessing(outputs.getString("ENABLE_LP_PROCESSING"));
      company.setURL(outputs.getString("URL"));
    }
    if (isEmpty)
     return null;
    else
      return company;
  }


  /**
   * This is a wrapper for CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS.
   * @throws java.lang.Exception
   * @param commentsVO
   */
  public void insertComment(CommentsVO commentsVO) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_CUSTOMER_ID", commentsVO.getCustomerId());
      inputParams.put("IN_ORDER_GUID", commentsVO.getOrderGuid());
      inputParams.put("IN_ORDER_DETAIL_ID", commentsVO.getOrderDetailId());
      inputParams.put("IN_COMMENT_ORIGIN",commentsVO.getCommentOrigin());
      inputParams.put("IN_REASON", commentsVO.getReason());
      inputParams.put("IN_DNIS_ID", commentsVO.getDnisId());
      inputParams.put("IN_COMMENT_TEXT", commentsVO.getComment());
      inputParams.put("IN_COMMENT_TYPE", commentsVO.getCommentType());
      inputParams.put("IN_CSR_ID", commentsVO.getCreatedBy());

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("INSERT_COMMENTS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);

      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS_PARAM");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE_PARAM");
        throw new Exception(message);
      }
    }


  /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_MESSAGE
   * stored procedure.
   *
   * @param String venusOrderNumber
   * @param String messsageType
   * @return List messages
   * @throws IOException, ParserConfigurationException, SQLException, SAXException
   */

  public List getVenusMessageByDetailId(String orderDetailId, String messageType) throws Exception
  {
    logger.debug("getVenusMessageByDetailId(String orderDetailId ("+orderDetailId+"), String messageType ("+messageType+"))");
    CachedResultSet crs = null;
    VenusMessageVO msgVO = null;
    ArrayList messages = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_VENUS_MESS_BY_ORDER_DETAIL");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      messages = new ArrayList();

      while ( crs.next() )
      {
        msgVO = new VenusMessageVO();
        msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
        msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
        msgVO.setVenusStatus((String)crs.getObject("VENUS_STATUS"));
        msgVO.setMsgType((String)crs.getObject("MSG_TYPE"));
        msgVO.setSendingVendor((String)crs.getObject("SENDING_VENDOR"));
        msgVO.setFillingVendor((String)crs.getObject("FILLING_VENDOR"));
        msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
        msgVO.setRecipient((String)crs.getObject("RECIPIENT"));
        msgVO.setBusinessName((String)crs.getObject("BUSINESS_NAME"));
        msgVO.setAddress1((String)crs.getObject("ADDRESS_1"));
        msgVO.setAddress2((String)crs.getObject("ADDRESS_2"));
        msgVO.setCity((String)crs.getObject("CITY"));
        msgVO.setState((String)crs.getObject("STATE"));
        msgVO.setZip((String)crs.getObject("ZIP"));
        msgVO.setCountry((String)crs.getObject("COUNTRY"));
        msgVO.setPhoneNumber((String)crs.getObject("PHONE_NUMBER"));
        msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
        msgVO.setFirstChoice((String)crs.getObject("FIRST_CHOICE"));
        msgVO.setPrice(new Double(crs.getDouble("PRICE")));
        msgVO.setCardMessage((String)crs.getObject("CARD_MESSAGE"));
        msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
        msgVO.setOperator((String)crs.getObject("OPERATOR"));
        msgVO.setComments((String)crs.getObject("COMMENTS"));
        msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
        msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
        msgVO.setProductID((String)crs.getObject("PRODUCT_ID"));
        msgVO.setCombinedReportNumber((String)crs.getObject("COMBINED_REPORT_NUMBER"));
        msgVO.setRofNumber((String)crs.getObject("ROF_NUMBER"));
        msgVO.setAdjReasonCode((String)crs.getObject("ADJ_REASON_CODE"));
        msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
        msgVO.setProcessedIndicator((String)crs.getObject("PROCESSED_INDICATOR"));
        msgVO.setMessageText((String)crs.getObject("MESSAGE_TEXT"));
        msgVO.setSendToVenus((String)crs.getObject("SEND_TO_VENUS"));
        msgVO.setOrderPrinted((String)crs.getObject("ORDER_PRINTED"));
        msgVO.setSubType((String)crs.getObject("SUB_TYPE"));
        msgVO.setErrorDescription((String)crs.getObject("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode((String)crs.getObject("CANCEL_REASON_CODE"));
        msgVO.setCompOrder((String)crs.getObject("COMP_ORDER"));
        msgVO.setOldPrice(new Double(crs.getDouble("OLD_PRICE")));
        msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
        msgVO.setVendorSKU(crs.getString("VENDOR_SKU"));
        msgVO.setProductDescription(crs.getString("PRODUCT_DESCRIPTION"));
        msgVO.setProductWeight(crs.getString("PRODUCT_WEIGHT"));
		msgVO.setZoneJumpFlag(crs.getString("ZONE_JUMP_FLAG"));
		msgVO.setZoneJumpLabelDate(crs.getDate("ZONE_JUMP_LABEL_DATE"));
		msgVO.setZoneJumpTrailerNumber(crs.getString("ZONE_JUMP_TRAILER_NUMBER"));
		msgVO.setOrigShipDate(getUtilDate(crs.getObject("ORIG_SHIP_DATE")));
		msgVO.setSecondaryShipDate(getUtilDate(crs.getObject("SECONDARY_SHIP_DATE")));

        messages.add(msgVO);
      }
    }
    return messages;
  }

  /*
   * Get the Long representation of a long.  If numeric value is zero
   * then return null.
   */
   private String getLong(long longValue)
   {
       return longValue == 0 ? null : Long.toString(longValue);
   }

     /*
     * Converts a database timestamp to a java.util.Date.
     */
    private java.util.Date getUtilDate(Object time)
    {


      java.util.Date theDate = null;
      boolean test  = false;
      if(time != null){
              if(test || time instanceof Timestamp)
              {
                Timestamp timestamp = (Timestamp)time;
                theDate = new java.util.Date(timestamp.getTime());
              }
              else
              {
                theDate = (java.util.Date)time;
                //theDate = new java.util.Date(timestamp.getTime());
               }
      }


      return theDate;
    }


        /**
         * Call stored procedure GET_MESSAGE_ORDER_STATUS and return
         * MessageOrderStatus Value Object
         * based on message order number and order detail id.
         * @param messageOrderNumber - String
         * @param messageType - String
         * @return MessageOrderStatusVO
         * @throws SAXException
         * @throws IOException
         * @throws ParserConfigurationException
         * @throws SQLException
         */
        public MessageOrderStatusVO getMessageOrderStatus(
            Connection conn, String messageOrderNumber, String orderDetailId, String messageType)
            throws SAXException, IOException, SQLException, ParserConfigurationException{
          MessageOrderStatusVO statusVO=new MessageOrderStatusVO();
          logger.debug("messageOrderNumber: " + messageOrderNumber);
          logger.debug("orderDetailId: " + orderDetailId);
          logger.debug("messageType: " + messageType);
          
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("GET_MESSAGE_ORDER_STATUS");
          dataRequest.addInputParam("IN_MESSAGE_ORDER_NUMBER", messageOrderNumber);
          dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
          dataRequest.addInputParam("IN_ORDER_DETAIL", orderDetailId);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet result=(CachedResultSet)dataAccessUtil.execute(dataRequest);
          statusVO.setMessageOrderNumber(messageOrderNumber);

          if(result.next()){

            statusVO.setOrderDetailId(result.getString("order_detail_id"));
            statusVO.setCancelSent(this.flagToBoolean(result.getString("cancel_sent")));
            statusVO.setDeliveryDate(result.getTimestamp("delivery_date"));
            statusVO.setFillingFloristCode(result.getString("filling_florist"));
            statusVO.setFloristRejectOrder(this.flagToBoolean(result.getString("florist_rej_order")));
            statusVO.setHasLiveFTD(this.flagToBoolean(result.getString("has_live_ftd")));

            statusVO.setAttemptedFTD(this.flagToBoolean(result.getString("ATTEMPTED_FTD")));
            statusVO.setOrderStatus(result.getString("order_status"));
            statusVO.setOrderType(result.getString("order_type"));
            logger.debug("OUT_ORDER_TYPE="+statusVO.getOrderType());
            statusVO.setHasInboundASKP(this.flagToBoolean(result.getString("inbound_askp")));
            statusVO.setATypeRefund(this.flagToBoolean(result.getString("a_type_refund")));
            statusVO.setCancelDenied(this.flagToBoolean(result.getString("cancel_denied")));
            statusVO.setRecipientCountry(result.getString("recipient_country"));
            logger.debug("ATTEMPTED_FTD "+result.getString("ATTEMPTED_FTD"));
            String compOrderFlag=result.getString("comp_order");
            statusVO.setCompOrder(StringUtils.isNotBlank(compOrderFlag)&&compOrderFlag.equalsIgnoreCase("C"));
            logger.debug("Comp Order "+statusVO.isCompOrder());

          }

          if(logger.isDebugEnabled())
          {

            logger.debug("status: attempted_ftd: "+statusVO.isAttemptedFTD());
            logger.debug("status: cancelled: "+statusVO.isCancelSent());
            logger.debug("status: cancel denied: "+statusVO.isCancelDenied());
            logger.debug("status: recipient_country: "+statusVO.getRecipientCountry());
          }
          return statusVO;


        }

    private boolean flagToBoolean(String flag){
      logger.debug("flag: ["+flag+"]");
      if(flag!=null&&flag.trim().equalsIgnoreCase("Y")){
        return true;
      }else{
        return false;
      }
    }

}