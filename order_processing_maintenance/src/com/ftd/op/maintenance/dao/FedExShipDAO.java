package com.ftd.op.maintenance.dao;

import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.ContactVO;
import com.ftd.op.maintenance.vo.FedExScheduleVO;
import com.ftd.op.maintenance.vo.InventoryVO;
import com.ftd.op.maintenance.vo.NotificationVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The purpose of this class is to move data in and out of the VENUS schema which
 * is specific to the FedEx shipping tables
 */
public class FedExShipDAO 
{
    private static String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.FedExShipDAO";
    private Connection conn;
    private Logger logger;
    
  /**
   * Constructor
   * @param conn Database connection
   */
    public FedExShipDAO(Connection conn)
    {
        this.conn = conn;
        logger = new Logger(LOGGER_CATEGORY);    
    }
  
  
  /**
   * Deletes all the records in the VENUS.FEDEX_GROUND_SCHEDULE table by calling
   * VENUS.SHIP_PKG.EMPTY_FEDEX_STAGING_TABLES.  All errors will be returned by 
   * throwning an exception.
   */
    public void emptyFedExGndScheduleStage() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("EMPTY_FEDEX_STAGING_TABLES");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);           
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
        logger.debug("emptyFedExGndScheduleStage - Successful");
    }
  
  /**
   * Inserts a record into the VENUS.FEDEX_GROUND_SCHEDULE table by calling
   * VENUS.SHIP_PKG.INSERT_FEDEX_SCHEDULE_STAGE. All errors will be returned by 
   * throwning an exception.
   * 
   * @throws java.lang.Exception
   * @param vo FedExGroundSchedule object to be inserted into database
   */
    public Document insertFedExGndScheduleStage(String zipImportXML) throws Exception 
    {
        DataRequest dataRequest = new DataRequest();
        
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_SCHED_XML", zipImportXML);
        dataRequest.setStatementID("INSERT_FEDEX_SCHEDULE_STAGE");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);   
        CachedResultSet searchResults = (CachedResultSet) outputs.get("OUT_ERRORS_XML");
        
        /* Build xml output from procedure results */
        Document responseDocument = null;
        if(searchResults.next())
        {
            Clob responseClob = searchResults.getClob(1);
            if(responseClob != null)
            {
            	responseDocument = DOMUtil.getDocument(responseClob.getSubString((long)1, (int) responseClob.length()));
            }
        }
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
        logger.debug("insertFedExGndScheduleStage - Successful");
        
        return responseDocument;
    }
  
  /**
   * Moves the Fedex ground schedules to the production table by calling
   * VENUS.SHIP_PKG.MOVE_FEDEX_STAGING_TO_PROD.  All errors will be returned by 
   * throwning an exception.
   */
    public void updateFedexGndScheduleToProduction() throws Exception 
    {
        DataRequest dataRequest = new DataRequest();
        
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("MOVE_FEDEX_STAGING_TO_PROD");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);           
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
        logger.debug("updateFedexGndScheduleToProduction - Successful");
    }
    
  /**
   * 
   */
    public String insertImportXML(String importXML, String batchId, int seqId, String userId, String system) throws Exception 
    {
        long time = System.currentTimeMillis(); 
        
        // System.out.println(importXML.length());
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_XML_IMPORT");
        dataRequest.addInputParam("IO_XML_IMPORT_BATCH_ID", batchId != null?new Integer(batchId):null);
        dataRequest.addInputParam("IN_XML_SEQ_ID", new Integer(seqId));
        dataRequest.addInputParam("IN_XML_IMPORT_DOC", importXML);
        dataRequest.addInputParam("IN_SYSTEM", system);
        dataRequest.addInputParam("IN_CREATED_BY", userId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest); 
        
        BigDecimal importId = (BigDecimal) outputs.get("IO_XML_IMPORT_BATCH_ID");
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
        logger.debug("Time taken to update database(updateFedexGndScheduleToProduction): " + (System.currentTimeMillis() - time)+ "ms");
        
        logger.debug("updateFedexGndScheduleToProduction - Successful");
        
        return importId.toString();
    }
}