package com.ftd.op.maintenance.dao;

import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.MaintenanceCommentsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.Connection;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CommentsDAO 
{

    private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.CommentsDAO";
    private Connection conn;
    private Logger logger;

    public CommentsDAO(Connection conn)
    {
        this.conn = conn;
        logger = new Logger(LOGGER_CATEGORY);        
    }
    
    public Document getCommentsXML(String commentType, String commentID, int startPage, int maxNumber) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_COMMENT_TYPE", commentType);
        dataRequest.addInputParam("IN_COMMENT_TYPE_ID", commentID);
        dataRequest.addInputParam("IN_STARTING_PAGE", new Integer(startPage));
        dataRequest.addInputParam("IN_RETURN_MAX_NUMBER", new Integer(maxNumber));        
        
        dataRequest.setStatementID("GET_MAINT_COMMENTS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        HashMap map = (HashMap) dataAccessUtil.execute(dataRequest);
      
        Document xmlDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element root = (Element) xmlDoc.createElement("comments");
        xmlDoc.appendChild(root);        
        
        Document parms = (Document)map.get("out-parameters");
        Document report = (Document)map.get("OUT_CURSOR");
        
        DOMUtil.addSection(xmlDoc,parms.getChildNodes());
        DOMUtil.addSection(xmlDoc,report.getChildNodes());
      
      return xmlDoc;
    
    }
    
   public void insertMaintenanceComment(MaintenanceCommentsVO comment) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_COMMENT_TYPE", comment.getCommentType());
        dataRequest.addInputParam("IN_COMMENT_TYPE_ID", comment.getCommentTypeId());
        dataRequest.addInputParam("IN_COMMENT_DESCRIPTION", comment.getDescription());
        dataRequest.addInputParam("IN_CREATED_BY", comment.getCreatedBy());
        dataRequest.setStatementID("INSERT_MAINT_COMMENTS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        logger.debug("Insert Comments Successful. Status="+status);
  }  

}