package com.ftd.op.maintenance.dao;

import com.ftd.op.maintenance.vo.AddonVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.AddOnVO;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Map;

import org.w3c.dom.Document;


public class AddOnDAO
{
    private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.AddOnDAO";
    private Connection conn;
    private Logger logger;

    public AddOnDAO(Connection conn)
    {
        this.conn = conn;
        logger = new Logger(LOGGER_CATEGORY);
    }

    /**
     * Pull in all of the add-on types so that the dropdown on the screen will be populated
     * @return XML with the add-on types and descriptions
     * @throws Exception
     */
    public Document getAllAddOnType()
        throws Exception
    {
        logger.debug("getAllAddOnType():: Document");
        Document data;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("AO_GET_ADDON_TYPE_ALL_XML");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        data = (Document) dataAccessUtil.execute(dataRequest);
        return data;
    }

    /**
     * Saves and add-on
     * @param addOnVO the add-on to save
     * @param csrId the CSR who requested the save
     * @throws Exception
     */
    
    /* #552 - Flexible Fullfillment - Addon Dash board and Maintanance changes  
    sending the parameter IN_DELIVERY_TYPE to the stored procedure.
    */
    public void updateAddon(AddOnVO addOnVO, String csrId)
        throws Exception
    {
        Map outputs = null;
        logger.debug("updateAddon()");
        HashMap inputMap = new HashMap();
        inputMap.put("IN_ADDON_ID", addOnVO.getAddOnId());
        inputMap.put("IN_ADDON_TYPE", addOnVO.getAddOnTypeId());
        inputMap.put("IN_DESCRIPTION", addOnVO.getAddOnDescription());
        inputMap.put("IN_PRICE", new BigDecimal(addOnVO.getAddOnPrice()));
        inputMap.put("IN_ADDON_TEXT", addOnVO.getAddOnText());
        inputMap.put("IN_UNSPSC", addOnVO.getAddOnUNSPSC());
        inputMap.put("IN_PRODUCT_ID", addOnVO.getProductId());
        inputMap.put("IN_ACTIVE_FLAG", addOnVO.getAddOnAvailableFlag()? "Y": "N");
        inputMap.put("IN_DEFAULT_PER_TYPE_FLAG", addOnVO.getDefaultPerTypeFlag()? "Y": "N");
        inputMap.put("IN_DISPLAY_PRICE_FLAG", addOnVO.getDisplayPriceFlag()? "Y": "N");
        inputMap.put("IN_ADDON_WEIGHT", new BigDecimal(addOnVO.getAddOnWeight()));
        inputMap.put("IN_DELIVERY_TYPE", addOnVO.getDeliveryType());
        inputMap.put("IN_CSR_ID", csrId);
        inputMap.put("IN_OCCASIONS", addOnVO.getsLinkedOccasions()); // Changes for Apollo.Q3.2015 - Addon-Occasion association
        /*
         * DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
         * Save the attribute values provided by the user into DB.
         */
        inputMap.put("IN_PQUAD_ACCESSORY_ID", addOnVO.getsPquadAccsryId()==null?null:new BigDecimal(addOnVO.getsPquadAccsryId()));
        inputMap.put("IN_IS_FTD_WEST_ADDON", addOnVO.isFtdWestAddon()?"Y":"N");
        logger.debug("addOnVO.getsLinkedOccasions():"+addOnVO.getsLinkedOccasions()+
        			 "\n. addOnVO.getsPquadAccsryId():"+addOnVO.getsPquadAccsryId()+
        			 "\n. addOnVO.isFtdWestAddon():"+addOnVO.isFtdWestAddon());
        

        DataRequest dataRequest = new DataRequest();
        dataRequest.setInputParams(inputMap);
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("AO_UPDATE_ADDON_CRS");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    /**
     * Changes for Apollo.Q3.2015 - Addon-Occasion association
     * Pull in all the occasions that are to be displayed on Add-on screen.
     * @return XML with the occasion names and their IDs.
     * @throws Exception
     */
    public Document getAllOccassions() throws Exception
    {
        logger.debug("getAllOccassions():: Document");
        Document data;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("AO_GET_ALL_OCCASIONS_XML");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        data = (Document) dataAccessUtil.execute(dataRequest);
        return data;
    }
    
    
    /**
     * This method is a wrapper for the  CLEAN.ORDER_QUERY_PKG.GET_ORDER_ADD_ON.
     * @param String - orderDetailId
     * @return List
     */
	public ArrayList getAddOns(String orderDetailId) throws SQLException,
			Exception {
		DataRequest dataRequest = new DataRequest();
		ArrayList addOnList = new ArrayList();

		CachedResultSet outputs = null;

		try {
			logger.info("getAddOns (String " + orderDetailId + ") ::AddOnVO[]");
			/* setup store procedure input parameters */
			HashMap inputParams = new HashMap();
			inputParams.put("ID_STR", orderDetailId);

			/* build DataRequest object */
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("OP_GET_ORDER_ADD_ON");
			dataRequest.setInputParams(inputParams);

			/* execute the store prodcedure */
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			while (outputs.next()) {
				AddonVO addOn = new AddonVO();
				addOn.setAddOnCode(outputs.getString("ADD_ON_CODE"));
				addOn.setAddOnId(outputs.getLong("ORDER_ADD_ON_ID"));
				addOn.setAddOnQuantity(outputs.getLong("ADD_ON_QUANTITY"));
				addOn.setOrderDetailId(outputs.getLong("ORDER_DETAIL_ID"));
				addOn.setDesciption(outputs.getString("DESCRIPTION"));
				addOn.setPrice(new Double(outputs.getDouble("PRICE")));
				addOnList.add(addOn);
			}
		} catch (Exception e) {
			logger.error(e);
			throw new Exception(e.toString());
		}
		return (addOnList);
	}

    
}
