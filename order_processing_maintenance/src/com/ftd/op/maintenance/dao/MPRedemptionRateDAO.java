package com.ftd.op.maintenance.dao;

import com.ftd.op.maintenance.vo.MPRedemptionRateVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Map;

import org.w3c.dom.Document;


public class MPRedemptionRateDAO 
{

    private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.MPRedemptionRateDAO";
    private Connection conn;
    private Logger logger;

    public MPRedemptionRateDAO(Connection conn)
    {
        this.conn = conn;
        logger = new Logger(LOGGER_CATEGORY);        
    }
    
    public Document getRedemptionRateList() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);      
        dataRequest.setStatementID("GET_MP_REDEMPTION_RATE_LIST");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document xmlDoc = (Document) dataAccessUtil.execute(dataRequest);
        return xmlDoc;
    }
     
    public Document getRedemptionRate(String redemptionRateId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("GET_MP_REDEMPTION_RATE");
        dataRequest.addInputParam("IN_MP_REDEMPTION_RATE_ID", redemptionRateId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document xmlDoc = (Document) dataAccessUtil.execute(dataRequest);
        return xmlDoc; 
    }
    
    public MPRedemptionRateVO getRedemptionRateObject(String redemptionRateId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("GET_MP_REDEMPTION_RATE_OBJECT");
        dataRequest.addInputParam("IN_MP_REDEMPTION_RATE_ID", redemptionRateId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        MPRedemptionRateVO rrvo = null;
        /* populate object */
        if (rs.next())
        {     
              rrvo = new MPRedemptionRateVO();
              rrvo.setRedemptionRateId(redemptionRateId);
              rrvo.setPaymentMethodId(rs.getString("PAYMENT_METHOD_ID"));
              rrvo.setDescription(rs.getString("DESCRIPTION"));
              rrvo.setRedemptionRateAmt(rs.getString("MP_REDEMPTION_RATE_AMT"));
        }
        return rrvo;
    }    
    
    public boolean deleteRedemptionRate(String redemptionRateId) throws Exception
    {
        boolean result = false;
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("DELETE_MP_REDEMPTION_RATE");
        dataRequest.addInputParam("IN_MP_REDEMPTION_RATE_ID", redemptionRateId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("Y"))
        {
            result = true;
        } else
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        return result;
    }    

    public boolean saveRedemptionRate(MPRedemptionRateVO vo) throws Exception
    {
        boolean result = false;
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("UPDATE_MP_REDEMPTION_RATE");
        dataRequest.addInputParam("IN_MP_REDEMPTION_RATE_ID", vo.getRedemptionRateId());
        dataRequest.addInputParam("IN_DESCRIPTION", vo.getDescription());
        dataRequest.addInputParam("IN_PAYMENT_METHOD_ID",vo.getPaymentMethodId());
        dataRequest.addInputParam("IN_REDEMPTION_RATE_AMT", vo.getRedemptionRateAmt());
        dataRequest.addInputParam("IN_UPDATED_BY", vo.getUpdatedBy());
        dataRequest.addInputParam("IN_REQUESTED_BY", vo.getRequestedBy());
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("Y"))
        {
            result = true;
        } else
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        return result;
    }    
    
    public Document getSourceCodeList(String redemptionRateId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("GET_SRC_MEMBER_LEVEL_BY_MP_REDEMPTION_RATE");        
        dataRequest.addInputParam("IN_MP_REDEMPTION_RATE_ID", redemptionRateId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document xmlDoc = (Document) dataAccessUtil.execute(dataRequest);
        return xmlDoc; 
    }       
    
    public Document getMPRedemptionRateHistory(String redemptionRateId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("GET_MP_REDEMPTION_RATE_HISTORY");
        dataRequest.addInputParam("IN_MP_REDEMPTION_RATE_ID", redemptionRateId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document xmlDoc = (Document) dataAccessUtil.execute(dataRequest);
        return xmlDoc; 
    }      
    
    /**
     * Returns a list of comma delimited miles points redemption payment method ids.
     * @return
     * @throws Exception
     */
    public Document getMPPaymentMethodIds() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("GET_MP_PAYMENT_METHOD");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document xmlDoc = (Document)  dataAccessUtil.execute(dataRequest);
        
        return xmlDoc; 
    }   
}