package com.ftd.op.maintenance.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.math.BigDecimal;

import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;


/**
 * ZoneJumpDAO
 *
 * This is the DAO that handles retrieving informtion for the zone jump screens.
 *
 * @author Ali Lakhani
 */
public class

ZoneJumpDAO
{
  private Connection conn;
  private Logger logger;
  private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.ZoneJumpDAO";

  /**
     * Constructor
     *
     * @param conn - database connection
     * @return n/a
     * @throws 
     */
  public ZoneJumpDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger(LOGGER_CATEGORY);
  }

  /**
   * Method to retrieve the injection hub info.  If injectionHubId is null, will retrieve all injection hubs.
   * Else, will only retrieve the info for the injectionHubId passed.
   *
   * @input  String injectionHubId - could be a null or the string containing the injection hub id.
   * @return XMLDocument containing the values from the cursor
   *
   * @throws java.lang.Exception
   */
  public Document getInjectionHubInfo(String injectionHubId)
    throws Exception
  {
    Document searchResults = DOMUtil.getDefaultDocument();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_INJECTION_HUB_INFO_XML");
    dataRequest.addInputParam("IN_INJECTION_HUB_ID", injectionHubId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (Document) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * method to insert an injection hub
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void insertInjectionHub(String ihCity, String ihState, String ihCarrier, String ihAddress, String ihZip, 
                                 String ihSDSAccountNum, String ihShipPointId, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CITY_NAME", ihCity);
    inputParams.put("IN_STATE_MASTER_ID", ihState);
    inputParams.put("IN_CARRIER_ID", ihCarrier);
    inputParams.put("IN_ADDRESS_DESC", ihAddress);
    inputParams.put("IN_ZIP_CODE", ihZip);
    inputParams.put("IN_SDS_ACCOUNT_NUM", ihSDSAccountNum);
    inputParams.put("IN_SHIP_POINT_ID", ihShipPointId);
    inputParams.put("IN_CREATED_BY", csrId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("INSERT_INJECTION_HUB_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to update injection hub
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void updateInjectionHub(String ihId, String ihCity, String ihState, String ihCarrier, String ihAddress, String ihZip, 
                                 String ihSDSAccountNum, String ihShipPointId, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_INJECTION_HUB_ID", ihId);
    inputParams.put("IN_CITY_NAME", ihCity);
    inputParams.put("IN_STATE_MASTER_ID", ihState);
    inputParams.put("IN_CARRIER_ID", ihCarrier);
    inputParams.put("IN_ADDRESS_DESC", ihAddress);
    inputParams.put("IN_ZIP_CODE", ihZip);
    inputParams.put("IN_SDS_ACCOUNT_NUM", ihSDSAccountNum);
    inputParams.put("IN_SHIP_POINT_ID", ihShipPointId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("UPDATE_INJECTION_HUB_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to update cubic inches on ALL active trips
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void updateCubicInchesAllowed(String totalCubicInchesAllowedPerPallet, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PLT_CUBIC_INCH_ALLOWED_QTY", totalCubicInchesAllowedPerPallet);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("UPDATE_TRIP_VENDOR_CUBIC_INCHES_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * Method to retrieve ALL the trips and the vendors associated with each trip.
   *
   * @input  tripStatusCode
   * @return CachedResultSet
   *
   * @throws java.lang.Exception
   */
  public HashMap getTripVendorInfo(String tripStatusCode)
    throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_STATUS_CODE", tripStatusCode);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_TRIP_VENDOR_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * Method to retrieve the trip, vendors, and vendor orders info associated with a trip.
   *
   * @input  tripId
   * @return CachedResultSet
   *
   * @throws java.lang.Exception
   */
  public HashMap getTripVendorOrderInfo(String tripId, String sortyBy)
    throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ID", tripId);
    inputParams.put("IN_SORT_BY", sortyBy);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_TRIP_VENDOR_ORDER_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   */
  public CachedResultSet getVendorBlockInfo(String inputDate, String blockType, String vendorIds)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_BLOCK_DATE", inputDate);
    inputParams.put("IN_BLOCK_TYPE", blockType);
    inputParams.put("IN_VENDOR_IDS", vendorIds);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_VENDOR_BLOCK_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * method to insert a trip
   *
   * @return void
   * @throws java.lang.Exception
   */
  public String insertTrip(String vendorLocation, String hubId, Calendar cDeliveryDate, Calendar cDepartureDate, 
                           String tripStatusCode, String sortCodeStateMasterId, String sortCodeDateMMDD, 
                           String totalPalletQty, String ftdPalletQty, String thirdPartyPalletQty, 
                           String ftdPalletQtyUsed, String csrId, String daysInTransitQty)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    java.sql.Date dDeliveryDate = new java.sql.Date(cDeliveryDate.getTimeInMillis());
    java.sql.Date dDepartureDate = new java.sql.Date(cDepartureDate.getTimeInMillis());

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ORIGINATION_DESC", vendorLocation);
    inputParams.put("IN_INJECTION_HUB_ID", hubId);
    inputParams.put("IN_DELIVERY_DATE", dDeliveryDate);
    inputParams.put("IN_DEPARTURE_DATE", dDepartureDate);
    inputParams.put("IN_TRIP_STATUS_CODE", tripStatusCode);
    inputParams.put("IN_SORT_CODE_STATE_MASTER_ID", sortCodeStateMasterId);
    inputParams.put("IN_SORT_CODE_DATE_MMDD", sortCodeDateMMDD);
    inputParams.put("IN_TOTAL_PLT_QTY", totalPalletQty);
    inputParams.put("IN_FTD_PLT_QTY", ftdPalletQty);
    inputParams.put("IN_THIRD_PARTY_PLT_QTY", thirdPartyPalletQty);
    inputParams.put("IN_FTD_PALLET_QTY_USED", ftdPalletQtyUsed);
    inputParams.put("IN_CREATED_BY", csrId);
    inputParams.put("IN_UPDATED_BY", csrId);
    inputParams.put("IN_DAYS_IN_TRANSIT_QTY", daysInTransitQty);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("INSERT_ZJ_TRIP_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    BigDecimal bdTripId = (BigDecimal) outputs.get("OUT_TRIP_ID");
    String tripId = bdTripId.toString();
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    return tripId;

  }


  /**
   * method to update a trip
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void updateTrip(String tripId, String vendorLocation, String hubId, Calendar cDeliveryDate, Calendar cDepartureDate, 
                           String tripStatusCode, String sortCodeStateMasterId, String sortCodeDateMMDD, 
                           String totalPalletQty, String ftdPalletQty, String thirdPartyPalletQty, 
                           String ftdPalletQtyUsed, String csrId, String daysInTransitQty)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    java.sql.Date dDeliveryDate = new java.sql.Date(cDeliveryDate.getTimeInMillis());
    java.sql.Date dDepartureDate = new java.sql.Date(cDepartureDate.getTimeInMillis());

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ID", tripId);
    inputParams.put("IN_TRIP_ORIGINATION_DESC", vendorLocation);
    inputParams.put("IN_INJECTION_HUB_ID", hubId);
    inputParams.put("IN_DELIVERY_DATE", dDeliveryDate);
    inputParams.put("IN_DEPARTURE_DATE", dDepartureDate);
    inputParams.put("IN_TRIP_STATUS_CODE", tripStatusCode);
    inputParams.put("IN_SORT_CODE_STATE_MASTER_ID", sortCodeStateMasterId);
    inputParams.put("IN_SORT_CODE_DATE_MMDD", sortCodeDateMMDD);
    inputParams.put("IN_TOTAL_PLT_QTY", totalPalletQty);
    inputParams.put("IN_FTD_PLT_QTY", ftdPalletQty);
    inputParams.put("IN_THIRD_PARTY_PLT_QTY", thirdPartyPalletQty);
    inputParams.put("IN_FTD_PALLET_QTY_USED", ftdPalletQtyUsed);
    inputParams.put("IN_UPDATED_BY", csrId);
    inputParams.put("IN_DAYS_IN_TRANSIT_QTY", daysInTransitQty);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("UPDATE_ZJ_TRIP_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to cancel a trip
   *
   * @return void
   * @throws java.lang.Exception
   */
  public CachedResultSet cancelTrip(String tripId, String csrId)
    throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ID", tripId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("CANCEL_ZJ_TRIP_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) searchResults.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) searchResults.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    CachedResultSet crs = (CachedResultSet) searchResults.get("OUT_CANCEL_CUR");
    
    return crs;
    
  }


  /**
   * method to insert a vendor
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void insertTripVendor(String tripId, String vendorId, String tripVendorStatusCode, String orderCutoffTime,
                           String pltCubicInchAllowedQty, String fullPltQty, String fullPltOrderQty, 
                           String prtlPltInUseFlag, String prtlPltOrderQty, String prtlPltCubicInUsedQty, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ID", tripId);
    inputParams.put("IN_VENDOR_ID", vendorId);
    inputParams.put("IN_TRIP_VENDOR_STATUS_CODE", tripVendorStatusCode);
    inputParams.put("IN_ORDER_CUTOFF_TIME", orderCutoffTime);
    inputParams.put("IN_PLT_CUBIC_INCH_ALLOWED_QTY", pltCubicInchAllowedQty);
    inputParams.put("IN_FULL_PLT_QTY", fullPltQty);
    inputParams.put("IN_FULL_PLT_ORDER_QTY", fullPltOrderQty);
    inputParams.put("IN_PRTL_PLT_IN_USE_FLAG", prtlPltInUseFlag);
    inputParams.put("IN_PRTL_PLT_ORDER_QTY", prtlPltOrderQty);
    inputParams.put("IN_PRTL_PLT_CUBIC_IN_USED_QTY", prtlPltCubicInUsedQty);
    inputParams.put("IN_CREATED_BY", csrId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("INSERT_ZJ_TRIP_VENDOR_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    
  }


  /**
   * method to update a vendor
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void updateTripVendor(String tripId, String vendorId, String tripVendorStatusCode, String orderCutoffTime,
                           String pltCubicInchAllowedQty, String fullPltQty, String fullPltOrderQty, 
                           String prtlPltInUseFlag, String prtlPltOrderQty, String prtlPltCubicInUsedQty, String csrId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ID", tripId);
    inputParams.put("IN_VENDOR_ID", vendorId);
    inputParams.put("IN_TRIP_VENDOR_STATUS_CODE", tripVendorStatusCode);
    inputParams.put("IN_ORDER_CUTOFF_TIME", orderCutoffTime);
    inputParams.put("IN_PLT_CUBIC_INCH_ALLOWED_QTY", pltCubicInchAllowedQty);
    inputParams.put("IN_FULL_PLT_QTY", fullPltQty);
    inputParams.put("IN_FULL_PLT_ORDER_QTY", fullPltOrderQty);
    inputParams.put("IN_PRTL_PLT_IN_USE_FLAG", prtlPltInUseFlag);
    inputParams.put("IN_PRTL_PLT_ORDER_QTY", prtlPltOrderQty);
    inputParams.put("IN_PRTL_PLT_CUBIC_IN_USED_QTY", prtlPltCubicInUsedQty);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("UPDATE_ZJ_TRIP_VENDOR_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    
  }


  /**
   * method to cancel a vendor
   *
   * @return void
   * @throws java.lang.Exception
   */
  public CachedResultSet cancelTripVendor(String tripId, String vendorId, String csrId)
    throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_TRIP_ID", tripId);
    inputParams.put("IN_VENDOR_ID", vendorId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("CANCEL_ZJ_TRIP_VENDOR_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) searchResults.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) searchResults.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    CachedResultSet crs = (CachedResultSet) searchResults.get("OUT_CANCEL_CUR");
    
    return crs;
    
  }


  /**
   * Method to retrieve the statues for vendors on any given trip
   *
   * @input  
   * @return XMLDocument containing the values from the cursor
   *
   * @throws java.lang.Exception
   */
  public Document getTripVendorStatus()
    throws Exception
  {
    Document searchResults = DOMUtil.getDefaultDocument();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_TRIP_VENDOR_STATUS_XML");

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (Document) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }




}
