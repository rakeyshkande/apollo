package com.ftd.op.maintenance.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.op.maintenance.vo.InventorySearchCriteriaVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.op.maintenance.vo.VendorProductVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * InventoryTrackingDAO
 *
 *
 * @author Ali Lakhani / Mike Kruger
 */
public class InventoryTrackingDAO
{
  private Connection conn;
  private Logger logger;
  private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.InventoryTrackingDAO";

  /**
     * Constructor
     *
     * @param conn - database connection
     * @return n/a
     * @throws
     */
  public InventoryTrackingDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger(LOGGER_CATEGORY);
  }

   /**
   * @return XMLDocument containing the values from the cursor
   *
   * @throws java.lang.Exception
   */
  public HashMap searchInvTrk(String sInvTrkId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
  
    inputParams.put("IN_INV_TRK_ID", sInvTrkId);
    dataRequest.setInputParams(inputParams);

    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("SEARCH_INV_TRK_CRS");
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);
    return results; 

  }


  /**
   * Modifies default vendors associated with an email address and modifying 
   * which inventory tracking records the email address is associated with.
   * 
   * @param emailAddress          Email address to be associated with vendors
   * @param vendorIdList          List of vendor ids to be associated with the email address
   * @param applyToCurrentFlag    If 'Y' apply to default associations and current inventory tracking records otherwise only apply to default associations
   * @param updatedBy             The user who performed the action
   * @throws Exception
   */
  public void saveInvTrkEmail(String emailAddress, String vendorIdList, String applyToCurrentFlag, String updatedBy)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_EMAIL_ADDRESS", emailAddress);
    inputParams.put("IN_VENDOR_ID_LIST", vendorIdList);
    inputParams.put("IN_APPLY_TO_CURRENT_FLAG", applyToCurrentFlag);
    inputParams.put("IN_UPDATED_BY", updatedBy);
    
    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("SAVE_INV_TRK_EMAIL");
    dataRequest.setConnection(this.conn);
  
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
  
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

  /**
   * Obtains default vendors associated with an email address and all 
   * active vendors that are not associated with the email address.
   * 
   * @param emailAddress          Email address to search on
   * @return                      HashMap containing default vendors, vendor_chosen, and available vendors, vendor_available  
   * @throws Exception
   */
  public HashMap getVendorByInvTrkEmail(String emailAddress)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_EMAIL_ADDRESS", emailAddress);
    
    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("GET_VENDOR_BY_INV_TRK_EMAIL");
    dataRequest.setConnection(this.conn);
  
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
  
    HashMap resultMap = (HashMap) dataAccessUtil.execute(dataRequest); 
    
    HashMap results = new HashMap();
    ArrayList vendorsAvailable = new ArrayList();
    ArrayList vendorsChosen = new ArrayList();
    VendorMasterVO vendorVO = null;
    CachedResultSet crs = null;
    
    crs = (CachedResultSet)resultMap.get("OUT_VENDOR_AVAIL_CUR");
    while(crs.next())
    {
      vendorVO = new VendorMasterVO();
      vendorVO.setVendorID(crs.getString("vendor_id"));
      vendorVO.setVendorName(DOMUtil.encodeChars(crs.getString("vendor_name")));
      vendorsAvailable.add(vendorVO);      
    }
    results.put("vendor_available", vendorsAvailable);
    
    crs = (CachedResultSet)resultMap.get("OUT_VENDOR_CHOSEN_CUR");
    while(crs.next())
    {
      vendorVO = new VendorMasterVO();
      vendorVO.setVendorID(crs.getString("vendor_id"));
      vendorVO.setVendorName(DOMUtil.encodeChars(crs.getString("vendor_name")));
      vendorsChosen.add(vendorVO);      
    }
    results.put("vendor_chosen", vendorsChosen);

    return results;
  }

  /**
   * Deletes all inventory tracking data associated with the email address 
   * and sets the email address to inactive.
   * 
   * @param emailAddress          Email address to be disassociated
   * @param updatedBy             The user who performed the action
   * @throws Exception
   */
  public void deleteInvTrkEmail(String emailAddress, String updatedBy)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_EMAIL_ADDRESS", emailAddress);
    inputParams.put("IN_UPDATED_BY", updatedBy);
    
    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("DELETE_INV_TRK_EMAIL");
    dataRequest.setConnection(this.conn);
  
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
  
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

  /**
   *
   * @throws Exception
   */
  public String createInventoryInfo(String productId, String vendorId, Calendar cStartDate, Calendar cEndDate, 
                                    String forecast, String warningThreshold, String shutdownThreshold, 
                                    String comments, String csrId, HashMap pageData)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();

    java.sql.Date dStartDate = null;
    if (cStartDate != null)
      dStartDate = new java.sql.Date(cStartDate.getTimeInMillis());

    java.sql.Date dEndDate = null;
    if (cEndDate != null)
      dEndDate = new java.sql.Date(cEndDate.getTimeInMillis());

    inputParams.put("IN_PRODUCT_ID", productId);
    inputParams.put("IN_VENDOR_ID", vendorId);
    inputParams.put("IN_START_DATE", dStartDate);
    inputParams.put("IN_END_DATE", dEndDate);
    inputParams.put("IN_FORECAST_QTY", forecast);
    inputParams.put("IN_WARNING_THRESHOLD_QTY", warningThreshold);
    inputParams.put("IN_SHUTDOWN_THRESHOLD_QTY", shutdownThreshold);
    inputParams.put("IN_COMMENTS", comments);
    inputParams.put("IN_CREATED_BY", csrId);

    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("CREATE_INVENTORY_CRS");
    dataRequest.setConnection(this.conn);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    String invTrkId = (String) outputs.get("OUT_INV_TRK_ID");
    if (status != null && status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      logger.error("Original Error message : " + message); 
      BigDecimal errorCode = (BigDecimal)outputs.get("OUT_ERR_CODE");
      if(errorCode != null && errorCode.intValue() == 1) {
    	  message = "Create inventory request cannot be processed succesfully.";
    	  logger.error("Error message presented to user: " + message);
      }
      pageData.put("errorMessage", message);
      invTrkId = null; 
    }

    return invTrkId;

  }


  /**
   *
   * @throws Exception
   */
  public void updateInventoryInfo(String invTrkId, Calendar cStartDate, Calendar cEndDate, String revised, 
                                  String warningThreshold, String shutdownThreshold, 
                                  String comments, List invTrkEmailIdsToDeleteList, String newEmailAddress, String csrId, HashMap pageData)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();

    String sInvTrkEmailIdsToDelete = "";
    //create a product string
    for (int x = 0; x < invTrkEmailIdsToDeleteList.size(); x++)
    {
        sInvTrkEmailIdsToDelete += "'" + invTrkEmailIdsToDeleteList.get(x) + "'" + ",";
    }
    //if product string created, remove the last ,
    if (sInvTrkEmailIdsToDelete.length() > 0)
      sInvTrkEmailIdsToDelete = sInvTrkEmailIdsToDelete.substring(0, sInvTrkEmailIdsToDelete.length() - 1);
    //else initialize to null 
    else
      sInvTrkEmailIdsToDelete = null;

    java.sql.Date dStartDate = null;
    if (cStartDate != null)
      dStartDate = new java.sql.Date(cStartDate.getTimeInMillis());

    java.sql.Date dEndDate = null;
    if (cEndDate != null)
      dEndDate = new java.sql.Date(cEndDate.getTimeInMillis());

    inputParams.put("IN_INV_TRK_ID", invTrkId);
    inputParams.put("IN_START_DATE", dStartDate);
    inputParams.put("IN_END_DATE", dEndDate);
    inputParams.put("IN_REVISED_FORECAST", revised);
    inputParams.put("IN_WARNING_THRESHOLD", warningThreshold);
    inputParams.put("IN_SHUTDOWN_THRESHOLD", shutdownThreshold);
    inputParams.put("IN_COMMENTS", comments);
    inputParams.put("IN_EMAIL_IDS_TO_DELETE", sInvTrkEmailIdsToDelete);
    inputParams.put("IN_NEW_EMAIL_ADDRESS", newEmailAddress);
    inputParams.put("IN_UPDATED_BY", csrId);

    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("UPDATE_INVENTORY_CRS");
    dataRequest.setConnection(this.conn);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      logger.error("Original Error message : " + message);
      BigDecimal errorCode = (BigDecimal)outputs.get("OUT_ERR_CODE");
      if(errorCode.intValue() == 1) {
    	  message = "Update inventory request cannot be processed succesfully.";
    	  logger.error("Error message presented to user: " + message);
      }
      
      pageData.put("errorMessage", message);
    }
  }

  /**
   *
   * @throws Exception
   */
  public String deleteInventoryInfo(String invTrkId)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();

    inputParams.put("IN_INV_TRK_ID", invTrkId);

    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("DELETE_INVENTORY_CRS");
    dataRequest.setConnection(this.conn);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    String message = null;
    if (status != null && status.equals("N"))
    {
      message = (String) outputs.get("OUT_MESSAGE");
    }
    
    return message; 
    
  }

  /**
   */
  public List getProductsByInvTrkIds(List invTrkIds)
    throws Exception
  {
	logger.debug("getProductsByInvTrkIds()");
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
    List results = new ArrayList(); 
    
    String sInvTrkIds = "";
    //create a product string
    for (int x = 0; x < invTrkIds.size(); x++)
    {
        sInvTrkIds += invTrkIds.get(x) + ",";
    }
    //if product string created, remove the last ,
    if (sInvTrkIds.length() > 0)
      sInvTrkIds = sInvTrkIds.substring(0, sInvTrkIds.length() - 1);
    //else initialize to null 
    else
      sInvTrkIds = null;

    inputParams.put("IN_INV_TRK_IDS", sInvTrkIds);
    
    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("RETRIEVE_PRODUCT_IDS_CRS");
    dataRequest.setConnection(this.conn);
  
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    while(crs.next())
    {
      results.add((String)crs.getString("product_id"));
    }
    
    return results;
  }


	/**
	 * @param invTrkIds
	 * @return
	 * @throws Exception
	 */
	public List<VendorProductVO> getProductsVendorsByInvTrkIds(List invTrkIds) throws Exception {
		logger.debug("getProductsVendorsByInvTrkIds()");
		DataRequest dataRequest = new DataRequest();
		HashMap<Object, Object> inputParams = new HashMap<Object, Object>();
		List<VendorProductVO> results = new ArrayList<VendorProductVO>();
		VendorProductVO vpVO;

	    String sInvTrkIds = "";
	    //create a product string
	    for (int x = 0; x < invTrkIds.size(); x++)
	    {
	        sInvTrkIds += invTrkIds.get(x) + ",";
	    }
	    //if product string created, remove the last ,
	    if (sInvTrkIds.length() > 0)
	      sInvTrkIds = sInvTrkIds.substring(0, sInvTrkIds.length() - 1);
	    //else initialize to null 
	    else
	      sInvTrkIds = null;

		inputParams.put("IN_INV_TRK_IDS", sInvTrkIds);
		
		dataRequest.setInputParams(inputParams);
		dataRequest.setStatementID("RETRIEVE_PRODUCT_VENDOR_CRS");
		dataRequest.setConnection(this.conn);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

		while (crs.next()) {
			vpVO = new VendorProductVO();
			vpVO.setProductSubcodeId(crs.getString("product_id"));
			vpVO.setVendorId(crs.getString("vendor_id"));
			vpVO.setInvTrkId(crs.getString("inv_trk_id"));
			vpVO.setInvStatus(crs.getString("inv_status"));
			vpVO.setAvailable(crs.getString("vp_available"));
			vpVO.setRemoved(crs.getString("removed"));
			vpVO.setProdType(crs.getString("product_type"));
			results.add(vpVO);
		}

		return results;
	}

  /**
   *
   * @throws Exception
   */
  public String updateShutdownThreshold(String productId, String vendorId, String updatedBy, List invTrkIds)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    HashMap inputParams = new HashMap();
    
    String sInvTrkIds = "";
    //create a product string
    for (int x = 0; x < invTrkIds.size(); x++)
    {
        sInvTrkIds += "'" + invTrkIds.get(x) + "'" + ",";
    }
    //if product string created, remove the last ,
    if (sInvTrkIds.length() > 0)
      sInvTrkIds = sInvTrkIds.substring(0, sInvTrkIds.length() - 1);
    //else initialize to null 
    else
      sInvTrkIds = null;


    inputParams.put("IN_INV_TRK_IDS", sInvTrkIds);
    inputParams.put("IN_PRODUCT_ID", productId);
    inputParams.put("IN_VENDOR_ID", vendorId);
    inputParams.put("IN_UPDATED_BY", updatedBy);

    dataRequest.setInputParams(inputParams);
    dataRequest.setStatementID("UPDATE_SHUTDOWN_THRESHOLD_CRS");
    dataRequest.setConnection(this.conn);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    String message = null;
    if (status != null && status.equals("N"))
    {
      message = (String) outputs.get("OUT_MESSAGE");
    }
    
    return message; 
    
  }


  /**
   * method for
   *
   *
   * @return XMLDocument containing the values from the cursor
   *
   * @throws java.lang.Exception
   */
  public HashMap getOrdersTaken(String productId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_ORDERS_TAKEN_INFO");
    dataRequest.addInputParam("IN_PRODUCT_ID", productId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    HashMap results = (HashMap)dataAccessUtil.execute(dataRequest);
    return results;
  }

	/********************************** 12863 - NEW SKU IT CHANGES *******************************************/
	public HashMap searchInventory(InventorySearchCriteriaVO searchVO) throws Exception {
		DataRequest dataRequest = new DataRequest();
		HashMap inputParams = new HashMap();
		HashMap results = null;
		
		if(searchVO == null) {
			throw new Exception("No Search criteria");		
		}

		inputParams.put("IN_PRODUCT_IDS", searchVO.getProductIdsStr());
		inputParams.put("IN_VENDOR_IDS", searchVO.getVendorIdsStr());		
		inputParams.put("IN_START_DATE", searchVO.getcStartDate());
		inputParams.put("IN_END_DATE", searchVO.getcEndDate());
		inputParams.put("IN_PRODUCT_STATUS", searchVO.getProductStatus());
		inputParams.put("IN_VENDOR_PRODUCT_STATUS", searchVO.getVendorProductStatus());
		inputParams.put("IN_ON_HAND", searchVO.getOnHand() == null ? null : Integer.parseInt(searchVO.getOnHand()));
		
		// new params
		inputParams.put("IN_START_REC", searchVO.getPageDetail().getStartRecord());
		inputParams.put("IN_END_REC", searchVO.getPageDetail().getEndRecord());
		inputParams.put("INOUT_TOT_REC_CNT", searchVO.getPageDetail().getTotalRecords());
		
		logger.info("searchInvTrk - fetching records from " + searchVO.getPageDetail().getStartRecord() + " to " + searchVO.getPageDetail().getEndRecord() + " and total records are " + searchVO.getPageDetail().getTotalPages());

		dataRequest.setInputParams(inputParams);

		dataRequest.setConnection(this.conn);
		dataRequest.setStatementID("SEARCH_INVENTORY");
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		results = (HashMap) dataAccessUtil.execute(dataRequest);			 
		return results;
		
	}

	/********************************** 12863 - NEW SKU IT CHANGES END HERE *******************************************/
}
