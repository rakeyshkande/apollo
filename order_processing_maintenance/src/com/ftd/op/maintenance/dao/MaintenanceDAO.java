package com.ftd.op.maintenance.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.op.maintenance.constants.DAOConstants;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.exception.DuplicateProductMappingException;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.ComponentSkuVO;
import com.ftd.op.maintenance.vo.CustomerHoldVO;
import com.ftd.op.maintenance.vo.DeliveryFeeVO;
import com.ftd.op.maintenance.vo.OrderVO;
import com.ftd.op.maintenance.vo.PricePayVendorVO;
import com.ftd.op.maintenance.vo.ProductSubCodeVO;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.op.maintenance.vo.SecondChoiceVO;
import com.ftd.op.maintenance.vo.ServiceFeeOverrideVO;
import com.ftd.op.maintenance.vo.ServiceFeeVO;
import com.ftd.op.maintenance.vo.VendorProductVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * MaintenanceDAO
 * 
 * This is the DAO that handles retrieving information for the maintenance screens.
 * 
 * @author Nicole Roberts
 */

public class MaintenanceDAO 
{
  private Connection conn;  
  private Logger logger;
  private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.MaintenanceDAO";
  
  /**
     * Constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
  public MaintenanceDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger(LOGGER_CATEGORY);
  }
  
   
  public ProductSubCodeVO getSubcode(String subcodeId) throws Exception
  {    
    DataRequest dataRequest = new DataRequest();
    ProductSubCodeVO productSubCodeVO = null;
    boolean isEmpty = true;
    CachedResultSet rs = null;
    
    try
    {
      logger.debug("getSubcodde (String subcodeId) :: " + subcodeId);
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("PRODUCT_ID", subcodeId);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_PRODUCT_SUBCODE");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (rs.next())
      {     
            productSubCodeVO = new ProductSubCodeVO();
            productSubCodeVO.setProductId(rs.getString(1));
            productSubCodeVO.setProductSubCodeId(rs.getString(2));
            productSubCodeVO.setSubCodeDescription(rs.getString(3));
            productSubCodeVO.setSubCodeHolidaySKU(rs.getString(5));
            productSubCodeVO.setSubCodeRefNumber(rs.getString(8));
            productSubCodeVO.setVendorSKU(rs.getString(10));
            productSubCodeVO.setDimWeight(rs.getString(11));
      }
    }
    catch (Exception e) 
    {            
        logger.error(e);
        throw (e);
    } 

      return productSubCodeVO;
  }
  
  
  /**
   * Calls the GET_OCCASIONS stored procedure that returns the all the occasions types.
   * 
   * @param none
   * @return XML Document
   */
  public Document getOccasionXML() throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("getOccasionXML() :: Document");
    }
      
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_OCCASION_LIST_XML");
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  
  /**
   * 
   * @param none
   * @return XML Document
   */
    public Document getStateList() 
        throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_STATES");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }


  /**
   * method to retrieve all the states.
   *
   * @return XMLDocument containing the values of all the states
   *
   * @throws java.lang.Exception
   */


  public CachedResultSet getStateCRS()
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_STATES_CRS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }



    /**
     * Returns an XMLDocument of all states.  The XML format is:
     *
     * <zipList>
     *   <zip num="{index}">
     *     {data for single zip: zipcode, city, state, countrycode}
     *   </zip>
     * </zipList>
     * 
     * @param zipcode
     * @param city
     * @param state
     * @return XMLDocument containing all zipcodes
     * @throws java.lang.Exception
     */
    public Document getZipcodes(String zipcode, String city, String state) 
        throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_ZIP_CODE_LIST");
        dataRequest.addInputParam("zip_code", zipcode);
        dataRequest.addInputParam("city", city);
        dataRequest.addInputParam("state", state);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }


  /**
   * 
   * @param none
   * @return XML Document
   */
    public Document getCarrierInfo() 
        throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_CARRIER_INFO_LIST");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }
  
    public Document getCompanyCodeList() 
        throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_COMPANY_MASTER_LIST");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }  
  
  
  /**
   * Calls the hold reasons stored procedure that returns the all the occasions types.
   * 
   * @param none
   * @return XML Document
   */
  public Document getHoldReasonsXML() throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();
    

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_CUST_HOLD_REASON_VAL_LIST");
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  
  public Document getCustomerHoldInfo(String customerId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
  
    Document xml = DOMUtil.getDocument();
  
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_ID", customerId);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_CUSTOMER_HOLD_CUST_INFO");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    HashMap cursors = (HashMap) dataAccessUtil.execute(dataRequest);      
    
    Document custDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_CUSTOMER","customers","customer");
    Document creditDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_CREDIT_CARDS","credit_cards","credit_card");
    Document phoneDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_PHONES","phonenumbers","phonenumber");
    Document memberDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_MEMBERSHIPS","memberships","membership");
    Document emailDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_EMAILS","emails","email");
    
    DOMUtil.addSection(xml,custDoc.getChildNodes());
    DOMUtil.addSection(xml,creditDoc.getChildNodes());
    DOMUtil.addSection(xml,phoneDoc.getChildNodes());
    DOMUtil.addSection(xml,memberDoc.getChildNodes());
    DOMUtil.addSection(xml,emailDoc.getChildNodes());    
    
    return xml;


  }
  
  public Document getCustomerHoldInfo(String orderDetailId, String customerType) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
  
    Document xml = DOMUtil.getDocument();
  
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
    inputParams.put("IN_CUTOMER_TYPE", customerType);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_CUSTOMER_HOLD_ORDER_INFO");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    HashMap cursors = (HashMap) dataAccessUtil.execute(dataRequest);      
    
    Document custDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_CUSTOMER","customers","customer");
    Document creditDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_CREDIT_CARDS","credit_cards","credit_card");
    Document phoneDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_PHONES","phonenumbers","phonenumber");
    Document memberDoc =CommonUtil.buildXML(cursors,"OUT_CURSOR_MEMBERSHIPS","memberships","membership");
    Document emailDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_EMAILS","emails","email");
    Document lossPrevDoc = CommonUtil.buildXML(cursors,"OUT_CURSOR_LOSS_PREVENTION","lps","lp");
    
    
    DOMUtil.addSection(xml,custDoc.getChildNodes());
    DOMUtil.addSection(xml,creditDoc.getChildNodes());
    DOMUtil.addSection(xml,phoneDoc.getChildNodes());
    DOMUtil.addSection(xml,memberDoc.getChildNodes());
    DOMUtil.addSection(xml,emailDoc.getChildNodes());    
    DOMUtil.addSection(xml,lossPrevDoc.getChildNodes());    
    
    return xml;


  }  
  
  
  
  /**
   * Calls the GET_OCCASION stored procedure.  If it returns data return true, 
   * else return false.
   * 
   * @param String - occasionID
   * @return boolean
   */
  public boolean occasionExists(String occasionID) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("occasionExists(String occasionID) :: boolean");
    }
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_OCCASION_ID", occasionID);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_OCCASION");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    if(outputs.next())
    {
        return true;
    }
    
    return false;
  }
  
  
  
  public List getHeldOrders(String customerId) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();    
      
    List orderList = new ArrayList();
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_ID", customerId);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_HELD_ORDERS");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (outputs.next())
    {
      orderList.add(outputs.getString("order_detail_id"));
    }
    
    return orderList;
  }
  
  
  
    public String getDNIS(String dnisId) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();    
      
    String desc = "";
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_DNIS_ID", dnisId);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_DNIS_BY_ID");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    if (outputs.next())
    {      
      desc = outputs.getString("DESCRIPTION");
    }
    
    return desc;
  }
  
  /**
   * This method is a wrapper for the SP_GET_ORDER SP.  
   * It populates a Order VO based on the returned record set.
   * 
   * @param String - orderGuid
   * @return OrderVO 
   */
  public OrderVO getOrder(String orderGuid) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    OrderVO order = new OrderVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    
    try
    {
      logger.debug("getOrder (String orderGuid) :: OrderVO");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_GUID", orderGuid);

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("SP_GET_ORDER_BY_GUID");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        order.setMasterOrderNumber(outputs.getString("MASTER_ORDER_NUMBER"));     
        order.setCustomerId(outputs.getLong("CUSTOMER_ID"));
        order.setMembershipId(outputs.getLong("MEMBERSHIP_ID"));
        order.setCompanyId(outputs.getString("COMPANY_ID"));        
        order.setSourceCode(outputs.getString("SOURCE_CODE"));
        order.setOriginId(outputs.getString("ORIGIN_ID"));
        order.setOrderDate(outputs.getDate("ORDER_DATE"));
        order.setOrderTotal(outputs.getDouble("ORDER_TOTAL"));
        order.setProductTotal(outputs.getDouble("PRODUCT_TOTAL"));
        order.setAddOnTotal(outputs.getDouble("ADD_ON_TOTAL"));
        order.setServiceFeeTotal(outputs.getDouble("SERVICE_FEE_TOTAL"));
        order.setShippingFeeTotal(outputs.getDouble("SHIPPING_FEE_TOTAL"));
        order.setDiscountTotal(outputs.getDouble("DISCOUNT_TOTAL"));
        order.setTaxTotal(outputs.getDouble("TAX_TOTAL"));
        order.setLossPreventionIndicator(outputs.getString("LOSS_PREVENTION_INDICATOR"));
      }
    }
    catch (Exception e) 
    {            
      logger.error(e);
      throw e;
    } 
    if (isEmpty)
      return null;
    else 
      return order;
  }
  
  public void updateOrderDisposition(long orderDetailId, String newDisposition, String updatedBy) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;   
       
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
      inputParams.put("IN_DISPOSITION", newDisposition);
      inputParams.put("IN_UPDATED_BY", updatedBy);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("UPDATE_ORDER_DISPOSITION");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS_PARAM");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE_PARAM");
        throw new Exception(message);
      }
    }
  
  
  
  
  public List getHeldOrders(String customerId,String orderGuid) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();    
      
    List orderList = new ArrayList();
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_ID", customerId);
    inputParams.put("IN_ORDER_GUID", orderGuid);    

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_HELD_ORDERS_BY_GUID");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (outputs.next())
    {
      orderList.add(outputs.getString("order_detail_id"));
    }
    
    return orderList;
  }  
  
  /**
   * Calls the INSERT_OCCASION stored procedure.
   * 
   * @param String - occasionID
   * @param String - description
   * @return none
   */
  public void insertOccasion(String occasionID, String description, String displayOrder, String active) throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("insertOccasion(String occasionID, String description, String displayOrder, String active) :: void");
    }
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_OCCASION_ID", occasionID);
    inputParams.put("IN_DESCRIPTION", description);
    inputParams.put("IN_DISPLAY_ORDER", displayOrder);
    inputParams.put("IN_ACTIVE", active);
    inputParams.put("IN_USER_EDITABLE", "Y");

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_OCCASION");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }
  
  /**
   * Calls the UPDATE_OCCASION stored procedure.
   * 
   * @param String - occasionID
   * @param String - description
   * @return none
   */
  public void updateOccasion(String occasionID, String description, String displayOrder, String active) throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {    
    DataRequest dataRequest = new DataRequest();
     
    if(logger.isDebugEnabled())
    {
      logger.debug("updateOccasion(String occasionID, String description, String displayOrder, String active) :: void");
    }
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_OCCASION_ID", occasionID);
    inputParams.put("IN_DESCRIPTION", description);
    inputParams.put("IN_DISPLAY_ORDER", displayOrder);
    inputParams.put("IN_ACTIVE", active);
    
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_OCCASION");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }
  
  /**
   * Calls the GET_SECOND_CHOICES stored procedure.
   * 
   * @param none
   * @return XML Document
   */
  public Document getSecondChoiceXML() throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("getSecondChoiceXML () :: Document");
    }
      
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_ALL_PROD_SECOND_CHOICES");
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  /**
   * Calls the INSERT_SECOND_CHOICE stored procedure.
   * 
   * @param String - secondChoiceID
   * @param String - oeDescription
   * @param String - mercuryDescription
   * @param String - oeHolidayDescription
   * @param String - mercuryHolidayDescription
   * @return none
   */
  public void insertSecondChoice(SecondChoiceVO secondChoice) throws Exception
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("insertSecondChoice(String secondChoiceID, String oe_description, String mercury_description, String oe_holiday_description, String mercury_holiday_description) :: void");
    }
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_PRODUCT_SECOND_CHOICE_ID", secondChoice.getProductSecondChoice());
    inputParams.put("IN_OE_DESCRIPTION", secondChoice.getOeDescription());
    inputParams.put("IN_MERCURY_DESCRIPTION", secondChoice.getMercuryDescription());
    inputParams.put("IN_OE_HOLIDAY_DESCRIPTION", secondChoice.getOeHolidayDescription());
    inputParams.put("IN_OE_MERCURY_DESCRIPTION", secondChoice.getMercuryHolidayDescription());

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_PRODUCT_SECOND_CHOICE");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }
  
  /**
   * Calls the UDPATE_SECOND_CHOICE stored procedure.
   * 
   * @param String - secondChoiceID
   * @param String - oeDescription
   * @param String - mercuryDescription
   * @param String - oeHolidayDescription
   * @param String - mercuryHolidayDescription
   * @return none
   */
  public void updateSecondChoice(SecondChoiceVO secondChoice) throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("updateSecondChoice(String secondChoiceID, String oe_description, String mercury_description, String oe_holiday_description, String mercury_holiday_description) :: void");
    }
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_SECOND_CHOICE_ID", secondChoice.getProductSecondChoice());
    inputParams.put("IN_DESCRIPTION", secondChoice.getOeDescription());
    inputParams.put("IN_MERCURY_DESCRIPTION", secondChoice.getMercuryDescription());
    inputParams.put("IN_OE_HOLIDAY_DESCRIPTION", secondChoice.getOeHolidayDescription());
    inputParams.put("IN_MERCURY_HOLIDAY_DESCRIPTION", secondChoice.getMercuryHolidayDescription());

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_PRODUCT_SECOND_CHOICE");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }
  
  /**
   * Calls the GET_SECOND_CHOICE  stored procedure.  If it returns data return true, 
   * else return false.
   * 
   * @param String - secondChoiceID
   * @return boolean
   */
  public boolean secondChoiceExists(String secondChoiceID) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
    DataRequest dataRequest = new DataRequest();
    
    if(logger.isDebugEnabled())
    {
      logger.debug("secondChoiceExists(String secondChoiceID) :: boolean");
    }
      
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_SECOND_CHOICE_ID", secondChoiceID);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_PRODUCT_SECOND_CHOICE");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    ResultSet outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
    while (outputs.next())
    {
      return true;
    }
    
    return false;
  }
  
  /**
     Insert a customer hold record into the database
     * 
     * @param holdVO
     * @throws java.lang.Exception
     */
  public void insertCustomerHold(CustomerHoldVO holdVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();    
     
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ENTITY_TYPE", holdVO.getEntityType());
    inputParams.put("IN_HOLD_VALUE_1", holdVO.getHoldValue1());
    inputParams.put("IN_HOLD_VALUE_2", holdVO.getHoldValue2());
    inputParams.put("IN_GLOBAL_ADD_ON", holdVO.getGlobalAddonOn());
    inputParams.put("IN_HOLD_REASON_CODE", holdVO.getHoldReasonCode());

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_CUSTOMER_HOLD_ENTITIES");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
      
  }
  
  
  /**
     Delete a customer hold record into the database
     * 
     * @param holdVO
     * @throws java.lang.Exception
     */
  public void deleteCustomerHold(CustomerHoldVO holdVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();    
     
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_HOLD_ENTITY_ID", holdVO.getEntityId());

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("DELETE_CUSTOMER_HOLD_ENTITIES");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
    }
      
  }  
  
  /**
     Update a customer hold record into the database
     * 
     * @param holdVO
     * @throws java.lang.Exception
     */
  public void updateCustomerHold(CustomerHoldVO holdVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();    
     
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_HOLD_ENTITY_ID", holdVO.getEntityId());
    inputParams.put("IN_HOLD_REASON_CODE", holdVO.getHoldReasonCode());    

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_CUSTOMER_HOLD_ENTITIES");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
      
  }    
  
  
  public void deleteFromQueue(String orderDetailId, String queueType, String csr) throws Exception
  {
    DataRequest dataRequest = new DataRequest();    
     
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
    inputParams.put("IN_QUEUE_TYPE", queueType);    
    inputParams.put("IN_CSR", csr);    

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("DELETE_FROM_QUEUE");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
      
  }      
  
  
  /**
   * This method is a wrapper for the SP_GET_PRODUCT SP.  
   * It populates a product VO based on the returned record set.
   * 
   * @param String - productId
   * @return ProductVO 
   */
   public ProductVO getProduct(String productId) throws Exception
  {    
    DataRequest dataRequest = new DataRequest();
    ProductVO product = new ProductVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    
    try
    {
      logger.debug("getProduct (String productId) :: ProductVO");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("PRODUCT_ID", productId);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("SP_GET_PRODUCT_BY_ID_RESULTSET");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        product.setProductId(outputs.getString(DAOConstants.PRODUCT_ID_LC));     
        product.setNovatorId(outputs.getString(DAOConstants.NOVATOR_ID_LC));     
        product.setProductName(outputs.getString(DAOConstants.PRODUCT_NAME_LC));
        product.setNovatorName(outputs.getString(DAOConstants.NOVATOR_NAME_LC));
        product.setStatus(outputs.getString(DAOConstants.STATUS_LC));     
        product.setDeliveryType(outputs.getString(DAOConstants.DELIVERY_TYPE_LC));
        product.setCategory(outputs.getString(DAOConstants.CATEGORY_LC));
        product.setProductType(outputs.getString(DAOConstants.PRODUCT_TYPE_LC));     
        product.setProductSubType(outputs.getString(DAOConstants.PRODUCT_SUB_TYPE_LC));
        product.setColorSizeFlag(outputs.getString(DAOConstants.COLOR_SIZE_FLAG_LC));
        product.setStandardPrice(outputs.getDouble(DAOConstants.STANDARD_PRICE_LC));
        product.setDeluxePrice(outputs.getDouble(DAOConstants.DELUXE_PRICE_LC));
        product.setPremiumPrice(outputs.getDouble(DAOConstants.PREMUIM_PRICE_LC));
        product.setPreferredPricePoint(outputs.getDouble(DAOConstants.PREFERRED_PRICE_POINT_LC));
        product.setVariablePriceMax(outputs.getDouble(DAOConstants.VARIABLE_PRICE_MAX_LC));
        product.setShortDescription(outputs.getString(DAOConstants.SHORT_DESCRIPTION_LC));
        product.setLongDescription(outputs.getString(DAOConstants.LONG_DESCRIPTION_LC));     
        product.setFloristReferenceNumber(outputs.getString(DAOConstants.FLORIST_REFERENCE_NUMBER_LC));
        product.setMercuryDescription(outputs.getString(DAOConstants.MERCURY_DESCRIPTION_LC));
        product.setItemComments(outputs.getString(DAOConstants.ITEM_COMMENTS_LC));     
        product.setAddOnBalloonsFlag(outputs.getString(DAOConstants.ADD_ON_BALLOONS_FLAG_LC));
        product.setAddOnBearsFlag(outputs.getString(DAOConstants.ADD_ON_BEARS_FLAG_LC));
        product.setAddOnCardsFlag(outputs.getString(DAOConstants.ADD_ON_CARDS_FLAG_LC));     
        product.setAddOnFuneralFlag(outputs.getString(DAOConstants.ADD_ON_FUNERAL_FLAG_LC));
        product.setCodifiedFlag(outputs.getString(DAOConstants.CODIFIED_FLAG_LC));
        product.setExceptionCode(outputs.getString(DAOConstants.EXECPTION_CODE_LC));     
        product.setExceptionStartDate(outputs.getDate(DAOConstants.EXCEPTION_START_DATE_LC));
        product.setExceptionEndDate(outputs.getDate(DAOConstants.EXECPTION_END_DATE_LC));
        product.setExceptionMessage(outputs.getString(DAOConstants.EXCEPTION_MESSAGE_LC));     
        product.setVendorId(outputs.getString(DAOConstants.VENDOR_ID_LC));
        product.setVendorCost(outputs.getDouble(DAOConstants.VENDOR_COST_LC));
        product.setVendorSku(outputs.getString(DAOConstants.VENDOR_SKU_LC));     
        product.setSecondChoiceCode(outputs.getString(DAOConstants.SECOND_CHOICE_CODE_LC));
        product.setHolidaySecondChoiceCode(outputs.getString(DAOConstants.HOLIDAY_SECOND_CHOICE_CODE_LC));
        product.setDropshipCode(outputs.getString(DAOConstants.DROPSHIP_CODE_LC));
        product.setDiscountAllowedFlag(outputs.getString(DAOConstants.DISCOUNT_ALLOWED_FLAG_LC));     
        product.setDeliveryIncludedFlag(outputs.getString(DAOConstants.DELIVERY_INCLUDED_FLAG_LC));
        product.setTaxFlag(outputs.getString(DAOConstants.TAX_FLAG_LC));
        product.setServiceFeeFlag(outputs.getString(DAOConstants.SERVICE_FEE_FLAG_LC));
        product.setExoticFlag(outputs.getString(DAOConstants.EXOTIC_FLAG_LC));     
        product.setEgiftFlag(outputs.getString(DAOConstants.EGIFT_FLAG_LC));
        product.setCountryId(outputs.getString(DAOConstants.COUNTRY_ID_LC));
        product.setArrangementSize(outputs.getString(DAOConstants.ARRANGEMENT_SIZE_LC));
        product.setArrangementColors(outputs.getString(DAOConstants.ARRANGEMENT_COLORS_LC));     
        product.setDominantFlowers(outputs.getString(DAOConstants.DOMINANT_FLOWERS_LC));
        product.setSearchPriority(outputs.getString(DAOConstants.SEARCH_PRIORITY_LC));
        product.setRecipe(outputs.getString(DAOConstants.RECIPE_LC));     
        product.setSubcodeFlag(outputs.getString(DAOConstants.SUBCODE_FLAG_LC));
        product.setDimWeight(outputs.getString(DAOConstants.DIM_WEIGHT_LC));
        product.setNextDayUpgradeFlag(outputs.getString(DAOConstants.NEXT_DAT_UPGRADE_FLAG_LC));     
        product.setCorporateSite(outputs.getString(DAOConstants.CORPORATE_SITE_LC));
        product.setUnspscCode(outputs.getString(DAOConstants.UNSPSC_CODE_LC));
        product.setPriceRank1(outputs.getString(DAOConstants.PRICE_RANK1_LC));     
        product.setPriceRank2(outputs.getString(DAOConstants.PRICE_RANK2_LC));
        product.setPriceRank3(outputs.getString(DAOConstants.PRICE_RANK3_LC));
        product.setShipMethodCarrier(outputs.getString(DAOConstants.SHIP_METHOD_CARRIER_LC));     
        product.setShipMethodFlorist(outputs.getString(DAOConstants.SHIP_METHOD_FLORIST_LC));
        product.setShippingKey(outputs.getString(DAOConstants.SHIPPING_KEY_LC));
        product.setVariablePriceFlag(outputs.getString(DAOConstants.VARIABLE_PRICE_FLAG_LC));
        product.setHolidaySku(outputs.getString(DAOConstants.HOLIDAY_SKU_LC));
        product.setHolidayPrice(outputs.getDouble(DAOConstants.HOLIDAY_PRICE_LC));
        product.setCatalogFlag(outputs.getString(DAOConstants.CATALOG_FLAG_LC));
        product.setHolidayDeluxePrice(outputs.getDouble(DAOConstants.HOLIDAY_DELUXE_PRICE_LC));
        product.setHolidayPremiumPrice(outputs.getDouble(DAOConstants.HOLIDAY_PREMIUM_PRICE_LC));
        product.setHolidayStartDate(outputs.getDate(DAOConstants.HOLIDAY_START_DATE_LC));
        product.setHolidayEndDate(outputs.getDate(DAOConstants.HOLIDAY_END_DATE_LC));
        product.setHoldUntilAvailable(outputs.getString(DAOConstants.HOLD_UNTIL_AVAILABLE_LC));
        product.setMondayDeliveryFreshcut(outputs.getString(DAOConstants.MONDAY_DELIVERY_FRESHCUT_LC));
        product.setTwoDaySatFreshcut(outputs.getString(DAOConstants.TWO_DAY_SAT_FRESHCUT_LC));
        product.setDefaultCarrier(outputs.getString(DAOConstants.DEFAULT_CARRIER_LC));
        product.setPersonalizationTemplateOrder(outputs.getString(DAOConstants.PERSONALIZATION_TEMPLATE_ORDER_LC));
        product.setUpdatedBy(outputs.getString(DAOConstants.UPDATED_BY));

      }
    }
    catch (Exception e) 
    {            
        logger.error(e);
    } 
    if (isEmpty)
      return null;
    else
      return product;
  }  
  
  
  public void insertComment(CommentsVO comment) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_COMMENTS");
        dataRequest.addInputParam("IN_CUSTOMER_ID", comment.getCustomerId());
        dataRequest.addInputParam("IN_ORDER_GUID", comment.getOrderGuid());
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", comment.getOrderDetailId());
        dataRequest.addInputParam("IN_COMMENT_ORIGIN", comment.getCommentOrigin());
        dataRequest.addInputParam("IN_REASON", comment.getReason());
        dataRequest.addInputParam("IN_DNIS_ID", comment.getDnisId());
        dataRequest.addInputParam("IN_COMMENT_TEXT", comment.getComment());
        dataRequest.addInputParam("IN_COMMENT_TYPE", comment.getCommentType());
        dataRequest.addInputParam("IN_CSR_ID", comment.getUpdatedBy());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        logger.debug("Insert Comments Successful. Status="+status);

  }  
  
  /**
   * Calls the UPDATE_SHIPPING_KEYS stored procedure.
   * 
   * @param shippingKeyID
   * @param shippingKeyDescription
   * @param shipper
   * @param trackingURL   
   * @param status
   * @param usagePercent
   * @param overrideStart
   * @param overrideEnd
   * @param overridePercent
   */
  public void updateVendorCarrierRef(final String vendorID, 
          final String carrierID,  final int usagePercent, 
          final Date overrideStart, final Date overrideEnd, 
          final Integer overridePercent) throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {
    final DataRequest dataRequest = new DataRequest();
     
    if(logger.isDebugEnabled())
    {
      logger.debug("updateVendorCarrierRef(String shippingKeyID, String shippingKeyDescription, String shipper, String trackingURL, String status, int usagePercent) :: void");
    }
      
    /* setup store procedure input parameters */
    final Map inputParams = new HashMap();
    inputParams.put("IN_VENDOR_ID", vendorID);
    inputParams.put("IN_CARRIER_ID", carrierID);
    inputParams.put("IN_USAGE_PERCENT", new Integer(usagePercent));
    inputParams.put("IN_OVERRIDE_START", overrideStart);
    inputParams.put("IN_OVERRIDE_END", overrideEnd);
    inputParams.put("IN_OVERRIDE_PERCENTAGE", overridePercent);
    
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_VENDOR_CARRIER_REF");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    final DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    final Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    final String outStatus = (String) outputs.get("OUT_STATUS");
    if(outStatus != null && outStatus.equalsIgnoreCase("N"))
    {
      final String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

  /**
   * Calls the VIEW_VENDOR_CARRIER_REF stored procedure.
   * 
   * @return XMLDocument - the contents of the VIEW_VENDOR_CARRIER_REF table
   */  
  public Document getVendorCarrierRef(final String vendorID) throws Exception {
    if(logger.isDebugEnabled())
    {
      logger.debug("getVendorCarrierRef(int vendorID) :: XMLDocument");
    }

    final Map inputParams = new HashMap();
    inputParams.put("IN_VENDOR_ID", vendorID);
        
    
    /* build DataRequest object */
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("VIEW_VENDOR_CARRIER_REF");
    dataRequest.setInputParams(inputParams);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  /**
   * Calls the VIEW_ACTIVE_VENDORS stored procedure.
   * 
   * @return XMLDocument - the contents of the VIEW_VENDOR_CARRIER_REF table
   */  
  public Document getActiveVendors() throws Exception {
    if(logger.isDebugEnabled())
    {
      logger.debug("getActiveVendors() :: XMLDocument");
    }

    /* build DataRequest object */
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("VIEW_ACTIVE_VENDORS");

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public Document getVendor(final String vendorId) throws Exception {
    if(logger.isDebugEnabled())
    {
      logger.debug("getVendor() :: XMLDocument");
    }

    /* build DataRequest object */
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("GET_VENDOR");

    final Map inputParams = new HashMap();
    inputParams.put("IN_VENDOR_ID", vendorId);
        
    dataRequest.setInputParams(inputParams);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    return (Document) dataAccessUtil.execute(dataRequest);
  }

  /**
  * Calls the GET_SNH_LIST stored procedure.
  * 
  * @param none
  * @return XML Document
  */
  public Document getServiceFeeList(String sortField) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
      DataRequest dataRequest = new DataRequest();
      
      if(logger.isDebugEnabled())
      {
        logger.debug("getServiceFeeList()");
      }
      
      Map inputParams = new HashMap();
      inputParams.put("IN_SORT_FIELD", sortField);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_SNH_LIST");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);
  }
  
 /**
 * Calls the GET_SERVICE_FEE_OVERRIDE_LIST stored procedure.
 * 
 * @param none
 * @return XML Document
 */
 public Document getServiceFeeOverrideList(String serviceFeeId) throws IOException, ParserConfigurationException, SAXException, SQLException
 {    
     DataRequest dataRequest = new DataRequest();
     
     if(logger.isDebugEnabled())
     {
       logger.debug("getServiceFeeOverrideList()");
     }
       
     /* build DataRequest object */
     dataRequest.setConnection(conn);
     dataRequest.setStatementID("GET_SERVICE_FEE_OVERRIDE_LIST");

     final Map inputParams = new HashMap();
     inputParams.put("IN_SERVICE_FEE_ID", serviceFeeId);
     dataRequest.setInputParams(inputParams);
     
     /* execute the store prodcedure */
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     return (Document) dataAccessUtil.execute(dataRequest);
 }

  /**
  * Calls the GET_SOURCE_BY_SERVICE_FEE stored procedure.
  * 
  * @param none
  * @return XML Document
  */
  public Document getSourceCodeList(String serviceFeeId) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
      DataRequest dataRequest = new DataRequest();
      
      if(logger.isDebugEnabled())
      {
          logger.debug("getSourceCodeList()");
      }
          
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_SOURCE_BY_SERVICE_FEE");

      final Map inputParams = new HashMap();
      inputParams.put("IN_SERVICE_FEE_ID", serviceFeeId);
      dataRequest.setInputParams(inputParams);
        
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);
  }

  /**
  * Calls the GET_SERVICE_FEE_HISTORY stored procedure.
  * 
  * @param none
  * @return XML Document
  */
  public Document getServiceFeeHistory(String serviceFeeId) throws IOException, ParserConfigurationException, SAXException, SQLException
  {    
      DataRequest dataRequest = new DataRequest();
        
      if(logger.isDebugEnabled())
      {
          logger.debug("getServiceFeeHistory()");
      }
          
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_SERVICE_FEE_HISTORY");

      final Map inputParams = new HashMap();
      inputParams.put("IN_SERVICE_FEE_ID", serviceFeeId);
      dataRequest.setInputParams(inputParams);
        
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (Document) dataAccessUtil.execute(dataRequest);
  }

  /**
  * Calls the UPDATE_INSERT_SERVICE_FEE stored procedure.
  * 
  * @param serviceFeeVO
  */
  public void updateServiceFee(ServiceFeeVO serviceFee) 
      throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {
      final DataRequest dataRequest = new DataRequest();
       
      if(logger.isDebugEnabled())
      {
        logger.debug("updateServiceFee()");
      }
        
      /* setup store procedure input parameters */
      final Map inputParams = new HashMap();
      inputParams.put("IN_SNH_ID", serviceFee.getServiceFeeId());
      inputParams.put("IN_DESCRIPTION", serviceFee.getDescription());
      inputParams.put("IN_FIRST_ORDER_DOMESTIC", serviceFee.getDomesticFee());
      inputParams.put("IN_SAME_DAY_UPCHARGE", serviceFee.getSameDayUpcharge());
      inputParams.put("IN_FIRST_ORDER_INTERNATIONAL", serviceFee.getInternationalFee());
      inputParams.put("IN_VENDOR_CHARGE", serviceFee.getVendorCharge());
      inputParams.put("IN_VENDOR_SAT_UPCHARGE", serviceFee.getVendorSatUpcharge());   
      inputParams.put("IN_VENDOR_SUN_UPCHARGE", serviceFee.getVendorSunUpcharge());      
      inputParams.put("IN_VENDOR_MON_UPCHARGE", serviceFee.getVendorMonUpcharge());      
      inputParams.put("IN_UPDATED_BY", serviceFee.getUpdatedBy());
      inputParams.put("IN_REQUESTED_BY", serviceFee.getRequestedBy());
      inputParams.put("IN_SAME_DAY_UPCHARGE_FS", serviceFee.getSameDayUpchargeFS());
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("UPDATE_SERVICE_FEE");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      final DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      final Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      final String outStatus = (String) outputs.get("OUT_STATUS");
      if(outStatus != null && outStatus.equalsIgnoreCase("N"))
      {
        final String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }
  }

 /**
 * Calls the DELETE_SERVICE_FEE stored procedure.
 * 
 * @param serviceFeeId
 */
 public void deleteServiceFee(String serviceFeeId, ServiceFeeVO serviceFee) 
     throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
 {
     final DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);

     if(logger.isDebugEnabled())
     {
       logger.debug("deleteServiceFee(): " + serviceFeeId);
     }
       
     /* setup store procedure input parameters */
     Map inputParams = new HashMap();
     inputParams.put("IN_SNH_ID", serviceFeeId);
     inputParams.put("IN_REQUESTED_BY", serviceFee.getRequestedBy());
     inputParams.put("IN_UPDATED_BY", serviceFee.getUpdatedBy());
     
     /* build DataRequest object */
     dataRequest.setStatementID("UPDATE_BEFORE_DELETE_SNH");
     dataRequest.setInputParams(inputParams);
     
     /* execute the store prodcedure */
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     Map outputs = (Map) dataAccessUtil.execute(dataRequest);
       
     /* read stored procedure output parameters to determine 
      * if the procedure executed successfully */
     String outStatus = (String) outputs.get("OUT_STATUS");
     if(outStatus != null && outStatus.equalsIgnoreCase("N"))
     {
       final String message = (String) outputs.get("OUT_MESSAGE");
       throw new Exception(message);
     }
      
     /* setup store procedure input parameters */
     inputParams = new HashMap();
     inputParams.put("IN_SNH_ID", serviceFeeId);
     
     /* build DataRequest object */
     dataRequest.setStatementID("DELETE_SERVICE_FEE");
     dataRequest.setInputParams(inputParams);
     
     /* execute the store prodcedure */
     dataAccessUtil = DataAccessUtil.getInstance();
     outputs = (Map) dataAccessUtil.execute(dataRequest);
       
     /* read stored procedure output parameters to determine 
      * if the procedure executed successfully */
     outStatus = (String) outputs.get("OUT_STATUS");
     if(outStatus != null && outStatus.equalsIgnoreCase("N"))
     {
       final String message = (String) outputs.get("OUT_MESSAGE");
       throw new Exception(message);
     }
 }

 /**
 * Calls the UPDATE_INSERT_SERVICE_FEE stored procedure.
 * 
 * @param serviceFeeOverrideVO
 */
 public String updateServiceFeeOverride(ServiceFeeOverrideVO serviceFee) 
     throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
 {
     final DataRequest dataRequest = new DataRequest();
     String result = null;
      
     if(logger.isDebugEnabled())
     {
       logger.debug("updateServiceFeeOverride()");
     }
       
     /* setup store procedure input parameters */
     final Map inputParams = new HashMap();
     inputParams.put("IN_SNH_ID", serviceFee.getServiceFeeId());
     inputParams.put("IN_OVERRIDE_DATE", new java.sql.Date(serviceFee.getOverrideDate().getTime()));
     inputParams.put("IN_FIRST_ORDER_DOMESTIC", serviceFee.getDomesticFee());
     inputParams.put("IN_SAME_DAY_UPCHARGE", serviceFee.getSameDayUpcharge());
     inputParams.put("IN_FIRST_ORDER_INTERNATIONAL", serviceFee.getInternationalFee());
     inputParams.put("IN_VENDOR_CHARGE", serviceFee.getVendorCharge());
     inputParams.put("IN_VENDOR_SAT_UPCHARGE", serviceFee.getVendorSatUpcharge());    
     inputParams.put("IN_VENDOR_SUN_UPCHARGE", serviceFee.getVendorSunUpcharge());  
     inputParams.put("IN_VENDOR_MON_UPCHARGE", serviceFee.getVendorMonUpcharge());  
     inputParams.put("IN_REQUESTED_BY", serviceFee.getRequestedBy());
     inputParams.put("IN_UPDATED_BY", serviceFee.getUpdatedBy());
     inputParams.put("IN_SAME_DAY_UPCHARGE_FS", serviceFee.getSameDayUpchargeFS());
     
     /* build DataRequest object */
     dataRequest.setConnection(conn);
     dataRequest.setStatementID("UPDATE_SERVICE_FEE_OVERRIDE");
     dataRequest.setInputParams(inputParams);
     
     /* execute the store prodcedure */
     final DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     final Map outputs = (Map) dataAccessUtil.execute(dataRequest);
       
     /* read store prodcedure output parameters to determine 
      * if the procedure executed successfully */
     final String outStatus = (String) outputs.get("OUT_STATUS");
     if(logger.isDebugEnabled())
     {
       logger.debug("outStatus: " + outStatus);
     }

     if(outStatus != null && outStatus.equalsIgnoreCase("N"))
     {
       final String message = (String) outputs.get("OUT_MESSAGE");
       throw new Exception(message);
     }
     // check if override date falls within a delivery date range
     if(outStatus != null && outStatus.equalsIgnoreCase("DATE_RANGE")) {
         result = "DATE_RANGE";
     }
     return result;
 }

 /**
 * Calls the DELETE_SERVICE_FEE stored procedure.
 *
 * @param serviceFeeId
 */
 public void deleteServiceFeeOverride(String serviceFeeId, String overrideDate, ServiceFeeOverrideVO serviceFee)
    throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
 {
    final DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);

    if(logger.isDebugEnabled())
    {
        logger.debug("deleteServiceFee(): " + serviceFeeId + "-" + overrideDate);
    }
       
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    Date newDate = new java.sql.Date(sdf.parse(overrideDate).getTime());
       
    /* setup store procedure input parameters */
    Map inputParams = new HashMap();
    inputParams.put("IN_SNH_ID", serviceFeeId);
    inputParams.put("IN_OVERRIDE_DATE", newDate);
    inputParams.put("IN_REQUESTED_BY", serviceFee.getRequestedBy());
    inputParams.put("IN_UPDATED_BY", serviceFee.getUpdatedBy());
     
    /* build DataRequest object */
    dataRequest.setStatementID("UPDATE_BEFORE_DELETE_SFO");
    dataRequest.setInputParams(inputParams);
     
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
       
    /* read stored procedure output parameters to determine 
     * if the procedure executed successfully */
    String outStatus = (String) outputs.get("OUT_STATUS");
    if(outStatus != null && outStatus.equalsIgnoreCase("N"))
    {
      final String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    /* setup store procedure input parameters */
    inputParams = new HashMap();
    inputParams.put("IN_SNH_ID", serviceFeeId);
    inputParams.put("IN_OVERRIDE_DATE", newDate);
    
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("DELETE_SERVICE_FEE_OVERRIDE");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    outStatus = (String) outputs.get("OUT_STATUS");
    if(outStatus != null && outStatus.equalsIgnoreCase("N"))
    {
      final String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
 }


  /**
   * Method to retrieve the box info.  If boxId is null, will retrieve all boxes. Else, will only retrieve the info 
   * for the boxId passed. 
   *
   * @input  String boxId - could be a null or the string containing the box id. 
   * @return XMLDocument containing the values from the cursor
   *
   * @throws java.lang.Exception
   */


  public Document getBoxInfo(String boxId)
        throws Exception
  {
	  	Document searchResults;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_BOX_INFO_XML");
        dataRequest.addInputParam("IN_BOX_ID", boxId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }



  /**
   * Method to retrieve the box info.  If boxId is null, will retrieve all boxes. Else, will only retrieve the info 
   * for the boxId passed. 
   *
   * @input  String boxId - could be a null or the string containing the box id. 
   * @return XMLDocument containing the values from the cursor
   *
   * @throws java.lang.Exception
   */


  public CachedResultSet getStandardBoxInfo()
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("GET_STANDARD_BOX_INFO_CRS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }


  /**
   * method to insert a box 
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void insertBoxInfo(String boxName, String boxDesc, String boxWidth, String boxHeight, 
                            String boxLength, String standardFlag, String csrId )
        throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_BOX_NAME", boxName);
    inputParams.put("IN_BOX_DESC", boxDesc);
    inputParams.put("IN_WIDTH_INCH_QTY", boxWidth);
    inputParams.put("IN_HEIGHT_INCH_QTY", boxHeight);
    inputParams.put("IN_LENGTH_INCH_QTY", boxLength);
    inputParams.put("IN_STANDARD_FLAG", standardFlag);
    inputParams.put("IN_CREATED_BY", csrId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("INSERT_BOX_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to update a box
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void updateBoxInfo(String boxId, String boxName, String boxDesc, String boxWidth, String boxHeight, 
                            String boxLength, String csrId )
        throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_BOX_ID", boxId);
    inputParams.put("IN_BOX_NAME", boxName);
    inputParams.put("IN_BOX_DESC", boxDesc);
    inputParams.put("IN_WIDTH_INCH_QTY", boxWidth);
    inputParams.put("IN_HEIGHT_INCH_QTY", boxHeight);
    inputParams.put("IN_LENGTH_INCH_QTY", boxLength);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("UPDATE_BOX_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to reassign a box to 1 or all products;
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void reassignBoxToProduct(String originalBoxId, String newBoxName, String productId, String csrId )
        throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORIGINAL_BOX_ID", originalBoxId);
    inputParams.put("IN_NEW_BOX_ID", newBoxName);
    inputParams.put("IN_PRODUCT_ID", productId);
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("REASSIGN_BOX_TO_PRODUCT_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to delete a box
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void deleteBoxInfo(String boxId)
        throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_BOX_ID", boxId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("DELETE_BOX_INFO_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
   * method to retrieve global parms for a given context
   *
   * @return XMLDocument containing the values from the cursor
   * @throws java.lang.Exception
   */


  public Document getGlobalInfoXML(String selectedContext)
        throws Exception
  {
	  	Document searchResults;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("VIEW_PARMS_BY_CONTEXT_XML");
        dataRequest.addInputParam("IN_CONTEXT", selectedContext);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }


  /**
   * method to retrieve global parms for a given context
   *
   * @return XMLDocument containing the values from the cursor
   * @throws java.lang.Exception
   */


  public CachedResultSet getGlobalInfoCRS(String selectedContext)
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("VIEW_PARMS_BY_CONTEXT_CRS");
        dataRequest.addInputParam("IN_CONTEXT", selectedContext);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }


  /**
   * method to update global parms for a given context
   *
   * @return void
   * @throws java.lang.Exception
   */


  public void updateGlobalParm(String context, String name, String value, String description, String csrId )
        throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CONTEXT", context);
    inputParams.put("IN_NAME", name);
    inputParams.put("IN_VALUE", value);
    inputParams.put("IN_DESCRIPTION",  description); 
    inputParams.put("IN_UPDATED_BY", csrId);

    /* build DataRequest object */
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("OPM_UPDATE_GLOBAL_PARMS_CRS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }

    /**
    * Calls the GET_COMPONENT_SKU_LIST stored procedure.
    * 
    * @param
    * @return XML Document
    */
    public Document getComponentSkuList(String componentSkuId) throws IOException, ParserConfigurationException, SAXException, SQLException
    {    
        DataRequest dataRequest = new DataRequest();
        
        if(logger.isDebugEnabled())
        {
          logger.debug("getComponentSkuList()");
        }
          
        final Map inputParams = new HashMap();
        if (componentSkuId != null) {
            inputParams.put("IN_COMPONENT_SKU_ID", componentSkuId);
        }

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_COMPONENT_SKU_LIST");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }
    
    /**
    * Calls the UPDATE_INSERT_SERVICE_FEE stored procedure.
    * 
    * @param componentSku - ComponentSkuVO
    */
    public void updateComponentSku(ComponentSkuVO componentSku) 
        throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
    {
        final DataRequest dataRequest = new DataRequest();
         
        if(logger.isDebugEnabled())
        {
            logger.debug("updateComponentSku()");
        }
          
        /* setup store procedure input parameters */
        final Map inputParams = new HashMap();
        inputParams.put("IN_COMPONENT_SKU_ID", componentSku.getComponentSkuId());
        inputParams.put("IN_COMPONENT_SKU_NAME", componentSku.getComponentSkuName());
        if (componentSku.getBaseCost() > 0) {
            inputParams.put("IN_BASE_COST_DOLLAR_AMT", componentSku.getBaseCost());
        }
        if (componentSku.getBaseCostEffectiveDate() != null) {
            inputParams.put("IN_BASE_COST_EFF_DATE", new java.sql.Date(componentSku.getBaseCostEffectiveDate().getTime()));
        }
        if (componentSku.getQaCost() > 0) {
            inputParams.put("IN_QA_COST_DOLLAR_AMT", componentSku.getQaCost());
        }
        if (componentSku.getQaCostEffectiveDate() != null) {
            inputParams.put("IN_QA_COST_EFF_DATE", new java.sql.Date(componentSku.getQaCostEffectiveDate().getTime()));
        }
        if (componentSku.getFreightInCost() > 0) {
            inputParams.put("IN_FREIGHT_IN_COST_DOLLAR_AMT", componentSku.getFreightInCost());
        }
        if (componentSku.getFreightInCostEffectiveDate() != null) {
            inputParams.put("IN_FREIGHT_IN_COST_EFF_DATE", new java.sql.Date(componentSku.getFreightInCostEffectiveDate().getTime()));
        }
        inputParams.put("IN_AVAILABLE_FLAG", componentSku.getAvailableFlag());
        inputParams.put("IN_COMMENT_TEXT", componentSku.getCommentText());
        inputParams.put("IN_UPDATED_BY", componentSku.getUpdatedBy());
        
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_COMPONENT_SKU");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        final DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        final Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        final String outStatus = (String) outputs.get("OUT_STATUS");
        if(outStatus != null && outStatus.equalsIgnoreCase("N"))
        {
            final String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    public String getPricePayVendorBatchId() throws IOException, SQLException, Exception {
        logger.debug("getPricePayVendorBatchId()");

        DataRequest dataRequest = new DataRequest();
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRICE_PAY_VENDOR_BATCH_ID");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String outStatus = (String) outputs.get("OUT_STATUS");
        if(outStatus != null && outStatus.equalsIgnoreCase("N"))
        {
            final String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        String batchId = (String) outputs.get("OUT_BATCH_ID");
        return batchId;
    }
    
    public void insertPricePayVendorUpdate(PricePayVendorVO ppvVO) throws Exception {

        logger.debug("insertPricePayVendorUpdate(PricePayVendorVO ppvVO) :: void");
         
        DataRequest dataRequest = new DataRequest();
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_BATCH_ID", ppvVO.getBatchId());
        inputParams.put("IN_SPREADSHEET_ROW_NUM", ppvVO.getSpreadsheetRowNum());
        inputParams.put("IN_PRODUCT_ID", ppvVO.getProductId());
        inputParams.put("IN_VENDOR_ID", ppvVO.getVendorId());
        inputParams.put("IN_VENDOR_COST", ppvVO.getVendorCost());
        inputParams.put("IN_STANDARD_PRICE", ppvVO.getStandardPrice());
        inputParams.put("IN_DELUXE_PRICE", ppvVO.getDeluxePrice());
        inputParams.put("IN_PREMIUM_PRICE", ppvVO.getPremiumPrice());
        inputParams.put("IN_PROCESSED_FLAG", ppvVO.getProcessedFlag());
        inputParams.put("IN_ERROR_MSG_TXT", ppvVO.getErrorMessage());
        inputParams.put("IN_VENDOR_COST_VARIANCE", ppvVO.getVendorCostVariancePct());
        inputParams.put("IN_STANDARD_PRICE_VARIANCE", ppvVO.getStandardPriceVariancePct());
        inputParams.put("IN_DELUXE_PRICE_VARIANCE", ppvVO.getDeluxePriceVariancePct());
        inputParams.put("IN_PREMIUM_PRICE_VARIANCE", ppvVO.getPremiumPriceVariancePct());
        inputParams.put("IN_USER_ID", ppvVO.getUserId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_PRICE_PAY_VENDOR_UPDATE");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
        
    }
    
    public void updatePricePayVendorUpdate(PricePayVendorVO ppvVO) throws Exception {

        logger.debug("updatePricePayVendorUpdate(PricePayVendorVO ppvVO) :: void");
        logger.debug(ppvVO.getBatchId() + " " + ppvVO.getSpreadsheetRowNum() + " " + ppvVO.getProcessedFlag() + " " + ppvVO.getErrorMessage());
         
        DataRequest dataRequest = new DataRequest();
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_BATCH_ID", ppvVO.getBatchId());
        inputParams.put("IN_SPREADSHEET_ROW_NUM", ppvVO.getSpreadsheetRowNum());
        inputParams.put("IN_PROCESSED_FLAG", ppvVO.getProcessedFlag());
        inputParams.put("IN_ERROR_MSG_TXT", ppvVO.getErrorMessage());
        inputParams.put("IN_USER_ID", ppvVO.getUserId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_PRICE_PAY_VENDOR_UPDATE");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
        
    }
    
    public List getPricePayVendorUpdateList(String batchId) throws Exception
    {
        logger.debug("getPricePayVendorUpdateList(" + batchId + ")");

        DataRequest dataRequest = new DataRequest();    
        List ppvList = new ArrayList();

        HashMap inputParams = new HashMap();
        inputParams.put("IN_BATCH_ID", batchId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRICE_PAY_VENDOR_LIST");
        dataRequest.setInputParams(inputParams);
      
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (outputs.next()) {
            PricePayVendorVO ppvVO = new PricePayVendorVO();
            ppvVO.setBatchId(outputs.getInt("price_pay_vendor_batch_id"));
            ppvVO.setSpreadsheetRowNum(outputs.getInt("spreadsheet_row_num"));
            ppvVO.setProductId(outputs.getString("product_id"));
            ppvVO.setVendorId(outputs.getString("vendor_id"));
            ppvVO.setVendorCost(outputs.getString("vendor_cost"));
            ppvVO.setStandardPrice(outputs.getString("standard_price"));
            ppvVO.setDeluxePrice(outputs.getString("deluxe_price"));
            ppvVO.setPremiumPrice(outputs.getString("premium_price"));
            ppvVO.setErrorMessage(outputs.getString("error_msg_txt"));
            ppvVO.setUserId(outputs.getString("created_by"));
            ppvVO.setVendorCostVariancePct(outputs.getDouble("vendor_cost_variance_pct"));
            ppvVO.setStandardPriceVariancePct(outputs.getDouble("STANDARD_PRICE_VARIANCE_PCT"));
            ppvVO.setDeluxePriceVariancePct(outputs.getDouble("DELUXE_PRICE_VARIANCE_PCT"));
            ppvVO.setPremiumPriceVariancePct(outputs.getDouble("PREMIUM_PRICE_VARIANCE_PCT"));

            ppvList.add(ppvVO);
        }
      
        return ppvList;
    }

    public Document getPricePayVendorApprovalList(String batchId) throws Exception {
        logger.debug("getPricePayVendorApprovalList(" + batchId + ")");

        DataRequest dataRequest = new DataRequest();    
        HashMap inputParams = new HashMap();
        inputParams.put("IN_BATCH_ID", batchId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PPV_APPROVAL_LIST");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        return (Document) dataAccessUtil.execute(dataRequest);
    }

    public CachedResultSet getPricePayVendorApprovalCounts(String batchId) throws Exception {
        logger.debug("getPricePayVendorApprovalCounts(" + batchId + ")");
        
        CachedResultSet rs = null;
        try {
            Map inputParams = new HashMap();
            inputParams.put("IN_BATCH_ID", batchId);
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("GET_PPV_APPROVAL_COUNTS");
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        } catch (Exception e) {            
            logger.error(e);
        } 
        
        return rs;
    }
    
  public VendorProductVO getVendorProduct(String vendorId, String productId) throws Exception {
      DataRequest dataRequest = new DataRequest();
      VendorProductVO vprod = new VendorProductVO();
      boolean isEmpty = true;
      
      try
      {
          logger.debug("getVendorProduct (String vendorId, String productId) :: VendorProductVO");

          HashMap inputParams = new HashMap();
          inputParams.put("IN_VENDOR_ID", vendorId);
          inputParams.put("IN_PRODUCT_ID", productId);
        
          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("GET_VENDOR_PRODUCT");
          dataRequest.setInputParams(inputParams);
        
          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
          CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
          while (outputs.next()) {
              isEmpty = false;
              vprod.setVendorCost(outputs.getDouble("vendor_cost"));
          }
      }
      catch (Exception e) {            
          logger.error(e);
      } 
      if (isEmpty)
        return null;
      else
        return vprod;
        
  }

    public CachedResultSet getReportById(String reportId) throws Exception {

        CachedResultSet reportRS = null;
        try {
            Map inputParams = new HashMap();
            inputParams.put("IN_REPORT_ID", reportId);
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("GET_REPORT_BY_ID");
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            reportRS = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        } catch (Exception e) {            
            logger.error(e);
        } 

        return reportRS;
    }

    public String insertReportLog(String reportId, String rdfFileName, String reportUrl) {
        String submissionId = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

        // Build db stored procedure input parms
        Map inputParams = new HashMap();
        inputParams.put("IN_REPORT_ID", reportId);

        // The file name inserted into the db must be unique, so add a unique timestamp suffix.
        String uniqueFileName = rdfFileName + "_" + sdf.format(new java.util.Date());
        inputParams.put("IN_REPORT_FILE_NAME", uniqueFileName);
        inputParams.put("IN_SUBMISSION_TYPE", "U");
        inputParams.put("IN_SUBMITTED_BY", "pdb");
        inputParams.put("IN_REPORT_STATUS", "O");
        inputParams.put("IN_ORACLE_REPORT_URL", reportUrl);

        // Submit the request to the database.
        try {
            /* build DataRequest object */
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("INSERT_REPORT_SUBMISSION_LOG");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
              
            String status = (String) outputs.get("OUT_STATUS");
            if(status != null && status.equalsIgnoreCase("N"))
            {
              String message = (String) outputs.get("OUT_MESSAGE");
              throw new Exception(message);
            }
            
            submissionId = (String) outputs.get("OUT_REPORT_SUBMISSION_ID");
            
        } catch (Exception e) {
            String errorMsg = "insertReportLog: Could not insert report submission into database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }

        return submissionId;
    }

    public void updateProductMasterPrices(PricePayVendorVO ppvVO) throws Exception {

        logger.debug("updateProductMasterPrices(PricePayVendorVO ppvVO) :: void");
        logger.debug(ppvVO.getProductId() + " " + ppvVO.getStandardPrice() + " " + ppvVO.getDeluxePrice() + " " + ppvVO.getPremiumPrice() + " " + ppvVO.getUserId());
         
        DataRequest dataRequest = new DataRequest();
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", ppvVO.getProductId());
        inputParams.put("IN_STANDARD_PRICE", ppvVO.getStandardPrice());
        inputParams.put("IN_DELUXE_PRICE", ppvVO.getDeluxePrice());
        inputParams.put("IN_PREMIUM_PRICE", ppvVO.getPremiumPrice());
        inputParams.put("IN_USER_ID", ppvVO.getUserId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_PRODUCT_MASTER_PRICES");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
    }
    
    public void updateVendorCost(PricePayVendorVO ppvVO) throws Exception {

        logger.debug("updateVendorCost(PricePayVendorVO ppvVO) :: void");
        logger.debug(ppvVO.getVendorId() + " " + ppvVO.getProductId() + " " + ppvVO.getVendorCost() + " " + ppvVO.getUserId());
         
        DataRequest dataRequest = new DataRequest();
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_VENDOR_ID", ppvVO.getVendorId());
        inputParams.put("IN_PRODUCT_ID", ppvVO.getProductId());
        inputParams.put("IN_VENDOR_COST", ppvVO.getVendorCost());
        inputParams.put("IN_USER_ID", ppvVO.getUserId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_VENDOR_COST");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
    }
    
/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
/*
 * Moved to com.ftd.op.maintenance.util.CommonUtil
private XMLDocument buildXML(HashMap outMap, String cursorName, String topName, String bottomName) throws Exception
  {

    ResultSet rs = (ResultSet) outMap.get(cursorName);
  
    XMLDocument doc =  null;
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", "element");
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );
  
    doc = (XMLDocument) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    if (rs != null)  
    {
      rs.close();
    }
  
    return doc;
  
  }  
  */
    
	/*******************************************************************************************************
	 ****************** 11945 - BBN - Delivery Fee Maintenance Methods start here **************************
	 *******************************************************************************************************/
    
	/** Convenient method to get the Delivery Fee list from DB.
	 * @param sortField
	 * @param deliveryFeeType
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 */
	public Document getDeliveryFeeList(String sortField, int deliveryFeeType) throws IOException,
			ParserConfigurationException, SAXException, SQLException {
		
		DataRequest dataRequest = new DataRequest();

		if (logger.isDebugEnabled()) {
			logger.debug("getDeliveryFeeList()");
		}

		Map inputParams = new HashMap();
		inputParams.put("IN_SORT_FIELD", sortField);

		/* build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID(getListDeliveryFeeStatementId(deliveryFeeType));
		dataRequest.setInputParams(inputParams);

		/* execute the stored procedure */
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (Document) dataAccessUtil.execute(dataRequest);
	}
	
	/** 11945 - Delete Delivery Fee information. It updates the requested_by, 
	 * Updated_by and then deletes the delivery fee info.
	 * @param deliveryFee
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 * @throws Exception
	 */
	public void deleteDeliveryFee(DeliveryFeeVO deliveryFee) 
			throws IOException, ParserConfigurationException, SAXException,	SQLException, Exception {
		final DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(conn);

		if (logger.isDebugEnabled()) {
			logger.debug("deleteDeliveryFee(): " + deliveryFee.getId());
		}

		// setup stored procedure input parameters 
		Map inputParams = new HashMap();
		inputParams.put("IN_DELETE_ID", deliveryFee.getId());
		inputParams.put("IN_REQUESTED_BY", deliveryFee.getRequestedBy());
		inputParams.put("IN_UPDATED_BY", deliveryFee.getUpdatedBy());

		// build DataRequest object 
		dataRequest.setStatementID("UPDATE_DELETE_DELIVERY_FEE");
		dataRequest.setInputParams(inputParams);

		// execute the store procedure 
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		
		//read stored procedure output parameters to determine if the procedure
		// executed successfully
		String outStatus = (String) outputs.get("OUT_STATUS");
		if (outStatus != null && outStatus.equalsIgnoreCase("N")) {
			final String message = (String) outputs.get("OUT_MESSAGE");
			throw new Exception(message);
		}
	}
	
	/** Calls the UPDATE_INSERT_DELIVERY_FEE stored procedure.
	 * @param deliveryFee
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 * @throws Exception
	 */
	public void saveDeliveryFee(DeliveryFeeVO deliveryFee) throws IOException,
			ParserConfigurationException, SAXException, SQLException, Exception {
		
		final DataRequest dataRequest = new DataRequest();
		
		if (logger.isDebugEnabled()) {
			logger.debug("saveDeliveryFee()");
		}

		/* setup store procedure input parameters */
		final Map inputParams = new HashMap();
		inputParams.put("IN_ID", deliveryFee.getId());
		inputParams.put("IN_DESCRIPTION", deliveryFee.getDescription());
		inputParams.put("IN_DELIVERY_FEE", deliveryFee.getDeliveryFee());
		inputParams.put("IN_UPDATED_BY", deliveryFee.getUpdatedBy());
		inputParams.put("IN_REQUESTED_BY", deliveryFee.getRequestedBy());

		/* build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID(getSaveDeliveryFeeStatementId(deliveryFee.getDeliveryFeeType()));
		dataRequest.setInputParams(inputParams);

		/* execute the store procedure */
		final DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		final Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		/*
		 * read stored procedure output parameters to determine if the procedure
		 * executed successfully
		 */
		final String outStatus = (String) outputs.get("OUT_STATUS");
		if (outStatus != null && outStatus.equalsIgnoreCase("N")) {
			final String message = (String) outputs.get("OUT_MESSAGE");
			throw new Exception(message);
		}
	}
	
	/** This method is to insert Morning Delivery Fee Feed when there are any changes.
	 * @param  xml
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 */
	
	public void insertDeliveryFeeFeed(String xml) throws IOException,
	ParserConfigurationException, SAXException, SQLException, Exception {

		final DataRequest dataRequest = new DataRequest();
		
		if (logger.isDebugEnabled()) {
			logger.debug("insertDeliveryFeeFeed()");
		}
		
		/* setup store procedure input parameters */
		final Map inputParams = new HashMap();
		
        inputParams.put("IN_FEED_XML",xml);
        inputParams.put("IN_FEED_TYPE","MORNING_DELIVERY");
        inputParams.put("IN_STATUS","NEW");
		
		/* build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID("INSERT_MDELIVERY_FEE_FEED");
		dataRequest.setInputParams(inputParams);
		
		/* execute the store procedure */
		final DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		final Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		
		/*
		 * read stored procedure output parameters to determine if the procedure
		 * executed successfully
		 */
		final String outStatus = (String) outputs.get("OUT_STATUS");
		if (outStatus != null && outStatus.equalsIgnoreCase("N")) {
			final String message = (String) outputs.get("OUT_MESSAGE");
			throw new Exception(message);
		}
	}
	
	/** Convenient method to get the history of a given Delivery Fee id.
	 * @param deliveryFeeId
	 * @param deliveryFeeType
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 */
	public Document getDeliveryFeeHistory(String deliveryFeeId, int deliveryFeeType)
			throws IOException, ParserConfigurationException, SAXException, SQLException {
		DataRequest dataRequest = new DataRequest();

		if (logger.isDebugEnabled()) {
			logger.debug("getDeliveryFeeHistory()");
		}

		/* build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID(getHistoryDeliveryFeeStatementId(deliveryFeeType));

		final Map inputParams = new HashMap();
		inputParams.put("IN_DELIVERY_FEE_ID", deliveryFeeId);
		dataRequest.setInputParams(inputParams);

		/* execute the stored procedure */
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (Document) dataAccessUtil.execute(dataRequest);
	}
	
	/**
	 * @param deliveryFeeId
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 */
	public Document getSourceCodesByDeliveryId(String deliveryFeeId) throws IOException,
			ParserConfigurationException, SAXException, SQLException {
		DataRequest dataRequest = new DataRequest();

		if (logger.isDebugEnabled()) {
			logger.debug("getSourceCodesByDeliveryId()");
		}

		/* build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID("GET_SOURCE_BY_DELIVERY_FEE_ID");

		final Map inputParams = new HashMap();
		inputParams.put("IN_DELIVERY_FEE_ID", deliveryFeeId);
		dataRequest.setInputParams(inputParams);

		/* execute the stored procedure */
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (Document) dataAccessUtil.execute(dataRequest);
	}
	
	/**
	 * This method is responsible to find out ship methods of original product and new product in the mapping.
	 * @param origProductId
	 * @param newProductId
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getShipMethodsBySKU(String origProductId, String newProductId) throws Exception
	{
		Map<String, String> shipMethodMap = new HashMap<String, String>();
		DataRequest dataRequest = new DataRequest();
		
			/* Build DataRequest object */
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("GET_SHIP_METHOD_BY_SKUS");
			Map inputParams = new HashMap();
			inputParams.put("IN_ORIGINAL_PRODUCT_ID", origProductId);
			inputParams.put("IN_NEW_PRODUCT_ID", newProductId);
			dataRequest.setInputParams(inputParams);
			
			/* execute the stored procedure */
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	        while (outputs.next()) {
	        	String productId = outputs.getString("product_id"); 
	        	String shipMethodFlorist = outputs.getString("ship_method_florist");
	        	String shipMethodCarrier = outputs.getString("ship_method_carrier");
	        	if(StringUtils.isNotBlank(productId) ){
	        		if("Y".equalsIgnoreCase(shipMethodFlorist) && "N".equalsIgnoreCase(shipMethodCarrier))
		            {
		            	shipMethodMap.put(productId.toUpperCase(), "F");
		            }
		            else if("Y".equalsIgnoreCase(shipMethodCarrier) && "N".equalsIgnoreCase(shipMethodFlorist))
		            {
		            	shipMethodMap.put(productId.toUpperCase(), "V");
		            }else if ("Y".equalsIgnoreCase(shipMethodCarrier) && "Y".equalsIgnoreCase(shipMethodFlorist)){
		            	shipMethodMap.put(productId.toUpperCase(), "SDFC");
		            }else{
		            	shipMethodMap.put(productId.toUpperCase(), "NA");
		            }
	        	}
	            
	            
	        }
		      
		return shipMethodMap;
		
	}
	
	/**
	 * This method is used to retrieve all existing product mappings from database.
	 * @return
	 * @throws Exception
	 */
	public CachedResultSet retrieveProductMappings() throws Exception
	{
		DataRequest dataRequest = new DataRequest();
		/* Build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID("GET_PRODUCT_MAPPINGS");
		Map inputParams = new HashMap();
		dataRequest.setInputParams(inputParams);
		
		/* execute the stored procedure */
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (CachedResultSet) dataAccessUtil.execute(dataRequest);
		
	}
    
    /**
     * This method is used to retrieve existing mappings for a single product.
     * @return
     * @throws Exception
     */
    public CachedResultSet retrieveProductMappingByFloristProduct(String origProductId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        /* Build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_MAPPING");
        Map inputParams = new HashMap();
        inputParams.put("IN_ORIGINAL_PRODUCT_ID", origProductId);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (CachedResultSet) dataAccessUtil.execute(dataRequest);
    }
	
    
	/**
	 * This method is used to insert a new product mapping to the database 
	 * @param origProductId
	 * @param newProductId
	 * @param goodBetterBest
	 * @throws Exception
	 */
	public void addProductMapping(String origProductId, String newProductId, String csrId, String goodBetterBest) throws Exception
	{
		DataRequest dataRequest = new DataRequest();
		/* Build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID("ADD_PRODUCT_MAPPING");
		Map inputParams = new HashMap();
		inputParams.put("IN_ORIGINAL_PRODUCT_ID", StringUtils.upperCase(origProductId));
		inputParams.put("IN_NEW_PRODUCT_ID", StringUtils.strip(StringUtils.upperCase(newProductId)));
		inputParams.put("IN_CSR_ID", csrId);
		inputParams.put("IN_PRODUCT_PRICE_POINT_KEY", goodBetterBest);
		dataRequest.setInputParams(inputParams);
		
		/* execute the stored procedure */
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
        	String message = (String) outputs.get("OUT_MESSAGE");
        	if (StringUtils.isNotEmpty(message) && "PRODUCT MAPPING ALREADY ON FILE".equalsIgnoreCase(message))
        	{
        		throw new DuplicateProductMappingException("Original product id already exists on file");
        	}
        	else
        	{
        		throw new Exception(message);
        	}
        	
        }
	}
	
	/**
	 * This method is used to delete an existing product mapping from database.
	 * @param origProductId
	 * @throws Exception
	 */
	public void deleteProductMapping(String origProductId) throws Exception
	{
		DataRequest dataRequest = new DataRequest();
		
		/* Build DataRequest object */
		dataRequest.setConnection(conn);
		dataRequest.setStatementID("DELETE_PRODUCT_MAPPING");
		Map inputParams = new HashMap();
		inputParams.put("IN_ORIGINAL_PRODUCT_ID", origProductId);
		dataRequest.setInputParams(inputParams);
		
		/* execute the stored procedure */
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
	}

	
	/**
	 * This method determines if product can be used as a Phoenix Mapping product.
	 * 
	 * @param prodId - Product ID to be confirmed
	 * @param isAddon - True to also confirm product is addon. False otherwise. 
	 * @return "Y" - Product is allowed, "N" - Product not allowed, "N/A" - No product found
	 */
	public String isProductVendorMappable(String prodId, boolean isAddon) throws Exception {
        DataRequest dataRequest = new DataRequest();
        /* Build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("IS_PRODUCT_VENDOR_MAPPABLE");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", prodId);
        if (isAddon) {
            inputParams.put("IN_IS_ADDON", "Y");
        } else {
            inputParams.put("IN_IS_ADDON", "N");            
        }
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String retval = (String) dataAccessUtil.execute(dataRequest);
        
        return retval;
	}
		
	/** gets the statement Id based on delivery fee type.
	 * @param deliveryFeeType
	 * @return
	 */
	private String getListDeliveryFeeStatementId(int deliveryFeeType) {
		switch(deliveryFeeType) {
		case OPMConstants.MORNING_DELIVERY_TYPE:
			return "GET_MORNING_DELIVERY_FEE_LIST";
		default:
			return null;
		}
	}	
	
	/** gets the statement Id based on delivery fee type.
	 * @param deliveryFeeType
	 * @return
	 */
	private String getSaveDeliveryFeeStatementId(int deliveryFeeType) {
		switch(deliveryFeeType) {
		case OPMConstants.MORNING_DELIVERY_TYPE:
			return "UPDATE_INSERT_MORNING_DELIVERY";
		default:
			return null;
		}
	}
	
	/** gets the statement Id based on delivery fee type.
	 * @param deliveryFeeType
	 * @return
	 */
	private String getHistoryDeliveryFeeStatementId(int deliveryFeeType) {
		switch(deliveryFeeType) {
		case OPMConstants.MORNING_DELIVERY_TYPE:
			return "GET_MORNING_DELIVERY_FEE_HIST";
		default:
			return null;
		}
	}
}
