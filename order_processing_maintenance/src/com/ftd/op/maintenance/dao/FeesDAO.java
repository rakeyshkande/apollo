package com.ftd.op.maintenance.dao;

import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Map;


public class FeesDAO 
{

    private String LOGGER_CATEGORY = "com.ftd.op.maintenance.dao.FeesDAO";
    private Connection conn;
    private Logger logger;

    public FeesDAO(Connection conn)
    {
        this.conn = conn;
        logger = new Logger(LOGGER_CATEGORY);        
    }
    
     
    public String getFuelSurcharge() throws Exception
    {
        FTDFuelSurchargeUtilities util = new FTDFuelSurchargeUtilities();
        String feeAmt = util.getFuelSurcharge(this.conn);
        CommonUtil commonUtil = new CommonUtil();
        
        feeAmt = commonUtil.formatDoubleAmount(Double.valueOf(feeAmt).doubleValue());
        return feeAmt; 
    }
        
    public boolean saveFuelSurcharge(String onOffFlag, String fuelSurchargeAmt,String sendToFloristFlag,String surchargeDescription, String showSurchchargeSeparately, String userid) throws Exception
    {
        boolean result = false;
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("UPDATE_FUEL_SURCHARGE");
        dataRequest.addInputParam("IN_FUEL_SURCHARGE_ON_OFF_FLAG",onOffFlag);
        dataRequest.addInputParam("IN_FUEL_SURCHARGE_AMT",fuelSurchargeAmt);
        dataRequest.addInputParam("IN_FUEL_SURCHARGE_DESC",surchargeDescription);
        dataRequest.addInputParam("IN_SEND_SURCHARGE_TO_FLORIST",sendToFloristFlag);
        dataRequest.addInputParam("IN_DISPLAY_SURCHARGE",showSurchchargeSeparately);
        dataRequest.addInputParam("IN_UPDATED_BY", userid);
       
        logger.debug("fuelSurchargeAmount:" + fuelSurchargeAmt);
        logger.debug("onOffFlag:" + onOffFlag);
        logger.debug("sendToFloristFlag:" + sendToFloristFlag);
        logger.debug("surchargeDescription:" + surchargeDescription);
        logger.debug("showSurchchargeSeparately:" + showSurchchargeSeparately);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("Y"))
        {
            result = true;
        } else
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        return result;
    }  
    
    /** This method is to insert Fuel Surcharge Feed when there are any changes.
	 * @param  xml
	 * @throws Exception
	 */
    
    
    public boolean InsertFuelSurchargeFeed(String xml) throws Exception{
    	
        boolean result = false;
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        dataRequest.setConnection(conn);        
        dataRequest.setStatementID("INSERT_FUEL_SURCHARGE_FEED");
        dataRequest.addInputParam("IN_FEED_XML",xml);
        dataRequest.addInputParam("IN_FEED_TYPE","FUEL");
        dataRequest.addInputParam("IN_STATUS","NEW");
        
       
        
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        /* read store prodcedure output parameters to determine 
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("Y"))
        {
            result = true;
        } else
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        return result;
        
    }
    
   
}