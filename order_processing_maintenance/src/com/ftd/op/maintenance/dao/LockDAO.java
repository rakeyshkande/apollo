package com.ftd.op.maintenance.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

/**
 * LockDAO
 * This is the DAO that handles creating a lock for a requesting CSR, checking whether a record is locked
 * by another CSR, retrieving the lock for a CSR.
 *
 * @author Srinivas Makam
 */

public class LockDAO
{
  private Connection connection;
  private static Logger logger = new Logger("com.ftd.customerordermanagement.dao.LockDAO");

  public LockDAO(Connection connection)
  {
    this.connection = connection;
  }

  public Document checkLockXML(String sessionId, String csrId, String entityId, String entityType)
    throws Exception
  {
    return checkLockXML(sessionId, csrId, entityId, entityType, null);
  }

  public Document checkLockXML(String sessionId, String csrId, String entityId, String entityType, String orderLevel)
    throws Exception
  {
    Document lock = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("CHECK_CSR_LOCKED_ENTITIES_XML");
    dataRequest.addInputParam("IN_SESSION_ID", sessionId);
    dataRequest.addInputParam("IN_CSR_ID", csrId);
    dataRequest.addInputParam("IN_ENTITY_TYPE", entityType);
    dataRequest.addInputParam("IN_ENTITY_ID", entityId);
    dataRequest.addInputParam("IN_ORDER_LEVEL", orderLevel);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    lock = (Document) dataAccessUtil.execute(dataRequest);

    return lock;
  }


  public Document retrieveLockXML(String sessionId, String csrId, String entityId, String entityType)
    throws Exception
  {
    return retrieveLockXML(sessionId, csrId, entityId, entityType, null);
  }


  public Document retrieveLockXML(String sessionId, String csrId, String entityId, String entityType, String orderLevel)
    throws Exception
  {
    Document lock = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("UPDATE_CSR_LOCKED_ENTITIES_XML");
    dataRequest.addInputParam("IN_SESSION_ID", sessionId);
    dataRequest.addInputParam("IN_CSR_ID", csrId);
    dataRequest.addInputParam("IN_ENTITY_TYPE", entityType);
    dataRequest.addInputParam("IN_ENTITY_ID", entityId);
    dataRequest.addInputParam("IN_ORDER_LEVEL", orderLevel);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    lock = (Document) dataAccessUtil.execute(dataRequest);

    String status = DOMUtil.getNodeValue(lock, "OUT_STATUS");

    //if the status is N, its a database error.  so, throw an error.  Check for the record
    //to be locked or not will be handle via OUT_LOCKED_OBTAINED parm in the BO/Action
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = DOMUtil.getNodeValue(lock, "OUT_MESSAGE");

      logger.debug("****LockDAO**** An DB error was encountered while retrieving a lock.  Error text = " + message);
      throw new Exception(message);
    }

    return lock;
  }


  /**
   * Calls the UPDATE_CSR_LOCKED_ENTITIES stored procedure.
   * This method will return that value that was returned by the stored proc.
   * (If the lock was obtained a Y is returned.  If the lock was not obtained
   * then a message indicating who has the lock is returned.)
   *
   * @param OrderDetailVO
   * @return HashMap
   */
  public HashMap retrieveLockCRS(String sessionId, String csrId, String entityId, String entityType)
    throws Exception
  {
    return retrieveLockCRS(sessionId, csrId, entityId, entityType, null); 
  }

  public HashMap retrieveLockCRS(String sessionId, String csrId, String entityId, String entityType, String orderLevel)
    throws Exception
  {
    Map outputs = null;
    DataRequest dataRequest = new DataRequest();
    /* build DataRequest object */
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_CSR_LOCKED_ENTITIES_CRS");
    dataRequest.addInputParam("IN_SESSION_ID", sessionId);
    dataRequest.addInputParam("IN_CSR_ID", csrId);
    dataRequest.addInputParam("IN_ENTITY_TYPE", entityType);
    dataRequest.addInputParam("IN_ENTITY_ID", entityId);
    dataRequest.addInputParam("IN_ORDER_LEVEL", orderLevel);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    /* execute the store prodcedure */
    outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get("OUT_STATUS");
    String message = (String) outputs.get("OUT_MESSAGE");
    String lockObtained = (String) outputs.get("OUT_LOCK_OBTAINED");
    String lockCsrId = (String) outputs.get("OUT_LOCKED_CSR_ID");
    HashMap data = new HashMap();

    //if status = N, there is a DB error. 
    if (status != null && status.equalsIgnoreCase("N"))
    {
        throw new Exception(message);
    }
    else
    {
      data.put("lockObtained", lockObtained);
      data.put("lockCsrId", lockCsrId);
    }
    
    return data; 
  }


  public void releaseLock(String sessionId, String userId, String entityId, String entityType)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_CSR_LOCKED_ENTITIES_CRS");
    dataRequest.addInputParam("IN_SESSION_ID", sessionId);
    dataRequest.addInputParam("IN_CSR_ID", userId);
    dataRequest.addInputParam("IN_ENTITY_ID", entityId);
    dataRequest.addInputParam("IN_ENTITY_TYPE", entityType);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    String message = (String) outputs.get("OUT_MESSAGE");
    if (status != null && status.equals("N"))
    {
      // Do not throw exception because we do not want the application
      // to error out if the lock is not present to delete
      logger.debug(message);
    }
    logger.debug("Lock successfuly released: " + message);
  }

}
