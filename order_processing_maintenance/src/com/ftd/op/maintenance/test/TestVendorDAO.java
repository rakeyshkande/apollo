package com.ftd.op.maintenance.test;

import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.test.TestUtil;
import com.ftd.op.maintenance.vo.GlobalParameterVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.List;

import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestVendorDAO extends TestCase
{
  public TestVendorDAO(String name)
  {
    super(name);
  }

  private Connection conn = null;
  private String userId = null;
  private static Logger logger  = new Logger("com.ftd.op.maintenance.test.TestVendorDAO");

  public void setUp() throws SQLException
  {
    conn = TestUtil.getConnection();
    userId = "mkruger";
  }
  
  public void tearDown() throws SQLException
  {
    try
    {
      if(conn != null)
        conn.close();
    }
    catch(Throwable t)
    {
      logger.error("Error closing connection");
      logger.error(t);
      t.printStackTrace();
    }
  }

  public void testGetVendorMaster()
  {
    String testVendorId = null;
    String testVendorName = null;
    String testCutoffTime = null;
    VendorMasterVO vmVO = null;
    VendorDAO vDAO = new VendorDAO(conn);
    try 
    {
      //insert vendor master record
      testVendorName = "TEST VENDOR";
      testCutoffTime = "13:00";
      testVendorId = insertVendorMaster(testVendorName, testCutoffTime, "Y");
      
      //obtain vendor master information
      vmVO = vDAO.getVendorMaster(String.valueOf(testVendorId));

      //delete vendor master record
      deleteVendorMaster(testVendorId);
        
      //test that what was inserted matches what was retrieved
      if(!vmVO.getVendorID().equals(testVendorId)) 
        fail("Vendor id does not match for vendor id " + testVendorId);
      if(!vmVO.getVendorName().equals(testVendorName))
        fail("Vendor name does not match for vendor id " + testVendorId);
      if(!vmVO.getCutoff().equals(testCutoffTime))
        fail("Vendor cutoff time does not match for vendor id " + testVendorId);
    }
    catch(Exception e)
    {
      logger.error(e);
      fail("There should not be an exception thrown.");
    }
  }


  public void testGetVendorIds()
  {
    String testVendorIdActive = null;
    String testVendorIdInactive = null;
    List vendorIds = null;
    VendorDAO vDAO = new VendorDAO(conn);
    try 
    {
      //insert vendor master record active
      testVendorIdActive = insertVendorMaster("TEST VENDOR", "13:00", "Y");
        
      //insert vendor master record active
      testVendorIdInactive = insertVendorMaster("TEST VENDOR", "13:00", "N");
      
      //obtain vendor master information
      vendorIds = vDAO.getVendorIds();

      //delete vendor master records
      deleteVendorMaster(testVendorIdActive);
      deleteVendorMaster(testVendorIdInactive);

      //test that the active vendor id exists within vendorIds
      if(!vendorIds.contains(testVendorIdActive))
        fail("VendorIds does not contain the active vendor id " + testVendorIdActive);

      //test that the inactive vendor does not exist within vendorIds
      if(vendorIds.contains(testVendorIdInactive))
        fail("VendorIds contains the inactive vendor id " + testVendorIdInactive);
    }
    catch(Exception e)
    {
      logger.error(e);
      fail("There should not be an exception thrown.");
    }
  }

  
  public void testInsertVendorCutoff()
  {
    VendorDAO vDAO = new VendorDAO(conn);
    String testVendorId = null;
    String testCutoffTime = null;
    ResultSet rs = null;
    Statement stmt = null;
    
    try 
    {
      testCutoffTime = "21:30";
      
      //insert vendor master record active
      testVendorId = insertVendorMaster("TEST VENDOR", "13:30", "Y");

      //insert vendor cutoff
      vDAO.insertVendorCutoff(testVendorId, testCutoffTime);
        
      //obtain vendor cutoff information
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select cutoff_time from ftd_apps.vendor_master where vendor_id = '" + testVendorId + "'");
      String cutoffTime = null;
      if(rs.next())
        cutoffTime = rs.getString(1);
      else
        fail("Could not obtain cutoff time for vendor id " + testVendorId);
      rs.close();
      
      //delete vendor master record
      deleteVendorMaster(testVendorId);

      //test that the active vendor id exists within vendorIds
      if(!testCutoffTime.equals(cutoffTime))
        fail("Test cutoff time does not equal actual cutoff time for vendor id " + testVendorId);
    }
    catch(Exception e)
    {
      logger.error(e);
      fail("There should not be an exception thrown.");
    }
    finally
    {
      try
      {
        if(rs != null)
          rs.close();
        if(stmt != null)
          stmt.close();      
      }
      catch(Throwable t)
      {
        logger.error("Failed to close Statement or ResultSet");        
      }
    }
  }


  public void testSetGlobalParameters()
  {
    VendorDAO vDAO = new VendorDAO(conn);
    ResultSet rs = null;
    Statement stmt = null;
    String testContext = null;
    String testName = null;
    String testValue = null;
    
    try 
    {
      testContext = "test_context";
      testName = "test_name";
      testValue = "test_value";
      
      //insert global parameter
      vDAO.setGlobalParameter(testContext, testName, testValue, userId);
        
      //obtain global parameter information
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select count(*) from frp.global_parms where "
        + " context = '" + testContext + "'"
        + " AND name = '" + testName + "'"
        + " AND value = '" + testValue + "'");
      
      int count = 0;
      if(rs.next())
        count = rs.getInt(1);
      else
        fail("Could not obtain global parameter information");
      rs.close();

      //delete global parameter
      int result = stmt.executeUpdate("delete from frp.global_parms where "
        + " context = '" + testContext + "'"
        + " AND name = '" + testName + "'"
        + " AND value = '" + testValue + "'");
      if(result != 1)
        fail("Could not delete "
          + "\nContext: " + testContext
          + "\nName: " + testName
          + "\nValue: " + testValue);

      //test that inserted global parameter exists
      if(count == 0)
        fail("Global parameter does not exist."
          + "\nContext: " + testContext
          + "\nName: " + testName
          + "\nValue: " + testValue);
    }
    catch(Exception e)
    {
      logger.error(e);
      fail("There should not be an exception thrown.");
    }
    finally
    {
      try
      {
        if(rs != null)
          rs.close();
        if(stmt != null)
          stmt.close();      
      }
      catch(Throwable t)
      {
        logger.error("Failed to close Statement or ResultSet");        
      }
    }
  }


  public void testGetGlobalParameters()
  {
    VendorDAO vDAO = new VendorDAO(conn);
    Statement stmt = null;
    String testContext = null;
    String testName = null;
    String testValue = null;
    int result = 0;
    
    try 
    {
      testContext = "test_context";
      testName = "test_name";
      testValue = "test_value";
      
      //insert global parameter
      stmt = conn.createStatement();
      result = stmt.executeUpdate("insert into frp.global_parms (context, name, value, created_by, updated_by) values ("
        + "'" + testContext + "',"
        + "'" + testName + "',"
        + "'" + testValue + "',"
        + "'" + userId + "',"
        + "'" + userId + "')");
      if(result != 1)
        fail("Could not insert global parameter");
        
      //obtain global parameter information
      GlobalParameterVO gpVO = vDAO.getGlobalParameter(testContext, testName);
            
      //delete global parameter
      result = stmt.executeUpdate("delete from frp.global_parms where "
        + " context = '" + testContext + "'"
        + " AND name = '" + testName + "'"
        + " AND value = '" + testValue + "'");
      if(result != 1)
        fail("Could not delete "
          + "\nContext: " + testContext
          + "\nName: " + testName
          + "\nValue: " + testValue);

      //test that inserted global parameter exists
      if(!testValue.equals(gpVO.getValue()))  
        fail("Test global parameter value does not match actual."
          + "\nContext: " + testContext
          + "\nName: " + testName
          + "\nValue: " + testValue);
    }
    catch(Exception e)
    {
      logger.error(e);
      fail("There should not be an exception thrown.");
    }
    finally
    {
      try
      {
        if(stmt != null)
          stmt.close();      
      }
      catch(Throwable t)
      {
        logger.error("Failed to close Statement or ResultSet");        
      }
    }
  }


  private String insertVendorMaster(String vendorName, String cutoffTime, String active)
    throws Exception
  {
    Statement stmt = null;
    ResultSet rs = null;
    String vendorId = null;
    
    try
    {
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select max(vendor_id) from ftd_apps.vendor_master");
      if(rs.next())
        vendorId = String.valueOf( (rs.getInt(1) + 1) );
      else
        fail("Could not obtain unique vendor id.");
      rs.close();

      String insert = "insert into FTD_APPS.VENDOR_MASTER "
        + "(VENDOR_ID, VENDOR_NAME, DEFAULT_CARRIER, MEMBER_NUMBER, VENDOR_TYPE,"
        + "ACTIVE, ADDRESS1, ADDRESS2, CITY, STATE, ZIP_CODE, LEAD_DAYS, COMPANY"
        + ", GENERAL_INFO, CUTOFF_TIME, ACCOUNTS_PAYABLE_ID, GL_ACCOUNT_NUMBER)"
        + " values ('" + vendorId + "', '" + vendorName + "', null, null, 'ESCALATE',"
        + "'" + active + "', null, null, null, null, null, null, null, null, '" + cutoffTime + "', 'N/A', 'N/A')";
        int result = stmt.executeUpdate(insert);
        if(result != 1)
          fail("Vendor Master insert failed for vendor id " + vendorId);
          
        return vendorId;
    }
    finally
    {
      if(rs != null)
        rs.close();
      if(stmt != null)
        stmt.close();
    }
  }


  private void deleteVendorMaster(String vendorId)
    throws Exception
  {
    Statement stmt = null;
    
    try
    {
      stmt = conn.createStatement();
      int result = stmt.executeUpdate("delete from ftd_apps.vendor_master where vendor_id = '" + vendorId + "'");
      if(result != 1)
        fail("Vendor Master delete failed for vendor id " + vendorId);
    }
    finally
    {
      if(stmt != null)
        stmt.close();
    }
  }




  public static TestSuite suite()
  {
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(TestVendorDAO.class);        
    } 
    catch (Exception e) 
    {
      System.out.println(e.toString());
      e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
}