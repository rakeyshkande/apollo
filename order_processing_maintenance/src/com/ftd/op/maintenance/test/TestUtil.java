package com.ftd.op.maintenance.test;

import com.ftd.op.maintenance.vo.ServiceFeeVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.text.DecimalFormat;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
//import junit.framework.TestCase;

public class TestUtil //extends TestCase
{
  public TestUtil()//String name)
  {
    //super(name);
  }
  
  public static void main (String args[]) throws Exception {
        ServiceFeeVO sfVO = new ServiceFeeVO();
        sfVO.setServiceFeeId("01");
        sfVO.setDomesticFee(12.99);
        sfVO.setInternationalFee(15.99);
        String xml = createServiceFeeXML(sfVO, "delete");
        System.out.println(xml);
  }
  
  public static Connection getConnection() throws SQLException
  {
    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
    Connection conn = DriverManager.getConnection ("jdbc:oracle:thin:@adonis.ftdi.com:1522:dev5", "mkruger", "mkruger");
    
    return conn;
  }
  
    private static String createServiceFeeXML(ServiceFeeVO sfVO, String action) throws Exception
    {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("0.00");

        XMLDocument doc = new XMLDocument();
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "serviceFee", "");
        doc.appendChild(root);
        
        Element newElement = JAXPUtil.buildSimpleXmlNode(doc,"action", action);
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"id", sfVO.getServiceFeeId());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"domesticFee", Double.toString(sfVO.getDomesticFee()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"internationalFee", Double.toString(sfVO.getInternationalFee()));
        root.appendChild(newElement);

        return JAXPUtil.toString(root);
    }
  
}
