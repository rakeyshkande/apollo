package com.ftd.op.maintenance.test;

import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.test.TestUtil;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.vo.GlobalParameterVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestVendorUtil extends TestCase
{
  public TestVendorUtil(String name)
  {
    super(name);
  }

  private Connection conn = null;
  private String userId = null;
  private static Logger logger  = new Logger("com.ftd.op.maintenance.test.TestVendorUtil");

  public void setUp() throws SQLException
  {
    conn = TestUtil.getConnection();
    userId = "mkruger";
  }
  
  public void tearDown() throws SQLException
  {
    try
    {
      if(conn != null)
        conn.close();
    }
    catch(Throwable t)
    {
      logger.error("Error closing connection");
      logger.error(t);
      t.printStackTrace();
    }
  }

  public void testDeleteVendorBlock()
  {
    VendorUtil vu = new VendorUtil();
    VendorBlockVO vbVO = null;
    Statement stmt = null;
    ResultSet rs = null;
    int result = 0;
    String vendorId = null;
    String startDateStr = null;
    String endDateStr = null;
    String globalFlag = null;
    Date startDate = null;
    
    try
    {
      stmt = conn.createStatement();
      
      globalFlag = "N";
      
      //obtain a vendor id
      vendorId = insertVendorMaster("test_vendor", "13:00", "Y");
      
      //obtain start and end dates
      HashMap dates = getStartAndEndDates();
      startDate = (Date)dates.get("startDate");
      startDateStr = (String)dates.get("startDateStr");
      endDateStr = (String)dates.get("endDateStr");
      
      //insert vendor shipping block
      stmt.executeUpdate("insert into ftd_apps.vendor_shipping_restrictions"
        + "(VENDOR_ID, START_DATE, END_DATE, GLOBAL_FLAG)"
        + " VALUES("
        + "'" + vendorId + "',"
        + "" + startDateStr + ","
        + "" + endDateStr + ","
        + "'" + globalFlag + "'"
        + ")");
      
      //insert vendor delivery block
      stmt.executeUpdate("insert into ftd_apps.vendor_delivery_restrictions"
        + "(VENDOR_ID, START_DATE, END_DATE, GLOBAL_FLAG)"
        + " VALUES("
        + "'" + vendorId + "',"
        + "" + startDateStr + ","
        + "" + endDateStr + ","
        + "'" + globalFlag + "'"
        + ")");

      //delete vendor blocks
      vbVO = new VendorBlockVO();
      vbVO.setVendorId(vendorId);
      vbVO.setBlockType("shipping");
      vbVO.setStartDate(startDate);
      vu.deleteVendorBlock(vbVO, conn);
      vbVO.setBlockType("delivery");
      vu.deleteVendorBlock(vbVO, conn);

      //see if vendor_shipping_restrictions still exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_shipping_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
        
      if(result != 0)
        fail("Shipping vendor block was not successfully deleted.");
      
      //see if vendor_delivery_restrictions still exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_delivery_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
      
      if(result != 0)
        fail("Delivery vendor block was not successfully deleted.");

      //delete vendor master record
      deleteVendorMaster(vendorId);
    }
    catch(Throwable t)
    {
      logger.error(t);
      t.printStackTrace();
      fail("No error should occur.");
    }
  }

  
  /*  Unable to obtain transaction with standalone JUnit test.
   *  The test will work if you use a DataSource as the connection.
   *  Will be tested manually as not to complicate the JUnit test suite.
  public void testDeleteVendorBlockSendNovator()
  {
    VendorUtil vu = new VendorUtil();
    VendorBlockVO vbVO = null;
    Statement stmt = null;
    ResultSet rs = null;
    int result = 0;
    String vendorId = null;
    String startDateStr = null;
    String endDateStr = null;
    String globalFlag = null;
    Date startDate = null;
    
    try
    {
      stmt = conn.createStatement();
      
      globalFlag = "N";
      
      //obtain a vendor id
      vendorId = insertVendorMaster("test_vendor", "13:00", "Y");
      
      //obtain start and end dates
      HashMap dates = getStartAndEndDates();
      startDate = (Date)dates.get("startDate");
      startDateStr = (String)dates.get("startDateStr");
      endDateStr = (String)dates.get("endDateStr");
      
      //insert vendor shipping block
      stmt.executeUpdate("insert into ftd_apps.vendor_shipping_restrictions"
        + "(VENDOR_ID, START_DATE, END_DATE, GLOBAL_FLAG)"
        + " VALUES("
        + "'" + vendorId + "',"
        + "" + startDateStr + ","
        + "" + endDateStr + ","
        + "'" + globalFlag + "'"
        + ")");
      
      //insert vendor delivery block
      stmt.executeUpdate("insert into ftd_apps.vendor_delivery_restrictions"
        + "(VENDOR_ID, START_DATE, END_DATE, GLOBAL_FLAG)"
        + " VALUES("
        + "'" + vendorId + "',"
        + "" + startDateStr + ","
        + "" + endDateStr + ","
        + "'" + globalFlag + "'"
        + ")");

      //delete vendor blocks
      vbVO = new VendorBlockVO();
      vbVO.setVendorId(vendorId);
      vbVO.setBlockType("shipping");
      vbVO.setStartDate(startDate);
      vu.deleteVendorBlockSendNovator(vbVO, conn);
      vbVO.setBlockType("delivery");
      vu.deleteVendorBlockSendNovator(vbVO, conn);

      //see if vendor_shipping_restrictions still exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_shipping_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
        
      if(result != 0)
        fail("Shipping vendor block was not successfully deleted.");
      
      //see if vendor_delivery_restrictions still exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_delivery_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
      
      if(result != 0)
        fail("Delivery vendor block was not successfully deleted.");

      //delete vendor master record
      deleteVendorMaster(vendorId);
    }
    catch(Throwable t)
    {
      logger.error(t);
      t.printStackTrace();
      fail("No error should occur.");
    }
  }
  */

  public void testInsertVendorBlock()
  {
    VendorUtil vu = new VendorUtil();
    VendorBlockVO vbVO = null;
    Statement stmt = null;
    ResultSet rs = null;
    int result = 0;
    String vendorId = null;
    String startDateStr = null;
    String endDateStr = null;
    String globalFlag = null;
    Date startDate = null;
    Date endDate = null;
    
    try
    {
      stmt = conn.createStatement();
      
      globalFlag = "N";
      
      //obtain a vendor id
      vendorId = insertVendorMaster("test_vendor", "13:00", "Y");
      
      //obtain start and end dates
      HashMap dates = getStartAndEndDates();
      startDate = (Date)dates.get("startDate");
      endDate = (Date)dates.get("endDate");
      startDateStr = (String)dates.get("startDateStr");
      endDateStr = (String)dates.get("endDateStr");
      
      //insert vendor blocks
      vbVO = new VendorBlockVO();
      vbVO.setVendorId(vendorId);
      vbVO.setBlockType("shipping");
      vbVO.setStartDate(startDate);
      vbVO.setEndDate(endDate);
      vbVO.setGlobalFlag(globalFlag);
      vu.insertVendorBlock(vbVO, conn);
      vbVO.setBlockType("delivery");
      vu.insertVendorBlock(vbVO, conn);

      //see if vendor_shipping_restrictions exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_shipping_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
        
      if(result == 0)
        fail("Shipping vendor does not exist.");
      
      //see if vendor_delivery_restrictions exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_delivery_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
      
      if(result == 0)
        fail("Shipping vendor does not exist.");

      //delete vendor shipping block
      stmt.executeUpdate("delete from ftd_apps.vendor_shipping_restrictions"
        + " WHERE vendor_id = '" + vendorId + "'"
        + " AND start_date = " + startDateStr
        + " AND end_date = " + endDateStr
        + " AND global_flag = '" + globalFlag + "'");
      
      //delete vendor delivery block
      stmt.executeUpdate("delete from ftd_apps.vendor_delivery_restrictions"
        + " WHERE vendor_id = '" + vendorId + "'"
        + " AND start_date = " + startDateStr
        + " AND end_date = " + endDateStr
        + " AND global_flag = '" + globalFlag + "'");

      //delete vendor master record
      deleteVendorMaster(vendorId);
    }
    catch(Throwable t)
    {
      logger.error(t);
      t.printStackTrace();
      fail("No error should occur.");
    }
  }


  /*  Unable to obtain transaction with standalone JUnit test.
   *  The test will work if you use a DataSource as the connection.
   *  Will be tested manually as not to complicate the JUnit test suite.
  public void testInsertVendorBlockSendNovator()
  {
    VendorUtil vu = new VendorUtil();
    VendorBlockVO vbVO = null;
    Statement stmt = null;
    ResultSet rs = null;
    int result = 0;
    String vendorId = null;
    String startDateStr = null;
    String endDateStr = null;
    String globalFlag = null;
    Date startDate = null;
    Date endDate = null;
    
    try
    {
      stmt = conn.createStatement();
      
      globalFlag = "N";
      
      //obtain a vendor id
      vendorId = insertVendorMaster("test_vendor", "13:00", "Y");
      
      //obtain start and end dates
      HashMap dates = getStartAndEndDates();
      startDate = (Date)dates.get("startDate");
      endDate = (Date)dates.get("endDate");
      startDateStr = (String)dates.get("startDateStr");
      endDateStr = (String)dates.get("endDateStr");
      
      //insert vendor blocks
      vbVO = new VendorBlockVO();
      vbVO.setVendorId(vendorId);
      vbVO.setBlockType("shipping");
      vbVO.setStartDate(startDate);
      vbVO.setEndDate(endDate);
      vbVO.setGlobalFlag(globalFlag);
      vu.insertVendorBlockSendNovator(vbVO, conn);
      vbVO.setBlockType("delivery");
      vu.insertVendorBlockSendNovator(vbVO, conn);

      //see if vendor_shipping_restrictions exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_shipping_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
        
      if(result == 0)
        fail("Shipping vendor does not exist.");
      
      //see if vendor_delivery_restrictions exists
      rs = stmt.executeQuery("select count(*) from ftd_apps.vendor_delivery_restrictions where vendor_id = " 
        + "'" + vendorId + "'"
        + " AND start_date = " + startDateStr);
      if(rs.next())
        result = rs.getInt(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
      
      if(result == 0)
        fail("Shipping vendor does not exist.");

      //delete vendor shipping block
      stmt.executeUpdate("delete from ftd_apps.vendor_shipping_restrictions"
        + " WHERE vendor_id = '" + vendorId + "'"
        + " AND start_date = " + startDateStr
        + " AND end_date = " + endDateStr
        + " AND global_flag = '" + globalFlag + "'");
      
      //delete vendor delivery block
      stmt.executeUpdate("delete from ftd_apps.vendor_delivery_restrictions"
        + " WHERE vendor_id = '" + vendorId + "'"
        + " AND start_date = " + startDateStr
        + " AND end_date = " + endDateStr
        + " AND global_flag = '" + globalFlag + "'");

      //delete vendor master record
      deleteVendorMaster(vendorId);
    }
    catch(Throwable t)
    {
      logger.error(t);
      t.printStackTrace();
      fail("No error should occur.");
    }
  }
  */

  public void testUpdateVendorCutoff()
  {
    VendorUtil vu = new VendorUtil();
    Statement stmt = null;
    ResultSet rs = null;
    String vendorId = null;
    String testCutoffTime = "23:00";
    String cutoffTime = null;
    
    try
    {
      stmt = conn.createStatement();
      
      //obtain a vendor id
      vendorId = insertVendorMaster("test_vendor", "13:00", "Y");
      
      //insert vendor blocks
      vu.updateVendorCutoff(vendorId, testCutoffTime, conn);

      //see if vendor_shipping_restrictions exists
      rs = stmt.executeQuery("select cutoff_time from ftd_apps.vendor_master where vendor_id = " 
        + "'" + vendorId + "'");
      if(rs.next())
        cutoffTime = rs.getString(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
        
      if(!testCutoffTime.equals(cutoffTime))
        fail("Test cutoff time does not equal actual cutoff time.");

      //delete vendor master record
      deleteVendorMaster(vendorId);
    }
    catch(Throwable t)
    {
      logger.error(t);
      t.printStackTrace();
      fail("No error should occur.");
    }
  }


  /*  Unable to obtain transaction with standalone JUnit test.
   *  The test will work if you use a DataSource as the connection.
   *  Will be tested manually as not to complicate the JUnit test suite.
  public void testUpdateVendorCutoffSendNovator()
  {
    VendorUtil vu = new VendorUtil();
    Statement stmt = null;
    ResultSet rs = null;
    String vendorId = null;
    String testCutoffTime = "23:00";
    String cutoffTime = null;
    
    try
    {
      stmt = conn.createStatement();
      
      //obtain a vendor id
      vendorId = insertVendorMaster("test_vendor", "13:00", "Y");
      
      //insert vendor blocks
      vu.updateVendorCutoffSendNovator(vendorId, testCutoffTime, conn);

      //see if vendor_shipping_restrictions exists
      rs = stmt.executeQuery("select cutoff_time from ftd_apps.vendor_master where vendor_id = " 
        + "'" + vendorId + "'");
      if(rs.next())
        cutoffTime = rs.getString(1);
      else
        fail("Could not obtain vendor_shipping_restrictions count");
        
      if(!testCutoffTime.equals(cutoffTime))
        fail("Test cutoff time does not equal actual cutoff time.");

      //delete vendor master record
      deleteVendorMaster(vendorId);
    }
    catch(Throwable t)
    {
      logger.error(t);
      t.printStackTrace();
      fail("No error should occur.");
    }
  }
  */

  private HashMap getStartAndEndDates()
    throws Exception
  {
    HashMap dates = new HashMap();
    
    //set start and end date
    SimpleDateFormat sdf = new SimpleDateFormat ( "yyyy/MM/dd HH:mm:ss" );
    GregorianCalendar cal = new GregorianCalendar();
    cal.add(Calendar.HOUR, 24);
    String startDateStr = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + " 00:00:00";
    cal.add(Calendar.HOUR, 24);
    String endDateStr = cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + " 00:00:00";
    
    dates.put("startDate", sdf.parse(startDateStr));
    dates.put("endDate", sdf.parse(endDateStr));
    dates.put("startDateStr", "to_date('" + startDateStr + "', 'yyyy/mm/dd hh24:mi:ss')");
    dates.put("endDateStr", "to_date('" + endDateStr + "', 'yyyy/mm/dd hh24:mi:ss')");
    
    return dates;
  }


  private String insertVendorMaster(String vendorName, String cutoffTime, String active)
    throws Exception
  {
    Statement stmt = null;
    ResultSet rs = null;
    String vendorId = null;
    
    try
    {
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select max(vendor_id) from ftd_apps.vendor_master");
      if(rs.next())
        vendorId = String.valueOf( (rs.getInt(1) + 1) );
      else
        fail("Could not obtain unique vendor id.");
      rs.close();

      String insert = "insert into FTD_APPS.VENDOR_MASTER "
        + "(VENDOR_ID, VENDOR_NAME, DEFAULT_CARRIER, MEMBER_NUMBER, VENDOR_TYPE,"
        + "ACTIVE, ADDRESS1, ADDRESS2, CITY, STATE, ZIP_CODE, LEAD_DAYS, COMPANY"
        + ", GENERAL_INFO, CUTOFF_TIME, ACCOUNTS_PAYABLE_ID, GL_ACCOUNT_NUMBER)"
        + " values ('" + vendorId + "', '" + vendorName + "', null, null, 'ESCALATE',"
        + "'" + active + "', null, null, null, null, null, null, null, null, '" + cutoffTime + "', 'N/A', 'N/A')";
        int result = stmt.executeUpdate(insert);
        if(result != 1)
          fail("Vendor Master insert failed for vendor id " + vendorId);
          
        return vendorId;
    }
    finally
    {
      if(rs != null)
        rs.close();
      if(stmt != null)
        stmt.close();
    }
  }


  private void deleteVendorMaster(String vendorId)
    throws Exception
  {
    Statement stmt = null;
    
    try
    {
      stmt = conn.createStatement();
      int result = stmt.executeUpdate("delete from ftd_apps.vendor_master where vendor_id = '" + vendorId + "'");
      if(result != 1)
        fail("Vendor Master delete failed for vendor id " + vendorId);
    }
    finally
    {
      if(stmt != null)
        stmt.close();
    }
  }


  public static TestSuite suite()
  {
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(TestVendorUtil.class);        
    } 
    catch (Exception e) 
    {
      System.out.println(e.toString());
      e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
}