package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.LockUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.AddOnUtility;

import com.ftd.osp.utilities.vo.AddOnVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.Document;

public class AddOnDashboardBO
{
    private static Logger logger = new Logger("com.ftd.op.maintenance.action.AddOnDashboardBO");
    
    /**
     * Controls the functionality to load the dashboard
     * @param responseMap returned data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */
    
    public void processLoad(HashMap responseMap, HashMap pageData, Connection con)
        throws Exception
    {
        AddOnUtility aoUtil = new AddOnUtility();
        HashMap<String, ArrayList<AddOnVO>> addOnsHash = aoUtil.getAllAddonList(false, true, con);
        Document addOnsXML = aoUtil.convertAddOnMapToXML(addOnsHash);

        responseMap.put("HK_add_ons", addOnsXML);
        pageData.put("forwardToType", "XSL");
        pageData.put("forwardToName", "dashboard");

        return;
    }

    /**
     * Controls the functionality to bring up the edit screen
     * @param inputMap special data for this action
     * @param responseMap returned data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */
    
    public void processEdit(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
        throws Exception
    {
        String addOnId = (String) inputMap.get("requestAddOnId");
        String securityToken = (String) inputMap.get("requestSecurityToken");
        String csrId = (String) inputMap.get("csrId");
        String lockedBy = retrieveLock(addOnId, securityToken, csrId, con);

        if (lockedBy == null || lockedBy.equalsIgnoreCase(""))
        {
            pageData.put("forwardToType", "ACTION");
            pageData.put("forwardToName", "maintenance");
        }
        else
        {
            pageData.put("errorMessage", 
                         "This add-on is in a locked state. You cannot view or save this add-on at this time.");
            processLoad(responseMap, pageData, con);
        }

        return;
    }


    /**
     * Controls the functionality to return to the main menu
     * @param pageData common data for pages
     * @throws Exception
     */
    
    public void processMainMenu(HashMap pageData)
        throws Exception
    {
        pageData.put("forwardToType", "ACTION");
        pageData.put("forwardToName", "MainMenuAction");

        return;
    }

    /**
     * Release a lock on a resource
     * @param entityId entity to lock
     * @param sessionId session id
     * @param csrId csr id
     * @param con database connection
     * @return
     * @throws Exception
     */
    
    private String retrieveLock(String entityId, String sessionId, String csrId, Connection con)
        throws Exception
    {
        String lockedBy = LockUtil.getLock(con, OPMConstants.ADD_ON_ENTITY_TYPE, entityId, sessionId, csrId);
        return lockedBy;
    }
}
