package com.ftd.op.maintenance.bo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.BaseProductMappingVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

public class ProductMappingBO {
	
	private static Logger logger = new Logger("com.ftd.op.maintenance.bo.ProductMappingBO");
	public void deleteProductMapping(String origProductId) throws Exception
	{
		Connection conn = null;
		try{
			
			if(StringUtils.isEmpty(origProductId))
			{
                throw new Exception(OPMConstants.FLORAL_PRODUCT_NOT_NULL_MSG);
			}
			//Connection/Database info        
            conn = CommonUtil.getDBConnection();
            MaintenanceDAO maintenanceDAO = new MaintenanceDAO(conn);
            maintenanceDAO.deleteProductMapping(origProductId.toUpperCase());
		}catch (Exception e) {
			logger.error("Error occured while deleting phoenix product mapping", e);
			throw e;
		}finally{
			CommonUtil.closeConnection(conn);
		}
	}

    /**
     * Method to get Phoenix mapping for a particular product.
     * 
     * Note, we should be able to just return list of BaseProductMappingVO since only one row returned from query,
     * but returning map is easier for front-end to work with (and is also consistent with getProductMappings).
     * 
     * @param productId
     * @return
     * @throws Exception
     */
    public Map<String, List<BaseProductMappingVO>> getProductMappingByFloristProductId(String productId) throws Exception
    {
        Connection conn = null;
        boolean userError = false;
        Map<String, List<BaseProductMappingVO>> productMappingHash = new TreeMap<String, List<BaseProductMappingVO>>();
        try{
            if(StringUtils.isEmpty(productId))
            {
                userError = true;
                throw new Exception(OPMConstants.FLORAL_PRODUCT_NOT_NULL_MSG);
            }
            String uProductId = productId.toUpperCase();

            //Connection/Database info        
            conn = CommonUtil.getDBConnection();
            MaintenanceDAO maintenanceDAO = new MaintenanceDAO(conn);
            CachedResultSet rs = maintenanceDAO.retrieveProductMappingByFloristProduct(uProductId);
            if (rs == null) {
                userError = true;
                throw new Exception(OPMConstants.FLORAL_PRODUCT_NOT_IN_PDB_MSG);
            }
            if (rs.next())  // Should only be one match
            {
                String shipMethodCarrier = rs.getString("SHIP_METHOD_CARRIER");
                String shipMethodFlorist = rs.getString("SHIP_METHOD_FLORIST");
                String addonFlag = rs.getString("ADDON_FLAG");
                String goodPrice = rs.getString("GOOD");
                String betterPrice = rs.getString("BETTER");
                String bestPrice = rs.getString("BEST");
                String productType = rs.getString("PRODUCT_TYPE");
                String pricePointMappings = rs.getString("PRICE_POINT_MAPPINGS");
    
                if (logger.isDebugEnabled()) {
                    logger.debug("Mapping for product: " + uProductId + " product_type: " + productType +
                                 " ship_method_carrier/florist: " + shipMethodCarrier + "/" + shipMethodFlorist +
                                 " price G/B/B: " + goodPrice + "/" + betterPrice + "/" + bestPrice +
                                 " price_point_mappings: " + pricePointMappings);
                }

                if (rs.getString("PRODUCT_ID") == null) {
                    userError = true;
                    throw new Exception(OPMConstants.FLORAL_PRODUCT_NOT_IN_PDB_MSG);
                }

                if(goodPrice==null || Double.parseDouble(goodPrice) <= 0){
                    goodPrice = null;
                }
                if(betterPrice==null || Double.parseDouble(betterPrice) <= 0){
                    betterPrice = null;
                }
                if(bestPrice==null || Double.parseDouble(bestPrice) <= 0){
                    bestPrice = null;
                }
                if("Y".equalsIgnoreCase(addonFlag)){
                    // Only use goodPrice - set others to null 
                    betterPrice = null;
                    bestPrice = null;
                }

                // If floral product settings don't match criteria for use in Phoenix mapping, then bail out
                String validationMsg = checkFloralProductCriteria(productType, shipMethodCarrier, shipMethodFlorist, goodPrice, betterPrice, bestPrice);
                if (validationMsg != null) {
                    userError = true;
                    throw new Exception(validationMsg);
                }
                
                productMappingHash.put(uProductId, parsePricePointDataAndMapToGbb(pricePointMappings, addonFlag, goodPrice, betterPrice, bestPrice));
            
            } else {
                userError = true;
                throw new Exception(OPMConstants.FLORAL_PRODUCT_NOT_IN_PDB_MSG);
            }
        }catch (Exception e) {
            if (!userError) {  // Only log if not user error
                logger.error("Error occured while getting phoenix product mapping", e);
            }
            throw e;
        }finally{
            CommonUtil.closeConnection(conn);
        }        
        return productMappingHash;
    }

    
    
	/**
	 * Method to get all Phoenix product mappings
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<BaseProductMappingVO>> getProductMappings() throws Exception
	{
		Connection conn = null;
		Map<String, List<BaseProductMappingVO>> productMappingHash = new TreeMap<String, List<BaseProductMappingVO>>();
		try{
			//Connection/Database info        
            conn = CommonUtil.getDBConnection();
            MaintenanceDAO maintenanceDAO = new MaintenanceDAO(conn);
			CachedResultSet rs = maintenanceDAO.retrieveProductMappings();
			String floralSKU = null;
			String pricePoints = null;
			String addonFlag = null;
			while (rs.next())
		    {
				floralSKU = rs.getString("original_product_id");
				pricePoints = rs.getString("price_points");
				addonFlag = rs.getString("addon_flag");
				if(!StringUtils.isEmpty(floralSKU) && !StringUtils.isEmpty(pricePoints)){
					productMappingHash.put(floralSKU, parsePricePointDataAndMapToGbb(pricePoints, addonFlag, null, null, null));
				}
		    }
			
		}catch (Exception e) {
			logger.error("Error occured while getting phoenix product mapping", e);
			throw e;
		}finally{
			CommonUtil.closeConnection(conn);
		}
		return productMappingHash;
	}

	
	
	/**
	 * Method to add (or update) a Phoenix product mapping entry
	 * @param origProductId
	 * @param newProductsHash
	 * @param csrId
	 * @return
	 * @throws Exception
	 */
	public List<String> addProductMapping(String origProductId, List<BaseProductMappingVO> newProductsHash, String csrId) 
	throws Exception
	{
		Connection conn = null;
		boolean error_addonMustBeVendor = false;
		boolean error_invalidVendorProduct = false;
		boolean error_vendorProductNotInPdb = false;
		boolean error_vendorProductNotNull = false;
		boolean addonFlag = false;
		List<String> validationList = new ArrayList<String>();

		try{
			if(StringUtils.isEmpty(origProductId))
			{
				validationList.add(OPMConstants.FLORAL_PRODUCT_NOT_NULL_MSG);
				return validationList;
			}
			String uProductId = origProductId.toUpperCase();

            //Connection/Database info        
            conn = CommonUtil.getDBConnection();
            MaintenanceDAO maintenanceDAO = new MaintenanceDAO(conn);
            
            // Get info about original floral product and any existing mappings
            //
            CachedResultSet rs = maintenanceDAO.retrieveProductMappingByFloristProduct(uProductId);
            if(rs == null){
                validationList.add(OPMConstants.FLORAL_PRODUCT_NOT_IN_PDB_MSG);
                return validationList;
            }
            if (rs.next())  // Should only be one match
            {
                String shipMethodCarrier = rs.getString("SHIP_METHOD_CARRIER");
                String shipMethodFlorist = rs.getString("SHIP_METHOD_FLORIST");
                String addon = rs.getString("ADDON_FLAG");
                String goodPrice = rs.getString("GOOD");
                String betterPrice = rs.getString("BETTER");
                String bestPrice = rs.getString("BEST");
                String productType = rs.getString("PRODUCT_TYPE");
                String pricePointMappings = rs.getString("PRICE_POINT_MAPPINGS");
                if ("Y".equalsIgnoreCase(addon)) {
                    addonFlag = true;
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Mapping for product: " + uProductId + " product_type: " + productType +
                                 " ship_method_carrier/florist: " + shipMethodCarrier + "/" + shipMethodFlorist +
                                 " price G/B/B: " + goodPrice + "/" + betterPrice + "/" + bestPrice +
                                 " price_point_mappings: " + pricePointMappings);
                }

                if(goodPrice==null || Double.parseDouble(goodPrice) <= 0){
                    goodPrice = null;
                }
                if(betterPrice==null || Double.parseDouble(betterPrice) <= 0){
                    betterPrice = null;
                }
                if(bestPrice==null || Double.parseDouble(bestPrice) <= 0){
                    bestPrice = null;
                }
                if(addonFlag){
                    // Only use goodPrice - set others to null 
                    betterPrice = null;
                    bestPrice = null;
                }
                
                // If floral product settings don't match criteria for use in Phoenix mapping, then bail out
                //
                String validationMsg = checkFloralProductCriteria(productType, shipMethodCarrier, shipMethodFlorist, goodPrice, betterPrice, bestPrice);
                if (validationMsg != null) {
                    validationList.add(validationMsg);
                    return validationList;
                }
                            
                // Now check vendor products (that original product is being mapped to)
                //
                if (newProductsHash == null) {
                    validationList.add(OPMConstants.VENDOR_PRODUCT_NOT_NULL_MSG);
                    return validationList;
                }
                Iterator<BaseProductMappingVO> it = newProductsHash.iterator();
                if (!it.hasNext()) {
                    validationList.add(OPMConstants.VENDOR_PRODUCT_NOT_NULL_MSG);
                    return validationList;                    
                }
                while(it.hasNext()) {                         // Loop over the three entries (Good/Better/Best)
                    BaseProductMappingVO bpmVo = it.next();
                    if (bpmVo.getProductPricePointName() == null) {
                        // A null price point means front-end wants us to ignore this entry, so just continue
                        continue;
                    }
                    String vprod[] = StringUtils.split(bpmVo.getVendorSKUs());  // Split string into array of entered vendor products
                    if ((vprod == null) || (vprod.length == 0)) {
                        error_vendorProductNotNull = true;                            
                        continue;
                    }
                    for (int i=0; i < vprod.length; i++) {   // Loop and check each vendor product
                        String isMappable = maintenanceDAO.isProductVendorMappable(vprod[i], addonFlag); 
                        if ("Y".equals(isMappable)) {
                            continue;
                        } else if ("N".equals(isMappable)) {
                            if (addonFlag) {
                                error_addonMustBeVendor = true;
                                if (logger.isDebugEnabled()) {
                                    logger.debug(OPMConstants.ADDON_MUST_BE_VENDOR_MSG + " - " + vprod[i]);
                                }
                            } else {
                                error_invalidVendorProduct = true;
                                if (logger.isDebugEnabled()) {
                                    logger.debug(OPMConstants.INVALID_VENDOR_PRODUCT_ERROR_MSG + " - " + vprod[i]);
                                }
                            }
                        } else {
                            error_vendorProductNotInPdb = true;
                            if (logger.isDebugEnabled()) {
                                logger.debug(OPMConstants.VENDOR_PRODUCT_NOT_IN_PDB_MSG + " - " + vprod[i]);
                            }
                        }                    
                    } // end for
                }
                
                if (error_addonMustBeVendor) {
                    validationList.add(OPMConstants.ADDON_MUST_BE_VENDOR_MSG);
                }
                if (error_invalidVendorProduct) {
                    validationList.add(OPMConstants.INVALID_VENDOR_PRODUCT_ERROR_MSG);                
                }
                if (error_vendorProductNotInPdb) {
                    validationList.add(OPMConstants.VENDOR_PRODUCT_NOT_IN_PDB_MSG);
                }
                if (error_vendorProductNotNull) {
                    validationList.add(OPMConstants.VENDOR_PRODUCT_NOT_NULL_MSG);
                }
                if (validationList.size() > 0) {
                    return validationList;
                }

				// If we survived to here, then save mappings to database.
                //
				// But first we get rid of any existing entries for product. 
                // We do this instead of just updating since some price points for the product may no longer exist
                // and wouldn't have been displayed on front-end, so we wouldn't know to remove them. 
                // By removing all first, we don't have to worry.
                //
                maintenanceDAO.deleteProductMapping(origProductId);
				it = newProductsHash.iterator();
				while(it.hasNext()) {
					BaseProductMappingVO bpmVo = it.next(); 
                    if (bpmVo.getProductPricePointName() != null) {
                        // Only save if price point is not null (since null price point means front-end wants us to ignore this entry)
                        maintenanceDAO.addProductMapping(uProductId, bpmVo.getVendorSKUs(), csrId, bpmVo.getProductPricePointName());
                    }
				}
			}      
		}catch (Exception e) {
			logger.error("Error occured while adding phoenix product mapping", e);
			throw e;
		}finally{
			CommonUtil.closeConnection(conn);
		}
		return null;
	}

	
    /** 
     * Private method to check if settings for a floral product meet criteria necessary for use in Phoenix mapping.
     * Returns null if criteria met, otherwise returns string indicating why it's not.
     * 
     * @param productType
     * @param shipMethodCarrier
     * @param shipMethodFlorist
     * @return
     */
    private String checkFloralProductCriteria(String productType, String shipMethodCarrier, String shipMethodFlorist, 
                                              String goodPrice, String betterPrice, String bestPrice) 
    {
        
        // Original product is Same-Day-Fresh-Cut
        //
        if("SDFC".equalsIgnoreCase(productType)){
            if(!shipMethodFlorist.equalsIgnoreCase("Y"))
            {
                logger.debug("Product type is SDFC, but shipMethodFlorist/Carrier not configured as such");
                return OPMConstants.INVALID_FLORIST_PRODUCT_ERROR_MSG;
            }
            
        // Original product is Floral product
        //
        } else if ("FLORAL".equalsIgnoreCase(productType)) {
            if(!shipMethodCarrier.equalsIgnoreCase("N") || !shipMethodFlorist.equalsIgnoreCase("Y"))
            {
                logger.debug("Product type is floral, but shipMethodFlorist/Carrier not configured as such");
                return OPMConstants.INVALID_FLORIST_PRODUCT_ERROR_MSG;
            }
            
        } else {
            logger.debug("Product type is not FLORAL or SDFC");
            return OPMConstants.INVALID_FLORIST_PRODUCT_ERROR_MSG;
        }

        // Make sure at least one price point greater than $0
        //
        if (goodPrice==null && betterPrice==null && bestPrice==null)
        {
            return OPMConstants.FLORAL_PRODUCT_NO_PRICE_MSG;
        }
        
        return null;
    }
	    
	
	private List<BaseProductMappingVO> parsePricePointDataAndMapToGbb(String pricePoints, String addonFlag, String good, String better, String best) throws Exception{
	    boolean isAddon = false;
		List<BaseProductMappingVO> prodMappingList = new ArrayList<BaseProductMappingVO>();
		BaseProductMappingVO prodMappingGoodVO = null;
        BaseProductMappingVO prodMappingBetterVO = null;
        BaseProductMappingVO prodMappingBestVO = null;

		if ("Y".equals(addonFlag)) {
		    isAddon = true;
		}
		
		if(good != null )
		{
			prodMappingGoodVO = new BaseProductMappingVO();
			prodMappingGoodVO.setProductPricePointName(OPMConstants.DB_GBB_GOOD);
			prodMappingGoodVO.setProductPricePoint(good);
			prodMappingGoodVO.setIsProductAddon(isAddon);
		}
		if(better != null )
		{
			prodMappingBetterVO = new BaseProductMappingVO();
			prodMappingBetterVO.setProductPricePointName(OPMConstants.DB_GBB_BETTER);
			prodMappingBetterVO.setProductPricePoint(better);
            prodMappingBetterVO.setIsProductAddon(isAddon);
		}
		if(best != null )
		{
			prodMappingBestVO = new BaseProductMappingVO();
			prodMappingBestVO.setProductPricePointName(OPMConstants.DB_GBB_BEST);
			prodMappingBestVO.setProductPricePoint(best);
            prodMappingBestVO.setIsProductAddon(isAddon);
		}
		if(pricePoints != null)
		{
		StringTokenizer st1 = new StringTokenizer(pricePoints, "|");
		while(st1.hasMoreElements())
		{
			String[] pricePoint = st1.nextElement().toString().split(" , ");
			if(pricePoint[0] != null){
                String gbb = pricePoint[0].trim();
				if (OPMConstants.DB_GBB_GOOD.equalsIgnoreCase(gbb)) {
			        if (prodMappingGoodVO == null) {
			            prodMappingGoodVO = new BaseProductMappingVO();
			        }
	                prodMappingGoodVO.setProductPricePointName(gbb);
					prodMappingGoodVO.setProductPricePoint(good);
		            prodMappingGoodVO.setIsProductAddon(isAddon);
		            prodMappingGoodVO.setVendorSKUs(pricePoint[1].trim());
				} else if (OPMConstants.DB_GBB_BETTER.equalsIgnoreCase(gbb)) {
                    if (prodMappingBetterVO == null) {
                        prodMappingBetterVO = new BaseProductMappingVO();
                    }
                    prodMappingBetterVO.setProductPricePointName(gbb);
					prodMappingBetterVO.setProductPricePoint(better);				    
		            prodMappingBetterVO.setIsProductAddon(isAddon);
		            prodMappingBetterVO.setVendorSKUs(pricePoint[1].trim());
				} else if (OPMConstants.DB_GBB_BEST.equalsIgnoreCase(gbb)) {
                    if (prodMappingBestVO == null) {
                        prodMappingBestVO = new BaseProductMappingVO();
                    }
                    prodMappingBestVO.setProductPricePointName(gbb);
					prodMappingBestVO.setProductPricePoint(best);
		            prodMappingBestVO.setIsProductAddon(isAddon);
		            prodMappingBestVO.setVendorSKUs(pricePoint[1].trim());
				}
			}
		}
		}

		if (prodMappingGoodVO != null) {
	        prodMappingList.add(prodMappingGoodVO);   
		}
        if (prodMappingBetterVO != null) {
            prodMappingList.add(prodMappingBetterVO);   
        }
        if (prodMappingBestVO != null) {
            prodMappingList.add(prodMappingBestVO);   
        }

		Collections.sort(prodMappingList);
		return prodMappingList;
	}
	
}
