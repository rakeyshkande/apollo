package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.FeesDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.vo.SurchargeVO;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.feed.INovatorFeedTransformer;
import com.ftd.osp.utilities.feed.NovatorFeedTransformerFactory;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataFuelSurchargeVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedRequestVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.vo.SurchargeVO;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.security.SecurityManager;


import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Handles logic to add/update fees.
 * If feeds need to happen for Novator, the feed will take place first. If feeding
 * to any requested Novator environment fails, action will not happen in Apollo.
 */
public class FeesBO
{
    private Logger logger;

    public FeesBO()
    {
        logger = new Logger("com.ftd.op.maintenance.bo.FeesBO");
    }

    /**
     * Retrieve the current fuel surcharge.
     * @param conn
     * @param request
     * @return
     * @throws Exception
     */
    public Document getFuelSurchargeXML(Connection conn) throws Exception
    {
        SurchargeVO surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(null,null, conn);
        
        Document doc = null;
        CommonUtil commonUtil = new CommonUtil();
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement("fuel_surcharge");
        doc.appendChild(root);
        
        commonUtil.addElement(doc, OPMConstants.REQUEST_APPLY_SURCHARGE_CODE, surchargeVO.getApplySurchargeCode());
        commonUtil.addElement(doc, OPMConstants.REQUEST_FUEL_SURCHARGE_AMT, String.valueOf(surchargeVO.getSurchargeAmount()));
        commonUtil.addElement(doc, OPMConstants.REQUEST_FUEL_SURCHARGE_DESCRIPTION, surchargeVO.getSurchargeDescription());
        commonUtil.addElement(doc, OPMConstants.REQUEST_SEND_FUEL_SURCHARGE_TO_FLORIST, surchargeVO.getSendSurchargeToFlorist());
        commonUtil.addElement(doc, OPMConstants.REQUEST_DISPLAY_SURCHARGE, surchargeVO.getDisplaySurcharge());        
        return doc;
    }    
    
    /**
     * @param request
     * @return
     * @throws Exception
     */
    public String saveFuelSurcharge(Connection conn, HttpServletRequest request) throws Exception
    {
        FeesDAO dao = new FeesDAO(conn);
        NovatorFeedDataFuelSurchargeVO vo = new NovatorFeedDataFuelSurchargeVO();
        boolean success = false;

        String returnMessage = "";
        String successEnv = "";
        String failureEnv = "";
        
        /* get security token */
        String sessionId = request.getParameter(OPMConstants.SEC_TOKEN);

        if(sessionId==null)
        {
            logger.error("Security token not found in request object");
        }
          
        /* retrieve user information */
        String userId = null;
        if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
            userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }        
        
        String fee = request.getParameter(OPMConstants.REQUEST_FUEL_SURCHARGE_AMT);
        String applySurchargeCode = request.getParameter(OPMConstants.REQUEST_APPLY_SURCHARGE_CODE);
        
        String novatorFeedFee = "" ;
        
        if (StringUtils.isEmpty(fee))
        {
          fee = "0.0" ;
        }
        
        if (StringUtils.equalsIgnoreCase(applySurchargeCode, "ON"))// If the applySurchargeCode is OFF or OFFC, the <external_fee> value will be blank , so if applySurchargeCode is not ON, novatorFeedFee remains "" the value it was initialized to 
        {
          novatorFeedFee = fee ;
        }
        
        String sendToFloristFlag = request.getParameter(OPMConstants.REQUEST_SEND_FUEL_SURCHARGE_TO_FLORIST);
        String surchargeDescription = request.getParameter(OPMConstants.REQUEST_FUEL_SURCHARGE_DESCRIPTION);
        String displaySurcharge = request.getParameter(OPMConstants.REQUEST_DISPLAY_SURCHARGE);
        
        logger.debug("generic fuelSurchargeAmount:" + fee);
        logger.debug("novatorFeed Fee:" + novatorFeedFee);
        logger.debug("applySurchargeCode:" + applySurchargeCode);
        logger.debug("sendToFloristFlag:" + sendToFloristFlag);
        logger.debug("surchargeDescription:" + surchargeDescription);
        logger.debug("showSurchchargeSeparately:" + displaySurcharge);
        
        NovatorUpdateVO novatorUpdateVO = buildNovatorUpdateVO(request);
        
        NovatorFeedUtil feedUtil = new NovatorFeedUtil();
        NovatorFeedRequestVO feedReqVO = null;
        NovatorFeedResponseVO feedResVO = null;
        
        NovatorFeedDataFuelSurchargeVO  feedDataVO= new NovatorFeedDataFuelSurchargeVO();
        feedDataVO.setFuelSurchargeAmt(novatorFeedFee);
        feedDataVO.setOnOffFlag(applySurchargeCode);
        
        feedReqVO = new NovatorFeedRequestVO();
        feedReqVO.setDataVO(feedDataVO);
        
        try {
            // Send to Novator
            if (novatorUpdateVO.isUpdateContent()) {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.CONTENT);              
                feedResVO = feedUtil.sendToNovator(feedReqVO);
                
                if(feedResVO.isSuccess()) {
                    successEnv = (successEnv.equals("")? "" : successEnv + ",") + NovatorFeedUtil.CONTENT;
                } else {
                    failureEnv = NovatorFeedUtil.CONTENT;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            if (novatorUpdateVO.isUpdateTest()) {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.TEST);              
                feedResVO = feedUtil.sendToNovator(feedReqVO);
                
                if(feedResVO.isSuccess()) {
                    successEnv = (successEnv.equals("")? "" : successEnv + ",") + NovatorFeedUtil.TEST;
                } else {
                    failureEnv = NovatorFeedUtil.TEST;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            if (novatorUpdateVO.isUpdateUAT()) {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.UAT);              
                feedResVO = feedUtil.sendToNovator(feedReqVO);
                
                if(feedResVO.isSuccess()) {
                    successEnv = (successEnv.equals("")? "" : successEnv + ",") + NovatorFeedUtil.UAT;
                } else {
                    failureEnv = NovatorFeedUtil.UAT;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            if (novatorUpdateVO.isUpdateLive()) {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.PRODUCTION);              
                feedResVO = feedUtil.sendToNovator(feedReqVO);
                
                if(feedResVO.isSuccess()) {
                    successEnv = (successEnv.equals("")? "" : successEnv + ",") + NovatorFeedUtil.PRODUCTION;
                } else {
                    failureEnv = NovatorFeedUtil.PRODUCTION;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            returnMessage = successEnv.equals("")? "" : "Update successfully sent to: " + successEnv + ". \n";
        } catch(Exception e) {
            logger.error(e);
            return returnMessage + "Update failed with " + e.getMessage() + "(" + failureEnv + ")";
                
        }      
        
        try {
            dao.saveFuelSurcharge(applySurchargeCode, fee, sendToFloristFlag, surchargeDescription, displaySurcharge, userId);
        } catch (Exception e) {
            logger.error(e);
            returnMessage = returnMessage + "\nFailed to save to Apollo.";
            return returnMessage;
        }
            
        return OPMConstants.RETURN_MESSAGE_SAVE_SUCCESSFUL;        
    }   
    
    /**
	 *This method is responsible for inserting Fuel surcharge feed when there is a change in the charge.
	 * @param connection
	 * @param document
	 * @throws Exception
	**/
    
    public void InsertFuelSurchargeFeed(Connection connection, Document document)  throws Exception{
    	 try {
    	 	FeesDAO dao = new FeesDAO(connection);
    	 	String xml = JAXPUtil.toStringNoFormat(document);
         	logger.info("fuelsurcharge feed::"+xml);
    	 	dao.InsertFuelSurchargeFeed(xml); 	 	
    	 }catch (Exception e) {
    		CommonUtil commUtil =  new CommonUtil();
 			commUtil.sendSystemMessage(e.getMessage(), "Fee Feed Error while sending Fuel Surcharge Feed", connection);
			throw new Exception("Error caught while inserting the fuel surcharge feed, " + e.getMessage());
			
		} 
    }
    
    /**
     * Build a list of IOTW program objects from XML
     * @param request
     * @return NovatorUpdateVO containing Novator feed information
     */
    private NovatorUpdateVO buildNovatorUpdateVO(HttpServletRequest request)
    {
        NovatorUpdateVO vo = new NovatorUpdateVO();
        String updateContent = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
        if (updateContent != null && updateContent.equals("on"))
        {
            vo.setUpdateContent(true);
        }
        String updateLive = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
        if (updateLive != null && updateLive.equals("on"))
        {
            vo.setUpdateLive(true);
        }
        String updateTest = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_TEST);
        if (updateTest != null && updateTest.equals("on"))
        {
            vo.setUpdateTest(true);
        }
        String updateUAT = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_UAT);
        if (updateUAT != null && updateUAT.equals("on"))
        {
            vo.setUpdateUAT(true);
        }

        logger.info("updateContent:" + updateContent);
        logger.info("updateLive:" + updateLive);
        logger.info("updateTest:" + updateTest);
        logger.info("updateUAT:" + updateUAT);
        
        return vo;
    }    
}