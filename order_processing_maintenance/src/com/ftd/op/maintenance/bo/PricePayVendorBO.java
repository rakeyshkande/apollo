package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.PricePayVendorUploadSpreadsheetRowVO;
import com.ftd.op.maintenance.vo.PricePayVendorVO;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.op.maintenance.vo.VendorProductVO;
import com.ftd.op.venus.vo.ProductNotificationVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.feed.NovatorFeedProductUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

public class PricePayVendorBO
{

  private static Logger logger = new Logger("com.ftd.op.maintenance.bo.PricePayVendorBO");

  //config:  file names
  public static final String PROPERTY_FILE = "maintenance-config.xml";
  private static String DATASOURCE_NAME = "CLEAN";

  /*******************************************************************************************
 * Constructor
 *******************************************************************************************/
  public PricePayVendorBO()
  {
  }

  /*******************************************************************************************
  * processUpload()
  ******************************************************************************************
  *
  */
  public String processUpload(HashMap inputMap) throws Exception {
      String batchId = null;
      String errorMessage = ""; 
      List requestUploadList = (List) inputMap.get("requestUploadList");
      Connection conn = null;

      if (requestUploadList.size() < 1) {
          errorMessage += "Spreadsheet input must contain at least one data row."; 
      } else {
          String productId = null;
          String vendorId = null;
          String vendorCost = null;
          String standardPrice = null;
          String deluxePrice = null;
          String premiumPrice = null;
          String userId = (String) inputMap.get("userId");
          logger.debug("userId: " + userId);

          try {

              conn = this.getDBConnection();
              MaintenanceDAO dao = new MaintenanceDAO(conn);
              batchId = dao.getPricePayVendorBatchId();
              HashMap productList = new HashMap();
              HashMap vendorProductList = new HashMap();
              
              ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
              String varianceParm = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT,OPMConstants.PPVU_PRICE_VARIANCE_THRESHOLD);
              int varianceThreshold = Integer.parseInt(varianceParm);
              inputMap.put("varianceThreshold", Integer.toString(varianceThreshold));

              for (int i = 0; i < requestUploadList.size(); i++) {
                  productId = ((PricePayVendorUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(PricePayVendorUploadSpreadsheetRowVO.FIELD_NAME_PRODUCT_ID).getValue();
                  vendorId = ((PricePayVendorUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(PricePayVendorUploadSpreadsheetRowVO.FIELD_NAME_VENDOR_ID).getValue();
                  if (vendorId != null && !vendorId.equals("")) {
                      vendorId = "000000" + vendorId;
                      vendorId = vendorId.substring(vendorId.length()-5);
                  }
                  vendorCost = ((PricePayVendorUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(PricePayVendorUploadSpreadsheetRowVO.FIELD_NAME_PAY_VENDOR_AMT).getValue();
                  standardPrice = ((PricePayVendorUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(PricePayVendorUploadSpreadsheetRowVO.FIELD_NAME_STANDARD_PRICE).getValue();
                  deluxePrice = ((PricePayVendorUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(PricePayVendorUploadSpreadsheetRowVO.FIELD_NAME_DELUXE_PRICE).getValue();
                  premiumPrice = ((PricePayVendorUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(PricePayVendorUploadSpreadsheetRowVO.FIELD_NAME_PREMIUM_PRICE).getValue();
                   logger.debug("standard price from uploaded file is :"+ standardPrice) ;
                   
                  logger.debug(productId + " " + vendorId + " " + vendorCost + " " + standardPrice + " " + deluxePrice + " " + premiumPrice); 
                  
                  PricePayVendorVO ppvVO = new PricePayVendorVO();
                  ppvVO.setBatchId(Integer.parseInt(batchId));
                  ppvVO.setSpreadsheetRowNum(i+1);
                  ppvVO.setProductId(productId);
                  ppvVO.setVendorId(vendorId);
                  ppvVO.setVendorCost(vendorCost);
                  ppvVO.setStandardPrice(standardPrice);
                  ppvVO.setDeluxePrice(deluxePrice);
                  ppvVO.setPremiumPrice(premiumPrice);
                  ppvVO.setProcessedFlag("N");
                  ppvVO.setUserId(userId);
                  ppvVO.setVendorCostVariancePct(0);
                  ppvVO.setStandardPriceVariancePct(0);
                  ppvVO.setDeluxePriceVariancePct(0);
                  ppvVO.setPremiumPriceVariancePct(0);
                  
                  errorMessage = "";
                  ProductVO productVO = dao.getProduct(productId);
                  logger.debug("standard price from product is :"+ productVO.getStandardPrice());
                  if (productVO == null) {
                      errorMessage = buildErrorMessage(errorMessage, "Product does not exist");
                      logger.error("Product does not exist");
                  } else {
                      if (standardPrice != null && !standardPrice.equals("")) {
                          try {
                              Double price = Double.valueOf(standardPrice);
                              if (productVO.getStandardPrice() <= 0) {
                                  errorMessage = buildErrorMessage(errorMessage, "Standard Price Point does not exist");
                                  logger.error("Standard Price Point does not exist");
                              } else {
                                  Double priceDiff = Math.abs(price - productVO.getStandardPrice());
                                      logger.debug("Price diff between standard price from product and file is :"+ priceDiff);
                                  Double variancePct = calculatePercent(priceDiff, productVO.getStandardPrice());
                                  logger.debug("Standard Price variancePct is :"+ variancePct);
                                  logger.debug("standard: " + price + " " + productVO.getStandardPrice() + " " + priceDiff + " " + variancePct);
                                  ppvVO.setStandardPriceVariancePct(variancePct);
                                  if (variancePct > varianceThreshold) {
                                      errorMessage = buildErrorMessage(errorMessage, "Dollar amount change exceeds the threshold");
                                      logger.error("Dollar amount change exceeds the threshold");
                                  }
                              }
                          } catch (NumberFormatException e) {
                              errorMessage = buildErrorMessage(errorMessage, "Invalid currency entered for Standard Price");
                              logger.error("Invalid currency entered for Standard Price");
                          }
                      }
                      if (deluxePrice != null && !deluxePrice.equals("")) {
                          try {
                              Double price = Double.valueOf(deluxePrice);
                              if (productVO.getDeluxePrice() <= 0) {
                                  errorMessage = buildErrorMessage(errorMessage, "Deluxe Price Point does not exist");
                                  logger.error("Deluxe Price Point does not exist");
                              } else {
                                  Double priceDiff = Math.abs(price - productVO.getDeluxePrice());
                                  Double variancePct = calculatePercent(priceDiff, productVO.getDeluxePrice());
                                  logger.debug("deluxe: " + price + " " + productVO.getDeluxePrice() + " " + priceDiff + " " + variancePct);
                                  ppvVO.setDeluxePriceVariancePct(variancePct);
                                  if (variancePct > varianceThreshold) {
                                      errorMessage = buildErrorMessage(errorMessage, "Dollar amount change exceeds the threshold");
                                      logger.error("Dollar amount change exceeds the threshold");
                                  }
                              }
                          } catch (NumberFormatException e) {
                              errorMessage = buildErrorMessage(errorMessage, "Invalid currency entered for Deluxe Price");
                              logger.error("Invalid currency entered for Deluxe Price");
                          }
                      }
                      if (premiumPrice != null && !premiumPrice.equals("")) {
                          try {
                              Double price = Double.valueOf(premiumPrice);
                              if (productVO.getPremiumPrice() <= 0) {
                                  errorMessage = buildErrorMessage(errorMessage, "Premium Price Point does not exist");
                                  logger.error("Premium Price Point does not exist");
                              } else {
                                  Double priceDiff = Math.abs(price - productVO.getPremiumPrice());
                                  Double variancePct = calculatePercent(priceDiff, productVO.getPremiumPrice());
                                  logger.debug("premium: " + price + " " + productVO.getPremiumPrice() + " " + priceDiff + " " + variancePct);
                                  ppvVO.setPremiumPriceVariancePct(variancePct);
                                  if (variancePct > varianceThreshold) {
                                      errorMessage = buildErrorMessage(errorMessage, "Dollar amount change exceeds the threshold");
                                      logger.error("Dollar amount change exceeds the threshold");
                                  }
                              }
                          } catch (NumberFormatException e) {
                              errorMessage = buildErrorMessage(errorMessage, "Invalid currency entered for Premium Price");
                              logger.error("Invalid currency entered for Premium Price");
                          }
                      }
                  }
                  if (vendorId != null && !vendorId.equals("")) {
                      Document vendorXML = dao.getVendor(vendorId);
                      logger.debug("VendorXML: " + XMLUtil.convertDocToString(vendorXML));
                      NodeList nl = DOMUtil.selectNodes(vendorXML, "vendors/vendor");
                      logger.debug("nl: " + nl.getLength());
                      if (nl.getLength() <= 0) {
                          errorMessage = buildErrorMessage(errorMessage, "Vendor does not exist");
                          logger.error("Vendor does not exist");
                      }
                      if (productVO != null && vendorXML != null) {
                          VendorProductVO vpVO = dao.getVendorProduct(vendorId, productId);
                          if (vpVO == null) {
                              errorMessage = buildErrorMessage(errorMessage, "Product does not have this Vendor assigned");
                              logger.error("Product does not have this Vendor assigned");
                          } else {
                              if (vendorCost != null && !vendorCost.equals("")) {
                                  try {
                                      Double price = Double.valueOf(vendorCost);
                                      if (vpVO.getVendorCost() <= 0) {
                                          errorMessage = buildErrorMessage(errorMessage, "Pay Vendor Amount does not exist");
                                          logger.error("Pay Vendor Amount does not exist");
                                      } else {
                                          Double priceDiff = Math.abs(price - vpVO.getVendorCost());
                                          Double variancePct = calculatePercent(priceDiff, vpVO.getVendorCost());
                                          logger.debug("vendor cost: " + price + " " + vpVO.getVendorCost() + " " + priceDiff + " " + variancePct);
                                          ppvVO.setVendorCostVariancePct(variancePct);
                                          if (variancePct > varianceThreshold) {
                                              errorMessage = buildErrorMessage(errorMessage, "Dollar amount change exceeds the threshold");
                                              logger.error("Dollar amount change exceeds the threshold");
                                          }
                                      }
                                  } catch (NumberFormatException e) {
                                      errorMessage = buildErrorMessage(errorMessage, "Invalid currency entered for Pay Vendor Amount");
                                      logger.error("Invalid currency entered for Pay Vendor Amount");
                                  }
                              }
                          }
                      }
                      String vpKey = productId + "*" + vendorId;
                      String vpExists = (String) vendorProductList.get(vpKey);
                      logger.debug("vpExists: " + vpExists);
                      if (vpExists == null || vpExists.equals("")) {
                          vendorProductList.put(vpKey, "" + (i+1));
                      } else {
                          errorMessage = buildErrorMessage(errorMessage, "Duplicate entry, this Product ID has already been updated");
                          logger.error("Duplicate entry, this Product ID has already been updated");
                      }
                  }
                  String productExists = (String) productList.get(productId);
                  logger.debug("productExists: " + productExists + " " + standardPrice);
                  if (productExists == null || productExists.equals("")) {
                      productList.put(productId, standardPrice);
                  } else {
                      if (vendorId == null || vendorId.equals("")) {
                          errorMessage = buildErrorMessage(errorMessage, "Duplicate entry, this Product ID has already been updated");
                          logger.error("Duplicate entry, this Product ID has already been updated");
                      } else if (!productExists.equals(standardPrice)) {
                          errorMessage = buildErrorMessage(errorMessage, "Retail Prices cannot be different across locations");
                          logger.error("Retail Prices cannot be different across locations");
                      }
                  }
                  logger.debug("errorMessage: " + errorMessage);
                  ppvVO.setErrorMessage(errorMessage);
                  
                  dao.insertPricePayVendorUpdate(ppvVO);

              }
              
              // get result list
              Document doc = dao.getPricePayVendorApprovalList(batchId);
              inputMap.put("approvalList", doc);

              CachedResultSet rs = dao.getPricePayVendorApprovalCounts(batchId);
              if (rs.next()) {
                  inputMap.put("no_product_master", rs.getString("no_product_master"));
                  inputMap.put("no_vendor_master", rs.getString("no_vendor_master"));
                  inputMap.put("no_vendor_product", rs.getString("no_vendor_product"));
              }

          } catch (Exception e) {
              logger.error("PPVU error: " + e);
              logger.error(e);
              throw new Exception("Price/Pay Vendor processUpload() error: " + e.getMessage());
          }
          finally {
              try
              {
            	  if (conn != null)
            		  conn.close();
              }
              catch (SQLException se)
              {
                  logger.error(se);
              }
          }
 
      }

      return batchId;

  }

    /*******************************************************************************************
    * updateBatch()
    ******************************************************************************************
    *
    */
    public void updateBatch(String batchId) throws Exception {
        logger.debug("updateBatch(" + batchId + ")");
        Connection conn = null;
        boolean overwriteSentTo = true;
        int errorCount = 0;
        
        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            int exceptionsAllowed = Integer.parseInt(configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT,OPMConstants.PPVU_UPDATE_EXCEPTIONS_ALLOWED));
            String  myBuysFeedEnabled  = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");
            
            conn = this.getDBConnection();
            MaintenanceDAO dao = new MaintenanceDAO(conn);
            
            List ppvList = dao.getPricePayVendorUpdateList(batchId);
            for (int i=0; i< ppvList.size(); i++) {
                PricePayVendorVO ppvVO = (PricePayVendorVO) ppvList.get(i);
                logger.debug(ppvVO.getSpreadsheetRowNum() + " " + ppvVO.getProductId() + " " + ppvVO.getVendorId() + " " + ppvVO.getErrorMessage());
                
                logger.debug("ppvVO.getVendorCostVariancePct(): " + ppvVO.getVendorCostVariancePct()+ "ppvVO.getStandardPriceVariancePct(): "+ ppvVO.getStandardPriceVariancePct()+ "ppvVO.getDeluxePriceVariancePct(): "+ ppvVO.getDeluxePriceVariancePct() + "ppvVO.getPremiumPriceVariancePct(): "+ ppvVO.getPremiumPriceVariancePct());
                
                if (ppvVO.getErrorMessage() == null) {
                    String productId = ppvVO.getProductId();
                    
                    //update PDB
                    logger.debug("Updating PDB");
                    boolean updateSuccess = true;
                    try {
                        if (ppvVO.getStandardPrice() == null) ppvVO.setStandardPrice("0");
                        if (ppvVO.getDeluxePrice() == null) ppvVO.setDeluxePrice("0");
                        if (ppvVO.getPremiumPrice() == null) ppvVO.setPremiumPrice("0");
                        dao.updateProductMasterPrices(ppvVO);
                        if (ppvVO.getVendorId() != null) {
                            dao.updateVendorCost(ppvVO);
                        }
                    } catch (Exception e) {
                        logger.error("PDB update failed: " + e);
                        ppvVO.setErrorMessage("PDB update failed: " + e.getMessage());
                        updateSuccess = false;
                    }

                    if (updateSuccess) {
                        //send to Novator
                        logger.debug("Sending to Novator");

                        try {
                            NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();
                            NovatorFeedResponseVO resVO = feedUtil.sendProductFeed(conn, productId, null, overwriteSentTo);
                            String errorString = "";
                            
                            if(!resVO.isSuccess()) {
                                errorString = resVO.getErrorString();
                            }
                            else 
                            {  
                            
                              if  (StringUtils.equalsIgnoreCase(myBuysFeedEnabled,"Y")
                              &&  (ppvVO.getVendorCostVariancePct() != 0
                              || ppvVO.getStandardPriceVariancePct() != 0 
                              || ppvVO.getDeluxePriceVariancePct() != 0 
                              || ppvVO.getPremiumPriceVariancePct() != 0 ) )
                              {
                              logger.debug("Calling MyBuys MDB for PricePayVendorUpdate for product id:" + ppvVO.getProductId());
                                     try
                                     {
                                        sendMyBuys(ppvVO.getProductId()); 
                                     } catch (Exception e)
                                     {
                                        logger.error("error in calling MyBuys MDB for sending PricePayVendorUpdate Message for productId: " +ppvVO.getProductId()+". " + e);
                                        ppvVO.setErrorMessage("Failed to notify MyBuys. Please re-load this record or contact the Tech team for assistance.");
                                                                                
                                      }
                                      
                              }
                                
                            }
                            if (!errorString.equals("")){
                                logger.info("errorString returned:" + errorString);
                                ppvVO.setErrorMessage("Failed to update Novator. Please re-load this record or contact the Tech team for assistance.");
                                errorCount += 1;
                            }
                        } catch (Exception e) {
                            logger.error(e);
                            ppvVO.setErrorMessage("Failed to update Novator. Please re-load this record or contact the Tech team for assistance.");
                            errorCount += 1;
                        }
                    }

                }
                
                ppvVO.setProcessedFlag("Y");
                dao.updatePricePayVendorUpdate(ppvVO);
                
                if (errorCount >= exceptionsAllowed) {
                    logger.error("Allowable exception threshold exceeded, process will terminate");
                    break;
                }
            }

            String successReportId = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT,OPMConstants.PPVU_SUCCESS_REPORT_ID);
            String errorReportId = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT,OPMConstants.PPVU_ERROR_REPORT_ID);
            String distributionList = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT,OPMConstants.PPVU_EMAIL_DISTRIBUTION_LIST);

            //Submit the Success Report
            CachedResultSet rs = dao.getReportById(successReportId);
            if (rs.next()) {
                String rdfFileName = rs.getString("oracle_rdf");
                String reportUrl = rs.getString("server_name");
                String parmValue = rs.getString("schedule_parm_value");
                reportUrl = reportUrl + parmValue;
                reportUrl = reportUrl + "&desname=" + distributionList;
                reportUrl = reportUrl + "&report=" + rdfFileName;
                reportUrl = reportUrl + "&p_batch_id=" + batchId;
                String submissionId = dao.insertReportLog(successReportId, rdfFileName, reportUrl);
                logger.debug("submissionId: " + submissionId);
                logger.debug("reportUrl: " + reportUrl);

                MessageToken token = new MessageToken();
                token.setMessage(submissionId);

                token.setStatus("REPORTING");
                try {
                    Dispatcher.getInstance().dispatchTextMessage(new InitialContext(), token);
                } catch (Exception e) {
                    throw new RuntimeException("dispatchReportRequest: Failed.", e);
                }
            } else {
                logger.error("Could not retrieve success report information");
            }

            //Submit the Error Report
            rs = dao.getReportById(errorReportId);
            if (rs.next()) {
                String rdfFileName = rs.getString("oracle_rdf");
                String reportUrl = rs.getString("server_name");
                String parmValue = rs.getString("schedule_parm_value");
                reportUrl = reportUrl + parmValue;
                reportUrl = reportUrl + "&desname=" + distributionList;
                reportUrl = reportUrl + "&report=" + rdfFileName;
                reportUrl = reportUrl + "&p_batch_id=" + batchId;
                String submissionId = dao.insertReportLog(errorReportId, rdfFileName, reportUrl);
                logger.debug("submissionId: " + submissionId);
                logger.debug("reportUrl: " + reportUrl);

                MessageToken token = new MessageToken();
                token.setMessage(submissionId);

                token.setStatus("REPORTING");
                try {
                    Dispatcher.getInstance().dispatchTextMessage(new InitialContext(), token);
                } catch (Exception e) {
                    throw new RuntimeException("dispatchReportRequest: Failed.", e);
                }
            } else {
                logger.error("Could not retrieve error report information");
            }

        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        finally {
            try
            {
            	if (conn != null)
            		conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }
    }

    private Double calculatePercent(Double a, Double b) {
        Double retVal = 0.00;
        if (b <= 0) {
            if (a == 0) {
                retVal = 0.00;
            } else {
                retVal = 100.00;
            }
        } else {
            retVal = a / b * 100;
            if (retVal > 999.99) retVal = 999.99;
        }
        
        return retVal;
    }

    /**
    * Obtain connectivity with the database
    */
    private Connection getDBConnection()
        throws IOException, ParserConfigurationException, SAXException, TransformerException, 
            Exception
    {
        Connection conn = null;
        conn = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE, DATASOURCE_NAME));

        return conn;
    }  

    private String buildErrorMessage(String errorMessage, String newText) {
        if (errorMessage == null || errorMessage.equals("")) {
            errorMessage = newText;
        } else {
            errorMessage = errorMessage + " / " + newText;
        }
        return errorMessage;
    }
    
        /**
     * calls the MDB for sending JMS message to myBuys queue
     * @param String productId of object to for whom messages need to be sent
     * @return void
     */
      
    public void sendMyBuys(String productId) throws Exception
    {
    
               MessageToken messageToken = new MessageToken();
               messageToken.setMessage(productId);
               messageToken.setStatus("MY BUYS");
               Dispatcher dispatcher = Dispatcher.getInstance();
               dispatcher.dispatchTextMessage(new InitialContext(),messageToken);
        
    }
}
