package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.DAOConstants;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.InventoryTrackingDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.LockUtil;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


public class InventoryTrackingMaintenanceBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static final String INPUT_DATE_FORMAT = "MM/dd/yyyy";
  


  /*******************************************************************************************
 * Constructor
 *******************************************************************************************/
  public InventoryTrackingMaintenanceBO()
  {
  }


  /*******************************************************************************************
  * processAdd()
  ******************************************************************************************
  *
  */
  public void processAdd(HashMap inputMap, HashMap pageData)
    throws Exception
  {
    pageData.put("forwardToType", "XSL");
    pageData.put("forwardToName", "maintenance");

    //store the action type
    pageData.put("original_action_type", (String) inputMap.get("requestActionType"));


    return;
  }


  /*******************************************************************************************
  * processEdit()
  ******************************************************************************************
  *
  */
  public void processEdit(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    String invTrkId = (String) inputMap.get("requestInvTrkId");

    //retrieve record and add it to the page data
    retrieveInvTrkVendorEmailInfo(invTrkId, responseMap, con);

    pageData.put("forwardToType", "XSL");
    pageData.put("forwardToName", "maintenance");

    //store the action type
    pageData.put("original_action_type", (String) inputMap.get("requestActionType"));


    return;
  }


  /*******************************************************************************************
  * processProductVendor()
  ******************************************************************************************
  *
  */
  public void processProductVendor(HashMap inputMap, HttpServletResponse response, Connection con)
    throws Exception
  {
    String productId = (String) inputMap.get("requestProductId");
    VendorDAO vDAO = new VendorDAO(con);

    CachedResultSet productVendorResultsCRS = vDAO.getProductVendor(productId);
    //product and vendor search results
    Document productVendorResultsXML = CommonUtil.buildXML(productVendorResultsCRS, "product_vendors", "product_vendor", "element");

    PrintWriter out = response.getWriter();
    DOMUtil.print(productVendorResultsXML, out);
    out.flush();

    return;
  }


  /*******************************************************************************************
  * processCopyEdit()
  ******************************************************************************************
  *
  */
  public void processCopyEdit(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    String invTrkId = (String) inputMap.get("requestInvTrkId");
    String securityToken = (String) inputMap.get("requestSecurityToken");
    String csrId = (String) inputMap.get("csrId");

    if (invTrkId != null && !invTrkId.equalsIgnoreCase(""))
      releaseLock(invTrkId, securityToken, csrId, con);

    //retrieve record and add it to the page data
    retrieveInvTrkVendorEmailInfo(invTrkId, responseMap, con);

    removeUnwantedInfo(responseMap);

    pageData.put("forwardToType", "XSL");
    pageData.put("forwardToName", "maintenance");

    //store the action type
    pageData.put("original_action_type", (String) inputMap.get("requestActionType"));

    return;
  }


  /*******************************************************************************************
  * processSave()
  ******************************************************************************************
  *
  */
  public void processSave(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat(this.INPUT_DATE_FORMAT);

    String comments = (String) inputMap.get("requestComments");
    String csrId = (String) inputMap.get("csrId");
    String endDate = (String) inputMap.get("requestEndDate");
    String forecast = (String) inputMap.get("requestForecast");
    List   invTrkEmailIdsToDeleteList = (ArrayList) inputMap.get("requestInvTrkEmailIdsToDelete"); 
    String invTrkId = (String) inputMap.get("requestInvTrkId");
    String productId = (String) inputMap.get("requestProductId");
    String newEmailAddress = (String) inputMap.get("requestNewEmailAddress");
    String revised = (String) inputMap.get("requestRevised"); 
    String securityToken = (String) inputMap.get("requestSecurityToken");
    String shutdownThreshold = (String) inputMap.get("requestShutdownThreshold");
    String startDate = (String) inputMap.get("requestStartDate");
    String vendorId = (String) inputMap.get("requestVendorId");
    String warningThreshold = (String) inputMap.get("requestWarningThreshold");

    //Create a Calendar Object for start and end dates
    Calendar cStartDate = Calendar.getInstance();
    Calendar cEndDate = Calendar.getInstance();

    //Set the Calendar Object using the date retrieved in startDate
    if (startDate != null && !startDate.equalsIgnoreCase(""))
      cStartDate.setTime(sdf.parse(startDate));

    //Set the Calendar Object using the date retrieved in endDate
    if (endDate != null && !endDate.equalsIgnoreCase("")){
      cEndDate.setTime(sdf.parse(endDate));
    //Set the Calendar Object using the max date
    }else{
      String maxDate = CacheUtil.getInstance().getGlobalParm(DAOConstants.SKU_IT_CONTEXT, DAOConstants.SKU_IT_RECORD_ENDDATE);
      if (StringUtils.isNotEmpty(maxDate)){
    	  cEndDate.setTime(sdf.parse(maxDate));
      }
    }
    /************************************************************************************************
     * if invTrkId exist, its a save invoked for an Edit; else, user is trying to create a new record
     ************************************************************************************************/
    if (invTrkId != null && !invTrkId.equalsIgnoreCase(""))
    {                                 /**********/
                                      /** EDIT **/
                                      /**********/
      String lockedBy = retrieveLock(invTrkId, securityToken, csrId, con);

      //if lock valid
      if (lockedBy == null || lockedBy.equalsIgnoreCase(""))
        updateInventoryInfo(invTrkId, cStartDate, cEndDate, revised, warningThreshold, shutdownThreshold, comments
        		, invTrkEmailIdsToDeleteList, newEmailAddress, csrId, con, pageData);
      else
        pageData.put("errorMessage", "This record is currently being viewed by " + lockedBy + ".  Your changes were NOT saved.  Please try again later. ");

      //store the action type
      pageData.put("original_action_type", "edit");


    }
    else
    {                                 /*********/
                                      /** ADD **/
                                      /*********/
    	String newInvTrkId = null;
    	if (cEndDate.compareTo(cStartDate) < 0)
        {
          String errorMessage = "Invalid data found - Start Date cannot be greater than the End Date"; 
          pageData.put("errorMessage", errorMessage);
        }
    	else {
      newInvTrkId = createInventoryInfo(productId, vendorId, cStartDate, cEndDate, forecast, warningThreshold, shutdownThreshold, 
                                               comments, csrId, con, pageData); 

      //repopulate the inputMap with the new invTrkId
      inputMap.put("requestInvTrkId", newInvTrkId);
    	}

      if (newInvTrkId != null && !newInvTrkId.equalsIgnoreCase(""))
      {
        //retrieve lock.  this should ALWAYS work, because we just generated the record
        String lockedBy = retrieveLock(newInvTrkId, securityToken, csrId, con);
        if (lockedBy != null && !lockedBy.equalsIgnoreCase(""))
          throw new Exception("For the newly created inventory record = " + invTrkId + ", the process SHOULD always retrieve a lock.");
        
        pageData.put("original_action_type", "edit");

      }
      else
        pageData.put("original_action_type", "add");
      
    }

    //retrieve the error message
    String errorMessage = (String) pageData.get("errorMessage");
    
    //at this point, this process could have been invoked from 
    // 1)edit and regardless of the results - invTrkId = request invTrkId
    // 2)add and is successful - invTrkId = new invTrkId
    // 3)add and is failed - invTrkId = null
    invTrkId = (String) inputMap.get("requestInvTrkId");

    //retrieve record and add it to the page data
    if (invTrkId != null && !invTrkId.equalsIgnoreCase(""))
      retrieveInvTrkVendorEmailInfo(invTrkId, responseMap, con);


    //if an error was found, we have to reset the data from the request; we cannot take it from the database
    if (errorMessage != null && !errorMessage.equalsIgnoreCase(""))
    {      
      //if its for an edit, we have the records based on invTrkId, and we need to reset the data
      if (invTrkId != null && !invTrkId.equalsIgnoreCase(""))
        resetPageFromRequestForEdit(responseMap, vendorId, warningThreshold, shutdownThreshold, startDate, endDate, revised, comments); 
      //if its for an add, we have to build the node with request data
      else
      {
        //build search node
        Document searchResultsXML = buildSearchXMLFromRequestForAdd(productId, vendorId, warningThreshold, shutdownThreshold, startDate, endDate, forecast, comments, con);
        responseMap.put("HK_search_results", searchResultsXML);

        //build vendor node
        Document vendorResultsXML = buildVendorXMLFromRequestForAdd(productId, con);
        responseMap.put("HK_vendor_results", vendorResultsXML);
      }
    }
    else
      pageData.put("errorMessage", "Record has been successfully saved");
    

    pageData.put("forwardToType", "XSL");
    pageData.put("forwardToName", "maintenance");


    return;
  }


  /*******************************************************************************************
  * processMainMenu()
  ******************************************************************************************
  *
  */
  public void processMainMenu(HashMap inputMap, HashMap pageData, Connection con)
    throws Exception
  {
    String invTrkId = (String) inputMap.get("requestInvTrkId");
    String securityToken = (String) inputMap.get("requestSecurityToken");
    String csrId = (String) inputMap.get("csrId");

    if (invTrkId != null && !invTrkId.equalsIgnoreCase(""))
      releaseLock(invTrkId, securityToken, csrId, con);

    pageData.put("forwardToType", "ACTION");
    pageData.put("forwardToName", "MainMenuAction");

    return;
  }


  /*******************************************************************************************
  * processDashboard()
  ******************************************************************************************
  *
  */
  public void processDashboard(HashMap inputMap, HashMap pageData, Connection con)
    throws Exception
  {
    String invTrkId = (String) inputMap.get("requestInvTrkId");
    String securityToken = (String) inputMap.get("requestSecurityToken");
    String csrId = (String) inputMap.get("csrId");

    if (invTrkId != null && !invTrkId.equalsIgnoreCase(""))
      releaseLock(invTrkId, securityToken, csrId, con);

    pageData.put("forwardToType", "ACTION");
    pageData.put("forwardToName", "dashboard");

    return;
  }


  /*******************************************************************************************
  * retrieveInvTrkVendorEmailInfo()
  ******************************************************************************************
  *
  */
  private void retrieveInvTrkVendorEmailInfo(String invTrkId, HashMap responseMap, Connection con)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);

    //CachedResultSet that will contain the output from the stored proce
    HashMap searchResults = itDAO.searchInvTrk(invTrkId);

    //inventory tracking search results
    Document searchResultsXML = CommonUtil.buildXML(searchResults, "OUT_INV_TRK_CUR", "search_results", "search_result", "element");
    responseMap.put("HK_search_results", searchResultsXML);

    //vendors available for this product
    Document vendorResultsXML = CommonUtil.buildXML(searchResults, "OUT_VENDOR_CUR", "vendors", "vendor", "element");
    responseMap.put("HK_vendor_results", vendorResultsXML);

    //email available for this inventory tracking id
    Document emailResultsXML = CommonUtil.buildXML(searchResults, "OUT_EMAIL_CTRL_CUR", "emails", "email", "element");
    responseMap.put("HK_email_results", emailResultsXML);

  }


  /*******************************************************************************************
  * removeEditInfo()
  ******************************************************************************************
  *
  */
  private void removeUnwantedInfo(HashMap responseMap)
    throws Exception
  {
    //reset some of the text() values on the inventory tracking node
	  Document searchResultsXML = (Document) responseMap.get("HK_search_results");

    Text invTrkIdXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/inv_trk_id/text()"));
    Text forecastXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/forecast_qty/text()"));
    Text revisedXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/revised_forecast_qty/text()"));
    Text ftdMsgCountXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/ftd_msg_count/text()"));
    Text canMsgCountXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/can_msg_count/text()"));
    Text rejMsgCountXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/rej_msg_count/text()"));
    Text shippedShippingXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/shipped_shipping/text()"));
    Text onHandXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/on_hand/text()"));

    if (invTrkIdXMLText != null)
      invTrkIdXMLText.setData(null);
    if (forecastXMLText != null)
      forecastXMLText.setData(null);
    if (revisedXMLText != null)
      revisedXMLText.setData(null);
    if (ftdMsgCountXMLText != null)
      ftdMsgCountXMLText.setData("0");
    if (canMsgCountXMLText != null)
      canMsgCountXMLText.setData("0");
    if (rejMsgCountXMLText != null)
      rejMsgCountXMLText.setData("0");
    if (shippedShippingXMLText != null)
      shippedShippingXMLText.setData("0");
    if (onHandXMLText != null)
      onHandXMLText.setData("0");

    //remove the email node
    responseMap.remove("HK_email_results");

  }


  /*******************************************************************************************
  * resetPageFromRequest()
  ******************************************************************************************
  *
  */
  private void resetPageFromRequestForEdit(HashMap responseMap, String vendorId, String warningThreshold, String shutdownThreshold, String startDate, String endDate, String revised, String comments)
    throws Exception
  {
    //reset some of the text() values on the inventory tracking node
    Document searchResultsXML = (Document) responseMap.get("HK_search_results");

    Text vendorIdXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/vendor_id/text()"));
    Text warningThresholdQtyXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/warning_threshold_qty/text()"));
    Text shutdownThresholdQtyXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/shutdown_threshold_qty/text()"));
    Text startDateXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/start_date/text()"));
    Text endDateXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/end_date/text()"));
    Text revisedForecastQtyXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/revised_forecast_qty/text()"));    
    Text commentsTextXMLText = (Text) (DOMUtil.selectSingleNode(searchResultsXML, "/search_results/search_result/comments/text()"));

    if (vendorIdXMLText != null)
      vendorIdXMLText.setData(vendorId);
    if (warningThresholdQtyXMLText != null)
      warningThresholdQtyXMLText.setData(warningThreshold);
    if (shutdownThresholdQtyXMLText != null)
      shutdownThresholdQtyXMLText.setData(shutdownThreshold);
    if (startDateXMLText != null)
      startDateXMLText.setData(startDate);
    if (endDateXMLText != null)
      endDateXMLText.setData(endDate);
    if (revisedForecastQtyXMLText != null)
      revisedForecastQtyXMLText.setData(revised);
    if (commentsTextXMLText != null)
      commentsTextXMLText.setData(comments);

    return; 
  }
  /*******************************************************************************************
  * retrieveLock()
  ******************************************************************************************
  *
  *
  */
  private String retrieveLock(String entityId, String sessionId, String csrId, Connection con)
    throws Exception
  {
    String lockedBy = LockUtil.getLock(con, OPMConstants.INV_TRK_ENTITY_TYPE, entityId, sessionId, csrId);
    return lockedBy;

  }


  /*******************************************************************************************
  * releaseLock()
  ******************************************************************************************
  *
  *
  */
  private void releaseLock(String entityId, String sessionId, String csrId, Connection con)
    throws Exception
  {
    LockUtil.releaseLock(con, OPMConstants.INV_TRK_ENTITY_TYPE, entityId, sessionId, csrId);
  }


  /*******************************************************************************************
  * createInventoryInfo()
  ******************************************************************************************
  *
  *
  */
  private String createInventoryInfo(String productId, String vendorId, Calendar cStartDate, Calendar cEndDate, 
                                     String forecast, String warningThreshold, String shutdownThreshold, 
                                     String comments, String csrId, Connection con, HashMap pageData)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);

    //CachedResultSet that will contain the output from the stored proce
    String invTrkId = itDAO.createInventoryInfo(productId, vendorId, cStartDate, cEndDate, forecast, warningThreshold, shutdownThreshold, 
                                                comments, csrId, pageData);

    return invTrkId;

  }

  /*******************************************************************************************
  * updateInventoryInfo()
  ******************************************************************************************
  *
  *
  */
  private void updateInventoryInfo(String invTrkId, Calendar cStartDate, Calendar cEndDate, String revised,
                                     String warningThreshold, String shutdownThreshold, 
                                     String comments, List invTrkEmailIdsToDeleteList, String newEmailAddress,
                                     String csrId, Connection con, HashMap pageData)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);

    //CachedResultSet that will contain the output from the stored proce
    itDAO.updateInventoryInfo(invTrkId, cStartDate, cEndDate, revised, warningThreshold, shutdownThreshold, 
                              comments, invTrkEmailIdsToDeleteList, newEmailAddress, csrId, pageData);

  }


  /******************************************************************************
  * 
  *******************************************************************************
  */

  private Document buildSearchXMLFromRequestForAdd(String productId, String vendorId, String warningThreshold, String shutdownThreshold, String startDate, String endDate, String forecast, String comments, Connection con)
  throws Exception
  {
    //Document that will contain the final XML, to be passed to the TransUtil and XSL page
    Document doc = DOMUtil.getDefaultDocument();
    
    MaintenanceDAO mDAO = new MaintenanceDAO(con);
    ProductVO pVO = mDAO.getProduct(productId); 

    
    //*****************************************************************************************
    //create the highest element
    //*****************************************************************************************
    Element searchResultsElement = doc.createElement("search_results");
    doc.appendChild( searchResultsElement );

    //*****************************************************************************************
    //create the node for the search_result
    //*****************************************************************************************
    Element searchResultElement = doc.createElement("search_result");
    searchResultsElement.appendChild(searchResultElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element invTrkIdElement = doc.createElement("inv_trk_id");
    searchResultElement.appendChild(invTrkIdElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element productIdElement = doc.createElement("product_id");
    productIdElement.appendChild(doc.createTextNode(productId));
    searchResultElement.appendChild(productIdElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element productNameElement = doc.createElement("product_name");
    productNameElement.appendChild(doc.createTextNode(pVO.getProductName()));
    searchResultElement.appendChild(productNameElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element productStatusElement = doc.createElement("product_status");
    productStatusElement.appendChild(doc.createTextNode(pVO.getStatus()));
    searchResultElement.appendChild(productStatusElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element productUpdateByElement = doc.createElement("product_updated_by");
    productUpdateByElement.appendChild(doc.createTextNode(pVO.getUpdatedBy()));
    searchResultElement.appendChild(productUpdateByElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element exceptionStartDateElement = doc.createElement("exception_start_date");
    exceptionStartDateElement.appendChild(doc.createTextNode(pVO.getExceptionStartDate() != null ? pVO.getExceptionStartDate().toString() : null));
    searchResultElement.appendChild(exceptionStartDateElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element vendorIdElement = doc.createElement("vendor_id");
    vendorIdElement.appendChild(doc.createTextNode(vendorId));
    searchResultElement.appendChild(vendorIdElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element vendorNameElement = doc.createElement("vendor_name");
    searchResultElement.appendChild(vendorNameElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element vendorProductStatusElement = doc.createElement("vendor_product_status");
    searchResultElement.appendChild(vendorProductStatusElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element vendorProductUpdatedByElement = doc.createElement("vendor_product_updated_by");
    searchResultElement.appendChild(vendorProductUpdatedByElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element warningThresholdQtyElement = doc.createElement("warning_threshold_qty");
    warningThresholdQtyElement.appendChild(doc.createTextNode(warningThreshold));
    searchResultElement.appendChild(warningThresholdQtyElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element shutdownThresholdQtyElement = doc.createElement("shutdown_threshold_qty");
    shutdownThresholdQtyElement.appendChild(doc.createTextNode(shutdownThreshold));
    searchResultElement.appendChild(shutdownThresholdQtyElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element startDateElement = doc.createElement("start_date");
    startDateElement.appendChild(doc.createTextNode(startDate));
    searchResultElement.appendChild(startDateElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element endDateElement = doc.createElement("end_date");
    endDateElement.appendChild(doc.createTextNode(endDate));
    searchResultElement.appendChild(endDateElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element forecastQtyElement = doc.createElement("forecast_qty");
    forecastQtyElement.appendChild(doc.createTextNode(forecast));
    searchResultElement.appendChild(forecastQtyElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element revisedForecastElement = doc.createElement("revised_forecast_qty");
    revisedForecastElement.appendChild(doc.createTextNode(forecast));
    searchResultElement.appendChild(revisedForecastElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element commentsElement = doc.createElement("comments");
    commentsElement.appendChild(doc.createTextNode(comments));
    searchResultElement.appendChild(commentsElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element ftdMsgCountElement = doc.createElement("ftd_msg_count");
    ftdMsgCountElement.appendChild(doc.createTextNode("0"));
    searchResultElement.appendChild(ftdMsgCountElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element canMsgCountElement = doc.createElement("can_msg_count");
    canMsgCountElement.appendChild(doc.createTextNode("0"));
    searchResultElement.appendChild(canMsgCountElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element rejMsgCountElement = doc.createElement("rej_msg_count");
    rejMsgCountElement.appendChild(doc.createTextNode("0"));
    searchResultElement.appendChild(rejMsgCountElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element shippedShippingElement = doc.createElement("shipped_shipping");
    shippedShippingElement.appendChild(doc.createTextNode("0"));
    searchResultElement.appendChild(shippedShippingElement);

    //*****************************************************************************************
    //create the node for the 
    //*****************************************************************************************
    Element onHandElement = doc.createElement("on_hand");
    onHandElement.appendChild(doc.createTextNode(forecast));
    searchResultElement.appendChild(onHandElement);



    return doc;

  }

  /******************************************************************************
  * 
  *******************************************************************************
  */

  private Document buildVendorXMLFromRequestForAdd(String productId, Connection con)
  throws Exception
  {
    VendorDAO vDAO = new VendorDAO(con);

    //vendor search results
    Document productVendorResultsXML = vDAO.getVendorIdForProduct(productId);


    return productVendorResultsXML;

  }
}