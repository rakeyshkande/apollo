package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MPRedemptionRateDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.NovatorMPRedemptionRateTransmission;
import com.ftd.op.maintenance.vo.MPRedemptionRateVO;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.security.SecurityManager;

import java.io.IOException;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * Handles logic to add/delete/update redemption rates.
 * If feeds need to happen for Novator, the feed will take place first. If feeding
 * to any requested Novator environment fails, action will not happen in Apollo.
 */
public class MPRedemptionRateBO
{
    private Logger logger;

    public MPRedemptionRateBO()
    {
        logger = new Logger("com.ftd.op.maintenance.bo.MPRedemptionRateBO");
    }
    
    /**
    * Obtain connectivity with the database
    */
    private Connection getDBConnection()
        throws IOException, ParserConfigurationException, SAXException, TransformerException, 
            Exception
    {
        return CommonUtil.getDBConnection();
    }      

    public Document getRedemptionRateListXML() throws Exception
    {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            
            doc = dao.getRedemptionRateList();
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return doc;
    }
    
    public Document getMPPaymentMethodIds() throws Exception
    {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            doc = dao.getMPPaymentMethodIds();
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return doc;
    }    
    
    public Document getRedemptionRateXML(HttpServletRequest request) throws Exception
    {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            String mpRedemptionRateId = request.getParameter(OPMConstants.REQUEST_MP_RD_PAYMENT_METHOD_ID);
            doc = dao.getRedemptionRate(mpRedemptionRateId);
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return doc;
    }    
    
    public String deleteRedemptionRate(HttpServletRequest request) throws Exception
    {
        Connection conn = null;
        boolean success = false;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            String returnMessage = null;
            CommonUtil commonUtil = new CommonUtil();
            boolean feedToNovator = commonUtil.getRedemptionRateFeedNovatorFlag();
            List list = new ArrayList();
            String deleteIds = request.getParameter("deleteValues");
            
            /* get security token */
            String sessionId = request.getParameter(OPMConstants.SEC_TOKEN);
    
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
              
            /* retrieve user information */
            String userId = null;
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }  
            
            StringBuffer result = new StringBuffer();
            for (int i = 0; i < deleteIds.length(); i++) {
                if (deleteIds.substring(i,i+1).equals(",")) {
                    list.add(result.toString());
                    result = new StringBuffer();
                } else {
                    result.append(deleteIds.substring(i,i+1));
                }
            }       
            list.add(result.toString());
            NovatorUpdateVO nuvo = buildNovatorUpdateVO(request);
            
            for (int i=0; i < list.size(); i++) {
                success = false;
                MPRedemptionRateVO rrVO = dao.getRedemptionRateObject((String)list.get(i));
                rrVO.setUpdatedBy(userId);
        
                if(feedToNovator) {
                    //feed to Novator
                    try {
                         NovatorMPRedemptionRateTransmission t = new NovatorMPRedemptionRateTransmission();
                         t.send(nuvo, rrVO, OPMConstants.NOVATOR_FEED_ACTION_DELETE);
                         success = true;
                    } catch (Exception e)
                    {
                         logger.error(e);
                         returnMessage = e.getMessage();
                         return returnMessage;
                    }
                } 
            
                // email accounting/finance
                if (success || !feedToNovator) {
                    try {
                        dao.deleteRedemptionRate(rrVO.getRedemptionRateId());
                    } catch (Exception e) {
                        logger.error(e);
                        returnMessage = OPMConstants.RETURN_MESSAGE_DELETE_FAILED;
                        return returnMessage;
                    }
                    try {
                        this.notifyMPRedemptionRateChange(rrVO, OPMConstants.NOVATOR_FEED_ACTION_DELETE);
                    } catch (Exception x) {
                        logger.error(x);
                        commonUtil.sendPage("Failed to send email to ACCTG_GROUP_EMAIL_ADDRESS about Miles Points redemption rate change.", x, "OP", SystemMessengerVO.LEVEL_PRODUCTION, "System Exception", "System Message",commonUtil.getDBConnection());
                    }       
                }
            }
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }

        return OPMConstants.RETURN_MESSAGE_DELETE_SUCCESSFUL;
    } 
    
    public String addRedemptionRate(HttpServletRequest request) throws Exception
    {
        Connection conn = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            MPRedemptionRateVO vo = new MPRedemptionRateVO();
            boolean success = false;
            CommonUtil commonUtil = new CommonUtil();
            String returnMessage = null;
            boolean feedToNovator = commonUtil.getRedemptionRateFeedNovatorFlag();
            
            /* get security token */
            String sessionId = request.getParameter(OPMConstants.SEC_TOKEN);
    
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
              
            /* retrieve user information */
            String userId = null;
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }        
            
            vo.setRedemptionRateId(request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_ID));
            vo.setPaymentMethodId(request.getParameter(OPMConstants.REQUEST_MP_RD_PAYMENT_METHOD_ID));
            vo.setDescription(request.getParameter(OPMConstants.REQUEST_MP_RD_DESCRIPTION));
            vo.setRedemptionRateAmt(request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_AMT));
            vo.setUpdatedBy(userId);
            vo.setRequestedBy(request.getParameter(OPMConstants.REQUEST_MP_RD_REQUESTED_BY));
    
            NovatorUpdateVO nuvo = buildNovatorUpdateVO(request);   
            if (feedToNovator) {
                //feed to Novator
                try {
                     NovatorMPRedemptionRateTransmission t = new NovatorMPRedemptionRateTransmission();
                     t.send(nuvo, vo, OPMConstants.NOVATOR_FEED_ACTION_ADD);
                     success = true;
                } catch (Exception e)
                {
                     logger.error(e);
                     returnMessage = e.getMessage();
                     return returnMessage;
                }
            } 
            
            // email accounting/finance
            if (success || !feedToNovator) {
                try {
                    dao.saveRedemptionRate(vo);
                } catch (Exception e) {
                    logger.error(e);
                    returnMessage = OPMConstants.RETURN_MESSAGE_SAVE_FAILED;
                    return returnMessage;
                }
                try {
                    this.notifyMPRedemptionRateChange(vo, OPMConstants.NOVATOR_FEED_ACTION_ADD);
                } catch (Exception x) {
                    logger.error(x);
                    commonUtil.sendPage("Failed to send email to ACCTG_GROUP_EMAIL_ADDRESS about Miles Points redemption rate change.", x, "OP", SystemMessengerVO.LEVEL_PRODUCTION, "System Exception", "System Message",commonUtil.getDBConnection());
                }
            }
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        
        return OPMConstants.RETURN_MESSAGE_SAVE_SUCCESSFUL;
        
    }      
    
    /**
     * @param request
     * @return
     * @throws Exception
     */
    public String saveRedemptionRate(HttpServletRequest request) throws Exception
    {
        Connection conn = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            MPRedemptionRateVO vo = new MPRedemptionRateVO();
            boolean success = false;
            CommonUtil commonUtil = new CommonUtil();
            String returnMessage = null;
            boolean feedToNovator = commonUtil.getRedemptionRateFeedNovatorFlag();
            
            /* get security token */
            String sessionId = request.getParameter(OPMConstants.SEC_TOKEN);
    
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
              
            /* retrieve user information */
            String userId = null;
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }        
            
            vo.setRedemptionRateId(request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_ID));
            vo.setPaymentMethodId(request.getParameter(OPMConstants.REQUEST_MP_RD_PAYMENT_METHOD_ID));
            vo.setDescription(request.getParameter(OPMConstants.REQUEST_MP_RD_DESCRIPTION));
            vo.setRedemptionRateAmt(request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_AMT));
            vo.setUpdatedBy(userId);
            vo.setRequestedBy(request.getParameter(OPMConstants.REQUEST_MP_RD_REQUESTED_BY));
            
            NovatorUpdateVO nuvo = buildNovatorUpdateVO(request);
            
            if (feedToNovator) {
                //feed to Novator
                try {
                     NovatorMPRedemptionRateTransmission t = new NovatorMPRedemptionRateTransmission();
                     returnMessage = t.send(nuvo, vo, OPMConstants.NOVATOR_FEED_ACTION_UPDATE);
                     success = true;
                } catch (Exception e)
                {
                     logger.error(e);
                     returnMessage = e.getMessage();
                     return returnMessage;
                }
            } 
            
            // email accounting/finance
            if (success || !feedToNovator) {
                try {
                    dao.saveRedemptionRate(vo);
                } catch (Exception e) {
                    logger.error(e);
                    returnMessage = OPMConstants.RETURN_MESSAGE_SAVE_FAILED;
                    return returnMessage;
                }
                try {
                    this.notifyMPRedemptionRateChange(vo, OPMConstants.NOVATOR_FEED_ACTION_UPDATE);
                } catch (Exception x) {
                    logger.error(x);
                    commonUtil.sendPage("Failed to send email to ACCTG_GROUP_EMAIL_ADDRESS about Miles Points redemption rate change.", x, "OP", SystemMessengerVO.LEVEL_PRODUCTION, "System Exception", "System Message",commonUtil.getDBConnection());
                }
            }
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return OPMConstants.RETURN_MESSAGE_SAVE_SUCCESSFUL;        
    }   
    
    public Document getSourceCodeListXML(HttpServletRequest request) throws Exception
    {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            String mpRedemptionRateId = request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_ID);       
            doc = dao.getSourceCodeList(mpRedemptionRateId);
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return getFormattedSurceCodeMemberLevel(doc);
    }    
    
    /**
     * This method formats the document which has source code and member levels for each source code into
     * source code and its member levels separated by comma.
     */
    private Document getFormattedSurceCodeMemberLevel(Document doc) throws Exception {
    	
    	NodeList list = doc.getChildNodes();            
        //Only element in the document is sources-codes.
        Node source_codes = list.item(0);
        if(source_codes.hasChildNodes()){
      	  //Check only if there are source codes for the given redemption rate.
          XPath xpath = XPathFactory.newInstance().newXPath();
      	  NodeList source_code = source_codes.getChildNodes();
      	  String oldSourceCode = null;
      	  String memberLevelId = null;      	  
      	  //To retain the position of the first row for each source code.            	 
      	  ArrayList<Integer> removeRecPos = new ArrayList<Integer>();
      	  int position = 0;
      	  for(int i=0; i< source_code.getLength(); i++) {
      		  //Iterate for the each source code details.                	  
      		  Node sourceCodeDetail = source_code.item(i);
      		  String newSourceCode;      		        		        		        		        		              		        	
      		  if(oldSourceCode == null){
      			  //Old source code will be null for the first row of each source code.
      			  //Both old and new source codes will be made equal      			 
      			  //Retain the position so that we can retain in the final document.
      			  position = i;            			  
      			  oldSourceCode = sourceCodeDetail.getFirstChild().getFirstChild().getNodeValue();
      			  newSourceCode = oldSourceCode;      			  
      			  memberLevelId = sourceCodeDetail.getLastChild().getLastChild() == null ? " " : sourceCodeDetail.getLastChild().getLastChild().getNodeValue();
      		  } else {       			  
      			  //The source code is repeated and have to append the member level id 
      			  //to the previous value with comma separation.
      			  newSourceCode = sourceCodeDetail.getFirstChild().getFirstChild().getNodeValue();
      			  if(oldSourceCode.equals(newSourceCode)) {
      				  //Append only if the source code is repeated.      				              				  
      				  memberLevelId = memberLevelId + "," + (sourceCodeDetail.getLastChild().getLastChild() == null ? " " : sourceCodeDetail.getLastChild().getLastChild().getNodeValue());      				  
      				  if(sourceCodeDetail.getNextSibling() == null) {
      					Node actualNode = source_code.item(position);
      					actualNode.getLastChild().getLastChild().setNodeValue(memberLevelId);  
      				  }
      				  removeRecPos.add(i);      				  
      			  } else {
      				  //If we get a new source code other than old one, edit the member level id with the appended value
      				  //for the row at "actualRecordPos" position 
      				  Node actualNode = source_code.item(position); 
      				  if(actualNode.getLastChild().getLastChild() != null) {
      					actualNode.getLastChild().getLastChild().setNodeValue(memberLevelId);  
      				  }
      				  oldSourceCode = null;
      				  i--;
      			  }            			  
      		  }            		 
      	  }// end for loop    
      	  //Remove the redundant source codes in the document
      	  for(int i: removeRecPos) {
      		  i++;
      		  XPathExpression expr = xpath.compile("//source_code[@num='"+i+"']");
      		  Object result = expr.evaluate(doc, XPathConstants.NODESET);
      		  NodeList nodes = (NodeList) result;            		  
      		  source_codes.removeChild(nodes.item(0));
      	  }      	  
        }
      return doc;    	
    }
    
    
    public Document getMPRedemptionHistoryXML(HttpServletRequest request) throws Exception
    {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = this.getDBConnection();
            MPRedemptionRateDAO dao = new MPRedemptionRateDAO(conn);
            String mpRedemptionRateId = request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_ID);
          
            doc = dao.getMPRedemptionRateHistory(mpRedemptionRateId);
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        return doc;
    }    
    
    public void notifyMPRedemptionRateChange(MPRedemptionRateVO vo, String action) throws Exception
    {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        CommonUtil commonUtil = new CommonUtil();
        String fromAddress = configUtil.getProperty(OPMConstants.EMAIL_CONFIG_FILE, OPMConstants.EMAIL_MP_RR_FROM_ADDRESS);
        String toAddress = configUtil.getFrpGlobalParm(OPMConstants.GLOBAL_PARM_CONTEXT_GLOBAL,OPMConstants.EMAIL_MP_RR_TO_ADDRESS);
        String SMTPHost = configUtil.getFrpGlobalParm(OPMConstants.GLOBAL_PARM_CONTEXT_MESSAGE_GENERATOR, OPMConstants.GLOBAL_PARM_NAME_SMTP_HOST);
        String subject = configUtil.getProperty(OPMConstants.EMAIL_CONFIG_FILE,OPMConstants.EMAIL_MP_RR_SUBJECT);
        String content = "";
        
        if(action.equalsIgnoreCase(OPMConstants.NOVATOR_FEED_ACTION_DELETE)) {
            content = "Redemption rate id " + vo.getRedemptionRateId() + " has been deleted by " + vo.getUpdatedBy() + ".";
        } else if(action.equalsIgnoreCase(OPMConstants.NOVATOR_FEED_ACTION_UPDATE)) {
            content = "Redemption rate id " + vo.getRedemptionRateId() + " has been update by " + vo.getUpdatedBy() + ". The current rate is " + vo.getRedemptionRateAmt() + ".";
        } else if(action.equalsIgnoreCase(OPMConstants.NOVATOR_FEED_ACTION_ADD)) {
            content = "Redemption rate id " + vo.getRedemptionRateId() + " of " + vo.getRedemptionRateAmt() + " has been added by " + vo.getUpdatedBy() + ".";
        } else {
            throw new Exception("Unknown action in notifying redemption rate change.");
        }
        
        commonUtil.sendEmail(fromAddress, toAddress, SMTPHost, subject, content);
    }
    
    /**
     * Build a list of IOTW program objects from XML
     * @param request
     * @return NovatorUpdateVO containing Novator feed information
     */
    private NovatorUpdateVO buildNovatorUpdateVO(HttpServletRequest request)
    {
        NovatorUpdateVO vo = new NovatorUpdateVO();
        String updateContent = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
        if (updateContent != null && updateContent.equals("on"))
        {
            vo.setUpdateContent(true);
        }
        String updateLive = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
        if (updateLive != null && updateLive.equals("on"))
        {
            vo.setUpdateLive(true);
        }
        String updateTest = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_TEST);
        if (updateTest != null && updateTest.equals("on"))
        {
            vo.setUpdateTest(true);
        }
        String updateUAT = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_UAT);
        if (updateUAT != null && updateUAT.equals("on"))
        {
            vo.setUpdateUAT(true);
        }

        logger.info("updateContent:" + updateContent);
        logger.info("updateLive:" + updateLive);
        logger.info("updateTest:" + updateTest);
        logger.info("updateUAT:" + updateUAT);
        
        return vo;
    }    
}