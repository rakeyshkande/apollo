package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.AddOnDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.LockUtil;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataAddOnVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedRequestVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringReader;

import java.sql.Connection;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;


public class AddOnMaintenanceBO
{
    private static Logger logger = new Logger("com.ftd.op.maintenance.action.AddOnMaintenanceBO");

    /**
     * Controls the functionality to add a new add-on
     * @param inputMap special data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */

    public void processAdd(HashMap inputMap, HashMap pageData, Connection con)
        throws Exception
    {
        AddOnDAO addOnDAO = new AddOnDAO(con);
        Document addOnTypeXML = addOnDAO.getAllAddOnType();
        Document allOccasionsXML = addOnDAO.getAllOccassions(); // Changes for Apollo.Q3.2015 - Addon-Occasion association

        String defaultCardPrice = null;
        if (ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                                             OPMConstants.DEFAULT_CARD_PRICE) != null)
        {
            //main menu url
            defaultCardPrice = 
                    ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                                                     OPMConstants.DEFAULT_CARD_PRICE);
            pageData.put("default_card_price", defaultCardPrice);
        }

        pageData.put("addOnTypeXML", addOnTypeXML);
        pageData.put("forwardToType", "XSL");
        pageData.put("forwardToName", "maintenance");
        pageData.put("occasionsXML", allOccasionsXML); // Changes for Apollo.Q3.2015 - Addon-Occasion association

        //store the action type
        pageData.put("original_action_type", (String) inputMap.get("requestActionType"));
    }

    /**
     * Controls the functionality to edit an add-on
     * @param inputMap special data for this action
     * @param responseMap returned data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */
    
    public void processEdit(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
        throws Exception
    {
        String addOnId = (String) inputMap.get("add_on_id");

        //retrieve record and add it to the page data
        AddOnDAO addOnDAO = new AddOnDAO(con);
        Document addOnTypeXML = addOnDAO.getAllAddOnType();
        
        // Changes for Apollo.Q3.2015 - Addon-Occasion association
        Document allOccasionsXML = addOnDAO.getAllOccassions();

        String defaultCardPrice = null;
        if (ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                                             OPMConstants.DEFAULT_CARD_PRICE) != null)
        {
            //main menu url
            defaultCardPrice = ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, OPMConstants.DEFAULT_CARD_PRICE);
            pageData.put("default_card_price", defaultCardPrice);
        }

        retrieveAddOnInfoFromDB(addOnId, responseMap, con);

        pageData.put("addOnTypeXML", addOnTypeXML);
        pageData.put("forwardToType", "XSL");
        pageData.put("forwardToName", "maintenance");
        pageData.put("occasionsXML", allOccasionsXML); // Changes for Apollo.Q3.2015 - Addon-Occasion association

        //store the action type
        pageData.put("original_action_type", (String) inputMap.get("requestActionType"));
    }

    /**
     * Controls the functionality to save a new add-on
     * @param inputMap special data for this action
     * @param responseMap returned data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */
    
    public void processSave(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
        throws Exception
    {
        con.setAutoCommit(false);

        String csrId = (String) inputMap.get("csrId");
        String securityToken = (String) inputMap.get("requestSecurityToken");

        AddOnVO addOnVO = (AddOnVO) inputMap.get("addOnVO");
        NovatorUpdateVO novatorUpdateVO = (NovatorUpdateVO) inputMap.get("requestNovatorUpdateVO");
        
        if (AddOnVO.ADD_ON_TYPE_VASE_ID.toString().equals(addOnVO.getAddOnTypeId()) && !addOnVO.getAddOnAvailableFlag())
        {
            AddOnUtility addOnUtility = new AddOnUtility();
            if (addOnUtility.isOnlyOfTypeAssigned(addOnVO.getAddOnId(),con))
            {
                pageData.put("errorMessage", "You cannot make " + addOnVO.getAddOnDescription() + " unavailable because 1 or more products have " + addOnVO.getAddOnDescription() + " as the only vase assigned.");
                logger.debug("update AddOn Failed due to one or more products having it as the only assigned add-on of its type");                
            }
        }
        
        if (pageData.get("errorMessage") == null || ((String) pageData.get("errorMessage")).equalsIgnoreCase(""))
        {
            String lockedBy = retrieveLock(addOnVO.getAddOnId(), securityToken, csrId, con);
    
            if (lockedBy == null || lockedBy.equalsIgnoreCase(""))
            {
                try
                {
                    updateAddOnVO(addOnVO, csrId, con);
                    
                    logger.debug("AddOn updated..");
                    try
                    {
                        sendNovatorFeed(addOnVO, novatorUpdateVO);
                    }
                    catch (Exception e)
                    {
                        pageData.put("errorMessage", "An error occurred. Add-on was not saved, and feed could not be sent.");
                        logger.error("Novator AddOn Feed Failed");
                        logger.error(e);
                    }
                    
                }
                catch (Exception e)
                {
                    pageData.put("errorMessage", "An error occurred. Add-on was not saved, and feed could not be sent.");
                    logger.error("update AddOn Failed");
                    logger.error(e);
                }
    
            }
            else
            {
                pageData.put("errorMessage", "This add-on is in a locked state. You cannot view or save this add-on at this time.");
            }
        }

        //retrieve the error message
        String errorMessage = (String) pageData.get("errorMessage");

        if (errorMessage == null || errorMessage.equalsIgnoreCase(""))
        {
            con.commit();

            //retrieve and convert addonVO from the DB
            retrieveAddOnInfoFromDB(addOnVO.getAddOnId(), responseMap, con);

            //reset the action type.  Upon successful save, "add" will be converted to "edit".  "edit" will essentially remain "edit"
            pageData.put("original_action_type", "edit");

            //put a success message in page data
            pageData.put("errorMessage", "Add-on has been saved. ");

        }
        else
        {
            con.rollback();

            //retrieve and convert addonVO from the Input fields
            retrieveAddOnInfoFromInput(addOnVO, responseMap);

            //do not reset the action type.
            pageData.put("original_action_type", (String) inputMap.get("requestOriginalActionType"));
        }
        con.setAutoCommit(true);

        AddOnDAO addOnDAO = new AddOnDAO(con);
        Document addOnTypeXML = addOnDAO.getAllAddOnType();
        Document allOccasionsXML = addOnDAO.getAllOccassions(); // Changes for Apollo.Q3.2015 - Addon-Occasion association

        String defaultCardPrice = null;
        if (ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                                             OPMConstants.DEFAULT_CARD_PRICE) != null)
        {
            //main menu url
            defaultCardPrice = 
                    ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                                                     OPMConstants.DEFAULT_CARD_PRICE);
            pageData.put("default_card_price", defaultCardPrice);
        }

        pageData.put("addOnTypeXML", addOnTypeXML);
        pageData.put("forwardToType", "XSL");
        pageData.put("forwardToName", "maintenance");
        pageData.put("occasionsXML", allOccasionsXML); // Changes for Apollo.Q3.2015 - Addon-Occasion association

    }


    /**
     * Controls the functionality to return to the main menu
     * @param inputMap special data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */
    
    public void processMainMenu(HashMap inputMap, HashMap pageData, Connection con)
        throws Exception
    {
        String securityToken = (String) inputMap.get("requestSecurityToken");
        String csrId = (String) inputMap.get("csrId");

        AddOnVO addOnVO = (AddOnVO) inputMap.get("addOnVO");

        if (addOnVO.getAddOnId() != null && !addOnVO.getAddOnId().equalsIgnoreCase(""))
        {
            releaseLock(addOnVO.getAddOnId(), securityToken, csrId, con);
        }

        pageData.put("forwardToType", "ACTION");
        pageData.put("forwardToName", "MainMenuAction");

        return;
    }


    /**
     * Controls the functionality return to the dashboard
     * @param inputMap special data for this action
     * @param pageData common data for pages
     * @param con database connection
     * @throws Exception
     */
    
    public void processDashboard(HashMap inputMap, HashMap pageData, Connection con)
        throws Exception
    {
        String securityToken = (String) inputMap.get("requestSecurityToken");
        String csrId = (String) inputMap.get("csrId");
        AddOnVO addOnVO = (AddOnVO) inputMap.get("addOnVO");

        if (addOnVO.getAddOnId() != null && !addOnVO.getAddOnId().equalsIgnoreCase(""))
        {
            releaseLock(addOnVO.getAddOnId(), securityToken, csrId, con);
        }

        pageData.put("forwardToType", "ACTION");
        pageData.put("forwardToName", "dashboard");

        return;
    }


    /**
     * Addon Info has been saved.  Retrieve the addon info from the database 
     * @param responseMap returned data for this action
     * @param con database connection
     * @throws Exception
     */
    
    private void retrieveAddOnInfoFromDB(String addOnId, HashMap responseMap, Connection con)
        throws Exception
    {
        AddOnUtility addOnUtility = new AddOnUtility();

        AddOnVO addOnVO = new AddOnVO();
        addOnVO = addOnUtility.getAddOnById(addOnId, false, false, con);

        //Document parser = new DOMParser();
        Document addOnXML = DOMUtil.getDefaultDocument();

        Document parser = DOMUtil.getDocument(addOnVO.toXML(1));
        addOnXML.appendChild(addOnXML.importNode(parser.getFirstChild(), true));

        responseMap.put("HK_add_on_results", addOnXML);
    }

    /**
     * Addon Info has NOT been saved.  Retrieve the addon info from the input request variables
     * @param responseMap returned data for this action
     * @param con database connection
     * @throws Exception
     */
    
    private void retrieveAddOnInfoFromInput(AddOnVO addOnVO, HashMap responseMap)
        throws Exception
    {
        Document addOnXML = DOMUtil.getDefaultDocument();

        Document parser = DOMUtil.getDocument(addOnVO.toXML(1));
        addOnXML.appendChild(addOnXML.importNode(parser.getFirstChild(), true));

        responseMap.put("HK_add_on_results", addOnXML);
    }


    /**
     * Perform a save of an add-on
     * @param addOnVO the add-on to update
     * @param csrId CSR who is performing the action
     * @param con database connection
     * @throws Exception
     */
    
    private void updateAddOnVO(AddOnVO addOnVO, String csrId, Connection con)
        throws Exception
    {
        //Instantiate AddOnDAO
        AddOnDAO aoDAO = new AddOnDAO(con);
        aoDAO.updateAddon(addOnVO, csrId);
    }

    /**
     * Check if the product is a valid product
     * @param inputMap input parameters for this action
     * @param response http response to add result to
     * @param con database connection
     * @throws Exception
     */
    
    public void processValidateProductId(HashMap inputMap, HttpServletResponse response, Connection con)
        throws Exception
    {
        String productId = (String) inputMap.get("requestProductId");
        logger.debug("processValidateProductId called with Id " + productId);
        MaintenanceDAO maintenanceDAO = new MaintenanceDAO(con);
        ProductVO productVO = maintenanceDAO.getProduct(productId);
        if (productVO != null)
        {
            inputMap.put("validate_product_id", "true");
        }
        else
        {
            inputMap.put("validate_product_id", "false");
        }
        PrintWriter out = response.getWriter();
        out.write((String) inputMap.get("validate_product_id"));
        out.flush();
    }

    /**
     * Check if the add-on is a valid add-on
     * @param inputMap input parameters for this action
     * @param response http response to add result to
     * @param con database connection
     * @throws Exception
     */
    
    public void processValidateAddOnId(HashMap inputMap, HttpServletResponse response, Connection con)
        throws Exception
    {
        String addOnId = (String) inputMap.get("requestAddOnId");
        logger.debug("processValidateAddOnId called with Id " + addOnId);
        AddOnUtility addOnUtility = new AddOnUtility();
        AddOnVO addOnVO = addOnUtility.getAddOnById(addOnId, false, false, con);
        if (addOnVO != null)
        {
            inputMap.put("validate_add_on_id", "true");
        }
        else
        {
            inputMap.put("validate_add_on_id", "false");
        }
        PrintWriter out = response.getWriter();
        out.write((String) inputMap.get("validate_add_on_id"));
        out.flush();
    }

    /**
     * Perform calls to send Novator feeds
     * @param addOnVO the add-on to send
     * @param novatorUpdateVO the NovatorUpdateVO that contains the environment information
     * @return String with error or success message
     * @throws Exception
     */
    
    /* #552 - Flexible Fullfillment - Addon Dash board and Maintanance changes 
    Sending the addon delivery type to novator.
    
    Again commenting the feed which has been send to novator.
    Again un-commenting the this delivery tag.
    */

    private String sendNovatorFeed(AddOnVO addOnVO, NovatorUpdateVO novatorUpdateVO)
        throws Exception
    {

        String returnMessage = "";
        String successEnv = "";
        String failureEnv = "";

        NovatorFeedUtil feedUtil = new NovatorFeedUtil();
        NovatorFeedRequestVO feedReqVO = null;
        NovatorFeedResponseVO feedResVO = null;

        NovatorFeedDataAddOnVO feedDataVO = new NovatorFeedDataAddOnVO();

        feedDataVO.setAdd_on_description(addOnVO.getAddOnDescription());
        feedDataVO.setAdd_on_id(addOnVO.getAddOnId());
        feedDataVO.setAdd_on_text(addOnVO.getAddOnText());
        feedDataVO.setAdd_on_type_description(addOnVO.getAddOnTypeDescription());
        feedDataVO.setAdd_on_type_id(addOnVO.getAddOnTypeId());
        feedDataVO.setPrice(addOnVO.getAddOnPrice());
        feedDataVO.setDelivery_type(addOnVO.getDeliveryType());
        feedDataVO.setUnspsc(addOnVO.getAddOnUNSPSC());
        feedDataVO.setAvailable_flag(addOnVO.getAddOnAvailableFlag()? "Y": "N");
        feedDataVO.setDefault_per_type_flag(addOnVO.getDefaultPerTypeFlag()? "Y": "N");
        feedDataVO.setDisplay_price_flag(addOnVO.getDisplayPriceFlag()? "Y": "N");
        /*
         * DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
         * The new attributes are included in the Novator feed.
         */
        feedDataVO.setsPquadAccsryId(addOnVO.getsPquadAccsryId());// DI-4: Add-on Dashboard Enhancements
        feedDataVO.setFtdWestAddon(addOnVO.isFtdWestAddon()? "Y" : "N");// DI-4: Add-on Dashboard Enhancements

        feedReqVO = new NovatorFeedRequestVO();
        feedReqVO.setDataVO(feedDataVO);
        logger.debug("feedDataVO.getAdd_on_id:" + feedDataVO.getAdd_on_id());
        logger.debug("feedDataVO.getPrice:" + feedDataVO.getPrice());

        try
        {
            // Send to Novator
            if (novatorUpdateVO.isUpdateContent())
            {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.CONTENT);
                feedResVO = feedUtil.sendToNovator(feedReqVO);

                if (feedResVO.isSuccess())
                {
                    successEnv = (successEnv.equals("")? "": successEnv + ",") + NovatorFeedUtil.CONTENT;
                }
                else
                {
                    failureEnv = NovatorFeedUtil.CONTENT;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            if (novatorUpdateVO.isUpdateTest())
            {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.TEST);
                feedResVO = feedUtil.sendToNovator(feedReqVO);

                if (feedResVO.isSuccess())
                {
                    successEnv = (successEnv.equals("")? "": successEnv + ",") + NovatorFeedUtil.TEST;
                }
                else
                {
                    failureEnv = NovatorFeedUtil.TEST;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            if (novatorUpdateVO.isUpdateUAT())
            {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.UAT);
                feedResVO = feedUtil.sendToNovator(feedReqVO);

                if (feedResVO.isSuccess())
                {
                    successEnv = (successEnv.equals("")? "": successEnv + ",") + NovatorFeedUtil.UAT;
                }
                else
                {
                    failureEnv = NovatorFeedUtil.UAT;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            if (novatorUpdateVO.isUpdateLive())
            {
                feedReqVO.setFeedToEnv(NovatorFeedUtil.PRODUCTION);
                feedResVO = feedUtil.sendToNovator(feedReqVO);

                if (feedResVO.isSuccess())
                {
                    successEnv = (successEnv.equals("")? "": successEnv + ",") + NovatorFeedUtil.PRODUCTION;
                }
                else
                {
                    failureEnv = NovatorFeedUtil.PRODUCTION;
                    throw new Exception(feedResVO.getErrorString());
                }
            }
            returnMessage = successEnv.equals("")? "": "Update successfully sent to: " + successEnv + ". \n";
        }
        catch (Exception e)
        {
            logger.error(returnMessage + "Update failed with " + e.getMessage() + "(" + failureEnv + ")");
            throw (e);
        }
        return returnMessage;
    }

    /**
     * Attempt to lock a resource
     * @param entityId entity to lock
     * @param sessionId session id
     * @param csrId csr id
     * @param con database connection
     * @return
     * @throws Exception
     */
    private String retrieveLock(String entityId, String sessionId, String csrId, Connection con)
        throws Exception
    {
        String lockedBy = LockUtil.getLock(con, OPMConstants.ADD_ON_ENTITY_TYPE, entityId, sessionId, csrId);
        return lockedBy;

    }

    /**
     * Release a lock on a resource
     * @param entityId entity to lock
     * @param sessionId session id
     * @param csrId csr id
     * @param con database connection
     * @return
     * @throws Exception
     */
    
    private void releaseLock(String entityId, String sessionId, String csrId, Connection con)
        throws Exception
    {
        LockUtil.releaseLock(con, OPMConstants.ADD_ON_ENTITY_TYPE, entityId, sessionId, csrId);
    }
}
