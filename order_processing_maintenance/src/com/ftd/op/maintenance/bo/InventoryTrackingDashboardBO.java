package com.ftd.op.maintenance.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.InventoryTrackingDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.LockUtil;
import com.ftd.op.maintenance.util.Transaction;
import com.ftd.op.maintenance.vo.InventorySearchCriteriaVO;
import com.ftd.op.maintenance.vo.InventoryTrackingUploadSpreadsheetRowVO;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.op.maintenance.vo.VendorProductVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.VendorProductAvailabilityUtility;
import com.ftd.osp.utilities.constants.VendorProductAvailabilityConstants;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.feed.NovatorFeedProductUtil;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.util.pagination.PaginationUtil;
import com.ftd.util.pagination.PaginationVO;


public class InventoryTrackingDashboardBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static final String INPUT_DATE_FORMAT = "MM/dd/yyyy";
  private static final Pattern DATE_PATTERN = Pattern.compile("\\d\\d/\\d\\d/\\d\\d\\d\\d");
  private static Logger logger = new Logger("com.ftd.op.maintenance.action.InventoryTrackingDashboardBO");



  /*******************************************************************************************
 * Constructor
 *******************************************************************************************/
  public InventoryTrackingDashboardBO()
  {
  }


  /*******************************************************************************************
  * processLoad()
  ******************************************************************************************
  *
  */
  public void processLoad(HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    List vendorsAvailableVOList = retrieveVendorsVOList(con);
    Document vendorsAvailableXML = convertVendorVOListToXML(vendorsAvailableVOList, "vendors_available");

    responseMap.put("HK_vendor_available", vendorsAvailableXML);
    pageData.put("forwardToType", "XSL");
    pageData.put("forwardToName", "dashboard");
    pageData.put("recordCount", "0");

    return; 
  }


  /*******************************************************************************************
  * processTaken()
  ******************************************************************************************
  *
  */
  public void processTaken(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    List productIdsList = (ArrayList) inputMap.get("requestProductIds"); 
    String productId = (String) productIdsList.get(0); 
    retrieveOrdersTaken(productId, responseMap, pageData, con);

    pageData.put("forwardToType", "XSL");
    pageData.put("forwardToName", "taken");

    return; 
  }


  /*******************************************************************************************
  * processDelete()
  ******************************************************************************************
  *
  */
  public void processDelete(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    List invTrkIds = (ArrayList) inputMap.get("requestInvTrkIds"); 
    List<String> products = retrieveProductsInvTrkIds(invTrkIds, con);
    String securityToken = (String) inputMap.get("requestSecurityToken"); 
    String csrId = (String) inputMap.get("csrId"); 
    String lockedBy;
    String invTrkId = "";
    String errorMessage = ""; 
  
		for (int i = 0; i < invTrkIds.size(); i++) {
			try {
				invTrkId = (String) invTrkIds.get(i);
				lockedBy = retrieveLock(invTrkId, securityToken, csrId, con);
	
				if (lockedBy == null || lockedBy.equalsIgnoreCase("")) {
					errorMessage += deleteInventory(invTrkId, con) + "\n";
				} else {
					errorMessage += "This record is currently being viewed by " + lockedBy + ".  Please try again later. \n";
				}
			} catch (Exception e) {
				if(!StringUtils.isEmpty(invTrkId)){
					logger.error("Error caught deleting inventory record:" + invTrkId + ", " + e.getMessage());
					errorMessage += "Inventory Tracking record #" + invTrkId + " could not be deleted. Please try again later. \n";
				}
			}
		}
	// Make product Unavailable if there is no inventory left for product.
	try {		
		VendorProductAvailabilityUtility.performPeriodShutdown(products, csrId, con);
	} catch (Exception e) {
		logger.error("Error caught performing product shutdown check when inventory is deleted: " + e.getMessage());
	}
	
    pageData.put("errorMessage", errorMessage);
    processSearch(inputMap, responseMap, pageData, con);

  }
  
  /**
   * processVendors
   * This function creates the VOs necessary to populate the vendors selected section of the web page
   * @param inputMap
   * @param responseMap
   * @param pageData
   * @param con
   * @throws Exception
  */
  public void processVendors(HashMap inputMap, HashMap responseMap, Connection con) 
    throws Exception
  {
  //retrieve all vendor ids and create lists for available vs. chosen vendors
    List vendorsVOList = retrieveVendorsVOList(con);
    List vendorsAvailableVOList = new ArrayList(); 
    List vendorsChosenVOList = new ArrayList(); 
    VendorMasterVO vmVO = null;
    List vendorIdsChosenList = (ArrayList) inputMap.get("requestVendorIdsChosen"); 

    for (int i=0; i < vendorsVOList.size(); i++)
    {
      vmVO = (VendorMasterVO) vendorsVOList.get(i);
      if (vendorIdsChosenList.contains(vmVO.getVendorID()))
        vendorsChosenVOList.add(vmVO);
      else
        vendorsAvailableVOList.add(vmVO);
    }

    //convert the available vo list to xml and append to response hashmap  
    Document vendorsAvailableXML = convertVendorVOListToXML(vendorsAvailableVOList, "vendors_available");
    responseMap.put("HK_vendor_available", vendorsAvailableXML);

    //convert the chosen vo list to xml and append to response hashmap  
    Document vendorsChosenXML = convertVendorVOListToXML(vendorsChosenVOList, "vendors_chosen");
    responseMap.put("HK_vendor_chosen", vendorsChosenXML);
    
    return; 
  }


  /*******************************************************************************************
  * processCOAD()
  ******************************************************************************************
  *
  */
  public void processCOAD(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    List invTrkIds = (ArrayList) inputMap.get("requestInvTrkIds"); 
    List productIds = retrieveProductsByInvTrkIds(invTrkIds, con);
    String errorMessage = ""; 
    String productId = null; 
    NovatorFeedResponseVO resVO;
    NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();

    UserTransaction userTransaction = null;

    //build the environment array for product feed utility
    NovatorUpdateVO novatorUpdateVO = (NovatorUpdateVO) inputMap.get("requestNovatorUpdateVO"); 
    List envKeys = new ArrayList(); 
    if (novatorUpdateVO.isUpdateContent())
      envKeys.add(NovatorFeedUtil.CONTENT);
    if (novatorUpdateVO.isUpdateLive())
      envKeys.add(NovatorFeedUtil.PRODUCTION);
    if (novatorUpdateVO.isUpdateTest())
      envKeys.add(NovatorFeedUtil.TEST);
    if (novatorUpdateVO.isUpdateUAT())
      envKeys.add(NovatorFeedUtil.UAT);
    
    //loop thru all the products and invoke the shutdown utility and feed utility
    for (int i = 0; i < productIds.size(); i++)
    {
      productId = (String) productIds.get(i); 
      try
      {
        userTransaction = Transaction.getTransaction();
  
        // Start the transaction with the begin method
        userTransaction.begin();

        //call product utility
        VendorProductAvailabilityUtility.clearDates(productId, con);
        errorMessage += productId + " - Product was successfully updated.  ";

        //call feed utility
        if (envKeys.size() > 0)
        {
        resVO = feedUtil.sendProductFeed(con, productId, envKeys);
        errorMessage += resVO.getErrorString() + "\n";

        if (!resVO.isSuccess())
          throw new Exception(); 
        }
        userTransaction.commit();

      }
      catch (Exception e)
      {
        //rollback transaction
        Transaction.rollback(userTransaction);

        errorMessage += productId + " could not be updated\n";
      }
      
    }

    pageData.put("errorMessage", errorMessage);
    processSearch(inputMap, responseMap, pageData, con);
    
  }


	/** Process request to make product/ vendor_product/ inventory available.
	 * @param inputMap
	 * @param responseMap
	 * @param pageData
	 * @param con
	 * @throws Exception
	 */
	public void processMA(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con) throws Exception {

		@SuppressWarnings("unchecked")
		List<String> invTrkIds = (ArrayList<String>) inputMap.get("requestInvTrkIds");
		List<VendorProductVO> vpVOList = retrieveProductsVendorsByInvTrkIds(invTrkIds, con);
		
		String csrId = (String) inputMap.get("csrId");
		StringBuilder errorMessage = new StringBuilder();
		NovatorFeedResponseVO resVO;
		NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();
		UserTransaction userTransaction = null;

		// build the environment array for product feed utility
		NovatorUpdateVO novatorUpdateVO = (NovatorUpdateVO) inputMap.get("requestNovatorUpdateVO");
		List<String> envKeys = new ArrayList<String>();
		if (novatorUpdateVO.isUpdateContent()) envKeys.add(NovatorFeedUtil.CONTENT);
		if (novatorUpdateVO.isUpdateLive())	envKeys.add(NovatorFeedUtil.PRODUCTION);
		if (novatorUpdateVO.isUpdateTest())	envKeys.add(NovatorFeedUtil.TEST);
		if (novatorUpdateVO.isUpdateUAT())	envKeys.add(NovatorFeedUtil.UAT);
		
		Map<String, List<String>> prodVendorMap = new HashMap<String, List<String>>();
		Map<String, List<String>> vendorInvTrkMap = new HashMap<String, List<String>>();
		List<String> invalidProds = new ArrayList<String>();
		List<String> invalidVendProds = new ArrayList<String>();		
		
		for (VendorProductVO vendorProductVO : vpVOList) {
			
			// Perform make available only when product type is valid.
			if (StringUtils.isEmpty(vendorProductVO.getProdType())
					|| !(GeneralConstants.OE_PRODUCT_TYPE_FRECUT.equals(vendorProductVO.getProdType())
					|| GeneralConstants.OE_PRODUCT_TYPE_SPEGFT.equals(vendorProductVO.getProdType())
					|| GeneralConstants.OE_PRODUCT_TYPE_SDG.equals(vendorProductVO.getProdType()) 
					|| GeneralConstants.OE_PRODUCT_TYPE_SDFC.equals(vendorProductVO.getProdType()))) {
				
				if(!invalidProds.contains(vendorProductVO.getProductSubcodeId())) {
					invalidProds.add(vendorProductVO.getProductSubcodeId());
					// TO-DO, not a proper error message, req should be modified to include product Id to this message?
					errorMessage.append("Product type is invalid.\n");
				}
				
				continue;
			}
			
			if (VendorProductAvailabilityConstants.ACTIVE.equals(vendorProductVO.getRemoved())) {
				if(!invalidVendProds.contains(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId())) {
					invalidVendProds.add(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId());
					// TO-DO, not a proper error message, req should be modified to include product, vendor Ids to this message?
					errorMessage.append("This vendor is not assigned to this product.\n");
				}
				continue;
			} 
			
			// Make inventory Available, if it is not in Available status.
			if(!VendorProductAvailabilityConstants.AVAILABLE.equals(vendorProductVO.getInvStatus())){
				if (vendorInvTrkMap.get(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId()) != null){
					if(!vendorInvTrkMap.get(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId()).contains(vendorProductVO.getInvTrkId())){
						vendorInvTrkMap.get(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId()).add(vendorProductVO.getInvTrkId());
					}
				} else{
					List<String> invsToUpdate = new ArrayList<String>();
					invsToUpdate.add(vendorProductVO.getInvTrkId());
					vendorInvTrkMap.put(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId(), invsToUpdate);
				}
			} else{
				logger.debug("Inventory - " + vendorProductVO.getInvTrkId() + ", is in available state. Do not perform any update.");
			}			
			
			if(prodVendorMap.get(vendorProductVO.getProductSubcodeId()) != null) {
				if(!prodVendorMap.get(vendorProductVO.getProductSubcodeId()).contains(vendorProductVO.getVendorId())) {
					prodVendorMap.get(vendorProductVO.getProductSubcodeId()).add(vendorProductVO.getVendorId());
				} 
			} else {
				List<String> vendors = new ArrayList<String>();
				vendors.add(vendorProductVO.getVendorId());
				prodVendorMap.put(vendorProductVO.getProductSubcodeId(), vendors);
			}			
		}
		
		// no need of the below data anymore.
		invalidProds = null;
		invalidVendProds = null;
		
		Iterator<Entry<String, List<String>>> prodVPIt = prodVendorMap.entrySet().iterator();		
		String productId = null;
		
		while (prodVPIt.hasNext()) {
			try {				
				userTransaction = Transaction.getTransaction();
				userTransaction.begin();
				Map.Entry<String, List<String>> prodVPEntry = (Map.Entry<String, List<String>>) prodVPIt.next();
				
				productId = prodVPEntry.getKey();
				List<String> vendors = prodVPEntry.getValue();
				
				int count = 1;				
				boolean updateProduct = false;
				
				for (String vendorId : vendors) {
					if(count == vendors.size()) {
						updateProduct = true;
					}		
					count++;					
					VendorProductAvailabilityUtility.makeAvailable(productId, vendorId, vendorInvTrkMap.get(productId + "_" +vendorId), 
							updateProduct,  csrId, con);					
					errorMessage.append(productId).append(" -- ").append(vendorId).append(" - Product/Vendor was successfully updated. \n");
				}
	
				// call feed utility
				if (envKeys.size() > 0) {
					resVO = feedUtil.sendProductFeed(con, productId, envKeys);
					errorMessage.append(resVO.getErrorString()).append("\n"); 
					if (!resVO.isSuccess()) {
						throw new Exception();
					}
				}
			
				userTransaction.commit();
				
			} catch (Exception e) {					
				Transaction.rollback(userTransaction);	
				errorMessage.append(productId).append(" could not be updated \n");
			}
		}
     
     pageData.put("errorMessage", errorMessage.toString());
     processSearch(inputMap, responseMap, pageData, con);
     
   }

  	/** Process action/ request to make inventory/ vendor product/ product unavailable
	 * @param inputMap
	 * @param responseMap
	 * @param pageData
	 * @param con
	 * @throws Exception
	 */
	public void processMU(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con) throws Exception {
		long muReqStartTime = System.currentTimeMillis();
		@SuppressWarnings("unchecked")
		List<String> invTrkIds = (ArrayList<String>) inputMap.get("requestInvTrkIds");
		List<VendorProductVO> vpVOList = retrieveProductsVendorsByInvTrkIds(invTrkIds, con);
		
		String csrId = (String) inputMap.get("csrId");			
		NovatorFeedResponseVO resVO;
		UserTransaction userTransaction = null;
		
		NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();		
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		String myBuysFeedEnabled = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");

		// build the environment array for product feed utility
		NovatorUpdateVO novatorUpdateVO = (NovatorUpdateVO) inputMap.get("requestNovatorUpdateVO");
		List<String> envKeys = new ArrayList<String>();
		if (novatorUpdateVO.isUpdateContent()) envKeys.add(NovatorFeedUtil.CONTENT);
		if (novatorUpdateVO.isUpdateLive()) envKeys.add(NovatorFeedUtil.PRODUCTION);
		if (novatorUpdateVO.isUpdateTest()) envKeys.add(NovatorFeedUtil.TEST);
		if (novatorUpdateVO.isUpdateUAT()) envKeys.add(NovatorFeedUtil.UAT);

		boolean myBuysFeedError = false;
		
		// Construct a Map separating a List of vendors for a product.
		Map<String, List<String>> prodVendorMap = new HashMap<String, List<String>>();
		Map<String, List<String>> vendorInvTrkMap = new HashMap<String, List<String>>();
		for (VendorProductVO vendorProductVO : vpVOList) {
			
			// Make inventory Unavailable, if it is in Available status.
			if(VendorProductAvailabilityConstants.AVAILABLE.equals(vendorProductVO.getInvStatus())){
				if (vendorInvTrkMap.get(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId()) != null){
					if(!vendorInvTrkMap.get(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId()).contains(vendorProductVO.getInvTrkId())){
						vendorInvTrkMap.get(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId()).add(vendorProductVO.getInvTrkId());
					}
				} else{
					List<String> invsToUpdate = new ArrayList<String>();
					invsToUpdate.add(vendorProductVO.getInvTrkId());
					vendorInvTrkMap.put(vendorProductVO.getProductSubcodeId() + "_" + vendorProductVO.getVendorId(), invsToUpdate);
				}
			} else{
				logger.debug("Inventory - " + vendorProductVO.getInvTrkId() + ", is not available. Do not perform any update.");
			}
			
			// If vendor product is active (is marked as 'Y'), make it Unavailable (mark as 'N').
			if(prodVendorMap.get(vendorProductVO.getProductSubcodeId()) != null) {
				if(!prodVendorMap.get(vendorProductVO.getProductSubcodeId()).contains(vendorProductVO.getVendorId())) {
					prodVendorMap.get(vendorProductVO.getProductSubcodeId()).add(vendorProductVO.getVendorId());
				} 
			} else {
				List<String> vendors = new ArrayList<String>();
				vendors.add(vendorProductVO.getVendorId());
				prodVendorMap.put(vendorProductVO.getProductSubcodeId(), vendors);
			}			
		}

		// Step 1: For each product check Product and vendor product availability.
		Iterator<Entry<String, List<String>>> prodVPIt = prodVendorMap.entrySet().iterator();
		StringBuffer message = new StringBuffer();
		String productId = null;
		
		while (prodVPIt.hasNext()) {
			try {
				
				userTransaction = Transaction.getTransaction();
				userTransaction.begin();
				Map.Entry<String, List<String>> prodVPEntry = (Map.Entry<String, List<String>>) prodVPIt.next();
				
				productId = prodVPEntry.getKey();
				List<String> vendors = prodVPEntry.getValue();			
				int count = 1;
				
				boolean performProdShutdownCheck = false;
				boolean shutdownPerformed = false;
				
				for (String vendorId : vendors) {
					if(count == vendors.size()) {
						performProdShutdownCheck = true;
					}
					
					// Step 2: Make each inventory for a given vendor as Unavailable.
					if(vendorInvTrkMap.get(productId + "_" + vendorId) != null) {
						VendorProductAvailabilityUtility.makeInvUnavailable(vendorInvTrkMap.get(productId + "_" +vendorId), con);
					} else {
						logger.debug("Error While Retrieving vendor Id from the Map");
					}
					
					count++;
					
					shutdownPerformed = VendorProductAvailabilityUtility.makeVendorProdUnavailable(productId, vendorId, performProdShutdownCheck, csrId, con);	
					message.append(productId).append(" -- ").append(vendorId).append(" - Product/Vendor was successfully updated. \n");
				}
			
				// Existing logic to send Product Feed, My Buys feed
				if (shutdownPerformed) {
    				try {
    					feedUtil.updateUpsellsForDisabledProduct(con, productId, envKeys);
    				} catch (Exception e1) {
    					logger.error("Error updating upsells for product " + productId + ". " + e1);
    					throw e1;
    				}
				}

				// call feed utility
				if (envKeys.size() > 0) {
					resVO = feedUtil.sendProductFeed(con, productId, envKeys);
					message.append(resVO.getErrorString()).append("\n"); 
					if (!resVO.isSuccess()) {
						throw new Exception();
					}
				}
			
				if (StringUtils.equalsIgnoreCase(myBuysFeedEnabled, "Y")) {
					try {
						logger.debug("Calling sendMyBuys");
						sendMyBuys(productId);
						logger.debug("Succesfull in sending MyBuys message() for order_processing_maintenance for productSubCode id: "
								+ productId);
					} catch (Exception e) {
						myBuysFeedError = true;
						logger.error("Error in sending MyBuys message() for productSubCode id. " + e);
					}
				}

				userTransaction.commit();
				
			} catch (Exception e) {					
				Transaction.rollback(userTransaction);	
				message.append(productId).append(" could not be updated \n");
			}
		}

		if (myBuysFeedError) {
			message.append("My Buys feed failure - please contact Apollo Support\n");
		}
		
		pageData.put("errorMessage", message.toString());		
		logger.debug("Time Taken to complete make unavailable request : " + (System.currentTimeMillis() - muReqStartTime));	
		logger.debug("processMU() completed -> Redirecting to process search");
		processSearch(inputMap, responseMap, pageData, con);
		logger.debug("Total time Taken to complete ProcessMU request : " + (System.currentTimeMillis() - muReqStartTime));	

	}

  /*******************************************************************************************
  * processEdit()
  ******************************************************************************************
  *
  */
  public void processEdit(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    List   invTrkIds = (ArrayList) inputMap.get("requestInvTrkIds"); 
    String invTrkId = (String) invTrkIds.get(0);
    String securityToken = (String) inputMap.get("requestSecurityToken"); 
    String csrId = (String) inputMap.get("csrId"); 
    String lockedBy = retrieveLock(invTrkId, securityToken, csrId, con); 
    
    if (lockedBy == null || lockedBy.equalsIgnoreCase(""))
    {
      pageData.put("forwardToType", "ACTION");
      pageData.put("forwardToName", "maintenance");
    }
    else
    {
      pageData.put("errorMessage", "This record is currently being viewed by " + lockedBy + ".  Please try again later. ");
      processSearch(inputMap, responseMap, pageData, con);
    }

    return; 
  }


  /*******************************************************************************************
  * processMainMenu()
  ******************************************************************************************
  *
  */
	public void processMainMenu(HashMap pageData) throws Exception {
		pageData.put("forwardToType", "ACTION");
		pageData.put("forwardToName", "MainMenuAction");
		return;
	}

  /*******************************************************************************************
  * processUpload()
  ******************************************************************************************
  *
  */
  public void processUpload(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    String errorMessage = ""; 
    List requestUploadList = (List) inputMap.get("requestUploadList");
    int successCount = 0; 
    int errorCount = 0; 

    if (requestUploadList.size() < 1)
    {
      errorMessage += "Spreadsheet input must contain at least one data row."; 
    }
    else
    {
      //Define the format
      SimpleDateFormat sdf = new SimpleDateFormat(this.INPUT_DATE_FORMAT);

      String productId = null;
      String vendorId = null;
      String startDate = null;
      String endDate = null;
      String forecast = null;
      String warningThreshold = null;
      String shutdownThreshold = null;
      String comments = null;
      int i = 0; 
      String csrId = (String) inputMap.get("csrId");
      String error; 

      for (i = 0; i < requestUploadList.size(); i++)
      {
        try
        {
          productId = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_PRODUCT_ID).getValue();
          vendorId = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_VENDOR_ID).getValue();
          startDate = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_START_DATE).getValue();
          endDate = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_END_DATE).getValue();
          forecast = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_FORECAST_QTY).getValue();
          warningThreshold = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_WARNING_THRESHOLD).getValue();
          shutdownThreshold = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_SHUTDOWN_THRESHOLD).getValue();          
          comments  = ((InventoryTrackingUploadSpreadsheetRowVO) requestUploadList.get(i)).getFieldByName(InventoryTrackingUploadSpreadsheetRowVO.FIELD_NAME_COMMENTS).getValue();
    
          logger.debug("Sku IT upload - productId = " + productId + " and vendorId = " + vendorId + " and startDate = " + startDate + " and endDate = " + endDate + " and forecast = " + forecast + " and warningThreshold = " + warningThreshold + " and shutdownThreshold = " + shutdownThreshold + " and comments = " + comments); 

          if (productId == null || productId.equalsIgnoreCase(""))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Product ID is missing" + "\n"; 
            errorCount++; 
          }
          else if (vendorId == null || vendorId.equalsIgnoreCase(""))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Vendor ID is missing" + "\n"; 
            errorCount++; 
          }
          else if (startDate == null || startDate.equalsIgnoreCase(""))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Start Date is missing" + "\n"; 
            errorCount++; 
          }
          else if (endDate == null || endDate.equalsIgnoreCase(""))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " End Date is missing" + "\n"; 
            errorCount++; 
          }
          else if (forecast == null || forecast.equalsIgnoreCase(""))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Forecast Quantity is missing" + "\n"; 
            errorCount++; 
          }
          else if (!StringUtils.isNumeric(forecast))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - Forecast Quantity must be numeric" + "\n"; 
            errorCount++; 
          }
          else if (!StringUtils.isNumeric(warningThreshold))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - Warning Threshold must be numeric" + "\n"; 
            errorCount++; 
          }
          else if (shutdownThreshold == null || shutdownThreshold.equalsIgnoreCase(""))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Shutdown Threshold is missing" + "\n"; 
            errorCount++; 
          }
          else if (!StringUtils.isNumeric(shutdownThreshold))
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - Shutdown Threshold must be numeric" + "\n"; 
            errorCount++; 
          }
          else if (!(DATE_PATTERN.matcher(startDate)).matches())
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - Start date must be in mm/dd/yyyy format" + "\n"; 
            errorCount++; 
          }
          else if (!(DATE_PATTERN.matcher(endDate)).matches())
          {
            errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - End date must be in mm/dd/yyyy format" + "\n"; 
            errorCount++; 
          }
          else
          {
            //Create a Calendar Object for start and end dates
            Calendar cStartDate = Calendar.getInstance();
            Calendar cEndDate = Calendar.getInstance();
        
            //Set the Calendar Object using the date retrieved in startDate
            cStartDate.setTime(sdf.parse(startDate));

            //Set the Calendar Object using the date retrieved in endDate
            cEndDate.setTime(sdf.parse(endDate));

            if (cEndDate.compareTo(cStartDate) < 0)
            {
              errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - Start Date cannot be greater than End Date" + "\n"; 
              errorCount++; 
            }
            else
            {
              String newInvTrkId = createInventoryInfo(productId.toUpperCase(), vendorId, cStartDate, cEndDate, forecast, warningThreshold, shutdownThreshold, comments, csrId, con, pageData);
              if (newInvTrkId != null && !newInvTrkId.equalsIgnoreCase(""))  
                successCount++; 
              else
              {
                error = (String) pageData.get("errorMessage");
                logger.error("Sku IT upload encountered an error - " + error);
                errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found - " + error + "\n"; 
                errorCount++; 
              }
            }
          }
        }
        catch (Exception e)
        {
          logger.error("Sku IT upload - Error found on row#" + (i+2));
          logger.error(e);
          errorMessage += "<<ERROR>> row#" + (i+2) + " Invalid data found" + "\n"; 
          errorCount++; 
        }
  
      }

    }

    pageData.put("successCount", (new Integer(successCount)).toString());
    pageData.put("errorCount", (new Integer(errorCount)).toString());
    pageData.put("errorMessage", errorMessage);
    processLoad(responseMap, pageData, con);
    
  }


  /*******************************************************************************************
  * retrieveVendorsVOList()
  ******************************************************************************************
  *
  */
  private List retrieveVendorsVOList(Connection con)
    throws Exception
  {
    List voList = new ArrayList(); 

    //Instantiate VendorDAO
    VendorDAO vDAO = new VendorDAO(con);
    CachedResultSet crs = vDAO.getVendorListCRS();

    while(crs.next())
    {
      VendorMasterVO vmVO = new VendorMasterVO(); 

      vmVO.setVendorID(crs.getString("vendor_id"));

      if (crs.getString("vendor_name") != null)
        vmVO.setVendorName(DOMUtil.encodeChars(crs.getString("vendor_name")));

      if (crs.getString("city") != null)
        vmVO.setCity(DOMUtil.encodeChars(crs.getString("city")));

      if (crs.getString("email") != null)
        vmVO.setEmail(DOMUtil.encodeChars(crs.getString("email")));

      vmVO.setState(crs.getString("state"));
      vmVO.setPhone(crs.getString("phone"));
      vmVO.setZoneJumpEligibleFlag(crs.getString("zone_jump_eligible_flag"));
      vmVO.setDaysBlocked(crs.getString("days_blocked"));

      voList.add(vmVO);
            
    }

    return voList; 
  }


  /*******************************************************************************************
  * retrieveProductsByInvTrkIds()
  ******************************************************************************************
  *
  */
  private List retrieveProductsByInvTrkIds(List invTrkIds, Connection con)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);
    List productIds = itDAO.getProductsByInvTrkIds(invTrkIds);

    return productIds; 
  }


  /*******************************************************************************************
  * retrieveProductsVendorsByInvTrkIds()
  ******************************************************************************************
  *
  */
  private List<VendorProductVO> retrieveProductsVendorsByInvTrkIds(List<String> invTrkIds, Connection con)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);
    List vpVOList = itDAO.getProductsVendorsByInvTrkIds(invTrkIds);

    return vpVOList; 
  }


  /*******************************************************************************************
  * retrieveProductsByInvTrkIds()
  ******************************************************************************************
  *
  */
  private void retrieveOrdersTaken(String productId, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);
    HashMap ordersTakenResults = itDAO.getOrdersTaken(productId);

    Document ordersTakenXML = CommonUtil.buildXML(ordersTakenResults, "OUT_CURSOR", "orders_taken", "order_taken", "element");
    responseMap.put("HK_orders_taken", ordersTakenXML);

    //Store the outWeekTotal in the pageData hashmap
    int outWeekTotal = 0;
    if (ordersTakenResults.get("OUT_WEEK_TOTAL") != null)
      outWeekTotal = new Integer(ordersTakenResults.get("OUT_WEEK_TOTAL").toString()).intValue();
    pageData.put("weekTotal", outWeekTotal);

  }


  /*******************************************************************************************
  * convertVendorVOListToXML()
  ******************************************************************************************
  *
  *
  */
  private Document convertVendorVOListToXML(List vendorsVOList, String topNode)
    throws Exception
  {

    Document parser;
    VendorMasterVO vmVO = null;
    Document vendorXML = DOMUtil.getDefaultDocument();

    //generate the top node
    Element vendorElement = vendorXML.createElement(topNode);
    //and append it
    vendorXML.appendChild(vendorElement);

    int elementNum = 0;
    for (int x = 0; x < vendorsVOList.size(); x++)
    {
      elementNum++;
      vmVO = (VendorMasterVO) vendorsVOList.get(x);
      parser = DOMUtil.getDocument(vmVO.toXML(elementNum));
      vendorXML.getDocumentElement().appendChild(vendorXML.importNode(parser.getFirstChild(), true));
    }

    return vendorXML;

  }

  /*******************************************************************************************
  * retrieveLock()
  ******************************************************************************************
  *
  *
  */
  private String retrieveLock(String entityId, String sessionId, String csrId, Connection con)
    throws Exception
  {
    String lockedBy = LockUtil.getLock(con, OPMConstants.INV_TRK_ENTITY_TYPE, entityId, sessionId, csrId);
    return lockedBy; 

  }

  /*******************************************************************************************
  * deleteInventory()
  ******************************************************************************************
  *
  *
  */
  private String deleteInventory(String invTrkId, Connection con)
    throws Exception
  {
    String message = ""; 
    
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);

    String deleteResults = itDAO.deleteInventoryInfo(invTrkId);

    if (deleteResults != null && !deleteResults.equalsIgnoreCase(""))
      message = "Inventory Tracking record #" + invTrkId + " could not be deleted because " + deleteResults.substring(1,50);     
    else 
      message = "Inventory Tracking record #" + invTrkId + " was successfully deleted";

    return message; 

  }


  /*******************************************************************************************
  * updateShutdownThreshold()
  ******************************************************************************************
  *
  *
  */
  private String updateShutdownThreshold(String productId, String vendorId, List invTrkIds, String csrId, Connection con)
    throws Exception
  {
    String message = ""; 
    
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);
    String updateResults = itDAO.updateShutdownThreshold(productId, vendorId, csrId, invTrkIds);
    
    if (updateResults != null && !updateResults.equalsIgnoreCase(""))
      message = productId + " and " + vendorId + " could not be deleted because " + updateResults.substring(1,50);     
    else 
      message = productId + " and " + vendorId + " was successfully deleted";     

    return message; 

  }

  /*******************************************************************************************
  * createInventoryInfo()
  ******************************************************************************************
  *
  *
  */
  private String createInventoryInfo(String productId, String vendorId, Calendar cStartDate, Calendar cEndDate, 
                                     String forecast, String warningThreshold, String shutdownThreshold, 
                                     String comments, String csrId, Connection con, HashMap pageData)
    throws Exception
  {
    //Instantiate InventoryTrackingDAO
    InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);

    //CachedResultSet that will contain the output from the stored proce
    String invTrkId = itDAO.createInventoryInfo(productId, vendorId, cStartDate, cEndDate, forecast, warningThreshold, shutdownThreshold, 
                                                comments, csrId, pageData);

    return invTrkId;

  }

    /**
     * calls the MDB for sending JMS message to myBuys queue
     * @param String productID of object for whom messages need to be sent
     * @return void
     */
      
    public void sendMyBuys(String productId) throws Exception
    {

         MessageToken messageToken = new MessageToken();
         messageToken.setMessage(productId);
         messageToken.setStatus("MY BUYS");
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessage(new InitialContext(),messageToken);
        
    }
    
    /**********************************12863 - NEW SKU IT CHANGES*******************************************/
    public final static String SEPERATOR = ",";
    public final static String QUOTE = "'";
    public final static String OPM_INVENTORY_CTXT = "OPM_INV_CTXT";
	/**Refined Process of inventory search, includes page details. 
	 * @param inputMap
	 * @param responseMap
	 * @param pageData
	 * @param con
	 * @throws Exception
	 */
	public void processSearch(HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con) throws Exception {
		// As the first step set forward to page, any exception caught in processing search request, will redirect to dashboard screen.
		long startTime = System.currentTimeMillis();
		try{
			pageData.put("forwardToType", "XSL");
			pageData.put("forwardToName", "dashboard");
			
			InventorySearchCriteriaVO searchVO = buildSearchVO(inputMap);			
			PaginationVO pageDetail = null;
			int currentPage = 0;
			int recordsPerPage = 0;
			int totalRecords = 0;
			
			if(!StringUtils.isEmpty((String)inputMap.get("hCurrentpage"))) {
				currentPage = Integer.parseInt((String)inputMap.get("hCurrentpage"));	
			}
			if(!StringUtils.isEmpty((String)inputMap.get("pageSize"))) {
				recordsPerPage = Integer.parseInt((String)inputMap.get("pageSize"));
			}
			if(!StringUtils.isEmpty((String)inputMap.get("hRecordCount"))) {
				totalRecords = Integer.parseInt((String)inputMap.get("hRecordCount"));
			}
			
			
			pageDetail = new PaginationUtil().getPaginationVO(currentPage, recordsPerPage, OPM_INVENTORY_CTXT);
			pageDetail.setTotalRecords(totalRecords);
			
			searchVO.setPageDetail(pageDetail);
			retrieveInventory(searchVO, responseMap, pageData, con);			
		} catch (Exception e) {
			pageData.put("errorMessage", "Unable to process search request.\n " + e.getMessage());			
		}
		processVendors(inputMap, responseMap, con);
		long endTime = System.currentTimeMillis();
		logger.debug("Time taken to process complete search request : " + (endTime - startTime) + " milli seconds");
		return;
	}	

	/**
	 * Builds the search VO using the requested search parameters.
	 * 
	 * @param inputMap
	 * @return
	 */
	private InventorySearchCriteriaVO buildSearchVO(HashMap inputMap) {
		SimpleDateFormat sdf = new SimpleDateFormat(this.INPUT_DATE_FORMAT);
		InventorySearchCriteriaVO searchVO = new InventorySearchCriteriaVO();
		Calendar calendar = Calendar.getInstance();
		try {
			if (inputMap.get("requestStartDate") != null) {
				calendar.setTime(sdf.parse((String) inputMap.get("requestStartDate")));
				searchVO.setcStartDate(new java.sql.Date(calendar.getTimeInMillis()));
			}
			if (inputMap.get("requestEndDate") != null) {
				calendar.setTime(sdf.parse((String) inputMap.get("requestEndDate")));
				searchVO.setcEndDate(new java.sql.Date(calendar.getTimeInMillis()));
			}
			
			searchVO.setProductStatus((String) inputMap.get("requestProductStatus"));
			searchVO.setVendorStatus((String) inputMap.get("requestVendorStatus"));
			searchVO.setVendorProductStatus((String) inputMap.get("requestVendorProductStatus"));
			
			if (inputMap.get("requestOnHand") != null
					&& StringUtils.isNumeric((String) inputMap.get("requestOnHand"))) {
				searchVO.setOnHand((String) inputMap.get("requestOnHand"));
			}
			searchVO.setProductIdsStr(getListAsString((ArrayList) inputMap.get("requestProductIds"), SEPERATOR, QUOTE));
			searchVO.setVendorIdsStr(getListAsString((ArrayList) inputMap.get("requestVendorIdsChosen"),SEPERATOR, QUOTE));
		} catch (Exception e) {
			logger.error("Unable to create search VO - " + e.getMessage());
		}
		return searchVO;
	}
	
	/** Retrieve inventory tracking records
	 * @param searchVO
	 * @param responseMap
	 * @param pageData
	 * @param con
	 * @throws Exception
	 */
	private void retrieveInventory(InventorySearchCriteriaVO searchVO,
			HashMap responseMap, HashMap pageData, Connection con) throws Exception {
		
	    HashMap searchResults = new HashMap();
	    CachedResultSet result = null;
	    try {
	    	long startTime = System.currentTimeMillis();
	    	searchResults = new InventoryTrackingDAO(con).searchInventory(searchVO);
	    	if(!StringUtils.isEmpty((String)searchResults.get("OUT_MESSAGE"))) {	    		
	    		throw new Exception((String)searchResults.get("OUT_MESSAGE"));
	    	}	    	
	    	long endTime = System.currentTimeMillis();
	    	logger.debug("Time taken to retrieve the inventory data: " + (endTime - startTime) + " milli seconds");
	    } catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	    
	    int recCount = ((BigDecimal)searchResults.get("INOUT_TOT_REC_CNT")).intValue();
	    
	   if(recCount > 0 && searchResults.get("OUT_CUR") != null) {
    		result = ((CachedResultSet)searchResults.get("OUT_CUR"));    		
    		if(result.next() && result.getString("1") == null) {
    			result.reset();
    			Document searchResultsXML = CommonUtil.buildXML(searchResults, "OUT_CUR", "search_results", "search_result", "element");
    			responseMap.put("HK_search_results", searchResultsXML);    			
    		}
    	}	   

	    //Store the count in the pageData hash map
	    int totalRecords = searchVO.getPageDetail().getTotalRecords();
	    int totalPages = 0;
	    if (searchResults.get("INOUT_TOT_REC_CNT") != null) {
	      totalRecords = ((BigDecimal)searchResults.get("INOUT_TOT_REC_CNT")).intValue();
	    }
	    logger.debug("Total inventory Records count fetched as: " + totalRecords);
	    if(totalRecords > 0) {
	    	int currPageSize = searchVO.getPageDetail().getPageSize();
	    	int tempTotalPages = totalRecords/currPageSize;	    	  
	    	totalPages = totalRecords%currPageSize == 0 ? tempTotalPages : tempTotalPages+1;	  
	 	    pageData.put("currentPage", searchVO.getPageDetail().getCurrentPage());
	 	    pageData.put("currPageSize", currPageSize);
	 	    pageData.put("startRecord", searchVO.getPageDetail().getStartRecord());
	    } 	    
	    pageData.put("recordCount", totalRecords);	   
	    pageData.put("totalPages", totalPages);		    
	}

	/**
	 * @param list
	 * @return
	 */
	private String getListAsString(List<String> list, String seperator, String quote) {
		StringBuffer string = new StringBuffer();
		if (list != null && list.size() > 0) {
			int count = 1;
			for (String element : list) {				
				string.append(quote);				
				string.append(element);
				string.append(quote);

				if (count != list.size()) {
					string.append(",");
				}
				count++;
			}
		}
		return string.toString();
	}
	

	/** Get the product Ids for the given inventory tracking Ids.
	 * @param invTrkIds
	 * @param con
	 * @return
	 * @throws Exception
	 */
	private List<String> retrieveProductsInvTrkIds(List invTrkIds, Connection con) throws Exception {		
		InventoryTrackingDAO itDAO = new InventoryTrackingDAO(con);
		List<VendorProductVO> vpVOList = itDAO.getProductsVendorsByInvTrkIds(invTrkIds);
		List products = new ArrayList<String>();
		for (VendorProductVO vpVO : vpVOList) {
			if(!products.contains(vpVO.getProductSubcodeId())) {
				products.add(vpVO.getProductSubcodeId());
			}
		}
		return products;
	}
	
	/**********************************12863 - NEW SKU IT CHANGES END HERE *******************************************/
	

}