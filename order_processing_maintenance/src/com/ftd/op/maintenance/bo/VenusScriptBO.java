package com.ftd.op.maintenance.bo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.AddOnDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.OrderDAO;
import com.ftd.op.maintenance.util.MessagingServiceLocator;
import com.ftd.op.maintenance.vo.AddonVO;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.CustomerVO;
import com.ftd.op.maintenance.vo.MessageOrderStatusVO;
import com.ftd.op.maintenance.vo.OrderDetailVO;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.op.maintenance.vo.VenusMessageVO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.core.domain.ProductAvailVO;


public class VenusScriptBO
{
    //private static final String POC_EMAIL_TYPE = "Email";
    private static final String PARAM_EXTERNAL_ORDER_NUMBER = "external_order_number";
    private static final String PARAM_ENTERED_SQL = "sql";
    //private static final String CANADA_COUNTRY_CODE = "CA";
    //private static final String US_COUNTRY_CODE = "US";
    private static final String ORDER_PROCESSING_COMMENT_ORIGIN = "OrderProc";
    private static final String COMMENT_TYPE_ORDER = "Order";
    private static final String ORDER_DELIMETER = " ";
    private static final String VENUS_EXCEPTION = "VenusException:";
    public static final String ERROR_TEXT = "Error - ";
    public static final String FATAL_TEXT = "Fatal - ";
    private Logger logger;


    public VenusScriptBO()
    {
        logger = new Logger("com.ftd.op.maintenance.action.VenusScriptBO");
    }

    /*
     * Execute sql to get a list of external order numbers
     */
     public String getOrders(Connection conn, String csr,HttpServletRequest request)
     {
        StringBuffer buffer = new StringBuffer("");

        try
        {
            //get values from request
            String enteredSQL = request.getParameter(PARAM_ENTERED_SQL);

            Statement stmt = conn.createStatement();
            ResultSet rs;

            rs = stmt.executeQuery(enteredSQL);
            while ( rs.next() ) {
                String orderNumber = rs.getString("external_order_number");
                if(buffer.length() > 0)
                {
                    buffer.append(ORDER_DELIMETER);
                }
                buffer.append(orderNumber) ;
            }

        }
        catch(SQLException e)
        {
            logger.error(e);
            return FATAL_TEXT + "Error with SQL. " + e.toString();
        }
        catch(Throwable t)
        {
            logger.error(t);
            return FATAL_TEXT + "Application Error.  " + t.toString();
        }
        return buffer.toString();
     }


     private String getENumber(Connection conn, String venusId) throws Exception
     {
         OrderDAO orderDAO = new OrderDAO(conn);
         VenusMessageVO vo = orderDAO.getVenusMessageById(venusId);
         return vo.getVenusOrderNumber();
     }

    /*
     *
     */
	@SuppressWarnings("rawtypes")
	public String changeDeliveryDate(Connection conn, String csr,
			HttpServletRequest request, boolean emailCustomer) throws Exception { 
		try {

			
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			int maxDelDayPush = Integer.parseInt(configUtil.getProperty(
					OPMConstants.PROPERTY_FILE,
					"VENUS_SCRIPT_MAX_DAYS_PUSH_DEL_DATE"));

			// needed DAOs
			OrderDAO orderDAO = new OrderDAO(conn); 

			// get values from request
			String externalOrderNumber = request
					.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);

			// get order detail id
			Map orderIdMap = orderDAO.findOrderNumber(externalOrderNumber);
			if (orderIdMap == null) {
				return ERROR_TEXT + externalOrderNumber
						+ " Order number not found.";
			}
			String orderDetailId = null;
			try {
				orderDetailId = (orderIdMap.get("OUT_ORDER_DETAIL_ID"))
						.toString();
			} catch (Exception ex) {
				return ERROR_TEXT + externalOrderNumber
						+ " Order number not found.";
			}

			// get the order detail record
			OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);

			// do some basic validation
			String validateResults = validateOrder(request, orderDetailVO);
			if (validateResults != null) {
				return validateResults;
			}

			// get the customer
			CustomerVO recipient = orderDAO.getCustomer(orderDetailVO
					.getRecipientId());

			// get lastFTD
			VenusMessageVO lastFTD = getLastFTD(conn, orderDetailId);
			
			AddOnDAO addOnDao = new AddOnDAO(conn);
			
			
			@SuppressWarnings("unchecked")
			List<AddonVO> addOnVOs = addOnDao.getAddOns(orderDetailId);
			List<String> addOns = null;
			
			if(addOnVOs != null){
				addOns = new ArrayList<String>();
				for(AddonVO addOnVO : addOnVOs){
					if(addOnVO.getAddOnCode() != null && addOnVO.getAddOnCode()!="")
					addOns.add(addOnVO.getAddOnCode());
				}
				logger.info("VenusScriptBO-changeDeliveryDate : AddOns = "+addOns.toString());
			}
			
			
			logger.info("VenusScriptBo-changeDeliveryDate : Calling PAS - getNextAvailableDeliveryDate - START");
			// get delivery date for this ship method
			Date oeDate = PASServiceUtil.getNextAvailableDeliveryDate(orderDetailVO.getProductId(), addOns, recipient.getZipCode(), recipient.getCountry(), orderDetailVO.getSourceCode());

			logger.info("VenusScriptBo-changeDeliveryDate : Calling PAS - getNextAvailableDeliveryDate - END, Date from PAS : "
					+ oeDate);
			String newShipMethod = null;

			if (oeDate == null) {
				return ERROR_TEXT + orderDetailVO.getExternalOrderNumber()
						+ " No next available ship date.";

			}
			// get the difference in days between requested and returned
			// delivery date
			Date foundDate = oeDate;
			long diffMillis = foundDate.getTime()
					- lastFTD.getDeliveryDate().getTime();
			int diffDays = new Long(diffMillis / (24 * 60 * 60 * 1000))
					.intValue() + 5;

			// only change del date if within range
			if (diffDays > maxDelDayPush) {
				return ERROR_TEXT + orderDetailVO.getExternalOrderNumber()
						+ " Order cannot be delivered within " + maxDelDayPush
						+ " days.";
			} else {

				logger.info("VenusScriptBo-changeDeliveryDate : Calling PAS - getProductAvailability - START");

				ProductAvailVO prodAvailVO = PASServiceUtil.getProductAvailShipData(orderDetailVO.getProductId(), oeDate,
						recipient.getZipCode(), recipient.getCountry(), null, null, addOns, orderDetailVO.getSourceCode());			
														

				logger.info("VenusScriptBo-changeDeliveryDate : Calling PAS - getProductAvailability - END");

				if (prodAvailVO != null && prodAvailVO.getDeliveryDate() != null) {
					Date shipDate = null;
					if (prodAvailVO.getShipDate() != null) {
						SimpleDateFormat requestFormat = new SimpleDateFormat("MM/dd/yyyy");
						shipDate = requestFormat.parse(prodAvailVO.getShipDate());
					}
					newShipMethod = prodAvailVO.getShipMethod(); 
					logger.info("newShipMethod: " + newShipMethod);
					// cancel original order
					ResultTO resultsTO = cancelOrder(conn, csr, lastFTD, orderDetailId);
					logger.info("cancel Order resultsTO Error?: " + resultsTO.getErrorString() + ", " + resultsTO.isSuccess());
					String cancelledVenusNumber = resultsTO.getKey();
					if (resultsTO.isSuccess()) {
						// create a new FTD based off the old message and send
						// out a
						// new order
						OrderTO orderTO = createFTDOrderMessageTO(lastFTD,
								recipient, csr, foundDate, shipDate,
								newShipMethod);
						resultsTO = resendOrder(orderTO, conn);
						logger.info("resend Order resultsTO Error?: " + resultsTO.getErrorString() + ", " + resultsTO.isSuccess());
						if (resultsTO.isSuccess()) {
							String newENumber = getENumber(conn,
									resultsTO.getKey());

							String formatttedOrigDate = FieldUtils
									.formatUtilDateToString(lastFTD
											.getDeliveryDate());
							String formatttedNewDate = FieldUtils
									.formatUtilDateToString(foundDate);

							// add comment to order
							String comment = "Sent CAN on order "
									+ cancelledVenusNumber + ".  Resent as "
									+ newENumber
									+ ".  Delivery date changed from "
									+ formatttedOrigDate + " to "
									+ formatttedNewDate;

							createComment(conn, comment, orderDetailVO);

							// check if customer should be emailed
							if (emailCustomer) {
								// send email - Commented below method as this
								// flow
								// never going to be touched
								// sendDeliveryDateEmail(conn,orderDetailVO,formatttedOrigDate,formatttedNewDate);
							}

							return "Success - "
									+ orderDetailVO.getExternalOrderNumber()
									+ " Date changed from" + formatttedOrigDate
									+ "(" + lastFTD.getShipMethod() + ") to "
									+ formatttedNewDate + "(" + newShipMethod
									+ ") New Venus Number:" + newENumber;

						} else {
							logger.error("Order cancelled, but new FTD could not be sent for "
									+ orderDetailVO.getExternalOrderNumber()
									+ ". Error:" + resultsTO.getErrorString());
							return ERROR_TEXT
									+ orderDetailVO.getExternalOrderNumber()
									+ " CAN successful, FTD failed. "
									+ resultsTO.getErrorString();
						}

					} else {
						logger.error("Could not cancel order "
								+ orderDetailVO.getExternalOrderNumber()
								+ ". Error:" + resultsTO.getErrorString());
						return ERROR_TEXT
								+ orderDetailVO.getExternalOrderNumber() + " "
								+ resultsTO.getErrorString();
					}

				} else {
					// TODO : Check what response to be returned
					return ERROR_TEXT + orderDetailVO.getExternalOrderNumber();
				}
			}
		} catch (Throwable t) {
			logger.error(t);
			return FATAL_TEXT + "Application Error.  " + t.toString();
		}
	}

    @SuppressWarnings("rawtypes")
	public String cancelOrder(Connection conn,String csr,HttpServletRequest request,boolean resendOrder) throws Exception
    {
       String response = null;
       try
        {
            //needed DAOs
            OrderDAO orderDAO = new OrderDAO(conn);
            //get values from request
            String externalOrderNumber = request.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);

            //get order detail id
            Map orderIdMap = orderDAO.findOrderNumber(externalOrderNumber);
            if(orderIdMap == null)
            {
                return ERROR_TEXT + externalOrderNumber + " Order number not found.";
            }

			String orderDetailId = null;
            try
            {
                orderDetailId = (orderIdMap.get("OUT_ORDER_DETAIL_ID")).toString();
			}
			catch (Exception ex){
		        return ERROR_TEXT + externalOrderNumber + " Order number not found.";
		    }

            //get the order detail record
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);

            //do some basic validation
            String validateResults = validateOrder(request, orderDetailVO);
            if(validateResults != null)
            {
                return validateResults;
            }

            //get the recipient
            CustomerVO recipient = orderDAO.getCustomer(orderDetailVO.getRecipientId());

            // get lastFTD
            VenusMessageVO lastFTD = getLastFTD(conn,orderDetailId);

            //cancel original order
            ResultTO resultsTO = cancelOrder(conn,csr,lastFTD, orderDetailId);
            String cancelledVenusNumber = resultsTO.getKey();

            //check if cancel worked
            if(resultsTO.isSuccess())
            {
                //if user requested the order to be resent
                if(resendOrder)
                {
                    String shouldResendOrder = this.shouldResendOrder(lastFTD, conn, orderDetailVO);
                    if (shouldResendOrder != null){
                        logger.error("Order cancelled, but new FTD could not be sent for " + orderDetailVO.getExternalOrderNumber() + ". Error:" + resultsTO.getErrorString());
                        response = ERROR_TEXT + orderDetailVO.getExternalOrderNumber() + " CAN successful, FTD failed: " + shouldResendOrder;
                        return response;
                    }

                    //create a new FTD based off the old message and send out a new order
                    OrderTO orderTO = createFTDOrderMessageTO(lastFTD, recipient, csr);
                    resultsTO = resendOrder(orderTO, conn);
                    if(resultsTO.isSuccess())
                    {
                        String newENumber = getENumber(conn,resultsTO.getKey());

                        //add comment to order
                        String comment = "Sent CAN on order " + cancelledVenusNumber + ".  Resent as " + newENumber;
                        createComment(conn,comment,orderDetailVO);

                        response = "Success - " + orderDetailVO.getExternalOrderNumber() + "New Venus Number: " + newENumber;

                    }
                    else
                    {
                        logger.error("Order cancelled, but new FTD could not be sent for " + orderDetailVO.getExternalOrderNumber() + ". Error:" + resultsTO.getErrorString());
                        response = ERROR_TEXT + orderDetailVO.getExternalOrderNumber() + " CAN sucessful, FTD failed. " + resultsTO.getErrorString();
                    }
                }
                else
                {
                    logger.debug("Order not being resent");
                    response = "Success - " + orderDetailVO.getExternalOrderNumber() + " CAN sucessful";
                }
            }
            else
            {
                logger.error("Could not cancel order " + orderDetailVO.getExternalOrderNumber() +". Error:" + resultsTO.getErrorString());
                response = ERROR_TEXT + orderDetailVO.getExternalOrderNumber() + " "+ resultsTO.getErrorString();
            }
        }
        catch(Throwable t)
        {
            logger.error(t);
            response =  FATAL_TEXT + "Application Error.  " + t.toString();
        }

        return response;

    }

    @Deprecated
    /*private void sendDeliveryDateEmail(Connection conn,OrderDetailVO orderDetailVO,String oldDeliveryDate, String newDeliveryDate) throws Exception
    {

        //get the sender email address from config file
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String fromEmailAddress = configUtil.getProperty(OPMConstants.PROPERTY_FILE,"DELIVERY_DATE_CHG_EMAIL_FROM_ADDRESS");
        String fedExEmailTemplate = configUtil.getProperty(OPMConstants.PROPERTY_FILE,"DELIVERY_DATE_CHG_EMAIL_TEMPLATE_ID");

        //Get data needed for email
        OrderDAO orderDAO = new OrderDAO(conn);

        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
        CustomerVO buyerVO = orderDAO.getCustomer(orderVO.getCustomerId());
        EmailVO emailVO = orderDAO.getCustomerEmailInfo(orderVO.getCustomerId(),orderVO.getCompanyId());

        //if email info was not found then do not send out email
        if(emailVO == null)
        {
            throw new Exception("Email infomration was not found for customer " + buyerVO.getCustomerId() + " for company " + orderVO.getCompanyId() + ".  Tracking number will not be emailed.");
        }

        CompanyVO companyVO = orderDAO.getCompany(orderVO.getCompanyId());

        //create poc xml
        Document xml = DOMUtil.getDocument();
        addElement(xml,"old_delivery_date",oldDeliveryDate);
        addElement(xml,"new_delivery_date",newDeliveryDate);
        addElement(xml,"external_order_number",orderDetailVO.getExternalOrderNumber());
        addElement(xml,"company_name",companyVO.getCompanyName());
        addElement(xml,"company_url",companyVO.getURL());

        Element root = xml.createElement("root");
        xml.appendChild(root);
        Element ordersNode = xml.createElement("ORDERS");
        root.appendChild(ordersNode);
        Element orderNode = xml.createElement("ORDER");
        ordersNode.appendChild(orderNode);
        Element value = xml.createElement("language_id");
        orderNode.appendChild(value);
        value.appendChild(xml.createTextNode(orderVO.getLanguageId()));

        //create a point of contact VO
        PointOfContactVO pocVO = new PointOfContactVO();
        pocVO.setCompanyId(orderVO.getCompanyId());
        pocVO.setSenderEmailAddress(fromEmailAddress + "@" + companyVO.getURL());
        pocVO.setRecipientEmailAddress(emailVO.getEmailAddress());
        pocVO.setTemplateId(null);
        pocVO.setLetterTitle(fedExEmailTemplate);
        pocVO.setPointOfContactType(POC_EMAIL_TYPE);
        pocVO.setCommentType("Order");

        pocVO.setDataDocument(xml);

        ArrayList pocVOList = new ArrayList();
        pocVOList.add(pocVO);

        StockMessageGenerator messageGenerator = new StockMessageGenerator();
        messageGenerator.processMessages(pocVOList);

    }

    private void addElement(Document xml, String keyName, String value)
    {
        Element nameElement = (Element) xml.createElement(keyName);

        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);
    }*/


    /*
     * Insert a comment into the DB
     */
    private void createComment(Connection conn, String comment, OrderDetailVO orderDetailVO)
        throws Exception
    {
            OrderDAO orderDAO = new OrderDAO(conn);
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment(comment);
            commentsVO.setCommentOrigin(ORDER_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCommentType(COMMENT_TYPE_ORDER);
            commentsVO.setCreatedBy(ORDER_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
            orderDAO.insertComment(commentsVO);
    }


    public OrderTO createFTDOrderMessageTO(VenusMessageVO lastFTD, CustomerVO recipient, String csr) throws ParseException, Exception {
        Date shipDate = null;
        if (lastFTD.getOrigShipDate() != null)
        {
        	logger.debug("updating shipDate on venus message");
        	shipDate = lastFTD.getOrigShipDate();
        }
        else{
        	logger.debug("leaving ship date as is on venus message");
        	shipDate = lastFTD.getShipDate();
        }
        logger.info("createFTDOrderMessageTO shipDate: " + shipDate);
        
    	return this.createFTDOrderMessageTO(lastFTD, recipient, csr, lastFTD.getDeliveryDate(), shipDate, lastFTD.getShipMethod());
    }


    public OrderTO createFTDOrderMessageTO(VenusMessageVO lastFTD, CustomerVO recipient, String csr, Date deliveryDate, Date shipDate, String shipMethod) throws ParseException, Exception {
        OrderTO orderTO = new OrderTO();
        // set to true even though we're not from the COM screen, so we don't revalidate ship date
        orderTO.setFromCommunicationScreen(true);
        orderTO.setReferenceNumber(lastFTD.getReferenceNumber());
        orderTO.setAddress1(recipient.getAddress1());
        orderTO.setAddress2("");
        orderTO.setCardMessage(lastFTD.getCardMessage());
        orderTO.setCity(recipient.getCity());
        orderTO.setDeliveryDate(deliveryDate);
        orderTO.setFillingVendor(lastFTD.getFillingVendor());
        orderTO.setMessageType("FTD");
        orderTO.setOperator(csr);
        orderTO.setOrderDate(lastFTD.getOrderDate());
        orderTO.setOverUnderCharge(new Double(0.0));
        orderTO.setPhoneNumber(lastFTD.getPhoneNumber());
        orderTO.setPrice(lastFTD.getPrice());
        orderTO.setProductId(lastFTD.getProductId());
        orderTO.setRecipient(lastFTD.getRecipient());
        orderTO.setState(lastFTD.getState());
        orderTO.setShipMethod(shipMethod);
        orderTO.setShipDate(shipDate);
        orderTO.setBusinessName(lastFTD.getBusinessName());
        orderTO.setZipCode(lastFTD.getZip());
        orderTO.setCountry(lastFTD.getCountry());
        return orderTO;
    }

    private ResultTO resendOrder(OrderTO orderTO, Connection connection) throws Exception
    {
       ResultTO result = new ResultTO();
       result= MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(orderTO, connection);
       //clean up error msg if needed
       if(!result.isSuccess())
       {
           result.setErrorString(cleanUpError(result.getErrorString()));
       }
       return result;
    }

    private ResultTO cancelOrder(Connection conn,String csr, VenusMessageVO lastFTD, String orderDetailId) throws Exception
    {
        ResultTO result = null;

        //check to see if you should cancel the original order
        String shouldCancelFTDError = this.shouldCancelFTD(conn, lastFTD, orderDetailId);
        if (shouldCancelFTDError != null){
             result = new ResultTO();
             result.setSuccess(false);
             result.setKey(lastFTD.getVenusOrderNumber());
             result.setErrorString(shouldCancelFTDError);
             return result;
        }

       ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
       String venusCancelCode = configUtil.getProperty(OPMConstants.PROPERTY_FILE,"SCRIPT_CANCEL_CODE");

       CancelOrderTO cancelTo = new CancelOrderTO();
       cancelTo.setCancelReasonCode(venusCancelCode);
       cancelTo.setComments("Order cancelled by reprocess screen.");
       cancelTo.setCsr(csr);
       cancelTo.setTransmitCancel(true);
       cancelTo.setVenusId(lastFTD.getVenusId());
       result= MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(cancelTo, conn);

       //changing key to be equal to the venus order number
       result.setKey(lastFTD.getVenusOrderNumber());

       //clean up error msg if needed
       if(!result.isSuccess())
       {
           result.setErrorString(cleanUpError(result.getErrorString()));
       }
       return result;
    }

    private String shouldCancelFTD(Connection conn, VenusMessageVO lastFTD, String orderDetailId) throws Exception
    {
        // Get product VO
       logger.debug("inside shouldCancelFTD");
    	MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
        ProductVO productVO = maintDAO.getProduct(lastFTD.getProductId());

        //Returns true if order is in printed or shipped status and is on or after
        //the ship date but before the delivery date
         Date shipDate = lastFTD.getShipDate();

        // if zone jump date exists, use it as the ship date
        if (lastFTD.getZoneJumpLabelDate() != null){
            shipDate = lastFTD.getZoneJumpLabelDate();
        }

        // if ship date is null, assume it's a florist order and return false
        if ( shipDate == null){
            return null;
        }

        Date today = new Date();
        if((lastFTD.getOrderPrinted() != null)
          && (lastFTD.getOrderPrinted().equalsIgnoreCase("Y"))
          &&  today.after(shipDate)
          &&  today.before(lastFTD.getDeliveryDate())
          )
        {
            return "This order is in printed or shipped status and is on or after the ship date but before the delivery date and cannot be canceled at this time.";
        }

        //See if the product is perzonalized.  Personalized products cannot be cancelled
        if (productVO.getPersonalizationTemplateOrder() != null){
            return "This option is not available for personalized products.";
        }

        OrderDAO orderDAO = new OrderDAO(conn);
        logger.debug("lastFTD.getVenusOrderNumber(): " + lastFTD.getVenusOrderNumber());
        logger.debug("orderDetailId: " + orderDetailId);
        logger.debug("Venus");
        
        MessageOrderStatusVO orderStatusVO = orderDAO.getMessageOrderStatus(conn, lastFTD.getVenusOrderNumber(), orderDetailId, "Venus");
        if (!orderStatusVO.isHasLiveFTD()){
            return "This order has no live FTD.";
        }
        return null;
    }


   /* private String shouldResendOrder(VenusMessageVO lastFTD) throws Exception
    {
        Calendar today = Calendar.getInstance();
        today.clear(java.util.Calendar.MINUTE);
        today.clear(java.util.Calendar.SECOND);
        today.clear(java.util.Calendar.MILLISECOND);
        today.set(java.util.Calendar.HOUR_OF_DAY,0);
        if (today.getTime().after(lastFTD.getShipDate())){
          return "Invalid ship date.";
        }
        return null;
    }*/

    private String shouldResendOrder(VenusMessageVO lastFTD, Connection conn,  OrderDetailVO orderDetailVO) throws Exception
    {
  	
    
    	AddOnDAO addOnDao = new AddOnDAO(conn);
		@SuppressWarnings("unchecked")
		List<AddonVO> addOnVOs = addOnDao.getAddOns(Long.toString(orderDetailVO.getOrderDetailId()));
		List<String> addOns = null;
		
		if(addOnVOs != null){
			addOns = new ArrayList<String>();
			for(AddonVO addOnVO : addOnVOs){
				if(addOnVO.getAddOnCode() != null && addOnVO.getAddOnCode()!=""){
					addOns.add(addOnVO.getAddOnCode());
				}
			}
			logger.info("VenusScriptBO.shouldResendOrder : AddOns = "+addOns.toString());
		}
		 
		logger.info("VenusScriptBO.shouldResendOrder : Calling PAS Start | ProductId : " + lastFTD.getProductId()
				+ " | Delivery Date : " + lastFTD.getDeliveryDate() + " | ZipCode : "
				+ lastFTD.getZip() + " | AddOns : " + addOns);
		
		ProductAvailVO paVO = PASServiceUtil.getProductAvailability(lastFTD.getProductId(), lastFTD.getDeliveryDate(),
				lastFTD.getZip(), null, addOns, orderDetailVO.getSourceCode(), false, true);
		
		logger.info("VenusScriptBO.shouldResendOrder : Calling PAS End");
        Calendar today = Calendar.getInstance();
        today.clear(java.util.Calendar.MINUTE);
        today.clear(java.util.Calendar.SECOND);
        today.clear(java.util.Calendar.MILLISECOND);
        today.set(java.util.Calendar.HOUR_OF_DAY,0);
        
		if (paVO != null) {
			logger.info("VenusScriptBO.shouldResendOrder : IsAddOnAvailable = "
					+ paVO.isIsAddOnAvailable() + " : IsAvailable = " + paVO.isIsAvailable());
		}

		if (paVO != null && (paVO.isIsAvailable().equals(Boolean.FALSE) || paVO.isIsAddOnAvailable().equals(Boolean.FALSE))) {
			logger.info("VenusScriptBO.shouldResendOrder : AddOnAvailable = false ProductAvailable = false");
			return "Invalid Delivery Date.";
		}
        
        if(paVO!=null && paVO.getDeliveryDate()!=null){
        	logger.info("VenusScriptBO.shouldResendOrder : Date Returned = " + paVO.getDeliveryDate().getTime());
        	if (today.getTime().after(paVO.getDeliveryDate())) {
                return "Invalid ship date.";
              }
        } 
        
        return null;
    }
    
    /*
     * Get the order message assocated with the past in venus messageVO
     */
    @SuppressWarnings("rawtypes")
	private VenusMessageVO getLastFTD(Connection conn,String orderDetailId) throws Exception
    {
        OrderDAO orderDAO = new OrderDAO(conn);
        VenusMessageVO foundOrder = null;

        //Retrieve venus FTD order message related to this message
        List venusList = orderDAO.getVenusMessageByDetailId(orderDetailId, "FTD");

        if(venusList == null || venusList.size() == 0)
        {
            //return null
        }
        else
        {
            //as part of issue 14853, venus.venus_pkg.GET_VENUS_MESS_BY_ORDER_DETAIL was changed so that its returning orders 
            //in a "order by created_on desc" order.  Thus, the first FTD should suffice.
            foundOrder = (VenusMessageVO)venusList.get(0);
        }
        return foundOrder;
    }

 private String cleanUpError(String error)
 {
     int pos = error.indexOf(VENUS_EXCEPTION);
     if(pos > 0)
     {
         error = error.substring(pos + VENUS_EXCEPTION.length());
     }

     //take away the stack trace if one exists.
     pos = error.indexOf("||");
     if(pos > 0)
     {
         error = error.substring(0,pos);
     }

     return error;
 }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String validateOrder(HttpServletRequest request, OrderDetailVO orderDetailVO) throws Exception {
		
		String result = null;
		Map prefPartnerMap = FTDCommonUtils.getPreferredPartner();
		List excludedPartners = new ArrayList();
		Iterator partnerIt = prefPartnerMap.keySet().iterator();
		String prefPartnerName = null;

		// Find excluded preferred partners.
		while (partnerIt.hasNext()) {
			String mapKey = (String) partnerIt.next();
			String includePartnerFlag = request.getParameter("sc" + mapKey);

			// The check-box value has been translated from on/off to true/false.
			if (includePartnerFlag == null
					|| includePartnerFlag.equalsIgnoreCase("false")) {
				excludedPartners.add(mapKey);
				logger.debug("excluding partner:" + mapKey);
			}
		}

		// If partner include check-box is not checked and the order is selected, treat as error.
		if (isPartnerExcluded(orderDetailVO.getSourceCode(), excludedPartners)) {
			prefPartnerName = getPreferredPartnerBySource(orderDetailVO.getSourceCode());
			result = ERROR_TEXT + orderDetailVO.getExternalOrderNumber() + " " + prefPartnerName + " orders were excluded and will not be affected by the selected procedure.";
		}

		
		if(orderDetailVO == null) { 
			result = ERROR_TEXT + orderDetailVO.getExternalOrderNumber() + " Order detail record does not exist."; 
		} else {
			if (orderDetailVO.getDeliveryDate() == null) {
				result = ERROR_TEXT + orderDetailVO.getExternalOrderNumber()
						+ " Order does not have a delivery date.";
			}
	
			if (orderDetailVO.getShipMethod() == null) {
				result = ERROR_TEXT + orderDetailVO.getExternalOrderNumber()
						+ " Order does not have a ship method.";
			}
		}

		return result;
	}
 

 private boolean isPartnerExcluded(String sourceCode, List<String> excludedPartners) {
    String partnerName = getPreferredPartnerBySource(sourceCode);
    return excludedPartners.contains(partnerName);
 }
 
 public String getPreferredPartnerBySource(String sourceCode) {
     PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(sourceCode);
     String partnerName = null;
     if(partnerVO != null)
     {
        partnerName = partnerVO.getPartnerName();
     }
     return partnerName;
 }

}