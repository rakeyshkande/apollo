package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.ZoneJumpDAO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;


public class ConfigurationBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static Logger logger = new Logger("com.ftd.op.maintenance.bo.ConfigurationBO");

  private HttpServletRequest request = null;
  private HttpServletResponse response = null;
  private Connection con = null;

  //request paramaters
  private String requestActionType = null;
  private String requestAdminAction = "";
  private String requestContext = null;
  private String requestSelectedContext = null;
  private String requestSessionId = null;

  //others
  private String csrId = null;
  private String forwardToName = null;
  private String forwardToType = null;
  private HashMap hashXML = new HashMap();
  private HashMap pageData = new HashMap();


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public ConfigurationBO()
  {
  }

  /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public ConfigurationBO(HttpServletRequest request, Connection con, HttpServletResponse response)
  {
    this.request = request;
    this.response = response;
    this.con = con;
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @param  
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest()
    throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo();

    //Get config file info
    getConfigInfo();

    //process the action
    processAction();

    //populate the remainder of the fields on the page data
    populatePageData();


    //At this point, we should have two HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many XMLDocuments
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps 3 into one HashMap, called returnHash

    //retrieve all the XMLDocuments from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while (iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData", (HashMap) this.pageData);

    return returnHash;
  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  
  * @return none
  * @throws Exception
  */
  private void getRequestInfo()
    throws Exception
  {
    //****retrieve the action type
    if (request.getParameter("action_type") != null)
      this.requestActionType = this.request.getParameter("action_type");

    //****retrieve the adminAction
    if (request.getParameter("adminAction") != null)
      this.requestAdminAction = this.request.getParameter("adminAction");

    //****retrieve the context
    if (request.getParameter("context") != null)
      this.requestContext = this.request.getParameter("context");

    //****retrieve the selected_context
    if (request.getParameter("selected_context") != null)
      this.requestSelectedContext = this.request.getParameter("selected_context");

    //****retrieve the context
    if (request.getParameter("securitytoken") != null)
      this.requestSessionId = this.request.getParameter("securitytoken");


  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param 
  * @return
  * @throws Exception
  */
  private void getConfigInfo()
    throws Exception
  {
    /* retrieve user information */
    if (SecurityManager.getInstance().getUserInfo(this.requestSessionId) != null)
    {
      this.csrId = SecurityManager.getInstance().getUserInfo(this.requestSessionId).getUserID();
    }

  }

  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param 
  * @return
  * @throws 
  */
  private void populatePageData()
  {
    //store the security token
    this.pageData.put("adminAction", this.requestAdminAction);

    //store the security token
    this.pageData.put("context", this.requestContext);

    //store the forwardToName
    this.pageData.put("forwardToName", this.forwardToName);

    //store the forwardToType
    this.pageData.put("forwardToType", this.forwardToType);

    //store the selectedContext
    this.pageData.put("selected_context", this.requestSelectedContext);

    //store the security token
    this.pageData.put("securitytoken", this.requestSessionId);


  }


  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction()
    throws Exception
  {
    //If the action equals "load" - that is Initial load or Refresh data
    if (this.requestActionType.equalsIgnoreCase("load"))
    {
      processLoad();
    }
    //If the action equals "update"
    else if (this.requestActionType.equalsIgnoreCase("update"))
    {
      processUpdate();
    }
    //If the action equals "exit"
    else if (this.requestActionType.equalsIgnoreCase("exit"))
    {
      processExit();
    }
    //If the action is not found as above, throw a new exception
    else
    {
      throw new Exception("Invalid Action Type - Please correct");
    }

  }

  /*******************************************************************************************
  * processLoad()
  ******************************************************************************************
  * Retrieve the data from the GLOBAL_PARMS table in the FRP database schema.
  *
  */
  private void processLoad()
    throws Exception
  {
    //Instantiate MaintenanceDAO
    MaintenanceDAO mDAO = new MaintenanceDAO(this.con);

    //Xmldocument that will contain the output from the stored proce
    Document globalParms;

    //Call getRemitInfo method in the DAO
    globalParms = mDAO.getGlobalInfoXML(this.requestSelectedContext);

    this.hashXML.put("HK_global", globalParms);
    this.forwardToName = "Configuration";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processExit()
  ******************************************************************************************
  * Go back to the Main Menu
  *
  */
  private void processExit()
    throws Exception
  {
    this.forwardToName = "MainMenuAction";
    this.forwardToType = "ACTION";
  }


  /*******************************************************************************************
  * processUpdate()
  ******************************************************************************************
  * Update the global parms and reload the page. 
  *
  */
  private void processUpdate()
    throws Exception
  {
    MaintenanceDAO mDAO = new MaintenanceDAO(this.con); 
    Enumeration enum1 = this.request.getParameterNames();
    String param = "";
    String id = "";

    while (enum1.hasMoreElements())
    {
      param = (String) enum1.nextElement();
      if (param.startsWith("id_"))
      {
        id = param.substring(3);
        String gpContext = this.request.getParameter("context_" + id);
        String gpName = this.request.getParameter("name_" + id);
        String gpValue = this.request.getParameter("value_" + id);
        String gpDescription = this.request.getParameter("desc_" + id);
        
        logger.debug("Change to GlobalParms entry: " + gpContext + " " + gpName + " was made by: " + this.csrId);
        
        mDAO.updateGlobalParm(gpContext, gpName, gpValue, gpDescription, this.csrId);

        if (gpContext.equalsIgnoreCase(OPMConstants.CONTEXT_MERCHANDISING) &&
            gpName.equalsIgnoreCase(OPMConstants.NAME_STANDARD_BOXES_PER_PALLET))
        {
          updateActiveTrips(gpValue); 
        }
      }
    }
    processLoad();
  }


  /*******************************************************************************************
  * updateActiveTrips()
  ******************************************************************************************
  *
  */
  private void updateActiveTrips(String standardBoxesPerPallet)
    throws Exception
  {
    MaintenanceDAO mDAO = new MaintenanceDAO(this.con);
    double width = 0; 
    double height = 0; 
    double length = 0; 
    CachedResultSet crs = mDAO.getStandardBoxInfo(); 
    
    while (crs.next())
    {
      //retrieve the width_inch_qty
      if (crs.getObject("width_inch_qty") != null)
        width = new Double(crs.getBigDecimal("width_inch_qty").toString()).doubleValue();

      //retrieve the height_inch_qty
      if (crs.getObject("height_inch_qty") != null)
        height = new Double(crs.getBigDecimal("height_inch_qty").toString()).doubleValue();

      //retrieve the length_inch_qty
      if (crs.getObject("length_inch_qty") != null)
        length = new Double(crs.getBigDecimal("length_inch_qty").toString()).doubleValue();

    }

    double totalCubicInchesAllowedPerPallet = 0; 
    int iStandardBoxesPerPallet = new Integer(standardBoxesPerPallet).intValue(); 
   
    
    double volume = height * width * length; 
    totalCubicInchesAllowedPerPallet = volume * iStandardBoxesPerPallet; 
   
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);
    zjDAO.updateCubicInchesAllowed(new Double(totalCubicInchesAllowedPerPallet).toString(), this.csrId);
   
  }


}
