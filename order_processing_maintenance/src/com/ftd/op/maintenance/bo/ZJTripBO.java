package com.ftd.op.maintenance.bo;

import com.ftd.op.common.to.ResultTO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.ZoneJumpDAO;
import com.ftd.op.maintenance.util.LockUtil;
import com.ftd.op.maintenance.util.MessagingServiceLocator;
import com.ftd.op.maintenance.util.Transaction;
import com.ftd.op.maintenance.vo.MessageVO;
import com.ftd.op.maintenance.vo.ZJInjectionHubVO;
import com.ftd.op.maintenance.vo.ZJTripStatusVO;
import com.ftd.op.maintenance.vo.ZJTripVO;
import com.ftd.op.maintenance.vo.ZJTripVendorOrderVO;
import com.ftd.op.maintenance.vo.ZJTripVendorVO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

import java.io.StringReader;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.transaction.UserTransaction;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;


public class ZJTripBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static Logger logger = new Logger("com.ftd.op.maintenance.bo.ZJTripBO");
  private static final String DELIVERY_DPEARTURE_DATE_FORMAT = "yyyy/MM/dd";
  private static final String SORT_CODE_DATE_FORMAT = "MMdd";


  private HttpServletRequest request = null;
  private HttpServletResponse response = null;
  private Connection con = null;

  //request paramaters
  private String requestActionType = null;
  private String requestAdminAction = "";
  private String requestContext = null;
  private String requestDaysInTransitQty = null;
  private String requestDeliveryDate = null;
  private String requestDepartureDate = null;
  private String requestIhId = null;
  private String requestIhState = null;
  private String requestOriginalActionType = null;
  private String requestSessionId = null;
  private String requestThirdPartyPltQty = null;
  private String requestTotalPltQty = null;
  private int requestTotalRecords = 0;
  private String requestTripFilter = null;
  private String requestTripId = null;
  private String requestVendorLocation = null;


  //others
  private String csrId = null;
  private String forwardToName = null;
  private String forwardToType = null;
  private HashMap hashXML = new HashMap();
  private String maxPalletsPerTruck = null;
  private String OK_ERROR_MESSAGE = null;
  private HashMap pageData = new HashMap();



  /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public ZJTripBO(HttpServletRequest request, Connection con, HttpServletResponse response)
  {
    this.request = request;
    this.response = response;
    this.con = con;
  }


  /*******************************************************************************************
  * buildTripStatusList()
  ******************************************************************************************
  *
  */
  private List buildTripStatusList(CachedResultSet crs)
    throws Exception
  {
    //define the vo
    ZJTripStatusVO statusVO = null;
    String status;
    List statusList = new ArrayList();

    /* populate object */
    while (crs.next())
    {
      statusVO = new ZJTripStatusVO();

      //retrieve and set the trip_status_code
      status = crs.getString("trip_status_code");

      //retrieve and set the created_by
      statusVO.setCreatedBy(crs.getString("created_by"));

      //retrieve and set the created_on
      if (crs.getObject("created_on") != null)
        statusVO.setCreatedOn((Date) crs.getDate("created_on"));

      //retrieve and set the updated_by
      statusVO.setUpdatedBy(crs.getString("updated_by"));

      //retrieve and set the updated_on
      if (crs.getObject("updated_on") != null)
        statusVO.setUpdatedOn((Date) crs.getDate("updated_on"));

      //"Active" status is not going to be displayed to the users.  Users will be displayed virtual statuses of 
      //"Accepting Orders" or "Cutoff Reached"
      if (status.equalsIgnoreCase(OPMConstants.STATUS_TRIP_ACTIVE))
      {
        //Add "Accepting Orders" to the list
        ZJTripStatusVO statusAcceptingVO = (ZJTripStatusVO) statusVO.clone();
        statusAcceptingVO.setTripStatusCode(OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS);
        statusAcceptingVO.setTripStatusDescription(OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS);
        statusList.add(statusAcceptingVO);

        //Add "Cutoff Reached" to the list
        ZJTripStatusVO statusCutoffVO = (ZJTripStatusVO) statusVO.clone();
        statusCutoffVO.setTripStatusCode(OPMConstants.STATUS_TRIP_CUTOFF_REACHED);
        statusCutoffVO.setTripStatusDescription(OPMConstants.STATUS_TRIP_CUTOFF_REACHED);
        statusList.add(statusCutoffVO);
      }
      else
      {
        //set the trip_status_code
        statusVO.setTripStatusCode(status);

        //retrieve and set the trip_status_desc
        statusVO.setTripStatusDescription(crs.getString("trip_status_desc"));

        statusList.add(statusVO);
      }
    }
    return statusList;
  }


  /*******************************************************************************************
  * buildTripVendorVOList()
  ******************************************************************************************
  *
  */
  private List buildTripVendorVOList(CachedResultSet crs)
    throws Exception
  {
    //define the vo and lists
    ZJTripVendorVO tvVO = null;
    ZJTripVendorOrderVO tvoVO = null;
    List tvVOList = new ArrayList();
    List tvoVOList = null;

    //temp place holders
    String oldVendorId = null;
    String newVendorId = null;
    boolean voExist = false;

    /* populate object */
    while (crs.next())
    {
      voExist = true;

      //retrieve the vendor_id
      newVendorId = crs.getString("vendor_id");

      //instantiate VO if necessary
      if (oldVendorId == null)
      {
        oldVendorId = newVendorId;
        tvoVOList = new ArrayList();
        tvVO = new ZJTripVendorVO();
      }
      //add VO to list and instantiate VO if necessary
      else if (!oldVendorId.equalsIgnoreCase(newVendorId))
      {
        //set the TripVendorOrderVOList in the TripVendorVO
        tvVO.setTripVendorOrderVOList(tvoVOList);
        //reset the TripVendorVOList
        tvoVOList = new ArrayList();
        //add the TripVendorVO to a list
        tvVOList.add(tvVO);
        //reset the TripVendorVO
        tvVO = new ZJTripVendorVO();
        //reset the trip id
        oldVendorId = newVendorId;
      }

      /*********************************************
       * retrieve info and populate the TripVendorVO
       *********************************************/
      //retrieve and set the vendor_id
      tvVO.setVendorId(newVendorId);

      //retrieve and set the vendor_name
      tvVO.setVendorName(crs.getString("vendor_name"));

      //retrieve and set the address1
      tvVO.setAddress1(crs.getString("address1"));

      //retrieve and set the city
      tvVO.setCity(crs.getString("city"));

      //retrieve and set the state
      tvVO.setState(crs.getString("state"));

      //retrieve the trip_id
      if (crs.getObject("trip_id") != null)
        tvVO.setTripId(Long.valueOf(crs.getBigDecimal("trip_id").toString()).longValue());

      //retrieve and set the tv_trip_vendor_status_code
      tvVO.setOriginalTripVendorStatusCode(crs.getString("tv_trip_vendor_status_code"));

      //retrieve and set the tv_trip_vendor_status_code
      tvVO.setNewTripVendorStatusCode(crs.getString("tv_trip_vendor_status_code"));

      //retrieve and set the tv_order_cutoff_time
      tvVO.setOriginalOrderCutoffTime(crs.getString("tv_order_cutoff_time"));

      //retrieve and set the tv_order_cutoff_time
      tvVO.setNewOrderCutoffTime(crs.getString("tv_order_cutoff_time"));

      //retrieve and set the tv_plt_cubic_inch_allowed_qty
      if (crs.getObject("tv_plt_cubic_inch_allowed_qty") != null)
        tvVO.setPltCubicInchAllowedQty(Double.valueOf(crs.getBigDecimal("tv_plt_cubic_inch_allowed_qty").toString()).doubleValue());

      //retrieve and set the tv_full_plt_qty
      if (crs.getObject("tv_full_plt_qty") != null)
        tvVO.setFullPltQty(Long.valueOf(crs.getBigDecimal("tv_full_plt_qty").toString()).longValue());

      //retrieve and set the tv_full_plt_order_qty
      if (crs.getObject("tv_full_plt_order_qty") != null)
        tvVO.setFullPltOrderQty(Long.valueOf(crs.getBigDecimal("tv_full_plt_order_qty").toString()).longValue());

      //retrieve and set the tv_prtl_plt_in_use_flag
      tvVO.setPrtlPltInUseFlag(crs.getString("tv_prtl_plt_in_use_flag"));

      //retrieve and set the tv_prtl_plt_order_qty
      if (crs.getObject("tv_prtl_plt_order_qty") != null)
        tvVO.setPrtlPltOrderQty(Long.valueOf(crs.getBigDecimal("tv_prtl_plt_order_qty").toString()).longValue());

      //retrieve and set the tv_prtl_plt_cubic_in_used_qty
      if (crs.getObject("tv_prtl_plt_cubic_in_used_qty") != null)
        tvVO.setPrtlPltCubicInUsedQty(Double.valueOf(crs.getBigDecimal("tv_prtl_plt_cubic_in_used_qty").toString()).doubleValue());

      //retrieve and set the tv_created_by
      tvVO.setCreatedBy(crs.getString("tv_created_by"));

      //retrieve and set the tv_created_on
      if (crs.getObject("tv_created_on") != null)
        tvVO.setCreatedOn((Date) crs.getDate("tv_created_on"));

      //retrieve and set the tv_updated_by
      tvVO.setUpdatedBy(crs.getString("tv_updated_by"));

      //retrieve and set the tv_updated_on
      if (crs.getObject("tv_updated_on") != null)
        tvVO.setUpdatedOn((Date) crs.getDate("tv_updated_on"));

      //retrieve and set the tvo_reject_printed_exist, if the flag has not been set already
      if (crs.getObject("tvo_reject_printed_exist") != null && 
          (tvVO.getTVORejectPrintedExist() == null || tvVO.getTVORejectPrintedExist().equalsIgnoreCase("N")))
        tvVO.setTVORejectPrintedExist(crs.getString("tvo_reject_printed_exist"));


      /**********************************************************************
       * retrieve info and instantiate and populate the ZJTripVendorOrderVO
       ***********************************************************************/
      /**  **/
      tvoVO = new ZJTripVendorOrderVO();

      //retrieve the trip_id
      if (crs.getObject("trip_id") != null)
        tvoVO.setTripId(Long.valueOf(crs.getBigDecimal("trip_id").toString()).longValue());

      //populate vendor id
      tvoVO.setVendorId(newVendorId);

      //retrieve and set the tvo_order_detail_id
      if (crs.getObject("tvo_order_detail_id") != null)
        tvoVO.setOrderDetailId(Long.valueOf(crs.getBigDecimal("tvo_order_detail_id").toString()).longValue());

      //retrieve and set the tvo_venus_id
      tvoVO.setVenusId(crs.getString("tvo_venus_id"));

      //retrieve and set the tvo_venus_order_number
      tvoVO.setVenusOrderNumber(crs.getString("tvo_venus_order_number"));

      //retrieve and set the tvo_created_by
      tvoVO.setCreatedBy(crs.getString("tvo_created_by"));

      //retrieve and set the tvo_created_on
      if (crs.getObject("tvo_created_on") != null)
        tvoVO.setCreatedOn((Date) crs.getDate("tvo_created_on"));

      //retrieve and set the tvo_updated_by
      tvoVO.setUpdatedBy(crs.getString("tvo_updated_by"));

      //retrieve and set the tvo_updated_on
      if (crs.getObject("tvo_updated_on") != null)
        tvoVO.setUpdatedOn((Date) crs.getDate("tvo_updated_on"));

      //retrieve and set the tvo_sds_status
      tvoVO.setSdsStatus(crs.getString("tvo_sds_status"));

      //add the TripVendorOrderVO to a list
      tvoVOList.add(tvoVO);

    }
    //add the last TripVendorVO to the list. 
    //set the TripVendorOrderVOList in the VendorVO
    if (voExist)
    {
      tvVO.setTripVendorOrderVOList(tvoVOList);
      //add the TripVendorVO to a list
      tvVOList.add(tvVO);
    }

    return tvVOList;

  }


  /*******************************************************************************************
  * buildTripVO()
  ******************************************************************************************
  *
  */
  private ZJTripVO buildTripVO(CachedResultSet crs)
    throws Exception
  {
    //define the vo and lists
    ZJTripVO tVO = new ZJTripVO();
    List tripVendorList = null;
    ZJInjectionHubVO ihVO = null;

    Date today = new Date();
    SimpleDateFormat sdfTime = new SimpleDateFormat("HHmm");
    SimpleDateFormat sdfDate = new SimpleDateFormat(DELIVERY_DPEARTURE_DATE_FORMAT);
    String sCurrentTime = sdfTime.format(today);
    String sCurrentDate = sdfDate.format(today);

    /* populate object */
    while (crs.next())
    {
      //retrieve the trip_id
      if (crs.getObject("trip_id") != null)
        tVO.setTripId(Long.valueOf(crs.getBigDecimal("trip_id").toString()).longValue());

      //retrieve and set the trip_origination_desc
      tVO.setTripOriginationDesc(crs.getString("trip_origination_desc"));

      //retrieve and set the injection_hub_id
      if (crs.getObject("injection_hub_id") != null)
        tVO.setInjectionHubId(Long.valueOf(crs.getBigDecimal("injection_hub_id").toString()).longValue());

      SimpleDateFormat sdf = new SimpleDateFormat(DELIVERY_DPEARTURE_DATE_FORMAT);
      //retrieve and set the delivery_date
      if (crs.getObject("delivery_date") != null)
      {
        Date dDeliveryDate = crs.getDate("delivery_date");
        tVO.setDeliveryDate(sdf.format(dDeliveryDate));
      }

      //retrieve and set the departure_date
      if (crs.getObject("departure_date") != null)
      {
        Date dDepartureDate = crs.getDate("departure_date");
        tVO.setDepartureDate(sdf.format(dDepartureDate));
      }

      //retrieve and set the trip_status_code
      tVO.setActualTripStatusCode(crs.getString("trip_status_code"));

      //retrieve and set the sort_code_seq
      if (crs.getObject("sort_code_seq") != null)
        tVO.setSortCodeSeq(Long.valueOf(crs.getBigDecimal("sort_code_seq").toString()).longValue());

      //retrieve and set the sort_code_state_master_id
      tVO.setSortCodeStateMasterId(crs.getString("sort_code_state_master_id"));

      //retrieve and set the sort_code_date_mmdd
      tVO.setSortCodeDateMMDD(crs.getString("sort_code_date_mmdd"));

      //retrieve and set the zone jump trailer number
      tVO.setSortCode(crs.getString("zone_jump_trailer_number"));

      //retrieve and set the total_plt_qty
      if (crs.getObject("total_plt_qty") != null)
        tVO.setTotalPltQty(Long.valueOf(crs.getBigDecimal("total_plt_qty").toString()).longValue());

      //retrieve and set the ftd_plt_qty
      if (crs.getObject("ftd_plt_qty") != null)
        tVO.setFtdPltQty(Long.valueOf(crs.getBigDecimal("ftd_plt_qty").toString()).longValue());

      //retrieve and set the third_party_plt_qty
      if (crs.getObject("third_party_plt_qty") != null)
        tVO.setThirdPartyPltQty(Long.valueOf(crs.getBigDecimal("third_party_plt_qty").toString()).longValue());

      //retrieve and set the ftd_plt_used_qty
      if (crs.getObject("ftd_plt_used_qty") != null)
        tVO.setFtdPltUsedQty(Long.valueOf(crs.getBigDecimal("ftd_plt_used_qty").toString()).longValue());

      //retrieve and set the t_created_by
      tVO.setCreatedBy(crs.getString("t_created_by"));

      //retrieve and set the t_created_on
      if (crs.getObject("t_created_on") != null)
        tVO.setCreatedOn((Date) crs.getDate("t_created_on"));

      //retrieve and set the t_updated_by
      tVO.setUpdatedBy(crs.getString("t_updated_by"));

      //retrieve and set the t_updated_on
      if (crs.getObject("t_updated_on") != null)
        tVO.setUpdatedOn((Date) crs.getDate("t_updated_on"));

      //retrieve and set the tv_reject_printed_exist, if the flag has not been set already
      if (crs.getObject("tv_reject_printed_exist") != null && 
          (tVO.getTVRejectPrintedExist() == null || tVO.getTVRejectPrintedExist().equalsIgnoreCase("N")))
        tVO.setTVRejectPrintedExist(crs.getString("tv_reject_printed_exist"));

      //retrieve and set the tv_printed_exist
      tVO.setTVPrintedExist(crs.getString("tv_printed_exist"));

      //retrieve and set the max_order_cutoff_time
      tVO.setMaxOrderCutoffTime(crs.getString("max_order_cutoff_time"));

      //retrieve and set the total_order_on_truck
      if (crs.getObject("total_order_on_truck") != null)
        tVO.setTotalOrderOnTruck(Long.valueOf(crs.getBigDecimal("total_order_on_truck").toString()).longValue());

      //change the status.  User is indifferent to Active status... they only know the virtual statuses of "Accepting
      //Orders" and "Cutoff Reached" instead of Active.
      if (tVO.getActualTripStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_ACTIVE))
      {
        if (tVO.getDepartureDate().compareTo(sCurrentDate) > 0)
          tVO.setVirtualTripStatusCode(OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS);
        else if (tVO.getDepartureDate().compareTo(sCurrentDate) == 0 && 
                 (tVO.getMaxOrderCutoffTime() == null || tVO.getMaxOrderCutoffTime().compareTo(sCurrentTime) >= 0))
          tVO.setVirtualTripStatusCode(OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS);
        else
          tVO.setVirtualTripStatusCode(OPMConstants.STATUS_TRIP_CUTOFF_REACHED);
      }

      tVO.setInjectionHubVO(ihVO);
      tVO.setTripVendorVOList(tripVendorList);

      //retrieve and set the days_in_transit_qty
      if (crs.getObject("days_in_transit_qty") != null)
        tVO.setDaysInTransitQty(Long.valueOf(crs.getBigDecimal("days_in_transit_qty").toString()).longValue());

    }

    return tVO;

  }


  /*******************************************************************************************
  * buildTripVOList()
  ******************************************************************************************
  *
  */
  private List buildTripVOList(CachedResultSet crs)
    throws Exception
  {
    //define the vo and lists
    ZJTripVO tVO = null;
    ZJTripVendorVO tvVO = null;
    ZJInjectionHubVO ihVO = null;
    List tripList = new ArrayList();
    List tripVendorList = null;

    //temp place holders
    long oldTripId = 0;
    long newTripId = 0;
    boolean voExist = false;

    Date today = new Date();
    SimpleDateFormat sdfTime = new SimpleDateFormat("HHmm");
    SimpleDateFormat sdfDate = new SimpleDateFormat(DELIVERY_DPEARTURE_DATE_FORMAT);
    String sCurrentTime = sdfTime.format(today);
    String sCurrentDate = sdfDate.format(today);

    /* populate object */
    while (crs.next())
    {
      voExist = true;

      //retrieve the trip_id
      if (crs.getObject("trip_id") != null)
        newTripId = Long.valueOf(crs.getBigDecimal("trip_id").toString()).longValue();

      //instantiate VO if necessary
      if (oldTripId == 0)
      {
        oldTripId = newTripId;
        tripVendorList = new ArrayList();
        tVO = new ZJTripVO();
      }
      //add VO to list and instantiate VO if necessary
      else if (oldTripId != newTripId)
      {
        //set the TripVendorVOList in the TripVO
        tVO.setTripVendorVOList(tripVendorList);
        //reset the TripVendorVOList
        tripVendorList = new ArrayList();
        //add the TripVO to a list
        tripList.add(tVO);
        //reset the TripVO
        tVO = new ZJTripVO();
        //reset the trip id
        oldTripId = newTripId;
      }

      /*********************************************
       * retrieve info and populate the TripVO
       *********************************************/
      tVO.setTripId(newTripId);

      //retrieve and set the trip_origination_desc
      tVO.setTripOriginationDesc(crs.getString("trip_origination_desc"));

      //retrieve and set the injection_hub_id
      if (crs.getObject("injection_hub_id") != null)
        tVO.setInjectionHubId(Long.valueOf(crs.getBigDecimal("injection_hub_id").toString()).longValue());

      SimpleDateFormat sdf = new SimpleDateFormat(DELIVERY_DPEARTURE_DATE_FORMAT);
      //retrieve and set the delivery_date
      if (crs.getObject("delivery_date") != null)
      {
        Date dDeliveryDate = crs.getDate("delivery_date");
        tVO.setDeliveryDate(sdf.format(dDeliveryDate));
      }

      //retrieve and set the departure_date
      if (crs.getObject("departure_date") != null)
      {
        Date dDepartureDate = crs.getDate("departure_date");
        tVO.setDepartureDate(sdf.format(dDepartureDate));
      }

      //retrieve and set the trip_status_code
      tVO.setActualTripStatusCode(crs.getString("trip_status_code"));

      //retrieve and set the sort_code_seq
      if (crs.getObject("sort_code_seq") != null)
        tVO.setSortCodeSeq(Long.valueOf(crs.getBigDecimal("sort_code_seq").toString()).longValue());

      //retrieve and set the sort_code_state_master_id
      tVO.setSortCodeStateMasterId(crs.getString("sort_code_state_master_id"));

      //retrieve and set the sort_code_date_mmdd
      tVO.setSortCodeDateMMDD(crs.getString("sort_code_date_mmdd"));

      //retrieve and set the zone jump trailer number
      tVO.setSortCode(crs.getString("zone_jump_trailer_number"));

      //retrieve and set the total_plt_qty
      if (crs.getObject("total_plt_qty") != null)
        tVO.setTotalPltQty(Long.valueOf(crs.getBigDecimal("total_plt_qty").toString()).longValue());

      //retrieve and set the ftd_plt_qty
      if (crs.getObject("ftd_plt_qty") != null)
        tVO.setFtdPltQty(Long.valueOf(crs.getBigDecimal("ftd_plt_qty").toString()).longValue());

      //retrieve and set the third_party_plt_qty
      if (crs.getObject("third_party_plt_qty") != null)
        tVO.setThirdPartyPltQty(Long.valueOf(crs.getBigDecimal("third_party_plt_qty").toString()).longValue());

      //retrieve and set the ftd_plt_used_qty
      if (crs.getObject("ftd_plt_used_qty") != null)
        tVO.setFtdPltUsedQty(Long.valueOf(crs.getBigDecimal("ftd_plt_used_qty").toString()).longValue());

      //retrieve and set the t_created_by
      tVO.setCreatedBy(crs.getString("t_created_by"));

      //retrieve and set the t_created_on
      if (crs.getObject("t_created_on") != null)
        tVO.setCreatedOn((Date) crs.getDate("t_created_on"));

      //retrieve and set the t_updated_by
      tVO.setUpdatedBy(crs.getString("t_updated_by"));

      //retrieve and set the t_updated_on
      if (crs.getObject("t_updated_on") != null)
        tVO.setUpdatedOn((Date) crs.getDate("t_updated_on"));

      //retrieve and set the tv_reject_printed_exist, if the flag has not been set already
      if (crs.getObject("tv_reject_printed_exist") != null && 
          (tVO.getTVRejectPrintedExist() == null || tVO.getTVRejectPrintedExist().equalsIgnoreCase("N")))
        tVO.setTVRejectPrintedExist(crs.getString("tv_reject_printed_exist"));

      //retrieve and set the tv_printed_exist
      tVO.setTVPrintedExist(crs.getString("tv_printed_exist"));

      //retrieve and set the max_order_cutoff_time
      tVO.setMaxOrderCutoffTime(crs.getString("max_order_cutoff_time"));

      //retrieve and set the total_order_on_truck
      if (crs.getObject("total_order_on_truck") != null)
        tVO.setTotalOrderOnTruck(Long.valueOf(crs.getBigDecimal("total_order_on_truck").toString()).longValue());

      //change the status.  User is indifferent to Active status... they only know the virtual statuses of "Accepting
      //Orders" and "Cutoff Reached" instead of Active.
      if (tVO.getActualTripStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_ACTIVE))
      {
        if (tVO.getDepartureDate().compareTo(sCurrentDate) > 0)
          tVO.setVirtualTripStatusCode(OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS);
        else if (tVO.getDepartureDate().compareTo(sCurrentDate) == 0 && 
                 (tVO.getMaxOrderCutoffTime() == null || tVO.getMaxOrderCutoffTime().compareTo(sCurrentTime) >= 0))
          tVO.setVirtualTripStatusCode(OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS);
        else
          tVO.setVirtualTripStatusCode(OPMConstants.STATUS_TRIP_CUTOFF_REACHED);
      }

      //retrieve and set the days_in_transit_qty
      if (crs.getObject("days_in_transit_qty") != null)
        tVO.setDaysInTransitQty(Long.valueOf(crs.getBigDecimal("days_in_transit_qty").toString()).longValue());

      /**********************************************************************
       * retrieve info and instantiate and populate the TripVendorVO
       ***********************************************************************/
      /**  **/
      tvVO = new ZJTripVendorVO();

      //populate trip_id
      tvVO.setTripId(newTripId);

      //retrieve and set the vendor_id
      tvVO.setVendorId(crs.getString("vendor_id"));

      //retrieve and set the vendor_name
      tvVO.setVendorName(crs.getString("vendor_name"));

      //retrieve and set the address1
      tvVO.setAddress1(crs.getString("address1"));

      //retrieve and set the city
      tvVO.setCity(crs.getString("city"));

      //retrieve and set the state
      tvVO.setState(crs.getString("state"));

      //retrieve and set the trip_vendor_status_code
      tvVO.setOriginalTripVendorStatusCode(crs.getString("trip_vendor_status_code"));

      //retrieve and set the trip_vendor_status_code
      tvVO.setNewTripVendorStatusCode(crs.getString("trip_vendor_status_code"));

      //retrieve and set the order_cutoff_time
      tvVO.setOriginalOrderCutoffTime(crs.getString("order_cutoff_time"));

      //retrieve and set the order_cutoff_time
      tvVO.setNewOrderCutoffTime(crs.getString("order_cutoff_time"));

      //retrieve and set the plt_cubic_inch_allowed_qty
      if (crs.getObject("plt_cubic_inch_allowed_qty") != null)
        tvVO.setPltCubicInchAllowedQty(Double.valueOf(crs.getBigDecimal("plt_cubic_inch_allowed_qty").toString()).doubleValue());

      //retrieve and set the full_plt_qty
      if (crs.getObject("full_plt_qty") != null)
        tvVO.setFullPltQty(Long.valueOf(crs.getBigDecimal("full_plt_qty").toString()).longValue());

      //retrieve and set the full_plt_order_qty
      if (crs.getObject("full_plt_order_qty") != null)
        tvVO.setFullPltOrderQty(Long.valueOf(crs.getBigDecimal("full_plt_order_qty").toString()).longValue());

      //retrieve and set the prtl_plt_in_use_flag
      tvVO.setPrtlPltInUseFlag(crs.getString("prtl_plt_in_use_flag"));

      //retrieve and set the prtl_plt_order_qty
      if (crs.getObject("prtl_plt_order_qty") != null)
        tvVO.setPrtlPltOrderQty(Long.valueOf(crs.getBigDecimal("prtl_plt_order_qty").toString()).longValue());

      //retrieve and set the prtl_plt_cubic_in_used_qty
      if (crs.getObject("prtl_plt_cubic_in_used_qty") != null)
        tvVO.setPrtlPltCubicInUsedQty(Double.valueOf(crs.getBigDecimal("prtl_plt_cubic_in_used_qty").toString()).doubleValue());

      //retrieve and set the tv_created_by
      tvVO.setCreatedBy(crs.getString("tv_created_by"));

      //retrieve and set the tv_created_on
      if (crs.getObject("tv_created_on") != null)
        tvVO.setCreatedOn((Date) crs.getDate("tv_created_on"));

      //retrieve and set the tv_updated_by
      tvVO.setUpdatedBy(crs.getString("tv_updated_by"));

      //retrieve and set the tv_updated_on
      if (crs.getObject("tv_updated_on") != null)
        tvVO.setUpdatedOn((Date) crs.getDate("tv_updated_on"));

      //add the TripVendorVO to a list
      tripVendorList.add(tvVO);

      /**********************************************************************
       * retrieve info and instantiate and populate the TripVendorVO
       ***********************************************************************/
      ihVO = new ZJInjectionHubVO();

      ihVO.setInjectionHubId(tVO.getInjectionHubId());

      //retrieve and set the city_name
      ihVO.setCityName(crs.getString("city_name"));

      //retrieve and set the state_master_id
      ihVO.setStateMasterId(crs.getString("state_master_id"));

      //retrieve and set the carrier_id
      ihVO.setCarrierId(crs.getString("carrier_id"));

      //retrieve and set the address_desc
      ihVO.setAddressDesc(crs.getString("address_desc"));

      //retrieve and set the zip_code
      ihVO.setZipCode(crs.getString("zip_code"));

      //retrieve and set the sds_account_num
      ihVO.setSdsAccountNum(crs.getString("sds_account_num"));

      //retrieve and set the ih_created_by
      ihVO.setCreatedBy(crs.getString("ih_created_by"));

      //retrieve and set the ih_created_on
      if (crs.getObject("ih_created_on") != null)
        ihVO.setCreatedOn((Date) crs.getDate("ih_created_on"));

      //retrieve and set the ih_updated_by
      ihVO.setUpdatedBy(crs.getString("ih_updated_by"));

      //retrieve and set the ih_updated_on
      if (crs.getObject("ih_updated_on") != null)
        ihVO.setUpdatedOn((Date) crs.getDate("ih_updated_on"));

      tVO.setInjectionHubVO(ihVO);

    }
    //add the last TripVO to the list. 
    //set the TripVendorVOList in the TripVO
    if (voExist)
    {
      tVO.setTripVendorVOList(tripVendorList);
      //add the TripVO to a list
      tripList.add(tVO);
    }

    return tripList;

  }


  /**
   * This creates an Document for the data in a cursor with the given top and bottom name.
   * Allows you to specify elements or attributes with type parameter.
   * @throws java.lang.Exception
   * @return
   * @param type
   * @param bottomName
   * @param topName
   * @param cursorName
   * @param outMap
   */
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type)
    throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc = null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName);

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }


  /*******************************************************************************************
  * cancelVendor()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private List cancelVendor(ZJTripVendorVO tvVO, String deliveryDate, String departureDate)
    throws Exception
  {
    //Instantiate MaintenanceDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    SimpleDateFormat sdf = new SimpleDateFormat(this.DELIVERY_DPEARTURE_DATE_FORMAT);
    Date dDeliveryDate = sdf.parse(deliveryDate);
    Date dDepartureDate = sdf.parse(departureDate);

    CachedResultSet crs = zjDAO.cancelTripVendor(new Long(tvVO.getTripId()).toString(), tvVO.getVendorId(), this.csrId);

    String comments = "ZoneJump - User Canceled the Vendor";
    List messageList = new ArrayList();

    while (crs.next())
    {
      //create a new VO
      MessageVO msgVO = new MessageVO(); 

      //set the delivery_date
      if (dDeliveryDate != null)
        msgVO.setDeliveryDate(dDeliveryDate);

      //set the departure_date
      if (dDepartureDate != null)  
        msgVO.setDepartureDate(dDepartureDate);
  
      //retrieve and set the business_name
      msgVO.setBusinessName(crs.getString("business_name"));

      //retrieve and set the card_message
      msgVO.setCardMessage(crs.getString("card_message"));

      //set the comments
      msgVO.setComments(comments);
      
      //retrieve and set the filling_vendor
      msgVO.setFillingVendor(crs.getString("filling_vendor"));

      //retrieve and set the order_date
      if (crs.getObject("order_date") != null)
        msgVO.setOrderDate((Date) crs.getDate("order_date"));

      //retrieve and set the order_detail_id
      msgVO.setOrderDetailId(crs.getString("order_detail_id"));

      //retrieve and set the price
      if (crs.getObject("price") != null)
        msgVO.setPrice(Double.valueOf(crs.getBigDecimal("price").toString()).doubleValue());

      //retrieve and set the product_id
      msgVO.setProductId(crs.getString("product_id"));

      //retrieve and set the address_1
      msgVO.setRecipientAddress1(crs.getString("address_1"));

      //retrieve and set the city
      msgVO.setRecipientCity(crs.getString("city"));

      //retrieve and set the country
      msgVO.setRecipientCountry(crs.getString("country"));

      //retrieve and set the recipient
      msgVO.setRecipientName(crs.getString("recipient"));

      //retrieve and set the phone_number
      msgVO.setRecipientPhoneNumber(crs.getString("phone_number"));

      //retrieve and set the state
      msgVO.setRecipientState(crs.getString("state"));

      //retrieve and set the zip
      msgVO.setRecipientZip(crs.getString("zip"));

      //retrieve and set the ship_method
      msgVO.setShipMethod(crs.getString("ship_method"));

      //retrieve and set the ship_date
      if (crs.getObject("ship_date") != null)
        msgVO.setShipDate((Date) crs.getDate("ship_date"));

      //retrieve and set the venus_id
      msgVO.setVenusId(crs.getString("venus_id"));

      //retrieve and set the venus_order_number
      msgVO.setVenusOrderNumber(crs.getString("venus_order_number"));

      //add the messageVO to the list
      messageList.add(msgVO);      

    }

    return messageList; 

  }

  /*******************************************************************************************
  * checkForDateBlock()
  ******************************************************************************************
  *
  *
  */
  private boolean checkForDateBlock(ZJTripVO tVO)
    throws Exception
  {
    List tvVOList = tVO.getTripVendorVOList();
    ZJTripVendorVO tvVO = null;
    boolean blockExists = false;
    String vendorsToBeChecked = "";

    //create a vendor list
    for (int x = 0; x < tvVOList.size(); x++)
    {
      tvVO = (ZJTripVendorVO) tvVOList.get(x);
      if (tvVO.getNewTripVendorStatusCode() != null && !tvVO.getNewTripVendorStatusCode().equalsIgnoreCase("") && 
          (tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_ASSIGNED) || 
           tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_UNASSIGNED)))
        vendorsToBeChecked += "'" + tvVO.getVendorId() + "'" + ",";
    }
    //if vendor list exist, remove the last ,
    if (vendorsToBeChecked.length() > 0)
      vendorsToBeChecked = vendorsToBeChecked.substring(0, vendorsToBeChecked.length() - 1);
    //else initialize to null 
    else
      vendorsToBeChecked = null;

    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    /*****************************************************************************************************************
     * Check for shipping block
     *****************************************************************************************************************/
    String shippingMessage = "";
    String shippingVendorNames = "";
    String shippingVendorNameOriginal = null;
    String shippingVendorNameNew = null;
    boolean shippingBlockExists = false;
    //Call getVendorBlockInfo method in the DAO
    CachedResultSet shipBlockCRS = 
      zjDAO.getVendorBlockInfo(this.requestDepartureDate, OPMConstants.BLOCK_TYPE_SHIPPING, vendorsToBeChecked);

    if (shipBlockCRS != null && shipBlockCRS.getRowCount() > 0)
    {
      shippingBlockExists = true;
      while (shipBlockCRS.next())
      {
        //retrieve and set the vendor_name
        shippingVendorNameNew = shipBlockCRS.getString("vendor_name");

        if (shippingVendorNameOriginal == null || !shippingVendorNameNew.equalsIgnoreCase(shippingVendorNameOriginal))
        {
          shippingVendorNames += shippingVendorNameNew + ", ";
          shippingVendorNameOriginal = shippingVendorNameNew;
        }
      }
      //if vendor names exist, remove the last ", "
      if (shippingVendorNames.length() > 0)
        shippingVendorNames = shippingVendorNames.substring(0, shippingVendorNames.length() - 2);
 
      shippingMessage = "A shipping block exists for the date selected for: " + shippingVendorNames + ".  Please fix.";
    }

    /*****************************************************************************************************************
     * Check for delivery block
     *****************************************************************************************************************/
    String deliveryMessage = "";
    String deliveryVendorNames = "";
    String deliveryVendorNameOriginal = null;
    String deliveryVendorNameNew = null;
    boolean deliveryBlockExists = false;
    //Call getVendorBlockInfo method in the DAO
    CachedResultSet deliveryBlockCRS = 
      zjDAO.getVendorBlockInfo(this.requestDeliveryDate, OPMConstants.BLOCK_TYPE_DELIVERY, vendorsToBeChecked);

    if (deliveryBlockCRS != null && deliveryBlockCRS.getRowCount() > 0)
    {
      deliveryBlockExists = true;
      while (deliveryBlockCRS.next())
      {
        //retrieve and set the vendor_name
        deliveryVendorNameNew = deliveryBlockCRS.getString("vendor_name");

        if (deliveryVendorNameOriginal == null || !deliveryVendorNameNew.equalsIgnoreCase(deliveryVendorNameOriginal))
        {
          deliveryVendorNames += deliveryVendorNameNew + ", ";
          deliveryVendorNameOriginal = deliveryVendorNameNew;
        }
      }

      //if vendor names exist, remove the last ", "
      if (deliveryVendorNames.length() > 0)
        deliveryVendorNames = deliveryVendorNames.substring(0, deliveryVendorNames.length() - 2);

      deliveryMessage = "A delivery block exists for the date selected for: " + deliveryVendorNames + ".  Please fix.";
    }

    if (deliveryBlockExists && shippingBlockExists)
      this.OK_ERROR_MESSAGE = deliveryMessage + "  " + shippingMessage;
    else if (deliveryBlockExists)
      this.OK_ERROR_MESSAGE = deliveryMessage;
    else if (shippingBlockExists)
      this.OK_ERROR_MESSAGE = shippingMessage;

    blockExists = deliveryBlockExists || shippingBlockExists;

    return blockExists;
  }


  /*******************************************************************************************
  * checkForDifferences()
  ******************************************************************************************
  *
  *
  */
  private boolean checkForDifference(ZJTripVO tOriginalVO, ZJTripVO tNewVO)
    throws Exception
  {
    boolean differenceFound = false;

    if (!differenceFound && 
        ((tOriginalVO.getTripOriginationDesc() != null && tNewVO.getTripOriginationDesc() == null) || (tOriginalVO.getTripOriginationDesc() == 
                                                                                                       null && 
                                                                                                       tNewVO.getTripOriginationDesc() != 
                                                                                                       null) || 
         (tOriginalVO.getTripOriginationDesc() != null && tNewVO.getTripOriginationDesc() != null && 
          !tOriginalVO.getTripOriginationDesc().equalsIgnoreCase(tNewVO.getTripOriginationDesc()))))
      differenceFound = true;

    if (!differenceFound && tOriginalVO.getInjectionHubId() != tNewVO.getInjectionHubId())
      differenceFound = true;

    if (!differenceFound && 
        ((tOriginalVO.getDeliveryDate() != null && tNewVO.getDeliveryDate() == null) || (tOriginalVO.getDeliveryDate() == 
                                                                                         null && 
                                                                                         tNewVO.getDeliveryDate() != null) || 
         (tOriginalVO.getDeliveryDate() != null && tNewVO.getDeliveryDate() != null && 
          !tOriginalVO.getDeliveryDate().equalsIgnoreCase(tNewVO.getDeliveryDate()))))
      differenceFound = true;

    if (!differenceFound && 
        ((tOriginalVO.getDepartureDate() != null && tNewVO.getDepartureDate() == null) || (tOriginalVO.getDepartureDate() == 
                                                                                           null && 
                                                                                           tNewVO.getDepartureDate() != 
                                                                                           null) || 
         (tOriginalVO.getDepartureDate() != null && tNewVO.getDepartureDate() != null && 
          !tOriginalVO.getDepartureDate().equalsIgnoreCase(tNewVO.getDepartureDate()))))
      differenceFound = true;

    if (!differenceFound && tOriginalVO.getTotalPltQty() != tNewVO.getTotalPltQty())
      differenceFound = true;

    if (!differenceFound && tOriginalVO.getThirdPartyPltQty() != tNewVO.getThirdPartyPltQty())
      differenceFound = true;

    if (!differenceFound && tOriginalVO.getDaysInTransitQty() != tNewVO.getDaysInTransitQty())
      differenceFound = true;


    if (!differenceFound)
    {
      List tOriginalVendorVOList = tOriginalVO.getTripVendorVOList();
      List tNewVendorVOList = tNewVO.getTripVendorVOList();

      HashMap tOriginalVendorVOHash = new HashMap();
      HashMap tNewVendorVOHash = new HashMap();

      for (int i = 0; i < tOriginalVendorVOList.size(); i++)
      {
        ZJTripVendorVO tvVO = (ZJTripVendorVO) tOriginalVendorVOList.get(i);
        tOriginalVendorVOHash.put(tvVO.getVendorId(), tvVO);
      }

      for (int i = 0; i < tNewVendorVOList.size(); i++)
      {
        ZJTripVendorVO tvVO = (ZJTripVendorVO) tNewVendorVOList.get(i);
        tNewVendorVOHash.put(tvVO.getVendorId(), tvVO);
      }

      for (int i = 0; i < tOriginalVendorVOList.size(); i++)
      {
        ZJTripVendorVO tvVO = (ZJTripVendorVO) tOriginalVendorVOList.get(i);
        String vendorId = tvVO.getVendorId();

        ZJTripVendorVO tvOriginalVO = (ZJTripVendorVO) tOriginalVendorVOHash.get(vendorId);
        ZJTripVendorVO tvNewVO = (ZJTripVendorVO) tNewVendorVOHash.get(vendorId);


        if (tvNewVO.getNewTripVendorStatusCode() != null)
        {
          if (!differenceFound && 
              ((tvOriginalVO.getOriginalTripVendorStatusCode() != null && tvNewVO.getNewTripVendorStatusCode() == null) || 
               (tvOriginalVO.getOriginalTripVendorStatusCode() == null && tvNewVO.getNewTripVendorStatusCode() != null) || 
               (tvOriginalVO.getOriginalTripVendorStatusCode() != null && tvNewVO.getNewTripVendorStatusCode() != null && 
                !tvOriginalVO.getOriginalTripVendorStatusCode().equalsIgnoreCase(tvNewVO.getNewTripVendorStatusCode()))))
          {
            differenceFound = true;
            break;
          }

          if (!differenceFound && 
              ((tvOriginalVO.getOriginalOrderCutoffTime() != null && tvNewVO.getNewOrderCutoffTime() == null) || 
               (tvOriginalVO.getOriginalOrderCutoffTime() == null && tvNewVO.getNewOrderCutoffTime() != null) || 
               (tvOriginalVO.getOriginalOrderCutoffTime() != null && tvNewVO.getNewOrderCutoffTime() != null && 
                !tvOriginalVO.getOriginalOrderCutoffTime().equalsIgnoreCase(tvNewVO.getNewOrderCutoffTime()))))
          {
            differenceFound = true;
            break;
          }
        }

      }
    }

    return differenceFound;
  }


  /*******************************************************************************************
  * checkIfPalletQtyCanBeAltered()
  ******************************************************************************************
  *
  *
  */
  private boolean checkPltQtySetTripStatus(ZJTripVO tOriginalVO, ZJTripVO tNewVO)
    throws Exception
  {
    boolean palletQtyCanBeAltered = true;

    long newFTDPltQty = tNewVO.getFtdPltQty();

    long usedFullFTDPltQty = 0;
    long usedPrtlFTDPltQty = 0;
    long usedTotalFTDPltQty = 0;

    for (int i = 0; i < tOriginalVO.getTripVendorVOList().size(); i++)
    {
      ZJTripVendorVO tvOriginalVO = (ZJTripVendorVO) tOriginalVO.getTripVendorVOList().get(i);

      if (tvOriginalVO.getOriginalTripVendorStatusCode() != null && 
          (tvOriginalVO.getOriginalTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_ASSIGNED) || 
           tvOriginalVO.getOriginalTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_UNASSIGNED)))
      {
        usedFullFTDPltQty += tvOriginalVO.getFullPltQty();
        if (tvOriginalVO.getPrtlPltInUseFlag().equalsIgnoreCase("Y"))
          usedPrtlFTDPltQty += 1;
      }
    }
    usedTotalFTDPltQty = usedFullFTDPltQty + usedPrtlFTDPltQty;

    if (usedTotalFTDPltQty > newFTDPltQty)
    {
      palletQtyCanBeAltered = false;
      this.OK_ERROR_MESSAGE = "Pallet Qty cannot be altered.  Please fix";
    }
    else if (usedPrtlFTDPltQty == 0 && usedFullFTDPltQty == newFTDPltQty)
    {
      tNewVO.setActualTripStatusCode(OPMConstants.STATUS_TRIP_FULL);
      tNewVO.setVirtualTripStatusCode(null);
    }
    else if (usedPrtlFTDPltQty > 0 || usedTotalFTDPltQty < newFTDPltQty)
    {
      tNewVO.setActualTripStatusCode(OPMConstants.STATUS_TRIP_ACTIVE);
      tNewVO.setVirtualTripStatusCode(null);
    }

    return palletQtyCanBeAltered;
  }


  /*******************************************************************************************
  * convertTripStatusListToXML()
  ******************************************************************************************
  *
  *
  */
  private Document convertTripStatusListToXML(List voList)
    throws Exception
  {

	Document parser;
    ZJTripStatusVO statusVO = null;
    Document statusXML = (Document) DOMUtil.getDefaultDocument();

    //generate the top node
    Element statusElement = statusXML.createElement("TRIP_STATUSES");
    //and append it
    statusXML.appendChild(statusElement);

    int elementNum = 0;
    for (int x = 0; x < voList.size(); x++)
    {
      elementNum++;
      statusVO = (ZJTripStatusVO) voList.get(x);
      parser = DOMUtil.getDocument(statusVO.toXML(elementNum));
      statusXML.getDocumentElement().appendChild(statusXML.importNode(parser.getFirstChild(), true));
    }

    return statusXML;

  }


  /*******************************************************************************************
    * convertTripVendorVOListToXML()
  ******************************************************************************************
  *
  *
  */
  private Document convertTripVendorVOListToXML(List tvVOList)
    throws Exception
  {

	Document parser;
    ZJTripVendorVO tvVO = null;
    Document tripVendorsXML = (Document) DOMUtil.getDefaultDocument();

    //generate the top node
    Element tripVendorElement = tripVendorsXML.createElement("TRIP_VENDORS");
    //and append it
    tripVendorsXML.appendChild(tripVendorElement);

    int elementNum = 0;
    for (int x = 0; x < tvVOList.size(); x++)
    {
      elementNum++;
      tvVO = (ZJTripVendorVO) tvVOList.get(x);
      parser = DOMUtil.getDocument(tvVO.toXML(elementNum));
      tripVendorsXML.getDocumentElement().appendChild(tripVendorsXML.importNode(parser.getFirstChild(), true));
    }

    return tripVendorsXML;

  }


  /*******************************************************************************************
  * convertTripVOListToXML()
  ******************************************************************************************
  *
  *
  */
  private Document convertTripVOListToXML(List voList)
    throws Exception
  {

	Document parser;
    ZJTripVO tVO = null;
    Document tripsXML = (Document) DOMUtil.getDefaultDocument();

    //generate the top node
    Element tripElement = tripsXML.createElement("TRIPS");
    //and append it
    tripsXML.appendChild(tripElement);

    int elementNum = 0;
    for (int x = 0; x < voList.size(); x++)
    {
      elementNum++;
      tVO = (ZJTripVO) voList.get(x);
      parser = DOMUtil.getDocument(tVO.toXML(elementNum));
      tripsXML.getDocumentElement().appendChild(tripsXML.importNode(parser.getFirstChild(), true));
    }

    return tripsXML;

  }


  /*******************************************************************************************
  * convertTripVOToXML()
  ******************************************************************************************
  *
  *
  */
  private Document convertTripVOToXML(ZJTripVO tVO)
    throws Exception
  {

		Document parser;
    Document tripsXML = (Document) DOMUtil.getDefaultDocument();

    //generate the top node
    Element tripElement = tripsXML.createElement("TRIPS");
    //and append it
    tripsXML.appendChild(tripElement);

    int elementNum = 0;
    parser = DOMUtil.getDocument(tVO.toXML(elementNum));
    tripsXML.getDocumentElement().appendChild(tripsXML.importNode(parser.getFirstChild(), true));

    return tripsXML;

  }


  /*******************************************************************************************
  * determinePltCubicInchAllowedQty()
  ******************************************************************************************
  *
  */
  private double determinePltCubicInchAllowedQty()
    throws Exception
  {
    /***************************************************************************************************
     *  Get the value for standard boxes per pallet from Global Parms
     ***************************************************************************************************/
    //retrieve global parms
    HashMap parms = new HashMap();
    MaintenanceDAO mDAO = new MaintenanceDAO(this.con);
    CachedResultSet crsParms = mDAO.getGlobalInfoCRS(OPMConstants.CONTEXT_MERCHANDISING);

    //and store all parms in a HashMap
    while (crsParms.next())
    {
      parms.put(crsParms.getString("name"), crsParms.getString("value"));
    }

    //obtain max box height, width, length, and standard boxes per pallet. 
    String standardBoxesPerPallet = (String) parms.get(OPMConstants.NAME_STANDARD_BOXES_PER_PALLET);
    int iStandardBoxesPerPallet = new Integer(standardBoxesPerPallet).intValue();

    /***************************************************************************************************
     *  Get the dimensions for a standard box
     ***************************************************************************************************/
    double width = 0;
    double height = 0;
    double length = 0;
    CachedResultSet crsBox = mDAO.getStandardBoxInfo();

    while (crsBox.next())
    {
      //retrieve the width_inch_qty
      if (crsBox.getObject("width_inch_qty") != null)
        width = new Double(crsBox.getBigDecimal("width_inch_qty").toString()).doubleValue();

      //retrieve the height_inch_qty
      if (crsBox.getObject("height_inch_qty") != null)
        height = new Double(crsBox.getBigDecimal("height_inch_qty").toString()).doubleValue();

      //retrieve the length_inch_qty
      if (crsBox.getObject("length_inch_qty") != null)
        length = new Double(crsBox.getBigDecimal("length_inch_qty").toString()).doubleValue();

    }

    /***************************************************************************************************
     *  Calculate volume per box, and then total volume per pallet
     ***************************************************************************************************/
    double volume = 0;
    double totalCubicInchesAllowedPerPallet = 0;

    volume = height * width * length;
    totalCubicInchesAllowedPerPallet = volume * iStandardBoxesPerPallet;

    return totalCubicInchesAllowedPerPallet;

  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  */
  private void getConfigInfo()
    throws Exception
  {
    /* retrieve user information */
    if (SecurityManager.getInstance().getUserInfo(this.requestSessionId) != null)
    {
      this.csrId = SecurityManager.getInstance().getUserInfo(this.requestSessionId).getUserID();
    }

    //retrieve global parms
    HashMap parms = new HashMap();
    MaintenanceDAO mDAO = new MaintenanceDAO(this.con);
    CachedResultSet crs = mDAO.getGlobalInfoCRS(OPMConstants.CONTEXT_MERCHANDISING);

    //and store all parms in a HashMap
    while (crs.next())
    {
      parms.put(crs.getString("name"), crs.getString("value"));
    }

    //obtain max pallets per truck. 
    this.maxPalletsPerTruck = (String) parms.get(OPMConstants.NAME_MAX_PALLETS_PER_TRUCK);

  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  */
  private void getRequestInfo()
    throws Exception
  {
    //****retrieve the action type
    if (request.getParameter("action_type") != null)
      this.requestActionType = this.request.getParameter("action_type");

    //****retrieve the adminAction
    if (request.getParameter("adminAction") != null)
      this.requestAdminAction = this.request.getParameter("adminAction");

    //****retrieve the context
    if (request.getParameter("context") != null)
      this.requestContext = this.request.getParameter("context");

    //****retrieve the days_in_transit
    if (request.getParameter("days_in_transit_qty") != null)
      this.requestDaysInTransitQty = this.request.getParameter("days_in_transit_qty");

    //****retrieve the delivery_date
    if (request.getParameter("delivery_date") != null)
      this.requestDeliveryDate = this.request.getParameter("delivery_date");

    //****retrieve the departure_date
    if (request.getParameter("departure_date") != null)
      this.requestDepartureDate = this.request.getParameter("departure_date");

    //****retrieve the filter
    if (request.getParameter("filter") != null && !request.getParameter("filter").equalsIgnoreCase(""))
      this.requestTripFilter = this.request.getParameter("filter");
    else
      this.requestTripFilter = OPMConstants.STATUS_TRIP_ACCEPTING_ORDERS;

    //****retrieve the ih_id
    if (request.getParameter("ih_id") != null)
      this.requestIhId = this.request.getParameter("ih_id");

    //****retrieve the ih_state
    if (request.getParameter("ih_state") != null)
      this.requestIhState = this.request.getParameter("ih_state");

    //****retrieve the original_action_type
    if (request.getParameter("original_action_type") != null)
      this.requestOriginalActionType = this.request.getParameter("original_action_type");

    //****retrieve the third_party_plt_qty
    if (request.getParameter("third_party_plt_qty") != null)
      this.requestThirdPartyPltQty = this.request.getParameter("third_party_plt_qty");

    //****retrieve the total_plt_qty
    if (request.getParameter("total_plt_qty") != null)
      this.requestTotalPltQty = this.request.getParameter("total_plt_qty");

    //****retrieve the total_records
    if (request.getParameter("total_records") != null)
      this.requestTotalRecords = new Integer(this.request.getParameter("total_records")).intValue();

    //****retrieve the trip id
    if (request.getParameter("trip_id") != null)
      this.requestTripId = this.request.getParameter("trip_id");

    //****retrieve the security token
    if (request.getParameter("securitytoken") != null)
      this.requestSessionId = this.request.getParameter("securitytoken");

    //****retrieve the vendor_location
    if (request.getParameter("vendor_location") != null)
      this.requestVendorLocation = this.request.getParameter("vendor_location");

  }


  /*******************************************************************************************
  * insertTrip()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private String insertTrip(ZJTripVO tVO)
    throws Exception
  {
    //Instantiate MaintenanceDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    SimpleDateFormat sdf = new SimpleDateFormat(DELIVERY_DPEARTURE_DATE_FORMAT);
    SimpleDateFormat sdf1 = new SimpleDateFormat(SORT_CODE_DATE_FORMAT);
    String sortCodeDateMMDD = sdf1.format(sdf.parse(tVO.getDepartureDate()));

    Calendar cDeliveryDate = Calendar.getInstance();
    cDeliveryDate.setTime(sdf.parse(tVO.getDeliveryDate()));

    Calendar cDepartureDate = Calendar.getInstance();
    cDepartureDate.setTime(sdf.parse(tVO.getDepartureDate()));

    String tripId = 
      zjDAO.insertTrip(tVO.getTripOriginationDesc(), new Long(tVO.getInjectionHubId()).toString(), cDeliveryDate, 
                       cDepartureDate, OPMConstants.STATUS_TRIP_ACTIVE, this.requestIhState, sortCodeDateMMDD, 
                       new Long(tVO.getTotalPltQty()).toString(), 
                       new Long(tVO.getTotalPltQty() - tVO.getThirdPartyPltQty()).toString(), 
                       new Long(tVO.getThirdPartyPltQty()).toString(), "0", this.csrId,
                       new Long(tVO.getDaysInTransitQty()).toString());

    return tripId;
  }


  /*******************************************************************************************
  * insertVendor()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private void insertVendor(String tripId, ZJTripVendorVO tvVO)
    throws Exception
  {
    //Instantiate MaintenanceDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    zjDAO.insertTripVendor(tripId, tvVO.getVendorId(), tvVO.getNewTripVendorStatusCode(), tvVO.getNewOrderCutoffTime(), 
                           new Double(tvVO.getPltCubicInchAllowedQty()).toString(), "0", "0", "N", "0", "0", this.csrId);
  }


  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  */
  private void populatePageData()
  {
    //store the admin action
    this.pageData.put("adminAction", this.requestAdminAction);

    //store the security token
    this.pageData.put("context", this.requestContext);

    //store the filter
    this.pageData.put("filter", this.requestTripFilter);

    //store the forwardToName
    this.pageData.put("forwardToName", this.forwardToName);

    //store the forwardToType
    this.pageData.put("forwardToType", this.forwardToType);

    //store the max pallet quantity
    this.pageData.put("max_pallets", this.maxPalletsPerTruck);

    this.pageData.put("OK_ERROR_MESSAGE", this.OK_ERROR_MESSAGE);

    //store the action type
    this.pageData.put("original_action_type", this.requestActionType);

    //store the trip id
    this.pageData.put("trip_id", requestTripId);

    //store the security token
    this.pageData.put("securitytoken", this.requestSessionId);

  }


  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction()
    throws Exception
  {
    //If the action equals "add"
    if (this.requestActionType.equalsIgnoreCase("add"))
    {
      processAdd();
    }
    //If the action equals "cancel_trip"
    else if (this.requestActionType.equalsIgnoreCase("cancel_trip"))
    {
      processCancelTrip(con);
    }
    //If the action equals "copy"
    else if (this.requestActionType.equalsIgnoreCase("copy"))
    {
      processCopyEdit();
    }
    //If the action equals "edit"
    else if (this.requestActionType.equalsIgnoreCase("edit"))
    {
      boolean locked = retrieveLock();

      //if record locked, redirect to the summary page
      if (locked)
        processLoad();
      else
        processCopyEdit();
    }
    //If the action equals "load" - that is Initial load or Refresh data
    else if (this.requestActionType.equalsIgnoreCase("load") || this.requestActionType.equalsIgnoreCase("dashboard"))
    {
      processLoad();
    }
    //If the action equals "main_menu"
    else if (this.requestActionType.equalsIgnoreCase("main_menu"))
    {
      processMainMenu();
    }
    //If the action equals "save"
    else if (this.requestActionType.equalsIgnoreCase("save"))
    {
      boolean locked = false;

      //check if we are saving from add, copy, or edit mode.  For edit mode, we want to check lock
      if (this.requestOriginalActionType.equalsIgnoreCase("edit"))
        locked = retrieveLock();

      //if record locked, redirect to the summary page
      if (locked)
        processLoad();
      else
        processSave();

    }
    //If the action is not found as above, throw a new exception
    else
    {
      throw new Exception("Invalid Action Type - Please correct");
    }

  }


  /*******************************************************************************************
  * processAdd()
  ******************************************************************************************
  * Go to the Trip maintenances page in Add mode
  *
  */
  private void processAdd()
    throws Exception
  {
    //define lists
    List tvVOList = null;
    List tripStatusList = null;

    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tvoMap = new HashMap();

    //set the sort by parameter
    String sortBy = "state";

    //Call getTripVendorInfo method in the DAO
    tvoMap = zjDAO.getTripVendorOrderInfo(this.requestTripId, sortBy);

    //Using the OUT_TRIP_CUR cursor, create an XML document
    Document tripXML = this.buildXML(tvoMap, "OUT_TRIP_CUR", "TRIPS", "TRIP", "element");
    this.hashXML.put("HK_trip", tripXML);

    //Using the OUT_INJECTION_HUB_CUR cursor, create an XML document
    Document injectionHubXML = 
      this.buildXML(tvoMap, "OUT_INJECTION_HUB_CUR", "INJECTION_HUBS", "INJECTION_HUB", "element");
    this.hashXML.put("HK_injection_hub", injectionHubXML);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripStatusCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_STATUS_CUR");
    tripStatusList = buildTripStatusList(tripStatusCRS);
    Document tripStatusXML = convertTripStatusListToXML(tripStatusList);
    this.hashXML.put("HK_trip_status", tripStatusXML);

    //Using the OUT_TRIP_VENDOR_STATUS_CUR cursor, create an XML document
    Document tripVendorStatusXML = 
      this.buildXML(tvoMap, "OUT_TRIP_VENDOR_STATUS_CUR", "TRIP_VENDOR_STATUSES", "TRIP_VENDOR_STATUS", "element");
    this.hashXML.put("HK_trip_vendor_status", tripVendorStatusXML);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripVendorOrderCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_VENDOR_ORDER_CUR");
    tvVOList = buildTripVendorVOList(tripVendorOrderCRS);
    Document tvXML = convertTripVendorVOListToXML(tvVOList);
    this.hashXML.put("HK_trip_vendor", tvXML);

    this.forwardToName = "TripMaintenance";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processCancelTrip()
  ******************************************************************************************
  * Cancel the trip and go to the Trip maintenances page in Edit mode
  *
  */
  private void processCancelTrip(Connection connection)
    throws Exception
  {
    UserTransaction userTransaction = null;
    String comments = "ZoneJump - User Canceled the Trip";
    List messageList = new ArrayList();

    try
    {
      userTransaction = Transaction.getTransaction();

      // Start the transaction with the begin method
      userTransaction.begin();

      //Instantiate MaintenanceDAO
      ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

      SimpleDateFormat sdf = new SimpleDateFormat(this.DELIVERY_DPEARTURE_DATE_FORMAT);
      Date dDeliveryDate = sdf.parse(this.requestDeliveryDate);
      Date dDepartureDate = sdf.parse(this.requestDepartureDate);

      CachedResultSet crs = zjDAO.cancelTrip(this.requestTripId, this.csrId);

      while (crs.next())
      {
        //create a new VO
        MessageVO msgVO = new MessageVO(); 
  
        //set the delivery_date
        if (dDeliveryDate != null)
          msgVO.setDeliveryDate(dDeliveryDate);
  
        //set the departure_date
        if (dDepartureDate != null)  
          msgVO.setDepartureDate(dDepartureDate);
    
        //retrieve and set the business_name
        msgVO.setBusinessName(crs.getString("business_name"));
  
        //retrieve and set the card_message
        msgVO.setCardMessage(crs.getString("card_message"));
  
        //set the comments
        msgVO.setComments(comments);
        
        //retrieve and set the filling_vendor
        msgVO.setFillingVendor(crs.getString("filling_vendor"));
  
        //retrieve and set the order_date
        if (crs.getObject("order_date") != null)
          msgVO.setOrderDate((Date) crs.getDate("order_date"));
  
        //retrieve and set the order_detail_id
        msgVO.setOrderDetailId(crs.getString("order_detail_id"));
  
        //retrieve and set the price
        if (crs.getObject("price") != null)
          msgVO.setPrice(Double.valueOf(crs.getBigDecimal("price").toString()).doubleValue());
  
        //retrieve and set the product_id
        msgVO.setProductId(crs.getString("product_id"));
  
        //retrieve and set the address_1
        msgVO.setRecipientAddress1(crs.getString("address_1"));
  
        //retrieve and set the city
        msgVO.setRecipientCity(crs.getString("city"));
  
        //retrieve and set the country
        msgVO.setRecipientCountry(crs.getString("country"));
  
        //retrieve and set the recipient
        msgVO.setRecipientName(crs.getString("recipient"));
  
        //retrieve and set the phone_number
        msgVO.setRecipientPhoneNumber(crs.getString("phone_number"));
  
        //retrieve and set the state
        msgVO.setRecipientState(crs.getString("state"));
  
        //retrieve and set the zip
        msgVO.setRecipientZip(crs.getString("zip"));
  
        //retrieve and set the ship_method
        msgVO.setShipMethod(crs.getString("ship_method"));
  
        //retrieve and set the ship_date
        if (crs.getObject("ship_date") != null)
          msgVO.setShipDate((Date) crs.getDate("ship_date"));

        //retrieve and set the venus_id
        msgVO.setVenusId(crs.getString("venus_id"));
  
        //retrieve and set the venus_order_number
        msgVO.setVenusOrderNumber(crs.getString("venus_order_number"));
  
        //add the messageVO to the list
        messageList.add(msgVO);      
  
      }
      userTransaction.commit();

      if (messageList != null && messageList.size() > 0)
      {
        logger.debug("ZoneJump - Canceling trip - " + messageList.size() + " orders will be canceled and resent.");
        sendCancelAndFTD(messageList, connection);
      }

      this.OK_ERROR_MESSAGE = "Successful";
    }
    catch (Exception e)
    {
      //rollback transaction
      Transaction.rollback(userTransaction);

      throw e;
    }

    this.requestActionType = "edit";
    processCopyEdit();

  }


  /*******************************************************************************************
  * processCopyEdit()
  ******************************************************************************************
  * retrieve the trip id to be edited, and display the maintenance screen in Copy/Edit mode
  *
  */
  private void processCopyEdit()
    throws Exception
  {
    //define lists and VOs
    List tvVOList = null;
    List tripStatusList = null;
    ZJTripVO tVO = null;

    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tvoMap = new HashMap();

    //set the sort by parameter
    String sortBy = null;
    if (this.requestActionType.equalsIgnoreCase("edit"))
      sortBy = "tv_trip_vendor_status_code, state, vendor_id";
    else
      sortBy = "state, vendor_id";

    //Call getTripVendorInfo method in the DAO
    tvoMap = zjDAO.getTripVendorOrderInfo(this.requestTripId, sortBy);

    //Using the OUT_TRIP_CUR cursor, create an XML document
    CachedResultSet tripCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_CUR");
    tVO = buildTripVO(tripCRS);
    //for copy, null/zero out the fields that users cannot copy
    if (this.requestActionType.equalsIgnoreCase("copy"))
    {
      tVO.setCreatedBy(null);
      tVO.setCreatedOn(null);
      tVO.setDeliveryDate(null);
      tVO.setDepartureDate(null);
      tVO.setMaxOrderCutoffTime(null);
      tVO.setSortCode(null);
      tVO.setSortCodeDateMMDD(null);
      tVO.setSortCodeSeq(0);
      tVO.setSortCodeStateMasterId(null);
      tVO.setTripId(0);
      this.requestTripId = null;
      tVO.setActualTripStatusCode(null);
      tVO.setVirtualTripStatusCode(null);
      tVO.setTVRejectPrintedExist("N");
      tVO.setTVPrintedExist("N");
      tVO.setUpdatedBy(null);
      tVO.setUpdatedOn(null);
    }

    //Using the VO, create an XML document
    Document tripXML = convertTripVOToXML(tVO);
    //tripXML.print(System.out);
    this.hashXML.put("HK_trip", tripXML);

    //Using the OUT_INJECTION_HUB_CUR cursor, create an XML document
    Document injectionHubXML = 
      this.buildXML(tvoMap, "OUT_INJECTION_HUB_CUR", "INJECTION_HUBS", "INJECTION_HUB", "element");
    this.hashXML.put("HK_injection_hub", injectionHubXML);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripStatusCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_STATUS_CUR");
    tripStatusList = buildTripStatusList(tripStatusCRS);
    Document tripStatusXML = convertTripStatusListToXML(tripStatusList);
    this.hashXML.put("HK_trip_status", tripStatusXML);

    //Using the OUT_TRIP_VENDOR_STATUS_CUR cursor, create an XML document
    Document tripVendorStatusXML = 
      this.buildXML(tvoMap, "OUT_TRIP_VENDOR_STATUS_CUR", "TRIP_VENDOR_STATUSES", "TRIP_VENDOR_STATUS", "element");
    this.hashXML.put("HK_trip_vendor_status", tripVendorStatusXML);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripVendorOrderCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_VENDOR_ORDER_CUR");
    tvVOList = buildTripVendorVOList(tripVendorOrderCRS);
    Document tvXML = convertTripVendorVOListToXML(tvVOList);
    this.hashXML.put("HK_trip_vendor", tvXML);

    this.forwardToName = "TripMaintenance";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processInsert()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private void processInsert(ZJTripVO tVO)
    throws Exception
  {
    UserTransaction userTransaction = null;

    try
    {
      userTransaction = Transaction.getTransaction();

      // Start the transaction with the begin method
      userTransaction.begin();

      //insert the trip
      String tripId = insertTrip(tVO);
      tVO.setTripId(new Long(tripId).longValue());
      this.requestTripId = tripId;

      //insert the vendors
      ZJTripVendorVO tvVO = null;
      for (int i = 0; i < tVO.getTripVendorVOList().size(); i++)
      {
        tvVO = (ZJTripVendorVO) tVO.getTripVendorVOList().get(i);
        if (tvVO.getNewTripVendorStatusCode() != null && !tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(""))
          insertVendor(tripId, tvVO);
      }

      userTransaction.commit();
    }
    catch (Exception e)
    {
      //rollback transaction
      Transaction.rollback(userTransaction);

      throw e;
    }
  }


  /*******************************************************************************************
  * processLoad()
  ******************************************************************************************
  * Retrieve the box info for the summary page.
  *
  */
  private void processLoad()
    throws Exception
  {
    if (this.requestTripId != null && !this.requestTripId.equalsIgnoreCase(""))
      releaseLock();

    //define lists
    List tripVendorList = null;
    List tripStatusList = null;

    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tvMap = new HashMap();

    //Call getTripVendorInfo method in the DAO
    tvMap = zjDAO.getTripVendorInfo(this.requestTripFilter);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripStatusCRS = (CachedResultSet) tvMap.get("OUT_TRIP_STATUS_CUR");
    tripStatusList = buildTripStatusList(tripStatusCRS);
    Document tripStatusXML = convertTripStatusListToXML(tripStatusList);
    this.hashXML.put("HK_trip_status", tripStatusXML);


    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripVendorCRS = (CachedResultSet) tvMap.get("OUT_TRIP_VENDOR_CUR");
    tripVendorList = buildTripVOList(tripVendorCRS);
    Document tripVendorXML = convertTripVOListToXML(tripVendorList);
    this.hashXML.put("HK_trip_vendor", tripVendorXML);

    //retrieve all the statues that a vendor can have 
    Document tripVendorStatusXML = zjDAO.getTripVendorStatus(); 
    this.hashXML.put("HK_trip_vendor_status", tripVendorStatusXML);
    
    this.forwardToType = "XSL";
    if (this.requestActionType.equalsIgnoreCase("dashboard"))
      this.forwardToName = "Dashboard";
    else
      this.forwardToName = "TripSummary";
  }


  /*******************************************************************************************
  * processMainMenu()
  ******************************************************************************************
  * Go back to the Main Menu
  *
  */
  private void processMainMenu()
    throws Exception
  {
    if (this.requestTripId != null && !this.requestTripId.equalsIgnoreCase(""))
      releaseLock();

    this.forwardToName = "MainMenuAction";
    this.forwardToType = "ACTION";
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest()
    throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo();

    //Get config file info
    getConfigInfo();

    //process the action
    processAction();

    //populate the remainder of the fields on the page data
    populatePageData();


    //At this point, we should have two HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps 3 into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while (iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData", (HashMap) this.pageData);

    return returnHash;
  }


  /*******************************************************************************************
  * processSave()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private void processSave()
    throws Exception
  {
    boolean errorFound = false;
    boolean differenceFound = false;
    boolean retrieveTripInfoAgain = false;

    ArrayList tvVOList = null;
    List tripStatusList = null;
    ZJTripVO tNewVO = new ZJTripVO();
    ZJTripVO tOriginalVO = null;

    /***************************************************************************************************
     * Retrieve ALL data from table just as if we were displaying the page.  Once VOs have been created,
     * we will override the trip and vendor info based on user changes
     **************************************************************************************************/

    //************************************************************************************************
    /**************************** create VOs like original load **************************************/
    //************************************************************************************************
    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tvoMap = new HashMap();

    //set the sort by parameter
    String sortBy = null;
    if (this.requestOriginalActionType.equalsIgnoreCase("edit"))
      sortBy = "tv_trip_vendor_status_code, state, vendor_id";
    else
      sortBy = "state, vendor_id";

    //Call getTripVendorInfo method in the DAO
    tvoMap = zjDAO.getTripVendorOrderInfo(this.requestTripId, sortBy);

    //Using the OUT_TRIP_CUR cursor, create an XML document
    CachedResultSet tripCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_CUR");
    tOriginalVO = buildTripVO(tripCRS);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripVendorOrderCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_VENDOR_ORDER_CUR");
    tvVOList = (ArrayList) buildTripVendorVOList(tripVendorOrderCRS);
    tOriginalVO.setTripVendorVOList(tvVOList);

    //************************************************************************************************
    /**************************** override info based on user input **********************************/
    //************************************************************************************************    
    tNewVO = (ZJTripVO) tOriginalVO.clone();
    tNewVO.setTripOriginationDesc(this.requestVendorLocation);
    tNewVO.setInjectionHubId(new Long(this.requestIhId).longValue());
    tNewVO.setDaysInTransitQty(new Long(this.requestDaysInTransitQty).longValue());
    tNewVO.setDeliveryDate(this.requestDeliveryDate);
    tNewVO.setDepartureDate(this.requestDepartureDate);
    tNewVO.setTotalPltQty(new Long(this.requestTotalPltQty).longValue());
    tNewVO.setThirdPartyPltQty(new Long(this.requestThirdPartyPltQty).longValue());
    tNewVO.setFtdPltQty((long) tNewVO.getTotalPltQty() - tNewVO.getThirdPartyPltQty());
    tNewVO.setTripVendorVOList(null);

    double maxPltCubicInchAllowedQty = determinePltCubicInchAllowedQty();
    List tvVONewList = new ArrayList();
    //loop thru all the vendors and update the tNewVO.vendors - also retrieve/set the max order cutoff time for a trip
    String maxOrderCutoffTime = "0000";
    for (int i = 0; i < tOriginalVO.getTripVendorVOList().size(); i++)
    {
      ZJTripVendorVO tvOriginalVO = (ZJTripVendorVO) tOriginalVO.getTripVendorVOList().get(i);
      ZJTripVendorVO tvNewVO = new ZJTripVendorVO();
      tvNewVO.setAddress1(tvOriginalVO.getAddress1());
      tvNewVO.setCity(tvOriginalVO.getCity());
      tvNewVO.setCreatedBy(tvOriginalVO.getCreatedBy());
      tvNewVO.setCreatedOn(tvOriginalVO.getCreatedOn());
      tvNewVO.setFullPltOrderQty(tvOriginalVO.getFullPltOrderQty());
      tvNewVO.setFullPltQty(tvOriginalVO.getFullPltQty());
      tvNewVO.setPltCubicInchAllowedQty(maxPltCubicInchAllowedQty);
      tvNewVO.setPrtlPltCubicInUsedQty(tvOriginalVO.getPrtlPltCubicInUsedQty());
      tvNewVO.setPrtlPltInUseFlag(tvOriginalVO.getPrtlPltInUseFlag());
      tvNewVO.setPrtlPltOrderQty(tvOriginalVO.getPrtlPltOrderQty());
      tvNewVO.setState(tvOriginalVO.getState());
      tvNewVO.setTripId(tvOriginalVO.getTripId());
      tvNewVO.setTripVendorOrderVOList(tvOriginalVO.getTripVendorOrderVOList());
      tvNewVO.setTVORejectPrintedExist(tvOriginalVO.getTVORejectPrintedExist());
      tvNewVO.setUpdatedBy(tvOriginalVO.getUpdatedBy());
      tvNewVO.setUpdatedOn(tvOriginalVO.getUpdatedOn());
      tvNewVO.setVendorId(tvOriginalVO.getVendorId());
      tvNewVO.setVendorName(tvOriginalVO.getVendorName());

      //retrieve status from the request
      String newVendorStatus = null;
      if (this.request.getParameter("new_vendor_status_" + tvNewVO.getVendorId()) != null && 
          !this.request.getParameter("new_vendor_status_" + tvNewVO.getVendorId()).equalsIgnoreCase(""))
        newVendorStatus = this.request.getParameter("new_vendor_status_" + tvNewVO.getVendorId());
      tvNewVO.setNewTripVendorStatusCode(newVendorStatus);
      tvNewVO.setOriginalTripVendorStatusCode(tvOriginalVO.getOriginalTripVendorStatusCode());

      //retrieve cutoff from the request
      String newVendorCutoff = null;
      if (this.request.getParameter("new_vendor_cutoff_" + tvNewVO.getVendorId()) != null && 
          !this.request.getParameter("new_vendor_cutoff_" + tvNewVO.getVendorId()).equalsIgnoreCase(""))
        newVendorCutoff = this.request.getParameter("new_vendor_cutoff_" + tvNewVO.getVendorId());
      tvNewVO.setNewOrderCutoffTime(newVendorCutoff);
      tvNewVO.setOriginalOrderCutoffTime(tvOriginalVO.getOriginalOrderCutoffTime());

      //set the max order cutoff 
      if (tvNewVO.getNewOrderCutoffTime().compareTo(maxOrderCutoffTime) > 0)
        maxOrderCutoffTime = tvNewVO.getNewOrderCutoffTime();

      tvVONewList.add(tvNewVO);
    }

    //set the remaining objecst in the trip vo
    tNewVO.setTripVendorVOList(tvVONewList);
    tNewVO.setMaxOrderCutoffTime(maxOrderCutoffTime);


    /***************************************************************************************************
     * check to see if there is a global/vendor/shipping block for the delivery/departure date. if yes,
     * load page with error
     **************************************************************************************************/
    if (!errorFound)
      errorFound = checkForDateBlock(tNewVO);


    /***************************************************************************************************
     * check if there is a difference between new and original vo's, both for trip and vendors
     * if difference found, set the difference flag
     **************************************************************************************************/
    if (!errorFound)
    {
      if (this.requestOriginalActionType.equalsIgnoreCase("add") || this.requestOriginalActionType.equalsIgnoreCase("copy"))
      {
        tNewVO.setActualTripStatusCode(OPMConstants.STATUS_TRIP_ACTIVE);
      }
      else
      {
        if (checkForDifference(tOriginalVO, tNewVO))
          differenceFound = true;

        /***************************************************************************************************
         * if pallet quantities have changed, we need to check if pallet quantities can be changed
         **************************************************************************************************/
        if (differenceFound && 
            (tOriginalVO.getTotalPltQty() != tNewVO.getTotalPltQty() || tOriginalVO.getFtdPltQty() != tNewVO.getFtdPltQty() || 
             tOriginalVO.getThirdPartyPltQty() != tNewVO.getThirdPartyPltQty()))
        {
          boolean palletQtyCanBeAltered = checkPltQtySetTripStatus(tOriginalVO, tNewVO);
          if (!palletQtyCanBeAltered)
            errorFound = true;
        }
      }
    }


    /***************************************************************************************************
     * if no errors found, insert/update the record in the database
     **************************************************************************************************/

    if (!errorFound)
    {
      if (this.requestOriginalActionType.equalsIgnoreCase("add") || this.requestOriginalActionType.equalsIgnoreCase("copy"))
      {
        processInsert(tNewVO);
        
        //once inserted, put a lock on this trip. 
        String sTripId = new Long(tNewVO.getTripId()).toString(); 
        boolean locked = retrieveLock(sTripId);
        if (locked)
          throw new Exception("Record was just created; it should not be locked. Trip id = " + sTripId); 
        retrieveTripInfoAgain = true;
      }
      else
      {
        if (differenceFound)
        {
          processUpdate(tNewVO, con);
          retrieveTripInfoAgain = true;
        }
      }
      this.OK_ERROR_MESSAGE = "Successful";
    }


    /***********************************************************************************************************
     * Note that we could have used the info from existing tvoMap but because of the OUT_TRIP_VENDOR_ORDER_CUR.
     * If we editted a trip, we could have potentially Canceled a vendor from a trip, in which case, we need new
     * info for OUT_TRIP_VENDOR_ORDER_CUR
     ************************************************************************************************************/
    if (retrieveTripInfoAgain)
    {
      //Call getTripVendorInfo method in the DAO
      tvoMap = zjDAO.getTripVendorOrderInfo(new Long(tNewVO.getTripId()).toString(), sortBy);

      //Using the OUT_TRIP_CUR cursor, create an XML document
      tripCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_CUR");
      tNewVO = buildTripVO(tripCRS);

      //Using the OUT_TRIP_CUR cursor, create an XML document
      tripVendorOrderCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_VENDOR_ORDER_CUR");
      tvVOList = (ArrayList) buildTripVendorVOList(tripVendorOrderCRS);

      //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
      Document tvXML = convertTripVendorVOListToXML(tvVOList);
      this.hashXML.put("HK_trip_vendor", tvXML);

      this.requestActionType = "edit";
    }
    else
    {
      //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
      Document tvXML = convertTripVendorVOListToXML(tNewVO.getTripVendorVOList());
      this.hashXML.put("HK_trip_vendor", tvXML);

      this.requestActionType = this.requestOriginalActionType;
    }
    /***************************************************************************************************
     * Build XMLs and forward to the maintenance page
     **************************************************************************************************/

    //null out the trip vendor vo list info... we do not want this info in the <TRIPS> node
    tNewVO.setTripVendorVOList(null);
    //Using the VO, create an XML document
    Document tripXML = convertTripVOToXML(tNewVO);
    //tripXML.print(System.out);
    this.hashXML.put("HK_trip", tripXML);

    //Using the OUT_INJECTION_HUB_CUR cursor, create an XML document
    Document injectionHubXML = 
      this.buildXML(tvoMap, "OUT_INJECTION_HUB_CUR", "INJECTION_HUBS", "INJECTION_HUB", "element");
    this.hashXML.put("HK_injection_hub", injectionHubXML);

    //Create a CachedResultSet object using the cursor name that was passed, an convert it into an XML document
    CachedResultSet tripStatusCRS = (CachedResultSet) tvoMap.get("OUT_TRIP_STATUS_CUR");
    tripStatusList = buildTripStatusList(tripStatusCRS);
    Document tripStatusXML = convertTripStatusListToXML(tripStatusList);
    this.hashXML.put("HK_trip_status", tripStatusXML);

    //Using the OUT_TRIP_VENDOR_STATUS_CUR cursor, create an XML document
    Document tripVendorStatusXML = 
      this.buildXML(tvoMap, "OUT_TRIP_VENDOR_STATUS_CUR", "TRIP_VENDOR_STATUSES", "TRIP_VENDOR_STATUS", "element");
    this.hashXML.put("HK_trip_vendor_status", tripVendorStatusXML);

    this.forwardToName = "TripMaintenance";
    this.forwardToType = "XSL";

  }


  /*******************************************************************************************
  * processUpdate()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private void processUpdate(ZJTripVO tVO, Connection connection)
    throws Exception
  {
    UserTransaction userTransaction = null;
    List newMessageList = null;
    List completeMessageList = new ArrayList();

    try
    {
      userTransaction = Transaction.getTransaction();

      // Start the transaction with the begin method
      userTransaction.begin();

      //update the trip
      updateTrip(tVO);

      //insert the vendors
      ZJTripVendorVO tvVO = null;
      for (int i = 0; i < tVO.getTripVendorVOList().size(); i++)
      {
        tvVO = (ZJTripVendorVO) tVO.getTripVendorVOList().get(i);

        if (((tvVO.getOriginalTripVendorStatusCode() == null || 
              tvVO.getOriginalTripVendorStatusCode().equalsIgnoreCase("")) && tvVO.getNewTripVendorStatusCode() != null && 
             !tvVO.getNewTripVendorStatusCode().equalsIgnoreCase("")) || 
            (tvVO.getOriginalTripVendorStatusCode() != null && !tvVO.getOriginalTripVendorStatusCode().equalsIgnoreCase("") && 
             tvVO.getNewTripVendorStatusCode() != null && !tvVO.getNewTripVendorStatusCode().equalsIgnoreCase("") && 
             (!tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(tvVO.getOriginalTripVendorStatusCode()) || 
              (tvVO.getOriginalOrderCutoffTime() != null && tvVO.getNewOrderCutoffTime() != null && 
               !tvVO.getNewOrderCutoffTime().equalsIgnoreCase(tvVO.getOriginalOrderCutoffTime())))))
        {
          if (tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_ASSIGNED) || 
              tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_UNASSIGNED))
          {
            //if a trip id is not associated with a trip vendor, it's an insert
            if (tvVO.getTripId() == 0)
              insertVendor(new Long(tVO.getTripId()).toString(), tvVO);
            //else its an update
            else
              updateVendor(tvVO);
          }
          else if (tvVO.getNewTripVendorStatusCode().equalsIgnoreCase(OPMConstants.STATUS_TRIP_VENDOR_CANCELED))
          {
            newMessageList = cancelVendor(tvVO, tVO.getDeliveryDate(), tVO.getDepartureDate());
            logger.debug("ZoneJump Canceling vendor on trip = " + tvVO.getTripId() + 
                         ", vendor id = " + tvVO.getVendorId() + 
                         ", vendor name = " + tvVO.getVendorName() + 
                         ", and total orders to be canceled for this vendor = " + newMessageList.size());

            completeMessageList.addAll(newMessageList); 

          }
        }
      }

      userTransaction.commit();

      if (completeMessageList != null && completeMessageList.size() > 0)
      {
        logger.debug("ZoneJump - Canceling vendors - " + completeMessageList.size() + " orders will be canceled and resent.");
        sendCancelAndFTD(completeMessageList, connection);
      }
    }
    catch (Exception e)
    {
      //rollback transaction
      Transaction.rollback(userTransaction);

      throw e;
    }
  }


  /*******************************************************************************************
  * releaseLock()
  ******************************************************************************************
  *
  *
  */
  private void releaseLock()
    throws Exception
  {
    LockUtil.releaseLock(this.con, OPMConstants.ZJ_TRIP_ENTITY_TYPE, this.requestTripId, this.requestSessionId, this.csrId);
  }


  /*******************************************************************************************
  * retrieveLock()
  ******************************************************************************************
  *
  *
  */
  private boolean retrieveLock()
    throws Exception
  {
    return retrieveLock(this.requestTripId);
  }


  /*******************************************************************************************
  * retrieveLock()
  ******************************************************************************************
  *
  *
  */
  private boolean retrieveLock(String tripId)
    throws Exception
  {
    String lockedBy = 
      LockUtil.getLock(this.con, OPMConstants.ZJ_TRIP_ENTITY_TYPE, tripId, this.requestSessionId, this.csrId);

    boolean locked = false;
    if (lockedBy != null && !lockedBy.equalsIgnoreCase(""))
    {
      locked = true;
      this.OK_ERROR_MESSAGE = "The record is currently locked by " + lockedBy + ".  Please try again later.";
    }

    return locked;
  }


  /*******************************************************************************************
  * sendCancelAndFTD()
  ******************************************************************************************
  * Send Cancel and Send new FTD
  *
  */
  private void sendCancelAndFTD(List messageList, Connection connection)
    throws Exception
  {
    ResultTO result = null;
    MessageVO msgVO = null;

    for (int x = 0; x < messageList.size(); x++)
    {
      msgVO = (MessageVO) messageList.get(x);

      //send a CAN
      logger.debug("ZoneJump Sending Cancel - order detail id = " + msgVO.getOrderDetailId() + " and vendor id = " + msgVO.getFillingVendor()); 
      sendCAN(result, msgVO, connection);

      //send a new FTD
      logger.debug("ZoneJump Sending FTD - order detail id = " + msgVO.getOrderDetailId() + " and vendor id = " + msgVO.getFillingVendor()); 
      sendFTD(result, msgVO, connection);
    }

  }

  /*******************************************************************************************
  * sendCAN()
  ******************************************************************************************
  * send a can message
  *
  */
  private void sendCAN(ResultTO result, MessageVO msgVO, Connection connection)
    throws Exception
  {
    //get flag to determine if JMS should be sent out.  (used while testing)
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String cancelReason = configUtil.getProperty(OPMConstants.PROPERTY_FILE, "CANCEL_REASON");

    //build venus cancel TO
    CancelOrderTO canTO = new CancelOrderTO();
    canTO.setVenusId(msgVO.getVenusId());
    canTO.setComments(msgVO.getComments());
    canTO.setCsr(this.csrId);
    canTO.setCancelReasonCode(cancelReason);

    //send venus cancel
    result = MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(canTO, connection);

    //check for errors
    if (!result.isSuccess())
    {
      throw new Exception("Could not create Venus CAN message (order detail id=" + msgVO.getOrderDetailId() + "). ERROR: " + 
                          result.getErrorString());
    }

  }


  /*******************************************************************************************
  * sendFTD()
  ******************************************************************************************
  * send a new FTD message
  *
  */
  private void sendFTD(ResultTO result, MessageVO msgVO, Connection connection)
    throws Exception
  {
    //create Venus FTD message TO.
    OrderTO ordTO = new OrderTO();

    ordTO.setReferenceNumber(msgVO.getOrderDetailId());
    ordTO.setAddress1(msgVO.getRecipientAddress1());
    ordTO.setAddress2("");
    ordTO.setRecipient(msgVO.getRecipientName());
    ordTO.setCity(msgVO.getRecipientCity());
    ordTO.setState(msgVO.getRecipientState());
    ordTO.setZipCode(msgVO.getRecipientZip());
    ordTO.setCountry(msgVO.getRecipientCountry());
    ordTO.setPhoneNumber(msgVO.getRecipientPhoneNumber());

    ordTO.setBusinessName(msgVO.getBusinessName());
    ordTO.setCardMessage(msgVO.getCardMessage());
    ordTO.setDeliveryDate(msgVO.getDeliveryDate());
    ordTO.setFillingVendor(msgVO.getFillingVendor());
    ordTO.setMessageType("FTD");
    ordTO.setOperator(this.csrId);
    ordTO.setOrderDate(msgVO.getOrderDate());
    ordTO.setOverUnderCharge(new Double(0.0));
    ordTO.setPrice(msgVO.getPrice());
    ordTO.setProductId(msgVO.getProductId());
    ordTO.setRecipient(msgVO.getRecipientName());
    ordTO.setShipMethod(msgVO.getShipMethod());
    ordTO.setShipDate(msgVO.getShipDate());
    ordTO.setVenusOrderNumber(msgVO.getVenusOrderNumber());
    ordTO.setFromCommunicationScreen(false);

    result = MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(ordTO, connection);

    //check for errors
    if (!result.isSuccess())
    {
      throw new Exception("Could not create Venus FTD message (order detail id=" + msgVO.getOrderDetailId() + "). ERROR: " + 
                          result.getErrorString());
    }
  }


  /*******************************************************************************************
  * updateTrip()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private void updateTrip(ZJTripVO tVO)
    throws Exception
  {
    //Instantiate MaintenanceDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    SimpleDateFormat sdf = new SimpleDateFormat(DELIVERY_DPEARTURE_DATE_FORMAT);
    SimpleDateFormat sdf1 = new SimpleDateFormat(SORT_CODE_DATE_FORMAT);
    String sortCodeDateMMDD = sdf1.format(sdf.parse(tVO.getDepartureDate()));

    Calendar cDeliveryDate = Calendar.getInstance();
    cDeliveryDate.setTime(sdf.parse(tVO.getDeliveryDate()));

    Calendar cDepartureDate = Calendar.getInstance();
    cDepartureDate.setTime(sdf.parse(tVO.getDepartureDate()));

    zjDAO.updateTrip(new Long(tVO.getTripId()).toString(), tVO.getTripOriginationDesc(), 
                     new Long(tVO.getInjectionHubId()).toString(), cDeliveryDate, cDepartureDate, 
                     tVO.getActualTripStatusCode(), this.requestIhState, sortCodeDateMMDD, 
                     new Long(tVO.getTotalPltQty()).toString(), 
                     new Long(tVO.getTotalPltQty() - tVO.getThirdPartyPltQty()).toString(), 
                     new Long(tVO.getThirdPartyPltQty()).toString(), new Long(tVO.getFtdPltUsedQty()).toString(), this.csrId,
                     new Long(tVO.getDaysInTransitQty()).toString());


  }


  /*******************************************************************************************
  * updateVendor()
  ******************************************************************************************
  * Save the trip info and reload the page.
  *
  */
  private void updateVendor(ZJTripVendorVO tvVO)
    throws Exception
  {
    //Instantiate MaintenanceDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    zjDAO.updateTripVendor(new Long(tvVO.getTripId()).toString(), tvVO.getVendorId(), tvVO.getNewTripVendorStatusCode(), 
                           tvVO.getNewOrderCutoffTime(), new Double(tvVO.getPltCubicInchAllowedQty()).toString(), 
                           new Long(tvVO.getFullPltQty()).toString(), new Long(tvVO.getFullPltOrderQty()).toString(), 
                           tvVO.getPrtlPltInUseFlag(), new Long(tvVO.getPrtlPltOrderQty()).toString(), 
                           new Double(tvVO.getPrtlPltCubicInUsedQty()).toString(), this.csrId);

  }


}
