package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.dao.InventoryTrackingDAO;

import java.sql.Connection;

import java.util.HashMap;

import org.w3c.dom.Document;


/**
 * Handles interactions with Inventory Tracking Email Alerts
 *
 *
 * @author Mike Kruger
 */
public class InventoryTrackingEmailAlertsBO
{

  public InventoryTrackingEmailAlertsBO()
  {
  }

  /**
   * Modifies default vendors associated with an email address and modifying 
   * which inventory tracking records the email address is associated with.
   * 
   * @param conn                  Database connection
   * @param emailAddres           Email address to be associated with vendors
   * @param vendorList            List of vendor ids to be associated with the email address
   * @param applyToCurrentFlag    If 'Y' apply to default associations and current inventory tracking records otherwise only apply to default associations
   * @param updatedBy             The user who performed the action
   * @throws Exception
   */
  public void saveInventoryTrackingEmail(Connection conn, String emailAddress, String vendorIdList, String applyToCurrentFlag, String updatedBy)
  throws Exception
  {
    InventoryTrackingDAO inventoryTrackingDAO = new InventoryTrackingDAO(conn);    
    inventoryTrackingDAO.saveInvTrkEmail(emailAddress, vendorIdList, applyToCurrentFlag, updatedBy);
  }

  /**
   * Obtains default vendors associated with an email address and all 
   * active vendors that are not associated with the email address.
   * 
   * @conn                        Database connection
   * @param emailAddres           Email address to search on
   * @return                      HashMap containing default vendors, vendor_chosen, and available vendors, vendor_available  
   * @throws Exception
   */
  public HashMap getVendorByInvTrkEmail(Connection conn, String emailAddress)
  throws Exception
  {
    InventoryTrackingDAO inventoryTrackingDAO = new InventoryTrackingDAO(conn);    
    HashMap results = inventoryTrackingDAO.getVendorByInvTrkEmail(emailAddress);
    return results;
  }

  /**
   * Deletes all inventory tracking data associated with the email address 
   * and sets the email address to inactive.
   * 
   * @param                       Database connection
   * @param emailAddres           Email address to be disassociated
   * @param updatedBy             The user who performed the action
   * @throws Exception
   */
  public void deleteInvTrkEmail(Connection conn, String emailAddress, String updatedBy)
  throws Exception
  {
    InventoryTrackingDAO inventoryTrackingDAO = new InventoryTrackingDAO(conn);    
    inventoryTrackingDAO.deleteInvTrkEmail(emailAddress, updatedBy);
  }

}
