/**
 * 
 */
package com.ftd.op.maintenance.bo;

import java.io.IOException;
import java.sql.Connection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.WebsiteDeliveryFeeTransmission;
import com.ftd.op.maintenance.vo.DeliveryFeeVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

import com.ftd.op.maintenance.util.CommonUtil;

/**
 * @author smeka
 * 
 */
public class DeliveryFeeMaintenanceBO {
	private static Logger logger = new Logger("com.ftd.op.maintenance.bo.DeliveryFeeMaintenanceBO");
	
	private static final String PROPERTY_FILE = "maintenance-config.xml";
	
	private static String DATASOURCE_NAME = "CLEAN";

	/**Convenient method to get the delivery fee info for maintenance from DB.
	 * @param sortField
	 * @param deliveryFeeType
	 * @return
	 */
	public Document getDeliveryFeeList(String sortField, int deliveryFeeType) throws Exception {
		logDebugMessage("getDeliveryFeeList: Sorting based on : " + sortField);
		Connection conn = null;
		Document responseDoc = null;
		
		try {
			conn = this.getDBConnection();
			MaintenanceDAO dao = new MaintenanceDAO(conn);
			responseDoc = dao.getDeliveryFeeList(sortField, deliveryFeeType);
			
		} catch (Exception e) {
			throw new Exception("Error caught getting delivery fee details, " + e.getMessage());
			
		} finally { 
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
		
		return responseDoc;
	}	
	
	/** Convenient method to delete the delivery fee detail.
	 * @param deliveryFeeVO
	 * @throws Exception
	 */
	public void deleteDeliveryFeeData(DeliveryFeeVO deliveryFeeVO) throws Exception {
		logDebugMessage("deleteDeliveryFeeData: for id : " + deliveryFeeVO.getId());
		Connection conn = null;
		
		try {
			conn = this.getDBConnection();
			MaintenanceDAO dao = new MaintenanceDAO(conn);
			dao.deleteDeliveryFee(deliveryFeeVO);

		} catch (Exception e) {
			throw new Exception("Error caught deleting delivery fee details, " + e.getMessage());
			
		} finally { 
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	/** Convenient method to save the delivery fee detail.
	 * @param deliveryFeeVO
	 * @throws Exception
	 */
	public void saveDeliveryFeeData(DeliveryFeeVO deliveryFeeVO) throws Exception {
		logDebugMessage("saveDeliveryFeeData: for id : " + deliveryFeeVO.getId());
		Connection conn = null;
		
		try {
			conn = this.getDBConnection();
			MaintenanceDAO dao = new MaintenanceDAO(conn);
			dao.saveDeliveryFee(deliveryFeeVO);

		} catch (Exception e) {
			throw new Exception("Error caught saving delivery fee details, " + e.getMessage());
			
		} finally { 
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	/** Convenient method to get the delivery fee history.
	 * @param deliveryFeeId
	 * @param deliveryFeeType
	 * @return
	 */
	public Document getDeliveryFeeHistory(String deliveryFeeId, int deliveryFeeType) throws Exception {
		logDebugMessage("getDeliveryFeeHistory() for id : " + deliveryFeeId);
		Connection conn = null;
		Document responseDoc = null;
		
		try {
			conn = this.getDBConnection();
			MaintenanceDAO dao = new MaintenanceDAO(conn);
			responseDoc = dao.getDeliveryFeeHistory(deliveryFeeId, deliveryFeeType);

		} catch (Exception e) {
			throw new Exception("Error caught getting history of a delivery fee id, " + e.getMessage());
			
		} finally { 
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return responseDoc;
	}
	
	/** Convenient method to get the Source codes associated with a given delivery fee Id.
	 * @param deliveryFeeId
	 * @return
	 * @throws Exception
	 */
	public Document getSourceCodesByDeliveryId(String deliveryFeeId) throws Exception {
		logDebugMessage("getSourceCodesByDeliveryId() for id : " + deliveryFeeId);
		Connection conn = null;
		Document responseDoc = null;
		
		try {
			conn = this.getDBConnection();
			MaintenanceDAO dao = new MaintenanceDAO(conn);
			responseDoc = dao.getSourceCodesByDeliveryId(deliveryFeeId);

		} catch (Exception e) {
			throw new Exception("Error caught getting source codes by delivery fee id, " + e.getMessage());
			
		} finally { 
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return responseDoc;
	}
	
	/**
	 * This method is responsible for inserting the Morning delivery fee feed when there is a change in the fee.
	 * @param deliveryFeeVO
	 * @param action
	 * @throws Exception
	
	**/
	public void insertDeliveryFeeFeed(DeliveryFeeVO deliveryFeeVO, String action) throws Exception{
		Document doc = null;
		WebsiteDeliveryFeeTransmission wt =  new WebsiteDeliveryFeeTransmission();
		
		Connection conn = null;
		
		try {
			
			if (deliveryFeeVO.getDeliveryFeeType() == OPMConstants.MORNING_DELIVERY_TYPE) {
				doc = wt.createMorningDeliveryFeeXML(deliveryFeeVO, action);
			}
			
			if(doc != null){
				String transmitData = JAXPUtil.toStringNoFormat(doc);
				conn = this.getDBConnection();
				MaintenanceDAO dao = new MaintenanceDAO(conn);
				dao.insertDeliveryFeeFeed(transmitData);
			}
			
		} catch (Exception e) {
			CommonUtil commUtil =  new CommonUtil();
			commUtil.sendSystemMessage(e.getMessage(),"Fee Feed Error while sending Morning Delivery Feed" ,conn);
			throw new Exception("Error caught saving delivery fee feed details, " + e.getMessage());
			
		} finally { 
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
		
	}
	
	/**
	 * Obtain connectivity with the database
	 */
	private Connection getDBConnection() throws IOException, ParserConfigurationException, 
		SAXException, TransformerException, Exception {
		
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE, DATASOURCE_NAME));
		return conn;
	}
	
	/** Logs the Debug message if debug is enabled
	 * @param message
	 */
	private static void logDebugMessage(String message) {
		if(logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}
	
}
