package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.ZoneJumpDAO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.security.SecurityManager;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;


public class BoxBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest request = null;
  private HttpServletResponse response = null;
  private Connection conn = null;

  //request paramaters
  private String    requestActionType = null;
  private String    requestAdminAction = "";
  private String    requestBoxDesc = null;
  private String    requestBoxHeight = null;
  private String    requestBoxId = null;
  private String    requestBoxLength = null;
  private String    requestBoxName = null;
  private String    requestBoxWidth = null;
  private String    requestContext = null;
  private String    requestSessionId = null;
  private String    requestStandardFlag = null;

  //others
  private String    csrId = null;
  private String    forwardToName = null;
  private String    forwardToType = null;
  private HashMap   hashXML = new HashMap();
  private String    maxBoxHeight = null;
  private String    maxBoxLength = null;
  private String    maxBoxWidth = null;
  private String    newBoxId = null;
  private HashMap   pageData = new HashMap();
  private String    standardBoxesPerPallet = null;


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public BoxBO()
  {
  }

  /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public BoxBO(HttpServletRequest request, Connection conn, HttpServletResponse response)
  {
    this.request = request;
    this.response = response;
    this.conn = conn;
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @param
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws Exception
  */
  public HashMap processRequest()
    throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo();

    //Get config file info
    getConfigInfo();

    //process the action
    processAction();

    //populate the remainder of the fields on the page data
    populatePageData();


    //At this point, we should have two HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many XMLDocuments
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps 3 into one HashMap, called returnHash

    //retrieve all the XMLDocuments from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while (iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData", (HashMap) this.pageData);

    return returnHash;
  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param
  * @return none
  * @throws Exception
  */
  private void getRequestInfo()
    throws Exception
  {
    //****retrieve the action type
    if (request.getParameter("action_type") != null)
      this.requestActionType = this.request.getParameter("action_type");

    //****retrieve the adminAction
    if (request.getParameter("adminAction") != null)
      this.requestAdminAction = this.request.getParameter("adminAction");

    //****retrieve the requestBoxDesc
    if (request.getParameter("box_desc") != null)
      this.requestBoxDesc = this.request.getParameter("box_desc");

    //****retrieve the requestBoxHeight
    if (request.getParameter("box_height") != null)
      this.requestBoxHeight = this.request.getParameter("box_height");

    //****retrieve the requestBoxId
    if (request.getParameter("box_id") != null)
      this.requestBoxId = this.request.getParameter("box_id");

    //****retrieve the requestBoxLength
    if (request.getParameter("box_length") != null)
      this.requestBoxLength = this.request.getParameter("box_length");

    //****retrieve the requestBoxName
    if (request.getParameter("box_name") != null)
      this.requestBoxName = this.request.getParameter("box_name");

    //****retrieve the requestBoxWidth
    if (request.getParameter("box_width") != null)
      this.requestBoxWidth = this.request.getParameter("box_width");

    //****retrieve the context
    if (request.getParameter("context") != null)
      this.requestContext = this.request.getParameter("context");

    //****retrieve the new requestBoxId
    if (request.getParameter("new_box_id") != null)
      this.newBoxId = this.request.getParameter("new_box_id");

    //****retrieve the security token
    if (request.getParameter("securitytoken") != null)
      this.requestSessionId = this.request.getParameter("securitytoken");

    //****retrieve the standard_flag
    if (request.getParameter("standard_flag") != null)
      this.requestStandardFlag = this.request.getParameter("standard_flag");


  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param
  * @return
  * @throws Exception
  */
  private void getConfigInfo()
    throws Exception
  {
    /* retrieve user information */
    if (SecurityManager.getInstance().getUserInfo(this.requestSessionId) != null)
    {
      this.csrId = SecurityManager.getInstance().getUserInfo(this.requestSessionId).getUserID();
    }

    //retrieve global parms
    HashMap parms = new HashMap(); 
    MaintenanceDAO mDAO = new MaintenanceDAO(this.conn);
    CachedResultSet crs = mDAO.getGlobalInfoCRS(OPMConstants.CONTEXT_MERCHANDISING);

    //and store all parms in a HashMap
    while (crs.next())
    {
      parms.put(crs.getString("name"), crs.getString("value"));
    }

    //obtain max box height, width, length, and standard boxes per pallet. 
    this.maxBoxHeight = (String) parms.get(OPMConstants.NAME_MAX_BOX_HEIGHT_IN_INCHES);
    this.maxBoxLength = (String) parms.get(OPMConstants.NAME_MAX_BOX_LENGTH_IN_INCHES);
    this.maxBoxWidth = (String) parms.get(OPMConstants.NAME_MAX_BOX_WIDTH_IN_INCHES);
    this.standardBoxesPerPallet = (String) parms.get(OPMConstants.NAME_STANDARD_BOXES_PER_PALLET);

  }

  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param
  * @return
  * @throws
  */
  private void populatePageData()
  {
    //store the action type
    this.pageData.put("action_type", this.requestActionType);

    //store the admin action
    this.pageData.put("adminAction", this.requestAdminAction);

    //store the security token
    this.pageData.put("context", this.requestContext);

    //store the forwardToName
    this.pageData.put("forwardToName", this.forwardToName);

    //store the forwardToType
    this.pageData.put("forwardToType", this.forwardToType);

    //store the maxBoxHeight
    this.pageData.put("max_box_height", this.maxBoxHeight);

    //store the maxBoxLength
    this.pageData.put("max_box_length", this.maxBoxLength);

    //store the maxBoxWidth
    this.pageData.put("max_box_width", this.maxBoxWidth);

    //store the security token
    this.pageData.put("securitytoken", this.requestSessionId);


  }


  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction()
    throws Exception
  {
    //If the action equals "load" - that is Initial load or Refresh data
    if (this.requestActionType.equalsIgnoreCase("load"))
    {
      processLoad();
    }
    //If the action equals "add"
    else if (this.requestActionType.equalsIgnoreCase("add"))
    {
      processAdd();
    }
    //If the action equals "edit"
    else if (this.requestActionType.equalsIgnoreCase("edit"))
    {
      processEdit();
    }
    //If the action equals "delete"
    else if (this.requestActionType.equalsIgnoreCase("delete"))
    {
      processDelete();
    }
    //If the action equals "exit"
    else if (this.requestActionType.equalsIgnoreCase("main_menu"))
    {
      processMainMenu();
    }
    //If the action equals "save"
    else if (this.requestActionType.equalsIgnoreCase("save"))
    {
      processSave();
    }
    //If the action is not found as above, throw a new exception
    else
    {
      throw new Exception("Invalid Action Type - Please correct");
    }

  }

  /*******************************************************************************************
  * processLoad()
  ******************************************************************************************
  * Retrieve the box info for the summary page.
  *
  */
  private void processLoad()
    throws Exception
  {
    //Instantiate ConfigurationDAO
    MaintenanceDAO mDAO = new MaintenanceDAO(this.conn);

    //Xmldocument that will contain the output from the stored proce
    Document boxInfo;

    //Call getBoxInfo method in the DAO
    boxInfo = mDAO.getBoxInfo(null);

    this.hashXML.put("HK_box", boxInfo);
    this.forwardToName = "BoxSummary";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processAdd()
  ******************************************************************************************
  * Go to the Box maintenances page in Add mode
  *
  */
  private void processAdd()
    throws Exception
  {
    this.forwardToName = "BoxMaintenance";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processEdit()
  ******************************************************************************************
  * retrieve the box id to be edited, and display the maintenance screen in Edit mode
  *
  */
  private void processEdit()
    throws Exception
  {
    //Instantiate MaintenanceDAO
    MaintenanceDAO mDAO = new MaintenanceDAO(this.conn);

    //Xmldocument that will contain the output from the stored proce
    Document boxInfo;

    //Call getBoxInfo method in the DAO
    boxInfo = mDAO.getBoxInfo(this.requestBoxId);

    this.hashXML.put("HK_box", boxInfo);
    this.forwardToName = "BoxMaintenance";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processDelete()
  ******************************************************************************************
  * Delete a box, reassign any products currently using this box to a new box, and refresh and
  * reload the summary screen
  *
  */
  private void processDelete()
    throws Exception
  {
    //Instantiate MaintenanceDAO
    MaintenanceDAO mDAO = new MaintenanceDAO(this.conn);

    //reassign the box on the product_master table to the new box id
    mDAO.reassignBoxToProduct(this.requestBoxId, this.newBoxId, null, this.csrId);

    //delete the box id
    mDAO.deleteBoxInfo(this.requestBoxId);

    //reload the summary page
    processLoad();
  }


  /*******************************************************************************************
  * processMainMenu()
  ******************************************************************************************
  * Go back to the Main Menu
  *
  */
  private void processMainMenu()
    throws Exception
  {
    this.forwardToName = "MainMenuAction";
    this.forwardToType = "ACTION";
  }


  /*******************************************************************************************
  * processSave()
  ******************************************************************************************
  * Save the box info and reload the page.
  *
  */
  private void processSave()
    throws Exception
  {
    //Instantiate MaintenanceDAO
    MaintenanceDAO mDAO = new MaintenanceDAO(this.conn);

    //if box id exist, this was an edit.  Therefore, update
    if (this.requestBoxId != null && !this.requestBoxId.equalsIgnoreCase(""))
    {
      mDAO.updateBoxInfo(this.requestBoxId, this.requestBoxName, this.requestBoxDesc, this.requestBoxWidth, 
                         this.requestBoxHeight, this.requestBoxLength, this.csrId);

      if (this.requestStandardFlag.equalsIgnoreCase("Y"))
      {
        updateActiveTrips(); 
      }
    }
    //if box id does not exist, this was an add.  Therefore, insert.
    else
    {
      mDAO.insertBoxInfo(this.requestBoxName, this.requestBoxDesc, this.requestBoxWidth, this.requestBoxHeight, 
                         this.requestBoxLength, "N", this.csrId);
    }

    //reload the summary page
    processLoad();
  }


  /*******************************************************************************************
  * updateActiveTrips()
  ******************************************************************************************
  *
  */
  private void updateActiveTrips()
    throws Exception
  {

    double volume = 0; 
    double totalCubicInchesAllowedPerPallet = 0; 
    
    double height = new Double(this.requestBoxHeight).doubleValue(); 
    double width = new Double(this.requestBoxWidth).doubleValue(); 
    double length = new Double(this.requestBoxLength).doubleValue(); 
    int iStandardBoxesPerPallet = new Integer(this.standardBoxesPerPallet).intValue(); 
    
    volume = height * width * length; 
    totalCubicInchesAllowedPerPallet = volume * iStandardBoxesPerPallet; 
   
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.conn);
    zjDAO.updateCubicInchesAllowed(new Double(totalCubicInchesAllowedPerPallet).toString(), this.csrId);
    
  }


}
