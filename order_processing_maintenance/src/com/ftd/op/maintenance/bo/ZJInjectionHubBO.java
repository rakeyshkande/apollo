package com.ftd.op.maintenance.bo;

import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.ZoneJumpDAO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

import java.io.StringReader;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;


public class ZJInjectionHubBO
{

  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest request = null;
  private HttpServletResponse response = null;
  private Connection con = null;

  //request paramaters
  private String    requestActionType = null;
  private String    requestAdminAction = "";
  private String    requestContext = null;
  private String    requestIhAddress = null;
  private String    requestIhCarrier = null;
  private String    requestIhCity = null;
  private String    requestIhId = null;
  private String    requestIhSDSAccountNum = null;
  private String    requestIhShipPointId = null;
  private String    requestIhState = null;
  private String    requestIhZip = null;
  private String    requestSessionId = null;

  //others
  private String    csrId = null;
  private String    forwardToName = null;
  private String    forwardToType = null;
  private HashMap   hashXML = new HashMap();
  private HashMap   pageData = new HashMap();


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public ZJInjectionHubBO()
  {
  }

  /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public ZJInjectionHubBO(HttpServletRequest request, Connection con, HttpServletResponse response)
  {
    this.request = request;
    this.response = response;
    this.con = con;
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest()
    throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo();

    //Get config file info
    getConfigInfo();

    //process the action
    processAction();

    //populate the remainder of the fields on the page data
    populatePageData();


    //At this point, we should have two HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps 3 into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while (iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData", (HashMap) this.pageData);

    return returnHash;
  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  
  * @return none
  * @throws Exception
  */
  private void getRequestInfo()
    throws Exception
  {
    //****retrieve the action type
    if (request.getParameter("action_type") != null)
      this.requestActionType = this.request.getParameter("action_type");

    //****retrieve the adminAction
    if (request.getParameter("adminAction") != null)
      this.requestAdminAction = this.request.getParameter("adminAction");

    //****retrieve the context
    if (request.getParameter("context") != null)
      this.requestContext = this.request.getParameter("context");

    //****retrieve the ih_address
    if (request.getParameter("ih_address") != null)
      this.requestIhAddress = this.request.getParameter("ih_address");

    //****retrieve the ih_carrier
    if (request.getParameter("ih_carrier") != null)
      this.requestIhCarrier = this.request.getParameter("ih_carrier");

    //****retrieve the ih_city
    if (request.getParameter("ih_city") != null)
      this.requestIhCity = this.request.getParameter("ih_city");

    //****retrieve the ih_id
    if (request.getParameter("ih_id") != null)
      this.requestIhId = this.request.getParameter("ih_id");

    //****retrieve the ih_SDS_Account_Num
    if (request.getParameter("ih_SDS_Account_Num") != null)
      this.requestIhSDSAccountNum = this.request.getParameter("ih_SDS_Account_Num");

    //****retrieve the ih_Ship_point_id
    if (request.getParameter("ih_ship_point_id") != null)
      this.requestIhShipPointId = this.request.getParameter("ih_ship_point_id");

    //****retrieve the ih_state
    if (request.getParameter("ih_state") != null)
      this.requestIhState = this.request.getParameter("ih_state");

    //****retrieve the ih_zip
    if (request.getParameter("ih_zip") != null)
      this.requestIhZip = this.request.getParameter("ih_zip");

    //****retrieve the securitytoken
    if (request.getParameter("securitytoken") != null)
      this.requestSessionId = this.request.getParameter("securitytoken");

  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param 
  * @return
  * @throws Exception
  */
  private void getConfigInfo()
    throws Exception
  {
    /* retrieve user information */
    if (SecurityManager.getInstance().getUserInfo(this.requestSessionId) != null)
    {
      this.csrId = SecurityManager.getInstance().getUserInfo(this.requestSessionId).getUserID();
    }

  }

  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param 
  * @return
  * @throws Exception
  */
  private void populatePageData()
  {
    //store the action type
    this.pageData.put("action_type", this.requestActionType);

    //store the admin action
    this.pageData.put("adminAction", this.requestAdminAction);

    //store the security token
    this.pageData.put("context", this.requestContext);

    //store the forwardToName
    this.pageData.put("forwardToName", this.forwardToName);

    //store the forwardToType
    this.pageData.put("forwardToType", this.forwardToType);

    //store the security token
    this.pageData.put("securitytoken", this.requestSessionId);


  }


  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction()
    throws Exception
  {
    //If the action equals "load" - that is Initial load or Refresh data
    if (this.requestActionType.equalsIgnoreCase("load"))
    {
      processLoad();
    }
    //If the action equals "add"
    else if (this.requestActionType.equalsIgnoreCase("add"))
    {
      processAdd();
    }
    //If the action equals "edit"
    else if (this.requestActionType.equalsIgnoreCase("edit"))
    {
      processEdit();
    }
    //If the action equals "exit"
    else if (this.requestActionType.equalsIgnoreCase("main_menu"))
    {
      processMainMenu();
    }
    //If the action equals "save"
    else if (this.requestActionType.equalsIgnoreCase("save"))
    {
      processSave();
    }
    //If the action is not found as above, throw a new exception
    else
    {
      throw new Exception("Invalid Action Type - Please correct");
    }

  }

  /*******************************************************************************************
  * processLoad()
  ******************************************************************************************
  * Retrieve the box info for the summary page.
  *
  */
  private void processLoad()
    throws Exception
  {
    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    //Document that will contain the output from the stored proce
    Document ihInfo = DOMUtil.getDefaultDocument();

    //Call getInjectionHubInfo method in the DAO
    ihInfo = zjDAO.getInjectionHubInfo(null);

    this.hashXML.put("HK_ih", ihInfo);
    this.forwardToName = "InjectionHubSummary";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processAdd()
  ******************************************************************************************
  * Go to the Box maintenances page in Add mode
  *
  */
  private void processAdd()
    throws Exception
  {
    /****** retreive and append state info **/
    Document statesInfo = getStates(); 
    this.hashXML.put("HK_states", statesInfo);
    
    /****** retreive and append carrier info **/
    Document carrierInfo = getCarriers(); 
    this.hashXML.put("HK_carriers", carrierInfo);
    
    this.forwardToName = "InjectionHubMaintenance";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processEdit()
  ******************************************************************************************
  * retrieve the box id to be edited, and display the maintenance screen in Edit mode
  *
  */
  private void processEdit()
    throws Exception
  {
    /****** retreive and append injection hub info **/
    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con);

    //Document that will contain the output from the stored proce
    Document ihInfo = DOMUtil.getDefaultDocument();

    //Call getInjectionHubInfo method in the DAO
    ihInfo = zjDAO.getInjectionHubInfo(this.requestIhId);

    this.hashXML.put("HK_ih", ihInfo);

    /****** retreive and append state info **/
    Document statesInfo = getStates(); 
    this.hashXML.put("HK_states", statesInfo);
    
    /****** retreive and append carrier info **/
    Document carrierInfo = getCarriers(); 
    this.hashXML.put("HK_carriers", carrierInfo);
    
    this.forwardToName = "InjectionHubMaintenance";
    this.forwardToType = "XSL";
  }


  /*******************************************************************************************
  * processMainMenu()
  ******************************************************************************************
  * Go back to the Main Menu
  *
  */
  private void processMainMenu()
    throws Exception
  {
    this.forwardToName = "MainMenuAction";
    this.forwardToType = "ACTION";
  }


  /*******************************************************************************************
  * processSave()
  ******************************************************************************************
  * Save the box info and reload the page. 
  *
  */
  private void processSave()
    throws Exception
  {
    //Instantiate ZoneJumpDAO
    ZoneJumpDAO zjDAO = new ZoneJumpDAO(this.con); 

    //if injection hub id exist, this was an edit.  Therefore, update
    if (this.requestIhId != null && !this.requestIhId.equalsIgnoreCase(""))
    {
      zjDAO.updateInjectionHub(this.requestIhId, this.requestIhCity, this.requestIhState, this.requestIhCarrier, this.requestIhAddress, this.requestIhZip, 
                                   this.requestIhSDSAccountNum, this.requestIhShipPointId, this.csrId);
    }
    //if injection hub id does not exist, this was an add.  Therefore, insert.
    else
    {
      zjDAO.insertInjectionHub(this.requestIhCity, this.requestIhState, this.requestIhCarrier, this.requestIhAddress, this.requestIhZip, 
                                   this.requestIhSDSAccountNum, this.requestIhShipPointId, this.csrId);
    }

    //reload the summary page
    processLoad();
  }


  /*******************************************************************************************
  * getStates()
  ******************************************************************************************
  * retrieve the states to be displayed on the maintenance screen in Add/Edit mode
  *
  */
  private Document getStates()
    throws Exception
  {
    Map statesVOMap = new HashMap();
    Map statesVOTMap = new TreeMap();

    //create a new xml document
    Document statesXML = (Document) DOMUtil.getDefaultDocument();

    StateMasterHandler stateHandler = (StateMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_STATE_MASTER);

    //get a hashmap of stateVOs
    statesVOMap = stateHandler.getStatesByNameMap();

    //if the states could not be loaded from the cache
    if(statesVOMap == null)
    {
      //Instantiate MaintenanceDAO
      MaintenanceDAO mDAO = new MaintenanceDAO(con);

      //Create a CachedResultSet object using the cursor name that was passed. 
      CachedResultSet rs = mDAO.getStateCRS();

      while(rs.next())
      {
        StateMasterVO sVO = new StateMasterVO();

        String id = rs.getObject("statemasterid").toString().toUpperCase();
        sVO.setStateMasterId(id);
        String name = rs.getObject("statename").toString().toUpperCase();
        sVO.setStateName(name);
        sVO.setCountryCode((String) rs.getObject("countrycode"));
        sVO.setTimeZone((String) rs.getObject("timezone"));

        //only add the US States
        if (sVO.getCountryCode() == null || sVO.getCountryCode().equalsIgnoreCase(""))
          statesVOTMap.put(name, sVO);
      }

    }
    else
    {
      Set ks1 = statesVOMap.keySet(); 
      StateMasterVO sVO = new StateMasterVO();
      Iterator iter = ks1.iterator();
      String key = null;
      
      //Iterate thru the keyset
      while(iter.hasNext())
      {
        //get the key
        key = iter.next().toString();
        sVO = (StateMasterVO) statesVOMap.get(key); 
        
        //only add the US States
        if (sVO.getCountryCode() == null || sVO.getCountryCode().equalsIgnoreCase(""))
          statesVOTMap.put(key, sVO);
      }
    }

    //if the states are loaded within the cache
    if (statesVOTMap != null)
    {
      //generate the top node
      Element stateElement = statesXML.createElement("STATES");
      //and append it
      statesXML.appendChild(stateElement);
       
      //get the keyset of the hashmap that contains the stateVOs
      Set ks1 = statesVOTMap.keySet(); 
      Iterator iter = ks1.iterator();
      String key = null;
      StateMasterVO sVO;      
      int count = 0;
      
      //iterate thru the hashmap, get the vo and process
      while(iter.hasNext())
      {
        count++;
        
        //get the key
        key = iter.next().toString();
        
        //get an object from the hashmap into a stateVO
        sVO = (StateMasterVO) statesVOTMap.get(key);
          
        //call the toXML() method  
        Document parser = DOMUtil.getDocument(sVO.toXML(count));
        statesXML.getDocumentElement().appendChild(statesXML.importNode(parser.getFirstChild(), true));
      }  
    }

    return statesXML;
  }
  

  /*******************************************************************************************
  * getCarriers()
  ******************************************************************************************
  * retrieve the carrier to be displayed on the maintenance screen in Add/Edit mode
  *
  */
  private Document getCarriers()
    throws Exception
  {
    //Instantiate MaintenanceDAO
    MaintenanceDAO mDAO = new MaintenanceDAO(this.con);

    //Document that will contain the output from the stored proce
    Document carrierInfo = DOMUtil.getDefaultDocument();

    //Call getCarrierInfo method in the DAO
    carrierInfo = mDAO.getCarrierInfo();

    return carrierInfo; 
    
  }





}
