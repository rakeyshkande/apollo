package com.ftd.op.maintenance.util;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.vo.MPRedemptionRateVO;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class NovatorMPRedemptionRateTransmission extends NovatorTransmission
{

  private static final Logger logger = new Logger("com.ftd.op.maintenance.util.NovatorMPRedemptionRateTransmission");

  public NovatorMPRedemptionRateTransmission()
  {
  }

    /**
     * Send the passed in XML message to Novator.
     * Feed to Novator. Valid actions: Add, Delete, Update
     * The procedure will throw an exception if any feed fails.
     * @param rrVO
     * @param action
     */
     
    public String send(NovatorUpdateVO novatorUpdateVO, MPRedemptionRateVO rrVO, String action) throws Exception
    {
        boolean content, test, uat, live;
        String response = null;

        String error = null;
        String environment = null;

        Document doc = createRedemptionRateXML(rrVO, action);
            
        String xmlString = JAXPUtil.toStringNoFormat(doc);
            
        if(logger.isDebugEnabled()) {
            logger.debug(xmlString);
        }
            
        try {
            // Send to Novator
            if (novatorUpdateVO.isUpdateContent()) {
                    try{
                        response = super.sendNovatorXML(xmlString, OPMConstants.NOVATOR_CONTENT_FEED_IP, OPMConstants.NOVATOR_CONTENT_FEED_PORT);
                        super.parseResponse(response);
                        content = true;
                    } catch (Throwable t){
                        environment = CONTENT_ENV; 
                        throw t;
                    }
            }
            if (novatorUpdateVO.isUpdateTest()) {
                    try{
                        response = super.sendNovatorXML(xmlString, OPMConstants.NOVATOR_TEST_FEED_IP, OPMConstants.NOVATOR_TEST_FEED_PORT);
                        super.parseResponse(response);
                        test = true;
                    } catch (Throwable t){
                        environment = TEST_ENV;        
                        throw t;
                    }
            }
            if (novatorUpdateVO.isUpdateUAT()) {
                    try{
                        response = super.sendNovatorXML(xmlString, OPMConstants.NOVATOR_UAT_FEED_IP, OPMConstants.NOVATOR_UAT_FEED_PORT);
                        super.parseResponse(response);
                        uat = true;
                    } catch (Throwable t){
                        environment = UAT_ENV;        
                        throw t;
                    }
            }
            if (novatorUpdateVO.isUpdateLive()) {
                    try{
                        response = super.sendNovatorXML(xmlString, OPMConstants.NOVATOR_PRODUCTION_FEED_IP, OPMConstants.NOVATOR_PRODUCTION_FEED_PORT);
                        super.parseResponse(response);
                        live = true;
                    } catch (Throwable t){
                        environment = LIVE_ENV;        
                        throw t;
                    }
                }
        } catch(Throwable t) {
                error = t.getMessage() + " (" + environment + ")";
                throw new Exception(error);
        }
            
        return null;
    }
    

    /**
     * Creates the input data that will be sent to Novator
     * Feed to Novator. Valid actions: Delete, Update
     * <mpRedemptionRate>
     *      <action>Delete</action>
     *      <id>01</id>
     *      <membershipType>UA</membershipType>
     *      <redemptionRate>0.006</redemptionRate>
     *      <description>United $0.006 per miles</description>
     *  </mpRedemptionRate>
     * @return java.lang.String - XML in a string format that will be sent to Novator
     * @param rrVO -  MPRedemptionRateVO
     * @param action - Add, Delete, or Update
     */
    private Document createRedemptionRateXML(MPRedemptionRateVO rrVO, String action)
        throws Exception
    {
        Document doc = JAXPUtil.createDocument();
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "mpRedemptionRate", "");
        doc.appendChild(root);
        
        Element newElement = JAXPUtil.buildSimpleXmlNode(doc,"action", action);
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"id", rrVO.getRedemptionRateId()==null? "" : rrVO.getRedemptionRateId());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"membershipType", rrVO.getPaymentMethodId()==null? "" : rrVO.getPaymentMethodId());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"redemptionRate", rrVO.getRedemptionRateAmt()==null? "" : rrVO.getRedemptionRateAmt());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"description", rrVO.getDescription()==null? "" : rrVO.getDescription());
        root.appendChild(newElement);

        return doc;
    }

}