package com.ftd.op.maintenance.util;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import javax.naming.InitialContext;

import javax.transaction.UserTransaction;

public class Transaction 
{
  private static Logger logger = new Logger("com.ftd.op.maintenance.util.Transaction");


  public Transaction()
  {
  }
  
  /**
  * Obtain a container managed transaction
  * @return UserTransaction � Container managed transaction
  * @throws Exception
  */
  public static UserTransaction getTransaction() throws Exception
  {
  
    //get the initial context
    InitialContext initialContext = new InitialContext();
    
    //get the transaction timeout
    int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
    
    //retrieve the UserTransaction object
    String jndi_usertransaction_entry =      ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE,  OPMConstants.JNDI_USERTRANSACTION_ENTRY);
    
    UserTransaction userTransaction = (UserTransaction)  initialContext.lookup(jndi_usertransaction_entry);
    userTransaction.setTransactionTimeout(transactionTimeout);
    
    return userTransaction;
  }


  /**
  * Rollback a container managed transaction
  * @param UserTransaction � Container managed transaction
  */
  public static void rollback(UserTransaction userTransaction)
  {
    try
    {
      if (userTransaction != null)  
      {
        // rollback the user transaction
        userTransaction.rollback();
        logger.debug("User transaction rolled back");
      }
    } 
    catch (Exception ex)
    {
      logger.error(ex);
    } 
  }

  
}