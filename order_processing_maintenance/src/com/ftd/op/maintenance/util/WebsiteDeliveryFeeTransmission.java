/**
 * 
 */
package com.ftd.op.maintenance.util;

import java.text.DecimalFormat;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.vo.DeliveryFeeVO;
import com.ftd.op.maintenance.vo.NovatorResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

/**
 * @author smeka
 * 
 */
public class WebsiteDeliveryFeeTransmission extends NovatorTransmission {
	private static final Logger logger = new Logger("com.ftd.op.maintenance.util.WebsiteDeliveryFeeTransmission");

	/**
	 * initialize the server configuration fields required for delivery fee web site operations.
	 */
	public WebsiteDeliveryFeeTransmission() {
		super(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, OPMConstants.NOVATOR_SERVER, OPMConstants.NOVATOR_PORT);
	}
	
	/** Construct the request XML, Send the XML request to Web site. 
	 * @param dfVO
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public String sendDeliveryFee(DeliveryFeeVO dfVO, String action) throws Exception {
		Document doc = null;
		if (dfVO.getDeliveryFeeType() == OPMConstants.MORNING_DELIVERY_TYPE) {
			doc = createMorningDeliveryFeeXML(dfVO, action);
		}		
		String transmitData = JAXPUtil.toStringNoFormat(doc);

		if (logger.isDebugEnabled()) {
			logger.debug("sendDeliveryFee request: " + transmitData);
		}
		String response = null;
		try {
			response = sendNovatorXML(transmitData);
			if (logger.isDebugEnabled()) {
				logger.debug("sendDeliveryFee response: " + response);
			}
			NovatorResponseVO responseVO = parseNovatorResponse(response);
			if (responseVO.isParseable()) {
				response = "CON";
				if (!responseVO.isSuccess()) {
					response = "DEN";
				}
			}
		} catch (Throwable t) {
			throw new Exception(t.getMessage());
		}
		return response;
	}

	/** Creates Morning Delivery fee request XML to be sent to web site
	 * @param dfVO
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public Document createMorningDeliveryFeeXML(DeliveryFeeVO dfVO, String action) throws Exception {
		return createDeliveryFeeXML(dfVO, action, "morningDelivery", "morningDeliveryFee");
	}

	/** Creates Delivery fee request XML to be sent to Novator, for a given delivery fee rootTag and fee tag
	 * @param dfVO
	 * @param action
	 * @param rootTag
	 * @param feeTag
	 * @return
	 * @throws Exception
	 */
	private Document createDeliveryFeeXML(DeliveryFeeVO dfVO, String action,
			String rootTag, String feeTag) throws Exception {
		DecimalFormat df = new DecimalFormat("#,###,##0.00");
		Document doc = JAXPUtil.createDocument();
		
		Element root = JAXPUtil.buildSimpleXmlNode(doc, rootTag, "");
		doc.appendChild(root);

		Element newElement = JAXPUtil.buildSimpleXmlNode(doc, "action", action);
		root.appendChild(newElement);
		
		newElement = JAXPUtil.buildSimpleXmlNode(doc, "id",	dfVO.getId() == null ? "" : dfVO.getId());
		root.appendChild(newElement);
		
		newElement = JAXPUtil.buildSimpleXmlNode(doc, feeTag, df.format(dfVO.getDeliveryFee()));
		root.appendChild(newElement);
		
		newElement = JAXPUtil.buildSimpleXmlNode(doc, "description", dfVO.getDescription());
		root.appendChild(newElement);
		
		return doc;
	}

}
