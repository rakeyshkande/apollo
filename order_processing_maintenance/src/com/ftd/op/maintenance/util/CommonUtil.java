package com.ftd.op.maintenance.util;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.HashMap;

import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

public class CommonUtil 
{ 
	private static Logger logger = new Logger("com.ftd.op.maintenance.CommonUtil");
   public static final String PROPERTY_FILE = "maintenance-config.xml";
   private static String DATASOURCE_NAME = "CLEAN";


    public CommonUtil()
    {
    }
    
	/**
	 * Obtain connectivity with the database
	 * 
	 * @param 
	 * @return Connection - db connection
	 * @throws ClassNotFoundException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
	 */
	public static Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,
															   DATASOURCE_NAME));

		return conn;
	}
	
	public static void closeConnection(Connection conn){
		try
        {
            //Close the connection
            if (conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
	}
    
    public static String convertToString(Document xml)
    throws Exception
    {
      StringWriter sw = new StringWriter();       
      DOMUtil.print(xml, new PrintWriter(sw));
      return sw.toString(); //string representation of xml document    
    }
    
    public static void addElement(Document xml, String keyName, String value)
    {
        Element nameElement = xml.createElement(keyName);
        
        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);    
    }    
    
  /**
   * This creates an XMLDocument for the data in a cursor with the given top and bottom name.
   * Specifially creates elements and not attibutes for cursor data.  
   * @throws java.lang.Exception
   * @return XML Document
   * @param bottomName
   * @param topName
   * @param cursorName
   * @param outMap
   */
  public static Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName) throws Exception
  {
    return CommonUtil.buildXML(outMap, cursorName, topName, bottomName, "element");
  }  

  /**
   * This creates an XMLDocument for the data in a cursor with the given top and bottom name.
   * Allows you to specify elements or attributes with type parameter.
   * @throws java.lang.Exception
   * @return 
   * @param type
   * @param bottomName
   * @param topName
   * @param cursorName
   * @param outMap
   */
  public static Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type) throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc = null;
  
    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName);

    //call the DOMUtil's converToXMLDOM method
    doc = DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;
  
  }  

  /**
   * This creates an XMLDocument for the data in a cursor with the given top and bottom name.
   * Allows you to specify elements or attributes with type parameter.
   * @throws java.lang.Exception
   * @return
   * @param crs
   * @param topName 
   * @param bottomName
   * @param type - element vs. attribute
   */
  public static Document buildXML(CachedResultSet crs, String topName, String bottomName, String type)
    throws Exception
  {

    Document doc = null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName);

    //call the DOMUtil's converToXMLDOM method
    doc = DOMUtil.convertToXMLDOM(crs, xmlFormat);

    //return the xml document.
    return doc;

  }



    public static String removeNonNumerics(String input)
    {
        String output = "";
    
        for (int i=0; i < input.length(); i++)
        {
            char n = input.charAt(i);
            Character nextCharacter = new Character(n);            
            if(nextCharacter.isDigit(n))
            {
                output = output + nextCharacter.toString();
            }
            
        }
   
        return output;
    }
    
    /**
     * Send email.
     * @param fromAddress
     * @param toAddress
     * @param mailServer
     * @param subject
     * @param content
     * @throws Exception
     */
    public void sendEmail(String fromAddress, String toAddress, String mailServer, String subject, String content) throws Exception
    {        
        try {
                NotificationVO notifVO = new NotificationVO();
                notifVO.setMessageTOAddress(toAddress);
                notifVO.setMessageFromAddress(fromAddress);
                notifVO.setSMTPHost(mailServer);
                notifVO.setMessageSubject(subject);
                notifVO.setMessageContent(content);
                
                /* send out the email */
                NotificationUtil mail = NotificationUtil.getInstance();
                mail.notify(notifVO);

            } catch (Exception e) {
                throw e;
            }    
    }    

    public static void sendPage(String message
                          , Exception exception
                          , String source
                          , int level
                          , String type
                          , String subject
                          , Connection connection) 
      throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException, Exception
    {
        
        // create system message from the passed in message and the exception 
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        if(exception != null){
           exception.printStackTrace(pw);
        }
        String logMessage = "";
        if(message != null && !message.equals("")){
            logMessage = message + "\n" + sw.toString();
        }
        else
        {
            logMessage = sw.toString();    
        }
        pw.close();  
        
        // create the System Messenger VO and send out the system message 
        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(level);
        systemMessengerVO.setSource(source);
        systemMessengerVO.setType(type);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        SystemMessenger.getInstance().send(systemMessengerVO, connection, false);

    }
    
    public boolean getRedemptionRateFeedNovatorFlag() throws Exception 
    {
        String flag = null;
        try {
            flag = ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE, "MP_RATE_FEED_TO_NOVATOR_FLAG");
            if(flag != null && flag.equals("N")) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }
    
    /**
     * This method formats a value to 2 decimal places.
     * @param value double
     * @return formatted decimal amount without decimal
     */
    public static String formatDoubleAmount(double value)
    {

     DecimalFormat df = new DecimalFormat("#.00");
     String formatted = df.format(value);

     return formatted;
    }


  /**
     * This method will convert a List to a delimited string.
     * @param aList - array list to be converted
     * @param delimitingCharacter - shows what the delimiter will be
     * @param removeTrailingDelimitingCharacter - tells if the trailing delimiting character has to be removed
     * @return formatted decimal amount without decimal
     */
  public static String convertListToString(List aList, String delimitingCharacter, boolean removeTrailingDelimitingCharacter)
    throws Exception
  {
    String returnString = "";

    Iterator iter = aList.iterator();
    // loop thru the result set and generate xml
    String value = null;
    int count = 0;
    while (iter.hasNext())
    {
      value = (String) iter.next();

      if (value != null && value.trim().length() > 0)
      {
        returnString += value + delimitingCharacter;
      }
      count++;
    }

    if (returnString != null && returnString.trim().length() > 0 && removeTrailingDelimitingCharacter)
      returnString = returnString.substring(0, returnString.lastIndexOf(delimitingCharacter));

    return returnString;


  }
  

	/**
   * This method sends a message to the System Messenger.
   * @param message
   * @return message id
   */
	public void sendSystemMessage(String message, String subject, Connection connection) throws Exception {
	    String messageID = "";

	    String messageSource = "Fees_Feed";

	    //build system vo
	    SystemMessengerVO sysMessage = new SystemMessengerVO();
	    sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
	    sysMessage.setSource(messageSource);
	    sysMessage.setType("ERROR");
	    sysMessage.setMessage(message);
	    sysMessage.setSubject(subject);
	    SystemMessenger sysMessenger = SystemMessenger.getInstance();
	    // Passing the flag to not close connection.
	    messageID = sysMessenger.send(sysMessage, connection, false);

	    if (messageID == null) {
	        String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
	        throw new Exception(msg);
	    }

	}
	
	


}
