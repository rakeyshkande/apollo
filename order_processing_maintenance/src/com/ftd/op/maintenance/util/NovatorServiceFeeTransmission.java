package com.ftd.op.maintenance.util;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.vo.NovatorResponseVO;
import com.ftd.op.maintenance.vo.ServiceFeeVO;
import com.ftd.op.maintenance.vo.ServiceFeeOverrideVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;


public class NovatorServiceFeeTransmission 
{

  private static final Logger logger = new Logger("com.ftd.op.maintenance.util.NovatorServiceFeeTransmission");



  public NovatorServiceFeeTransmission()
  {
  }

  /**
   * Send the passed in XML message to Novator.
   * The passed in header will be used as the XML header.
   * @param sfVO - ValueObject containing data to send
   * @param action - Add / Delete
   *
   */
    public String sendServiceFee(ServiceFeeVO sfVO, String action) 
        throws Exception
    {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        int timeout = Integer.parseInt(configUtil.getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.NOVATOR_TIMEOUT_KEY));

        String transmitData = createServiceFeeXML(sfVO, action);
        logger.debug("transmitData: " + transmitData);

        HashMap serverData = getNovatorServer();
        String server = serverData.get("server").toString();
        String port = serverData.get("port").toString();
        
        String response = "";
        if (server != null && !server.equals("") &&
            port != null && !port.equals("")) {

            response = SendUpdateFeed.send(server, Integer.parseInt(port), timeout, transmitData);
            logger.debug("sendServiceFee response: " + response);
            NovatorResponseVO responseVO = parseResponse(response);

            if (responseVO.isParseable()){
                response = "CON";
                if(!responseVO.isSuccess()){
                    response = "DEN";
                }
            } 
        }
        
        return response;

    }  

    /**
     * Send the passed in XML message to Novator.
     * The passed in header will be used as the XML header.
     * @param sfVO - ValueObject containing data to send
     * @param action - Add / Delete
     *
     */
    public String sendServiceFeeOverride(ServiceFeeOverrideVO sfVO, String action) 
        throws Exception
    {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        int timeout = Integer.parseInt(configUtil.getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.NOVATOR_TIMEOUT_KEY));

        String transmitData = createServiceFeeOverrideXML(sfVO, action);
        logger.debug("transmitData: " + transmitData);

        HashMap serverData = getNovatorServer();
        String server = serverData.get("server").toString();
        String port = serverData.get("port").toString();
        
        String response = "";
        if (server != null && !server.equals("") &&
            port != null && !port.equals("")) {

            response = SendUpdateFeed.send(server, Integer.parseInt(port), timeout, transmitData);
            logger.debug("sendServiceFeeOverride response: " + response);
            NovatorResponseVO responseVO = parseResponse(response);

            if (responseVO.isParseable()){
                response = "CON";
                if(!responseVO.isSuccess()){
                    response = "DEN";
                }
            } 
        }
        
        return response;
    }  

    /**
     * Creates the input data that will be sent to Novator
     *
     * @return java.lang.String - XML in a string format that will be sent to Novator
     * @param sfVO -  ServiceFeeVO
     * @param action - Add / Delete
     */
    private String createServiceFeeXML(ServiceFeeVO sfVO, String action) throws Exception
    {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("0.00");

        Document doc = DOMUtil.getDefaultDocument();
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "serviceFee", "");
        doc.appendChild(root);
        
        Element newElement = JAXPUtil.buildSimpleXmlNode(doc,"action", action);
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"id", sfVO.getServiceFeeId());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"domesticFee", df.format(sfVO.getDomesticFee()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"floristSameDayUpcharge", df.format(sfVO.getSameDayUpcharge()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"floristSameDayUpchargeFS", df.format(sfVO.getSameDayUpchargeFS()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"internationalFee", df.format(sfVO.getInternationalFee()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"description", sfVO.getDescription());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorCharge", df.format(sfVO.getVendorCharge()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorSatUpcharge", df.format(sfVO.getVendorSatUpcharge()));
        root.appendChild(newElement); 
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorSunUpcharge", df.format(sfVO.getVendorSunUpcharge()));
        root.appendChild(newElement); 
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorMonUpcharge", df.format(sfVO.getVendorMonUpcharge()));
        root.appendChild(newElement); 
        return JAXPUtil.toString(root);
    }

    /**
     * Creates the input data that will be sent to Novator
     *
     * @return java.lang.String - XML in a string format that will be sent to Novator
     * @param sfVO -  ServiceFeeOverrideVO
     * @param action - Add / Delete
     */
    private String createServiceFeeOverrideXML(ServiceFeeOverrideVO sfVO, String action) throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("0.00");

        Document doc = DOMUtil.getDefaultDocument();
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "serviceFeeOverride", "");
        doc.appendChild(root);
        
        Element newElement = JAXPUtil.buildSimpleXmlNode(doc,"action", action);
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"id", sfVO.getServiceFeeId());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"overrideDate", sdf.format(sfVO.getOverrideDate()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"domesticFee", df.format(sfVO.getDomesticFee()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"floristSameDayUpcharge", df.format(sfVO.getSameDayUpcharge()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"floristSameDayUpchargeFS", df.format(sfVO.getSameDayUpchargeFS()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"internationalFee", df.format(sfVO.getInternationalFee()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"description", sfVO.getDescription());
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorCharge", df.format(sfVO.getVendorCharge()));
        root.appendChild(newElement);
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorSatUpcharge", df.format(sfVO.getVendorSatUpcharge()));
        root.appendChild(newElement);  
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorSunUpcharge", df.format(sfVO.getVendorSunUpcharge()));
        root.appendChild(newElement);  
        newElement = JAXPUtil.buildSimpleXmlNode(doc,"vendorMonUpcharge", df.format(sfVO.getVendorMonUpcharge()));
        root.appendChild(newElement);  
     
        return JAXPUtil.toString(root);
    }
    
    /**
     * Determines which Novator server to send updates
     *
     * @return java.lang.String - XML in a string format that will be sent to Novator
     */
    private HashMap getNovatorServer() throws IOException, Exception {

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

        HashMap serverData = new HashMap();

        String server = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                       OPMConstants.NOVATOR_SERVER);
        String port = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, 
                                       OPMConstants.NOVATOR_PORT);

        serverData.put("server", server);
        serverData.put("port", port);
        
        logger.debug("Novator server IP: " + server + " Port: " + port);

        return serverData;
    }

    /**
    * Interrupts response from Novator
    *
    *
    * Creation date: (4/18/03) -ed
    * @return java.lang.String - NovatorResponseVO
    * @param novatorResponse - Novator response string
    */
    private static NovatorResponseVO parseResponse(String novatorResponse)
    {
        NovatorResponseVO responseVO = new NovatorResponseVO();

        // Some times the response comes back with CON on the end.  
        // Remove CON if present
        if(novatorResponse.endsWith("CON"))
        {
            novatorResponse = novatorResponse.substring(0,(novatorResponse.length() - 3));
        }

        //parse string in XML document
        
        try
        {
        	Document parser = DOMUtil.getDocument(new ByteArrayInputStream(novatorResponse.getBytes()));

            //attempt to get update tab
            NodeList nl = parser.getElementsByTagName("status");
            if (nl.getLength() <= 0 )           
            {
                responseVO.setSuccess(false);
                responseVO.setParseable(false);
                responseVO.setDescription("Unknown response");                                            
            }    
            else 
            {
                String status = getTextValue(nl.item(0));
                if (status.equalsIgnoreCase("success"))
                {            
                    responseVO.setSuccess(true);
                    responseVO.setParseable(true);
                    responseVO.setDescription("");
                }
                else
                {
                    responseVO.setSuccess(false);
                    responseVO.setParseable(true);
                    responseVO.setDescription(status);                    
                }
            }        
        }//end try
        catch (Throwable ioe)           
        {
            //Could not parse Novator response            
            responseVO.setSuccess(false);
            responseVO.setParseable(false);
            responseVO.setDescription(novatorResponse);                 
        }

        return responseVO;
    }

    /**
     * Extracts the Node.TEXT value of a node. If one does not exist,
     * it returns an empty string.
     *
     * @param node - The Node object containing the text.
     * @return String
     * @author York Davis
     **/
    private static String getTextValue(Node node)
    {
        Node textNode = node.getFirstChild();
        return textNode == null ? "" : textNode.getNodeValue().trim();
    }


}