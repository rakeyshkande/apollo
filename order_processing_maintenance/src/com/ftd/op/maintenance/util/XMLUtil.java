package com.ftd.op.maintenance.util;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.List;

import javax.servlet.ServletContext;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLUtil 
{
    //adds an element to an XML document
     public static void addElement(Document xml, String keyName, String value)
    {
        Element nameElement = (Element) xml.createElement(keyName);
        
        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        
        xml.getDocumentElement().appendChild(nameElement);    
    }    

    public static Document createErrorXML() throws Exception
    {
      Document errorXML = (Document)DOMUtil.getDocumentBuilder().newDocument();
      errorXML.appendChild((Element)errorXML.createElement("errors"));
      return errorXML;
    }

     public static void addError(Document xml, String keyName, String value)
    {
        Element errors = xml.getDocumentElement();
        Element errorElement = (Element) xml.createElement("error"); 
        Element nameElement = (Element) xml.createElement(keyName);
        
        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        
        errorElement.appendChild(nameElement);
        errors.appendChild(errorElement);
    }    


    public static void addErrors(Document xml, String[] keyNameList, String[] valueList)
    {
       Element errors = xml.getDocumentElement();
       Element errorElement = (Element) xml.createElement("error");
       for (int i = 0; i < keyNameList.length; i++){
            
           String keyName = (String) keyNameList[i];
           String value = (String) valueList[i];
           Element nameElement = (Element) xml.createElement(keyName);
           
           if(value != null && value.length() > 0 )
           {
               nameElement.appendChild(xml.createTextNode(value));
           }
           
           errorElement.appendChild(nameElement);
       }
       errors.appendChild(errorElement);
    }

     public static Element addElement(Document xml, Element parent, String keyName, String value)
    {
        Element nameElement = (Element) xml.createElement(keyName);
        if(parent != null && value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        
        parent.appendChild(nameElement);    
        return parent;
    }    
    
    //converts XML document to a string
    public static String convertDocToString(Document xml)
    throws Exception
    {
      StringWriter sw = new StringWriter();       
      DOMUtil.print(xml, new PrintWriter(sw));
      return sw.toString(); //string representation of xml document    
    }    
    
  //obtains and XSL file for the given forward name
  public static File getXSL(ActionMapping mapping, String forwardName, ServletContext servletContext)
  {
    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(forwardName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(servletContext.getRealPath(xslFilePathAndName));
    return xslFile;
  }            

}