package com.ftd.op.maintenance.util;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.exception.NovatorTransmissionException;
import com.ftd.op.maintenance.vo.BlockFlagVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.UserTransaction;

public class VendorUtil 
{

    public static BlockFlagVO getBlockFlags(Connection conn,Date enteredBlockDate,List blockList)
        throws Exception
    {
         BlockFlagVO blockFlagVO = new BlockFlagVO();
         blockFlagVO.setBlockedForDelivery(false);
         blockFlagVO.setBlockedForShipping(false);
         
         VendorDAO vendorDAO = new VendorDAO(conn);
    
         //check if this entered date exists as ship or delivery block
         if(blockList != null)
         {
             Iterator iter = blockList.iterator();
             while(iter.hasNext())
             {
                 VendorBlockVO blockVO = (VendorBlockVO)iter.next();
                 //is block date equal to or in date range
                 if((enteredBlockDate.equals(blockVO.getStartDate()) || enteredBlockDate.equals(blockVO.getEndDate()))
                    || (enteredBlockDate.after(blockVO.getStartDate()) && enteredBlockDate.before(blockVO.getEndDate())) )
                 {
                    if(blockVO.getBlockType().equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING))
                    {
                        blockFlagVO.setBlockedForShipping(true);
                    }
                    else if(blockVO.getBlockType().equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY))
                    {
                        blockFlagVO.setBlockedForDelivery(true);
                    }
                    else
                    {
                        throw new Exception("Unknown block type:" + blockVO.getBlockType());
                    }
                 }//end date is equal

             }//end while loop
         }//end if block list null
         
         return blockFlagVO;
    }
  /**
   * This takes a date MM/DD/YYYY and validates that it is in the correct format,
   * not before today, and not more than a year in the future.  
   * @throws java.lang.Exception
   * @return 
   * @param date
   */
  public static String validateDate(String date)throws Exception
  {
         String errorText = null;
         Date enteredBlockDate = null;
         SimpleDateFormat formatter =  new SimpleDateFormat("MM/dd/yyyy");
         try{
            enteredBlockDate = FieldUtils.formatStringToUtilDate(date);
         }
         catch(Exception e)
         {
             return "Invalid date format";
         }
         if (enteredBlockDate.before(new Date()) && !date.equalsIgnoreCase(formatter.format(new Date())))
         {
           return "Date cannot be before today.";
         }
         Calendar dateCal = Calendar.getInstance();
         dateCal.setTime(new Date());
         dateCal.add(Calendar.YEAR, 1);
         Date oneYearForward = dateCal.getTime();
         if (oneYearForward.before(enteredBlockDate))
         {
           return "Date must be within 365 days.";
         }
         return errorText;
  }

//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************
//************************************************************************************************



   /**
     * Updates the vendor cutoff time and send an update to the Novator as part of a transaction
     * @param vendorId - vendor id
     * @param cutoff - vendor cuttoff time
     * @param conn - Database Connection
     * @return n/a
     * @throws Exception
     */
    public void updateVendorCutoffSendNovator(String vendorId, String cutoff, Connection conn) throws NovatorTransmissionException, Exception 
    {

      //used for container managed transaction
      UserTransaction ut = null;
      
      try 
      {
        //start transaction
        ut = Transaction.getTransaction();
        ut.begin();                 
        
        //Update vendor cutoff
        updateVendorCutoff(vendorId, cutoff, conn);

        //send the updated vendor info to Novator
        sendVendorInfoToNovator(vendorId, conn);
        
        //commit transaction
        ut.commit();
      }
      catch(NovatorTransmissionException nte) 
      {
        //if an exception is found, rollback the transaction
        Transaction.rollback(ut);

        throw nte;
      }  
      catch(Exception e) 
      {
        //if an exception is found, rollback the transaction
        Transaction.rollback(ut);
        throw e;
      }  

    }


   /**
     * Insert a vendor block date.
     * @param VendorBlockVO � Vendor block information
     */
    public void insertVendorBlockSendNovator(VendorBlockVO blockVO, Connection conn) throws Exception 
    {
      
      //used for container managed transaction
      UserTransaction ut = null;
      
      try 
      {
        //start transaction
        ut = Transaction.getTransaction();
        ut.begin();                 
        
        //Update vendor cutoff
        insertVendorBlock(blockVO, conn);

        //send the updated vendor info to Novator
        sendVendorInfoToNovator(blockVO.getVendorId(), conn);
        
        //commit transaction
        ut.commit();
      }
      catch(NovatorTransmissionException nte) 
      {
        //if an exception is found, rollback the transaction
        Transaction.rollback(ut);

        throw nte;
      }  
      catch(Exception e) 
      {
        Transaction.rollback(ut);
        throw e;
      }  
  
    } 


   /**
     * Insert vendor block dates.
     * @param List � List of vendor block information to insert
     */
    public void insertVendorBlocksSendNovator(List blockVoList, Connection conn) throws Exception 
    {
      String vendorId = null;
      String lastVendorId = null;
      
      //used for container managed transaction
      UserTransaction ut = null;
      
      try 
      {
        //start transaction
        ut = Transaction.getTransaction();
        ut.begin();                 

        for (int bvo=0; bvo < blockVoList.size(); bvo++) {        
            //Update vendor cutoff
            insertVendorBlock((VendorBlockVO) blockVoList.get(bvo), conn);
        }
    
        for (int bvo=0; bvo < blockVoList.size(); bvo++) {        
            vendorId = ((VendorBlockVO) blockVoList.get(bvo)).getVendorId();
            
            //send the updated vendor info to Novator (only once)
            if (! vendorId.equalsIgnoreCase(lastVendorId)) {
              sendVendorInfoToNovator(vendorId, conn);
            }
            lastVendorId = vendorId;
        }
        
        //commit transaction
        ut.commit();
      }
      catch(NovatorTransmissionException nte) 
      {
        //if an exception is found, rollback the transaction
        Transaction.rollback(ut);

        throw nte;
      }  
      catch(Exception e) 
      {
        Transaction.rollback(ut);
        throw e;
      }  
  
    } 


   /**
     * Delete a vendor block date.
     * @param VendorBlockVO � Vendor block information
     */
    public void deleteVendorBlockSendNovator(VendorBlockVO blockVO, Connection conn) throws Exception 
    {
      
      //used for container managed transaction
      UserTransaction ut = null;
      
      try 
      {
        //start transaction
        ut = Transaction.getTransaction();
        ut.begin();                 
        
        //delete the vendor block
        deleteVendorBlock(blockVO, conn);
        
        //send the updated vendor info to Novator
        sendVendorInfoToNovator(blockVO.getVendorId(), conn);
        
        //commit transaction
        ut.commit();
      }
      catch(NovatorTransmissionException nte) 
      {
        //if an exception is found, rollback the transaction
        Transaction.rollback(ut);

        throw nte;
      }  
      catch(Exception e) 
      {
        Transaction.rollback(ut);
        throw e;
      }
    }


   /**
     * Delete vendor block dates.
     * @param List � List of vendor block information to remove
     */
    public void deleteVendorBlocksSendNovator(List blockVoList, Connection conn) throws Exception 
    {
      String vendorId = null;
      String lastVendorId = null;
      
      //used for container managed transaction
      UserTransaction ut = null;
      
      try 
      {
        //start transaction
        ut = Transaction.getTransaction();
        ut.begin();                 
        
        for (int bvo=0; bvo < blockVoList.size(); bvo++) {        
            //delete the vendor block
            deleteVendorBlock((VendorBlockVO) blockVoList.get(bvo), conn);
        }
        
        for (int bvo=0; bvo < blockVoList.size(); bvo++) {        
            vendorId = ((VendorBlockVO) blockVoList.get(bvo)).getVendorId();
            
            //send the updated vendor info to Novator (only once)
            if (! vendorId.equalsIgnoreCase(lastVendorId)) {
              sendVendorInfoToNovator(vendorId, conn);
            }
            lastVendorId = vendorId;
        }
        
        //commit transaction
        ut.commit();
      }
      catch(NovatorTransmissionException nte) 
      {
        //if an exception is found, rollback the transaction
        Transaction.rollback(ut);

        throw nte;
      }  
      catch(Exception e) 
      {
        Transaction.rollback(ut);
        throw e;
      }
    }


   /**
     * Send vendor info to Novator
     * @param vendorId - vendor id
     * @param conn - Database Connection
     * @return n/a
     * @throws Exception
     */
    public void sendVendorInfoToNovator(String vendorId, Connection conn) throws NovatorTransmissionException, Exception 
    {
        //instantiate Vendor DAO
        VendorDAO vDAO = new VendorDAO(conn); 
        
        //Retrieve the vendor info from the vendor master table
        VendorMasterVO vendorVO = vDAO.getVendorMaster(vendorId);
        
        //Retrieve the vendor blocks 
        List vendorBlocks = vDAO.getVendorBlocksList(vendorId);

        //Obtain global block info for vendor
        List globalBlocks = vDAO.getGlobalVendorBlocksList(vendorId);
        for(Iterator it = globalBlocks.iterator(); it.hasNext();) 
        {
            vendorBlocks.add((VendorBlockVO)it.next());
        }
        
        //obtain Novator server and port
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String server = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, OPMConstants.NOVATOR_SERVER);
        String port = configUtil.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, OPMConstants.NOVATOR_PORT);
        
        //send update to Novator
        new NovatorVendorTransmission().send(vendorVO, vendorBlocks, server, port);
    }


   /**
     * Update vendor cutoff.
     * @param vendorId - vendor id
     * @param cutoff - vendor cuttoff time
     * @param conn - Database Connection
     * @return n/a
     * @throws Exception
     */
    public void updateVendorCutoff(String vendorId, String cutoff, Connection conn) throws Exception 
    {
        //instantiate Vendor DAO
        VendorDAO vDAO = new VendorDAO(conn); 
        
        //insert the vendor cutoff
        vDAO.insertVendorCutoff(vendorId, cutoff);

    }


   /**
     * Insert vendor block.
     * @param blockVO - vendor block vo
     * @param conn - Database Connection
     * @return n/a
     * @throws Exception
     */
    public void insertVendorBlock(VendorBlockVO blockVO, Connection conn) throws Exception 
    {
        //instantiate Vendor DAO
        VendorDAO vDAO = new VendorDAO(conn); 
        
        //insert the vendor block date
        vDAO.insertVendorBlock(blockVO);

    }


   /**
     * Delete vendor block.
     * @param blockVO - vendor block vo
     * @param conn - Database Connection
     * @return n/a
     * @throws Exception
     */
    public void deleteVendorBlock(VendorBlockVO blockVO, Connection conn) throws Exception 
    {
        //instantiate Vendor DAO
        VendorDAO vDAO = new VendorDAO(conn); 
        
        //delete the vendor block date
        vDAO.deleteVendorBlock(blockVO);

    }


   
}