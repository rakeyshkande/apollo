package com.ftd.op.maintenance.util;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.exception.NovatorTransmissionException;
import com.ftd.op.maintenance.vo.NovatorResponseVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.net.URLEncoder;

import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class NovatorVendorTransmission 
{

  private static HashMap novatorFields;  
  private static final Logger logger = new Logger("com.ftd.op.maintenance.util.NovatorVendorTransmission");



  public NovatorVendorTransmission()
  {
  }

  /**
   * Send the passed in XML message to Novator.
   * The passed in header will be used as the XML header.
   * @param header xml header
   * @param vo ValueObject containing data to send
   * @param saveNovatorFeeds map of what novator instances to update
   *
   */
    public void send(VendorMasterVO vmVO, List vendorBlockList, String server, String port) 
        throws NovatorTransmissionException
    {
        String errorString = "";

        try
        {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            int timeout = Integer.parseInt(configUtil.getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.NOVATOR_TIMEOUT_KEY));
        
            String transmitData = createVendorXML(vmVO, vendorBlockList);
            logger.debug("Sending XML:" + transmitData);
        
            NovatorResponseVO responseVO = parseResponse(SendUpdateFeed.send(server, Integer.parseInt(port), timeout, transmitData));
        
           //could response be parsed?
            if (responseVO.isParseable())
            {
              //did an error occur?
              if(!responseVO.isSuccess())
              {
                  errorString = responseVO.getDescription();
              }//end if success
            } 
            else
            {
              errorString = "<BR>" + server + ":" +"Could not interrupt Novator's response:" + responseVO.getDescription();
            }
            
            //if any error occured throw exception
            if (StringUtils.isNotEmpty(errorString))
            {
              throw new NovatorTransmissionException(errorString);        
            }       
        }
        catch(Throwable t) 
        {
            errorString = "Error occurred sending vendor feed to Novator.  Server:" + server + " Port:" + port;
            throw new NovatorTransmissionException(errorString, t);    
        }
    }  

    /**
     * Interrupts response from Novator
     *
     *
     * Creation date: (4/18/03) -ed
     * @return java.lang.String - NovatorResponseVO
     * @param novatorResponse Novator response string
     */
    private static NovatorResponseVO parseResponse(String novatorResponse)
    {
        NovatorResponseVO responseVO = new NovatorResponseVO();

        // Some times the response comes back with CON on the end.  
        // Remove CON if present
        if(novatorResponse.endsWith("CON"))
        {
            novatorResponse = novatorResponse.substring(0,(novatorResponse.length() - 3));
        }

        //parse string in XML document
        try
        {
            Document doc = JAXPUtil.parseDocument(novatorResponse);

            //attempt to get update tab
            Element updateElement = JAXPUtil.selectSingleNode(doc, "//update");

            if (updateElement == null )    	
            {
                logger.debug("No update element exists within the response.");
                //update was not found...look for Validation Error
                Element errorElement = JAXPUtil.selectSingleNode(doc, "//validationError");
                if (errorElement ==  null )  
                {
                    //Not Validation Error...dont know what this message means
                    responseVO.setSuccess(false);
                    responseVO.setParseable(false);
                    responseVO.setDescription(novatorResponse);                                            
                }
                else
                {
                    //We got a validation error
                    responseVO.setSuccess(false);
                    responseVO.setParseable(true);
                    responseVO.setDescription(JAXPUtil.getTextValue(errorElement));
                }           
            }    
            else 
            {
                //We got an update message
                //Get Status
                String status = "";
                Element statusElement = JAXPUtil.selectSingleNode(doc, "//update/status");
                if(statusElement != null)
                    status = JAXPUtil.getTextValue(statusElement);

                if (status.equals("success"))
                {            
                    responseVO.setSuccess(true);
                    responseVO.setParseable(true);
                    responseVO.setDescription("");
                }
                else
                {
                    responseVO.setSuccess(false);
                    responseVO.setParseable(true);
                    responseVO.setDescription(status);                    
                }
            }        
        }//end try        
        catch (Throwable ioe)    	
        {
            //Could not parse Novator response            
            responseVO.setSuccess(false);
            responseVO.setParseable(false);
            responseVO.setDescription(novatorResponse);                	
        }

        return responseVO;
    }

    /**
     * Creates the input data that will be sent to Novator
     *
     * @return java.lang.String - XML in a string format that will be sent to Novator
     * @param vmVO -  VendorMasterVO
     * @param vendorBlockList - list of VendorBlockVO
     */
    private String createVendorXML(VendorMasterVO vmVO, List vendorBlockList)
        throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Document doc = DOMUtil.getDefaultDocument();
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "vendor_feed", "");
        doc.appendChild(root);

        Element vendorElement = JAXPUtil.buildSimpleXmlNode(doc, "vendor", "");
        root.appendChild(vendorElement);
        
        //encode vendor name
        String vendorName = URLEncoder.encode(vmVO.getVendorName(), "UTF-8");

        JAXPUtil.addAttribute(vendorElement, "vendor_id", vmVO.getVendorID());
        JAXPUtil.addAttribute(vendorElement, "vendor_name", vendorName);
        JAXPUtil.addAttribute(vendorElement, "cutoff", vmVO.getCutoff());
        
        if (vendorBlockList != null)
        {
          VendorBlockVO vbVO = null;
          Element blockElement = null;
          for (Iterator it = vendorBlockList.iterator(); it.hasNext(); )
          {
              vbVO = (VendorBlockVO)it.next();
              
              blockElement = JAXPUtil.buildSimpleXmlNode(doc, "block", "");
              vendorElement.appendChild(blockElement);

              JAXPUtil.addAttribute(blockElement, "type", vbVO.getBlockType().toLowerCase());
              JAXPUtil.addAttribute(blockElement, "start_date", sdf.format(vbVO.getStartDate()));
              JAXPUtil.addAttribute(blockElement, "end_date", sdf.format(vbVO.getEndDate()));
          }
        }
        
        Element addOnParentElement = JAXPUtil.buildSimpleXmlNode(doc, "add_ons", "");
        vendorElement.appendChild(addOnParentElement);
        
        if (vmVO.getAddOnMap() != null)
        {
            for (String key : vmVO.getAddOnMap().keySet())
            {
                for (AddOnVO addOnVO : vmVO.getAddOnMap().get(key))
                {
                    Element addOnElement = JAXPUtil.buildSimpleXmlNode(doc, "add_on", "");
                    addOnParentElement.appendChild(addOnElement);
                    Element addOnIdElement = JAXPUtil.buildSimpleXmlNode(doc, "add_on_id", addOnVO.getAddOnId());
                    addOnElement.appendChild(addOnIdElement);             
                    Element addOnPquadAccsryIdIdElement = JAXPUtil.buildSimpleXmlNode(doc, "pquad_accessory_id", addOnVO.getsPquadAccsryId());
                    addOnElement.appendChild(addOnPquadAccsryIdIdElement);
                }
            }
        }

        return JAXPUtil.toStringNoFormat(doc);
    }
}