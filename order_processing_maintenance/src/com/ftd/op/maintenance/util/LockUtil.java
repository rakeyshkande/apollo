package com.ftd.op.maintenance.util;

import com.ftd.op.maintenance.dao.LockDAO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;


/**
 * This class handles the locking and unlocking of records
 */
public class LockUtil
{

  private static final String LOGGER_CATEGORY = "com.ftd.op.maintenance.util.LockUtil";

  public LockUtil()
  {
  }

  /**
     * This procedure will attempt to get a lock.  If the lock was obtained a
     * null will be returned.  If a lock was not obtained the userid of whoever
     * currently has the lock will be returned.
     */
  public static  String getLock(Connection conn, String entityType, String entityId, String sessionId, String csrId)
    throws Exception
  {
    Logger logger = new Logger(LOGGER_CATEGORY);
    logger.debug("Requesting lock. EntityType=" + entityType + " EntityId=" + entityId + " csrId=" + csrId + " Token=" + 
                 sessionId);

    LockDAO lDAO = new LockDAO(conn);
    HashMap lockResults = (HashMap) lDAO.retrieveLockCRS(sessionId, csrId, entityId, entityType);

    String lockObtained = (String) lockResults.get("lockObtained");
    String lockCsrId = (String) lockResults.get("lockCsrId");

    if (lockObtained != null && lockObtained.equalsIgnoreCase("N"))
    {
      logger.debug("Item already locked by " + lockCsrId);
    }
    else
    {
      logger.debug("Lock obtained.");
      lockCsrId = null; 
    }

    return lockCsrId;

  }


  public static void releaseLock(Connection conn, String entityType, String entityId, String sessionId, String csrId)
    throws Exception
  {

    Logger logger = new Logger(LOGGER_CATEGORY);
    logger.debug("Releasing lock. EntityType=" + entityType + " EntityId=" + entityId + " csrId=" + csrId + " Token=" + 
                 sessionId);

    LockDAO lDAO = new LockDAO(conn);
    lDAO.releaseLock(sessionId, csrId, entityId, entityType);

  }


}
