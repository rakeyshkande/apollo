package com.ftd.op.maintenance.util;

import java.io.ByteArrayInputStream;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.vo.NovatorResponseVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;


public abstract class NovatorTransmission 
{
    private static final Logger logger = new Logger("com.ftd.op.maintenance.util.NovatorTransmission");
    private ConfigurationUtil configurationUtil;
    public static String CREATE_FEED = "create";
    public static String UPDATE_FEED = "update";
    public static String REMOVE_FEED = "remove";
    public static String CONTENT_ENV = "CONTENT";
    public static String TEST_ENV = "TEST";
    public static String UAT_ENV = "UAT";
    public static String LIVE_ENV = "LIVE";
    
    // Web site server configuration fields
    private String configContext;
    private String ipParamName;
    private String portParamName;    

    /** Returns the web site configuration context
     * @return
     */
    public String getConfigContext() {
		return configContext;
	}

	/** Sets the web site configuration context
	 * @param configContext
	 */
	public void setConfigContext(String configContext) {
		this.configContext = configContext;
	}

	/** Returns the property name to get web site server IP
	 * @return
	 */
	public String getIpParamName() {
		return ipParamName;
	}

	/** Sets the property name to retrieve the web site server IP
	 * @param ipParamName
	 */
	public void setIpParamName(String ipParamName) {
		this.ipParamName = ipParamName;
	}

	/** Returns the property name to retrieve the web site port number
	 * @return
	 */
	public String getPortParamName() {
		return portParamName;
	}

	/** Sets the property name to fetch the web site server port number
	 * @param portParamName
	 */
	public void setPortParamName(String portParamName) {
		this.portParamName = portParamName;
	}
	
	/**
	 * Default Constructor
	 */
	public NovatorTransmission() {
		
	}

	/**
	 * @param configContext
	 * @param ipParamName
	 * @param portParamName
	 */
	public NovatorTransmission(String configContext, String ipParamName,
			String portParamName) {		
		this.configContext = configContext;
		this.ipParamName = ipParamName;
		this.portParamName = portParamName;
	}

	public ConfigurationUtil getConfigurationUtil() throws Exception{
        if (configurationUtil == null)
        {
            configurationUtil = ConfigurationUtil.getInstance();
        }
        return configurationUtil;
    }
    
    /**
     * Send information to Novator
     * @param xmlString Information
     * @param ipParamName IP configuration parameter
     * @param portParamName Port configuratin parameter
     * @return Novator response
     * @throws Exception
     */
    public String sendNovatorXML(String xmlString, String ipParamName, String portParamName) throws Exception
    {
        ConfigurationUtil cu = getConfigurationUtil();
        String server = cu.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,ipParamName);
        String port = cu.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,portParamName);
        Integer portInt = Integer.parseInt(port);
        String timeout = cu.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_FEED_TIMEOUT);
        Integer timeoutInt = Integer.parseInt(timeout);
        String response = SendUpdateFeed.send(server, portInt, timeoutInt, xmlString);
        
        return response;
    }
    
	/**Send XML to Novator for a given context, server IP and port configuration.
	 * @param xmlString
	 * @return
	 * @throws Exception
	 */
	public String sendNovatorXML(String xmlString) throws Exception {
		ConfigurationUtil cu = getConfigurationUtil();
		String server = cu.getFrpGlobalParm(this.configContext, this.ipParamName);
		String port = cu.getFrpGlobalParm(this.configContext, this.portParamName);
		Integer portInt = Integer.parseInt(port);
		int timeout = Integer.parseInt(cu.getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.NOVATOR_TIMEOUT_KEY));
		
		if(logger.isDebugEnabled()) {
			logger.debug("Sending Feed to IP: " + server + ", Port: " + port);
		}
		
		String response = SendUpdateFeed.send(server, portInt, timeout, xmlString);
		return response;
	}

    /**
     * Evaluate Novator response
     * @param novatorResponse
     * @return
     */
    public void parseResponse(String novatorResponse) throws Exception
    {
        if(logger.isDebugEnabled())
            logger.debug("Novator response: " + novatorResponse);
        
        
        // Sometimes the response comes back with CON on the end.  
        // Remove CON if present
        if(novatorResponse.endsWith("CON"))
        {
            novatorResponse = novatorResponse.substring(0,(novatorResponse.length() - 3));
        }

        try
        {
            Document doc = JAXPUtil.parseDocument(novatorResponse);

            // Attempt to obtain update node
            Element updateElement = JAXPUtil.selectSingleNode(doc, "//update");
            if(updateElement == null)
                updateElement = JAXPUtil.selectSingleNode(doc, "//create");
            if(updateElement == null)
                updateElement = JAXPUtil.selectSingleNode(doc, "//remove");
            

            if (updateElement == null )         
            {
                if(logger.isDebugEnabled())
                    logger.debug("No update element exists within the response.");

                // update was not found...look for a validation error
                Element errorElement = JAXPUtil.selectSingleNode(doc, "//validationError");
                if (errorElement ==  null )  
                {
                    // No validation error found...don't know what this message means
                    throw new Exception("Unable to parse the following Novator response: " + novatorResponse);
                }
                else
                {
                    // Obtain the validation error description
                    String description = "";
                    Element errorDescriptionElement = JAXPUtil.selectSingleNode(doc, "//validationError/error/description");
                    if(errorDescriptionElement != null)
                    {
                        description = JAXPUtil.getTextValue(errorDescriptionElement);
                    }
                    throw new Exception("Novator validation error: " + description);
                }           
            }    
            else 
            {
                // Obtaint the status of the update
                String status = "";
                Element statusElement = JAXPUtil.selectSingleNode(doc, "//update/status");
                if(statusElement == null)
                    statusElement = JAXPUtil.selectSingleNode(doc, "//create/status");
                if(statusElement == null)
                    statusElement = JAXPUtil.selectSingleNode(doc, "//remove/status");
                
                if(statusElement != null)
                    status = JAXPUtil.getTextValue(statusElement);

                if (status.equals("success"))
                {            
                    // Do nothing
                }
                else
                {
                    throw new Exception("Novator feed was unsuccessful: " + status);
                }
            }        
        }//end try        
        catch (Throwable t)           
        {
            // Could not parse Novator response or an error occurred           
            logger.warn(t);
            throw new Exception("Novator feed error.  " + t.getMessage());
        }
    }
    
    /**
     * Procedure to retrieve novator update setup in a document - value dependent on setting in global parm.
     * 
     * <novator_update_setup>
     *   <content_feed_allowed>NO</content_feed_allowed>
     *   <test_feed_allowed>YES</test_feed_allowed>
     *   <production_feed_allowed>NO</production_feed_allowed>
     *   <uat_feed_allowed>NO</uat_feed_allowed>
     *   <content_feed_checked>YES</content_feed_checked>
     *   <test_feed_checked>YES</test_feed_checked>
     *   <production_feed_checked>NO</production_feed_checked>
     *   <uat_feed_checked>YES</uat_feed_checked>
     *  </novator_update_setup>
     *
     * @return
     * @throws Exception
     */
      public Document getNovatorUpdateSetup() throws Exception
      {
          Document doc = null;
          
          try 
          {
              doc = JAXPUtil.createDocument();
              Element root = doc.createElement(OPMConstants.NOVATOR_UPDATE_SETUP);
              doc.appendChild(root);

              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_CONTENT_FEED_ALLOWED );
              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_TEST_FEED_ALLOWED );
              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_PRODUCTION_FEED_ALLOWED );
              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_UAT_FEED_ALLOWED );

              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_CONTENT_FEED_CHECKED );
              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_TEST_FEED_CHECKED );
              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_PRODUCTION_FEED_CHECKED );
              addNovatorUpdateAllowedNode(doc, root,OPMConstants.NOVATOR_UAT_FEED_CHECKED );
          } 
          catch (Throwable t) 
          {
              logger.error("Error in update()",t);
              throw new Exception("Could not retrieve data.");
          }
          
          return doc;
      }
      
      protected void addNovatorUpdateAllowedNode(Document doc, Element parent, String name) throws Exception
      {
          String updateAllowed = getConfigurationUtil().getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,name);
          addNode(doc, parent, name, updateAllowed);            
      }
      
      protected Element addNode(Document doc, Element parent, String nodeName, String nodeValue)
      {    
          Element newElement = JAXPUtil.buildSimpleXmlNode(doc,nodeName, nodeValue==null? "" : nodeValue);
          parent.appendChild(newElement);
          return newElement;
      }  
      
	/** Convenient method to parse the Web site response and return a string
	 * indicating if web site response states confirmed or denied
	 * @param novatorResponse
	 * @return
	 */
	protected static NovatorResponseVO parseNovatorResponse (String novatorResponse) {
		
		NovatorResponseVO responseVO = new NovatorResponseVO();
		
		// Some times the response comes back with CON on the end. Remove CON if present
		if (novatorResponse.endsWith("CON")) {
			novatorResponse = novatorResponse.substring(0, (novatorResponse.length() - 3));
		}

		// parse string in XML document 
		if(StringUtils.isEmpty(novatorResponse)){
			if(logger.isDebugEnabled()) {
				logger.debug("Novator Response is empty, unable to parse");
			}
			responseVO.setParseable(false);
			return responseVO;
		}
		
		try {
			Document parser = DOMUtil.getDocument(new ByteArrayInputStream(novatorResponse.getBytes()));

			// attempt to get update tab
			NodeList nl = parser.getElementsByTagName("status");
			if (nl.getLength() <= 0) {
				responseVO.setSuccess(false);
				responseVO.setParseable(false);
				responseVO.setDescription("Unknown response");
			} else {
				String status = JAXPUtil.getTextValue(nl.item(0));
				if (status.equalsIgnoreCase("success")) {
					responseVO.setSuccess(true);
					responseVO.setParseable(true);
					responseVO.setDescription("");
				} else {
					responseVO.setSuccess(false);
					responseVO.setParseable(true);
					responseVO.setDescription(status);
				}
			}
		} catch (Throwable ioe) {
			// Could not parse Novator response
			// An IOE is thrown when parsing empty document. Premature EOD error caught.
			responseVO.setSuccess(false);
			responseVO.setParseable(false);
			responseVO.setDescription(novatorResponse);
		}
		return responseVO;
	}
}