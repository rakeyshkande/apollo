package com.ftd.op.maintenance.util;

import com.ftd.op.venus.bo.VenusAPIBO;
import com.ftd.osp.utilities.plugins.Logger;

/**
* This locator class provides access to external or dependent services.
* The class is a singleton.
*/
public class MessagingServiceLocator
{

  private static MessagingServiceLocator messagingServiceLocator;
  private Logger logger = new Logger("com.ftd.messaging.util.MessagingServiceLocator");
  
  private VenusAPIBO venusAPIBO;   

  /**
  * Private constructor to enforce singleton. 
  * 
  * @see #getInstance()
  */
  private MessagingServiceLocator() throws Exception
  {
     venusAPIBO = new VenusAPIBO();
  }

  /**
  * Initializes and returns a reference to the Locator singleton instance.
  */
  public static MessagingServiceLocator getInstance() throws Exception
  {
    if(messagingServiceLocator==null)
    {
      messagingServiceLocator=new MessagingServiceLocator();
    }
    return messagingServiceLocator;
  }
  
  /**
  * @return Reference to the Venus API
  */
   public VenusAPIBO  getVenusAPI() throws Exception
  {
      return venusAPIBO;
  }

}