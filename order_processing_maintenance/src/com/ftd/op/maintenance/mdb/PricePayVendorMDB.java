package com.ftd.op.maintenance.mdb;

import com.ftd.op.maintenance.bo.PricePayVendorBO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * This a message driven bean.
 *
 * When the container is start the setMessageDrivenContext is executed.
 *
 */
public class PricePayVendorMDB implements MessageDrivenBean, MessageListener
{
  private MessageDrivenContext context;
  private static String LOGGER_CATEGORY = "com.ftd.op.maintenance.mdb.PricePayVendorMDB";

  /**
   * ejbCreate
   */
  public void ejbCreate()
  {
  }

  /**
   * onMesage...a JMS message was received
   * @param msg
   */
  public void onMessage(Message msg)
  {

      Logger logger = new Logger(LOGGER_CATEGORY);
      String batchId = null;

      try {

          TextMessage textMessage = (TextMessage)msg;
          batchId = textMessage.getText();
          logger.debug("New message: " + batchId);

          PricePayVendorBO ppvBO = new PricePayVendorBO();
          ppvBO.updateBatch(batchId);
          logger.debug("Back from updateBatch()");
      } catch (Exception e) {
          logger.error(e);
          try {
        	  CommonUtil.sendPage("Failed to update price pay vendor batch:" + batchId, e, "PricePayVendorMDB", SystemMessengerVO.LEVEL_PRODUCTION, "System Exception", "System Message",CommonUtil.getDBConnection());
          }
          catch (Exception se) {
        		logger.error(se);
          }
      } catch (Throwable t) {
    	  logger.error(t);
      }

  }

  public void ejbRemove()
  {
  }

  /**
   * This message is invoked when the container is started
   * @param ctx
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {

      this.context = ctx;
      Logger logger = new Logger(LOGGER_CATEGORY);
      logger.info("MDB started");

  }

}