package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.ZJTripBO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public class ZJTripAction
  extends Action
{
  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
 
  private static Logger logger = new Logger("com.ftd.op.maintenance.action.ZJTripAction");


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public ZJTripAction()
  {
  }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
  {

	 Connection con = null;

    ActionForward forward = null;

    try
    {
      String forwardToType = "";
      String forwardToName = "";

      //Connection/Database info
      con = CommonUtil.getDBConnection();

      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = DOMUtil.getDocument();

      //This Hashmap will be used to hold all the XML objects, page data, and search criteria that 
      //will be returned from the business object
      HashMap responseHash = new HashMap();

      //Call the Business Object, which will return a hashmap containing
      //    0 to Many - XML documents 
      //    HashMap1  = pageData - hashmap that contains String data at page level
      ZJTripBO tripBO = new ZJTripBO(request, con, response);
      responseHash = tripBO.processRequest();

      //After the Business Object returns control back, we need to populate the XML document that
      //will be passed to the Transform Utility.  This entails:
      //  1)  append all the XML documents from the responseHash to this returnDocument
      //  2)  convert pageData in responseHash (from HashMap to XMLDocument), and append
      //      to returnDocument

      //Get all the keys in the hashmap returned from the business object
      Set ks = responseHash.keySet();
      Iterator iter = ks.iterator();
      String key;

      //Iterate thru the hashmap returned from the business object using the keyset
      while (iter.hasNext())
      {
        key = iter.next().toString();
        if (key.equalsIgnoreCase("pageData"))
        {
          //The page data hashmap within the main hashmap contains objects whose key are forwardToType
          //and forwardToName.  The key refers to if the control will be transferred to an action or an
          //XSL, and which one.
          //However, these keys should not be included in the page data XML
          if (((HashMap) responseHash.get("pageData")).get("forwardToType") != null)
          {
            forwardToType = (String) ((HashMap) responseHash.get("pageData")).get("forwardToType").toString();
            forwardToName = (String) ((HashMap) responseHash.get("pageData")).get("forwardToName").toString();
            ((HashMap) responseHash.get("pageData")).remove("forwardToType");
            ((HashMap) responseHash.get("pageData")).remove("forwardToName");
          }

          HashMap pageData = (HashMap) responseHash.get("pageData");

          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
        }
        else
        {
          //Append all the existing XMLs to the final XML
          Document xmlDoc = (Document) responseHash.get(key);
          NodeList nl = xmlDoc.getChildNodes();
          DOMUtil.addSection(responseDocument, nl);
        }
      }

//responseDocument.print(System.out);


      //check if we have to forward to another action, or just transform the XSL.
      if (forwardToType.equalsIgnoreCase("ACTION"))
      {
        forward = mapping.findForward(forwardToName);
        logger.info("forward: " + forward);
        return forward;
      }
      else if (forwardToType.equalsIgnoreCase("XSL"))
      {
        //Get XSL File name
        File xslFile = getXSL(forwardToName, mapping);
        if (xslFile != null && xslFile.exists())
        {
          //Transform the XSL File
          try
          {
            forward = mapping.findForward(forwardToName);
            String xslFilePathAndName = forward.getPath(); //get real file name

            // Retrieve the parameter map set by data filter and update
            //cbr_action with the next action.
            HashMap params = getParameters(request);

            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, xslFile, xslFilePathAndName, 
                                                           params);
          }
          catch (Exception e)
          {
            logger.error("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " transformation failed" + 
                         forwardToName, e);
            if (!response.isCommitted())
            {
              throw e;
            }
          }
        }
        else
        {
          throw new Exception("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " does not exist." + 
                              forwardToName);
        }

        return null;
      }
      else
      {
        return null;
      }
    }

    catch (Exception e)
    {
      logger.error(e);
      forward = mapping.findForward("ErrorPage");
      return forward;
    }

    finally
    {
      try
      {
        //Close the connection
        if (con != null)
        {
          con.close();
        }
      }
      catch (Exception e)
      {
        logger.error(e);
        forward = mapping.findForward("ErrorPage");
        return forward;
      }
    }
  }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */
  private

  File getXSL(String forwardToName, ActionMapping mapping)
  {
    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(forwardToName);
    xslFilePathAndName = forward.getPath(); //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


  /**
     * retrieve the attribute “parameters” HashMap from request
     * Return the HashMap.
     * @param request - HttpServletRequest
     * @return HashMap
     * @throws n/a
     */
  private HashMap getParameters(HttpServletRequest request)
  {
    HashMap parameters = new HashMap();

    try
    {
      String action = request.getParameter("action_type");
      parameters = (HashMap) request.getAttribute("parameters");
      parameters.put("action", action);

    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getParameters");
      }
    }

    return parameters;
  }


}
