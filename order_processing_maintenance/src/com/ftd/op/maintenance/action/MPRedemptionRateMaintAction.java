package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.MPRedemptionRateBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.NovatorMPRedemptionRateTransmission;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * MPRedemptionRateMaintAction class
 *
 * The “Miles Points Redemption Rate Maintenance” screen allows a user to view/add/edit the redemption rate.
 *
 */
public final class MPRedemptionRateMaintAction extends Action 
{
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.MPRedemptionRateMaintAction");
  
  //request xsl filename
  private static final String MP_REDEMPTION_RATE_LIST_XSL = "MPRedemptionRateListXSL";
  private static final String SOURCE_CODES_XSL = "SourceCodesXSL";
  private static final String MP_REDEMPTION_RATE_HISTORY_XSL = "MPRedemptionRateHistoryXSL";
  
  //application:
  public static final String APP_PARAMETERS = "parameters";
  
  //application:  actions
  public static final String ACTN_ERROR = "Error";
  
  //request parameters
  public static final String ACTION_TYPE = "action_type";
  public static final String ADMIN_ACTION = "admin_action";
  
  //action types
  public static final String  LOAD_REDEMPTION_RATE_LIST_ACTION = "loadRedemptionRateList";
  public static final String  LOAD_REDEMPTION_RATE_ACTION = "loadRedemptionRate";
  public static final String  DELETE_REDEMPTION_RATE_ACTION = "deleteRedemptionRate";
  public static final String  ADD_REDEMPTION_RATE_ACTION = "addRedemptionRate";
  public static final String  SAVE_REDEMPTION_RATE_ACTION = "saveRedemptionRate";
  public static final String  GET_SOURCE_CODE_LIST_ACTION = "getSourceCodeList";
  public static final String  GET_HISTORY_ACTION = "getHistory";
  private static final String MAIN_MENU_ACTION = "MainMenuAction";
  
  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";
  public static final String VALID_FLAG = "valid_flag";
  public static final String ERROR_VALUE = "error_value";
  public static final String UPDATE_ALLOWED = "update_allowed";
  public static final String LOCK_VALUE = "lock_value";
  public static final String LOCK_FLAG = "lock_flag";
  
     
  /**
  This is the Redemption Rate Maintenance Action called from the Struts framework.
  * 
  * @param mapping            - ActionMapping used to select this instance
  * @param form               - ActionForm (optional) bean for this request
  * @param request            - HTTP Request we are processing
  * @param response           - HTTP Response we are processing
  * @return forwarding action - next action to "process" request
  * @throws IOException
  * @throws ServletException
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException
  {
    String actionType = null;
  
    /* get action type request parameter */
    if(request.getParameter(ACTION_TYPE)!=null) {
      actionType = request.getParameter(ACTION_TYPE);
    }
    else {
      actionType = this.LOAD_REDEMPTION_RATE_ACTION;
    }
    logger.debug("action: " + actionType);
    
    // Load redemptin rate list
    if(actionType.equals(LOAD_REDEMPTION_RATE_LIST_ACTION)) {             
          return performLoadRedemptionRateList(mapping, form, request, response);
    }  
    // Delete redemption rate
    else if(actionType.equals(ADD_REDEMPTION_RATE_ACTION)) {
        return performAddRedemptionRate(mapping, form, request, response);
    }     
    // Delete redemption rate
    else if(actionType.equals(DELETE_REDEMPTION_RATE_ACTION)) {
      return performDeleteRedemptionRate(mapping, form, request, response);
    } 
    // save redemption rate data
    else if(actionType.equals(SAVE_REDEMPTION_RATE_ACTION)) { 
      return performSaveRedemptionRate(mapping, form, request, response);        
    }
    // get source codes by redemption rate
    else if(actionType.equals(GET_SOURCE_CODE_LIST_ACTION)) {
      return performLoadSourceCodes(mapping, form, request, response);
    }
    // get history
     else if(actionType.equals(GET_HISTORY_ACTION)) {
       return performLoadHistory(mapping, form, request, response);
     }
    // return to main menu
    else if(actionType.equals(MAIN_MENU_ACTION)) {
      return performMainMenu(mapping);
    }
    
    return null;
  } 
  
    private ActionForward performLoadRedemptionRateList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, String actionResponse) 
      {
        Document xmlDoc  = null;
        File xslFile = null;
        MPRedemptionRateBO bo = new MPRedemptionRateBO();
        Document paymentMethodDoc = null;
        
        try
        {
            xslFile = getXSL(MP_REDEMPTION_RATE_LIST_XSL,mapping);
              
            xmlDoc = (Document) DOMUtil.getDocument();
              
            /* get security token */
            String sessionId = request.getParameter(OPMConstants.SEC_TOKEN);
            String context = request.getParameter(OPMConstants.CONTEXT);

            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
              
            /* retrieve user information */
            String userId = null;
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }       
            
            Document doc = bo.getRedemptionRateListXML();
            DOMUtil.addSection(xmlDoc,doc.getChildNodes()); 
            
            paymentMethodDoc = bo.getMPPaymentMethodIds();
            if(paymentMethodDoc != null) {
                  DOMUtil.addSection(xmlDoc,paymentMethodDoc.getChildNodes()); 
            }
            
            NovatorMPRedemptionRateTransmission t = new NovatorMPRedemptionRateTransmission();
            Document updateSetupDoc = t.getNovatorUpdateSetup();
            DOMUtil.addSection(xmlDoc,updateSetupDoc.getChildNodes());
              
            /* build page data */
            HashMap pageData = new HashMap();
            
            /* check if user has access */
            boolean updateAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"RdmptnRateData","Update");

            if(updateAllowed) {
                pageData.put(UPDATE_ALLOWED, "Y"); 
            } else {
                pageData.put(UPDATE_ALLOWED, "N");
            }
            pageData.put(ADMIN_ACTION, actionResponse);
            
            /* convert the page data hashmap to XML and append it to the final XML */
            DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
            
            StringWriter sw = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(sw));
            logger.debug(sw.toString());
            
            HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
            TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
            
            
        } catch (Exception e) 
        {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
        } 
        return null;
    }  

  /**
     * Load redemption rate list.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
  private ActionForward performLoadRedemptionRateList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
        return performLoadRedemptionRateList(mapping, form, request, response, "");
  }
  
  /**
     * Add a redemption rate.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
  private ActionForward performAddRedemptionRate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
          MPRedemptionRateBO bo = new MPRedemptionRateBO();
          String actionResponse = "";
          
          try {
              actionResponse = bo.addRedemptionRate(request);
          } catch (Exception e)  {
                logger.error(e);
                actionResponse = OPMConstants.RETURN_MESSAGE_SAVE_FAILED;
          }             
          try {
                performLoadRedemptionRateList(mapping, form, request, response, actionResponse);
          } catch (Exception e) {
              return mapping.findForward(ACTN_ERROR);
          }
        
          return null;
  }

  /**
     * Save changes to a redemption rate.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
  private ActionForward performSaveRedemptionRate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
      MPRedemptionRateBO bo = new MPRedemptionRateBO();
      String actionResponse = "";
      
      try {
          actionResponse = bo.saveRedemptionRate(request);
      } catch (Exception e)  {
            logger.error(e);
            actionResponse = OPMConstants.RETURN_MESSAGE_SAVE_FAILED;
      }             
      try {
            performLoadRedemptionRateList(mapping, form, request, response, actionResponse);
      } catch (Exception e) {
          return mapping.findForward(ACTN_ERROR);
      }

      return null;
  }

  /**
     * Delete a list of redemption rates.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
  private ActionForward  performDeleteRedemptionRate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
      MPRedemptionRateBO bo = new MPRedemptionRateBO();
      String actionResponse = "";
      
      try {
            actionResponse = bo.deleteRedemptionRate(request);
      } catch (Exception e)  {
            logger.error(e);
            actionResponse = OPMConstants.RETURN_MESSAGE_DELETE_FAILED;
      }             
      try {
            performLoadRedemptionRateList(mapping, form, request, response, actionResponse);
      } catch (Exception e) {
          return mapping.findForward(ACTN_ERROR);
      }

      return null;
  }
      
    /**
     * Load source codes associated with a redemption rate.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    private ActionForward performLoadSourceCodes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
    {
        Document xmlDoc  = null;
        File xslFile = null;
        MPRedemptionRateBO bo = new MPRedemptionRateBO();
        
        try
        {
              xmlDoc = (Document) DOMUtil.getDocument();
              xslFile = getXSL(SOURCE_CODES_XSL,mapping);
              Document doc = bo.getSourceCodeListXML(request);
              DOMUtil.addSection(xmlDoc,doc.getChildNodes());
              HashMap pageData = new HashMap();
              pageData.put("mpRedemptionRateId", request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_ID));
              DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
                          
              HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
              TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
        } catch (Exception e) 
        {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
        } 
        return null;
    }

    /**
     * Load change history related to a redemption rate.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    private ActionForward performLoadHistory(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
    {
        Document xmlDoc  = null;
        File xslFile = null;
        MPRedemptionRateBO bo = new MPRedemptionRateBO();
        
        try
        {
            xslFile = getXSL(MP_REDEMPTION_RATE_HISTORY_XSL,mapping);
            xmlDoc = (Document) DOMUtil.getDocument();
            Document doc = bo.getMPRedemptionHistoryXML(request);
            DOMUtil.addSection(xmlDoc,doc.getChildNodes()); 
              
            /* build page data */
            HashMap pageData = new HashMap();

            pageData.put("mpRedemptionRateId", request.getParameter(OPMConstants.REQUEST_MP_RD_RATE_ID));
            pageData.put("mpRedemptionRateDescription", request.getParameter(OPMConstants.REQUEST_MP_RD_DESCRIPTION));
            
            /* convert the page data hashmap to XML and append it to the final XML */
            DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 

            HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
            TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);              
              
        } catch (Exception e) 
        {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
        } 
        return null;
    }

    /**
     * Main menu.
     * @param mapping
     * @return
     */
  private ActionForward  performMainMenu(ActionMapping mapping)
  {
    try {
      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
      return newForward;
    } catch(Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    }
  }

  /**
  * get XSL file
  */
  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
    


}