package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.LockDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;



/**
 * LockAction class
 *
 */

public final class LockAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.op.maintenance.action.LockAction");

  /**
   * This is the main action called from the Struts framework.
   *
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Connection conn = null;
        HashMap pageData = new HashMap();

        try
        {
            // Set request variables
            String action = request.getParameter("action_type");
            String securityToken = (String) request.getParameter("securitytoken");

            String lockId = (String) request.getParameter("lock_id");
            String lockApp = (String) request.getParameter("lock_app");
            String forwardAction = (String) request.getParameter("forward_action");
            pageData.put("forward_action", forwardAction);

            //Document that will contain the final XML, to be passed to the TransUtil and XSL page
            Document responseDocument = DOMUtil.getDocument();


            // Pull DB connection
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.DATASOURCE_NAME));

            // Unlock ID
            if( action != null
                && action.equals("unlock")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE, "SECURITY_IS_ON");
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        LockDAO lockObj = new LockDAO(conn);
                        lockObj.releaseLock(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), lockId, lockApp);
                    }
                }
            }
            // Retrieve Lock
            else if( action != null
                && action.equalsIgnoreCase("retrieve")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE, "SECURITY_IS_ON");
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        Document lockInfo;

                        LockDAO lockObj = new LockDAO(conn);

                        //Call retrieveLockInfo which will call the DAO to retrieve the results
                        lockInfo = lockObj.retrieveLockXML(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), lockId, lockApp);

                        //get the lock obtained indicator.
                        String sLockedObtained = DOMUtil.getNodeValue(lockInfo, "OUT_LOCK_OBTAINED");

                        //if locked_obtained = yes, you have the lock (regardless if you just established a lock
                        //or have had it for a while).  Else, somebody else has the record locked
                        if (sLockedObtained.equalsIgnoreCase("N"))
                        {
                          //get the locked csr id
                          String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

                          //set an output message
                          String message = "The vendor is currently being updated by " + sCsrId + ".  No other updates can be made to the vendor at this time.";

                          pageData.put("message_display",   message);
                          pageData.put("got_lock", "N");
                        }
                        else
                        {
                          pageData.put("got_lock", "Y");
                        }

                        //Convert the page data hashmap to XML and append it to the final XML
                        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                        //set the iFrame name to be given control back to.
                        String xslName = "CheckLockIFrame";

                        //Get XSL File name
                        File xslFile = getXSL(xslName, mapping);

                        //Transform the XSL File
                        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument,
                            xslFile, (HashMap)request.getAttribute("parameters"));
                    }//end "if (SecurityManager..."
                }//end "if (securityFlag..."
            }//end else if(action.equalsIgnoreCase("retrieve"))
            // Check Lock
            else if( action != null
                && action.equalsIgnoreCase("check")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE, "SECURITY_IS_ON");
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        Document lockInfo;

                        LockDAO lockObj = new LockDAO(conn);

                        //Call retrieveLockInfo which will call the DAO to retrieve the results
                        lockInfo = lockObj.checkLockXML(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), lockId, lockApp);

                        //get the locked indicator.  If somebody else has the lock, this will have
                        //a value = 'Y'.  If you have the lock, or nobody has the lock, it will
                        //carry a value of 'N'.
                        String sLockedIndicator = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_IND");

                        if (sLockedIndicator.equalsIgnoreCase("Y"))
                        {
                          //get the locked csr id
                          String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

                          //set an output message
                          String message = "The vendor is currently being updated by " + sCsrId + ".  No other updates can be made to the vendor at this time.";

                          pageData.put("message_display",   message);
                          pageData.put("got_lock", "N");
                        }
                        else
                        {
                          pageData.put("got_lock","Y");
                        }

                        //Convert the page data hashmap to XML and append it to the final XML
                        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                        //set the iFrame name to be given control back to.
                        String xslName = "CheckLockIFrame";

                        //Get XSL File name
                        File xslFile = getXSL(xslName, mapping);

                        //Transform the XSL File
                        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument,
                            xslFile, (HashMap)request.getAttribute("parameters"));
                    }//end "if (SecurityManager..."
                }//end "if (securityFlag..."
            }//end else if(action.equalsIgnoreCase("check"))
        }//end try
        catch (Throwable ex)
        {
            logger.error(ex);
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }

        return null;
    }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


}