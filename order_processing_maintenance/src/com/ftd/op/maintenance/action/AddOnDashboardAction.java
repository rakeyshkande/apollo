package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.AddOnDashboardBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class AddOnDashboardAction
    extends Action
{

	private static Logger logger = new Logger("com.ftd.op.maintenance.action.AddOnDashboardAction");
  
    /**
     * This is the main action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response)
    {

        HashMap responseMap;
    	HashMap pageData = new HashMap();
        Connection con=null;
		
        ActionForward forward = null;

        try
        {
            String forwardToType = "";
            String forwardToName = "";

            //Connection/Database info        
            con = CommonUtil.getDBConnection();
            
            //Document that will contain the final XML, to be passed to the TransUtil and XSL page
            Document responseDocument = DOMUtil.getDocument();

            //process the request
            responseMap = processRequest(request,con,pageData);

            //Get all the keys in the hashmap returned from the business object
            Set ks = responseMap.keySet();
            Iterator iter = ks.iterator();
            String key;

            //Iterate thru the hashmap returned from the business object using the keyset
            while (iter.hasNext())
            {
                key = iter.next().toString();
                //Append all the existing XMLs to the final XML
                Document xmlDoc = (Document) responseMap.get(key);
                NodeList nl = xmlDoc.getChildNodes();
                DOMUtil.addSection(responseDocument, nl);
            }

            //retrieve the forward info fromt the pageData, and remove it from the pageData. 
            if (pageData.get("forwardToType") != null)
            {
                forwardToType = pageData.get("forwardToType").toString();
                forwardToName = pageData.get("forwardToName").toString();

                pageData.remove("forwardToType");
                pageData.remove("forwardToName");
            }

            //Convert the page data hashmap to XML and append it to the final XML
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            //check if we have to forward to another action, or just transform the XSL.
            if (forwardToType.equalsIgnoreCase("ACTION"))
            {
                forward = mapping.findForward(forwardToName);
                logger.info("forward: " + forward);
                return forward;
            }
            else if (forwardToType.equalsIgnoreCase("XSL"))
            {
                //Get XSL File name
                File xslFile = getXSL(forwardToName, mapping);
                if (xslFile != null && xslFile.exists())
                {
                    //Transform the XSL File
                    try
                    {
                        forward = mapping.findForward(forwardToName);
                        String xslFilePathAndName = forward.getPath(); //get real file name

                        // Retrieve the parameter map set by data filter and update
                        //cbr_action with the next action.
                        HashMap params = getParameters(request);

                        // Change to client side transform
                        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, xslFile, 
                                                                       xslFilePathAndName, params);
                    }
                    catch (Exception e)
                    {
                        logger.error("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + 
                                     " transformation failed" + forwardToName, e);
                        if (!response.isCommitted())
                        {
                            throw e;
                        }
                    }
                }
                else
                {
                    throw new Exception("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + 
                                        " does not exist." + forwardToName);
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        catch (Exception e)
        {
            logger.error(e);
            forward = mapping.findForward("ErrorPage");
            return forward;
        }

        finally
        {
            try
            {
                //Close the connection
                if (con != null)
                {
                    con.close();
                }
            }
            catch (Exception e)
            {
                logger.error(e);
                forward = mapping.findForward("ErrorPage");
                return forward;
            }
        }
    }

    /** 
     * This method walks the request through its processing steps
     * @throws Exception
     */
    private HashMap processRequest(HttpServletRequest request,Connection con, HashMap pageData)
        throws Exception
    {
    	HashMap inputMap;
    	HashMap responseMap;
    	
    	//Get info from the request object
        inputMap = getRequestInfo(request);

        //Get config file info
        getConfigInfo(inputMap);

        //process the action
       responseMap = processAction(request,con,inputMap,pageData);

        //populate the remainder of the fields on the page data
        populatePageData(inputMap,pageData);
        
       
		return responseMap;

    }
    
    /** 
     * Retrieves the request information from the request parameter
     * @throws Exception
     */
    
    private HashMap getRequestInfo(HttpServletRequest request)
        throws Exception
    {
        logger.debug("getRequestInfo()");
        //request paramaters
        String requestActionType = null;
        String requestAddOnId = null;
        String requestAdminAction = null;
        String requestContext = null;
        String requestSecurityToken = null;
        HashMap inputMap = new HashMap();

        //****retrieve the action type
        if (request.getParameter("action_type") != null && !request.getParameter("action_type").equalsIgnoreCase(""))
            requestActionType = request.getParameter("action_type");

        //****retrieve the adminAction
        if (request.getParameter("adminAction") != null && !request.getParameter("adminAction").equalsIgnoreCase(""))
            requestAdminAction = request.getParameter("adminAction");

        //****retrieve the addOnId
        if (request.getParameter("add_on_id") != null && !request.getParameter("add_on_id").equalsIgnoreCase(""))
            requestAddOnId = request.getParameter("add_on_id");

        //****retrieve the context
        if (request.getParameter("context") != null && !request.getParameter("context").equalsIgnoreCase(""))
            requestContext = request.getParameter("context");

        //****retrieve the securitytoken
        if (request.getParameter("securitytoken") != null && 
            !request.getParameter("securitytoken").equalsIgnoreCase(""))
            requestSecurityToken = request.getParameter("securitytoken");

        inputMap.put("requestActionType", requestActionType);
        inputMap.put("requestAdminAction", requestAdminAction);
        inputMap.put("requestAddOnId", requestAddOnId);
        inputMap.put("requestContext", requestContext);
        inputMap.put("requestSecurityToken", requestSecurityToken);
        return inputMap;

    }

    /**  
     * Pulls in the security information
     * @throws Exception
     */

    private void getConfigInfo(HashMap inputMap)
        throws Exception
    {
        String csrId = null;
        String context = (String) inputMap.get("requestContext");
        String securityToken = (String) inputMap.get("requestSecurityToken");

        /* retrieve user information */
        if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
        {
            csrId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
        }

        inputMap.put("csrId", csrId);


        String updateAllowed = null;
        //check if user has edit abilities
        SecurityManager securityManager = SecurityManager.getInstance();
        if (securityManager.assertPermission(context, securityToken, "AddOn Maintenance", 
                                             OPMConstants.SECURITY_UPDATE))
        {
            updateAllowed = "Y";
        }
        else
        {
            updateAllowed = "N";
        }
        inputMap.put("updateAllowed", updateAllowed);
    }

    /** 
     * Determines the type of action the request is for and calls the appropriate BO methods
     * @throws Exception
     */
    
    private HashMap processAction(HttpServletRequest request, Connection con, HashMap inputMap, HashMap pageData)
        throws Exception
    {
        AddOnDashboardBO aodBO = new AddOnDashboardBO();
        String requestActionType = (String) inputMap.get("requestActionType");

        HashMap responseMap=new HashMap();
		//If the action equals "load"
        if (requestActionType.equalsIgnoreCase("load") || requestActionType.equalsIgnoreCase("dashboard"))
        {
            aodBO.processLoad(responseMap, pageData, con);
        }
        //If the action equals "main_menu"
        else if (requestActionType.equalsIgnoreCase("main_menu"))
        {
            aodBO.processMainMenu(pageData);
        }
        //If the action equals edit
        else if (requestActionType.equalsIgnoreCase("edit"))
        {
            aodBO.processEdit(inputMap, responseMap, pageData, con);
            request.setAttribute("hAddOnId", inputMap.get("requestAddOnId"));
        }
        //If the action is not found as above, throw a new exception
        else
        {
            throw new Exception("Invalid Action Type - Please correct");
        }
        return responseMap;
    }

    /** 
     * Places the necessary information into the pagedata object
     * @throws Exception
     */

    private void populatePageData(HashMap inputMap,HashMap pageData)
        throws Exception
    {
        //store the admin action
        pageData.put("adminAction", (String) inputMap.get("requestAdminAction"));

        //store the security token
        pageData.put("context", (String) inputMap.get("requestContext"));

        //store the action type
        pageData.put("original_action_type", (String) inputMap.get("requestActionType"));

        //store the security token
        pageData.put("securitytoken", (String) inputMap.get("requestSecurityToken"));

        //store the updateAllowed
        pageData.put("updateAllowed", (String) inputMap.get("updateAllowed"));
    }

    /** 
     * Retrieve the XSL file that the request maps to
     * @param forwardToName
     * @param mapping
     * @return filename of the XSL file
     */
    private File getXSL(String forwardToName, ActionMapping mapping)
    {
        File xslFile = null;
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardToName);
        xslFilePathAndName = forward.getPath(); //get real file name
        xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }

     /** 
      * Grabs the parameters and action_type attributes from the request object
      * @param request
      * @return contains the parameters and action_type attributes from the request object
      */
    
    private HashMap getParameters(HttpServletRequest request)
    {
        HashMap parameters = new HashMap();

        try
        {
            String action = request.getParameter("action_type");
            parameters = (HashMap) request.getAttribute("parameters");
            parameters.put("action", action);
        }
        finally
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Exiting getParameters");
            }
        }
        return parameters;
    }
}