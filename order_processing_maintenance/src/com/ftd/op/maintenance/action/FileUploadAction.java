package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.FedExShipDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.BlockFlagVO;
import com.ftd.op.maintenance.vo.ContactVO;
import com.ftd.op.maintenance.vo.FedExScheduleVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.util.ServletHelper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.*;
import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * Struts action class
 */
public class FileUploadAction extends Action 
{
    private Logger logger;
    private static final String SUCCESS = "LoadShippingTable";
    private static final String SECURITY_CONTEXT = "context";
    private static final String SECURITY_TOKEN = "securitytoken";
    private static final String PARAMETERS = "parameters";
    private static final String INSERT_RESPONSE = "insert_response";
    private static final String ERRORS_EXIST = "errors_exist";
    private static final String SYSTEM_ERROR_MESSAGE = "system_error_message";
    
    private static final String START_SCHEDULE_XML = "<FedExScheduleList>";
    private static final String END_SCHEDULE_XML = "</FedExScheduleList>";
    
    /**
    * Default constructor.
    * Sets the logger variable
    */
    public FileUploadAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.FileUploadAction");
    }
    
  /**
   * This method will:
   * 1.  Validate security
   * 2.  Validate the action_type parameter passed
   * 3.  Get a database connection
   * 4.  Forward the request to other methods depending on the processing required
   * 5.  Return the results in the response object
   * 6.  All errors are returned in the response object
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
        HashMap requestParameters = new HashMap();
        
        List items = null;
        Connection conn = null;
        ActionForward forward = null;
        String errorResponseMessage = null;
        
        try
        {
            /* Pull DB connection */
            conn = CommonUtil.getDBConnection();
        
            /* Create a new file upload handler */
            DiskFileUpload upload = new DiskFileUpload();
            
            /* Parse the request..this method generates a list of all the items 
            found in the HTTPServlet Request */
            boolean multipartRequest = true;
            try
            {
                items = upload.parseRequest(request);    
            }
            catch(Exception e)
            {
                /* This request is not multipart, attempt to process an non-multpart request
                this type of requests are received during testing */
                logger.error("File parse received a non-multipart form so parsing failed"); 
                errorResponseMessage = "File parse received a non-multipart form so parsing failed";
                multipartRequest = false;
            }
            
            Map fileMap = null;
            FileItem homeFile = null;
            FileItem groundFile = null;
            InputStream fileStream = null;
            
            if(multipartRequest)
            { 
                /* loop through each item in the request */
                fileMap = this.processMultiPartForm(items, requestParameters);
                
                /* Validate security */
                boolean isSecure = this.checkSecurity(requestParameters);
          
                if(isSecure)
                {
                    /* Add file data to List object */
                    Stack zipDataList = new Stack();
                    Iterator fileIterator = fileMap.keySet().iterator(); 
                    while(fileIterator.hasNext())
                    {
                        String fileType = (String) fileIterator.next();
                        FileItem currentFile = (FileItem) fileMap.get(fileType);
                        this.readFileData(zipDataList, fileType, currentFile.getInputStream());
                    }
                                        
                    /* Transorm value object to XML */
                    List zipImportXmlList = this.buildImportXML(zipDataList);
                    
                    /* Insert XML into database for async processing */
                    this.updateImportXML(zipImportXmlList, conn);
                    
                    /* Persist file data to database in batches */
                    //Object[] insertResponses = this.updateZipData(zipImportXmlList, conn);
                    
                    /* Set error list to request to display to user on reload */
                    //request.setAttribute(ERRORS_EXIST, insertResponses[0]);
                    //request.setAttribute(INSERT_RESPONSE, insertResponses[1]);
                }
                else
                {
                    logger.error("Invalid Security Token.");
                    errorResponseMessage = "User security session timed out";
                }
            }

            request.setAttribute(SYSTEM_ERROR_MESSAGE, errorResponseMessage);
            request.setAttribute(PARAMETERS, requestParameters);
            forward = mapping.findForward(SUCCESS);
            
            return forward;   
        }
        catch (Throwable  t)
        {
			logger.error(t);
            forward = mapping.findForward("ErrorPage");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward("ErrorPage");
			}
		}    
        
        return forward;
    }
    
    private Map processMultiPartForm(List items, HashMap requestParameters)
    {
        HashMap fileMap = new HashMap();
        
        /* loop through each item in the request */
        Iterator iter = items.iterator();
        List valList = null;
        while (iter.hasNext()) 
        {
            /* get next item */
            FileItem item = (FileItem) iter.next();

            // determine if item is a file or form parameter
            if (item.isFormField()) 
            {
                /* Maintain security parameters */
                requestParameters.put(item.getFieldName(), (item.getString() == null ? "" : item.getString()));
            } 
            else 
            {
                /* Add the file reference for later processing */
                fileMap.put(item.getFieldName(), item);
            }        
        }   
        
        return fileMap;
    }
    
    private List readFileData(Stack fileDataList, String fileType, InputStream fileStream) throws Exception
    {
        long time = System.currentTimeMillis();
        
        /* Create buffered reader from file stream */
        BufferedReader inFileReader = new BufferedReader(new InputStreamReader(fileStream));
        String fileLine = null;
        
        /* Parse each line and store to primary lost as array */
        int lineNumber = 0;
        FedExScheduleVO fedExSchedule = null;
        StringTokenizer stringTokenizer = null;
        while((fileLine = inFileReader.readLine()) != null)
        {
            stringTokenizer = new StringTokenizer(fileLine, ",");
            
            try
            {
                fedExSchedule = new FedExScheduleVO();
                fedExSchedule.setFileLine(lineNumber++);
                fedExSchedule.setServiceType(fileType);
                fedExSchedule.setOriginZip(formatToken(stringTokenizer.nextToken()));
                fedExSchedule.setOriginTerminal(Integer.parseInt(stringTokenizer.nextToken()));
                fedExSchedule.setDestZip(formatToken(stringTokenizer.nextToken()));
                fedExSchedule.setDestTerminal(Integer.parseInt(stringTokenizer.nextToken()));
                fedExSchedule.setServiceDays(Integer.parseInt(stringTokenizer.nextToken()));
                
                // temp
                if(stringTokenizer.hasMoreTokens())
                {
                    fedExSchedule.setOnTimeService(Double.parseDouble(stringTokenizer.nextToken()));
                }
                
                fileDataList.push(fedExSchedule);
            }
            catch (Exception  e)
            {
                logger.error("Problems occurred parsing line " + fedExSchedule.getFileLine() + " of the " + fileType + " file");
                throw e;
            }
        }
        
        logger.debug("Time taken to read file: " + (System.currentTimeMillis() - time)+ "ms");
        
        return fileDataList;
    }
    
    private List buildImportXML(Stack zipDataList) throws Exception
    {
        long time = System.currentTimeMillis();
        
        LinkedList batchList = new LinkedList();
        int recordCount = 0;
        int batchCount = 0;
        int batchSize = Integer.parseInt(ConfigurationUtil.getInstance().getProperty(OPMConstants.PROPERTY_FILE, OPMConstants.FEDEX_IMPORT_BATCH_SIZE));
       
        FedExScheduleVO fedExScheduleVO = null;
        Iterator fileDataIterator = zipDataList.iterator(); 
        StringBuffer zipImportXML = new StringBuffer();
        
        /* Create xml */
        zipImportXML.append(START_SCHEDULE_XML);
        while(!zipDataList.empty())
        {
            recordCount++;
            
            fedExScheduleVO = (FedExScheduleVO) zipDataList.pop();
            zipImportXML.append(fedExScheduleVO.toXML());
            
            if(recordCount == batchSize)
            {
                logger.debug("Batch " + batchCount++ + " created");
                zipImportXML.append(END_SCHEDULE_XML);
                batchList.add(zipImportXML.toString());
                
                zipImportXML = new StringBuffer();
                zipImportXML.append(START_SCHEDULE_XML);
                
                recordCount = 0;
            }
        }
        zipImportXML.append(START_SCHEDULE_XML);
        
        batchList.add(zipImportXML.toString());
        
        logger.debug("Time taken to build import xml: " + (System.currentTimeMillis() - time)+ "ms");
        
        return batchList;
    }
    
    private String formatToken(String token)
    {
        if(token != null)
        {
            /* Removes quotes from file string */
            return token.substring(1, token.length() - 1);
        }
        
        return "";
    }
    
    private void updateImportXML(List zipImportXmlList, Connection conn) throws Exception
    {
        long time = System.currentTimeMillis();      
        
        /* Insert batches of new data into database */
        int batchSequence = 0;
        String batchId = null;
        String zipImportXml = null;
        FedExShipDAO fedExShipDAO = new FedExShipDAO(conn);
        Iterator zipImportXmlIterator = zipImportXmlList.iterator();
        while(zipImportXmlIterator.hasNext())
        {
            zipImportXml = (String) zipImportXmlIterator.next();
            batchId = fedExShipDAO.insertImportXML(zipImportXml, batchId, batchSequence++, "bmunter", "FED_EX");
        }
        
        logger.debug("Time taken to update database: " + (System.currentTimeMillis() - time)+ "ms");
    }
    
    private Object[] updateZipData(List zipImportXmlList, Connection conn) throws Exception
    {
        long time = System.currentTimeMillis();      

        int totalInserts = 0;
        String xmlString = null;
        Document statusXML = null;
        NodeList errorNodes = null;
        NodeList childNodes = null;
        Node currentChildNode = null;
        
        /* Remove current data from staging table */
        FedExShipDAO fedExShipDAO = new FedExShipDAO(conn);
        fedExShipDAO.emptyFedExGndScheduleStage();
        
        /* Insert batches of new data into database */
        LinkedList errorList = new LinkedList();
        Iterator zipImportXmlIterator = zipImportXmlList.iterator();
        while(zipImportXmlIterator.hasNext())
        {
            xmlString = (String) zipImportXmlIterator.next();
            statusXML = fedExShipDAO.insertFedExGndScheduleStage(xmlString);
                    
            errorNodes = DOMUtil.selectNodes(statusXML, "/FedExScheduleList/FedExSchedule[error != '']");
            if(errorNodes != null && errorNodes.getLength() > 0)
            {
                for (int i = 0; i < errorNodes.getLength(); i++) 
                {
                    childNodes = errorNodes.item(i).getChildNodes();
                    
                    FedExScheduleVO fedExSchedule = new FedExScheduleVO();
                    fedExSchedule.setOriginZip(childNodes.item(0).getFirstChild().getNodeValue());
                    fedExSchedule.setOriginTerminal(Integer.parseInt(childNodes.item(2).getFirstChild().getNodeValue()));
                    fedExSchedule.setDestZip(childNodes.item(4).getFirstChild().getNodeValue());
                    fedExSchedule.setDestTerminal(Integer.parseInt(childNodes.item(6).getFirstChild().getNodeValue()));
                    fedExSchedule.setServiceDays(Integer.parseInt(childNodes.item(8).getFirstChild().getNodeValue()));
                    fedExSchedule.setOnTimeService(Double.parseDouble(childNodes.item(10).getFirstChild().getNodeValue()));
                    fedExSchedule.setServiceType(childNodes.item(12).getFirstChild().getNodeValue());
                    fedExSchedule.setFileLine(Integer.parseInt(childNodes.item(14).getFirstChild().getNodeValue()));
                    errorList.add(fedExSchedule);
                }
            }
            else
            {
                totalInserts += Integer.parseInt(DOMUtil.selectSingleNode(statusXML, "/FedExScheduleList/counts/insert_count/text()").getNodeValue());
            }
        }
        
        /* Determine return value */
        Object[] returnArray = new Object[2];
        if(errorList.size() > 0)
        {
            returnArray[0] = new Boolean(true);
            returnArray[1] = errorList;
        }
        else
        {
            returnArray[0] = new Boolean(false);
            returnArray[1] = new Integer(totalInserts);
        }
        
        logger.debug("Time taken to update database: " + (System.currentTimeMillis() - time)+ "ms");
        
        return returnArray;
    }
    
    private boolean checkSecurity(HashMap requestParameters)
    {
        boolean isSecure = true;
        try
        {
            if (! ServletHelper.isValidToken((String) requestParameters.get("context"), (String) requestParameters.get("securitytoken")))
            {
                isSecure = false;
            }
        }
        catch(Exception e)
        {
            isSecure = false;
        }
        
        return isSecure;
    }
}