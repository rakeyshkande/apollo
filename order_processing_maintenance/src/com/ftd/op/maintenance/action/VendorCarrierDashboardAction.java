package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Struts action class
 */
public class VendorCarrierDashboardAction extends Action 
{
    private Logger logger;

    private static final String APP_PARAMETERS = "parameters";
    private static final String CARRIER_DASHBOARD_XSL = "VendorCarrierDashboardXSL";
    private static final String ERRORS_EXIST = "errors_exist";
    private static final String ERROR_PAGE = "ErrorPage";
    private static final String INSERT_RESPONSE = "insert_response";
    private static final String LOAD_PAGE_PARAMETER = "load_page";
    private static final String PRODUCTION_SUCCESS = "production_success";
    private static final String SUCCESS = "EditCarrier";
    private static final String SYSTEM_ERROR_MESSAGE = "system_error_message";

    /**
    * Default constructor.
    * Sets the logger variable
    */
    public VendorCarrierDashboardAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.VendorCarrierDashboardAction");
    }
    
  /**
   * This method will:
   * 1.  Validate security
   * 2.  Validate the action_type parameter passed
   * 3.  Get a database connection
   * 4.  Forward the request to other methods depending on the processing required
   * 5.  Return the results in the response object
   * 6.  All errors are returned in the response object
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try
        {        
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;
            
            if(sessionId == null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
            
            processLoadPage(request, response, mapping, conn);

        }
        catch (Throwable  t)
        {
            System.out.println(t);
            logger.error(t);
            forward = mapping.findForward(ERROR_PAGE);
        }
        finally
        {
          try
          {
            conn.close();
          }
          catch (SQLException se)
          {
            logger.error(se);
            forward = mapping.findForward(ERROR_PAGE);
          }
        }    
    
        return forward;
  }
    
  /**
   * This method will: 
   * 1.  Gather any requried information
   * 2.  Create an XML transformation to produce the web page 
   *     using loadShippingTables.xsl.
   * 3.  All errors will be returned by throwning an exception
   * 
   * @throws java.lang.Exception
   * @param conn
   * @param mapping
   * @param response
   * @param request
   */
    private void processLoadPage(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn)
        throws Exception
    {
        final File xslFile = getXSL(CARRIER_DASHBOARD_XSL, mapping);
        final MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
        final Document shippingKeys = maintDAO.getActiveVendors();
        
        XMLUtil.addElement((Document)shippingKeys,"origin",request.getParameter("origin"));     
        
        /* Transform output page */
        TraxUtil.getInstance().transform(request, response, shippingKeys, xslFile, (Map)request.getAttribute(APP_PARAMETERS));
    }

   /**
    * get XSL file
    *
    * Retrieve name of Carrier Action XSL file
    * @param1 String - the xsl name returned from the Business Object
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {
        ActionForward forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath();      
        File xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        
        return xslFile;
    }
}

