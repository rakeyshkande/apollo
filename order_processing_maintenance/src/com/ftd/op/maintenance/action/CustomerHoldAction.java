package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.util.LockUtil;
import com.ftd.op.maintenance.vo.OrderVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.security.SecurityManager;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.CustomerHoldVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.osp.framework.dispatcher.Dispatcher;


public class CustomerHoldAction extends Action
{


    private static Logger logger  = new Logger("com.ftd.op.maintenance.action.CustomerHoldAction");
    private String DISPLAY_LOAD_CUSTOMER = "loadcustomer";
    private String SAVE_CUSTOMER = "savecustomer";
    private static final String CANCEL_ORDERS = "cancelorders";
    private static final String RELEASE_ORDERS = "releaseorders";
    
    private String CONFIG_FILE = "maintenance-config.xml";
    
    private final static String ORDER_PROCESSING_STATUS = "ORDER PROCESSING";
    
    private static final int DO_NOTHING = 1;
    private static final int INSERT_HOLD = 2;
    private static final int DELETE_HOLD = 3;
    private static final int UPDATE_HOLD = 4;
    
    private static final String REQUEST_PARAM_FROM_PAGE = "from_page";
    private static final String REQUEST_PARAM_CUST_ID = "customer_id";
    private static final String REQUEST_PARAM_DETAIL_ID= "order_detail_id";
    private static final String REQUEST_PARAM_ORDER_GUID= "order_guid";    
    private static final String REQUEST_PARAM_DNIS_ID = "dnis_id";
    private static final String REQUEST_PARAM_DNIS_DESC = "dnis_desc";
    private static final String REQUEST_PARAM_SECURITYTOKEN = "securitytoken";
    private static final String REQUEST_PARAM_LOCK_VALUE = "lockvalue";
    private static final String REQUEST_PARAM_USER_ID = "userid";
    
    private static final String CUSTOMER_HOLD_LOCK_ENTITY = "CUSTOMER_HOLD";
    
    private static final String FROM_PAGE_CUSTOMER_ACCOUNT = "customer_account";
    private static final String FROM_PAGE_CART = "cart";
    private static final String FROM_PAGE_ORDER_DETAIL = "order_detail";    
    
    private static final String CUSTOMER_TYPE_RECIPIENT = "R";
    private static final String CUSTOMER_TYPE_BUYER = "B";
    
    private static final String LOSS_PREVENTION_QUEUE = "LP";
    
    private static final String EMPTY_USER_ID = "empty";

    public CustomerHoldAction()
    {
    }
    
    /**
     * This is the method that the struts framework invokes.  It takes a look
     * at the request and determines what type of processing should be done.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return 
     * @throws java.lang.Exception
     */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response) 
                                throws Exception
      {
          
        Connection conn = null;          
        ActionForward forward = null;          
        
        try
            {
              //Get the parameter that determine what needs to be done
              String actionType = request.getParameter(OPMConstants.ACTION_TYPE);
        
              /* get action type request parameter */
              if(actionType ==null)
              {
                //forward to error page
                logger.error("Missing action_type in CustomerHoldAction");
                return mapping.findForward(OPMConstants.CONS_SYSTEM_ERROR);
              }

              //get guid and type hold that is being done                  
              String holdType = request.getParameter("r_hold_type");     
                    
              /* get db connection */
              conn = CommonUtil.getDBConnection();
              MaintenanceDAO dao = new MaintenanceDAO(conn);
      
              /* get security token */
              String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);
              String context = (String) request.getParameter(OPMConstants.CONTEXT);
              String customerId = (String) request.getParameter(REQUEST_PARAM_CUST_ID);
              String lockValue = (String) request.getParameter(REQUEST_PARAM_LOCK_VALUE);
              String lockResult = "";
              String userId = null;
        
              if(sessionId==null)
              {
                logger.error("Security token not found in request object");
              }
        
              /* retrieve user information */
              if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
              {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
              }
			
              //get token
              String securityToken = request.getParameter(REQUEST_PARAM_SECURITYTOKEN);                  
            
              /* determine which action to process */
      
              //Execute the method to do whatever the user requested to be done 
              if(actionType.equals(DISPLAY_LOAD_CUSTOMER))
              {                      
                  //lock the customer
                  lockResult = LockUtil.getLock(conn,CUSTOMER_HOLD_LOCK_ENTITY,customerId,securityToken,userId);      
      
                  processLoadCustomer(conn,request,response,mapping,lockResult);
              }
              else if(actionType.equals(SAVE_CUSTOMER))
              {        
                  processSaveCustomer(conn,request,response,mapping);
                  
                  //unlock cusstomer
                  if(lockValue == null || lockValue.length() == 0){
                      LockUtil.getLock(conn,CUSTOMER_HOLD_LOCK_ENTITY,customerId,securityToken,userId);
                  }
              }
              else if(actionType.equals(CANCEL_ORDERS))
              {        
                  processCancel(conn,request,response,mapping);

                  //unlock cusstomer
                  if(lockValue == null || lockValue.length() == 0){
                      LockUtil.getLock(conn,CUSTOMER_HOLD_LOCK_ENTITY,customerId,securityToken,userId);
                  }                  
              }
              else if(actionType.equals(RELEASE_ORDERS))
              {        
                  processRelease(conn,request,response,mapping,userId);
                  
                  //unlock cusstomer
                  if(lockValue == null || lockValue.length() == 0){
                      LockUtil.getLock(conn,CUSTOMER_HOLD_LOCK_ENTITY,customerId,securityToken,userId);
                  }                  
              }              
            }
            catch(Exception e)
            {
              logger.error(e);  
              forward = mapping.findForward(OPMConstants.CONS_SYSTEM_ERROR);
            }
            finally
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            
            return null;
            
      }
            
    /**
     * This method loads the page up with the initial data.
     * 
     * @param conn
     * @param request
     * @param response
     * @param mapping
     * @throws java.lang.Exception
     */
    private void processLoadCustomer(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping,String lockResult)
        throws Exception
    {
    
     // XMLDocument xmlDoc = (XMLDocument) DOMUtil.getDocument(); 
      String xslFile = "customerHold.xsl";      

      //Get the customer id from request
      String customerId = request.getParameter(REQUEST_PARAM_CUST_ID);
      String orderDetailId = request.getParameter(REQUEST_PARAM_DETAIL_ID);
      String securityToken = request.getParameter(REQUEST_PARAM_SECURITYTOKEN);      
      
      //Retreive the lock information
      MaintenanceDAO dao = new MaintenanceDAO(conn);     
      
      //if coming from account page get all holds associated with the customer
      String fromPage = request.getParameter(REQUEST_PARAM_FROM_PAGE);
      Document xmlDoc = null;
      if(fromPage.equalsIgnoreCase(FROM_PAGE_CUSTOMER_ACCOUNT))
      {          
          xmlDoc = dao.getCustomerHoldInfo(customerId);
      }
      else if(fromPage.equalsIgnoreCase(FROM_PAGE_CART))
      {
          xmlDoc = dao.getCustomerHoldInfo(orderDetailId,CUSTOMER_TYPE_BUYER);
      }
      else if(fromPage.equalsIgnoreCase(FROM_PAGE_ORDER_DETAIL))
      {
          xmlDoc = dao.getCustomerHoldInfo(orderDetailId,CUSTOMER_TYPE_RECIPIENT);
      }
      else
      {
          throw new Exception("Invalid value in 'page_from' parameter.:" + fromPage);
      }
      

      
     //create page data document
      Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
      Element pageDataXML = (Element) pageDataDoc.createElement("pagedata");
      pageDataDoc.appendChild(pageDataXML);      

      //get security info
      SecurityManager securityManager = SecurityManager.getInstance();
      UserInfo userInfo = securityManager.getUserInfo(request.getParameter("securitytoken"));

      //dnis info
      String dnisId = request.getParameter(REQUEST_PARAM_DNIS_ID);
      String dnisDesc = "";
      if(dnisId != null)
      {
          dnisDesc = dao.getDNIS(dnisId);
      }

      //page data
      CommonUtil.addElement(pageDataDoc,"holdreason",getHoldReason(xmlDoc));
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_FROM_PAGE,request.getParameter(REQUEST_PARAM_FROM_PAGE));
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_CUST_ID,request.getParameter(REQUEST_PARAM_CUST_ID));
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_DETAIL_ID,request.getParameter(REQUEST_PARAM_DETAIL_ID));
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_ORDER_GUID,request.getParameter(REQUEST_PARAM_ORDER_GUID));
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_DNIS_ID,dnisId);
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_DNIS_DESC,dnisDesc);      
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_LOCK_VALUE,lockResult);            
      CommonUtil.addElement(pageDataDoc,REQUEST_PARAM_USER_ID,userInfo.getUserID());                  

      DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());
            
      //add hold reason
      DOMUtil.addSection(xmlDoc,dao.getHoldReasonsXML().getChildNodes());
      
      // JP Puzon 01-21-2006
      // The line below has been commented out for PCI compliance
      // since the xml document contains credit card numbers.
      // logger.debug(CommonUtil.convertToString(xmlDoc));
    
      TraxUtil.getInstance().transform(request, response, xmlDoc, getXSL(xslFile,mapping), (HashMap)request.getAttribute(OPMConstants.CONS_APP_PARAMETERS));

    }
    
  /**
     * This method is invoked when teh user clicks save on the page.  It causes
     * the checked holds to be inserted and unchecked holds to be deleted.
     * 
     * @param conn
     * @param request
     * @param response
     * @param mapping
     * @throws java.lang.Exception
     */
  private void processSaveCustomer(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping)
    throws Exception
  {
        int counter = 1;
        int change = 0;
  
        String origHoldType = null; 
        String holdChkBox = null;
        boolean holdTypeChange = false;
        boolean entityTypeInserted = false;
        
        //history of what happened
        List insertHoldList = new ArrayList();
        List updateHoldList = new ArrayList();
        List deleteHoldList = new ArrayList();
        
        CustomerHoldVO holdVO = new CustomerHoldVO();
  
        //get original hold type
        String origOverallHoldType = request.getParameter("orig_hold_reason");

        //get customer id
        String customerId = request.getParameter("cust_num_value");
       
        //get new hold type        
        holdVO.setHoldReasonCode(request.getParameter("holdreason"));;
        
        //check if the hold type changed
        if(!origOverallHoldType.equals(holdVO.getHoldReasonCode()))
        {
            holdTypeChange = true;
        }
        
        //check customer id        
        origHoldType = request.getParameter("cust_num_hold_value");        
        holdVO.setHoldValue1(request.getParameter("cust_num_value"));        
        holdChkBox = request.getParameter("cust_num_hold_chk");
        holdVO.setEntityId(request.getParameter("cust_num_hold_id"));
        holdVO.setEntityType("CUSTOMER_ID");
        change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
        performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);
        
        //check customer name        
        origHoldType = request.getParameter("cust_name_hold_value");        
        holdVO.setHoldValue1(request.getParameter("cust_name_value"));        
        holdChkBox = request.getParameter("cust_name_hold_chk");
        holdVO.setEntityId(request.getParameter("cust_name_hold_id"));
        holdVO.setEntityType("NAME");
        change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
        performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);

        //check customer address        
        origHoldType = request.getParameter("cust_addr_hold_value");        
        holdVO.setHoldValue1(request.getParameter("cust_addr_value"));        
        holdChkBox = request.getParameter("cust_addr_hold_chk");
        holdVO.setEntityId(request.getParameter("cust_addr_hold_id"));
        holdVO.setEntityType("ADDRESS");
        change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
        performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);
        
        //check credit card
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_card_value" + counter));
        entityTypeInserted = false;
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_card_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_card_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_card_hold_id" + counter));
            holdVO.setEntityType("CC_NUMBER");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
            
            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_card_value" + counter));        
        }

        //check phone number
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_phone_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_phone_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_phone_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_phone_hold_id" + counter));
            holdVO.setEntityType("PHONE_NUMBER");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_phone_value" + counter));        
        }

        //check member number
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_member_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_member_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_member_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_member_hold_id" + counter));    
            holdVO.setEntityType("MEMBERSHIP_ID");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_member_value" + counter));        
        }
        
        //check email
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_email_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_email_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_email_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_email_hold_id" + counter));
            holdVO.setEntityType("EMAIL_ADDRESS");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_email_value" + counter));        
        }        

        //check lp indicator
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_lp_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_lp_hold_value" + counter);
            holdChkBox = request.getParameter("cust_lp_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_lp_hold_id" + counter));
            holdVO.setEntityType("LOSS_PREVENTION_INDICATOR");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_lp_value" + counter));        
        }        

        //get commetn origin
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String origin = configUtil.getProperty("maintenance-config.xml","CUST_HOLD_COMMENT_ORIGIN");

        //get user
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(request.getParameter("securitytoken"));

        //Comment object
        CommentsVO commentsVO = new CommentsVO();        
        commentsVO.setCommentOrigin(origin);
        commentsVO.setCommentType("Customer");
        commentsVO.setCustomerId(customerId);    
        commentsVO.setUpdatedBy(userInfo.getUserID());
        
        if(commentsVO.getUpdatedBy() == null || commentsVO.getUpdatedBy().length() == 0)
        {
            commentsVO.setUpdatedBy("empty");
        }
        
        //dao to insert comments
        MaintenanceDAO dao = new MaintenanceDAO(conn);
        
        //add comments to customer for inserted holds
        if(insertHoldList != null && insertHoldList.size() > 0)
        {
           String msg = "Customer field(s) " +convertToString(insertHoldList )+" placed on hold status for " + holdVO.getHoldReasonCode(); 
           commentsVO.setComment(msg);
           dao.insertComment(commentsVO);
        }

        //add comments to customer for deleted holds
        if(deleteHoldList != null && deleteHoldList.size() > 0)
        {
           String msg = "Customer field(s) " +convertToString(deleteHoldList )+" removed from hold status for " + holdVO.getHoldReasonCode(); 
           commentsVO.setComment(msg);
           dao.insertComment(commentsVO);
        }

        //add comments to customer for updated holds
        if(updateHoldList != null && updateHoldList.size() > 0)
        {
           String msg = "Customer field(s) " +convertToString(updateHoldList )+" hold status type changed to " + holdVO.getHoldReasonCode(); 
           commentsVO.setComment(msg);
           dao.insertComment(commentsVO);
        }        
        
  }


  private void processRelease(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping,String userId)
      throws Exception
  {


     String fromPage = request.getParameter(REQUEST_PARAM_FROM_PAGE);
     String customerId = request.getParameter(REQUEST_PARAM_CUST_ID);
     String orderDetailId = request.getParameter(REQUEST_PARAM_DETAIL_ID);
     String orderGuid = request.getParameter(REQUEST_PARAM_ORDER_GUID);
     
     //get commetn origin
     ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
     String origin = configUtil.getProperty("maintenance-config.xml","CUST_HOLD_COMMENT_ORIGIN");     
     
     MaintenanceDAO dao = new MaintenanceDAO(conn);
     
     List orderList = new ArrayList();

     CommentsVO commentsVO = new CommentsVO();        
     commentsVO.setCommentOrigin(origin);
     commentsVO.setCommentType("Order");
     commentsVO.setCustomerId(customerId);    
     commentsVO.setUpdatedBy(userId);
     
     //if we are releasing all orders on the cart
     if(fromPage.equalsIgnoreCase(FROM_PAGE_CART))
     {
        //get list of orders on hold
        orderList = dao.getHeldOrders(customerId,orderGuid);   
        if(orderList != null)
        {
            //retrieve the associated order
            OrderVO order = dao.getOrder(orderGuid);       
        
            //put comment on order
            String msg = "Shopping cart "+order.getMasterOrderNumber() +" orders released for processing.";
            commentsVO.setComment(msg); 
            commentsVO.setOrderGuid(orderGuid);
            dao.insertComment(commentsVO);
        
            //loop through orders
            Iterator iter = orderList.iterator();
            while(iter.hasNext())
            {
                releaseOrder(conn,request.getParameter("securitytoken"),(String)iter.next());
            }
        }
     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_ORDER_DETAIL))
     {

        //put comment on order
        String msg = "Order released for processing.";         
        commentsVO.setComment(msg);  
        commentsVO.setOrderDetailId(orderDetailId);
        dao.insertComment(commentsVO);     
     
        releaseOrder(conn,request.getParameter("securitytoken"),orderDetailId);
     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_CUSTOMER_ACCOUNT))
     {
        //get list of orders on hold
        orderList = dao.getHeldOrders(customerId);   
        if(orderList != null)
        {
            //loop through orders
            Iterator iter = orderList.iterator();
            while(iter.hasNext())
            {
                //put comment on order
                String msg = "Order released for processing.";        
                commentsVO.setComment(msg);  
                commentsVO.setOrderDetailId(orderDetailId);
                dao.insertComment(commentsVO);     
            
                releaseOrder(conn,request.getParameter("securitytoken"),(String)iter.next());
            }
        }         
     }
     else
     {
         throw new Exception("Unknown value in 'from_page' parameter.");
     }
     
  }
  

    private void processCancel(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping)
      throws Exception
  {

     String fromPage = request.getParameter(REQUEST_PARAM_FROM_PAGE);
     String customerId = request.getParameter(REQUEST_PARAM_CUST_ID);
     String orderDetailId = request.getParameter(REQUEST_PARAM_DETAIL_ID);
     String orderGuid = request.getParameter(REQUEST_PARAM_ORDER_GUID);
     
     MaintenanceDAO dao = new MaintenanceDAO(conn);
     
     List orderList = new ArrayList();
     
     //if we are releasing all orders on the cart
     if(fromPage.equalsIgnoreCase(FROM_PAGE_CART))
     {
        //get list of orders on hold
        orderList = dao.getHeldOrders(customerId,orderGuid);   
        if(orderList != null)
        {
            //loop through orders
            Iterator iter = orderList.iterator();
            while(iter.hasNext())
            {
                cancelOrder(conn,request.getParameter("securitytoken"),(String)iter.next());
            }
        }
     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_ORDER_DETAIL))
     {
         cancelOrder(conn,request.getParameter("securitytoken"),orderDetailId);
     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_CUSTOMER_ACCOUNT))
     {
        //get list of orders on hold
        orderList = dao.getHeldOrders(customerId);   
        if(orderList != null)
        {
            //loop through orders
            Iterator iter = orderList.iterator();
            while(iter.hasNext())
            {
                cancelOrder(conn,request.getParameter("securitytoken"),(String)iter.next());
            }
        }         
     }
     else
     {
         throw new Exception("Unknown value in 'from_page' parameter.");
     }
     
  }

  private void cancelOrder(Connection conn,String securitytoken, String orderDetailId)
    throws Exception
  {
        //get user
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securitytoken);

        /* retrieve user information */
        String userId = EMPTY_USER_ID;
        if(SecurityManager.getInstance().getUserInfo(securitytoken)!=null)
        {
            userId = SecurityManager.getInstance().getUserInfo(securitytoken).getUserID();
        }

        MaintenanceDAO dao = new MaintenanceDAO(conn);    
        dao.deleteFromQueue(orderDetailId,LOSS_PREVENTION_QUEUE,userId);
     
        dao.updateOrderDisposition(Long.parseLong(orderDetailId),"Cancelled",userId);
     
  }


  private void releaseOrder(Connection conn,String securitytoken,String orderDetailId)
    throws Exception
  {
  
    //get flag to determine if JMS should be sent out.  (used while testing)
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String sendJMS = configUtil.getProperty(CONFIG_FILE,"SEND_JMS_TO_OP");  
  
    //send  out notification to order processing
    if(sendJMS == null || !sendJMS.equals("N")){
        MessageToken messageToken = new MessageToken();
        messageToken.setMessage("RELEASE|" + orderDetailId);          
        messageToken.setStatus(ORDER_PROCESSING_STATUS);
        Dispatcher dispatcher = Dispatcher.getInstance();       
        dispatcher.dispatchTextMessage(new InitialContext(),messageToken);  
    }      

        //get user
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securitytoken);

        /* retrieve user information */
        String userId = EMPTY_USER_ID;
        if(SecurityManager.getInstance().getUserInfo(securitytoken)!=null)
        {
            userId = SecurityManager.getInstance().getUserInfo(securitytoken).getUserID();
        }
    
        MaintenanceDAO dao = new MaintenanceDAO(conn);    
        dao.deleteFromQueue(orderDetailId,LOSS_PREVENTION_QUEUE,userId);
    
  }


  private String convertToString(List list)
  {
        if(list == null)
        {
            return null;
        }
        
        Iterator iter = list.iterator();
        String msg = "";
        while(iter.hasNext())
        {
            if(msg.length() > 0)
            {
                msg = msg + ",";                
            }
            msg = msg + (String)iter.next();
        }
        
        return msg;
  }

  /*
     * 
     * @param conn = Database connection
     * @param origHoldType = The current hold type for the item (or null if not on hold)
     * @param holdChkBox = The value of the hold checkbox
     * @param holdType = The hold type to be used on any holds that are created
     * @param holdTypeChange = 
     * @param value = The hold value
     * @param existingHoldId = the id of the existing hold record..if one exists
     * @throws java.lang.Exception
     */
  private int checkForChange(  String origHoldType,                                 
                                String currentholdType,
                                String holdChkBox)
    throws Exception
  {

      
      //Was this item on hold before?
      if(origHoldType == null || origHoldType.length() == 0)
      {
          //It wasn't on hold, check if should now be put on hold
          if(checked(holdChkBox))
          {
              //insert the hold
              return INSERT_HOLD;
          }
          else
          {
              //Do nothing. Item was not hold befoe and will still not be on hold.
              return DO_NOTHING;
          }
      }
      //else this was on hold before, should we keep it on hold?
      else if(checked(holdChkBox))
      {
          //Keep item on hold, check if the hold type should be changed
          if(origHoldType.equals(currentholdType))
          {
              //Do nothing.  The item will stay on hold with the same hold type.
              return DO_NOTHING;
          }
          else
          {
              //hold type was changed, update record
              return UPDATE_HOLD;
          }
      }else
      {
          //Take the item off hold
          return DELETE_HOLD;
      }
      

  }


  private void performChange(Connection conn,
                                int change,
                                CustomerHoldVO holdVO,
                                List insertHoldList,
                                List updateHoldList,
                                List deleteHoldList)
    throws Exception
  {
        performChange(conn,change,holdVO,insertHoldList,updateHoldList,deleteHoldList,true);
  }


  private void performChange(Connection conn,
                                int change,
                                CustomerHoldVO holdVO)
    throws Exception
  {
        performChange(conn,change,holdVO,null,null,null,false);
  }

 
  private void performChange(Connection conn,
                                int change,
                                CustomerHoldVO holdVO,
                                List insertHoldList,
                                List updateHoldList,
                                List deleteHoldList,
                                boolean insertIntoList)
    throws Exception
  {


      //Create dao for saving the changes
      MaintenanceDAO dao = new MaintenanceDAO(conn);     
      
     
      //Was this item on hold before?
      if(change == DO_NOTHING)
      {
          // JP Puzon 01-21-2006
          // The line below has been commented out for PCI compliance
          // since the hold value can be a credit card number.
          //System.out.println("Do nothing. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType());      
      }
      else if(change == INSERT_HOLD)
      {
          System.out.println("Insert Hold. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType()); 
          dao.insertCustomerHold(holdVO);          
          
          if(insertIntoList)
          {
              insertHoldList.add(holdVO.getEntityType());              
          }
      }
      else if(change == UPDATE_HOLD)
      {
          System.out.println("Update hold type. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType());
          //dao.updateCustomerHold(holdVO);    
          dao.deleteCustomerHold(holdVO);
          dao.insertCustomerHold(holdVO);
          
          if(insertIntoList)
          {
              updateHoldList.add(holdVO.getEntityType());
          }          
      }
      else if(change == DELETE_HOLD)
      {
          System.out.println("Delete hold type. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType());
          dao.deleteCustomerHold(holdVO);    
          
          if(insertIntoList)
          {
              deleteHoldList.add(holdVO.getEntityType());
          }                    
      }
      else
      {
          throw new Exception("Invalud change type: " + change);
      }

  }
  
  private boolean checked(String value)
  {
      return (value !=null && value.equalsIgnoreCase("on")) ? true : false;
  }
  
    
  private String getHoldReason(Document xmlDoc) throws Exception
  {
        String xpath = "root/customers/customer/id_reason";
        String holdValue = null;
        NodeList nl = DOMUtil.selectNodes(xmlDoc, xpath);
        if(nl.getLength() > 0){
            for (int i = 0; i < nl.getLength(); i++)
            {
                Text node = (Text)nl.item(i).getFirstChild();
                if(node != null)
                {
                    holdValue = node.getNodeValue();                    
                }                
                
                if(holdValue != null && holdValue.length() >0)
                {
                    return holdValue;
                }
            }
        }

        
        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "root/customers/customer/name_reason";
            nl = DOMUtil.selectNodes(xmlDoc, xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                
                }
            }            
        }

        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "root/customers/customer/address_reason";
            nl = DOMUtil.selectNodes(xmlDoc, xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                       return holdValue;
                    }                                    
                }
            }            
        }

        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "//hold_reason_code";
            nl = DOMUtil.selectNodes(xmlDoc, xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                                    
                }
            }            
        }
        
        return holdValue;
                
  }
    
  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward("CustomerHoldXSL");
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }        
  
     /** This method converts an XML document to a String. 
     * @param XMLDocument document to convert 
     * @return String version of XML */
    private String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";      
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }        
    
}