package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.FeesBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;


/**
 * FuelSurchargeAction class
 *
 * The �Fuel Surcharge Maintenance� screen allows a user to view/add/edit the fuel surcharge.
 *
 */
public final class FuelSurchargeMaintAction extends Action 
{
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.FuelSurchargeMaintAction");
  private Connection conn = null;
  //request xsl filename
  private static final String FUEL_SURCHARGE_XSL = "FuelSurchargeXSL";
  
  //application:
  public static final String APP_PARAMETERS = "parameters";
  
  //request parameters
  public static final String ACTION_TYPE = "action_type";
  public static final String ACTION_RESPONSE = "action_response";
  
  //action types
  public static final String  LOAD_ACTION = "Load";
  public static final String  SAVE_ACTION = "Save";
  public static final String  BACK_ACTION = "BackAction";
  private static final String MAIN_MENU_ACTION = "MainMenuAction";
  private static final String ERROR_ACTION = "Error";
  
  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";
  public static final String VALID_FLAG = "valid_flag";
  public static final String ERROR_VALUE = "error_value";
  public static final String UPDATE_ALLOWED = "update_allowed";
  public static final String LOCK_VALUE = "lock_value";
  public static final String LOCK_FLAG = "lock_flag";
  
     
  /**
  This is the Fuel Surcharge Maintenance Action called from the Struts framework.
  * 
  * @param mapping            - ActionMapping used to select this instance
  * @param form               - ActionForm (optional) bean for this request
  * @param request            - HTTP Request we are processing
  * @param response           - HTTP Response we are processing
  * @return forwarding action - next action to "process" request
  * @throws IOException
  * @throws ServletException
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException
  {
    String actionType = null;
    CommonUtil commonUtil = new CommonUtil();
    
    try {
        
        conn = commonUtil.getDBConnection();

      
        /* get action type request parameter */
        if(request.getParameter(ACTION_TYPE)!=null) {
          actionType = request.getParameter(ACTION_TYPE);
        }
        else {
          actionType = this.LOAD_ACTION;
        }
        logger.debug("action: " + actionType);
        
        // Load 
        if(actionType.equals(LOAD_ACTION)) {             
              return performLoad(mapping, form, request, response);
        }  
        // save 
        else if(actionType.equals(SAVE_ACTION)) { 
          return performSave(mapping, form, request, response);        
        }
        // return to main menu
        else if(actionType.equals(MAIN_MENU_ACTION)) {
          return performMainMenu(mapping);
        }    
        else if(actionType.equals(BACK_ACTION)) {
          return performBack(mapping);
        }
    } catch (Exception e) {
        logger.error(e);
        return goToError(mapping);
    } finally {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception ce) {
                logger.error(ce);
            }
        }
    }
    return null;
  } 

    private ActionForward performLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        return performLoad(mapping, form, request, response, "");
        
    }
  
    private ActionForward performLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, String actionResponse) throws Exception
      {
            Document xmlDoc  = null;
            File xslFile = null;
            FeesBO bo = new FeesBO();

            xslFile = getXSL(FUEL_SURCHARGE_XSL,mapping);
              
            xmlDoc = DOMUtil.getDocument();
              
            /* get security token */
            String sessionId = request.getParameter(OPMConstants.SEC_TOKEN);
            String context = request.getParameter(OPMConstants.CONTEXT);

            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
              
            /* retrieve user information */
            String userId = null;
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }       
            
            Document doc = bo.getFuelSurchargeXML(conn);
            DOMUtil.addSection(xmlDoc,doc.getChildNodes()); 
            
            NovatorFeedUtil feedUtil = new NovatorFeedUtil();;
            Document updateSetupDoc = feedUtil.getNovatorEnvironmentSetup();
            DOMUtil.addSection(xmlDoc,updateSetupDoc.getChildNodes());
              
            /* build page data */
            HashMap pageData = new HashMap();
            
            /* check if user has access */
            boolean updateAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"FuelSurchargeData","Update");

            if(updateAllowed) {
                pageData.put(UPDATE_ALLOWED, "Y"); 
            } else {
                pageData.put(UPDATE_ALLOWED, "N");
            }
            pageData.put(ACTION_RESPONSE, actionResponse);
            
            /* convert the page data hashmap to XML and append it to the final XML */
            DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
            
            StringWriter sw = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(sw));
            logger.debug(sw.toString());
            
            HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
            TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
            
            return null;
        } 
        

  /**
     * Save changes to a redemption rate.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
  private ActionForward performSave(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
  {
      FeesBO bo = new FeesBO();
      String actionResponse = "";
      
      try {
          actionResponse = bo.saveFuelSurcharge(conn, request);
          //Implemented as per Fees Micro service
          Document doc = bo.getFuelSurchargeXML(conn);
          bo.InsertFuelSurchargeFeed(conn, doc);
      } catch (Exception e)  {
            logger.error(e);
            actionResponse = OPMConstants.RETURN_MESSAGE_SAVE_FAILED;
      }      
      
      return performLoad(mapping, form, request, response, actionResponse);
      
  }

    /**
     * Main menu.
     * @param mapping
     * @return
     */
  private ActionForward  performMainMenu(ActionMapping mapping)
  {
      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
      return newForward;

  }
  
    /**
     * Operations menu.
     * @param mapping
     * @return
     */
    private ActionForward  performBack(ActionMapping mapping)
    {
          /* forward to main menu action */
          ActionForward newForward = mapping.findForward(BACK_ACTION);
          return newForward;
    }  
    
    private ActionForward  goToError(ActionMapping mapping)
    {
        ActionForward newForward = null;
        try {
            /* forward to main menu action */
            newForward = mapping.findForward(ERROR_ACTION);
            
        } catch (Exception e) {
            logger.error(e);
        }
        return newForward;
    }    

  /**
  * get XSL file
  */
  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
    


}