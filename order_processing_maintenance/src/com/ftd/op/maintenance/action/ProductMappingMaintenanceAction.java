package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.action.ProductMappingMaintenanceForm;
import com.ftd.op.maintenance.bo.ProductMappingBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.BaseProductMappingVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import org.apache.struts.action.ActionForward;   

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;



public class ProductMappingMaintenanceAction  extends Action
{
  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/

  private static Logger logger = new Logger("com.ftd.op.maintenance.action.ProductMappingMaintenanceAction");


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public ProductMappingMaintenanceAction()
  {
  }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {

    ActionForward forward = null;
    ProductMappingBO productMappingBO = new ProductMappingBO();
    DataSource db  = null; 
    Connection conn = null;
    
	  
	HashMap requestInfoMap = new HashMap();
	String requestActionType = "";
	
	try
    {
      
      //Get info from the request object
      requestInfoMap = getRequestInfo(request);
      
      String csrId = getConfigInfo(requestInfoMap);
      
      //Get Action Type
      if(requestInfoMap.containsKey("requestActionType")){
    	  requestActionType = (String) requestInfoMap.get("requestActionType");
  	  }
      
      //If the action equals "load" - that is Initial load or Refresh data
      if (requestActionType.equalsIgnoreCase("load") || requestActionType.equalsIgnoreCase("clear"))
      {
    	  if(requestActionType.equalsIgnoreCase("clear"))
    	  {	  ProductMappingMaintenanceForm prodform = (ProductMappingMaintenanceForm) form;
    	  	  prodform.setFloralSKU("");	
    	  	  prodform.setVendorProdSkuList(null);
    	  }
    	  
    	  Map<String, List<BaseProductMappingVO>> allProductMappings = productMappingBO.getProductMappings();
    	  request.setAttribute("phoenixStatus", "none");
    	  request.setAttribute("allProductMappings",allProductMappings);
    	  request.setAttribute("action_type","load");
    	  
    	  return mapping.findForward("ProductMappingMaintenance");
      }
      
      //If the action equals "remove"
      else if (requestActionType.equalsIgnoreCase("remove"))
      {
    	  String original_floral_product_sku = (String)request.getParameter("floral_product_sku");
    	  productMappingBO.deleteProductMapping(original_floral_product_sku);
    	  
    	  Map<String, List<BaseProductMappingVO>> allProductMappings = productMappingBO.getProductMappings();
    	  request.setAttribute("phoenixStatus", "none");
    	  request.setAttribute("allProductMappings",allProductMappings);
    	  request.setAttribute("action_type","load");
    	  
    	  return mapping.findForward("ProductMappingMaintenance");    	  
      }
      
    //If the action equals "doPhoenixProcessing"
      else if (requestActionType.equalsIgnoreCase("doPhoenixProcessing"))
      {
    	  conn =  CommonUtil.getDBConnection();
    	  OrderDAO dao = new OrderDAO(conn);
    	  
    	  GlobalParameterVO globalParam = dao.getGlobalParameter(
                  "PHOENIX", "BULK_PHOENIX_RUNNING_STATUS");
          
          if((globalParam.getValue() != null)
                  && (globalParam.getValue()
                          .equalsIgnoreCase("N"))){
        	  OrderService orderService = new OrderService(conn);
        	  String phoenixStatus = orderService.doBulkPhoenixProcessing();
        	  if(phoenixStatus.equals("processing")){
        		  request.setAttribute("phoenixStatus", "Processing");
        	  }
        	  else if(phoenixStatus.equals("hold")){
        		  request.setAttribute("phoenixStatus", "Hold");
        	  }else if(phoenixStatus.equals("error")){
        		  request.setAttribute("phoenixStatus","Error");
        	  }
          }else{
        	  request.setAttribute("phoenixStatus", "Hold");
          }
    	  
    	 
    	  Map<String, List<BaseProductMappingVO>> allProductMappings = productMappingBO.getProductMappings();
    	  request.setAttribute("allProductMappings",allProductMappings);
    	  request.setAttribute("action_type","load");
    	  
    	  return mapping.findForward("ProductMappingMaintenance");    	  
      }
      
      //If the action equals "save"
      else if (requestActionType.equalsIgnoreCase("save"))
      {
    	  Boolean disabled = new Boolean(false);   
    	  ProductMappingMaintenanceForm prodform = (ProductMappingMaintenanceForm) form;
    	  List<BaseProductMappingVO> baseProdMapVo = prodform.getVendorProdSkuList();    	  
    	  String origProductId = prodform.getFloralSKU();

    	  List<String> errMsgList = null;
    	  
    	  if(origProductId != null && prodform.getVendorProdSkuList() == null)
    	  {
    		  errMsgList = new ArrayList<String>();
    		  errMsgList.add(OPMConstants.VENDOR_PRODUCT_NOT_NULL_MSG);
    		  request.setAttribute("errorMessage",errMsgList);
    		  request.setAttribute("disabled", disabled);  
    		  request.setAttribute("action_type","load");
    	  }
    	  else
    	  {
    		  try{
    			  errMsgList =  productMappingBO.addProductMapping(origProductId, baseProdMapVo, csrId);    		
    			  if(errMsgList != null && errMsgList.size() > 0 )
    			  {
    				  disabled = new Boolean(true);

    				  request.setAttribute("errorMessage",errMsgList);
    				  request.setAttribute("disabled", disabled);  
    				  request.setAttribute("action_type","update");
    			  }
    			  else
    			  {
    				  prodform.setFloralSKU("");	
    				  prodform.setVendorProdSkuList(null);
    				  request.setAttribute("action_type","load");
    				  request.setAttribute("errorMessage",errMsgList);
    			  }
    		  }
    		  catch(Exception e)
    		  {
    			  errMsgList = new ArrayList<String>();
    			  errMsgList.add(e.getMessage());
    			  request.setAttribute("errorMessage",errMsgList);
    			  request.setAttribute("disabled", disabled);  
    			  request.setAttribute("action_type","update");
    		  }
    	  }

    	  Map<String, List<BaseProductMappingVO>> allProductMappings = productMappingBO.getProductMappings();
    	  request.setAttribute("allProductMappings",allProductMappings);    
    	  request.setAttribute("phoenixStatus", "none");
    	  return mapping.findForward("ProductMappingMaintenance");    	      	  
      }
      
      //If the action equals "edit or add"
      else if (requestActionType.equalsIgnoreCase("update"))
      {
    	  Boolean disabled = new Boolean(false);   
    	  List<String> errorMessageList = null;
    	  String errorMessage = "";
    	  String original_floral_product_sku = (String)request.getParameter("floral_product_sku");
      	  ProductMappingMaintenanceForm indexProdMapForm = (ProductMappingMaintenanceForm)form;
      	  
    	  Map<String, List<BaseProductMappingVO>> productMapping = null;  
    	  Map<String, List<BaseProductMappingVO>> allProductMappings = productMappingBO.getProductMappings();
    	  
    	  try
    	  {
    		  original_floral_product_sku = original_floral_product_sku.toUpperCase();
    		  productMapping = productMappingBO.getProductMappingByFloristProductId(original_floral_product_sku);
    		  if(productMapping != null && productMapping.size() > 0)
    		  {
    			  indexProdMapForm.setVendorProdSkuList(productMapping.size() > 0 ? productMapping.get(original_floral_product_sku) : null );
    			  indexProdMapForm.setFloralSKU(original_floral_product_sku);   
    			  disabled = new Boolean(true); //disable the floral sku and price points fields
    			  request.setAttribute("action_type","update");
    			  
    			  if(request.getParameter("prod_type") != null && request.getParameter("prod_type").equals("new"))
    			  {
    				  if ( allProductMappings.get(original_floral_product_sku) != null && allProductMappings.get(original_floral_product_sku).size() > 0)
    				  {
    					  errorMessageList = new ArrayList<String>();
    					  errorMessageList.add(OPMConstants.FLORIST_SKU_ALREADY_EXISTS_ON_FILE_ERROR_MSG);
    				  }
    				  request.setAttribute("errorMessage",errorMessageList);    
    			  }
    		  }
    		  else
    		  {
    			  errorMessageList = new ArrayList<String>();
    			  errorMessageList.add(OPMConstants.FLORAL_PRODUCT_NOT_IN_PDB_MSG);
    			  request.setAttribute("errorMessage",errorMessageList);
    			  
    			  indexProdMapForm.setFloralSKU(original_floral_product_sku); 
    			  request.setAttribute("action_type","load");
    		  }
    	  }
    	  catch(Exception e)
    	  {
    		  errorMessageList = new ArrayList<String>();
    		  errorMessageList.add(e.getMessage());   
    		  request.setAttribute("errorMessage",errorMessageList);
    		  
    		  indexProdMapForm.setFloralSKU(original_floral_product_sku); 
    		  request.setAttribute("action_type","load");
    	  }    	    

    	  request.setAttribute("disabled", disabled);      	
    	  request.setAttribute("allProductMappings",allProductMappings);
    	  request.setAttribute("phoenixStatus", "none");

    	  return mapping.findForward("ProductMappingMaintenance");  
      }      
      else if (requestActionType.equalsIgnoreCase("exit"))
      {
    	  forward = mapping.findForward("MainMenuAction");
          logger.info("forward: " + forward);
          return forward;    	  
      }      
      //If the action is not found as above, throw a new exception
      else
      {
        throw new Exception("Invalid Action Type - Please correct");
      }      

    }
	catch (Exception e)
	{
      logger.error(e);
      forward = mapping.findForward("ErrorPage");
      return forward;
    }finally{
    	if(conn!=null){
    		try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
  }


  //Methods for processing request - PROREQ#
  /*******************************************************************************************
   * getRequestInfo(HttpServletRequest request)
   ******************************************************************************************
   * Retrieve the info from the request object
   * @param request  
   * @return HashMap
   * @throws Exception
   */
  private HashMap getRequestInfo(HttpServletRequest request)
  throws Exception
  {	
	  HashMap _requestInfoMap = new HashMap();

	  //****retrieve the action type
	  if (request.getParameter("action_type") != null)
		  _requestInfoMap.put("requestActionType", request.getParameter("action_type"));

	  //****retrieve the adminAction
	  if (request.getParameter("adminAction") != null)
		  _requestInfoMap.put("requestAdminAction", request.getParameter("adminAction"));

	  //****retrieve the context
	  if (request.getParameter("context") != null)
		  _requestInfoMap.put("requestContext", request.getParameter("context"));

	  //****retrieve the selected_context
	  if (request.getParameter("selected_context") != null)
		  _requestInfoMap.put("requestSelectedContext", request.getParameter("selected_context"));

	  //****retrieve the context
	  if (request.getParameter("securitytoken") != null)
		  _requestInfoMap.put("requestSessionId", request.getParameter("securitytoken"));

	  return _requestInfoMap;
  }
   
   
   
   /*******************************************************************************************
    * getConfigInfo()
    ******************************************************************************************
    * Retrieve the info from the configuration file
    * @param requestInfoMap 
    * @return String
    * @throws Exception
    */
    private String getConfigInfo(HashMap requestInfoMap)
      throws Exception
    {
    	String _requestSessionId = null;
    	String _csrId = null;
    	
    	if(requestInfoMap.containsKey("requestSessionId")){
    		_requestSessionId = (String) requestInfoMap.get("requestSessionId");
    	}
    	/* retrieve user information */
	      if (SecurityManager.getInstance().getUserInfo(_requestSessionId) != null)
	      {
	    	  _csrId = SecurityManager.getInstance().getUserInfo(_requestSessionId).getUserID();
	      }
	      
	      return _csrId;
    }
    
  //! End of methods - PROREQ#  
    
    private List<String> validate(ProductMappingMaintenanceForm prodMapForm)
    {
    	List<String> errorMsgList = null;
    	if(prodMapForm != null)
    	{
    		errorMsgList = new ArrayList<String>();
    		if(prodMapForm.getFloralSKU() == null || prodMapForm.getFloralSKU().trim().length() == 0)
    		{
    			errorMsgList.add(OPMConstants.FLORAL_PRODUCT_NOT_NULL_MSG);
    		}
    		else if(prodMapForm.getFloralSKU() != null && prodMapForm.getFloralSKU().trim().length() > 0)
    		{
    			 List<BaseProductMappingVO> prodMapList = prodMapForm.getVendorProdSkuList();
    			 if( prodMapList == null || prodMapList.size() == 0 )
    			 {
    				 errorMsgList.add(OPMConstants.FLORAL_PRODUCT_NOT_IN_PDB_MSG);
    			 }    			 
    		}
    	}
    	return errorMsgList;    	
    }
   
}
