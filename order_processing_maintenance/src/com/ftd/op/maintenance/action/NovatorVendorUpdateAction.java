package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.exception.NovatorTransmissionException;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.NovatorVendorTransmission;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;


/**
 * NovatorVendorUpdateAction
 *
 * Sends Vendor updates to Novator.
 *
 */
public class NovatorVendorUpdateAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.op.maintenance.action.NovatorVendorUpdateAction");
    private static final String MAIN_MENU_ACTION = "MainMenuAction";
    private static final String ERROR_PAGE = "ErrorPage";


    /**
     * Routes the request based on action_type.
     * 
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response) 
        throws IOException, ServletException 
    {
        ActionForward forward = null;
        String actionType = null;
        Connection conn = null;

        try 
        {
          //obtain database connection
          conn = CommonUtil.getDBConnection();

          actionType = request.getParameter("action_type");
          
          //if action_type is display load the page
          if(actionType == null || actionType.equals("display"))
          {
              processDisplay(mapping, form, request, response);
          }
          //if action_type is update use input from the page to update Vendor
          //information at Novator
          else if(actionType == null || actionType.equals("update"))
          {
              processUpdate(mapping, form, request, response, conn);
          }
          else if(actionType == null || actionType.equals("MainMenuAction"))
          {
              forward = performMainMenu(mapping, form, request, response);
          }
        }
        catch (Throwable  t)
        {
            logger.error(t);
            forward = mapping.findForward(ERROR_PAGE);
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }    
        
        //forward will be null unless an error occurs
        return forward;
    }

    /**
     * Obtains necessary information for display and handles the transformation
     * of the NovatorVendorUpdate page.
     * 
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws IOException
     * @throws ServletException
     * @throws Exception
    */
    public void processDisplay(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException, Exception
    {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

        //used for page data information
        HashMap pageData = new HashMap();
        
        // This wil set the admin action so that the page returns to the correct menu
        pageData.put("adminAction", request.getParameter("adminAction"));

        NovatorFeedUtil feedUtil = new NovatorFeedUtil();
        Document updateSetupDoc = feedUtil.getNovatorEnvironmentSetup();
  
        //convert the page data hashmap to XML and append it to the final XML
        Document returnDocument = DOMUtil.getDocument();
        DOMUtil.addSection(returnDocument,updateSetupDoc.getChildNodes());
        DOMUtil.addSection(returnDocument, "pageData", "data", pageData, true);

        //transform page
        TraxUtil.getInstance().transform(request, response, returnDocument, XMLUtil.getXSL(mapping,"UpdateNovatorVendorXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute(OPMConstants.CONS_APP_PARAMETERS));        
    }
    
    
    /**
     * Updates Novator Vendors based on input from the page and returns back
     * 'success' or error text.
     * 
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws IOException
     * @throws ServletException
     * @throws Exception
    */
    public void processUpdate(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response, Connection conn)
        throws IOException, ServletException, Exception 
    {
        String server = null;
        String port = null;
        StringBuffer responseText = new StringBuffer ("\n");

        NovatorUpdateVO vo = new NovatorUpdateVO();
        
        try 
        {
            //obtain vendorId to update
            String vendorId = request.getParameter("vendorId");
            
            
            VendorDAO vendorDAO = new VendorDAO(conn);
            
            //obtain vendor block information
            ArrayList vendorBlocks = (ArrayList)vendorDAO.getVendorBlocksList(vendorId);
            VendorMasterVO vendorVO = null;
            try
            {
                //obtain vendor information
                vendorVO = vendorDAO.getVendorMaster(vendorId); 
            }
            catch(Exception e)
            {
                responseText.append("Vendor id could not be found.\n");  
                throw(e);
            }
            
            //send to update information to Novator
            logger.info("Sending vendor " + vendorId);
            
            
            //if a vendorId exists update
            if (vendorId != null && vendorId.length() > 0) {
            
                vo = buildNovatorUpdateVO(request);
   
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            
                if (vo.isUpdateLive()) {
                  try {
                      server = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_LIVE_IP_KEY);
                      port = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_LIVE_PORT_KEY);
                      
                    logger.debug("Sending update to Live server"  + server + " " + port);
         
                    new NovatorVendorTransmission().send(vendorVO, vendorBlocks, server, port);
                    responseText.append("Live: Success\n");
                    logger.debug("Sent update to Live server\n");
                    
                  }
                  catch(NovatorTransmissionException nte) {
                  
                        logger.error(nte);
                        logger.error(nte.getMessage());
                        responseText.append("Live :Error\n");  
                        
                  }
                  catch(Exception e)
                  {
                       logger.error(e);
                       responseText.append("Live: Error\n");  
                  }
                  
                }

                if (vo.isUpdateTest()) {
                  try {
                    server = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_TEST_IP_KEY);
                    port = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_TEST_PORT_KEY);
                      
                    logger.debug("Sending update to Test server"  + server + " " + port);
     
                    new NovatorVendorTransmission().send(vendorVO, vendorBlocks, server, port);
                    responseText.append("Test: Success\n");
                    logger.debug("Sucessfully sent update to Test server");
                  }
                  catch(NovatorTransmissionException nte) {    
                      logger.error(nte);
                      logger.error(nte.getMessage());
                      responseText.append("Test :Error\n");  
                      
                  }
                    catch(Exception e)
                    {
                        logger.error(e);
                        responseText.append("Test: Error\n");  
                    }

                }
                if (vo.isUpdateContent()) {
                  try { 
                    server = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_CONTENT_IP_KEY);
                    port = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_CONTENT_PORT_KEY);
                      
                    logger.debug("Sending update to Content server" + server + " " + port);
    
                    new NovatorVendorTransmission().send(vendorVO, vendorBlocks, server, port);
                    responseText.append("Content: Success\n");
                    logger.debug("Sucessfully sent update to Content server");
                  }
                    catch(NovatorTransmissionException nte) {    
                        logger.error(nte);
                        logger.error(nte.getMessage());
                        responseText.append("Content :Error\n");  
                        
                    }
                      catch(Exception e)
                      {
                          logger.error(e);
                          responseText.append("Content: Error\n");  
                      }
                }   

                if (vo.isUpdateUAT()) {
                  try {
                    server = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_UAT_IP_KEY);
                    port = configUtil.getFrpGlobalParm(OPMConstants.NOVATOR_CONFIG,OPMConstants.NOVATOR_UAT_PORT_KEY);
                      
                    logger.debug("Sending update to UAT server " + server + " " + port);

                    new NovatorVendorTransmission().send(vendorVO, vendorBlocks, server, port);
                    responseText.append("UAT: Success\n");
                    logger.debug("Sucessfully sent update to UAT server");
                  }
                    catch(NovatorTransmissionException nte) {    
                        logger.error(nte);
                        logger.error(nte.getMessage());
                        responseText.append("UAT :Error\n");  
                        
                    }
                      catch(Exception e)
                      {
                          logger.error(e);
                          responseText.append("UAT: Error\n");  
                      }
                }
 
            } 

            else 
            {
                responseText.append("A vendor id was not specified");
            }
        }
        catch(NovatorTransmissionException nte)
        {
            logger.error(nte);
            responseText.append("Error occurred.  " + nte.getMessage());  
        }
        catch(Exception e)
        {
            logger.error(e);
            responseText.append("Error occurred while processing vendor update.");  
        }

        //write response text
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println(responseText);
        out.flush();  
    }

    private ActionForward  performMainMenu (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    {

        logger.debug("Calling performMainMenu");
 
        ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
        return newForward;
    }
    
    /**
     * This function takes input from a HttpServletRequest and returns a NovatorUpdateVO based on the information given.
     * @param request                   a HttpServletRequest
     * @return                          NovatorUpdateVO 
     */
    
    private NovatorUpdateVO buildNovatorUpdateVO(HttpServletRequest request)
    {
        NovatorUpdateVO vo = new NovatorUpdateVO();
        String updateContent = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
        if (updateContent != null && updateContent.equals("true"))
        {
            vo.setUpdateContent(true);
        }
        String updateLive = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
        if (updateLive != null && updateLive.equals("true"))
        {
            vo.setUpdateLive(true);
        }
        String updateTest = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_TEST);
        if (updateTest != null && updateTest.equals("true"))
        {
            vo.setUpdateTest(true);
        }
        String updateUAT = request.getParameter(OPMConstants.PARAMETER_NOVATOR_UPDATE_UAT);

        if (updateUAT != null && updateUAT.equals("true"))
        {
            vo.setUpdateUAT(true);
        }

        logger.debug("updateContent:" + vo.isUpdateContent());
        logger.debug("updateLive:" + vo.isUpdateLive());
        logger.debug("updateTest:" + vo.isUpdateTest());
        logger.debug("updateUAT:" + vo.isUpdateUAT());
        
        return vo;
    }    
    
    
}
