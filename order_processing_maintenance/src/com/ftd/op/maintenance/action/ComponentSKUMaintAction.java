package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.vo.ComponentSkuVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.text.DecimalFormat;

import java.text.SimpleDateFormat;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * ComponentSKUMaintAction class 
 * 
 * The “Component SKU Maintenance” screen allows a user to view/add/edit the Component SKU's 
 * that are associated with Product Database Maintenance
 * 
 */
public final class ComponentSKUMaintAction extends Action 
{
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.ComponentSKUMaintAction");
  
  //request xsl filename
  private static final String COMPONENT_SKU_LIST_XSL = "ComponentSKUListXSL";
  private static final String COMPONENT_SKU_MAINT_XSL = "ComponentSKUMaintXSL";
  
  //config:  file names
  public static final String PROPERTY_FILE = "maintenance-config.xml";
  private static String DATASOURCE_NAME = "CLEAN";
  
  //application:
  public static final String APP_PARAMETERS = "parameters";
  
  //application:  actions
  public static final String ACTN_ERROR = "Error";
  
  //request parameters
  public static final String ACTION_TYPE = "action_type";
  public static final String ADMIN_ACTION = "admin_action";
  public static String adminAction = "";
  public static String inComponentSku = "";
  
  //action types
  public static final String  LOAD_COMPONENT_SKU_LIST_ACTION = "loadComponentSkuList";
  public static final String  LOAD_COMPONENT_SKU_ACTION = "loadComponentSku";
  public static final String  SAVE_COMPONENT_SKU_ACTION = "saveComponentSku";
  private static final String MAIN_MENU_ACTION = "MainMenuAction";
  
  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";
  public static final String UPDATE_ALLOWED = "update_allowed";
  
  //Security
  public static final String SECURITY_TOKEN = "securitytoken";
  public static final String CONTEXT = "context";
     
  /**
  This is the Component SKU Maintenance Action called from the Struts framework.
  * 
  * @param mapping            - ActionMapping used to select this instance
  * @param form               - ActionForm (optional) bean for this request
  * @param request            - HTTP Request we are processing
  * @param response           - HTTP Response we are processing
  * @return forwarding action - next action to "process" request
  * @throws IOException
  * @throws ServletException
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException
  {
    String actionType = null;
    adminAction = "";
  
    /* get action type request parameter */
    if(request.getParameter(ACTION_TYPE)!=null) {
      actionType = request.getParameter(ACTION_TYPE);
    }
    else {
      actionType = this.LOAD_COMPONENT_SKU_LIST_ACTION;
    }
    logger.debug("action: " + actionType);
  
    /* Component SKU actionss */
    // load the component sku list
    if(actionType.equals(LOAD_COMPONENT_SKU_LIST_ACTION)) {             
        return performLoadComponentSkuList(mapping, form, request, response);
    }
    // load component sku data by id
    else if(actionType.equals(LOAD_COMPONENT_SKU_ACTION)) { 
        return performLoadComponentSku(mapping, form, request, response);        
    }
    // save component sku data
    else if(actionType.equals(SAVE_COMPONENT_SKU_ACTION)) { 
      return performSaveComponentSku(mapping, form, request, response);        
    }
    // return to main menu
    else if(actionType.equals(MAIN_MENU_ACTION)) {
      return performMainMenu(mapping);
    }
    
    return null;
  }

    private ActionForward performLoadComponentSkuList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
    {
      Document xmlDoc  = null;
      File xslFile = null;
      Connection conn = null;

      try
      {
        conn = this.getDBConnection();
        MaintenanceDAO dao = new MaintenanceDAO(conn);
        /* pull base document */
        xmlDoc = DOMUtil.getDocument();    
        
        /* get security token */
        String sessionId = request.getParameter(SECURITY_TOKEN);
        String context = request.getParameter(CONTEXT);

        if(sessionId==null)
        {
            logger.error("Security token not found in request object");
        }
          
        /* retrieve user information */
        String userId = null;
        if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
            userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }

        /* retrieve component sku data and add it to root DOM */
        Document componentSkuDoc = dao.getComponentSkuList(null);
        DOMUtil.addSection(xmlDoc,componentSkuDoc.getChildNodes()); 

        /* build page data */
        HashMap pageData = new HashMap();
      
        /* check if user has access to add service fees */
        boolean updateAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"ComponentSkuData","Update");
        if(updateAllowed) {
            pageData.put(UPDATE_ALLOWED, "Y"); 
        } else {
            pageData.put(UPDATE_ALLOWED, "N");
        }

        pageData.put(ADMIN_ACTION, adminAction);
        pageData.put("component_sku_id", inComponentSku);
        
        /* convert the page data hashmap to XML and append it to the final XML */
        DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
        StringWriter sw = new StringWriter();
        DOMUtil.print(xmlDoc, new PrintWriter(sw));
        logger.debug(sw.toString());

        /* forward to addOccasionPopupXSL action */
        xslFile = getXSL(COMPONENT_SKU_LIST_XSL,mapping);
        
        HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
        TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
         
      } catch (Exception e) 
      {
        logger.error(e);
        return mapping.findForward(ACTN_ERROR);
      } finally 
      {
        try {
          conn.close();
        } catch (Exception e) 
        {
          logger.error(e);
        }
      }
      return null;
      
    }

  private ActionForward performLoadComponentSku(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
      Document xmlDoc  = null;
      File xslFile = null;
      Connection conn = null;

      try
      {
          conn = this.getDBConnection();
          MaintenanceDAO dao = new MaintenanceDAO(conn);
          /* pull base document */
          xmlDoc = DOMUtil.getDocument();    

          /* get security token */
          String sessionId = request.getParameter(SECURITY_TOKEN);
          String context = request.getParameter(CONTEXT);

          if(sessionId==null)
          {
              logger.error("Security token not found in request object");
          }

          /* retrieve user information */
          String userId = null;
          if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
          {
              userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
          }

          String addEditFlag = request.getParameter("add_edit_flag");
          logger.debug("Add/Edit: " + addEditFlag);
          String componentSkuId = request.getParameter("component_sku_id");
          logger.debug("componentSku: " + componentSkuId);
    
          if (addEditFlag != null && addEditFlag.equalsIgnoreCase("edit")) {
              /* retrieve component sku data and add it to root DOM */
              Document componentSkuDoc = dao.getComponentSkuList(componentSkuId);
              DOMUtil.addSection(xmlDoc,componentSkuDoc.getChildNodes());
          }

          /* build page data */
          HashMap pageData = new HashMap();
    
          /* check if user has access to add service fees */
          boolean updateAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"ComponentSkuData","Update");
          if(updateAllowed) {
              pageData.put(UPDATE_ALLOWED, "Y"); 
          } else {
              pageData.put(UPDATE_ALLOWED, "N");
          }

          pageData.put(ADMIN_ACTION, adminAction);
          pageData.put("component_sku_id", inComponentSku);
          pageData.put("add_edit_flag", addEditFlag);
          pageData.put("idList", request.getParameter("idList"));

          /* convert the page data hashmap to XML and append it to the final XML */
          DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
          StringWriter sw = new StringWriter();
          DOMUtil.print(xmlDoc, new PrintWriter(sw));
          logger.debug(sw.toString());

          /* forward to addOccasionPopupXSL action */
          xslFile = getXSL(COMPONENT_SKU_MAINT_XSL,mapping);
      
          HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
          TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
       
      } catch (Exception e) 
      {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
      } finally 
      {
          try {
              conn.close();
          } catch (Exception e) 
          {  
              logger.error(e);
          }
      }
      return null;
    
  }

  private ActionForward performSaveComponentSku(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Connection conn = null;
    adminAction = "saveFailed";
    String userId = "";

    try {
        /* get security token */
        String sessionId = request.getParameter(SECURITY_TOKEN);
        //sessionId = "FTD_GUID_14927453150132425541209236743340-7803816000-31193282101013728928021446202440-267605821-555169505020457718590-11523992201138878281814443913745611721807708215-973868771232-1391863293159";
        /* retrieve user information */
        if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
          userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }

        conn = this.getDBConnection();
        MaintenanceDAO dao = new MaintenanceDAO(conn);

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("0.00");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        
        ComponentSkuVO componentSku = new ComponentSkuVO();
        componentSku.setComponentSkuId(request.getParameter("component_sku_id"));
        inComponentSku = componentSku.getComponentSkuId();
        
        String tempDate = null;
        String tempCost = request.getParameter("base_cost");
        if (tempCost != null && !tempCost.equals("")) {
            logger.debug("base_cost: " + tempCost);
            componentSku.setBaseCost(new Double(tempCost));
            tempDate = request.getParameter("base_cost_eff_date");
            if (tempDate != null && !tempDate.equals("")) {
                logger.debug("base_cost_eff_date: " + tempDate);
                componentSku.setBaseCostEffectiveDate(sdf.parse(tempDate));
            }
        }

        tempCost = request.getParameter("qa_cost");
        if (tempCost != null && !tempCost.equals("")) {
            logger.debug("qa_cost: " + tempCost);
            componentSku.setQaCost(new Double(tempCost));
            tempDate = request.getParameter("qa_cost_eff_date");
            if (tempDate != null && !tempDate.equals("")) {
                logger.debug("qa_cost_eff_date: " + tempDate);
                componentSku.setQaCostEffectiveDate(sdf.parse(tempDate));
            }
        }
        
        tempCost = request.getParameter("freight_in_cost");
        if (tempCost != null && !tempCost.equals("")) {
            logger.debug("freight_in_cost: " + tempCost);
            //componentSku.setFreightInCost(new Double(df.format(tempCost)));
            componentSku.setFreightInCost(new Double(tempCost));
            tempDate = request.getParameter("freight_in_cost_eff_date");
            if (tempDate != null && !tempDate.equals("")) {
                logger.debug("freight_in_cost_eff_date: " + tempDate);
                componentSku.setFreightInCostEffectiveDate(sdf.parse(tempDate));
            }
        }
        
        componentSku.setComponentSkuName(request.getParameter("component_sku_name"));
        componentSku.setUpdatedBy(userId);
        componentSku.setAvailableFlag(request.getParameter("available_flag"));
        componentSku.setCommentText(request.getParameter("comment_text"));
        
        logger.debug("Saving " + componentSku.getComponentSkuId()
            + " " + componentSku.getComponentSkuName()
            + " " + componentSku.getAvailableFlag()
            + " " + componentSku.getUpdatedBy());

        dao.updateComponentSku(componentSku);
        adminAction = "saveSuccessful";

        /* forward to the load component sku action */
        return performLoadComponentSkuList(mapping, form, request, response);
    } catch (Exception e) 
    {
        logger.error(e);
        return mapping.findForward(ACTN_ERROR);
    } finally 
    {
        try {
            conn.close();
        } catch (Exception e) 
        {
            logger.error(e);
        }
    }

  }

  private ActionForward  performMainMenu(ActionMapping mapping)
  {
    try {
      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
      return newForward;
    } catch(Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    }
  }

  /**
  * get XSL file
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
    
  /**
  * Obtain connectivity with the database
  */
  private Connection getDBConnection()
      throws IOException, ParserConfigurationException, SAXException, TransformerException, 
          Exception
  {
      Connection conn = null;
      conn = DataSourceUtil.getInstance().getConnection(
          ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE, DATASOURCE_NAME));

      return conn;
  }  
}