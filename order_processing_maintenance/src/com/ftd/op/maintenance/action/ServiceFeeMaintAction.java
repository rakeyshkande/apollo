package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.vo.ServiceFeeVO;
import com.ftd.op.maintenance.vo.ServiceFeeOverrideVO;
import com.ftd.op.maintenance.util.NovatorServiceFeeTransmission;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.text.DecimalFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * ServiceFeeMaintAction class 
 * 
 * The “Service Fee Maintenance” screen allows a user to view/add/edit the service fees and 
 * override dates available on the source code database with FTD.COM.
 * 
 */
public final class ServiceFeeMaintAction extends Action 
{
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.ServiceFeeMaintAction");
  
  //request xsl filename
  private static final String SERVICE_FEE_MAINT_XSL = "ServiceFeeMaintXSL";
  private static final String SERVICE_FEE_OVERRIDE_MAINT_XSL = "ServiceFeeOverrideMaintXSL";
  private static final String SOURCE_CODES_XSL = "SourceCodesXSL";
  private static final String SERVICE_FEE_HISTORY_XSL = "ServiceFeeHistoryXSL";
  
  //config:  file names
  public static final String PROPERTY_FILE = "maintenance-config.xml";
  private static String DATASOURCE_NAME = "CLEAN";
  
  //application:
  public static final String APP_PARAMETERS = "parameters";
  
  //application:  actions
  public static final String ACTN_ERROR = "Error";
  
  //request parameters
  public static final String ACTION_TYPE = "action_type";
  public static final String ADMIN_ACTION = "admin_action";
  public static String adminAction = "";
  public static String inServiceFee = "";
  public static String overrideDate = "";
  public static String dateRangeFlag = "";
  
  //action types
  public static final String  LOAD_SERVICE_FEE_ACTION = "loadServiceFee";
  public static final String  DELETE_SERVICE_FEE_ACTION = "deleteServiceFee";
  public static final String  SAVE_SERVICE_FEE_ACTION = "saveServiceFee";
  public static final String  GET_OVERRIDE_LIST_ACTION = "getOverrideList";
  public static final String  DELETE_SERVICE_FEE_OVERRIDE_ACTION = "deleteServiceFeeOverride";
  public static final String  SAVE_SERVICE_FEE_OVERRIDE_ACTION = "saveServiceFeeOverride";
  public static final String  GET_SOURCE_CODE_LIST_ACTION = "getSourceCodeList";
  public static final String  GET_HISTORY_ACTION = "getHistory";
  private static final String MAIN_MENU_ACTION = "MainMenuAction";
  
  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";
  public static final String VALID_FLAG = "valid_flag";
  public static final String ERROR_VALUE = "error_value";
  public static final String UPDATE_ALLOWED = "update_allowed";
  public static final String LOCK_VALUE = "lock_value";
  public static final String LOCK_FLAG = "lock_flag";
  
  //Security
  public static final String SECURITY_TOKEN = "securitytoken";
  public static final String CONTEXT = "context";
     
  /**
  This is the Service Fee Maintenance Action called from the Struts framework.
  * 
  * @param mapping            - ActionMapping used to select this instance
  * @param form               - ActionForm (optional) bean for this request
  * @param request            - HTTP Request we are processing
  * @param response           - HTTP Response we are processing
  * @return forwarding action - next action to "process" request
  * @throws IOException
  * @throws ServletException
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException
  {
    String actionType = null;
    adminAction = "";
    dateRangeFlag = "";
  
    /* get action type request parameter */
    if(request.getParameter(ACTION_TYPE)!=null) {
      actionType = request.getParameter(ACTION_TYPE);
    }
    else {
      actionType = this.LOAD_SERVICE_FEE_ACTION;
    }
    logger.debug("action: " + actionType);
  
    /* Service Fee actionss */
    if(actionType.equals(LOAD_SERVICE_FEE_ACTION)) {             
        return performLoadServiceFee(mapping, form, request, response);
    }
    // Delete selected service fees
    else if(actionType.equals(DELETE_SERVICE_FEE_ACTION)) {
      return performDeleteServiceFee(mapping, form, request, response);
    } 
    // save service fee data
    else if(actionType.equals(SAVE_SERVICE_FEE_ACTION)) { 
      return performSaveServiceFee(mapping, form, request, response);        
    }
    // get service fee override list
    else if(actionType.equals(GET_OVERRIDE_LIST_ACTION)) {
        return performLoadServiceFeeOverride(mapping, form, request, response);
    }
    // Delete selected service fee overrides
    else if(actionType.equals(DELETE_SERVICE_FEE_OVERRIDE_ACTION)) {
      return performDeleteServiceFeeOverride(mapping, form, request, response);
    } 
    // save service fee data
    else if(actionType.equals(SAVE_SERVICE_FEE_OVERRIDE_ACTION)) { 
      return performSaveServiceFeeOverride(mapping, form, request, response);        
    }
    // get source codes by service fee
    else if(actionType.equals(GET_SOURCE_CODE_LIST_ACTION)) {
      return performLoadSourceCodes(mapping, form, request, response);
    }
    // get history
     else if(actionType.equals(GET_HISTORY_ACTION)) {
       return performLoadHistory(mapping, form, request, response);
     }
    // return to main menu
    else if(actionType.equals(MAIN_MENU_ACTION)) {
      return performMainMenu(mapping, form, request, response);
    }
    
    return null;
  }

  private ActionForward performLoadServiceFee(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Document xmlDoc  = null;
    File xslFile = null;
    Connection conn = null;
    
    try
    {
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      /* pull base document */
      xmlDoc = (Document) DOMUtil.getDocument();    
      
      /* get security token */
      String sessionId = request.getParameter(SECURITY_TOKEN);
      String context = request.getParameter(CONTEXT);

      if(sessionId==null)
      {
          logger.error("Security token not found in request object");
      }
        
      /* retrieve user information */
      String userId = null;
      if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
      {
          userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
      }
      
      String selectedSortField = request.getParameter("sort_field");
      String sortField = null;
      if(selectedSortField == null || selectedSortField.length() == 0) {
        sortField = OPMConstants.DEFAULT_SORT_FIELD;
        selectedSortField = OPMConstants.DEFAULT_SORT_FIELD;
      } else {
        sortField = selectedSortField + "," + OPMConstants.DEFAULT_SORT_FIELD;
      }

      /* retrieve service fee data and add it to root DOM */
      Document serviceFeeDoc = (Document)dao.getServiceFeeList(sortField);
      DOMUtil.addSection(xmlDoc,serviceFeeDoc.getChildNodes()); 
      
      DOMUtil.addSection(xmlDoc,this.addSortFieldList().getChildNodes());

      /* build page data */
      HashMap pageData = new HashMap();
    
      /* check if user has access to add service fees */
      boolean updateAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"SourceSnhData","Update");
      if(updateAllowed) {
          pageData.put(UPDATE_ALLOWED, "Y"); 
      } else {
          pageData.put(UPDATE_ALLOWED, "N");
      }

      pageData.put(ADMIN_ACTION, adminAction);
      pageData.put("serviceFeeId", inServiceFee);
      pageData.put("overrideDate", overrideDate);
      pageData.put("dateRangeFlag", dateRangeFlag);
      pageData.put("sortField", selectedSortField);
      /* convert the page data hashmap to XML and append it to the final XML */
      DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
      StringWriter sw = new StringWriter();
      DOMUtil.print(xmlDoc, new PrintWriter(sw));
      //logger.info(sw.toString());

      /* forward to addOccasionPopupXSL action */
      xslFile = getXSL(SERVICE_FEE_MAINT_XSL,mapping);
      
      HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
      parameters.put(SECURITY_TOKEN, sessionId);
      parameters.put(CONTEXT, context);
      TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
       
    } catch (Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        logger.error(e);
      }
    }
    return null;
    
  }
  
  private Document addSortFieldList() throws ParserConfigurationException {
      Document doc = DOMUtil.getDefaultDocument();
      Element root = JAXPUtil.buildSimpleXmlNode(doc, "sort_fields", "");
      doc.appendChild(root);
      
      Element fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      Element idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "SNH_ID");
      fieldElement.appendChild(idElement);
      Element descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "ID");
      fieldElement.appendChild(descElement);      
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "DESCRIPTION");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Description");
      fieldElement.appendChild(descElement);    
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "FIRST_ORDER_DOMESTIC");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Domestic (aka Florist) Charge");
      fieldElement.appendChild(descElement);
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "SAME_DAY_UPCHARGE");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Same Day Upcharge");
      fieldElement.appendChild(descElement); 
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "SAME_DAY_UPCHARGE_FS");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Same Day Upcharge FS Members");
      fieldElement.appendChild(descElement); 
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "FIRST_ORDER_INTERNATIONAL");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "International Charge");
      fieldElement.appendChild(descElement);   
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "VENDOR_CHARGE");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Vendor Charge");
      fieldElement.appendChild(descElement);   
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "VENDOR_SAT_UPCHARGE");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Vendor Saturday Upcharge");
      fieldElement.appendChild(descElement); 
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "VENDOR_SUN_UPCHARGE");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Vendor Sunday Upcharge");
      fieldElement.appendChild(descElement); 
      
      fieldElement = JAXPUtil.buildSimpleXmlNode(doc,"sort_field", "");
      root.appendChild(fieldElement);
      idElement = JAXPUtil.buildSimpleXmlNode(doc,"id", "VENDOR_MON_UPCHARGE");
      fieldElement.appendChild(idElement);
      descElement = JAXPUtil.buildSimpleXmlNode(doc,"description", "Vendor Monday Upcharge");
      fieldElement.appendChild(descElement); 
      
      return doc;
  }

  private ActionForward performSaveServiceFee(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Connection conn = null;
    adminAction = "saveFailed";
    String userId = "";

    try {
        /* get security token */
        String sessionId = request.getParameter(SECURITY_TOKEN);
        //sessionId = "FTD_GUID_14927453150132425541209236743340-7803816000-31193282101013728928021446202440-267605821-555169505020457718590-11523992201138878281814443913745611721807708215-973868771232-1391863293159";
        /* retrieve user information */
        if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
          userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }

        conn = this.getDBConnection();
        MaintenanceDAO dao = new MaintenanceDAO(conn);

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("0.00");
        
        ServiceFeeVO serviceFee = new ServiceFeeVO();
        serviceFee.setServiceFeeId(request.getParameter("serviceFeeId"));
        inServiceFee = serviceFee.getServiceFeeId();
        
        double domFee = new Double(request.getParameter("domestic_fee"));
        serviceFee.setDomesticFee(new Double(df.format(domFee)));
        double sameDayUpcharge = new Double(request.getParameter("same_day_upcharge"));
        serviceFee.setSameDayUpcharge(new Double(df.format(sameDayUpcharge)));
        double sameDayUpchargeFS = new Double(request.getParameter("same_day_upcharge_fs"));
        serviceFee.setSameDayUpchargeFS(new Double(df.format(sameDayUpchargeFS)));
        double intFee = new Double(request.getParameter("international_fee"));
        serviceFee.setInternationalFee(new Double(df.format(intFee)));
        double vendorCharge = new Double(request.getParameter("vendor_charge"));
        serviceFee.setVendorCharge(new Double(df.format(vendorCharge)));
        double vendorSatUpcharge = new Double(request.getParameter("vendor_sat_upcharge"));
        serviceFee.setVendorSatUpcharge(new Double(df.format(vendorSatUpcharge)));  
        
        serviceFee.setVendorSunUpcharge(new Double(request.getParameter("vendor_sun_upcharge")));
        serviceFee.setVendorMonUpcharge(new Double(request.getParameter("vendor_mon_upcharge")));     
        
        //serviceFee.setDescription("$" + df.format(domFee) + "/$" + df.format(intFee));
        serviceFee.setDescription(request.getParameter("description"));
        serviceFee.setUpdatedBy(userId);
        serviceFee.setRequestedBy(request.getParameter("requested_by"));
        
        logger.debug("Sending to Novator");
        NovatorServiceFeeTransmission nsft = new NovatorServiceFeeTransmission();
        
        String novatorResponse = "";
        try {
            novatorResponse = nsft.sendServiceFee(serviceFee, "Add");
        } catch (Exception e) {
            logger.error("exception: " + e);
            adminAction = "novatorConnect";
        }
       if (novatorResponse != null && novatorResponse.equalsIgnoreCase("CON")) {

            logger.debug("Saving " + serviceFee.getServiceFeeId()
                + " " + serviceFee.getDescription()
                + " " + serviceFee.getDomesticFee()
                + " " + serviceFee.getSameDayUpcharge()
                + " " + serviceFee.getSameDayUpchargeFS()
                + " " + serviceFee.getInternationalFee()
                + " " + serviceFee.getVendorCharge()
                + " " + serviceFee.getVendorSatUpcharge()
                + " " + serviceFee.getVendorSunUpcharge()
                + " " + serviceFee.getVendorMonUpcharge()
                + " " + serviceFee.getRequestedBy()
                + " " + serviceFee.getUpdatedBy());
            dao.updateServiceFee(serviceFee);
            adminAction = "saveSuccessful";
       } else if (novatorResponse != null && novatorResponse.equalsIgnoreCase("DEN")) {
          adminAction = "novatorDEN";
       }

        /* forward to the load occasion action */
        return performLoadServiceFee(mapping, form, request, response);
    } catch (Exception e) 
    {
        logger.error(e);
        return mapping.findForward(ACTN_ERROR);
    } finally 
    {
        try {
            conn.close();
        } catch (Exception e) 
        {
            logger.error(e);
        }
    }

  }

  private ActionForward  performDeleteServiceFee(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
      Connection conn = null;
      adminAction = "deleteFailed";
      String userId = "";
      
      try
      {
          String sessionId = request.getParameter(SECURITY_TOKEN);
          /* retrieve user information */
          if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
          {
            userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
          }

          String deleteIds = request.getParameter("deleteValues");
          inServiceFee = deleteIds;
          
          List list = new ArrayList();
          
          StringBuffer result = new StringBuffer();
          for (int i = 0; i < deleteIds.length(); i++) {
              if (deleteIds.substring(i,i+1).equals(",")) {
                  list.add(result.toString());
                  result = new StringBuffer();
              } else {
                  result.append(deleteIds.substring(i,i+1));
              }
          }
          list.add(result.toString());
          
          conn = this.getDBConnection();
          MaintenanceDAO dao = new MaintenanceDAO(conn);  

          for (int i=0; i < list.size(); i++) {
              logger.debug("Sending to Novator: " + list.get(i).toString());
              NovatorServiceFeeTransmission nsft = new NovatorServiceFeeTransmission();
              
              ServiceFeeVO serviceFee = new ServiceFeeVO();
              serviceFee.setServiceFeeId(list.get(i).toString());
              serviceFee.setDomesticFee(new Double(0.00));
              serviceFee.setSameDayUpcharge(new Double(0.00));
              serviceFee.setSameDayUpchargeFS(new Double(0.00));
              serviceFee.setInternationalFee(new Double(0.00));
              serviceFee.setVendorCharge(new Double(0.00));
              serviceFee.setVendorSatUpcharge(new Double(0.00));
              
              String novatorResponse = "";
              try {
                  novatorResponse = nsft.sendServiceFee(serviceFee, "Delete");
              } catch (Exception e) {
                  logger.error("exception: " + e);
                  adminAction = "novatorConnect";
              }
              if (novatorResponse != null && novatorResponse.equals("CON")) {
                  serviceFee.setUpdatedBy(userId);
                  String requestedBy = request.getParameter("requested_by");
                  serviceFee.setRequestedBy(requestedBy);
                  dao.deleteServiceFee(list.get(i).toString(), serviceFee);
                  adminAction = "deleteSuccessful";
             } else if (novatorResponse != null && novatorResponse.equals("DEN")) {
                  adminAction = "novatorDEN";
             }
              
          }

          return performLoadServiceFee(mapping, form, request, response);
      } catch (Exception e) 
      {
          e.printStackTrace();
          return mapping.findForward(ACTN_ERROR);
      } finally 
      {
          try {
              conn.close();
          } catch (Exception e) 
          {
              logger.error(e);
          }
      }
  }
    
  private ActionForward performLoadServiceFeeOverride(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
      Document xmlDoc  = null;
      File xslFile = null;
      Connection conn = null;
      
      String serviceFeeId = request.getParameter("serviceFeeId");
      String serviceFeeDescription = request.getParameter("serviceFeeDescription");
      String updateAllowed = request.getParameter("updateAllowed");
      String domesticFee = request.getParameter("domesticFee");
      String sameDayUpcharge = request.getParameter("sameDayUpcharge");
      String sameDayUpchargeFS = request.getParameter("sameDayUpchargeFS");
      String intlFee = request.getParameter("intlFee");
      String vendorCharge = request.getParameter("vendorCharge");
      String vendorSatUpcharge = request.getParameter("vendorSatUpcharge");
      String vendorSunUpcharge = request.getParameter("vendorSunUpcharge");
      String vendorMonUpcharge = request.getParameter("vendorMonUpcharge");
      
      try
      {
        conn = this.getDBConnection();
        MaintenanceDAO dao = new MaintenanceDAO(conn);
        /* pull base document */
        xmlDoc = (Document) DOMUtil.getDocument();    

        /* get security token */
        String sessionId = request.getParameter(SECURITY_TOKEN);
        String context = request.getParameter(CONTEXT);
        
        /* retrieve service fee data and add it to root DOM */
        Document serviceFeeDoc = (Document)dao.getServiceFeeOverrideList(serviceFeeId);
        DOMUtil.addSection(xmlDoc,serviceFeeDoc.getChildNodes()); 

        /* build page data */
        HashMap pageData = new HashMap();
      
        pageData.put(ADMIN_ACTION, adminAction);
        pageData.put("serviceFeeId", serviceFeeId);
        pageData.put("serviceFeeDescription", serviceFeeDescription);
        pageData.put("overrideDate", overrideDate);
        pageData.put("dateRangeFlag", dateRangeFlag);
        pageData.put("domesticFee", domesticFee);
        pageData.put("intlFee", intlFee);
        pageData.put("vendorCharge", vendorCharge);
        pageData.put("vendorSatUpcharge", vendorSatUpcharge);
        pageData.put("sameDayUpcharge", sameDayUpcharge);
        pageData.put("sameDayUpchargeFS", sameDayUpchargeFS);
        pageData.put("vendorSunUpcharge", vendorSunUpcharge);
        pageData.put("vendorMonUpcharge", vendorMonUpcharge);
        if (updateAllowed != null && updateAllowed.equals("N")) {
            pageData.put("update_allowed", "N");
        } else {
            pageData.put("update_allowed", "Y");
        }
          
        /* convert the page data hashmap to XML and append it to the final XML */
        DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
        StringWriter sw = new StringWriter();
        DOMUtil.print(xmlDoc, new PrintWriter(sw));
        //logger.debug(sw.toString());

        /* forward to addOccasionPopupXSL action */
        xslFile = getXSL(SERVICE_FEE_OVERRIDE_MAINT_XSL,mapping);
        
        HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
        parameters.put(SECURITY_TOKEN, sessionId);
        parameters.put(CONTEXT, context);
        TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
         
      } catch (Exception e) 
      {
        logger.error(e);
        return mapping.findForward(ACTN_ERROR);
      } finally 
      {
        try {
          conn.close();
        } catch (Exception e) 
        {
          logger.error(e);
        }
      }
      return null;
      
  }

  private ActionForward performSaveServiceFeeOverride(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
      Connection conn = null;
      adminAction = "saveOverrideFailed";
      String userId = "";
      String overrideSave = null;

      try {
          /* get security token */
          String sessionId = request.getParameter(SECURITY_TOKEN);
          //sessionId = "FTD_GUID_14927453150132425541209236743340-7803816000-31193282101013728928021446202440-267605821-555169505020457718590-11523992201138878281814443913745611721807708215-973868771232-1391863293159";
          /* retrieve user information */
          if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
          {
            userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
          }

          conn = this.getDBConnection();
          MaintenanceDAO dao = new MaintenanceDAO(conn);

          SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
          DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
          df.applyPattern("0.00");
          
          ServiceFeeOverrideVO serviceFee = new ServiceFeeOverrideVO();
          serviceFee.setServiceFeeId(request.getParameter("serviceFeeId"));
          serviceFee.setOverrideDate(sdf.parse(request.getParameter("override_date")));
          overrideDate = sdf.format(serviceFee.getOverrideDate());
          
          double domFee = new Double(request.getParameter("domestic_fee"));
          serviceFee.setDomesticFee(new Double(df.format(domFee)));
          double sameDayUpcharge = new Double(request.getParameter("same_day_upcharge"));
          serviceFee.setSameDayUpcharge(new Double(df.format(sameDayUpcharge)));
          double sameDayUpchargeFS = new Double(request.getParameter("same_day_upcharge_fs"));
          serviceFee.setSameDayUpchargeFS(new Double(df.format(sameDayUpchargeFS)));
          double intFee = new Double(request.getParameter("international_fee"));
          serviceFee.setInternationalFee(new Double(df.format(intFee)));
          double vendorCharge = new Double(request.getParameter("vendor_charge"));
          serviceFee.setVendorCharge(new Double(df.format(vendorCharge)));
          double vendorSatUpcharge = new Double(request.getParameter("vendor_sat_upcharge"));
          serviceFee.setVendorSatUpcharge(new Double(df.format(vendorSatUpcharge)));   
          serviceFee.setVendorSunUpcharge(new Double(request.getParameter("vendor_sun_upcharge")));   
          serviceFee.setVendorMonUpcharge(new Double(request.getParameter("vendor_mon_upcharge")));   
          
          serviceFee.setDescription(request.getParameter("description"));
          serviceFee.setRequestedBy(request.getParameter("requestedBy"));
          serviceFee.setRequestedBy(request.getParameter("requested_by"));
          serviceFee.setUpdatedBy(userId);

          logger.debug("Sending Override to Novator");
          NovatorServiceFeeTransmission nsft = new NovatorServiceFeeTransmission();
          
          String novatorResponse = "";
          try {
              novatorResponse = nsft.sendServiceFeeOverride(serviceFee, "Add");
          } catch (Exception e) {
              logger.error("exception: " + e);
              adminAction = "novatorConnect";
          }
          if (novatorResponse != null && novatorResponse.equals("CON")) {
              overrideSave = dao.updateServiceFeeOverride(serviceFee);
              if (overrideSave != null && overrideSave.equalsIgnoreCase("DATE_RANGE")) {
                  dateRangeFlag = "Y";
              } else {
                  dateRangeFlag = "";
              }
              adminAction = "saveOverrideSuccessful";
          } else if (novatorResponse != null && novatorResponse.equals("DEN")) {
              adminAction = "novatorDEN";
          }

          /* forward to the load occasion action */
          String calledFrom = request.getParameter("calledFrom");
          logger.debug("calledFrom:" + calledFrom);
          if (calledFrom != null && calledFrom.equals("serviceFee")) {
              return performLoadServiceFee(mapping, form, request, response);
          } else {
              return performLoadServiceFeeOverride(mapping, form, request, response);
          }
      } catch (Exception e) 
      {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
      } finally 
      {
          try {
              conn.close();
          } catch (Exception e) 
          {
              logger.error(e);
          }
      }

  }

  private ActionForward  performDeleteServiceFeeOverride(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
      Connection conn = null;
      adminAction = "deleteFailed";
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      String userId = "";
       
      try
      {
          /* get security token */
          String sessionId = request.getParameter(SECURITY_TOKEN);
          /* retrieve user information */
          if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
          {
            userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
          }

          String deleteIds = request.getParameter("deleteValues");
          overrideDate = deleteIds;
          
          List list = new ArrayList();
            
          StringBuffer result = new StringBuffer();
          for (int i = 0; i < deleteIds.length(); i++) {
              if (deleteIds.substring(i,i+1).equals(",")) {
                  list.add(result.toString());
                  result = new StringBuffer();
              } else {
                  result.append(deleteIds.substring(i,i+1));
              }
          }
          list.add(result.toString());
          
          String serviceFeeId = request.getParameter("serviceFeeId");
          conn = this.getDBConnection();
          MaintenanceDAO dao = new MaintenanceDAO(conn);  

          for (int i=0; i < list.size(); i++) {
              logger.debug("Sending to Novator: " + serviceFeeId + " " + list.get(i).toString());
              NovatorServiceFeeTransmission nsft = new NovatorServiceFeeTransmission();
              
              ServiceFeeOverrideVO serviceFee = new ServiceFeeOverrideVO();
              serviceFee.setServiceFeeId(serviceFeeId);
              serviceFee.setOverrideDate(sdf.parse(list.get(i).toString()));
              serviceFee.setDomesticFee(new Double(0.00));
              serviceFee.setSameDayUpcharge(new Double(0.00));
              serviceFee.setSameDayUpchargeFS(new Double(0.00));
              serviceFee.setInternationalFee(new Double(0.00));
              serviceFee.setVendorCharge(new Double(0.00));
              serviceFee.setVendorSatUpcharge(new Double(0.00));
              serviceFee.setVendorSunUpcharge(new Double(0.00));
              serviceFee.setVendorMonUpcharge(new Double(0.00));
              
              String novatorResponse = "";
              try {
                  novatorResponse = nsft.sendServiceFeeOverride(serviceFee, "Delete");
              } catch (Exception e) {
                  logger.error("exception: " + e);
                  adminAction = "novatorConnect";
              }
             if (novatorResponse != null && novatorResponse.equals("CON")) {
                  serviceFee.setUpdatedBy(userId);
                  String requestedBy = request.getParameter("requested_by");
                  serviceFee.setRequestedBy(requestedBy);
                  dao.deleteServiceFeeOverride(serviceFeeId, list.get(i).toString(), serviceFee);
                  adminAction = "deleteSuccessful";
             } else if (novatorResponse != null && novatorResponse.equals("DEN")) {
                  adminAction = "novatorDEN";
             }
              
          }
          
          return performLoadServiceFeeOverride(mapping, form, request, response);
      } catch (Exception e) 
      {
          e.printStackTrace();
          return mapping.findForward(ACTN_ERROR);
      } finally 
      {
          try {
              conn.close();
          } catch (Exception e) 
          {
              logger.error(e);
          }
      }
  }
      
    private ActionForward performLoadSourceCodes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
    {
        Document xmlDoc  = null;
        File xslFile = null;
        Connection conn = null;
        
        String serviceFeeId = request.getParameter("id");
        logger.debug("id: " + serviceFeeId);
        
        try
        {
          conn = this.getDBConnection();
          MaintenanceDAO dao = new MaintenanceDAO(conn);
          /* pull base document */
          xmlDoc = (Document) DOMUtil.getDocument();    
          
          /* retrieve service fee data and add it to root DOM */
          Document sourceCodeDoc = (Document)dao.getSourceCodeList(serviceFeeId);
          DOMUtil.addSection(xmlDoc,sourceCodeDoc.getChildNodes()); 

          /* build page data */
          HashMap pageData = new HashMap();
        
          pageData.put(ADMIN_ACTION, adminAction);
          pageData.put("serviceFeeId", serviceFeeId);
          
          /* convert the page data hashmap to XML and append it to the final XML */
          DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
          StringWriter sw = new StringWriter();
          DOMUtil.print(xmlDoc, new PrintWriter(sw));
          //logger.debug(sw.toString());

          /* forward to addOccasionPopupXSL action */
          xslFile = getXSL(SOURCE_CODES_XSL,mapping);
          
          TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
           
        } catch (Exception e) 
        {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
        } finally 
        {
          try {
            conn.close();
          } catch (Exception e) 
          {
            logger.error(e);
          }
        }
        return null;
        
    }

    private ActionForward performLoadHistory(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
    {
        Document xmlDoc  = null;
        File xslFile = null;
        Connection conn = null;
        
        String serviceFeeId = request.getParameter("id");
        logger.debug("id: " + serviceFeeId);
        
        try
        {
          conn = this.getDBConnection();
          MaintenanceDAO dao = new MaintenanceDAO(conn);
          /* pull base document */
          xmlDoc = (Document) DOMUtil.getDocument();    
          
          /* retrieve service fee data and add it to root DOM */
          Document serviceFeeDoc = (Document)dao.getServiceFeeHistory(serviceFeeId);
          DOMUtil.addSection(xmlDoc,serviceFeeDoc.getChildNodes()); 

          /* build page data */
          HashMap pageData = new HashMap();
        
          pageData.put(ADMIN_ACTION, adminAction);
          pageData.put("serviceFeeId", serviceFeeId);
          String serviceFeeDescription = request.getParameter("serviceFeeDescription");
          pageData.put("serviceFeeDescription", serviceFeeDescription);
          
          /* convert the page data hashmap to XML and append it to the final XML */
          DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
          StringWriter sw = new StringWriter();
          DOMUtil.print(xmlDoc, new PrintWriter(sw));
          //logger.debug(sw.toString());

          /* forward to addOccasionPopupXSL action */
          xslFile = getXSL(SERVICE_FEE_HISTORY_XSL,mapping);
          
          TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
           
        } catch (Exception e) 
        {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
        } finally 
        {
          try {
            conn.close();
          } catch (Exception e) 
          {
            logger.error(e);
          }
        }
        return null;
        
    }

  private ActionForward  performMainMenu(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    try {
      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
      return newForward;
    } catch(Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    }
  }

  /**
  * get XSL file
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
    
  /**
  * Obtain connectivity with the database
  */
  private Connection getDBConnection()
      throws IOException, ParserConfigurationException, SAXException, TransformerException, 
          Exception
  {
      Connection conn = null;
      conn = DataSourceUtil.getInstance().getConnection(
          ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE, DATASOURCE_NAME));

      return conn;
  }  
}