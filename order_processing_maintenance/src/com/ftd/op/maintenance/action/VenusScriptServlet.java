package com.ftd.op.maintenance.action;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;

import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.OrderDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.MessagingServiceLocator;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.CompanyVO;
import com.ftd.op.maintenance.vo.CustomerVO;
import com.ftd.op.maintenance.vo.EmailVO;
import com.ftd.op.maintenance.vo.OrderDetailVO;
import com.ftd.op.maintenance.vo.OrderVO;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.op.maintenance.vo.TransitTimeVO;
import com.ftd.op.maintenance.vo.VenusMessageVO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.PrintWriter;
import java.io.IOException;
import org.w3c.dom.Element;


public class VenusScriptServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    private static final String PAGE_ACTION = "mode";

    private static final String POC_EMAIL_TYPE = "Email";

    private static final String ACTION_GET_ORDERS = "sql";
    private static final String ACTION_UPGRADE_SHIP_METHOD = "upgrade_shipping";
    private static final String ACTION_CHANGE_DELIVERY_DATE = "change_delivery_date";
    private static final String ACTION_CHANGE_DELIVERY_DATE_EMAIL = "change_delivery_date_email";
    private static final String ACTION_CANCEL_ORDER = "cancel_order";    
    private static final String ACTION_CANCEL_ORDER_NEW_FTD = "cancel_order_new_ftd";    
    
    private static final String RESEND_ORDER = "resend_order";
    
    private static final String PARAM_EXTERNAL_ORDER_NUMBER = "external_order_number";
    private static final String PARAM_CSR = "csr_id";
    private static final String PARAM_ENTERED_SQL = "entered_sql";
    
    private static final String CANADA_COUNTRY_CODE = "CA";
    private static final String US_COUNTRY_CODE = "US";
    
    private static final String ORDER_PROCESSING_COMMENT_ORIGIN = "OrderProc";
    private static final String COMMENT_TYPE_ORDER = "Order";  
    
    private static final String ORDER_DELIMETER = " ";
    
    private static final String DEFAULT_CSR = "SystemReprocess";
    
    private Logger logger;  
      
    /* THIS CODE IS IN VENUSSCRIPTBO. DON'T USE THIS FILE
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.op.maintenance.action.VenusScriptServlet");
    }
    


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        //get the action type
        String action = request.getParameter(PAGE_ACTION);

        Connection conn = null;    
        
        String responseOutput = null;
        
        try
        {        
            conn = CommonUtil.getDBConnection();
            
            //process action
            if(action == null)
            {
                responseOutput = ("Application Error.  Passed in action is null.");
            }
            else if(action.equalsIgnoreCase(ACTION_GET_ORDERS))
            {
                responseOutput = getOrders(conn,request);
            }
            else if(action.equalsIgnoreCase(ACTION_UPGRADE_SHIP_METHOD))
            {
                responseOutput = upgradeShipMethod(conn,request);
            }
            else if(action.equalsIgnoreCase(ACTION_CHANGE_DELIVERY_DATE))
            {
                responseOutput = changeDeliveryDate(conn,request,false);
            }
            else if(action.equalsIgnoreCase(ACTION_CHANGE_DELIVERY_DATE_EMAIL))
            {
                responseOutput = changeDeliveryDate(conn,request,true);
            }
            else if(action.equalsIgnoreCase(ACTION_CANCEL_ORDER))
            {
                responseOutput = cancelOrder(conn,request,false);
            }
            else if(action.equalsIgnoreCase(ACTION_CANCEL_ORDER_NEW_FTD))
            {
                responseOutput = cancelOrder(conn,request,true);
            }
            else
            {
                responseOutput = ("FATAL -  Unknown page action.");
            }            

        }
        catch(Throwable e)
        {
            logger.error(e);
            responseOutput = "FATAL - " + e.toString();
        }
        finally
        {
            try
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }                
            }
            catch(Exception e)
            {
                //do nothing
            }

        }

            //write out response
            PrintWriter out = response.getWriter(); 
            out.println(responseOutput);
            out.close();        

    }


     private String getOrders(Connection conn, HttpServletRequest request)
     {
        StringBuffer buffer = new StringBuffer("");
     
        try
        {
            //get values from request
            String enteredSQL = request.getParameter(PARAM_ENTERED_SQL);
        
            Statement stmt = conn.createStatement(); 
            ResultSet rs;             
  
            rs = stmt.executeQuery(enteredSQL); 
            while ( rs.next() ) { 
                String orderNumber = rs.getString("external_order_number"); 
                if(buffer.length() > 0)
                {
                    buffer.append(ORDER_DELIMETER);
                }
                buffer.append(orderNumber) ;
            } 
            
        }
        catch(SQLException e)
        {
            logger.error(e);
            return "Error with SQL. " + e.toString();
        }
        catch(Throwable t)
        {
            logger.error(t);
            return "Application error.  " + t.toString();
        }
        
        return buffer.toString();

         
     }


     private String upgradeShipMethod(Connection conn,HttpServletRequest request) throws Exception
     {
  
            boolean error = false;
         
            //needed DAOs
            OrderDAO orderDAO = new OrderDAO(conn);
            MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
            
            DeliveryDateUTIL dateUtil = new DeliveryDateUTIL();
            
            //get all the transit times
            List transitTimes = getTransitTimes();
         
            //get values from request
            String externalOrderNumber = request.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);
            String csr = request.getParameter(PARAM_CSR);
            if(csr == null)
            {
                csr = DEFAULT_CSR;
            }
            
            //get order detail id
            Map orderIdMap = orderDAO.findOrderNumber(externalOrderNumber);
            String orderDetailId = (orderIdMap.get("OUT_ORDER_DETAIL_ID")).toString();
            
            //get the order detail record
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
            
            //get the customer
            CustomerVO customerVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
                        
            //get vendor Id
            ProductVO productVO = maintDAO.getProduct(orderDetailVO.getProductId());

            //get number of days before delivery date...this saves some in deliveryDateUtil
            long diffMillis = orderDetailVO.getDeliveryDate().getTime() - (new Date()).getTime();
            int diffDays = new Long(diffMillis/(24*60*60*1000)).intValue() + 5;  

            //get Delivery Date Util parameters            
            OEDeliveryDateParm dateParam = dateUtil.getCOMParameterData(customerVO.getCountry(),orderDetailVO.getProductId(),customerVO.getZipCode(),CANADA_COUNTRY_CODE, US_COUNTRY_CODE, "1", orderDetailVO.getOccasion(),conn, orderDetailVO.getOccasion(), "FTD", false, false,diffDays);        
            
            if(orderDetailVO.getShipMethod() == null)
            {
                throw new Exception("Ship method does not exist on order.");
            }
            
            //determine current transit time
            int currentTransitTime = getTransitTime(transitTimes,orderDetailVO.getShipMethod());
            
            //check for errors
            if(currentTransitTime < 0)
            {
                throw new Exception("Transit time not found for ship method " + orderDetailVO.getShipMethod());
            }            
            
            //default to can't upgrade
            String response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Reason:Cannot upgrade ship method.";               
            
            //loop through transit times to find one that works
            boolean foundUpgrade = false;
            Iterator iter = transitTimes.iterator();
            while(!foundUpgrade && iter.hasNext())
            {
                TransitTimeVO transitVO = (TransitTimeVO)iter.next();
            
                //if current ship method is saturday, then only consider the SA ship method
                if(!orderDetailVO.getShipMethod().equalsIgnoreCase("SA") ||
                        transitVO.getShipMethod().equals("SA"))
                {
                    //only consider if transit time is less then current transit time
                    if(transitVO.getTransitTime() < currentTransitTime)
                    {
                        //get delivery date for this ship method
                        OEDeliveryDate oeDate = dateUtil.getFirstAvailableDate(dateParam,transitVO.getShipMethod());
                        
                        //if delivery date is less then or equal to current delivery date on order
                        Date firstDeliveryDate = FieldUtils.formatStringToUtilDate(oeDate.getDeliveryDate());
                        if(firstDeliveryDate.before(orderDetailVO.getDeliveryDate()) || firstDeliveryDate.equals(orderDetailVO.getDeliveryDate()))
                        {
                            //set flag to indicated ship method upgraded
                            foundUpgrade = true;
                        
                            //get ship date for new ship type
                            String shipDateString = dateUtil.getShipDate(dateParam,transitVO.getShipMethod(), oeDate.getDeliveryDate(),
                              customerVO.getState(), customerVO.getCountry(),
                              orderDetailVO.getProductId(), conn);                            
                            Date shipDate = FieldUtils.formatStringToUtilDate(shipDateString);

                            
                            
                            //cancel original order
                            ResultTO resultsTO = cancelOrder(conn,csr,Long.toString(orderDetailVO.getOrderDetailId()));
                            String cancelledVenusNumber = resultsTO.getKey();
                            if(resultsTO.isSuccess())
                            {
                                //update the ship method, ship date, and delivery date in Clean
                                OrderDetailVO newOrderDetailVO = new OrderDetailVO();
                                newOrderDetailVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
                                newOrderDetailVO.setShipDate(shipDate);
                                newOrderDetailVO.setDeliveryDate(firstDeliveryDate);
                                newOrderDetailVO.setShipMethod(transitVO.getShipMethod());
                                orderDAO.updateOrderDetail(newOrderDetailVO,csr);
    
                                //send out a new order
                                resultsTO = resendOrder(csr,Long.toString(orderDetailVO.getOrderDetailId()));
                                if(resultsTO.isSuccess())
                                {
                                    String newENumber = getENumber(conn,resultsTO.getKey());
                                
                                    //add comment to order
                                    String comment = "Sent CAN on order " + cancelledVenusNumber + ".  Resent as " +
                                        newENumber + ".  Ship method changed from " + 
                                        orderDetailVO.getShipMethod() + " to " + newOrderDetailVO.getShipMethod();
                                    createComment(conn,comment,orderDetailVO);
                                    
                                    response = "SUCCESS - " + orderDetailVO.getExternalOrderNumber() + " New E Number:" + newENumber;
                                    
                                }
                                else
                                {
                                    //ERROR
                                    error = true;
                                    logger.error("Order cancelled, but new FTD could not be sent for " + orderDetailVO.getExternalOrderNumber() + ". Error:" + resultsTO.getErrorString());
                                    
                                    response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Error:" + resultsTO.getErrorString();                                
                                }
                                
                            }
                            else
                            {
                                //ERROR
                                error = true;
                                logger.error("Could not cancel order " + orderDetailVO.getExternalOrderNumber() +". Error:" + resultsTO.getErrorString());
                                response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Error:" + resultsTO.getErrorString();
                            }
                            
                           
                            //set return xml to indicate order ship method updated                                            
                            
                        }//if del date < current del date
                            
                    }//if transit time < current transit time
                    
                }
            
                
            }//end loop
  

        return response;
                
     }
     
     private String getENumber(Connection conn, String venusId) throws Exception
     {
         OrderDAO orderDAO = new OrderDAO(conn);
         VenusMessageVO vo = orderDAO.getVenusMessageById(venusId);
         return vo.getVenusOrderNumber();
     }
     
     
    private String changeDeliveryDate(Connection conn,HttpServletRequest request,boolean emailCustomer) throws Exception     
    {
            String response = null;
  
            boolean error = false;
         
           ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
           int maxDelDayPush = Integer.parseInt(configUtil.getProperty(OPMConstants.PROPERTY_FILE,"VENUS_SCRIPT_MAX_DAYS_PUSH_DEL_DATE"));                     
         
            //needed DAOs
            OrderDAO orderDAO = new OrderDAO(conn);
            MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
            
            DeliveryDateUTIL dateUtil = new DeliveryDateUTIL();
            
            //get all the transit times
            List transitTimes = getTransitTimes();
         
            //get values from request
            String externalOrderNumber = request.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);
            String csr = request.getParameter(PARAM_CSR);
            if(csr == null)
            {
                csr = DEFAULT_CSR;
            }
            
            //get order detail id
            Map orderIdMap = orderDAO.findOrderNumber(externalOrderNumber);
            String orderDetailId = (orderIdMap.get("OUT_ORDER_DETAIL_ID")).toString();
            
            //get the order detail record
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
            
            //get the customer
            CustomerVO customerVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
                        
            //get vendor Id
            ProductVO productVO = maintDAO.getProduct(orderDetailVO.getProductId());

            //get Delivery Date Util parameters            
            OEDeliveryDateParm dateParam = dateUtil.getCOMParameterData(customerVO.getCountry(),orderDetailVO.getProductId(),customerVO.getZipCode(),CANADA_COUNTRY_CODE, US_COUNTRY_CODE, "1", orderDetailVO.getOccasion(),conn, orderDetailVO.getOccasion(), "FTD", false, false,maxDelDayPush + 2);        
            
            //get delivery date for this ship method
            OEDeliveryDate oeDate = dateUtil.getFirstAvailableDate(dateParam,orderDetailVO.getShipMethod());  
            
            //get the difference in days between requested and returned delivery date
            Date foundDate = FieldUtils.formatStringToUtilDate(oeDate.getDeliveryDate());
            long diffMillis = foundDate.getTime() - orderDetailVO.getDeliveryDate().getTime();
            int diffDays = new Long(diffMillis/(24*60*60*1000)).intValue() + 5;  
            
            //only change del date if within range
            if(diffDays > maxDelDayPush)
            {
                response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Reason:Cannot upgrade ship method.";
            }
            else
            {
                  //get ship date for new ship type
                  String shipDateString = dateUtil.getShipDate(dateParam,orderDetailVO.getShipMethod(), oeDate.getDeliveryDate(),
                  customerVO.getState(), customerVO.getCountry(),
                  orderDetailVO.getProductId(), conn);                            
                  Date shipDate = FieldUtils.formatStringToUtilDate(shipDateString);

                    //cancel original order
                    ResultTO resultsTO = cancelOrder(conn,csr,Long.toString(orderDetailVO.getOrderDetailId()));
                    String cancelledVenusNumber = resultsTO.getKey();
                    if(resultsTO.isSuccess())
                    {
                        //update the ship date, and delivery date in Clean
                        OrderDetailVO newOrderDetailVO = new OrderDetailVO();
                        newOrderDetailVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
                        newOrderDetailVO.setShipDate(shipDate);
                        newOrderDetailVO.setDeliveryDate(foundDate);
                        orderDAO.updateOrderDetail(newOrderDetailVO,csr);

                        //send out a new order
                        resultsTO = resendOrder(csr,Long.toString(orderDetailVO.getOrderDetailId()));
                        if(resultsTO.isSuccess())
                        {
                            String newENumber = getENumber(conn,resultsTO.getKey());                            
             
                            String formatttedOrigDate = FieldUtils.formatUtilDateToString(orderDetailVO.getDeliveryDate());
                            String formatttedNewDate = FieldUtils.formatUtilDateToString(newOrderDetailVO.getDeliveryDate());
                        
                            //add comment to order
                            String comment = "Sent CAN on order " + cancelledVenusNumber + ".  Resent as " +
                                newENumber + ".  Delivery date changed from " + 
                               formatttedOrigDate + " to " + formatttedNewDate;
                                
                            createComment(conn,comment,orderDetailVO);
                            
                            response = "SUCCESS - " + orderDetailVO.getExternalOrderNumber() + " New E Number:" + newENumber;
                            
                            //check if customer should be emailed
                            if(emailCustomer)
                            {
                                //send email
                                sendDeliveryDateEmail(conn,orderDetailVO,formatttedOrigDate,formatttedNewDate);
                            }
                            
                        }
                        else
                        {
                            //ERROR
                            error = true;
                            logger.error("Order cancelled, but new FTD could not be sent for " + orderDetailVO.getExternalOrderNumber() + ". Error:" + resultsTO.getErrorString());
                            
                            response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Error:" + resultsTO.getErrorString();                                
                        }
                        
                    }
                    else
                    {
                        //ERROR
                        error = true;
                        logger.error("Could not cancel order " + orderDetailVO.getExternalOrderNumber() +". Error:" + resultsTO.getErrorString());
                        response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Error:" + resultsTO.getErrorString();
                    }
            }
        
        return response;
    }
     
    private String cancelOrder(Connection conn,HttpServletRequest request,boolean resendOrder) throws Exception
    {
            String response = null;
  
            boolean error = false;         
         
            //needed DAOs
            OrderDAO orderDAO = new OrderDAO(conn);
            MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
            
            DeliveryDateUTIL dateUtil = new DeliveryDateUTIL();
            
            //get all the transit times
            List transitTimes = getTransitTimes();
         
            //get values from request
            String externalOrderNumber = request.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);
            String csr = request.getParameter(PARAM_CSR);
            if(csr == null)
            {
                csr = DEFAULT_CSR;
            }
            
            //get order detail id
            Map orderIdMap = orderDAO.findOrderNumber(externalOrderNumber);
            String orderDetailId = (orderIdMap.get("OUT_ORDER_DETAIL_ID")).toString();
            
            //get the order detail record
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
            
            //get the customer
            CustomerVO customerVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
                        
            //get vendor Id
            ProductVO productVO = maintDAO.getProduct(orderDetailVO.getProductId());

            //cancel original order
            ResultTO resultsTO = cancelOrder(conn,csr,Long.toString(orderDetailVO.getOrderDetailId()));
            String cancelledVenusNumber = resultsTO.getKey();

            //check if cancel worked
            if(resultsTO.isSuccess())
            {            
                //if user requested the order to be resent
                if(resendOrder)
                {
                    //send out a new order
                    resultsTO = resendOrder(csr,Long.toString(orderDetailVO.getOrderDetailId()));
                    if(resultsTO.isSuccess())
                    {
                        String newENumber = getENumber(conn,resultsTO.getKey()); 
                    
                        //add comment to order
                        String comment = "Sent CAN on order " + cancelledVenusNumber + ".  Resent as " + newENumber;
                        createComment(conn,comment,orderDetailVO);
                        
                        response = "SUCCESS - " + orderDetailVO.getExternalOrderNumber() + " New E Number: " + newENumber;
                        
                    }                    
                    else
                    {
                        //ERROR
                        error = true;
                        logger.error("Order cancelled, but new FTD could not be sent for " + orderDetailVO.getExternalOrderNumber() + ". Error:" + resultsTO.getErrorString());
                        
                        response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Error:" + resultsTO.getErrorString();                                
                    }
                }
                else
                {
                    logger.debug("Order not being resent");
                    response = "SUCCESS - " + orderDetailVO.getExternalOrderNumber();
                }
            }
            else
            {
                //ERROR
                error = true;
                logger.error("Could not cancel order " + orderDetailVO.getExternalOrderNumber() +". Error:" + resultsTO.getErrorString());
                response = "ERROR - " + orderDetailVO.getExternalOrderNumber() + " Error:" + resultsTO.getErrorString();
            }
        
        return response;        
    }
     
    private void sendDeliveryDateEmail(Connection conn,OrderDetailVO orderDetailVO,String oldDeliveryDate, String newDeliveryDate) throws Exception
    {
     
        //get the sender email address from config file
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String fromEmailAddress = configUtil.getProperty(OPMConstants.PROPERTY_FILE,"DELIVERY_DATE_CHG_EMAIL_FROM_ADDRESS");                 
        String fedExEmailTemplate = configUtil.getProperty(OPMConstants.PROPERTY_FILE,"DELIVERY_DATE_CHG_EMAIL_TEMPLATE_ID");                 

        //Get data needed for email
        OrderDAO orderDAO = new OrderDAO(conn);

        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
        //CustomerVO recipientVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
        CustomerVO buyerVO = orderDAO.getCustomer(orderVO.getCustomerId());
        EmailVO emailVO = orderDAO.getCustomerEmailInfo(orderVO.getCustomerId(),orderVO.getCompanyId());
    
        //if email info was not found then do not send out email
        if(emailVO == null)
        {
            throw new Exception("Email infomration was not found for customer " + buyerVO.getCustomerId() + " for company " + orderVO.getCompanyId() + ".  Tracking number will not be emailed.");
        }

        CompanyVO companyVO = orderDAO.getCompany(orderVO.getCompanyId());
                   
        //create poc xml
        XMLDocument xml = (XMLDocument) DOMUtil.getDocument();
        addElement(xml,"old_delivery_date",oldDeliveryDate);
        addElement(xml,"new_delivery_date",newDeliveryDate);
        addElement(xml,"external_order_number",orderDetailVO.getExternalOrderNumber());
        addElement(xml,"company_name",companyVO.getCompanyName());
        addElement(xml,"company_url",companyVO.getURL());

        //create a point of contact VO
        PointOfContactVO pocVO = new PointOfContactVO();
        pocVO.setCompanyId(orderVO.getCompanyId());
        pocVO.setSenderEmailAddress(fromEmailAddress + "@" + companyVO.getURL());
        pocVO.setRecipientEmailAddress(emailVO.getEmailAddress());
        pocVO.setTemplateId(null);
        pocVO.setLetterTitle(fedExEmailTemplate);
        pocVO.setPointOfContactType(POC_EMAIL_TYPE);
        pocVO.setCommentType("Order");

        pocVO.setDataDocument(xml);     
        
        ArrayList pocVOList = new ArrayList();
        pocVOList.add(pocVO);
     
        StockMessageGenerator messageGenerator = new StockMessageGenerator();                      
        messageGenerator.processMessages(pocVOList);                      
     
    }     
     
    private void addElement(XMLDocument xml, String keyName, String value)
    {
        Element nameElement = (Element) xml.createElement(keyName);
        
        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);    
    }    
    
     
    private void createComment(Connection conn, String comment, OrderDetailVO orderDetailVO)
        throws Exception
    {
            OrderDAO orderDAO = new OrderDAO(conn);    
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment(comment);
            commentsVO.setCommentOrigin(ORDER_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCommentType(COMMENT_TYPE_ORDER);
            commentsVO.setCreatedBy(ORDER_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
            orderDAO.insertComment(commentsVO);
    }     
     
    private ResultTO resendOrder(String csr, String orderDetailId) throws Exception
    {
       OrderDetailKeyTO keyTo = new OrderDetailKeyTO();
       keyTo.setOrderDetailId(orderDetailId);
       keyTo.setCsr(csr);
       ResultTO result= MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(keyTo);
       return result;
    }

    private ResultTO cancelOrder(Connection conn,String csr, String orderDetailId) throws Exception
    {
    
       VenusMessageVO ftdMsg = getLastFTD(conn,orderDetailId);
       
       ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
       String venusCancelCode = configUtil.getProperty(OPMConstants.PROPERTY_FILE,"SCRIPT_CANCEL_CODE");            
    
       CancelOrderTO cancelTo = new CancelOrderTO();
       cancelTo.setCancelReasonCode(venusCancelCode);
       cancelTo.setComments("Order cancelled by Venus scripts.");
       cancelTo.setCsr(csr);
       cancelTo.setTransmitCancel(true);
       cancelTo.setVenusId(ftdMsg.getVenusId());
       ResultTO result= MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(cancelTo);

       //changing key to be equal to the venus order number
       result.setKey(ftdMsg.getVenusOrderNumber());
        
       return result;
    }
     
    private VenusMessageVO getLastFTD(Connection conn,String orderDetailId) throws Exception
    {
        OrderDAO orderDAO = new OrderDAO(conn);
    
        VenusMessageVO foundOrder = null;
    
        //Retrieve venus FTD order message related to this message 
        List venusList = orderDAO.getVenusMessageByDetailId(orderDetailId, "FTD");

        if(venusList == null || venusList.size() == 0)
        {
            //return null            
        }
        else
        {
            //find the most recent order (this is based on venus id)
            Iterator iter = venusList.iterator();
            while(iter.hasNext())
            {
                VenusMessageVO loopVO = (VenusMessageVO)iter.next();
                if(foundOrder == null || Integer.parseInt(foundOrder.getVenusId()) < Integer.parseInt(loopVO.getVenusId()) )
                {
                    foundOrder = loopVO;
                }
            }//end loop
            
        }
        
        return foundOrder;
    }     
     
    private int getTransitTime(List transitTimes, String shipMethod)
    {
        int transitTime = -999;
        
        Iterator iter = transitTimes.iterator();
        while(transitTime < 0 && iter.hasNext())
        {
            TransitTimeVO transitVO = (TransitTimeVO)iter.next();
            if(transitVO.getShipMethod().equals(shipMethod))
            {
                transitTime = transitVO.getTransitTime();
            }
        }
        
        return transitTime;
    }
     
    private List getTransitTimes()
    {
        List transitList = new ArrayList();
        
        TransitTimeVO vo = null;
        
        vo = new TransitTimeVO();
        vo.setShipMethod("GR");
        vo.setTransitTime(DeliveryDateUTIL.getStandardTransitTime());        
        transitList.add(vo);    

        vo = new TransitTimeVO();
        vo.setShipMethod("2F");
        vo.setTransitTime(DeliveryDateUTIL.getSecondDayTransitTime());        
        transitList.add(vo);

        vo = new TransitTimeVO();
        vo.setShipMethod("ND");
        vo.setTransitTime(DeliveryDateUTIL.getNextDayTransitTime());
        transitList.add(vo);        

        vo = new TransitTimeVO();
        vo.setShipMethod("SA");
        vo.setTransitTime(DeliveryDateUTIL.getNextDayTransitTime());
        transitList.add(vo);        

        
        return transitList;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Servlet1</title></head>");
        out.println("<body>");
        out.println("<p>The servlet has received a GET. This is the reply.</p>");
        out.println("<form action='com.ftd.op.maintenance.action.VenusScriptServlet' method='post'>");
        out.println("<table>");        
        out.println("<tr><td>action</td><td><input type='text' name='mode' value='change_delivery_date'/></td></tr>");
        out.println("<tr><td>entered_sql</td><td><input type='text' name='sql' value='select * from z;'/></td></tr>");
        out.println("<tr><td>action</td><td><input type='text' name='external_order_number' value='C1000697589'/></td></tr>");
        out.println("<tr><td colspan=2><input type='submit'></td></tr>");
        out.println("</table>");        
        out.println("</form>");        
        out.println("</body></html>");
        out.close();
    }
    
     */

    
}