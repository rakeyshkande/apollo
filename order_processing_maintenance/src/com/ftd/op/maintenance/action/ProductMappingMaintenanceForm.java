package com.ftd.op.maintenance.action;

import java.util.ArrayList;
import java.util.List;


import org.apache.struts.action.ActionForm;

import com.ftd.op.maintenance.vo.BaseProductMappingVO;

public class ProductMappingMaintenanceForm extends ActionForm {
	
	
	private String floralSKU;
	
	private List<BaseProductMappingVO> vendorProdSkuList;	

	public String getFloralSKU() {
		return floralSKU;
	}

	public void setFloralSKU(String floralSKU) {
		this.floralSKU = floralSKU;
	}

	public List<BaseProductMappingVO> getVendorProdSkuList() {
		return vendorProdSkuList;
	}

	public void setVendorProdSkuList(List<BaseProductMappingVO> vendorProdSkuList) {
		this.vendorProdSkuList = vendorProdSkuList;
	}
	public BaseProductMappingVO getProdMapping(int index)
	{
		// make sure that orderList is not null
		if(this.vendorProdSkuList == null)
		{
			this.vendorProdSkuList = new ArrayList<BaseProductMappingVO>();
		}

		// indexes do not come in order, populate empty spots
		while(index >= this.vendorProdSkuList.size())
		{
			this.vendorProdSkuList.add(new BaseProductMappingVO());
		}

		// return the requested item
		return (BaseProductMappingVO) vendorProdSkuList.get(index);
	}


	
}
