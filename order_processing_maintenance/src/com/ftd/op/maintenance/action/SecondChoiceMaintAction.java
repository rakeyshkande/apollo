package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.dao.LockDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.SecondChoiceVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * SecondChoiceMaintAction class 
 * 
 * The “Second Choice Maintenance” screen allows a user to view/add/edit the list second-choice 
 * options available on the product database with FTD.COM.  This screen will provide the user 
 * with the ability to update basic information about each second-choice on file and to flag each 
 * option as usable.
 * 
 */
public final class SecondChoiceMaintAction extends Action 
{
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.SecondChoiceMaintAction");
  
  //request xsl filename
  private static final String SECOND_CHOICE_MAINTENANCE_XSL = "secondChoiceMaintenanceXSL";
  
  //action forwards
  private static final String MAIN_MENU_ACTION = "MainMenuAction";
  
  //config:  file names
  public static final String PROPERTY_FILE = "maintenance-config.xml";
  private static String DATASOURCE_NAME = "CLEAN";
  
  //application:
  public static final String APP_PARAMETERS = "parameters";
  
  //application:  actions
  public static final String ACTN_ERROR = "Error";
  
  //request parameters
  public static final String ACTION_TYPE = "action_type";
  public static final String R_OCCASION_ID = "r_occasion_id";
  public static final String R_DESCRIPTION = "r_description";
  public static final String R_ACTIVE = "r_active";
  
  //action types
  public static final String  ADD_SECOND_CHOICE_ACTION = "addSecondChoiceAction";
  public static final String  SAVE_SECOND_CHOICE_ACTION = "saveSecondChoiceAction";
  public static final String  LOAD_SECOND_CHOICE_ACTION = "loadSecondChoiceAction";
  
  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";
  public static final String VALID_FLAG = "valid_flag";
  public static final String ERROR_VALUE = "error_value";
  public static final String EDIT_ALLOWED = "edit_allowed";
  public static final String ADD_ALLOWED = "add_allowed";
  public static final String LOCK_VALUE = "lock_value";
  public static final String LOCK_FLAG = "lock_flag";
  
  //Security
  public static final String SECURITY_TOKEN = "securitytoken";
  public static final String CONTEXT = "context";
     
  /**
	 * This is the Second Choice Maintenance Action called from the Struts framework.
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    String actionType = null;
  
    /* get action type request parameter */
    if(request.getParameter(ACTION_TYPE)!=null)
    {
      actionType = request.getParameter(ACTION_TYPE);
    }
    else
    {
      actionType = this.LOAD_SECOND_CHOICE_ACTION;
    }
  
    /* displays the add occasion popup */
    if(actionType.equals(ADD_SECOND_CHOICE_ACTION))
    {
      return performAddSecondChoice(mapping, form, request, response);
    }
  
    /* called by add occasion popup to validate the data */
    else if(actionType.equals(SAVE_SECOND_CHOICE_ACTION))
    { 
      return performSaveSecondChoice(mapping, form, request, response);        
    }
  
    // saves changes on the main window
    else if(actionType.equals(MAIN_MENU_ACTION))
    {
      return performMainMenu(mapping, form, request, response);
    }
    
    // loads occasion maintenance page
    else if(actionType.equals(LOAD_SECOND_CHOICE_ACTION))
    {             
      return performLoadSecondChoice(mapping, form, request, response);
    }
    
    return null;
	}

  private ActionForward performLoadSecondChoice(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Document xmlDoc  = null;
    ActionForward forward = null;
    File xslFile = null;
    HashMap dataHash = new HashMap();        
    Connection conn = null;
    
		try
		{
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      /* pull base document */
      xmlDoc = DOMUtil.getDocument();    
      
      /* get security token */
      String sessionId = (String) request.getParameter(SECURITY_TOKEN);
      String context = (String) request.getParameter(CONTEXT);
      String userId = null;

      if(sessionId==null)
      {
        logger.error("Security token not found in request object");
      }
        
      /* retrieve user information */
      if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
      }

      /* retrieve occasion data and add it to root DOM */
      Document secondChoiceDoc = dao.getSecondChoiceXML();
      DOMUtil.addSection(xmlDoc,secondChoiceDoc.getChildNodes()); 

      /* build page data */
      HashMap pageData = new HashMap();
    
      /* check if user has access to edit occasions */
      boolean editAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"Second_Choice_Maint","Update");
              
      /* check if user has access to add occasions */
      boolean addAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"Second_Choice_Maint","Add");

      pageData.put("adminAction", request.getParameter("adminAction"));
      
      /* if user has access to edit or add, check if page is locked */
      if(editAllowed || addAllowed)
      {
        LockDAO lockDao = new LockDAO(conn);
        Document lockDoc = lockDao.checkLockXML(sessionId, userId, "SecondChoiceMaint", "MaintScreen");
        NodeList lockList = lockDoc.getFirstChild().getChildNodes();
        String lockStatus = "N";
        
        // put the lock data in the pageData Node
        for(int i = 0; i < lockList.getLength(); i++) 
        {
          Node node = lockList.item(0);
          if(node.getNodeName().equalsIgnoreCase("OUT_LOCKED_IND")) 
          {
            lockStatus = node.getFirstChild().getNodeValue();
          }
          pageData.put("lock_value", node.getFirstChild().getNodeValue()); 
          
        }
        if(lockStatus.equals("N"))
        {
          // page was not locked; set user access accordingly 
          if(editAllowed)
            pageData.put(EDIT_ALLOWED, "Y"); 
          if(addAllowed)  
            pageData.put(ADD_ALLOWED, "Y"); 
        }
        else
        {
          // page was locked; set user access accordingly and lock message from SP 
          pageData.put(EDIT_ALLOWED, "N");
          pageData.put(ADD_ALLOWED, "N");
        }
      }
      else
      {
        pageData.put(EDIT_ALLOWED, "N");
        pageData.put(ADD_ALLOWED, "N");
      }
      
      /* convert the page data hashmap to XML and append it to the final XML */
      DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 

      /* forward to addOccasionPopupXSL action */
      xslFile = getXSL(SECOND_CHOICE_MAINTENANCE_XSL,mapping);
      
      /* save parameters onto request object */
			request.setAttribute("SecondChoiceMaintenance",xmlDoc);
			request.setAttribute("SecondChoiceMaintenanceXSL",xslFile);
      
      TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
       
    } catch (Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
    return null;
    
  }
  private ActionForward  performAddSecondChoice(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Connection conn = null;
    
		try
		{
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);  
      SecondChoiceVO secondChoice = new SecondChoiceVO();
      secondChoice.setProductSecondChoice(request.getParameter("r_product_second_choice"));
      secondChoice.setOeDescription(request.getParameter("r_oe_description"));
      secondChoice.setMercuryDescription(request.getParameter("r_mercury_description"));
      secondChoice.setOeHolidayDescription(request.getParameter("r_oe_holiday_description"));
      secondChoice.setMercuryHolidayDescription(request.getParameter("r_mercury_holiday_description"));
      dao.insertSecondChoice(secondChoice);
      return performLoadSecondChoice(mapping, form, request, response);
    } catch (Exception e) 
    {
      e.printStackTrace();
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
  }
  private ActionForward performSaveSecondChoice(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Connection conn = null;
    try {
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      List list = new ArrayList();
      Enumeration e = request.getParameterNames();
      while(e.hasMoreElements())
      {
        String name = (String)e.nextElement();    
        if(name.startsWith("r_updated"))
        {          
          String end = name.substring(10);
          if(request.getParameter("r_updated_"+end).equalsIgnoreCase("Y")) 
          {
            SecondChoiceVO secondChoice = new SecondChoiceVO();
            secondChoice.setProductSecondChoice(request.getParameter("r_product_second_choice_"+end));
            secondChoice.setOeDescription(request.getParameter("r_oe_description_"+end));
            secondChoice.setMercuryDescription(request.getParameter("r_mercury_description_"+end));
            secondChoice.setOeHolidayDescription(request.getParameter("r_oe_holiday_description_"+end));
            secondChoice.setMercuryHolidayDescription(request.getParameter("r_mercury_holiday_description_"+end));
            
            dao.updateSecondChoice(secondChoice);            
          }
        }          
      }
      
      /* forward to the load occasion action */
      return performLoadSecondChoice(mapping, form, request, response);
    } catch (Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }

    
  }
  private ActionForward  performMainMenu(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Connection conn = null;
    try {
      conn = this.getDBConnection();
      LockDAO dao = new LockDAO(conn);

      /* get security token */
      String sessionId = (String) request.getParameter(SECURITY_TOKEN);
      String context = (String) request.getParameter(CONTEXT);
      String userId = null;
      if(sessionId==null)
      {
        logger.error("Security token not found in request object");
      }
        
      /* retrieve user information */
      if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
      }

      dao.releaseLock(sessionId, userId, "SecondChoiceMaint", "MaintScreen");
      
      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
      return newForward;
    } catch(Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
  }
  /**
  * get XSL file
  *
  * Retrieve name of CSZ Zipcode Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
    
	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
	 * @return Connection - db connection
	 * @throws ClassNotFoundException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,
															   DATASOURCE_NAME));

		return conn;
	}  
}