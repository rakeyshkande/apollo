/**
 * 
 */
package com.ftd.op.maintenance.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.op.maintenance.bo.DeliveryFeeMaintenanceBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.WebsiteDeliveryFeeTransmission;
import com.ftd.op.maintenance.vo.DeliveryFeeVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

/**
 * @author smeka
 *
 */
public class DeliveryFeeMaintenanceAction extends DispatchAction {
	private static Logger logger = new Logger("com.ftd.op.maintenance.action.DeliveryFeeMaintenanceAction");
	
	private static final String ACCESS_MODE = "mode";
	private static final String VIEW_MODE = "view";
	private static final String EDIT_MODE = "edit";

	private static final String DELIVERY_FEE_TYPE = "deliveryType";
	private static final String MORNING_DELIVERY_SCREEN = "Morning Delivery";
	private static final String ADMIN_ACTION = "admin_action";
	private static final String DELIVERY_FEE_ID = "deliveryId";
	private static final String SECURITY_TOKEN = "securitytoken";
	private static final String CONTEXT = "context";

	// XSL paths
	private static final String DELIVERY_FEE_LISTING_XSL = "deliveryFeeList";
	private static final String DISPLAY_DELIVERY_FEE_HIST = "displayDeliveryFeeHist";
	private static final String DISPLAY_SRC_CODES_XSL = "displayDeliveryFeeSourceCodes";

	private static final String SYSTEM_ERROR = "systemError";
	private static final String NOVATOR_CONNECT_ERROR = "novatorConnect";
	private static final String NOVATOR_DENIED_REQ = "novatorDEN";

	private static final String DELIETE_SUCCESSFUL = "deleteSuccessful";
	private static final String SAVE_SUCCESSFUL = "saveSuccessful";
	
	/* Action Methods */

	/** Action method to get all the delivery fee rows from DB for a given delivery type and sort field.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public ActionForward loadDeliveryFeeList(ActionMapping mapping,	ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		logDebugMessage("Load Delivery Fee List based on delivery type : " + request.getParameter(DELIVERY_FEE_TYPE));
		Document doc = null;
		
		try {			

			int deliveryType = Integer.parseInt(request.getParameter(DELIVERY_FEE_TYPE));

			DeliveryFeeMaintenanceBO dfBO = new DeliveryFeeMaintenanceBO();
			doc = dfBO.getDeliveryFeeList(getSortField(request), deliveryType);

			transformResponse(request, response, doc, DELIVERY_FEE_LISTING_XSL,	mapping);

		} catch (Exception e) {
			logger.error("Error caught in loadDeliveryFeeList : " + e.getMessage());
			return mapping.findForward(SYSTEM_ERROR);
		}
		return null;
	}
	
	/** Action method to delete the selected delivery fee id(s).
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public ActionForward deleteDeliveryFee(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		logDebugMessage("Delete Delivery Fee Ids : " + request.getParameter("deleteIds"));
		
		String adminAction = null;
		String[] deleteIds;
		List<String> list = new ArrayList<String>();
		
		if(request.getParameter("deleteIds") != null) {
			deleteIds = (request.getParameter("deleteIds")).split(",");
			list = Arrays.asList(deleteIds);			
		} else {
			logger.error("No delelteIds were found in request");
			return mapping.findForward(SYSTEM_ERROR);
		}
		
		try {
			DeliveryFeeMaintenanceBO dfBO = new DeliveryFeeMaintenanceBO();
			WebsiteDeliveryFeeTransmission ndft = new WebsiteDeliveryFeeTransmission();
			
			for (int i = 0; i < list.size(); i++) {	
				
				DeliveryFeeVO deliveryFee = new DeliveryFeeVO();
				deliveryFee.setId(list.get(i).toString());
				deliveryFee.setDeliveryFee(new Double(0.00));
				deliveryFee.setUpdatedBy(getUserId(request));
				deliveryFee.setRequestedBy(request.getParameter("requested_by"));
				int deliveryType = 0;
				if (request.getParameter(DELIVERY_FEE_TYPE) != null) {
					deliveryType = Integer.parseInt(request.getParameter(DELIVERY_FEE_TYPE));
				}
				deliveryFee.setDeliveryFeeType(deliveryType);
				
				String novatorResponse = "";
				try {
					novatorResponse = ndft.sendDeliveryFee(deliveryFee, "Delete");
				} catch (Exception e) {
					logger.error("exception: " + e.getMessage());
					adminAction = NOVATOR_CONNECT_ERROR ;
				}
				if (novatorResponse != null	&& novatorResponse.equalsIgnoreCase("CON")) {
					dfBO.deleteDeliveryFeeData(deliveryFee);
					
					/*Implemented as per Fee Micro Service
					 * This call is responsible for generating Morning delivery feed for "Delete" action and insert it into Fees_Feeds table
					 */
					dfBO.insertDeliveryFeeFeed(deliveryFee, "Delete");
					adminAction = DELIETE_SUCCESSFUL;
				} else if (novatorResponse != null && novatorResponse.equalsIgnoreCase("DEN")) {
					adminAction = NOVATOR_DENIED_REQ;
				}
				logDebugMessage("Processed delete request for Id : " + deliveryFee.getId() + " and " + adminAction);
				request.setAttribute(ADMIN_ACTION, adminAction);				
			}
			
			return loadDeliveryFeeList(mapping, form, request, response);
			
		} catch (Exception e) {
			logger.error("Error caught in deleteDeliveryFee : " + e.getMessage());
			return mapping.findForward(SYSTEM_ERROR);
		}
	}
	
	/** Action method to save the delivery fee info.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public ActionForward saveDeliveryFee(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		logDebugMessage("Save Delivery Fee For Id : " + request.getParameter(DELIVERY_FEE_ID));
		String adminAction = null;
		
		try {
				        
			DeliveryFeeMaintenanceBO dfBO = new DeliveryFeeMaintenanceBO();
			DeliveryFeeVO deliveryVO = new DeliveryFeeVO();
			deliveryVO.setId(request.getParameter(DELIVERY_FEE_ID));
			deliveryVO.setDescription(request.getParameter("description"));			
			double fee = new Double(request.getParameter("deliveryFee"));
			deliveryVO.setDeliveryFee(fee);			
			deliveryVO.setUpdatedBy(getUserId(request));
			deliveryVO.setRequestedBy(request.getParameter("requested_by"));
			
			//default the deliveryType to 0 - invalid column
			int deliveryType = 0;
			if (request.getParameter(DELIVERY_FEE_TYPE) != null) {
				deliveryType = Integer.parseInt(request.getParameter(DELIVERY_FEE_TYPE));
			}
			deliveryVO.setDeliveryFeeType(deliveryType);
			
			logDebugMessage("**Sending Message to Novator**");
	        WebsiteDeliveryFeeTransmission ndft = new WebsiteDeliveryFeeTransmission();	        
	        String novatorResponse = "";
	        
	        try {
	            novatorResponse = ndft.sendDeliveryFee(deliveryVO, "Add");
	        } catch (Exception e) {
	            logger.error("exception: " + e.getMessage());
	            adminAction = NOVATOR_CONNECT_ERROR;
	        }
	        
	        if (novatorResponse != null && novatorResponse.equalsIgnoreCase("CON")) {
	        	dfBO.saveDeliveryFeeData(deliveryVO);
	        	
	        	/*Implemented as per Fee Micro Service
				 * This call is responsible for generating Morning delivery feed for "Add"(Update/Insert) action and insert it into Fees_Feeds table
				 */
	        	dfBO.insertDeliveryFeeFeed(deliveryVO, "Add");
	            adminAction = SAVE_SUCCESSFUL;
	        } else if (novatorResponse != null && novatorResponse.equalsIgnoreCase("DEN")) {
	            adminAction = NOVATOR_DENIED_REQ;
	        }			
			
	        logDebugMessage("Processed Save request for Id : " + deliveryVO.getId() + " and " + adminAction);
			request.setAttribute(ADMIN_ACTION, adminAction);		
			
			return loadDeliveryFeeList(mapping, form, request, response);

		} catch (Exception e) {
			logger.error("Error caught in saveDeliveryFee : " + e.getMessage());
			return mapping.findForward(SYSTEM_ERROR);
		}
	}
	
	/** Action method to load the delivery fee history for a given delivery fee id.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public ActionForward loadDeliveryFeeHistory(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		logDebugMessage("Load Delivery Fee History for Id : " +  request.getParameter(DELIVERY_FEE_ID));
		Document histDoc = null;
		
        try {
        	DeliveryFeeMaintenanceBO dfBO = new DeliveryFeeMaintenanceBO(); 
			//default the deliveryType to 0 - invalid column
			int deliveryType = 0;
			if (request.getParameter(DELIVERY_FEE_TYPE) != null) {
				deliveryType = Integer.parseInt(request.getParameter(DELIVERY_FEE_TYPE));
			}
        	histDoc = dfBO.getDeliveryFeeHistory(request.getParameter(DELIVERY_FEE_ID), deliveryType);        	
        	transformResponse(request, response, histDoc, DISPLAY_DELIVERY_FEE_HIST, mapping);
        	
        } catch (Exception e) {
        	logger.error("Error caught in loadDeliveryFeeHistory : " + e.getMessage());
			return mapping.findForward(SYSTEM_ERROR);
        } 
        return null;        
    }
	
	/** Action Method to load source codes associated with a particular delivery fee id.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public ActionForward loadSourceCodesByFeeId(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) throws IOException, ServletException {
		
		logDebugMessage("Load Source Codes associated with Delivery Fee Id : " + request.getParameter(DELIVERY_FEE_ID));
		
		Document sourceCodeDoc = null;
		
		try {
			DeliveryFeeMaintenanceBO dfBO = new DeliveryFeeMaintenanceBO();
			sourceCodeDoc = dfBO.getSourceCodesByDeliveryId(request.getParameter(DELIVERY_FEE_ID));
			transformResponse(request, response, sourceCodeDoc,	DISPLAY_SRC_CODES_XSL, mapping);
		} catch (Exception e) {
			logger.error("Error caught in loadSourceCodesByFeeId : " + e.getMessage());
			return mapping.findForward(SYSTEM_ERROR);
		}
		return null;
	}
	
	/* Private methods */
	
	/** Logs the Debug message if debug is enabled
	 * @param message
	 */
	private static void logDebugMessage(String message) {
		if(logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}
	
	/** Convenient method to transform the Document type response to XML
	 * @param request
	 * @param response
	 * @param doc
	 * @param xslFilePath
	 * @param mapping
	 */
	private void transformResponse(HttpServletRequest request, HttpServletResponse response, Document doc, 
			String xslFilePath,	ActionMapping mapping) throws Exception {
		Document xmlDoc  = null;
		
		try {
			xmlDoc = (Document) DOMUtil.getDocument();
			DOMUtil.addSection(xmlDoc, doc.getChildNodes());
			
			//default the deliveryType to 0
			int deliveryType = 0;
			if (request.getParameter(DELIVERY_FEE_TYPE) != null) {
				deliveryType = Integer.parseInt(request.getParameter(DELIVERY_FEE_TYPE));
			}
			
			DOMUtil.addSection(xmlDoc, this.addSortFieldOptions(getFeeColumn(deliveryType),	getScreenName(deliveryType, request)).getChildNodes());

			// set the page data
			setPageData(request, xmlDoc);

			StringWriter sw = new StringWriter();
			DOMUtil.print(xmlDoc, new PrintWriter(sw));

			File xslFile = getXSL(xslFilePath, mapping);
			
			String sessionId = request.getParameter(SECURITY_TOKEN);
			String context = request.getParameter(CONTEXT);

			HashMap<String, String> parameters = (HashMap) request.getAttribute("parameters");
			parameters.put(SECURITY_TOKEN, sessionId);
			parameters.put(CONTEXT, context);

			TraxUtil.getInstance().transform(request, response, xmlDoc,	xslFile, parameters);

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new Exception("Unable to transform response : " + e.getMessage());
		}
	}
	
	/** Returns the fee column based on the deliveryType. Defaulted to return Morning_delivery_fee column. 
	 * @param deliveryType
	 * @return
	 */
	private String getFeeColumn(int deliveryType) {
		switch (deliveryType) {
			case OPMConstants.MORNING_DELIVERY_TYPE:				
				return OPMConstants.MORNING_DELIVERY_FEE_COL;
			default:
				return OPMConstants.MORNING_DELIVERY_FEE_COL;
		}
	}
	
	/**Returns the screen name based on the delivery type
	 * @param deliveryType
	 * @param request
	 * @return
	 */
	private String getScreenName(int deliveryType, HttpServletRequest request) {
		if(request.getParameter("screenName") != null) {
			return request.getParameter("screenName");
		}
		switch (deliveryType) {
		case OPMConstants.MORNING_DELIVERY_TYPE:
			return MORNING_DELIVERY_SCREEN;
		default:
			return MORNING_DELIVERY_SCREEN;
		}
	}
	
	/** returns the logged in user id details.
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private String getUserId(HttpServletRequest request) throws Exception {
		if (SecurityManager.getInstance().getUserInfo(request.getParameter(SECURITY_TOKEN)) != null) {
			 return SecurityManager.getInstance().getUserInfo(request.getParameter(SECURITY_TOKEN)).getUserID();
		}
		return null;
	}
	
	/** Sets all the page data required into response Document
	 * @param request
	 * @param xmlDoc
	 * @return
	 */
	private void setPageData(HttpServletRequest request, Document xmlDoc) {

		String selectedSortField = request.getParameter("sortField");
		
		int deliveryType = 0;
		if (request.getParameter(DELIVERY_FEE_TYPE) != null) {
			deliveryType = Integer.parseInt(request.getParameter(DELIVERY_FEE_TYPE));
		} 

		HashMap<String, String> pageData = new HashMap<String, String>();
		pageData.put("sortField", selectedSortField);
		pageData.put("screenName", getScreenName(deliveryType, request));
		pageData.put(DELIVERY_FEE_ID, request.getParameter(DELIVERY_FEE_ID));
		pageData.put("description", request.getParameter("description"));
		pageData.put(DELIVERY_FEE_TYPE, request.getParameter(DELIVERY_FEE_TYPE));
		String accessMode = request.getParameter(ACCESS_MODE);
		
		// check if user has access to add/edit/delete Delivery Fee and source codes		 
		try {
			if (!StringUtils.isEmpty(accessMode)) {
				pageData.put(ACCESS_MODE, accessMode);
			} else {
				if (isEditAllowed(request)) {
					pageData.put(ACCESS_MODE, EDIT_MODE);
				} else {					
					pageData.put(ACCESS_MODE, VIEW_MODE);
				}
			}			
		} catch (Exception e) {
			logger.error("Error caught setting page data : " + e.getMessage());
			pageData.put(ACCESS_MODE, VIEW_MODE);
		}		
		
		if(request.getAttribute(ADMIN_ACTION) !=null) {
			pageData.put(ADMIN_ACTION, (String)request.getAttribute(ADMIN_ACTION));
		}
		/* convert the page data hash map to XML and append it to the final XML */
		DOMUtil.addSection(xmlDoc, "pageData", "data", pageData, true);
	}
	
	/** Convenient method to check if all the resources of a resource group, "Delivery Fee Maint Edit Only" are assigned to user.
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private boolean isEditAllowed(HttpServletRequest request) throws Exception {
		return SecurityManager.getInstance().assertPermission (
				request.getParameter(CONTEXT), request.getParameter(SECURITY_TOKEN), "SourceSnhData", "Update");
	}	

	/** Convenient method to get the real path of the XSL file
	 * @param xslName
	 * @param mapping
	 * @return
	 */
	private File getXSL(String xslName, ActionMapping mapping) {

		File xslFile = null;
		String xslFilePathAndName = null;

		ActionForward forward = mapping.findForward(xslName);
		xslFilePathAndName = forward.getPath(); // get real file name
		xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
		return xslFile;
	}
	
	/** Sets the options available to sort the delivery fee list as a doc.
	 * @param feeColumn
	 * @return
	 * @throws ParserConfigurationException
	 */
	private Document addSortFieldOptions(String feeColumn, String screenName) throws ParserConfigurationException {
		Document doc = DOMUtil.getDefaultDocument();

		Element root = JAXPUtil.buildSimpleXmlNode(doc, "sort_fields", "");
		doc.appendChild(root);

		Element fieldElement = JAXPUtil.buildSimpleXmlNode(doc, "sort_field","");
		root.appendChild(fieldElement);		
		Element idElement = JAXPUtil.buildSimpleXmlNode(doc, "id", "DELIVERY_FEE_ID");
		fieldElement.appendChild(idElement);
		Element descElement = JAXPUtil.buildSimpleXmlNode(doc, "description", "ID");
		fieldElement.appendChild(descElement);

		fieldElement = JAXPUtil.buildSimpleXmlNode(doc, "sort_field", "");
		root.appendChild(fieldElement);		
		idElement = JAXPUtil.buildSimpleXmlNode(doc, "id", "DESCRIPTION");
		fieldElement.appendChild(idElement);
		descElement = JAXPUtil.buildSimpleXmlNode(doc, "description", "Description");
		fieldElement.appendChild(descElement);

		fieldElement = JAXPUtil.buildSimpleXmlNode(doc, "sort_field", "");
		root.appendChild(fieldElement);
		idElement = JAXPUtil.buildSimpleXmlNode(doc, "id", feeColumn);
		fieldElement.appendChild(idElement);
		descElement = JAXPUtil.buildSimpleXmlNode(doc, "description", screenName + " Fee ");
		fieldElement.appendChild(descElement);

		return doc;
	}
	
	/** Returns the sort field selected on delivery fee maintenance page. The sort field is defaulted to Delivery Fee Id.
	 * @param request
	 * @return
	 */
	private String getSortField(HttpServletRequest request) {
		String sortField = null;
		String selectedSortField = request.getParameter("sortField");
		if (selectedSortField == null || selectedSortField.length() == 0) {
			sortField = OPMConstants.DEFAULT_DELIVERY_FEE_SORT_ID;
			selectedSortField = OPMConstants.DEFAULT_DELIVERY_FEE_SORT_ID;
		} else {
			if(selectedSortField.indexOf("ID") >= 0) {
				sortField = "to_number(" + selectedSortField +")";
			} else {
				sortField = selectedSortField; 
			}
			sortField += "," + OPMConstants.DEFAULT_DELIVERY_FEE_SORT_ID;
		}
		return sortField;
	}

}
