package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.dao.LockDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.OccasionVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;

import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import sun.rmi.transport.proxy.HttpReceiveSocket;

/**
 * OccasionMaintAction class 
 * 
 * The �Occasion Codes List� allows a Customer Service Representative options for 
 * choosing occasions when generating an order.  The �Occasion Maintenance� screen 
 * will set and maintain the available drop down occasions when creating a product 
 * in the Product Database.
 * 
 */
public final class OccasionMaintAction extends Action 
{
  /**
   * This inner class provides a comparator for use in ordering occasions by sort order
   */
  private class OccasionComparator implements Comparator
  {
      public int compare(Object o1, Object o2) throws ClassCastException
      {
        OccasionVO occasion1 = (OccasionVO) o1;
        OccasionVO occasion2 = (OccasionVO) o2;
        
        // deal with null display orders in a reasonable fashion -- nulls always go at the end
        if(occasion1.getDisplayOrder() == null && occasion2.getDisplayOrder() == null) return 0;
        if(occasion1.getDisplayOrder() == null) return 1;
        if(occasion2.getDisplayOrder() == null) return -1;
        
        //deal with blank display orders in a similar fashion
        if(occasion1.getDisplayOrder().length() == 0 && occasion2.getDisplayOrder().length() == 0) return 0;
        if(occasion1.getDisplayOrder().length() == 0) return 1;
        if(occasion2.getDisplayOrder().length() == 0) return -1;
        
        if(new Integer(occasion1.getDisplayOrder()).intValue() < new Integer(occasion2.getDisplayOrder()).intValue()) 
        {
          return -1;
        } else if(new Integer(occasion1.getDisplayOrder()).intValue() == new Integer(occasion2.getDisplayOrder()).intValue()) 
        {
          return 0;
        } else 
        {
          return 1;
        }
      }
  }  
  
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.OccasionMaintAction");
  
  //request xsl filename
  private static final String OCCASION_MAINTENANCE_XSL = "occasionMaintenanceXSL";
  private static final String ADD_OCCASION_POPUP_XSL = "addOccasionPopupXSL";
  
  //action forwards
  private static final String MAIN_MENU_ACTION = "MainMenuAction";
  
  //config:  file names
  public static final String PROPERTY_FILE = "maintenance-config.xml";
  private static String DATASOURCE_NAME = "CLEAN";
  
  //application:
  public static final String APP_PARAMETERS = "parameters";
  
  //application:  actions
  public static final String ACTN_ERROR = "Error";
  
  //request parameters
  public static final String ACTION_TYPE = "action_type";
  public static final String R_OCCASION_ID = "r_occasion_id";
  public static final String R_DESCRIPTION = "r_description";
  public static final String R_ACTIVE = "r_active";
  
  //action types
  public static final String  DISPLAY_ADD_OCCASION_ACTION = "displayAddOccasionAction";
  public static final String  ADD_OCCASION_ACTION = "addOccasionAction";
  public static final String  UPDATE_OCCASION_ACTION = "updateOccasionAction";
  public static final String  SAVE_OCCASION_ACTION = "saveOccasionAction";
  public static final String  LOAD_OCCASION_ACTION = "loadOccasionAction";
  public static final String  RELEASE_LOCK_ACTION = "releaseLockAction";
  
  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";
  public static final String VALID_FLAG = "valid_flag";
  public static final String ERROR_VALUE = "error_value";
  public static final String EDIT_ALLOWED = "edit_allowed";
  public static final String ADD_ALLOWED = "add_allowed";
  public static final String LOCK_VALUE = "lock_value";
  public static final String LOCK_FLAG = "lock_flag";
  
  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private String  context = null;
  private HashMap pageData = null;
  private String  sessionId = null;

  private ActionForward performDisplayAddOccasion(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Connection conn = null;
    File xslFile = null;
    Document xmlDoc  = null;
    try {
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      xslFile = getXSL(ADD_OCCASION_POPUP_XSL,mapping);
      xmlDoc = DOMUtil.getDocument();    
      
      this.pageData.put("next_occasion_seq", request.getParameter("next_occasion_seq")); 

      DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, this.pageData, true); 
      
      /* save parameters onto request object */
			request.setAttribute("OccasionMaintenance",xmlDoc);
			request.setAttribute("OccasionMaintenanceXSL",xslFile);

      StringWriter sw = new StringWriter();       
      DOMUtil.print(xmlDoc, new PrintWriter(sw));
//      System.out.println(sw.toString()); //string representation of xml document

      TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
    } catch(Exception e) 
    {
      e.printStackTrace();
    }
    finally
    {
      //close the connection
      try
      {
        conn.close();
      }
      catch(Exception e)
      {}
    }
    
    return null;
  }
  private ActionForward performReleaseLockAction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Connection conn = null;
    try {
      conn = this.getDBConnection();
      LockDAO dao = new LockDAO(conn);

      String userId = null;
//      System.out.println("Global update: " + request.getParameter("global_update"));
      if(this.sessionId==null)
      {
        logger.error("Security token not found in request object");
      }
        
      /* retrieve user information */
      if(SecurityManager.getInstance().getUserInfo(this.sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(this.sessionId).getUserID();
      }

      dao.releaseLock(this.sessionId,userId,"OccasionMaint","MaintScreen");
      
      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
//      System.out.println(newForward.getPath());
      return newForward;
    } catch(Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
  }
  private ActionForward performSaveOccasionAction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Connection conn = null;
    try {
      conn = this.getDBConnection();
      String displayOrder = null; 
      String occasionId = null; 
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      List list = new ArrayList();
      Enumeration e = request.getParameterNames();
      while(e.hasMoreElements())
      {
        String name = (String)e.nextElement();    
//        System.out.println(name);
        if(name.startsWith("r_occasion_id"))
        {          
          String end = name.substring(14);
          OccasionVO occasion = new OccasionVO();            
          occasion.setDescription(request.getParameter("r_description_"+end));
          //display_order
          displayOrder = request.getParameter("r_display_order_"+end); 
          if (displayOrder != null && !displayOrder.equalsIgnoreCase(""))
            displayOrder =  displayOrder.trim(); 
          occasion.setDisplayOrder(displayOrder);
          //occasion_id
          occasionId = request.getParameter("r_occasion_id_"+end);
          if (occasionId != null && !occasionId.equalsIgnoreCase(""))
            occasionId =  occasionId.trim(); 
          occasion.setOccasionId(occasionId);
          occasion.setUpdated(request.getParameter("r_updated_"+end));
          if(request.getParameter("r_active_"+end)!=null && request.getParameter("r_active_"+end).equalsIgnoreCase("on"))
            occasion.setActive("Y");
          else
            occasion.setActive("N");
          list.add(occasion);
        }          
      }
      
      // sort the list of occasions by display order
      Collections.sort((List) list, new OccasionComparator());
      
      /* determine if update or add and process accordingly */
      for(int i=0; i<list.size(); i++)
      {
        OccasionVO occasion = (OccasionVO)list.get(i);
 
        if(new Integer(occasion.getDisplayOrder()).intValue() != (i + 1) || occasion.getUpdated().equalsIgnoreCase("Y")) {
          // modify the display order so that display order does not skip numbers
          occasion.setDisplayOrder((i + 1) + "");
          /* update occasion information */
          dao.updateOccasion(occasion.getOccasionId(), occasion.getDescription(), occasion.getDisplayOrder(), occasion.getActive());              
        }
      }
        
      /* forward to the load occasion action */
      return performLoadOccasionAction(mapping, form, request, response);
    } catch (Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
  }
  private ActionForward performAddOccasionAction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Connection conn = null;
    Document xmlDoc  = null;
    ActionForward forward = null;
    File xslFile = null;
  
    String occasionId;
    String active;
    String description;
		try
		{
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      xmlDoc = DOMUtil.getDocument();    

      // build fallback XML doc
      Document occasionDoc = DOMUtil.getDocumentBuilder().newDocument();
      Element occasionXML = (Element) occasionDoc.createElement("occasion");
      occasionDoc.appendChild(occasionXML);     
      XMLUtil.addElement((Document)occasionDoc,"occasionid", request.getParameter("r_occasion_id"));         
      XMLUtil.addElement((Document)occasionDoc,"description", request.getParameter("r_description"));         
      if(request.getParameter("r_active") == null) 
      {
        XMLUtil.addElement((Document)occasionDoc,"active", "N");         
      } else 
      {
        XMLUtil.addElement((Document)occasionDoc,"active", request.getParameter("r_active").equalsIgnoreCase("on") ? "Y" : "N");         
      }
      DOMUtil.addSection(xmlDoc,occasionDoc.getChildNodes()); 
      
      // Set initial page data
      this.pageData.put("next_occasion_seq", request.getParameter("next_occasion_seq")); 

      /* get request parameters */
      if(request.getParameter(R_OCCASION_ID)!=null)
      {
        occasionId = request.getParameter(R_OCCASION_ID);
      }
      else
      {
        //forward to error page
        logger.error("Missing occasion_id in OccasionMaintAction");
        return mapping.findForward(ACTN_ERROR);
      }
      
      if(request.getParameter(R_DESCRIPTION)!=null)
      {
        description = request.getParameter(R_DESCRIPTION);
      }
      else
      {
        //forward to error page
        logger.error("Missing description in OccasionMaintAction");
        return mapping.findForward(ACTN_ERROR);
      }
    
      if(request.getParameter(R_ACTIVE) == null) 
      {
        active = "N";
      } else {
        if(request.getParameter(R_ACTIVE).equalsIgnoreCase("on"))
        {
          active = "Y";
        }
        else
        {
          active = "N";
        }
      }
    
      /* check if the entered occasion exists */                  
      if(dao.occasionExists(occasionId))
      {
        /* populate hashmap with page data */
        this.pageData.put(VALID_FLAG,"N");
        this.pageData.put(ERROR_VALUE,"Entered occasion code already exists");
        
        /* convert the page data hashmap to XML and append it to the final XML */
        DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, this.pageData, true); 
        
        /* forward to addOccasionPopupXSL action */
        xslFile = getXSL(ADD_OCCASION_POPUP_XSL,mapping);                    
      }
      else /* check if the entered occasion contains 2 numeric characters */
      {
        boolean isValidOccasion = true;
        
        /* validate length */
        occasionId = occasionId.trim();
        if(occasionId.length() != 2)
        {
            isValidOccasion = false;
        }
    
        /* validate numeric */
        try
        {
            Integer.parseInt(occasionId);
        }
        catch(NumberFormatException e)
        {
            isValidOccasion = false;
        }
        
        if(isValidOccasion)
        {
          /* populate hashmap with page data */            
          this.pageData.put(VALID_FLAG,"Y");
          String displayValue = request.getParameter("next_occasion_seq");
          /* insert occasion */
          dao.insertOccasion(occasionId,description, displayValue, active);
          return performLoadOccasionAction(mapping, form, request, response);   
        }
        else
        {
          /* populate hashmap with page data */            
          this.pageData.put(VALID_FLAG,"N");
          this.pageData.put(ERROR_VALUE,"Occasion code must consist of two numerics");
        }
        
        /* convert the page data hashmap to XML and append it to the final XML */
        DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, this.pageData, true); 
        
        StringWriter sw = new StringWriter();      
        DOMUtil.print(xmlDoc, new PrintWriter(sw));
//        System.out.println(sw.toString()); //string representation of xml document
        logger.debug(sw.toString());
                
        xslFile = getXSL(ADD_OCCASION_POPUP_XSL,mapping);                   
      }

      /* save parameters onto request object */
			request.setAttribute("OccasionMaintenance",xmlDoc);
			request.setAttribute("OccasionMaintenanceXSL",xslFile);
      
      StringWriter sw = new StringWriter();       
      DOMUtil.print(xmlDoc, new PrintWriter(sw));
//      System.out.println(sw.toString()); //string representation of xml document

//      System.out.println(xslFile.getAbsolutePath());
      TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
    } catch (Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
    return null;
  }
  private ActionForward performLoadOccasionAction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
  {
    Document xmlDoc  = null;
    ActionForward forward = null;
    File xslFile = null;
    HashMap dataHash = new HashMap();        
    Connection conn = null;
    
		try
		{
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      /* pull base document */
      xmlDoc = DOMUtil.getDocument();    
      
      String userId = null;

      if(this.sessionId==null)
      {
        logger.error("Security token not found in request object");
      }
        
      /* retrieve user information */
      if(SecurityManager.getInstance().getUserInfo(this.sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(this.sessionId).getUserID();
      }

      /* retrieve occasion data and add it to root DOM */
      Document occasionDoc = (Document)dao.getOccasionXML();
      DOMUtil.addSection(xmlDoc, occasionDoc.getChildNodes());
      NodeList list = xmlDoc.getElementsByTagName("displayorder");
      int max = 0;
      for(int i = 0; i < list.getLength(); i++) 
      {
        Node current = list.item(i);
        String display = current.getFirstChild().getNodeValue();
        if(display != null && !display.equalsIgnoreCase("")) 
        {
          int dispInt = new Integer(display).intValue();
          if(dispInt > max) 
          {
            max = dispInt;
          }
        }
      }
      /* build page data */
      this.pageData.put("next_occasion_seq", new String((max + 1) + ""));

      /* check if user has access to edit occasions */
      boolean editAllowed = SecurityManager.getInstance().assertPermission(this.context,this.sessionId,"Occasion_Maint","Update");
              
      /* check if user has access to add occasions */
      boolean addAllowed = SecurityManager.getInstance().assertPermission(this.context,this.sessionId,"Occasion_Maint","Add");

             
      /* if user has access to edit or add, check if page is locked */
      if(editAllowed || addAllowed)
      {
        LockDAO lockDao = new LockDAO(conn);
        Document lockDoc = lockDao.checkLockXML(this.sessionId, userId, "OccasionMaint", "MaintScreen");
//        System.out.println(XMLUtil.convertDocToString(lockDoc));    
        NodeList lockList = lockDoc.getFirstChild().getChildNodes();
        String lockStatus = "N";
        
        // put the lock data in the pageData Node
        for(int i = 0; i < lockList.getLength(); i++) 
        {
          Node node = lockList.item(0);
          if(node.getNodeName().equalsIgnoreCase("OUT_LOCKED_IND")) 
          {
            lockStatus = node.getFirstChild().getNodeValue();
          }
          this.pageData.put(node.getNodeName(), node.getFirstChild().getNodeValue()); 
          
        }
        //if nobody is maintaining Occasions, set user access according to the role
        if(lockStatus.equals("N"))
        {
          this.pageData.put(EDIT_ALLOWED, editAllowed?"Y":"N"); 
          this.pageData.put(ADD_ALLOWED, addAllowed?"Y":"N"); 
        }
        //else, regardless of the role, set Add/Edit access to false
        else
        {
          // page was locked; set user access accordingly and lock message from SP 
          this.pageData.put(EDIT_ALLOWED, "N");
          this.pageData.put(ADD_ALLOWED, "N");
        }

      }
      //user doesnt have access to Add/Edit.  No need to check for locks; set Add/Edit to false
      else
      {
        this.pageData.put(EDIT_ALLOWED, "N");
        this.pageData.put(ADD_ALLOWED, "N");
      }

      /* convert the page data hashmap to XML and append it to the final XML */
      DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, this.pageData, true); 

      /* forward to addOccasionPopupXSL action */
      xslFile = getXSL(OCCASION_MAINTENANCE_XSL,mapping);
      
      /* save parameters onto request object */
			request.setAttribute("OccasionMaintenance",xmlDoc);
			request.setAttribute("OccasionMaintenanceXSL",xslFile);
      
      StringWriter sw = new StringWriter();       
      DOMUtil.print(xmlDoc, new PrintWriter(sw));
//      System.out.println(sw.toString()); //string representation of xml document
//      System.out.println(xslFile.getAbsolutePath());
      TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
       
    } catch (Exception e) 
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally 
    {
      try {
        conn.close();
      } catch (Exception e) 
      {
        // do nothing
      }
    }
    return null;
  }
  /**
	 * This is the Occasion Maintenance Action called from the Struts framework.
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    String actionType = null;
    this.pageData = new HashMap(); 
    
    /* get action type request parameter */
    if(request.getParameter(ACTION_TYPE)!=null)
    {
      actionType = request.getParameter(ACTION_TYPE);
    }
    else
    {
      actionType = this.LOAD_OCCASION_ACTION;
    }

    //****retrieve the context 
    if (request.getParameter("context") != null)
      this.context = request.getParameter("context");
    //store the security token
    this.pageData.put("context", this.context);

    //****retrieve the security token
    if (request.getParameter("securitytoken") != null)
      this.sessionId = request.getParameter("securitytoken");
    //store the security token
    this.pageData.put("securitytoken", this.sessionId);


  
    /* displays the add occasion popup */
    if(actionType.equals(DISPLAY_ADD_OCCASION_ACTION))
    {
      return performDisplayAddOccasion(mapping, form, request, response);
    }
  
    /* called by add occasion popup to validate the data */
    else if(actionType.equals(ADD_OCCASION_ACTION))
    { 
      return performAddOccasionAction(mapping, form, request, response);        
    }
  
    // saves changes on the main window
    else if(actionType.equals(SAVE_OCCASION_ACTION))
    {
      return performSaveOccasionAction(mapping, form, request, response);
    }
    
    // loads occasion maintenance page
    else if(actionType.equals(LOAD_OCCASION_ACTION))
    {             
      return performLoadOccasionAction(mapping, form, request, response);
    }
    
    // goes to the main menu
    else if(actionType.equals(RELEASE_LOCK_ACTION))
    {
      return performReleaseLockAction(mapping, form, request, response);
    }
    return null;
	}

  /**
  * get XSL file
  *
  * Retrieve name of CSZ Zipcode Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
    
	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
	 * @return Connection - db connection
	 * @throws ClassNotFoundException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,
															   DATASOURCE_NAME));

		return conn;
	}  
}