package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.InventoryTrackingMaintenanceBO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public class InventoryTrackingMaintenanceAction
  extends Action
{
  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static Logger logger = new Logger("com.ftd.op.maintenance.action.InventoryTrackingMaintenanceAction");

  //others
  private String OK_ERROR_MESSAGE;


  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public InventoryTrackingMaintenanceAction()
  {
  }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
  {

	HashMap inputMap  = new HashMap();
	HashMap responseMap = new HashMap();
	HashMap pageData = new HashMap();
    

    //request paramaters
    this.OK_ERROR_MESSAGE = null;
    Connection con = null;
    ActionForward forward = null;

    try
    {
      String forwardToType = "";
      String forwardToName = "";

      //Connection/Database info
      con = CommonUtil.getDBConnection();

      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = DOMUtil.getDocument();

      //process the request
      processRequest(request,response, inputMap, responseMap, pageData,con);

      //Get all the keys in the hashmap returned from the business object
      Set ks = responseMap.keySet();
      Iterator iter = ks.iterator();
      String key;

      //Iterate thru the hashmap returned from the business object using the keyset
      while (iter.hasNext())
      {
        key = iter.next().toString();
        //Append all the existing XMLs to the final XML
        Document xmlDoc = (Document) responseMap.get(key);
        NodeList nl = xmlDoc.getChildNodes();
        DOMUtil.addSection(responseDocument, nl);
      }

      //retrieve the forward info fromt the pageData, and remove it from the pageData. 
      if (pageData.get("forwardToType") != null)
      {
        forwardToType = pageData.get("forwardToType").toString();
        forwardToName = pageData.get("forwardToName").toString();

        pageData.remove("forwardToType");
        pageData.remove("forwardToName");
      }

      //Convert the page data hashmap to XML and append it to the final XML
      DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

      //check if we have to forward to another action, or just transform the XSL.
      if (forwardToType.equalsIgnoreCase("ACTION"))
      {
        forward = mapping.findForward(forwardToName);
        logger.info("forward: " + forward);
        return forward;
      }
      else if (forwardToType.equalsIgnoreCase("XSL"))
      {
        //Get XSL File name
        File xslFile = getXSL(forwardToName, mapping);
        if (xslFile != null && xslFile.exists())
        {
          //Transform the XSL File
          try
          {
            forward = mapping.findForward(forwardToName);
            String xslFilePathAndName = forward.getPath(); //get real file name

            // Retrieve the parameter map set by data filter and update
            //cbr_action with the next action.
            HashMap params = getParameters(request);

            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, xslFile, xslFilePathAndName, 
                                                           params);
          }
          catch (Exception e)
          {
            logger.error("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " transformation failed" + 
                         forwardToName, e);
            if (!response.isCommitted())
            {
              throw e;
            }
          }
        }
        else
        {
          throw new Exception("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " does not exist." + 
                              forwardToName);
        }

        return null;
      }
      else
      {
        return null;
      }
    }

    catch (Exception e)
    {
      logger.error(e);
      forward = mapping.findForward("ErrorPage");
      return forward;
    }

    finally
    {
      try
      {
        //Close the connection
        if (con != null)
        {
          con.close();
        }
      }
      catch (Exception e)
      {
        logger.error(e);
        forward = mapping.findForward("ErrorPage");
        return forward;
      }
    }
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  private void processRequest(HttpServletRequest request,HttpServletResponse response,
		  HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con )
    throws Exception
  {
    //Get info from the request object
    getRequestInfo(request, inputMap);

    //Get config file info
    getConfigInfo(inputMap);

    //process the action
    processAction(inputMap, pageData, responseMap, con, response);

    //populate the remainder of the fields on the page data
    populatePageData(inputMap, pageData);

  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  */
  private void getRequestInfo(HttpServletRequest request, HashMap inputMap)
    throws Exception
  {
    //request paramaters
    String requestActionType                 = null;
    String requestAdminAction                = null;
    String requestComments                   = null;
    String requestContext                    = null;
    String requestEndDate                    = null;
    String requestForecast                   = null;
    List   requestInvTrkEmailIdsToDelete     = new ArrayList();
    String requestInvTrkId                   = null;
    String requestNewEmailAddress            = null;
    String requestProductId                  = null;
    String requestRevised                    = null;
    String requestSecurityToken              = null;
    String requestShutdownThreshold          = null;
    String requestStartDate                  = null;
    String requestVendorId                   = null;
    String requestWarningThreshold           = null;
    String hSearchRequestVendorsChosen       = null;
    String hSearchRequestStartDate           = null;
    String hSearchRequestEndDate             = null;
    String hSearchRequestVendorProductStatus = null;
    String hSearchRequestProductStatus       = null;
    String hSearchRequestOnHand              = null;
    String hSearchProductIds                 = null;
    
    //****retrieve the 
    if (request.getParameter("action_type") != null && !request.getParameter("action_type").equalsIgnoreCase("")) 
      requestActionType = request.getParameter("action_type");

    //****retrieve the adminAction
    if (request.getParameter("adminAction") != null && !request.getParameter("adminAction").equalsIgnoreCase("")) 
      requestAdminAction = request.getParameter("adminAction");

    //****retrieve the taComments
    if (request.getParameter("taComments") != null && !request.getParameter("taComments").equalsIgnoreCase("")) 
      requestComments = request.getParameter("taComments");

    //****retrieve the context
    if (request.getParameter("context") != null && !request.getParameter("context").equalsIgnoreCase("")) 
      requestContext = request.getParameter("context");

    //****retrieve the tEndDate
    if (request.getParameter("tEndDate") != null && !request.getParameter("tEndDate").equalsIgnoreCase("")) 
      requestEndDate = request.getParameter("tEndDate");

    //****retrieve the hForecast
    if (request.getParameter("tForecast") != null && !request.getParameter("tForecast").equalsIgnoreCase("")) 
      requestForecast = request.getParameter("tForecast");
    
    //****retrieve the inv trk email ids to be deleted
    Enumeration enum1 = request.getParameterNames();
    String param = "";
    String cbId = "";
    String cbval = "";

    while (enum1.hasMoreElements())
    {
      param = (String) enum1.nextElement();
      if (param.startsWith("cb_"))
      {
        cbId = param.substring(3);
        cbval = request.getParameter("cbval_" + cbId);
        requestInvTrkEmailIdsToDelete.add(cbval);
      }
    }

    //****retrieve the hInvTrkId
    if (request.getAttribute("hInvTrkId") != null && !((String)request.getAttribute("hInvTrkId")).equalsIgnoreCase("")) 
      requestInvTrkId = (String)request.getAttribute("hInvTrkId");
    else if (request.getParameter("hInvTrkId") != null && !request.getParameter("hInvTrkId").equalsIgnoreCase("")) 
      requestInvTrkId = request.getParameter("hInvTrkId");

    //****retrieve the tNewEmailAddress
    if (request.getParameter("tNewEmailAddress") != null && !request.getParameter("tNewEmailAddress").equalsIgnoreCase("")) 
      requestNewEmailAddress = request.getParameter("tNewEmailAddress");

    //****retrieve the tProductId
    if (request.getParameter("tProductId") != null && !request.getParameter("tProductId").equalsIgnoreCase("")) 
      requestProductId = request.getParameter("tProductId");

    //****retrieve the hRevised
    if (request.getParameter("tRevised") != null && !request.getParameter("tRevised").equalsIgnoreCase("")) 
      requestRevised = request.getParameter("tRevised");

    //****retrieve the securitytoken
    if (request.getParameter("securitytoken") != null && !request.getParameter("securitytoken").equalsIgnoreCase("")) 
      requestSecurityToken = request.getParameter("securitytoken");

    //****retrieve the tShutdownThreshold
    if (request.getParameter("tShutdownThreshold") != null && !request.getParameter("tShutdownThreshold").equalsIgnoreCase("")) 
      requestShutdownThreshold = request.getParameter("tShutdownThreshold");

    //****retrieve the tStartDate
    if (request.getParameter("tStartDate") != null && !request.getParameter("tStartDate").equalsIgnoreCase("")) 
      requestStartDate = request.getParameter("tStartDate");

    //****retrieve the sVendorId
    if (request.getParameter("sVendorId") != null && !request.getParameter("sVendorId").equalsIgnoreCase("")) 
      requestVendorId = request.getParameter("sVendorId");

    //****retrieve the tWarningThreshold
    if (request.getParameter("tWarningThreshold") != null && !request.getParameter("tWarningThreshold").equalsIgnoreCase("")) 
      requestWarningThreshold = request.getParameter("tWarningThreshold");
    
    //****retrieve the hVendorsChosen
    if (request.getAttribute("hSearchRequestVendorsChosen") != null && !((String)request.getAttribute("hSearchRequestVendorsChosen")).equalsIgnoreCase("")) 
    {
      hSearchRequestVendorsChosen = (String)request.getAttribute("hSearchRequestVendorsChosen");
    }
    else if (request.getParameter("hSearchRequestVendorsChosen") != null && !request.getParameter("hSearchRequestVendorsChosen").equalsIgnoreCase("")) 
    {
      hSearchRequestVendorsChosen = request.getParameter("hSearchRequestVendorsChosen");
    }
    //****retrieve the searchRequestStartDate
    if (request.getAttribute("hSearchRequestStartDate") != null && !((String)request.getAttribute("hSearchRequestStartDate")).equalsIgnoreCase("") ) 
    {
      hSearchRequestStartDate = (String)request.getAttribute("hSearchRequestStartDate");
    }
    else if (request.getParameter("hSearchRequestStartDate") != null && !request.getParameter("hSearchRequestStartDate").equalsIgnoreCase("")) 
    {
      hSearchRequestStartDate = request.getParameter("hSearchRequestStartDate");
    }
    //****retrieve the searchRequestEndDate
    if (request.getAttribute("hSearchRequestEndDate") != null && !((String)request.getAttribute("hSearchRequestEndDate")).equalsIgnoreCase("")) 
    {
      hSearchRequestEndDate = (String)request.getAttribute("hSearchRequestEndDate");
    }
    else if (request.getParameter("hSearchRequestEndDate") != null && !request.getParameter("hSearchRequestEndDate").equalsIgnoreCase("")) 
    {
      hSearchRequestEndDate = request.getParameter("hSearchRequestEndDate");
    }
    //****retrieve the vendor status
    if (request.getAttribute("hSearchRequestProductStatus") != null && !((String)request.getAttribute("hSearchRequestProductStatus")).equalsIgnoreCase("")) 
    {
      hSearchRequestProductStatus = (String)request.getAttribute("hSearchRequestProductStatus");
    }
    else if (request.getParameter("hSearchRequestProductStatus") != null && !request.getParameter("hSearchRequestProductStatus").equalsIgnoreCase("")) 
    {
      hSearchRequestProductStatus = request.getParameter("hSearchRequestProductStatus");
    }
    //****retrieve the vendor product status
    if (request.getAttribute("hSearchRequestVendorProductStatus") != null && !((String)request.getAttribute("hSearchRequestVendorProductStatus")).equalsIgnoreCase("")) 
    {
      hSearchRequestVendorProductStatus = (String)request.getAttribute("hSearchRequestVendorProductStatus");
    }
    else if (request.getParameter("hSearchRequestVendorProductStatus") != null && !request.getParameter("hSearchRequestVendorProductStatus").equalsIgnoreCase("")) 
    {
      hSearchRequestVendorProductStatus = request.getParameter("hSearchRequestVendorProductStatus");
    }
    
    //****retrieve the product ids
    if (request.getAttribute("hSearchProductIds") != null && !((String)request.getAttribute("hSearchProductIds")).equalsIgnoreCase("")) 
    {
      hSearchProductIds = (String)request.getAttribute("hSearchProductIds");
    }
    else if (request.getParameter("hSearchProductIds") != null && !request.getParameter("hSearchProductIds").equalsIgnoreCase("")) 
    {
      hSearchProductIds = request.getParameter("hSearchProductIds");
    }
    
    //****retrieve the on hand number
    if (request.getAttribute("hSearchRequestOnHand") != null && !((String)request.getAttribute("hSearchRequestOnHand")).equalsIgnoreCase("")) 
    {
      hSearchRequestOnHand = (String)request.getAttribute("hSearchRequestOnHand");
    }
    else if (request.getParameter("hSearchRequestOnHand") != null && !request.getParameter("hSearchRequestOnHand").equalsIgnoreCase("")) 
    {
      hSearchRequestOnHand = request.getParameter("hSearchRequestOnHand");
    }

    inputMap.put("requestActionType",                 requestActionType);
    inputMap.put("requestAdminAction",                requestAdminAction);
    inputMap.put("requestComments",                   requestComments);
    inputMap.put("requestContext",                    requestContext);
    inputMap.put("requestEndDate",                    requestEndDate);
    inputMap.put("requestForecast",                   requestForecast);
    inputMap.put("requestInvTrkEmailIdsToDelete",     requestInvTrkEmailIdsToDelete);
    inputMap.put("requestInvTrkId",                   requestInvTrkId);
    inputMap.put("requestNewEmailAddress",            requestNewEmailAddress);
    inputMap.put("requestProductId",                  requestProductId);
    inputMap.put("requestRevised",                    requestRevised);
    inputMap.put("requestSecurityToken",              requestSecurityToken);
    inputMap.put("requestShutdownThreshold",          requestShutdownThreshold);
    inputMap.put("requestStartDate",                  requestStartDate);
    inputMap.put("requestVendorId",                   requestVendorId);
    inputMap.put("requestWarningThreshold",           requestWarningThreshold);
    inputMap.put("hSearchRequestVendorsChosen",       hSearchRequestVendorsChosen);
    inputMap.put("hSearchRequestStartDate",           hSearchRequestStartDate);
    inputMap.put("hSearchRequestEndDate",             hSearchRequestEndDate);
    inputMap.put("hSearchRequestProductStatus",       hSearchRequestProductStatus);
    inputMap.put("hSearchRequestVendorProductStatus", hSearchRequestVendorProductStatus);
    inputMap.put("hSearchProductIds",                 hSearchProductIds);
    inputMap.put("hSearchRequestOnHand",              hSearchRequestOnHand);
  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  */
  private void getConfigInfo(HashMap inputMap)
    throws Exception
  {
    String csrId = null; 
    String securityToken = (String) inputMap.get("requestSecurityToken"); 
    
    /* retrieve user information */
    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
    {
      csrId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
    }

    inputMap.put("csrId",         csrId);

    // TODO - retrieve stuff for novator feed environment 

  }


  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction(HashMap inputMap, HashMap pageData,HashMap responseMap, Connection con, HttpServletResponse response)
    throws Exception
  {
    InventoryTrackingMaintenanceBO itmBO = new InventoryTrackingMaintenanceBO(); 
    String requestActionType = "";
    try {
    	
    requestActionType = (String)inputMap.get("requestActionType");
    
    //If the action equals "add"
    if (requestActionType.equalsIgnoreCase("add"))
    {
      itmBO.processAdd(inputMap, pageData);
    }
    //If the action equals "edit"
    else if (requestActionType.equalsIgnoreCase("edit"))
    {
      itmBO.processEdit(inputMap, responseMap, pageData, con);
    }
    //If the action equals "copy"
    else if (requestActionType.equalsIgnoreCase("copy"))
    {
      itmBO.processCopyEdit(inputMap, responseMap, pageData, con);
    }
    //If the action equals "product_vendor"
    else if (requestActionType.equalsIgnoreCase("product_vendor"))
    {
      itmBO.processProductVendor(inputMap, response, con);
    }
    //If the action equals "main_menu"
    else if (requestActionType.equalsIgnoreCase("main_menu"))
    {
      itmBO.processMainMenu(inputMap, pageData, con);
    }
    //If the action equals "dashboard"
    else if (requestActionType.equalsIgnoreCase("dashboard"))
    {
      itmBO.processDashboard(inputMap, pageData, con);
    }
    //If the action equals "save"
    else if (requestActionType.equalsIgnoreCase("save"))
    {
      itmBO.processSave(inputMap, responseMap, pageData, con);
    }
    //If the action is not found as above, throw a new exception
    else
    {
      throw new Exception("Invalid Action Type - Please correct");
    }
    } catch (Exception e) {
    	logger.error("Error caught for the request action: " + requestActionType +", " + e.getMessage());
		// set the error message if not already set.
		if(pageData.get("errorMessage") == null) {
			pageData.put("errorMessage", "Unable to process the request.  Please try again later. \n");
		}
		// Set the forward page to inventory error page if not set already.
		if(pageData.get("forwardToName") == null) {
			pageData.put("forwardToType", "XSL");
			pageData.put("forwardToName", "InventoryErrorPage");
		}	
    }
  }


  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  */
  private void populatePageData(HashMap inputMap, HashMap pageData) throws Exception
  {
    //store the admin action
    pageData.put("adminAction", (String) inputMap.get("requestAdminAction") );

    //store the security token
    pageData.put("context", (String) inputMap.get("requestContext") );

    //store the filter
    //this.pageData.put("filter", (String) this.inputMap.get("requestTripFilter") );

    pageData.put("OK_ERROR_MESSAGE", OK_ERROR_MESSAGE);

    //store the security token
    pageData.put("securitytoken", (String) inputMap.get("requestSecurityToken") );

    //store search values

    pageData.put("hSearchRequestVendorsChosen", (String) inputMap.get("hSearchRequestVendorsChosen") );
    pageData.put("hSearchRequestStartDate", (String) inputMap.get("hSearchRequestStartDate") );
    pageData.put("hSearchRequestEndDate", (String) inputMap.get("hSearchRequestEndDate") );
    pageData.put("hSearchRequestProductStatus", (String) inputMap.get("hSearchRequestProductStatus") );
    pageData.put("hSearchRequestVendorProductStatus", (String) inputMap.get("hSearchRequestVendorProductStatus") );
    pageData.put("hSearchProductIds", (String) inputMap.get("hSearchProductIds") );
    pageData.put("hSearchRequestOnHand", (String) inputMap.get("hSearchRequestOnHand") );
   
  }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  */
  private

  File getXSL(String forwardToName, ActionMapping mapping)
  {
    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(forwardToName);
    xslFilePathAndName = forward.getPath(); //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


  /**
     * retrieve the attribute �parameters� HashMap from request
     * Return the HashMap.
     * @param request - HttpServletRequest
     * @return HashMap
     */
  private HashMap getParameters(HttpServletRequest request)
  {
    HashMap parameters = new HashMap();

    try
    {
      String action = request.getParameter("action_type");
      parameters = (HashMap) request.getAttribute("parameters");
      parameters.put("action", action);

    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getParameters");
      }
    }

    return parameters;
  }


    /**
     * Query the reques for non-multipart/form-data request, build a HashMap of input request parms, and return the HashMap
     * @param request HttpServletRequest
     * @return HashMap
     * @throws Exception
     */
    private HashMap retrieveRequestParms(HttpServletRequest request) throws Exception
    {
      HashMap requestMap = new HashMap();

      for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); )
      {
        String key = (String) i.nextElement();
        String value = request.getParameter(key);

        if ((value != null) && (value.trim().length() > 0))
        {
          requestMap.put(key, value.trim());
        }
      }

      return requestMap;
    }

}