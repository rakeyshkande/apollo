package com.ftd.op.maintenance.action;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.op.maintenance.bo.InventoryTrackingDashboardBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.InventoryTrackingUploadSpreadsheetRowVO;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.util.ServletHelper;
import com.ftd.util.fileprocessing.FileProcessorUtil;


public class InventoryTrackingDashboardAction
  extends Action
{
  /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private static Logger logger = new Logger("com.ftd.op.maintenance.action.InventoryTrackingDashboardAction");

  /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public InventoryTrackingDashboardAction()
  {
  }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
  {

	long startTime = System.currentTimeMillis();
    HashMap inputMap  = new HashMap();
    HashMap responseMap = new HashMap();
    HashMap pageData = new HashMap();
    
    Connection con = null;
    ActionForward forward = null;

    try
    { 

      String forwardToType = "";
      String forwardToName = "";

      //Connection/Database info
      con = CommonUtil.getDBConnection();
      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = DOMUtil.getDocument();

      //process the request
      processRequest(request, inputMap, responseMap, pageData, con);

      //Get all the keys in the hashmap returned from the business object
      Set ks = responseMap.keySet();
      Iterator iter = ks.iterator();
      String key;

      //Iterate thru the hashmap returned from the business object using the key set
      while (iter.hasNext())
      {
        key = iter.next().toString();
        //Append all the existing XMLs to the final XML
        Document xmlDoc = (Document) responseMap.get(key);
        NodeList nl = xmlDoc.getChildNodes();
        DOMUtil.addSection(responseDocument, nl);
      }

      //retrieve the forward info fromt the pageData, and remove it from the pageData. 
      if (pageData.get("forwardToType") != null)
      {
        forwardToType = pageData.get("forwardToType").toString();
        forwardToName = pageData.get("forwardToName").toString();

        pageData.remove("forwardToType");
        pageData.remove("forwardToName");
      }

      //Convert the page data hashmap to XML and append it to the final XML
      DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

      //check if we have to forward to another action, or just transform the XSL.
      if (forwardToType.equalsIgnoreCase("ACTION"))
      {
        forward = mapping.findForward(forwardToName);
        logger.info("forward: " + forward);
        return forward;
      }
      else if (forwardToType.equalsIgnoreCase("XSL"))
      {
    	long xslTransformStartTime = System.currentTimeMillis();
        //Get XSL File name
        File xslFile = getXSL(forwardToName, mapping);
        if (xslFile != null && xslFile.exists())
        {
          //Transform the XSL File
          try
          {
            forward = mapping.findForward(forwardToName);
            String xslFilePathAndName = forward.getPath(); //get real file name

            // Retrieve the parameter map set by data filter and update
            //cbr_action with the next action.
            HashMap params = getParameters(request);

            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, xslFile, xslFilePathAndName, 
                                                           params);
            long xslTransformEndTime = System.currentTimeMillis();
            logger.debug("execute() for transforming XML took " + (xslTransformEndTime - xslTransformStartTime) + " milli seconds");
          }
          catch (Exception e)
          {
            logger.error("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " transformation failed" + 
                         forwardToName, e);
            if (!response.isCommitted())
            {
              throw e;
            }
          }
        }
        else
        {
          throw new Exception("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " does not exist." + 
                              forwardToName);
        }

        return null;
      }
      else
      {
        return null;
      }
    }

    catch (Exception e)
    {
      logger.error(e);
      forward = mapping.findForward("ErrorPage");
      return forward;
    }

    finally
    {
      try
      {
        //Close the connection
        if (con != null)
        {
          con.close();
        }
        
        long endTime = System.currentTimeMillis();
        logger.debug("execute() for action took " + (endTime - startTime) + " milli seconds");
      }
      catch (Exception e)
      {
        logger.error(e);
        forward = mapping.findForward("ErrorPage");
        return forward;
      }
    }
  }


  /*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  private void processRequest(HttpServletRequest request, HashMap inputMap, HashMap responseMap, HashMap pageData, Connection con)
    throws Exception
  {
    //Get info from the request object
    getRequestInfo(request, inputMap);

    //Get config file info
    getConfigInfo(inputMap);

    //process the action
    processAction(request, inputMap, responseMap, pageData, con);

    //add Novator environment
    processNovatorEnvironment(responseMap);

    //populate the remainder of the fields on the page data
    populatePageData(inputMap,pageData);

  }


  /*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  */
  private void getRequestInfo(HttpServletRequest request, HashMap inputMap)
    throws Exception
  {
    //request paramaters
    String requestActionType = null;
    String requestAdminAction = null;
    String requestContext = null;
    String requestEndDate = null;
    List requestInvTrkIds = new ArrayList();
    String requestOnHand = null;
    List requestProductIds = new ArrayList();
    String requestProductStatus = null;
    String requestSecurityToken = null;
    String requestStartDate = null;
    List requestVendorIdsChosen = new ArrayList();
    List requestUploadList = null;
    String requestVendorProductStatus = null;
    String hVendorsChosen = null;
    String taProductIds = null;


    HashMap requestMap = new HashMap();
    if (ServletHelper.isMultipartFormData(request)) 
    {
      requestMap = ServletHelper.getMultipartParam(request);
      logger.debug("multipart form detected");
    }
    else
    {
      requestMap = this.retrieveRequestParms(request);
      logger.debug("non-multipart form detected");
    }


    //****retrieve the action type
    if (requestMap.get("action_type") != null && !((String)requestMap.get("action_type")).equalsIgnoreCase(""))
      requestActionType = (String)requestMap.get("action_type");

    //****retrieve the adminAction
    if (requestMap.get("adminAction") != null && !((String)requestMap.get("adminAction")).equalsIgnoreCase(""))
      requestAdminAction = (String)requestMap.get("adminAction");

    //****retrieve the context
    if (requestMap.get("context") != null && !((String)requestMap.get("context")).equalsIgnoreCase(""))
      requestContext = (String)requestMap.get("context");

    //****retrieve the end date
    
    if ((requestMap.get("tEndDate") != null && !((String)requestMap.get("tEndDate")).equalsIgnoreCase("")) ||
      (requestMap.get("hSearchRequestEndDate") != null && !((String)requestMap.get("hSearchRequestEndDate")).equalsIgnoreCase("")))
    {
      if ((requestMap.get("tEndDate") != null && !((String)requestMap.get("tEndDate")).equalsIgnoreCase(""))) 
      {
         requestEndDate = (String) requestMap.get("tEndDate");
      }
      else
      {
        requestEndDate = (String) requestMap.get("hSearchRequestEndDate");
      }
    }
                                                                                                                                                                                                                                                                                                                                                                                                                               
    //****retrieve the inv trk email ids to be deleted
    Set ks = requestMap.keySet();
    Iterator iter = ks.iterator();
    String param = "";
    String cbId = "";
    String cbval = "";

    while (iter.hasNext())
    {
      param = iter.next().toString();
      if (param.startsWith("cb_"))
      {
        cbId = param.substring(3);
        cbval = (String)requestMap.get("cbval_" + cbId);
        requestInvTrkIds.add(cbval);
      }
    }

    //****retrieve the on hand
    if (requestMap.get("tOnHand") != null && !((String)requestMap.get("tOnHand")).equalsIgnoreCase("") ||
           (requestMap.get("hSearchRequestOnHand") != null && !((String)requestMap.get("hSearchRequestOnHand")).equalsIgnoreCase("")) )
    {
      if (requestMap.get("tOnHand") != null && !((String)requestMap.get("tOnHand")).equalsIgnoreCase(""))
      {
        requestOnHand = (String) requestMap.get("tOnHand");
      }
      else
      { 
        requestOnHand = (String) requestMap.get("hSearchRequestOnHand");
      }
    }  

    //****retrieve the product ids
    if (requestMap.get("taProductIds") != null && !((String)requestMap.get("taProductIds")).equalsIgnoreCase("") ||
         (requestMap.get("hSearchProductIds") != null && !((String)requestMap.get("hSearchProductIds")).equalsIgnoreCase("")) )
    {
      String productIds = null;
      if (requestMap.get("taProductIds") != null && !((String)requestMap.get("taProductIds")).equalsIgnoreCase(""))
      {                 
        productIds = (String) requestMap.get("taProductIds");
      }
      else
      {
        productIds = (String) requestMap.get("hSearchProductIds");
      }
      StringTokenizer stp = new StringTokenizer(productIds, "\n");
      while (stp.hasMoreTokens())
      {
        String productId = stp.nextToken().trim();
        requestProductIds.add(productId.toUpperCase());
      }
      taProductIds = productIds;
    }

    //****retrieve the status
    if (requestMap.get("sProductStatus") != null && !((String)requestMap.get("sProductStatus")).equalsIgnoreCase("") ||
         (requestMap.get("hSearchRequestProductStatus") != null && !((String)requestMap.get("hSearchRequestProductStatus")).equalsIgnoreCase("")) )
    {
       if (requestMap.get("sProductStatus") != null && !((String)requestMap.get("sProductStatus")).equalsIgnoreCase(""))
       {
        requestProductStatus = (String) requestMap.get("sProductStatus");
       }
       else 
       {
         requestProductStatus = (String) requestMap.get("hSearchRequestProductStatus");
       }
    }  
    
    //****retrieve the security token
    if (requestMap.get("securitytoken") != null && !((String)requestMap.get("securitytoken")).equalsIgnoreCase(""))
      requestSecurityToken = (String)requestMap.get("securitytoken");

    //****retrieve the start date
    if ((requestMap.get("tStartDate") != null && !((String)requestMap.get("tStartDate")).equalsIgnoreCase("")) ||
      (requestMap.get("hSearchRequestStartDate") != null && !((String)requestMap.get("hSearchRequestStartDate")).equalsIgnoreCase("")))
    {
      if (requestMap.get("tStartDate") != null && !((String)requestMap.get("tStartDate")).equalsIgnoreCase(""))
      {
        requestStartDate = (String) requestMap.get("tStartDate");
      }
      else
      {
        requestStartDate = (String) requestMap.get("hSearchRequestStartDate");
      }
    }

    //****retrieve the vendor_ids
    if (requestMap.get("hVendorsChosen") != null && !((String)requestMap.get("hVendorsChosen")).equalsIgnoreCase("") ||
         (requestMap.get("hSearchRequestVendorsChosen") != null && !((String)requestMap.get("hSearchRequestVendorsChosen")).equalsIgnoreCase("")) )
    {
      String vendorIds = null;
      if (requestMap.get("hVendorsChosen") != null && !((String)requestMap.get("hVendorsChosen")).equalsIgnoreCase(""))
      {
        vendorIds = (String) requestMap.get("hVendorsChosen");
      }
      else
      {
        vendorIds = (String) requestMap.get("hSearchRequestVendorsChosen");
      }

      StringTokenizer stv = new StringTokenizer(vendorIds, ",");
      while (stv.hasMoreTokens())
      {
        String vendorId = stv.nextToken().trim();
        requestVendorIdsChosen.add(vendorId);
      }
      hVendorsChosen =  vendorIds;
    }

    if (requestMap.get("hInputExcelFileName") != null && !((String)requestMap.get("hInputExcelFileName")).equalsIgnoreCase(""))
    {
      requestUploadList = retrieveFile(requestMap);
      inputMap.put("requestUploadList", requestUploadList);
    }

    //****retrieve the status
    if (requestMap.get("sVendorProductStatus") != null && !((String)requestMap.get("sVendorProductStatus")).equalsIgnoreCase("") ||
         (requestMap.get("hSearchRequestVendorProductStatus") != null && !((String)requestMap.get("hSearchRequestVendorProductStatus")).equalsIgnoreCase("")) )
    {
      if (requestMap.get("sVendorProductStatus") != null && !((String)requestMap.get("sVendorProductStatus")).equalsIgnoreCase(""))
      {
        requestVendorProductStatus = (String) requestMap.get("sVendorProductStatus");
      }
      else
      {
        requestVendorProductStatus = (String) requestMap.get("hSearchRequestVendorProductStatus");
      }
    }
     
    NovatorUpdateVO novatorUpdateVO = buildNovatorUpdateVO(requestMap);

    inputMap.put("requestActionType", requestActionType);
    inputMap.put("requestAdminAction", requestAdminAction);
    inputMap.put("requestContext", requestContext);
    inputMap.put("requestEndDate", requestEndDate);
    inputMap.put("requestInvTrkIds", requestInvTrkIds);
    inputMap.put("requestNovatorUpdateVO", novatorUpdateVO);
    inputMap.put("requestOnHand", requestOnHand);
    inputMap.put("requestProductIds", requestProductIds);
    inputMap.put("requestProductStatus", requestProductStatus);
    inputMap.put("requestSecurityToken", requestSecurityToken);
    inputMap.put("requestStartDate", requestStartDate);
    inputMap.put("requestVendorIdsChosen", requestVendorIdsChosen);
    inputMap.put("requestVendorProductStatus", requestVendorProductStatus);
    inputMap.put("hVendorsChosen", hVendorsChosen);
    inputMap.put("taProductIds", taProductIds);
    
    //set page detail
    if(!StringUtils.isEmpty((String)requestMap.get("paginationAction"))) {
    	inputMap.put("paginationAction", requestMap.get("paginationAction"));
    }
    if(!StringUtils.isEmpty((String)requestMap.get("hCurrentpage"))) {
    	inputMap.put("hCurrentpage", requestMap.get("hCurrentpage"));
    }
    if(!StringUtils.isEmpty((String)requestMap.get("pageSize"))) {
    	inputMap.put("pageSize", requestMap.get("pageSize"));
    }
    if(!StringUtils.isEmpty((String)requestMap.get("hRecordCount"))) {
    	inputMap.put("hRecordCount", requestMap.get("hRecordCount"));
    }

  }


  /*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  */
  private void getConfigInfo(HashMap inputMap)
    throws Exception
  {
    String csrId = null;
    String context = (String) inputMap.get("requestContext");
    String securityToken = (String) inputMap.get("requestSecurityToken");

    /* retrieve user information */
    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
    {
      csrId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
    }

    inputMap.put("csrId", csrId);


    String updateAllowed = null;
    //check if user has edit abilities
    SecurityManager securityManager = SecurityManager.getInstance();
    if (securityManager.assertPermission(context, securityToken, "Inventory Tracking", OPMConstants.SECURITY_UPDATE))
    {
      updateAllowed = "Y";
    }
    else
    {
      updateAllowed = "N";
    }                
           
    inputMap.put("updateAllowed", updateAllowed);


  }


  /*******************************************************************************************
  * processNovatorEnvironment()
  ******************************************************************************************
  *
  */
  private void processNovatorEnvironment(HashMap responseMap)
    throws Exception
  {

    NovatorFeedUtil feedUtil = new NovatorFeedUtil();
    Document updateSetupDoc = feedUtil.getNovatorEnvironmentSetup();

    responseMap.put("HK_novator_environments", updateSetupDoc);

  }

  /**
     * Build a list of IOTW program objects from XML
     * @param requestMap HashMap
     * @return NovatorUpdateVO containing Novator feed information
     */
  private NovatorUpdateVO buildNovatorUpdateVO(HashMap requestMap)
  {
    NovatorUpdateVO vo = new NovatorUpdateVO();
    String updateContent = (String)requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
    if (updateContent != null && updateContent.equals("on"))
    {
      vo.setUpdateContent(true);
    }
    String updateLive = (String)requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
    if (updateLive != null && updateLive.equals("on"))
    {
      vo.setUpdateLive(true);
    }
    String updateTest = (String)requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_TEST);
    if (updateTest != null && updateTest.equals("on"))
    {
      vo.setUpdateTest(true);
    }
    String updateUAT = (String)requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_UAT);
    if (updateUAT != null && updateUAT.equals("on"))
    {
      vo.setUpdateUAT(true);
    }

    return vo;
  }

  /*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
	private void processAction(HttpServletRequest request, HashMap inputMap,
			HashMap responseMap, HashMap pageData, Connection con) throws Exception {
		InventoryTrackingDashboardBO itdBO = new InventoryTrackingDashboardBO();
		String requestActionType = (String) inputMap.get("requestActionType");
		try {
			// If the action equals "add"
			if (requestActionType.equalsIgnoreCase("load") || requestActionType.equalsIgnoreCase("dashboard")) {
				itdBO.processLoad(responseMap, pageData, con);
				itdBO.processVendors(inputMap, responseMap, con);
			}
			
			// If the action equals "cancel_trip"
			else if (requestActionType.equalsIgnoreCase("search")) {
				itdBO.processSearch(inputMap, responseMap, pageData, con);
			}
			
			// If the action equals taken
			else if (requestActionType.equalsIgnoreCase("taken")) {
				itdBO.processTaken(inputMap, responseMap, pageData, con);
			}
			
			// If the action equals "main_menu"
			else if (requestActionType.equalsIgnoreCase("main_menu")) {
				itdBO.processMainMenu(pageData);
			}
			
			// If the action equals coad
			else if (requestActionType.equalsIgnoreCase("coad")) {
				itdBO.processCOAD(inputMap, responseMap, pageData, con);
			}
			
			// If the action equals delete
			else if (requestActionType.equalsIgnoreCase("delete")) {
				itdBO.processDelete(inputMap, responseMap, pageData, con);
			}
			
			// If the action equals edit
			else if (requestActionType.equalsIgnoreCase("edit")) {
				itdBO.processEdit(inputMap, responseMap, pageData, con);
				itdBO.processVendors(inputMap, responseMap, con);

				List invTrkIds = (ArrayList) inputMap.get("requestInvTrkIds");
				String invTrkId = (String) invTrkIds.get(0);
				request.setAttribute("hInvTrkId", invTrkId);
				request.setAttribute("hSearchRequestVendorsChosen", inputMap.get("hVendorsChosen"));
				request.setAttribute("hSearchRequestStartDate", inputMap.get("requestStartDate"));
				request.setAttribute("hSearchRequestEndDate", inputMap.get("requestEndDate"));
				request.setAttribute("hSearchRequestProductStatus", inputMap.get("requestProductStatus"));
				request.setAttribute("hSearchRequestVendorProductStatus", inputMap.get("requestVendorProductStatus"));
				request.setAttribute("hSearchRequestOnHand", inputMap.get("requestOnHand"));
				request.setAttribute("hSearchProductIds", inputMap.get("taProductIds"));
			}
			
			// If the action equals ma
			else if (requestActionType.equalsIgnoreCase("ma")) {
				itdBO.processMA(inputMap, responseMap, pageData, con);
			}
			
			// If the action equals mu
			else if (requestActionType.equalsIgnoreCase("mu")) {
				itdBO.processMU(inputMap, responseMap, pageData, con);
			}
			
			// If the action equals "upload"
			else if (requestActionType.equalsIgnoreCase("upload")) {
				itdBO.processUpload(inputMap, responseMap, pageData, con);
			}
			
			// If the action is not found as above, throw a new exception
			else {
				throw new Exception("Invalid Action Type - Please correct");
			}
			
		} catch (Exception e) {
			logger.error("Error caught for the request action: " + requestActionType +", " + e.getMessage());
			// set the error message if not already set.
			if(pageData.get("errorMessage") == null) {
				pageData.put("errorMessage", "Unable to process the request.  Please try again later. \n");
			}
			// Set the forward page to inventory error page if not set already.
			if(pageData.get("forwardToName") == null) {
				pageData.put("forwardToType", "XSL");
				pageData.put("forwardToName", "InventoryErrorPage");
			}
		}

	}


  /*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  */
  private void populatePageData(HashMap inputMap, HashMap pageData)
    throws Exception
  {
    //store the admin action
    pageData.put("adminAction", (String) inputMap.get("requestAdminAction"));

    //store the security token
    pageData.put("context", (String) inputMap.get("requestContext"));

    //store the on hand
    pageData.put("onHand", inputMap.get("requestOnHand"));

    //store the action type
    pageData.put("original_action_type", (String) inputMap.get("requestActionType"));

    //convert and store the product ids
    pageData.put("productIds", 
                      CommonUtil.convertListToString((ArrayList) inputMap.get("requestProductIds"), "\n", false));

    //store the product status
    pageData.put("productStatus", inputMap.get("requestProductStatus"));

    //store the start date
    pageData.put("startDate", inputMap.get("requestStartDate"));

    //store the end date
    pageData.put("endDate", inputMap.get("requestEndDate"));

    //store the security token
    pageData.put("securitytoken", (String) inputMap.get("requestSecurityToken"));

    //store the vendor status
    pageData.put("vendorProductStatus", inputMap.get("requestVendorProductStatus"));

    //store the updateAllowed
    pageData.put("updateAllowed", (String) inputMap.get("updateAllowed") );
    
  }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  */
  private

  File getXSL(String forwardToName, ActionMapping mapping)
  {
    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(forwardToName);
    xslFilePathAndName = forward.getPath(); //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


  /**
     * retrieve the attribute �parameters� HashMap from request
     * Return the HashMap.
     * @param request - HttpServletRequest
     * @return HashMap
     */
  private HashMap getParameters(HttpServletRequest request)
  {
    HashMap parameters = new HashMap();

    try
    {
      String action = request.getParameter("action_type");
      parameters = (HashMap) request.getAttribute("parameters");
      parameters.put("action", action);

    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getParameters");
      }
    }

    return parameters;
  }



  /**
   * Query the reques for non-multipart/form-data request, build a HashMap of input request parms, and return the HashMap
   * @param request HttpServletRequest
   * @return HashMap
   * @throws Exception
   */
  private HashMap retrieveRequestParms(HttpServletRequest request) throws Exception
  {
    HashMap requestMap = new HashMap();

    for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); )
    {
      String key = (String) i.nextElement();
      String value = request.getParameter(key);

      if ((value != null) && (value.trim().length() > 0))
      {
        requestMap.put(key, value.trim());
      }
    }

    return requestMap;
  }


  /**
   * Check the input Map for a file item.  If exists, create a list of InventoryTrackingUploadSpreadsheetRowVO and return the list
   * @param requestMap HashMap
   * @return List 
   * @throws Exception
   */
   private List retrieveFile(HashMap requestMap) throws Exception 
   {
    FileItem fileItem = (FileItem) requestMap.get("file_item");
    if (fileItem == null)
    {
      throw new RuntimeException("processFileUpload: Upload data file can not be null.");
    }
    else
    {
      return FileProcessorUtil.getInstance().processSpreadsheetFile(fileItem.getInputStream(), InventoryTrackingUploadSpreadsheetRowVO.class);
    }
    
   }

}