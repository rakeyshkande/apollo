package com.ftd.op.maintenance.action;


import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class redirects the user to either the Main Menu page or the Call Code Reason page. 
 *
 * @author Ali Lakhani
 */
 


public final class MainMenuAction extends Action 
{

    private static    Logger logger  = new Logger("com.ftd.customerordermanagement.actions.MainMenuAction");

  /******************************************************************************
  * execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
  *******************************************************************************
  *
  * This is the main action called from the Struts framework.
  * @param mapping The ActionMapping used to select this instance.
  * @param form The optional ActionForm bean for this request.
  * @param request The HTTP Request we are processing.
  * @param response The HTTP Response we are processing.
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception
  {

    String  mainMenuUrl     = "";
    String  callPage        = "";
    ActionForward forward   = null;
    HashMap requestParms    = new HashMap();

    //Get info passed in the request object
    requestParms = getRequestInfo(request);


      if(ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, OPMConstants.CONS_MAIN_MENU_URL) != null)
      {
        //main menu url
        mainMenuUrl = ConfigurationUtil.getInstance().getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT, OPMConstants.CONS_MAIN_MENU_URL);
        
        //append adminAction
        mainMenuUrl += "?adminAction=";
        mainMenuUrl += (String)requestParms.get("adminAction");

        //append security to the main menu url
        mainMenuUrl += "&context=";
        mainMenuUrl += (String)requestParms.get("context");
        mainMenuUrl += "&securitytoken=";
        mainMenuUrl += (String)requestParms.get("securityToken");               

        response.sendRedirect(mainMenuUrl);
      }

      return null;


  }



  /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none 
  * @throws none
  */
  private HashMap getRequestInfo(HttpServletRequest request)
  {
    String  adminAction                   = "";
    String  context                       = "";
    String  securityToken                 = ""; 
    HashMap requestParms                  = new HashMap();    
  

    /**********************retreive the security info**********************/
    //retrieve the context
    if(request.getParameter(OPMConstants.ADMIN_ACTION)!=null)
      adminAction = request.getParameter(OPMConstants.ADMIN_ACTION);

    //retrieve the context
    if(request.getParameter(OPMConstants.CONTEXT)!=null)
      context = request.getParameter(OPMConstants.CONTEXT);

    //retrieve the security token
    if(request.getParameter(OPMConstants.SEC_TOKEN)!=null)
      securityToken = request.getParameter(OPMConstants.SEC_TOKEN);

 
    requestParms.put("adminAction",         		      adminAction);
    requestParms.put("context",             		      context);
    requestParms.put("securityToken", 			          securityToken);
  
  
    return requestParms;  
  }









  /******************************************************************************
  * getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }





}