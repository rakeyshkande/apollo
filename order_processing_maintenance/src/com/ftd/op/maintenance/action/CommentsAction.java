package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.CommentsDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.BlockFlagVO;
import com.ftd.op.maintenance.vo.CommentsVO;
import com.ftd.op.maintenance.vo.ContactVO;
import com.ftd.op.maintenance.vo.MaintenanceCommentsVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.security.cache.vo.UserInfo;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class CommentsAction   extends Action
{

    private Logger logger;
    
    private int MAX_COMMENTS_PER_PAGE = 150;
    
    public CommentsAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.CommentsAction");
    }
    
  /**
	 * 
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    
        Connection conn = null;
        ActionForward forward = null;
    
        try
        {
            conn = CommonUtil.getDBConnection();
            
            //get action from page
            String actionType = request.getParameter("action_type");
            
            if(actionType == null || actionType.equalsIgnoreCase("display"))
            {
                processDisplay(request,response,mapping,conn);
            }
            else if(actionType.equalsIgnoreCase("add_comment"))
            {
                processAddComment(request,response,mapping,conn);
            }
            else
            {
                throw new Exception("Unknown action: " + actionType);
            }
            

        }
		catch (Throwable  t)
		{
			logger.error(t);
			forward = mapping.findForward("Error");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
//				forward = mapping.findForward("Error");
			}
		}//end finally     
        
        return null;
        
    }//end execute
    
    private void processDisplay(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn)
        throws Exception
    {
            //get comments page properties from request
            String commentType = request.getParameter("comment_type");
            String commentRequired = request.getParameter("comment_required");
            String commentId = request.getParameter("vendor_id");
            String displayPage = request.getParameter("display_page");
            String canEnterComment = request.getParameter("can_enter_comment");
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String commentSize = (String) request.getParameter("comment_max_size");
            String commentNumbering = (String)request.getParameter("comment_numbering");
            
            if(commentNumbering == null || commentNumbering.length() == 0) 
            {
              commentNumbering = "N";
            }
            System.out.println("COMMENT_NUMBERING = " + commentNumbering);
            //check if display page was passed in            
            if(displayPage == null || displayPage.length() == 0)
            {
                displayPage = "1";
            }

            //create master XML
            Document xmlDoc = DOMUtil.getDocument();               
            
            //retrieve existing comments
            CommentsDAO commentsDAO = new CommentsDAO(conn);
            Document commentsXML = commentsDAO.getCommentsXML(commentType,commentId,Integer.parseInt(displayPage),MAX_COMMENTS_PER_PAGE);
            
            //create page data document
            Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
            Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
            XMLUtil.addElement(pageDataDoc, pageDataXML, "comment_type",commentType);  
            XMLUtil.addElement(pageDataDoc, pageDataXML,"comment_required",commentRequired);         
            XMLUtil.addElement(pageDataDoc, pageDataXML,"vendor_id",commentId); 
            XMLUtil.addElement(pageDataDoc, pageDataXML, "display_page",displayPage); 
            XMLUtil.addElement(pageDataDoc, pageDataXML, "can_enter_comment",canEnterComment); 
            XMLUtil.addElement(pageDataDoc, pageDataXML, "securitytoken", sessionId);
            XMLUtil.addElement(pageDataDoc, pageDataXML, "context", context);
            
            if(commentSize != null && commentSize.length() > 0) 
            {
              XMLUtil.addElement(pageDataDoc, pageDataXML, "limit_comment_size", "Y");
              XMLUtil.addElement(pageDataDoc, pageDataXML, "comment_max_size", commentSize);
            } else {
              XMLUtil.addElement(pageDataDoc, pageDataXML, "limit_comment_size", "N");
            }
            XMLUtil.addElement(pageDataDoc, pageDataXML, "comment_numbering", commentNumbering);
            
            pageDataDoc.appendChild(pageDataXML);    

            //Combine pages
            if(commentsXML != null){
                DOMUtil.addSection(xmlDoc,commentsXML.getChildNodes());            
            }
            if(pageDataXML != null){
                DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());            
            }

        System.out.println(XMLUtil.convertDocToString(xmlDoc));
        //transform page
        logger.debug(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"CommentsXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));                    
    }

    private void processAddComment(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn)
    throws Exception
    {
            //get values from page
            String commentType = request.getParameter("comment_type");
            String commentId = request.getParameter("vendor_id");
            String comment = request.getParameter("comment");
            
            //create master XML
            Document xmlDoc = DOMUtil.getDocument();                           
            
            //get user
            String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            
            //create vo           
            MaintenanceCommentsVO commentsVO= new MaintenanceCommentsVO();
            commentsVO.setCommentType(commentType);
            commentsVO.setCommentTypeId(commentId);
            commentsVO.setDescription(comment);
            commentsVO.setCreatedBy(userInfo.getUserID());
            
            //insert comment into DB
            CommentsDAO commentsDAO = new CommentsDAO(conn);
            commentsDAO.insertMaintenanceComment(commentsVO);            
            
            //place flag in pagedate so page will close
            //create page data document
            Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
            Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
            pageDataDoc.appendChild(pageDataXML);     
            XMLUtil.addElement(pageDataDoc,pageDataXML,"close_page","Y");  
            
            //Combine pages
            if(pageDataDoc != null){
                DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());            
            }
            
        //transform page
        logger.debug(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"CommentsXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));                                
    }

    
}