package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.exception.NovatorTransmissionException;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.BlockFlagVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.IOException;
import java.io.PrintWriter;

import java.lang.Throwable;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class VendorListAction extends Action 
{

    private static final String PARAMETER_GLOBAL_BLOCK_DATE = "global_block_date";
    private static final String PARAMETER_GLOBAL_BLOCK_TYPE = "global_block_type";
    private static final String PARAMETER_DELETE_GLOBAL_BLOCK_DATE = "global_delete_block_date";
    private static final String PARAMETER_DELETE_GLOBAL_BLOCK_TYPE = "global_delete_block_type";
    
    public static final String VENDOR_LIST_DELIMETER = " ";
    public static String ACTIVE_ZONE_JUMP_ERROR_MESSAGE = "";


    private Logger logger;

    public VendorListAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.VendorListAction");
    }
    
  /**
	 * This is the Second Choice Maintenance Action called from the Struts framework.
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try{
    
            /* get action type request parameter */
            if(request.getParameter("action_type")!=null)
            {
                actionType = request.getParameter("action_type");
            }
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            VendorDAO dao = new VendorDAO(conn);
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;
            
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
             userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
                
            /* determine which action to process */
            
            /* add second choice */
            if(actionType == null || actionType.length() == 0 || actionType.equals("display_list"))
            {
                processDisplayList(request,response,mapping,conn, null, null);
            }
            else if (actionType.equals("delete_global_block"))
            {
                 processDeleteBlock(request, response, mapping,conn);
            }
            else if (actionType.equals("add_global_block"))
            {
                processAddBlock(request, response, mapping,conn);
            }
            else if (actionType.equalsIgnoreCase("reset_cutoff"))
            {
                processResetVendorCutoff(request, response, conn);              
            }
            else if (actionType.equalsIgnoreCase("save_cutoff_delay"))
            {
                processSaveBackendCutoffDelay(request, response, mapping, conn, userId);              
            }
            else
            {
                throw new Exception("Unknown action type:" + actionType);
            }
        }
		catch (Throwable  t)
		{
			logger.error(t);
			forward = mapping.findForward("ErrorPage");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward("ErrorPage");
			}
		}    
        
        return null;
    
    }
    private void processDisplayList(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, String errorMsg, String errorPopupMsg) throws Exception    
    {
      processDisplayList(request, response,mapping, conn, errorMsg, null, errorPopupMsg);
    }
    /**
     * Display the list of available vendors
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private void processDisplayList(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, String errorMsg, String globalBlockDate, String errorPopupMsg) throws Exception
    {

        VendorDAO vendorDAO = new VendorDAO(conn);    

    
        //create base document
        Document xmlDoc = DOMUtil.getDocument();   

        //get user
        String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
        UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
        String context = (String) request.getParameter(OPMConstants.CONTEXT);
        String popup = (String) request.getParameter("popup");        
        String vendorId = (String) request.getParameter("vendor_id");        
        
        Document lockXML = DOMUtil.getDocument();
        String canCreateFlag = null;
        //check if user has edit abilities
        SecurityManager securityManager = SecurityManager.getInstance();
        if (securityManager.assertPermission(context, sessionId, "VendorMaint", OPMConstants.SECURITY_UPDATE))
        {
          canCreateFlag = "Y";
        }
        else
        {
          canCreateFlag = "N";
        }        
        
        String canBlockFlag = null;
        //check if user has edit abilities
        if (securityManager.assertPermission(context, sessionId, "VendorMaintBlock", OPMConstants.SECURITY_UPDATE))
        {
          canBlockFlag = "Y";
        }
        else
        {
          canBlockFlag = "N";
        }                
        
        //check request for lock XML
        String lockValue = request.getParameter("lock_value");
            
        //get the list of vendors
        Document vendorList = vendorDAO.getVendorList();
        
        //get the list of global blocks
        Document globalBlocks = vendorDAO.getGlobalBlocksXML();
        //create page data document
        Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
        pageDataDoc.appendChild(pageDataXML);     
        XMLUtil.addElement((Document)pageDataDoc,"lock_value",lockValue);  
        XMLUtil.addElement((Document)pageDataDoc,"error_message",errorMsg); 
        XMLUtil.addElement((Document)pageDataDoc,"error_popup_message",errorPopupMsg); 
        XMLUtil.addElement((Document)pageDataDoc, "securitytoken", sessionId);
        XMLUtil.addElement((Document)pageDataDoc, "context", context);
        XMLUtil.addElement((Document)pageDataDoc, "adminAction", request.getParameter("adminAction"));
        XMLUtil.addElement((Document)pageDataDoc, "can_block_vendor", canBlockFlag);
        XMLUtil.addElement((Document)pageDataDoc, "can_create_vendor", canCreateFlag);
        XMLUtil.addElement((Document)pageDataDoc, "popup", popup);
        XMLUtil.addElement((Document)pageDataDoc, "vendor_id", vendorId);
        
        XMLUtil.addElement( (Document)pageDataDoc, "backend_cutoff_delay", 
                            (vendorDAO.getGlobalParameter(OPMConstants.CONTEXT_SHIPPING_PARMS, OPMConstants.NAME_BACKEND_CUTOFF_DELAY)
                            ).getValue()
                          ); 
        
        if(globalBlockDate != null) {
          XMLUtil.addElement((Document)pageDataDoc, "global_block_date", globalBlockDate);
        } else 
        {
          XMLUtil.addElement((Document)pageDataDoc, "global_block_date", "");
        }
        
        //Combine pages
        DOMUtil.addSection(xmlDoc,vendorList.getChildNodes());
        DOMUtil.addSection(xmlDoc,globalBlocks.getChildNodes());
        DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());
        
        //transform page
        //logger.debug(XMLUtil.convertDocToString(xmlDoc));
        //System.out.println(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"VendorListXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));
        
    }
    
     public static void main(String[] args)
     {
        try
        {
          VendorUtil.validateDate("01/10/2008");
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
     }


    /**
     * Saves the backend cutoff delay
     * @param request http request object
     * @param response http response object
     * @param conn database connection
     * @throws Exception
     */
    private void processSaveBackendCutoffDelay(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn, String userId)
      throws Exception
    {
      String responseText = "success";
      try 
      {
        String cutoffDelay = request.getParameter("cutoff_delay");
        
        if( cutoffDelay!=null ) 
        {
          cutoffDelay = cutoffDelay.trim();
        }
        
        //Validate the cutoff_delay
        try 
        {
          int testInt = Integer.parseInt(cutoffDelay);  
          
          //TODO:  put values into config file
          if( testInt < 30 || testInt > 300 ) 
          {
            throw new Exception();    
          }
        } 
        catch (Exception e) 
        {
          responseText = "Error - Delay time must be a numeric value between 30 and 300 (minutes).";
          return;
        }       
        
        VendorDAO vendorDAO = new VendorDAO(conn); 
        vendorDAO.setGlobalParameter(OPMConstants.CONTEXT_SHIPPING_PARMS, OPMConstants.NAME_BACKEND_CUTOFF_DELAY, cutoffDelay, userId);
      } 
      catch (Exception e) 
      {
        logger.error("Failed to update the Back End Cutoff ",e);
        responseText = "REDIRECT=" + mapping.findForward("ErrorPage").getPath();
      }
      finally 
      {
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println(responseText);
        out.flush();  
      }
    }


    /**
     * Reset the cutoff
     * @param request http request object
     * @param response http response object
     * @param conn database connection
     * @throws Exception
     */
    private void processResetVendorCutoff(HttpServletRequest request, HttpServletResponse response, Connection conn)
      throws Exception
    {

      String responseText = "success";
      try 
      {
        String cutoffTime = request.getParameter("cutoff_time");
        
        if( cutoffTime!=null ) 
        {
          cutoffTime = cutoffTime.trim();
        }
        
        try 
        {
          VendorDAO vendorDAO = new VendorDAO(conn); 
          
          // Obtain all vendor ids utilizing VendorDAO.getVendorIds()
          List vendorIds = vendorDAO.getVendorIds(); 
          
          int totalVendors = vendorIds.size(); 
          
          //Iterate through vendor ids.  IT has decided to update vendors individually since 
          //we cannot send a batch of vendors to Novator.  
          for (int i = 0; i < totalVendors; i++)
          {
            //Update all vendors with the global cutoff time and send updates to Novator
            VendorUtil vUtil = new VendorUtil();
            vUtil.updateVendorCutoffSendNovator(vendorIds.get(i).toString(), cutoffTime, conn);
          }
          
        } 
        catch (NovatorTransmissionException nte) 
        {
          String message = "Unable to set new Cutoff Time for all vendors.  Connectivity to Novator failed.\r\n";
          logger.error(message,nte);
          responseText = message;
        }
        catch (Exception e) 
        {
          responseText = "Error - Failed to update the cutoff for all vendors.  Please try again.\r\n"+e.getMessage();
          logger.error("Failed to reset the vendor cutoffs",e);
        }
      } 
      finally 
      {
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println(responseText);
        out.flush();  
      }

    }
   

  /**
   * Add a global block
   * 
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @param mapping            - ActionMapping used to select this instance
   * @param conn               - Database Connection
	 * @return n/a
	 * @throws Exception
	 */
    private void processAddBlock( HttpServletRequest request, HttpServletResponse response, 
                                  ActionMapping mapping, Connection conn)
      throws Exception
    {
      VendorDAO vendorDAO = new VendorDAO(conn);
         
      //get needed values from request
      String blockDateString = request.getParameter(PARAMETER_GLOBAL_BLOCK_DATE);
      String enteredBlockType = request.getParameter(PARAMETER_GLOBAL_BLOCK_TYPE);
         
      String errorText = null;
      
      //if date is not found, set the error message and return
      if (blockDateString.equalsIgnoreCase(""))
      {
         errorText = "Global Block Date required";
         processDisplayList(request,response,mapping,conn,errorText,null);
         return;
      }

      //validate the block date
      errorText = VendorUtil.validateDate(blockDateString);

      //if date is not found, set the error message and return
      if (errorText != null)
      {
        processDisplayList(request,response,mapping,conn,errorText,blockDateString,null);
        return;
      }
      
      //check to see if any vendors are assigned to any active zone jump trips for 
      //block date sent in, if there are return error message
      boolean zoneJumpTripExists = checkForZoneJumpTrip(null, blockDateString, enteredBlockType, conn);
      if(zoneJumpTripExists)
      {
          errorText = this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE;
          processDisplayList(request,response,mapping,conn,errorText,null);
          return;
      }
      

      Date enteredBlockDate = FieldUtils.formatStringToUtilDate(blockDateString);
      
      //check if this entered date exists as ship or delivery block
      List globalBlockList = vendorDAO.getGlobalBlocksList();
      BlockFlagVO blockFlagVO = VendorUtil.getBlockFlags(conn,enteredBlockDate,globalBlockList);
        
      boolean insertShipBlock = ( enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING) ||
                                  enteredBlockType.equalsIgnoreCase("Both")?true:false);
      boolean insertDeliveryBlock = ( enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY)  ||
                                      enteredBlockType.equalsIgnoreCase("Both")?true:false);
         
      //check if the shipping block is valid
      if(  enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING) &&
                blockFlagVO.isBlockedForShipping())
      {
        //set ship flag to false;           
        insertShipBlock = false; 
        
        //set the error text
        errorText = "Ship date is already blocked globally.";
      }
      //check if the delivery block is valid
      else if(  enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY)   &&
                blockFlagVO.isBlockedForDelivery())
      {
        //set delivery flag to false;           
        insertDeliveryBlock = false; 

        //set the error text
        errorText = "Delivery date is already blocked globally.";
      }
      //when block type is "both", either the ship/delivery are both valid, both invalid, or a 
      //combination of the two
      else if(enteredBlockType.equalsIgnoreCase("Both"))
      {
        //if both ship and delivery are blocked 
        if(blockFlagVO.isBlockedForShipping() && blockFlagVO.isBlockedForDelivery())
        {
          //set both flags to false;           
          insertShipBlock = false; 
          insertDeliveryBlock = false; 

          //set the error text
          errorText = "Date is already blocked for delivery and shipping.";
        }
        //if ship is blocked but delivery is ok
        else if(blockFlagVO.isBlockedForShipping() && !blockFlagVO.isBlockedForDelivery())
        {
          //set ship flag to false;           
          insertShipBlock = false; 
          
          //set the error text
          errorText = "Date is already blocked for shipping.  Delivery block inserted.";
        }
        //if delivery is blocked but ship is ok
        else if(!blockFlagVO.isBlockedForShipping() && blockFlagVO.isBlockedForDelivery())
        {
          //set delivery flag to false;           
          insertDeliveryBlock = false; 
          
          //set the error text
          errorText = "Date is already blocked for delivery.  Shipping block inserted.";

        }
      }
      else if ( !enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING) &&
                !enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY) &&
                !enteredBlockType.equalsIgnoreCase("Both")
              )
      {
        throw new Exception("Invalid block type:" + enteredBlockType);
      }

      try
      {
          //if either flag is true, process for that block for all vendors. 
          if (insertShipBlock || insertDeliveryBlock)
          {
            // Obtain all vendor ids utilizing VendorDAO.getVendorIds()
            List vendorIds = vendorDAO.getVendorIds(); 
              
            //get the total vendor in a primitive
            int totalVendors = vendorIds.size(); 
            
            VendorBlockVO block1VO = new VendorBlockVO();
            block1VO.setStartDate(enteredBlockDate);
            block1VO.setEndDate(enteredBlockDate);
            block1VO.setGlobalFlag("Y");

            VendorBlockVO block2VO = new VendorBlockVO();
            block2VO.setStartDate(enteredBlockDate);
            block2VO.setEndDate(enteredBlockDate);
            block2VO.setGlobalFlag("Y");
    
            //Iterate through vendor ids.  IT has decided to update vendors individually since 
            //we cannot send a batch of vendors to Novator.  
            for (int i = 0; i < totalVendors; i++)
            {
              //set the vendor id
              block1VO.setVendorId(vendorIds.get(i).toString());
              block2VO.setVendorId(vendorIds.get(i).toString());
    
              VendorUtil vUtil = new VendorUtil();
    
              // if both ship and delivery block
              if (insertShipBlock && insertDeliveryBlock) {
                 List bothBlocksList = new ArrayList();
                 //delivery block
                 block1VO.setBlockType(OPMConstants.BLOCK_TYPE_DELIVERY);
                 bothBlocksList.add(block1VO);
                 //shipping block
                 block2VO.setBlockType(OPMConstants.BLOCK_TYPE_SHIPPING);
                 bothBlocksList.add(block2VO);
                 // insert both blocks and send to novator
                 new VendorUtil().insertVendorBlocksSendNovator(bothBlocksList, conn);
              
              //if ship block is valid 
              } else if(insertShipBlock) {
                block1VO.setBlockType(OPMConstants.BLOCK_TYPE_SHIPPING);
                vUtil.insertVendorBlockSendNovator(block1VO, conn);
    
              //if delivery block is valid 
              } else if (insertDeliveryBlock) {
                block1VO.setBlockType(OPMConstants.BLOCK_TYPE_DELIVERY);
                vUtil.insertVendorBlockSendNovator(block1VO, conn);
              }
            }
    
          }
    
          //process the display list with appropriate message
          processDisplayList(request,response,mapping,conn,errorText,null);
      }
      catch(NovatorTransmissionException nte) 
      {
          String message = "Unable to save Global Block Date.  Connectivity to Novator failed.";
          logger.error(message,nte);
          //process the display list with appropriate message
          processDisplayList(request,response,mapping,conn,null,message);
      }

    }
     

  /**
   * Delete a global block
   * 
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @param mapping            - ActionMapping used to select this instance
   * @param conn               - Database Connection
	 * @return n/a
	 * @throws Exception
	 */
    private void processDeleteBlock(HttpServletRequest request, HttpServletResponse response, 
                                    ActionMapping mapping, Connection conn)
    throws Exception
    {
      VendorDAO vendorDAO = new VendorDAO(conn);
      
      String errorText = null;
      
      //get needed values from request
      String blockDateString = request.getParameter(PARAMETER_DELETE_GLOBAL_BLOCK_DATE);
      String enteredBlockType = request.getParameter(PARAMETER_DELETE_GLOBAL_BLOCK_TYPE);         
      
      //convert date to string
      Date enteredBlockDate = null;
      try
      {
        enteredBlockDate = FieldUtils.formatStringToUtilDate(blockDateString);
      }
      catch(Exception e)
      {
        errorText = "Invalid date format";
      }
      
      try 
      {
          
          if(errorText != null)
          {
          //do not delete the record, invalid date was passed in
          }
          else if ( !enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING) &&
                    !enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY) &&
                    !enteredBlockType.equalsIgnoreCase("Both")
                  )
          {
            throw new Exception("Invalid block type:" + enteredBlockType);
          }
          else 
          {
            // Obtain all vendor ids utilizing VendorDAO.getVendorIds()
            List vendorIds = vendorDAO.getVendorIds(); 
              
            //get the total vendor in a primitive
            int totalVendors = vendorIds.size(); 
            
            VendorBlockVO block1VO = new VendorBlockVO();
            block1VO.setStartDate(enteredBlockDate);
            block1VO.setEndDate(enteredBlockDate);
            block1VO.setGlobalFlag("Y");
            
            VendorBlockVO block2VO = new VendorBlockVO();
            block2VO.setStartDate(enteredBlockDate);
            block2VO.setEndDate(enteredBlockDate);
            block2VO.setGlobalFlag("Y");
    
            //Iterate through vendor ids.  IT has decided to update vendors individually since 
            //we cannot send a batch of vendors to Novator.  
            for (int i = 0; i < totalVendors; i++)
            {
              //set the vendor id
              block1VO.setVendorId(vendorIds.get(i).toString());
              block2VO.setVendorId(vendorIds.get(i).toString());
    
              VendorUtil vUtil = new VendorUtil();
    
              if( enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING) || 
                  enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY))
              {
                block1VO.setBlockType(enteredBlockType);
                vUtil.deleteVendorBlockSendNovator(block1VO, conn);
              }
              else
              {
                 List bothBlocksList = new ArrayList();
                 //delivery block
                 block1VO.setBlockType(OPMConstants.BLOCK_TYPE_DELIVERY);
                 bothBlocksList.add(block1VO);
                 //shipping block
                 block2VO.setBlockType(OPMConstants.BLOCK_TYPE_SHIPPING);
                 bothBlocksList.add(block2VO);
                 // delete both blocks and send to novator
                 new VendorUtil().deleteVendorBlocksSendNovator(bothBlocksList, conn);
              }
            }//end for
          }//end else
          
          processDisplayList(request,response,mapping,conn,errorText,null);
      }
      catch(NovatorTransmissionException nte)
      {
          String message = "Unable to delete Global Block Date.  Connectivity to Novator failed.";
          logger.error(message,nte);
          processDisplayList(request,response,mapping,conn,null,message);
      }
    }
    
    private boolean checkForZoneJumpTrip(String vendorId, String blockDate, String blockType, Connection connection) throws Exception
    {
        VendorDAO vDAO = new VendorDAO(connection); 
        List zjVendors = vDAO.zoneJumpTripExists(vendorId, blockDate, blockType);

        boolean zoneJumpTripExists = false;
        StringBuffer sb = new StringBuffer();
        for(Iterator it = zjVendors.iterator(); it.hasNext();) 
        {
            zoneJumpTripExists = true;
            sb.append((String)it.next());
            sb.append(VENDOR_LIST_DELIMETER); 
        }
        if(zoneJumpTripExists)
        {
            if(blockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING))
                this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE = FieldUtils.replaceAll(OPMConstants.ZONE_JUMP_SHIPPING_DATE_BLOCK_ERROR, "vendorIds", sb.toString());
            else if(blockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY))
                this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE = FieldUtils.replaceAll(OPMConstants.ZONE_JUMP_DELIVERY_DATE_BLOCK_ERROR, "vendorIds", sb.toString());
            else if(blockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_BOTH))
                this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE = FieldUtils.replaceAll(OPMConstants.ZONE_JUMP_BLOCK_ERROR, "vendorIds", sb.toString());
        }
        return zoneJumpTripExists; 
    }
}
