package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.BlockFlagVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.security.cache.vo.UserInfo;
import java.lang.Throwable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class VendorProductListAction  extends Action
{

    private Logger logger;
     
    public VendorProductListAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.VendorProductListAction");    
    }
    
  /**
	 * This is the Second Choice Maintenance Action called from the Struts framework.
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try{
    
            /* get action type request parameter */
            if(request.getParameter("action_type")!=null)
            {
                actionType = request.getParameter("action_type");
            }
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            VendorDAO dao = new VendorDAO(conn);
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;
            
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
             userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
                
            /* determine which action to process */
            
            processDisplayList(request,response,mapping,conn, null);

        }
		catch (Throwable  t)
		{
			logger.error(t);
			forward = mapping.findForward("Error");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward("ErrorPage");
			}
		}    
        
        return null;
    
    }
    
    
    /**
     * Display the list of available vendors
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private void processDisplayList(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, String errorMsg) throws Exception
    {

        VendorDAO vendorDAO = new VendorDAO(conn);    
    
        //create base document
        Document xmlDoc = DOMUtil.getDocument();   
        
        //get vendor id
        String vendorId = request.getParameter("vendor_id");

        //get user
        String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
        UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
        String context = (String) request.getParameter(OPMConstants.CONTEXT);
        
        String updateAllowed = null;
        //check if user has edit abilities
        SecurityManager securityManager = SecurityManager.getInstance();
        if (securityManager.assertPermission(context, sessionId, "InventoryMaint", OPMConstants.SECURITY_UPDATE))
        {
          updateAllowed = "Y";
        }
        else
        {
          updateAllowed = "N";
        }                
           
        //get the list of products
        Document productList = vendorDAO.getProductList(vendorId);        
        
        //create page data document
        Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
        pageDataDoc.appendChild(pageDataXML);     
        XMLUtil.addElement((Document)pageDataDoc,"can_update",updateAllowed); 
        XMLUtil.addElement((Document)pageDataDoc,"vendor_id",vendorId); 
        XMLUtil.addElement((Document)pageDataDoc, "securitytoken", sessionId);
        XMLUtil.addElement((Document)pageDataDoc, "context", context);
        XMLUtil.addElement((Document)pageDataDoc, "adminAction", request.getParameter("adminAction"));        
        XMLUtil.addElement((Document)pageDataDoc, "edit_mode", request.getParameter("edit_mode")); 

        
        //Combine pages
        DOMUtil.addSection(xmlDoc,productList.getChildNodes());
        DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());
        
        //transform page
        logger.debug(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductListXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));
        
    }
    
    
    
}