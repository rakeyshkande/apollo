package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.op.maintenance.bo.VenusScriptBO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.OrderDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.OrderDetailVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;


public class VenusOrderReprocessAction extends Action
{
  private static Logger logger  = new Logger("com.ftd.op.maintenance.action.VenusOrderReprocessAction");

  //request xsl filename
  private static final String VENUS_ORDER_REPROCESS_XSL = "venusOrderReprocessXSL";

  //action forwards
  private static final String MAIN_MENU_ACTION = "MainMenuAction";

  //config:  file names
  public static final String PROPERTY_FILE = "maintenance-config.xml";
  private static String DATASOURCE_NAME = "CLEAN";

  //application:
  public static final String APP_PARAMETERS = "parameters";

  //application:  actions
  public static final String ACTN_ERROR = "Error";

  //request parameters
  private static final String PARAM_EXTERNAL_ORDER_NUMBER = "external_order_number";
  private static final String PARAM_CSR = "csr_id";
  private static final String PARAM_ENTERED_SQL = "entered_sql";

  //action types
  public static final String  LOAD_REPROCESS_VENUS_ORDERS_ACTION = "loadVenusOrderReprocessAction";
  public static final String ACTION_TYPE = "action_type";
  private static final String GET_ORDERS = "sql";
  private static final String CHANGE_DELIVERY_DATE = "change_delivery_date";
  private static final String CHANGE_DELIVERY_DATE_EMAIL = "change_delivery_date_email";
  private static final String CANCEL_ORDER = "cancel_order";
  private static final String RESEND_ORDER = "resend_order";

  //Security
  public static final String SECURITY_TOKEN = "securitytoken";
  public static final String CONTEXT = "context";

  //Unit Testing
  public static final String UNIT_TEST = "unit_test";

  //Misc
  public static final String ORDER_DELIMETER = " ";

  //XML
  public static final String PAGE_DATA = "pageData";
  public static final String DATA = "data";

  /**
   * This is the Second Choice Maintenance Action called from the Struts framework.
   *
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
  public  ActionForward execute(ActionMapping mapping, ActionForm form,
                                HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
 {
    String actionType = null;
    Connection conn = null;

    try
    {
	conn = CommonUtil.getDBConnection();
        String csr = "SystemReprocess";
        String test = request.getParameter(UNIT_TEST);
        //Do unit testing if requested
        if( request.getParameter(UNIT_TEST)!=null )
        {
          return performPageUnitTests(mapping, form, request, response);
        }

        /* get action type request parameter */
        actionType = request.getParameter(ACTION_TYPE);
        if(actionType==null || actionType.length()==0 )
        {
          actionType = this.LOAD_REPROCESS_VENUS_ORDERS_ACTION;
        }

        //Retrieve the orders from the database
        if( actionType.equals(GET_ORDERS) )
        {
          return performGetOrders(conn,csr,mapping, form, request, response);
        }
        //Change the delivery date to the next available
        else if( actionType.equals(CHANGE_DELIVERY_DATE) )
        {
          return performChangeDeliveryDate(conn,csr,mapping, form, request, response);
        }

        //Change the delivery date to the next available
        //Send the customer an email
        else if( actionType.equals(CHANGE_DELIVERY_DATE_EMAIL) )
        {
          return performChangeDeliveryDateEmail(conn,csr,mapping, form, request, response);
        }

        //Cancel the order
        else if( actionType.equals(CANCEL_ORDER) )
        {
          return performCancelOrder(conn,csr,mapping, form, request, response);
        }

        //Cancel the order and send a new order
        else if( actionType.equals(RESEND_ORDER) )
        {
          return performResendOrder(conn,csr,mapping, form, request, response);
        }

        // saves changes on the main window
        else if(actionType.equals(MAIN_MENU_ACTION))
        {
          return performMainMenu(mapping, form, request, response);
        }

        // loads occasion maintenance page
        else if(actionType.equals(LOAD_REPROCESS_VENUS_ORDERS_ACTION))
        {
          return performLoad(mapping, form, request, response);
        }
    }
        catch(Throwable e)
        {
            logger.error(e);
        }
        finally
        {
            try
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                //do nothing
            }

        }

    return null;
  }

  private ActionForward performPageUnitTests(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String actionType = null;
    String responseOutput = "";

    if(request.getParameter(ACTION_TYPE)!=null)
    {
      actionType = request.getParameter(ACTION_TYPE);
    }
    else
    {
      actionType = this.GET_ORDERS;
    }

    Random r = new Random();
    r.setSeed(new java.util.Date().getTime());

    if( actionType.equals(GET_ORDERS) )
    {
      int count = r.nextInt(100);

      StringBuffer sb = new StringBuffer();
      for( int idx=0; idx<count; idx++ )
      {
        String strVal = "0000000"+String.valueOf(idx+1);
        sb.append("E");
        sb.append(strVal.substring(strVal.length()-7,strVal.length()));
        sb.append(ORDER_DELIMETER);
      }

      responseOutput = sb.toString();
    }
    else
    {
      String externalOrderNumber = request.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);
      if(externalOrderNumber==null)
      {
        responseOutput = "Fatal - no external order number provided";
      }
      else
      {
        switch( r.nextInt(100) )
        {
          case 1:
            responseOutput = "Error - "+externalOrderNumber;
          break;

          case 2:
            responseOutput = "Fatal - Something awful has happened";
          break;

          case 0:
          default:
            responseOutput = "Success - "+externalOrderNumber;
          break;
        }
      }
    }

    //write out response
    PrintWriter out = null;
    try {
      response.setContentType("text/plain");
      out = response.getWriter();
      out.println(responseOutput);
    } catch (IOException ioe)
    {
      if( out!=null )
      {
        out.close();
      }
    }

    return null;
  }

  private ActionForward performGetOrders(Connection conn,String csr,ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws Exception
  {
        VenusScriptBO bo = new VenusScriptBO();
        String responseOutput = bo.getOrders(conn,csr,request);

        //write out response
        PrintWriter out = response.getWriter();
        out.println(responseOutput);
        out.close();

    return null;
  }

  private ActionForward performChangeDeliveryDate(Connection conn,String csr,ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws Exception
  {
        VenusScriptBO bo = new VenusScriptBO();
        String responseOutput = bo.changeDeliveryDate(conn,csr,request, false);
        logger.info("responseOutput: " + responseOutput);
        
        // Append preferred partner name to error message.
        responseOutput = formatError(conn, request, responseOutput);
        
        logger.info("formatError responseOutput: " + responseOutput);
        
        //write out response
        PrintWriter out = response.getWriter();
        out.println(responseOutput);
        out.close();

    return null;
  }


  private ActionForward performChangeDeliveryDateEmail(Connection conn,String csr,ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws Exception
  {
        VenusScriptBO bo = new VenusScriptBO();
        String responseOutput = bo.changeDeliveryDate(conn,csr,request, true);

        // Append preferred partner name to error message.
        responseOutput = formatError(conn, request, responseOutput);
      
        //write out response
        PrintWriter out = response.getWriter();
        out.println(responseOutput);
        out.close();

    return null;
  }

  private ActionForward performCancelOrder(Connection conn,String csr,ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws Exception
  {
        VenusScriptBO bo = new VenusScriptBO();
        String responseOutput = bo.cancelOrder(conn,csr,request,false);
        
        // Append preferred partner name to error message.
        responseOutput = formatError(conn, request, responseOutput);

        //write out response
        PrintWriter out = response.getWriter();
        out.println(responseOutput);
        out.close();

    return null;
  }

  private ActionForward performResendOrder(Connection conn,String csr,ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
  {
        VenusScriptBO bo = new VenusScriptBO();
        String responseOutput = bo.cancelOrder(conn,csr,request,true);
        
        // Append preferred partner name to error message.
        responseOutput = formatError(conn, request, responseOutput);

        //write out response
        PrintWriter out = response.getWriter();
        out.println(responseOutput);
        out.close();

    return null;
  }

  private ActionForward performLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Document xmlDoc  = null;
    ActionForward forward = null;
    File xslFile = null;
    HashMap dataHash = new HashMap();
    Connection conn = null;

    try
    {
      conn = this.getDBConnection();
      MaintenanceDAO dao = new MaintenanceDAO(conn);
      /* pull base document */
      xmlDoc = DOMUtil.getDocument();
      Document prefPartnerDoc = FTDCommonUtils.getPreferredPartnerForMassProcess();

      /* get security token */
      String sessionId = (String) request.getParameter(SECURITY_TOKEN);
      String context = (String) request.getParameter(CONTEXT);
      String userId = null;

      if(sessionId==null)
      {
        logger.error("Security token not found in request object");
      }

      /* retrieve user information */
      if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
      }

      /* build page data */
      HashMap pageData = new HashMap();

      pageData.put("adminAction", request.getParameter("adminAction"));

      /* convert the page data hashmap to XML and append it to the final XML */
      DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true);
      DOMUtil.addSection(xmlDoc, prefPartnerDoc.getChildNodes());

      /* forward to addOccasionPopupXSL action */
      xslFile = getXSL(VENUS_ORDER_REPROCESS_XSL,mapping);

      /* save parameters onto request object */
			request.setAttribute("VenusOrderReprocess",xmlDoc);
			request.setAttribute("VenusOrderReprocessXSL",xslFile);

      //xmlDoc.print(System.out);

      TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));

    } catch (Exception e)
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    } finally
    {
      try {
        conn.close();
      } catch (Exception e)
      {
        // do nothing
      }
    }

    return null;
  }

  private ActionForward  performMainMenu(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    Connection conn = null;
    try
    {
      conn = this.getDBConnection();

      /* get security token */
      String sessionId = (String) request.getParameter(SECURITY_TOKEN);
      String context = (String) request.getParameter(CONTEXT);
      String userId = null;
      if(sessionId==null)
      {
        logger.error("Security token not found in request object");
      }

      /* retrieve user information */
      if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
      }


      /* forward to main menu action */
      ActionForward newForward = mapping.findForward(MAIN_MENU_ACTION);
      return newForward;
    }
    catch(Exception e)
    {
      logger.error(e);
      return mapping.findForward(ACTN_ERROR);
    }
    finally
    {
      //close the connection
      try
      {
        conn.close();
      }
      catch(Exception e)
      {}
    }
  }

  /**
  * get XSL file
  *
  * Retrieve name of CSZ Zipcode Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }

	/**
	 * Obtain connectivity with the database
	 *
	 * @param none
	 * @return Connection - db connection
	 * @throws ClassNotFoundException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException,
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,
															   DATASOURCE_NAME));

		return conn;
	}
        
    /**
     * Append preferred partner name to error messages.
     * @param request
     * @param message
     * @return
     */
    private String formatError(Connection conn, HttpServletRequest request, String message)   
    {
        String formattedMessage = message;
        try { 
            if (message != null && (message.startsWith(VenusScriptBO.ERROR_TEXT) || message.startsWith(VenusScriptBO.FATAL_TEXT))) {
                 OrderDAO orderDAO = new OrderDAO(conn);
    
                 //get values from request
                 String externalOrderNumber = request.getParameter(PARAM_EXTERNAL_ORDER_NUMBER);
    
                 //get order detail id
                 Map orderIdMap = orderDAO.findOrderNumber(externalOrderNumber);
                 if(orderIdMap == null)
                 {
                     throw new Exception(" Order number not found.");
                 }
                 String orderDetailId = null;
                 try {
                    orderDetailId = (orderIdMap.get("OUT_ORDER_DETAIL_ID")).toString();
                 }  catch (Exception ex){
                    throw new Exception(" Order number not found.");
                 }    
                 //get the order detail record
                 OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId); 
                 
                 if(orderDetailVO == null) {
                    throw new Exception("Order detail not found.");
                 }
                 VenusScriptBO bo = new VenusScriptBO();
                 String prefPartnerName = bo.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
                 
                 if(prefPartnerName != null && prefPartnerName.length()>0) {
                    formattedMessage = formattedMessage + " - " + prefPartnerName;
                 }
            }
        } catch (Exception e) {
            logger.error(e);
            //just return the original message in case of exception.
        }
        return formattedMessage;
    }
}