package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.PricePayVendorBO;
import com.ftd.op.maintenance.vo.PricePayVendorUploadSpreadsheetRowVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.ExcelProcessingException;
import com.ftd.security.util.FileProcessor;
import com.ftd.security.util.ServletHelper;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class PricePayVendorAction extends Action {

    private Logger logger= new Logger("com.ftd.op.maintenance.action.PricePayVendorAction");
    private HashMap inputMap = new HashMap();

    //XML
    public static final String PAGE_DATA = "pageData";
    public static final String DATA = "data";
    public static final String UPDATE_ALLOWED = "update_allowed";
    
    //Security
    public static final String SECURITY_TOKEN = "securitytoken";
    public static final String CONTEXT = "context";
    public static final String ADMIN_ACTION = "adminAction";

    //application:
    public static final String APP_PARAMETERS = "parameters";
    
    //application:  actions
    public static final String ACTN_ERROR = "Error";
           
    public PricePayVendorAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException {

        String actionType = null;

        HashMap fieldMap = new HashMap();
        try {
            if (ServletHelper.isMultipartFormData(request)) {
                logger.debug("multipart form");
                fieldMap = ServletHelper.getMultipartParam(request);

                FileItem fileItem = (FileItem)fieldMap.get("file_item");
                if (fileItem == null) {
                    logger.error("ERROR_NO_FILE_ITEM");
                } else {
                    logger.debug("file name: " + fileItem.getName());
                    List requestUploadList = retrieveFile(fieldMap);
                    inputMap.put("requestUploadList", requestUploadList);
                    
                    /* retrieve user information */
                    String userId = "System";
                    String sessionId = request.getParameter(SECURITY_TOKEN);
                    if(SecurityManager.getInstance().getUserInfo(sessionId)!=null) {
                        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
                    }
                    inputMap.put("userId", userId);

                    PricePayVendorBO ppvBO = new PricePayVendorBO();
                    String batchId = ppvBO.processUpload(this.inputMap);
                    request.setAttribute("batchId", batchId);
                    request.setAttribute("approvalList", inputMap.get("approvalList"));
                    request.setAttribute("varianceThreshold", inputMap.get("varianceThreshold"));
                    request.setAttribute("no_product_master", inputMap.get("no_product_master"));
                    request.setAttribute("no_vendor_master", inputMap.get("no_vendor_master"));
                    request.setAttribute("no_vendor_product", inputMap.get("no_vendor_product"));
                    request.setAttribute("errorMessage", "");
                    performView(mapping, "viewApproval", request, response);
                }

            } else {
                
                /* get action type request parameter */
                if(request.getParameter("formAction")!=null) {
                  actionType = request.getParameter("formAction");
                }
                else {
                  actionType = "view";
                }
                logger.debug("action: " + actionType);
                
                if(actionType.equals("view")) { 
                    return performView(mapping, "viewUpdate", request, response);
                } else if (actionType.equalsIgnoreCase("load")) {
                    String batchId = request.getParameter("batchId");
                    
                    // post JMS message to PRICE_PAY_VENDOR
                    MessageToken token = null;
                    token = new MessageToken();
                    token.setStatus("PRICE PAY VENDOR UPDATE");
                    token.setMessage(batchId);

                    logger.debug("MESSAGE : " + (String)token.getMessage());
                    logger.debug("STATUS : " + (String)token.getStatus());
                    
                    InitialContext context = new InitialContext();
                    Dispatcher dispatcher = Dispatcher.getInstance();
                    dispatcher.dispatchTextMessage(context, token);

                    logger.debug("Message sent");
                    
                    request.setAttribute("batchId", batchId);
                    request.setAttribute("errorMessage", "");
                    return performView(mapping, "updateResult", request, response);
                } else if (actionType.equalsIgnoreCase("report")) {
                    MessageToken token = new MessageToken();
                    token.setMessage("123456");

                    token.setStatus("REPORTING");
                    try {
                        Dispatcher.getInstance().dispatchTextMessage(new InitialContext(), token);
                    } catch (Exception e) {
                        throw new RuntimeException("dispatchReportRequest: Failed.", e);
                    }
                    return performView(mapping, "viewUpdate", request, response);
                }
                
            }
            
        } catch (ExcelProcessingException epe) {
            logger.error("Excel Processing Exception: " + epe);
            logger.error("message: " + epe.getMessage());
            request.setAttribute("errorMessage", epe.getMessage());
            performView(mapping, "updateResult", request, response);
        } catch (Exception e) {
            logger.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            performView(mapping, "updateResult", request, response);
        }

        return null;
        
    }
    
    private ActionForward performView(ActionMapping mapping, String form, HttpServletRequest request, HttpServletResponse response) 
    {
      Document xmlDoc  = null;
      File xslFile = null;

      try
      {
          xmlDoc = DOMUtil.getDocument();    
        
          /* get security token */
          String sessionId = request.getParameter(SECURITY_TOKEN);
          String context = request.getParameter(CONTEXT);
          String adminAction = request.getParameter(ADMIN_ACTION);

          if(sessionId==null) {
              logger.error("Security token not found in request object");
          }
          
          Document approvalDoc = (Document) request.getAttribute("approvalList");
          if (approvalDoc != null) {
              DOMUtil.addSection(xmlDoc, approvalDoc.getChildNodes());
          }

          /* build page data */
          HashMap pageData = new HashMap();
      
          pageData.put(SECURITY_TOKEN, sessionId);
          pageData.put(CONTEXT, context);
          pageData.put(ADMIN_ACTION, adminAction);
          /* check if user has access to add service fees */
          boolean updateAllowed = SecurityManager.getInstance().assertPermission(context,sessionId,"Price Pay Vendor Link","View");
          if(updateAllowed) {
              pageData.put(UPDATE_ALLOWED, "Y"); 
          } else {
              pageData.put(UPDATE_ALLOWED, "N");
          }
          
          String errorMessage = (String) request.getAttribute("errorMessage");
          if (errorMessage != null && !errorMessage.equals("")) {
              pageData.put("error_message", errorMessage);
          }
          
          String varianceThreshold = (String) request.getAttribute("varianceThreshold");
          if (varianceThreshold != null) {
              pageData.put("variance_threshold", varianceThreshold);
          }
          
          String noProductMaster = (String) request.getAttribute("no_product_master");
          if (noProductMaster == null || noProductMaster.equals("")) noProductMaster = "0" ;
          pageData.put("no_product_master", noProductMaster);

          String noVendorMaster = (String) request.getAttribute("no_vendor_master");
          if (noVendorMaster == null || noVendorMaster.equals("")) noVendorMaster = "0" ; 
          pageData.put("no_vendor_master", noVendorMaster);

          String noVendorProduct = (String) request.getAttribute("no_vendor_product");
          if (noVendorProduct == null || noVendorProduct.equals("")) noVendorProduct = "0" ;
          pageData.put("no_vendor_product", noVendorProduct);

          String batchId = (String) request.getAttribute("batchId");
          if (batchId == null) batchId = "";
          pageData.put("batch_id", batchId);
          logger.debug("batch_id: " + batchId);
          
/*
          String checked = "checked";
          String disabled = "disabled";
          NovatorFeedUtil nfu = new NovatorFeedUtil();
          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.TEST))
            pageData.put("testDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.TEST))
            pageData.put("testChecked", checked);
          
          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.UAT))
            pageData.put("uatDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.UAT))
            pageData.put("uatChecked", checked);

          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.CONTENT))
            pageData.put("contentDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.CONTENT))
            pageData.put("contentChecked", checked);

          if(!nfu.isNovatorEnvironmentAllowed(NovatorFeedUtil.PRODUCTION))
            pageData.put("liveDisabled", disabled);
          if(nfu.isNovatorEnvironmentChecked(NovatorFeedUtil.PRODUCTION))
            pageData.put("liveChecked", checked);
*/

          /* convert the page data hashmap to XML and append it to the final XML */
          DOMUtil.addSection(xmlDoc, PAGE_DATA, DATA, pageData, true); 
          StringWriter sw = new StringWriter();
          DOMUtil.print(xmlDoc, new PrintWriter(sw));
          logger.debug(sw.toString());

          /* forward to addOccasionPopupXSL action */
          xslFile = getXSL(form, mapping);
          logger.debug("xslFile: " + xslFile.getPath());
        
          HashMap parameters = (HashMap)request.getAttribute(APP_PARAMETERS);
          if (parameters != null)  
          {
              String name = null, value = null;
              for (Iterator i = parameters.keySet().iterator(); i.hasNext() ; )  
              {
                  name = (String) (i.next());
                  if (name != null) {
                      value = (String) parameters.get(name);
                      if(value==null)
                      {
                        value="";
                      }
                  }
                  logger.debug("parameter: " + name + " " + value);
              }
          }
          TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, parameters);
         
      } catch (Exception e) {
          logger.error(e);
          return mapping.findForward(ACTN_ERROR);
      }

      return null;
      
    }

    /**
     * Check the input Map for a file item.  If exists, create a list of PricePayVendorUploadSpreadsheetRowVO and return the list
     * @param requestMap HashMap
     * @return List 
     * @throws Exception
     */
    private List retrieveFile(HashMap requestMap) throws Exception {

        FileItem fileItem = (FileItem) requestMap.get("file_item");
        if (fileItem == null) {
            throw new RuntimeException("processFileUpload: Upload data file can not be null.");
        } else {
            return FileProcessor.getInstance().processSpreadsheetFile(fileItem.getInputStream(), PricePayVendorUploadSpreadsheetRowVO.class);
        }
      
     }

    /**
    * get XSL file
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }
    
}
