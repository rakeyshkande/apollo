package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.FedExShipDAO;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.BlockFlagVO;
import com.ftd.op.maintenance.vo.ContactVO;
import com.ftd.op.maintenance.vo.FedExScheduleVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.security.cache.vo.UserInfo;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * Struts action class
 */
public class FedExShippingTableAction extends Action 
{
    private Logger logger;
    private static final String SUCCESS = "LoadShippingTable";
    public static final String APP_PARAMETERS = "parameters";
    private static final String SHIPPING_TABLE_XSL = "ShippingTableXSL";
    private static final String INSERT_RESPONSE = "insert_response";
    private static final String ERRORS_EXIST = "errors_exist";
    private static final String SYSTEM_ERROR_MESSAGE = "system_error_message";
    private static final String PRODUCTION_SUCCESS = "production_success";
    
    /**
    * Default constructor.
    * Sets the logger variable
    */
    public FedExShippingTableAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.FedExShippingTableAction");
    }
    
  /**
   * This method will:
   * 1.  Validate security
   * 2.  Validate the action_type parameter passed
   * 3.  Get a database connection
   * 4.  Forward the request to other methods depending on the processing required
   * 5.  Return the results in the response object
   * 6.  All errors are returned in the response object
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try
        {        
            /* get action type request parameter */
            actionType = request.getParameter("action_type");
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;
            
            if(sessionId == null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
            
            /* determine which action to process */            
            if(actionType == null || actionType.equals("load_page"))
            {
                processLoadPage(request, response, mapping, conn);
            }
            else if (actionType.equals("load_production"))
            {
                processLoadProduction(request, response, mapping, conn);
                processLoadPage(request, response, mapping, conn);
            }
            else
            {
                throw new Exception("Unknown action type:" + actionType);
            }
        }
        catch (Throwable  t)
        {
			logger.error(t);
            forward = mapping.findForward("ErrorPage");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward("ErrorPage");
			}
		}    
        
        return forward;
    }
    
  /**
   * This method will: 
   * 1.  Gather any requried information
   * 2.  Create an XML transformation to produce the web page 
   *     using loadShippingTables.xsl.
   * 3.  All errors will be returned by throwning an exception
   * 
   * @throws java.lang.Exception
   * @param conn
   * @param mapping
   * @param response
   * @param request
   */
    private void processLoadPage(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn)
        throws Exception
    {
        HashMap pageData = new HashMap();
        Document xmlDoc = DOMUtil.getDocument();
        File xslFile = getXSL(SHIPPING_TABLE_XSL, mapping);
        
        boolean errorsExist = false;
        if(request.getAttribute(ERRORS_EXIST) != null)
        {
            errorsExist = ((Boolean)request.getAttribute(ERRORS_EXIST)).booleanValue();
        }
        
        /* Move attributes to pagedata */
        Object insertResponse = request.getAttribute(INSERT_RESPONSE);
        pageData.put(SYSTEM_ERROR_MESSAGE, request.getAttribute(SYSTEM_ERROR_MESSAGE));
        pageData.put(PRODUCTION_SUCCESS, request.getAttribute(PRODUCTION_SUCCESS));
        
        /* Check for errors to return to user */
        if(errorsExist)
        {
            pageData.put("errors_exist", "true");
            
            FedExScheduleVO fedExScheduleVO = null;
            for (int i = 0; i < ((List)insertResponse).size(); i++) 
            {
                fedExScheduleVO = (FedExScheduleVO)((List) insertResponse).get(i);
                Document parser = DOMUtil.getDocument(fedExScheduleVO.toXML());
                DOMUtil.addSection(xmlDoc, parser.getChildNodes());
            }
        }
        else
        {
            pageData.put("errors_exist", "false");
            pageData.put("insert_count", ((Integer) insertResponse));
        }
        
        /* Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() */
        DOMUtil.addSection(xmlDoc, "pageData", "data", pageData, true);
    
        /* Transform output page */
        TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, (HashMap)request.getAttribute(APP_PARAMETERS));
    }
    
  /**
   * This method will:
   * 1.  Call the FexEdShipDAO.updateFedexGndScheduleToProduction method
   * 2.  All errors will be returned by throwning an exception
   * 
   * @param conn
   * @param mapping
   * @param response
   * @param request
   */
    private void processLoadProduction(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn)
    {
        boolean productionSuccess = true;
        
        int hour = Integer.parseInt(request.getParameter("production_time"));
        
        /* Determine execution time */
        Calendar timerCal = Calendar.getInstance();
        timerCal.set(Calendar.HOUR_OF_DAY, hour);
        timerCal.set(Calendar.MINUTE, 0);
        timerCal.set(Calendar.SECOND, 0);
        timerCal.set(Calendar.MILLISECOND, 0);
        
        if(System.currentTimeMillis() < timerCal.getTime().getTime())
        {
            System.out.println("today");
        }
        else
        {
            /* Execute the next day because time has passed */
            timerCal.add(Calendar.DATE, 1);
        }
        
        try
        {
            /* Update production with staging data */
            FedExShipDAO fedExShipDAO = new FedExShipDAO(conn);
            fedExShipDAO.updateFedexGndScheduleToProduction();
        }
        catch(Exception e)
        {
            productionSuccess = false;
        }
        
        /* Set success to request */
        request.setAttribute(PRODUCTION_SUCCESS, new Boolean(productionSuccess));
    }
    
   /**
    * get XSL file
    *
    * Retrieve name of CSZ Zipcode Search Action XSL file
    * @param1 String - the xsl name returned from the Business Object
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {
        ActionForward forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath();      
        File xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        
        return xslFile;
    }
}