package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.ProductSubCodeVO;
import com.ftd.op.maintenance.vo.ProductVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.IOException;
import java.io.PrintWriter;

import java.lang.Throwable;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class VendorProductSearchAction   extends Action
{
     private Logger logger;

    public VendorProductSearchAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.VendorProductSearchAction");     
    }
    
  /**
	 * This is the Second Choice Maintenance Action called from the Struts framework.
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try{
    
            /* get action type request parameter */
            if(request.getParameter("action_type")!=null)
            {
                actionType = request.getParameter("action_type");
            }
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            VendorDAO dao = new VendorDAO(conn);
            
            //process the action
            if(actionType != null && actionType.equals("search"))
            {
                forward = processSearch(request, response, mapping, conn);
            }
            else if(actionType != null && actionType.equals("get_vendors"))
            {
                forward = processGetVendors(request, response, mapping, conn);
            }
            else
            {
                forward = processDisplay(request, response, mapping, conn);
            }

            
            
        }
		catch (Throwable  t)
		{
			logger.error(t);
			forward = mapping.findForward("Error");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward("ErrorPage");
			}
		}
        
        return forward;
    }
    
    /**
     * Display page
     */
     private ActionForward processDisplay(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn)
        throws Exception
        {
            //create base document
            Document xmlDoc = DOMUtil.getDocument();    
            
            //create page data document
            Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
            Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
            pageDataDoc.appendChild(pageDataXML);     
            XMLUtil.addElement((Document)pageDataDoc, "securitytoken", request.getParameter("securitytoken"));                    
            XMLUtil.addElement((Document)pageDataDoc, "context", request.getParameter("context"));                    
            XMLUtil.addElement((Document)pageDataDoc, "adminAction", request.getParameter("adminAction"));                    

            //Combine pages
            DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());                        
            
            //transform page
            logger.debug(XMLUtil.convertDocToString(xmlDoc));
            TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductSearchXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));            
            
            return null;
        }


    /**
     * Display page
     */
     private ActionForward processSearch(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn)
        throws Exception
        {
            //create base document
            Document xmlDoc = DOMUtil.getDocument();   
            
            ActionForward actionForward = null;
            
            boolean sendToInventoryDetail = false;
            
            //create page data document
            Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
            Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
            pageDataDoc.appendChild(pageDataXML);     
            XMLUtil.addElement((Document)pageDataDoc, "securitytoken", request.getParameter("securitytoken"));                    
            XMLUtil.addElement((Document)pageDataDoc, "context", request.getParameter("context"));                    
            XMLUtil.addElement((Document)pageDataDoc, "adminAction", request.getParameter("adminAction"));                    
            XMLUtil.addElement((Document)pageDataDoc, "product_id", request.getParameter("product_id"));                               
            XMLUtil.addElement((Document)pageDataDoc, "vendor_id", request.getParameter("vendor_id"));                                  
            XMLUtil.addElement((Document)pageDataDoc, "vendor_list", request.getParameter("vendor_list"));       
            
            //get product id from request
            String productId = request.getParameter("product_id");
            
            //get the vendor id from the request
            String vendorId = request.getParameter("vendor_id");
                    
            logger.debug("**********************************************************");        
            logger.debug("Product id: "+request.getParameter("product_id"));
            logger.debug("Vendor id: "+request.getParameter("vendor_id"));
            logger.debug("**********************************************************");
            
            String errorMessage = "";
            
            String popupFlag = "";            
            
            //if a product was not entered
            if(productId == null || productId.length() == 0)
            {
               errorMessage = "Required"; 
            }
            else
            {
                //retrieve product from the DB
                MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
                ProductVO productVO = maintDAO.getProduct(productId);
                
                
                //if product not found
                if(productVO == null)
                {
                    //check if entered value is a subcode
                    ProductSubCodeVO subCodeVO = maintDAO.getSubcode(productId);    
    
                    //if found
                    if(subCodeVO == null)
                    {
                        errorMessage = "Invalid Product ID";
                    }
                    else
                    {
                        //entered product id is valid...subcodes can be entered                        
                    }                    
                }
                else
                {
                    //if product has subcodes display message
                    if(productVO.getSubcodeFlag() != null && productVO.getSubcodeFlag().equals("Y"))
                    {
                        errorMessage = "Product has subcodes.  Please enter the subcode id.";
                    }                    
                    else if(productVO.getShipMethodCarrier() == null || !productVO.getShipMethodCarrier().equalsIgnoreCase("Y"))
                    {
                        errorMessage = "Not a vendor delivered product";                    
                    }                    
                }//end returned product not null                               
               }//end product id entered

            //if no erro was found
            if(errorMessage.length() == 0)
            {
                //product is vendor delivered and valid
                
                //check if an inventory record exist for the product
                VendorDAO vendorDAO = new VendorDAO(conn);
                boolean invRecordExists = vendorDAO.inventoryRecordExists(productId, vendorId);
                if(invRecordExists)
                {
                    //inventory record exists, forward to page
                    sendToInventoryDetail = true;                        
                }
                else
                {
                    //else, see if user has access to create record                        
                    //get user
                    String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
                    UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
                    String context = (String) request.getParameter(OPMConstants.CONTEXT);
                    
                    //check if user has edit abilities
                    SecurityManager securityManager = SecurityManager.getInstance();
                    if (securityManager.assertPermission(context, sessionId, "VendorMaint", OPMConstants.SECURITY_UPDATE))
                    {
                      popupFlag = "can_create";
                    }
                    else
                    {
                      popupFlag = "cannot_create";
                    }//end if sec manager stuff                                                      
                }//end if(invRecordExists)                        
            }//end product validation checks               
            
            
            //add error and popup date to page data
            XMLUtil.addElement((Document)pageDataDoc, "error_message", errorMessage);                    
            XMLUtil.addElement((Document)pageDataDoc, "popup_flag", popupFlag);                    
            
            //Combine pages
            DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());                        
            
            //if an error occured display the page again
            if(sendToInventoryDetail)
            {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String opsmainturl = cu.getFrpGlobalParm(OPMConstants.ORDER_PROCESSING_MAINT_CONFIG_CONTEXT,"OPM_URL");

                //forward to product inventory detail
                 actionForward = mapping.findForward("InventoryDetail");
                 String path = actionForward.getPath() 
                                + "?" + OPMConstants.SEC_TOKEN + "=" + request.getParameter(OPMConstants.SEC_TOKEN)
                                + "&" + "product_id" + "=" + productId
                                + "&" + "vendor_id" + "=" + vendorId
                                + "&" + "update_type=update"
                                + "&" + "from_page=product_search"
                                + "&" + OPMConstants.ADMIN_ACTION + "=" + request.getParameter(OPMConstants.ADMIN_ACTION)
                                + "&" + OPMConstants.CONTEXT + "=" +request.getParameter(OPMConstants.CONTEXT);

                 path = opsmainturl + path;
                 actionForward = new ActionForward(path,true);                     
            }
            else
            {
            
                //transform page
                logger.debug(XMLUtil.convertDocToString(xmlDoc));
                TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductSearchXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));                            

            }
            
            return actionForward;
            
        }
    
    /**
     * Obtain all vendor ids for a given product id.  This method is used via 
     * an AJAX call.
     */
     private ActionForward processGetVendors(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn)
        throws Exception
    {
        String forward = null;
        try {        
            String productId = request.getParameter("product_id");
            VendorDAO vDAO = new VendorDAO(conn);

            logger.debug("Product id: "+productId);
            Document responseDoc = vDAO.getVendorIdForProduct(productId);

            PrintWriter out = response.getWriter();
            DOMUtil.print(responseDoc, (out));
            out.flush();
        } catch (Exception e) {
            logger.error("Error retrieving list of vendors for product",e);
            String responseText = "REDIRECT=" + mapping.findForward("ErrorPage").getPath();
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.println(responseText);
            out.flush();  
        }
        
        return null;
    }
}