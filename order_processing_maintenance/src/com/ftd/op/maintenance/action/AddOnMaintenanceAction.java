package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.AddOnMaintenanceBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;

import java.sql.Connection;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.op.maintenance.util.XMLUtil;

import com.ftd.osp.utilities.vo.AddOnVO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.apache.commons.lang.StringEscapeUtils;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public class AddOnMaintenanceAction
    extends Action
{
  
    private static Logger logger = new Logger("com.ftd.op.maintenance.action.AddOnMaintenanceAction");


    /*******************************************************************************************
     * Constructor 1
     *******************************************************************************************/
    public AddOnMaintenanceAction()
    {
    }

    /**
     * This is the main action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response)
    {
       
        //request paramaters
    	HashMap responseMap;
        HashMap pageData = new HashMap();
        
        Connection con=null;

        ActionForward forward = null;

        try
        {
            String forwardToType = "";
            String forwardToName = "";

            //Connection/Database info
            con = CommonUtil.getDBConnection();

            //Document that will contain the final XML, to be passed to the TransUtil and XSL page
            Document responseDocument = DOMUtil.getDocument();

            //process the request
            responseMap = processRequest(request,response,pageData,con);

            //Get all the keys in the hashmap returned from the business object
            Set ks = responseMap.keySet();
            Iterator iter = ks.iterator();
            String key;

            //Iterate thru the hashmap returned from the business object using the keyset
            while (iter.hasNext())
            {
                key = iter.next().toString();
                //Append all the existing XMLs to the final XML
                Document xmlDoc = (Document) responseMap.get(key);
                NodeList nl = xmlDoc.getChildNodes();
                DOMUtil.addSection(responseDocument, nl);
            }


            //retrieve the forward info fromt the pageData, and remove it from the pageData. 
            if (pageData.get("forwardToType") != null)
            {
                forwardToType = pageData.get("forwardToType").toString();
                forwardToName = pageData.get("forwardToName").toString();

                pageData.remove("forwardToType");
                pageData.remove("forwardToName");
            }


            if (pageData.get("addOnTypeXML") != null)
            {
                Document addOnTypeXML = (Document) pageData.get("addOnTypeXML");
                DOMUtil.addSection(responseDocument, addOnTypeXML.getChildNodes());
                pageData.remove("addOnTypeXML");
            }
            // Changes for Apollo.Q3.2015 - Addon-Occasion association
            if (pageData.get("occasionsXML") != null)
            {
                Document occasionsXML = (Document) pageData.get("occasionsXML");
                DOMUtil.addSection(responseDocument, occasionsXML.getChildNodes());
                pageData.remove("occasionsXML");
            }
            
            
            //Convert the page data hashmap to XML and append it to the final XML
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            //check if we have to forward to another action, or just transform the XSL.
            if (forwardToType.equalsIgnoreCase("ACTION"))
            {
                forward = mapping.findForward(forwardToName);
                logger.info("forward: " + forward);
                return forward;
            }
            else if (forwardToType.equalsIgnoreCase("XSL"))
            {
                //Get XSL File name
                File xslFile = getXSL(forwardToName,mapping);
                if (xslFile != null && xslFile.exists())
                {
                    //Transform the XSL File
                    try
                    {
                        forward = mapping.findForward(forwardToName);
                        String xslFilePathAndName = forward.getPath(); //get real file name

                        HashMap params = getParameters(request);

                        // Change to client side transform
                        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, xslFile, 
                                                                       xslFilePathAndName, params);
                    }
                    catch (Exception e)
                    {
                        logger.error("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + 
                                     " transformation failed" + forwardToName, e);
                        if (!response.isCommitted())
                        {
                            throw e;
                        }
                    }
                }
                else
                {
                    throw new Exception("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + 
                                        " does not exist." + forwardToName);
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        catch (Exception e)
        {
            logger.error(e);
            forward = mapping.findForward("ErrorPage");
            return forward;
        }

        finally
        {
            try
            {
                //Close the connection
                if (con != null)
                {
                    con.close();
                }
            }
            catch (Exception e)
            {
                logger.error(e);
                forward = mapping.findForward("ErrorPage");
                return forward;
            }
        }
    }



    /** 
     * This method walks the request through its processing steps
     * @throws Exception
     */
    
    private HashMap processRequest(HttpServletRequest request,HttpServletResponse response,HashMap pageData, Connection con)
        throws Exception
    {
    	HashMap inputMap=new HashMap();
    	HashMap responseMap = new HashMap();
    	
        //Get info from the request object
        inputMap = getRequestInfo(request,inputMap);

        //Get config file info
        getConfigInfo(inputMap);

        //process the action
        processAction(inputMap,pageData,responseMap,response,con);

        //add Novator environment
        processNovatorEnvironment(responseMap);


        //populate the remainder of the fields on the page data
        populatePageData(pageData,inputMap);
        
        return responseMap;

    }


    /** 
     * Retrieves the request information from the request parameter
     * @throws Exception
     */
    private HashMap getRequestInfo(HttpServletRequest request,HashMap inputMap)
        throws Exception
    {
        HashMap requestMap = new HashMap();
       
        requestMap = this.retrieveRequestParms(request);

        AddOnVO addOnVO = new AddOnVO();

        //request paramaters
        String requestActionType = null;
        String requestAdminAction = null;
        String requestContext = null;
        String requestSecurityToken = null;
        String requestAddOnId = null;
        String requestOriginalActionType = null;
        String requestOrigAddOnId = null;
        String requestAddOnTypeOption = null;
        String requestAddOnDescription = null;
        String requestAddOnText = null;
        String requestAddOnPrice = null;
        String requestDisplayPriceOption = null;
        String requestProductId = null;
        String requestAvailableOption = null;
        String requestAddOnDefaultOption = null;
        String requestUnspsc = null;
        String requestWeight = null;
        String requestAddOnDeliveryType = null ;
        String requestAddOnTypeDescription = null;
        String linkedOccasions = null;// Changes for Apollo.Q3.2015 - Addon-Occasion association
        String requestPQuadAccId = null;// DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON. 
        String requestFtdWestAddon = null;// DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.


        //****retrieve the action_type
        if (requestMap.get("action_type") != null && !((String) requestMap.get("action_type")).equalsIgnoreCase(""))
            requestActionType = (String) requestMap.get("action_type");

        //****retrieve the original_action_type
        if (requestMap.get("original_action_type") != null && !((String) requestMap.get("original_action_type")).equalsIgnoreCase(""))
            requestOriginalActionType = (String) requestMap.get("original_action_type");

        //****retrieve the adminAction
        if (requestMap.get("adminAction") != null && !((String) requestMap.get("adminAction")).equalsIgnoreCase(""))
            requestAdminAction = (String) requestMap.get("adminAction");

        //****retrieve the context
        if (requestMap.get("context") != null && !((String) requestMap.get("context")).equalsIgnoreCase(""))
            requestContext = (String) requestMap.get("context");

        //****retrieve the securitytoken
        if (requestMap.get("securitytoken") != null && 
            !((String) requestMap.get("securitytoken")).equalsIgnoreCase(""))
            requestSecurityToken = (String) requestMap.get("securitytoken");

        //****retrieve the tAddOnId
        if (requestMap.get("tAddOnId") != null && !((String) requestMap.get("tAddOnId")).equalsIgnoreCase(""))
        {
            requestAddOnId = (String) requestMap.get("tAddOnId");
            addOnVO.setAddOnId(request.getParameter("tAddOnId"));
        }

        //****retrieve the hAddOnId
        if (request.getAttribute("hAddOnId") != null && 
            !((String) request.getAttribute("hAddOnId")).equalsIgnoreCase(""))
            requestOrigAddOnId = (String) request.getAttribute("hAddOnId");

        //****retrieve the sAddOnTypeOption
        if (requestMap.get("sAddOnTypeOption") != null && 
            !((String) requestMap.get("sAddOnTypeOption")).equalsIgnoreCase(""))
        {
            requestAddOnTypeOption = (String) requestMap.get("sAddOnTypeOption");
            addOnVO.setAddOnTypeId(request.getParameter("sAddOnTypeOption"));
        }

        //****retrieve the addOnTypeDescription
        if (requestMap.get("hAddOnTypeDescription") != null && 
            !((String) requestMap.get("hAddOnTypeDescription")).equalsIgnoreCase(""))
        {
            requestAddOnTypeDescription = (String) requestMap.get("hAddOnTypeDescription");
            addOnVO.setAddOnTypeDescription(requestAddOnTypeDescription);
        }

        //****retrieve the tAddOnDescription
        if (requestMap.get("tAddOnDescription") != null && 
            !((String) requestMap.get("tAddOnDescription")).equalsIgnoreCase(""))
        {
            requestAddOnDescription = (String) requestMap.get("tAddOnDescription");
            addOnVO.setAddOnDescription(requestAddOnDescription);
        }


        //****retrieve the tAddOnText
        if (requestMap.get("tAddOnText") != null && !((String) requestMap.get("tAddOnText")).equalsIgnoreCase(""))
        {
            requestAddOnText = (String) requestMap.get("tAddOnText");
            addOnVO.setAddOnText(requestAddOnText);
        }

        //****retrieve the tAddOnPrice
        if (requestMap.get("tAddOnPrice") != null && !((String) requestMap.get("tAddOnPrice")).equalsIgnoreCase(""))
        {
            requestAddOnPrice = (String) requestMap.get("tAddOnPrice");
            addOnVO.setAddOnPrice(request.getParameter("tAddOnPrice"));
        }

        //****retrieve the sDisplayPriceOption
        if (requestMap.get("sDisplayPriceOption") != null && 
            !((String) requestMap.get("sDisplayPriceOption")).equalsIgnoreCase(""))
        {
            requestDisplayPriceOption = (String) requestMap.get("sDisplayPriceOption");
            addOnVO.setDisplayPriceFlag(request.getParameter("sDisplayPriceOption").equals("Yes"));
        }

        //****retrieve the tProductId
        if (requestMap.get("tProductId") != null && !((String) requestMap.get("tProductId")).equalsIgnoreCase(""))
        {
            requestProductId = (String) requestMap.get("tProductId");
            addOnVO.setProductId(request.getParameter("tProductId"));
        }

        //****retrieve the sAvailableOption
        if (requestMap.get("sAvailableOption") != null && 
            !((String) requestMap.get("sAvailableOption")).equalsIgnoreCase(""))
        {
            requestAvailableOption = (String) requestMap.get("sAvailableOption");
            addOnVO.setAddOnAvailableFlag(request.getParameter("sAvailableOption").equals("Yes"));
        }

        //****retrieve the sAddOnDefaultOption
        if (requestMap.get("sAddOnDefaultOption") != null && 
            !((String) requestMap.get("sAddOnDefaultOption")).equalsIgnoreCase(""))
        {
            requestAddOnDefaultOption = (String) requestMap.get("sAddOnDefaultOption");
            addOnVO.setDefaultPerTypeFlag(request.getParameter("sAddOnDefaultOption").equals("Yes"));
        }

        //****retrieve the tUnspsc
        if (requestMap.get("tUnspsc") != null && !((String) requestMap.get("tUnspsc")).equalsIgnoreCase(""))
        {
            requestUnspsc = (String) requestMap.get("tUnspsc");
            addOnVO.setAddOnUNSPSC(request.getParameter("tUnspsc"));
        }

        //****retrieve the tWeight
        if (requestMap.get("tWeight") != null && !((String) requestMap.get("tWeight")).equalsIgnoreCase(""))
        {
            requestWeight = (String) requestMap.get("tWeight");
            addOnVO.setAddOnWeight(request.getParameter("tWeight"));
        }
        
        // #552 - Flexible Fullfillment - Addon Dash board and Maintanance changes 
        //****retrieve the tDeliverytype
        if (requestMap.get("tDeliverytype") != null && !((String) requestMap.get("tDeliverytype")).equalsIgnoreCase(""))
        {
        	requestAddOnDeliveryType = (String) requestMap.get("tDeliverytype");
            addOnVO.setAddOnDeliveryType(request.getParameter("tDeliverytype"));
        }

        //****retrieve the add_on_id
        if (requestMap.get("add_on_id") != null && !((String) requestMap.get("add_on_id")).equalsIgnoreCase(""))
        {
            requestAddOnId = (String) requestMap.get("add_on_id");
        }
        
        // Changes for Apollo.Q3.2015 - Addon-Occasion association
        //retrieve hLinkedOccasions
        if (requestMap.get("hLinkedOccasions") != null && !("").equalsIgnoreCase((String) requestMap.get("hLinkedOccasions")))
        {
        	linkedOccasions = (String) requestMap.get("hLinkedOccasions");
        	addOnVO.setsLinkedOccasions(linkedOccasions);
        	logger.info("linkedOccasions:"+linkedOccasions);
        }
        
        /*
         *  DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
         *  Capturing the attribute values provided on Add/Edit Add-on page.
         *  Added trim() on PQuadAccessoryID to prevent the cases where user provided just multiple spaces as PQuad ID and it could result in NumberFormatException 
         *  while saving it to DB (BigDecimal) 
         */
        if (requestMap.get("tPQuadAccId") != null && !("").equalsIgnoreCase(((String) requestMap.get("tPQuadAccId")).trim()))
        {
        	requestPQuadAccId = (String) requestMap.get("tPQuadAccId");
        	addOnVO.setsPquadAccsryId(requestPQuadAccId);
        	logger.info("requestPQuadAccId:"+requestPQuadAccId);
        }

        if (requestMap.get("isFtdWestAddon") != null && !("").equalsIgnoreCase((String) requestMap.get("isFtdWestAddon")))
        {
        	requestFtdWestAddon = (String) requestMap.get("isFtdWestAddon");
        	addOnVO.setFtdWestAddon("Yes".equalsIgnoreCase(requestFtdWestAddon));
        	logger.info("requestPQuadAccId:"+requestPQuadAccId);
        }


        NovatorUpdateVO novatorUpdateVO = buildNovatorUpdateVO(requestMap);

        
		inputMap.put("requestNovatorUpdateVO", novatorUpdateVO);
        inputMap.put("addOnVO", addOnVO);


        inputMap.put("requestActionType",            requestActionType);
        inputMap.put("requestOriginalActionType",    requestOriginalActionType);
        inputMap.put("requestAdminAction",           requestAdminAction);
        inputMap.put("requestContext",               requestContext);
        inputMap.put("requestSecurityToken",         requestSecurityToken);
        inputMap.put("add_on_id",                    requestAddOnId);
        inputMap.put("requestProductId",             requestProductId);
        inputMap.put("requestAddOnId",               requestAddOnId);
        inputMap.put("linkedOccasions",               linkedOccasions); // Changes for Apollo.Q3.2015 - Addon-Occasion association
        
        return inputMap;
    }


    /**  
     * Pulls in the security information
     * @throws Exception
     */
    private void getConfigInfo(HashMap inputMap)
        throws Exception
    {
        String csrId = null;
        String securityToken = (String) inputMap.get("requestSecurityToken");

        if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
        {
            csrId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
        }

        inputMap.put("csrId", csrId);
    }


    /** 
     * Determines the type of action the request is for and calls the appropriate BO methods
     * @throws Exception
     */
    private void processAction(HashMap inputMap,HashMap pageData, HashMap responseMap,HttpServletResponse response,Connection con)
        throws Exception
    {
        AddOnMaintenanceBO aomBO = new AddOnMaintenanceBO();
        String requestActionType = (String) inputMap.get("requestActionType");

        
		//If the action equals "add"
        if (requestActionType.equalsIgnoreCase("add"))
        {
            aomBO.processAdd(inputMap, pageData, con);
        }
        //If the action equals "edit"
        else if (requestActionType.equalsIgnoreCase("edit"))
        {
            aomBO.processEdit(inputMap, responseMap, pageData, con);
        }
        //If the action equals "main_menu"
        else if (requestActionType.equalsIgnoreCase("main_menu"))
        {
            aomBO.processMainMenu(inputMap, pageData, con);
        }
        //If the action equals "dashboard"
        else if (requestActionType.equalsIgnoreCase("dashboard"))
        {
            aomBO.processDashboard(inputMap, pageData, con);
        }
        //If the action equals "save"
        else if (requestActionType.equalsIgnoreCase("save"))
        {
            aomBO.processSave(inputMap, responseMap, pageData, con);
        }
        else if (requestActionType.equalsIgnoreCase("validate_product_id"))
        { 
            aomBO.processValidateProductId(inputMap,response, con);
        }
        else if (requestActionType.equalsIgnoreCase("validate_add_on_id"))
        {
            aomBO.processValidateAddOnId(inputMap,response, con);
        }
        //If the action is not found as above, throw a new exception
        else
        {
            throw new Exception("Invalid Action Type - Please correct");
        }

    }


    /** 
     * Places the necessary information into the pagedata object
     * @throws Exception
     */
    private void populatePageData(HashMap pageData, HashMap inputMap)
        throws Exception
    {
        //store the admin action
        pageData.put("adminAction", (String) inputMap.get("requestAdminAction"));

        //store the security token
        pageData.put("context", (String) inputMap.get("requestContext"));

        //store the security token
        pageData.put("securitytoken", (String) inputMap.get("requestSecurityToken"));

    }


     /** 
      * Retrieve the XSL file that the request maps to
      * @param forwardToName
      * @param mapping
      * @return filename of the XSL file
      */

    private File getXSL(String forwardToName, ActionMapping mapping)
    {
        File xslFile = null;
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardToName);
        xslFilePathAndName = forward.getPath(); //get real file name
        xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }

    /** 
     * Grabs the parameters and action_type attributes from the request object
     * @param request
     * @return contains the parameters and action_type attributes from the request object
     */
   
    private HashMap getParameters(HttpServletRequest request)
    {
        HashMap parameters = new HashMap();

        try
        {
            String action = request.getParameter("action_type");
            parameters = (HashMap) request.getAttribute("parameters");
            parameters.put("action", action);

        }
        finally
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Exiting getParameters");
            }
        }
        return parameters;
    }


    /**
     * Query the reques for non-multipart/form-data request, build a HashMap of input request parms, and return the HashMap
     * @param request HttpServletRequest
     * @return HashMap of parameters
     * @throws Exception
     */
    private HashMap retrieveRequestParms(HttpServletRequest request)
        throws Exception
    {
        HashMap requestMap = new HashMap();

        for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); )
        {
            String key = (String) i.nextElement();
            String value = request.getParameter(key);
            
            logger.info(key+"-"+value);

            if ((value != null) && (value.trim().length() > 0))
            {
                requestMap.put(key, value.trim());
            }
        }
        return requestMap;
    }

    /**
     * Build a NovatorUpdateVO from the page request
     * @param requestMap HashMap
     * @return NovatorUpdateVO containing Novator feed information
     */
    private NovatorUpdateVO buildNovatorUpdateVO(HashMap requestMap)
    {
        NovatorUpdateVO vo = new NovatorUpdateVO();
        String updateContent = (String) requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
        if (updateContent != null && updateContent.equals("on"))
        {
            vo.setUpdateContent(true);
        }
        String updateLive = (String) requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
        if (updateLive != null && updateLive.equals("on"))
        {
            vo.setUpdateLive(true);
        }
        String updateTest = (String) requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_TEST);
        if (updateTest != null && updateTest.equals("on"))
        {
            vo.setUpdateTest(true);
        }
        String updateUAT = (String) requestMap.get(OPMConstants.PARAMETER_NOVATOR_UPDATE_UAT);
        if (updateUAT != null && updateUAT.equals("on"))
        {
            vo.setUpdateUAT(true);
        }

        return vo;
    }

    /**
     * Builds the XML to populate the Novator feed information
     * @throws Exception
     */
    private void processNovatorEnvironment(HashMap responseMap)
        throws Exception
    {
        NovatorFeedUtil feedUtil = new NovatorFeedUtil();
        Document updateSetupDoc = feedUtil.getNovatorEnvironmentSetup();

        responseMap.put("HK_novator_environments", updateSetupDoc);
    }
}
