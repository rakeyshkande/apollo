package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.InventoryVO;
import com.ftd.op.maintenance.vo.NotificationVO;
import com.ftd.op.maintenance.vo.ProductSubCodeVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class VendorInventoryControlAction extends Action
{
    private Logger logger;
    
    private static final String INVALID_FORMAT = "Invalid Format";
    private static final String REQUIRED = "Required";
    
    public VendorInventoryControlAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.VendorInventoryControlAction");    
    }


  /**
	 * 
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
    
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try{
    
            actionType = request.getParameter("action_type");
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            VendorDAO dao = new VendorDAO(conn);
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;           


        
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
             userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
                
            String updateAllowed = null;
            //check if user has edit abilities
            SecurityManager securityManager = SecurityManager.getInstance();
            if (securityManager.assertPermission(context, sessionId, "InventoryMaint", OPMConstants.SECURITY_UPDATE))
            {
              updateAllowed = "Y";
            }
            else
            {
              updateAllowed = "N";
            }                       
                
            //create page data document
            Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
            Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
            pageDataDoc.appendChild(pageDataXML);     
            XMLUtil.addElement((Document)pageDataDoc, "securitytoken", sessionId);
            XMLUtil.addElement((Document)pageDataDoc, "context", context);
            XMLUtil.addElement((Document)pageDataDoc, "adminAction", request.getParameter("adminAction"));                    
            XMLUtil.addElement((Document)pageDataDoc,"can_edit",updateAllowed); 
            XMLUtil.addElement((Document)pageDataDoc,"from_page",request.getParameter("from_page"));                        
            XMLUtil.addElement((Document)pageDataDoc,"product_id",request.getParameter("product_id"));                   
            XMLUtil.addElement((Document)pageDataDoc,"vendor_id",request.getParameter("vendor_id"));
        
            logger.debug("**********************************************************");        
            logger.debug("Product id: "+request.getParameter("product_id"));
            logger.debug("Vendor id: "+request.getParameter("vendor_id"));
            logger.debug("**********************************************************");
            
            //saves update the this value, so we can't just grab it from request
            String updateType = request.getParameter("update_type");
            //if(actionType != null && !actionType.equals("save"))
            if(updateType != null)
            {
                XMLUtil.addElement((Document)pageDataDoc,"update_type",request.getParameter("update_type"));                
            }                
                
            /* determine which action to process */            
            if(actionType == null || actionType.equals("display"))
            {
                forward = processDisplayInventory(request, response, mapping,conn,(Document)pageDataDoc);
            }
            else if (actionType.equals("save"))
            {
                forward = processSave(request, response, mapping,conn,(Document)pageDataDoc);
            }
            else if (actionType.equals("delete"))
            {
                forward = processDelete(request, response, mapping,conn,(Document)pageDataDoc);
            }
            else if (actionType.equals("add_email"))
            {
                forward = processAddEmail(request, response, mapping,conn,(Document)pageDataDoc);
            }
            else if (actionType.equals("remove_email"))
            {
                forward = processRemoveEmail(request, response, mapping,conn,(Document)pageDataDoc);
            }            
            else if (actionType.equals("back"))
            {
                forward = processBack(request, response, mapping,conn);
            }            
            else
            {
                throw new Exception("Unknown action type:" + actionType);
            }
        }
		catch (Throwable  t)
		{
			logger.error(t);
			forward = mapping.findForward("ErrorPage");
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
				forward = mapping.findForward("Error");
			}
		}    
        
        return forward;
    
    }    
    
    
    private ActionForward processBack(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn) throws Exception
    {
        //get the from page
        String fromPage = request.getParameter("from_page");

        ActionForward actionForward = null;
        
        //if from product list
        if(fromPage.equals("product_list"))
        {

            //forward to product list page
            actionForward = mapping.findForward("ProductList");
            String path = actionForward.getPath() 
                        + "?" + "action_type=display"
                        + "&" + OPMConstants.SEC_TOKEN + "=" + request.getParameter(OPMConstants.SEC_TOKEN)
                        + "&" + OPMConstants.CONTEXT + "=" +request.getParameter(OPMConstants.CONTEXT);            
        }
        else if(fromPage.equals("product_search"))
        {
            //forward to product list page
            actionForward = mapping.findForward("ProductSearch");
            String path = actionForward.getPath() 
                        + "?" + "action_type=display"
                        + "&" + OPMConstants.SEC_TOKEN + "=" + request.getParameter(OPMConstants.SEC_TOKEN)
                        + "&" + OPMConstants.CONTEXT + "=" +request.getParameter(OPMConstants.CONTEXT);            
            
        }
        else
        {
            throw new Exception("Unknown from page:" + fromPage);
        }
        
        return actionForward;
    }
    
    
    /**
     * Display the list of available vendors
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private ActionForward processDisplayInventory(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, Document pageDataDoc) throws Exception
    {

        VendorDAO vendorDAO = new VendorDAO(conn);  
        MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
    
        //create base document
        Document xmlDoc = (Document) DOMUtil.getDocument();   

        //get user
        String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
        UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
        String context = (String) request.getParameter(OPMConstants.CONTEXT);
        
        //get request info
        String productID = request.getParameter("product_id");
        
        //get the vendor id from the request
        String vendorId = request.getParameter("vendor_id");
            
        //check if entered product is a subcode
        ProductSubCodeVO subCodeVO = maintDAO.getSubcode(productID); 
           
        //get the inventory data
        //if subocode use the subcode id
        Document invRecord = null;    
        if(subCodeVO != null)
        {
            invRecord = vendorDAO.getInventoryRecord(subCodeVO.getProductSubCodeId(), vendorId);            
        }
        else
        {
            invRecord = vendorDAO.getInventoryRecord(productID, vendorId);            
        }
        //invRecord.print(System.out);
        //System.out.println("----------------------------------------");        
        //get inventory report
        Document invReport = vendorDAO.getInventoryReport(productID, vendorId);
        //invReport.print(System.out);        
        //Combine pages
        DOMUtil.addSection(xmlDoc,invRecord.getChildNodes());
        DOMUtil.addSection(xmlDoc,invReport.getChildNodes());
        DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());
        
        //transform page
        logger.debug(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductInventoryXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));
        
        return null;
    }    
    
    
    /**
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private ActionForward processAddEmail(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, Document pageDataDoc) throws Exception
    {    
        boolean validEmail = true;
    
        //create base document
        Document xmlDoc = (Document) DOMUtil.getDocument();   
        
        //get request info
        String fromPage = request.getParameter("from_page");        
        
        //get the entered email address
        String enteredEmail = request.getParameter("notice_email");
        
        //error XML
        Document errorXML = (Document)getDocument("errors");
        
        //validated email address
        if(enteredEmail.indexOf("@") > 0 && enteredEmail.indexOf(".") > 0)
        {
            validEmail = true;
        }
        else //invalid
        {
            validEmail = false;
            XMLUtil.addElement(errorXML,"notice_email_error"," Invalid email");
        }
        
        //get all the entered values from the page and put into an XML document        
        Document vendorXML = getPageXML(request,validEmail,!validEmail,false,false);            

        VendorDAO vendorDAO = new VendorDAO(conn);
        Document invReport = vendorDAO.getInventoryReport(request.getParameter("product_id"),
                                                             request.getParameter("vendor_id") );
        
        
        //combine documents
        DOMUtil.addSection(xmlDoc,vendorXML.getChildNodes());   
        DOMUtil.addSection(xmlDoc,errorXML.getChildNodes());   
        DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());   
        DOMUtil.addSection(xmlDoc,invReport.getChildNodes());
        
        //transform page
        logger.debug(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductInventoryXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));
        
        return null;
    }
    
    
    private Document getPageXML(HttpServletRequest request,boolean includeEmailTextBox,boolean keepEmailTextBox,
                boolean removedCheckEmails, boolean removeAllEmails)
        throws Exception
    {
        
        Document inventorydataXML = (Document)getDocument("inventorydata");
    
        Document inventoryXML = (Document)getDocument("inventory");
        Document inventoryRecordsXML = (Document)getDocument("records");
        Document inventoryRecordXML = (Document)getDocument("record");        
        XMLUtil.addElement(inventoryRecordXML,"product_id",request.getParameter("product_id"));
        XMLUtil.addElement(inventoryRecordXML,"status",request.getParameter("status"));
        XMLUtil.addElement(inventoryRecordXML,"product_name",request.getParameter("product_name"));
        XMLUtil.addElement(inventoryRecordXML,"vendor_id",request.getParameter("vendor_id"));
        XMLUtil.addElement(inventoryRecordXML,"vendor_name",request.getParameter("vendor_name"));
        XMLUtil.addElement(inventoryRecordXML,"updated_on",request.getParameter("updated_on"));
        XMLUtil.addElement(inventoryRecordXML,"contact_number",request.getParameter("contact_number"));
        XMLUtil.addElement(inventoryRecordXML,"contact_extension",request.getParameter("contact_extension"));
        XMLUtil.addElement(inventoryRecordXML,"start_date",request.getParameter("start_date"));
        XMLUtil.addElement(inventoryRecordXML,"end_date",request.getParameter("end_date"));
        XMLUtil.addElement(inventoryRecordXML,"inventory_level",request.getParameter("inventory_level"));
        XMLUtil.addElement(inventoryRecordXML,"notice_threshold",request.getParameter("notice_threshold"));
        if(keepEmailTextBox)
        {
            XMLUtil.addElement(inventoryRecordXML,"notice_email",request.getParameter("notice_email"));            
        }
        DOMUtil.addSection(inventoryRecordsXML,inventoryRecordXML.getChildNodes());
        DOMUtil.addSection(inventoryXML,inventoryRecordsXML.getChildNodes());

        Document notificationXML = (Document)getDocument("notifications");
        Document notificationRecordsXML = (Document)getDocument("records");
        
        //get email addresses until there are no more
        int emailId = 1;
        String email = request.getParameter("email" + emailId);
        while(!removeAllEmails && email != null && email.length() > 0)
        {
            String checkBox = request.getParameter("emailChkBox" + emailId);
            if(checkBox == null)
            {
                checkBox = "off";
            }
            
            if((removedCheckEmails && !checkBox.equalsIgnoreCase("on")) || !removedCheckEmails)
            {
                Document notificationRecordXML = (Document)getDocument("record");        
                XMLUtil.addElement(notificationRecordXML,"product_id",request.getParameter("product_id"));
                XMLUtil.addElement(notificationRecordXML,"email_address",email);
                DOMUtil.addSection(notificationRecordsXML,notificationRecordXML.getChildNodes());                    
            }            
            
            email = request.getParameter("email" + ++emailId);
        }
        if(includeEmailTextBox)
        {
            Document notificationRecordXML = (Document)getDocument("record");        
            XMLUtil.addElement(notificationRecordXML,"product_id",request.getParameter("product_id"));
            XMLUtil.addElement(notificationRecordXML,"email_address",request.getParameter("notice_email"));
            DOMUtil.addSection(notificationRecordsXML,notificationRecordXML.getChildNodes());                                
        }
        DOMUtil.addSection(notificationXML,notificationRecordsXML.getChildNodes());    
        
        //add both parts to one doc
        DOMUtil.addSection(inventorydataXML,inventoryXML.getChildNodes());    
        DOMUtil.addSection(inventorydataXML,notificationXML.getChildNodes());    


        return inventorydataXML;
    }
    
    private Document validate(HttpServletRequest request)
        throws Exception
    {
        Document errorXML = (Document)getDocument("errors");    
        String value = null;
    
        boolean error = false;
    
        //validate phone number ..must be 10 numbers, if entered
        String contactNumber = request.getParameter("contact_number");
        String phone = removeNonNumerics(contactNumber);
        if(contactNumber != null && contactNumber.length() > 0 && phone.length() != 10)
        {
            XMLUtil.addElement(errorXML,"contact_number",INVALID_FORMAT);
            error = true;
        }
        
        //validate start date required and valid date
        value = request.getParameter("start_date");
        if(value == null || value.length() == 0)
        {
            XMLUtil.addElement(errorXML,"start_date",REQUIRED);
            error = true;
        }
        else
        {
            try
            {
                //check if valid date
                FieldUtils.formatStringToUtilDate(value);                
            }
            catch(Exception e)
            {
                XMLUtil.addElement(errorXML,"start_date",INVALID_FORMAT);
                error = true;
            }
        }

        
        //validate end date is valid date, if entered
        value = request.getParameter("end_date");
        if(value != null || value.length() > 0)
        {
            try
            {
                //check if valid date
                FieldUtils.formatStringToUtilDate(value);                
            }
            catch(Exception e)
            {
                XMLUtil.addElement(errorXML,"end_date",INVALID_FORMAT);
                error = true;
            }
        }
        
        //validate inventory level is numeric, if entered
        value = request.getParameter("inventory_level");
         if(value != null && value.length() > 0)
         {
             //check if numeric
             try
             {
                 int number = Integer.parseInt(value);                 
             }
             catch(Exception e)
             {
                 XMLUtil.addElement(errorXML,"inventory_level",INVALID_FORMAT);
                 error = true;
             }
         }
        
        //validate threshold is positive numeric, if entered
        value = request.getParameter("notice_threshold");
         if(value != null && value.length() > 0)
         {
             //check if numeric
             try
             {
                 int number = Integer.parseInt(value);  
                 if(number < 0)
                 {
                     XMLUtil.addElement(errorXML,"notice_threshold",INVALID_FORMAT);
                     error = true;
                 }
             }
             catch(Exception e)
             {
                 XMLUtil.addElement(errorXML,"notice_threshold",INVALID_FORMAT);
                 error = true;
             }
         }
         
        return error ? errorXML : null;
        
        
    }
    
    public String removeNonNumerics(String input)
    {
        String output = "";
    
        for (int i=0; i < input.length(); i++)
        {
            char n = input.charAt(i);
            Character nextCharacter = new Character(n);            
            if(nextCharacter.isDigit(n))
            {
                output = output + nextCharacter.toString();
            }
            
        }
   
        return output;
    }
    
    private InventoryVO getPageVO(HttpServletRequest request)
        throws Exception
    {
        
        InventoryVO invVO = new InventoryVO();
    
        invVO.setContactExt(request.getParameter("contact_extension"));
        invVO.setContactNumber(removeNonNumerics(request.getParameter("contact_number")));
        
        if(request.getParameter("start_date") != null && request.getParameter("start_date").length() > 0)
        {
            invVO.setStartDate(FieldUtils.formatStringToUtilDate(request.getParameter("start_date")));            
        }
        if(request.getParameter("end_date") != null && request.getParameter("end_date").length() > 0)
        {
            invVO.setEndDate(FieldUtils.formatStringToUtilDate(request.getParameter("end_date")));            
        }
        if(request.getParameter("inventory_level") != null && request.getParameter("inventory_level").length() > 0)
        {
            invVO.setInventoryLevel(new Integer(request.getParameter("inventory_level")));            
        }
        if(request.getParameter("notice_threshold") != null && request.getParameter("notice_threshold").length() > 0)
        {
            invVO.setInventoryThreshold(new Integer(request.getParameter("notice_threshold")));            
        }
        
        invVO.setProductId(request.getParameter("product_id"));
        invVO.setProductName(request.getParameter("product_name"));
        invVO.setProductStatus(request.getParameter("status"));
        invVO.setVendorId(request.getParameter("vendor_id"));
        invVO.setVendorName(request.getParameter("vendor_name"));
        
        List notifications = new ArrayList();        
        //get email addresses until there are no more
        int emailId = 1;
        String email = request.getParameter("email" + emailId);
        while(email != null && email.length() > 0)
        {
            NotificationVO notifVO = new NotificationVO();        
            notifVO.setEmailAddress(email);
            notifVO.setProductId(request.getParameter("product_id"));
            notifVO.setVendorId(request.getParameter("vendor_id"));
            notifications.add(notifVO);
            email = request.getParameter("email" + ++emailId);
        }
        
        //add emails to inventory vo
        invVO.setNotifications(notifications);


        return invVO;
    }    

    /**
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private ActionForward processRemoveEmail(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, Document pageDataDoc) throws Exception
    {           
    
        //get remove type from the request    
        String removeType = request.getParameter("remove_type");
        
        //get request info
        String fromPage = request.getParameter("from_page");        
   
        //create base document
        Document xmlDoc = (Document) DOMUtil.getDocument();                            
            
        //combine documents
        Document vendorXML = null;
        if(removeType.equals("remove_checked"))
        {
            //get all the entered values from the page and put into an XML document        
            vendorXML = getPageXML(request,false,false,true,false);                        
        }
        else if(removeType.equals("remove_all"))
        {
            //get all the entered values from the page and put into an XML document        
            vendorXML = getPageXML(request,false,false,false,true);                                    
        }
        else
        {
            throw new Exception("Invalid remove type:" + removeType);
        }       

        VendorDAO vendorDAO = new VendorDAO(conn);
        Document invReport = vendorDAO.getInventoryReport(request.getParameter("product_id"),
                                                             request.getParameter("vendor_id") );
        
        //combine sections
        DOMUtil.addSection(xmlDoc,vendorXML.getChildNodes());   
        DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());   
        DOMUtil.addSection(xmlDoc,invReport.getChildNodes());
        
        //transform page
        logger.debug(XMLUtil.convertDocToString(xmlDoc));
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductInventoryXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));
        
        return null;
    
    }    
    
    /**
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private ActionForward processSave(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, Document pageDataDoc) throws Exception
    {               

        //Create main document
        Document xmlDoc = (Document) DOMUtil.getDocument();   

        //validate the data
        Document errorXML = validate(request);


        VendorDAO vendorDAO = new VendorDAO(conn);
       
        //if invalid 
        if(errorXML != null)
        {           
            //add error document
            DOMUtil.addSection(xmlDoc,errorXML.getChildNodes()); 
            XMLUtil.addElement((Document)pageDataDoc,"update_type",request.getParameter("update_type"));            
        }
        else  //valid
        {   
            //Add the update element
            XMLUtil.addElement((Document)pageDataDoc,"update_type","update");
        
            //get update type from request (insert or update)
            String updateType = request.getParameter("update_type");
            
            //get a VO from data in the request
            InventoryVO invVO = getPageVO(request);
            
            //if insert
            if(updateType.equals("insert"))
            {
                
                //call dao methods to insert inventory record, insert all notification emails
                vendorDAO.insertInventory(invVO);
                List notifications = invVO.getNotifications();
                if(notifications != null)
                {
                    Iterator iter = notifications.iterator();
                    while(iter.hasNext())
                    {
                      
                        NotificationVO notifyVO = (NotificationVO)iter.next();
                        vendorDAO.insertInventoryNotification(notifyVO);
                    }
                }
                
                //put message in XML: Record successfully added 
                XMLUtil.addElement((Document)pageDataDoc,"save_message","Record successfully added");                                     
                
            }     
            else if(updateType.equals("update"))
            {
            
                //call dao methods to udpate inventory record, delete all existing notifications, insert all notification emails
                vendorDAO.updateInventory(invVO);
                vendorDAO.deleteInventoryNotification(invVO.getProductId());
                List notifications = invVO.getNotifications();
                if(notifications != null)
                {
                    Iterator iter = notifications.iterator();
                    while(iter.hasNext())
                    {
                        NotificationVO notifyVO = (NotificationVO)iter.next();
                        vendorDAO.insertInventoryNotification(notifyVO);
                    }
                }
                
                //put message in XML: Record successfully udpated                 
                XMLUtil.addElement((Document)pageDataDoc,"save_message","Record successfully udpated");                                     
            }
            else
            {
                throw new Exception("Invalid update type:"+ updateType);
            }        
            
           
        }    
        
            //get all the entered values from the page and put into an XML document
            Document pageXML = getPageXML(request,false,false,false,false);
            Document invReport = vendorDAO.getInventoryReport(request.getParameter("product_id"),
                                                                 request.getParameter("vendor_id") );
          
            //combine documents
            DOMUtil.addSection(xmlDoc,pageXML.getChildNodes());   
            DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());   
            DOMUtil.addSection(xmlDoc,invReport.getChildNodes());
            
            //transform page
            logger.debug(XMLUtil.convertDocToString(xmlDoc));
            TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"ProductInventoryXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));                    
        
            return null;
    }      
    
    /**
     * 
     * @param request
     * @param response
     * @param mapping
     * @param conn
     * @throws java.lang.Exception
     */
    private ActionForward processDelete(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, Document pageDataDoc) throws Exception
    {  
    
        ActionForward actionForward = null;
    
        VendorDAO vendorDAO = new VendorDAO(conn);
    
        //get product id from request
        String productId = request.getParameter("product_id");
        String vendorId = request.getParameter("vendor_id");
        
        //call dao method to delete notification records and inventory record
        vendorDAO.deleteInventory(productId);
        vendorDAO.deleteInventoryNotification(productId);
        
        //get 'from_page' from request
        String fromPage = request.getParameter("from_page");
        
        //if from page is 'product_list'
        if(fromPage.equals("product_list"))
        {        
            //go back to product list page
             actionForward = mapping.findForward("ProductList");
             String path = actionForward.getPath() 
                            + "?vendor_id=" + vendorId
                            + "&" + OPMConstants.SEC_TOKEN + "=" + request.getParameter(OPMConstants.SEC_TOKEN)
                            + "&" + OPMConstants.CONTEXT + "=" +request.getParameter(OPMConstants.CONTEXT);
    
             actionForward = new ActionForward(path,true);            
            
        }            
        //else if from page is 'product_search'
        else if(fromPage.equals("product_search"))        
        {
            //go back to product search page      
             actionForward = mapping.findForward("ProductSearch");
             String path = actionForward.getPath() 
                            + "?vendor_id=" + vendorId
                            + "&" + OPMConstants.SEC_TOKEN + "=" + request.getParameter(OPMConstants.SEC_TOKEN)
                            + "&" + OPMConstants.CONTEXT + "=" +request.getParameter(OPMConstants.CONTEXT);
    
             actionForward = new ActionForward(path,true);                  
        }
        //else
        else
        {        
            //throw exception, invalid from page
            throw new Exception("Invalid from page:" + fromPage);
        }    
        
        return actionForward;
    }
    
 
    /**
     * Returns an empty DOM document with root node
     * 
     * @exception ParserConfigurationException
     * @return an empty DOM document with root node
     */
    private Document getDocument(String rootName) throws ParserConfigurationException
    {
        Document doc = DOMUtil.getDocumentBuilder().newDocument();
        Element root = (Element) doc.createElement(rootName);
        doc.appendChild(root);
        
        return doc;    
    } 
    
    public static void main(String args[])
    {
        try{
            VendorInventoryControlAction test = new VendorInventoryControlAction();
            System.out.println(test.removeNonNumerics("{630)555-1223"));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
}
