package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.dao.VendorDAO;
import com.ftd.op.maintenance.exception.NovatorTransmissionException;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.Transaction;
import com.ftd.op.maintenance.util.VendorUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.op.maintenance.vo.ContactVO;
import com.ftd.op.maintenance.vo.PartnerShippingAccountVO;
import com.ftd.op.maintenance.vo.VendorBlockVO;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.security.cache.vo.UserInfo;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.VendorAddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.util.ArrayList;

import java.util.HashSet;
import java.util.Iterator;

import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class VendorDetailAction  extends Action
{

    private Logger logger;
    private static final String ERROR_REQUIRED_FIELD = "Required";
    
    private static final String PARAMETER_VENDOR_BLOCK_DATE = "vendor_block_date";
    private static final String PARAMETER_VENDOR_BLOCK_TYPE = "vendor_block_type";
    private static final String PARAMETER_ORIG_VENDOR_TYPE = "orig_vendor_type";
    private static final String SDS_VENDOR_TYPE = "SDS";
   
    public static String ACTIVE_ZONE_JUMP_ERROR_MESSAGE = "";

    public VendorDetailAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.VendorDetailAction");
    }
    
  /**
   * 
   * 
   * @param mapping            - ActionMapping used to select this instance
   * @param form               - ActionForm (optional) bean for this request
   * @param request            - HTTP Request we are processing
   * @param response           - HTTP Response we are processing
   * @return forwarding action - next action to "process" request
   * @throws IOException
   * @throws ServletException
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try{
    
            /* get action type request parameter */
            if(request.getParameter("action_type")!=null)
            {
                actionType = request.getParameter("action_type");
            }
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            VendorDAO dao = new VendorDAO(conn);
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;
            
            if(sessionId==null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
             userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }


                
            /* determine which action to process */            
            if(actionType == null || actionType.equals("display_vendor"))
            {
                processDisplayVendor(request, response, mapping,conn,null,null,null,null,null,true);
            }
            else if (actionType.equals("delete_vendor"))
            {
                forward = processDeleteVendor(request, response, mapping,conn);
            }
            else if (actionType.equals("save_vendor"))
            {
                processSaveVendor(request, response, mapping,conn);
            }
            else if (actionType.equals("add_block"))
            {
                processAddVendorBlock(request, response, mapping,conn);
            }
            else if (actionType.equals("delete_vendor_block"))
            {
                processDeleteVendorBlock(request, response, mapping,conn);
            }
            else if (actionType.equals("new_vendor"))
            {
                //pass in a null vendor document so that the page will display blank text boxes
                Document xmlDoc = DOMUtil.getDocument();                
                processDisplayVendor(request, response, mapping,conn,null,xmlDoc,null,null,null,true);
            }
            else
            {
                throw new Exception("Unknown action type:" + actionType);
            }
        }
    catch (Throwable  t)
    {
      logger.error(t);
      forward = mapping.findForward("ErrorPage");
    }
    finally
    {
      try
      {
        conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        forward = mapping.findForward("ErrorPage");
      }
    }    
        
        return forward;
    
    }    
    
     private void processDeleteVendorBlock(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn)
        throws Exception
     {
        try
        {
            VendorDAO vendorDAO = new VendorDAO(conn);
            
            String errorText = null;
         
            //get needed values from request
            String blockDateString = request.getParameter("delete_vendor_date");
            String enteredBlockType = request.getParameter("vendor_delete_block_type");         
            String vendorId = request.getParameter("vendor_id");
        
        
             //convert date to string
             Date enteredBlockDate = null;
             try{
                enteredBlockDate = FieldUtils.formatStringToUtilDate(blockDateString);
             }
             catch(Exception e)
             {
                 errorText = "Invalid date format";
             }
            
            if(errorText != null)
            {
                //do not delete the record, invalid date was passed in
                Document errorXML = XMLUtil.createErrorXML();
                XMLUtil.addError(errorXML,"block_error","Invalid date was passed in for vendor.  Vendor block was not deleted.");
                processDisplayVendor(request, response,mapping, conn, errorXML, null, null, "display", null,true);
            }
            VendorBlockVO blockVO = new VendorBlockVO();
            blockVO.setVendorId(vendorId);
            blockVO.setStartDate(enteredBlockDate);
            blockVO.setGlobalFlag("N");
            if(enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING) || enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY))
            {
                blockVO.setBlockType(enteredBlockType);
                new VendorUtil().deleteVendorBlockSendNovator(blockVO, conn);
                processDisplayVendor(request, response,mapping, conn, null, null, null, "display", "comments",true);
            }
            else if(enteredBlockType.equalsIgnoreCase("Both"))
            {
                List bothBlocksList = new ArrayList();

                VendorBlockVO blockShipVO = new VendorBlockVO();
                blockShipVO.setVendorId(vendorId);
                blockShipVO.setStartDate(enteredBlockDate);
                blockShipVO.setGlobalFlag("N");
                blockShipVO.setBlockType(OPMConstants.BLOCK_TYPE_SHIPPING);
                bothBlocksList.add(blockShipVO);
                
                VendorBlockVO blockDelivVO = new VendorBlockVO();
                blockDelivVO.setVendorId(vendorId);
                blockDelivVO.setStartDate(enteredBlockDate);
                blockDelivVO.setGlobalFlag("N");
                blockDelivVO.setBlockType(OPMConstants.BLOCK_TYPE_DELIVERY);
                bothBlocksList.add(blockDelivVO);

                new VendorUtil().deleteVendorBlocksSendNovator(bothBlocksList, conn);

                processDisplayVendor(request, response,mapping, conn, null, null, null, "display", "comments",true);
            }
            else
            {
                throw new Exception("Invalid block type:" + enteredBlockType);
            }
        }
        catch(NovatorTransmissionException nte) 
        {
            String message = "Vendor block was not deleted.  Connectivity to Novator failed.";
            logger.error(message,nte);
            Document errorXML = XMLUtil.createErrorXML();
            XMLUtil.addError(errorXML, "error_message", message);
            processDisplayVendor(request,response,mapping,conn,errorXML,null, null,"display", "",true);
        }
     }
    /**
     * Add a vendor block
     */
     private void processAddVendorBlock(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn)
        throws Exception
     {
        try 
        {
            
             VendorDAO vendorDAO = new VendorDAO(conn);
             
             //get needed values from request
             String blockDateString = request.getParameter(PARAMETER_VENDOR_BLOCK_DATE);
             String enteredBlockType = request.getParameter(PARAMETER_VENDOR_BLOCK_TYPE);
             String vendorId = request.getParameter("vendor_id");
             Document errorXML = XMLUtil.createErrorXML();
    
             // check for date //
             if (blockDateString.equalsIgnoreCase(""))
             {
                 XMLUtil.addError(errorXML,"block_error","Vendor Block Date required");
                 processDisplayVendor(request, response,mapping, conn, errorXML, null, null, "display", null,true);         
             }
    
            String errorText = VendorUtil.validateDate(blockDateString);
            if (errorText != null)
            {
                XMLUtil.addError(errorXML,"block_error",errorText);
                processDisplayVendor(request, response,mapping, conn, errorXML, null, null, "display", null,true);         
            }
            Date enteredBlockDate = FieldUtils.formatStringToUtilDate(blockDateString);         
             
             //if shipping block
             if(enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING))
             {
                 //check to see if vendor is assigned to active zone jump trip
                 boolean zoneJumpTripExists = checkForZoneJumpTrip(vendorId, blockDateString, OPMConstants.BLOCK_TYPE_SHIPPING, conn);
                 if(zoneJumpTripExists)
                 {
                     XMLUtil.addError(errorXML,"block_error",this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE);
                     processDisplayVendor(request, response,mapping, conn, errorXML, null, null, "display", null,true);
                     return;
                 }
                 VendorBlockVO blockVO = new VendorBlockVO();
                 blockVO.setBlockType(OPMConstants.BLOCK_TYPE_SHIPPING);
                 blockVO.setEndDate(enteredBlockDate);
                 blockVO.setGlobalFlag("N");
                 blockVO.setStartDate(enteredBlockDate);
                 blockVO.setVendorId(vendorId);
                 new VendorUtil().insertVendorBlockSendNovator(blockVO, conn);
    
                 processDisplayVendor(request, response,mapping, conn, null, null, null, "display", "comments",true);
    
             }//end shipping block
             else if(enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY))
             {
                 
                 //check to see if vendor is assigned to active zone jump trip
                 boolean zoneJumpTripExists = checkForZoneJumpTrip(vendorId, blockDateString, OPMConstants.BLOCK_TYPE_DELIVERY, conn);
                 if(zoneJumpTripExists)
                 {
                     XMLUtil.addError(errorXML,"block_error",this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE);
                     processDisplayVendor(request, response,mapping, conn, errorXML, null, null, "display", null,true);
                     return;
                 }
                 VendorBlockVO blockVO = new VendorBlockVO();
                 blockVO.setBlockType(OPMConstants.BLOCK_TYPE_DELIVERY);
                 blockVO.setEndDate(enteredBlockDate);
                 blockVO.setGlobalFlag("N");
                 blockVO.setStartDate(enteredBlockDate);
                 blockVO.setVendorId(vendorId);
                 new VendorUtil().insertVendorBlockSendNovator(blockVO, conn);
                 
                 processDisplayVendor(request, response,mapping, conn, null, null, null, "display", "comments",true);
    
             }//end delivery block         
             else if(enteredBlockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_BOTH))
             {
                 //check to see if vendor is assigned to active zone jump trip
                 boolean zoneJumpTripExists = checkForZoneJumpTrip(vendorId, blockDateString, OPMConstants.BLOCK_TYPE_BOTH, conn);
                 if(zoneJumpTripExists)
                 {
                     XMLUtil.addError(errorXML,"block_error",this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE);
                     processDisplayVendor(request, response,mapping, conn, errorXML, null, null, "display", null,true);
                     return;
                 }
                 List bothBlocksList = new ArrayList();
                 
                 //insert delivery block
                 VendorBlockVO blockDelivVO = new VendorBlockVO();
                 blockDelivVO.setBlockType(OPMConstants.BLOCK_TYPE_DELIVERY);
                 blockDelivVO.setEndDate(enteredBlockDate);
                 blockDelivVO.setGlobalFlag("N");
                 blockDelivVO.setStartDate(enteredBlockDate);
                 blockDelivVO.setVendorId(vendorId);
                 bothBlocksList.add(blockDelivVO);
        
                 //insert shipping block
                 VendorBlockVO blockShipVO = new VendorBlockVO();
                 blockShipVO.setBlockType(OPMConstants.BLOCK_TYPE_SHIPPING);
                 blockShipVO.setEndDate(enteredBlockDate);
                 blockShipVO.setGlobalFlag("N");
                 blockShipVO.setStartDate(enteredBlockDate);
                 blockShipVO.setVendorId(vendorId);
                 bothBlocksList.add(blockShipVO);

                 new VendorUtil().insertVendorBlocksSendNovator(bothBlocksList, conn);
                 
                 processDisplayVendor(request, response,mapping, conn, null, null, null, "display", "comments",true);                                  
             }//end both
             else
             {
                 throw new Exception("Invalid block type:" + enteredBlockType);
             }
        }
        catch(NovatorTransmissionException nte) 
        {
            String message = "Vendor block was not saved.  Connectivity to Novator failed.";
            logger.error(message,nte);
            Document errorXML = XMLUtil.createErrorXML();
            XMLUtil.addError(errorXML, "error_message", message);
            processDisplayVendor(request,response,mapping,conn,errorXML,null, null,"display", "",true);
        }
     }    
    
    
    //Delete a vendor
    private ActionForward processDeleteVendor(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn)
        throws Exception
    {
            String vendorId = request.getParameter("vendor_id");
            
              ActionForward actionForward = null;            
            
            VendorDAO vendorDAO = new VendorDAO(conn);
            
            //get user
            String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            String context = (String) request.getParameter(OPMConstants.CONTEXT);          
            
            //delete the vendor
            vendorDAO.deleteVendor(vendorId);
            
            
             //forward to Cart page
             actionForward = mapping.findForward("VendorList");
             String path = actionForward.getPath() 
                            +"?popup=comments&vendor_id="+vendorId
                            + "&" + OPMConstants.SEC_TOKEN + "=" + request.getParameter(OPMConstants.SEC_TOKEN)
                            + "&" + OPMConstants.CONTEXT + "=" +request.getParameter(OPMConstants.CONTEXT);
    
             actionForward = new ActionForward(path,true);     
             
             return actionForward;
    }
      
    //This will be called to display a vendor in non-edit mode, edit mode, and when creating a new vendor.
    //This will also be called when returning validation errors from edit mode
    private void processDisplayVendor(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn, Document errorXML, Document vendorXML, Document addOnXML, String editMode, String popUp, boolean buildPartnerXML)
        throws Exception
    {
            VendorDAO vendorDAO = new VendorDAO(conn);
            MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
        
            
            String vendorId = request.getParameter("vendor_id");
            
            //get user
            String sessionId = (String) request.getParameter(OPMConstants.SEC_TOKEN);        
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            String context = (String) request.getParameter(OPMConstants.CONTEXT);  
            
            //if edit mode was not passed then get it from request
            if(editMode == null)
            {
                editMode = request.getParameter("edit_mode");                
            }

            //get edit mode, if null set to display
            if(editMode == null || editMode.length() == 0)
            {
                editMode = "display";
            }

            //check what rights the user has
            String canEditVendor = "N";
            String canAddVendor = "N";//if user can edit then they can add, BA does not want seperate user role
            String canBlockVendor = "N";
            String canDeleteVendor = "N";
            SecurityManager securityManager = SecurityManager.getInstance();
            if (securityManager.assertPermission(context, sessionId, "VendorMaint","Update"))
            {
                canEditVendor = "Y";
                canAddVendor = "Y";
            }
            if (securityManager.assertPermission(context, sessionId, "VendorMaint","Delete"))
            {
                canDeleteVendor = "Y";
            }
            if (securityManager.assertPermission(context, sessionId, "VendorMaintBlock","Update"))
            {
                canBlockVendor = "Y";
            }
    
   
        //create master XML
        Document xmlDoc = DOMUtil.getDocument();   

        //if edit mode is 'new' get the next vendor id
        String newVendorId = null;
        
        //if vendor information was not passed in then get it from the DB
        if(!editMode.equals("new") && vendorXML == null)
        {
            vendorXML = vendorDAO.getVendor(vendorId);            
        } 

        Node vendorIdNode = DOMUtil.selectSingleNode(vendorXML, "/vendor_details/vendors/vendor/vendor_id/text()");
        if (null != vendorIdNode) 
        {
          vendorId = vendorIdNode.getNodeValue();
        }
        
        //retrieve vendor blocks
        Document vendorBlockXML = vendorDAO.getVendorBlocksXML(vendorId);
        
        //retrieve global blocks
        Document globalBlockXML = vendorDAO.getGlobalBlocksXML();
        
        //retrieve state list
        Document stateXML = maintDAO.getStateList();
        
        //retreive ship via's
        Document shipViaXML = maintDAO.getCarrierInfo();
        
        //retrieve company codes
        Document companyXML = maintDAO.getCompanyCodeList();
        
        //retreive vendor types
        Document vendorTypeXML = vendorDAO.getVendorTypeList();
        
        //retrieve vendor carrier information
        Document vendorCarrierXML = maintDAO.getVendorCarrierRef(vendorId);
        
        // get vendor/partner/shipper account number information
        Document partnerShippingXML = null;
        if (buildPartnerXML){
            if (editMode.equalsIgnoreCase("new")){
                partnerShippingXML = vendorDAO.getDefaultShippingAccounts();
            }
            else {
                partnerShippingXML = vendorDAO.getPartnerShippingAccounts(vendorId);
            }
        }
        
        //retrieve add-on XML
        if (addOnXML == null)
        {
            addOnXML = vendorDAO.getVendorAddon(vendorId);            
        }

        //create page data document
        Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element pageDataXML = (Element) pageDataDoc.createElement(OPMConstants.REQUSET_PARAM_PAGE_DATA);
        pageDataDoc.appendChild(pageDataXML);     
        XMLUtil.addElement((Document)pageDataDoc,"can_edit_vendor",canEditVendor);  
        XMLUtil.addElement((Document)pageDataDoc,"can_block_vendor",canBlockVendor);         
        XMLUtil.addElement((Document)pageDataDoc,"can_add_vendor",canAddVendor); 
        XMLUtil.addElement((Document)pageDataDoc,"can_delete_vendor",canDeleteVendor); 
        XMLUtil.addElement((Document)pageDataDoc,"edit_mode",editMode); 
        XMLUtil.addElement((Document)pageDataDoc,"popup",popUp); 
        XMLUtil.addElement((Document)pageDataDoc,"newVendorId",newVendorId); 
        XMLUtil.addElement((Document)pageDataDoc, "securitytoken", sessionId);
        XMLUtil.addElement((Document)pageDataDoc, "context", context);
        XMLUtil.addElement((Document)pageDataDoc, "adminAction", request.getParameter("adminAction"));
        
        String origVendorType = request.getParameter(PARAMETER_ORIG_VENDOR_TYPE);
        if(origVendorType == null || "".equals(origVendorType))
        {
            Node vendorTypeNode = DOMUtil.selectSingleNode(vendorXML, "/vendor_details/vendors/vendor/vendor_type/text()");
            if (vendorTypeNode != null) 
            {
                origVendorType = vendorTypeNode.getNodeValue();
            }
        }
        XMLUtil.addElement((Document)pageDataDoc, "orig_vendor_type", origVendorType);

        //create selected carrier info to save the state of the SDS carrier list
        Document selectedCarrierXML = DOMUtil.getDocumentBuilder().newDocument();
        Element carriers = (Element) selectedCarrierXML.createElement("selected_carriers");
        selectedCarrierXML.appendChild(carriers);
        String vendorType = request.getParameter("vendor_type");
        //only want to set selected carriers for SDS vendors if the vendor is new or editable
        if(vendorType != null && SDS_VENDOR_TYPE.equalsIgnoreCase(vendorType) && (editMode.equalsIgnoreCase("new") || editMode.equalsIgnoreCase("edit")) ) {
            String[] selectedCarrierKeys = request.getParameterValues("active_carriers");
            for(int i = 0; i < selectedCarrierKeys.length; i++) {
                Element selectedCarrier = (Element) selectedCarrierXML.createElement("selected_carrier");
                Element carrierId = (Element) selectedCarrierXML.createElement("carrier_id");
                carrierId.appendChild(selectedCarrierXML.createTextNode(selectedCarrierKeys[i]));
                selectedCarrier.appendChild(carrierId);
                carriers.appendChild(selectedCarrier);
            }
        }
        
        //Combine pages
        if(vendorXML != null){
            DOMUtil.addSection(xmlDoc,vendorXML.getChildNodes());            
        }
        if(vendorBlockXML != null){
            DOMUtil.addSection(xmlDoc,vendorBlockXML.getChildNodes());            
        }
        if(addOnXML != null){
            DOMUtil.addSection(xmlDoc,addOnXML.getChildNodes());
        }
        if(globalBlockXML != null){
            DOMUtil.addSection(xmlDoc,globalBlockXML.getChildNodes());            
        }
        if(stateXML != null){
            DOMUtil.addSection(xmlDoc,stateXML.getChildNodes());            
        }
        if(shipViaXML != null){
            DOMUtil.addSection(xmlDoc,shipViaXML.getChildNodes());            
        }
        if(companyXML != null){
            DOMUtil.addSection(xmlDoc,companyXML.getChildNodes());            
        }
        if(vendorTypeXML != null){
            DOMUtil.addSection(xmlDoc,vendorTypeXML.getChildNodes());            
        }        
        if(pageDataDoc != null){
            DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());            
        }                        
        if (vendorCarrierXML != null) 
        {
          DOMUtil.addSection(xmlDoc, vendorCarrierXML.getChildNodes());
        }
        if (selectedCarrierXML != null) 
        {
            DOMUtil.addSection(xmlDoc,selectedCarrierXML.getChildNodes());            
        }
        if (partnerShippingXML != null) 
        {
            DOMUtil.addSection(xmlDoc,partnerShippingXML.getChildNodes());            
        }
        //if error xml is not null addd it
        if(errorXML != null)
        {
            DOMUtil.addSection(xmlDoc,errorXML.getChildNodes());            
        }
        
        
        //StringWriter sw = new StringWriter();
        //xmlDoc.print(sw);
        //logger.debug(sw.toString());
        //xmlDoc.print(System.out);
        //logger.debug(XMLUtil.convertDocToString(xmlDoc));

        //transform page
        TraxUtil.getInstance().transform(request, response, xmlDoc, XMLUtil.getXSL(mapping,"VendorDetailXSL",this.getServlet().getServletContext()), (HashMap)request.getAttribute("parameters"));        
    }

    //this processes all insert and update vendor requests
    private void processSaveVendor(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping, Connection conn) throws Exception
    {
        //transaction
        UserTransaction ut = null;
        
        //get a vendor vo using data from request
        VendorMasterVO vendorVO = convertToVO(request);
        
        //create daos
        VendorDAO vendorDAO = new VendorDAO(conn);
        
        String editMode = request.getParameter("edit_mode");
        
        //validate the data
        Document errorXML = validate(vendorVO, editMode);
        
        AddOnUtility addOnUtility = new AddOnUtility();
        
        String sessionId = (String) request.getParameter("securitytoken");
        String userId = null;
        
        if(sessionId==null)
        {
            logger.error("Security token not found in request object");
        }
        
        /* retrieve user information */
        if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
         userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }
       
        try 
        {
            ut = Transaction.getTransaction();
            ut.begin();
            
            if(errorXML == null) 
            {
                /*  If no errors are found and the vendor type has been changed to
                 *  SDS attempt to update products associated with the vendor.  Please
                 *  see the vendorDAO.updateVendorProductsForSDS method for more information.
                 */  
                //obtain original vendor type
                String origVendorType = request.getParameter(PARAMETER_ORIG_VENDOR_TYPE);
                if(origVendorType != null && !SDS_VENDOR_TYPE.equals(origVendorType) && SDS_VENDOR_TYPE.equals(vendorVO.getVendorType())) 
                {
                    //call stored proc to update products for vendor type switch to SDS
                    String message = vendorDAO.updateVendorProductsForSDS(vendorVO);
                    
                    //if the update did not occur display the reason why to the user
                    if(message != null) 
                    {
                        errorXML = XMLUtil.createErrorXML();
                        XMLUtil.addError(errorXML,"vendor_type","Cannot update products to correspond with new vendor type.  " + message);
                    }
                }
            }

            //if valid
            if(errorXML == null)
            {       
    
                //if this a new vendor
                if(editMode.equalsIgnoreCase("new"))
                {            
                    // get the new vendor ID
                    vendorVO.setVendorID(vendorDAO.getNextVendorId());
                    
                    //insert vendor record                
                    vendorDAO.insertVendor(vendorVO, userId);
                    
                    ContactVO mainContactVO = vendorVO.getMainContact();
                    mainContactVO.setVendorId(vendorVO.getVendorID());
                    
                    //insert main contact
                    if ((mainContactVO != null)&&(mainContactVO.getName() != null) && (mainContactVO.getName().length()!=0))
                    {
                      vendorDAO.insertContact(mainContactVO);
                    }
                    
                    
                    ContactVO altContactVO = vendorVO.getAltContact();
                    altContactVO.setVendorId(vendorVO.getVendorID());
                    //insert alt contact if one exists
                    if ((altContactVO != null)&&(altContactVO.getName() != null) && (altContactVO.getName().length()!=0))
                    {
                      vendorDAO.insertContact(altContactVO);
                    }

                    // Insert/Update Partner Shipping Accounts
                    vendorDAO.updatePartnerShippingAccounts(vendorVO);
                    
                }                
                //else, update the data
                else
                {
                
                    //update vendor record
                    vendorDAO.updateVendor(vendorVO, userId);
                    
                    //delete all associated contacts
                    vendorDAO.deleteAllContacts(vendorVO.getVendorID());
                    
                    ContactVO mainContactVO = vendorVO.getMainContact();
                    //insert main contact
                    if (mainContactVO != null)
                    {
                      vendorDAO.insertContact(mainContactVO);
                    }
                    
                    
                    ContactVO altContactVO = vendorVO.getAltContact();
                    //insert alt contact
                    if (altContactVO != null)
                    {
                      vendorDAO.insertContact(altContactVO);
                    }
                    
                    // Update Partner Shipping Accounts
                    vendorDAO.updatePartnerShippingAccounts(vendorVO);
                }     
                
                //If this is for the new shipping system save the carriers
                if(SDS_VENDOR_TYPE.equals(vendorVO.getVendorType())) 
                {
                    String[] selectedCarrierKeys = request.getParameterValues("active_carriers");
                    String[] allCarrierKeys = request.getParameterValues("active_carriers_all");

                    processSaveCarrier(vendorVO.getVendorID(), allCarrierKeys, selectedCarrierKeys, conn);    
                }
                
                Document vendorXML = convertToXML(vendorVO);
                Document addOnXML = null;
                HashMap <String, ArrayList <AddOnVO>> addOnMap = vendorVO.getAddOnMap();
                if (addOnMap != null)
                {
                    addOnXML = addOnUtility.convertAddOnMapToXML(addOnMap);
                }
                
                //Send vendor information to Novator
                new VendorUtil().sendVendorInfoToNovator(vendorVO.getVendorID(), conn);
    
                //display vendor in read only mode
                processDisplayVendor(request,response,mapping,conn,null,vendorXML,addOnXML,"display", "comments", false);
                
                ut.commit();
            }
            //else, error with entered data
            else
            {
                //get vendor info that the user entered
                Document vendorXML = convertToXML(vendorVO);
                Document addOnXML = null;
                HashMap <String, ArrayList <AddOnVO>> addOnMap = vendorVO.getAddOnMap();
                if (addOnMap != null)
                {
                    addOnXML = addOnUtility.convertAddOnMapToXML(addOnMap);
                }
            
                //display vendor with errors in edit mode
                processDisplayVendor(request,response,mapping,conn,errorXML,vendorXML,addOnXML,editMode,null,false);
            }
        }
        //This may be thrown from the following code:  new VendorUtil().sendVendorInfoToNovator(vendorVO.getVendorID(), conn);
        catch(NovatorTransmissionException nte) 
        {
            //rollback transaction
            Transaction.rollback(ut);
            
            String message = "Vendor information was not saved.  Connectivity to Novator failed.";
            logger.error(message,nte);
            errorXML = XMLUtil.createErrorXML();
            XMLUtil.addError(errorXML, "error_message", message);
            Document vendorXML = convertToXML(vendorVO);
            Document addOnXML = null;
            HashMap <String, ArrayList <AddOnVO>> addOnMap = vendorVO.getAddOnMap();
            if (addOnMap != null)
            {
                addOnXML = addOnUtility.convertAddOnMapToXML(addOnMap);
            }
            processDisplayVendor(request,response,mapping,conn,errorXML,vendorXML,addOnXML,"edit", "",true);
        }
        catch(Exception e) 
        {
            //rollback transaction
            Transaction.rollback(ut);

            throw e;
        }
    }
    
    
    /**
     * This method will: 
     * 1.  Gather any requried information
     * 2.  Selected values will be give a value of -1 and unselected, a value of 0
     * 3.  Update the database with valid usage
     * 4.  All errors will be returned by throwing an exception
     * 
     * @throws java.lang.Exception
     * @param conn
     * @param vendorId
     * @param allCarrierKeys
     * @param selectedCarrierKeys
     */
    private void processSaveCarrier(String vendorId, String[] allCarrierKeys, 
        String[] selectedCarrierKeys,
        Connection conn
    ) throws Exception
    {
        int selectedValue = -1;
        int unselectedValue = 0;

        final MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
        if( selectedCarrierKeys.length==0 ) 
        {
            throw new Exception("You must select at least one carrier.");
        }

        int selectedState;
        String carrierKey;
        String selectedCarrierKey;
        Date exceptionStartDateAsDate = null;
        Date exceptionEndDateAsDate = null;
        Integer exceptionUsageAsInteger = null;
        for(int pos = 0; pos < allCarrierKeys.length; pos++)
        {
            selectedState = unselectedValue;
            carrierKey = allCarrierKeys[pos];
            
            for(int i = 0; i < selectedCarrierKeys.length; i++)
            {
                selectedCarrierKey = selectedCarrierKeys[i];
                if(carrierKey.equals(selectedCarrierKey))
                {
                    selectedState = selectedValue;
                    break;                  
                }
            }
            
            maintDAO.updateVendorCarrierRef(
                vendorId, 
                carrierKey, 
                selectedState, 
                (java.sql.Date)exceptionStartDateAsDate, 
                (java.sql.Date)exceptionEndDateAsDate, 
                (Integer)exceptionUsageAsInteger);
        }
    }


    //validate vendor data
    private Document validate(VendorMasterVO vendorVO, String editMode)
        throws Exception
    {
        Document errorXML = XMLUtil.createErrorXML();

        
        boolean errorFound = false;
    
        //validations that only apply to adds
        if(editMode.equalsIgnoreCase("new"))
        {
            if(vendorVO.getVendorCode() == null || vendorVO.getVendorCode().length() == 0)
            {
                XMLUtil.addError(errorXML,"vendor_code",ERROR_REQUIRED_FIELD);
                errorFound = true;
            }
            else if(!Pattern.matches("\\d\\d-\\d\\d\\d\\d\\p{Alpha}\\p{Alpha}",vendorVO.getVendorCode()))
            {
                XMLUtil.addError(errorXML,"vendor_code","Invalid Format");
                errorFound = true;
            } else 
            {
              Connection conn = null;
              try
              {
                conn = CommonUtil.getDBConnection();
                VendorDAO vendorDAO = new VendorDAO(conn);
                if (vendorDAO.existsVendor(vendorVO.getVendorCode())) 
                {
                  XMLUtil.addError(errorXML, "vendor_code", "Vendor Code must be unique");
                  errorFound = true;
                }
              }
              finally
              {
                //close the connection
                try
                {
                  conn.close();
                }
                catch(Exception e)
                {}
              }
            }
        }
        
        //validations that apply to new and edits
        if(vendorVO.getAccountsPayableId() == null || vendorVO.getAccountsPayableId().length() == 0)
        {
            XMLUtil.addError(errorXML,"accounts_payable_id",ERROR_REQUIRED_FIELD);
            errorFound = true;
        }        
        else if(!Pattern.matches("\\d{7}",vendorVO.getAccountsPayableId())) 
        {
            XMLUtil.addError(errorXML,"accounts_payable_id","Please enter a seven digit number for the Accounts Payable Id");
            errorFound = true;
        }
        if(vendorVO.getGlAccountNumber() == null || vendorVO.getGlAccountNumber().length() == 0)
        {
            XMLUtil.addError(errorXML,"gl_account_number",ERROR_REQUIRED_FIELD);
            errorFound = true;
        }        
        else if(!Pattern.matches("\\d{4}.\\d{4}",vendorVO.getGlAccountNumber())) 
        {
            XMLUtil.addError(errorXML,"gl_account_number","GL Account Number must be in the format ####.####");
            errorFound = true;
        }
        if(vendorVO.getVendorName() == null || vendorVO.getVendorName().length() == 0)
        {
            XMLUtil.addError(errorXML,"vendor_name",ERROR_REQUIRED_FIELD);
            errorFound = true;
        }        
        if(vendorVO.getAddress1() == null || vendorVO.getAddress1().length() == 0)
        {
            XMLUtil.addError(errorXML,"address1",ERROR_REQUIRED_FIELD);
            errorFound = true;
        }        
        if(vendorVO.getCity() == null || vendorVO.getCity().length() == 0)
        {
            XMLUtil.addError(errorXML,"city",ERROR_REQUIRED_FIELD);
            errorFound = true;
        }
        else
        {   
            //if city invalid don't check state..since they are on the same line, only need 1 error msg
            if(vendorVO.getState() == null || vendorVO.getState().length() == 0)
            {
                XMLUtil.addError(errorXML,"state",ERROR_REQUIRED_FIELD);
                errorFound = true;
            }                
        }
        if(vendorVO.getZipCode() == null || vendorVO.getZipCode().length() == 0)
        {
            XMLUtil.addError(errorXML,"zipcode",ERROR_REQUIRED_FIELD);
            errorFound = true;
        }
        else
        {
            if(!(Pattern.matches("\\d\\d\\d\\d\\d",vendorVO.getZipCode()) ||
                Pattern.matches("\\d\\d\\d\\d\\d-\\d\\d\\d\\d",vendorVO.getZipCode())||
                Pattern.matches("\\p{Alpha}\\d\\p{Alpha} \\p{Alpha}\\d\\p{Alpha}",vendorVO.getZipCode()) ))
                {
                    XMLUtil.addError(errorXML,"zipcode","Invalid Format");
                    errorFound = true;                    
                }
        }
        
        ContactVO mainContact = vendorVO.getMainContact();        
        if(mainContact.getPhone() != null && mainContact.getPhone().length() != 0)
        {
            mainContact.setPhone(CommonUtil.removeNonNumerics(mainContact.getPhone()));
            //phone must be 10 digits.  By this point all non-numerics were stripped out        
            if(mainContact.getPhone().length() != 10)
            {
                XMLUtil.addError(errorXML,"main_phone","Invalid Format");
                errorFound = true;                
            }
        }                
        
        if(mainContact.getFax() != null && mainContact.getFax().length() != 0)
        {
            mainContact.setFax(CommonUtil.removeNonNumerics(mainContact.getFax()));
            //phone must be 10 digits.  By this point all non-numerics were stripped out        
            if(mainContact.getFax().length() != 10)
            {
                XMLUtil.addError(errorXML,"main_fax","Invalid Format");
                errorFound = true;                
            }
        }                        
        
        if(mainContact.getEmail() != null && mainContact.getEmail().length() != 0)
        {
            //phone must contain an '@' and a period.        
            if((mainContact.getEmail().indexOf("@") <= 0) || mainContact.getEmail().indexOf(".") <= 0)
            {
                XMLUtil.addError(errorXML,"main_email","Invalid Format");
                errorFound = true;                
            }
        }                                

        ContactVO altContact = vendorVO.getAltContact();
        if(altContact.getEmail() != null && altContact.getEmail().length() != 0)
        {
            //phone must contain an '@' and a period.        
            if(altContact.getEmail().indexOf("@") <= 0 || altContact.getEmail().indexOf(".") <= 0)
            {
                XMLUtil.addError(errorXML,"alt_email","Invalid Format");
                errorFound = true;                
            }
        }                                

        if(altContact.getPhone() != null && altContact.getPhone().length() != 0)
        {
            altContact.setPhone(CommonUtil.removeNonNumerics(altContact.getPhone()));
            //phone must be 10 digits.  By this point all non-numerics were stripped out        
            if(altContact.getPhone().length() != 10)
            {
                XMLUtil.addError(errorXML,"alt_phone","Invalid Format");
                errorFound = true;                
            }
        }                
        
        if(altContact.getFax() != null && altContact.getFax().length() != 0)
        {
            altContact.setFax(CommonUtil.removeNonNumerics(altContact.getFax()));
            //phone must be 10 digits.  By this point all non-numerics were stripped out        
            if(altContact.getFax().length() != 10)
            {
                XMLUtil.addError(errorXML,"alt_fax","Invalid Format");
                errorFound = true;                
            }
        } 
        List partnerShippingAccounts = vendorVO.getPartnerShippingAccounts();
        for (int i = 0; i < partnerShippingAccounts.size(); i++){
            PartnerShippingAccountVO psa = (PartnerShippingAccountVO)partnerShippingAccounts.get(i);
            if (psa.getValidationRegex() != null
                && !psa.getValidationRegex().equalsIgnoreCase("") 
                && psa.getAccountNum() != null
                && !psa.getAccountNum().equalsIgnoreCase("") 
               ){
                boolean b = Pattern.matches(psa.getValidationRegex(), psa.getAccountNum());
                if (!b){
                    String[] names = {"psaError","errPosition"};
                    String[] values = {"Invalid Format", new Integer(i+1).toString()};
                    XMLUtil.addErrors(errorXML, names, values);
                    errorFound = true;
                }
            }
        }
               
        if (vendorVO.getAddOnMap() != null) 
        {
            Connection conn = null;
            HashSet <String> vendorAddOnChangedSet = new HashSet <String> ();

            try
            {
                conn = CommonUtil.getDBConnection();
                
                for (String key : vendorVO.getAddOnMap().keySet())
                {
                    for (int i = 0; i< vendorVO.getAddOnMap().get(key).size(); i++)
                    {
                        AddOnVO addOnVO = vendorVO.getAddOnMap().get(key).get(i);
                        VendorAddOnVO vendorAddOnVO = null;

                        vendorAddOnVO = addOnVO.getVendorCostsMap().get(vendorVO.getVendorID());
    
                        if (vendorAddOnVO.getAssignedFlag())
                        {
                            if (StringUtils.isBlank(vendorAddOnVO.getSKU()))
                            {
                                XMLUtil.addError(errorXML,"add_on_options","Vendor SKU for " + addOnVO.getAddOnId() + " - " + addOnVO.getAddOnDescription() + " is empty. Please enter a Vendor SKU.");
                                errorFound = true;
                            }
                            if (StringUtils.isBlank(vendorAddOnVO.getCost()))
                            {
                                XMLUtil.addError(errorXML,"add_on_options","Vendor cost for " + addOnVO.getAddOnId() + " - " + addOnVO.getAddOnDescription() + " is empty. Please enter a Vendor Cost.");
                                errorFound = true;
                            }      
                        }
                        if(!vendorAddOnVO.getCost().matches("^[0-9]{0,3}(\\.[0-9]{0,2})?$"))
                        {
                            XMLUtil.addError(errorXML,"add_on_options","Vendor Cost for " + addOnVO.getAddOnId() + " - " + addOnVO.getAddOnDescription() + " must be a numeric value in the range of 0.00 to 999.99.");
                            errorFound = true;
                        }
                        if(!editMode.equalsIgnoreCase("new"))
                        {
                            AddOnUtility addOnUtility = new AddOnUtility();
                            AddOnVO addOnVOVendorCheck = addOnUtility.getAddOnById(vendorAddOnVO.getAddOnId(),true,true,conn);                           
                            VendorAddOnVO vendorAddOnVOCheck = addOnVOVendorCheck.getVendorCostsMap().get(vendorVO.getVendorID());
                            if (vendorAddOnVOCheck != null && vendorAddOnVOCheck.getAssignedFlag() && !vendorAddOnVO.getAssignedFlag() && addOnVO.getAddOnAvailableFlag())
                            {
                                if (addOnUtility.isLastVendorAssignedToAddOn(vendorVO.getVendorID(), vendorAddOnVO.getAddOnId(), conn))
                                {
                                    logger.debug("This is the last vendor that supplies this add-on and more than one vendor-delivered " +
                                        "product uses this add-on.  Error added");
                                    XMLUtil.addError(errorXML,"add_on_options" ,addOnVO.getAddOnDescription() + " is assigned to 1 or more vendor-delivered products, and this is the last vendor assigned to " + addOnVO.getAddOnDescription() + ". You cannot unassign this vendor.");
                                    vendorAddOnVO.setAssignedFlag(true);
                                    errorFound = true; 
                                }
                            }
                            
                            if (vendorAddOnVOCheck == null || ! vendorAddOnVOCheck.getAssignedFlag().equals(vendorAddOnVO.getAssignedFlag())
                                || vendorAddOnVOCheck.getSKU() == null ^ StringUtils.isBlank(vendorAddOnVO.getSKU()) || (vendorAddOnVOCheck.getSKU() != null && ! vendorAddOnVOCheck.getSKU().equals(vendorAddOnVO.getSKU())) 
                                || vendorAddOnVOCheck.getCost() == null ^ StringUtils.isBlank(vendorAddOnVO.getCost()) || (vendorAddOnVOCheck.getCost() != null && ! vendorAddOnVOCheck.getCost().equals(vendorAddOnVO.getCost())) )
                            {
                                vendorAddOnChangedSet.add(addOnVO.getAddOnId());
                            }
                        }
                    }
                    vendorVO.setVendorAddOnChangedSet(vendorAddOnChangedSet);
                }
            }
            finally
            {
                conn.close();
            }
        }
    return  errorFound ? errorXML : null;
    }
    
    
    
    //Get data from request and put it into a VO
    private VendorMasterVO convertToVO(HttpServletRequest request)
    {
        VendorMasterVO vendorVO = new VendorMasterVO();
        
        vendorVO.setAddress1(request.getParameter("address1"));
        vendorVO.setAddress2(request.getParameter("address2"));
        vendorVO.setCity(request.getParameter("city"));
        vendorVO.setCompanyCode(request.getParameter("company"));        
        vendorVO.setGeneralInfo(request.getParameter("general_info"));
        vendorVO.setLeadDays(request.getParameter("lead_days"));
        vendorVO.setShipVia(request.getParameter("default_carrier"));
        vendorVO.setState(request.getParameter("state"));
        vendorVO.setVendorCode(request.getParameter("vendor_code"));
        vendorVO.setVendorID(request.getParameter("vendor_id"));
        vendorVO.setVendorName(request.getParameter("vendor_name"));
        vendorVO.setVendorType(request.getParameter("vendor_type"));
        vendorVO.setZipCode(request.getParameter("zipcode"));
        vendorVO.setCutoff(request.getParameter("cutoff_time"));        
        vendorVO.setAccountsPayableId(request.getParameter("accounts_payable_id"));        
        vendorVO.setGlAccountNumber(request.getParameter("gl_account_number"));  
        
        //Arrays for addons from the request object
        String [] vendorAddOnAssignedArray = request.getParameterValues("vendor_add_on_assignment");
        String [] vendorAddOnSkuArray = request.getParameterValues("vendor_add_on_sku");
        String [] vendorAddOnCostArray = request.getParameterValues("vendor_add_on_cost");
        String [] addOnIdArray = request.getParameterValues("add_on_id");
        String [] addOnAvailableArray = request.getParameterValues("add_on_available_flag");
        String [] addOnDescriptionArray = request.getParameterValues("add_on_description");
        String [] addOnTypeId = request.getParameterValues("add_on_type_id");

        //Arrays for vases from the request object
        String [] vendorVaseAssignedArray = request.getParameterValues("vendor_vase_assignment");
        String [] vendorVaseSkuArray = request.getParameterValues("vendor_vase_sku");
        String [] vendorVaseCostArray = request.getParameterValues("vendor_vase_cost");
        String [] vaseIdArray = request.getParameterValues("vase_id");
        String [] vaseAvailableArray = request.getParameterValues("vase_available_flag");
        String [] vaseDescriptionArray = request.getParameterValues("vase_description");
        String [] vaseTypeId = request.getParameterValues("vase_type_id");

        //HashMap that will contain 2 arraylists - 1 for addon, 2 for vases
        HashMap <String, ArrayList<AddOnVO>> addonMap = new HashMap <String, ArrayList<AddOnVO>>();

        HashMap <String,VendorAddOnVO> vendorAddOnMap = null;

        //ArrayList to hold AddonVO for addons
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();

        //ArrayList to hold AddonVO for vases
        ArrayList <AddOnVO> vaseAddOnVOList = new ArrayList<AddOnVO>();
        
        //HashSet to store all the addons and vases that are assigned
        HashSet <String> vendorAddOnAndVaseAssignedSet = new HashSet <String> ();
 
        if (vendorAddOnAssignedArray != null)
        {
          for (String addOn : vendorAddOnAssignedArray)
            vendorAddOnAndVaseAssignedSet.add(addOn);
        }

        if (vendorVaseAssignedArray != null)
        {
          for (String vase : vendorVaseAssignedArray)
            vendorAddOnAndVaseAssignedSet.add(vase);
        }

        //process addons
        if (addOnIdArray != null)
        {
          for (int i = 0; i < addOnIdArray.length; i++)
          {
            AddOnVO addOnVO = new AddOnVO();
            addOnVO.setAddOnId(addOnIdArray[i]);
            addOnVO.setAddOnDescription(addOnDescriptionArray[i]);
            addOnVO.setAddOnTypeId(addOnTypeId[i]);
            addOnVO.setAddOnAvailableFlag(addOnAvailableArray[i].equalsIgnoreCase("Y")||addOnAvailableArray[i].equalsIgnoreCase("YES")||addOnAvailableArray[i].equalsIgnoreCase("TRUE")?true:false);
            
            vendorAddOnMap = new HashMap <String,VendorAddOnVO> ();
            VendorAddOnVO vendorAddOnVO = new VendorAddOnVO();
            vendorAddOnVO.setAddOnId(addOnIdArray[i]);
            vendorAddOnVO.setSKU(vendorAddOnSkuArray[i]);
            vendorAddOnVO.setCost(vendorAddOnCostArray[i]);
            vendorAddOnVO.setAssignedFlag(vendorAddOnAndVaseAssignedSet.contains(addOnIdArray[i]));
            
            if (StringUtils.isBlank(vendorVO.getVendorID()))
              vendorVO.setVendorID("NEW_VENDOR");

            vendorAddOnMap.put(vendorVO.getVendorID(),vendorAddOnVO);
   
            addOnVO.setVendorCostsMap(vendorAddOnMap);
            
            addOnVOList.add(addOnVO);

          }
          //add addonVO arraylist for addons to addonMap
          addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
        }
        
        //process vases
        if (vaseIdArray != null)
        {
          for (int i = 0; i < vaseIdArray.length; i++)
          {
            AddOnVO addOnVO = new AddOnVO();
            addOnVO.setAddOnId(vaseIdArray[i]);
            addOnVO.setAddOnDescription(vaseDescriptionArray[i]);
            addOnVO.setAddOnTypeId(vaseTypeId[i]);
            addOnVO.setAddOnAvailableFlag(vaseAvailableArray[i].equalsIgnoreCase("Y")||vaseAvailableArray[i].equalsIgnoreCase("YES")||vaseAvailableArray[i].equalsIgnoreCase("TRUE")?true:false);
            
            vendorAddOnMap = new HashMap <String,VendorAddOnVO> ();
            VendorAddOnVO vendorAddOnVO = new VendorAddOnVO();
            vendorAddOnVO.setAddOnId(vaseIdArray[i]);
            vendorAddOnVO.setSKU(vendorVaseSkuArray[i]);
            vendorAddOnVO.setCost(vendorVaseCostArray[i]);
            vendorAddOnVO.setAssignedFlag(vendorAddOnAndVaseAssignedSet.contains(vaseIdArray[i]));
            
            if (StringUtils.isBlank(vendorVO.getVendorID()))
              vendorVO.setVendorID("NEW_VENDOR");

            vendorAddOnMap.put(vendorVO.getVendorID(),vendorAddOnVO);
   
            addOnVO.setVendorCostsMap(vendorAddOnMap);
            
            vaseAddOnVOList.add(addOnVO);

          }
          //add addonVO arraylist for vases to addonMap
          addonMap.put(AddOnVO.ADD_ON_VO_VASE_KEY,vaseAddOnVOList);        
        }
        
        //Now that we have processed all addons and all vases, add the mappings to vendorVO
        vendorVO.setVendorAddOnAssignedSet(vendorAddOnAndVaseAssignedSet);
        vendorVO.setAddOnMap(addonMap);
  
        //need to check to see if zone jump check box was checked
        //if so, replace the "on" value with 'Y'
        String zoneJumpable = "N";
        if(request.getParameter("zone_jump_eligible_flag") != null 
             && request.getParameter("zone_jump_eligible_flag").equals("on"))
        {
            zoneJumpable = "Y";
        }    
        vendorVO.setZoneJumpEligibleFlag(zoneJumpable);
        
        //need to check to see if new shipping system check box was checked
        //if so, replace the "on" value with 'Y'
        String newShippingSystem = "N";
        if(request.getParameter("new_shipping_system") != null 
             && request.getParameter("new_shipping_system").equals("on"))
        {
        	newShippingSystem = "Y";
        }    
        vendorVO.setNewShippingSystem(newShippingSystem);  

        ContactVO mainContactVO = new ContactVO();
        mainContactVO.setVendorId(request.getParameter("vendor_id"));
        mainContactVO.setEmail(request.getParameter("main_email"));
        mainContactVO.setFax(request.getParameter("main_fax"));
        mainContactVO.setFaxExt(request.getParameter("main_fax_ext"));
        mainContactVO.setName(request.getParameter("main_name"));
        mainContactVO.setPhone(removeNonDigits(request.getParameter("main_phone")));
        mainContactVO.setPhoneExt(request.getParameter("main_phone_ext"));
        mainContactVO.setPrimaryContact("Y");
        vendorVO.setMainContact(mainContactVO);        

        ContactVO altContactVO = new ContactVO();
        altContactVO.setVendorId(request.getParameter("vendor_id"));
        altContactVO.setPrimaryContact("N");
        altContactVO.setEmail(request.getParameter("alt_email"));
        altContactVO.setFax(request.getParameter("alt_fax"));
        altContactVO.setFaxExt(request.getParameter("alt_fax_ext"));
        altContactVO.setName(request.getParameter("alt_name"));
        altContactVO.setPhone(removeNonDigits(request.getParameter("alt_phone")));
        altContactVO.setPhoneExt(request.getParameter("alt_ext"));   
        vendorVO.setAltContact(altContactVO);  
        
        // if there is partner/carrier/shipping information save it
        String[] partnerIdArr = request.getParameterValues("psa_partner_id");
        String[] carrierIdArr = request.getParameterValues("psa_carrier_id");
        String[] validationRegexArr = request.getParameterValues("psa_validation_regex");
        String[] accountNumArr = request.getParameterValues("psa_account_num");
        
        ArrayList list = new ArrayList();
        if (partnerIdArr != null){
            for (int i = 0; i < partnerIdArr.length; i++)
            {
                PartnerShippingAccountVO psa = new PartnerShippingAccountVO();
                psa.setVendorId(vendorVO.getVendorID());
                psa.setPartnerId(partnerIdArr[i]);
                psa.setCarrierId(carrierIdArr[i]);
                psa.setValidationRegex(validationRegexArr[i]);
                psa.setAccountNum(accountNumArr[i]);
                list.add(psa);
            }
        }
        vendorVO.setPartnerShippingAccounts(list);        
        return vendorVO;
    }
    
    private Document convertToXML(VendorMasterVO vendorVO) throws Exception
    {
        ContactVO mainContactVO = vendorVO.getMainContact();
        ContactVO altContactV0 = vendorVO.getAltContact();
        //populate a vo with data from the page
        Document vendorDoc = (Document) DOMUtil.getDocumentBuilder().newDocument();
        // create vendor_detail node
        Element vendor = (Element) vendorDoc.createElement("vendor");
        vendor = XMLUtil.addElement(vendorDoc, vendor, "vendor_id", vendorVO.getVendorID());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "vendor_name", vendorVO.getVendorName());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "default_carrier", vendorVO.getShipVia());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "member_number", vendorVO.getVendorCode());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "vendor_type", vendorVO.getVendorType());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "active", "Y");
        vendor = XMLUtil.addElement(vendorDoc, vendor, "address1", vendorVO.getAddress1());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "address2", vendorVO.getAddress2());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "city", vendorVO.getCity());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "state", vendorVO.getState());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "zip_code", vendorVO.getZipCode());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "lead_days", vendorVO.getLeadDays());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "company", vendorVO.getCompanyCode());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "general_info", vendorVO.getGeneralInfo());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "cutoff_time", vendorVO.getCutoff());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "accounts_payable_id", vendorVO.getAccountsPayableId());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "gl_account_number", vendorVO.getGlAccountNumber());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "zone_jump_eligible_flag", vendorVO.getZoneJumpEligibleFlag());
        vendor = XMLUtil.addElement(vendorDoc, vendor, "new_shipping_system", vendorVO.getNewShippingSystem());
        Element vendors = (Element) vendorDoc.createElement("vendors");
        vendors.appendChild(vendor);
        Element vendorDetails = (Element) vendorDoc.createElement("vendor_details");
        vendorDetails.appendChild(vendors);
        // create main contact node
        Element mainContact = (Element) vendorDoc.createElement("maincontact"); 
        if (mainContactVO != null)
        {
          mainContact = this.getContactElement(vendorDoc, mainContactVO, "maincontact");
        }
        Element mainContacts = (Element) vendorDoc.createElement("maincontacts");
        mainContacts.appendChild(mainContact);
        vendorDetails.appendChild(mainContacts);
        // create alt contact node
        Element altContact = (Element) vendorDoc.createElement("altcontact"); 
        if (mainContactVO != null)
        {
          altContact = this.getContactElement(vendorDoc, altContactV0, "altcontact");
        }
        Element altContacts = (Element) vendorDoc.createElement("altcontacts");
        Element psa = this.getPartnerShippingElement(vendorDoc, vendorVO.getPartnerShippingAccounts(), "partnerShippingAccounts");
        altContacts.appendChild(altContact);
        vendorDetails.appendChild(altContacts);
        vendorDetails.appendChild(psa);
        vendorDoc.appendChild(vendorDetails);
        return ((Document) vendorDoc);        
    }
    
    private Element getContactElement(Document xml, ContactVO contactVO, String keyName)throws Exception
    {
        Element vendor = (Element) xml.createElement(keyName);
        vendor = XMLUtil.addElement(xml, vendor, "vendor_id", contactVO.getVendorId());
        vendor = XMLUtil.addElement(xml, vendor, "contact_name", contactVO.getName());
        vendor = XMLUtil.addElement(xml, vendor, "primary", contactVO.getPrimaryContact());
        vendor = XMLUtil.addElement(xml, vendor, "phone", contactVO.getPhone());
        vendor = XMLUtil.addElement(xml, vendor, "phone_extension", contactVO.getPhoneExt());
        vendor = XMLUtil.addElement(xml, vendor, "fax", contactVO.getFax());
        vendor = XMLUtil.addElement(xml, vendor, "fax_extension", contactVO.getFaxExt());
        vendor = XMLUtil.addElement(xml, vendor, "email", contactVO.getEmail());
        return vendor;
    }
    

    private Element getPartnerShippingElement(Document xml, List partnerShippingAccounts, String keyName)throws Exception
    {
        Element psaParent = (Element) xml.createElement(keyName);
        for (int i=0; i < partnerShippingAccounts.size(); i++){
            Element psa = (Element) xml.createElement(keyName);
            PartnerShippingAccountVO vo = (PartnerShippingAccountVO)partnerShippingAccounts.get(i);
            psa = XMLUtil.addElement(xml, psa, "carrier_id", vo.getCarrierId());
            psa = XMLUtil.addElement(xml, psa, "partner_id", vo.getPartnerId());
            psa = XMLUtil.addElement(xml, psa, "account_num", vo.getAccountNum());
            psa = XMLUtil.addElement(xml, psa, "validation_regex", vo.getValidationRegex());
            psaParent.appendChild(psa);
        }
        return psaParent;
    }
    private Document getNewVendorXML(String vendorId)throws Exception
    {
        Document vendorDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element vendorDetails = (Element) vendorDoc.createElement("vendor_details");
        Element vendors = (Element) vendorDoc.createElement("vendors");
        Element vendor = (Element) vendorDoc.createElement("vendor");
        Element vendorIdNode = (Element) vendorDoc.createElement("vendor_id");
        vendorIdNode.appendChild(vendorDoc.createTextNode(vendorId));
        vendor.appendChild(vendorIdNode);
        vendors.appendChild(vendor);
        vendorDetails.appendChild(vendors);
        vendorDoc.appendChild(vendorDetails);     
        return ((Document) vendorDoc);      
    }
    
    //remove any non-digits from the passed in string
    private String removeNonDigits(String input)
    {
          String output = "";
          
          if(input != null){
          
              for(int i=0; i< input.length();i++)
              {
                  if(Character.isDigit(input.charAt(i)))
                  {
                      output = output + input.charAt(i);
                  }
              }        
          }
          
          return output;
    }
    
    private boolean checkForZoneJumpTrip(String vendorId, String blockDate, String blockType, Connection connection) throws Exception
    {
        VendorDAO vDAO = new VendorDAO(connection); 
        List zjVendors = vDAO.zoneJumpTripExists(vendorId, blockDate, blockType);
        
        boolean zoneJumpTripExists = false;
        StringBuffer sb = new StringBuffer();
        for(Iterator it = zjVendors.iterator(); it.hasNext();) 
        {
            zoneJumpTripExists = true;
            sb.append((String)it.next());
        }
        if(zoneJumpTripExists)
        {
            if(blockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_SHIPPING))
                this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE = FieldUtils.replaceAll(OPMConstants.ZONE_JUMP_SHIPPING_DATE_BLOCK_ERROR, "vendorIds", sb.toString());
            else if(blockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_DELIVERY))
                this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE = FieldUtils.replaceAll(OPMConstants.ZONE_JUMP_DELIVERY_DATE_BLOCK_ERROR, "vendorIds", sb.toString());
            else if(blockType.equalsIgnoreCase(OPMConstants.BLOCK_TYPE_BOTH))
                this.ACTIVE_ZONE_JUMP_ERROR_MESSAGE = FieldUtils.replaceAll(OPMConstants.ZONE_JUMP_BLOCK_ERROR, "vendorIds", sb.toString());
        }
        return zoneJumpTripExists; 
    }
    
    public static void main(String[] args)
    {
      try
      {
/*
        VendorDetailAction action = new VendorDetailAction();
        VendorMasterVO vendorVO = new VendorMasterVO();
        vendorVO.setVendorID("10000");
        System.out.println(CommonUtil.convertToString(action.convertToXML(vendorVO)));    
*/
String x = "q1q q1q";
        if(Pattern.matches("\\d\\d\\d\\d\\d",x) ||
            Pattern.matches("\\d\\d\\d\\d\\d-\\d\\d\\d\\d",x)||
            Pattern.matches("\\p{Alpha}\\d\\p{Alpha} \\p{Alpha}\\d\\p{Alpha}",x) )
        {
            System.out.println("match");
        }
        else
        {
            System.out.println("no match");
        }

//        XMLDocument errorXML = XMLUtil.createErrorXML();
//        XMLUtil.addError(errorXML, "laura", "laura2");
//        XMLUtil.addError(errorXML, "wheeeeeeeee", "hurrah!");
//        System.out.println(CommonUtil.convertToString(errorXML));



/*
        System.out.println("-----");
        if (Pattern.matches("\\d\\d-\\d\\d\\d\\d\\p{Alpha}\\p{Alpha}","21-1234TQ"))
        {
            System.out.println("matches1");
        }
        if (Pattern.matches("789",""))        
        if (Pattern.matches("\\d\\d-\\d\\d\\d\\d\\p{Digit}\\p{Alpha}","11-2222AF"))
        {
            System.out.println("matches1");
        }
        if (Pattern.matches("789",""))
        {
            System.out.println("matches2");
        }
        if (Pattern.matches("11-2222DF",""))
        {
            System.out.println("matches3");
        }        
  */   
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
    
    
}