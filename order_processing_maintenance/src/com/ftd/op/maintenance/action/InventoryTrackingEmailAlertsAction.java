package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.bo.InventoryTrackingEmailAlertsBO;
import com.ftd.op.maintenance.constants.OPMConstants;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.vo.VendorMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.StringReader;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;


/**
 * Handles interactions with the Inventory Tracking Email Alerts screen
 *
 *
 * @author Mike Kruger
 */
public class InventoryTrackingEmailAlertsAction
  extends Action
{
  private static Logger logger = new Logger("com.ftd.op.maintenance.action.InventoryTrackingEmailAlertsAction");

  public InventoryTrackingEmailAlertsAction()
  {
  }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                               HttpServletResponse response)
  {
    ActionForward forward = null;
    Connection conn = null;
    String emailAddress = null;
    HashMap pageData = new HashMap();

    try
    {
      conn = CommonUtil.getDBConnection();
      
      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = DOMUtil.getDocument();

      //Type of action requested
      String actionType = request.getParameter(OPMConstants.ACTION_TYPE);
      if(actionType == null)
      {
        actionType = "";
      }

      //Obtain the user id
      String userId = getUserId(request);
      
      if(logger.isDebugEnabled())
      {
        logger.debug("actionType: " + actionType);
        logger.debug("userId: " + userId);        
      }

      InventoryTrackingEmailAlertsBO inventoryTrackingEmailAlertsBO = new InventoryTrackingEmailAlertsBO();

      //A search is performed for every action type so just set page data at this point
      if(actionType.equals("search"))
      {
        //Obtain the searched on email address
        emailAddress = request.getParameter("tEmailAddress");
        logger.debug("emailAddress: " + emailAddress);

        //Set page data
        pageData.put("email_address", emailAddress);
        pageData.put("search_done", "Y");              
      }
      //Save changes
      else if(actionType.equals("save"))
      {
        try
        {
          //Obtain the selected email address
          emailAddress = request.getParameter("hEmailAddress");
          logger.debug("emailAddress: " + emailAddress);

          //Obtain the list of vendors chosen
          String vendorIdsChosen = request.getParameter("hVendorsChosen"); 
          
          //Obtain whether the updates apply to the vendor defaults and current
          //inventory tracking records or just the defaults
          String applyToCurrentFlag = request.getParameter("sApplyToCurrentFlag");
          
          if(logger.isDebugEnabled())
          {
            logger.debug("vendorIdsChosen: " + vendorIdsChosen);
            logger.debug("applyToCurrentFlag: " + applyToCurrentFlag);
            logger.debug("userId: " + userId);
          }
            
          //Apply the modifications
          inventoryTrackingEmailAlertsBO.saveInventoryTrackingEmail(conn, emailAddress, vendorIdsChosen, applyToCurrentFlag, userId);
  
          //If the modifications were successful display a message
          pageData.put("action_response", "Save Successful");                              

          //Set page data
          pageData.put("email_address", emailAddress);
          pageData.put("search_done", "Y");              
          pageData.put("apply_to_current_flag", applyToCurrentFlag);
        }
        catch(Throwable t)
        {
          logger.error(t);
          pageData.put("action_error_response", "Save Unsuccessful.  Please try again.");                                        
        }
      }
      //Delete all inventory tracking information associated with the email address
      else if(actionType.equals("deleteAll"))
      {
        try
        {
          //Obtain the selected email address
          emailAddress = request.getParameter("hEmailAddress");
          logger.debug("emailAddress: " + emailAddress);

          //Delete all associations
          inventoryTrackingEmailAlertsBO.deleteInvTrkEmail(conn, emailAddress, userId);
          
          //If delete was successful set email address to null so it isn't searched on below
          emailAddress = null;
          
          //If the deletes were successful display a message
          pageData.put("action_response", "Delete Successful");                              

          //Set page data
          pageData.put("email_address", null);
          pageData.put("search_done", null);              
          pageData.put("apply_to_current_flag", null);          
        }
        catch(Throwable t)
        {
          logger.error(t);
          pageData.put("action_error_response", "Delete Unsuccessful.  Please try again.");                                        
        }
      }
      
      //If no error occurred obtain the vendors available and chosen
      HashMap vendorMap = null;
      if(StringUtils.isBlank((String)pageData.get("action_error_response")))
      {
        try
        {
          //Obtain current Inventory Tracking Email Alert information by email address
          vendorMap = inventoryTrackingEmailAlertsBO.getVendorByInvTrkEmail(conn, emailAddress);
        }
        catch(Throwable t)
        {
          logger.error(t);
          pageData.put("action_error_response", "An error occurred while retrieving information for email address " + emailAddress + ".  Please try again.");
          
          //If an error occurs when executing a search set search_done to null
          pageData.put("search_done", null);              
        }
      }
      //If an error occurred rebuild the vendors available and vendors chosen based 
      //on what the user had selected and set the previous page state. 
      else
      {
        String vendorIdsChosen = request.getParameter("hVendorsChosen"); 
        vendorMap = rebuildVendors(conn, vendorIdsChosen);

        //Set previous page state
        pageData.put("email_address", emailAddress);
        pageData.put("search_done", 'Y');              
        pageData.put("apply_to_current_flag", request.getParameter("sApplyToCurrentFlag"));          
      }
      
      //If the vendor map is not null add vendors to the response document
      if(vendorMap != null)
      {
        Document vendorDoc = null;
        vendorDoc = convertVendorVOListToXML((ArrayList)vendorMap.get("vendor_available"), "vendor_available");
        DOMUtil.addSection(responseDocument, vendorDoc.getChildNodes());
        vendorDoc = convertVendorVOListToXML((ArrayList)vendorMap.get("vendor_chosen"), "vendor_chosen");
        DOMUtil.addSection(responseDocument, vendorDoc.getChildNodes());
      }

      //Get XSL File name
      File xslFile = getXSL("InventoryTrackingEmailAlertsXSL", mapping);
      if (xslFile != null && xslFile.exists())
      {
        //Transform the XSL File
        try
        {
          //Retrieve the parameter map set by data filter
          HashMap params = getParameters(request);
          
          //Add security information to page data
          populateSecurityAccess(request, pageData, userId);
          
          //Convert the page data hashmap to XML and append it to the final XML
          populatePageData(request, pageData);
          DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

          //responseDocument.print(System.out);
          
          forward = mapping.findForward("InventoryTrackingEmailAlertsXSL");
          String xslFilePathAndName = forward.getPath(); //get real file name

          //Transform
          TraxUtil.getInstance().getInstance().transform(request, response, 
            responseDocument, xslFile, xslFilePathAndName, params);
        }
        catch (Throwable t)
        {
          logger.error("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " transformation failed.", t);
          if (!response.isCommitted())
          {
            throw t;
          }
        }
      }
      else
      {
        throw new Exception("XSL file " + (xslFile == null? "null": xslFile.getCanonicalPath()) + " does not exist.");
      }

      return null;
    }
    catch (Throwable t)
    {
      logger.error(t);
      forward = mapping.findForward("ErrorPage");
      return forward;
    }
    finally
    {
      try
      {
        //Close the connection
        if (conn != null)
        {
          conn.close();
        }
      }
      catch (Throwable t)
      {
        logger.error(t);
        forward = mapping.findForward("ErrorPage");
        return forward;
      }
    }
  }


  /**
  * Retrieve name of Action XSL file
  * @param  String - the xsl name returned from the Business Object
  * @param  ActionMapping
  * @return File - XSL File name
  * @throws none
  */
  private File getXSL(String forwardToName, ActionMapping mapping)
  {
    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(forwardToName);
    xslFilePathAndName = forward.getPath(); //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


  /**
    * Retrieve the attribute “parameters” HashMap from request.
    * Return the HashMap.
    * @param request  HttpServletRequest
    * @return         Map of parameters
    */
  private HashMap getParameters(HttpServletRequest request)
  {
    HashMap parameters = new HashMap();

    try
    {
      String action = request.getParameter(OPMConstants.ACTION_TYPE);
      parameters = (HashMap) request.getAttribute("parameters");
      parameters.put("action", action);

    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getParameters");
      }
    }

    return parameters;
  }

  /**
    * Add security access to page data 
    *
    *@param request   HttpServletRequest
    *@param pageData  HashMap to populate
    *@param userId    User id
    */
  private void populateSecurityAccess(HttpServletRequest request, HashMap pageData, String userId) throws Exception
  {
    String securityToken = request.getParameter(OPMConstants.SEC_TOKEN);
    String context = request.getParameter(OPMConstants.CONTEXT);

    String updateAllowed = null;
    //Check if user has edit abilities
    SecurityManager securityManager = SecurityManager.getInstance();
    if (securityManager.assertPermission(context, securityToken, "Inventory Tracking", OPMConstants.SECURITY_UPDATE))
    {
      updateAllowed = "Y";
    }
    else
    {
      updateAllowed = "N";
    }                
           
    pageData.put("update_allowed", updateAllowed);
  }    

  /**
    * Populate page data 
    *
    *@param pageData  HashMap to populate
    *@param request   HttpServletRequest
    */
  private void populatePageData(HttpServletRequest request, HashMap pageData) throws Exception
  {
    String value = null;

    //store the admin action
    value = request.getParameter(OPMConstants.ADMIN_ACTION);
    pageData.put(OPMConstants.ADMIN_ACTION, value);
  }    


  /**
    * Obtain the user id 
    *
    *@param request   HttpServletRequest
    *@return          User id
    */
  private String getUserId(HttpServletRequest request) throws Exception
  {
    //Retrieve user information
    String securityToken = request.getParameter(OPMConstants.SEC_TOKEN);
    String userId = null;
    if(SecurityManager.getInstance().getUserInfo(securityToken)!=null)
    {
      userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
    }
    return userId;
  }


  /**
    * Rebuild the vendors available and vendors chosen lists based 
    * on a list of vendor ids.  
    *
    * @param conn             Database connection
    * @param vendorIdsChosen  Comma delimited list of vendor ids
    * @return                 HashMap of vendors_availabe and vendors_chosen
    * @throws Exception
  */
  private HashMap rebuildVendors(Connection conn, String vendorIdsChosen)
    throws Exception
  {
    VendorMasterVO vendorVO = null;
    HashMap vendorMap = new HashMap();
    HashMap vendorsAvailableMap = new HashMap();
    ArrayList vendorsChosenList = new ArrayList();

    //Obtain all vendors
    InventoryTrackingEmailAlertsBO inventoryTrackingEmailAlertsBO = new InventoryTrackingEmailAlertsBO();
    HashMap results = inventoryTrackingEmailAlertsBO.getVendorByInvTrkEmail(conn, null);
    ArrayList vendorsAvailableList = (ArrayList)results.get("vendor_available");

    //Build a map of all vendors
    for(Iterator it = vendorsAvailableList.iterator(); it.hasNext();)
    {
      vendorVO = (VendorMasterVO)it.next();
      vendorsAvailableMap.put(vendorVO.getVendorID(), vendorVO);
    }

    //Iterate through the comma delimited list of vendors chosen
    StringTokenizer st = new StringTokenizer(vendorIdsChosen, ",");
    String vendorId;
    while(st.hasMoreTokens())
    {
      //If the chosen vendor exists in the vendors available map
      //add that vendor to the vendors chosen list and remove
      //remove the vendor from the map of vendors available
      vendorId = st.nextToken();
      vendorVO = (VendorMasterVO)vendorsAvailableMap.get(vendorId);
      if(vendorVO != null)
      {
        vendorsChosenList.add(vendorVO);
        vendorsAvailableMap.remove(vendorId);
      }
    }

    //Rebuild the vendors available list
    vendorsAvailableList = new ArrayList();
    for(Iterator it = vendorsAvailableMap.keySet().iterator(); it.hasNext();)
    {
      vendorVO = (VendorMasterVO)vendorsAvailableMap.get((String)it.next());
      vendorsAvailableList.add(vendorVO);
    }

    //Set the vendor map
    vendorMap = new HashMap();
    vendorMap.put("vendor_available", vendorsAvailableList);
    vendorMap.put("vendor_chosen", vendorsChosenList);
    
    return vendorMap;
  }

  /**
    * Converts vendor value objects to XML
    *
    * @param vendorsVOList List of vendors to convert
    * @param topNode       Root XML node
    * @return              XML document of vendors
    * @throws Exception
  */
  private Document convertVendorVOListToXML(List vendorsVOList, String topNode)
    throws Exception
  {

    VendorMasterVO vmVO = null;
    Document vendorXML = DOMUtil.getDefaultDocument();
  
    //generate the top node
    Element vendorElement = vendorXML.createElement(topNode);
    //and append it
    vendorXML.appendChild(vendorElement);
  
    int elementNum = 0;
    for (int x = 0; x < vendorsVOList.size(); x++)
    {
      elementNum++;
      vmVO = (VendorMasterVO) vendorsVOList.get(x);
      Document parser = DOMUtil.getDocument(vmVO.toXML(elementNum));
      vendorXML.getDocumentElement().appendChild(vendorXML.importNode(parser.getFirstChild(), true));
    }

    return vendorXML;
  }

}
