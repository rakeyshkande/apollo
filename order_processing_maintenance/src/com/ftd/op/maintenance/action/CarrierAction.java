package com.ftd.op.maintenance.action;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.op.maintenance.util.CommonUtil;
import com.ftd.op.maintenance.util.XMLUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Struts action class
 */
public class CarrierAction extends Action 
{
    private Logger logger;
    
    private static final int USAGE_PERCENT_TOTAL = 100;
    private static final String APP_PARAMETERS = "parameters";
    private static final String CARRIER_ACTION_XSL = "VendorShipServicesEditXSL";
    private static final String EDIT_CARRIER_PARAMETER = "edit_carrier";
    private static final String ERRORS_EXIST = "errors_exist";
    private static final String ERROR_PAGE = "ErrorPage";
    private static final String INSERT_RESPONSE = "insert_response";
    private static final String LOAD_PAGE_PARAMETER = "load_page";
    private static final String PRODUCTION_SUCCESS = "production_success";
    private static final String SUCCESS = "EditCarrier";
    private static final String SYSTEM_ERROR_MESSAGE = "system_error_message";

    /**
    * Default constructor.
    * Sets the logger variable
    */
    public CarrierAction()
    {
        logger  = new Logger("com.ftd.op.maintenance.action.CarrierAction");
    }
    
  /**
   * This method will:
   * 1.  Validate security
   * 2.  Validate the action_type parameter passed
   * 3.  Get a database connection
   * 4.  Forward the request to other methods depending on the processing required
   * 5.  Return the results in the response object
   * 6.  All errors are returned in the response object
   * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
	 * @return forwarding action - next action to "process" request
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
        String actionType = null;
        Connection conn = null;
        ActionForward forward = null;
        
        try
        {        
            /* get action type request parameter */
            actionType = request.getParameter("action_type");
            
            /* get db connection */
            conn = CommonUtil.getDBConnection();
            
            /* get security token */
            String sessionId = (String) request.getParameter("securitytoken");
            String context = (String) request.getParameter("context");
            String userId = null;
            
            if(sessionId == null)
            {
                logger.error("Security token not found in request object");
            }
            
            /* retrieve user information */
            if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
            
            /* determine which action to process */            
            if(actionType == null || actionType.equals(LOAD_PAGE_PARAMETER))
            {
                processLoadPage(request, response, mapping, conn, false);
            }
            else if (actionType.equals(EDIT_CARRIER_PARAMETER))
            {
                processEditCarrier(request, response, mapping, conn);
                processLoadPage(request, response, mapping, conn, true);
            }
            else
            {
                throw new Exception("Unknown action type:" + actionType);
            }
        }
        catch (Throwable  t)
        {
            logger.error(t);
            forward = mapping.findForward(ERROR_PAGE);
        }
        finally
        {
          try
          {
            conn.close();
          }
          catch (SQLException se)
          {
            logger.error(se);
            forward = mapping.findForward(ERROR_PAGE);
          }
        }    
    
        return forward;
  }
    
  /**
   * This method will: 
   * 1.  Gather any requried information
   * 2.  Create an XML transformation to produce the web page 
   *     using loadShippingTables.xsl.
   * 3.  All errors will be returned by throwning an exception
   * 
   * @throws java.lang.Exception
   * @param conn
   * @param mapping
   * @param response
   * @param request
   */
    private void processLoadPage(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping, Connection conn, boolean saved)
        throws Exception
    {
        final File xslFile = getXSL(CARRIER_ACTION_XSL, mapping);
        final MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
        
        final String vendorId = request.getParameter("vendor_id");
        
        final Document shippingKeys = maintDAO.getVendorCarrierRef(vendorId);

        final Document vendorInfo = maintDAO.getVendor(vendorId);
        
        // TODO: set vendorID in page
        
        //create page data document
        Document vendorDetailDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element pageDataXML = (Element) vendorDetailDoc.createElement("vendor_detail");
        vendorDetailDoc.appendChild(pageDataXML);     
        XMLUtil.addElement(vendorDetailDoc,"origin",request.getParameter("origin"));     
        
        if (saved)
        {
          XMLUtil.addElement(vendorDetailDoc,"saved","true");     
        }
        else
        {
          XMLUtil.addElement(vendorDetailDoc,"saved","false");
        }
        
        if (null != vendorDetailDoc)
        {
            DOMUtil.addSection(shippingKeys,vendorDetailDoc.getChildNodes());            
        }
        
        if (null != vendorInfo) 
        {
          DOMUtil.addSection(shippingKeys,vendorInfo.getChildNodes());  
        }
        
        /* Transform output page */
        TraxUtil.getInstance().transform(request, response, shippingKeys, xslFile, (Map)request.getAttribute(APP_PARAMETERS));
    }
    
  /**
   * This method will: 
   * 1.  Gather any requried information
   * 2.  Validate the data before storing
   * 3.  Update the database with valid usage
   * 4.  All errors will be returned by throwning an exception
   * 
   * @throws java.lang.Exception
   * @param conn
   * @param mapping
   * @param response
   * @param request
   */
    private void processEditCarrier(HttpServletRequest request, 
        HttpServletResponse response, 
        ActionMapping mapping, 
        Connection conn) throws Exception
    {
        int usageTotal = 0;

        final MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
        final String vendorID = request.getParameter("vendor_id");
        int position = 1;
  
        while (true) 
        {
          final String keyID = request.getParameter("KeyID" + position);
          
          if (null == keyID) 
          {
            break;
          }
          
          final String usageAsString = request.getParameter("Usage" + position);
          final int usageAsInt = Integer.parseInt(usageAsString);
      
          String exceptionStartDateAsString = request.getParameter("overrideStartDate");
          String exceptionEndDateAsString = request.getParameter("overrideEndDate");
          String exceptionUsageAsString = request.getParameter("ExceptionUsage" + position);

          Date exceptionStartDateAsDate = null;
          
          if (null != exceptionStartDateAsString && !"".equals(exceptionStartDateAsString))
          {
            exceptionStartDateAsDate = FieldUtils.formatStringToSQLDate(exceptionStartDateAsString);
          }
          
          Date exceptionEndDateAsDate = null;
          
          if (null != exceptionEndDateAsString && !"".equals(exceptionEndDateAsString))
          {
            exceptionEndDateAsDate = FieldUtils.formatStringToSQLDate(exceptionEndDateAsString);
          }
          
          Integer exceptionUsageAsInteger = null;
          
          if (null != exceptionUsageAsString && !"".equals(exceptionUsageAsString))
          {
            exceptionUsageAsInteger = new Integer(exceptionUsageAsString);
          }
          
          // TODO: Get vendorID from page information
          maintDAO.updateVendorCarrierRef(vendorID, keyID, usageAsInt, 
              exceptionStartDateAsDate, exceptionEndDateAsDate, 
              exceptionUsageAsInteger);
          
          position++;
        }
    }
    
  /**
   * This method will: 
   * 1. Checks if a usage percent is within the valid range
   * 2. Return true if usage percent is greater than 0 and less than or
   *    equal to 100. Otherwise it will return false..
   * 
   * @param usagePercent
   * @return boolean - whether the usage percent is valid
   */
    private boolean isValidUsagePercent(final int usagePercent)
    {
      return 0 < usagePercent && USAGE_PERCENT_TOTAL >= usagePercent;
    }
    
   /**
    * get XSL file
    *
    * Retrieve name of Carrier Action XSL file
    * @param1 String - the xsl name returned from the Business Object
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {
        ActionForward forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath();      
        File xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        
        return xslFile;
    }
}