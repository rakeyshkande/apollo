package com.ftd.op.maintenance.action;

import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 * <code>LocationLookupAction</code> looks up city, state, and zipcode information.
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Id: LocationLookupAction.java,v 1.3 2011/06/30 15:19:01 gsergeycvs Exp $
 */
public class LocationLookupAction extends Action { 

    public static final String PROPERTY_FILE = "maintenance-config.xml";
    private static String DATASOURCE_NAME = "CLEAN";
    public static final String ACTN_ERROR = "Error";


    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LocationLookupAction");

    /**
     * Default constructor.
     */
    public LocationLookupAction() {
        super();
    }

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception {

        Connection connection = getDBConnection();
        try {
            String zip = request.getParameter("zipCodeInput");
            String city = request.getParameter("cityInput");
            String state = request.getParameter("stateInput");
            String country = request.getParameter("countryInput");

            // create base document
            Document responseDocument = DOMUtil.getDocument();
            MaintenanceDAO dao = new MaintenanceDAO(connection);
            DOMUtil.addSection( responseDocument, dao.getStateList().getChildNodes() ); // states
            DOMUtil.addSection( responseDocument, dao.getZipcodes(zip, city, state).getChildNodes() ); // zipcodes

            // pageData
            HashMap pageData = new HashMap();
            pageData.put("zipCodeInput", zip);
            pageData.put("cityInput", city);
            pageData.put("stateInput", state);
            pageData.put("countryInput", country);
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true); 

            ActionForward forward = mapping.findForward("success");
            File xslFile = new File(this.getServlet().getServletContext().getRealPath(forward.getPath()));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, (HashMap)request.getAttribute("parameters"));
            return null;
        }
        catch (SQLException se) {
            logger.error(se);
            return mapping.findForward(ACTN_ERROR);
        }
        finally {
            try {
                if(connection != null)
                    connection.close();
            }
            catch (SQLException se) {
                logger.error(se);
                return mapping.findForward(ACTN_ERROR);
            }
        }
    }
    
    	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
	 * @return Connection - db connection
	 * @throws ClassNotFoundException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,
															   DATASOURCE_NAME));

		return conn;
	}  
}