package com.ftd.op.maintenance.exception;

/**
 * NovatorTransmissionException
 *
 * Custom exception for Novator transmission errors.
 *
 */
public class NovatorTransmissionException extends Exception
{
    Throwable exceptionCause = null;

    /**
     * Constructor which accepts a message.
     * 
     * @param message Message associated with the error.
    */
    public NovatorTransmissionException(String message)
    {
        super(message);
    }
  
    /**
     * Constructor which accepts a message and an exception.
     * 
     * @param message Message associated with the error.
     * @param exception Exception.
    */
    public NovatorTransmissionException(String message, Throwable exception)
    {
        super(message, exception);
        exceptionCause = exception;
    }

    /**
     * Overriding the printStackTraceMethod
     * */
    public void printStackTrace()
    {
        if (exceptionCause!=null)
        {
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}
