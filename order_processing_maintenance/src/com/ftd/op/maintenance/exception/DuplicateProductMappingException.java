package com.ftd.op.maintenance.exception;

public class DuplicateProductMappingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateProductMappingException() {
		
	}

	public DuplicateProductMappingException(String message) {
		super(message);
	}

	public DuplicateProductMappingException(Throwable cause) {
		super(cause);
	}

	public DuplicateProductMappingException(String message, Throwable cause) {
		super(message, cause);
	}

}
