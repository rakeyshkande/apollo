package com.ftd.op.maintenance.constants;


/**
 * Static information for order processing maintenance
 */

public class OPMConstants {
	
    /*********************************************************************************************
    //Request Parameters
    *********************************************************************************************/
    public static final String ACTION_TYPE                  = "action_type";
    public static final String START_POSITION               = "start_position";

    public static final String PROPERTY_FILE                = "maintenance-config.xml";
    public static final String ORDER_PROCESSING_MAINT_CONFIG_CONTEXT = "ORDER_PROCESSING_MAINT_CONFIG";
    public static final String DATASOURCE_NAME              = "CLEAN";
    public static final String CONS_MAIN_MENU_URL                   = "MAIN_MENU_URL";    
    public static final String ADMIN_ACTION                   = "adminAction";  
    public static final String DEFAULT_CARD_PRICE             = "DEFAULT_CARD_PRICE";    

    

    public static final String BLOCK_TYPE_SHIPPING = "Shipping";
    public static final String BLOCK_TYPE_DELIVERY = "Delivery";
    public static final String BLOCK_TYPE_BOTH     = "Both";
    public static final String FEDEX_IMPORT_BATCH_SIZE = "FEDEX_IMPORT_BATCH_SIZE";

    /**
     *  Request info used by the DataFilter
    **/
    //comments related
    public static final String START_ORIGIN                 = "start_origin";

    /*********************************************************************************************
    //Constants
    *********************************************************************************************/
  	public static final String CONS_APP_PARAMETERS                  = "parameters";
    public static final String CONS_SYSTEM_ERROR                    = "Error";
    public final static String NOVATOR_LIVE_KEY                     = "novator.live";
    public final static String NOVATOR_UAT_KEY                      = "novator.uat";
    public final static String NOVATOR_TEST_KEY                     = "novator.test";
    public final static String NOVATOR_CONTENT_KEY                  = "novator.content";

    /*********************************************************************************************
    //TRIP STATUS Constants
    *********************************************************************************************/
    //Trip Status
  	public static final String STATUS_TRIP_ACCEPTING_ORDERS              = "Accepting Orders";
  	public static final String STATUS_TRIP_ACTIVE                        = "A";
  	public static final String STATUS_TRIP_CANCELED                      = "X";
  	public static final String STATUS_TRIP_CLOSED                        = "C";
  	public static final String STATUS_TRIP_CUTOFF_REACHED                = "Cutoff Reached";
  	public static final String STATUS_TRIP_FULL                          = "F";

    //Trip Vendor Status
  	public static final String STATUS_TRIP_VENDOR_ASSIGNED               = "A";
  	public static final String STATUS_TRIP_VENDOR_CANCELED               = "X";
  	public static final String STATUS_TRIP_VENDOR_UNASSIGNED             = "U";

    /*********************************************************************************************
    //Lock Constants
    *********************************************************************************************/
  	public static final String ZJ_TRIP_ENTITY_TYPE                       = "ZJ_TRIP";
  	public static final String INV_TRK_ENTITY_TYPE                       = "INV_TRK";
  	public static final String ADD_ON_ENTITY_TYPE                        = "ADD_ON";

    /********************************************************************************************
     * Global Table Constants
     ********************************************************************************************/
    //CONTEXT Constants
    public static final String CONTEXT_SHIPPING_PARMS               = "SHIPPING_PARMS";
    public static final String CONTEXT_MERCHANDISING                = "MERCHANDISING";
    
    //NAME Constants
    public static final String NAME_BACKEND_CUTOFF_DELAY            = "BACKEND_CUTOFF_DELAY";
    public static final String NAME_MAX_BOX_HEIGHT_IN_INCHES        = "MAX_BOX_HEIGHT_IN_INCHES";
    public static final String NAME_MAX_BOX_LENGTH_IN_INCHES        = "MAX_BOX_LENGTH_IN_INCHES";
    public static final String NAME_MAX_BOX_WIDTH_IN_INCHES         = "MAX_BOX_WIDTH_IN_INCHES";
    public static final String NAME_MAX_PALLETS_PER_TRUCK           = "MAX_PALLETS_PER_TRUCK";
    public static final String NAME_STANDARD_BOXES_PER_PALLET       = "STANDARD_BOXES_PER_PALLET";



    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT    = "context";
    public static final String SEC_TOKEN  = "securitytoken";
    public static final String SECURITY_IS_ON  = "SECURITY_IS_ON";
    
    public static final String SECURITY_UPDATE = "Update";
    
    public static final String REQUSET_PARAM_PAGE_DATA = "pagedata";


    /*********************************************************************************************
    // Transaction parms used to send data over to Novator
    *********************************************************************************************/
    public static final String NOVATOR_SERVER                 = "novator.server.ip";
    public static final String NOVATOR_PORT                   = "novator.server.port";
    public static final String TRANSACTION_TIMEOUT_IN_SECONDS = "transaction_timeout_in_seconds";
    public static final String JNDI_USERTRANSACTION_ENTRY     = "jndi_usertransaction_entry";
    public static final String NOVATOR_TIMEOUT_KEY            = "novator.timeout"; 
    public static final String NOVATOR_XML_VENDOR_HEADER      = "<?xml version='1.0'?>";

    public static final String NOVATOR_CONTENT_IP_KEY         = "content_feed_ip";  
    public static final String NOVATOR_CONTENT_PORT_KEY       = "content_feed_port";  
    public static final String NOVATOR_LIVE_IP_KEY            = "production_feed_ip";  
    public static final String NOVATOR_LIVE_PORT_KEY          = "production_feed_port";  
    public static final String NOVATOR_TEST_IP_KEY            = "test_feed_ip";  
    public static final String NOVATOR_TEST_PORT_KEY          = "test_feed_port";  
    public static final String NOVATOR_UAT_IP_KEY             = "uat_feed_ip";  
    public static final String NOVATOR_UAT_PORT_KEY           = "uat_feed_port"; 
    /*********************************************************************************************
    // Zone Jump error messages
    *********************************************************************************************/
    public static final String ZONE_JUMP_SHIPPING_DATE_BLOCK_ERROR     = "A shipping date block cannot be on the same day as a Zone Jump trip departure date.  Vendor(s) effected: { vendorIds}.";  
    public static final String ZONE_JUMP_DELIVERY_DATE_BLOCK_ERROR     = "A delivery date block cannot be on the same day as a Zone Jump trip delivery date.  Vendor(s) effected: { vendorIds}";  
    public static final String ZONE_JUMP_BLOCK_ERROR                   = "A block date cannot be on the same day as a Zone Jump trip departure or delivery date.  Vendor(s) effected: { vendorIds}.";    
    
    /*********************************************************************************************
    // Miles Points Redemption Rate Maintenance
    *********************************************************************************************/    
    public static final String REQUEST_MP_RD_RATE_ID = "mp_rd_rate_id";
    public static final String REQUEST_MP_RD_DESCRIPTION = "mp_rd_description";
    public static final String REQUEST_MP_RD_PAYMENT_METHOD_ID = "mp_rd_payment_method_id";
    public static final String REQUEST_MP_RD_RATE_AMT = "mp_rd_rate_amt";
    public static final String REQUEST_MP_RD_REQUESTED_BY = "mp_rd_requested_by";
    
    public static final String NOVATOR_FEED_ACTION_ADD = "Add";
    public static final String NOVATOR_FEED_ACTION_DELETE = "Delete";
    public static final String NOVATOR_FEED_ACTION_UPDATE = "Update";
    
    public static final String GLOBAL_PARM_CONTEXT_GLOBAL = "FTDAPPS_PARMS";
    public static final String GLOBAL_PARM_CONTEXT_MESSAGE_GENERATOR = "MESSAGE_GENERATOR_CONFIG";
    public static final String GLOBAL_PARM_NAME_SMTP_HOST = "SMTP_HOST_NAME";
    public static final String EMAIL_CONFIG_FILE = "email_information.xml";
    public static final String EMAIL_MP_RR_FROM_ADDRESS = "mp_redemption_rate_from_address";
    public static final String EMAIL_MP_RR_TO_ADDRESS = "ACCTG_GROUP_EMAIL_ADDRESS";
    public static final String EMAIL_MP_RR_SUBJECT = "mp_redemption_rate_subject";
    
    public static final String RETURN_MESSAGE_CANNOT_CONNECT_NOVATOR = "novatorConnect";
    public static final String RETURN_MESSAGE_NOVATOR_DEN = "novatorDEN";
    public static final String RETURN_MESSAGE_SAVE_SUCCESSFUL = "saveSuccessful";
    public static final String RETURN_MESSAGE_SAVE_FAILED = "saveFailed";
    public static final String RETURN_MESSAGE_DELETE_SUCCESSFUL = "deleteSuccessful";
    public static final String RETURN_MESSAGE_DELETE_FAILED = "deleteFailed";
    
    public static final String PARAMETER_NOVATOR_UPDATE_LIVE = "NOVATOR_UPDATE_LIVE";
    public static final String PARAMETER_NOVATOR_UPDATE_TEST = "NOVATOR_UPDATE_TEST";
    public static final String PARAMETER_NOVATOR_UPDATE_CONTENT = "NOVATOR_UPDATE_CONTENT";
    public static final String PARAMETER_NOVATOR_UPDATE_UAT = "NOVATOR_UPDATE_UAT";   
    
    public static final String NOVATOR_CONFIG = "NOVATOR_CONFIG";
    public static final String NOVATOR_PRODUCTION_FEED_IP = "production_feed_ip";
    public static final String NOVATOR_PRODUCTION_FEED_PORT = "production_feed_port";
    public static final String NOVATOR_CONTENT_FEED_IP = "content_feed_ip";
    public static final String NOVATOR_CONTENT_FEED_PORT = "content_feed_port";
    public static final String NOVATOR_TEST_FEED_IP = "test_feed_ip";
    public static final String NOVATOR_TEST_FEED_PORT = "test_feed_port";
    public static final String NOVATOR_UAT_FEED_IP = "uat_feed_ip";
    public static final String NOVATOR_UAT_FEED_PORT = "uat_feed_port";
    public static final String NOVATOR_FEED_TIMEOUT = "feed_timeout";
    
    public static final String NOVATOR_CONTENT_FEED_ALLOWED  = "content_feed_allowed";
    public static final String NOVATOR_PRODUCTION_FEED_ALLOWED  = "production_feed_allowed";
    public static final String NOVATOR_TEST_FEED_ALLOWED  = "test_feed_allowed";
    public static final String NOVATOR_UAT_FEED_ALLOWED  = "uat_feed_allowed";
    public static final String NOVATOR_CONTENT_FEED_CHECKED  = "content_feed_checked";
    public static final String NOVATOR_PRODUCTION_FEED_CHECKED  = "production_feed_checked";
    public static final String NOVATOR_TEST_FEED_CHECKED  = "test_feed_checked";
    public static final String NOVATOR_UAT_FEED_CHECKED  = "uat_feed_checked";
    
    public static final String NOVATOR_UPDATE_SETUP  = "novator_update_setup";    
    
    public static final String REQUEST_APPLY_SURCHARGE_CODE = "apply_surcharge_code";
    public static final String REQUEST_FUEL_SURCHARGE_AMT = "fuel_surcharge_amt";
    public static final String REQUEST_SEND_FUEL_SURCHARGE_TO_FLORIST = "send_surcharge_to_florist";
    public static final String REQUEST_FUEL_SURCHARGE_DESCRIPTION = "surcharge_description";
    public static final String REQUEST_DISPLAY_SURCHARGE = "display_surcharge";

    /*********************************************************************************************
    //Price Pay Vendor Update Constants
    *********************************************************************************************/
    public static final String PPVU_PRICE_VARIANCE_THRESHOLD = "PPVU_PRICE_VARIANCE_THRESHOLD";
    public static final String PPVU_UPDATE_EXCEPTIONS_ALLOWED = "PPVU_UPDATE_EXCEPTIONS_ALLOWED";
    public static final String PPVU_EMAIL_DISTRIBUTION_LIST = "PPVU_EMAIL_DISTRIBUTION_LIST";
    public static final String PPVU_SUCCESS_REPORT_ID = "PPVU_SUCCESS_REPORT_ID";
    public static final String PPVU_ERROR_REPORT_ID = "PPVU_ERROR_REPORT_ID";
    
    /*********************************************************************************************
    //Service Fee Maint Constants
    *********************************************************************************************/    
    public static final String DEFAULT_SORT_FIELD  = "SNH_ID";
    
    /********************************************************************************************
    	11945 - DELIVERY FEE MAINT CONSTANTS
    *********************************************************************************************/  
    public static final String DEFAULT_DELIVERY_FEE_SORT_ID = "to_number(DELIVERY_FEE_ID)";
    public static final int MORNING_DELIVERY_TYPE = 1;
    public static final String MORNING_DELIVERY_FEE_COL = "morning_delivery_fee";
    
    /*********************************************************************************************
     * Phoenix Product Mapping Constants
     * 
     * *******************************************************************************************/
    public static final String INVALID_FLORIST_PRODUCT_ERROR_MSG = "Product SKU must be eligible Floral Product";
    public static final String INVALID_VENDOR_PRODUCT_ERROR_MSG = "Vendor Product SKU must be eligible Vendor Product";
    public static final String FLORIST_SKU_ALREADY_EXISTS_ON_FILE_ERROR_MSG  = "Duplicate Product SKU. Product SKU already exists in Product Mapping List";
    public static final String FLORAL_PRODUCT_NOT_IN_PDB_MSG = "Product SKU must exist in PDB";
    public static final String VENDOR_PRODUCT_NOT_IN_PDB_MSG = "Vendor Product SKU must exist in PDB";
    public static final String FLORAL_PRODUCT_NOT_NULL_MSG = "Product SKU/Add-On must not be blank.";
    public static final String VENDOR_PRODUCT_NOT_NULL_MSG = "Vendor Product SKU must not be blank.";
    public static final String ADDON_MUST_BE_VENDOR_MSG = "Florist Add-ons can only be mapped to Vendor Add-ons.  ";
    public static final String FLORAL_PRODUCT_NO_PRICE_MSG = "Choose another Product SKU.  No price points greater than $0 exist in PDB.  ";
     
     // GBB mappings for phoenix_product_mappings table
     public static final String DB_GBB_GOOD   = "GOOD";  
     public static final String DB_GBB_BETTER = "BETTER";  
     public static final String DB_GBB_BEST   = "BEST";  
     
     
}