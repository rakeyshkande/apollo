package com.ftd.op.maintenance.constants;

public class DAOConstants 
{
  //SP_GET_PRODUCT_BY_ID
  public final static String PRODUCT_ID_LC = "productId";
  public final static String NOVATOR_ID_LC = "novatorId";
  public final static String PRODUCT_NAME_LC = "productName";
  public final static String NOVATOR_NAME_LC = "novatorName";
  public final static String STATUS_LC = "status";
  public final static String DELIVERY_TYPE_LC = "deliveryType";
  public final static String CATEGORY_LC = "category";
  public final static String PRODUCT_TYPE_LC = "productType";
  public final static String PRODUCT_SUB_TYPE_LC = "productSubType";
  public final static String COLOR_SIZE_FLAG_LC = "colorSizeFlag";
  public final static String STANDARD_PRICE_LC = "standardPrice";
  public final static String DELUXE_PRICE_LC = "deluxePrice";
  public final static String PREMUIM_PRICE_LC = "premiumPrice";
  public final static String PREFERRED_PRICE_POINT_LC = "preferredPricePoint";
  public final static String VARIABLE_PRICE_MAX_LC = "variablePriceMax";
  public final static String SHORT_DESCRIPTION_LC = "shortDescription";
  public final static String LONG_DESCRIPTION_LC = "longDescription";
  public final static String FLORIST_REFERENCE_NUMBER_LC = "floristReferenceNumber";
  public final static String MERCURY_DESCRIPTION_LC = "mercuryDescription";
  public final static String ITEM_COMMENTS_LC = "itemsComments";
  public final static String ADD_ON_BALLOONS_FLAG_LC = "addOnBallonsFlag";
  public final static String ADD_ON_BEARS_FLAG_LC = "addOnBearsFlag";
  public final static String ADD_ON_CARDS_FLAG_LC = "addOnCardsFlag";
  public final static String ADD_ON_FUNERAL_FLAG_LC = "addOnFuneralFlag";
  public final static String CODIFIED_FLAG_LC = "codifiedFlag";
  public final static String EXECPTION_CODE_LC = "codifiedFlag";
  public final static String EXCEPTION_START_DATE_LC = "exceptionStartDate";
  public final static String EXECPTION_END_DATE_LC = "exceptionEndDate";
  public final static String EXCEPTION_MESSAGE_LC = "exceptionMessage";
  public final static String VENDOR_ID_LC = "vendorId";
  public final static String VENDOR_COST_LC = "vendorCost";
  public final static String VENDOR_SKU_LC = "vendorSku";
  public final static String SECOND_CHOICE_CODE_LC = "secondChoiceCode";
  public final static String HOLIDAY_SECOND_CHOICE_CODE_LC = "holidaySecondChoiceCode";
  public final static String DROPSHIP_CODE_LC = "dropshipCode";
  public final static String DISCOUNT_ALLOWED_FLAG_LC = "discountAllowedFlag";
  public final static String DELIVERY_INCLUDED_FLAG_LC = "discountAllowedFlag";
  public final static String TAX_FLAG_LC = "taxFlag";
  public final static String SERVICE_FEE_FLAG_LC = "serviceFeeFlag";
  public final static String EXOTIC_FLAG_LC = "exoticFlag";
  public final static String EGIFT_FLAG_LC = "egiftFlag";
  public final static String COUNTRY_ID_LC = "countryId";
  public final static String ARRANGEMENT_SIZE_LC = "arrangementSize";
  public final static String ARRANGEMENT_COLORS_LC = "arrangementColors";
  public final static String DOMINANT_FLOWERS_LC = "dominantFlowers";
  public final static String SEARCH_PRIORITY_LC = "searchPriority";
  public final static String RECIPE_LC = "recipe";
  public final static String SUBCODE_FLAG_LC = "subcodeFlag";
  public final static String DIM_WEIGHT_LC = "dimWeight";
  public final static String NEXT_DAT_UPGRADE_FLAG_LC = "nextDayUpgradeFlag";
  public final static String CORPORATE_SITE_LC = "corporateSite";
  public final static String UNSPSC_CODE_LC = "unspscCode";
  public final static String PRICE_RANK1_LC = "priceRank1";
  public final static String PRICE_RANK2_LC = "priceRank1";
  public final static String PRICE_RANK3_LC = "priceRank3";
  public final static String SHIP_METHOD_CARRIER_LC = "shipMethodCarrier";
  public final static String SHIP_METHOD_FLORIST_LC = "shipMethodFlorist";
  public final static String SHIPPING_KEY_LC = "shippingKey";
  public final static String VARIABLE_PRICE_FLAG_LC = "variablePriceFlag";
  public final static String HOLIDAY_SKU_LC = "holidaySku";
  public final static String HOLIDAY_PRICE_LC = "holidayPrice";
  public final static String CATALOG_FLAG_LC = "catalogFlag";
  public final static String HOLIDAY_DELUXE_PRICE_LC = "holidayDeluxePrice";
  public final static String HOLIDAY_PREMIUM_PRICE_LC = "holidayPremiumPrice";
  public final static String HOLIDAY_START_DATE_LC = "holidayStartDate";
  public final static String HOLIDAY_END_DATE_LC = "holidayEndDate";
  public final static String DEFAULT_CARRIER_LC = "defaultCarrier";
  public final static String HOLD_UNTIL_AVAILABLE_LC = "holdUntilAvailable";
  public final static String MONDAY_DELIVERY_FRESHCUT_LC = "mondayDeliveryFreshcut";
  public final static String TWO_DAY_SAT_FRESHCUT_LC = "twoDayShipSatFreshcut";
  public final static String VENDOR_TYPE_LC = "vendorType";
  public final static String PERSONALIZATION_TEMPLATE_ORDER_LC = "personalizationTemplateOrder";
  public final static String UPDATED_BY = "updatedBy";
  
  public final static String SKU_IT_CONTEXT = "OPM_INV_CTXT";
  public final static String SKU_IT_RECORD_ENDDATE = "IT_DEFAULT_ENDDATE";


    public DAOConstants()
    {
    }
}