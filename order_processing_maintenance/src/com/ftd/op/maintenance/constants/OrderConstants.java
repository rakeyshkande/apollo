package com.ftd.op.maintenance.constants;


public class OrderConstants 
{
  /**
     * constructor
     * 
     * @param none
     * @return n/a
     * @throws none
     */
  public OrderConstants()
  {
    super();
  }
  
  /* db stored procedure */
  public final static String SP_GET_COMPANY = "GET_COMPANY";
  public final static String SP_GET_CUSTOMER = "GET_CUSTOMER";
  public final static String SP_GET_GLOBAL_PARM_VALUE = "GET_GLOBAL_PARM_VALUE";
  public final static String SP_GET_GLOBAL_PARMETER = "GET_GLOBAL_PARAMETER";
  public final static String SP_GET_GNADD_STATUS = "GET_GNADD_STATUS";
  public final static String SP_GET_ORDER_BY_GUID = "GET_ORDER_BY_GUID";
  public final static String SP_GET_ORDER_DETAIL_PAYMENT = "GET_ORDER_DETAIL_PAYMENT";
  public final static String SP_GET_SOURCE_CODE = "GET_SOURCE_CODE_RECORD_RESULTSET";
  public final static String SP_UPDATE_ORDER_DETAILS = "UPDATE_ORDER_DETAILS";
  public final static String SP_GET_PRODUCT_BY_ID = "GET_PRODUCT_BY_ID";
  public final static String SP_GET_LP_INDICATOR_COUNT = "GET_LP_INDICATOR_COUNT";
  public final static String SP_CAN_FLORIST_SUPPORT_ORDER = "CAN_FLORIST_SUPPORT_ORDER";
  public final static String SP_IS_CUSTOMER_ON_ORDER_HOLD = "IS_CUSTOMER_ON_ORDER_HOLD";
  public final static String SP_GET_DAILY_FLORIST = "GET_DAILY_FLORIST";
  public final static String SP_GET_VENDOR = "GET_VENDOR";
  public final static String SP_GET_INTERNATIONAL_FLORIST = "GET_INTERNATIONAL_FLORIST";
  public final static String SP_GET_ROUTABLE_FLORISTS = "GET_ROUTABLE_FLORISTS";
  public final static String SP_INSERT_ORDER_FLORIST_USED = "INSERT_ORDER_FLORIST_USED";
  public final static String SP_UPDATE_CSR_LOCKED_ENTITIES = "UPDATE_CSR_LOCKED_ENTITIES";
  public final static String SP_DELETE_CSR_LOCKED_ENTITIES = "DELETE_CSR_LOCKED_ENTITIES";
  public final static String SP_GET_ORDER_BILLS = "GET_ORDER_BILLS";
  public final static String SP_GET_ORDER_DETAIL = "GET_ORDER_DETAILS";
   
  
  /* db column names and input parameters for db stored procedure */
  
  //common
  public final static String ORDER_GUID = "ORDER_GUID";
  public final static String SOURCE_CODE = "SOURCE_CODE";
  public final static String COMPANY_ID = "COMPANY_ID";
  public final static String ENABLE_LP_PROCESSING = "ENABLE_LP_PROCESSING";
  public final static String PHONE_NUMBER = "PHONE_NUMBER";
  public final static String ORDER_DETAIL_ID = "ORDER_DETAIL_ID";  
  public final static String DELIVERY_DATE = "DELIVERY_DATE";
  public final static String PRODUCT_ID = "PRODUCT_ID";
  public final static String FLORIST_ID = "FLORIST_ID";
  public final static String CUSTOMER_ID = "CUSTOMER_ID";
  
  //SP_UPDATE_ORDER_DETAIL
  public final static String RECIPIENT_ID = "RECIPIENT_ID";  
  public final static String QUANTITY = "QUANTITY";
  public final static String COLOR_1 = "COLOR_1";
  public final static String COLOR_2 = "COLOR_2";
  public final static String SUBSTITUTION_INDICATOR = "SUBSTITUTION_INDICATOR";
  public final static String SAME_DAY_GIFT = "SAME_DAY_GIFT";
  public final static String OCCASION = "OCCASION";
  public final static String CARD_MESSAGE = "CARD_MESSAGE";
  public final static String CARD_SIGNATURE = "CARD_SIGNATURE";
  public final static String SPECIAL_INSTRUCTIONS = "SPECIAL_INSTRUCTIONS";
  public final static String RELEASE_INFO_INDICATOR = "RELEASE_INFO_INDICATOR";  
  public final static String SHIP_METHOD = "SHIP_METHOD";
  public final static String SHIP_DATE = "SHIP_DATE";
  public final static String ORDER_DISP_CODE = "ORDER_DISP_CODE";
  public final static String ORDER_HELD = "ORDER_HELD";
  public final static String SECOND_CHOICE_PRODUCT = "SECOND_CHOICE_PRODUCT";
  public final static String ZIP_QUEUE_COUNT = "ZIP_QUEUE_COUNT";
  public final static String RANDOM_WEIGHT_DIST_FAILURES = "RANDOM_WEIGHT_DIST_FAILURES";
  public final static String UPDATED_BY = "UPDATED_BY";
  public final static String DELIVERY_DATE_RANGE_END = "DELIVERY_DATE_RANGE_END";
  public final static String SCRUBBED_ON = "SCRUBBED_ON";
  public final static String SCRUBBED_ON_DATE = "SCRUBBED_ON_DATE";
  public final static String SCRUBBED_BY = "SCRUBBED_BY";
  public final static String USER_ID = "USER_ID";
  public final static String ARIBA_UNSPSC_CODE = "ARIBA_UNSPSC_CODE";
  public final static String ARIBA_PO_NUMBER = "ARIBA_PO_NUMBER";
  public final static String ARIBA_AMS_PROJECT_CODE = "ARIBA_AMS_PROJECT_CODE";
  public final static String ARIBA_COST_CENTER = "ARIBA_COST_CENTER";
  public final static String SIZE_INDICATOR = "SIZE_INDICATOR";
  public final static String MILES_POINTS = "MILES_POINTS";
  public final static String SUBCODE = "SUBCODE";
  
  //SP_GET_LP_ORDER_COUNT
  public final static String LP_ORDER_INDICATOR = "LP_ORDER_INDICATOR";
  public final static String HOURS = "HOURS";
  
  //SP_GET_ORDER_DETAIL_PAYMENT
  public final static String PAYMENT_ID = "PAYMENT_ID";
  public final static String ADDITIONAL_BILL_ID = "ADDITIONAL_BILL_ID";
  public final static String PAYMENT_TYPE = "PAYMENT_TYPE";
  public final static String CC_ID = "CC_ID";
  public final static String CREDIT_AMOUNT = "CREDIT_AMOUNT";
  public final static String DEBIT_AMOUNT = "DEBIT_AMOUNT";
  public final static String AUTH_RESULT = "AUTH_RESULT";
  public final static String AUTH_NUMBER = "AUTH_NUMBER";
  public final static String AVS_CODE = "AVS_CODE";
  public final static String ACQ_REFERENCE_NUMBER = "ACQ_REFERENCE_NUMBER";
  public final static String GC_COUPON_NUMBER = "GC_COUPON_NUMBER";
  public final static String AUTH_OVERRIDE_FLAG = "AUTH_OVERRIDE_FLAG";
  public final static String AUTH_FLAG = "AUTH_FLAG";
  public final static String PAYMENT_INDICATOR = "PAYMENT_INDICATOR";
  public final static String REFUND_ID = "REFUND_ID";
    
  //SP_GET_GNADD_STATUS
  public final static String GNADD_STATUS = "GNADD_STATUS";
  public final static String GNADD_DATE = "GNADD_DATE";
  public final static String GNADD_HOLD = "GNADD_HOLD";
  public final static String NAME_LC = "name";
  public final static String VALUE_LC = "value";
  
  //SP_GET_COMPANY  
  public final static String COMPANY_NAME = "COMPANY_NAME";
  public final static String INTERNET_ORIGIN = "INTERNET_ORIGIN";
  public final static String DEFAULT_PROGRAM_ID = "DEFAULT_PROGRAM_ID";
  public final static String CLEARING_MEMBER_NUMBER = "CLEARING_MEMBER_NUMBER";
  public final static String LOGO_FILE_NAME = "LOGO_FILENAME";  
  
  //SP_GET_SOURCE_CODE
  public final static String SOURCE_TYPE = "SOURCE_TYPE";
  public final static String DEPARTMENT_CODE = "DEPARTMENT_CODE";
  public final static String DESCRIPTION = "DESCRIPTION";
  public final static String START_DATE = "START_DATE";
  public final static String END_DATE = "END_DATE";
  public final static String PRICING_CODE= "PRICING_CODE";
  public final static String SHIPPING_CODE = "SHIPPING_CODE";
  public final static String PARTNER_ID = "PARTNER_ID";
  public final static String VALID_PAY_METHOD = "VALID_PAY_METHOD";
  public final static String LIST_CODE_FLAG = "LIST_CODE_FLAG";
  public final static String DEFAULT_SOURCE_CODE_FLAG = "DEFAULT_SOURCE_CODE_FLAG";
  public final static String BILLING_INFO_PROMPT = "BILLING_INFO_PROMPT";
  public final static String BILLING_INFO_LOGIC = "BILLING_INFO_LOGIC";
  public final static String MARKETING_GROUP = "MARKETING_GROUP";
  public final static String EXTERNAL_CALL_CENTER_FLAG = "EXTERNAL_CALL_CENTER_FLAG";
  public final static String HIGHLIGHT_DESCRIPTION_FLAG = "HIGHLIGHT_DESCRIPTION_FLAG";
  public final static String DISCOUNT_ALLOWED_FLAG = "DISCOUNT_ALLOWED_FLAG";
  public final static String BIN_NUMBER_CHECK_FLAG = "BIN_NUMBER_CHECK_FLAG";
  public final static String ORDER_SOURCE = "ORDER_SOURCE";
  public final static String MILEAGE_BONUS = "MILEAGE_BONUS";
  public final static String PROMOTION_CODE = "PROMOTION_CODE";
  public final static String MILEAGE_CALCULATION_SOURCE = "MILEAGE_CALCULATION_SOURCE";
  public final static String BONUS_POINTS = "BONUS_POINTS";
  public final static String BONUS_TYPE = "BONUS_TYPE";
  public final static String SEPARATE_DATA = "SEPARATE_DATA";
  public final static String YELLOW_PAGES_CODE = "YELLOW_PAGES_CODE";
  public final static String POINTS_MILES_PER_DOLLAR = "POINTS_MILES_PER_DOLLAR";
  public final static String MAXIMUM_POINTS_MILES = "MAXIMUM_POINTS_MILES";
  public final static String JCPENNEY_FLAG = "JCPENNEY_FLAG";
  public final static String OE_DEFAULT_FLAG = "OE_DEFAULT_FLAG";
  public final static String SEND_TO_SCRUB = "SEND_TO_SCRUB";
  public final static String FRAUD_FLAG = "FRAUD_FLAG";
  public final static String EMERGENCY_TEXT_FLAG = "EMERGENCY_TEXT_FLAG";
  public final static String REQUIRES_DELIVERY_CONFIRMATION = "REQUIRES_DELIVERY_CONFIRMATION";
  
  //SP_GET_GLOBAL_PARAMETER  
  public final static String OP = "ORDER_PROCESSING";
  public final static String CONTEXT = "CONTEXT";
  public final static String NAME = "NAME";
  public final static String VALUE = "VALUE";
  public final static String V_VALUE = "V_VALUE";
  public final static String AUTH_FLAG_LC = "AUTH_FLAG";  
  public final static String LP_ORDER_COUNT = "LP_ORDER_COUNT";
  public final static String HOLD_WINDOW = "HOLD_WINDOW";
  public final static String LP_INDICATOR_TIMEFRAME = "LP_INDICATOR_TIMEFRAME";
  public final static String LP_INDICATOR_THRESHOLD = "LP_INDICATOR_THRESHOLD";
    
  //SP_GET_CUSTOMER  
  public final static String CONCAT_ID = "CONCAT_ID";
  public final static String CUSTOMER_FIRST_NAME = "CUSTOMER_FIRST_NAME";
  public final static String CUSTOMER_LAST_NAME = "CUSTOMER_LAST_NAME";
  public final static String BUSINESS_NAME = "BUSINESS_NAME";
  public final static String CUSTOMER_ADDRESS_1 = "CUSTOMER_ADDRESS_1";
  public final static String CUSTOMER_ADDRESS_2 = "CUSTOMER_ADDRESS_2";
  public final static String CUSTOMER_CITY = "CUSTOMER_CITY";
  public final static String CUSTOMER_STATE = "CUSTOMER_STATE";
  public final static String CUSTOMER_ZIP_CODE = "CUSTOMER_ZIP_CODE";
  public final static String CUSTOMER_COUNTRY = "CUSTOMER_COUNTRY";
    
  //SP_GET_PRODUCT_BY_ID
  public final static String PRODUCT_ID_LC = "productId";
  public final static String NOVATOR_ID_LC = "novatorId";
  public final static String PRODUCT_NAME_LC = "productName";
  public final static String NOVATOR_NAME_LC = "novatorName";
  public final static String STATUS_LC = "status";
  public final static String DELIVERY_TYPE_LC = "deliveryType";
  public final static String CATEGORY_LC = "category";
  public final static String PRODUCT_TYPE_LC = "productType";
  public final static String PRODUCT_SUB_TYPE_LC = "productSubType";
  public final static String COLOR_SIZE_FLAG_LC = "colorSizeFlag";
  public final static String STANDARD_PRICE_LC = "standardPrice";
  public final static String DELUXE_PRICE_LC = "deluxePrice";
  public final static String PREMUIM_PRICE_LC = "premiumPrice";
  public final static String PREFERRED_PRICE_POINT_LC = "preferredPricePoint";
  public final static String VARIABLE_PRICE_MAX_LC = "variablePriceMax";
  public final static String SHORT_DESCRIPTION_LC = "shortDescription";
  public final static String LONG_DESCRIPTION_LC = "longDescription";
  public final static String FLORIST_REFERENCE_NUMBER_LC = "floristReferenceNumber";
  public final static String MERCURY_DESCRIPTION_LC = "mercuryDescription";
  public final static String ITEM_COMMENTS_LC = "itemsComments";
  public final static String ADD_ON_BALLOONS_FLAG_LC = "addOnBallonsFlag";
  public final static String ADD_ON_BEARS_FLAG_LC = "addOnBearsFlag";
  public final static String ADD_ON_CARDS_FLAG_LC = "addOnCardsFlag";
  public final static String ADD_ON_FUNERAL_FLAG_LC = "addOnFuneralFlag";
  public final static String CODIFIED_FLAG_LC = "codifiedFlag";
  public final static String EXECPTION_CODE_LC = "codifiedFlag";
  public final static String EXCEPTION_START_DATE_LC = "exceptionStartDate";
  public final static String EXECPTION_END_DATE_LC = "exceptionEndDate";
  public final static String EXCEPTION_MESSAGE_LC = "exceptionMessage";
  public final static String VENDOR_ID_LC = "vendorId";
  public final static String VENDOR_COST_LC = "vendorCost";
  public final static String VENDOR_SKU_LC = "vendorSku";
  public final static String SECOND_CHOICE_CODE_LC = "secondChoiceCode";
  public final static String HOLIDAY_SECOND_CHOICE_CODE_LC = "holidaySecondChoiceCode";
  public final static String DROPSHIP_CODE_LC = "dropshipCode";
  public final static String DISCOUNT_ALLOWED_FLAG_LC = "discountAllowedFlag";
  public final static String DELIVERY_INCLUDED_FLAG_LC = "discountAllowedFlag";
  public final static String TAX_FLAG_LC = "taxFlag";
  public final static String SERVICE_FEE_FLAG_LC = "serviceFeeFlag";
  public final static String EXOTIC_FLAG_LC = "exoticFlag";
  public final static String EGIFT_FLAG_LC = "egiftFlag";
  public final static String COUNTRY_ID_LC = "countryId";
  public final static String ARRANGEMENT_SIZE_LC = "arrangementSize";
  public final static String ARRANGEMENT_COLORS_LC = "arrangementColors";
  public final static String DOMINANT_FLOWERS_LC = "dominantFlowers";
  public final static String SEARCH_PRIORITY_LC = "searchPriority";
  public final static String RECIPE_LC = "recipe";
  public final static String SUBCODE_FLAG_LC = "subcodeFlag";
  public final static String DIM_WEIGHT_LC = "dimWeight";
  public final static String NEXT_DAT_UPGRADE_FLAG_LC = "nextDayUpgradeFlag";
  public final static String CORPORATE_SITE_LC = "corporateSite";
  public final static String UNSPSC_CODE_LC = "unspscCode";
  public final static String PRICE_RANK1_LC = "priceRank1";
  public final static String PRICE_RANK2_LC = "priceRank1";
  public final static String PRICE_RANK3_LC = "priceRank3";
  public final static String SHIP_METHOD_CARRIER_LC = "shipMethodCarrier";
  public final static String SHIP_METHOD_FLORIST_LC = "shipMethodFlorist";
  public final static String SHIPPING_KEY_LC = "shippingKey";
  public final static String VARIABLE_PRICE_FLAG_LC = "variablePriceFlag";
  public final static String HOLIDAY_SKU_LC = "holidaySku";
  public final static String HOLIDAY_PRICE_LC = "holidayPrice";
  public final static String CATALOG_FLAG_LC = "catalogFlag";
  public final static String HOLIDAY_DELUXE_PRICE_LC = "holidayDeluxePrice";
  public final static String HOLIDAY_PREMIUM_PRICE_LC = "holidayPremiumPrice";
  public final static String HOLIDAY_START_DATE_LC = "holidayStartDate";
  public final static String HOLIDAY_END_DATE_LC = "holidayEndDate";
  public final static String DEFAULT_CARRIER_LC = "defaultCarrier";
  public final static String HOLD_UNTIL_AVAILABLE_LC = "holdUntilAvailable";
  public final static String MONDAY_DELIVERY_FRESHCUT_LC = "mondayDeliveryFreshcut";
  public final static String TWO_DAY_SAT_FRESHCUT_LC = "twoDayShipSatFreshcut";
  public final static String VENDOR_TYPE_LC = "vendorType";
  
  //SP_GET_INTERNATIONAL_FLORIST
  public final static String FLORIST_CODE = "FLORIST_CODE";
  
  //SP_GET_ROUTABLE_FLORISTS
  public final static String SCORE = "SCORE";
  public final static String WEIGHT = "WEIGHT";
  public final static String CUTOFF_TIME = "CUTOFF_TIME";
  public final static String STATUS = "STATUS";
  public final static String ORDER_VALUE = "ORDER_VALUE";
  public final static String RETURN_ALL = "RETURN_ALL";
  public final static String DELIVERY_DATE_END = "DELIVERY_DATE_END";
  public final static String ZIP_CODE = "ZIP_CODE";
  
  //SP_CAN_FLORIST_SUPPORT_ORDER
  public final static String CAN_FLORIST_SUPPORT_ORDER = "CAN_FLORIST_SUPPORT_ORDER";
  
  //SP_GET_ROUTABLE_FLORISTS
  public final static String FLORIST_NAME = "FLORIST_NAME";
  public final static String ADDRESS = "ADDRESS";  
  public final static String CITY_STATE_NUMBER = "CITY_STATE_NUMBER";
  public final static String RECORD_TYPE = "RECORD_TYPE";
  public final static String MERCURY_FLAG = "MERCURY_FLAG";
  public final static String FLORIST_WEIGHT = "FLORIST_WEIGHT";
  public final static String SUPER_FLORIST_FLAG = "SUPER_FLORIST_FLAG";
  public final static String SUNDAY_DELIVERY_FLAG = "SUNDAY_DELIVERY_FLAG";
  public final static String DELIVERY_CITY = "DELIVERY_CITY";
  public final static String DELIVERY_STATE = "DELIVERY_STATE";
  public final static String OWNERS_NAME = "OWNERS_NAME";
  public final static String MINIMUM_ORDER_AMOUNT = "MINIMUM_ORDER_AMOUNT";
  public final static String TERRITORY = "TERRITORY";
  public final static String FAX_NUMBER = "FAX_NUMBER";
  public final static String EMAIL_ADDRESS = "EMAIL_ADDRESS";
  public final static String INITIAL_WEIGHT = "INITIAL_WEIGHT";
  public final static String ADJUSTED_WEIGHT = "ADJUSTED_WEIGHT";
  public final static String ADJUSTED_WEIGHT_START_DATE = "ADJUSTED_WEIGHT_START_DATE";
  public final static String ADJUSTED_WEIGHT_END_DATE = "ADJUSTED_WEIGHT_END_DATE";
  public final static String ADJUSTED_WEIGHT_PERMENANT_FLAG = "ADJUSTED_WEIGHT_PERMENANT_FLAG";
  public final static String TOP_LEVEL_FLORIST_ID = "TOP_LEVEL_FLORIST_ID";
  public final static String PARENT_FLORIST_ID = "PARENT_FLORIST_ID";
  public final static String ALT_PHONE_NUMBER = "ALT_PHONE_NUMBER";
  public final static String ORDERS_SENT = "ORDERS_SENT";
  public final static String SENDING_RANK = "SENDING_RANK";
  public final static String SUNDAY_DELIVERY_BLOCK_ST_DT = "SUNDAY_DELIVERY_BLOCK_ST_DT";
  public final static String SUNDAY_DELIVERY_BLOCK_END_DT = "SUNDAY_DELIVERY_BLOCK_END_DT";
  public final static String LOCKED_BY = "LOCKED_BY";
  public final static String LOCKED_DATE = "LOCKED_DATE";
  public final static String INTERNAL_LINK_NUMBER = "INTERNAL_LINK_NUMBER";
  public final static String VENDOR_FLAG = "VENDOR_FLAG";
  public final static String SUSPEND_OVERRIDE_FLAG = "SUSPEND_OVERRIDE_FLAG";
   
  //SP_GET_VENDOR
  public final static String VENDOR_ID = "VENDOR_ID";
  public final static String VENDOR_NAME = "VENDOR_NAME";
  public final static String VENDOR_TYPE = "VENDOR_TYPE";
 
  //SP_GET_ORDER_BY_GUID
  public final static String MASTER_ORDER_NUMBER = "MASTER_ORDER_NUMBER";
  public final static String MEMBERSHIP_ID = "MEMBERSHIP_ID";
  public final static String ORIGIN_ID = "ORIGIN_ID";
  public final static String ORDER_DATE = "ORDER_DATE";
  public final static String ORDER_TOTAL = "ORDER_TOTAL";
  public final static String PRODUCT_TOTAL = "PRODUCT_TOTAL";
  public final static String ADD_ON_TOTAL = "ADD_ON_TOTAL";
  public final static String SERVICE_FEE_TOTAL = "SERVICE_FEE_TOTAL";
  public final static String SHIPPING_FEE_TOTAL = "SHIPPING_FEE_TOTAL";
  public final static String DISCOUNT_TOTAL = "DISCOUNT_TOTAL";
  public final static String TAX_TOTAL = "TAX_TOTAL";
  public final static String LOSS_PREVENTION_INDICATOR = "LOSS_PREVENTION_INDICATOR";
  
  //SP_LOCK_ORDER and SP_UNLOCK_ORDER
  public final static String ENTITY_TYPE = "ENTITY_TYPE";
  public final static String ENTITY_TYPE_VL = "ORDER_DETAILS";
  public final static String ENTITY_ID = "ENTITY_ID";
  public final static String SESSION_ID = "SESSION_ID";
  public final static String CRS_ID = "CSR_ID";
  public final static String CRS_ID_VL = "SYS";
  public final static String ORDER_LEVEL = "ORDER_LEVEL";
  public final static String LOCK_OBTAINED = "OUT_LOCK_OBTAINED";
  public static final String LOCK_CSR = "OUT_LOCKED_CSR_ID";
  
  //SP_GET_ORDER_BILLS
  public final static String TOTAL_TYPE = "TOTAL_TYPE";
  public final static String PRODUCT_AMOUNT = "PRODUCT_AMOUNT";
  public final static String ADD_ON_AMOUNT = "ADD_ON_AMOUNT";
  public final static String SERV_SHIP_FEE = "SERV_SHIP_FEE";
  public final static String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
  public final static String TAX = "TAX";
  public final static String BILL_TOTAL = "BILL_TOTAL";
  
  /* used for logic processing */
  public final static String VL_YES = "Y";
  public final static String VL_NO = "N";
  public final static String VL_ON = "ON";
  public final static String VL_OFF = "OFF";
  public final static boolean FALSE = false;
  public final static boolean TRUE = true;
  public final static String VL_VENDOR = "VENDOR";
  public final static String VL_FLORIST = "FLORIST";
  public final static String VL_ERROR = "ERROR";
  public final static String VL_NO_VENDOR = "00000";
  public final static String UPDATED_BY_VL = "OP";
  public final static int VL_EMAIL = 1;
  public final static int VL_FTP = 2;
  public final static int VL_VENUS = 3;  
  
  /* custom parameters returned from database procedure */
  public static final String OUT_STATUS_PARAM = "OUT_STATUS";
  public static final String OUT_MESSAGE_PARAM = "OUT_MESSAGE";
  
  /* config:  file names */
  public static final String PROPERTY_FILE = "order_config.xml";
  
  /* database:  data source name */
  public static final String DATASOURCE_NAME = "CLEANDS";
  
  /* logger:  category name */
  public static final String LOGGER_CATEGORY_ORDERDAO = "com.ftd.op.order.dao.OrderDAO";
  public static final String LOGGER_CATEGORY_ORDERSERVICE = "com.ftd.op.order.service.OrderService";
  public static final String LOGGER_CATEGORY_QUEUESERVICE = "com.ftd.op.order.QueueService";
  public static final String LOGGER_CATEGORY_ORDERFACADE = "com.ftd.op.order.OrderFacade";  
  public static final String LOGGER_CATEGORY_ORDERAPIBEAN = "com.ftd.op.order.api.OrderAPIBean";
}