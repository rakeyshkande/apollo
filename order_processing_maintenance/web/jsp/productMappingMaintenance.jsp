<%@page import="java.util.HashMap"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>FTD - Phoenix Product Mapping Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css" />
<link rel="stylesheet" type="text/css" href="css/tooltip.css" />
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/copyright.js"></script>
<script type="text/javascript" src="js/clock.js"></script>

<script language="javascript">	

function stopRKey(evt)
{ 	
	var evt = (evt) ? evt : ((event) ? event : null); 	
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
	if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 

} 
document.onkeypress = stopRKey; 

function ajax_get_price_points()
{	
	
	var search_key_val = document.getElementsByName('floralSKU')[0].value;	
	var securitytoken = '<%= request.getParameter("securitytoken") %>';
	var context = '<%= request.getParameter("context") %>';
	var adminAction = '<%= request.getParameter("adminAction") %>';
	
	if (search_key_val == null || search_key_val == "") {
		alert("Product SKU/Add-On must not be blank");
		return false;
	}	
	
	document.forms[0].action = "/orderprocessingmaint/ProductMappingMaintenanceAction.do?action_type=update&prod_type=new&floral_product_sku=" + search_key_val
								+"&securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction+"#ADDEDITPRODUCT";
	document.forms[0].submit();
}
function submitTheForm(action,securitytoken,context,adminAction)
{	
	if(action === "save" || action === "clear" )
	{
		document.forms[0].action = "/orderprocessingmaint/ProductMappingMaintenanceAction.do?action_type="+action+"&securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction+"#ADDEDITPRODUCT";
	}	
	else
    {
		document.forms[0].action = "/orderprocessingmaint/ProductMappingMaintenanceAction.do?action_type="+action+"&securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction;
    }
	document.forms[0].submit();
}
function submitUpdate(action,securitytoken,context,adminAction,product_sku)
{
	var floral_sku=product_sku;		
	if(action === "update" )
	{	
		document.forms[0].action = "/orderprocessingmaint/ProductMappingMaintenanceAction.do?action_type="+action+"&securitytoken="+securitytoken+"&context="+context+"&floral_product_sku="+floral_sku+"&adminAction="+adminAction+"#ADDEDITPRODUCT";
	}
	else
	{
		document.forms[0].action = "/orderprocessingmaint/ProductMappingMaintenanceAction.do?action_type="+action+"&securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction+"&floral_product_sku="+floral_sku;
	}
	document.forms[0].submit();
}

function remove_product_mapping()
{
	var removeTxt = displayConfirmError("Are you sure you want to remove?");
	if(removeTxt)
		return true;
	else
		return false;	
}

function doPhoenixProcessing(action,securitytoken,context,adminAction)
{
	document.forms[0].action = "/orderprocessingmaint/ProductMappingMaintenanceAction.do?action_type="+action+"&securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction;
	document.forms[0].submit();
	}
</script>
</head>

<body>
	<div align="center">
		<table width="98%" border="0" cellspacing="2" cellpadding="0">
			<tr>
				<td width="20%" align="left"
					style="float: left; vertical-align: middle; padding-top: .4em;"><img
					src="images/wwwftdcom_131x32.gif" alt="ftd.com" border="0"
					height="32">
				</TD>
				<td width="60%" align="center" colspan="1" class="Header">
					Phoenix Product Mapping Maintenance</td>
				<td id="time" align="right" valign="bottom" class="Label"
					width="30%" height="30"><script type="text/javascript">
						startClock();
					</script>
				</td>
			</tr>
			<tr>
				<td colspan="3"><HR>
				</td>
			</tr>
		</table>
	</div>

	<br />
	<br />
	
	<A NAME="PRODUCTLISTING"></A>	
	<% Integer tabindex = 0; %>
		
	<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
		<tr>
			<td>
				<% 
				if (request.getAttribute("phoenixStatus").equals("Hold")) 
				{
				%>
				<br/>	
				<span><font color="Red">Bulk Phoenix process is already running. Please wait.</font></span>
				<% }
				else if(request.getAttribute("phoenixStatus").equals("Processing")){ %>
				<br/>
				<span><font color="green">Bulk Phoenix processing has started. Process might take several minutes to complete.</font></span>
				<% }
				else if(request.getAttribute("phoenixStatus").equals("Error")){ %>
				<br/>
				<span><font color="Red">Error occured while Bulk Phoenix processing. Please try again later.</font></span>
				<% } %>
				<br/>
				<table align="center" width="100%" bgcolor="#006699" cellpadding="2" cellspacing="2">
					<tr>
						<td width="100%" align="center"	style="font-size: 16px; font-style: bold; color:white;">Product Mapping List</td>
					</tr>
				</table>
				<br/>
				
				<!-- System Configuration -->
				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" bordercolor="#006699">
					<tr>
					<td width="90%" colspan="4" align="center"> 
					<table width="90%" border="0" cellpadding="1" cellspacing="1" bordercolor="#006699">
						<tr>
						<td width="10%" align="center" bgcolor="#006699" style="font-size: 14px; font-style: bold; color:white;">Edit/Remove</td>
						<td width="20%" align="center" bgcolor="#006699" style="font-size: 14px; font-style: bold; color:white;">Floral Product/Add On</td>
						<td width="20%" align="center" bgcolor="#006699" style="font-size: 14px; font-style: bold; color:white;">Good/Better/Best</td>
						<td width="40%" align="center" bgcolor="#006699" style="font-size: 14px; font-style: bold; color:white;">Vendor Product SKU(s)</td>
						</tr>
					</table>
					<br/>
					</td>
					</tr>
				
					<tr>
					 	<td width="90%" colspan="4" align="center"> 
							<div id="configListing" align="center" style="overflow-y:auto; height:300px;">
									<table width="89%" border="1" cellpadding="0" cellspacing="0" bordercolor="#006699">
									<logic:iterate id="prod" name="allProductMappings">									
										<tr>
										<!--  display edit/remove link -->
										<td width="10%" align="center">										
											
											<a href="javascript:submitUpdate('update','<%= request.getParameter("securitytoken") %>','<%= request.getParameter("context") %>','<%= request.getParameter("adminAction") %>','<bean:write name="prod"	property="key"/>');" >
											Edit</a>											
											<a href="javascript:submitUpdate('remove','<%= request.getParameter("securitytoken") %>','<%= request.getParameter("context") %>','<%= request.getParameter("adminAction") %>','<bean:write name="prod"	property="key"/>');" 
											 onclick="javascript:return remove_product_mapping()">
											Remove</a> 
											
										</td>
										<!-- display floral product -->
										<td width="20%" align="center"><bean:write name="prod"	property="key"/> </td>
										<td width ="20%" align="center">
											<logic:iterate name="prod" id="nextVal"	property="value">
												<table><tr><td>
													<bean:write name="nextVal" property="productPricePointName" />
												</td></tr></table>
											</logic:iterate>
										</td>
										<td width ="40%" align="center">
											<logic:iterate name="prod" id="nextVal" property="value">
												<table><tr><td>
													<bean:write name="nextVal" property="vendorSKUs" />
													</td></tr></table>
											</logic:iterate>
										</td>
									    </tr>										
									</logic:iterate>	
									</table>							
							</div>
						</td>
					</tr>
				</table> 
				
				<br/> <br/> <br/> <br/>
				
				<A NAME="ADDEDITPRODUCT"></A>	
									
				<html:form method="post" action="ProductMappingMaintenanceAction.do">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#006699">
					<tr>
						<td width="80%" align="center"	style="font-size: 16px; font-style: bold; color:white;">Add/Edit Product Mapping</td>
					</tr>
				</table>

				<br/>
				<br/>
				<table width="80%" border="0" align="center">				
					<logic:present name="errorMessage">
					<logic:iterate id="errMsgList" name="errorMessage">   
					<tr>
						<td width="80%" align="left"	style="font-size: 14px; font-style: bold; color:red;">
        					<bean:write name="errMsgList"/>   
     					 </td>  
     				</tr> 
    				</logic:iterate>   	
    				</logic:present>					
				</table>
				<!-- On load of the Page do below -->
				<% 
				if (request.getAttribute("action_type").equals("load")) 
				{
				%>				

				<table width="80%" border="1" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
					<tr>
						<td width="15%" align ="center" bgcolor="#006699" style="font-size: 14px; font-style: bold; color:white;">Floral Product/ Add On</td>
						<td width="15%" align ="center" bgcolor="#006699"  style="font-size: 14px; font-style: bold; color:white;">Good/Better/Best</td>
						<td width="40%" align ="center" bgcolor="#006699"  style="font-size: 14px; font-style: bold; color:white;">Vendor Product SKU(s)</td>
					</tr>
					<tr>
						<td colspan="5" width="100%" align="center">
							<table width="100%" cellpadding="1" cellspacing="1">
								<tr>
									<td width="15%" align="center">
									<html:text	name="ProductMappingMaintenanceForm" property="floralSKU"  size="10"
											maxlength="20"  onblur="javascript:ajax_get_price_points();"></html:text>
									 <font color="green"><b> Use Tab key </b> </font></td>									 
									<td></td>
									<td></td>
								</tr>	
							</table></td>
					</tr>
				</table>
				<% }
				else {	
				Boolean disabled = (Boolean) request.getAttribute("disabled"); 				
				Boolean vendorSkusDisabled = new Boolean("false");					
				%>
				<table width="80%" border="1" align="center" cellpadding="1"
						cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="15%" align="center" bgcolor="#006699"
								style="font-size: 14px; font-style: bold; color: white;">Floral
								Product/ Add On</td>
							<td width="15%" align="center" bgcolor="#006699"
								style="font-size: 14px; font-style: bold; color: white;">Good/Better/Best</td>
							<td width="40%" align="center" bgcolor="#006699"
								style="font-size: 14px; font-style: bold; color: white;">Vendor
								Product SKU(s)</td>
						</tr>
						<tr>
							<td colspan="3" width="100%" align="center">
								<table width="100%" cellpadding="1" cellspacing="1">

									<tr>
										<td width="20%" align="center">
												<html:text
													name="ProductMappingMaintenanceForm" property="floralSKU"  size="10"
													maxlength="20" disabled="<%=disabled.booleanValue()%>">
												</html:text>						
										</td>
										<html:hidden property="floralSKU" name="ProductMappingMaintenanceForm"></html:hidden>
										
										<td width="80%" align="center">
										<logic:present name="ProductMappingMaintenanceForm" property="vendorProdSkuList">
										
										<logic:iterate id="prodMapping"
											name="ProductMappingMaintenanceForm"
											property="vendorProdSkuList">
										<% tabindex = tabindex++; %>  	
										<logic:equal name="prodMapping" property="productPricePoint" value="">
										<% vendorSkusDisabled= new Boolean("true"); %>
										</logic:equal>
										<% if(!vendorSkusDisabled){ %>
											<html:hidden property="productPricePointName" name="prodMapping" indexed="true"></html:hidden>	
										  	<html:hidden property="productPricePoint" name="prodMapping" indexed="true"></html:hidden>	
										  	<html:hidden property="isProductAddon" name="prodMapping" indexed="true"></html:hidden>
										<% } %>
										  	
										<table width="100%" align="center">
											<tr>
												<td width="20%" align="center">
													<table>
														<tr>
															<td><html:text property="pricePointNameAndPrice"
																	name="prodMapping" size="20" maxlength="40"
																	disabled="<%=disabled.booleanValue()%>" indexed="true"></html:text>								
															</td>
														</tr>
													</table></td>
												<td width="60%" align="center">
													<table>
														<tr>
															<td><html:text property="vendorSKUs"
																	name="prodMapping" size="56" maxlength="54" tabindex="<%= tabindex.toString()%>"  disabled="<%= vendorSkusDisabled.booleanValue()%>"
																	indexed="true" ></html:text></td>
														</tr>
													</table></td>
											</tr>
										</table>
										<% vendorSkusDisabled= new Boolean("false"); %>
										</logic:iterate>
										</logic:present>
										</td>
									</tr>

								</table></td>							
						</tr>										
					</table>
					<% } %>								
					<br/>
					<table width="25%" border="0" cellpadding="2" cellspacing="2" align="right">
						<tr>
							<td align="center">	         
							<button class="BlueButton" name="saveButton" id="saveButton" tabindex="<%=tabindex%>"  accesskey="S" 
								onclick="javascript:submitTheForm('save','<%= request.getParameter("securitytoken") %>','<%= request.getParameter("context") %>','<%= request.getParameter("adminAction") %>');" style="margin-right: 12px">Save</button>
							<button class="BlueButton" name="clearButton" id="clearButton" tabindex="<%= tabindex.toString()%>" accesskey="C"
								onclick="javascript:submitTheForm('clear','<%= request.getParameter("securitytoken") %>','<%= request.getParameter("context") %>','<%= request.getParameter("adminAction") %>');" style="margin-right: 12px">Clear</button>
							</td>	
						</tr>
					</table>
			</html:form>
			<br/> <br/>
			</td>
		</tr>

	</table>
	<br/> <br/>
	
	<!-- Display of syntax -->
	<table width="75%" border="0" align="center" cellpadding="0" cellspacing="1" >
						<tr>
							<td width="15%" align="left" style="font-size: 14px; font-style: bold;  color: green;">
							Floral Add-On SKUs						
							</td>							
							<td width="15%" align="center" style="font-size: 14x; font-style: bold; color: green;"></td>
							<td width="40%" align="right" style="font-size: 14px; font-style: bold; color: green;">
							Vendor Product SKU syntax: VSKU1 VSKU2 VSKU3 VSKU4 VSKU5</td>
						</tr>
						<tr>
						<td width="15%" align="left" style="font-size: 14px; font-style: bold; color: green;">BKJ=Bear,CKJ=Choclate</td>
						</tr>
	</table>
	<br/> <br/>

	<!-- Action Buttons -->
	<table width="98%" border="0" align="center" cellpadding="2"
		cellspacing="2">
		<tr>
			<td align="right">
				
			</td>
			<td align="right">
				<button class="BlueButton" name="reprocessPhoenix" id="reprocessPhoenix"   
								onclick="javascript:doPhoenixProcessing('doPhoenixProcessing','<%= request.getParameter("securitytoken") %>','<%= request.getParameter("context") %>','<%= request.getParameter("adminAction") %>');">Phoenix Existing Queue Items</button>
				&nbsp;
				<button class="BlueButton" name="exitButton" id="exitButton" tabindex="<%= tabindex.toString()%>" accesskey="M"
					onclick="javascript:submitTheForm('exit','<%= request.getParameter("securitytoken") %>','<%= request.getParameter("context") %>','<%= request.getParameter("adminAction") %>');">Main Menu</button></td>
		</tr>
	</table>
	



	<table width="98%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="disclaimer"></td>
			<script>
				showCopyright();
			</script>

		</tr>
	</table>

</body>
</html>

