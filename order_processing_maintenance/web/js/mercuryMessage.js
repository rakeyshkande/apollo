//TODOs: fields || stub || modal || hardcoded || commented

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
	return false;
}


/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8)
   {
	  var formElement = false;

	  for(i = 0; i < document.forms[0].elements.length; i++)
	  {
		 if(document.forms[0].elements[i].name == document.activeElement.name)
		 {
			formElement = true;
			break;
		 }
	  }

	  if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password"))
	  {
		 window.event.cancelBubble = true;
		 window.event.returnValue = false;
		 return false;
	  }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116)
   {
	  window.event.keyCode = 0;
	  window.event.returnValue = false;
	  return false;
   }

   // Alt + Left Arrow
   if(window.event.altKey && (window.event.keyCode == 37))
   {
		window.event.returnValue = false;
		return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey && (window.event.keyCode == 39))
   {
		window.event.returnValue = false;
		return false;
   }
}

/*
	Catch all numeric keypresses and send them to the search box.
*/
function evaluateKeyPress()
{
	var a;
	var keypress = window.event.keyCode;

	if (keypress == 48)
		a = 0;
	if (keypress == 49)
		a = 1;
	if (keypress == 50)
		a = 2;
	if (keypress == 51)
		a = 3;
	if (keypress == 52)
		a = 4;
	if (keypress == 53)
		a = 5;
	if (keypress == 54)
		a = 6;
	if (keypress == 55)
		a = 7;
	if (keypress == 56)
		a = 8;
	if (keypress == 57)
		a = 9;

	if (a >= 0 && a <= 9)
	{
		document.getElementById("numberEntry").focus();
	}
}

/*
Remove most of the elements in the page header from the tabbing sequence.
*/
function untabHeader()
{
	var printerIcon = document.getElementById("printer");
	if(printerIcon != null)
	{
		printerIcon.parentNode.tabIndex = -1;
	}

	var viewedBy = document.getElementById("currentCSRs");
	if(viewedBy != null)
	{
		viewedBy.tabIndex = -1;
	}

	var csrIframe = document.getElementById("CSRFrame");
	if(csrIframe != null)
	{
		csrIframe.tabIndex = -1;
	}

	var backButton = document.getElementById("backButtonHeader");
	if(backButton != null)
	{
		backButton.tabIndex = -1;
	}
}


/*
   This is a helper function to return the two security parameters
   required by the security framework.

   areOnlyParams -   is used to change the url string (? vs. &) depending if these
					 are the only parameters in the url
*/
function getSecurityParams(areOnlyParams)
{
	return ((areOnlyParams) ? "?" : "&")
			+ "securitytoken=" + document.getElementById("securitytoken").value
			+ "&context=" + document.getElementById("context").value;
}


/*
This function is executed when the csr
uses the search box. If the csr hits the 'enter' key
the associated td with the same id will be located and the customer
id is grabbed from the hidden field and a search is performed.
*/
function doEntryAction()
{
	if(window.event.keyCode != 13)
	{
		return false;
	}

	if(isWhitespace(event.srcElement.value))
	{
		window.event.returnValue = false;
		return false;
	}


	var messageNumber = stripZeros(stripWhitespace(event.srcElement.value));
	if(isInteger(messageNumber))
	{
	   openMessage(messageNumber);
	}
	else
	{
		alert(messageNumber + " isn't a valid message number.");
		window.event.returnValue = false;
		return false;
	}
}

/*
Using the specified URL submit the form
*/
function performAction(url)
{
//	showWaitMessage("content", "wait", "Searching");
	document.forms[0].action = url;
	document.forms[0].submit();
}


/*
Handler for the printer icon in the header, as created by header.xsl.
*/
function printIt()
{
	window.print();
}


/*
Handlers for the mercMessButtons.xsl footer buttons.
*/
function openRecipientOrder()
{
	var url = "customerOrderSearch.do?action=customer_account_search"
				+ "&order_number=" + document.getElementById("msg_external_order_number").value
				+ "&order_guid=" + document.getElementById("msg_guid").value
				+ "&customer_id=" + document.getElementById("msg_customer_id").value;

	performAction(url);
}

function openHistory()
{
	var url = "loadHistory.do?history_type=Order&page_position=1"
				+ "&order_detail_id=" + document.getElementById("order_detail_id").value;

	performAction(url);
}

function openOrderComments()
{
	var url = "loadComments.do"
				+ "?page_position=1"
				+ "&comment_type=Order"
				+ "&order_detail_id=" + document.getElementById("order_detail_id").value
				+ "&order_guid="
				+ document.getElementById("msg_guid").value
				+ "&customer_id="
				+ document.getElementById("msg_customer_id").value;

	performAction(url);
}

function openEmail()
{
	if(document.getElementById("msg_email_indicator").value == "true")
	{
		var url = "loadSendMessage.do?message_type=Email&action_type=load_titles&external_order_number="
					+ document.getElementById("msg_external_order_number").value;
	
		performAction(url);
	}
	else
	{
		alert("There is no email address on file for this customer.");
	}
}

function openLetters()
{
	var url = "loadSendMessage.do?message_type=Letter&action_type=load_titles&external_order_number="
				+ document.getElementById("msg_external_order_number").value;

	performAction(url);
}

function openRefund()
{
	var url = "loadRefunds.do?order_detail_id=";
				+ document.getElementById("order_detail_id").value;
				
	performAction(url);
}

function openFloristInquiry(url)
{
	url += "action=loadFloristSearch"
			+ "&adminAction=distributionFulfillment"
			+ "&applicationcontext="
			+ document.getElementById("applicationcontext").value
			+ getSecurityParams(false);

 	var modal_dim = 'dialogWidth=800px,dialogHeight=400px,dialogLeft=0px,dialogTop=90px';
	modal_dim += 'location=0,status=0,menubar=0,scrollbars=1,resizable=0';

    window.showModalDialog(url, "", modal_dim);
}

/*
Handler for the communicationScreen.xsl and floristLookup.xsl functionality of clicking on a
florist's number in the table of Mercury messages, being taken to that florist's information
page.
*/
function openFlorist(floristCode, url)
{
	url += "action=loadFloristSearch"
			+ "&adminAction=distributionFulfillment"
			+ "&mNum=" + floristCode
			+ "&applicationcontext="
			+ document.getElementById("applicationcontext").value
			+ getSecurityParams(false);

	var modal_dim = 'dialogWidth=800px,dialogHeight=400px,dialogLeft=0px,dialogTop=90px';
	modal_dim +='location=0,status=0,menubar=0,scrollbars=1,resizable=0';

	window.showModalDialog(url, "", modal_dim);
}

/*
Handler for the "(S)earch" button, as created by mercMessButtons.xsl.
*/
function doSearchAction()
{//TODO commented fields
	var url = "customerOrderSearch.do?action=load_page";

	performAction(url);
}

/*
Handler for the "(B)ack" button, as created by header.xsl and mercMessButtons.xsl.
*/
function doBackAction()
{
	var url = "backButton.do?action=back_button" + getSecurityParams(false);

	performAction(url);
}

/*
Handler for the "(M)ain Menu" button, as created by mercMessButtons.xsl.
*/
function doMainMenuAction()
{
	var url = '';

	//Modal_URL
	var modalUrl = "confirm.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = 'Have you completed your work on this order?';

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:no; scroll:no';

	//get a true/false value from the modal dialog.
	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

	if(ret)
	{
		url = "mainMenu.do?work_complete=y" + getSecurityParams(false);
	}
	else
	{
		url = "mainMenu.do?work_complete=n" + getSecurityParams(false);
	}

	performAction(url);
}


/*
Handler for the communicationScreen.xsl "(Q)Delete" button in the Mercury
message table. This button creates a popup window listing the messages in the queue, as
created by the qDelete.xsl.
*/
function openQDelete()
{
	/*
	The action needs a map of all the messages on the communication screen. The map will
	be from the messages' line-numbers to their mercury ids. All the message hidden input
	fields are named "n" where n is that message's line number. Their values are the
	concatenation of their mercury ids and their message types(mercury, venus, email or
	letter). We use an appropriate regular expression to select the message hidden fields,
	split off just the message id portions of those fields'	values, and use them to make a
	list of key-value pairs which we append to our url.
  */

	var messageMap = "";

	var formFields = document.getElementById("communicationScreenForm").elements;
	var messageNameRegex = /^\d+$/;
	var messageId;

	for(var i = 0; i < formFields.length; i++)
	{
		if(messageNameRegex.test(formFields[i].name))
		{
			messageId = formFields[i].value.split("|");
			messageMap += "&queue_ids=" + messageId[0];
		}
	}

	var url = "displayQDelete.do?order_detail_id=" + document.getElementById("order_detail_id").value
              + messageMap
			  + getSecurityParams(false);

	var modal_dim = "dialogWidth:570px; dialogHeight:245px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no;";
	var shouldReload = window.showModalDialog(url, '', modal_dim);

	if(shouldReload == "Y")
	{
		location.reload();
	}
}

/*
Handler for the communicationScreen.xsl "Florist (D)ashboard" button in the Mercury
message table. This button creates a popup window providing florist data relating to
the order, as created by the floristDashboard.xsl.
*/
function openDashboard()
{
	var url = "displayFloristDashboard.do?action_type=florist_dashboard"
				+ "&order_detail_id=" + document.getElementById("order_detail_id").value
				+ getSecurityParams(false);

	var modal_dim = "dialogWidth:570px; dialogHeight:245px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";

	window.showModalDialog(url, '', modal_dim);
}


/*
Handler for the message type links in communicationScreen.xsl. This function accepts an
integer parameter representing the message number whose details it will attempt to bring up.
*/
function openMessage(recordNumber)
{
	var hiddenMessageField = document.getElementById(recordNumber.toString());
	if(hiddenMessageField == null)
	{
		alert("There is no message " + recordNumber + " on this page.");
		window.event.returnValue = false;
		return false;
	}
	else
	{
		document.forms[0].message_info.value = hiddenMessageField.value;
		document.forms[0].message_line_number.value = recordNumber;

		document.forms[0].submit();
	}
}



/*
Handler for the "I would like to:" drop down box. This box contains a number of options
for determining precisely how a message should be forwarded. It appears in
viewMessage.xsl, viewPriceMessage.xsl, and viewFTDMessage.xsl.
*/
function messageForward()
{
	var selectBox = document.getElementById("would_like_to");
	var action = selectBox.options[selectBox.selectedIndex].value;

	if(action == "pay_original_send_new")
	{
		var url = '';

		//Modal_URL
		var modalUrl = "confirm.html";

		//Modal_Arguments
		var modalArguments = new Object();
		modalArguments.modalText = "Do you want to cancel the original order?";

		//Modal_Features
		var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
		modalFeatures += 'resizable:no; scroll:no';

		//get a true/false value from the modal dialog.
		var choseYes = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

		if(choseYes)
		{
			alert("You must select 'Cancel Original Order & Send New FTD.'");
			return false;
		}
	}

	var url = "prepareMessage.do?order_detail_id="
				+ document.getElementById("order_detail_id").value
				+ "&msg_message_order_number="
				+ document.getElementById("msg_message_order_number").value
				+ "&msg_message_type="
				+ document.getElementById("msg_message_type").value
				+ "&action_type="
				+ action
				+ getSecurityParams(false);

	MessageForwardFrame.location.href = url;
}

function resetFormActionAndSubmit(forwardAction, subsequentAction, orderType, orderStatus)
{
	//set parent window's forward action field.
	if(document.forms[0] && forwardAction != "")
	{
		document.forms[0].action = forwardAction;
	}

	//set parent window's subsequent action field, if it has one.
	if(document.forms[0].subsequent_action && subsequentAction != "")
	{
		document.forms[0].subsequent_action.value = subsequentAction;
	}

	//set parent window's msg_order_type field, if it has one.
	if(document.forms[0].msg_order_type && orderType != "")
	{
		document.forms[0].msg_order_type.value = orderType;
	}

	//set parent window's msg_order_status field, if it has one.
	if(document.forms[0].msg_order_status && orderStatus != "")
	{
		document.forms[0].msg_order_status.value = orderStatus;
	}

	document.forms[0].submit();
}

/*
Handler for the "(Q)Delete" button in the qDelete.xsl.
*/
function doQDeleteAction()
{
	if(qDeleteForm.messageCheckboxes != null && qDeleteForm.messageCheckboxes.length > 0)
	{
		var toBeDeleted = "";
		for(var i = 0; i < qDeleteForm.messageCheckboxes.length; i++)
		{
			if(qDeleteForm.messageCheckboxes[i].checked)
			{
				toBeDeleted += qDeleteForm.messageCheckboxes[i].value + ',';
			}
		}

		if(toBeDeleted != "")
		{
			toBeDeleted = toBeDeleted.slice(0, -1);

			var url = "deleteQueue.do?action_type=delete"
						+ "&order_detail_id=" + document.getElementById("order_detail_id").value
						+ "&queue_ids=" + toBeDeleted;


			document.forms[0].target = window.name;
			performAction(url);
		}
		else
		{
			alert("You must select at least one message to delete.");
		}
	}
}

/*
Handler for the "(C)lose" buttons in the qDelete screen. The qDelete modal popup window
must be closed, and the parent window, the Communication screen, must be updated, since
it's possible that deletions made on the qDelete screen have caused some of the messages
on the Communications screen to cease to exist.
*/
function closeQDelete()
{
	window.returnValue = shouldReload;

	window.close();
}

/*
Handler for the "QDelete(A)ll" button in the qDelete.xsl. This action checks all the boxes
in the table, and then invokes the doQDeleteAction() function to have them all deleted.
*/
function checkAll()
{
	if(qDeleteForm.messageCheckboxes != null)
	{
		//Check all the messages in the table
		for(var i = 0; i < qDeleteForm.messageCheckboxes.length; i++)
		{
			if(!qDeleteForm.messageCheckboxes[i].checked)
			{
				qDeleteForm.messageCheckboxes[i].checked = true;
			}
		}

		doQDeleteAction();
	}
}



/*
Validation Functions which are specific to Mercury Messaging.
*/
function isFloristCode(str)
{
	var floristCodeRegex = /^\s*\d{2}-\d{4}AA\s*$/;
	return floristCodeRegex.test(str);
}

function checkFloristCode(elementName, errorFieldName)
{
	var code = document.getElementById(elementName).value;

	if(isWhitespace(code))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	if(!isFloristCode(code))
	{
		writeError(errorFieldName, "Invalid format, must be 99-9999AA");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientName(elementName, errorFieldName)
{
	var rName = document.getElementById(elementName).value;

	if(isWhitespace(rName))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientStreet(elementName, errorFieldName)
{
	var street = document.getElementById(elementName).value;

	if(isWhitespace(street))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientCity(elementName, errorFieldName)
{
	var city = document.getElementById(elementName).value;

	if(isWhitespace(city))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientState(elementName, errorFieldName)
{
	var state = document.getElementById(elementName).value;

	if(isWhitespace(state))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientZip(elementName, errorFieldName)
{
	var zip = document.getElementById(elementName).value;

	if(isWhitespace(zip))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function isPhone(str)
{
	var phoneRegex = /^\s*\d{3}\/\d{3}-\d{4}\s*$/;
	return phoneRegex.test(str);
}

function checkRecipientPhone(elementName, errorFieldName)
{
	var phone = document.getElementById(elementName).value;

	if(isWhitespace(phone))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	if(!isPhone(phone))
	{
		writeError(errorFieldName, "Format: ###/###-#####");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkOrderDeliveryDate(elementName, errorFieldName)
{
	var orderDateString = document.getElementById(elementName).value.split(" ");
	var month = orderDateString[0];
	var date = stripZeros(orderDateString[1]);

	//Check that they entered a date.
	if(isWhitespace(date))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	//Check that date field is one or two digits.
	var dateRegex = /^\s*\d{1,2}\s*$/;
	if(!dateRegex.test(date))
	{
		writeError(errorFieldName, "Format: ##");
		return false;
	}

	//Check that date is within legal range for any date
	var dateNum = parseInt(date);
	if(dateNum > 31 || dateNum < 1)
	{
		writeError(errorFieldName, "Invalid Date");
		return false;
	}


	/*
	Create a date object for the order based on the form values. The time is set to the
	maximum	value in case the order is for same-day delivery. This makes the delivery date
	no later than whenever this order was processed on that day. Thus, it's possible
	to create an order whose delivery date is now, or only seconds away; the issue was
	deemed unimportant for now.
	*/
	var today = new Date();

	var orderDate = new Date(date + " " + month + " " + today.getFullYear());
	orderDate.setUTCHours(23, 59, 59, 999);

	/*
	There is no year associated with the order. Assuming that we don't get orders whose
	dates have expired implies that an order whose date is before today is for next year.
	*/
	if(orderDate.valueOf() < today.valueOf())
	{
		orderDate.setFullYear(orderDate.getFullYear() + 1);
	}

	//Convert the date to a string so we can parse it.
	var myDate_string = orderDate.toUTCString();

	/*
	Split the string at every space and put the values into an array so,
	using the previous example, the first element in the array is "Wed", the
	second element is "Jan", the third element is "1", etc.
	*/
	var myDate_array = myDate_string.split(" ");

	/*
	If we entered "Feb 31, 1975" in the form, the "new Date()" function
	converts the value to "Mar 3, 1975". Therefore, we compare the month
	in the array with the month we entered into the form. If they match,
	then the date is valid, otherwise, the date is NOT valid.
	*/
	if(myDate_array[2].toUpperCase() != month)
	{
		writeError(errorFieldName, "Invalid Date");
		return false;
	}

	//Check that order date <= 120 in the future.
	//Compute the deadline date: today + 120 days.
	var deadline = new Date();
	deadline.setDate(today.getDate() + 120);
	deadline.setUTCHours(23, 59, 59, 999);

	if(orderDate.valueOf() > deadline.valueOf())
	{
		var errorMessage = "Delivery must be no later than " + deadline.toDateString();
		writeError(errorFieldName, errorMessage);
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkFirstChoice(elementName, errorFieldName)
{
	var first = document.getElementById(elementName).value;

	if(isWhitespace(first))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkSecondChoice(elementName, errorFieldName)
{
	var second = document.getElementById(elementName).value;

	if(isWhitespace(second))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function isPrice(str)
{
	var priceRegex = /^\s*\d{0,3}\.\d{2}\s*$/;
	return priceRegex.test(str);
}

function checkPrice(elementName, errorFieldName)
{
	var price = document.getElementById(elementName).value;

	if(isWhitespace(price))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	if(!isPrice(price))
	{
		writeError(errorFieldName, "Invalid Format, must be 999.99.");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkCardMessage(elementName, errorFieldName)
{
	var card = document.getElementById(elementName).value;

	if(isWhitespace(card))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkInstructions(elementName, errorFieldName)
{
	var instructions = document.getElementById(elementName).value;

	if(isWhitespace(instructions))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkSpokeTo()
{
	var name = document.getElementById("spoke_to").value;

	if(isWhitespace(name))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkReasonCode(elementName, errorFieldName)
{
	var selectBox = document.getElementById(elementName);

	var action = selectBox.options[selectBox.selectedIndex].value;

	if(action == "0")
	{
		writeError(errorFieldName, "You must select a reason code.");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function writeError(errorFieldName, errorMessage)
{
	var span = document.getElementById(errorFieldName);
	var newMessage = document.createTextNode("  " + errorMessage);

	if(span.childNodes)
	{
		var kids = span.childNodes;
		for(var i = 0; i < kids.length; i++)
		{
			span.removeChild(kids[i]);
		}
	}
	span.appendChild(newMessage);
	span.style.display = "inline";
}







/*
Handler for the onclick event of the various reason anchors for outgoing messages. Clicking
on the reason writes its text into the "Reason" textarea.
*/
function addReason()
{	// Get the text to be inserted.
	var kids = event.srcElement.childNodes;
	var i = 0;
	while(kids[i].nodeType !=  3 /*Node.TEXT_NODE*/)
	{
		i++;
	}
	var statement = kids[i].data.substring(2);

	var input = document.getElementById("message_comment");
	input.focus();
	document.selection.createRange().text += statement;
}





function newPage(pageNumber)
{
	var url = "displayCommunication.do?order_detail_id="
							+ document.forms[0].order_detail_id.value
							+ "&page_number="
							+ pageNumber

	performAction(url);
}



/*****
Low-level validation and formatting functions.
*****/
function isEmpty(s)
{
	return ((s == null) || (s.length == 0));
}

function isDigit(c)
{
	return ((c >= "0") && (c <= "9"));
}


var defaultEmptyOK = false;

function isInteger(s)
{
	var i;

	if(isEmpty(s))
	{
		if(isInteger.arguments.length == 1)
		{
			return defaultEmptyOK;
		}
		else
		{
			return (isInteger.arguments[1] == true);
		}
	}

	// Search through string's characters one by one
	// until we find a non-numeric character.
	// When we do, return false; if we don't, return true.

	for(i = 0; i < s.length; i++)
	{
		// Check that current character is number.
		var c = s.charAt(i);

		if(!isDigit(c))
		{
			return false;
		}
	}

	// All characters are numbers.
	return true;
}

function stripCharsInBag(s, bag)
{
	var i;
	var returnString = "";

	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for(i = 0; i < s.length; i++)
	{
		// Check that current character isn't whitespace.
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1)
		{
			returnString += c;
		}
	}

	return returnString;
}

/*
Remove any leading zeros from the search number
*/
function stripZeros(num)
{
	var num, newTerm;

	while(num.charAt(0) == "0")
	{
		newTerm = num.substring(1, num.length);
		num = newTerm;
	}

	if(num == "")
		num = "0";

	return num;
}

// whitespace characters
var whitespace = " \t\n\r";

function stripWhitespace(s)
{
	return stripCharsInBag(s, whitespace);
}

function isWhitespace(s)
{
    // Is s empty?
    if((s == null) || (s.length == 0))
    	return true;

    // Search through string's characters one by one
    // until we find a non-whitespace character.
    // When we do, return false; if we don't, return true.
    var whitespace = " \t\n\r";

    for (var i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}



function showModalAlert(message)
{
	//Modal_URL
	modalUrl = "alert.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = message;

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:no; scroll:no';

	window.showModalDialog(modalUrl, modalArguments, modalFeatures);
}


function updateCurrentUsers()
{
	var iframe = document.getElementById('CSRFrame');

	var url = "customerOrderSearch.do?action=refresh_ids"
				+ "&entity_type=ORDER_DETAIL"
				+ "&entity_id="
				+ document.getElementById("order_detail_id").value
				+ getSecurityParams(false);

	iframe.src = url;
}