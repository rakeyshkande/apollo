//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
    if (document.getElementById("sAddOnTypeOption").value != 4)
    {
       document.getElementById("sAddOnDefaultOption").value = "No";
       document.getElementById("sAddOnDefaultOption").disabled = true;        
    }
    else
    {
      document.getElementById("tAddOnPrice").disabled = true;
    }
    if (document.getElementById("sAddOnDefaultOption").value == "Yes")
    {
      document.getElementById("sAddOnDefaultOption").disabled = true;
      document.getElementById("sAddOnTypeOption").disabled = true;
      document.getElementById("sAvailableOption").disabled = true;
    }
    
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;

  setFeedCheckBox('NOVATOR_UPDATE_LIVE',liveFeedEnabled,liveFeedOn);
  setFeedCheckBox('NOVATOR_UPDATE_CONTENT',contentFeedEnabled,contentFeedOn);
  setFeedCheckBox('NOVATOR_UPDATE_TEST',testFeedEnabled,testFeedOn);
  setFeedCheckBox('NOVATOR_UPDATE_UAT',uatFeedEnabled,uatFeedOn);
  
}


//****************************************************************************************************
//* Instead of using the common function to initialize Novator feed checkboxes, this page will use a
//* custom method.  This is because the checkboxes may/may not be checked, but will ALWAYS be disabled
//****************************************************************************************************
function setFeedCheckBox(cbName, allowed, boxOn)
{
  var checkBox = document.getElementById(cbName);

  if (allowed != null && allowed == 'YES')
    checkBox.disabled = false;
  else
    checkBox.disabled = true;
  

  if (boxOn != null && boxOn == 'YES')
    checkBox.checked = true;
  else
    checkBox.checked = false;

}


//****************************************************************************************************
//* defaultModifierChanged()
//****************************************************************************************************
function defaultModifierChanged()
{
    if (document.getElementById("sAddOnTypeOption").value != 4)
    {
       document.getElementById("sAddOnDefaultOption").value = "No";
       document.getElementById("sAddOnDefaultOption").disabled = true;  
       document.getElementById("tAddOnPrice").disabled = false;
    }
    else
    {
      document.getElementById("sAddOnDefaultOption").disabled = false;
      document.getElementById("tAddOnPrice").value = document.getElementById("default_card_price").value;
      document.getElementById("tAddOnPrice").disabled = true;
    }
    if (document.getElementById("sAddOnDefaultOption").value == "Yes")
    {
      document.getElementById("sAddOnTypeOption").disabled = true;
      document.getElementById("sAvailableOption").value = "Yes";
      document.getElementById("sAvailableOption").disabled = true;
    }
    else
    {
      document.getElementById("sAddOnTypeOption").disabled = false;
      document.getElementById("sAvailableOption").disabled = false;
    }
    somethingChanged = true;
}

//****************************************************************************************************
//* trim
//****************************************************************************************************
function trim(str) 
{
  val = str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  return val;
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
  showWaitMessage("content", "wait", "Processing");
  document.forms[0].action = url;
  document.forms[0].submit();
}


//****************************************************************************************************
//* validateProductId()
//****************************************************************************************************
function validateProductId()
{
  var productId = document.getElementById("tProductId").value;

  if( productId != undefined && productId.length > 0)
  {
    document.getElementById("tProductId").value = productId.toUpperCase();

    //XMLHttpRequest
    var newRequest = new FTD_AJAX.Request('AddOnMaintenanceAction.do', FTD_AJAX_REQUEST_TYPE_POST, validateProductIdCallback, true, false);
    newRequest.addParam('action_type', 'validate_product_id');
    newRequest.addParam('tProductId', productId.toUpperCase());
    newRequest.send();
  }
}
   

//****************************************************************************************************
//* validateProductIdCallback()
//****************************************************************************************************
function validateProductIdCallback(data)
{
  var result = data;
  document.getElementById("productIdInuse").value = result; 
}

function validateAddOnId()
{
  var addOnId = document.getElementById("tAddOnId").value;

  if( addOnId != undefined && addOnId.length > 0 && document.getElementById("original_action_type").value == "add") 
  {
    document.getElementById("tAddOnId").value = addOnId.toUpperCase();
    //XMLHttpRequest
    var newRequest = new FTD_AJAX.Request('AddOnMaintenanceAction.do', FTD_AJAX_REQUEST_TYPE_POST, validateAddOnIdCallback, true, false);
    newRequest.addParam('action_type', 'validate_add_on_id');
    newRequest.addParam('tAddOnId', addOnId.toUpperCase());
    newRequest.send();
  }
}


//****************************************************************************************************
//* validateAddOnIdCallback()
//****************************************************************************************************
function validateAddOnIdCallback(data)
{
  var result = data;
  document.getElementById("addOnIdInuse").value = result; 
}

//****************************************************************************************************
//* performDashboard()
//****************************************************************************************************
function performDashboard()
{
  var errorMessage = "Are you sure you want to leave the screen with unsaved changes ?  Click YES to continue or NO to return to the screen";
  var url = "AddOnMaintenanceAction.do" + getSecurityParams(true) + "&action_type=dashboard";
  
  if (didSomethingChange())
  {
    var ret = displayConfirmError(errorMessage);
    if (ret)
      performAction(url);
  }
  else
    performAction(url);

}


//****************************************************************************************************
//* performMainMenu()
//****************************************************************************************************
function performMainMenu()
{
  var errorMessage = "Are you sure you want to leave the screen with unsaved changes ?  Click YES to continue or NO to return to the screen";
  var url = "AddOnMaintenanceAction.do" + getSecurityParams(true) + "&action_type=main_menu";
  
  if (didSomethingChange())
  {
    var ret = displayConfirmError(errorMessage);
    if (ret)
      performAction(url);
  }
  else
    performAction(url);


}


//****************************************************************************************************
//* performSave()
//****************************************************************************************************
function performSave()
{
  if (validate())
  {
    var url = "AddOnMaintenanceAction.do" + getSecurityParams(true) + "&action_type=save";

    var addOnType = document.getElementById('sAddOnTypeOption');
    document.getElementById("hAddOnTypeDescription").value = addOnType.options[addOnType.selectedIndex].addon_type_id_description;

    //pass the elementId for which you want to remove the line breaks. 
    removeLineBreaks('tAddOnText');

    //enable the disabled fields, so that Java code can pick up the values
    doEnableFields();
	
    /********************************************************************************************************************************
     * Changes for Apollo.Q3.2015 - Addon-Occasion association.
     * Loop through selected occasions:
     * 				Extract the occasion ID. 
     * 				Make a comma delimited string out of them.
     * 				Store it in a HTML form's hidden parameter (it gets submitted to server along with the form)
     * Sample occasion ID : OCC-10.
     ********************************************************************************************************************************/
	var selectedOccasions = '';
	
	$(".cOccasions:checked").each(function(){
		var occId = this.id;
		selectedOccasions = selectedOccasions+','+occId.split("-")[1];
	});
	
	$("#hLinkedOccasions").val(selectedOccasions.substr(1));	
	
	performAction(url);

  }
}


//****************************************************************************************************
//* validate
//****************************************************************************************************
function validate()
{
  resetErrorFields();
  var errorMessage = "";
  
  validateAddOnId();

  var addOnIdInUse = document.getElementById("addOnIdInUse").value;
  if (addOnIdInUse == "true")
  {
    errorMessage = errorMessage + "Add-On ID has already been used. Please enter a different value.<br/>";
    document.getElementById("tAddOnId").className="ErrorField";
  }

  //*************************
  //**ADDONID MUST BE ENTERED
  //*************************
  if (document.getElementById("tAddOnId") != null && trim(document.getElementById("tAddOnId").value) == "")
  {
    errorMessage = errorMessage + "Add-On ID is empty. Please enter an Add-On ID.<br/>";
    document.getElementById("tAddOnId").className="ErrorField";
  }

  //*************************
  //**ADDON DESCRIPTION MUST BE ENTERED
  //*************************
  if (document.getElementById("tAddOnDescription") != null && trim(document.getElementById("tAddOnDescription").value) == "")
  {
    errorMessage = errorMessage + "Add-on Description cannot be blank. Please enter an Add-on Description.<br/>";
    document.getElementById("tAddOnDescription").className="ErrorField";
  }

  //***************************************
  //**PRICE MUST BE SELECTED
  //***************************************
  if (document.getElementById("tAddOnPrice") != null && document.getElementById("tAddOnPrice").value == "")
  {
    errorMessage = errorMessage + "Price is empty. Please enter a Retail Price.<br/>";
    document.getElementById("tAddOnPrice").className="ErrorField";
  }
  else
  {
    var i = parseFloat(document.getElementById("tAddOnPrice").value);
    if(isNaN(i) || ! /^(\d{1,6})?(\.\d{1,2})?$/.test(document.getElementById("tAddOnPrice").value))
    {
      errorMessage = errorMessage + "Retail Price must be a numeric value between 0.00 and 999.99.<br/>";
      document.getElementById("tAddOnPrice").className="ErrorField";
    }
    else if(i > 999.99)
    {
      errorMessage = errorMessage + "Retail Price must be a numeric value between 0.00 and 999.99.<br/>";
      document.getElementById("tAddOnPrice").className="ErrorField";
    }
  }

  validateProductId();

  var productIdInUse = document.getElementById("productIdInUse").value;
  if (productIdInUse == "false")
  {
    errorMessage = errorMessage + "Product ID entered does not exist. Please enter an existing Product ID or leave this field empty.<br/>";
    document.getElementById("tProductId").className="ErrorField";
  }
  
  //***************************************
  //**WEIGHT MUST BE SELECTED
  //***************************************
  if (document.getElementById("tWeight") != null && document.getElementById("tWeight").value == "")
  {
    errorMessage = errorMessage + "Add-On Weight field cannot be empty.<br/>";
    document.getElementById("tWeight").className="ErrorField";
  }
  else
  {
    var w = parseFloat(document.getElementById("tWeight").value);
    if(isNaN(w) || ! /^(\d{1,6})?(\.\d{1,2})?$/.test(document.getElementById("tWeight").value))
    {
      errorMessage = errorMessage + "Add-On Weight must be a numeric value between 0.00 and 99.99.<br/>";
      document.getElementById("tWeight").className="ErrorField";
    }
    else if(w > 99.99)
    {
      errorMessage = errorMessage + "Add-On Weight must be a numeric value between 0.00 and 99.99.<br/>";
      document.getElementById("tWeight").className="ErrorField";
    }
  }

  //***************************************
  //**UNSPSC MUST BE SELECTED
  //***************************************
  if (document.getElementById("tUnspsc") != null && trim(document.getElementById("tUnspsc").value) == "")
  {
    errorMessage = errorMessage + "You must enter a UNSPSC.<br/>";
    document.getElementById("tUnspsc").className="ErrorField";
  }
  else
  {
    document.getElementById("tUnspsc").value = trim(document.getElementById("tUnspsc").value);
    if(document.getElementById("tUnspsc").value.length != 10)
    {
      errorMessage = errorMessage + "You must enter a 10 digit UNSPSC.<br/>";
      document.getElementById("tUnspsc").className="ErrorField";
    }
  }
  
  //*********************************************************************************************************************
  //**Changes for Apollo.Q3.2015 - Addon-Occasion association: Card type add-on must not have multiple occasions selected.
  //** addonType = 4 stands for "Card"
  //*********************************************************************************************************************
  var addonType = $("#sAddOnTypeOption option:selected").val();
  var noOfSelOccasions = $(".cOccasions:checked").size();
  
  if( addonType == '4' && noOfSelOccasions > 1)
  {
	errorMessage = errorMessage + "You cannot choose more than one occasion for an add-on of type Card <br/>";
    $("#sAddOnTypeOption").attr("class","ErrorField");
  }
  
  /***********************************************************************************************************************
   * DI-4: Add-on Dashboard Enhancements.
   * Added 2 new attributes PQuad Accessory Id and FTD West Add-on to the add-on.
   * Validations:
   * 	PQuad Accessory Id must be numeric.
   * 	If the addon is a FTDWestAddon, then PQuad Accessory Id must be provided. 
  ***********************************************************************************************************************/
  var jsPquadAccId = $("#tPQuadAccId").val();
  var numbers = /^[0-9]+$/;  
  if(trim(jsPquadAccId) != "" && !jsPquadAccId.match(numbers))  
  {  
	  errorMessage = errorMessage + "PQuad Accessory ID must be a numeric value. <br/>";
	  $("#tPQuadAccId").attr("class","ErrorField");
  }
  
  var FtdWestAddon = $("#isFtdWestAddon option:selected").val();
  if(FtdWestAddon == 'Yes' && trim(jsPquadAccId) == "")
  {
	  errorMessage = errorMessage + "PQuad Accessory ID is empty. Please enter a PQuad Accessory ID. <br/>";
	  $("#tPQuadAccId").attr("class","ErrorField");
  }	   
   
  

  if (errorMessage != null && errorMessage != "")
  {
    displayOkError(errorMessage);
    return false;
  }
  else
    return true;

}


//****************************************************************************************************
//* resetErrorFields
//****************************************************************************************************
function resetErrorFields()
{
  if (document.getElementById("tAddOnId") != null)
    document.getElementById("tAddOnId").className="input";

  if (document.getElementById("tAddOnDescription") != null)
    document.getElementById("tAddOnDescription").className="input";

  if(document.getElementById("tAddOnPrice") != null)
    document.getElementById("tAddOnPrice").className="input";

  if(document.getElementById("tWeight") != null)
    document.getElementById("tWeight").className="input";

  if(document.getElementById("tUnspsc") != null)
    document.getElementById("tUnspsc").className="input";
    
  if(document.getElementById("tProductId") != null)
    document.getElementById("tProductId").className="input";
    
  document.getElementById("productIdInUse").value = "true";
  document.getElementById("addOnIdInuse").value = "false";

}


//****************************************************************************************************
//* doEnableFields() - this will enable fields so that the back-end code receives non-Null values
//****************************************************************************************************
function doEnableFields()
{
  document.getElementById("sAddOnDefaultOption").disabled = false;
  document.getElementById("sAddOnTypeOption").disabled = false;
  document.getElementById("tAddOnPrice").disabled = false;
  document.getElementById("sAvailableOption").disabled = false;
  
  document.getElementById("NOVATOR_UPDATE_LIVE").disabled = false; 
  document.getElementById("NOVATOR_UPDATE_CONTENT").disabled = false; 
  document.getElementById("NOVATOR_UPDATE_TEST").disabled = false; 
  document.getElementById("NOVATOR_UPDATE_UAT").disabled = false; 
  
}



//***************************************************************************************************
// clearField()
//***************************************************************************************************/
function clearField(field)
{
  document.getElementById(field).value = ""
}


//***************************************************************************************************
// didSomethingChange()
//***************************************************************************************************/
function didSomethingChange()
{
  resetErrorFields();
  var ret = false; 

  //special characters to be removed
  var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

  if (somethingChanged)
    ret = true; 

  return ret;

}

//***************************************************************************************************
// limitText()
//***************************************************************************************************
function limitText(limitField, limitNum) 
{
	if (limitField.value.length > limitNum) 
  {
		limitField.value = limitField.value.substring(0, limitNum);
	} 
}

//***************************************************************************************************
// trim()
//***************************************************************************************************
function trim(a) 
{
  a = a.replace(/^\s+/, '');
  return a.replace(/\s+$/, '');
}


//***************************************************************************************************
// removeLineBreaks() - removes line breaks for the elementId that is passed
//***************************************************************************************************
function removeLineBreaks(inputId)
{
  var newValue = document.getElementById(inputId).value;

  if (trim(newValue).length > 0)
  {
    var regExp = new RegExp("\n","g");
    var regExp1 = new RegExp("\r","g");
    newValue = newValue.replace(regExp,"");
    newValue = newValue.replace(regExp1,"");

    document.getElementById(inputId).value = newValue;

  }

}  

