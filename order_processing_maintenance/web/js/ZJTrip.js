//****************************************************************************************************
//* doAdd
//****************************************************************************************************
function doAdd()
{
	var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=add";
	performAction(url);
}


//****************************************************************************************************
//* doCancelTrip
//****************************************************************************************************
function doCancelTrip()
{
	//set the message
	var errorMessage = "Are you sure you want to cancel this zone jump trip? All the orders will be cancelled or need to be rejected.";

	//get a true/false value from the modal dialog.
	var ret = displayConfirmError(errorMessage);

	if (ret)
	{
		var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=cancel_trip";

		//enable the disabled fields, so that Java code can pick up the values
		doEnableFields();

		performAction(url);
	}

}


//****************************************************************************************************
//* doCopyEdit
//****************************************************************************************************
function doCopyEdit(actionType)
{
	var tripId = retrieveTripId();
	if (tripId == null)
	{
		//set the message
		var errorMessage = "Please select a Trip that you want to " + actionType + ".";

		//display the popup
		displayOkError(errorMessage);
	}
	else
	{
		var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=" + actionType + "&trip_id=" + tripId;
		performAction(url);
	}
}


//****************************************************************************************************
//* doEnableFields() - this will enable fields so that the back-end code receives non-Null values
//****************************************************************************************************
function doEnableFields()
{
	document.getElementById('departure_date').disabled = false;
	document.getElementById('delivery_date').disabled = false;
	document.getElementById('vendor_location').disabled = false;
	document.getElementById('ih_id').disabled = false;
	document.getElementById('total_plt_qty').disabled = false;
	document.getElementById('third_party_plt_qty').disabled = false;
  document.getElementById('days_in_transit_qty').disabled = false;

	var vendorId = '';
	for (var x=1; x <= totalRecords; x++)
	{
		//get the vendor id
		vendorId = document.getElementById('line_num_' + x).value;

		document.getElementById('new_vendor_status_' + vendorId).disabled = false;
		document.getElementById('new_vendor_cutoff_' + vendorId).disabled = false;
	}

}


//****************************************************************************************************
//* doExit
//****************************************************************************************************
function doExit(actionType)
{
	//**since the delivery and departure dates are populated via Calendar.js, event handling on the input
	//**field (like onchange) does not work.  Have to do the comparision and trip the flag manually.
	//**
	//** NOTE: this function is called by MainMenuButton from ZJTripSummary, ZJTripMaintenance and ZJDashboard
	//**       thus, we have to check if the delivery_date and departure_date exist on the page

	//special characters to be removed
  var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

	var newDeliveryDate = document.getElementById('delivery_date');
	var newDepartureDate = document.getElementById('departure_date');

	if (newDeliveryDate != null && newDepartureDate != null)
	{
		var newDeliveryDateValue = stripCharsInBag(newDeliveryDate.value, bag);
		var newDepartureDateValue = stripCharsInBag(newDepartureDate.value, bag);

		if (newDeliveryDateValue != originalDeliveryDateValue || newDepartureDateValue != originalDepartureDateValue)
			somethingChanged = true;
	}

	if (somethingChanged)
	{
		//set the message
		var errorMessage = "Data changes were detected on this page. Are you sure want to exit without saving?";

		//get a true/false value from the modal dialog.
		var ret = displayConfirmError(errorMessage);

		if (ret)
		{
			var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=" + actionType;
			performAction(url);
		}
	}
	else
	{
		var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=" + actionType;
		performAction(url);
	}
}


//****************************************************************************************************
//* doFilter
//****************************************************************************************************
function doFilter(actionType)
{
	var filter = document.getElementById('sFilter');
	var filterValue = filter.options[filter.selectedIndex].value;

	var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=" + actionType + "&filter=" + filterValue;
	performAction(url);
}


//****************************************************************************************************
//* doLoadDefault
//****************************************************************************************************
function doLoadDefault(actionType)
{
	var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=" + actionType + "&filter=";
	performAction(url);
}


//****************************************************************************************************
//* doRefresh
//****************************************************************************************************
function doRefresh(actionType)
{
	var filterValue = document.getElementById('filter').value;

	var url = "ZJTripAction.do" + getSecurityParams(true) + "&action_type=" + actionType + "&filter=" + filterValue;
	performAction(url);
}


//****************************************************************************************************
//* doSave
//****************************************************************************************************
function doSave()
{
	if(validateAll())
	{
		var ihId = document.getElementById('ih_id');
		var ihIdValue = ihId.options[ihId.selectedIndex].value;
		var ihIdState = ihId.options[ihId.selectedIndex].state;

		var url =  "ZJTripAction.do" + getSecurityParams(true) + "&action_type=save&total_records=" + totalRecords;
		    url += "&ih_state=" + ihIdState;

		//enable the disabled fields, so that Java code can pick up the values
		doEnableFields();

		performAction(url);
	}

}


//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
  initErrorMessages();

}


/**********************************************************************************************************
 * Called when the page loads, this function displays any error messages
 * generated on load or from validation failures.
 **********************************************************************************************************/
function initErrorMessages()
{
	if (serverErrorMessage !=null && serverErrorMessage.length > 0)
	{
		displayOkError(serverErrorMessage);
	}
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	showWaitMessage("content", "wait", "Processing");
	document.forms[0].action = url;
	document.forms[0].submit();
}


//****************************************************************************************************
//* populateDropDown()
//****************************************************************************************************
function populateDropDown(currentlySavedValue, minValue, maxValue)
{
	var tempValue	= minValue;

	while(tempValue <= maxValue)
	{
		if (tempValue==currentlySavedValue)
			document.write( "<OPTION selected='selected' value=" + tempValue + ">" + tempValue + "</OPTION>" );
		else
			document.write( "<OPTION value=" + tempValue + ">" + tempValue + "</OPTION>" );

		tempValue += 1;
	}
}


//****************************************************************************************************
//* populateDropDownTime()
//****************************************************************************************************
function populateDropDownTime(currentlySavedValue)
{
	var currentlySavedHour = currentlySavedValue.substring(0,2);
	var currentlySavedMin = currentlySavedValue.substring(2);
	//alert ("currentlySavedValue = " + currentlySavedValue + " and currentlySavedHour = " + currentlySavedHour + " and currentlySavedMin = " +  currentlySavedMin);

//alert (disabled);

	var hour = 0;
	var stringHour;
	var stringMinute00 = '00';
	var stringMinute30 = '30';
	var option00 = '';
	var option00Value = '';
	var option30 = '';
	var option30Value = '';
	var optionEnd = '</OPTION>';

	for (hour=0; hour<24; hour++ )
	{
		if (hour<10)
			stringHour  = '0' + hour;
		else
			stringHour  = hour;

		//set the option for 0 on the hour
		option00 = "<OPTION value=" + stringHour + stringMinute00;
		if (stringHour == currentlySavedHour && stringMinute00 == currentlySavedMin)
			option00 = option00 + " selected ";
		option00 = option00 + " >";

		option00Value = stringHour + ':' + stringMinute00;


		//set the option for 30 on the hour
		option30 = "<OPTION value=" + stringHour + stringMinute30;
		if (stringHour == currentlySavedHour && stringMinute30 == currentlySavedMin)
			option30 = option30 + " selected ";
		option30 = option30 + " >";

		option30Value = stringHour + ':' + stringMinute30;


		document.write( option00 + option00Value + optionEnd );
		document.write( option30 + option30Value + optionEnd );


	}
}


//****************************************************************************************************
//* retrieveTripId
//****************************************************************************************************
function retrieveTripId()
{
	var radio = document.forms[0].tripRadio;
	var radioSelected;

	if (radio != null)
	{
		if (radio.length == null)
		{
			if (radio.checked)
				radioSelected = radio;
			else
				radioSelected = null;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++){
				if ( radio[i].checked ){
					radioSelected = radio[i];
				}
			}
		}


		// if radio was not selected, return null
		if ( radioSelected == null )
		{
			return null;
		}
		// else return the radio value
		else
		{
			var injectionHubId = radioSelected.value;
			return injectionHubId;
		}
	}
}


/*********************************************************************************************************
 * This function is called to ensure that the user has entered valid values for all the enterable fields
 *********************************************************************************************************/
function validateAll()
{
	var valid = true;
	validateResetBackground();

	valid = validateVendorLocation();

	if (valid)
		valid = validateDates();

	if (valid)
		valid = validatePalletCounts();

	if (valid)
		valid = validateVendors();

	return valid;
}


//****************************************************************************************************
//* validateResetBackground
//****************************************************************************************************
function validateResetBackground()
{
	document.getElementById('vendor_location').style.backgroundColor = 'white';
	document.getElementById('delivery_date').style.backgroundColor = 'white';
	document.getElementById('departure_date').style.backgroundColor = 'white';
	document.getElementById('total_plt_qty').style.backgroundColor = 'white';
	document.getElementById('third_party_plt_qty').style.backgroundColor = 'white';
  document.getElementById('days_in_transit_qty').style.backgroundColor = 'white';

}


//****************************************************************************************************
//* validateVendorLocation
//****************************************************************************************************
function validateVendorLocation()
{
	var returnVal = true;

	//****validate location exist
	var vendorLocation = document.getElementById('vendor_location').value;
	if (vendorLocation == null || vendorLocation.length == 0)
	{
		document.getElementById('vendor_location').style.backgroundColor = 'pink';
		displayOkError("Please enter a vendor location");
		returnVal = false;
	}
	return returnVal;

}


//****************************************************************************************************
//* validateDates
//****************************************************************************************************
function validateDates()
{
	var returnVal = true;
	var message = '';

  var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

	var deliveryDate          = document.getElementById('delivery_date').value;
	var departureDate         = document.getElementById('departure_date').value;
  var daysInTransit         = document.getElementById("days_in_transit_qty");
  var daysInTransitValue    = daysInTransit.options[daysInTransit.selectedIndex].value - 0;
  var daysFromHubToDelivery = 1;
  var totalDaysInTransit    = daysInTransitValue + daysFromHubToDelivery; 

	var today = new Date();

	var todayYear = today.getFullYear();

	var todayMonth = today.getMonth() + 1;
	if (todayMonth < 10)
		todayMonth = '0' + todayMonth;

	var todayDay = today.getDate();
	if (todayDay < 10)
		todayDay = '0' + todayDay;


	var todayDate = '' + todayYear + todayMonth + todayDay;

	//****validate delivery date exist
	if (deliveryDate == null || deliveryDate.length == 0)
	{
		document.getElementById('delivery_date').style.backgroundColor = 'pink';
		message = "Date field(s) cannot be empty.  Please fix";
		returnVal = false;
	}

	//****validate departure date exist
	if (departureDate == null || departureDate.length == 0)
	{
		document.getElementById('departure_date').style.backgroundColor = 'pink';
		message = "Date field(s) cannot be empty.  Please fix";
		returnVal = false;
	}


	//****if date fields are populated
	if (deliveryDate != null && deliveryDate.length > 0 &&
			departureDate != null && departureDate.length > 0)
	{
    var departureDateFormatted = stripCharsInBag(departureDate, bag);
		var deliveryDateFormatted = stripCharsInBag(deliveryDate, bag);

    //create correct delivery date field based on departure date plus total  time in transit (from vendor to recipient) - Note this was done this way to ensure that month cross over works ok
    //get the departure date
    var departureDateFormattedYear = departureDateFormatted.substring(0,4) - 0; 
    var departureDateFormattedMonth = departureDateFormatted.substring(4,6) - 0; 
    var departureDateFormattedDay = departureDateFormatted.substring(6) - 0; 

    //create correctDeliveryDate variable to be equal to departure date initially
    var correctDeliveryDate = new Date(departureDateFormattedYear, departureDateFormattedMonth - 1, departureDateFormattedDay)

    //now, add the total time in transit
    correctDeliveryDate.setDate (correctDeliveryDate.getDate() + totalDaysInTransit)

    //get the year
    var correctDeliveryDateYear = correctDeliveryDate.getFullYear();

    //get the month
    var correctDeliveryDateMonth = correctDeliveryDate.getMonth() + 1;
    if (correctDeliveryDateMonth < 10)
      correctDeliveryDateMonth = '0' + correctDeliveryDateMonth;

    //get the day
    var correctDeliveryDateDay = correctDeliveryDate.getDate();
    if (correctDeliveryDateDay < 10)
      correctDeliveryDateDay = '0' + correctDeliveryDateDay;

    //and create a formatted field that should be correct delivery date
    var correctDeliveryDateFormatted = '' + correctDeliveryDateYear + correctDeliveryDateMonth + correctDeliveryDateDay;

		//****validate that the delivery date is not in the past
		if (departureDateFormatted < todayDate)
		{
			document.getElementById('departure_date').style.backgroundColor = 'pink';
			message = "Departure Date cannot be in the past. Please fix";
			returnVal = false;
		}
		//****validate that the delivery date is not in the past
		else if (deliveryDateFormatted < todayDate)
		{
			document.getElementById('delivery_date').style.backgroundColor = 'pink';
			message = "Delivery Date cannot be in the past. Please fix";
			returnVal = false;
		}
		//****validate that the delivery date is greater than the departure date
		else if (deliveryDateFormatted <= departureDateFormatted)
		{
			document.getElementById('delivery_date').style.backgroundColor = 'pink';
			document.getElementById('departure_date').style.backgroundColor = 'pink';
			message = "Delivery Date should be greater than the Departure Date";
			returnVal = false;
		}
    else if (correctDeliveryDateFormatted != deliveryDateFormatted)
    {
      document.getElementById('delivery_date').style.backgroundColor = 'pink';
      document.getElementById('departure_date').style.backgroundColor = 'pink';
      document.getElementById('days_in_transit_qty').style.backgroundColor = 'pink';
      message = "Delivery Date incorrect as per selected Days In Transit.";
      returnVal = false;
    }
	}

	if (!returnVal)
		displayOkError(message);

	return returnVal;

}


//****************************************************************************************************
//* validatePalletCounts
//****************************************************************************************************
function validatePalletCounts()
{
	var returnVal = true;

	//****validate difference between 3rd party and total pallets >= 0
	var totalPallets      = document.getElementById("total_plt_qty");
	var totalPalletsValue = totalPallets.options[totalPallets.selectedIndex].value - 0;

	var thirdPartyPallets      = document.getElementById("third_party_plt_qty");
	var thirdPartyPalletsValue = thirdPartyPallets.options[thirdPartyPallets.selectedIndex].value - 0;

//alert("thirdPartyPalletsValue = " + thirdPartyPalletsValue + " and totalPalletsValue = " + totalPalletsValue);
	if (thirdPartyPalletsValue > totalPalletsValue)
	{
		document.getElementById('total_plt_qty').style.backgroundColor = 'pink';
		document.getElementById('third_party_plt_qty').style.backgroundColor = 'pink';
		displayOkError("You cannot allocate more pallets to 3rd Party than the total number of pallets.  Please fix");
		returnVal = false;
	}

	return returnVal;

}


//****************************************************************************************************
//* validateVendors
//****************************************************************************************************
function validateVendors()
{
	//for add and copy, at least one should exist in Assigned status
	//for edit, at least one must exist in either assigned or unassigned status

	//go thru all and ensure that the assigned/unassigned exist in the same state

	var returnVal = true;
	var assignedCount = 0;
	var unassignedCount = 0;
	var canceledCount = 0;

	var originalVendorStatusCode = '';
	var newVendorStatus = '';
	var newVendorStatusCode = '';
	var vendorId = '';
	var newStateValue = ''
	var oldStateValue = ''
	var differentStatesExist = false;
	var vendorStatusCodeChangedToUnassigned = false;
	var vendorStatusCodeChangedToCanceled = false;


	for (var x=1; x <= totalRecords; x++)
	{
		//get the vendor id
		vendorId = document.getElementById('line_num_' + x).value;

		//get the vendor statuses
		originalVendorStatusCode = document.getElementById('original_vendor_status_' + vendorId).value;
		newVendorStatus = document.getElementById('new_vendor_status_' + vendorId);
		newVendorStatusCode = newVendorStatus.options[newVendorStatus.selectedIndex].value;

		//get the vendor state, and initialize the temp variable
		newStateValue = document.getElementById('vendor_state_' + vendorId).value;
		if (oldStateValue == '' && newVendorStatusCode != '')
			oldStateValue = newStateValue;

		//get the count of Assigned, Unassigned and Canceled.  Also, check if the vendor status has been switched from an
		//Assigned->Unassigned, Assigned->Canceled, Unassigned->Canceled, and if yes, trip appropriate flag
		if (newVendorStatusCode == 'A')
		{
			assignedCount++;
		}
		else if (newVendorStatusCode == 'U')
		{
			unassignedCount++;
//alert("originalVendorStatusCode = " + originalVendorStatusCode + " and newVendorStatusCode = " + newVendorStatusCode);
			if (originalVendorStatusCode == 'A')
				vendorStatusCodeChangedToUnassigned = true;
		}
		else if (newVendorStatusCode == 'X')
		{
			canceledCount++;
			if (originalVendorStatusCode == 'A' || originalVendorStatusCode == 'U' )
				vendorStatusCodeChangedToCanceled = true;
		}


		//if vendors on a trip reside in different states, trip a flag
		if (oldStateValue != newStateValue && newVendorStatusCode != '')
			differentStatesExist = true;

	}

//alert("assignedCount = " + assignedCount + " and unassignedCount = " + unassignedCount + " and canceledCount = " + canceledCount);


	if (assignedCount == 0)
	{
		if (originalActionType == 'add' || originalActionType == 'copy')
		{
			displayOkError("To be accepting packages, a zone jump trip must have at least one vendor in assigned or unassigned status.  Please select a vendor or cancel the zone jump trip.");
			returnVal = false;
		}
		else if (originalActionType == 'edit' && unassignedCount == 0)
		{
			if (canceledCount > 0)
			{
				displayOkError("To cancel ALL vendors on a trip, please cancel the entire trip");
				returnVal = false;
			}
			else
			{
				displayOkError("To be accepting packages, a zone jump trip must have at least one vendor in assigned or unassigned status.  Please select a vendor or cancel the zone jump trip.");
				returnVal = false;
			}
		}
	}


	if (returnVal && differentStatesExist)
		returnVal = displayConfirmError("Please note: some vendors on this trip reside in different states.  Press YES to continue, or NO to change");

//alert(vendorStatusCodeChangedToUnassigned);

	if (returnVal && vendorStatusCodeChangedToUnassigned)
		returnVal = displayConfirmError("Are you sure you want to unassign vendor(s)?  No more orders will go to the unassigned vendor(s).  Press YES to continue, or NO to change");

	if (returnVal && vendorStatusCodeChangedToCanceled)
		returnVal = displayConfirmError("Are you sure you want to cancel vendor(s)? All the orders will be canceled or need to be rejected and no future orders will go to the canceled vendor(s).  Press YES to continue, or NO to change");


	return returnVal;

}
