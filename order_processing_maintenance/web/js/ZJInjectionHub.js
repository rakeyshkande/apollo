//****************************************************************************************************
//* doAdd
//****************************************************************************************************
function doAdd()
{
	var url = "ZJInjectionHubAction.do" + getSecurityParams(true) + "&action_type=add";
	performAction(url);
}


//****************************************************************************************************
//* doEdit
//****************************************************************************************************
function doEdit()
{
	var ihIdToBeEdited = retrieveInjectionHubId();
	if (ihIdToBeEdited == null)
	{
		//set the message
		var errorMessage = "Please select an Injection Hub that you want to edit.";
		displayOkError(errorMessage);
	}
	else
	{
		var url = "ZJInjectionHubAction.do" + getSecurityParams(true) + "&action_type=edit&ih_id=" + ihIdToBeEdited;
		performAction(url);
	}
}


//****************************************************************************************************
//* doExit
//****************************************************************************************************
function doExit(actionType)
{
	if (somethingChanged)
	{
		//set the message
		var errorMessage = "Data changes were detected on this page. Are you sure want to exit without saving?";

		//get a true/false value from the modal dialog.
		var ret = displayConfirmError(errorMessage);

		if (ret)
		{
			var url = "ZJInjectionHubAction.do" + getSecurityParams(true) + "&action_type=" + actionType;
			performAction(url);
		}
	}
	else
	{
		var url = "ZJInjectionHubAction.do" + getSecurityParams(true) + "&action_type=" + actionType;
		performAction(url);
	}
}


//****************************************************************************************************
//* doSave
//****************************************************************************************************
function doSave()
{
	if(validate())
	{
		var url = "ZJInjectionHubAction.do" + getSecurityParams(true) + "&action_type=save";
		performAction(url);
	}
	else
	{
		var url = '';

		//Modal_URL
		var modalUrl = "alert.html";

		//Modal_Arguments
		var modalArguments = new Object();
		modalArguments.modalText = "Please fix the highlighted fields before saving";

		//Modal_Features
		var modalFeatures = 'dialogWidth:400px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
		modalFeatures += 'resizable:no; scroll:no';

		//show the modal dialog.
		window.showModalDialog(modalUrl, modalArguments, modalFeatures);
	}
}


//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	document.forms[0].action = url;
	document.forms[0].submit();
}


//****************************************************************************************************
//* retrieveInjectionHubId
//****************************************************************************************************
function retrieveInjectionHubId()
{
	var radio = document.forms[0].injectionHubRadio;
	var radioSelected;

	if (radio != null)
	{
		if (radio.length == null)
		{
			if (radio.checked)
				radioSelected = radio;
			else
				radioSelected = null;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++){
				if ( radio[i].checked ){
					radioSelected = radio[i];
				}
			}
		}


		// if radio was not selected, return null
		if ( radioSelected == null )
		{
			return null;
		}
		// else return the radio value
		else
		{
			var injectionHubId = radioSelected.value;
			return injectionHubId;
		}

	}
}


//****************************************************************************************************
//* validate
//****************************************************************************************************
function validate()
{
	var returnVal = true;

	//****validate city
	document.getElementById('ih_city').style.backgroundColor = 'white';
	var ihCityVal = document.getElementById('ih_city').value;
	if (ihCityVal == null || ihCityVal.length == 0)
	{
		document.getElementById('ih_city').style.backgroundColor = 'pink';
		returnVal = false;
	}

	//****validate address
	document.getElementById('ih_address').style.backgroundColor = 'white';
	var ihAddressVal = document.getElementById('ih_address').value;
	if (ihAddressVal == null || ihAddressVal.length == 0)
	{
		document.getElementById('ih_address').style.backgroundColor = 'pink';
		returnVal = false;
	}

	//****validate zip - note that checkZIPCode() was not called because it displays a warning message. Per business,
	//no warning message was desired
	document.getElementById('ih_zip').style.backgroundColor = 'white';
	var ihZipVal = document.getElementById('ih_zip').value;
	ihZipVal = stripCharsInBag(ihZipVal, ZIPCodeDelimiters)
	if (!isZIPCode(ihZipVal, false))
	{
		document.getElementById('ih_zip').style.backgroundColor = 'pink';
		returnVal = false;
	}

	//****validate sds account num
	document.getElementById('ih_SDS_Account_Num').style.backgroundColor = 'white';
	var ihSDSAccountNumVal = document.getElementById('ih_SDS_Account_Num').value;
	if (ihSDSAccountNumVal == null || ihSDSAccountNumVal.length == 0)
	{
		document.getElementById('ih_SDS_Account_Num').style.backgroundColor = 'pink';
		returnVal = false;
	}

	//****validate ship point id
	document.getElementById('ih_ship_point_id').style.backgroundColor = 'white';
	var ihShipPointId = document.getElementById('ih_ship_point_id').value;
	if (ihShipPointId == null || ihShipPointId.length == 0)
	{
		document.getElementById('ih_ship_point_id').style.backgroundColor = 'pink';
		returnVal = false;
	}


	return returnVal;

}

