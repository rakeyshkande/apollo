//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
    document.getElementById('tEmailAddress').focus();
    if(document.getElementById('hSearchDone').value == 'Y' && document.getElementById('hUpdateAllowed').value == 'Y')
    {     
        document.getElementById('bSave').disabled = false;
        document.getElementById('bDeleteAll').disabled = false;
    }
}	


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	showWaitMessage("content", "wait", "Processing");
	document.forms[0].action = url;
	document.forms[0].submit();
}


//****************************************************************************************************
//* moveSelectedAll
//****************************************************************************************************
function moveSelectedAll()
{
  var vendorsAvailableDropDown = document.getElementById("sVendorsAvailable");

  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		vendorsAvailableDropDown[i].selected = true; 
	}
	moveSelected(); 

}


//****************************************************************************************************
//* moveSelected
//****************************************************************************************************
function moveSelected()
{
  var vendorsAvailableDropDown = document.getElementById("sVendorsAvailable");
  var vendorsChosenDropDown  = document.getElementById("sVendorsChosen");

	var newVendorsAvailableArrayList = new Array();
	var newVendorsChosenArrayList = new Array();

	//create existing Chosen list
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		newVendorsChosenArrayList[i] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
	}

	//define index positions for the arrays - new data will be added according to these positions
	var newVendorsChosenIndex = newVendorsChosenArrayList.length - 1; 
	var newVendorsAvailableIndex = -1; 

	//(1) create new Available ArrayList 
	//(2) append to Chosen ArrayList
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		if (vendorsAvailableDropDown[i].selected)
		{
			newVendorsChosenIndex++; 
			newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
		}
		else
		{
			newVendorsAvailableIndex++; 
			newVendorsAvailableArrayList[newVendorsAvailableIndex] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
		}
	}

	//sort Available ArrayList
	bubbleSortAsc(newVendorsAvailableArrayList, newVendorsAvailableArrayList.length, 1); 

	//sort Chosen ArrayList
	bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1); 

	//remove options from existing Available select dropdown
  for (var i = vendorsAvailableDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsAvailableDropDown.remove(i); 
	}  	

	//remove options from existing Chosen select dropdown
  for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsChosenDropDown.remove(i); 
	}  	


	//add items from existing Available select/options
  for (var i = 0; i < newVendorsAvailableArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsAvailableOption = new Option(newVendorsAvailableArrayList[i][1], newVendorsAvailableArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsAvailableDropDown.add(newVendorsAvailableOption); 
	}  	

	//add items from existing Available select/options
  for (var i = 0; i < newVendorsChosenArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][1], newVendorsChosenArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsChosenDropDown.add(newVendorsChosenOption); 
	}  	

}

//****************************************************************************************************
//* moveUnSelectedAll
//****************************************************************************************************
function moveUnSelectedAll()
{
  var vendorsChosenDropDown = document.getElementById("sVendorsChosen");

  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		vendorsChosenDropDown[i].selected = true; 
	}
	moveUnSelected(); 

}


//****************************************************************************************************
//* moveUnSelected
//****************************************************************************************************
function moveUnSelected()
{
  var vendorsChosenDropDown = document.getElementById("sVendorsChosen");
  var vendorsAvailableDropDown  = document.getElementById("sVendorsAvailable");

	var newVendorsChosenArrayList = new Array();
	var newVendorsAvailableArrayList = new Array();

	//create existing Available list
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
		newVendorsAvailableArrayList[i] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
	}

	//define index positions for the arrays - new data will be added according to these positions
	var newVendorsAvailableIndex = newVendorsAvailableArrayList.length - 1; 
	var newVendorsChosenIndex = -1; 

	//(1) create new Chosen ArrayList 
	//(2) append to Available ArrayList
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
		if (vendorsChosenDropDown[i].selected)
		{
			newVendorsAvailableIndex++; 
			newVendorsAvailableArrayList[newVendorsAvailableIndex] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
		}
		else
		{
			newVendorsChosenIndex++; 
			newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
		}
	}

	//sort Chosen ArrayList
	bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1); 

	//sort Available ArrayList
	bubbleSortAsc(newVendorsAvailableArrayList, newVendorsAvailableArrayList.length, 1); 

	//remove options from existing Chosen select dropdown
  for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsChosenDropDown.remove(i); 
	}  	

	//remove options from existing Available select dropdown
  for (var i = vendorsAvailableDropDown.length - 1; i >= 0 ; i--)
  {
		vendorsAvailableDropDown.remove(i); 
	}  	


	//add items from existing Chosen select/options
  for (var i = 0; i < newVendorsChosenArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][1], newVendorsChosenArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsChosenDropDown.add(newVendorsChosenOption); 
	}  	

	//add items from existing Chosen select/options
  for (var i = 0; i < newVendorsAvailableArrayList.length; i++)
  {
		//Create a new option  - definition = new Option(text, value, defaultSelected, selected)  
		var newVendorsAvailableOption = new Option(newVendorsAvailableArrayList[i][1], newVendorsAvailableArrayList[i][0], false, false);

		//and add it to the drop down
		vendorsAvailableDropDown.add(newVendorsAvailableOption); 
	}  	

}



//****************************************************************************************************
//* bubbleSortAsc
//****************************************************************************************************
function bubbleSortAsc(arrayName,length,sortBy) 
{
	for (var i=0; i<(length-1); i++)
	{
		for (var j=i+1; j<length; j++)
		{
			if (arrayName[j][sortBy] < arrayName[i][sortBy]) 
			{
				var dummy = arrayName[i];
				arrayName[i] = arrayName[j];
				arrayName[j] = dummy;
			}
		}
	}
}


//***************************************************************************************************
// getVendorsChosen()
//***************************************************************************************************/
function getVendorsChosen()
{
  var vc = document.getElementById("sVendorsChosen");
  var vendorsChosen = ""; 
  
  for (var i = 0; i < vc.length; i++)
  {
    vendorsChosen += vc[i].value + ","; 
  }

  vendorsChosen = vendorsChosen.substring(0,vendorsChosen.length - 1)
  return vendorsChosen; 
  
}


//****************************************************************************************************
//* search
//****************************************************************************************************
function search() 
{
	if (document.getElementById("tEmailAddress").value == '')
	{
    displayOkError("Email Address is required");
  }
  else if(!isEmail(document.getElementById("tEmailAddress").value))
  {
      displayOkError("Invalid email address")
  }
  else
  {
		var url = "InventoryTrackingEmailAlertsAction.do" + getSecurityParams(true) + "&action_type=search";

		performAction(url);
  }
}


//****************************************************************************************************
//* deleteAll
//****************************************************************************************************
function deleteAll() 
{
    if(document.getElementById("hEmailAddress").value == '')
    {
        displayOkError("Please search for an email address");
        return false;
    }
    else
    {
	//set the message
	var errorMessage = "Are you sure you want to delete all references to this email address?";

	//get a true/false value from the modal dialog.
	var ret = displayConfirmError(errorMessage);        

        if(ret)
        {
            document.getElementById("hVendorsChosen").value = getVendorsChosen();
            var url = "InventoryTrackingEmailAlertsAction.do" + getSecurityParams(true) + "&action_type=deleteAll";
            performAction(url);
        }
    }
}


//****************************************************************************************************
//* save
//****************************************************************************************************
function save() 
{
    if(document.getElementById("hEmailAddress").value == '')
    {
        displayOkError("Please search for an email address");
        return false;
    }
    else if(document.getElementById("sApplyToCurrentFlag").value == '')
    {
        displayOkError("Please select an action");
        return false;
    }
    else
    {
        document.getElementById("hVendorsChosen").value = getVendorsChosen();
        var url = "InventoryTrackingEmailAlertsAction.do" + getSecurityParams(true) + "&action_type=save";

	performAction(url);
    }
}


//****************************************************************************************************
//* setChanged()
//****************************************************************************************************
function setChanged()
{
  changed = true;
}


//****************************************************************************************************
//* mainMenu()
//****************************************************************************************************
function mainMenu()
{
    var ret = true;
    if(changed)
    {
        //set the message
        var errorMessage = "Are you sure you want to leave the screen with unsaved changes?";
        
        //get a true/false value from the modal dialog.
        ret = displayConfirmError(errorMessage);        
    }        

    if(ret)
    {
        var url = "MainMenuAction.do" + getSecurityParams(true);
        performAction(url);
    }
}


//***************************************************************************************************
// getVendorsChosen()
//***************************************************************************************************/
function getVendorsChosen()
{
  var vc = document.getElementById("sVendorsChosen");
  var vendorsChosen = ""; 
  
  for (var i = 0; i < vc.length; i++)
  {
    vendorsChosen += vc[i].value + ","; 
  }

  vendorsChosen = vendorsChosen.substring(0,vendorsChosen.length - 1)
  return vendorsChosen; 
  
}


