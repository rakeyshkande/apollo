/*
 * Common warning messages.
 */
var WARNING_MSG_1 = "Changing from a Domestic address to an international address will require you to change the product and maybe the delivery date.  Do you wish to Continue?";
var WARNING_MSG_2 = "Changing from an International Order to a Domestic order will require you to choose a different product.  Do you wish to Continue?";
var WARNING_MSG_3 = "The assigned product cannot be delivered on the assigned delivery date.  Would you like to select a new product or update the delivery date?";
var WARNING_MSG_4 = "Are you sure you want to exit without saving your changes?";
var WARNING_MSG_5 = "Miles / Points have been posted to the customer's account already.  Do you wish to continue?";
var WARNING_MSG_6 = "This product is not available in the selected delivery area for the selected delivery date.  Do you wish to continue?";


/*
   Helper function to print the user screen
*/
function printIt(){
	window.print();
}

/*
    Shared function to initialize checkbox. Used for Novator feed.
*/
function setupCheckBox(cbName, allowed, boxOn)
{
        var checkBox = document.getElementById(cbName);

        if (allowed != null && allowed == 'YES')
        {
            checkBox.checked = false;
            checkBox.disabled = false;
            if (boxOn != null && boxOn == 'YES')
            {
                checkBox.checked = true;
            }
        }
        else
        {
            checkBox.checked = false;
            checkBox.disabled = true;
        }
}
 /*
  This function handles the main menu functionality for all pages.
 */
 function doMainMenuAction(){
	var url = '';

  //Modal_URL
  var modalUrl = "confirm.html";
  //Modal_Arguments
  var modalArguments = new Object();
  var dnis = document.getElementById('call_dnis').value;

   if((dnis != '') && (dnis != null)) {

      modalArguments.modalText = 'Have you completed your work on this order?';

      //Modal_Features
      var modalFeatures  =  'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
      modalFeatures +=  'resizable:no; scroll:no';

      //get a true/false value from the modal dialog.
      var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

      if(ret)
      {
          url = "mainMenu.do" +	"?work_complete=y" + getSecurityParams(false);
      }
      else
      {
        url = "mainMenu.do" + "?work_complete=n" + getSecurityParams(false);
      }

  }else{
     url = "mainMenu.do" + "?work_complete=n" + getSecurityParams(false);
  }

  performAction(url);
}

/*
 Displays the Florist Maintenance screen for the user as
 a modal dialog window
*/
function doFloristMaintenanceAction(url){
		var modal_dim = 'dialogWidth=800px,dialogHeight=400px,dialogLeft=0px,dialogTop=90px';
	     	  modal_dim +='location=0,status=0,menubar=0,scrollbars=1,resizable=0';
    window.showModalDialog(url, "", modal_dim);

}

/*
	Displays the Load Memeber Data Search page
	in a modal dialog.
*/
 function doFloristInquiryAction(url){
 	var modal_dim = 'dialogWidth=800px,dialogHeight=400px,dialogLeft=0px,dialogTop=90px';
	     	  modal_dim +='location=0,status=0,menubar=0,scrollbars=1,resizable=0';
    window.showModalDialog(url, "", modal_dim);
}


/***********************************************************************************
* doSearchAction()
************************************************************************************/
function doSearchAction()
{
  var url = "customerOrderSearch.do" +
    "?action=load_page";
	performAction(url);
}

/***********************************************************************************
*doMainMenuActionNoPopup() -- take to the main menu without asking if work is complete
************************************************************************************/
function doMainMenuActionNoPopup(){
	var url = "mainMenu.do" + "?work_complete=n" + getSecurityParams(false);
	performAction(url);
}



/***********************************************************************************
* doExitPageAction()
************************************************************************************/
function doExitPageAction(popupText){
  //Modal_URL
  	var modalUrl = "confirm.html";

  	//Modal_Arguments
  	var modalArguments = new Object();
  	modalArguments.modalText = popupText;

  	//Modal_Features
  	var modalFeatures  =  'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
  	modalFeatures +=  'resizable:no; scroll:no';

  	//get a true/false value from the modal dialog.
  	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  	if(ret)
  	{
  		return true;
	}
  	else
  	{
		return false;
	}

}


/***********************************************************************************
* getXMLobjXML()
************************************************************************************/
function getXMLobjXML(xmlToLoad)
{
	var xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
	xml.async = false;
	xml.resolveExternals = false;
	try
	{
		xml.loadXML(xmlToLoad);
		return(xml)
	}
	catch(e)
	{
		throw(xml.parseError.reason)
	}
}


/**********************************************************************************************************
 * Displays the confirm error
 **********************************************************************************************************/
function displayConfirmError(errorMessage)
{

	//Modal_URL
	var modalUrl = "confirm.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = errorMessage;

	//Modal_Features
	var modalFeatures = 'dialogWidth:400px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
	modalFeatures += 'resizable:no; scroll:no';

	//get a true/false value from the modal dialog.
	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

	return ret;
}


/**********************************************************************************************************
 * Displays the ok error
 **********************************************************************************************************/
function displayOkError(errorMessage)
{
	//Modal_URL
	var modalUrl = "alert.html";

	// modal arguments
	var modalArguments = new Object();
	modalArguments.modalText = errorMessage;

	// modal features
	var modalFeatures = 'dialogWidth:400px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';

	// show the message
	window.showModalDialog(modalUrl, modalArguments, modalFeatures);

}

