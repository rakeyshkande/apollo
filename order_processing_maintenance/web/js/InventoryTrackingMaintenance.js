//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
  if (document.getElementById("tProductId") != null)
    document.getElementById("tProductId").focus(); 
  
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
  showWaitMessage("content", "wait", "Processing");
  document.forms[0].action = url;
  document.forms[0].submit();
}


//****************************************************************************************************
//* forecastChanged
//****************************************************************************************************
function forecastChanged()
{
  var forecast = document.getElementById("tForecast").value; 
  var newOnHand = forecast - shippedShipping; 
  
  document.getElementById("lRevised").innerText = forecast; 
  document.getElementById("lOnHand").innerText = newOnHand; 
  
}


//****************************************************************************************************
//* revisedChanged
//****************************************************************************************************
function revisedChanged()
{
  var revised = document.getElementById("tRevised").value; 
  var newOnHand = revised - shippedShipping; 
  
  document.getElementById("lOnHand").innerText = newOnHand; 
  
}


//****************************************************************************************************
//* doGetVendors()
//****************************************************************************************************
function getVendors()
{
  var productId = document.getElementById("tProductId").value;

  if( productId != undefined && productId.length>0 ) 
  {
    document.getElementById("tProductId").value = productId.toUpperCase();

    //XMLHttpRequest
    var newRequest = new FTD_AJAX.Request('InventoryTrackingMaintenanceAction.do', FTD_AJAX_REQUEST_TYPE_POST, getVendorsCallback, false);
    newRequest.addParam('action_type', 'product_vendor');
    newRequest.addParam('tProductId', productId.toUpperCase());
    newRequest.send();
  }
}
   

//****************************************************************************************************
//* getVendorsCallback()
//****************************************************************************************************
function getVendorsCallback(data)
{
  var originalVendorId = document.getElementById("hOriginalVendorId").value;

  //convert data to xml
  var xml = getXMLobjXML(data);

  //this will return an array set of "vendors"
  var newProductVendors = xml.getElementsByTagName("product_vendor");

  var vendorList = document.getElementById("sVendorId");

  //iterate through vendor nodes and set to null
  for( var i = vendorList.length-1; i>=0; i-- ) 
  {
      vendorList.options[i] = null;
  }
    
  var newProductName = "";
  var newProductStatus = "";
  var newProductExceptionStartDate = "";
  var newProductUpdatedBy = ""; 

  if (newProductVendors.length > 0)
  {
    newProductName               = newProductVendors.item(0).getElementsByTagName("product_name").item(0).text;
    newProductStatus             = newProductVendors.item(0).getElementsByTagName("product_status").item(0).text;
    newProductExceptionStartDate = newProductVendors.item(0).getElementsByTagName("exception_start_date").item(0).text;
    newProductUpdatedBy          = newProductVendors.item(0).getElementsByTagName("product_updated_by").item(0).text;
  }


  var newVendorId = ""; 
  var newVendorName = ""; 
  vendorList.options[0] = new Option(newVendorName, newVendorId); 

  //iterate through vendor nodes to populate vendor select
  for( var i = 0; i < newProductVendors.length; i++)
  {
    newVendorId = newProductVendors.item(i).getElementsByTagName("vendor_id").item(0).text;
    newVendorName = newProductVendors.item(i).getElementsByTagName("vendor_name").item(0).text;
    vendorList.options[i+1] = new Option(newVendorName, newVendorId); 
  }

  var newProductStatusDetail = ""; 
  if (newProductStatus == "A")
  { 
    if (newProductExceptionStartDate == "")
    {
      newProductStatusDetail = "Available"; 
    }
    else 
    {
      newProductStatusDetail = "Restricted"; 
    }
  }
  else 
  {
    if (newProductStatus == "U")
    {
      if (newProductUpdatedBy == "SYSTEM_INV_TRK")
      {
        newProductStatusDetail = "Shutdown"; 
      }
      else
      {
        newProductStatusDetail = "Unavailable"; 
      }
    }
  }
  
  document.getElementById("lProductName").innerText = newProductName; 
  document.getElementById("lProductStatus").innerText = newProductStatusDetail; 
  document.getElementById("lAvailabilityDates").innerText = newProductExceptionStartDate; 

}


//****************************************************************************************************
//* performDashboard()
//****************************************************************************************************
function performDashboard()
{
  var errorMessage = "Are you sure you want to leave the screen with unsaved changes ?  Click YES to continue or NO to return to the screen";
  var url = "InventoryTrackingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=dashboard";
  
  if (didSomethingChange())
  {
    var ret = displayConfirmError(errorMessage);
    if (ret)
      performAction(url);
  }
  else
    performAction(url);

}


//****************************************************************************************************
//* performMainMenu()
//****************************************************************************************************
function performMainMenu()
{
  var errorMessage = "Are you sure you want to leave the screen with unsaved changes ?  Click YES to continue or NO to return to the screen";
  var url = "InventoryTrackingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=main_menu";
  
  if (didSomethingChange())
  {
    var ret = displayConfirmError(errorMessage);
    if (ret)
      performAction(url);
  }
  else
    performAction(url);


}


//****************************************************************************************************
//* performCopy()
//****************************************************************************************************
function performCopy()
{
  var errorMessage = "Are you sure you want to leave the screen with unsaved changes ?  Click YES to continue or NO to return to the screen";
  var url = "InventoryTrackingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=copy";
  
  if (didSomethingChange())
  {
    var ret = displayConfirmError(errorMessage);
    if (ret)
      performAction(url);
  }
  else
    performAction(url);

}


//****************************************************************************************************
//* performSave()
//****************************************************************************************************
function performSave()
{
  if (validate())
  {
    var url = "InventoryTrackingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=save";

    //enable the disabled fields, so that Java code can pick up the values
    doEnableFields();

    performAction(url);

  }
}


//****************************************************************************************************
//* validate
//****************************************************************************************************
function validate()
{
  resetErrorFields();
  var errorMessage = "";

  //*************************
  //**PRODUCT MUST BE ENTERED
  //*************************
  if (document.getElementById("tProductId") != null && document.getElementById("tProductId").value == "")
  {
    errorMessage = errorMessage + "You must enter a product.<BR>";
    document.getElementById("tProductId").className="ErrorField";
  }


  //***************************************
  //**VENDOR MUST BE SELECTED
  //***************************************
  if (document.getElementById("sVendorId") != null && document.getElementById("sVendorId").value == "")
  {
    errorMessage = errorMessage + "You must select a vendor.<BR>";
    document.getElementById("sVendorId").className="ErrorField";
  }


  //********************************************************
  //**START DATE CANNOT BE NULL, AND IT CANNOT BE > END DATE
  //********************************************************
  //retrieve the start date
  var startDate = document.getElementById("tStartDate").value;
  var startDateTime = 0;

  if (startDate == null || startDate == "")
  {
    errorMessage = errorMessage + "You must enter a start date.<BR>";
    document.getElementById("tStartDate").className="ErrorField";
  }

  if (startDate != null && startDate != "")
  {
    //parse and store the start date
    var startDateTokens = startDate.tokenize("/", true, true);
    var startMonth = startDateTokens[0];
    var startDay = startDateTokens[1];
    var startYear = startDateTokens[2];

    //create a new start date as a date variable, using the user entered date
    var dStartDate = new Date();
    dStartDate.setMonth(startMonth - 1); //month is 0 based, so, subtract 1
    dStartDate.setYear(startYear);
    dStartDate.setDate(startDay);

    //get the time for the start date
    startDateTime = dStartDate.getTime();

  }

  //retrieve the end date
  var endDate = document.getElementById("tEndDate").value;
  var endDateTime = 0;

  if (endDate != null && endDate != "")
  {
    //parse and store the end date
    var endDateTokens = endDate.tokenize("/", true, true);
    var endMonth = endDateTokens[0];
    var endDay = endDateTokens[1];
    var endYear = endDateTokens[2];

    //create a new end date as a date variable, using the user entered date
    var dEndDate = new Date();
    dEndDate.setMonth(endMonth - 1); //month is 0 based, so, subtract 1
    dEndDate.setYear(endYear);
    dEndDate.setDate(endDay);

    //get the time for the end date
    endDateTime = dEndDate.getTime();
  }
  

  if (startDateTime > 0 && endDateTime > 0 && startDateTime > endDateTime)
  {
    errorMessage = errorMessage + "Start Date cannot be greater than the End Date.<BR>";
    document.getElementById("tStartDate").className="ErrorField";
    document.getElementById("tEndDate").className="ErrorField";
  }

  //*****************************************
  //**FORECAST MUST BE ENTERED AND BE NUMERIC
  //*****************************************
  if (document.getElementById("tForecast") != null && !isInteger(document.getElementById("tForecast").value, false))
  {
    errorMessage = errorMessage + "Forecast must be entered, and it must be numeric.<BR>";
    document.getElementById("tForecast").className="ErrorField";
  }
  
  //*****************************************
  //**REVISED MUST BE ENTERED AND BE NUMERIC
  //*****************************************
  if (document.getElementById("tRevised") != null && !isInteger(document.getElementById("tRevised").value, false))
  {
    errorMessage = errorMessage + "Revised must be entered, and it must be numeric.<BR>";
    document.getElementById("tRevised").className="ErrorField";
  }


  //**************************************
  //**WARNING, IF ENTERED, MUST BE NUMERIC
  //**************************************
  if (document.getElementById("tWarningThreshold") != null && !isSignedInteger(document.getElementById("tWarningThreshold").value, true))
  {
    errorMessage = errorMessage + "Warning Threshold must be numeric.<BR>";
    document.getElementById("tWarningThreshold").className="ErrorField";
  }


  //***************************************
  //**SHUTDOWN, MUST BE ENTERED AND MUST BE NUMERIC
  //***************************************
  if (document.getElementById("tShutdownThreshold") != null && !isSignedInteger(document.getElementById("tShutdownThreshold").value, false))
  {
    errorMessage = errorMessage + "Shutdown Threshold must be entered, and it must be numeric.<BR>";
    document.getElementById("tShutdownThreshold").className="ErrorField";
  }


  //*****************************
  //**EMAIL ADDRESS MUST BE VALID
  //*****************************
  //if object exists and user has entered something, check the validity
  if (document.getElementById("tNewEmailAddress") != null && 
      document.getElementById("tNewEmailAddress").value.length > 0 && 
      !isEmail(document.getElementById("tNewEmailAddress").value)
     )
  {
    errorMessage = errorMessage + "Email address is invalid.<BR>";
    document.getElementById("tNewEmailAddress").className="ErrorField";
  }




  if (errorMessage != null && errorMessage != "")
  {
    displayOkError(errorMessage);
    return false;
  }
  else
    return true;



}


//****************************************************************************************************
//* resetErrorFields
//****************************************************************************************************
function resetErrorFields()
{
  if (document.getElementById("tProductId") != null)
    document.getElementById("tProductId").className="input";

  if (document.getElementById("sVendorId") != null)
    document.getElementById("sVendorId").className="input";

  if(document.getElementById("tForecast") != null)
    document.getElementById("tForecast").className="input";

  if(document.getElementById("tRevised") != null)
    document.getElementById("tRevised").className="input";

  if(document.getElementById("tWarningThreshold") != null)
    document.getElementById("tWarningThreshold").className="input";

  if(document.getElementById("tShutdownThreshold") != null)
    document.getElementById("tShutdownThreshold").className="input";

  if(document.getElementById("tNewEmailAddress") != null)
    document.getElementById("tNewEmailAddress").className="input";

  document.getElementById("tStartDate").className="input";
  document.getElementById("tEndDate").className="input";
  document.getElementById("tWarningThreshold").className="input";
  document.getElementById("tShutdownThreshold").className="input";  
  document.getElementById("taComments").className="input";

}


//****************************************************************************************************
//* doEnableFields() - this will enable fields so that the back-end code receives non-Null values
//****************************************************************************************************
function doEnableFields()
{
  document.getElementById('tStartDate').disabled = false;
  document.getElementById('tEndDate').disabled = false;

}



//***************************************************************************************************
// clearField()
//***************************************************************************************************/
function clearField(field)
{
  document.getElementById(field).value = ""
}


//***************************************************************************************************
// didSomethingChange()
//***************************************************************************************************/
function didSomethingChange()
{
  resetErrorFields();
  var ret = false; 

  //special characters to be removed
  var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

  var newStartDate = document.getElementById('tStartDate');
  var newEndDate = document.getElementById('tEndDate');


  if (somethingChanged)
    ret = true; 
  

  //for START DATE - retrieve the new value, and check it against the original value
  var newStartDateValue = stripCharsInBag(newStartDate.value, bag);
  if (newStartDateValue != originalStartDateValue)
    ret = true;


  //for END DATE - retrieve the new value, and check it against the original value
  var newEndDateValue = stripCharsInBag(newEndDate.value, bag);

  if (newEndDateValue == '')
    newEndDateValue = '12319999';

  if (newEndDateValue != originalEndDateValue)
    ret = true;

  return ret;

}

//****************************************************************************************************
//* doTakenPopup
//****************************************************************************************************
function doTakenPopup(productId)
{
  var url = "InventoryTrackingDashboardAction.do"  + getSecurityParams(true) + "&action_type=taken&taProductIds=" + productId;
  var sFeatures="dialogHeight:275px;dialogWidth:800px;";
  sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";

  showModalDialogIFrame(url, "" ,sFeatures);


}
