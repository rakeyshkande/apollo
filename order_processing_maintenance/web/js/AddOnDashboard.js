//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;

  
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
  showWaitMessage("content", "wait", "Processing");
  document.forms[0].action = url;
  document.forms[0].submit();
}



//****************************************************************************************************
//* performAddNew
//****************************************************************************************************
function performAdd()
{
  document.getElementById("action_type").value = "add";

  var url = "AddOnMaintenanceAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=add";
  performAction(url);
}



//****************************************************************************************************
//* performMainMenu()
//****************************************************************************************************
function performMainMenu()
{
  document.getElementById("action_type").value = "main_menu";

  var url = "AddOnDashboardAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=main_menu";
  performAction(url);
}


//****************************************************************************************************
//* doEdit
//****************************************************************************************************
function performEdit()
{
  var addOnId = retrieveAddOnId();
  if (addOnId == null)
  {
    //set the message
    var errorMessage = "Please select an Add-On Id to edit.";

    //display the popup
    displayOkError(errorMessage);
  }
  else
  {
    var url = "AddOnDashboardAction.do" + getSecurityParams(true) + "&action_type=edit" + "&add_on_id=" + addOnId;
    performAction(url);
  }
}

//****************************************************************************************************
//* retrieveAddOnId
//****************************************************************************************************
function retrieveAddOnId()
{
  var radio = document.forms[0].addOnIdRadio;
  var radioSelected;

  if (radio != null)
  {
    if (radio.length == null)
    {
      if (radio.checked)
        radioSelected = radio;
      else
        radioSelected = null;
    }
    else
    {
      for ( var i = 0; i < radio.length; i++){
        if ( radio[i].checked ){
          radioSelected = radio[i];
        }
      }
    }


    // if radio was not selected, return null
    if ( radioSelected == null )
    {
      return null;
    }
    // else return the radio value
    else
    {
      var addOnId = radioSelected.value;
      return addOnId;
    }
  }
}
