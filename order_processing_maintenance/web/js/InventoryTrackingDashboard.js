//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init() {
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
  var recordCount = 0;
  var hRecCountRef = document.getElementById("hRecordCount"); 
  if(hRecCountRef && hRecCountRef.value != '') {
	recordCount = parseInt(hRecCountRef.value);
  }
  var totalPages = 0;
  var hTotalPagesRef = document.getElementById("hTotalPages");
  if(hTotalPagesRef && hTotalPagesRef.value != '') {	
	totalPages = parseInt(hTotalPagesRef.value);
  }
  //recordCount is total count and not page count with new SKU-IT change it to pageSize
  
 	if(totalPages == null || totalPages == '' || parseInt(totalPages) <= 1) {		
		document.getElementById("paginationDiv").style.display='none';
 	}
	if((totalPages != null) && (totalPages != '') && (parseInt(totalPages) >= 1)) {
		var currPageRecCount;
  		var recordsPerPage = 0;
  		var currentPage = 1;
  		var hCurrentpageRef = document.getElementById("hCurrentpage");
  		var pageSizeRef = document.getElementById("pageSize");
  		if(hCurrentpageRef && hCurrentpageRef.value != ''){
  			currentPage = parseInt(hCurrentpageRef.value);
  		}  		
		if(pageSizeRef && pageSizeRef.value != '' && parseInt(pageSizeRef.value) > 0 ) {
			recordsPerPage = parseInt(pageSizeRef.value);
  		}
		if(recordCount <= recordsPerPage){
			currPageRecCount = recordCount;
		} else if((recordCount>recordsPerPage) && (currentPage<totalPages)){
			currPageRecCount = recordsPerPage;
		} else{
			currPageRecCount = recordCount - recordsPerPage*(currentPage-1);				
		}
		for (var i = 0; i < currPageRecCount; i++) {
			newTableData[i] = new Array(
			document.getElementById("cbval_" + i).value,
			document.getElementById("td_" + i + "_01").innerText,
			document.getElementById("td_" + i + "_02").innerText,
			document.getElementById("td_" + i + "_03").innerText,
			document.getElementById("td_" + i + "_04").innerText,
			document.getElementById("td_" + i + "_05").innerText,
			document.getElementById("td_" + i + "_06").innerText,
			document.getElementById("td_" + i + "_07").innerText,
			document.getElementById("td_" + i + "_08").innerText,
			document.getElementById("td_" + i + "_09").innerText,
			document.getElementById("td_" + i + "_10").innerText,
			document.getElementById("td_" + i + "_11").innerText,
			document.getElementById("td_" + i + "_12").innerText,
			document.getElementById("td_" + i + "_13").innerText,
			document.getElementById("td_" + i + "_14").innerText,
			document.getElementById("td_" + i + "_15").innerText);
		}
 	}
  	setupCheckBox('NOVATOR_UPDATE_LIVE',liveFeedAllowed,liveFeedOn);
  	setupCheckBox('NOVATOR_UPDATE_CONTENT',contentFeedAllowed,contentFeedOn);
  	setupCheckBox('NOVATOR_UPDATE_TEST',testFeedAllowed,testFeedOn);
  	setupCheckBox('NOVATOR_UPDATE_UAT',uatFeedAllowed,uatFeedOn); 
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
  showWaitMessage("content", "wait", "Processing");
  document.forms[0].action = url;
  document.forms[0].submit();
}


//****************************************************************************************************
//* performMoveSelectedAll
//****************************************************************************************************
function performMoveSelectedAll()
{
  var vendorsAvailableDropDown = document.getElementById("sVendorsAvailable");

  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
    vendorsAvailableDropDown[i].selected = true;
  }
  performMoveSelected();

}


//****************************************************************************************************
//* performMoveSelected
//****************************************************************************************************
function performMoveSelected()
{
  var vendorsAvailableDropDown = document.getElementById("sVendorsAvailable");
  var vendorsChosenDropDown  = document.getElementById("sVendorsChosen");

  var newVendorsAvailableArrayList = new Array();
  var newVendorsChosenArrayList = new Array();

  //create existing Chosen list
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
    newVendorsChosenArrayList[i] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
  }

  //define index positions for the arrays - new data will be added according to these positions
  var newVendorsChosenIndex = newVendorsChosenArrayList.length - 1;
  var newVendorsAvailableIndex = -1;

  //(1) create new Available ArrayList
  //(2) append to Chosen ArrayList
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
    if (vendorsAvailableDropDown[i].selected)
    {
      newVendorsChosenIndex++;
      newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
    }
    else
    {
      newVendorsAvailableIndex++;
      newVendorsAvailableArrayList[newVendorsAvailableIndex] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
    }
  }

  //sort Available ArrayList
  bubbleSortAsc(newVendorsAvailableArrayList, newVendorsAvailableArrayList.length, 1);

  //sort Chosen ArrayList
  bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1);

  //remove options from existing Available select dropdown
  for (var i = vendorsAvailableDropDown.length - 1; i >= 0 ; i--)
  {
    vendorsAvailableDropDown.remove(i);
  }    

  //remove options from existing Chosen select dropdown
  for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  {
    vendorsChosenDropDown.remove(i);
  }    


  //add items from existing Available select/options
  for (var i = 0; i < newVendorsAvailableArrayList.length; i++)
  {
    //Create a new option  - definition = new Option(text, value, defaultSelected, selected)
    var newVendorsAvailableOption = new Option(newVendorsAvailableArrayList[i][1], newVendorsAvailableArrayList[i][0], false, false);

    //and add it to the drop down
    vendorsAvailableDropDown.add(newVendorsAvailableOption);
  }    

  //add items from existing Available select/options
  for (var i = 0; i < newVendorsChosenArrayList.length; i++)
  {
    //Create a new option  - definition = new Option(text, value, defaultSelected, selected)
    var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][1], newVendorsChosenArrayList[i][0], false, false);

    //and add it to the drop down
    vendorsChosenDropDown.add(newVendorsChosenOption);
  }    

}

//****************************************************************************************************
//* performMoveUnSelectedAll
//****************************************************************************************************
function performMoveUnSelectedAll()
{
  var vendorsChosenDropDown = document.getElementById("sVendorsChosen");

  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
    vendorsChosenDropDown[i].selected = true;
  }
  performMoveUnSelected();

}


//****************************************************************************************************
//* performMoveUnSelected
//****************************************************************************************************
function performMoveUnSelected()
{
  var vendorsChosenDropDown = document.getElementById("sVendorsChosen");
  var vendorsAvailableDropDown  = document.getElementById("sVendorsAvailable");

  var newVendorsChosenArrayList = new Array();
  var newVendorsAvailableArrayList = new Array();

  //create existing Available list
  for (var i = 0; i < vendorsAvailableDropDown.length; i++)
  {
    newVendorsAvailableArrayList[i] = new Array(vendorsAvailableDropDown[i].value, vendorsAvailableDropDown[i].innerText)
  }

  //define index positions for the arrays - new data will be added according to these positions
  var newVendorsAvailableIndex = newVendorsAvailableArrayList.length - 1;
  var newVendorsChosenIndex = -1;

  //(1) create new Chosen ArrayList
  //(2) append to Available ArrayList
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
    if (vendorsChosenDropDown[i].selected)
    {
      newVendorsAvailableIndex++;
      newVendorsAvailableArrayList[newVendorsAvailableIndex] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
    }
    else
    {
      newVendorsChosenIndex++;
      newVendorsChosenArrayList[newVendorsChosenIndex] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
    }
  }

  //sort Chosen ArrayList
  bubbleSortAsc(newVendorsChosenArrayList, newVendorsChosenArrayList.length, 1);

  //sort Available ArrayList
  bubbleSortAsc(newVendorsAvailableArrayList, newVendorsAvailableArrayList.length, 1);

  //remove options from existing Chosen select dropdown
  for (var i = vendorsChosenDropDown.length - 1; i >= 0 ; i--)
  {
    vendorsChosenDropDown.remove(i);
  }    

  //remove options from existing Available select dropdown
  for (var i = vendorsAvailableDropDown.length - 1; i >= 0 ; i--)
  {
    vendorsAvailableDropDown.remove(i);
  }    


  //add items from existing Chosen select/options
  for (var i = 0; i < newVendorsChosenArrayList.length; i++)
  {
    //Create a new option  - definition = new Option(text, value, defaultSelected, selected)
    var newVendorsChosenOption = new Option(newVendorsChosenArrayList[i][1], newVendorsChosenArrayList[i][0], false, false);

    //and add it to the drop down
    vendorsChosenDropDown.add(newVendorsChosenOption);
  }    

  //add items from existing Chosen select/options
  for (var i = 0; i < newVendorsAvailableArrayList.length; i++)
  {
    //Create a new option  - definition = new Option(text, value, defaultSelected, selected)
    var newVendorsAvailableOption = new Option(newVendorsAvailableArrayList[i][1], newVendorsAvailableArrayList[i][0], false, false);

    //and add it to the drop down
    vendorsAvailableDropDown.add(newVendorsAvailableOption);
  }    

}


//****************************************************************************************************
//* bubbleSortAsc
//****************************************************************************************************
function bubbleSortAsc(arrayName,length,sortBy)
{
  for (var i=0; i<(length-1); i++)
  {
    for (var j=i+1; j<length; j++)
    {
      if (arrayName[j][sortBy] < arrayName[i][sortBy])
      {
        var dummy = arrayName[i];
        arrayName[i] = arrayName[j];
        arrayName[j] = dummy;
      }
    }
  }
}


//****************************************************************************************************
//* bubbleSortDesc
//****************************************************************************************************
function bubbleSortDesc(arrayName,length,sortBy)
{
  for (var i=0; i<(length-1); i++)
  {
    for (var j=i+1; j<length; j++)
    {
      if (arrayName[j][sortBy] >= arrayName[i][sortBy])
      {
        var dummy = arrayName[i];
        arrayName[i] = arrayName[j];
        arrayName[j] = dummy;
      }
    }
  }
}


//****************************************************************************************************
//* shellSortAsc
//****************************************************************************************************
function shellSortAsc(arrayName,length,sortBy)
{
  var j, i, v, h=1, s=3, k;
  while(h < length)
    h=s*h+1;
  while(h > 1) {
    h=(h-1)/s;
    for (k=0; k<h; k++)
      for (i=k+h, j=i; i<length; i+=h, j=i) {
        v=arrayName[i];
        while(true)
          if ((j-=h) >= 0 && arrayName[j][sortBy] > v[sortBy])
            arrayName[j+h]=arrayName[j];
          else
            break;
        arrayName[j+h]=v;
      }
  }

}

//****************************************************************************************************
//* sortFields
//****************************************************************************************************
function sortFields(columnNumber, sortType)
{

  var newBackgroundColor;

  //sort the ArrayList
  if (sortType == 'asc')
    bubbleSortAsc(newTableData, newTableData.length, columnNumber);
    //shellSortAsc(newTableData, newTableData.length, columnNumber);
  else
    bubbleSortDesc(newTableData, newTableData.length, columnNumber);


  //repopulate the table grid, and set the hidden values
  for (var i = 0; i < newTableData.length; i++)
  {
    newBackgroundColor = "white";
    if (Trim(newTableData[i][8]) == "ALL VENDORS")
      newBackgroundColor = "lightgrey";

    if (newTableData[i][0] != 0)
    {
      //set the value of the hidden field
      document.getElementById("cbval_" + i).value = newTableData[i][0];

      //empty the TD and add a new checkbox
      var newCheckbox = document.createElement('input');
      newCheckbox.setAttribute('type', 'checkbox');
      newCheckbox.setAttribute('id','cb_'+ i);
      newCheckbox.setAttribute('name','cb_'+ i);
      newCheckbox.setAttribute('value', newTableData[i][0]);

      document.getElementById("td_" + i + "_00").innerText = "";
      document.getElementById("td_" + i + "_00").appendChild(newCheckbox);
      
    }
    else
    {
      //set the value of the hidden field
      document.getElementById("cbval_" + i).value = 0;

      //empty the TD
      document.getElementById("td_" + i + "_00").innerText = " ";
    }

    document.getElementById("td_" + i + "_01").innerText = newTableData[i][1];
    document.getElementById("td_" + i + "_02").innerText = newTableData[i][2];
    document.getElementById("td_" + i + "_03").innerText = newTableData[i][3];
    document.getElementById("td_" + i + "_04").innerText = newTableData[i][4];
    document.getElementById("td_" + i + "_05").innerText = newTableData[i][5];
    document.getElementById("td_" + i + "_06").innerText = newTableData[i][6];
    document.getElementById("td_" + i + "_07").innerText = newTableData[i][7];
    document.getElementById("td_" + i + "_08").innerText = newTableData[i][8];
    document.getElementById("td_" + i + "_09").innerText = newTableData[i][9];
    document.getElementById("td_" + i + "_10").innerText = newTableData[i][10];
    document.getElementById("td_" + i + "_11").innerText = newTableData[i][11];
    document.getElementById("td_" + i + "_12").innerText = newTableData[i][12];
    document.getElementById("td_" + i + "_13").innerText = newTableData[i][13];
    document.getElementById("td_" + i + "_14").innerText = newTableData[i][14];


    if (Trim(newTableData[i][8]) == "ALL VENDORS")
    {

      //empty the TD and add a new link

      var productId = newTableData[i][3]; 
      var newLink = document.createElement('a');

      newLink.setAttribute('href', '#');
      newLink.onclick = function()  { doTakenPopup(productId); } ;
      newLink.setAttribute('innerText', 'Taken');
      
      document.getElementById("td_" + i + "_15").innerText = "";
      document.getElementById("td_" + i + "_15").appendChild(newLink);
    }
    else
    {
      document.getElementById("td_" + i + "_15").innerText = newTableData[i][15];
    }

    document.getElementById("td_" + i + "_00").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_01").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_02").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_03").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_04").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_05").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_06").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_07").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_08").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_09").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_10").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_11").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_12").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_13").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_14").style.backgroundColor = newBackgroundColor;
    document.getElementById("td_" + i + "_15").style.backgroundColor = newBackgroundColor;
  }                                      


}                                        


//****************************************************************************************************
//* performSearch
//****************************************************************************************************
function performSearch()
{
  if (validateSearch())
  {
    document.getElementById("hVendorsChosen").value = getVendorsChosen();
    document.getElementById("action_type").value = "search";

    var url = "InventoryTrackingDashboardAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=search";

    //enable the disabled fields, so that Java code can pick up the values
    doEnableFields();

    performAction(url);

  }
  
}


//****************************************************************************************************
//* validateSearch
//****************************************************************************************************
function validateSearch()
{
  resetErrorFields();
  var errorMessage = "";


  //*********************************
  //**START DATE CANNOT BE > END DATE
  //*********************************
  //retrieve the start date
  var startDate = document.getElementById("tStartDate").value;
  var startDateTime = 0;

  if (startDate != null && startDate != "")
  {
    //parse and store the start date
    var startDateTokens = startDate.tokenize("/", true, true);
    var startMonth = startDateTokens[0];
    var startDay = startDateTokens[1];
    var startYear = startDateTokens[2];

    //create a new start date as a date variable, using the user entered date
    var dStartDate = new Date();
    dStartDate.setMonth(startMonth - 1); //month is 0 based, so, subtract 1
    dStartDate.setYear(startYear);
    dStartDate.setDate(startDay);

    //get the time for the start date
    startDateTime = dStartDate.getTime();

  }

  //retrieve the end date
  var endDate = document.getElementById("tEndDate").value;
  var endDateTime = 0;

  if (endDate != null && endDate != "")
  {
    //parse and store the end date
    var endDateTokens = endDate.tokenize("/", true, true);
    var endMonth = endDateTokens[0];
    var endDay = endDateTokens[1];
    var endYear = endDateTokens[2];

    //create a new end date as a date variable, using the user entered date
    var dEndDate = new Date();
    dEndDate.setMonth(endMonth - 1); //month is 0 based, so, subtract 1
    dEndDate.setYear(endYear);
    dEndDate.setDate(endDay);

    //get the time for the end date
    endDateTime = dEndDate.getTime();
  }
  

  if (startDateTime > 0 && endDateTime > 0 && startDateTime > endDateTime)
  {
    errorMessage = errorMessage + "Start Date cannot be greater than the End Date.<BR>";
    document.getElementById("tStartDate").className="ErrorField";
    document.getElementById("tEndDate").className="ErrorField";
  }


  //*******************************************************************************
  //**retrieve products, and remove line breaks before implementing any validations
  //*******************************************************************************
  var productIds = document.getElementById("taProductIds").value;

  var regExp = new RegExp("\n","g");
  var regExp1 = new RegExp("\r","g");
  productIds = productIds.replace(regExp,"");
  productIds = productIds.replace(regExp1,"");
  
  if (productIds.length == 0 || productIds == "")
    document.getElementById("taProductIds").value = "";
  

  //***************************************
  //**PRODUCT AND/OR VENDOR MUST BE ENTERED
  //***************************************
  var vendorsChosenArrayList = new Array();
  var vendorsChosenDropDown  = document.getElementById("sVendorsChosen");
  //create existing Chosen list
  for (var i = 0; i < vendorsChosenDropDown.length; i++)
  {
    vendorsChosenArrayList[i] = new Array(vendorsChosenDropDown[i].value, vendorsChosenDropDown[i].innerText)
  }

  if (productIds == "" && vendorsChosenArrayList.length == 0 )
  {
    errorMessage = errorMessage + "You must enter either a vendor or a product.<BR>";
    document.getElementById("taProductIds").className="ErrorField";
    document.getElementById("sVendorsChosen").className="ErrorField";
  }


  //***************************************
  //**PRODUCT CANNOT EXCEED 400 BYTES
  //***************************************
  if (productIds.length > 400)
  {
    errorMessage = errorMessage + "You can only enter 400 characters in Product Ids.<BR>";
    document.getElementById("taProductIds").className="ErrorField";
  }

  
  //*********************************************************************
  //**PRODUCT CANNOT HAVE ANY CHARACTERS EXCEPT ALPHANUMERIC and NEW LINE
  //*********************************************************************
  var x = 0;
  var productIdChar; 
  var validCharactersFound = true; 
  
  for (x = 0 ; x < productIds.length; x++)
  {
    productIdChar = productIds.substring(x,x+1); 
    // #17768: including hyphen(-) to the productId character set 
    validCharactersFound = validateProductIdChar(productIdChar);
    if (!validCharactersFound)
      break; 
  }
  
  if (!validCharactersFound)
  {
    errorMessage = errorMessage + "Please enter valid Product IDs.<BR>";
    document.getElementById("taProductIds").className="ErrorField";
  }
  
  
  //*************************
  //**ON-HAND MUST BE NUMERIC
  //*************************
  if (!isInteger(document.getElementById("tOnHand").value, true))
  {
    errorMessage = errorMessage + "O/H must be a postive whole number.<BR>";
    document.getElementById("tOnHand").className="ErrorField";
  }


  if (errorMessage != null && errorMessage != "")
  {
    displayOkError(errorMessage);
    return false;
  }
  else
    return true;
  

}


//****************************************************************************************************
//* resetErrorFields
//****************************************************************************************************
function resetErrorFields()
{
  document.getElementById("tStartDate").className="input";
  document.getElementById("tEndDate").className="input";
  document.getElementById("taProductIds").className="input";
  document.getElementById("sVendorsChosen").className="input";
  document.getElementById("tOnHand").className="input";
  document.getElementById("fileInputExcelFile").className="input";

}


//****************************************************************************************************
//* doEnableFields() - this will enable fields so that the back-end code receives non-Null values
//****************************************************************************************************
function doEnableFields()
{
  document.getElementById('tStartDate').disabled = false;
  document.getElementById('tEndDate').disabled = false;
  document.getElementById('sVendorsChosen').disabled = false;

}



//***************************************************************************************************
// getVendorsChosen()
//***************************************************************************************************/
function getVendorsChosen()
{
  var vc = document.getElementById("sVendorsChosen");
  var vendorsChosen = "";

  for (var i = 0; i < vc.length; i++)
  {
    vendorsChosen += vc[i].value + ",";
  }

  vendorsChosen = vendorsChosen.substring(0,vendorsChosen.length - 1)
  return vendorsChosen;

}


//***************************************************************************************************
// clearField()
//***************************************************************************************************/
function clearField(field)
{
  document.getElementById(field).value = ""
}


//***************************************************************************************************
// checkAll()
//***************************************************************************************************/
function checkAll()
{
  var cb;
  for (var i = 0; i < newTableData.length; i++)
  {
     cb = document.getElementById("cb_"+ i);
     if (cb != null)
       cb.checked = true;
  }
}


//***************************************************************************************************
// uncheckAll()
//***************************************************************************************************/
function uncheckAll()
{
  var cb;
  for (var i = 0; i < newTableData.length; i++)
  {
     cb = document.getElementById("cb_"+ i);
     if (cb != null)
       cb.checked = false;
  }
}


//****************************************************************************************************
//* performAddNew
//****************************************************************************************************
function performAddNew()
{
  document.getElementById("action_type").value = "add";

  var url = "InventoryTrackingMaintenanceAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=add";
  performAction(url);
}


//****************************************************************************************************
//* performGo
//****************************************************************************************************
function performGo()
{
  var actionOptions = document.getElementById("sActionOptions");
  var actionOptionsValue = actionOptions.options[actionOptions.selectedIndex].value;
  var actionOptionsText = actionOptions.options[actionOptions.selectedIndex].text; 

  //set the message
  var errorMessage = "";




  var cb;
  var cbCheckedCount = 0;
  var url; 

  for (var i = 0; i < newTableData.length; i++)
  {
    cb = document.getElementById("cb_"+ i);
    if (cb != null && cb.checked)
    {
      cbCheckedCount++; 
    }
     
  }


  if (cbCheckedCount == 0)
  {
    errorMessage = "Please select an inventory tracking record to " + actionOptionsText; 
    displayOkError(errorMessage);    
  }
  else if (cbCheckedCount > 1 && actionOptionsValue == "edit")
  {
    errorMessage = "Please select only one inventory tracking record to " + actionOptionsText; 
    displayOkError(errorMessage);    
  }
  else
  {
    if (actionOptionsValue == "delete")
    {
      errorMessage = "Are you sure you want to delete these record(s)?  Click YES to continue or NO to return to the screen"; 
      var ret = displayConfirmError(errorMessage);
      if (ret) 
      {
        document.getElementById("hVendorsChosen").value = getVendorsChosen();
        document.getElementById("action_type").value = actionOptionsValue;

        url = "InventoryTrackingDashboardAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=" + actionOptionsValue;

        //enable the disabled fields, so that Java code can pick up the values
        doEnableFields();

        performAction(url);
      }
    }
    else
    {
      document.getElementById("hVendorsChosen").value = getVendorsChosen();
      document.getElementById("action_type").value = actionOptionsValue;

      url = "InventoryTrackingDashboardAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=" + actionOptionsValue;

      //enable the disabled fields, so that Java code can pick up the values
      doEnableFields();

      performAction(url);
    }
  }

}


//****************************************************************************************************
//* doTakenPopup
//****************************************************************************************************
function doTakenPopup(productId)
{
  document.getElementById("action_type").value = "taken"; 

  var url = "InventoryTrackingDashboardAction.do"  + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=taken&taProductIds=" + productId;
  var sFeatures="dialogHeight:275px;dialogWidth:800px;";
  sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";

  showModalDialogIFrame(url, "" ,sFeatures);


}


//****************************************************************************************************
//* performMainMenu()
//****************************************************************************************************
function performMainMenu()
{
  document.getElementById("action_type").value = "main_menu";

  var url = "InventoryTrackingDashboardAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=main_menu";
  performAction(url);
}


//****************************************************************************************************
//* performUpload
//****************************************************************************************************
function performUpload()
{

  if (validateUpload())
  {
    document.getElementById("hInputExcelFileName").value = document.getElementById("fileInputExcelFile").value; 
    document.getElementById("action_type").value = "upload";

    var url = "InventoryTrackingDashboardAction.do" + getSecurityParams(true) + "&adminAction=" + document.getElementById("adminAction").value + "&action_type=upload";

    //enable the disabled fields, so that Java code can pick up the values
    doEnableFields();

    performAction(url);
  }

}


//****************************************************************************************************
//* validateUpload
//****************************************************************************************************
function validateUpload()
{

  resetErrorFields();
  var errorMessage = "";

  var fileName = document.getElementById("fileInputExcelFile").value;
  
  if (Trim(fileName) == "")
  {
    errorMessage = "You must enter a file name to upload";
    document.getElementById("fileInputExcelFile").className="ErrorField";
  }
  else
  {
    var extension = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    if (extension != "xls" && extension != "XLS" && extension != "xlsx" && extension != "XLSX")
    {
      errorMessage = "You must choose an Excel file ending in .xls or .xlsx to upload";
      document.getElementById("fileInputExcelFile").className="ErrorField";
    }
  }
 

  if (errorMessage != null && errorMessage != "")
  {
    displayOkError(errorMessage);
    return false;
  }
  else
    return true;
 

}

function performInvSearchOnInput(){
	var showingPage = document.getElementById("showingPage").value;
	var totalPages = document.getElementById("hTotalPages").value;
	var currentPage = document.getElementById("hCurrentpage").value;
	if(!isPositiveInteger(showingPage) || parseInt(showingPage) > parseInt(totalPages) ){
		var errorMessage = "Pagination input must be numeric between 1 and "+totalPages;
		document.getElementById("showingPage").className="ErrorField";
		displayOkError(errorMessage);
		document.getElementById("showingPage").value = currentPage;
		return false;
	}
	if(isPositiveInteger(showingPage) && parseInt(showingPage)!=parseInt(currentPage) && parseInt(showingPage) <= parseInt(totalPages) ){
		document.getElementById("hCurrentpage").value = showingPage;
		document.getElementById("paginationAction").value = "yes";            
		performSearch();   
	}
	else{
		return false;
	}
}

function performInvSearchLeft(currPage){
	currPage = parseInt(currPage);
	if(currPage > 1){
		document.getElementById("hCurrentpage").value = currPage-1;
		document.getElementById("paginationAction").value = "yes";							
		performSearch();
	}
}

function performInvSearchRight(currPage){
	currPage = parseInt(currPage);
	if(currPage<parseInt(document.getElementById("hTotalPages").value)){
		document.getElementById("hCurrentpage").value = currPage+1;
		document.getElementById("paginationAction").value = "yes";							
		performSearch();
	}
}

//****************************************************************************************************
//* performDashboard
//****************************************************************************************************
function performDashboard()
{
	  document.getElementById("action_type").value = "load";
	  var url = "InventoryTrackingDashboardAction.do" + getSecurityParams(true) + "&action_type=load";
	  performAction(url);
}


function validateProductIdChar(c){
	return (c=="-" || isLetterOrDigit(c));
}

function invalidateEnterKey(e){
	if (e.keyCode == 13) {	
		e.cancelBubble = true;
		e.returnValue = false;
	}
}