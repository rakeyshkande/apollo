/*
 * Constructor
 */
CityPopup = function(params) {
   this.focusObj = null;
   this.cityName = params.city;
   this.stateName = params.state;
   this.zipName = params.zip;
   this.countryName = params.country;
};

CityPopup._C = null;

/*
 * Class functions
 */
CityPopup.setup = function(params) {
   window.popup = Popup.setup(params, new CityPopup(params));
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

CityPopup.enterCitySearch = function(ev) {
   if (ev.keyCode == 13)
      CityPopup.searchCity(ev);
};

CityPopup.searchCity = function(ev) {
   //First validate the inputs
   var check = true;
   var state = document.getElementById("stateInput");
   var city = document.getElementById("cityInput");
   var zip = document.getElementById("zipCodeInput");

   //State is required if zip is empty
   if((stripWhitespace(state.value).length == 0) && (stripWhitespace(zip.value).length == 0)) {
     if (check == true) {
       state.focus();
       check = false;
    }
     state.style.backgroundColor = 'pink';
   }

   //City is required if zip is empty
   if((stripWhitespace(city.value).length == 0) && (stripWhitespace(zip.value).length == 0)) {
     if (check == true) {

       city.focus();
       check = false;
     }
     city.style.backgroundColor = 'pink';
   }

   if (!check) {
     alert("Please correct the marked fields")
     return false;
   }

   //Now that everything is valid, open the popup after removing special characters
   var bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
   var cityVal = stripCharsInBag(city.value, bag2);
   var stateVal = stripCharsInBag(state.value, bag2);
   var zipVal = stripCharsInBag(zip.value, bag2);
   var countryInput = document.getElementById(CityPopup._C.countryName).value;

   var form = document.forms[0];
   var url_source="lookupLocation.do?" +
   "&cityInput=" + cityVal +
   "&stateInput=" + stateVal +
   "&zipCodeInput=" + zipVal +
   "&countryInput=" + countryInput + getSecurityParams(false);

   //Open the window
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = window.showModalDialog(url_source, "", modal_dim);

   // Set the new city, state, and zip
   if (ret && ret != null && ret[0] != ""){
       document.getElementById(CityPopup._C.cityName).value = ret[0];
       setSelectedState(document.getElementById(CityPopup._C.stateName), ret[1]);
       document.getElementById(CityPopup._C.zipName).value = ret[2];
   }
   Popup.hide();
   CityPopup._C = null;
};


/*
 * Member Functions
 */
CityPopup.prototype.renderContent = function(div) {
   CityPopup._C = this;
   
   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   table.setAttribute("className", "LookupTable");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("City Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   // City
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("City:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "cityInput");
   input.setAttribute("tabIndex", "96");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // State
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("State:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("select", cell);
   input.setAttribute("id", "stateInput");
   input.setAttribute("tabIndex", "97");

   // Zip
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Zip/Postal Code:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "zipCodeInput");
   input.setAttribute("tabIndex", "98");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Buttons
   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Search");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "99");
   Popup.addEvent(button, "click", CityPopup.searchCity);
   Popup.addEvent(button, "keydown", CityPopup.enterCitySearch);

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Close screen");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "100");
   Popup.addEvent(button, "click", Popup.hide);
   Popup.addEvent(button, "keydown", Popup.pressHide);
};

CityPopup.prototype.setFocus = function() {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

CityPopup.prototype.setValues = function() {
   var sState = document.getElementById(CityPopup._C.stateName);
   document.getElementById("cityInput").value = document.getElementById(CityPopup._C.cityName).value;
   document.getElementById("zipCodeInput").value = document.getElementById(CityPopup._C.zipName).value;
   populateStates(CityPopup._C.countryName, "stateInput", sState[sState.selectedIndex].value);

};

/* Populates a select box containing states using the supplied parameters.  If 'selectedState'
 * whitespace only or undefined, the select box will default to the first option.
 *
 * @param countryHiddenName  The name of the hidden containing a country
 * @param stateSelectBoxName    The name of the select box containing a list of states
 * @param selectedState         The value to set on the stateSelectBoxName.  Example: 'IL'
 */
var ORIG_COUNTRY;
function populateStates(countryHiddenName, stateSelectBoxName, selectedState){
  var countryHidden = document.getElementById(countryHiddenName);
  // in a hidden on the page //
  var selectedCountry = countryHidden.value;
  var toPopulate = (selectedCountry == "US") ? usStates : (selectedCountry == "CA") ? caStates : naStates;
  ORIG_COUNTRY = selectedCountry;
  var states = document.getElementById(stateSelectBoxName);
  states.options.length = 0;
 for (var i = 0; i < toPopulate.length; i++){
    states.add(new Option(toPopulate[i][1], toPopulate[i][0], false, false));
  }
  setSelectedState(states, selectedState);
}

/* Sets a select box containing states to the 'state' supplied.  If 'selectedState'
 * whitespace only or undefined, the select box will default to the first option.
 *
 * @param selectBoxObj  The select box object containing a list of states
 * @param state         The value to set on the stateSelectBoxName.  Example: 'IL'
 */
function setSelectedState(selectBoxObj, state){
  if (state != "" && (!isEmpty(state))) {
    for (var i = 0; i < selectBoxObj.length; i++) {
      if (selectBoxObj[i].value == state) {
        selectBoxObj.selectedIndex = i;
        selectBoxObj.options[i].defaultSelected = true;
        break;
      }
    }
  }
}
/*

// Check whether string s is empty.

function isEmpty(s)
{   return ((s == null) || (s.length == 0))
// Removes all characters which appear in string bag from string s.

function stripCharsInBag (s, bag)

{   var i;
    var returnString = "";

    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.

    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }

    return returnString;
}
}

// Removes all whitespace characters from s.
// Global variable whitespace (see above)
// defines which characters are considered whitespace.

function stripWhitespace (s)

{   return stripCharsInBag (s, whitespace)
}

// Removes all characters which appear in string bag from string s.

function stripCharsInBag (s, bag)

{   var i;
    var returnString = "";

    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.

    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }

    return returnString;
}
*/