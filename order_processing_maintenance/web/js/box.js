//****************************************************************************************************
//* doAdd
//****************************************************************************************************
function doAdd()
{
	var url = "BoxAction.do" + getSecurityParams(true) + "&action_type=add";
	performAction(url);
}


//****************************************************************************************************
//* doDelete
//****************************************************************************************************
function doDelete()
{

	var boxIdToBeDeleted = retrieveBoxId();
	var errorMessage = '';

	if (boxIdToBeDeleted == null)
	{
		//set the message
		errorMessage = "Please select a box that you want to delete.";

		//inovke the popup
		displayOkError(errorMessage);
	}
	else
	{
		var standardFlag = document.getElementById('standard_flag_' + boxIdToBeDeleted).value;
		if (standardFlag == 'Y')
		{
			//set the message
			errorMessage = "Standard box cannot be deleted.";

			//inovke the popup
			displayOkError(errorMessage);
		}
		else
		{

			//Modal_URL
			var modalUrl = "boxDelete.html";

			//Modal_Arguments
			var modalArguments = new Object();

			//Modal_Features
			var modalFeatures = 'dialogWidth:400px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';

			//create the arguments
			var boxObj = performCreateObject(boxIdToBeDeleted);
			var boxNameToBeDeleted = document.getElementById('box_name_' + boxIdToBeDeleted).value;

			//set the arguments
			modalArguments.boxObj = boxObj;
			modalArguments.boxNameToBeDeleted = boxNameToBeDeleted;

			//inovke the popup
			var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
			if (ret != 'false')
			{
				var url = "BoxAction.do" + getSecurityParams(true) + "&action_type=delete&box_id=" + boxIdToBeDeleted + "&new_box_id=" + ret;
				performAction(url);
			}
		}
	}
}


//****************************************************************************************************
//* doEdit
//****************************************************************************************************
function doEdit()
{
	var boxIdToBeEdited = retrieveBoxId();
	if (boxIdToBeEdited == null)
	{
		//set the message
		var errorMessage = "Please select a box that you want to edit.";
		displayOkError(errorMessage);
	}
	else
	{
		var url = "BoxAction.do" + getSecurityParams(true) + "&action_type=edit&box_id=" + boxIdToBeEdited;
		performAction(url);
	}
}


//****************************************************************************************************
//* doExit
//****************************************************************************************************
function doExit(actionType)
{
	if (somethingChanged)
	{
		//set the message
		var errorMessage = "Data changes were detected on this page. Are you sure want to exit without saving?";

		//get a true/false value from the modal dialog.
		var ret = displayConfirmError(errorMessage);

		if (ret)
		{
			var url = "BoxAction.do" + getSecurityParams(true) + "&action_type=" + actionType;
			performAction(url);
		}
	}
	else
	{
		var url = "BoxAction.do" + getSecurityParams(true) + "&action_type=" + actionType;
		performAction(url);
	}
}


//****************************************************************************************************
//* doSave
//****************************************************************************************************
function doSave()
{
	if(validate())
	{
		var url = "BoxAction.do" + getSecurityParams(true) + "&action_type=save";
		performAction(url);
	}
	else
	{
		//set the message
		var errorMessage = "Please fix the highlighted fields before saving";

		//show the modal dialog.
		displayOkError(errorMessage);

	}
}


//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	document.forms[0].action = url;
	document.forms[0].submit();
}


//****************************************************************************************************
//* performCreateObject
//****************************************************************************************************
function performCreateObject(boxIdToBeDeleted)
{
	var boxObj = new Array();

	var boxId;
	var boxName;
	var count = 0;
	var firstInvokation = true;
	//alert("totalRecords = " + totalRecords);

	for (var x=1; x <= totalRecords; x++)
	{
		boxId = document.getElementById('line_num_' + x).value;
		//alert("boxId retrieved = " + boxId + " and boxIdToBeDeleted = " + boxIdToBeDeleted);
		if (boxId != boxIdToBeDeleted)
		{
			boxName = document.getElementById('box_name_' + boxId).value;

			var boxData = new Array();
			boxData[1] = boxId;
			boxData[2] = boxName;
			if (firstInvokation)
			{
				firstInvokation = false;
			}
			else
			{
				count++;
			}
			boxObj[count] = boxData;

		}
	}

	return boxObj;
}


//****************************************************************************************************
//* populateDropDown()
//****************************************************************************************************
function populateDropDown(currentlySavedValue, maxValue)
{
	var tempValue	= 0.5;

	while(tempValue <= maxValue)
	{
		if (tempValue==currentlySavedValue)
			document.write( "<OPTION selected='selected' value=" + tempValue + ">" + tempValue + " inches" + "</OPTION>" );
		else
			document.write( "<OPTION value=" + tempValue + ">" + tempValue + " inches" + "</OPTION>" );

		tempValue += 0.5;
	}

}


//****************************************************************************************************
//* retrieveBoxId
//****************************************************************************************************
function retrieveBoxId()
{
	var radio = document.forms[0].boxRadio;
	var radioSelected;

	if (radio != null)
	{
		if (radio.length == null)
		{
			if (radio.checked)
				radioSelected = radio;
			else
				radioSelected = null;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++){
				if ( radio[i].checked ){
					radioSelected = radio[i];
				}
			}
		}

		// if radio was not selected, return null
		if ( radioSelected == null )
		{
			return null;
		}
		// else return the radio value
		else
		{
			var boxId = radioSelected.value;
			return boxId;
		}

	}
}

//****************************************************************************************************
//* validate
//****************************************************************************************************
function validate()
{
	var returnVal = true;

	//****validate city
	document.getElementById('box_name').style.backgroundColor = 'white';
	var boxName = document.getElementById('box_name').value;
	if (boxName == null || boxName.length == 0)
	{
		document.getElementById('box_name').style.backgroundColor = 'pink';
		returnVal = false;
	}

	return returnVal;

}




