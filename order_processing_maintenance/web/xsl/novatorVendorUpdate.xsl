<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Imports -->
  <xsl:import href="commonUtil.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="header.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  
  

  <!-- Keys -->
  <xsl:key name="pageData" match="/root/pageData/data" use="name"/>

  <xsl:template match="/">
    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html">
        </META>
        <title>Send Vendor Updates</title>
        
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>

        <script type="text/javascript" src="js/Tokenizer.js"></script>
        <script type="text/javascript" src="js/prototype.js"></script>
        <script type="text/javascript" src="js/rico.js"></script>
    	<script type="text/javascript" src="js/ftdajax.js"></script>
        
        <script type="text/javascript">
        //Set novator environment variables
            
            var liveFeedAllowed = null;
            var liveFeedOn = null;
            var contentFeedAllowed = null;
            var contentFeedOn = null;
            var testFeedAllowed = null;
            var testFeedOn = null;
            var uatFeedAllowed = null;
            var uatFeedOn = null;
            liveFeedAllowed = '<xsl:value-of select="root/novator_update_setup/production_feed_allowed"/>';
            liveFeedOn = '<xsl:value-of select="root/novator_update_setup/production_feed_checked"/>';
            contentFeedAllowed = '<xsl:value-of select="root/novator_update_setup/content_feed_allowed"/>';
            contentFeedOn = '<xsl:value-of select="root/novator_update_setup/content_feed_checked"/>';
            testFeedAllowed = '<xsl:value-of select="root/novator_update_setup/test_feed_allowed"/>';
            testFeedOn = '<xsl:value-of select="root/novator_update_setup/test_feed_checked"/>';
            uatFeedAllowed = '<xsl:value-of select="root/novator_update_setup/uat_feed_allowed"/>';
            uatFeedOn = '<xsl:value-of select="root/novator_update_setup/uat_feed_checked"/>';

        
        
          <![CDATA[
          
                  //configures checkboxes
          
          function setupCheckBox(cbName, allowed, boxOn)
            {
               var checkBox = document.getElementById(cbName);
    
               if (allowed != null && allowed == 'YES')
                  {
                    checkBox.checked = false;
                    checkBox.disabled = false;
                    if (boxOn != null && boxOn == 'YES')
                      {
                        checkBox.checked = true;
                      }
                  }
                else
                  {
                    checkBox.checked = false;
                    checkBox.disabled = true;
                  }
           }
          
          
          
          
          
          

          //keeps track of which vendor is being updated
          var idx=0;          
          
          //holds string tokens read in from console
          var tokens;
          
          //stores the last console text displayed
          var lastConsoleText;
          
          //novator server specified
          var novatorServer;
          //var novatorServer = [];
  
          function initCheckBoxes() {
            setupCheckBox('NOVATOR_UPDATE_LIVE',liveFeedAllowed,liveFeedOn);
            setupCheckBox('NOVATOR_UPDATE_CONTENT',contentFeedAllowed,contentFeedOn);
            setupCheckBox('NOVATOR_UPDATE_TEST',testFeedAllowed,testFeedOn);
            setupCheckBox('NOVATOR_UPDATE_UAT',uatFeedAllowed,uatFeedOn);
          }

          /*
             Submits specified Novator Vendor updates.
          */
          function submitData() {
            //disable submit button
            document.forms[0].btnSendUpdates.disabled = true;

            //blank out progress console
            document.forms[0].console.value="";

            //If form is valid update the specified vendors else
            //enable the submit button.
            if( validateForm() ) {
              idx=0;
              document.forms[0].console.style.visibility='visible';
              sendVendor(); 
            } else {
              document.forms[0].btnSendUpdates.disabled = false;
            }
          
            return false; //never submit the form
          }

          /*
             Validates the form.
          */
          function validateForm() {
            var returnValid = true;
            var errorString = "";
            
            if (!(document.forms[0].NOVATOR_UPDATE_LIVE.checked || 
                    document.forms[0].NOVATOR_UPDATE_CONTENT.checked ||
                    document.forms[0].NOVATOR_UPDATE_TEST.checked ||
                    document.forms[0].NOVATOR_UPDATE_UAT.checked)) {
                          errorString = "Please select a server to update.";
                          returnValid = false;
            } else {
              //Validate that there are values in the vendor list
              var strValues = document.forms[0].updateList.value;
              if( strValues==undefined || strValues.length==0 ) {
                errorString = "No values were entered in the vendor list";
                returnValid = false;
              } else {
                //Validate that there are valid values in the vendor list
                strValues = strValues.toUpperCase();
                var str = specialCharsToSpaces(strValues);
                tokens = specialCharsToSpaces(strValues).tokenize(" ", true, true);
              
                if( tokens==null || tokens.length==0 ) {
                  errorString = "No valid values were entered in the vendor list";
                  returnValid = false;
                }
              }
            }
            
            //display validation error if one exists
            if( returnValid==false ) {
              alert(errorString);
            }
            
            return returnValid;
          }

          /*
             Send Novator Vendor update request via AJAX.
          */
          function sendVendor() {
            var vendorId = tokens[idx];
            
            //if vendors still exist send update
            if(vendorId != undefined) {
              //store last console text minus the 'Working' text for later use
              lastConsoleText = document.forms[0].console.value + "Sending " + tokens[idx] + "...";
              
              //set 'Working' console text
              //var newValue = document.forms[0].console.value + tokens[idx] + " \tWorking...";
              document.forms[0].console.value = lastConsoleText;
              scrollToBottom(document.forms[0].console);
         
              //send vendor update request via AJAX
              var newRequest = new FTD_AJAX.Request('NovatorVendorUpdateAction.do', FTD_AJAX_REQUEST_TYPE_POST, processVendorUpdateResponse, false);
              newRequest.addParam('action_type', 'update');
              newRequest.addParam('vendorId', escape(tokens[idx]));
              newRequest.addParam('NOVATOR_UPDATE_LIVE', escape(document.forms[0].NOVATOR_UPDATE_LIVE.checked));
              newRequest.addParam('NOVATOR_UPDATE_CONTENT', escape(document.forms[0].NOVATOR_UPDATE_CONTENT.checked));
              newRequest.addParam('NOVATOR_UPDATE_UAT', escape(document.forms[0].NOVATOR_UPDATE_UAT.checked));
              newRequest.addParam('NOVATOR_UPDATE_TEST', escape(document.forms[0].NOVATOR_UPDATE_TEST.checked));
              newRequest.send();
              
              //increment the tokens index counter
              idx++;
            }
            else {
              //if no more vendors exist display a 'DONE' message
              document.forms[0].btnSendUpdates.disabled = false;
              document.forms[0].console.value = document.forms[0].console.value + "\nProcessing Complete";
              alert("Send Vendor Update process complete");
              scrollToBottom(document.forms[0].console);
            }
          }

          /*
             Process AJAX response to determine success or error.
          */
          function processVendorUpdateResponse(data) {
            //var testText = "success";
            
            //if update was successful display 'Success' text within the console
            var index = data.indexOf("Error");
            if( index < 0 ) {
              document.forms[0].console.value = lastConsoleText + data;     
              scrollToBottom(document.forms[0].console);
              sendVendor();
            } else {
              //if an error occurred display the error text and ask the user if
              //they would like to continue or abort
              document.forms[0].console.value = lastConsoleText + " " + data;     
              scrollToBottom(document.forms[0].console);
              var input_box=confirm("Problem: " + data+"\r\rClick OK to continue or Cancel to Abort");
              
              //if the user chooses to continue send the next vendor
              if (input_box==true) {
                sendVendor();
              } else {
                //if the user aborts enable the submit button
                document.forms[0].btnSendUpdates.disabled = false;
              }
            }
          }

          /*
             Scrolls to the bottom of the supplied element.
          */
          function scrollToBottom (element) {
            element.scrollTop = element.scrollHeight;
          }

          /* 
             Obtains the checked radio button value.
             Return the value of the radio button that is checked,
             return an empty string if none are checked, or
             there are no radio buttons.
          */
          function getCheckedValue(radioObj) {
            if(!radioObj)
              return "";
            var radioLength = radioObj.length;
            if(radioLength == undefined)
              if(radioObj.checked)
                return radioObj.value;
              else
                return "";
            for(var i = 0; i < radioLength; i++) {
              if(radioObj[i].checked) {
                return radioObj[i].value;
              }
            }
            return "";
          }

          /* 
             Convert special characters to spaces.
          */
          function specialCharsToSpaces(strObject) {
            //convert any special characters to spaces
            while(strObject.indexOf("\n") != -1)
              strObject = strObject.replace("\n"," ");
        
            while(strObject.indexOf("\f") != -1)
              strObject = strObject.replace("\f"," ");
        
            while(strObject.indexOf("\b") != -1)
              strObject = strObject.replace("\b"," ");
        
            while(strObject.indexOf("\r") != -1)
              strObject = strObject.replace("\r"," ");
        
            while(strObject.indexOf("\t") != -1)
              strObject = strObject.replace("\t"," ");
              
              return strObject;
          }
          
          /* 
             Routes to Main Menu action.
          */
          function doMainMenuAction()
          {
            document.getElementById("action_type").value = "MainMenuAction";
            document.forms[0].action = "NovatorVendorUpdateAction.do";
            document.forms[0].submit();
          }
          		 ]]>
        </script>

      </head>
      <body onload="javascript:initCheckBoxes();">

        <xsl:call-template name="addHeader"/>

        <form method="post">
        <input type="hidden" id="action_type" name="action_type"/>
        <input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>

        <xsl:call-template name="securityanddata"/>

        <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
          <tr>
            <td colspan="2">
              <hr/>
            </td>
          </tr>
          <tr>
            <th class="label" align="right" valign="top" width="30%">Vendor List
            </th>
            <td>
              &nbsp;<textarea class="textarea" id="updateList" name="updateList" cols="100" rows="5"/>
            </td>
          </tr>
  
          <tr>
            <td>
            </td>
            <td>
              <textarea class="textarea" id="console" name="console" cols="100" rows="5" style="visibility: hidden; vertical-align: top"/>
            </td>
          </tr>
          
      
          <tr>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td colspan="2">
              <hr/>
            </td>
          </tr>
          
                    <tr>
            <td valign="top" align="center" colspan="2">
              <table border="0" align="center">
                <tr align="center">
    
                  <td align="center">
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_LIVE" 
                                              title="Send updates to Novator production web site"/>
                                        <label class="radioLabel">Live</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_CONTENT" 
                                              title="Send updates to Novator content web site"/>
                                        <label class="radioLabel">Content</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_TEST" 
                                              title="Send updates to Novator test web site"/>
                                        <label class="radioLabel">Test</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_UAT" 
                                              title="Send updates to Novator UAT web site"/>
                                        <label class="radioLabel">UAT</label>
                  </td>
                  
                </tr>
              </table>
            </td>
          </tr>
          
          <tr>
            <td colspan="2" align="center">
							<button class="BlueButton" style="width:80;" name="btnSendUpdates" tabindex=""  accesskey="S" onclick="javascript:submitData();">(S)ubmit</button>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
                                                        <button class="BlueButton" style="width:80;" name="mainMenu" tabindex="" accesskey="M" onclick="javascript:doMainMenuAction();">(M)ain Menu </button>
            </td>
          </tr>
        </table>
      </form>
      <xsl:call-template name="footer"/>
    </body>
    </html>
  </xsl:template>

	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="false()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="''"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Send Vendor Updates'"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>