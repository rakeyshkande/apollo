<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />
  <xsl:import href="commonUtil.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>


<xsl:template match="/">


<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"></META>
      <title>Vendor Maintenance</title>
	  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>	
  	<script type="text/javascript" src="js/calendar.js"/>
		<script type="text/javascript" src="js/prototype.js"></script>
  	<script type="text/javascript" src="js/rico.js"/>
  	<script type="text/javascript" src="js/ftdajax.js"/>
  	<script>
	var popupValue = "<xsl:value-of select='//popup_flag'/>";

		<![CDATA[


    function clickSearch(){
        var vendorList = document.getElementById("vendor_list");
        document.getElementById("vendor_id").value = vendorList.options[vendorList.selectedIndex].value;
        var url = "VendorProductSearchAction.do" +
                           "?action_type=search&from_page=product_search" + getSecurityParams(false);
        //alert(document.getElementById("vendor_id").value);
        performAction(url);
    }
  
  
    function performAction(url)
    {
        document.forms[0].action = url;
        document.forms[0].submit();
    }

    function clickMainMenu(){

     var url = "MainMenuAction.do" +  getSecurityParams(true);
     performAction(url);
   }
   function clickExit() 
   {
     var url = "MainMenuAction.do" +  getSecurityParams(true);
     performAction(url);
   }
   function init()
{
	var productID = document.getElementById("product_id").value;
        var vendorID = document.getElementById("vendor_id").value;
        
        //alert('Product id: '+productID+' Vendor id: ' + vendorID);
        if( productID != undefined && productID.length>0 && vendorID != undefined && vendorID.length>0 ) {
            doGetVendors();
        }

	if(popupValue != null && popupValue == "cannot_create")
	{
            alert("No inventory record for product.");
	}
	else
	{
		if(popupValue != null && popupValue == "can_create")
		{
			if(doExitPageAction("No inventory record for product.  Do you want to add it?"))
			{
				var url = "VendorInventoryControlAction.do" +
					   "?action_type=display" + "&from_page=product_search" + "&update_type=insert" + "&product_id=" + productID + "&vendor_id=" + vendorID + "&edit_mode=display" + getSecurityParams(false);

				performAction(url);
			}

		}		
	}
	
	document.getElementById('product_id').focus();
}
   
   
	//****************************************************************************************************
	//* doGetVendors()
	//****************************************************************************************************
  function doGetVendors()
  {
    var productId = document.getElementById("product_id").value;
    if( productId != undefined && productId.length>0 ) {
        document.getElementById("product_id").value = productId.toUpperCase();
        
        //XMLHttpRequest
        var newRequest = new FTD_AJAX.Request('VendorProductSearchAction.do', FTD_AJAX_REQUEST_TYPE_POST, getVendorsCallback, false);
        newRequest.addParam('action_type', 'get_vendors');
        newRequest.addParam('product_id', productId.toUpperCase());
        newRequest.send();
    }
  }
   

	//****************************************************************************************************
	//* doGetVendors()
	//****************************************************************************************************
  function getVendorsCallback(data)
  {
    // If there was an error display the error page
    //var isError = xml.selectSingleNode("root/is_error").text;
    //if(isError == "Y")
    //  doCallErrorPage();
      
    //convert data to xml
    var xml = getXMLobjXML(data);

    //this will return an array set of "vendors"
    var vendors = xml.getElementsByTagName("vendor");
    
    var vendorList = document.getElementById("vendor_list");
    var vendorLabel = document.getElementById("vendor_label");
    var vendorDiv = document.getElementById("vendorDiv");
    var searchButton = document.getElementById("search");
    
    //iterate through vendor nodes and set to null
    for( var i = vendorList.length-1; i>=0; i-- ) {
        vendorList.options[i] = null;
    }
    
    var selectedIdx = -1;
    var pageVendorId = document.getElementById("vendor_id").value;
    //iterate through vendor nodes to populate vendor select
    for( var i = 0; i < vendors.length; i++)
    {
      var vendorId = vendors.item(i).getElementsByTagName("vendor_id").item(0).text;
      var vendorName = vendors.item(i).getElementsByTagName("vendor_name").item(0).text
        + ' (' + vendorId + ')';
      vendorList.options[i] = new Option(vendorName, vendorId); 
      
      if( pageVendorId != undefined && vendorId == pageVendorId ) {
        selectedIdx = i;
      }
    }

    if( selectedIdx > -1 ) {
        vendorList.options[selectedIdx].selected=true;
    }
    
    //enable vendor div and search button
    vendorDiv.style.display = "block";
    searchButton.disabled = false;
  }

]]>
		</script>

   </head>
<body onload="javascript:init();">
  <form name="vendormaint" method="post"  >
    <input type="hidden" name="securitytoken" value="{//pagedata/securitytoken}"/>
    <input type="hidden" name="adminAction" value="{//pagedata/adminAction}"/>
    <input type="hidden" name="context" value="{//pagedata/context}"/>
    <input type="hidden" id="vendor_id" name="vendor_id" value="{//vendor_id}"/>
<!-- Header-->
   <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
   		<tr>
   			<td width="25%">				
          
            <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
          
        </td>
      </tr>
      <tr>
        <td width="25%"/>
   			<td  align="center" valign="top" class="Header" id="pageHeader">
           		Dropship Inventory Control
         	</td>
			<td  id="time" align="right" valign="bottom" class="Label" width="25%" height="30">
			  <script type="text/javascript">startClock();</script>
			</td>
       </tr>
	</table>

<!-- Display table-->
   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
        <table border="0" width="100%" align="center" class="innerTable">
          <tr>
            <td align="center" class="TotalLine">
            	<div align="center"><strong>Product Information</strong><br></br>
             	 </div></td>
                <div align="left"></div>
          </tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          
          <tr>
            <td>
                <table align="center" border="0" cellpadding="2" cellspacing="0">
                  <tr>
                    <td align="center" height="10" class="Label">Product ID:</td>
                    <td><input type="text" id="product_id" name="product_id" size="10" tabindex="1" value="{//product_id}" onBlur="doGetVendors();"/></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center">
                        <span class="ErrorMessage"><xsl:value-of select="//error_message"/></span>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" height="10" class="Label">Vendors: </td>
                    <td>
                        <div id="vendorDiv">
                            <select id="vendor_list" name="vendor_list" tabindex="2"><option value="xx">--select a vendor--</option></select>
                        </div>
                    </td>
                  </tr>
              </table>
            </td>
          </tr>
          
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr><td><BR></BR></td></tr>
          <tr>
            <td align="center" height="10" class="Label">
              <button class="BlueButton" style="width:80;" accesskey="S" disabled="true"
                      name="search" tabindex="34" onclick="javascript:clickSearch()">(S)earch </button>
            </td>
          </tr>
          <tr><td></td></tr>

     <tr>
     <td>

     </td>
     </tr>

        </table></td>
     </tr>
	</table>
 



   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
      	 <td>
			 <table>
			 	<tr><td  align="left"></td></tr>
			 </table>
		  </td>
		  <td align="right">
			 <table>
				<tr>
					<td align="right" valign="top">
				 	<button class="BlueButton" style="width:80;" name="exit" tabindex=""  accesskey="E" onclick="javascript:clickExit();">(E)xit </button>
				 	</td>
				</tr>
			 </table>
		</td>
      </tr>
</table>
<xsl:call-template name="footer"/>


</form>
</body>
</html>

</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="false()"/>
			<xsl:with-param name="showPrinter" select="false()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
		</xsl:call-template>
	</xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
	<xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
      <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>
	</xsl:template>
</xsl:stylesheet>
