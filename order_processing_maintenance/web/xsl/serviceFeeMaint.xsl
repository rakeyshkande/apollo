<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Service Charge Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/mercuryMessage.js"></script>

<script type="text/javascript"><![CDATA[

var deleteArray = new Array();
var idArray = new Array();
var arrayCount = 0;

function init()
{
]]>
<xsl:if test="key('pageData','update_allowed')/value = 'N'">
document.getElementById('addCode').disabled = true;
document.getElementById('deleteCode').disabled = true;
</xsl:if>

<xsl:for-each select="root/service_fees/service_fee">
  document.maint.idList.value = document.maint.idList.value + '<xsl:value-of select="snh_id"/>' + ",";
</xsl:for-each>

<xsl:if test="key('pageData','dateRangeFlag')/value = 'Y'">
  msg = "Date is part of a delivery date range.  Please make sure\n";
  msg = msg + "to create an override entry for all dates in the range.";
  alert(msg); 
</xsl:if>
<![CDATA[
}

function doMainMenuAction()
{
    document.maint.action = "ServiceFeeMaintAction.do";
    document.maint.action_type.value = "MainMenuAction";
    document.maint.submit();
}

function doSortAction()
{
    document.maint.action = "ServiceFeeMaintAction.do";
    document.maint.action_type.value = "loadServiceFee";
    document.maint.submit();
    
}

function doDeleteAction()
{
    document.maint.deleteValues.value = "";
    for (i=0;i<deleteArray.length;i++) {
        isChecked = document.getElementById(deleteArray[i]).checked;
        if (isChecked) {
            if (document.maint.deleteValues.value == "") {
                document.maint.deleteValues.value = idArray[i];
            } else {
                document.maint.deleteValues.value = document.maint.deleteValues.value + "," + idArray[i];
            }
        }
    }
    if (document.maint.deleteValues.value == "") {
        alert("No record was selected for deletion");
        return;
    }
    msg = "Are you sure you want to delete?";
    if (doExitPageAction(msg)) {
        var modal_dim = "dialogWidth:400px; dialogHeight:250px; center:yes; status:0; help:no; scroll:no";
        var popURL = "serviceFeeDeletePop.html";
        var results=showModalDialog(popURL, "", modal_dim);
        if (results && results != null) {
            showWaitMessage("serviceFeeDiv", "wait", "Processing");
            document.maint.action = "ServiceFeeMaintAction.do";
            document.maint.action_type.value = "deleteServiceFee";
            document.maint.requested_by.value = results;
            document.maint.submit();
        }
    }
    
}

function popEdit(snhId, domesticFee, intlFee, vendorCharge, vendorSatUpcharge, snhDescription, action, requestedBy, sameDayUpcharge, sameDayUpchargeFS, vendorSunUpcharge, vendorMonUpcharge)
{
    if (document.maint.updateAllowed.value != "Y") {
        action = "View";
    }
    var modal_dim = "dialogWidth:500px; dialogHeight:400px; center:yes; status:0; help:no; scroll:no";
    var popURL = "serviceFeePop.html?id=" + snhId;
    popURL = popURL + "&domesticFee=" + domesticFee;
    popURL = popURL + "&intlFee=" + intlFee;
    popURL = popURL + "&vendorCharge=" + vendorCharge;
    popURL = popURL + "&vendorSatUpcharge=" + vendorSatUpcharge;
    popURL = popURL + "&snhDescription=" + snhDescription;
    popURL = popURL + "&action=" + action;
    popURL = popURL + "&requestedBy=" + requestedBy;
    popURL = popURL + "&idList=" + document.maint.idList.value;
    popURL = popURL + "&sameDayUpcharge=" + sameDayUpcharge;
    popURL = popURL + "&sameDayUpchargeFS=" + sameDayUpchargeFS;
	popURL = popURL + "&vendorSunUpcharge=" + vendorSunUpcharge;
	popURL = popURL + "&vendorMonUpcharge=" + vendorMonUpcharge;
    var results=showModalDialog(popURL, "", modal_dim);
    if (results && results != null) {
        showWaitMessage("serviceFeeDiv", "wait", "Processing");
        document.maint.serviceFeeId.value = results[1];
        document.maint.domestic_fee.value = results[2];
        document.maint.international_fee.value = results[3];
        document.maint.requested_by.value = results[4];
        document.maint.vendor_charge.value = results[5];
        document.maint.vendor_sat_upcharge.value = results[6];
        document.maint.description.value = results[7];
        document.maint.same_day_upcharge.value = results[8];
        document.maint.same_day_upcharge_fs.value = results[9];
		document.maint.vendor_sun_upcharge.value = results[10];
		document.maint.vendor_mon_upcharge.value = results[11];
        document.maint.action = "ServiceFeeMaintAction.do";
        document.maint.action_type.value = "saveServiceFee";
        document.maint.submit();
    }
}

function popViewOverride(snhId, description, domesticFee, intlFee, vendorCharge, vendorSatUpcharge, sameDayUpcharge, sameDayUpchargeFS, vendorSunUpcharge, vendorMonUpcharge)
{
    var modal_dim = "dialogWidth:780px; dialogHeight:400px; center:yes; status:0; help:no; scroll:no";
    var popURL = "ServiceFeeMaintAction.do?serviceFeeId=" + snhId;
    popURL = popURL + "&serviceFeeDescription=" + description;
    popURL = popURL + "&action_type=getOverrideList";
    popURL = popURL + "&updateAllowed=" + document.maint.updateAllowed.value;
    popURL = popURL + "&securitytoken=" + document.maint.securitytoken.value;
    popURL = popURL + "&context=" + document.maint.context.value;
    popURL = popURL + "&domesticFee=" + domesticFee;
    popURL = popURL + "&intlFee=" + intlFee;
    popURL = popURL + "&vendorCharge=" + vendorCharge;
    popURL = popURL + "&vendorSatUpcharge=" + vendorSatUpcharge;
    popURL = popURL + "&sameDayUpcharge=" + sameDayUpcharge;
    popURL = popURL + "&sameDayUpchargeFS=" + sameDayUpchargeFS;
	popURL = popURL + "&vendorSunUpcharge=" + vendorSunUpcharge;
	popURL = popURL + "&vendorMonUpcharge=" + vendorMonUpcharge;
    var results=showModalDialogIFrame(popURL, "", modal_dim);
    if (results && results != null) {
        showWaitMessage("serviceFeeDiv", "wait", "Processing");
        document.maint.action = "ServiceFeeMaintAction.do";
        document.maint.action_type.value = "loadServiceFee";
        document.maint.submit();
    }
}

function popAddOverride(snhId, description, domesticFee, intlFee, vendorCharge, vendorSatUpcharge, sameDayUpcharge, sameDayUpchargeFS, vendorSunUpcharge, vendorMonUpcharge)
{
    var modal_dim = "dialogWidth:500px; dialogHeight:375px; center:yes; status:0; help:no; scroll:no";
    var popURL = "serviceFeeOverridePop.html?id=" + snhId;
    popURL = popURL + "&description=" + description;
    popURL = popURL + "&overrideDate=";
    popURL = popURL + "&domesticFee=" + domesticFee;
    popURL = popURL + "&intlFee=" + intlFee;
    popURL = popURL + "&vendorCharge=" + vendorCharge;
    popURL = popURL + "&vendorSatUpcharge=" + vendorSatUpcharge;
	popURL = popURL + "&vendorSunUpcharge=" + vendorSunUpcharge;
	popURL = popURL + "&vendorMonUpcharge=" + vendorMonUpcharge;
    popURL = popURL + "&action=Add";
    popURL = popURL + "&idList=";
    popURL = popURL + "&requestedBy=";
    popURL = popURL + "&sameDayUpcharge=" + sameDayUpcharge;
    popURL = popURL + "&sameDayUpchargeFS=" + sameDayUpchargeFS;
    
    var results=showModalDialog(popURL, "", modal_dim);
    if (results && results != null) {
        showWaitMessage("serviceFeeDiv", "wait", "Processing");
        document.maint.serviceFeeId.value = results[1];
        document.maint.override_date.value = results[2];
        document.maint.domestic_fee.value = results[3];
        document.maint.international_fee.value = results[4];
        document.maint.requested_by.value = results[5];
        document.maint.vendor_charge.value = results[6];
        document.maint.vendor_sat_upcharge.value = results[7];
        document.maint.description.value = results[8];
        document.maint.same_day_upcharge.value = results[9];
        document.maint.same_day_upcharge_fs.value = results[10];
		document.maint.vendor_sun_upcharge.value = results[11];
		document.maint.vendor_mon_upcharge.value = results[12];
        document.maint.action = "ServiceFeeMaintAction.do";
        document.maint.action_type.value = "saveServiceFeeOverride";
        document.maint.submit();
    }
}

function popSourceView(snhId)
{
    var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
    var popURL = "ServiceFeeMaintAction.do?id=" + snhId;
    popURL = popURL + "&action_type=getSourceCodeList";
    popURL = popURL + "&securitytoken=" + document.maint.securitytoken.value;
    popURL = popURL + "&context=" + document.maint.context.value;
    var results=showModalDialog(popURL, "", modal_dim);
    if (results && results != null) {
        alert(results);
    }
}

function popHistoryView(snhId, description)
{
    var modal_dim = "dialogWidth:650px; dialogHeight:450px; center:yes; status:0; help:no; scroll:no";
    var popURL = "ServiceFeeMaintAction.do?id=" + snhId;
    popURL = popURL + "&serviceFeeDescription=" + description;
    popURL = popURL + "&action_type=getHistory";
    popURL = popURL + "&securitytoken=" + document.maint.securitytoken.value;
    popURL = popURL + "&context=" + document.maint.context.value;
    var results=showModalDialogIFrame(popURL, "", modal_dim);
    if (results && results != null) {
        alert(results);
    }
}

]]>
</script>
</head>

<body onload="javascript:init();">

<table width="825" border="0" align="center">
    <tr>
        <td width="30%">
        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </td>
        <td width="40%" align="center" valign="center">
        <font class="Header">Service Charge Maintenance</font>
        </td>
        <td  id="time" align="right" valign="center" class="Label" width="30%">
        <script type="text/javascript">startClock();</script>
        </td>
    </tr>
</table>

<form name="maint" method="post">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
<input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
<input type="hidden" name="deleteValues" value=""/>
<input type="hidden" name="serviceFeeId" value=""/>
<input type="hidden" name="domestic_fee" value=""/>
<input type="hidden" name="international_fee" value=""/>
<input type="hidden" name="vendor_charge" value=""/>
<input type="hidden" name="vendor_sat_upcharge" value=""/>
<input type="hidden" name="description" value=""/>
<input type="hidden" name="override_date" value="" />
<input type="hidden" name="idList" value="" />
<input type="hidden" name="calledFrom" value="serviceFee" />
<input type="hidden" name="requested_by" value="" />
<input type="hidden" name="same_day_upcharge" value=""/>
<input type="hidden" name="same_day_upcharge_fs" value=""/>
<input type="hidden" name="vendor_sun_upcharge" value=""/>
<input type="hidden" name="vendor_mon_upcharge" value=""/>

<table width="910" align="center" cellspacing="1">
    <tr>
        <td align="right">
        <button accesskey="M" class="BlueButton" style="width:75;" name="MainMenu" tabindex="1" onclick="javascript:doMainMenuAction();">(M)ain Menu</button>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<table width="825" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
        <div id="serviceFeeDiv">

            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr>
                            <xsl:choose>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveSuccessful'">
                                    <td class="OperationSuccess">
                                    Service Charge&nbsp;<xsl:value-of select="key('pageData','serviceFeeId')/value"/>&nbsp;was successfully saved.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveFailed'">
                                    <td class="errorMessage">
                                    Request to update Service Charge&nbsp;<xsl:value-of select="key('pageData','serviceFeeId')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'novatorConnect'">
                                    <td class="errorMessage">
                                    Cannot connect to Novator
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'novatorDEN'">
                                    <td class="errorMessage">
                                    Novator has denied the request
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'deleteSuccessful'">
                                    <td class="OperationSuccess">
                                    Service Charge&nbsp;<xsl:value-of select="key('pageData','serviceFeeId')/value"/>&nbsp;was successfully deleted.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'deleteFailed'">
                                    <td class="errorMessage">
                                    Request to delete Service Charge&nbsp;<xsl:value-of select="key('pageData','serviceFeeId')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveOverrideSuccessful'">
                                    <td class="OperationSuccess">
                                    Override Date&nbsp;<xsl:value-of select="key('pageData','overrideDate')/value"/>&nbsp;was successfully saved.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveOverrideFailed'">
                                    <td class="errorMessage">
                                    Request to update Override Date&nbsp;<xsl:value-of select="key('pageData','overrideDate')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td>&nbsp;</td>
                                </xsl:otherwise>
                            </xsl:choose>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div style="padding-right: 16px; width: 800;">																													
                                    <table width="870" border="0" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <th class="TotalLine" width="35" align="center">Delete</th>
                                            <th class="TotalLine" width="30" align="center">ID</th>
                                            <th class="TotalLine" width="150" align="center">Description</th>
                                            <th class="TotalLine" width="90" align="right">Domestic<br/>(aka Florist)<br/>Charge</th>
                                            <th class="TotalLine" width="75" align="right">Same<br/>Day<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="right">Same Day<br/>Upcharge<br/>FS-Members</th>
                                            <th class="TotalLine" width="75" align="right">Int'l<br/>Charge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Charge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Saturday<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Sunday<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Monday<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="center">Override</th>
                                            <th class="TotalLine" width="75" align="center">Source<br/>Codes</th>
                                            <th class="TotalLine" width="75" align="center">History</th>
                                            <th class="TotalLine" width="10">&nbsp;</th>
                                        </tr>
                                    </table>
                                    </div>
                                    <div style="width: 870; overflow:auto; overflow-x:hidden; padding-right: 16px; height: 270px;">																													
                                    <table width="865" border="0" cellpadding="2" cellspacing="0">	
                                        <xsl:for-each select="root/service_fees/service_fee">							                                  		
                                            <tr>
                                                <td align="center" width="35">
                                                    <input type="checkbox" name="delete_{position()}" value="{snh_id}" tabindex="{position()}" />
                                                </td>
                                                <td align="center" width="30">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="snh_id" />
                                                    </a>
                                                </td>
                                                <td align="center" style="word-wrap:break-word" width="115">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="description" />
                                                    </a>
                                                </td>
                                                <td align="right" width="90" style="padding-right:6px">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="first_order_domestic" />
                                                    </a>
                                                </td>
                                                <td align="right" width="80" style="padding-right:10px">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="same_day_upcharge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="80" style="padding-right:10px">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="same_day_upcharge_fs" />
                                                    </a>
                                                </td>
                                                <td align="right" width="60">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="first_order_international" />
                                                    </a>
                                                </td>
                                                <td align="right" width="60">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="vendor_charge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="70">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="vendor_sat_upcharge" />
                                                    </a>
                                                </td>  
                                                <td align="right" width="90">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}', '{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="vendor_sun_upcharge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="75">
                                                    <a href="javascript:popEdit('{snh_id}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}','{description}','Edit','{requested_by}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}', '{vendor_mon_upcharge}')" tabindex="{position()}">
                                                    <xsl:value-of select="vendor_mon_upcharge" />
                                                    </a>
                                                </td>                                              
                                                <td align="center" width="75">
                                                    <xsl:choose>
                                                        <xsl:when test="overrides_exist = 'Y'">
                                                            <a href="javascript:popViewOverride('{snh_id}', '{description}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                            <font color="0000ff">View</font>
                                                            </a>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:choose>
                                                                <xsl:when test="key('pageData','update_allowed')/value = 'Y'">
                                                                    <a href="javascript:popAddOverride('{snh_id}', '{description}','{first_order_domestic}','{first_order_international}','{vendor_charge}','{vendor_sat_upcharge}', '{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')" tabindex="{position()}">
                                                                    <font color="009900">Add</font>
                                                                    </a>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <font color="009900">Add</font>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                <td align="center" width="70">
                                                    <xsl:choose>
                                                        <xsl:when test="source_codes_exist = 'Y'">
                                                            <a href="javascript:popSourceView('{snh_id}')" tabindex="{position()}">
                                                            <font color="0000ff">View</font>
                                                            </a>
<script type="text/javascript">
    elementId = "delete_" + <xsl:value-of select="@num" />
    document.getElementById(elementId).disabled = true;
</script>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <font class="ErrorMessage">None</font>
<script type="text/javascript">
    deleteArray[arrayCount] = "delete_" + <xsl:value-of select="@num" />
    idArray[arrayCount] = '<xsl:value-of select="snh_id" />'
    arrayCount = arrayCount + 1;
</script>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                <td align="center" width="75" colspan="2">
                                                    <a href="javascript:popHistoryView('{snh_id}', '{description}')" tabindex="{position()}">
                                                    <font color="0000ff">View</font>
                                                    </a>
                                                </td>
                                               <!--  <td width="10">&nbsp;</td> -->
                                            </tr>                 		
                                        </xsl:for-each>
                                    </table>																
                                    </div>	
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <select name="sort_field" tabindex="96">
                            <xsl:for-each select="root/sort_fields/sort_field">
                                <option value="{id}"><xsl:if test="id=key('pageData','sortField')/value"><xsl:attribute name="selected"/></xsl:if>
                                    <xsl:value-of select="description"/>
                                </option>
                            </xsl:for-each>
                        </select>
                        <button accesskey="S" class="BlueButton" style="width:150;" name="SortCode" tabindex="97" onclick="javascript:doSortAction();">(S)ort</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button accesskey="A" class="BlueButton" style="width:50;" name="AddCode" tabindex="98" onclick="javascript:popEdit('','','','','','','Add','','','','','');">(A)dd</button>
                        &nbsp;&nbsp;
                        <button accesskey="D" class="BlueButton" style="width:150;" name="DeleteCode" tabindex="99" onclick="javascript:doDeleteAction();">(D)elete Selected</button>
                    </td>
                </tr>
            </table>

        </div>
        </td>
    </tr>
</table>
</form>

<div id="waitDiv" style="display:none">
    <table id="waitTable" width="825" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
            <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                        <td id="waitTD" width="50%" class="waitMessage"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<!-- call footer template-->
<xsl:call-template name="footer"/>
</body>
</html>
</xsl:template>  
</xsl:stylesheet>
