
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl" />
	<xsl:import href="footer.xsl" />
	<xsl:import href="securityanddata.xsl" />
	<xsl:output method="html" indent="no" />
	<xsl:output indent="yes" />
	<xsl:key name="pageData" match="root/pageData/data" use="name" />
	<xsl:template match="/">

		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html" />
				<title>	FTD - <xsl:value-of select="key('pageData','screenName')/value" /> History </title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css" />
				<script type="text/javascript"> <![CDATA[
					function doCloseAction() {
					    window.close();
					}
	
					function popDetail(operation, timestamp, updated_by, requestedBy, deliveryFee) {
					    var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
					    var popURL = "deliveryFeeHistoryPop.html?id=" + document.maint.deliveryId.value;
					    popURL = popURL + "&operation=" + operation;
					    popURL = popURL + "&timestamp=" + timestamp;
					    popURL = popURL + "&updatedBy=" + updated_by;
					    popURL = popURL + "&requestedBy=" + requestedBy;
					    popURL = popURL + "&deliveryFee=" + deliveryFee;
					    popURL = popURL + "&screenName=" +document.maint.screenName.value;
					    var results = showModalDialog(popURL, "", modal_dim);
					}
					]]>
				</script>
			</head>

			<body>

				<form name="maint" method="post">
					<xsl:call-template name="securityanddata" />
					
					<input type="hidden" name="deliveryId" value="{key('pageData','deliveryId')/value}" />
					<input type="hidden" name="description" value="{key('pageData','description')/value}" />
					<input type="hidden" name="screenName" value="{key('pageData','screenName')/value}" />
					<input type="hidden" name="deliveryType" value="{key('pageData','deliveryType')/value}" />

					<center>
						<h2> Maintenance History for ID&nbsp;<xsl:value-of select="key('pageData','deliveryId')/value" />&nbsp;-&nbsp;<xsl:value-of select="key('pageData','description')/value" />	</h2>

						<table width="550" align="center" cellspacing="1" class="mainTable">
							<tr>
								<td>
									<table width="100%" align="center" class="innerTable">
										<tr>
											<td align="center">
												<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
													<tr> <td>&nbsp;</td></tr>
													<tr>
														<td align="left">
															<div style="padding-right: 16px; width: 425;">
																<table width="525" border="0" cellpadding="2"
																	cellspacing="0">
																	<tr>
																		<th class="TotalLine" width="75" align="left">Operation</th>
																		<th class="TotalLine" width="125" align="left">Date/Time</th>
																		<th class="TotalLine" width="75" align="left">Updated By</th>
																		<th class="TotalLine" width="25">&nbsp;</th>
																	</tr>
																</table>
															</div>
															<div style="width: 525; overflow:auto; overflow-x:hidden; padding-right: 16px; height: 200px;">
																<table width="525" border="0" cellpadding="2" cellspacing="0">
																	<xsl:for-each select="root/delivery_fees/delivery_fee">
																		<tr>
																			<td align="left" width="75">
																				<a
																					href="javascript:popDetail('{operation}','{timestamp}','{updated_by}','{requested_by}','{delivery_fee}')">
																					<xsl:choose>
																						<xsl:when test="operation = 'INS'">
																							Insert
																						</xsl:when>
																						<xsl:when test="operation = 'DEL'">
																							Delete
																						</xsl:when>
																						<xsl:when test="operation = 'UPD_NEW'">
																							Update
																						</xsl:when>
																					</xsl:choose>
																				</a>
																			</td>
																			<td align="left" width="125">
																				<a
																					href="javascript:popDetail('{operation}','{timestamp}','{updated_by}','{requested_by}','{delivery_fee}')">
																					<xsl:value-of select="timestamp" />
																				</a>
																			</td>
																			<td align="left" width="75">
																				<a
																					href="javascript:popDetail('{operation}','{timestamp}','{updated_by}','{requested_by}','{delivery_fee}')">
																					<xsl:value-of select="updated_by" />
																				</a>
																			</td>
																			<td width="25">&nbsp;
																			</td>
																		</tr>
																	</xsl:for-each>
																</table>
															</div>
														</td>
													</tr>
													<tr></tr>
												</table>
											</td>
										</tr>
									</table>
									<table width="98%" align="center" cellpadding="5"
										cellspacing="1">
										<tr>
											<td align="center">
												<button accesskey="C" class="BlueButton" style="width:50;"
													name="Close" tabindex="99" onclick="javascript:doCloseAction();">(C)lose</button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</center>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
