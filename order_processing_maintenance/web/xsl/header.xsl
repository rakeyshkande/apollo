<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="header" >

<xsl:param name="showBackButton"/>
<xsl:param name="showSearchBox"/>
<xsl:param name="searchLabel"/>
<xsl:param name="headerName"/>
<xsl:param name="showTime"/>

	<!--
	  If you plan to use the search box on your page you will need
	  to define a doEntryAction() function on your page in order to
	  catch and user actions
	-->
	<script type="text/javascript" src="js/clock.js"/>
	<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td width="30%" height="33" valign="top">
				<div class="floatleft">
					<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
				</div>
				<xsl:if test="$showSearchBox">
				  <div class="floatright" id="searchBlock">
					     &nbsp;
             <label for="numberEntry" id="numberLabel"><xsl:value-of select="$searchLabel"/><br/>
               <input type="text" size="7" maxlength="20" name="numberEntry" id="numberEntry" onkeypress="doEntryAction();"/>&nbsp;&nbsp;
             </label>
				  </div>
				</xsl:if>
			</td>
			<td></td>
      <xsl:if test="$showBackButton">
        <td  align="right" width="5%" valign="bottom">
          <input type="button" class="BlueButton" style="width:80;" name="backButtonHeader" id="backButtonHeader" value="(B)ack" onclick="javascript:doBackAction();"/>
        </td>
      </xsl:if>
		</tr>	
		<tr>
			<td></td>
			<td  align="center" valign="bottom" class="Header" id="pageHeader">
        <xsl:value-of select="$headerName"/>
      </td>
      <td  id="time" align="right" valign="bottom" class="Label" width="30%" height="30">
        <xsl:if test="$showTime"><script type="text/javascript">startClock();</script></xsl:if>
      </td>  
    </tr>    
	</table>
  <div id="iframe_div">
	   <iframe id="CSRFrame" name="CSRFrame" width="0px" height="0px"  border="0"></iframe>
  </div>
  </xsl:template>
</xsl:stylesheet>