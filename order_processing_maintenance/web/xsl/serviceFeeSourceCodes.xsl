<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Service Charge Override Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>

<script type="text/javascript">

function init()
{
    <xsl:for-each select="root/source_codes/source_code">							                                  		
        document.getElementById("sourceCodes").value =
            document.getElementById("sourceCodes").value + '<xsl:value-of select="source_code"/>' + " ";
    </xsl:for-each>
}

function doCancelAction()
{
    window.close();
}

</script>
</head>

<body onLoad="init()">

<input type="hidden" name="serviceFeeId" value="{key('pageData','serviceFeeId')/value}"/>

<center>

<h2>
Source Codes attached to ID&nbsp;
<xsl:value-of select="key('pageData','serviceFeeId')/value"/>
</h2>

<table width="350" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="425" border="0" cellpadding="2" cellspacing="0">	
                                        <tr>
                                            <td align="center" width="100%">
                                                <textarea name="sourceCodes" rows="10" cols="80"/>
                                            </td>
                                        </tr>                 		
                                    </table>																
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <button accesskey="C" class="BlueButton" style="width:50;" name="Cancel" tabindex="99" onclick="javascript:doCancelAction();">(C)lose</button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</center>

</body>
</html>
</xsl:template>  
</xsl:stylesheet>
