<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Fuel Surcharge Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/FormChek.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>

<script type="text/javascript">
var liveFeedAllowed = null;
var liveFeedOn = null;
var contentFeedAllowed = null;
var contentFeedOn = null;
var testFeedAllowed = null;
var testFeedOn = null;
var uatFeedAllowed = null;
var uatFeedOn = null;
liveFeedAllowed = '<xsl:value-of select="root/novator_update_setup/production_feed_allowed"/>';
liveFeedOn = '<xsl:value-of select="root/novator_update_setup/production_feed_checked"/>';
contentFeedAllowed = '<xsl:value-of select="root/novator_update_setup/content_feed_allowed"/>';
contentFeedOn = '<xsl:value-of select="root/novator_update_setup/content_feed_checked"/>';
testFeedAllowed = '<xsl:value-of select="root/novator_update_setup/test_feed_allowed"/>';
testFeedOn = '<xsl:value-of select="root/novator_update_setup/test_feed_checked"/>';
uatFeedAllowed = '<xsl:value-of select="root/novator_update_setup/uat_feed_allowed"/>';
uatFeedOn = '<xsl:value-of select="root/novator_update_setup/uat_feed_checked"/>';


<![CDATA[

function init()
{
setupCheckBox('NOVATOR_UPDATE_LIVE',liveFeedAllowed,liveFeedOn);
setupCheckBox('NOVATOR_UPDATE_CONTENT',contentFeedAllowed,contentFeedOn);
setupCheckBox('NOVATOR_UPDATE_TEST',testFeedAllowed,testFeedOn);
setupCheckBox('NOVATOR_UPDATE_UAT',uatFeedAllowed,uatFeedOn);

]]>
<xsl:if test="key('pageData','update_allowed')/value = 'N'">
document.getElementById('SaveCode').disabled = true;
</xsl:if>

<![CDATA[
}

function submitForm(actionType)
{
    document.maint.action = "FuelSurchargeMaintAction.do";
    document.maint.action_type.value = actionType;
    document.maint.submit();
}

function doMainMenuAction()
{
    submitForm ("MainMenuAction");
}

function doBackAction()
{
    document.maint.adminAction.value = "operations";
    submitForm ("BackAction");
}

function performSave()
{
    if(validateForm()) {
        submitForm ("Save");
    }
}

function validateForm()
{
    var form = document.maint;
    var msg = "";
    form.fuel_surcharge_amt.value    = trim(form.fuel_surcharge_amt.value);
    form.surcharge_description.value = trim(form.surcharge_description.value);
    
    if(form.fuel_surcharge_amt.value != "" && (!isFloat(form.fuel_surcharge_amt.value)))
    {
            form.fuel_surcharge_amt.className="ErrorField";
            if (msg == "") form.fuel_surcharge_amt.focus();
            msg = msg + "Fuel Surcharge must be numeric.\n";
    }

    if(form.apply_surcharge_code.value == "ON" )
    {
    
        if(form.fuel_surcharge_amt.value == "")
        {
            form.fuel_surcharge_amt.className="ErrorField";
            if (msg == "") form.fuel_surcharge_amt.focus();
            msg = msg + "Fuel Surcharge is required.\n";
        }

       if(form.surcharge_description.value == "")
       {
            form.surcharge_description.className="ErrorField";
            if (msg == "") form.surcharge_description.focus();
            msg = msg + "Surcharge Description is required.\n";
       }

    } 
    if (msg != "" ) {
        alert(msg);
        return false;
    }
    return true;
}

function trim(str)
{
	str=str.replace(/^\s*(.*)/, "$1");
	str=str.replace(/(.*?)\s*$/, "$1");
	return str;
}     

]]>
</script>
</head>

<body onload="javascript:init();">

<table width="750" border="0" align="center">
    <tr>
        <td width="30%">
        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </td>
        <td width="40%" align="center" valign="center">
        <font class="Header">Fuel Surcharge Maintenance</font>
        </td>
        <td  id="time" align="right" valign="center" class="Label" width="30%">
        <script type="text/javascript">startClock();</script>
        </td>
    </tr>
</table>

<form name="maint" method="post">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
<input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>

<table width="750" align="center" cellspacing="1">
    <tr>
        <td align="right">
        <button accesskey="B" class="BlueButton" style="width:75;" name="Back" tabindex="1" onclick="javascript:doBackAction();">(B)ack</button>
        <button accesskey="M" class="BlueButton" style="width:75;" name="MainMenu" tabindex="1" onclick="javascript:doMainMenuAction();">(M)ain Menu</button>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<table width="750" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
        <div id="fuelSurchargeDiv">

            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="key('pageData','action_response')/value = 'saveSuccessful'">
                                    <td class="OperationSuccess">
                                        &nbsp;Fuel Surcharge configuration saved successfully.
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td class="errorMessage">
                                        <xsl:value-of select="key('pageData','action_response')/value"/>
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>        
                        <table width="50%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr>
                                <td class="Label" align="right">
                                    Fuel Surcharge Flag:&nbsp;&nbsp;
                                </td>
                                <td>
                                    <select name="apply_surcharge_code" tabindex="10" >
                                    <xsl:choose>
                                        <xsl:when test="root/fuel_surcharge/apply_surcharge_code = 'ON'">
                                            <option value="ON" selected="selected">On</option>
                                            <option value="OFFC" >Off/Custom</option>
                                            <option value="OFF">Off</option>
                                        </xsl:when>
                                        <xsl:when test="root/fuel_surcharge/apply_surcharge_code = 'OFFC'">
                                            <option value="ON" >On</option>
                                            <option value="OFFC" selected="selected">Off/Custom</option>
                                            <option value="OFF">Off</option>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <option value="ON">On</option>
                                            <option value="OFFC" >Off/Custom</option>
                                            <option value="OFF" selected="selected">Off</option>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    </select>
                                </td>
                            </tr>     
                            <tr>
                                <td class="Label" align="right">
                                    Fuel Surcharge Default: $&nbsp;&nbsp;
                                </td>
                                <td>
                                    <input type="text" name="fuel_surcharge_amt" size="4" maxlength="4" tabindex="11" value="{root/fuel_surcharge/fuel_surcharge_amt}"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label" align="right">
                                    Surcharge Description Default:&nbsp;&nbsp;
                                </td>
                                <td>
                                    <input type="text" name="surcharge_description" tabindex="12" maxlength="50" value="{root/fuel_surcharge/surcharge_description}"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label" align="right">
                                    Send Surcharge To Florist: $&nbsp;&nbsp;
                                </td>
                                <td>
                                    <select name="send_surcharge_to_florist" tabindex="13" >
                                    <xsl:choose>
                                        <xsl:when test="root/fuel_surcharge/send_surcharge_to_florist = 'Y'">
                                            <option value="Y" selected="selected">Yes</option>
                                            <option value="N" >No</option>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <option value="N" selected="selected">No</option>
                                            <option value="Y" >Yes</option>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                 </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label" align="right">
                                    Display Surcharge on Separate Line for Order Confirmation(Default): $&nbsp;&nbsp;
                                </td>
                                <td>
                                    <select name="display_surcharge" tabindex="14" >
                                    <xsl:choose>
                                        <xsl:when test="root/fuel_surcharge/display_surcharge = 'Y'">
                                            <option value="Y" selected="selected">Yes</option>
                                            <option value="N" >No</option>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <option value="N" selected="selected">No</option>
                                            <option value="Y" >Yes</option>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                 </select>
                                </td>
                            </tr>
                            
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">                   
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
                            <tr>
                                <td align="left">	
                                    <table width="725" border="0" cellpadding="2" cellspacing="0">
                                      <tr><td align="center">
                                        <label id="toLiveLabel">Feed to Novator: </label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_LIVE"
                                              title="Send updates to Novator production web site"/>
                                        <label class="radioLabel">Live</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_CONTENT"
                                              title="Send updates to Novator content web site"/>
                                        <label class="radioLabel">Content</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_TEST"
                                              title="Send updates to Novator test web site"/>
                                        <label class="radioLabel">Test</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_UAT"
                                              title="Send updates to Novator UAT web site"/>
                                        <label class="radioLabel">UAT</label>
                                      </td></tr>
                                    </table>                                    
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <button accesskey="S" class="BlueButton" style="width:50;" name="SaveCode" tabindex="30" onclick="javascript:performSave();">(S)ave</button>
                    </td>
                </tr>
            </table>

        </div>
        </td>
    </tr>
</table>
</form>

<div id="waitDiv" style="display:none">
    <table id="waitTable" width="750" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
            <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                        <td id="waitTD" width="50%" class="waitMessage"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<!-- call footer template-->
<xsl:call-template name="footer"/>
</body>
</html>
</xsl:template>  
</xsl:stylesheet>
