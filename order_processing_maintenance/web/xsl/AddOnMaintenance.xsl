<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="commonUtil.xsl"/>

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->


<xsl:template match="/root">



<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <title>AddOn Maintenance</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>
    <link rel="stylesheet" type="text/css" href="css/calendar.css"></link>

    <script type="text/javascript" src="js/AddOnMaintenance.js"></script>
    <script type="text/javascript" src="js/Tokenizer.js"></script>
    <script type="text/javascript" src="js/FormChek.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>  
    <script type="text/javascript" src="js/calendar.js"/>
    <script type="text/javascript" src="js/prototype.js"></script>
    <script type="text/javascript" src="js/rico.js"/>
    <script type="text/javascript" src="js/ftdajax.js"/>
    <script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/copyright.js"></script>
	<script type="text/javascript" src="js/jquery-1.4.4.js"></script>
    <script type="text/javascript" language="javascript">


//****************************************************************************************************
//*  Global variables
//****************************************************************************************************

  var somethingChanged = false;
  var liveFeedEnabled = null;
  var liveFeedOn = null;
  var contentFeedEnabled = null;
  var contentFeedOn = null;
  var testFeedEnabled = null;
  var testFeedOn = null;
  var uatFeedEnabled = null;
  var uatFeedOn = null;
  var linkedOccasions = null;
  liveFeedEnabled = 'NO';
  liveFeedOn = '<xsl:value-of select="novator_update_setup/production_feed_checked"/>';
  contentFeedEnabled = 'NO';
  contentFeedOn = '<xsl:value-of select="novator_update_setup/content_feed_checked"/>';
  testFeedEnabled = 'NO';
  testFeedOn = '<xsl:value-of select="novator_update_setup/test_feed_checked"/>';
  uatFeedEnabled = 'NO';
  uatFeedOn = '<xsl:value-of select="novator_update_setup/uat_feed_checked"/>';
  linkedOccasions = '<xsl:value-of select="AddOnVO/sLinkedOccasions"/>';
  
  // Changes for Apollo.Q3.2015 - Addon-Occasion association.  
  $(document).ready(function() {
  	//alert('Page load complete... '+linkedOccasions');
  	var occasionTokens = linkedOccasions.split(",");
	<![CDATA[
		for(i=0;i<occasionTokens.length;i++)
		{
			$('#OCC-'+occasionTokens[i]).attr("checked",true);
		}
	]]>	
  });
  
  <![CDATA[
  function dispCopyright()
  {
		var copyright = '';
		
		copyright += '<div align="center">';
		copyright += 'COPYRIGHT &copy; '+new Date().getFullYear()+'. FTD INC. ALL RIGHTS RESERVED.';
		copyright += '</div>';
		
		var jsDisc = document.getElementById("disclaimer");
		jsDisc.innerHTML = copyright;
  }
  	
  ]]>
   </script>


  </head>
  <body onload="javascript: init()">
    <form name="fAddOnMaintenance" method="post">

      <input type="hidden" name="securitytoken"          id="securitytoken"           value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"                id="context"                 value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"            id="adminAction"             value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="hAddOnId"               id="hAddOnId"                value="{key('pageData', 'hAddOnId')/value}"/>
      <input type="hidden" name="addOnIdInUse"           id="addOnIdInUse"            value="false"/>
      <input type="hidden" name="productIdInUse"         id="productIdInUse"          value="true"/>
      <input type="hidden" name="default_card_price"     id="default_card_price"      value="{key('pageData', 'default_card_price')/value}"/>
      <input type="hidden" name="hAddOnTypeDescription"  id="hAddOnTypeDescription"   value=""/>
      <input type="hidden" name="original_action_type"   id="original_action_type"    value="{key('pageData', 'original_action_type')/value}"/>
	  <input type="hidden" name="hLinkedOccasions" 		 id="hLinkedOccasions"/>

      
      <!-- Main Div -->
      <div id="content" style="display:block">


        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
          <tr>
            <td width="30%" height="33" valign="top">
              <div class="floatleft">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
              </div>
            </td>
            <td width="40%" align="center" valign="top" class="Header" id="pageHeader">
              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <script language="javascript">document.write("<H2>Edit Add-On</H2>");</script>
                     <input type="hidden" name="original_action_type" id="original_action_type"   value="edit"/>
                </xsl:when>
                <xsl:otherwise>
                  <script language="javascript">document.write("<H2>Add Add-On</H2>");</script>
                     <input type="hidden" name="original_action_type" id="original_action_type"   value="add"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td width="30%">&nbsp;</td>
          </tr>
          <tr><td colspan="3">&nbsp;</td></tr>
        </table>
        
        <table border="0" width="100%"  style="border-collapse: collapse">
			<tr>
				<td align="left" width="50%">

        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">


          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td width = "50%" class="label" align="right">
              Add-On Id&nbsp;:&nbsp;&nbsp;
            </td>
            <td width = "50%" align="left">
              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <label name="lAddOnId" id="lAddOnId">
                    <xsl:value-of select="AddOnVO/addOnId"/>
                    <input type="hidden" name="tAddOnId" id="tAddOnId">
                      <xsl:attribute name="value"><xsl:value-of select="AddOnVO/addOnId"/></xsl:attribute>
                  </input>
                  </label>
                </xsl:when>
                <xsl:otherwise>
                  <input type="text" name="tAddOnId" id="tAddOnId" onchange="javascript:somethingChanged=true;" maxlength="10">
                    <xsl:attribute name="value"><xsl:value-of select="AddOnVO/addOnId"/></xsl:attribute>
                  </input>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          
                    <!-- DI-4: Add-on Dashboard Enhancements - Added 2 new attributes PQuad Accessory Id and FTD West Add-on to the add-on. -->
          <tr>
            <td class="label" align="right">
              PQuad Accessory Id&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tPQuadAccId" id="tPQuadAccId" onchange="javascript:somethingChanged=true;" size="40" maxlength="32">
                <xsl:attribute name="value"><xsl:value-of select="AddOnVO/sPquadAccsryId"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          
          <tr>
            <td class="label" align="right">
              FTD West Add-on&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
             <select name="isFtdWestAddon" id="isFtdWestAddon">
                <option value="Yes"><xsl:if test="AddOnVO/isFtdWestAddon = 'true'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>Yes</option>
                <option value="No"> <xsl:if test="(AddOnVO/isFtdWestAddon = 'false') or (string-length(AddOnVO/isFtdWestAddon) = 0)"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>No</option>
              </select>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          
          
          <tr>
            <td class="label" align="right">
              Add-On Type&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <select name="sAddOnTypeOption" id="sAddOnTypeOption" onchange="javascript:defaultModifierChanged();">
                <xsl:for-each select="/root/add_on_types/add_on_type">          
                    <xsl:choose>
                            <xsl:when test="/root/AddOnVO/addOnTypeId = addon_type_id">
                                    <option id="{addon_type_id}" value="{addon_type_id}" addon_type_id_description="{description}" selected="selected" xsl:use-attribute-sets=""><xsl:value-of select="description"/></option>
                            </xsl:when>
                            <xsl:otherwise>
                                    <option id="{addon_type_id}" value="{addon_type_id}" addon_type_id_description="{description}" xsl:use-attribute-sets=""><xsl:value-of select="description"/></option>
                            </xsl:otherwise>
                    </xsl:choose>
               </xsl:for-each>
            </select>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>


          <tr>
            <td class="label" align="right">
              Add-On Description&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tAddOnDescription" id="tAddOnDescription" onchange="javascript:somethingChanged=true;" size="60" maxlength="50">
                <xsl:attribute name="value"><xsl:value-of select="AddOnVO/addOnDescription"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>


          <tr>
            <td class="label" align="right">
              Add-On Text&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <textarea name="tAddOnText" COLS="50" ROWS="3" id="tAddOnText" onchange="javascript:somethingChanged=true;" onKeyDown="limitText(this.form.tAddOnText, 225);">
                <xsl:value-of select="AddOnVO/addOnText"/>
              </textarea>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Price $&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tAddOnPrice" id="tAddOnPrice" onchange="javascript:somethingChanged=true;" maxlength="6">
                <xsl:if test="AddOnVO/addOnPrice != ''">
                  <xsl:attribute name="value">
                    <xsl:value-of select='format-number(AddOnVO/addOnPrice, "0.00")' />
                  </xsl:attribute>
                </xsl:if>
              </input>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Display Price&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <select name="sDisplayPriceOption" id="sDisplayPriceOption" onchange="javascript:somethingChanged=true;">
                <option value="Yes"><xsl:if test="AddOnVO/displayPriceFlag = 'true'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>Yes</option>
                <option value="No"> <xsl:if test="AddOnVO/displayPriceFlag = 'false'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>No</option>
              </select>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Product Id&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tProductId" id="tProductId" onchange="javascript:somethingChanged=true;" maxlength="10">
                <xsl:attribute name="value"><xsl:value-of select="AddOnVO/productId"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Available&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <select name="sAvailableOption" id="sAvailableOption" onchange="javascript:somethingChanged=true;">
                <option value="Yes"><xsl:if test="AddOnVO/addOnAvailableFlag = 'true'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>Yes</option>
                <option value="No"> <xsl:if test="AddOnVO/addOnAvailableFlag = 'false'"> <xsl:attribute name="selected">true</xsl:attribute></xsl:if>No</option>
              </select>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Add-On Default&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <select name="sAddOnDefaultOption" id="sAddOnDefaultOption" onchange="javascript:defaultModifierChanged();">
                <option value="Yes"><xsl:if test="AddOnVO/defaultPerTypeFlag = 'true'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>Yes</option>
                <option value="No"> <xsl:if test="AddOnVO/defaultPerTypeFlag = 'false'"> <xsl:attribute name="selected">true</xsl:attribute></xsl:if>No</option>
              </select>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              UNSPSC&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tUnspsc" id="tUNSPSC" onchange="javascript:somethingChanged=true;" maxlength="10">
                <xsl:attribute name="value"><xsl:value-of select="AddOnVO/addOnUNSPSC"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          <tr>
            <td class="label" align="right">
              Weight&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tWeight" id="tWeight" onchange="javascript:somethingChanged=true;" maxlength="5">
                <xsl:if test="AddOnVO/addOnWeight != ''">
                  <xsl:attribute name="value">
                    <xsl:value-of select='format-number(AddOnVO/addOnWeight, "0.00")' />
                  </xsl:attribute>
                </xsl:if>
              </input>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
           <tr>                                                                                                                                          
            <td class="label" align="right"> Add-on Delivery Type&nbsp;:&nbsp;&nbsp; </td>
            
            <td align="left" valign="top">                                                                                                              
              <table>                                                                                                                                    
                <tr>
               
				 <td>
					<input type="radio" name="tDeliverytype" value="florist" checked="true">
					<xsl:if test="AddOnVO/addOnDeliveryType = 'florist' "> 
						<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
				   </input>
				</td>
					<td>
						<label class="radioLabel">Florist</label>&nbsp;&nbsp;
					</td>
					
					<td>
						<input type="radio" name="tDeliverytype"  value="dropship">
						<xsl:if test="AddOnVO/addOnDeliveryType = 'dropship'" >
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>	
						</input>
					</td>
					<td>
						<label class="radioLabel">Dropship</label>&nbsp;&nbsp;
					</td>
				
					<td>
						<input type="radio" name="tDeliverytype" value="both">
						<xsl:if test="AddOnVO/addOnDeliveryType = 'both'" >
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>	
						</input>
					</td>
					<td>
						<label class="radioLabel">Both</label>&nbsp;&nbsp;
					</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>                                                                                                                                          
            <td class="label" align="right">
              Website Update&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left" valign="top">                                                                                                              
              <table>                                                                                                                                    
                <tr>                        
                  <td>
                    <input type="checkbox" name="NOVATOR_UPDATE_LIVE" title="Send updates to Novator production web site"/>
                  </td>
                  <td>
                    <label class="radioLabel">Live</label>&nbsp;&nbsp;
                  </td>
                  <td>
                    <input type="checkbox" name="NOVATOR_UPDATE_CONTENT" title="Send updates to Novator content web site"/>
                  </td>
                  <td>
                    <label class="radioLabel">Content</label>&nbsp;&nbsp;
                  </td>
                  <td>
                    <input type="checkbox" name="NOVATOR_UPDATE_TEST" title="Send updates to Novator test web site"/>
                  </td>
                  <td>
                    <label class="radioLabel">Test</label>&nbsp;&nbsp;
                  </td>
                  <td>
                    <input type="checkbox" name="NOVATOR_UPDATE_UAT" title="Send updates to Novator UAT web site"/>
                  </td>
                  <td>
                    <label class="radioLabel">UAT</label>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

<!--
          <tr><td colspan="2">&nbsp;</td></tr>


          <tr>
            <td align="left" valign="top">
              <button name="bSave"      id="bSave"      class="BlueButton" style="width:120;" accesskey="S" onclick="performSave();">(S)ave</button>
            </td>
            <td align="right" valign="top">
              <button name="bDashboard" id="bDashboard" class="BlueButton" style="width:120;" accesskey="B" onclick="performDashboard();">Add-on Dash(b)oard</button>
              <button name="bMainMenu"  id="bMainMenu"  class="BlueButton" style="width:120;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
            </td>
          </tr>

          <tr><td colspan="2">&nbsp;</td></tr>
-->
        </table>
        
        		</td>
        		<td width="5%"><img src="images/Seperator.gif"/></td>
        		<td align="left" width="49%" valign="top">
					<div style="margin-top:15px; height:440px; width:300px; border:1px solid black; overflow:scroll; " >
						<table width="100%" border="0" cellpadding="1" cellspacing="0" align="center">
							<tr>
							<td class="Header"><h4>Occasions</h4></td>
							</tr>
							<!-- Apollo.Q3.2015 changes: XSL code to loop through occasions goes here -->
							<xsl:for-each select="/root/occasions/occasion">
							<tr>
								<td><input type="checkbox" class="cOccasions" id="{occasion_id}">&nbsp; <xsl:value-of select="description"/></input></td>
							</tr>
							</xsl:for-each>
							
						</table>
					</div>
					<div style="margin-top:5px">
						<button name="bSelAll" id="bSelAll" class="BlueButton" style="width:80;" accesskey="A" onclick='$(".cOccasions").attr("checked",true);'>Select (A)ll</button>
						&nbsp;&nbsp;
						<button name="bUnSelAll" id="bUnSelAll" class="BlueButton" style="width:80;" accesskey="U" onclick='$(".cOccasions").attr("checked",false);'>(U)nselect All</button>
					</div>
				</td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td align="left" valign="top">
						<button name="bSave" id="bSave" class="BlueButton" style="width:120;" accesskey="S" onclick="performSave();">(S)ave</button>
					</td>
					<td align="right" valign="top" colspan="2">
						<button name="bDashboard" id="bDashboard" class="BlueButton" style="width:120;" accesskey="B" onclick="performDashboard();">Add-on Dash(b)oard</button>
						<button name="bMainMenu" id="bMainMenu" class="BlueButton" style="width:120;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
					</td>
				</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
		</table>					
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>

      



      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="disclaimer" id="disclaimer"></td><script>dispCopyright();</script>
        </tr>
      </table>

    </form>
    <iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
    <script type="text/javascript" language="javascript">
        <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->
      <xsl:if test = "key('pageData', 'errorMessage')/value != ''">
         displayOkError('<xsl:value-of select = "key('pageData', 'errorMessage')/value"/>');
      </xsl:if>
      
      
      
    </script>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
