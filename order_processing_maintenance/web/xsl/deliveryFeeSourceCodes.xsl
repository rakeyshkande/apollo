<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="header.xsl" />
	<xsl:import href="footer.xsl" />
	<xsl:import href="securityanddata.xsl" />
	<xsl:output method="html" indent="no" />
	<xsl:output indent="yes" />
	<xsl:key name="pageData" match="root/pageData/data" use="name" />
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html" />
				<title>
					FTD - <xsl:value-of select="key('pageData','screenName')/value" /> Maintenance
				</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css" />
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
	
				<script type="text/javascript"> <![CDATA[
					function init() {
						]]>
						<xsl:for-each select="root/source_codes/source_code">
							document.getElementById("sourceCodes_textarea").value =
							document.getElementById("sourceCodes_textarea").value + '<xsl:value-of select="source_code" />' + " ";
						</xsl:for-each>
						document.getElementById("sourceCodes_textarea").disabled = true;
						<![CDATA[
					}
					]]>
				</script>
			</head>
	
			<body onload="javascript:init();">
				<center>
					<h2> Source Codes attached to ID&nbsp;<xsl:value-of select="key('pageData','deliveryId')/value" /></h2>
					<table width="350" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td align="center">
											<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
												<tr></tr>											
												<tr>
													<td align="center">
														<table width="425" border="0" cellpadding="2" cellspacing="0">
															<tr> 
																<td align="left" width="100%" height="100%">
																	<textarea name="sourceCodes_textarea" rows="10"	cols="80"/>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</center>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
