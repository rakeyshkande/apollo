<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<xsl:template match="/">

<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"></META>
      <title>Vendor Maintenance</title>
	  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" src="js/calendar.js"/>	
  	  <script type="text/javascript" src="js/copyright.js"></script>
		<script>
		<![CDATA[
    
    var PAGE_URL = '';
    var has_changed = false;
  
    /*
     * If any changes were made to this page this flag is set to true.
     * We make no data checks here the server will handle that. 
     * We simply check if a user modified any fields in anyway.
     */
    function doOnChangeAction()
    {	
      has_changed = true;
    }
    
    function doCheckOnKeyDown()
    {
      has_changed = true;
    }


		/*
		Using the specified URL submit the form
		*/
		function performAction(url)
		{
			document.forms[0].action = url;
			document.forms[0].submit();
		}
		
		function addEmail()
		{
			var url = "VendorInventoryControlAction.do" +
					   "?action_type=add_email"  + getSecurityParams(false);
			performActionWithLock(url);
		}
		
		function deleteEmail(removeType)
		{
			var url = "VendorInventoryControlAction.do" +
					   "?action_type=remove_email"  + "&remove_type=" + removeType + getSecurityParams(false);
			performActionWithLock(url);
		}
		
		function doBack()
		{
      if(!has_changed ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
      {
        var url = "VendorInventoryControlAction.do" +
               "?action_type=back"  +  getSecurityParams(false);
        performActionWithUnlock(url);
      }
		}
	
		function doMenu()
		{
      if(!has_changed ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
      {
        var url = "VendorInventoryControlAction.do" +
               "?action_type=menu"  +  getSecurityParams(false);
        performActionWithUnlock(url);
      }
		}
		
		function saveRecord()
		{
			var url = "VendorInventoryControlAction.do" +
					   "?action_type=save"  + getSecurityParams(false);
			performActionWithLock(url);
		}
		
		function deleteRecord()
		{
      if(doExitPageAction("Are you sure you want to delete this record?")) {
        var url = "VendorInventoryControlAction.do" +
               "?action_type=delete"  + getSecurityParams(false);
        performActionWithLock(url);
      }
		}

 		function performActionWithLock(url)
 		{
      PAGE_URL = url;
      doRecordLock('VENDOR_INVENTORY','check', url); 
    }
              
    function performActionWithUnlock(url)
    {
      PAGE_URL = url;
      doRecordLock('VENDOR_INVENTORY','unlock', url); 
      doContinueProcess();
    }

    function doContinueProcess()
    {
      document.forms[0].action = PAGE_URL;
      document.forms[0].target = window.name;
      document.forms[0].submit();
    }
            
    function checkAndForward(forwardAction)
    {
      doContinueProcess();
    }

    function doMainMenu() 
    {
      var url = 'MainMenuAction.do';
      url += '?adminAction=' + document.customerHold.adminAction.value + '&';
      
      performActionWithUnlock(url);    
    }
		 ]]>
		</script>


   </head>
	<body>
	<form name="customerHold" method="post"  >
	<input type="hidden" name="update_type" value="{//update_type}"/>								
	<input type="hidden" name="from_page" value="{//from_page}"/>
  <input type="hidden" name="lock_id" value="{//pagedata/product_id}" />
  <input type="hidden" name="adminAction" value="{//adminAction}" />
  
	<xsl:call-template name="securityanddata"/>

<!-- Header-->
   <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
   		<tr>
   			<td width="30%" height="33" valign="top">
   				<div class="floatleft">
   					<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
   				</div>
   			</td>
   			<td></td>
            <td  align="right" width="5%" valign="bottom">

            </td>
        </tr>
   		<tr>
   			<td></td>
   			<td  align="center" valign="top" class="Header" id="pageHeader">
           		Dropship Inventory Control
         	</td>
			<td  id="time" align="right" valign="bottom" class="Label" width="20%" height="30">
			  <script type="text/javascript">startClock();</script>
			</td>
       </tr>
	</table>

<!-- Display table-->
   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
        	<table width="100%" align="center" border="0" class="innerTable">
          		<tr>
					<td align="left" height="10">
						<span id="invalidDiv"  class="ErrorMessage">

						</span>
					</td>
		  		</tr>
				<tr>
					<td>
						<table width="100%" border="0" align="center" >
							<tr>
								<td class="label" align="right" width="20%">
									Product ID:
								</td>
								<td  width="30%"><xsl:value-of select="//pagedata/product_id"/>
								<input type="hidden" name="product_id" value="{//pagedata/product_id}"/>								
								</td>
								<td class="label" align="right"  width="10%">
									Product Status:
								</td>
								<td  ><xsl:value-of select="//status"/>
								<input type="hidden" name="status" value="{//status}"/>
								</td>
							</tr>
							<tr>
								<td class="label" align="right">
									Product Name:
								</td>
								<td><xsl:value-of select="//product_name"/>
								<input type="hidden" name="product_name" value="{//product_name}"/>
								</td>
								<td class="label" align="right">
								</td>
								<td>
								</td>
							</tr>

							<tr>
							<td align="center" class="TotalLine" colspan="4">
								<div align="center"><strong>Vendor Information</strong><br></br>
								  </div>
								<div align="left"></div>
							</td>
							</tr>

							 <tr>
							 <td colspan="4">
							 <table width="100%" border="0">
							 
														<tr>
															<td class="label" align="right" width="25%">
																Vendor Number:
															</td>
															<td  width="25%">
																<xsl:value-of select="//vendor_id"/>
																<input type="hidden" name="vendor_id" value="{//vendor_id}"/>
															</td>
															<td class="label" align="right" width="20%">
																Vendor Name:
															</td>
															<td  width="30%">
																<xsl:value-of select="//vendor_name"/>
																<input type="hidden" name="vendor_name" value="{//vendor_name}"/>
															</td>
														</tr>
							
							
							
														<tr>
															<td class="label" align="right">
																Last Updated:
															</td>
															<td >
																													

											                    <xsl:call-template name="formatDate">
											                      <xsl:with-param name="dateTimeString" select="//updated_on"/>
											                    </xsl:call-template>

											                  
																<input type="hidden" name="updated_on" value="{//updated_on}"/>
															</td>
															<td class="label" align="right">
																Contact Number:
															</td>
															<td >
												                <xsl:choose>
												                  <xsl:when test="root/pagedata/can_edit = 'Y'">
																		
																<input type="text" name="contact_number" size="12"  
                                maxlength="12" tabindex="1"
                                onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()">
																		<xsl:attribute name="value">
																		
														                <xsl:choose>
														                  <xsl:when test="string-length(//errors/contact_number) > 0">
																			<xsl:value-of select="//record/contact_number"/>
																		  </xsl:when>
																		  <xsl:otherwise>
														                    <xsl:call-template name="formatPhoneNumber">
														                      <xsl:with-param name="phoneNumber" select="//record/contact_number"/>
														                    </xsl:call-template>
																		  </xsl:otherwise>
																		 </xsl:choose>
																			
																		

																		</xsl:attribute>
																	</input>


																<b>Ext:</b>
																<input type="text" name="contact_extension" size="10"  
                                maxlength="10" tabindex="1"  value="{//record/contact_extension}"
                                onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()"/>
																<span id="invalidDiv"  class="ErrorMessage"><xsl:value-of select="//errors/contact_number"/></span>
																	</xsl:when>
																<xsl:otherwise>
																		<xsl:value-of select="//record/contact_number"/>
																		<b>Ext:</b>
																		<xsl:value-of select="//record/contact_extension"/>
																</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>
							
							
														<tr>
															<td class="label" align="right">
																Start Date:
															</td>
															<td >
												                <xsl:choose>
												                  <xsl:when test="root/pagedata/can_edit = 'Y'">
																	<input type="text" name="start_date" size="8"  tabindex="1"  onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()">
																		<xsl:attribute name="value">
																		
											                    <xsl:call-template name="formatDate">
											                      <xsl:with-param name="dateTimeString" select="//record/start_date"/>
											                    </xsl:call-template>

																		
																		</xsl:attribute>
																	</input>
																	<img id="calstart" src="images\calendar.gif" />
																	<span id="invalidDiv"  class="ErrorMessage"><xsl:value-of select="//errors/start_date"/></span>
																	</xsl:when>		
																<xsl:otherwise>
																<input type="hidden" name="start_date" size="8"  tabindex="1"  ></input>
											                    <xsl:call-template name="formatDate">
											                      <xsl:with-param name="dateTimeString" select="//record/start_date"/>
											                    </xsl:call-template>
																</xsl:otherwise>	
																</xsl:choose>						
															</td>
															<td class="label" align="right">
																End Date:
															</td>
															<td >
												                <xsl:choose>
												                  <xsl:when test="root/pagedata/can_edit = 'Y'">
																<input type="text" name="end_date" size="8"  
                                tabindex="1"  value="{//record/end_date}"
                                onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()">
																		<xsl:attribute name="value">

											                    <xsl:call-template name="formatDate">
											                      <xsl:with-param name="dateTimeString" select="//end_date"/>
											                    </xsl:call-template>


																		</xsl:attribute>

																</input>
																<img id="calend" src="images\calendar.gif" />
																<span id="invalidDiv"  class="ErrorMessage"><xsl:value-of select="//errors/end_date"/></span>
																	</xsl:when>
																<xsl:otherwise>
																<input type="hidden" name="end_date" size="8"  tabindex="1"  ></input>
											                    <xsl:call-template name="formatDate">
											                      <xsl:with-param name="dateTimeString" select="//end_date"/>
											                    </xsl:call-template>
																</xsl:otherwise>
																</xsl:choose>
							
															</td>
														</tr>
							
							
														<tr>
															<td class="label" align="right">
																Inventory Level:
															</td>
															<td >
												                <xsl:choose>
												                  <xsl:when test="root/pagedata/can_edit = 'Y'">
																<input type="text" name="inventory_level" size="6"  
                                tabindex="1"  value="{//record/inventory_level}"
                                onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()"/>
																<span id="invalidDiv"  class="ErrorMessage"><xsl:value-of select="//errors/inventory_level"/></span>
																	</xsl:when>
																<xsl:otherwise>
																		<xsl:value-of select="//record/inventory_level"/>
																</xsl:otherwise>
																</xsl:choose>
							
															</td>
															<td class="label" align="right">
																Qty sold last 30 days:
															</td>
															<td >
																<xsl:value-of select="//month_count"/>
															</td>
														</tr>
														
											</table>			
								</td>
							</tr>

							<tr>
							<td align="center" class="TotalLine" colspan="4">
								<div align="center"><strong>Threshold Alerts</strong><br></br>
								  </div>
								<div align="left"></div>
								</td>
							</tr>

							<tr>
								<td colspan="2" align="center" width = "50%">
									<table align="center">
										<tr>
										<td class="label">Notice Threshold</td>
										<td>
					                <xsl:choose>
					                  <xsl:when test="root/pagedata/can_edit = 'Y'">
										<input type="text" name="notice_threshold" size="6"  
                    tabindex="1"  value="{//record/notice_threshold}"
                    onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()"/>
										<span id="invalidDiv"  class="ErrorMessage"><xsl:value-of select="//errors/notice_threshold"/></span>
										</xsl:when>
									<xsl:otherwise>
											<xsl:value-of select="//record/notice_threshold"/>
									</xsl:otherwise>
									</xsl:choose>


										</td>
										</tr>
										<tr>
										<td class="label">Notice Email Address</td>
										<td>
					                <xsl:choose>
					                  <xsl:when test="root/pagedata/can_edit = 'Y'">
											<input type="text" name="notice_email" size="55"  
                      tabindex="1" value="{//notice_email}"
                      onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()"/>
											<a href="#" onclick="addEmail();">Add</a>
											<span class="ErrorMessage"><xsl:value-of select="//notice_email_error"/></span>
										</xsl:when>
									<xsl:otherwise>

									</xsl:otherwise>
									</xsl:choose>

										</td>
										</tr>
									</table>
								</td>
								<td  width = "30%" >
									<div style="overflow:auto; width:70%; height=75; border-left: 3px solid #000; border-right: 3px solid #000; border-bottom: 3px solid #000;border-top: 3px solid #000;">
										<xsl:for-each select="root/inventorydata/notifications/records/record">
					
					            <xsl:choose>
                        <xsl:when test="//can_edit = 'Y'">
                          <input type="checkbox" name="emailChkBox{position()}"
                                 onchange = "doOnChangeAction()" onkeypress="doCheckOnKeyDown()"><xsl:value-of select="email_address"/></input><br></br>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="email_address"/><br></br>
                        </xsl:otherwise>
                      </xsl:choose>
										
                      <input TYPE="HIDDEN" NAME="email{position()}"  value="{email_address}"></input>
										</xsl:for-each>
									</div>
								</td>
								<td align="left">
					                <xsl:choose>
					                  <xsl:when test="root/pagedata/can_edit = 'Y'">
									<a href="#" onclick="deleteEmail('remove_checked');">Remove Checked</a><br></br>
									<a href="#" onclick="deleteEmail('remove_all');">Clear All</a><br></br>
										</xsl:when>
									<xsl:otherwise>

									</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>


							<tr>
							<td align="center" class="TotalLine" colspan="4">
								<div align="center"><strong>Weekly Sales Status</strong><br></br>
								  </div>
								<div align="left"></div>
								</td>
							</tr>

							<tr>
							<td colspan="4">

							<table width="70%" align="center">
							<tr><td></td></tr>


							<tr>
								<td class="label">Inventory</td>
								
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//sunday"/>
								    </xsl:call-template>									
								</td>
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//monday"/>
								    </xsl:call-template>									
								</td>
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//tuesday"/>
								    </xsl:call-template>									
								</td>
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//wednesday"/>
								    </xsl:call-template>									
								</td>
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//thursday"/>
								    </xsl:call-template>									
								</td>
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//friday"/>
								    </xsl:call-template>									
								</td>
								<td >								
								    <xsl:call-template name="formatDate">
								      <xsl:with-param name="dateTimeString" select="//saturday"/>
								    </xsl:call-template>									
								</td>								
								
								<td class="label">Week</td>
							</tr>
							<tr>
								<td class="label">Level</td>
								
								<td ><b>Sunday</b></td>
								<td ><b>Monday</b></td>
								<td ><b>Tuesday</b></td>
								<td ><b>Wednesday</b></td>
								<td ><b>Thursday</b></td>
								<td ><b>Friday</b></td>
								<td ><b>Saturday</b></td>
								
								<td class="label">Total</td>
							</tr>
							<tr>
								<td><xsl:value-of select="//inventory_level"/></td>
								
								<td ><xsl:value-of select="//sun_count"/></td>
								<td ><xsl:value-of select="//mon_count"/></td>
								<td ><xsl:value-of select="//tue_count"/></td>
								<td ><xsl:value-of select="//wed_count"/></td>
								<td ><xsl:value-of select="//thur_count"/></td>
								<td ><xsl:value-of select="//fri_count"/></td>
								<td ><xsl:value-of select="//sat_count"/></td>
								
								<td><xsl:value-of select="//week_count"/></td>
							</tr>
							
							

							</table>


							</td>
							</tr>



							<tr>
								<td  align="right" valign="top" colspan="4">
								<span id="invalidDiv"  class="ErrorMessage"><xsl:value-of select="//pagedata/save_message"/></span>

					                <xsl:choose>
					                  <xsl:when test="root/pagedata/can_edit = 'Y'">
								 			<button class="BlueButton" style="width:80;" name="savebtn" tabindex="10" onclick="saveRecord();">(S)ave </button>

					                		<xsl:choose>
					                  			<xsl:when test="root/pagedata/update_type = 'update'">
										 		<button class="BlueButton" style="width:80;" name="deletebtn" tabindex="10" onclick="deleteRecord();">(D)elete </button>
												</xsl:when>
											</xsl:choose>										

										</xsl:when>
									</xsl:choose>
								</td>
							</tr>

						</table>
					</td>
				</tr>





		  	</table>
       </td>
     </tr>


	</table>

   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
      	 <td>
			 <table>
			 	<tr>
				 <td  align="left">

				 </td>
				 </tr>
			 </table>
		  </td>
		  <td align="right">
			 <table>
			 	<tr>
				 	<td  align="right" valign="top">
					 <button class="BlueButton" style="width:80;" name="backButton" tabindex="10" onclick="doBack();">(B)ack </button>
				 	</td>
				</tr>
				<tr>
					<td align="right" valign="top">
				 	<button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10" onclick="doMainMenu()">(M)ain Menu </button>
				 	</td>
				</tr>
			 </table>
		</td>
      </tr>
</table>



<!--Copyright bar-->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
      <td></td>
   </tr>
   <tr>
      <td class="disclaimer"></td><script>showCopyright();</script>
      
   </tr>
</table>

<script type="text/javascript">
  Calendar.setup(
  {
    inputField  : "start_date",  // ID of the input field
    ifFormat    : "mm/dd/y",  // the date format
    button      : "calstart"  // ID of the button


  }
);
  Calendar.setup(
  {

    inputField  : "end_date",  // ID of the input field
    ifFormat    : "mm/dd/y",  // the date format
    button      : "calend"  // ID of the button

  }


);
</script>


</form>
<iframe id="checkLock" width="0px" height="0px" border="0"/>
</body>
</html>

</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
		</xsl:call-template>
	</xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
	<xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>

     <xsl:choose>
       <xsl:when test="substring($dateTimeString, 5, 1) != '-'">
	       <xsl:value-of select="$dateTimeString"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>
		</xsl:otherwise>
	  </xsl:choose>

	</xsl:template>



	<xsl:template name="formatDate2">
    <xsl:param name="dateTimeString"/>

		<xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>

	</xsl:template>



     <xsl:template name="formatPhoneNumber">
     <xsl:param name="phoneNumber"/>
     <xsl:choose>
       <xsl:when test="substring($phoneNumber, 4, 1)">
	       <xsl:value-of select="$phoneNumber"/>
		</xsl:when>
		<xsl:otherwise>
		<xsl:if test = "$phoneNumber != '' ">
		     <xsl:value-of select="substring($phoneNumber, 1, 3)"/>-<xsl:value-of select="substring($phoneNumber, 4, 3)"/>-<xsl:value-of select="substring($phoneNumber, 7, 4)"/>
		</xsl:if>
		</xsl:otherwise>

     </xsl:choose>
   </xsl:template>

	
</xsl:stylesheet>