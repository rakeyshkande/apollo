<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">
    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html"/>
        <title>FTD - Occasion Code Maintenance</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script type="text/javascript" src="js/clock.js"></script>
        <script type="text/javascript" src="js/commonUtil.js"></script>
        <script type="text/javascript" src="js/mercuryMessage.js"></script>
        <script type="text/javascript" src="js/FormChek.js"></script>
        <script type="text/javascript"><![CDATA[
          function init()
          {
            ]]>
            <xsl:choose>
            <xsl:when test="key('pageData','valid_flag')/value = 'Y'">
              parent.doUpdateOccasionAction();
            </xsl:when>
            <xsl:otherwise>
              replaceText('invalidDiv', '<xsl:value-of select="key('pageData','error_value')/value"/>');
              document.getElementById('r_occasion_id').focus();
            </xsl:otherwise>
            </xsl:choose>
            <![CDATA[
          }

          function replaceText( elementId, newString )
          {
						var node = document.getElementById( elementId );
						var newTextNode = document.createTextNode( newString );
						if(node.childNodes[0]!=null)
						{
							node.removeChild( node.childNodes[0] );
						}
						node.appendChild( newTextNode );
          }

          function doAddAction()
          {
            //change backgroud to white
            document.getElementById('r_occasion_id').style.backgroundColor = 'white';
            document.getElementById('r_description').style.backgroundColor = 'white';

            var returnValue = true;
            var occasionId = document.getElementById('r_occasion_id').value;
            occasionId = Trim(occasionId);
            if(  isWhitespace(occasionId) || !isInteger(occasionId) )
            {
              replaceText('invalidDiv', 'Please correct the highlighted field(s)');
              document.getElementById('r_occasion_id').style.backgroundColor = 'pink';
              document.getElementById('r_occasion_id').focus();
              returnValue = false;
            }


            if(isWhitespace(document.getElementById('r_description').value))
            {
              replaceText('invalidDiv', 'Please correct the highlighted field(s)');
              document.getElementById('r_description').style.backgroundColor = 'pink';
              document.getElementById('r_description').focus();
              returnValue = false;
            }

            if (!returnValue)
            {
              return returnValue;
            }
            else
            {
              document.maint2.action = "OccasionMaintAction.do?action_type=addOccasionAction"
                                        +  "&securitytoken=" + document.forms[0].securitytoken.value
                                        +  "&context=" + document.forms[0].context.value;
               document.maint2.submit();
             }
          }

          function doCancelAction()
          {
            document.maint2.action = "OccasionMaintAction.do?action_type=loadOccasionAction"
                                      +  "&securitytoken=" + document.forms[0].securitytoken.value
                                      +  "&context=" + document.forms[0].context.value;
            document.maint2.submit();
          }
          ]]>
        </script>
      </head>
      <body onload="javascript:init();">
        <form name="maint2" method="post">
          <input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
          <input type="hidden" name="dnis_id" value="" />
          <input type="hidden" name="next_occasion_seq" value="{key('pageData','next_occasion_seq')/value}"/>
          <input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
          <input type="hidden" name="context"                   value="{key('pageData', 'context')/value}"/>
          <input type="hidden" name="securitytoken"              value="{key('pageData', 'securitytoken')/value}"/>

          <table width="98%" align="center" cellspacing="1">
            <tr>
              <td id="lockError" class="ErrorMessage"></td>
            </tr>
          </table>
          <table width="98%" align="center" cellspacing="1" class="mainTable">
            <tr>
              <td>
                <table width="100%" align="center" class="innerTable">
                  <tr>
                    <td align="left" id="invalidDiv" class="ErrorMessage" height="10"></td>
                  </tr>
                  <tr>
                    <td align="center" class="TotalLine"><strong>Add New Occasion Code</strong></td>
                  </tr>
                  <tr>
                    <td align="center" height="10"></td>
                  </tr>
                  <tr>
                    <td align="center">
                      <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
                        <tr>
                          <td align="right" width="40%"><b>Occasion Code:</b></td>
                          <td align="left" width="60%">
                            <input type="text" name="r_occasion_id" size="6" maxlength="2" value="{root/occasion/occasionid}" tabindex="1"/>
                          </td>
                        </tr>
                        <tr>
                          <td align="right"><b>Description:</b></td>
                          <td align="left">
                            <input type="text" name="r_description" size="56" maxlength="55" value="{root/occasion/description}" tabindex="2"/>
                          </td>
                        </tr>
                        <tr>
                          <td align="right"><b>Active?</b></td>
                          <td align="left">
                            <xsl:variable name="active" select="root/occasion/active"/>
                            <xsl:if test="$active='Y'">
                              <input type="checkbox" name="r_active" tabindex="3" checked="checked"/>
                            </xsl:if>
                            <xsl:if test="not($active='Y')">
                              <input type="checkbox" name="r_active" tabindex="3"/>
                            </xsl:if>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div class="floatright">
                              <button class="BlueButton" name="ok" tabindex="4" accesskey="O" onclick="javascript:doAddAction();">(O)k </button>
                            </div>
                          </td>
                          <td>
                            <div class="floatleft">
                              <button class="BlueButton" name="cancel" tabindex="5" accesskey="C" onclick="javascript:doCancelAction();">(C)ancel </button>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
        </form>
        <div id="waitDiv" style="display:none">
          <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
            <tr>
              <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                    <td id="waitTD" width="50%" class="waitMessage"></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->