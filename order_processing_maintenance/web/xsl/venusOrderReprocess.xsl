
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
	<xsl:import href="header.xsl"/>
  -->
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<!-- Keys -->
	<xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:key name="pageData" match="root/pageData/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Reprocess Dropship Orders</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css" />
				<style type="text/css">
          #helpDiv {
            POSITION: absolute; 
            left: 17px;
            top: 58px;
          }
          
          .helpContainer {
            Z-INDEX: 200; 
            FILTER: alpha(opacity:90); 
            WIDTH: 550px; 
            POSITION: absolute; 
            opacity: 0.9;
            vertical-align: top
          }
          
          .helpDetails {
            HEIGHT: 300px
          }
          
          .helpDetails DIV {
            BORDER-RIGHT: #c5dea1 1px solid; 
            PADDING-RIGHT: 4px; 
            BORDER-TOP: #c5dea1 1px solid; 
            PADDING-LEFT: 4px; 
            PADDING-BOTTOM: 4px; 
            BORDER-LEFT: #c5dea1 1px solid; 
            PADDING-TOP: 4px; 
            BORDER-BOTTOM: #c5dea1 1px solid; 
            BACKGROUND-COLOR: #ecf3e1
          }
          
          .helpDetails a:link {
          	color: blue;
            text-decoration: underline;
          }
          
          .helpDetails a:visited {
          	color: blue;
            text-decoration: underline;
          }
          
          a:link {
          	color: #000099;
            text-decoration: none;
            font-weight: bold;
          }
          
          a:visited {
          	color: #000099;
            text-decoration: none;
            font-weight: bold;
          }
				</style>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/prototype.js"></script>
				<script type="text/javascript" src="js/scriptaculous.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
        <script type="text/javascript" src="js/Tokenizer.js"></script>
				<script type="text/javascript"> <![CDATA[
            var shadePos = "up";
            var method;
            var METHOD_SQL = "sql";
            var METHOD_LIST = "orderList";
            var updateType;
            var req;
            var READY_STATE_COMPLETE=4;
            var STATUS_OK = 200;
            //var UNIT_TEST = "&unit_test=true";
            var UNIT_TEST = "";
            var savedSQL = "";
            var savedOrderList = "";
            var tokens;
            var orderIdx=0;
            var failCount=0;

            var prefPartnerArray = new Array();
            var prefPartnerCount = 0;
            
            function init() {
              
            }
            
            function setShade() {
              if( shadePos == "up" ) {
                shadePos = "down";
                Effect.SlideDown('results');
              }
              else {
                shadePos = "up";
                Effect.SlideUp('results');
              }
            }
            
            function restoreSQL() {
              if( savedSQL.length > 0 ) {
                document.sqlform.orderList.value = savedSQL;
              }
            }
            
            function restoreOrderList() {
              if( savedOrderList.length > 0 ) {
                document.sqlform.orderList.value = savedOrderList;
              }
            }
          
            function doSubmitAction()
            {
              document.sqlform.submitPageButton.disabled = true;
              if( validateForm() ) {
                //alert("Radio button selected is "+method+"\r\nChoice is "+updateType);
                if( shadePos == "up" ) {
                  setShade();
                }
                
                if( method==METHOD_SQL ) {
                  document.sqlform.failedColumn.value = "";
                  document.sqlform.passedColumn.value = "Processing sql..."
                  var sqlStmt = stringTrim(document.sqlform.orderList.value);
                  sqlStmt = specialCharsToSpaces(sqlStmt);
                  var url = "VenusOrderReprocessAction.do?action_type="+escape(METHOD_SQL)+"&sql="+escape(sqlStmt)+UNIT_TEST+getSecurityParamsByName(false);
                  retrieveURL(url,processSqlResponse);
                } else {
                  document.sqlform.passedColumn.value = "";
                  document.sqlform.failedColumn.value = "";
                  savedOrderList = stringTrim(specialCharsToSpaces(document.sqlform.orderList.value));
                  tokens = savedOrderList.tokenize(" ", true, true);
                  savedOrderList = savedOrderList.replace(/ /g,"\r");
                  restoreOrderList();
                  
                  //alert("Token count = "+tokens.length);
                  if( tokens==null || tokens.length==0 )
                  {
                    alert("No orders numbers were entered.");
                    document.sqlform.submitPageButton.disabled = false;
                  }
                  else 
                  {
                    startProcessingOrders();
                  }
                }
                
              }
              else {
                document.sqlform.submitPageButton.disabled = false;
              }
              
              return false;
            }
  
            function processSqlResponse() {
              if (req.readyState == READY_STATE_COMPLETE) { // Complete
                document.sqlform.passedColumn.value = "Processing sql...Completed"
                //alert("Status returned of "+req.status);
                if (req.status == STATUS_OK) { // OK response
                  var returnedText = req.responseText;
                  var testText;
                  
                  returnedText = stringTrim(specialCharsToSpaces(returnedText));
                  tokens = returnedText.tokenize(" ", true, true);
                  if( tokens==null || tokens.length==0 /*|| tokens[0].substr(0,testText.length)==testText*/ )
                  {
                    alert("No orders were returned for the selected criteria.");
                    document.sqlform.submitPageButton.disabled = false;
                  //} else if( returnedText.substr(0,securityText.length)==securityText ) {
                  //  window.location.href="admin.do?securitytoken=<%=securityToken%>"; 
                  } else {
                    testText = "Error"
                    if( returnedText.substr(0,testText.length)==testText ) {
                      //alert(returnedText);
                      //document.sqlform.submitPageButton.disabled = false;
                      reprocessOrder();
                    }
                    else {
                      testText = "Fatal"
                      if( returnedText.substr(0,testText.length)==testText ) {
                        alert(returnedText);
                        document.sqlform.submitPageButton.disabled = false;
                      }
                      else {
                        //alert("A total of "+tokens.length+" orders to process.");
                        savedOrderList = returnedText.replace(/ /g,"\r");
                        var sqlStmt = stringTrim(document.sqlform.orderList.value);
                        if( sqlStmt.length>0 ) {
                          savedSQL = sqlStmt;
                        }
                        document.getElementById("listRadio").click();
                        
                        document.sqlform.orderList.value = stringTrim(savedOrderList);
                        if( confirm("A total of "+tokens.length+" orders were returned.  Do you want to continue?") ) {
                          startProcessingOrders();
                        }
                        else {
                          document.sqlform.submitPageButton.disabled = false;
                        }
                      }
                    }
                  }
                } else {
                  alert("Problem: " + req.statusText);
                  document.sqlform.failedColumn.value = "Problem: " + req.statusText;
                  document.sqlform.submitPageButton.disabled = false;
                }
              }
            }
            
            function processReprocessResponse() {
              if (req.readyState == READY_STATE_COMPLETE) { // Complete
                if (req.status == STATUS_OK) { // OK response
                  var returnedText = req.responseText;
                  var testText = "Error"
                  if( returnedText.substr(0,testText.length)==testText ) {
                    markDone();
                    failCount++;
                    var newValue = document.sqlform.failedColumn.value + returnedText;
                    document.sqlform.failedColumn.value = newValue;
                    scrollToBottom(document.sqlform.failedColumn);
                    reprocessOrder();
                  }
                  else {
                    testText = "Fatal"
                    if( returnedText.substr(0,testText.length)==testText ) {
                      markFatal();
                      alert(returnedText);
                      document.sqlform.submitPageButton.disabled = false;
                    }
                    else {
                      markDone();
                      var newValue = document.sqlform.passedColumn.value + returnedText.replace("Success - ","");
                      document.sqlform.passedColumn.value = newValue;
                      scrollToBottom(document.sqlform.passedColumn);
                      reprocessOrder();
                    }
                  }
                }
              }
            }
            
            function markDone() {
              var newValue = document.sqlform.orderList.value;
              newValue = newValue.replace(" processing"," done");
              document.sqlform.orderList.value = newValue;
              scrollToBottom(document.sqlform.orderList);
            }
            
            function markFatal() {
              var newValue = document.sqlform.orderList.value;
              newValue = newValue.replace(" processing"," a fatal error has occurred");
              document.sqlform.orderList.value = newValue;
              scrollToBottom(document.sqlform.orderList);
            }
            
            function startProcessingOrders() {
              orderIdx=0;
              document.sqlform.orderList.value = "";
              document.sqlform.passedColumn.value = "";
              document.sqlform.failedColumn.value = "";
              document.sqlform.submitPageButton.disabled = true;
              reprocessOrder();
            }
            
            function reprocessOrder() {
              //alert("Got to reprocessOrder() with an orderIdx of "+orderIdx);
              if( orderIdx==tokens.length ) {
                //scrollToBottom(document.sqlform.orderList);
                alert("Process complete.  "+orderIdx+" orders processed.  "+failCount+" failed.");
                failCount=0;
                document.sqlform.submitPageButton.disabled = false;
              }
              else {
                var orderId = tokens[orderIdx];
                var newValue = document.sqlform.orderList.value;
                newValue = newValue + orderId + " processing\r";
                document.sqlform.orderList.value = newValue;
                scrollToBottom(document.sqlform.orderList);
                var url = "VenusOrderReprocessAction.do?action_type="+escape(updateType)+"&external_order_number="+escape(orderId)+UNIT_TEST+getSecurityParamsByName(false);
                
                //Append preferred partner checkbox values.
                for (i=0;i<prefPartnerArray.length;i++) {
                    var parmName = prefPartnerArray[i];
                    var parmValue = document.getElementById(prefPartnerArray[i]).checked;

                    url = url + "&" + parmName + "=" + parmValue;
                }
    
                retrieveURL(url,processReprocessResponse);
                orderIdx++;
              }
            }
            
            function validateForm() {
              var returnValid = true;
              method = getCheckedValue(document.sqlform.orderSelect);
              updateType = document.sqlform.actionType.value;
              
              return true;
            }
            
            // return the value of the radio button that is checked
            // return an empty string if none are checked, or
            // there are no radio buttons
            function getCheckedValue(radioObj) {
              if(!radioObj)
                return "";
              var radioLength = radioObj.length;
              if(radioLength == undefined)
                if(radioObj.checked)
                  return radioObj.value;
                else
                  return "";
              for(var i = 0; i < radioLength; i++) {
                if(radioObj[i].checked) {
                  return radioObj[i].value;
                }
              }
              return "";
            }

            function retrieveURL(url, callback) {
              if (window.ActiveXObject) { // IE
                req = new ActiveXObject("Microsoft.XMLHTTP");
                if (req) {
                  req.onreadystatechange = callback;
                  req.open("GET", url, true);
                  req.send();
                }
              } else if (window.XMLHttpRequest) { // Non-IE browsers
                req = new XMLHttpRequest();
                req.onreadystatechange = callback;
                try {
                  req.open("GET", url, true);
                } catch (e) {
                  alert(e);
                }
                req.send(null);
              }
            }
            
            function getSecurityParamsByName(areOnlyParams){
              var securitytoken = document.sqlform.securitytoken.value;
              var context = document.sqlform.context.value;

              return   ((areOnlyParams) ? "?" : "&") +
                "securitytoken=" + securitytoken +
                "&context=" + context;
            }
  
            function specialCharsToSpaces(strObject) {
              //convert any special characters to spaces
              while(strObject.indexOf("\n") != -1)
                strObject = strObject.replace("\n"," ");
          
              while(strObject.indexOf("\f") != -1)
                strObject = strObject.replace("\f"," ");
          
              while(strObject.indexOf("\b") != -1)
                strObject = strObject.replace("\b"," ");
          
              while(strObject.indexOf("\r") != -1)
                strObject = strObject.replace("\r"," ");
          
              while(strObject.indexOf("\t") != -1)
                strObject = strObject.replace("\t"," ");
                
                return strObject;
            }
            
            function stringTrim(value) {
              var temp = value;
              var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
              if (obj.test(temp)) { temp = temp.replace(obj, '$2'); }
              var obj = /  /g;
              while (temp.match(obj)) { temp = temp.replace(obj, " "); }
              return temp;
            }
  
            function scrollToBottom (element) {
              element.scrollTop = element.scrollHeight;
            }

            function doMainMenuAction()
            {
              document.sqlform.action = "VenusOrderReprocessAction.do?action_type=MainMenuAction&";
              document.sqlform.submit();
            }
            
   				]]></script>
			</head>
			<body onload="javascript:init();">
        <!-- Header-->
        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
          <tr>
            <td width="33%" height="33" valign="top">
              <div class="floatleft">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
              </div>
            </td>
            <td align="center" height="32" class="Header" id="pageHeader">Reprocess Dropship Orders</td>
            <td id="time" align="right" class="Label" width="33%" height="32">
              <script type="text/javascript">startClock();</script>
            </td>
          </tr>
          <tr><td colspan="3"><HR/></td></tr>
        </table>
        
        <div id="helpDiv" align="center">
          <div id="helpsqlcontainer" class="helpContainer" align="center">
            <div id="helpList" class="helpDetails" style="display:none;" align="center">
              <div align="left">
                <a href="#" id="helpclose1" onclick="new Effect.SlideUp('helpList');document.getElementById('comboSelect').style.visibility='visible';return false;">Close</a>
                <ul>                   
                  <li>
                    <strong>Providing a list of external order numbers</strong><br/>
                    <ul>
                      <li>
                        Click on the "Order List" radio button
                      </li>
                      <li>
                        Enter a list of external order numbers.  This numbers should be seperated by spaces or lines.
                      </li>
                    </ul>
                  </li>
                </ul>
                <a href="#" id="helpclose2" onclick="new Effect.SlideUp('helpList');document.getElementById('comboSelect').style.visibility='visible';return false;">Close</a>
              </div>
            </div>
          </div>
          <div id="helpselectcontainer" class="helpContainer" align="center">
            <div id="helpSelect" class="helpDetails" style="display:none;" align="center">
              <div align="left">
                <a href="#" id="helpclose3" onclick="document.getElementById('comboSelect').style.visibility='visible';new Effect.SlideUp('helpSelect');return false;">Close</a>
                <ul> 
				          <li>
                    <strong>Change delivery to next available date</strong><br/>
                    Change the delivery date on the order to the next available delivery date.
                  </li>
				          <li>
                    <strong>Cancel order</strong><br/>
                    Just cancel the order.
                  </li>
				          <li>
                    <strong>Cancel order and create new FTD order</strong><br/>
                    Cancel the order and create a new FTD order without changing any order parameters.
                  </li>
                </ul>
                <a href="#" id="helpclose4" onclick="new Effect.SlideUp('helpSelect');document.getElementById('comboSelect').style.visibility='visible';return false;">Close</a>
              </div>
            </div>
          </div>
        </div>
        <br/>
        
				<form name="sqlform" method="post">
					<xsl:call-template name="securityanddata"/>
					<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
					<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
					<div align="center">
						<table border="0">
							<tbody>
								<tr>
									<td align="right"><a href="#" id="helpclose" onclick="new Effect.SlideDown('helpSelect');document.getElementById('comboSelect').style.visibility='hidden';return false;">Operation:</a></td>
									<td>
										<div id="comboSelect">
										  <select id="actionType" tabindex="1">
											<option value="change_delivery_date">Change delivery to next available date</option>
											<option value="cancel_order">Cancel order</option>
											<option value="resend_order">Cancel order and create new FTD order</option>
										  </select>
										</div>
									</td>
                                                                        <td>
                                                                                <!--Preferred Partners checkboxes-->
                                                                                <xsl:for-each select="/root/preferredPartners/preferredPartner">
                                                                                    <input name="sc{name}" id="sc{name}" type="checkbox" tabindex="5"/>
                                                                                    Include <xsl:value-of select="value"/>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <script type="text/javascript">
                                                                                        prefPartnerArray[prefPartnerCount] = "sc<xsl:value-of select="name" />";
                                                                                        prefPartnerCount = prefPartnerCount + 1;
                                                                                    </script>
                                                                                </xsl:for-each>
                                                                        </td>
								</tr>
								<tr>
									<td valign="top">
										<table border="0" >
											<tr>
												<td valign="top">
													<input id="listRadio" name="orderSelect" type="radio" value="orderList" checked="true" tabindex="3" onclick="javascript:restoreOrderList();" ><a href="#" id="helpclose" onclick="document.getElementById('comboSelect').style.visibility='hidden';new Effect.SlideDown('helpList');return false;">Order List</a></input>
												</td>
											</tr>
										</table>
									</td>
									<td colspan="2">
										<textarea id="orderList" name="orderList" tabindex="4" cols="80" rows="5"></textarea>
									</td>
								</tr>
                <tr>
                  <td align="center" colspan="3">Warning: FTD West fulfilled orders will be reprocessed if included in the Order List.</td>                    
                </tr>

                <tr><td colspan="3">&nbsp;</td></tr>
							</tbody>
						</table>
            <div id="results" style="display:none;" align="center">
              <table border="0" width="98%">
                <tbody>
                  <tr>
                    <td width="100%" align="center">
                        <tr>
                        </tr>
                      <table width="100%">
                        <tr>
                          <td align="center" class="header">Success</td>
                          <td align="center" class="header">Failure</td>
                        </tr>
                        <tr>
                          <td align="center">
                            <textarea name="passedColumn" cols="70" rows="10" readonly="readonly"></textarea>
                          </td>
                          <td align="center">
                            <textarea name="failedColumn" cols="70" rows="10" readonly="readonly"></textarea>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <table border="0" width="98%">
            <tbody>

 <tr>
									<td align="left" width="33%">
										<a href="#" onclick="javascript:setShade();; return false;" tabindex="5">results</a>
									</td>
									<td align="center" width="33%">
										<INPUT id="submitPageButton" name="submitPageButton" tabindex="6" accesskey="S" type="button" class="BlueButton" style="width:80;" onclick="javascript:doSubmitAction();" value="(S)ubmit"/>
									</td>
                  <td align="right" width="33%">
                    <button class="BlueButton" style="width:80;" name="mainMenu" tabindex="7" accesskey="M" onclick="javascript:doMainMenuAction();">(M)ain Menu </button>
                  </td>
                </tr>
							</tbody>
						</table>
					</div>
          <!-- call footer template-->
          <xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>