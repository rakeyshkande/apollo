<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>


<xsl:template match="/root">


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
		<meta http-equiv="Content-Type" content="text/html"></meta>
		<title>Box Maintenance</title>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/box.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/util.js"/>
    <script type="text/javascript" src="js/copyright.js"></script>

    <script type="text/javascript" language="javascript">

//****************************************************************************************************
//*  Max values
//****************************************************************************************************
var maxHeight = <xsl:value-of select="key('pageData','max_box_height')/value"/>;
var maxLength = <xsl:value-of select="key('pageData','max_box_length')/value"/>;
var maxWidth  = <xsl:value-of select="key('pageData','max_box_width')/value"/>;

    </script>


    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;

    ]]>

		</script>


	</head>
	<body onload="javascript: init()">
		<form name="fTruckMaintenance" method="post">
			<input type="hidden" name="securitytoken" id="securitytoken"  value="{key('pageData', 'securitytoken')/value}"/>
			<input type="hidden" name="context"       id="context"				value="{key('pageData', 'context')/value}"/>
			<input type="hidden" name="adminAction" 	id="adminAction" 		value="{key('pageData', 'adminAction')/value}"/>
			<input type="hidden" name="box_id" 				id="box_id" 				value="{BOXES/BOX/box_id}"/>
			<input type="hidden" name="standard_flag"	id="standard_flag"	value="{BOXES/BOX/standard_flag}"/>

			<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td width="30%" height="33" valign="top">
						<div class="floatleft">
							<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
						</div>
					</td>
					<td>&nbsp;</td>
					<td align="right" width="30%" valign="bottom">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center" valign="top" class="Header" id="pageHeader">Box Maintenance</td>
					<td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
				</tr>
			</table>
			<table width="98%" align="center" cellspacing="1" class="mainTable" border="0" >
				<tr>
					<td>
						<table width="100%" align="center" class="innerTable" border="0">
							<tr>
								<td>
									<table width="100%" align="center" border="0">
										<tr>
											<td class="label" align="right" width="30%">Box Name</td>
											<td align="left" width="70%">
												<table>
													<tr>
														<td>
															<xsl:choose>
																<xsl:when test="BOXES/BOX">
																	<xsl:choose>
																		<xsl:when test="BOXES/BOX/standard_flag = 'N'">
																			<input type="text" id="box_name" name="box_name" maxlength="30" size="30" onchange="javascript:somethingChanged=true;">
																				<xsl:attribute name="value">
																					<xsl:value-of select="BOXES/BOX/box_name"/>
																				</xsl:attribute>
																			</input>
																		</xsl:when>
																		<xsl:otherwise>
																			<input type="hidden" id="box_name" name="box_name" value="{BOXES/BOX/box_name}"/>
																			<xsl:value-of select="BOXES/BOX/box_name"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:when>
																<xsl:otherwise>
																	<input type="text" id="box_name" name="box_name" maxlength="30" size="30" onchange="javascript:somethingChanged=true;"></input>
																</xsl:otherwise>
															</xsl:choose>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="label" align="right">Box Description</td>
											<td align="left">
												<table>
													<tr>
														<td>
															<input type="text" id="box_desc" name="box_desc" maxlength="30" size="30" onchange="javascript:somethingChanged=true;">
																<xsl:if test="BOXES/BOX/box_desc">
																	<xsl:attribute name="value">
																		<xsl:value-of select="BOXES/BOX/box_desc"/>
																	</xsl:attribute>
																</xsl:if>
															</input>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="label" align="right">Box Height</td>
											<td align="left">
												<table>
													<tr>
														<td>
															<select name="box_height" id="box_height" onchange="javascript:somethingChanged=true;">
																<xsl:choose>
																	<xsl:when test="BOXES/BOX">
																		<script>
																			var boxHeight = <xsl:value-of select="BOXES/BOX/height_inch_qty"/>;
																			populateDropDown(boxHeight, maxHeight);
																		</script>
																	</xsl:when>
																	<xsl:otherwise>
																		<script>
																			populateDropDown(0, maxHeight);
																		</script>
																	</xsl:otherwise>
																</xsl:choose>
															</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="label" align="right">Box Width</td>
											<td align="left">
												<table>
													<tr>
														<td>
															<select name="box_width" id="box_width" onchange="javascript:somethingChanged=true;">
																<xsl:choose>
																	<xsl:when test="BOXES/BOX">
																		<script>
																			var boxWidth = <xsl:value-of select="BOXES/BOX/width_inch_qty"/>;
																			populateDropDown(boxWidth, maxWidth);
																		</script>
																	</xsl:when>
																	<xsl:otherwise>
																		<script>
																			populateDropDown(0, maxWidth);
																		</script>
																	</xsl:otherwise>
																</xsl:choose>
															</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="label" align="right">Box Length</td>
											<td align="left">
												<table>
													<tr>
														<td>
															<select name="box_length" id="box_length" onchange="javascript:somethingChanged=true;">
																<xsl:choose>
																	<xsl:when test="BOXES/BOX">
																		<script>
																			var boxLength = <xsl:value-of select="BOXES/BOX/length_inch_qty"/>;
																			populateDropDown(boxLength, maxLength);
																		</script>
																	</xsl:when>
																	<xsl:otherwise>
																		<script>
																			populateDropDown(0, maxLength);
																		</script>
																	</xsl:otherwise>
																</xsl:choose>
															</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
				<tr>
					<td>
						<table>
							<tr>
								<td align="left">
									<button class="BlueButton" style="width:80;" name="bSave" accesskey="S" onclick="javascript:doSave();">
										(S)ave
									</button>
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<table>
							<tr>
								<td align="right" valign="top">
									<button class="BlueButton" style="width:80;" accesskey="B" name="bBack" onclick="javascript:doExit('load');">(B)ack </button>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">
									<button class="BlueButton" style="width:80;" accesskey="M" name="bMainMenu" onclick="javascript:doExit('main_menu');">(M)ain Menu </button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="disclaimer"></td><script>showCopyright();</script>
				</tr>
			</table>
		</form>
	</body>
</html>

</xsl:template>
</xsl:stylesheet>
