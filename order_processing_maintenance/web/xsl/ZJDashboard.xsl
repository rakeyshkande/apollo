<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->
<xsl:variable name="FILTER" select="key('pageData', 'filter')/value"/>


<xsl:template match="/root">

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Zone Jump Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/ZJTrip.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/util.js"/>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//* Global variables
//****************************************************************************************************
var somethingChanged = false;
var serverErrorMessage = '';

    ]]>

    </script>

  </head>
  <body onload="javascript: init()">
    <form name="fDashboard" method="post">
      <input type="hidden" name="securitytoken" id="securitytoken"   value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"       id="context"         value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"   id="adminAction"     value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="filter"        id="filter"          value="{key('pageData', 'filter')/value}"/>

      <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="30%" height="33" valign="top">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td>&nbsp;</td>
          <td align="right" width="30%" valign="bottom">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" valign="top" class="Header" id="pageHeader">Zone Jump Dashboard</td>
          <td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
        </tr>
      </table>


      <!-- content to be hidden once the search begins -->
      <div id="content" style="display:block">

        <table width="98%" align="center" cellspacing="0" border="0">

          <!-- HEADER -->
          <tr>
            <td align="left" valign="top">
              <table>
                <tr>
                  <td>
                    <table width="1100px" align="center" border="0" style="background-color:#eeeeaa;" >
                      <tr>
                        <td class="label" colspan="10" align="center" style="background-color:#eeeeaa;font-size: 14pt;">
                          Sort Code
                        </td>
                      </tr>
                    </table>
                    <table width="1100px" align="center" border="0" style="background-color:#EBDDE2;" >
                      <tr>
                        <td class="label" align="left"   style="background-color:#EBDDE2;" width="200px">Vendor Location</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="100px">Hub Location</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="50px" >Carrier</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="90px" >Ven Deprt Date</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="90px" >Delivery Date</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="90px" >Status</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="120px">3rd Party Pallets</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="120px">FTD Pallets Allocated</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="120px">FTD Pallets in use</td>
                        <td class="label" align="center" style="background-color:#EBDDE2;" width="120px">Total Orders On Truck</td>
                      </tr>
                    </table>
                    <table width="1100px" align="center" border="0" style="background-color:#ccccff;" >
                      <tr>
                        <td colspan="3" class="label" align="left"   style="background-color:#ccccff;" width="380px">Vendor Name (Vendor Status)</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="100px">Pallets Full</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="100px">Orders on Full Pallets</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="100px">Pallet in Use</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="125px">Orders on Partial Pallets</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="125px">Partial Pallet Used Pct</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="125px">Vendor Pallets in Use</td>
                        <td             class="label" align="center" style="background-color:#ccccff;" width="125px">Total Orders</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <!-- DETAILS -->
          <tr>
            <td align="left" valign="top">
              <table>
                <tr>
                  <td>
                    <div style="height:28em; overflow:auto;">
                      <xsl:for-each select="TRIPS/TRIP">
                        <table width="1100px" align="center" border="0" style="background-color:#EBDDE2;" >

                          <tr>
                            <td class="label" colspan="10" align="center" style="background-color:#eeeeaa;font-size: 13pt;">
                              <xsl:value-of select="sort_code"/>
                            </td>
                          </tr>
                        </table>

                        <table width="1100px" align="center" border="0" style="background-color:#EBDDE2;" >
                          <tr>
                            <td align="left"   style="background-color:#EBDDE2;" width="200px"><xsl:value-of select="trip_origination_desc"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="100px"><xsl:value-of select="INJECTION_HUB/city_name"/>,&nbsp;<xsl:value-of select="INJECTION_HUB/state_master_id"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="50px" ><xsl:value-of select="INJECTION_HUB/carrier_id"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="90px" ><xsl:value-of select="substring(departure_date,6,2)"/>-<xsl:value-of select="substring(departure_date,9,2)"/>-<xsl:value-of select="substring(departure_date,1,4)"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="90px" ><xsl:value-of select="substring(delivery_date,6,2)"/>-<xsl:value-of select="substring(delivery_date,9,2)"/>-<xsl:value-of select="substring(delivery_date,1,4)"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="90px" >
                              <xsl:choose>
                                <xsl:when test="virtual_trip_status_code != ''">
                                  <xsl:value-of select="virtual_trip_status_code"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:variable name="ACTUAL_TRIP_STATUS_CODE" select="actual_trip_status_code"/>
                                  <xsl:for-each select="/root/TRIP_STATUSES/TRIP_STATUS">
                                    <xsl:if test="$ACTUAL_TRIP_STATUS_CODE = trip_status_code">
                                      <xsl:value-of select="trip_status_desc"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td align="center" style="background-color:#EBDDE2;" width="120px"><xsl:value-of select="third_party_plt_qty"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="120px"><xsl:value-of select="ftd_plt_qty"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="120px"><xsl:value-of select="ftd_plt_used_qty"/></td>
                            <td align="center" style="background-color:#EBDDE2;" width="120px"><xsl:value-of select="total_order_on_truck"/></td>
                          </tr>
                        </table>

                        <xsl:for-each select="TRIP_VENDOR">
													<xsl:variable name="NEW_TRIP_VENDOR_STATUS_CODE" select="new_trip_vendor_status_code"/>
                          <table width="1100px" align="center" border="0" style="background-color:#ccccff;" >
                            <tr>
                              <td colspan="3" align="left"   style="background-color:#ccccff;" width="300px"><xsl:value-of select="vendor_name"/>
                              		(<xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS"><xsl:if test="$NEW_TRIP_VENDOR_STATUS_CODE = trip_vendor_status_code"><xsl:value-of select="trip_vendor_status_desc"/></xsl:if></xsl:for-each>)
                              </td>
                              <td             align="center" style="background-color:#ccccff;" width="100px"><xsl:value-of select="full_plt_qty"/></td>
                              <td             align="center" style="background-color:#ccccff;" width="100px"><xsl:value-of select="full_plt_order_qty"/></td>
                              <td             align="center" style="background-color:#ccccff;" width="100px"><xsl:value-of select="prtl_plt_in_use_flag"/></td>
                              <td             align="center" style="background-color:#ccccff;" width="125px"><xsl:value-of select="prtl_plt_order_qty"/></td>
                              <td             align="center" style="background-color:#ccccff;" width="125px">
                                <script>
                                  var prtlPltInUseFlag = '<xsl:value-of select="prtl_plt_in_use_flag"/>';
                                  var prtlPltCubicInUsedQty = <xsl:value-of select="prtl_plt_cubic_in_used_qty"/>;
                                  var pltCubicInchAllowedQty = <xsl:value-of select="plt_cubic_inch_allowed_qty"/>;

                                  if (prtlPltInUseFlag == 'Y' || prtlPltInUseFlag == 'y')
                                  {
                                    prtlPltCubicInUsedQty = prtlPltCubicInUsedQty - 0;
                                    pltCubicInchAllowedQty = pltCubicInchAllowedQty - 0;

                                    var prtlPltOrderQtyPct = (prtlPltCubicInUsedQty / pltCubicInchAllowedQty) * 100;

                                    document.write(prtlPltOrderQtyPct.toFixed(2) + '%');
                                  }
                                  else
                                    document.write(0 + '%');
                                </script>
                              </td>
                              <td             align="center" style="background-color:#ccccff;" width="125px">
                                <script>
                                  var prtlPltInUseFlag = '<xsl:value-of select="prtl_plt_in_use_flag"/>';
                                  var fullPltQty = <xsl:value-of select="full_plt_qty"/>;
                                  var totalPalletsUsed = fullPltQty - 0;

                                  if (prtlPltInUseFlag == 'Y' || prtlPltInUseFlag == 'y')
                                  {
                                    totalPalletsUsed += 1;
                                  }
                                  document.write(totalPalletsUsed);
                                </script>
                              </td>
                              <td             align="center" style="background-color:#ccccff;" width="125px">
                                <script>
                                  var ordersOnFullPallets = <xsl:value-of select="full_plt_order_qty"/>;
                                  var ordersOnPartialPallet = <xsl:value-of select="prtl_plt_order_qty"/>;

                                  ordersOnFullPallets = ordersOnFullPallets - 0;
                                  ordersOnPartialPallet = ordersOnPartialPallet - 0;

                                  var totalOrders = ordersOnFullPallets + ordersOnPartialPallet;
                                  document.write(totalOrders);
                                </script>
                              </td>
                            </tr>
                          </table>
                        </xsl:for-each>

                        <table width="1100px" align="center" border="0" style="background-color:white;" >
                          <tr><td colspan="10">&nbsp;</td></tr>
                          <tr><td colspan="10">&nbsp;</td></tr>
                        </table>


                      </xsl:for-each>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
          <tr><td colspan="2" align="left">&nbsp;</td></tr>
          <tr><td colspan="2" align="left">&nbsp;</td></tr>
          <tr>
            <td>
              <table>
                <tr>
                  <td align="left">
                    <select name="sFilter" id="sFilter">
                      <option value="All">All</option>
                      <xsl:for-each select="TRIP_STATUSES/TRIP_STATUS">
                        <option value="{trip_status_code}">
                          <xsl:if test="trip_status_code = $FILTER"><xsl:attribute name="selected"/></xsl:if>
                          <xsl:value-of select="trip_status_desc"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bFilter" accesskey="F" onclick="javascript:doFilter('dashboard');">
                      (F)ilter
                    </button>
                  </td>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bRefresh" accesskey="R" onclick="javascript:doRefresh('dashboard');">
                      (R)efresh
                    </button>
                  </td>
                </tr>
              </table>
            </td>
            <td align="right">
              <table>
                <tr>
                  <td align="right" valign="top">
                    <button class="BlueButton" style="width:80;" accesskey="T" name="bTripSummary" onclick="javascript:doLoadDefault('load');">(T)rip Summary</button>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top">
                    <button class="BlueButton" style="width:80;" accesskey="M" name="bMainMenu" onclick="javascript:doExit('main_menu');">(M)ain Menu</button>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>


      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
         <td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
      </table>
    </form>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
