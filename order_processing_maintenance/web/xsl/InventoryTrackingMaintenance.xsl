<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="commonUtil.xsl"/>

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->


<xsl:template match="/root">



<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Inventory Tracking Maintenance</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>
    <link rel="stylesheet" type="text/css" href="css/calendar.css"></link>

    <script type="text/javascript" src="js/InventoryTrackingMaintenance.js"></script>
    <script type="text/javascript" src="js/Tokenizer.js"></script>
    <script type="text/javascript" src="js/FormChek.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>  
    <script type="text/javascript" src="js/calendar.js"/>
    <script type="text/javascript" src="js/prototype.js"></script>
    <script type="text/javascript" src="js/rico.js"/>
    <script type="text/javascript" src="js/ftdajax.js"/>
    <script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script>
//****************************************************************************************************
//*  values
//****************************************************************************************************


var shippedShipping = <xsl:choose><xsl:when test="search_results/search_result/shipped_shipping != ''"><xsl:value-of select="search_results/search_result/shipped_shipping"/></xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose>; 
var somethingChanged = false;

var originalStartDateValue =  '';
    originalStartDateValue += '<xsl:value-of select="substring(search_results/search_result/start_date, 1, 2)"/>';
    originalStartDateValue += '<xsl:value-of select="substring(search_results/search_result/start_date, 4, 2)"/>';
    originalStartDateValue += '<xsl:value-of select="substring(search_results/search_result/start_date, 7, 4)"/>';

var originalEndDateValue =  '';
    originalEndDateValue += '<xsl:value-of select="substring(search_results/search_result/end_date, 1, 2)"/>';
    originalEndDateValue += '<xsl:value-of select="substring(search_results/search_result/end_date, 4, 2)"/>';
    originalEndDateValue += '<xsl:value-of select="substring(search_results/search_result/end_date, 7, 4)"/>';


  
function doTakenPrePopup() {
  addEditFlag = document.getElementById('addEditFlag').value;
  var productId = '';
  if (addEditFlag == 'edit') {
    productId = document.getElementById('hOriginalProductId').value;
  } else {
    productId = document.getElementById('tproductId').value;
    if (trim(productId) == '') {
      alert('Product Id is required');
      document.getElementById('tproductId').focus();
      return false;
    }
  }
  doTakenPopup(productId);
}

function trim(str) {
  val = str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  return val;
}
    </script>


    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;

    ]]>

    </script>


  </head>
  <body onload="javascript: init()">
    <form name="fInventoryTrackingMaintenance" method="post">

      <input type="hidden" name="securitytoken"       id="securitytoken"       value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"             id="context"             value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"         id="adminAction"         value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="hOriginalVendorId"   id="hOriginalVendorId"   value="{search_results/search_result/vendor_id}"/>
      <input type="hidden" name="hOriginalProductId"  id="hOriginalProductId"  value="{search_results/search_result/product_id}"/>
      <input type="hidden" name="hInvTrkId"           id="hInvTrkId"           value="{search_results/search_result/inv_trk_id}"/>
      <input type="hidden" name="addEditFlag"         id="addEditFlag"         value="{key('pageData', 'original_action_type')/value}"/>
      <input type="hidden" name="hSearchRequestVendorsChosen"       id="hSearchRequestVendorsChosen"       value="{key('pageData', 'hSearchRequestVendorsChosen')/value}"/>
      <input type="hidden" name="hSearchRequestStartDate"           id="hSearchRequestStartDate"           value="{key('pageData', 'hSearchRequestStartDate')/value}"/>
      <input type="hidden" name="hSearchRequestEndDate"             id="hSearchRequestEndDate"             value="{key('pageData', 'hSearchRequestEndDate')/value}"/>
      <input type="hidden" name="hSearchRequestProductStatus"       id="hSearchRequestProductStatus"       value="{key('pageData', 'hSearchRequestProductStatus')/value}"/>
      <input type="hidden" name="hSearchRequestVendorProductStatus" id="hSearchRequestVendorProductStatus" value="{key('pageData', 'hSearchRequestVendorProductStatus')/value}"/>
      <input type="hidden" name="hSearchProductIds"                 id="hSearchProductIds"                 value="{key('pageData', 'hSearchProductIds')/value}"/>
      <input type="hidden" name="hSearchRequestOnHand"              id="hSearchRequestOnHand"              value="{key('pageData', 'hSearchRequestOnHand')/value}"/>

      <!-- Main Div -->
      <div id="content" style="display:block">


        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
          <tr>
            <td width="30%" height="33" valign="top">
              <div class="floatleft">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
              </div>
            </td>
            <td width="40%" align="center" valign="top" class="Header" id="pageHeader">
              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <script language="javascript">document.write("<H2>Inventory Tracking Edit</H2>");</script>
                </xsl:when>
                <xsl:otherwise>
                  <script language="javascript">document.write("<H2>Inventory Tracking Add</H2>");</script>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td width="30%">&nbsp;</td>
          </tr>
          <tr><td colspan="3">&nbsp;</td></tr>
          <tr><td colspan="3">&nbsp;</td></tr>
          <tr><td colspan="3">&nbsp;</td></tr>
        </table>

        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">

          <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->
          <xsl:if test = "key('pageData', 'errorMessage')/value != ''">
            <tr>
              <td colspan="2" align="left" style="color:blue;">
                <H5>YOUR REQUEST YIELDED FOLLOWING RESULTS:&nbsp;</H5>
              </td>
              <td colspan="4" align="left">
                <textarea name="errorMessageTextArea" id="errorMessageTextArea" rows="3" cols="110" tabindex="5">
                  <xsl:value-of select = "key('pageData', 'errorMessage')/value"/>
                </textarea>
              </td>
            </tr>
          </xsl:if>

          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Product Id&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <label name="lProductId" id="lProductId">
                    <xsl:value-of select="search_results/search_result/product_id"/>
                  </label>
                </xsl:when>
                <xsl:otherwise>
                  <input type="text" name="tProductId" id="tProductId" onblur="getVendors();" onchange="javascript:somethingChanged=true;">
                    <xsl:attribute name="value"><xsl:value-of select="search_results/search_result/product_id"/></xsl:attribute>
                  </input>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td class="label" align="right">
              Product Name&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <label name="lProductName" id="lProductName">
                <xsl:value-of select="search_results/search_result/product_name"/>
              </label>
            </td>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr>
            <td class="label" align="right">
              Product Status&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <label name="lProductStatus" id="lProductStatus">
                <xsl:choose>
                  <xsl:when test="search_results/search_result/product_status = 'A' and search_results/search_result/exception_start_date = ''">
                    Available
                  </xsl:when>
                  <xsl:when test="search_results/search_result/product_status = 'A' and search_results/search_result/exception_start_date != ''">
                    Restricted
                  </xsl:when>
                  <xsl:when test="search_results/search_result/product_status = 'U' and search_results/search_result/product_updated_by != 'SYSTEM_INV_TRK'">
                    Unavailable
                  </xsl:when>
                  <xsl:when test="search_results/search_result/product_status = 'U' and search_results/search_result/product_updated_by = 'SYSTEM_INV_TRK'">
                    Shutdown
                  </xsl:when>
                  <xsl:otherwise>
                    &nbsp;
                  </xsl:otherwise>
                </xsl:choose>
              </label>
            </td>
            <td class="label" align="right">
              Availability Dates&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <label name="lAvailabilityDates" id="lAvailabilityDates">
                <xsl:value-of select="search_results/search_result/exception_start_date"/>
              </label>
            </td>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr>
            <td>&nbsp;</td>
            <td><a id="" href="#" onclick="javascript:doTakenPrePopup();" class="textlink">Taken</a></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Vendor&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">

              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <label name="lVendorId" id="lVendorId">
                    <xsl:value-of select="search_results/search_result/vendor_name"/>
                  </label>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:variable name="VENDOR_ID" select="search_results/search_result/vendor_id"/>
                  <select name="sVendorId" id="sVendorId" onchange="javascript:somethingChanged=true;">
                    <option value=""></option>                                                                      
                    <xsl:for-each select="vendors/vendor">
                      <option value="{vendor_id}">
                        <xsl:if test="vendor_id = $VENDOR_ID">
                          <xsl:attribute name="selected">true</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="vendor_name"/>
                      </option>                                                                      
                    </xsl:for-each>
                  </select>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td class="label" align="right">
              Period Location Status&nbsp;:&nbsp;&nbsp;
            </td>
            <xsl:choose>
              <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                <td align="left">
                  <label name="lVendorProductStatus" id="lVendorProductStatus">
                   
                    <xsl:choose>
                      <xsl:when test="search_results/search_result/inv_status = 'A'">
                        Available
                      </xsl:when>
                      <xsl:when test="search_results/search_result/inv_status = 'U'">
                        Unavailable
                      </xsl:when>
                      <xsl:when test="search_results/search_result/inv_status = 'N'">
                        Unavailable
                      </xsl:when>
                      <xsl:when test="search_results/search_result/inv_status = 'X' ">
                        Shutdown
                      </xsl:when>
                      <xsl:otherwise>
                        &nbsp;
                      </xsl:otherwise>
                    </xsl:choose>
                  </label>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td colspan="2">&nbsp;</td>
              </xsl:otherwise>
            </xsl:choose>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr>
            <td class="label" align="right">
              Start Date&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <table>
                <tr>
                  <td>
                    <input type="text" name="tStartDate" id="tStartDate" maxlength="10" size="10">
                      <xsl:attribute name="disabled"/>
                      <xsl:attribute name="value"><xsl:value-of select="search_results/search_result/start_date"/></xsl:attribute>
                    </input>
                    &nbsp;&nbsp;&nbsp;
                    <img id="start_date_img" src="images\calendar.gif"></img>
                    &nbsp;&nbsp;&nbsp;
                    <img id="removed_img" src="images\removed.gif" onclick="clearField('tStartDate');"></img>
                  </td>
                </tr>
              </table>
            </td>
            <td class="label" align="right">
              End Date&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <table>
                <tr>
                  <td>
                    <input type="text" name="tEndDate" id="tEndDate" maxlength="10" size="10">
                      <xsl:attribute name="disabled"/>
                      <xsl:attribute name="value">
                          <xsl:value-of select="search_results/search_result/end_date"/>
                      </xsl:attribute>
                    </input>
                    &nbsp;&nbsp;&nbsp;
                    <img id="end_date_img" src="images\calendar.gif"></img>
                    &nbsp;&nbsp;&nbsp;
                    <img id="removed_img" src="images\removed.gif" onclick="clearField('tEndDate');"></img>
                  </td>
                </tr>
              </table>
            </td>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Forecast&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <label name="lForecast" id="lForecast">
                    <xsl:value-of select="search_results/search_result/forecast_qty"/>
                  </label>
                </xsl:when>
                <xsl:otherwise>
                  <input type="text" name="tForecast" id="tForecast" onblur="forecastChanged();" onchange="javascript:somethingChanged=true;">
                    <xsl:attribute name="value"><xsl:value-of select="search_results/search_result/forecast_qty"/></xsl:attribute>
                  </input>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td class="label" align="right">
              Warning Threshold&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tWarningThreshold" id="tWarningThreshold" onchange="javascript:somethingChanged=true;">
                <xsl:attribute name="value"><xsl:value-of select="search_results/search_result/warning_threshold_qty"/></xsl:attribute>
              </input>
            </td>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr>
            <td class="label" align="right">
              Revised&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <xsl:choose>
                <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                  <input type="text" name="tRevised" id="tRevised" onblur="revisedChanged();" onchange="javascript:somethingChanged=true;">
                    <xsl:attribute name="value"><xsl:value-of select="search_results/search_result/revised_forecast_qty"/></xsl:attribute>
                  </input>
                </xsl:when>
                <xsl:otherwise>
                  <label name="lRevised" id="lRevised">
                    <xsl:value-of select="search_results/search_result/revised_forecast_qty"/>
                  </label>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td class="label" align="right">
              Shutdown Threshold&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <input type="text" name="tShutdownThreshold" id="tShutdownThreshold" onchange="javascript:somethingChanged=true;">
                  <xsl:attribute name="value"><xsl:value-of select="search_results/search_result/shutdown_threshold_qty"/></xsl:attribute>
              </input>
            </td>            
          </tr>

          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Shipped / Shipping&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <label name="lShippedShipping" id="lShippedShipping">
                <xsl:choose>
                  <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                    <xsl:value-of select="search_results/search_result/shipped_shipping"/>
                  </xsl:when>
                  <xsl:otherwise>
                    0
                  </xsl:otherwise>
                </xsl:choose>
              </label>
            </td>
            <td colspan="4">&nbsp;</td>
          </tr>

          <tr>
            <td class="label" align="right">
              Balance O/H&nbsp;:&nbsp;&nbsp;
            </td>
            <td align="left">
              <label name="lOnHand" id="lOnHand">
                <xsl:value-of select="search_results/search_result/on_hand"/>
              </label>
            </td>
            <xsl:choose>
              <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                <td class="label" align="left">
                  Threshold Emails&nbsp;:&nbsp;&nbsp;
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>&nbsp;</td>
              </xsl:otherwise>
            </xsl:choose>
            <td colspan="3">&nbsp;</td>
          </tr>

          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td class="label" align="right">
              Comments&nbsp;:&nbsp;&nbsp;
            </td>
            <td colspan="1" align="left">
              <textarea name="taComments" id="taComments" rows="4" >
                <xsl:value-of select="search_results/search_result/comments"/>
              </textarea>
            </td>


            <xsl:choose>
              <xsl:when test="key('pageData', 'original_action_type')/value = 'edit'">
                <td colspan="2">
                  <table border="0">
                    <tr>
                      <td class="label" align="right">
                        Add&nbsp;:&nbsp;&nbsp;
                      </td>
                      <td align="left">
                        <input type="text" name="tNewEmailAddress" id="tNewEmailAddress" onchange="javascript:somethingChanged=true;"/>
                      </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td  class="label" colspan="2" align="left">Check To Remove:</td></tr>

                    <xsl:for-each select="emails/email">
                      <tr>
                        <td class="label" align="right">
                          <input type="checkbox" name="cb_{position()}"     id="cb_{position()}" onclick="javascript:somethingChanged=true;"/>
                          <input type="hidden"   name="cbval_{position()}"  id="cbval_{position()}" value="{inv_trk_email_id}"/>
                        </td>
                        <td align="left">
                          <label name="lEmailAddress_{position()}_00" id="lEmailAddress_{position()}_00">
                            <xsl:value-of select="email_address"/>
                          </label>
                        </td>
                      </tr>
                    </xsl:for-each>

                  </table>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td colspan="2">&nbsp;</td>
              </xsl:otherwise>
            </xsl:choose>

            <td colspan="2">&nbsp;</td>
          </tr>

          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="4" align="center" valign="top">
              <button name="bSave"      id="bSave"      class="BlueButton" style="width:120;" accesskey="S" onclick="performSave();">(S)ave</button>
              <button name="bCopy"      id="bCopy"      class="BlueButton" style="width:120;" accesskey="C" onclick="performCopy();">
                <xsl:if test="key('pageData', 'original_action_type')/value != 'edit'">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                </xsl:if>

                (C)opy
              </button>
              <button name="bDashboard" id="bDashboard" class="BlueButton" style="width:120;" accesskey="D" onclick="performDashboard();">(D)ashboard</button>
              <button name="bMainMenu"  id="bMainMenu"  class="BlueButton" style="width:120;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
            </td>
          </tr>

          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>


        </table>
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>

      



      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
      </table>

      <script type="text/javascript">
        Calendar.setup
        (
          {
            inputField  : "tStartDate",    // ID of the input field
            ifFormat    : "mm/dd/y",       // the date format
            button      : "start_date_img" // ID of the button
          }
        );
        Calendar.setup
        (
          {
            inputField  : "tEndDate",      // ID of the input field
            ifFormat    : "mm/dd/y",       // the date format
            button      : "end_date_img"   // ID of the button
          }
        );
      </script>
    </form>
    <iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
