<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="securityData" match="/root/security/data" use="name"/>

<xsl:template match="/root">

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Inventory Tracking Email Alerts</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/InventoryTrackingEmailAlerts.js"></script>
    <script type="text/javascript" src="js/FormChek.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>
    <script type="text/javascript" src="js/clock.js"></script>
    <script type="text/javascript" src="js/copyright.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//* Global variables
//****************************************************************************************************
var changed = false;

    ]]>

    </script>

  </head>
  <body onload="javascript: init()">
    <form name="emailAlerts" method="post">
      <input type="hidden" name="securitytoken"         id="securitytoken"         value="{key('securityData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"               id="context"               value="{key('securityData', 'context')/value}"/>
      <input type="hidden" name="adminAction"           id="adminAction"           value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="hUpdateAllowed"        id="hUpdateAllowed"        value="{key('pageData', 'update_allowed')/value}"/>
      <input type="hidden" name="hEmailAddress"         id="hEmailAddress"         value="{key('pageData', 'email_address')/value}"/>
      <input type="hidden" name="hSearchDone"           id="hSearchDone"           value="{key('pageData', 'search_done')/value}"/>
      <input type="hidden" name="hActionResponse"       id="hActionResponse"       value="{key('pageData', 'action_response')/value}"/>
      <input type="hidden" name="hActionErrorResponse"  id="hActionErrorResponse"  value="{key('pageData', 'action_error_response')/value}"/>
      <input type="hidden" name="hVendorsChosen"        id="hVendorsChosen"/>

      <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="30%" height="33" valign="top">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td>&nbsp;</td>
          <td align="right" width="30%" valign="bottom">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" valign="top" class="Header" id="pageHeader">Inventory Tracking Email Alerts</td>
          <td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
          <script type="text/javascript">startClock();</script>
        </tr>
      </table>

      <div id="content" style="display:block">
        <table width="98%" align="center" cellspacing="1" class="mainTable" border="0">
        <tr>
        <td >
        <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
        <tr>
        <td>
        <table align="center" border="0">
            <tr>
                    <td align="center" valign="top" colspan="4">
                            <label id="lActionResponse" name="lActionResponse" class="OperationSuccess"><xsl:value-of select="key('pageData', 'action_response')/value"/></label>
                            <br/><label id="lActionErrorResponse" name="lActionErrorResponse" class="ErrorMessage"><xsl:value-of select="key('pageData', 'action_error_response')/value"/></label>
                    </td>
            </tr>
            <tr>
                    <td align="center" valign="top" colspan="4">
                            &nbsp;
                    </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table align="left">
                      <tr>
                              <td align="left" valign="top">
                                      &nbsp;Email Address:
                              </td>
                              <td align="left" valign="top">
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="tEmailAddress" id="tEmailAddress" maxlength="40" onkeypress="setChanged();if(event.keyCode == 13) search();" value="{key('pageData', 'email_address')/value}" tabindex="1"/>
                              </td>
                              <td align="left" valign="top">
                                      <button name="bSearch" id="bSearch" class="BlueButton" style="width:120;" accesskey="S" onclick="search();" tabindex="2">(S)earch</button>
                              </td>
                              <td align="left" valign="top">
                              </td>
                      </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table align="left">
                      <tr>
                              <td align="left" valign="top">
                                      &nbsp;Selected Email Address:
                              </td>
                              <td align="left" valign="top">
                                      <xsl:value-of select="key('pageData', 'email_address')/value"/>
                              </td>
                              <td align="left" valign="top" colspan="2">
                              </td>
                      </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table align="center">
                        <tr>
                                <td>
                                        <table>
                                                <tr>
                                                        <td>Available Vendors:</td>
                                                        <td>&nbsp;</td>
                                                        <td>Selected Vendors:</td>
                                                </tr>
                                                <tr>
                                                        <td>
                                                                <select name="sVendorsAvailable" id="sVendorsAvailable" ondblclick="moveSelected();setChanged();" size="6" style="width:300;" tabindex="3">
                                                                        <xsl:attribute name="multiple"/>
                                                                        <xsl:for-each select="vendor_available/VendorVO">
                                                                                <option value="{VendorID}">
                                                                                        <xsl:value-of select="VendorName"/>&nbsp;(<xsl:value-of select="VendorID"/>)
                                                                                </option>
                                                                        </xsl:for-each>
                                                                </select>
                                                        </td>
                                                        <td>
                                                                <button name="bMoveSelectedAll"   id="bMoveSelectedAll"   class="BlueButton" style="width:20;" onclick="moveSelectedAll();setChanged();"   tabindex="4">&gt;&gt;</button><BR/>
                                                                <button name="bMoveSelected"      id="bMoveSelected"      class="BlueButton" style="width:20;" onclick="moveSelected();setChanged();"      tabindex="5">&gt;</button><BR/>
                                                                <button name="bMoveUnSelected"    id="bMoveUnSelected"    class="BlueButton" style="width:20;" onclick="moveUnSelected();setChanged();"    tabindex="6" >&lt;</button><BR/>
                                                                <button name="bMoveUnSelectedAll" id="bMoveUnSelectedAll" class="BlueButton" style="width:20;" onclick="moveUnSelectedAll();setChanged();" tabindex="7">&lt;&lt;</button>
                                                        </td>
                                                        <td>
                                                                <select name="sVendorsChosen" id="sVendorsChosen" ondblclick="moveUnSelected();setChanged();" size="6" style="width:300;" tabindex="8">
                                                                        <xsl:attribute name="multiple"/>
                                                                        <xsl:for-each select="vendor_chosen/VendorVO">
                                                                                <option value="{VendorID}" name="sVendorsChosen" id="sVendorsChosen">
                                                                                        <xsl:value-of select="VendorName"/>&nbsp;(<xsl:value-of select="VendorID"/>)
                                                                                </option>
                                                                        </xsl:for-each>
                                                                </select>
                                                        </td>
                                                </tr>
                                        </table>
                                </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table align="center">
                      <tr>
                              <td align="right" valign="top">
                              </td>
                              <td align="right" valign="top">
                                      <select name="sApplyToCurrentFlag" id="sApplyToCurrentFlag" onchange="setChanged();" tabindex="9">
                                              <option value=""></option>
                                              <option value="Y"><xsl:if test="key('pageData', 'apply_to_current_flag')/value = 'Y'"><xsl:attribute name="selected"/></xsl:if>Update Records &amp; Change Defaults</option>
                                              <option value="N"><xsl:if test="key('pageData', 'apply_to_current_flag')/value = 'N'"><xsl:attribute name="selected"/></xsl:if>Change Defaults</option>
                                      </select>
                              </td>
                              <td align="center" valign="top">
                                      <button disabled="true" name="bSave" id="bSave" class="BlueButton" style="width:120;" accesskey="A" onclick="save();" tabindex="10">S(a)ve</button>
                              </td>
                              <td align="center" valign="top">
                                      <button disabled="true" name="bDeleteAll" id="bDeleteAll" class="BlueButton" style="width:120;" accesskey="D" onclick="deleteAll();" tabindex="11">(D)elete All</button>
                              </td>
                      </tr>
                    </table>
                </td>
            </tr>
        </table>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <table width="98%" align="center" border="0">
            <tr>
                    <td align="right" valign="top">
                            <button name="bMainMenu" id="bMainMenu" class="BlueButton" style="width:120;" accesskey="M" onclick="mainMenu();" tabindex="12">(M)ain Menu</button>
                    </td>
            </tr>
        </table>
        </div>
        

        <!-- Used to dislay Processing... message to user -->
        <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
        
        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
        </table>
      </form>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
