<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
  <head>
    <META http-equiv="Content-Type" content="text/html"/>
    <title>Price/Pay Vendor Update Approval</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/clock.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>

    <script language="javascript"><![CDATA[

    function init(){
    }

    function getSecurityParams() {
        return  "?securitytoken=" + document.forms[0].securitytoken.value +
            "&context=" + document.forms[0].context.value;
    }

    function approveFile() {
        submitForm("PricePayVendorUpdate.do", "load");
    }

    function submitForm(servlet, act){
        document.forms[0].action = servlet + getSecurityParams();
        document.forms[0].formAction.value = act;
        document.forms[0].submit();
    }

    function goMain() {
        submitForm("MainMenuAction.do", "merchandising");
    }

   ]]>
   </script>

  </head>
  <body onload="javascript:init();">
     
    <form name="uploadForm" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="formAction" value="load"/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="subject" value="user"/>
    <input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
    <input type="hidden" name="errorMessage" value="{key('pageData','error_message')/value}"/>
    <input type="hidden" name="batchId" value="{key('pageData','batch_id')/value}"/>

    <table width="775" border="0" align="center">
        <tr>
            <td width="30%">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
            </td>
            <td width="40%" align="center" valign="center">
                <font class="Header">Price/Pay Vendor Update - Approval</font>
            </td>
            <td  id="time" align="right" valign="center" class="Label" width="30%">
                <script type="text/javascript">startClock();</script>
            </td>
        </tr>
    </table>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td align="center">
        <table border="2" cellpadding="3" cellspacing="0" bordercolor="#006699">
          <tr>
            <th class="ColHeader">Product Id</th>
            <th class="ColHeader">Vendor Id</th>
            <th class="ColHeader">Pay Vendor Amt</th>
            <th class="ColHeader">Standard Price</th>
            <th class="ColHeader">Deluxe Price</th>
            <th class="ColHeader">Premium Price</th>
            <th class="ColHeader">Errors</th>
            <th class="ColHeader">Variance</th>
          </tr>
          <xsl:variable name="varianceThreshold" select="root/pageData/data[name='variance_threshold']/value"/>
          <xsl:for-each select="root/price_pay_vendor_list/price_pay_vendor">
            <tr>
                <td title="{product_id} {product_name}">
                    <xsl:value-of select="product_id"/>
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="vendor_id = ''">&nbsp;</xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="vendor_id"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="vendor_cost = ''">&nbsp;</xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="vendor_cost"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="standard_price = ''">&nbsp;</xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="standard_price"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="deluxe_price = ''">&nbsp;</xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="deluxe_price"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="premium_price = ''">&nbsp;</xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="premium_price"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
                <xsl:choose>
                    <xsl:when test="error_msg_txt = ''">
                        <td>Success</td>
                    </xsl:when>
                    <xsl:otherwise>
                        <td bgcolor="ff0000" title="{error_msg_txt}">Error</td>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="number(sort_variance) >= number($varianceThreshold)">
                        <td bgcolor="ff0000" title="Vendor Cost: {vc_variance}% Standard Price: {standard_variance}% Deluxe Price: {deluxe_variance}% Premium Price: {premium_variance}%">
                            <xsl:value-of select="sort_variance"/>
                        </td>
                    </xsl:when>
                    <xsl:otherwise>
                        <td title="Vendor Cost: {vc_variance}% Standard Price: {standard_variance}% Deluxe Price: {deluxe_variance}% Premium Price: {premium_variance}%">
                            <xsl:value-of select="sort_variance"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>
            </tr>
          </xsl:for-each>
        </table>
        </td>
      </tr>
      <tr>
        <td align="center">
        <table width="99%" border="0" cellpadding="3" cellspacing="0">
          <tr>
            <td align="center">
              <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                  <td align="right"><b>Error Counts:</b></td>
                  <xsl:variable name="noProductMaster" select="root/pageData/data[name='no_product_master']/value"/>
                  <xsl:variable name="noVendorMaster" select="root/pageData/data[name='no_vendor_master']/value"/>
                  <xsl:variable name="noVendorProduct" select="root/pageData/data[name='no_vendor_product']/value"/>
                  <td align="left">Product does not exist - 
                      <font color="ff0000"><xsl:value-of select="root/pageData/data[name='no_product_master']/value"/></font>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="left">Vendor does not exist - 
                      <font color="ff0000"><xsl:value-of select="root/pageData/data[name='no_vendor_master']/value"/></font>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="left">Product not assigned to Vendor - 
                      <font color="ff0000"><xsl:value-of select="root/pageData/data[name='no_vendor_product']/value"/></font>
                  </td>
                </tr>
              </table>
            </td>
            <td align="left">
                <input type="button" name="approveButton" id="approveButton" class="BlueButton" value="Approve" onclick="javascript:approveFile()"/>
                <input type="button" name="MainMenu" class="BlueButton" Value="Cancel" onclick="javascript: goMain();"/>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>