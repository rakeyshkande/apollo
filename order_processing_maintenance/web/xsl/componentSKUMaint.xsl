<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <xsl:variable name="component_sku_id" select="root/component_skus/component_sku/component_sku_id"/>
  <xsl:variable name="component_sku_name" select="root/component_skus/component_sku/component_sku_name"/>
  <xsl:variable name="base_cost" select="root/component_skus/component_sku/base_cost_dollar_amt"/>
  <xsl:variable name="base_cost_eff_date" select="root/component_skus/component_sku/base_cost_eff_date"/>
  <xsl:variable name="qa_cost" select="root/component_skus/component_sku/qa_cost_dollar_amt"/>
  <xsl:variable name="qa_cost_eff_date" select="root/component_skus/component_sku/qa_cost_eff_date"/>
  <xsl:variable name="freight_in_cost" select="root/component_skus/component_sku/freight_in_cost_dollar_amt"/>
  <xsl:variable name="freight_in_cost_eff_date" select="root/component_skus/component_sku/freight_in_cost_eff_date"/>
  <xsl:variable name="available_flag" select="root/component_skus/component_sku/available_flag"/>
  <xsl:variable name="comment_text" select="root/component_skus/component_sku/comment_text"/>

<html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Component SKU Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/FormChek.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/date.js"></script>

<script type="text/javascript">
<![CDATA[

function init() {
    madeChanges = "N";
    action = document.component.add_edit_flag.value;
    updateAllowed = document.component.updateAllowed.value;
    if (updateAllowed != "Y") {
        action = "View";
    }
    if (action == "Add") {
        document.getElementById('component_sku_id').focus();
    } else if (action == "Edit") {
        document.getElementById('component_sku_id').disabled = true;
        document.getElementById('component_sku_name').focus();
    } else {
        document.getElementById('component_sku_id').disabled = true;
        document.getElementById('component_sku_name').disabled = true;
        document.getElementById('base_cost').disabled = true;
        document.getElementById('base_cost_eff_date').disabled = true;
        document.getElementById('qa_cost').disabled = true;
        document.getElementById('qa_cost_eff_date').disabled = true;
        document.getElementById('freight_in_cost').disabled = true;
        document.getElementById('freight_in_cost_eff_date').disabled = true;
        //document.getElementById('available_flag').disabled = true;
        document.component.available_flag[0].disabled = true;
        document.component.available_flag[1].disabled = true;
        document.getElementById('comment_text').disabled = true;
        document.getElementById('saveButton').disabled = true;
        document.getElementById('calendar1').disabled = true;
        document.getElementById('calendar2').disabled = true;
        document.getElementById('calendar3').disabled = true;
    }

    idList = new Array();
    temp = document.component.idList.value.split(',');
    for (var i=0; i<temp.length; i++) {
        idList[temp[i]] = temp[i];
    }
    oldBaseCostEffDate = document.component.base_cost_eff_date.value;
    oldQACostEffDate = document.component.qa_cost_eff_date.value;
    oldFreightInCostEffDate = document.component.freight_in_cost_eff_date.value;
}

function closeWindow(){
    if (updateAllowed == "Y") {
        var form = document.component;
        var baseCostEffDate = form.base_cost_eff_date.value;
        var qaCostEffDate = form.qa_cost_eff_date.value;
        var freightInCostEffDate = form.freight_in_cost_eff_date.value;
        if (oldBaseCostEffDate != baseCostEffDate) {
            madeChanges = "Y";
        }
        if (oldQACostEffDate != qaCostEffDate) {
            madeChanges = "Y";
        }
        if (oldFreightInCostEffDate != freightInCostEffDate) {
            madeChanges = "Y";
        }
        if (madeChanges == "Y") {
            msg = "Do you want to save the data you have entered?";
            ret = doExitPageAction(msg);
            if (ret) {
                sendData();
                return;
            }
        }
    }
    document.component.action = "ComponentSKUMaintAction.do";
    document.component.action_type.value = "loadComponentSkuList";
    document.component.submit();
}

function doMainMenuAction()
{
    document.component.action = "ComponentSKUMaintAction.do";
    document.component.action_type.value = "MainMenuAction";
    document.component.submit();
}

function validate()
{
    var form = document.component;
    form.component_sku_id.className="input";
    form.component_sku_name.className="input";
    form.base_cost.className="input";
    form.base_cost_eff_date.className="input";
    form.qa_cost.className="input";
    form.qa_cost_eff_date.className="input";
    form.freight_in_cost.className="input";
    form.freight_in_cost_eff_date.className="input";
    form.comment_text.className="input";
    msg = "";

    var baseCost = form.base_cost.value;
    var baseCostEffDate = form.base_cost_eff_date.value;
    var qaCost = form.qa_cost.value;
    var qaCostEffDate = form.qa_cost_eff_date.value;
    var freightInCost = form.freight_in_cost.value;
    var freightInCostEffDate = form.freight_in_cost_eff_date.value;
    if (oldBaseCostEffDate != baseCostEffDate) {
        madeChanges = "Y";
    }
    if (oldQACostEffDate != qaCostEffDate) {
        madeChanges = "Y";
    }
    if (oldFreightInCostEffDate != freightInCostEffDate) {
        madeChanges = "Y";
    }
    
    if (madeChanges == "N") {
        msg = msg + "You haven't made any changes !\n";
    }

    //alert(baseCostEffDate + "\n" + qaCostEffDate + "\n" + freightInCostEffDate);
    if (action != "Edit") {
        if (isWhitespace(form.component_sku_id.value))
        {
            form.component_sku_id.className="ErrorField";
            if (msg == "") form.component_sku_id.focus();
            msg = msg + "Component SKU is required\n";
        } else {
            if (!isAlphanumeric(form.component_sku_id.value, false, true))
            {
                form.component_sku_id.className="ErrorField";
                if (msg == "") form.component_sku_id.focus();
                msg = msg + "Component SKU must be Alpha Numeric\n";
            } else {
                form.component_sku_id.value = form.component_sku_id.value.toUpperCase();
                if (idList[form.component_sku_id.value] == form.component_sku_id.value) {
                    form.component_sku_id.className="ErrorField";
                    if (msg == "") form.component_sku_id.focus();
                    msg = msg + "Component SKU already exists\n";
                }
            }
        }
    }
    if (isWhitespace(form.component_sku_name.value))
    {
        form.component_sku_name.className="ErrorField";
        if (msg == "") form.component_sku_name.focus();
        msg = msg + "Component SKU Name is required\n";
    } else {
        if (!isAlphanumeric(form.component_sku_name.value, false, true))
        {
            form.component_sku_name.className="ErrorField";
            if (msg == "") form.component_sku_name.focus();
            msg = msg + "Component SKU Name must be Alpha Numeric\n";
        }
    }

    if (baseCost == "" && baseCostEffDate != "") {
        form.base_cost.className="ErrorField";
        if (msg == "") form.base_cost.focus();
        msg = msg + "Base Cost is required\n";
    } else {
        if (baseCostEffDate == "" && baseCost != "") {
            form.base_cost_eff_date.className="ErrorField";
            if (msg == "") form.base_cost_eff_date.focus();
            msg = msg + "Base Cost Effective Date is required\n";
        }
    }
    if (baseCost != "" && !(isFloat(baseCost))) {
        form.base_cost.className="ErrorField";
        if (msg == "") form.base_cost.focus();
        msg = msg + "Base Cost must be numeric\n";
    }
    if (baseCostEffDate != "" && !(validateDate(baseCostEffDate, "U", "A"))) {
        form.base_cost_eff_date.className="ErrorField";
        if (msg == "") form.base_cost_eff_date.focus();
        msg = msg + "Invalid Base Cost Effective Date (must be MM/DD/YYYY)\n";
    }

    if (qaCost == "" && qaCostEffDate != "") {
        form.qa_cost.className="ErrorField";
        if (msg == "") form.qa_cost.focus();
        msg = msg + "QA Cost is required\n";
    } else {
        if (qaCostEffDate == "" && qaCost != "") {
            form.qa_cost_eff_date.className="ErrorField";
            if (msg == "") form.qa_cost_eff_date.focus();
            msg = msg + "QA Cost Effective Date is required\n";
        }
    }
    if (qaCost != "" && !(isFloat(qaCost))) {
        form.qa_cost.className="ErrorField";
        if (msg == "") form.qa_cost.focus();
        msg = msg + "QA Cost must be numeric\n";
    }
    if (qaCostEffDate != "" && !(validateDate(qaCostEffDate, "U", "A"))) {
        form.qa_cost_eff_date.className="ErrorField";
        if (msg == "") form.qa_cost_eff_date.focus();
        msg = msg + "Invalid QA Cost Effective Date (must be MM/DD/YYYY)\n";
    }

    if (freightInCost == "" && freightInCostEffDate != "") {
        form.freight_in_cost.className="ErrorField";
        if (msg == "") form.freight_in_cost.focus();
        msg = msg + "Freight In Cost is required\n";
    } else {
        if (freightInCostEffDate == "" && freightInCost != "") {
            form.freight_in_cost_eff_date.className="ErrorField";
            if (msg == "") form.freight_in_cost_eff_date.focus();
            msg = msg + "Freight In Cost Effective Date is required\n";
        }
    }
    if (freightInCost != "" && !(isFloat(freightInCost))) {
        form.freight_in_cost.className="ErrorField";
        if (msg == "") form.freight_in_cost.focus();
        msg = msg + "Freight In Cost must be numeric\n";
    }
    if (freightInCostEffDate != "" && !(validateDate(freightInCostEffDate, "U", "A"))) {
        form.freight_in_cost_eff_date.className="ErrorField";
        if (msg == "") form.freight_in_cost_eff_date.focus();
        msg = msg + "Invalid Freight In Cost Effective Date (must be MM/DD/YYYY)\n";
    }

    if (form.comment_text.value.length > 4000) {
        form.comment_text.className="ErrorField";
        if (msg == "") form.comment_text.focus();
        msg = msg + "Please limit your comments to 4000 characters\n";
    }
    if (msg != "" ) {
        alert(msg);
        return false;
    }
 
    return true;
}

function sendData()
{

    if(validate())
    {
        document.component.component_sku_id.disabled = false;
        document.component.action = "ComponentSKUMaintAction.do";
        document.component.action_type.value = "saveComponentSku";
        document.component.submit();
    }
    else
    {
        // user has to fix errors and resubmit the form
        return;
    }
}

function markChanges() {
    madeChanges = "Y";
}

function enterkey()
{
    if(window.event.keyCode == 13) {
        return false;
    }
}

]]>
</script>

</head>

<body onLoad="init()">
<form name="component" action="" method="post">

<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
<input type="hidden" name="add_edit_flag" value="{key('pageData','add_edit_flag')/value}"/>
<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
<input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
<input type="hidden" name="idList" value="{key('pageData','idList')/value}"/>

<CENTER>

<table width="775" border="0" align="center">
    <tr>
        <td width="30%">
        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </td>
        <td width="40%" align="center" valign="center">
        <font class="Header">Component SKU Maintenance</font>
        </td>
        <td  id="time" align="right" valign="center" class="Label" width="30%">
        <script type="text/javascript">startClock();</script>
        </td>
    </tr>
</table>

<table width="775" align="center" cellspacing="1">
    <tr>
        <td align="right">
        <button accesskey="M" class="BlueButton" style="width:75;" name="MainMenu" tabindex="1" onclick="javascript:doMainMenuAction();">(M)ain Menu</button>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<TABLE width="98%" border="0" cellspacing="1" cellpadding="1">
    <TR>
        <TD align="right" class="Label">Component SKU:&nbsp;</TD>
        <td align="left">
            <input type="text" name="component_sku_id" value="{$component_sku_id}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Component SKU Name:&nbsp;</TD>
        <td align="left">
            <input type="text" name="component_sku_name" value="{$component_sku_name}" size="20" maxlength="25" onChange="markChanges()" onkeypress="return enterkey();" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Base Cost:&nbsp;</TD>
        <td align="left">
            <input type="text" name="base_cost" value="{$base_cost}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Base Cost Effective Date:&nbsp;</TD>
        <td align="left">
            <input type="text" name="base_cost_eff_date" value="{$base_cost_eff_date}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
            &nbsp;
            <input type="image" id="calendar1" src="images\calendar.gif" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">QA Cost:&nbsp;</TD>
        <td align="left">
            <input type="text" name="qa_cost" value="{$qa_cost}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">QA Cost Effective Date:&nbsp;</TD>
        <td align="left">
            <input type="text" name="qa_cost_eff_date" value="{$qa_cost_eff_date}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
            &nbsp;
            <input type="image" id="calendar2" src="images\calendar.gif" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Freight In Cost:&nbsp;</TD>
        <td align="left">
            <input type="text" name="freight_in_cost" value="{$freight_in_cost}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Freight In Cost Effective Date:&nbsp;</TD>
        <td align="left">
            <input type="text" name="freight_in_cost_eff_date" value="{$freight_in_cost_eff_date}" size="10" maxlength="10" onChange="markChanges()" onkeypress="return enterkey();" />
            &nbsp;
            <input type="image" id="calendar3" src="images\calendar.gif" />
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Available to Select:&nbsp;</TD>
        <td align="left">
            <input type="radio" name="available_flag" value="Y" onChange="markChanges()" onkeypress="return enterkey();">
            <xsl:if test="$available_flag = 'Y'">
                <xsl:attribute name="checked">
                true
                </xsl:attribute>
            </xsl:if>
            Yes</input>
            <input type="radio" name="available_flag" value="N" onChange="markChanges()" onkeypress="return enterkey();">
            <xsl:if test="$available_flag = 'N' or not($available_flag)">
                <xsl:attribute name="checked">
                true
                </xsl:attribute>
            </xsl:if>
            No</input>
        </td>
    </TR>
    <TR>
        <TD align="right" class="Label">Comments:&nbsp;</TD>
        <td align="left">
            <textarea name="comment_text" rows="5" cols="50" onChange="markChanges()" >
                <xsl:value-of select="$comment_text"/>
            </textarea>
        </td>
    </TR>
</TABLE>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
    <tr>
        <td align="center">
            <button style="width:80" accesskey="S" name="saveButton" onclick="sendData()" class="BlueButton">(S)ave</button>
            &nbsp;&nbsp;
            <button style="width:80" accesskey="C" onclick="closeWindow()" class="BlueButton">(C)ancel</button>
        </td>
    </tr>
</table>

</CENTER>
</form>

<div id="waitDiv" style="display:none">
    <table id="waitTable" width="750" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
            <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                        <td id="waitTD" width="50%" class="waitMessage"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<!-- call footer template-->
<xsl:call-template name="footer"/>
</body>
</html>

<script type="text/javascript">
Calendar.setup(
{
    inputField  : "base_cost_eff_date",  // ID of the input field
    ifFormat    : "mm/dd/y",  // the date format
    button      : "calendar1"  // ID of the button
}
);
Calendar.setup(
{
    inputField  : "qa_cost_eff_date",  // ID of the input field
    ifFormat    : "mm/dd/y",  // the date format
    button      : "calendar2"  // ID of the button
}
);
Calendar.setup(
{
    inputField  : "freight_in_cost_eff_date",  // ID of the input field
    ifFormat    : "mm/dd/y",  // the date format
    button      : "calendar3"  // ID of the button
}
);
</script>

</xsl:template>  
</xsl:stylesheet>
