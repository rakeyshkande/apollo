<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="securitytoken"/>
	<xsl:param name="context"/>	
	<xsl:param name="applicationcontext"/>
	<xsl:param name="sc_cust_ind"/>
	<xsl:param name="sc_tracking_number"/>
	<xsl:param name="sc_cust_number"/>
	<xsl:param name="sc_phone_number"/>
	<xsl:param name="sc_rewards_number"/>
	<xsl:param name="sc_cc_number"/>
	<xsl:param name="sc_recip_ind"/>
	<xsl:param name="sc_order_number"/>
	<xsl:param name="sc_email_address"/>
	<xsl:param name="sc_zip_code"/>
	<xsl:param name="sc_last_name"/>
	<xsl:param name="call_cs_number"/>
	<xsl:param name="call_type_flag"/>
	<xsl:param name="call_brand_name"/>
	<xsl:param name="call_dnis"/>
	<xsl:param name="t_call_log_id"/>
	<xsl:param name="t_entity_history_id"/>
	<xsl:param name="t_comment_origin_type"/>

  	<xsl:output method="html" indent="yes"/>
	<xsl:template name="securityanddata">
			<!--security-->
			    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
			    <input type="hidden" name="context" value="{$context}"/>
				  <input type="hidden" name="applicationcontext" value="{$applicationcontext}"/>
		
			<!-- searchCriteria -->
					<input type="hidden" name="sc_cust_ind" value="{$sc_cust_ind}" />
					<input type="hidden" name="sc_tracking_number" value="{$sc_tracking_number}" />
					<input type="hidden" name="sc_cust_number" value="{$sc_cust_number}" />
					<input type="hidden" name="sc_phone_number" value="{$sc_phone_number}" />
					<input type="hidden" name="sc_rewards_number" value="{$sc_rewards_number}" />
					<input type="hidden" name="sc_cc_number" value="{$sc_cc_number}" />
					<input type="hidden" name="sc_recip_ind" value="{$sc_recip_ind}" />
					<input type="hidden" name="sc_order_number" value="{$sc_order_number}" />
					<input type="hidden" name="sc_email_address" value="{$sc_email_address}" />
					<input type="hidden" name="sc_zip_code" value="{$sc_zip_code}" />
					<input type="hidden" name="sc_last_name" value="{$sc_last_name}" />
		 <!-- header.xsl -->
		 		<input type="hidden" name="call_cs_number" value="{$call_cs_number}"/>
				<input type="hidden" name="call_type_flag" value="{$call_type_flag}"/>
				<input type="hidden" name="call_brand_name" value="{$call_brand_name}"/>
				<input type="hidden" name="call_dnis" value="{$call_dnis}"/>
				
		<!-- call log-->
				<input type="hidden" name="t_call_log_id" value="{$t_call_log_id}"/>
			
	  <!-- timer -->
	  		<input type="hidden" name="t_entity_history_id" value="{$t_entity_history_id}"/>
			<input type="hidden" name="t_comment_origin_type" value="{$t_comment_origin_type}"/>
	</xsl:template>	
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->