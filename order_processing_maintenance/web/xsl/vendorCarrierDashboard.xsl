<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="commonUtil.xsl"/>
  <!-- Keys -->
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <TITLE>FTD - Vendor Carrier Dashboard</TITLE>
        <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
          <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
          <script type="text/javascript" src="js/clock.js"/>
          <script type="text/javascript" src="js/FormChek.js"/>
          <script type="text/javascript" src="js/cityLookup.js"/>
          <script type="text/javascript" src="js/popup.js"/>
          <script type="text/javascript" src="js/util.js"/>
          <script type="text/javascript" src="js/commonUtil.js"/>
          <script type="text/javascript" src="js/calendar.js"/>
          <script language="JavaScript" type="text/javascript">
          <![CDATA[   
            var PAGE_URL = '';
          
            function init() { }
          
            function editCarrier(vendorId)
            {
              var id_field = document.getElementById("vendor_id");
              id_field.value = vendorId;
              
              var lock_field = document.getElementById("lock_id");
              lock_field.value = vendorId;
              
              document.forms.DashboardForm.action = "CarrierAction.do";
              document.forms.DashboardForm.action_type.value = "load_page";
              
              var url = "CarrierAction.do" + "?action_type=load_page" + 
                  "&vendor_id=" + vendorId + getSecurityParams(false);

              doRecordLock('VENDOR','retrieve', null);
              
              setTimeout("performActionWithLock('" + url + "')", 100 );
              
            }
          
            function performAction(url)
            {
              PAGE_URL = url;
              doContinueProcess();
            }
          
            function clickMainMenu()
            {
              var url = "MainMenuAction.do" +  getSecurityParams(true);
              performActionWithUnlock(url);
            }
          
            function performActionWithUnlock(url)
            {
              PAGE_URL = url;        
              doRecordLock('VENDOR','unlock', null); 

              doContinueProcess();
          	}
            
            function performActionWithLock(url)
            {
              PAGE_URL = url;
              doRecordLock('VENDOR','check', null);
            }
            
            function doContinueProcess()
            {
              document.forms[0].action = PAGE_URL;
              document.forms[0].target = window.name;
              document.forms[0].submit();
            }
            
            function checkAndForward(forwardAction)
            {
              doContinueProcess();
            }
          ]]>
          </script>
        </META>
      </HEAD>
      <BODY onload="javascript:init();">
        <TABLE cellSpacing="0" cellPadding="0" width="98%" align="center"
               border="0">
          <TBODY>
            <TR>
              <TD align="left" width="20%">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131"
                     height="32"/>
              </TD>
              <TD class="header" align="center" width="60%">Vendor Carrier Dashboard </TD>
              <TD class="label" align="right" width="20%">
                <TABLE cellSpacing="0" cellPadding="0" width="100%">
                  <TBODY>
                    <TR>
                      <TD class="label" id="time" align="right">
                      </TD>
                      <SCRIPT type="text/javascript">
                        startClock();
                      </SCRIPT>
                    </TR>
                    <TR>
                      <TD align="right">&nbsp;</TD>
                    </TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>
            <TR>
              <TD colSpan="3">
                <HR/>
              </TD>
            </TR>
          </TBODY>
        </TABLE> 
        <FORM name="DashboardForm" action="BoxXDashboardServlet" method="get">
          <xsl:call-template name="securityanddata"/>
          <INPUT type="hidden" name="formSuffix"/>
          <INPUT type="hidden" name="action_type"/>
          <INPUT type="hidden" name="vendor_id" id="vendor_id"/>
          <INPUT type="hidden" name="vendor_name" id="vendor_name"/>
          <input type="hidden" name="lock_id" id="lock_id"/>
          <INPUT type="hidden" name="origin" id="origin" value="dashboard"/>
          <TABLE width="98%" align="center" cellspacing="1" class="mainTable">
            <TBODY>
              <tr valign="top">
                <td>
                  <table width="100%" align="center" cellspacing="3">
                    <tr>
                      <td align="center" class="TotalLine">
                        <div align="center">
                          <strong>Vendor Shipping Services</strong>
                          <br/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="600" align="center" class="mainTable">
                          <tr>
                            <td>
                              <table width="100%" align="center" cellpadding="3"
                                     cellspacing="0" border="0">
                                <tr class="TotalLine">
                                  <td class="TotalLine" align="center">Vendor</td>
                                  <xsl:for-each select="vendor_carriers/vendor_carrier">
                                    <xsl:if test="position() &lt;= counter">
                                      <td width="35" class="TotalLine"
                                          align="center"><xsl:value-of select="carrier_id"/></td>
                                    </xsl:if>
                                  </xsl:for-each>
                                  <td width="70" class="TotalLine" align="center">Start Date</td>
                                  <td width="70" class="TotalLine" align="center">End Date</td>
                                  <xsl:for-each select="vendor_carriers/vendor_carrier">
                                    <xsl:if test="position() &lt;= counter">
                                      <td width="35" class="TotalLine"
                                          align="center"><xsl:value-of select="carrier_id"/></td>
                                    </xsl:if>
                                  </xsl:for-each>
                                  <td width="25">&nbsp;</td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div id="targetAudience"
                                   style="overflow:auto; width:100%; height:250; padding:0px; margin:0px">
                                <table width="100%" align="center"
                                       cellpadding="3" cellspacing="0"
                                       border="0">
                                       <tr> <td>
                                <table width="100%" align="center"
                                       cellpadding="3" cellspacing="0"
                                       border="0">
                                  <tr bgcolor="#FFFFFF">
                                  <xsl:for-each select="vendor_carriers/vendor_carrier">
                                      <xsl:if test="position() mod counter = 1">
                                        <td align="left">
                                          <a href="#" onclick="editCarrier('{vendor_id}')">
                                            <xsl:value-of select="vendor_id"/>-<xsl:value-of select="vendor_name"/>
                                          </a>
                                        </td>
                                      </xsl:if>
                                      <td width="35" align="center">
                                        <xsl:value-of select="usage_percent"/>%</td>
                                      
                                      <xsl:if test="position() mod counter = 0">
                                        <tr>
                                        </tr>
                                      </xsl:if>
                                  </xsl:for-each>
                                  </tr>
                                </table>
                                </td>
                                <td>
                                <table width="100%"
                                       cellpadding="3" cellspacing="0"
                                       border="0">
                                  <tr bgcolor="#FFFFFF">
                                  <xsl:for-each select="vendor_carriers/vendor_carrier">
                                      <xsl:if test="position() mod counter = 1">
                                        <td width="70" align="left">
                                            <xsl:value-of select="substring(override_start,1,10)"/>&nbsp;
                                        </td>
                                        <td width="70" align="left">
                                            <xsl:value-of select="substring(override_end,1,10)"/>&nbsp;
                                        </td>
                                      </xsl:if>
                                      <td width="35" align="center">
                                        <xsl:if test="string-length(override_percentage) != 0">
                                          <xsl:value-of select="override_percentage"/>%
                                        </xsl:if>&nbsp;
                                      </td>
                                      <xsl:if test="position() mod counter = 0">
                                        <tr>
                                        </tr>
                                      </xsl:if>
                                  </xsl:for-each>
                                  </tr>
                                </table>
                                </td>
                                </tr>
                                </table>
                                
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </TBODY>
          </TABLE>
          <TABLE cellSpacing="2" cellPadding="2" width="98%" align="center"
                 border="0">
            <TBODY>
              <TR>
                <TD width="10%">
                </TD>
                <TD valign="bottom" align="center" width="74%">&nbsp;</TD>
                <td width="16%" align="right">
                  <table>
                    <tr>
                      <td align="right" valign="top">
                        <button class="BlueButton" style="width: 100px;"
                                accesskey="M" name="mainMenu" tabindex="39"
                                onclick="javascript:clickMainMenu()">(M)ain Menu </button>
                      </td>
                    </tr>
                  </table>
                </td>
              </TR>
            </TBODY>
          </TABLE>
          <xsl:call-template name="footer"/>
        </FORM>
      <iframe id="checkLock" width="0px" height="0px" border="0"/>
      </BODY>
    </HTML>
  </xsl:template>
  <xsl:template name="addHeader">
    <xsl:call-template name="header">
      <xsl:with-param name="showBackButton" select="true()"/>
      <xsl:with-param name="showPrinter" select="true()"/>
      <xsl:with-param name="showSearchBox" select="false()"/>
      <xsl:with-param name="searchLabel" select="'Order #'"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
    </xsl:call-template>
  </xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
    <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/
    <xsl:value-of select="substring($dateTimeString, 9, 2)"/>/
    <xsl:value-of select="substring($dateTimeString, 1, 4)"/>
  </xsl:template>
</xsl:stylesheet>