<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="commonUtil.xsl"/>
  
  <xsl:variable name="zoneJumpable" select="//vendor/zone_jump_eligible_flag"/>
  <xsl:variable name="newShippingSystem" select="//vendor/new_shipping_system"/>
  <!-- Keys -->
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:decimal-format NaN="" name="notANumber"/>

  <xsl:template match="/">
    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html">
        </META>
        <title>Vendor Maintenance</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/clock.js">
        </script>
        <script type="text/javascript" src="js/FormChek.js"/>
        <script type="text/javascript" src="js/cityLookup.js"/>
        <script type="text/javascript" src="js/popup.js"/>
        <script type="text/javascript" src="js/util.js">
        </script>
        <script type="text/javascript" src="js/commonUtil.js">
        </script>
        <script type="text/javascript" src="js/calendar.js"/>
        <script>
           var usStates = new Array(
          <xsl:for-each select="//STATE[countrycode='']">["<xsl:value-of select="statemasterid"/>", "<xsl:value-of select="statename"/>"]
            <xsl:choose>
              <xsl:when test="position()!=last()">,</xsl:when>
            </xsl:choose>
          </xsl:for-each>);

        </script>
        <script>
          <![CDATA[

            var PAGE_URL = '';
            var has_changed = false;

            /*
             * If any changes were made to this page this flag is set to true.
             * We make no data checks here the server will handle that.
             * We simply check if a user modified any fields in anyway.
             */
            function doOnChangeAction()
            {
              has_changed = true;

              //set selected cutoff time
              document.getElementById("cutoff_time").value = document.getElementById("cutoff").value;
            }

            function doCheckOnKeyDown()
            {
              has_changed = true;
            }

            function init()
            {

              document.oncontextmenu = contextMenuHandler;
              document.onkeydown = backKeyHandler;

              //obtain current and original vendor type
              var vendorType = document.getElementById("vendor_type").value;

              //set selected SDS carriers
              setSelectedCarriers();

              //determine which carrier information to display
              doVendorTypeChange(vendorType, true);

              //obtain cutoff time select
              var cutoffTimeObj = document.getElementById("cutoff");

              //set current cutoff time
              var i = 0;
              var done = false;
              while(cutoffTimeObj.length > i && !done)
              {
                if(cutoffTimeObj.options[i].value == document.getElementById("cutoff_time").value)
                {
                  cutoffTimeObj.selectedIndex = i;
                  done = true;
                }
                i = i + 1;
              }

              var editMode = document.getElementById("edit_mode").value;

              if (editMode == "edit" || editMode == "new")
              {
                if(vendorType == "SDS")
                {
                    //enable cutoff time
                    cutoffTimeObj.disabled = false;
                }

                if (document.getElementById("lead_days").leadDaysValue != "")
                {
                  document.getElementById("lead_days").selectedIndex = document.getElementById("lead_days").leadDaysValue;
                }
                else
                {
                  document.getElementById("lead_days").selectedIndex = 2;
                }
              }

              var errorMsg = document.getElementById("error_message").value;
              if(errorMsg != "")
              {
                alert(errorMsg);
              }

              var popup = document.getElementById("popup").value;
              if (popup == "comments")
              {
                displayComments('Y', 'Y');
              }
            }

            function setSelectedCarriers()
            {
                var editMode = document.getElementById("edit_mode").value;
                if(document.getElementById("selected_carriers").options.length > 0 && editMode != "display")
                {
                    selectedCarriers = document.getElementById("selected_carriers").options;
                }
                else if(document.getElementById("shipping_keys").options.length > 0)
                {
                    selectedCarriers = document.getElementById("shipping_keys").options;
                }
                //must be a new vendor just set it to the empty shipping keys
                else
                {
                   selectedCarriers = document.getElementById("shipping_keys").options;
                }

                activeCarriers = document.getElementById("active_carriers").options;
                var selected;

                for(var i = 0; i < selectedCarriers.length; i++)
                {
                    selected = false;
                    for(var j = 0; j < activeCarriers.length && selected == false; j++)
                    {
                        if(selectedCarriers[i].value == activeCarriers[j].value)
                            activeCarriers[j].selected = true;
                    }
                }
            }

            function goBack()
            {
              if(!has_changed || doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var editMode = document.getElementById("edit_mode").value;
                var url = "";

                //if in display or new mode go back to the vendor list
                if (editMode == "display" || editMode == "new")
                {
                  url = "VendorListAction.do?popup=false&" + getSecurityParams(false);
                }
                else
                {
                  var vendorId = document.getElementById("vendor_id").value;
                  url = "VendorDetailAction.do" + "?action_type=display_vendor" + "&vendor_id=" + vendorId + "&edit_mode=display" + getSecurityParams(false);
                }

                performActionWithUnlock(url);
              }
            }

            function deleteBlock(blockDate, blockType)
            {
              if(!has_changed ||  doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                if(doExitPageAction("Are you sure you want to delete this record?"))
                {
                  var url = "VendorListAction.do" +
          "?action_type=delete_global_block" +"&global_delete_block_date=" + blockDate + "&global_delete_block_type=" + blockType + getSecurityParams(false);
                  performActionWithLock(url);
                }
              }
            }

            function displayComments(required, canEdit)
            {
               var vendorId = document.getElementById("vendor_id").value;
               var url = '';
               url = "CommentsAction.do"+"?action_type=display"+"&comment_type=VENDOR_MAINT&vendor_id=" + vendorId +"&can_enter_comment=" + canEdit +"&comment_required=" + required + "&comment_max_size=255" + "&comment_numbering=Y" + getSecurityParams(false);
               var modalFeatures  =  'dialogWidth:750px;dialogHeight:460px;center:yes;status:no;help:no;';
               modalFeatures +=  'resizable:no;scroll:no';
               ret =  window.showModalDialog(url, "", modalFeatures);
            }

            function editVendor()
            {
              if(!has_changed ||  doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var vendorId = document.getElementById("vendor_id").value;

                var url = "VendorDetailAction.do" +
                  "?action_type=display_vendor" + "&vendor_id=" + vendorId + "&edit_mode=edit" + getSecurityParams(false);
                performActionWithLock(url);
              }
            }

            function saveVendor(editMode)
            {
              var vendorType = document.getElementById("vendor_type").value;

              //validate at least one carrier was chosen if vendor type is SDS
              if(vendorType == "SDS")
              {
                 if(document.getElementById("active_carriers").selectedIndex == -1)
                 {
                    alert("Please choose at least one carrier.");
                    return false;
                 }
              }

              var vendorId = document.getElementById("vendor_id").value;
              var url = "VendorDetailAction.do" +
                "?action_type=save_vendor" + "&vendor_id=" + vendorId + "&edit_mode=" + editMode + getSecurityParams(false);
              PAGE_URL = url;

              enableVendorSkuAndCost();

              doContinueProcess();
              //performActionWithLock(url);
            }


            function enableVendorSkuAndCost()
            {
              var inputId, inputName, inputs;
              inputs = document.getElementsByTagName('input');

              for(i = 0; i < inputs.length; i++)
              {
                inputName = inputs[i].name;
                inputId = inputs[i].id;
                if (inputs[i].name == 'vendor_add_on_sku' || inputs[i].name == 'vendor_add_on_cost' || inputs[i].name == 'vendor_vase_sku' || inputs[i].name == 'vendor_vase_cost' )
                {
                  document.getElementById(inputId).disabled = false;
                }
              }
            }


            function deleteVendor()
            {
              if(!has_changed ||  doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var vendorId = document.getElementById("vendor_id").value;
                if(doExitPageAction("Are you sure you want to delete this record?"))
                {
                  var url = "VendorDetailAction.do" +
                   "?action_type=delete_vendor" + "&vendor_id=" + vendorId + getSecurityParams(false);
                  performActionWithLock(url);
                }
              }
            }

            function addVendorBlock()
            {
              var url = "VendorDetailAction.do" + "?action_type=add_block"  + getSecurityParams(false);
              performActionWithLock(url);
            }

            function deleteVendorBlock(blockDate, blockType)
            {
              if(!has_changed ||  doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                if(doExitPageAction("Are you sure you want to delete this record?"))
                {
                  var url = "VendorDetailAction.do" +
          "?action_type=delete_vendor_block" +"&delete_vendor_date=" + blockDate + "&vendor_delete_block_type=" + blockType + getSecurityParams(false);
                  performActionWithLock(url);
                }
              }
            }

            function clickMainMenu()
            {
              if(!has_changed ||  doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var url = "MainMenuAction.do" +  getSecurityParams(true);
                performActionWithUnlock(url);
              }
            }

            function displayProductList()
            {
              var vendorId = document.getElementById("vendor_id").value;
              var edit_mode = document.getElementById("edit_mode").value;

              var url = "VendorProductListAction.do" + "?action_type=display_list"  +  "&vendor_id=" + vendorId + "&edit_mode" + edit_mode + getSecurityParams(false);
              performActionWithUnlock(url);
            }

            function performActionWithLock(url)
            {
              PAGE_URL = url;
              doRecordLock('VENDOR','check', null);
            }

            function performActionWithUnlock(url)
            {
              PAGE_URL = url;
              doRecordLock('VENDOR','unlock', null);
              doContinueProcess();
            }

            function doContinueProcess()
            {
              showWaitMessage("content", "wait", "Processing");

              document.forms[0].action = PAGE_URL;
              document.forms[0].target = window.name;
              document.forms[0].submit();
            }

            function checkAndForward(forwardAction)
            {
              doContinueProcess();
            }

            function doCityLookup()
            {
              CityPopup.setup(
              {
                displayArea:'cityLookupLink',
                city: 'city',
                state: 'state',
                zip: 'zipcode',
                country: 'country'
              }
            );

          }

          function editCarrier()
          {
            document.forms.vendormaint.action = "CarrierAction.do";
            document.forms.vendormaint.submit();
          }

          function switchCarrier(vendorType)
          {
            var editMode = document.getElementById("edit_mode").value;

            //if in edit mode and not creating a new vendor decide if the carrier link or label should be displayed
            if ( editMode == "edit" || editMode != "new" )
            {
              //decide if the carrier link should be displayed based on vendor type
              if(vendorType == "ESCALATE")
              {
                document.getElementById("venusCarrierLinkDiv").style.display = "block";
                document.getElementById("carrierLabDiv").style.display = "none";
              }
              else
              {
                document.getElementById("venusCarrierLinkDiv").style.display = "none";
                document.getElementById("carrierLabDiv").style.display = "block";
              }
            }
            //if not in edit mode or creating a new vendor only display the carrier label
            else
            {
              document.getElementById("venusCarrierLinkDiv").style.display = "none";
              document.getElementById("carrierLabDiv").style.display = "block";
            }
          }

          function doVendorTypeChange(vendorType, initialLoad)
          {
            var origVendorType = document.getElementById("orig_vendor_type").value;
            var editMode = document.getElementById("edit_mode").value;
            if(!initialLoad && origVendorType == "SDS" && editMode != "new")
            {
                alert("Once the vendor type is ARGO it cannot be changed")
                options = document.getElementById("vendor_type").options;
                for(var i = 0; i < options.length; i++)
                {
                    if(options[i].value == "SDS")
                    {
                        options[i].selected = true;
                        i = options.length;
                    }
                }
                return false;
            }

            var editMode = document.getElementById("edit_mode").value;

            //obtain cutoff time select
            var cutoffTimeObj = document.getElementById("cutoff");

            //enable carrier div based on vendor type
            if(vendorType == "SDS")
            {
              document.getElementById("sdsCarrierDiv").style.display = "block";
              document.getElementById("venusCarrierDiv").style.display = "none";

              document.getElementById("nonSDSCutoffDiv").style.display = "none";

              //only make the active_carriers editable in edit mode
              if(editMode == "edit" || editMode == "new")
              {
                document.getElementById("active_carriers").disabled = false;
                cutoffTimeObj.disabled = false;
              }
            }
            else if(vendorType == "ESCALATE")
            {
              document.getElementById("sdsCarrierDiv").style.display = "none";
              document.getElementById("venusCarrierDiv").style.display = "block";
              document.getElementById("nonSDSCutoffDiv").style.display = "block";
              cutoffTimeObj.disabled = true;
            }
            else
            {
              document.getElementById("sdsCarrierDiv").style.display = "none";
              document.getElementById("venusCarrierDiv").style.display = "none";
              document.getElementById("nonSDSCutoffDiv").style.display = "block";
              cutoffTimeObj.disabled = true;
            }

            //determine if carrier link or label should be displayed
            switchCarrier(vendorType);
          }
               ]]>
        </script>
      </head>
      <body onload="javascript: init()">
        <form name="vendormaint" method="post">
          <input type="hidden" name="securitytoken"
                 value="{//pagedata/securitytoken}"/>
          <input type="hidden" name="context" value="{//pagedata/context}"/>
          <input type="hidden" name="adminAction"
                 value="{//pagedata/adminAction}"/>
          <input type="hidden" name="edit_mode" value="{//pagedata/edit_mode}"/>
          <input type="hidden" name="cutoff_time" value="{//vendor/cutoff_time}"/>
          <input type="hidden" name="popup" value="{//pagedata/popup}"/>
          <input type="hidden" name="country" value="US"/>
          <input type="hidden" name="lock_id" value="{//vendor/vendor_id}"/>
          <input type="hidden" name="orig_vendor_type" value="{//pagedata/orig_vendor_type}"/>
          <input type="hidden" name="error_message" value="{//error/error_message}"/>
          <xsl:call-template name="securityanddata"/>

          <!-- Main Div -->
          <div id="content" style="display:block">

            <table width="98%" border="0" cellpadding="0" cellspacing="0"
                   align="center">
              <tr>
                <td width="30%" height="33" valign="top">
                  <div class="floatleft">
                    <img border="0" src="images/wwwftdcom_131x32.gif" width="131"
                         height="32"/>
                  </div>
                </td>
                <td>
                </td>
                <td align="right" width="30%" valign="bottom">
                </td>
              </tr>
              <tr>
                <td>
                </td>
                <td align="center" valign="top" class="Header" id="pageHeader">
                  <xsl:choose>
                    <xsl:when test="root/pagedata/edit_mode = 'edit'">Update Vendor</xsl:when>
                    <xsl:when test="root/pagedata/edit_mode = 'display'">View Vendor</xsl:when>
                    <xsl:when test="root/pagedata/edit_mode = 'new'">Create Vendor</xsl:when>
                  </xsl:choose>
                </td>
                <td id="time" align="right" valign="bottom" class="Label"
                    width="30%" height="30">
                </td>
                <script type="text/javascript">startClock();</script>
              </tr>
            </table>
            <!-- Display table-->
            <table width="98%" align="center" cellspacing="1" class="mainTable" border="0">
              <tr>
                <td>
                  <table width="100%" align="center" class="innerTable" border="0">
                    <tr>
                      <td align="left" height="10">
                        <div id="invalidDiv" style="display:none"
                             class="ErrorMessage floatleft">
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" align="center" border="0">
                          <tr>
                            <td class="label" align="right">
                    Vendor ID Number:
                  </td>
                            <td>
                              <input type="hidden" name="vendor_id"
                                     value="{//vendor/vendor_id}" maxLength="5"
                                     onchange="doOnChangeAction()"
                                     onkeypress="doCheckOnKeyDown()"/>
                              <xsl:value-of select="//vendor/vendor_id"/>
                            </td>
                            <td class="label" align="right">
                    Vendor Code:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit'">
                                  <xsl:value-of select="//vendor/member_number"/>
                                  <input type="hidden" name="vendor_code"
                                         value="{//vendor/member_number}"
                                         tabindex="1"/>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/member_number"/>
                                  <input type="hidden" name="vendor_code"
                                         value="{//vendor/member_number}"/>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="vendor_code"
                                               size="9" tabindex="1"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               value="{//vendor/member_number}"
                                               maxLength="9"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/vendor_code"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" class="TotalLine" colspan="4">
                              <div align="center">
                                <strong>Vendor Information</strong>
                                <br>
                                </br>
                              </div>
                              <div align="left">
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Vendor Name:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="vendor_name"
                                               size="50" maxLength="50"
                                               tabindex="2"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               value="{//vendor/vendor_name}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/vendor_name"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/vendor_name"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>                          <td class="label" align="right">
                        Lead Days:
                      </td>
                            <td>
                              <table>
                                <tr>
                                  <xsl:choose>
                                    <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                      <td>
                                        <select name="lead_days" id="lead_days"
                                                tabIndex="18"
                                                leadDaysValue="{//vendor/lead_days}">
                                          <OPTION VALUE="0">0</OPTION>
                                          <OPTION VALUE="1">1</OPTION>
                                          <OPTION VALUE="2">2</OPTION>
                                          <OPTION VALUE="3">3</OPTION>
                                          <OPTION VALUE="4">4</OPTION>
                                          <OPTION VALUE="5">5</OPTION>
                                          <OPTION VALUE="6">6</OPTION>
                                          <OPTION VALUE="7">7</OPTION>
                                        </select>
                                      </td>
                                    </xsl:when>
                                    <xsl:when test="root/pagedata/edit_mode = 'display'">
                                      <td>
                                        <xsl:value-of select="//vendor/lead_days"/>
                                      </td>
                                    </xsl:when>
                                  </xsl:choose>
                                  <td class="ErrorMessage">
                                    <xsl:value-of select="//error/default_carrier"/>
                                    <xsl:value-of select="//error/lead_days"/>
                                  </td>
                                </tr>
                              </table>
                            </td>

                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Cutoff Time:
                  </td>
                            <td>
                              <table>
                                <tr>
                                  <td>
                                    <select name="cutoff" id="cutoff" tabindex="3" onChange="doOnChangeAction();" disabled="true">
                                      <OPTION VALUE="00:00">00:00</OPTION>
                                      <OPTION VALUE="00:30">00:30</OPTION>
                                      <OPTION VALUE="01:00">01:00</OPTION>
                                      <OPTION VALUE="01:30">01:30</OPTION>
                                      <OPTION VALUE="02:00">02:00</OPTION>
                                      <OPTION VALUE="02:30">02:30</OPTION>
                                      <OPTION VALUE="03:00">03:00</OPTION>
                                      <OPTION VALUE="03:30">03:30</OPTION>
                                      <OPTION VALUE="04:00">04:00</OPTION>
                                      <OPTION VALUE="04:30">04:30</OPTION>
                                      <OPTION VALUE="05:00">05:00</OPTION>
                                      <OPTION VALUE="05:30">05:30</OPTION>
                                      <OPTION VALUE="06:00">06:00</OPTION>
                                      <OPTION VALUE="06:30">06:30</OPTION>
                                      <OPTION VALUE="07:00">07:00</OPTION>
                                      <OPTION VALUE="07:30">07:30</OPTION>
                                      <OPTION VALUE="08:00">08:00</OPTION>
                                      <OPTION VALUE="08:30">08:30</OPTION>
                                      <OPTION VALUE="09:00">09:00</OPTION>
                                      <OPTION VALUE="09:30">09:30</OPTION>
                                      <OPTION VALUE="10:00">10:00</OPTION>
                                      <OPTION VALUE="10:30">10:30</OPTION>
                                      <OPTION VALUE="11:00">11:00</OPTION>
                                      <OPTION VALUE="11:30">11:30</OPTION>
                                      <OPTION VALUE="12:00">12:00</OPTION>
                                      <OPTION VALUE="12:30">12:30</OPTION>
                                      <OPTION VALUE="13:00">13:00</OPTION>
                                      <OPTION VALUE="13:30">13:30</OPTION>
                                      <OPTION VALUE="14:00">14:00</OPTION>
                                      <OPTION VALUE="14:30">14:30</OPTION>
                                      <OPTION VALUE="15:00">15:00</OPTION>
                                      <OPTION VALUE="15:30">15:30</OPTION>
                                      <OPTION VALUE="16:00">16:00</OPTION>
                                      <OPTION VALUE="16:30">16:30</OPTION>
                                      <OPTION VALUE="17:00">17:00</OPTION>
                                      <OPTION VALUE="17:30">17:30</OPTION>
                                      <OPTION VALUE="18:00">18:00</OPTION>
                                      <OPTION VALUE="18:30">18:30</OPTION>
                                      <OPTION VALUE="19:00">19:00</OPTION>
                                      <OPTION VALUE="19:30">19:30</OPTION>
                                      <OPTION VALUE="20:00">20:00</OPTION>
                                      <OPTION VALUE="20:30">20:30</OPTION>
                                      <OPTION VALUE="21:00">21:00</OPTION>
                                      <OPTION VALUE="21:30">21:30</OPTION>
                                      <OPTION VALUE="22:00">22:00</OPTION>
                                      <OPTION VALUE="22:30">22:30</OPTION>
                                      <OPTION VALUE="23:00">23:00</OPTION>
                                      <OPTION VALUE="23:30">23:30</OPTION>
                                    </select>
                                  </td>
                                  <td class="Label">
                                    <div id="nonSDSCutoffDiv">
                                      Cutoff Time not available for Vendor Type
                                    </div>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td class="label" align="right">
                        Company Code:
                      </td>
                            <td>
                              <table>
                                <tr>
                                  <xsl:choose>
                                    <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                      <td>
                                        <xsl:variable name="company"
                                                      select="//vendor/company"/>
                                        <select name="company" id="company"
                                                tabindex="19">
                                          <xsl:for-each select="//COMPANY">
                                            <xsl:choose>
                                              <xsl:when test="companyid = $company or (string-length($company) = 0 and companyid = 'FTD')">
                                                <option value="{companyid}"
                                                        selected="selected">
                                                  <xsl:value-of select="companyid"/>
                                                </option>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <option value="{companyid}">
                                                  <xsl:value-of select="companyid"/>
                                                </option>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:for-each>
                                        </select>
                                      </td>
                                    </xsl:when>
                                    <xsl:when test="root/pagedata/edit_mode = 'display'">
                                      <td>
                                        <xsl:value-of select="//vendor/company"/>
                                      </td>
                                    </xsl:when>
                                  </xsl:choose>
                                  <td class="ErrorMessage">
                                    <xsl:value-of select="//error/company"/>
                                  </td>
                                </tr>
                              </table>
                            </td>

                           </tr>
                          <tr>
                            <td class="label" align="right">
                    Address 1:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="address1"
                                               size="45" maxLength="45"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="4"
                                               value="{//vendor/address1}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/address1"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/address1"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                            <td class="label" align="right">
                            Vendor Type:
                      </td>
                            <td width="30%">
                              <table>
                                <tr>
                                  <xsl:choose>
                                    <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                      <td>
                                        <xsl:variable name="vendor_type"
                                                      select="//vendor/vendor_type"/>
                                        <select name="vendor_type"
                                                id="vendor_type" tabindex="20" onChange="doVendorTypeChange(this.value, false);">
                                          <xsl:for-each select="//vendor_type">
                                            <xsl:choose>
                                              <xsl:when test="vendor_code = $vendor_type or (string-length($vendor_type) = 0 and vendor_code = 'ESCALATE')">
                                                <option value="{vendor_code}"
                                                        selected="selected">
                                                  <xsl:value-of select="display_name"/>
                                                </option>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <option value="{vendor_code}">
                                                  <xsl:value-of select="display_name"/>
                                                </option>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:for-each>
                                        </select>
                                      </td>
                                    </xsl:when>
                                    <xsl:when test="root/pagedata/edit_mode = 'display'">
                                     <td>
                                        <xsl:variable name="vendorType"
                                                      select="//vendor/vendor_type"/>
                                        <xsl:value-of select="//vendor_types/vendor_type[vendor_code = $vendorType]/display_name "/>
                                        <label id="vendor_type" name="vendor_type" value="{//vendor_types/vendor_type[vendor_code = $vendorType]/vendor_code}"/>
                                      </td>
                                    </xsl:when>
                                  </xsl:choose>
                                  <td class="ErrorMessage">
                                <xsl:value-of select="//error/vendor_type"/>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Address 2:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="address2"
                                               size="45" maxLength="45"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="5"
                                               value="{//vendor/address2}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/address2"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/address2"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                            <td class="label" align="right">
                        Accounts Payable Id:
                      </td>
                            <td>
                              <table>
                                <tr>
                                  <xsl:choose>
                                    <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                      <td>
                                          <input type="text" name="accounts_payable_id"
                                                 size="8" maxLength="8"
                                                 tabindex="21"
                                                 onchange="doOnChangeAction()"
                                                 onkeypress="doCheckOnKeyDown()"
                                                 value="{//vendor/accounts_payable_id}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/accounts_payable_id"/>
                                      </td>
                                    </xsl:when>
                                    <xsl:when test="root/pagedata/edit_mode = 'display'">
                                      <td>
                                        <xsl:value-of select="//vendor/accounts_payable_id"/>
                                      </td>
                                    </xsl:when>
                                  </xsl:choose>
                                </tr>
                              </table>
                            </td>

                          </tr>
                          <tr>
                            <td class="label" align="right">
                    City:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="city" size="30"
                                               maxLength="30"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="6"
                                               value="{//vendor/city}"/>
                                        <b>State:</b>
                                        <xsl:variable name="state"
                                                      select="//vendor/state"/>
                                        <select name="state" id="state"
                                                tabindex="7">
                                          <option value="" selected="selected"/>
                                          <xsl:for-each select="//STATE[countrycode='']">
                                            <xsl:choose>
                                              <xsl:when test="statemasterid = $state">
                                                <option value="{statemasterid}"
                                                        selected="selected">
                                                  <xsl:value-of select="statemasterid"/>
                                                </option>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <option value="{statemasterid}">
                                                  <xsl:value-of select="statemasterid"/>
                                                </option>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:for-each>
                                        </select>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/city"/>
                                        <xsl:value-of select="//error/state"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/city"/>
                                  <xsl:if test="//vendor/city != ''  and //vendor/state != ''">,&#160;</xsl:if>
                                  <xsl:value-of select="//vendor/state"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                            <td class="label" align="right">
                          GL Account Number:
                      </td>
                            <td>
                              <table>
                                <tr>
                                  <xsl:choose>
                                    <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                      <td>
                                          <input type="text" name="gl_account_number"
                                                 size="30" maxLength="30"
                                                 tabindex="21"
                                                 onchange="doOnChangeAction()"
                                                 onkeypress="doCheckOnKeyDown()"
                                                 value="{//vendor/gl_account_number}"/>
                                      </td>
                                    </xsl:when>
                                    <xsl:when test="root/pagedata/edit_mode = 'display'">
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/gl_account_number"/>
                                      </td>
                                     <td>
                                        <xsl:value-of select="//vendor/gl_account_number"/>
                                      </td>
                                    </xsl:when>
                                  </xsl:choose>
                                  <td class="ErrorMessage">
                                    <xsl:value-of select="//error/gl_account_number"/>
                                  </td>
                                </tr>
                              </table>
                            </td>

                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Zip/Postal:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="zipcode" size="9"
                                               maxLength="12"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="8"
                                               value="{//vendor/zip_code}"/>
                                        <a id="cityLookupLink" href="#"
                                           tabindex="9" class="textlink"
                                           onclick="javascript:doCityLookup();">Lookup by City</a>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/zipcode"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/zip_code"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                            <td rowspan="5" colspan="2" width="50%" valign="top">
                              <table border="0" width="100%">
                                  <xsl:variable name="edit_mode" select="root/pagedata/edit_mode"/>
                                  <xsl:for-each select="//partnerShippingAccounts/partnerShippingAccounts">
                                      <tr>
                                          <td class="label" align="right" width="40%">
                                              <xsl:value-of select="partner_id"/>&#160;Billing Account ID:
                                              <input type="hidden" name="psa_partner_id" value="{partner_id}"/>
                                              <input type="hidden" name="psa_carrier_id" value="{carrier_id}"/>
                                          </td>
                                          <td width="30%">
                                              <input type="hidden" name="psa_validation_regex" value="{validation_regex}"/>
                                              <xsl:choose>
                                                  <xsl:when test="$edit_mode = 'display'">
                                                      <input type="hidden" name="psa_account_num" value="{account_num}"/>
                                                      <xsl:value-of select="account_num"/>
                                                  </xsl:when>
                                                  <xsl:when test="$edit_mode = 'edit' or $edit_mode = 'new'">
                                                  <input type="text" name="psa_account_num"
                                                         size="10" maxLength="10"
                                                         tabindex="22"
                                                         onchange="doOnChangeAction()"
                                                         onkeypress="doCheckOnKeyDown()"
                                                         value="{account_num}"/>
                                                  </xsl:when>
                                              </xsl:choose>
                                          </td>
                                          <td class="ErrorMessage" width="30%">
                                              <xsl:variable name="counter" select="position()"/>
                                              <xsl:value-of select="//error[errPosition=$counter]/psaError"/>
                                          </td>
                                      </tr>
                                  </xsl:for-each>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                                   Contact Name:
                            </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="main_name"
                                               size="50" maxLength="50"
                                               tabindex="10"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               value="{//maincontact/contact_name}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/main_name"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//maincontact/contact_name"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Main Phone:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="main_phone"
                                               size="12" tabindex="11"
                                               maxLength="12"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()">
                                          <xsl:attribute name="value">
                                            <xsl:call-template name="formatPhoneNumber">
                                              <xsl:with-param name="phoneNumber"
                                                              select="//maincontact/phone"/>
                                            </xsl:call-template>
                                          </xsl:attribute>
                                        </input>
                                        <b>&#160;Ext:&#160;</b>
                                        <input type="text" name="main_phone_ext"
                                               size="10" tabindex="12"
                                               maxLength="10"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               value="{//maincontact/phone_extension}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/main_phone"/>
                                        <xsl:value-of select="//error/main_phone_extension"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:call-template name="formatPhoneNumber">
                                    <xsl:with-param name="phoneNumber"
                                                    select="//maincontact/phone"/>
                                  </xsl:call-template>
                                  <b>&#160;Ext:&#160;</b>
                                  <xsl:value-of select="//maincontact/phone_extension"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Main Fax:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="main_fax"
                                               size="12" maxLength="12"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="13">
                                          <xsl:attribute name="value">
                                            <xsl:call-template name="formatPhoneNumber">
                                              <xsl:with-param name="phoneNumber"
                                                              select="//maincontact/fax"/>
                                            </xsl:call-template>
                                          </xsl:attribute>
                                        </input>
                                        <b>&#160;Ext:&#160;</b>
                                        <input type="text" name="main_fax_ext"
                                               size="10"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="14" maxLength="10"
                                               value="{//maincontact/fax_extension}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/main_fax"/>
                                        <xsl:value-of select="//error/main_fax_extension"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:call-template name="formatPhoneNumber">
                                    <xsl:with-param name="phoneNumber"
                                                    select="//maincontact/fax"/>
                                  </xsl:call-template>
                                  <b>&#160;Ext:&#160;</b>
                                  <xsl:value-of select="//maincontact/fax_extension"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Email Address:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="main_email"
                                               size="55" maxLength="55"
                                               tabindex="15"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               value="{//maincontact/email}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/main_email"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//maincontact/email"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                              Zone Jump Eligible:
                </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                          <input type="checkbox" tabindex="16" name="zone_jump_eligible_flag" id="zone_jump_eligible_flag">
                                            <xsl:if test="$zoneJumpable = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                                          </input>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/zone_jump_eligible_flag"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                           <tr>
                            <td class="label" align="right">
                              New Shipping System:
                			</td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                          <input type="checkbox" tabindex="17" name="new_shipping_system" id="new_shipping_system">
                                            <xsl:if test="$newShippingSystem = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                                          </input>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//vendor/new_shipping_system"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>


                          <!--******************************-->
                          <!--**** Add-On Changes Begin ****-->
                          <!--******************************-->
                          <tr>
                            <td align="center" class="TotalLine" colspan="4">
                              <div align="center">
                                <strong>Add-on &amp; Vase Availability Options</strong>
                                <br></br>
                              </div>
                              <div align="left"></div>
                            </td>
                          </tr>
                          <tr>
                            <td align = "left" colspan = "4" class="ErrorMessage">
                              <xsl:for-each select="//error/add_on_options">
                                <xsl:value-of select="text()" disable-output-escaping="yes"/><br/>
                              </xsl:for-each>
                            </td>
                          </tr>
                          <tr>
                            <!--******** Begin Add-On Column ********-->
                            <td align = "center" colspan = "2">
                              <div style = "height : 150px; overflow : auto; border-style: solid; border-color: #006699; border-width: 2px; text-align : left;">
                                <table width = "100%">

                                  <!-- Add-On Header -->
                                  <tr width = "100%">
                                    <xsl:choose>
                                      <xsl:when test="/root/pagedata/edit_mode = 'edit' or /root/pagedata/edit_mode = 'new'">
                                        <td class="label" align="center">Assigned</td>
                                      </xsl:when>
                                    </xsl:choose>
                                    <td class="label" align="left">Add-on</td>
                                    <td class="label" align="center">Vendor SKU</td>
                                    <td class="label" align="center">Vendor Cost</td>
                                  </tr>

                                  <!-- Add-On Details -->
                                  <xsl:for-each select="root/add_ons/add_on/AddOnVO">
                                    <xsl:variable name="vendorCost" select="format-number(VendorAddOnVO/cost, '0.00', 'notANumber')" /> 
                                    <xsl:variable name="vendorCostFormatted" select="format-number(VendorAddOnVO/cost, '#,###,##0.00', 'notANumber')" />
                                    <xsl:choose>

                                      <xsl:when test="/root/pagedata/edit_mode = 'edit' or /root/pagedata/edit_mode = 'new'">
                                        <tr width = "100%">
                                          <!--Assigned-->
                                          <td align="center">
                                            <input name="vendor_add_on_assignment" id="vendor_add_on_assignment" tabindex="23" onclick="doOnChangeAction()" value="{addOnId}" type="checkbox">
                                              <xsl:if test="VendorAddOnVO/assignedFlag = 'true'"><xsl:attribute name="CHECKED"/></xsl:if>
                                              <xsl:if test="addOnAvailableFlag = 'false'"><xsl:attribute name="DISABLED"/></xsl:if>
                                            </input>
                                          </td>

                                          <!--Add-On-->
                                          <td align="left">
                                            <xsl:value-of select="addOnId"/>&nbsp;-&nbsp;<xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>
                                          </td>

                                          <!--Vendor SKU-->
                                          <td align="center">
                                            <input type="text" name="vendor_add_on_sku" id="vendor_add_on_sku_{position()}" size="8" maxLength="25" tabindex="23" onchange="doOnChangeAction()" onkeypress="doCheckOnKeyDown()" value="{VendorAddOnVO/sKU}">
                                              <xsl:if test="addOnAvailableFlag = 'false'"><xsl:attribute name="DISABLED"/></xsl:if>
                                            </input>
                                          </td>

                                          <!--Vendor Cost-->
                                          <td align="center">
                                           $<input type="text" name="vendor_add_on_cost" id="vendor_add_on_cost_{position()}" size="6" maxLength="6" tabindex="23" onchange="doOnChangeAction()" onkeypress="doCheckOnKeyDown()" value="{$vendorCost}">
                                              <xsl:if test="addOnAvailableFlag = 'false'"><xsl:attribute name="DISABLED"/></xsl:if>
                                            </input>   
                                          </td>

                                          <input name="add_on_id"                 id="add_on_id"                  value="{addOnId}"            type="hidden"/>
                                          <input name="add_on_available_flag"     id="add_on_available_flag"      value="{addOnAvailableFlag}" type="hidden"/>
                                          <input name="add_on_description"        id="add_on_description"         value="{addOnDescription}"   type="hidden"/>                                                            
                                          <input name="add_on_type_id"            id="add_on_type_id"             value="{addOnTypeId}"        type="hidden"/>            

                                        </tr>
                                      </xsl:when>
                                      
                                      <xsl:otherwise>
                                        <xsl:if test="VendorAddOnVO/assignedFlag = 'true'">
                                          <tr width = "100%">
                                            <!--Add-On-->
                                            <td align="left">
                                              <xsl:value-of select="addOnId"/>&nbsp;-&nbsp;<xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>
                                            </td>

                                            <!--Vendor SKU-->
                                            <td align="center">
                                              <xsl:value-of select="VendorAddOnVO/sKU"/>
                                            </td>

                                            <!--Vendor Cost-->
                                            <td align="center">
                                              <xsl:value-of select="$vendorCostFormatted"/>
                                            </td>
                                          </tr>
                                        </xsl:if>
                                      </xsl:otherwise>

                                    </xsl:choose>
                                  </xsl:for-each>      
                                </table>
                              </div>
                            </td>
                            <!--******** End AddOn Column ********-->


                            <!--******** Begin Vase Column ********-->
                            <td align = "center" colspan = "2">
                              <div style = "height : 150px; overflow : auto; border-style: solid; border-color: #006699; border-width: 2px; text-align : left;">
                                <table width = "100%">
                                  <!-- Vase Header -->
                                  <tr width = "100%">
                                    <xsl:choose>
                                      <xsl:when test="/root/pagedata/edit_mode = 'edit' or /root/pagedata/edit_mode = 'new'">
                                        <td class="label" align="center">Assigned</td>
                                      </xsl:when>
                                    </xsl:choose>
                                    <td class="label" align="left">Vase</td>
                                    <td class="label" align="center">Vendor SKU</td>
                                    <td class="label" align="center">Vendor Cost</td>
                                  </tr>

                                  <!-- Vase Details -->
                                  <xsl:for-each select="root/add_ons/vase/AddOnVO">
                                    <xsl:variable name="vendorCost" select="format-number(VendorAddOnVO/cost, '0.00', 'notANumber')" />   
                                    <xsl:variable name="vendorCostFormatted" select="format-number(VendorAddOnVO/cost, '#,###,##0.00', 'notANumber')" />
                                    <xsl:choose>

                                      <xsl:when test="/root/pagedata/edit_mode = 'edit' or /root/pagedata/edit_mode = 'new'">
                                        <tr width = "100%">
                                          <!--Assigned-->
                                          <td align="center">
                                            <input name="vendor_vase_assignment" id="vendor_vase_assignment" tabindex="23" onclick="doOnChangeAction()" value="{addOnId}" type="checkbox">
                                              <xsl:if test="VendorAddOnVO/assignedFlag = 'true'"><xsl:attribute name="CHECKED"/></xsl:if>
                                              <xsl:if test="addOnAvailableFlag = 'false'"><xsl:attribute name="DISABLED"/></xsl:if>
                                            </input>
                                          </td>

                                          <!--Vase-->
                                          <td align="left">
                                            <xsl:value-of select="addOnId"/>&nbsp;-&nbsp;<xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>
                                          </td>

                                          <!--Vendor SKU-->
                                          <td align="center">
                                            <input type="text" name="vendor_vase_sku" id="vendor_vase_sku_{position()}" size="8" maxLength="25" tabindex="23" onchange="doOnChangeAction()" onkeypress="doCheckOnKeyDown()" value="{VendorAddOnVO/sKU}">
                                              <xsl:if test="addOnAvailableFlag = 'false'"><xsl:attribute name="DISABLED"/></xsl:if>
                                            </input>   
                                          </td>

                                          <!--Vendor Cost-->
                                          <td align="center">
                                           $<input type="text" name="vendor_vase_cost" id="vendor_vase_cost_{position()}" size="6" maxLength="6" tabindex="23" onchange="doOnChangeAction()" onkeypress="doCheckOnKeyDown()" value="{$vendorCost}"> 
                                              <xsl:if test="addOnAvailableFlag = 'false'"><xsl:attribute name="DISABLED"/></xsl:if>
                                            </input>
                                          </td>

                                          <input name="vase_id"                 id="vase_id"                  value="{addOnId}"            type="hidden"/>
                                          <input name="vase_available_flag"     id="vase_available_flag"      value="{addOnAvailableFlag}" type="hidden"/>
                                          <input name="vase_description"        id="vase_description"         value="{addOnDescription}"   type="hidden"/>                                                            
                                          <input name="vase_type_id"            id="vase_type_id"             value="{addOnTypeId}"        type="hidden"/>            

                                        </tr>
                                      </xsl:when>

                                      <xsl:otherwise>
                                        <xsl:if test="VendorAddOnVO/assignedFlag = 'true'">
                                          <tr width = "100%">

                                            <!--Vase-->
                                            <td align="left">
                                              <xsl:value-of select="addOnId"/>&nbsp;-&nbsp;<xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>
                                            </td>

                                            <!--Vendor SKU-->
                                            <td align="center">
                                              <xsl:value-of select="VendorAddOnVO/sKU"/>
                                            </td>


                                            <!--Vendor Cost-->
                                            <td align="center">
                                              <xsl:value-of select="$vendorCostFormatted"/>
                                            </td>
                                          </tr>
                                        </xsl:if>
                                      </xsl:otherwise>

                                    </xsl:choose>
                                  </xsl:for-each>      
                                </table>
                              </div>
                            </td>
                            <!--******** End Vase Column ********-->
                          </tr>
                          <!--******************************-->
                          <!--***** Add-On Changes End *****-->
                          <!--******************************-->

                          <tr>
                            <td align="center" class="TotalLine" colspan="4">
                              <div align="center">
                                <strong>Alternate Contact Information</strong>
                                <br>
                                </br>
                              </div>
                              <div align="left">
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Alt Contact Name:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="alt_name"
                                               size="50" maxLength="50"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="23"
                                               value="{//altcontact/contact_name}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/alt_name"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//altcontact/contact_name"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                            <td class="label" align="right">
                    Alt. Phone:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="alt_phone"
                                               size="12" maxLength="12"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="25">
                                          <xsl:attribute name="value">
                                            <xsl:call-template name="formatPhoneNumber">
                                              <xsl:with-param name="phoneNumber"
                                                              select="//altcontact/phone"/>
                                            </xsl:call-template>
                                          </xsl:attribute>
                                        </input>
                                        <b>&#160;Ext:&#160;</b>
                                        <input type="text" name="alt_ext"
                                               size="10" maxLength="10"
                                               tabindex="25"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               value="{//altcontact/phone_extension}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/alt_phone"/>
                                        <xsl:value-of select="//error/alt_phone_extension"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:call-template name="formatPhoneNumber">
                                    <xsl:with-param name="phoneNumber"
                                                    select="//altcontact/phone"/>
                                  </xsl:call-template>
                                  <b>&#160;Ext:&#160;</b>
                                  <xsl:value-of select="//altcontact/phone_extension"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="right">
                    Alt Email Address:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="alt_email"
                                               size="55" maxLength="55"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="24"
                                               value="{//altcontact/email}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/alt_email"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:value-of select="//altcontact/email"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                            <td class="label" align="right">
                    Alt. Fax:
                  </td>
                            <td>
                              <xsl:choose>
                                <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                  <table>
                                    <tr>
                                      <td>
                                        <input type="text" name="alt_fax"
                                               size="12" maxLength="12"
                                               tabindex="27"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()">
                                          <xsl:attribute name="value">
                                            <xsl:call-template name="formatPhoneNumber">
                                              <xsl:with-param name="phoneNumber"
                                                              select="//altcontact/fax"/>
                                            </xsl:call-template>
                                          </xsl:attribute>
                                        </input>
                                        <b>&#160;Ext:&#160;</b>
                                        <input type="text" name="alt_fax_ext"
                                               size="10" maxLength="10"
                                               onchange="doOnChangeAction()"
                                               onkeypress="doCheckOnKeyDown()"
                                               tabindex="28"
                                               value="{//altcontact/fax_extension}"/>
                                      </td>
                                      <td class="ErrorMessage">
                                        <xsl:value-of select="//error/alt_fax"/>
                                        <xsl:value-of select="//error/alt_fax_extension"/>
                                      </td>
                                    </tr>
                                  </table>
                                </xsl:when>
                                <xsl:when test="root/pagedata/edit_mode = 'display'">
                                  <xsl:call-template name="formatPhoneNumber">
                                    <xsl:with-param name="phoneNumber"
                                                    select="//altcontact/fax"/>
                                  </xsl:call-template>
                                  <b>&#160;Ext:&#160;</b>
                                  <xsl:value-of select="//altcontact/fax_extension"/>
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" class="TotalLine" colspan="4">
                              <div align="center">
                                <strong>Vendor Shipping Detail</strong>
                                <br>
                                </br>
                              </div>
                              <div align="left">
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" colspan="4">
                              <table>
                                <tr>
                                  <td width="70%">
                                    <table>
                                      <tr>
                                        <td class="label" align="right">
                        General Information:
                      </td>
                                        <td colspan="2">
                                          <xsl:choose>
                                            <xsl:when test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                                              <table>
                                                <tr>
                                                  <td>
                                                    <textarea name="general_info"
                                                              rows="2" cols="125"
                                                              wrap="virtual"
                                                              tabindex="29"
                                                              onblur="javascript:limitTextarea(this, '256')"
                                                              onkeypress="javascript:limitTextarea(this, '256')">
                                                      <xsl:value-of select="//vendor/general_info"/>
                                                    </textarea>
                                                    <td class="ErrorMessage">
                                                      <xsl:value-of select="//error/general_info"/>
                                                    </td>
                                                  </td>
                                                </tr>
                                              </table>
                                            </xsl:when>
                                            <xsl:when test="root/pagedata/edit_mode = 'display'">
                                              <xsl:value-of select="//vendor/general_info"/>
                                            </xsl:when>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="label" align="right">
                        Vendor Block Dates:
                      </td>
                                        <td colspan="3">
                                          <xsl:for-each select="//vendor_block">
                                            <xsl:variable name="startDateVar">
                                              <xsl:call-template name="formatDate">
                                                <xsl:with-param name="dateTimeString"
                                                                select="start_date"/>
                                              </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:variable name="editMode"
                                                          select="//pagedata/edit_mode"/>
                                            <xsl:variable name="blockType"
                                                          select="block_type"/>
                                            <xsl:choose>
                                              <xsl:when test="$editMode = 'edit' or $editMode = 'new'">
                                                <xsl:value-of select="$startDateVar"/>
  &#160;-&#160;
                                                <xsl:value-of select="block_type"/>
   &#160;


                                                <xsl:choose>
                                                  <xsl:when test="//can_block_vendor = 'Y'">
                                                    <a href="#"
                                                       onclick="deleteVendorBlock('{$startDateVar}','{$blockType}');">[Delete]</a>
  ;
                           </xsl:when>
                                                </xsl:choose>

                                      ;



                                              </xsl:when>
                                              <xsl:when test="$editMode = 'display'">
                                                <xsl:value-of select="$startDateVar"/>
  &#160;-&#160;
                                                <xsl:value-of select="block_type"/>
  ;
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="$editMode"/>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:for-each>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="label" align="right">
                        Global Block Dates:
                      </td>
                                        <td colspan="3">
                                          <xsl:for-each select="//global_block">
                                            <xsl:variable name="startDateVar">
                                              <xsl:call-template name="formatDate">
                                                <xsl:with-param name="dateTimeString"
                                                                select="start_date"/>
                                              </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:value-of select="$startDateVar"/>




  -





                                            <xsl:value-of select="block_type"/>





                          ;
                        </xsl:for-each>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="label" align="right">
                        Products:
                      </td>
                                        <td colspan="3">
                                          <xsl:choose>
                                              <xsl:when test="root/pagedata/edit_mode = 'new'">
                                                  <label tabIndex="34">View product list</label>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                  <a href="#"
                                                     onclick="javascript: displayProductList()"
                                                     tabIndex="34">View product list</a>
                                              </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="label" align="right"
                                            valign="top">
                                          <div id="venusCarrierLinkDiv">
                                            <a id="editCarrierLink" href="#"
                                              tabindex="9" class="textlink"
                                              onclick="javascript:editCarrier();">Carrier:</a>
                                          </div>
                                          <div id="carrierLabDiv">
                                            Carrier:
                                          </div>
                                        </td>
                                        <td>
                                          <div id="venusCarrierDiv">
                                            <table style="border-style: solid; border-color: #006699; border-width: 2px;">
                                              <xsl:for-each select="root/shipping_keys/shipping_key">
                                                <tr>
                                                  <td>
                                                    <xsl:value-of select="carrier_name"/>
                                                  </td>
                                                  <td align="right">
                                                    <xsl:value-of select="usage_percent"/>%
                                                  </td>
                                                </tr>
                                              </xsl:for-each>
                                            </table>
                                          </div>
                                          <div id="sdsCarrierDiv">
                                            <table style="border-style: solid; border-color: #006699; border-width: 2px;">
                                              <tr>
                                                <td>
                                                  <select id="active_carriers" name="active_carriers" multiple="true" onChange="doCheckOnKeyDown();" disabled="true">
                                                    <xsl:for-each select="root/carriers/carrier">
                                                        <option value="{carrier_id}"><xsl:value-of select="carrier_name"/></option>
                                                    </xsl:for-each>
                                                  </select>
                                                  <select id="active_carriers_all" name="active_carriers_all" multiple="true" style="display:none">
                                                    <xsl:for-each select="root/carriers/carrier">
                                                      <option selected="selected" value="{carrier_id}"><xsl:value-of select="carrier_name"/></option>
                                                    </xsl:for-each>
                                                  </select>
                                                  <select id="shipping_keys" name="shipping_keys" multiple="true" style="display:none">
                                                    <xsl:for-each select="root/shipping_keys/shipping_key">
                                                        <xsl:if test="usage_percent = '-1'">
                                                          <option value="{carrier_id}"></option>
                                                        </xsl:if>
                                                    </xsl:for-each>
                                                  </select>
                                                  <select id="selected_carriers" name="selected_carriers" multiple="true" style="display:none">
                                                    <xsl:for-each select="root/selected_carriers/selected_carrier">
                                                      <option value="{carrier_id}"></option>
                                                    </xsl:for-each>
                                                  </select>
                                                </td>
                                              </tr>
                                            </table>
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td>
                                    <xsl:choose>
                                      <xsl:when test="root/pagedata/edit_mode = 'display'">
                                        <table>
                                          <tr>
                                            <td class="label" align="right">
                                      Vendor Block Date:
                                    </td>
                                            <td align="left">
                                              <table>
                                                <tr>
                                                  <td>
                                                    <input type="text"
                                                           id="vendor_block_date"
                                                           name="vendor_block_date"
                                                           maxLength="10"
                                                           onchange="doOnChangeAction()"
                                                           onkeypress="doCheckOnKeyDown()"
                                                           size="13" tabindex="30"/>
                                                    <img id="vendor_date_img"
                                                         src="images\calendar.gif"
                                                         tabIndex="31"/>
                                                  </td>
                                                  <td class="ErrorMessage">
                                                    <xsl:value-of select="//error/block_error"/>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td class="label" align="right">
                                      Block Type:
                                    </td>
                                            <td align="left">
                                              <SELECT NAME="vendor_block_type"
                                                      tabindex="32">
                                                <OPTION VALUE="Shipping">Shipping</OPTION>
                                                <OPTION VALUE="Delivery">Delivery</OPTION>
                                                <OPTION VALUE="Both">Both</OPTION>
                                              </SELECT>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                            </td>
                                            <td>
                                              <xsl:choose>
                                                <xsl:when test="//can_block_vendor = 'Y'">
                                                  <button class="bigBlueButton"
                                                          style="width:80;"
                                                          accesskey="A"
                                                          tabindex="33"
                                                          onclick="addVendorBlock()">(A)dd Block
                                                    <br>
                                                    </br>
  Date </button>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <button class="bigBlueButton"
                                                          style="width:80;"
                                                          accesskey="A"
                                                          disabled="true"
                                                          tabindex="33"
                                                          onclick="addVendorBlock()">(A)dd Block
                                                    <br>
                                                    </br>
  Date </button>
                                                </xsl:otherwise>
                                              </xsl:choose>
                                            </td>
                                          </tr>
                                        </table>
                                      </xsl:when>
                                    </xsl:choose>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <xsl:variable name="editMode"
                                        select="//pagedata/edit_mode"/>
                          <xsl:if test="root/pagedata/edit_mode = 'edit' or root/pagedata/edit_mode = 'new'">
                            <tr>
                              <td align="right" valign="top" colspan="4">
                                <button class="BlueButton" style="width:80;"
                                        tabindex="35" accesskey="S"
                                        onclick="javascript:saveVendor('{$editMode}')">(S)ave </button>
                              </td>
                            </tr>
                          </xsl:if>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="98%" border="0" align="center" cellpadding="0"
                   cellspacing="1">
              <tr>
                <td>
                  <table>
                    <tr>
                      <td align="left">
                        <xsl:choose>
                          <xsl:when test="root/pagedata/edit_mode = 'edit'">
                            <xsl:choose>
                              <xsl:when test="//can_delete_vendor = 'Y'"/>
                              <xsl:otherwise>
                                <button class="bigBlueButton" style="width:80;"
                                        name="addNew" tabindex="36" accesskey="D"
                                        disabled="true"
                                        onclick="javascript:deleteVendor();">(D)elete
                                  <br>
                                  </br>
  Vendor</button>
                              </xsl:otherwise>
                            </xsl:choose>
                            <button class="bigBlueButton" style="width:80;"
                                    name="addNew" tabindex="37" accesskey="V"
                                    onclick="javascript: displayComments('N','Y')">(V)iew




                              <br>
                              </br>




  Comments</button>
                          </xsl:when>
                          <xsl:when test="root/pagedata/edit_mode = 'display'">
                            <xsl:choose>
                              <xsl:when test="//can_edit_vendor = 'Y'">
                                <button class="bigBlueButton" style="width:80;"
                                        name="addNew" tabindex="38" accesskey="E"
                                        onclick="javascript:editVendor();">(E)dit
                                  <br>
                                  </br>
  Vendor</button>
                              </xsl:when>
                              <xsl:otherwise>
                                <button class="bigBlueButton" style="width:80;"
                                        name="addNew" tabindex="38" accesskey="E"
                                        disabled="true"
                                        onclick="javascript:editVendor();">(E)dit
                                  <br>
                                  </br>
  Vendor</button>
                              </xsl:otherwise>
                            </xsl:choose>
                            <button class="bigBlueButton" style="width:80;"
                                    name="addNew" tabindex="39" accesskey="V"
                                    onclick="displayComments('N','Y');">(V)iew




                              <br>
                              </br>




  Comments</button>
                          </xsl:when>
                          <xsl:when test="root/pagedata/edit_mode = 'new'">
                          </xsl:when>
                        </xsl:choose>
                      </td>
                    </tr>
                  </table>
                </td>
                <td align="right">
                  <table>
                    <tr>
                      <td align="right" valign="top">
                        <button class="BlueButton" style="width:80;" accesskey="B"
                                name="back" tabindex="40"
                                onclick="javascript:goBack()">(B)ack </button>
                      </td>
                    </tr>
                    <tr>
                      <td align="right" valign="top">
                        <button class="BlueButton" style="width:80;" accesskey="M"
                                name="mainMenu" tabindex="41"
                                onclick="javascript:clickMainMenu()">(M)ain Menu </button>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </div>

          <!-- Used to dislay Processing... message to user -->
          <div id="waitDiv" style="display:none">
             <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
                <tr>
                  <td width="100%">
                    <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                     <tr>
                        <td>
                           <table width="100%" cellspacing="0" cellpadding="0" border="0">
                               <tr>
                                 <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                                 <td id="waitTD"  width="50%" class="waitMessage"></td>
                               </tr>
                           </table>
                         </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
          </div>
        

          <xsl:call-template name="footer"/>
          <script type="text/javascript">
            var editMode = document.getElementById("edit_mode").value;
            if (editMode == "display")
            {
              Calendar.setup(
                {
                  inputField  : "vendor_block_date",  // ID of the input field
                  ifFormat    : "mm/dd/y",  // the date format
                  button      : "vendor_date_img"  // ID of the button
                }
              );
            }
          </script>
        </form>
        <iframe id="checkLock" width="0px" height="0px" border="0"/>
      </body>
    </html>
  </xsl:template>
  <xsl:template name="addHeader">
    <xsl:call-template name="header">
      <xsl:with-param name="showBackButton" select="true()"/>
      <xsl:with-param name="showPrinter" select="true()"/>
      <xsl:with-param name="showSearchBox" select="false()"/>
      <xsl:with-param name="searchLabel" select="'Order #'"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
    </xsl:call-template>
  </xsl:template>
</xsl:stylesheet>
