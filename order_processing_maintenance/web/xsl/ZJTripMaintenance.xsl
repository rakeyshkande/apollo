<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<xsl:decimal-format NaN="0.00" name="notANumber"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->
<xsl:variable name="ORIGINAL_ACTION_TYPE" select="key('pageData', 'original_action_type')/value"/>
<xsl:variable name="OK_ERROR_MESSAGE" select="key('pageData', 'OK_ERROR_MESSAGE')/value"/>
<xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="OK" select="'O'"/>
<xsl:variable name="CONFIRM" select="'C'"/>
<xsl:variable name="ORIGINAL_ACTUAL_TRIP_STATUS_CODE" select="root/TRIPS/TRIP/actual_trip_status_code"/>
<xsl:variable name="ORIGINAL_VIRTUAL_TRIP_STATUS_CODE" select="root/TRIPS/TRIP/virtual_trip_status_code"/>


<xsl:template match="/root">


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Zone Jump Maintenance</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>
    <link rel="stylesheet" type="text/css" href="css/calendar.css"></link>

    <script type="text/javascript" src="js/ZJTrip.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/util.js"/>
    <script type="text/javascript" src="js/calendar.js"></script>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script type="text/javascript" language="javascript">

//****************************************************************************************************
//*  values
//****************************************************************************************************
var serverErrorMessage = '<xsl:value-of select="key('pageData', 'OK_ERROR_MESSAGE')/value"/>';

var maxPallets = <xsl:value-of select="key('pageData','max_pallets')/value"/>;

var originalActionType = '<xsl:value-of select="key('pageData','original_action_type')/value"/>';

var originalDeliveryDateValue =  '';
    originalDeliveryDateValue += '<xsl:value-of select="substring(TRIPS/TRIP/delivery_date, 1, 4)"/>';
    originalDeliveryDateValue += '<xsl:value-of select="substring(TRIPS/TRIP/delivery_date, 6, 2)"/>';
    originalDeliveryDateValue += '<xsl:value-of select="substring(TRIPS/TRIP/delivery_date, 9, 2)"/>';

var originalDepartureDateValue =  '';
    originalDepartureDateValue += '<xsl:value-of select="substring(TRIPS/TRIP/departure_date, 1, 4)"/>';
    originalDepartureDateValue += '<xsl:value-of select="substring(TRIPS/TRIP/departure_date, 6, 2)"/>';
    originalDepartureDateValue += '<xsl:value-of select="substring(TRIPS/TRIP/departure_date, 9, 2)"/>';

    </script>

    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;
var totalRecords = 0;

    ]]>

    </script>

  </head>
  <body onload="javascript: init()">
    <form name="fTruckMaintenance" method="post">
      <input type="hidden" name="securitytoken"         id="securitytoken"        value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"               id="context"              value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"           id="adminAction"           value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="original_action_type"  id="original_action_type"  value="{key('pageData', 'original_action_type')/value}"/>
      <input type="hidden" name="filter"                id="filter"                value="{key('pageData', 'filter')/value}"/>
      <input type="hidden" name="trip_id"                id="trip_id"              value="{key('pageData', 'trip_id')/value}"/>

      <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="30%" height="33" valign="top">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td>&nbsp;</td>
          <td align="right" width="30%" valign="bottom">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" valign="top" class="Header" id="pageHeader">Zone Jump Trip Maintenance</td>
          <td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
        </tr>
      </table>

      <!-- content to be hidden once the search begins -->
      <div id="content" style="display:block">
        <table width="98%" align="center" cellspacing="1" class="mainTable" border="0">
          <tr>
            <td>
              <table width="100%" align="center" class="innerTable" border="0">
                <tr>
                  <td>
                    <table width="100%" align="center" border="0">
                      <tr>
                        <td class="label" align="right">Vendor Location</td>
                        <td align="left">
                          <table>
                            <tr>
                              <td>
                                <input type="text" id="vendor_location" name="vendor_location" maxlength="25" size="25" onchange="javascript:somethingChanged=true;">
                                  <xsl:if test="TRIPS/TRIP/trip_origination_desc">

                                    <xsl:if test="(TRIPS/TRIP/tv_printed_exist = 'Y' or
                                                   TRIPS/TRIP/total_order_on_truck > 0 or
                                                   $ORIGINAL_ACTUAL_TRIP_STATUS_CODE  = 'C' or
                                                   $ORIGINAL_ACTUAL_TRIP_STATUS_CODE  = 'X'
                                                  ) and
                                                   translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT'
                                                 ">
                                      <xsl:attribute name="disabled"/>
                                    </xsl:if>

                                    <xsl:attribute name="value">
                                      <xsl:value-of select="TRIPS/TRIP/trip_origination_desc"/>
                                    </xsl:attribute>
                                  </xsl:if>
                                </input>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="label" align="right">Injection Hub</td>
                        <td align="left">
                          <table>
                            <tr>
                              <td>
                                <select name="ih_id" id="ih_id" onchange="javascript:somethingChanged=true;">
                                  <xsl:if test="(TRIPS/TRIP/tv_printed_exist = 'Y' or
                                                 TRIPS/TRIP/total_order_on_truck > 0 or
                                                 $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or
                                                 $ORIGINAL_ACTUAL_TRIP_STATUS_CODE  = 'X'
                                                ) and
                                                translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT'
                                               ">
                                    <xsl:attribute name="disabled"/>
                                  </xsl:if>
                                  <xsl:for-each select="INJECTION_HUBS/INJECTION_HUB">
                                    <option value="{injection_hub_id}" state="{state_master_id}">
                                      <xsl:if test="injection_hub_id = /root/TRIPS/TRIP/injection_hub_id">
                                        <xsl:attribute name="selected"/>
                                      </xsl:if>
                                      <xsl:value-of select="city_name"/>,&nbsp;<xsl:value-of select="state_master_id"/>&nbsp;-&nbsp;<xsl:value-of select="carrier_id"/>
                                    </option>
                                  </xsl:for-each>
                                </select>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="label" align="right">Vendor Departure Date</td>
                        <td align="left">
                          <table>
                            <tr>
                              <td>
                              <!-- disabled -->
                                <input type="text" id="departure_date" name="departure_date" maxlength="10" size="10" >
                                  <xsl:attribute name="disabled"/>
                                  <xsl:if test="(translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and
                                                 TRIPS/TRIP/departure_date) or
                                                 $OK_ERROR_MESSAGE != ''">
                                    <xsl:attribute name="value">
                                      <xsl:value-of select="substring(TRIPS/TRIP/departure_date, 1, 4)"/>/<xsl:value-of select="substring(TRIPS/TRIP/departure_date, 6, 2)"/>/<xsl:value-of select="substring(TRIPS/TRIP/departure_date, 9, 2)"/>
                                    </xsl:attribute>
                                  </xsl:if>
                                </input>
                                <xsl:if test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'ADD'  or
                                              translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'COPY' or
                                              ( translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and
                                                TRIPS/TRIP/tv_printed_exist = 'N' and
                                                TRIPS/TRIP/total_order_on_truck = 0 and
                                                $ORIGINAL_ACTUAL_TRIP_STATUS_CODE != 'C' and
                                                $ORIGINAL_ACTUAL_TRIP_STATUS_CODE != 'X'
                                              )">
                                  <img id="departure_date_img" src="images\calendar.gif"></img>
                                </xsl:if>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="label" align="right">Days in Transit</td>
                        <td align="left">
                          <select name="days_in_transit_qty" id="days_in_transit_qty" onchange="javascript:somethingChanged=true;">
                            <xsl:if test="($ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or
                                           $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X' or
                                           TRIPS/TRIP/tv_printed_exist = 'Y' or
                                           TRIPS/TRIP/total_order_on_truck > 0
                                          ) and
                                          translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT'
                                         ">
                              <xsl:attribute name="disabled"/>
                            </xsl:if>
                            <xsl:choose>
                              <xsl:when test="TRIPS/TRIP">
                                <script>
                                  var daysInTransit = <xsl:value-of select="TRIPS/TRIP/days_in_transit_qty"/>;
                                  populateDropDown(daysInTransit, 0, 1);
                                </script>
                              </xsl:when>
                              <xsl:otherwise>
                                <script>
                                  populateDropDown(0, 0, 1);
                                </script>
                              </xsl:otherwise>
                            </xsl:choose>
                          </select>
                          &nbsp;&nbsp;&nbsp;(<b>Note:</b> Vendor Departure Date + Days In Transit = Hub Injection Date)
                        </td>
                      </tr>
                      <tr>
                        <td class="label" align="right">Delivery Date</td>
                        <td align="left">
                          <table>
                            <tr>
                              <td>
                                <input type="text" id="delivery_date" name="delivery_date" maxlength="10" size="10" >
                                  <xsl:attribute name="disabled"/>
                                  <xsl:if test="(translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and
                                                 TRIPS/TRIP/delivery_date) or
                                                 $OK_ERROR_MESSAGE != ''">
                                    <xsl:attribute name="value">
                                      <xsl:value-of select="substring(TRIPS/TRIP/delivery_date, 1, 4)"/>/<xsl:value-of select="substring(TRIPS/TRIP/delivery_date, 6, 2)"/>/<xsl:value-of select="substring(TRIPS/TRIP/delivery_date, 9, 2)"/>
                                    </xsl:attribute>
                                  </xsl:if>
                                </input>
                                <xsl:if test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'ADD'  or
                                              translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'COPY' or
                                              ( translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and
                                                TRIPS/TRIP/tv_printed_exist = 'N' and
                                                TRIPS/TRIP/total_order_on_truck = 0 and
                                                $ORIGINAL_ACTUAL_TRIP_STATUS_CODE != 'C' and
                                                $ORIGINAL_ACTUAL_TRIP_STATUS_CODE != 'X'
                                              )">
                                  <img id="delivery_date_img" src="images\calendar.gif"></img>
                                </xsl:if>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="label" align="right">Total Pallets Per Truck</td>
                        <td align="left">
                          <select name="total_plt_qty" id="total_plt_qty" onchange="javascript:somethingChanged=true;">
                            <xsl:if test="($ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or
                                           $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X' or
                                           TRIPS/TRIP/tv_printed_exist = 'Y' or
                                           TRIPS/TRIP/total_order_on_truck > 0
                                          ) and
                                          translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT'
                                         ">
                              <xsl:attribute name="disabled"/>
                            </xsl:if>
                            <xsl:choose>
                              <xsl:when test="TRIPS/TRIP">
                                <script>
                                  var totalPallets = <xsl:value-of select="TRIPS/TRIP/total_plt_qty"/>;
                                  populateDropDown(totalPallets, 1, maxPallets);
                                </script>
                              </xsl:when>
                              <xsl:otherwise>
                                <script>
                                  populateDropDown(26, 1, maxPallets);
                                </script>
                              </xsl:otherwise>
                            </xsl:choose>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td class="label" align="right">Pallets Allocated to 3rd Party</td>
                        <td align="left">
                          <select name="third_party_plt_qty" id="third_party_plt_qty" onchange="javascript:somethingChanged=true;">
                            <xsl:if test="$ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X'">
                              <xsl:attribute name="disabled"/>
                            </xsl:if>
                            <xsl:choose>
                              <xsl:when test="TRIPS/TRIP">
                                <script>
                                  var thirdPartyPallets = <xsl:value-of select="TRIPS/TRIP/third_party_plt_qty"/>;
                                  populateDropDown(thirdPartyPallets, 0, maxPallets);
                                </script>
                              </xsl:when>
                              <xsl:otherwise>
                                <script>
                                  populateDropDown(0, 0, maxPallets);
                                </script>
                              </xsl:otherwise>
                            </xsl:choose>
                          </select>
                        </td>
                      </tr>
                      <xsl:if test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and TRIPS/TRIP">
                        <tr>
                          <td class="label" align="right">Zone Jump Status</td>
                          <td align="left">
                            <table>
                              <tr>
                                <td>
                                  <xsl:choose>
                                    <xsl:when test="$ORIGINAL_VIRTUAL_TRIP_STATUS_CODE != ''">
                                      <xsl:value-of select="$ORIGINAL_VIRTUAL_TRIP_STATUS_CODE"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:for-each select="/root/TRIP_STATUSES/TRIP_STATUS">
                                        <xsl:if test="$ORIGINAL_ACTUAL_TRIP_STATUS_CODE = trip_status_code">
                                          <xsl:value-of select="trip_status_desc"/>
                                        </xsl:if>
                                      </xsl:for-each>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </xsl:if>
                      <xsl:if test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and TRIPS/TRIP">
                        <tr>
                          <td class="label" align="right">Sort Code</td>
                          <td align="left">
                            <table>
                              <tr>
                                <td>
                                  Z<xsl:if test="TRIPS/TRIP/sort_code_seq &lt; 10">0</xsl:if><xsl:value-of select="TRIPS/TRIP/sort_code_seq"/>-<xsl:value-of select="TRIPS/TRIP/sort_code_state_master_id"/>-<xsl:value-of select="TRIPS/TRIP/sort_code_date_mmdd"/>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </xsl:if>
                      <tr>
                        <td class="label" align="right" valign="top">Eligible Vendors</td>
                        <td align="left" valign="bottom">
                          <table>
                            <tr>
                              <td>
                                <table width="750px" align="center" border="0">
                                  <tr>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="100px">Select</td>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="200px">Vendor Name</td>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="150px">City</td>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="50px" >State</td>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="75px" >Order Cutoff Time</td>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="75px" >Vendor Status</td>
                                    <td class="label" align="left" style="background-color:#ccccff;" width="100px">Rejected E Numbers</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="top">
                          <table>
                            <tr>
                              <td>
                                <div style="height:15em; overflow:auto;">
                                  <table width="750px" align="center" border="0">

                                  <!-- ********************************************************************** -->
                                  <!-- VENDOR SECTION - START                                                 -->
                                  <!-- ********************************************************************** -->
                                    <xsl:for-each select="TRIP_VENDORS/TRIP_VENDOR">
                                      <script>totalRecords++;</script>
                                      <xsl:variable name="ORIGINAL_TRIP_VENDOR_STATUS_CODE" select="original_trip_vendor_status_code"/>
                                      <xsl:variable name="NEW_TRIP_VENDOR_STATUS_CODE" select="new_trip_vendor_status_code"/>
                                      <tr>
                                        <td width="100px">
                                          <xsl:choose>

                                            <!-- Action type = ADD or COPY -->
                                            <xsl:when test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'ADD' or
                                                            translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'COPY'">
                                              <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                <option value=""></option>
                                                <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                  <xsl:if test="trip_vendor_status_code = 'A'">
                                                    <option value="{trip_vendor_status_code}">
                                                      <xsl:if test="$NEW_TRIP_VENDOR_STATUS_CODE = trip_vendor_status_code">
                                                        <xsl:attribute name="selected"/>
                                                      </xsl:if>
                                                      <xsl:value-of select="trip_vendor_status_desc"/>
                                                    </option>
                                                  </xsl:if>
                                                </xsl:for-each>
                                              </select>
                                            </xsl:when>


                                            <!-- Action type = EDIT -->
                                            <xsl:when test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT'">
                                              <xsl:choose>

                                                <!-- Trip status is canceled/closed -->
                                                <xsl:when test="$ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X'">
                                                  <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                    <xsl:attribute name="disabled"/>
                                                    <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                      <xsl:if test="$NEW_TRIP_VENDOR_STATUS_CODE = trip_vendor_status_code">
                                                        <option value="{trip_vendor_status_code}">
                                                          <xsl:attribute name="selected"/>
                                                          <xsl:value-of select="trip_vendor_status_desc"/>
                                                        </option>
                                                      </xsl:if>
                                                    </xsl:for-each>


                                                  </select>
                                                </xsl:when>

                                                <!-- New Vendor Status = '' i.e. vendor has not been assigned to a trip -->
                                                <xsl:when test="$NEW_TRIP_VENDOR_STATUS_CODE = '' and $ORIGINAL_TRIP_VENDOR_STATUS_CODE = ''">
                                                  <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                    <option value=""></option>
                                                    <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                      <xsl:if test="trip_vendor_status_code = 'A'">
                                                        <option value="{trip_vendor_status_code}">
                                                          <xsl:value-of select="trip_vendor_status_desc"/>
                                                        </option>
                                                      </xsl:if>
                                                    </xsl:for-each>
                                                  </select>
                                                </xsl:when>

                                                <!-- New Vendor Status != '' but Original Vendor Status = '' i.e. user tried saving, but there was an issue -->
                                                <xsl:when test="$NEW_TRIP_VENDOR_STATUS_CODE != '' and $ORIGINAL_TRIP_VENDOR_STATUS_CODE = ''">
                                                  <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                    <option value=""></option>
                                                    <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                      <xsl:if test="trip_vendor_status_code = 'A'">
                                                        <option value="{trip_vendor_status_code}">
                                                          <xsl:attribute name="selected"/>
                                                          <xsl:value-of select="trip_vendor_status_desc"/>
                                                        </option>
                                                      </xsl:if>
                                                    </xsl:for-each>
                                                  </select>
                                                </xsl:when>

                                                <!-- New Vendor Status = CANCEL, but it has not been saved -->
                                                <xsl:when test="$NEW_TRIP_VENDOR_STATUS_CODE = 'X' and $ORIGINAL_TRIP_VENDOR_STATUS_CODE != $NEW_TRIP_VENDOR_STATUS_CODE">
                                                  <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                    <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                      <option value="{trip_vendor_status_code}">
                                                        <xsl:if test="$NEW_TRIP_VENDOR_STATUS_CODE = trip_vendor_status_code">
                                                          <xsl:attribute name="selected"/>
                                                        </xsl:if>
                                                        <xsl:value-of select="trip_vendor_status_desc"/>
                                                      </option>
                                                    </xsl:for-each>
                                                  </select>
                                                </xsl:when>


                                                <!-- Current Vendor Status = CANCEL -->
                                                <xsl:when test="$NEW_TRIP_VENDOR_STATUS_CODE = 'X'  and $ORIGINAL_TRIP_VENDOR_STATUS_CODE = $NEW_TRIP_VENDOR_STATUS_CODE">
                                                  <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                    <xsl:attribute name="disabled"/>
                                                    <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                      <xsl:if test="trip_vendor_status_code = 'X'">
                                                        <option value="{trip_vendor_status_code}">
                                                          <xsl:value-of select="trip_vendor_status_desc"/>
                                                        </option>
                                                      </xsl:if>
                                                    </xsl:for-each>
                                                  </select>
                                                </xsl:when>


                                                <xsl:otherwise>
                                                  <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                    <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                      <option value="{trip_vendor_status_code}">
                                                        <xsl:if test="$NEW_TRIP_VENDOR_STATUS_CODE = trip_vendor_status_code">
                                                          <xsl:attribute name="selected"/>
                                                        </xsl:if>
                                                        <xsl:value-of select="trip_vendor_status_desc"/>
                                                      </option>
                                                    </xsl:for-each>
                                                  </select>
                                                </xsl:otherwise>

                                              </xsl:choose>
                                            </xsl:when>

                                            <!-- For all others -->
                                            <xsl:otherwise>
                                              <select name="new_vendor_status_{vendor_id}" id="new_vendor_status_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                                <option value=""></option>
                                                <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                                  <xsl:if test="trip_vendor_status_code = 'A'">
                                                    <option value="{trip_vendor_status_code}">
                                                      <xsl:value-of select="trip_vendor_status_desc"/>
                                                    </option>
                                                  </xsl:if>
                                                </xsl:for-each>
                                              </select>
                                            </xsl:otherwise>

                                          </xsl:choose>
                                        </td>
                                        <td width="200px"><xsl:value-of select="vendor_name"/></td>
                                        <td width="150px"><xsl:value-of select="city"/></td>
                                        <td width="50px" ><xsl:value-of select="state"/></td>
                                        <td width="75px" >
                                          <select name="new_vendor_cutoff_{vendor_id}" id="new_vendor_cutoff_{vendor_id}" onchange="javascript:somethingChanged=true;">
                                            <xsl:if test="$ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or
                                                          $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X' or
                                                          ($ORIGINAL_TRIP_VENDOR_STATUS_CODE = 'X' and translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT')">
                                              <xsl:attribute name="disabled"/>
                                            </xsl:if>
                                            <script>
                                              var orderCutoffTime = '<xsl:value-of select="new_order_cutoff_time"/>';
                                              populateDropDownTime(orderCutoffTime);
                                            </script>
                                          </select>
                                        </td>
                                        <td width="75px" >
                                          <xsl:for-each select="/root/TRIP_VENDOR_STATUSES/TRIP_VENDOR_STATUS">
                                            <xsl:if test="$ORIGINAL_TRIP_VENDOR_STATUS_CODE = trip_vendor_status_code">
                                              <xsl:value-of select="trip_vendor_status_desc"/>
                                            </xsl:if>
                                          </xsl:for-each>
                                        </td>


                                        <td width="100px">
                                          <xsl:choose>
                                            <xsl:when test="translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'EDIT' and translate(tvo_reject_printed_exist, $lower_case, $upper_case) = 'Y'">
                                              <textarea rows="1" cols="12">
                                                <xsl:for-each select="TRIP_VENDOR_ORDER">
                                                  <xsl:if test="translate(sds_status, $lower_case, $upper_case)  = 'PRINTED'">
                                                    <xsl:value-of select="venus_order_number"/>
                                                    <xsl:text>,&nbsp;</xsl:text>
                                                  </xsl:if>
                                                </xsl:for-each>
                                              </textarea>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              &nbsp;
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>

                                        <input type="hidden" id="line_num_{position()}" name="line_num_{position()}" value="{vendor_id}"/>
                                        <input type="hidden" id="vendor_state_{vendor_id}" name="vendor_state_{vendor_id}" value="{state}"/>
                                        <input type="hidden" id="original_vendor_status_{vendor_id}" name="original_vendor_status_{vendor_id}" value="{$ORIGINAL_TRIP_VENDOR_STATUS_CODE}"/>
                                      </tr>
                                    </xsl:for-each>

                                  <!-- ********************************************************************** -->
                                  <!-- VENDOR SECTION - END                                                   -->
                                  <!-- ********************************************************************** -->

                                  </table>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
          <tr>
            <td>
              <table>
                <tr>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bSave" accesskey="S" onclick="javascript:doSave();">
                      <xsl:if test="$ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X'">
                        <xsl:attribute name="disabled"/>
                      </xsl:if>
                      (S)ave
                    </button>
                  </td>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bCancel" accesskey="C" onclick="javascript:doCancelTrip();">
                      <xsl:if test="$ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'C' or
                                    $ORIGINAL_ACTUAL_TRIP_STATUS_CODE = 'X' or
                                    translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'ADD'  or
                                    translate($ORIGINAL_ACTION_TYPE, $lower_case, $upper_case) = 'COPY'">
                        <xsl:attribute name="disabled"/>
                      </xsl:if>
                      (C)ancel Trip
                    </button>
                  </td>
                </tr>
              </table>
            </td>
            <td align="right">
              <table>
                <tr>
                  <td align="right" valign="top">
                    <button class="BlueButton" style="width:80;" accesskey="B" name="bBack" onclick="javascript:doExit('load');">(B)ack </button>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top">
                    <button class="BlueButton" style="width:80;" accesskey="M" name="bMainMenu" onclick="javascript:doExit('main_menu');">(M)ain Menu </button>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>


      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
        	<td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
      </table>
      <script type="text/javascript">
        Calendar.setup
        (
          {
            inputField  : "delivery_date",      // ID of the input field
            ifFormat    : "y/mm/dd",            // the date format
            button      : "delivery_date_img"    // ID of the button
          }
        );
        Calendar.setup
        (
          {
            inputField  : "departure_date",      // ID of the input field
            ifFormat    : "y/mm/dd",            // the date format
            button      : "departure_date_img"  // ID of the button
          }
        );
      </script>
    </form>
    <iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
