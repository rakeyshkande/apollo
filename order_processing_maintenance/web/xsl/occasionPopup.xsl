<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="securityanddata.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Occasion Code Maintenance</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="../css/calendar.css"/>
				<script type="text/javascript" src="../js/util.js"></script>
				<script type="text/javascript" src="../js/calendar.js"></script>
				<script type="text/javascript" src="../js/clock.js"></script>
				<script type="text/javascript" src="../js/commonUtil.js"></script>
				<script type="text/javascript" src="../js/mercuryMessage.js"></script>
				<script type="text/javascript">
					function init()
					{
					
					}
					
					function doAddAction()
					{
					
					}
					
					function doCancelAction()
					{
					
					}
				</script>
			</head>
			<body onload="javascript:init();">
				<form name="csvAvailMaintenance" method="post">
					<xsl:call-template name="securityanddata"/>
					<input type="hidden" name="action_type" value="{key('pagedata','action_type')/value}"/>
					<input type="hidden" name="dnis_id" value="" />
					<table width="98%" align="center" cellspacing="1">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td align="center" height="10">
											<div id="invalidDiv" style="display:none" class="ErrorMessage floatleft">
												PLEASE ENTER VALID OCCASION CODE
											</div>	
										</td>
									</tr>
									<tr>
										<td align="center" class="TotalLine"><strong>Add New Occasion Code</strong></td>
									</tr>
									<tr>
										<td align="center" height="10"></td>
									</tr>
									<tr>
										<td align="center">
											<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
												<tr>
													<td align="right" width="40%"><b>Occasion Code:</b></td>														
													<td align="left" width="60%">
														<input type="text" name="r_occasion_code" size="6" value="" tabindex="1"/>
													</td>													
												</tr>
												<tr>
													<td align="right"><b>Description:</b></td>
													<td align="left">
														<input type="text" name="r_description" size="56" value="" tabindex="2"/>
													</td>													
												</tr>
												<tr>
													<td align="right"><b>Active?</b></td>
													<td align="left">
														<input type="checkbox" name="r_active" tabindex="3" checked="checked"/>
													</td>
												</tr>
												<tr>
						                          <td>
						                            <div class="floatright">
						                              <button class="BlueButton" name="ok" tabindex="4" onclick="javascript:doAddAction();">(O)k </button>
						                            </div>
						                          </td>
						                          <td>
						                            <div class="floatleft">
						                              <button class="BlueButton" name="cancel" tabindex="5" onclick="javascript:doCancelAction();">(C)ancel </button>
						                            </div>
						                          </td>
						                        </tr>
											</table>
										</td>
									</tr>									
								</table>
							</td>
						</tr>
					</table>					
					<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
				</form>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</body>
		</html>
	</xsl:template>  
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->