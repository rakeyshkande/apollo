<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
    <xsl:if test = "$dateTimeString != ''">
      <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="formatPhoneNumber">
    <xsl:param name="phoneNumber"/>
    <xsl:if test = "$phoneNumber != ''">
    <xsl:value-of select="substring($phoneNumber, 1, 3)"/>-<xsl:value-of select="substring($phoneNumber, 4, 3)"/>-<xsl:value-of select="substring($phoneNumber, 7, 4)"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>