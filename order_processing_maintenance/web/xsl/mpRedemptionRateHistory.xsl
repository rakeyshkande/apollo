<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Redemption Rate History</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/mercuryMessage.js"></script>

<script type="text/javascript"><![CDATA[

function init()
{
}

function doCloseAction()
{
    window.close();
}


]]>
</script>
</head>

<body onload="javascript:init();">

<form name="maint" method="post">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="mpRedemptionRateId" value="{key('pageData','mpRedemptionRateId')/value}"/>
<input type="hidden" name="mpRedemptionRateDescription" value="{key('pageData','mpRedemptionRateDescription')/value}"/>

<center>

<h2>
Maintenance History for&nbsp;
<xsl:value-of select="key('pageData','mpRedemptionRateId')/value"/>
&nbsp;-&nbsp;
<xsl:value-of select="key('pageData','mpRedemptionRateDescription')/value"/>
</h2>

<table width="550" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div style="padding-right: 16px; width: 425;">																													
                                    <table width="525" border="0" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <th class="TotalLine" width="75" align="left">Operation</th>
                                            <th class="TotalLine" width="75" align="left">Date/Time</th>
                                            <th class="TotalLine" width="75" align="left">Description</th>
                                            <th class="TotalLine" width="75" align="left">Payment Type</th>
                                            <th class="TotalLine" width="75" align="left">Redemption Rate</th>
                                            <th class="TotalLine" width="75" align="left">Requested By</th>
                                            <th class="TotalLine" width="75" align="left">Updated By</th>
                                            <th class="TotalLine" width="25">&nbsp;</th>
                                        </tr>
                                    </table>
                                    </div>
                                    <div style="width: 525; overflow:auto; overflow-x:hidden; padding-right: 16px; height: 200px;">																													
                                    <table width="525" border="0" cellpadding="2" cellspacing="0">	
                                        <xsl:for-each select="root/mp_redemption_rates/mp_redemption_rate">							                                  		
                                            <tr>
                                                <td align="left" width="50">
                                                    <xsl:choose>
                                                        <xsl:when test="operation = 'INS'">
                                                        Insert
                                                        </xsl:when>
                                                        <xsl:when test="operation = 'DEL'">
                                                        Delete
                                                        </xsl:when>
                                                        <xsl:when test="operation = 'UPD_NEW'">
                                                        Update
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </td>
                                                <td align="left" width="100">
                                                    <xsl:value-of select="timestamp" />
                                                </td>
                                                <td align="left" width="75">
                                                    <xsl:value-of select="description" />
                                                </td>
                                                <td align="left" width="75">
                                                    <xsl:value-of select="payment_method_id" />
                                                </td>             
                                                <td align="left" width="100">
                                                    <xsl:value-of select="mp_redemption_rate_amt" />
                                                </td>         
                                                <td align="left" width="75">
                                                    <xsl:value-of select="requested_by" />
                                                </td>                                                
                                                <td align="left" width="75">
                                                    <xsl:value-of select="updated_by" />
                                                </td>
                                                <td width="25">&nbsp;</td>
                                            </tr>                 		
                                        </xsl:for-each>
                                    </table>																
                                    </div>	
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <button accesskey="C" class="BlueButton" style="width:50;" name="Close" tabindex="99" onclick="javascript:doCloseAction();">(C)lose</button>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</center>
</form>

</body>
</html>
</xsl:template>  
</xsl:stylesheet>
