<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />
  <xsl:import href="commonUtil.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>


<xsl:template match="/">


<html>
	<head>
		<META http-equiv="Content-Type" content="text/html"></META>
    <title>Vendor Maintenance</title>
	  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
  	<script type="text/javascript" src="js/calendar.js"/>
  	<script type="text/javascript" src="js/FormChek.js"/>

		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/rico.js"></script>
		<script type="text/javascript" src="js/ftdajax.js"></script>

  	<script>
		<![CDATA[

  var PAGE_URL = '';

  function init()
  {
    var errorPopupMsg = document.getElementById("error_popup_message").value;
    if(errorPopupMsg != "")
    {
        alert(errorPopupMsg);
    }
    
    var popup = document.getElementById("popup").value;
    if (popup == "comments")
    {
      displayComments('Y', 'Y');
    }
  }


  function displayComments(required, canEdit)
  {
     var vendorId = document.getElementById("vendor_id").value;
     var url = '';
     url = "CommentsAction.do"+"?action_type=display"+"&comment_type=VENDOR_MAINT&vendor_id=" + vendorId +"&can_enter_comment=" + canEdit +"&comment_required=" + required + getSecurityParams(false);
     var modalFeatures  =  'dialogWidth:1100px;dialogHeight:460px;center:yes;status:no;help:no;';
     modalFeatures +=  'resizable:no;scroll:no';
     ret =  window.showModalDialog(url, "", modalFeatures);
  }

  function addBlock()
  {
    var url = "VendorListAction.do" +
      "?action_type=add_global_block"  + getSecurityParams(false);
    alert("Please be patient.  Processing can take up to 10 minutes.");

    performAction(url);
	}


	function createVendor()
  {
    /* Action type changes when the vendor is saved and validation errors occur.  The new_vendor
     * variable has been added to keep track that this is indeed a new vendor and not the
     * edit of an existing vendor when validation errors occur during a save.
     */ 
    var url = "VendorDetailAction.do" +
			"?action_type=new_vendor" + "&new_vendor=true" + "&edit_mode=new" + getSecurityParams(false);
    doRecordLock('VENDOR','retrieve', null);
		performAction(url);
	}

	function displayVendor(vendorId)
  {
    document.getElementById("lock_id").value = vendorId;
		var url = "VendorDetailAction.do" +
		  "?action_type=display_vendor" + "&vendor_id=" + vendorId + "&edit_mode=display" + getSecurityParams(false);
    doRecordLock('VENDOR','retrieve', null);
		performAction(url);
	}

	function deleteBlock(blockDate, blockType)
  {
		if(confirm("Are you sure you want to delete this record?"))
		{
			var url = "VendorListAction.do" +
				"?action_type=delete_global_block" +"&global_delete_block_date=" + blockDate + "&global_delete_block_type=" + blockType + getSecurityParams(false);
                        alert("Please be patient.  Processing can take up to 10 minutes.");
                        performAction(url);
		}
	}

  function performAction(url)
	{
    PAGE_URL = url;
    doContinueProcess();
	}

  function doContinueProcess()
  {
    document.forms[0].action = PAGE_URL;
    document.forms[0].target = window.name;
    document.forms[0].submit();
  }

  function checkAndForward(forwardAction)
  {
    doContinueProcess();
  }

  function clickMainMenu()
  {
    var url = "MainMenuAction.do" +  getSecurityParams(true);
    performAction(url);
  }


	//****************************************************************************************************
	//* doResetCutoff()
	//****************************************************************************************************
  function doResetCutoff()
  {
		//disable buttons, select boxes, and links.
		disableAll();

		if (confirm("Are you sure you want to reset the cutoff times for all vendors?"))
		{
      var newCutoff = document.getElementById("new_cutoff")
			var newCutoffValue = newCutoff.options[newCutoff.selectedIndex].value;

			//XMLHttpRequest
			var newRequest = new FTD_AJAX.Request('VendorListAction.do', FTD_AJAX_REQUEST_TYPE_POST, resetCutoffCallback, false);
			newRequest.addParam('action_type', 'reset_cutoff');
			newRequest.addParam('cutoff_time', newCutoffValue);
			newRequest.send();
                        this.popupWindow = window.open("","popupwindow","width=350,height=150");
                        this.popupWindow.document.write("<html><head><title>Processing Request...</title></head><body><h2>Processing...</h2><p><strong>Please be patient.  Processing can take up to 10 minutes.</strong><p/></body></html>");
		}
		else
		{
			enableAll();
		}

  }


	//****************************************************************************************************
	//* resetCutoffCallback()
	//****************************************************************************************************
  function resetCutoffCallback(data)
  {
    self.popupWindow.close();
    // Display 'success' or the error which was returned
    if(data.length >= 7)
    {
        var result = data.substring(0,7);
        if(result == 'success')
            alert("Global Cutoff was reset successfully.");
        else
            alert(data);
    }
    else
        alert(data);
    enableAll();
  }


	//****************************************************************************************************
	//* saveBackendDelay()
	//****************************************************************************************************
  function saveBackendDelay()
  {
		//disable buttons, select boxes, and links.
		disableAll();

		var cutoffDelay = document.getElementById("cutoff_delay");
		var cutoffDelayVal = stripInitialWhitespace(cutoffDelay.value);
    var isValid = isInteger(cutoffDelayVal, false);

		if (isValid && cutoffDelayVal >= 30 && cutoffDelayVal <= 300)
		{
			//XMLHttpRequest
			var newRequest = new FTD_AJAX.Request("VendorListAction.do", FTD_AJAX_REQUEST_TYPE_POST, saveBackendDelayCallback , true);
			newRequest.addParam("action_type", "save_cutoff_delay");
			newRequest.addParam("cutoff_delay", cutoffDelayVal);
			newRequest.send();
		}
		else
		{
			alert("Please you enter a valid Back-End Cutoff Delay.  A numeric value between 30 and 300 is required");
		}
		enableAll();

  }


	//****************************************************************************************************
	//* saveBackendDelayCallback()
	//****************************************************************************************************
  function saveBackendDelayCallback(data)
  {
    // Display 'success' or the error which was returned
    if(data.length >= 7)
    {
        var result = data.substring(0,7);
        if(result == 'success')
            alert("Back End Cutoff Delay was set successfully.");
        else
            alert(data);
    }
    else
        alert(data);

  	enableAll();
	}


	//****************************************************************************************************
	//* cancelLink()
	//****************************************************************************************************
	function cancelLink ()
	{
	  return false;
	}


	//****************************************************************************************************
	//* disableAll()
	//****************************************************************************************************
	function disableAll()
	{

		//disable buttons and other elements
		disableElements();

		//disable the links
		var links = document.links;
		var link;

		for(i=0; i<links.length; i++)
		{
			link = links[i];
			disableLink(link);
		}

	}


	//****************************************************************************************************
	//* disableElements()
	//****************************************************************************************************
	function disableElements()
	{
		var i;
		var form = document.forms[0];

		//disable the buttons and selects
		for(i=0; i<form.elements.length; i++)
		{
			if (form.elements[i].type != "hidden")
			{
				form.elements[i].disabled = true;
			}
		}
	}


	//****************************************************************************************************
	//* disableLink(link)
	//****************************************************************************************************
	function disableLink (link)
	{
		if (link.onclick)
			link.oldOnClick = link.onclick;

		link.onclick = cancelLink;

		if (link.style)
			link.style.cursor = 'default';
	}


	//****************************************************************************************************
	//* enableAll()
	//****************************************************************************************************
	function enableAll()
	{
		//enable buttons and other elements
		enableElements();

		//enable the links
		var links = document.links;
		var link;

		//enable the links
		for(i=0; i<links.length; i++)
		{
			link = links[i];
			enableLink(link);
		}

	}


	//****************************************************************************************************
	//* enableElements()
	//****************************************************************************************************
	function enableElements()
	{
		var i;
		var form = document.forms[0];

		//enable the buttons and selects
		for(i=0; i<form.elements.length; i++)
		{
			if (form.elements[i].type != "hidden")
			{
				form.elements[i].disabled = false;
			}
		}
	}


	//****************************************************************************************************
	//* enableLink(link)
	//****************************************************************************************************
	function enableLink(link)
	{
		link.onclick = link.oldOnClick ? link.oldOnClick : null;

		if (link.style)
		{
			link.style.cursor = document.all ? 'hand' : 'pointer';
		}
	}


	//****************************************************************************************************
	//* populateTime()
	//****************************************************************************************************
	function populateTime()
	{
		var hour	= 0;
		var value0;
		var value30;
		var valSelected = "";
		for (hour=0; hour<24; hour++ )
		{
			valSelected = "";
			if (hour<10)
			{
				value0  = '0' + hour + ':' + '00';
				value30 = '0' + hour + ':' + '30';
			}
			else
			{
				value0  = hour + ':' + '00';
				value30 = hour + ':' + '30';

				if (hour == 13)
					valSelected = "selected";
			}

			document.write( "<OPTION value=" + value0  + " >" + value0	+ "</OPTION>" );

			if (valSelected=="")
				document.write( "<OPTION value=" + value30 + ">" + value30	+ "</OPTION>" );
			else
				document.write( "<OPTION selected='selected' value=" + value30 + ">" + value30	+ "</OPTION>" );
		}
	}


	//****************************************************************************************************
	//* testTextFunction()
	//****************************************************************************************************
  function testTextFunction()
  {
		var cutoff = document.getElementById("new_cutoff")
		alert(cutoff.options[cutoff.selectedIndex].value);

		var testText = document.getElementById("test_text");

		if (testText.value == "d")
		{
			//disable the buttons and selects
			disableAll();
		}


		if (testText.value == "e")
		{
			//enable the buttons and selects
			enableAll();
		}


  }



		 ]]>
		</script>

  </head>

	<body onload = "javascript: init()">
		<form name="vendormaint" method="post">
			<input type="hidden" name="securitytoken" value="{//pagedata/securitytoken}"/>
			<input type="hidden" name="popup" value="{//pagedata/popup}"/>
			<input type="hidden" name="error_popup_message" value="{//pagedata/error_popup_message}"/>
			<input type="hidden" name="vendor_id" value=""/>
			<input type="hidden" name="adminAction" value="{//pagedata/adminAction}"/>
			<input type="hidden" name="lock_id"/>
			<input type="hidden" name="context" value="{//pagedata/context}"/>

			<!-- Header-->
			<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td width="30%" height="33" valign="top">
						<div class="floatleft">
							<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
						</div>
					</td>
					<td></td>
					<td  align="right" width="30%" valign="bottom">
					</td>
				</tr>
				<tr>
					<td></td>
					<td  align="center" valign="top" class="Header" id="pageHeader">
						Vendor Maintenance
					</td>
					<td id="time" align="right" valign="bottom" class="Label" width="20%" height="30">
						<script type="text/javascript">startClock();</script>
					</td>
				</tr>
			</table>

			<!-- Display table-->
			<table width="98%" align="center" cellspacing="1" class="mainTable">
				<tr>
					<td>
						<table width="100%" align="center" class="innerTable">
							<tr>
								<td align="left" height="10">
									<div id="invalidDiv" style="display:none" class="ErrorMessage floatleft">
									</div>
								</td>
							</tr>
							<tr>
								<td align="center" class="TotalLine">
									<div align="center"><strong>Vendor List</strong><br></br>
									</div></td>
									<div align="left">
									</div>
							</tr>
							<tr>
								<td align="center" height="10">
								</td>
							</tr>
							<tr>
								<td align="center">
									<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
										<tr>
											<td align="center">
												<div style="width:90%; border-left: 3px solid #000; border-right: 3px solid #000; border-top: 3px solid #000; border-bottom: 3px solid #000;">
													<table width="100%" border="0" cellpadding="2" cellspacing="0">
														<tr>
															<th class="label" width="10%" align="center">Vendor ID</th>
															<th class="label" width="20%" align="center">Vendor Name</th>
															<th class="label" width="15%" align="center">City, State</th>
															<th class="label" width="10%" align="center">Main Phone</th>
															<th class="label" width="15%" align="center">Main Email</th>
															<th class="label" width="10%" align="center">Blocked Dates</th>
                                                            <th class="label" width="10%" align="center">Zone Jump Eligibile</th>
                                                            <th class="label" width="10%" align="center">New Shipping System</th>                                                            
														</tr>
													</table>
												</div>
												<div style="overflow:auto; width:90%; height=200; border-left: 3px solid #000; border-right: 3px solid #000; border-bottom: 3px solid #000;">
													<table width="100%" border="0" cellpadding="2" cellspacing="0">
														<xsl:for-each select="root/vendors/vendor">
															<tr>
																<td align="center" width="10%">
																	<a id="HREF{vendor_id}" href="#" onClick="displayVendor('{vendor_id}')"><xsl:value-of select="vendor_id"/></a>
																</td>
																<td align="center" width="20%">
																	<xsl:value-of select="vendor_name"/>
																</td>
																<td align="center" width="15%">
																	<xsl:value-of select="city"/><xsl:if test ="city != ''  and state != ''">,&#160;</xsl:if><xsl:value-of select="state"/>
																</td>
																<td align="center" width="10%">
																	<xsl:call-template name="formatPhoneNumber">
																		<xsl:with-param name="phoneNumber" select="phone"/>
																	</xsl:call-template>
																</td>
																<td align="center" width="15%">
																	<xsl:value-of select="email"/>
																</td>
																<td align="center" width="10%">
																	<xsl:value-of select="days_blocked"/>
																</td>
                                                                <td align="center" width="10%">
                                                                    <xsl:choose>
                                                                        <xsl:when test="zone_jump_eligible_flag = 'Y'">
                                                                            <xsl:value-of select="zone_jump_eligible_flag"/>
                                                                        </xsl:when>
                                                                    </xsl:choose>
                                                               </td>
                                                               <td align="center" width="10%">
                                                                    <xsl:choose>
                                                                        <xsl:when test="new_shipping_system = 'Y'">
                                                                            <xsl:value-of select="new_shipping_system"/>
                                                                        </xsl:when>
                                                                    </xsl:choose>
                                                               </td>
															</tr>
														</xsl:for-each>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td>
									<table width="100%" align="center">
										<tr>
											<td align="center" colspan="2" class="TotalLine" style="text-align:center;font-size:14pt;font-weight:bold">
												<div align="center">Global Settings<br></br></div>
											</td>
											<div align="left"></div>
										</tr>

										<tr>
											<td width="50%" valign="top">
												<div style="overflow:auto; width:100%; border-left: 3px double #000; border-right: 3px double #000; border-bottom: 3px double #000; border-top: 3px double #000;">
													<table width="100%" align="center">
														<tr>
															<td align="center" class="TotalLine" colspan="2">
																<div align="center"><strong>Globally Blocked Dates</strong><br></br></div>
															</td>
														</tr>

														<tr>
															<td colspan="2">
																<div style="overflow:auto; width:90%; height=200; border-left: 3px solid #000; border-right: 3px solid #000; border-bottom: 3px solid #000;border-top: 3px solid #000;">
																	<xsl:for-each select="root/global_blocks/global_block">
																		<xsl:variable name="startDateVar">
																			<xsl:call-template name="formatDate">
																				<xsl:with-param name="dateTimeString" select="start_date"/>
																			</xsl:call-template>
																		</xsl:variable>
																		<xsl:value-of select="$startDateVar"/> - <xsl:value-of select="block_type"/>&#160;
																		<xsl:choose>
																			<xsl:when test="//can_block_vendor = 'Y'">
																				<a href="#" onClick="deleteBlock('{$startDateVar}','{block_type}');">[delete]</a>
																		 </xsl:when>
																		</xsl:choose>
																		<br></br>
																	</xsl:for-each>
																</div>
															</td>
														</tr>

														<tr>
															<td class="label" width="30%">
																Global Block Date:
															</td>
															<td align="left" width="20%">
																<input type="text" name="global_block_date" size="13"  tabindex="1" value="{//pagedata/global_block_date}"  />
																<img id="global_date_img" src="images\calendar.gif" tabindex="2" />
															</td>
														</tr>

														<tr>
															<td class="label">
																Global Block Type:
															</td>
															<td align="left">
																<SELECT NAME="global_block_type" tabindex="3">
																<OPTION VALUE="Shipping">Shipping</OPTION>
																<OPTION VALUE="Delivery">Delivery</OPTION>
																<OPTION VALUE="Both">Both</OPTION>
																</SELECT>
															</td>
														</tr>

														<tr>
															<td>
																<xsl:choose>
																<xsl:when test="//can_block_vendor = 'Y'">
																<button class="bigBlueButton" style="width:80;" id="addBlockInput" tabindex="4" accesskey="A" onClick="javascript:addBlock();">(A)dd Block<br></br>Date </button>
																 </xsl:when>
																 <xsl:otherwise>
																<button class="bigBlueButton" style="width:80;" disabled="true" id="addBlockInput" tabindex="4" accesskey="A" onClick="javascript:addBlock();">(A)dd Block<br></br>Date </button>
																 </xsl:otherwise>
																</xsl:choose>
															</td>
															<td align="left" class="ErrorMessage">
																<xsl:value-of select="//pagedata/error_message"/>
															</td>
														</tr>
													</table>
												</div>
											</td>



											<td width="50%" valign="top">
												<table width="100%" align="center">
													<tr>
														<td>
															<div style="overflow:auto; width:100%; border-left: 3px double #000; border-right: 3px double #000; border-bottom: 3px double #000; border-top: 3px double #000;">
																<table width="100%" align="center">
																	<tr>
																		<td align="center" class="TotalLine" colspan="3">
																			<div align="center"><strong>Global Front-End Cutoff Reset</strong><br></br></div>
																		</td>
																	</tr>
																	<tr>
																		<td align="left"><strong>
																			&nbsp; New Cutoff:</strong>
																		</td>
																		<td align="left">
																			<select id="new_cutoff" name="new_cutoff" tabindex="5" >
																				<script>
																					populateTime();
																				</script>
																			</select>
																		</td>
																		<td align="left">
																			<button id="reset" name="reset" class="bigBlueButton" style="width:80;" tabindex="6" accesskey="R" onClick="javascript:doResetCutoff();">(R)eset</button>
																		</td>
																	</tr>
																</table>
															</div>
														</td>
													</tr>

													<tr><td colspan="3">&nbsp;</td></tr>

													<tr>
														<td>
															<div style="overflow:auto; width:100%; border-left: 3px double #000; border-right: 3px double #000; border-bottom: 3px double #000; border-top: 3px double #000;">
																<table width="100%" align="center">
																	<tr>
																		<td align="center" class="TotalLine" colspan="3">
																			<div align="center"><strong>Other Global Options</strong><br></br></div>
																		</td>
																	</tr>
																	<tr>
																		<td align="center"><strong>
																			&nbsp; Back-End Cutoff Delay:</strong>
																		</td>
																		<td align="left" colspan="2">
																			<input type="text" id="cutoff_delay" name="cutoff_delay" size="13"  tabindex="7" value="" />
																		</td>
																	</tr>
																	<tr><td colspan="3">&nbsp;</td></tr>
																	<tr>
																		<td align="center" colspan="3">
																			<button id="save" name="save" class="bigBlueButton" style="width:80;" tabindex="8" accesskey="S" onClick="javascript:saveBackendDelay();">(S)ave</button>
																		</td>
																	</tr>


																</table>
															</div>
														</td>
													</tr>

												</table>
											</td>
											<div align="left"></div>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>


			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
				<tr>
					<td>
						<table>
							<tr>
								<td  align="left">
									<xsl:choose>
										<xsl:when test="//can_create_vendor = 'Y'">
											<button class="bigBlueButton" style="width:80;" name="createvendor" tabindex="9" accesskey="C" onclick="javascript:createVendor();">(C)reate New<br></br>Vendor </button>
										</xsl:when>
										<xsl:otherwise>
											<button class="bigBlueButton" style="width:80;" disabled="true" name="createvendor" tabindex="9" accesskey="C" onclick="javascript:createVendor();">(C)reate New<br></br>Vendor </button>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<table>
							<tr>
								<td align="right" valign="top">
									<button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10"  accesskey="M" onclick="javascript:clickMainMenu();">(M)ain Menu </button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<xsl:call-template name="footer"/>
			<script type="text/javascript">
				Calendar.setup(
				{
					inputField  : "global_block_date",  // ID of the input field
					ifFormat    : "mm/dd/y",  // the date format
					button      : "global_date_img"  // ID of the button
				}
			);
			</script>

		</form>

		<iframe id="checkLock" width="0px" height="0px" border="0"/>
	</body>
</html>

</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
		</xsl:call-template>
	</xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
	<xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
      <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>
	</xsl:template>
</xsl:stylesheet>