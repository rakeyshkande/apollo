<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:variable name="serviceFeeId" select="key('pageData','serviceFeeId')/value"/>
  <xsl:variable name="domesticFee" select="key('pageData','domesticFee')/value"/>
  <xsl:variable name="intlFee" select="key('pageData','intlFee')/value"/>
  <xsl:variable name="vendorCharge" select="key('pageData','vendorCharge')/value"/>
  <xsl:variable name="vendorSatUpcharge" select="key('pageData','vendorSatUpcharge')/value"/>
  <xsl:variable name="vendorSunUpcharge" select="key('pageData','vendorSunUpcharge')/value"/>
  <xsl:variable name="vendorMonUpcharge" select="key('pageData','vendorMonUpcharge')/value"/>
  <xsl:variable name="sameDayUpcharge" select="key('pageData','sameDayUpcharge')/value"/>
  <xsl:variable name="sameDayUpchargeFS" select="key('pageData','sameDayUpchargeFS')/value"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Service Charge Override Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/mercuryMessage.js"></script>

<script type="text/javascript">

var deleteArray = new Array();
var idArray = new Array();
var arrayCount = 0;

function init()
{
<xsl:for-each select="root/service_fee_overrides/service_fee_override">
  document.maint.idList.value = document.maint.idList.value + '<xsl:value-of select="override_date"/>' + ",";
</xsl:for-each>

<xsl:if test="key('pageData','update_allowed')/value = 'N'">
document.getElementById('addCode').disabled = true;
document.getElementById('deleteCode').disabled = true;
</xsl:if>

<xsl:if test="key('pageData','dateRangeFlag')/value = 'Y'">
  msg = "Date is part of a delivery date range.  Please make sure\n";
  msg = msg + "to create an override entry for all dates in the range.";
  alert(msg); 
</xsl:if>
}

<![CDATA[
function doCancelAction()
{
    //window.returnValue = "Cancel";
    window.close();
}

function doDeleteAction()
{
    document.maint.deleteValues.value = "";
    for (i=0;i<deleteArray.length;i++) {
        isChecked = document.getElementById(deleteArray[i]).checked;
        if (isChecked) {
            if (document.maint.deleteValues.value == "") {
                document.maint.deleteValues.value = idArray[i];
            } else {
                document.maint.deleteValues.value = document.maint.deleteValues.value + "," + idArray[i];
            }
        }
    }
    if (document.maint.deleteValues.value == "") {
        alert("No record was selected for deletion");
        return;
    }
    msg = "Are you sure you want to delete?";
    if (doExitPageAction(msg)) {
        var modal_dim = "dialogWidth:400px; dialogHeight:250px; center:yes; status:0; help:no; scroll:no";
        var popURL = "serviceFeeDeletePop.html";
        var results=showModalDialog(popURL, "", modal_dim);
        if (results && results != null) {
            showWaitMessage("serviceFeeDiv", "wait", "Processing");
            document.maint.action = "ServiceFeeMaintAction.do";
            document.maint.action_type.value = "deleteServiceFeeOverride";
            document.maint.requested_by.value = results;
            document.maint.submit();
        }
    }
    
}

function popEdit(snhId, overrideDate, domesticFee, intlFee, vendorCharge, vendorSatUpcharge, action, requestedBy, sameDayUpcharge, sameDayUpchargeFS, vendorSunUpcharge, vendorMonUpcharge)
{
    if (document.maint.updateAllowed.value == "N") {
        action = "View";
    }
    var modal_dim = "dialogWidth:500px; dialogHeight:400px; center:yes; status:0; help:no; scroll:no";
    var popURL = "serviceFeeOverridePop.html?id=" + document.maint.serviceFeeId.value;
    popURL = popURL + "&description=" + document.maint.description.value;
    popURL = popURL + "&overrideDate=" + overrideDate;
    popURL = popURL + "&domesticFee=" + domesticFee;
    popURL = popURL + "&intlFee=" + intlFee;
    popURL = popURL + "&vendorCharge=" + vendorCharge;
    popURL = popURL + "&vendorSatUpcharge=" + vendorSatUpcharge;
    popURL = popURL + "&action=" + action;
    popURL = popURL + "&idList=" + document.maint.idList.value;
    popURL = popURL + "&requestedBy=" + requestedBy;
    popURL = popURL + "&sameDayUpcharge=" + sameDayUpcharge;
    popURL = popURL + "&sameDayUpchargeFS=" + sameDayUpchargeFS;
    popURL = popURL + "&vendorSunUpcharge=" + vendorSunUpcharge;
    popURL = popURL + "&vendorMonUpcharge=" + vendorMonUpcharge;
    var results = showModalDialog(popURL, "", modal_dim);
    if (results && results != null) {
        showWaitMessage("serviceFeeDiv", "wait", "Processing");
        document.maint.override_date.value = results[2];
        document.maint.domestic_fee.value = results[3];
        document.maint.international_fee.value = results[4];
        document.maint.requested_by.value = results[5];
        document.maint.vendor_charge.value = results[6];
        document.maint.vendor_sat_upcharge.value = results[7];
        document.maint.description.value = results[8];
        document.maint.same_day_upcharge.value = results[9];
        document.maint.same_day_upcharge_fs.value = results[10];
        document.maint.vendor_sun_upcharge.value = results[11];
        document.maint.vendor_mon_upcharge.value = results[12];
        document.maint.action = "ServiceFeeMaintAction.do";
        document.maint.action_type.value = "saveServiceFeeOverride";
        document.maint.submit();
    }
}


]]>
</script>
</head>

<body onload="javascript:init();">

<form name="maint" method="post">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
<input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
<input type="hidden" name="deleteValues" value=""/>
<input type="hidden" name="serviceFeeId" value="{key('pageData','serviceFeeId')/value}"/>
<input type="hidden" name="description" value="{key('pageData','serviceFeeDescription')/value}"/>
<input type="hidden" name="domestic_fee" value=""/>
<input type="hidden" name="international_fee" value=""/>
<input type="hidden" name="vendor_charge" value=""/>
<input type="hidden" name="vendor_sat_upcharge" value=""/>
<input type="hidden" name="vendor_sun_upcharge" value=""/>
<input type="hidden" name="vendor_mon_upcharge" value=""/>
<input type="hidden" name="override_date" value="" />
<input type="hidden" name="idList" value="" />
<input type="hidden" name="calledFrom" value="serviceFeeOverride" />
<input type="hidden" name="requested_by" value="" />
<input type="hidden" name="same_day_upcharge" value=""/>
<input type="hidden" name="same_day_upcharge_fs" value=""/>

<center>

<h2>
Overrides for&nbsp;
<xsl:value-of select="key('pageData','serviceFeeId')/value"/>
&nbsp;-&nbsp;
<xsl:value-of select="key('pageData','serviceFeeDescription')/value"/>
</h2>

<table width="350" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
          <div id="serviceFeeDiv">
            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr>
                            <xsl:choose>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveOverrideSuccessful'">
                                    <td class="OperationSuccess">
                                    Override Date&nbsp;<xsl:value-of select="key('pageData','overrideDate')/value"/>&nbsp;was successfully saved.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveOverrideFailed'">
                                    <td class="errorMessage">
                                    Request to update Override Date&nbsp;<xsl:value-of select="key('pageData','overrideDate')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'novatorConnect'">
                                    <td class="errorMessage">
                                    Cannot connect to Novator
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'novatorDEN'">
                                    <td class="errorMessage">
                                    Novator has denied the request
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'deleteSuccessful'">
                                    <td class="OperationSuccess">
                                    Override Date&nbsp;<xsl:value-of select="key('pageData','overrideDate')/value"/>&nbsp;was successfully deleted.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'deleteFailed'">
                                    <td class="errorMessage">
                                    Request to delete Override Date&nbsp;<xsl:value-of select="key('pageData','overrideDate')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td>&nbsp;</td>
                                </xsl:otherwise>
                            </xsl:choose>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div style="padding-right: 16px; width: 700;">																													
                                    <table width="700" border="0" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <th class="TotalLine" width="35" align="center">Delete</th>
                                            <th class="TotalLine" width="125" align="center">Override Date</th>
                                            <th class="TotalLine" width="75" align="right">Domestic<br/>(aka Florist)<br/>Charge</th>
                                            <th class="TotalLine" width="75" align="right">Same<br/>Day<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="right">Same Day<br/>Upcharge<br/>FS-Members</th>
                                            <th class="TotalLine" width="75" align="right">Int'l<br/>Charge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Charge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Saturday<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Sunday<br/>Upcharge</th>
                                            <th class="TotalLine" width="75" align="right">Vendor<br/>Monday<br/>Upcharge</th>
                                            <th class="TotalLine" width="15">&nbsp;</th>
                                        </tr>
                                    </table>
                                    </div>
                                    <div style="width: 700; overflow:auto; overflow-x:hidden; padding-right: 16px; height: 150px;">																													
                                    <table width="680" border="0" cellpadding="2" cellspacing="0">	
                                        <xsl:for-each select="root/service_fee_overrides/service_fee_override">							                                  		
                                            <tr>
                                                <td align="center" width="40">
                                                    <input type="checkbox" name="delete_{position()}" value="{snh_id}" tabindex="{position()+10}" />
<script type="text/javascript">
    deleteArray[arrayCount] = "delete_" + <xsl:value-of select="@num" />
    idArray[arrayCount] = '<xsl:value-of select="override_date" />'
    arrayCount = arrayCount + 1;
</script>
                                                </td>
                                                <td align="center" width="100">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="override_date" />
                                                    </a>
                                                </td>
                                                <td align="right" width="75">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="domestic_charge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="75">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="same_day_upcharge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="75">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="same_day_upcharge_fs" />
                                                    </a>
                                                </td>
                                                <td align="right" width="70">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="international_charge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="65">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="vendor_charge" />
                                                    </a>
                                                </td>
                                                <td align="right" width="70">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="vendor_sat_upcharge" />
                                                    </a>
                                                </td>   
                                                 <td align="right" width="75">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="vendor_sun_upcharge" />
                                                    </a>
                                                </td>
                                                 <td align="right" width="75">
                                                    <a href="javascript:popEdit('{snh_id}','{override_date}','{domestic_charge}','{international_charge}','{vendor_charge}','{vendor_sat_upcharge}','Edit','{requested_by}','{same_day_upcharge}', '{same_day_upcharge_fs}', '{vendor_sun_upcharge}','{vendor_mon_upcharge}')">
                                                    <xsl:value-of select="vendor_mon_upcharge" />
                                                    </a>
                                                </td>                                             
                                                <!--  <td width="15">&nbsp;</td> -->
                                            </tr>                 		
                                        </xsl:for-each>
                                    </table>																
                                    </div>	
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <button accesskey="A" class="BlueButton" style="width:50;" name="AddCode" tabindex="98" onclick="javascript:popEdit('{$serviceFeeId}','','{$domesticFee}','{$intlFee}','{$vendorCharge}','{$vendorSatUpcharge}','Add','','{$sameDayUpcharge}','{$sameDayUpchargeFS}','{$vendorSunUpcharge}','{$vendorMonUpcharge}');">(A)dd</button>
                        &nbsp;&nbsp;
                        <button accesskey="D" class="BlueButton" style="width:150;" name="DeleteCode" tabindex="99" onclick="javascript:doDeleteAction();">(D)elete Selected</button>
                        &nbsp;&nbsp;
                        <button accesskey="C" class="BlueButton" style="width:50;" name="Cancel" tabindex="99" onclick="javascript:doCancelAction();">(C)ancel</button>
                    </td>
                </tr>
            </table>
          </div>
        </td>
    </tr>
</table>
</center>
</form>

<div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
            <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                        <td id="waitTD" width="50%" class="waitMessage"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
</xsl:template>  
</xsl:stylesheet>
