<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->
<xsl:variable name="FILTER" select="key('pageData', 'filter')/value"/>


<xsl:template match="/root">

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Zone Jump Summary</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/ZJTrip.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/util.js"/>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script type="text/javascript" language="javascript">

//****************************************************************************************************
//*  values
//****************************************************************************************************
var somethingChanged = false;
var serverErrorMessage = '<xsl:value-of select="key('pageData', 'OK_ERROR_MESSAGE')/value"/>';

    </script>



  </head>
  <body onload="javascript: init()">
    <form name="fTruckSummary" method="post">
      <input type="hidden" name="securitytoken" id="securitytoken"   value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"       id="context"         value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"   id="adminAction"     value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="filter"        id="filter"          value="{key('pageData', 'filter')/value}"/>

      <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="30%" height="33" valign="top">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td>&nbsp;</td>
          <td align="right" width="30%" valign="bottom">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" valign="top" class="Header" id="pageHeader">Zone Jump Trip Summary</td>
          <td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
        </tr>
      </table>


      <!-- content to be hidden once the search begins -->
      <div id="content" style="display:block">
        <table width="98%" align="center" cellspacing="1" class="mainTable" border="0">
          <tr>
            <td>
              <table width="100%" align="center" class="innerTable" border="0">
                <tr>
                  <td>
                    <table width="100%" align="center" border="1">
                      <tr>
                        <td class="label" align="center" style="background-color:#ccccff;"></td>
                        <td class="label" align="center" style="background-color:#ccccff;">Vendor Location</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Injection Hub</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Vendors Selected</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Vendor Departure Date</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Delivery Date</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Status</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Sort Code</td>
                        <td class="label" align="center" style="background-color:#ccccff;">Rejects Exist</td>
                      </tr>
                      <xsl:for-each select="TRIPS/TRIP">
                        <tr>
                          <td align="center"><input type="radio" id="tripRadio" name="tripRadio" value="{trip_id}"/></td>
                          <td align="center"><xsl:value-of select="trip_origination_desc"/></td>
                          <td align="center">
                            <xsl:value-of select="INJECTION_HUB/city_name"/>,&nbsp;
                            <xsl:value-of select="INJECTION_HUB/state_master_id"/>&nbsp;-&nbsp;
                            <xsl:value-of select="INJECTION_HUB/carrier_id"/>
                          </td>
                          <td align="left">
                            <select size="3">
                              <xsl:for-each select="TRIP_VENDOR">
                                <option value="{trip_id}" vendor="{vendor_id}">
                                  <xsl:value-of select="vendor_name"/>
                                </option>
                              </xsl:for-each>
                            </select>
                          </td>
                          <td align="center">
                            <xsl:value-of select="substring(departure_date,6,2)"/>-<xsl:value-of select="substring(departure_date,9,2)"/>-<xsl:value-of select="substring(departure_date,1,4)"/>
                          </td>
                          <td align="center">
                            <xsl:value-of select="substring(delivery_date,6,2)"/>-<xsl:value-of select="substring(delivery_date,9,2)"/>-<xsl:value-of select="substring(delivery_date,1,4)"/>
                          </td>
                          <td align="center">
                            <xsl:choose>
                              <xsl:when test="virtual_trip_status_code != ''">
                                <xsl:value-of select="virtual_trip_status_code"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:variable name="ACTUAL_TRIP_STATUS_CODE" select="actual_trip_status_code"/>
                                <xsl:for-each select="/root/TRIP_STATUSES/TRIP_STATUS">
                                  <xsl:if test="$ACTUAL_TRIP_STATUS_CODE = trip_status_code">
                                    <xsl:value-of select="trip_status_desc"/>
                                  </xsl:if>
                                </xsl:for-each>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td align="center"><xsl:value-of select="sort_code"/></td>
                          <td align="center">
                            &nbsp;<xsl:if test="tv_reject_printed_exist = 'Y'">Y</xsl:if>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
          <tr>
            <td>
              <table>
                <tr>
                  <td align="left">
                    <select name="sFilter" id="sFilter">
                      <option value="All">All</option>
                      <xsl:for-each select="TRIP_STATUSES/TRIP_STATUS">
                        <option value="{trip_status_code}">
                          <xsl:if test="trip_status_code = $FILTER"><xsl:attribute name="selected"/></xsl:if>
                          <xsl:value-of select="trip_status_desc"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bFilter" accesskey="F" onclick="javascript:doFilter('load');">
                      (F)ilter
                    </button>
                  </td>
                </tr>
              </table>
            </td>
            <td align="right">
              <table>
                <tr>
                  <td align="right" valign="top">
                    <button class="BlueButton" style="width:80;" name="bDashboard" accesskey="D" onclick="javascript:doLoadDefault('dashboard');">(D)ashboard</button>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
          <tr>
            <td>
              <table>
                <tr>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bAdd" accesskey="A" onclick="javascript:doAdd();">(A)dd</button>
                  </td>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bEdit" accesskey="E" onclick="javascript:doCopyEdit('edit');">(E)dit</button>
                  </td>
                  <td align="left">
                    <button class="BlueButton" style="width:80;" name="bCopy" accesskey="P" onclick="javascript:doCopyEdit('copy');">Co(p)y</button>
                  </td>
                </tr>
              </table>
            </td>
            <td align="right">
              <table>
                <tr>
                  <td align="right" valign="top">
                    <button class="BlueButton" style="width:80;" accesskey="M" name="bMainMenu" onclick="javascript:doExit('main_menu');">(M)ain Menu</button>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>

      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
             <td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
      </table>
    </form>
    <iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
