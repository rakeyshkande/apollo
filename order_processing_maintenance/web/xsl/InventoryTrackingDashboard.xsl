<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:variable name="currPage"><xsl:value-of select="key('pageData','currentPage')/value"/></xsl:variable>

<!-- Variables -->
<xsl:decimal-format NaN="0.00" name="notANumber"/>


<xsl:template match="/root">



<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Inventory Tracking Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>
    <link rel="stylesheet" type="text/css" href="css/calendar.css"></link>

    <script>
        var newTableData = new Array();
        var liveFeedAllowed = null;
        var liveFeedOn = null;
        var contentFeedAllowed = null;
        var contentFeedOn = null;
        var testFeedAllowed = null;
        var testFeedOn = null;
        var uatFeedAllowed = null;
        var uatFeedOn = null;
        liveFeedAllowed = '<xsl:value-of select="novator_update_setup/production_feed_allowed"/>';
        liveFeedOn = '<xsl:value-of select="novator_update_setup/production_feed_checked"/>';
        contentFeedAllowed = '<xsl:value-of select="novator_update_setup/content_feed_allowed"/>';
        contentFeedOn = '<xsl:value-of select="novator_update_setup/content_feed_checked"/>';
        testFeedAllowed = '<xsl:value-of select="novator_update_setup/test_feed_allowed"/>';
        testFeedOn = '<xsl:value-of select="novator_update_setup/test_feed_checked"/>';
        uatFeedAllowed = '<xsl:value-of select="novator_update_setup/uat_feed_allowed"/>';
        uatFeedOn = '<xsl:value-of select="novator_update_setup/uat_feed_checked"/>';	
    </script>
	
    <script type="text/javascript" src="js/calendar.js"></script>
    <script type="text/javascript" src="js/InventoryTrackingDashboard.js"></script>
    <script type="text/javascript" src="js/Tokenizer.js"></script>
    <script type="text/javascript" src="js/FormChek.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>
	<script type="text/javascript" src="js/copyright.js"></script>

  </head>
  <body onload="javascript: init()">
    <form name="fInventoryTrackingDashboard" method="post" enctype="multipart/form-data">

      <input type="hidden" name="securitytoken"           id="securitytoken"     value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"                 id="context"           value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"             id="adminAction"       value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="action_type"             id="action_type"/>
      <input type="hidden" name="hRecordCount"            id="hRecordCount"      value="{key('pageData', 'recordCount')/value}"/>
      <input type="hidden" name="hVendorsChosen"          id="hVendorsChosen" />
      <input type="hidden" name="hInputExcelFileName"     id="hInputExcelFileName"/>
      
      <input type = "hidden" name = "hCurrentpage" id = "hCurrentpage" value="{$currPage}"/>
      <input type = "hidden" name = "hTotalPages"  id = "hTotalPages" value="{key('pageData', 'totalPages')/value}"/>
	  <input type = "hidden" name = "hStartRecord" id = "hStartRecord" value="{key('pageData', 'startRecord')/value}"/>
	  <input type = "hidden" name = "paginationAction" id="paginationAction" value="no"/>
	  <input type = "hidden" name = "pageSize" id= "pageSize" value="{key('pageData', 'currPageSize')/value}"/>
       
      <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="30%" height="33" valign="top">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td>&nbsp;</td>
          <td align="right" width="30%" valign="bottom">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" valign="top" class="Header" id="pageHeader">Inventory Tracking Dashboard</td>
          <td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
        </tr>
      </table>
      

      <!-- Main Div -->
      <div id="content" style="display:block">

        <!-- Search filters -->
        <tr>
          <td align="left" valign="top">
            <table>

            <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->
            <xsl:if test = "key('pageData', 'errorMessage')/value != '' or key('pageData', 'successCount')/value != ''">
              <tr>
                <td>
                  <table width="1205" align="center" cellspacing="1" border="0" cellpadding="0" >
                    <tr>
                      <td align="left" valign="center" >
                        <H5 style="color:blue;">YOUR REQUEST YIELDED FOLLOWING RESULTS:&nbsp;</H5>
                        <xsl:if test="key('pageData', 'successCount')/value != ''">
                          <H6 style="color:green;"><xsl:value-of select="key('pageData','successCount')/value"/> record(s) successfully uploaded</H6> 
                        </xsl:if>
                        <xsl:if test="key('pageData', 'errorCount')/value != ''">
                          <H6 style="color:red;"><xsl:value-of select="key('pageData','errorCount')/value"/> record(s) could not be uploaded</H6> 
                        </xsl:if>
                      </td>
                      <td align="left" >
                        <textarea name="errorMessageTextArea" id="errorMessageTextArea" rows="3" cols="110" tabindex="5">
                          <xsl:value-of select = "key('pageData', 'errorMessage')/value"/>
                        </textarea>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </xsl:if>


              <tr>
                <td>
                  <table width="1205" align="center" cellspacing="1" border="0" cellpadding="0" >
                    <tr>Please provide at least one Product or one Vendor:
                      <td>
                        <table>
                          <tr>
                            <td align="right">
                              Product IDs<BR/>(line delimited):
                            </td>
                            <td align="left">
                              <textarea name="taProductIds" id="taProductIds" rows="3" cols="20">
                                <xsl:if test="key('pageData', 'productIds')/value != ''">
                                  <xsl:value-of select="key('pageData','productIds')/value"/>
                                </xsl:if>
                              </textarea>
                            </td>
                          </tr>
                          <tr>
                            <td align="right">
                              Product Status:
                            </td>
                            <td align="left">
                              <select name="sProductStatus" id="sProductStatus" onchange="">
                                <option value=""></option>
                                <option value="A"><xsl:if test="key('pageData', 'productStatus')/value = 'A'"><xsl:attribute name="selected"/></xsl:if>Available</option>
                                <option value="R"><xsl:if test="key('pageData', 'productStatus')/value = 'R'"><xsl:attribute name="selected"/></xsl:if>Restricted</option>
                                <option value="X"><xsl:if test="key('pageData', 'productStatus')/value = 'X'"><xsl:attribute name="selected"/></xsl:if>Shutdown</option>
                                <option value="U"><xsl:if test="key('pageData', 'productStatus')/value = 'U'"><xsl:attribute name="selected"/></xsl:if>Unavailable</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td align="right">
                              Period Status:
                            </td>
                            <td align="left">
                              <select name="sVendorProductStatus" id="sVendorProductStatus" onchange="">
                                <option value=""></option>
                                <option value="A"><xsl:if test="key('pageData', 'vendorProductStatus')/value = 'A'"><xsl:attribute name="selected"/></xsl:if>Available</option>
                                <option value="X"><xsl:if test="key('pageData', 'vendorProductStatus')/value = 'X'"><xsl:attribute name="selected"/></xsl:if>Shutdown</option>
                                <option value="U"><xsl:if test="key('pageData', 'vendorProductStatus')/value = 'U'"><xsl:attribute name="selected"/></xsl:if>Unavailable</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td align="right">
                              Start Date:
                            </td>
                            <td align="left">
                              <table>
                                <tr>
                                  <td>
                                    <input type="text" name="tStartDate" id="tStartDate" maxlength="10" size="10">
                                      <xsl:attribute name="disabled"/>
                                      <xsl:if test="key('pageData', 'startDate')/value != ''">
                                          <xsl:attribute name="VALUE"><xsl:value-of select="key('pageData','startDate')/value"/></xsl:attribute>
                                      </xsl:if>
                                    </input>
                                    &nbsp;&nbsp;&nbsp;
                                    <img id="start_date_img" src="images\calendar.gif" ALT="Entered value must be less than or equal to the inventory record start date"></img>
                                    &nbsp;&nbsp;&nbsp;
                                    <img id="removed_img" src="images\removed.gif" onclick="clearField('tStartDate');" ALT="Click here to reset the Start Date"></img>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td align="right">
                              End Date:
                            </td>
                            <td align="left">
                              <table>
                                <tr>
                                  <td>
                                    <input type="text" name="tEndDate" id="tEndDate" maxlength="10" size="10">
                                      <xsl:attribute name="disabled"/>
                                      <xsl:if test="key('pageData', 'endDate')/value != ''">
                                          <xsl:attribute name="VALUE"><xsl:value-of select="key('pageData','endDate')/value"/></xsl:attribute>
                                      </xsl:if>
                                    </input>
                                    &nbsp;&nbsp;&nbsp;
                                    <img id="end_date_img" src="images\calendar.gif" ALT="Entered value must be greater than or equal to the inventory record end date"></img>
                                    &nbsp;&nbsp;&nbsp;
                                    <img id="removed_img" src="images\removed.gif" onclick="clearField('tEndDate');" ALT="Click here to reset the End Date"></img>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td align="right">
                              O/H less than:
                            </td>
                            <td align="left">
                              <input type="text" name="tOnHand" id="tOnHand">
                                <xsl:if test="key('pageData', 'onHand')/value != ''">
                                    <xsl:attribute name="VALUE"><xsl:value-of select="key('pageData','onHand')/value"/></xsl:attribute>
                                </xsl:if>
                              </input>
                            </td>
                          </tr>
                          <tr><td colspan="2">&nbsp;</td></tr>
                          <!--************* Excel File Search *************-->
                          <tr>
                            <td align="right">
                              Input File (.xls)
                            </td>
                            <!-- Input File (.Xls) -->
                            <td align="left">
                              <input type="file" name="fileInputExcelFile" id="fileInputExcelFile" tabindex="5"/>
                              &nbsp;&nbsp;&nbsp;&nbsp;
                              <xsl:choose>
                                <xsl:when test="key('pageData', 'updateAllowed')/value = 'Y'">
                                  <button name="bUpload" id="bUpload" class="BlueButton" style="width:120;" accesskey="U" onclick="performUpload();">(U)pload</button>
                                </xsl:when>
                                <xsl:otherwise>                           
                                  <button name="bUpload" id="bUpload" class="BlueButton" style="width:120;" disabled="true">(U)pload</button>
                                </xsl:otherwise>                            
                              </xsl:choose>
                            </td>
                          </tr>
                        </table>
                      </td>

                      <td align="right">
                        <table>
                          <tr>
                            <td align="left">
                              <H4>Vendors:</H4>
                            </td>
                          </tr>

                          <tr>
                            <td>
                              <table>
                                <tr>
                                  <td>Available:</td>
                                  <td>&nbsp;</td>
                                  <td>Selected:</td>
                                </tr>
                                <tr>
                                  <td>
                                    <select name="sVendorsAvailable" id="sVendorsAvailable" ondblclick="performMoveSelected();" size="8" style="width:300;">
                                      <xsl:attribute name="multiple"/>
                                      <xsl:for-each select="vendors_available/VendorVO">
                                        <option value="{VendorID}">
                                          <xsl:value-of select="VendorName"/>&nbsp;(<xsl:value-of select="VendorID"/>)
                                        </option>
                                      </xsl:for-each>
                                    </select>
                                  </td>
                                  <td>
                                    <button name="bMoveSelectedAll"   id="bMoveSelectedAll"   class="BlueButton" style="width:20;" onclick="performMoveSelectedAll()"  >&gt;&gt;</button><BR/>
                                    <button name="bMoveSelected"      id="bMoveSelected"      class="BlueButton" style="width:20;" onclick="performMoveSelected();"    >&gt;</button><BR/>
                                    <button name="bMoveUnSelected"    id="bMoveUnSelected"    class="BlueButton" style="width:20;" onclick="performMoveUnSelected()"   >&lt;</button><BR/>
                                    <button name="bMoveUnSelectedAll" id="bMoveUnSelectedAll" class="BlueButton" style="width:20;" onclick="performMoveUnSelectedAll()">&lt;&lt;</button>
                                  </td>
                                  <td>
                                    <select name="sVendorsChosen" id="sVendorsChosen" ondblclick="performMoveUnSelected();" size="8" style="width:300;">
                                      <xsl:attribute name="multiple"/>
                                      <xsl:for-each select="vendors_chosen/VendorVO">
                                        <option value="{VendorID}">
                                          <xsl:value-of select="VendorName"/>&nbsp;(<xsl:value-of select="VendorID"/>)
                                        </option>
                                      </xsl:for-each>
                                    </select>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>

                          <tr><td>&nbsp;</td></tr>

                          <tr>
                            <td align="right" valign="top">
                              <button name="bSearch" id="bSearch" class="BlueButton" style="width:120;" accesskey="S" onclick="performSearch();">(S)earch</button>
                              <xsl:choose>
                                <xsl:when test="key('pageData', 'updateAllowed')/value = 'Y'">
                                  <button name="bAddNew" id="bAddNew" class="BlueButton" style="width:120;" accesskey="A" onclick="performAddNew();">(A)dd New</button>
                                </xsl:when>
                                <xsl:otherwise>                           
                                  <button name="bAddNew" id="bAddNew" class="BlueButton" style="width:120;" disabled="true">(A)dd New</button>
                                </xsl:otherwise>                            
                              </xsl:choose>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>



        <!-- Header -->
        <!-- Note that the colors are defined at the TD level instead of Table level because we didnt want to display a different color table border -->
        <tr>
          <td align="left" valign="top">
            <table>
              <tr>
                <td>
                  <table width="1205" align="center" border="0" style="background-color:rgb(209,238,252);" >
                    <tr>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:45;"  >
                        <table>
                          <tr>
                            <td colspan="2" class="label" align="left"   style="background-color:rgb(209,238,252); width:45;">
                              <img border="0" src="images/all_check.gif" width="40" onclick="checkAll();"></img>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class="label" align="left"   style="background-color:rgb(209,238,252); width:45;">
                              <img border="0" src="images/all_uncheck.gif" width="40" onclick="uncheckAll();"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:65;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:55">Prod</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(1,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:55;">Status</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(1,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:65;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:55;">Period</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(2,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:55;">Status</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(2,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:52;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:42;">Prod</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(3,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:42;">Id</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(3,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:125;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:115;">&nbsp;</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(4,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:115;">Product Name</td>
                            <td class="label" align="left" style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(4,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:70;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:60;">Start</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(5,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:65;">Date</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(5,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:70;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:60;">End</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(6,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:65;">Date</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(6,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:65;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:55;">Vndr</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(7,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:55;">Id</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(7,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:90;" >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:80;">Vendor</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(8,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:80;">Name</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(8,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:60;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">&nbsp;</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(9,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">Fcst</td>
                            <td class="label" align="left" style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(9,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:60;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">&nbsp;</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(10,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">Rvsd</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(10,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:75;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:65;">Shipped<BR/>/</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(11,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:65;">Shipping</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(11,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:60;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">On</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(12,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">Hand</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(12,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:50;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:40;">Sell</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(13,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:40;">Thru<BR/>%</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(13,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:80;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:70;">Shutdown</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(14,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:70;">Threshold</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(14,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="label" align="center" style="background-color:rgb(209,238,252); width:60;"  >
                        <table>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">&nbsp;</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_up" src="images/sorted_up.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(15,'asc');"></img>
                            </td>
                          </tr>
                          <tr>
                            <td class="label" align="center" style="background-color:rgb(209,238,252); font-size:10; width:50;">Sales</td>
                            <td class="label" align="left"   style="background-color:rgb(209,238,252); width:10;">
                              <img id="sorted_down" src="images/sorted_down.gif" width="7" onmouseover="document.body.style.cursor = 'hand';" onmouseout="document.body.style.cursor = 'default';" onclick="sortFields(15,'desc');"></img>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <!-- Details -->
        <tr>
          <td align="left" valign="top">
            <div id="detailDiv" style="width:1230px; height:12em; overflow:auto;">
              <table>
                <tr>
                  <td>
                    <table width="1205" align="center" cellspacing="1" class="mainTable" border="1">
                      <!-- Detail Lines -->
                      <xsl:for-each select="search_results/search_result">
                        <tr><xsl:if test="vendor_name = 'ALL VENDORS'"><xsl:attribute name="style">background-color:lightgrey</xsl:attribute></xsl:if>
                          <td id="td_{position()-1}_00" align="center" style="width:50;">&nbsp;
                            <xsl:choose>
                              <xsl:when test="inv_trk_id != '0'">
                                <input type="checkbox" name="cb_{position()-1}" id="cb_{position()-1}"/>
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <input type="hidden" name="cbval_{position()-1}" id="cbval_{position()-1}" value="{inv_trk_id}"/>
                          <td id="td_{position()-1}_01" align="center" style="width:65;" >
                            <xsl:choose>
                              <xsl:when test="product_status = 'A' and exception_start_date = ''">
                                Available
                              </xsl:when>
                              <xsl:when test="product_status = 'A' and exception_start_date != ''">
                                Restricted
                              </xsl:when>
                              <xsl:when test="product_status = 'U' and product_updated_by != 'SYSTEM_INV_TRK'">
                                Unavailable
                              </xsl:when>
                              <xsl:when test="product_status = 'U' and product_updated_by = 'SYSTEM_INV_TRK'">
                                Shutdown
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td id="td_{position()-1}_02" align="center" style="width:65;" >
                          	
                          	<xsl:choose>
                          	
                          	 	<xsl:when test="vendor_name = 'ALL VENDORS'">
                          			<xsl:choose>
							<xsl:when test="number(on_hand) &lt;= number(shutdown_threshold_qty)"> Shutdown </xsl:when>
							<xsl:when test="inv_a_status = 'A'">
								<xsl:choose>
									<xsl:when test="period_shutdown = 'Y'">Shutdown</xsl:when>
									<xsl:otherwise>Available</xsl:otherwise>
								</xsl:choose> 
							</xsl:when>
							<xsl:when test="inv_x_status = 'X' and inv_u_status = 'U'">
								<xsl:choose>
									<xsl:when test="most_recent_status = 'U'">Unavailable</xsl:when>
									<xsl:otherwise>Shutdown</xsl:otherwise>
								</xsl:choose> 
							</xsl:when>
							<xsl:when test="inv_x_status = 'X' and inv_u_status != 'U'"> Shutdown </xsl:when> <!-- If Only Shutdown exists, Show Shutdown -->
							<xsl:when test="inv_u_status = 'U'"> Unavailable </xsl:when> <!-- If Only Unavailable exists, Show Unavailable -->
							<xsl:otherwise> &nbsp;</xsl:otherwise>						 
						</xsl:choose>
                          		</xsl:when> 
                          	
                          		<xsl:when test="vendor_name = 'PENDING'"> &nbsp; </xsl:when>
                          		
                          		<xsl:otherwise> 
                          			<xsl:choose>
										<xsl:when test="inv_status = 'A'"> Available </xsl:when>
										<xsl:when test="inv_status = 'X'"> Shutdown </xsl:when>
										<xsl:when test="inv_status = 'U'"> Unavailable </xsl:when>
										<xsl:otherwise> Unavailable </xsl:otherwise> <!-- It should be N/inactive otherwise-->
									</xsl:choose> 
                          		</xsl:otherwise>
                          		
                          	</xsl:choose> 
                          	
                          </td>
                          
                          <td id="td_{position()-1}_03" align="center" style="width:50;" >
                            <xsl:value-of select="product_id"/>
                          </td>
                          <td id="td_{position()-1}_04" align="center" style="width:150;">
                            <xsl:value-of select="product_name"/>
                          </td>
                          <td id="td_{position()-1}_05" align="center" style="width:75;" >
                            <xsl:value-of select="start_date"/>
                          </td>
                          <td id="td_{position()-1}_06" align="center" style="width:75;" >
                            <xsl:value-of select="end_date"/>
                          </td>
                          <td id="td_{position()-1}_07" align="center" style="width:60;" >
                            <xsl:choose>
                              <xsl:when test="vendor_id != ''">
                                <xsl:value-of select="vendor_id"/>
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td id="td_{position()-1}_08" align="center" style="width:90;" >
                            <xsl:value-of select="vendor_name"/>
                          </td>
                          <td id="td_{position()-1}_09" align="center" style="width:70;" >
                            <xsl:value-of select="forecast_qty"/>
                          </td>
                          <td id="td_{position()-1}_10" align="center" style="width:70;" >
                            <xsl:value-of select="revised_forecast_qty"/>
                          </td>
                          <td id="td_{position()-1}_11" align="center" style="width:85;" >
                            <xsl:value-of select="shipped_shipping"/>
                          </td>
                          <td id="td_{position()-1}_12" align="center" style="width:55;" >
                            <xsl:value-of select="on_hand"/>
                          </td>
                          <td id="td_{position()-1}_13" align="center" style="width:60;" >
                            <xsl:value-of select="format-number(sell_thru_percent, '#0.00','notANumber')"/>%
                          </td>
                          <td id="td_{position()-1}_14" align="center" style="width:90;" >
                            <xsl:choose>
                              <xsl:when test="shutdown_threshold_qty != ''">
                                <xsl:value-of select="shutdown_threshold_qty"/>
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td id="td_{position()-1}_15" align="center" style="width:70;" >
                            <xsl:choose>
                              <xsl:when test="vendor_name = 'ALL VENDORS'">
                                <a id="" href="#" onclick="javascript:doTakenPopup('{product_id}');" class="textlink">Taken</a>&nbsp;
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>                                                                                                                          
                  </td>                                                                                                                                
                </tr>                                                                                                                                  
              </table>                                                                                                                                
            </div>                                                                                                                                    
          </td>                                                                                                                                        
        </tr>                                                                                                                                          
                                                                                                                                                      
        <tr><td>&nbsp;</td></tr>                                                                                                                      
                                                                                                                                                      
        <tr>                                                                                                                                          
          <td align="left" valign="top">                                                                                                              
            <table>                                                                                                                                    
              <tr>                        
                <td>                                                                                                                                  
                  <table width="1205" align="center" cellspacing="1" border="0">
                    <tr>
                      <td>
                        <input type="checkbox" name="NOVATOR_UPDATE_LIVE" title="Send updates to Novator production web site"/>
                      </td>
                      <td>
                        <label class="radioLabel">Live</label>&nbsp;&nbsp;
                      </td>
                      <td>
                        <input type="checkbox" name="NOVATOR_UPDATE_CONTENT" title="Send updates to Novator content web site"/>
                      </td>
                      <td>
                        <label class="radioLabel">Content</label>&nbsp;&nbsp;
                      </td>
                      <td>
                        <input type="checkbox" name="NOVATOR_UPDATE_TEST" title="Send updates to Novator test web site"/>
                      </td>
                      <td>
                        <label class="radioLabel">Test</label>&nbsp;&nbsp;
                      </td>
                      <td>
                        <input type="checkbox" name="NOVATOR_UPDATE_UAT" title="Send updates to Novator UAT web site"/>
                      </td>
                      <td>
                        <label class="radioLabel">UAT</label>
                      </td>

                      <td>                                                                                                                            
                        <select name="sActionOptions" id="sActionOptions" onchange="">                                                                
                          <option value="coad">Clear Out Availability Dates</option>                                                                      
                          <option value="delete">Delete</option>                                                                                            
                          <option value="edit" selected="true">Edit</option>                                                                                              
                          <option value="ma">Make Available</option>                                                  
                          <option value="mu">Make Unavailable</option>                                                                                  
                        </select>                                                                                                                      
                      </td> 
                      
					 
					  <td width="100%">
						<div id="paginationDiv">
						<table id="paginationTable" width="98%" height="100%">
							<tr>								
								<td align="center">					   
									<span id="previousPage">
									<xsl:choose>
										<xsl:when test="key('pageData','currentPage')/value &gt; '1'">				
											<input type="image" src="images/backItem.gif" width="18" height="10" title="Go to previous page" onclick="performInvSearchLeft({$currPage});return false;"/>
										</xsl:when>
										<xsl:otherwise>
											<input disabled="true" type="image" src="images/backItem.gif" width="18" height="10"/>
										</xsl:otherwise>
									</xsl:choose> 									
									</span>               
									&nbsp;&nbsp;Showing <input type="text" name="showingPage" id="showingPage" maxlength="10" size="1" value = "{$currPage}" title="Enter input, then press Tab key or click outside the box" onblur="performInvSearchOnInput()" onkeypress="invalidateEnterKey(event)"/> of 
									<xsl:choose>
										<xsl:when test="key('pageData', 'totalPages')"> 							
											<xsl:value-of select="key('pageData','totalPages')/value"/> 
										</xsl:when>
										<xsl:otherwise>--</xsl:otherwise>
									</xsl:choose> &nbsp;&nbsp;									
									<span id="nextPage">
										<xsl:choose>
										<xsl:when test="key('pageData','currentPage')/value &lt; key('pageData','totalPages')/value">				
											<input type="image" src="images/nextItem.gif" width="18" height="10" title="Go to next page" onclick="performInvSearchRight({$currPage});return false;"/>
										</xsl:when>
										<xsl:otherwise>
											<input disabled="true" type="image" src="images/nextItem.gif" width="18" height="10"/>
										</xsl:otherwise>
									</xsl:choose>
									</span>
							</td>
							</tr>
							</table>
							</div>
						</td>
					                                                                                                                                               
                      <td align="right" valign="top">                                                                                                  
                        <xsl:choose>
                          <xsl:when test="key('pageData', 'updateAllowed')/value = 'Y'">
                            <button name="bGo"    id="bGo"       class="BlueButton" style="width:80;" accesskey="G" onclick="performGo();">(G)o</button>
                          </xsl:when>
                          <xsl:otherwise>                       
                            <button name="bGo"    id="bGo"       class="BlueButton" style="width:80;" disabled="true">(G)o</button>
                          </xsl:otherwise>                            
                        </xsl:choose>
                        <button name="bMainMenu"  id="bMainMenu" class="BlueButton" style="width:80;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>



      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
      </table>

      <script type="text/javascript">
        Calendar.setup
        (
          {
            inputField  : "tStartDate",    // ID of the input field
            ifFormat    : "mm/dd/y",       // the date format
            button      : "start_date_img" // ID of the button
          }
        );
        Calendar.setup
        (
          {
            inputField  : "tEndDate",       // ID of the input field
            ifFormat    : "mm/dd/y",       // the date format
            button      : "end_date_img"   // ID of the button
          }
        );
      </script>
    </form>
    <iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>