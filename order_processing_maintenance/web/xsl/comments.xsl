<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>


<xsl:template match="/">

<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"></META>
      <title>Comments</title>
	  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
    <link rel="stylesheet" type="text/css" href="css/communication.css"/>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
		<script>
		<![CDATA[


	function gotoPage(page){

				var vendorId = document.getElementById("vendorId").value;
        var commentNumbering = document.getElementById("comment_numbering").value;
          var url = '';
           url = "CommentsAction.do"+"?action_type=display"+ "&display_page=" + page  +"&comment_type=VENDOR_MAINT&vendor_id=" + vendorId + "&comment_numbering=" + commentNumbering +  getSecurityParams(false);
           performAction(url);
	
	
	}

  function replaceText( elementId, newString )
  {
      var node = document.getElementById( elementId );
      var newTextNode = document.createTextNode( newString );
      if(node.childNodes[0]!=null)
      {
        node.removeChild( node.childNodes[0] );
      }
      node.appendChild( newTextNode );
  }
	function movePage(startPage,moveAmt){

	var page = startPage + moveAmt;
	
				var vendorId = document.getElementById("vendorId").value;
          var url = '';
           url = "CommentsAction.do"+"?action_type=display"+ "&display_page=" + page  +"&comment_type=VENDOR_MAINT&vendor_id=" + vendorId + getSecurityParams(false);
           performAction(url);


	}


    function init()
    {
				var closePage = document.getElementById("closePage").value;
        if (closePage == "Y")
        {
          window.close();
        }
    }

		/*
		Using the specified URL submit the form
		*/
		function performAction(url)
		{
			document.forms[0].action = url;
      window.name = "commentsWindow";
      document.forms[0].target = window.name;
			document.forms[0].submit();
		}

 			function addComment(){
				var vendorId = document.getElementById("vendorId").value;
				var comment = document.getElementById("commentInput").value;
        if (comment != "")
        {
          var url = '';
           url = "CommentsAction.do"+"?action_type=add_comment"+"&comment_type=VENDOR_MAINT&vendor_id=" + vendorId +"&comment=" + comment + getSecurityParams(false);
           performAction(url);
        } else {
          replaceText('invalidDiv','COMMENT IS REQUIRED');
          return false;
        }
        
        return true;
      }
		 ]]>
		</script>


   </head>

<body onload="javascript:init()">
  <form name="comments" method="post" onsubmit="javascript:return addComment();">
   <input type="hidden" name="securitytoken" value="{//pagedata/securitytoken}"/>
   <input type="hidden" name="context" value="{//pagedata/context}"/>
   <input type="hidden" name="closePage" value="{//pagedata/close_page}"/>
   <input type="hidden" name="comment_numbering" value = "{//pagedata/comment_numbering}"/>
   
   <input type="hidden" name="comment_required" value="{//comment_required}"/>
   <input type="hidden" name="can_enter_comment" value="{//can_enter_comment}"/>

<!-- Header-->
   <table width="735" border="0" cellpadding="0" cellspacing="0" align="center">

   		<tr>

   			<td  align="center" valign="top" class="Header" id="pageHeader">
           		Comments
         	</td>

       </tr>
	</table>

<!-- Display table-->

   <table width="735" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
        	<table width="735" align="center" class="innerTable">
            <tr>
              <td class="label">
                Vendor ID: <xsl:value-of select="//pagedata/vendor_id"/><input type="hidden" name = "vendorId" value="{//pagedata/vendor_id}" />
              </td>
            </tr>

            <tr>
              <td>
                <table width="720" align="center">

                  <tr>
                    <td align="center">
                      <div style="width:662; border-left: 3px solid #000; border-right: 3px solid #000; border-top: 3px solid #000; border-bottom: 3px solid #000;">
                        <table width="655" border="0" cellpadding="2" cellspacing="0">
													<tr>
                            <th class="label" width="90" align="left">User ID</th>
														<th class="label" width="106" align="left">Date/Time</th>
														<th class="label" width="459" align="left">Comment</th>
                          </tr>
												</table>
											</div>
											<div style="overflow:auto; width:662; height=200; border-left: 3px solid #000; border-right: 3px solid #000; border-bottom: 3px solid #000;">
                        <table width="655" border="0" cellpadding="2" cellspacing="0">
                          <xsl:for-each select="//comment">
                            <tr>
                              <td align="left" width="90">
                                <xsl:if test="//comment_numbering='Y'">
                                  <xsl:value-of select="rnum"/>)&nbsp;
                                </xsl:if>
                                <xsl:value-of select = "created_by"/>
															</td>
															<td align="left" width="106">
                                <xsl:call-template name="formatDate">
                                  <xsl:with-param name="dateTimeString" select="created_on"/>
                                </xsl:call-template>
															</td>
															<td align="left" width="455">
                                <xsl:value-of select="comment_description"/>
															</td>
														</tr>
                          </xsl:for-each>
												</table>
											</div>
										</td>
									</tr>

									<tr>
                    <td align="center">
                      Page <xsl:value-of select="//display_page"/> of <xsl:value-of select="//OUT_TOTAL_PAGES"/>
										</td>
									</tr>
									<tr>
										<td align="center">

            					<xsl:choose>
              					<xsl:when test="//display_page != 1">
                					<button name="firstItem" tabindex="1" class="arrowButton" id="firstItem" onclick="gotoPage(1);">7</button>
												</xsl:when>
												<xsl:otherwise>
													<button name="firstItem" disabled="true" tabindex="1" class="arrowButton" id="firstItem" onclick="">7</button>
												</xsl:otherwise>
											</xsl:choose>

											<xsl:choose>
                  			<xsl:when test="//display_page > 1">
                    			<button name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="movePage({//display_page},-1);">3</button>
                      	</xsl:when>
												<xsl:otherwise>
                        	<button name="backItem" disabled="true" tabindex="1" class="arrowButton" id="backItem" onclick="">3</button>
												</xsl:otherwise>
											</xsl:choose>
													
											<xsl:choose>
                        <xsl:when test="//display_page &lt; //OUT_TOTAL_PAGES">													
                          <button name="nextItem" tabindex="1" class="arrowButton" id="nextItem" onclick="movePage({//display_page},1);">4</button>
												</xsl:when>
                        <xsl:otherwise>
                          <button name="nextItem" disabled="true" tabindex="1" class="arrowButton" id="nextItem" onclick="">4</button>													
                        </xsl:otherwise>
											</xsl:choose>

											<xsl:choose>
                        <xsl:when test="//OUT_TOTAL_PAGES != ''">
                          <xsl:choose>
                            <xsl:when test="//display_page != //OUT_TOTAL_PAGES">
                              <button name="lastItem" tabindex="1" class="arrowButton" id="lastItem" onclick="gotoPage({//OUT_TOTAL_PAGES});">8</button>
                            </xsl:when>
                            <xsl:otherwise>
                              <button name="lastItem" disabled="true" tabindex="1" class="arrowButton" id="lastItem" onclick="">8</button>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <button name="lastItem" disabled="true" tabindex="1" class="arrowButton" id="lastItem" onclick="">8</button>
                        </xsl:otherwise>
											</xsl:choose>

										</td>
									</tr>
                  <xsl:if test="//pagedata/can_enter_comment = 'Y'">
									<tr>
                    <td class="label">Comment:</td>
									</tr>

									<tr>
                    <td align="center">
                      <xsl:choose>
                        <xsl:when test="//pagedata/limit_comment_size = 'Y'">
                          <input type="text" name="commentInput" size="120"  maxlength="{//pagedata/comment_max_size}"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <textarea  id="commentInput" rows="2" cols="120" wrap="virtual" align="center">
                          </textarea>
                        </xsl:otherwise>
                      </xsl:choose>
                      <div id="invalidDiv" class="ErrorMessage">
                      
                      </div>                    

          					</td>
                  </tr>
                </xsl:if>
							</table>

						</td>
					</tr>
							
					<tr>
						<td  align="right" valign="top" colspan="4">
              <xsl:if test="//pagedata/comment_required = 'N'">
    					 <button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10" accesskey="C" onclick="javascript:window.close();">(C)lose </button>
              </xsl:if>
              <xsl:if test="//pagedata/can_enter_comment = 'Y'">
  						 <button class="BlueButton" style="width:80;" name="mainMenu" type="submit" accesskey="S" tabindex="10">(S)ave </button>
	            </xsl:if>
  					</td>
          </tr>

			</table>
					</td>
				</tr>





		  	</table>
   

   <table width="735" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
      	 <td>
			 <table>
			 	<tr>
				 <td  align="left">

				 </td>
				 </tr>
			 </table>
		  </td>
		  <td align="right">

		</td>
      </tr>
</table>




</form>
</body>
</html>

</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
		</xsl:call-template>
	</xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
    <xsl:variable name="hours" select="number(substring($dateTimeString, 12, 2))" />
    <xsl:choose>
      <xsl:when test="number($hours) = number('0')">
        <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>&#160;12<xsl:value-of select="substring($dateTimeString, 14, 6)" /> AM
      </xsl:when>
      <xsl:when test="number($hours) &gt; number('12')">
        <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>&#160;<xsl:value-of select ="$hours mod 12"/><xsl:value-of select="substring($dateTimeString, 14, 6)" /> PM
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>&#160;<xsl:value-of select ="$hours"/><xsl:value-of select="substring($dateTimeString, 14, 6)" /> AM
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>