<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Miles Points Redemption Rate Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/mercuryMessage.js"></script>

<script type="text/javascript">
var liveFeedAllowed = null;
var liveFeedOn = null;
var contentFeedAllowed = null;
var contentFeedOn = null;
var testFeedAllowed = null;
var testFeedOn = null;
var uatFeedAllowed = null;
var uatFeedOn = null;
liveFeedAllowed = '<xsl:value-of select="root/novator_update_setup/production_feed_allowed"/>';
liveFeedOn = '<xsl:value-of select="root/novator_update_setup/production_feed_checked"/>';
contentFeedAllowed = '<xsl:value-of select="root/novator_update_setup/content_feed_allowed"/>';
contentFeedOn = '<xsl:value-of select="root/novator_update_setup/content_feed_checked"/>';
testFeedAllowed = '<xsl:value-of select="root/novator_update_setup/test_feed_allowed"/>';
testFeedOn = '<xsl:value-of select="root/novator_update_setup/test_feed_checked"/>';
uatFeedAllowed = '<xsl:value-of select="root/novator_update_setup/uat_feed_allowed"/>';
uatFeedOn = '<xsl:value-of select="root/novator_update_setup/uat_feed_checked"/>';


<![CDATA[

var deleteArray = new Array();
var idArray = new Array();
var paymentMethodArray = new Array();
var arrayCount = 0;


function init()
{
setupCheckBox('NOVATOR_UPDATE_LIVE',liveFeedAllowed,liveFeedOn);
setupCheckBox('NOVATOR_UPDATE_CONTENT',contentFeedAllowed,contentFeedOn);
setupCheckBox('NOVATOR_UPDATE_TEST',testFeedAllowed,testFeedOn);
setupCheckBox('NOVATOR_UPDATE_UAT',uatFeedAllowed,uatFeedOn);

]]>
<xsl:if test="key('pageData','update_allowed')/value = 'N'">
document.getElementById('addCode').disabled = true;
document.getElementById('deleteCode').disabled = true;
</xsl:if>

<xsl:for-each select="root/mp_redemption_rates/mp_redemption_rate">
  document.maint.idList.value = document.maint.idList.value + '<xsl:value-of select="mp_redemption_rate_id"/>' + ",";
</xsl:for-each>

<xsl:for-each select="root/mp_payment_methods/mp_payment_method">
  document.maint.paymentMethodIds.value = document.maint.paymentMethodIds.value + '<xsl:value-of select="payment_method_id"/>' + ",";
  paymentMethodArray['<xsl:value-of select="@num" />'] = '<xsl:value-of select="payment_method_id"/>';
</xsl:for-each>

<![CDATA[
}

function doMainMenuAction()
{
    document.maint.action = "MPRedemptionRateMaintAction.do";
    document.maint.action_type.value = "MainMenuAction";
    document.maint.submit();
}

function popDeleteError(position, sourceCodesExist)
{
    thisCheckBox = document.getElementById('delete_' + position);
    if(thisCheckBox.checked && sourceCodesExist == 'Y') {
        alert('You must remove the redemption rate from the source code before deleting.');
        thisCheckBox.checked = false;
    }
}

function doDeleteAction()
{
    document.maint.deleteValues.value = "";
    for (i=0;i<deleteArray.length;i++) {
        isChecked = document.getElementById(deleteArray[i]).checked;
        if (isChecked) {
            if (document.maint.deleteValues.value == "") {
                document.maint.deleteValues.value = idArray[i];
            } else {
                document.maint.deleteValues.value = document.maint.deleteValues.value + "," + idArray[i];
            }
        }
    }
    if (document.maint.deleteValues.value == "") {
        alert("No record was selected for deletion.");
        return;
    }
    msg = "Are you sure?";
    if (doExitPageAction(msg)) {
            document.maint.action = "MPRedemptionRateMaintAction.do";
            document.maint.action_type.value = "deleteRedemptionRate";
            document.maint.submit();
    }
    
}

function popEdit(mpRedemptionRateId, paymentMethodId, description, mpRedemptionRateAmt, action, requestedBy)
{
    if (document.maint.updateAllowed.value != "Y") {
        action = "View";
    }
    var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
    var popURL = "mpRedemptionRatePop.html?id=" + mpRedemptionRateId;
    popURL = popURL + "&mp_rd_payment_method_id=" + paymentMethodId;
    popURL = popURL + "&mp_rd_description=" + escape(description);
    popURL = popURL + "&mp_rd_rate_amt=" + mpRedemptionRateAmt;
    popURL = popURL + "&action=" + action;
    popURL = popURL + "&requestedBy=" + escape(requestedBy);
    popURL = popURL + "&idList=" + document.maint.idList.value;
    popURL = popURL + "&paymentMethodIds=" + document.maint.paymentMethodIds.value; 
    popURL = popURL + "&liveFeedAllowed=" + liveFeedAllowed;
    popURL = popURL + "&liveFeedOn=" + liveFeedOn;
    popURL = popURL + "&contentFeedAllowed=" + contentFeedAllowed;
    popURL = popURL + "&contentFeedOn=" + contentFeedOn;
    popURL = popURL + "&testFeedAllowed=" + testFeedAllowed;
    popURL = popURL + "&testFeedOn=" + testFeedOn;
    popURL = popURL + "&uatFeedAllowed=" + uatFeedAllowed;
    popURL = popURL + "&uatFeedOn=" + uatFeedOn;
    
    var results=showModalDialog(popURL, "", modal_dim);
    if (results && results != null) {
        showWaitMessage("redemptionRateDiv", "wait", "Processing");
        document.maint.mp_rd_rate_id.value = results[1];
        document.maint.mp_rd_description.value = results[2];
        document.maint.mp_rd_payment_method_id.value = results[3];
        document.maint.mp_rd_rate_amt.value = results[4];
        document.maint.mp_rd_requested_by.value = results[5];
        document.maint.NOVATOR_UPDATE_LIVE.checked = results[6];
        document.maint.NOVATOR_UPDATE_CONTENT.checked = results[7];
        document.maint.NOVATOR_UPDATE_TEST.checked = results[8];
        document.maint.NOVATOR_UPDATE_UAT.checked = results[9];

        document.maint.action = "MPRedemptionRateMaintAction.do";

        if (action == "Add") {
            document.maint.action_type.value = "addRedemptionRate";
        } else {
            document.maint.action_type.value = "saveRedemptionRate";
        }
        document.maint.submit();
    }
}

function popSourceView(mpRedemptionRateId)
{
    var modal_dim = "dialogWidth:500px; dialogHeight:350px; center:yes; status:0; help:no; scroll:no";
    var popURL = "MPRedemptionRateMaintAction.do?mp_rd_rate_id=" + mpRedemptionRateId;
    popURL = popURL + "&action_type=getSourceCodeList";
    popURL = popURL + "&securitytoken=" + document.maint.securitytoken.value;
    popURL = popURL + "&context=" + document.maint.context.value;
    var results=showModalDialog(popURL, "", modal_dim);
    if (results && results != null) {
        alert(results);
    }
}

function popHistoryView(redemptionRateId, description)
{
    var modal_dim = "dialogWidth:650px; dialogHeight:450px; center:yes; status:0; help:no; scroll:no";
    var popURL = "MPRedemptionRateMaintAction.do?mp_rd_rate_id=" + redemptionRateId;
    popURL = popURL + "&action_type=getHistory";
    popURL = popURL + "&mp_rd_description=" + escape(description);
    popURL = popURL + "&securitytoken=" + document.maint.securitytoken.value;
    popURL = popURL + "&context=" + document.maint.context.value;
    var results=showModalDialogIFrame(popURL, "", modal_dim);
    if (results && results != null) {
        alert(results);
    }
}



]]>
</script>
</head>

<body onload="javascript:init();">

<table width="750" border="0" align="center">
    <tr>
        <td width="30%">
        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </td>
        <td width="40%" align="center" valign="center">
        <font class="Header">Redemption Rate Maintenance</font>
        </td>
        <td  id="time" align="right" valign="center" class="Label" width="30%">
        <script type="text/javascript">startClock();</script>
        </td>
    </tr>
</table>

<form name="maint" method="post">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
<input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
<input type="hidden" name="paymentMethodIds" value=""/>
<input type="hidden" name="deleteValues" value=""/>
<input type="hidden" name="mp_rd_rate_id" value=""/>
<input type="hidden" name="mp_rd_description" value=""/>
<input type="hidden" name="mp_rd_payment_method_id" value=""/>
<input type="hidden" name="mp_rd_rate_amt" value="" />
<input type="hidden" name="idList" value="" />
<input type="hidden" name="calledFrom" value="redemptionRate" />
<input type="hidden" name="mp_rd_requested_by" value="" />

<table width="750" align="center" cellspacing="1">
    <tr>
        <td align="right">
        <button accesskey="M" class="BlueButton" style="width:75;" name="MainMenu" tabindex="1" onclick="javascript:doMainMenuAction();">(M)ain Menu</button>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<table width="750" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
        <div id="redemptionRateDiv">

            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr>
                            <xsl:choose>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveSuccessful'">
                                    <td class="OperationSuccess">
                                    Redemption Rate&nbsp;<xsl:value-of select="key('pageData','mp_redemption_rate_id')/value"/>&nbsp;was successfully saved.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveFailed'">
                                    <td class="errorMessage">
                                    Request to update Redemption Rate &nbsp;<xsl:value-of select="key('pageData','mp_redemption_rate_id')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'novatorConnect'">
                                    <td class="errorMessage">
                                    Cannot connect to Novator
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'novatorDEN'">
                                    <td class="errorMessage">
                                    Novator has denied the request
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'deleteSuccessful'">
                                    <td class="OperationSuccess">
                                    Redemption Rate&nbsp;<xsl:value-of select="key('pageData','mp_redemption_rate_id')/value"/>&nbsp;was successfully deleted.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'deleteFailed'">
                                    <td class="errorMessage">
                                    Request to delete Redemption Rate&nbsp;<xsl:value-of select="key('pageData','mp_redemption_rate_id')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td class="errorMessage">
                                    <xsl:value-of select="key('pageData','admin_action')/value"/></td>
                                </xsl:otherwise>
                            </xsl:choose>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div style="padding-right: 16px; width: 725;">																													
                                    <table width="725" border="0" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <th class="TotalLine" width="50" align="center">Delete</th>
                                            <th class="TotalLine" width="25" align="center">ID</th>
                                            <th class="TotalLine" width="175" align="center">Description</th>
                                            <th class="TotalLine" width="75" align="right">Payment<br/>Type</th>
                                            <th class="TotalLine" width="75" align="right">Redemption<br/>Rate</th>
                                            <th class="TotalLine" width="90" align="center">Source Codes/<br/>Member Levels</th>
                                            <th class="TotalLine" width="75" align="center">History</th>
                                            <th class="TotalLine" width="25">&nbsp;</th>
                                        </tr>
                                    </table>
                                    </div>
                                    <div style="width: 725; overflow:auto; overflow-x:hidden; padding-right: 16px; height: 270px;">																													
                                    <table width="725" border="0" cellpadding="2" cellspacing="0">	
                                        <xsl:for-each select="root/mp_redemption_rates/mp_redemption_rate">							                                  		
                                            <tr>
                                                <td align="center" width="50">
                                                    <input type="checkbox" name="delete_{position()}" value="{mp_redemption_rate_id}" tabindex="{position()}" onclick="javascript:popDeleteError('{position()}', '{source_codes_exist}')" />
                                                </td>
                                                <td align="center" width="25">
                                                    <a href="javascript:popEdit('{mp_redemption_rate_id}','{payment_method_id}','{description}','{mp_redemption_rate_amt}','Edit','{requested_by}')" tabindex="{position()}">
                                                    <xsl:value-of select="mp_redemption_rate_id" />
                                                    </a>
                                                </td>
                                                <td align="center" width="175">
                                                    <a href="javascript:popEdit('{mp_redemption_rate_id}','{payment_method_id}','{description}','{mp_redemption_rate_amt}','Edit','{requested_by}')" tabindex="{position()}">
                                                    <xsl:value-of select="description" />
                                                    </a>
                                                </td>
                                                <td align="right" width="75">
                                                    <a href="javascript:popEdit('{mp_redemption_rate_id}','{payment_method_id}','{description}','{mp_redemption_rate_amt}','Edit','{requested_by}')" tabindex="{position()}">
                                                    <xsl:value-of select="payment_method_id" />
                                                    </a>
                                                </td>
                                                <td align="right" width="75">
                                                    <a href="javascript:popEdit('{mp_redemption_rate_id}','{payment_method_id}','{description}','{mp_redemption_rate_amt}','Edit','{requested_by}')" tabindex="{position()}">
                                                    <xsl:value-of select="mp_redemption_rate_amt" />
                                                    </a>
                                                </td>
                                                <td align="center" width="100">
                                                    <xsl:choose>
                                                        <xsl:when test="source_codes_exist = 'Y'">
                                                            <a href="javascript:popSourceView('{mp_redemption_rate_id}')" tabindex="{position()}">
                                                            <font color="0000ff">View</font>
                                                            </a>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <font class="ErrorMessage">None</font>
<script type="text/javascript">
    deleteArray[arrayCount] = "delete_" + <xsl:value-of select="@num" />
    idArray[arrayCount] = '<xsl:value-of select="mp_redemption_rate_id" />'
    arrayCount = arrayCount + 1;
</script>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                <td align="center" width="75">
                                                    <a href="javascript:popHistoryView('{mp_redemption_rate_id}', '{description}')" tabindex="{position()}">
                                                    <font color="0000ff">View</font>
                                                    </a>
                                                </td>
                                                <td width="25">&nbsp;</td>
                                            </tr>                 		
                                        </xsl:for-each>
                                    </table>																
                                    </div>	
                                    <table width="725" border="0" cellpadding="2" cellspacing="0">
                                      <tr><td align="center">
                                        <label id="toLiveLabel">Delete from Novator: </label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_LIVE"
                                              title="Send updates to Novator production web site"/>
                                        <label class="radioLabel">Live</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_CONTENT"
                                              title="Send updates to Novator content web site"/>
                                        <label class="radioLabel">Content</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_TEST"
                                              title="Send updates to Novator test web site"/>
                                        <label class="radioLabel">Test</label>&nbsp;&nbsp;
                                        <input type="checkbox"
                                              name="NOVATOR_UPDATE_UAT"
                                              title="Send updates to Novator UAT web site"/>
                                        <label class="radioLabel">UAT</label>
                                      </td></tr>
                                    </table>                                    
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <button accesskey="A" class="BlueButton" style="width:50;" name="AddCode" tabindex="98" onclick="javascript:popEdit('','','','','Add','');">(A)dd</button>
                        &nbsp;&nbsp;
                        <button accesskey="D" class="BlueButton" style="width:150;" name="DeleteCode" tabindex="99" onclick="javascript:doDeleteAction();">(D)elete Selected</button>
                    </td>
                </tr>
            </table>

        </div>
        </td>
    </tr>
</table>
</form>

<div id="waitDiv" style="display:none">
    <table id="waitTable" width="750" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
            <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                        <td id="waitTD" width="50%" class="waitMessage"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<!-- call footer template-->
<xsl:call-template name="footer"/>
</body>
</html>
</xsl:template>  
</xsl:stylesheet>
