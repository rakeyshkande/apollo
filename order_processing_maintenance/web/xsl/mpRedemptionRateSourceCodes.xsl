<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Redemption Rate Source Codes</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>

<script type="text/javascript">

function doCancelAction()
{
    window.close();
}

</script>
</head>

<body>

<center>

<h2>
Source Codes/Member Levels for<br/> Redemption Rate&nbsp;
<xsl:value-of select="key('pageData','mpRedemptionRateId')/value"/>
</h2>

	<table width="350" align="center" cellspacing="1" class="mainTable">
	  <tr>
		<td>
			<div id="contentd" class="innerTableDiv">
				<table border="1" width="100%" align="center" class="innerTableCollapse">
					<tr>
						<th class="TotalLine" width="150" align="center">Source Codes</th>
						<th class="TotalLine" width="270" align="center">Member Levels</th>
					</tr>
					<xsl:for-each select="root/source_codes/source_code">
						<tr>
							<td align="center" class="innerTableTd">
								<xsl:value-of select="source_code"/>
							</td>
							<td align="center" class="innerTableTd">
								<xsl:choose>
									<xsl:when test="mp_member_level_id != ''">
										<xsl:value-of select="mp_member_level_id" />
									</xsl:when>
									<xsl:otherwise>&nbsp;
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</div>
			<table width="98%" align="center" cellpadding="5" cellspacing="1">
				<tr>
					<td align="center">
						<button accesskey="C" class="BlueButton" style="width:50;"
							name="Cancel" tabindex="99" onclick="javascript:doCancelAction();">(C)lose</button>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</center>

</body>
</html>
</xsl:template>  
</xsl:stylesheet>
