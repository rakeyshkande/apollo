<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>


<xsl:template match="/">


<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"></META>
      <title>Vendor Maintenance</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" src="js/copyright.js"></script>
	  
		<script>
		<![CDATA[

    var PAGE_URL = '';

 		function performActionWithLock(url)
 		{
      PAGE_URL = url;
      doRecordLock('VENDOR_INVENTORY','retrieve', null); 
  	}
              
  	function performAction(url)
		{
      PAGE_URL = url;
      doContinueProcess();
		}

    function doContinueProcess(){
      document.forms[0].action = PAGE_URL;
      document.forms[0].target = window.name;
      document.forms[0].submit();
    }
            
    function checkAndForward(forwardAction){
      doContinueProcess();
    }

		/*
		Using the specified URL submit the form
		*/
		function performAction(url)
		{
			document.forms[0].action = url;
			document.forms[0].submit();
		}

		function doInventoryClick(productID, inventoryLevel)
		{

       document.getElementById("lock_id").value = productID;
		   var vendorId = document.getElementById("vendor_id").value;

			//does an inventory record exist?
			if(inventoryLevel == "NONE")
			{
				var updateFlag = document.getElementById("can_update").value;
				if(updateFlag == "Y")
				{
					if(doExitPageAction("No inventory record for product.  Do you want to add it?"))
					{
						var url = "VendorInventoryControlAction.do" +
							   "?action_type=display" + "&from_page=product_list" + "&update_type=insert" + "&product_id=" + productID + "&edit_mode=display" + getSecurityParams(false);
            performActionWithLock(url);
					}
					else
					{
						//stay on this page
					}
				}
				else
				{
					alert("No inventory record for product.");
				}

			}
			else
			{
				var url = "VendorInventoryControlAction.do" +
						   "?action_type=display"  + "&from_page=product_list" + "&update_type=update" + "&product_id=" + productID + "&edit_mode=display" + getSecurityParams(false);
        performActionWithLock(url);
			}
		}


		function displayVendor(){

				var vendorId = document.getElementById("vendor_id").value;
        var edit_mode = document.getElementById("edit_mode").value;

				var url = "VendorDetailAction.do" +"?action_type=display_vendor" + "&vendor_id=" + vendorId + "&edit_mode=" + edit_mode + getSecurityParams(false);

				performAction(url);

			}
      function clickMainMenu()
      {
        var url = "MainMenuAction.do" +  getSecurityParams(true);
        performAction(url);
      }



		 ]]>
		</script>

   </head>
<body>
	<form name="vendormaint" method="post"  >
	<xsl:call-template name="securityanddata"/>
	<Input TYPE="HIDDEN" NAME="can_update" value='{//root/pagedata/can_update}'></Input>	
	<Input TYPE="HIDDEN" NAME="vendor_id" value='{//root/pagedata/vendor_id}'></Input>	
	<Input TYPE="HIDDEN" NAME="lock_id"/>
  <input type="hidden" name="edit_mode" value="{//pagedata/edit_mode}"/>
<!-- Header-->
   <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
   		<tr>
   			<td width="30%" height="33" valign="top">
   				<div class="floatleft">
   					<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
   				</div>
   			</td>
   			<td></td>
            <td  align="right" width="5%" valign="bottom">

            </td>
        </tr>
   		<tr>
   			<td></td>
   			<td  align="center" valign="top" class="Header" id="pageHeader">
           		Vendor Product List
         	</td>
			<td  id="time" align="right" valign="bottom" class="Label" width="20%" height="30">
			  <script type="text/javascript">startClock();</script>
			</td>
       </tr>
	</table>

<!-- Display table-->
   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
        	<table width="100%" align="center" class="innerTable">

				<tr>
					<td>
						<table width="100%" align="center">


							<tr>
							<td align="center" class="TotalLine" colspan="4">
								<div align="center"><strong>Inventory Management</strong><br></br>
								  </div>
								<div align="left"></div>
							</td>
							</tr>
							<tr><td> </td></tr>
							<tr>
							<td align="center">

								<div style="width:50%; border-left: 3px solid #000; border-right: 3px solid #000; border-top: 3px solid #000; border-bottom: 3px solid #000;">
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<th class="label" width="15%" align="center">Product SKU</th>
											<th class="label" width="50%" align="center">Product Name</th>
											<th class="label" width="12%" align="center">Available</th>
											<th class="label" width="10%" align="center">Inventory Count</th>
                                                                                        <th class="label" width="13%" align="center">Zone Jump Eligible</th>
										</tr>
									</table>
								</div>
								<div style="overflow:auto; width:50%; height=200; border-left: 3px solid #000; border-right: 3px solid #000; border-bottom: 3px solid #000;">
									<table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">

										<xsl:for-each select="root/products/product">

											<tr>
												<td align="center" width="15%">
													<xsl:value-of select="product_id"/>
												</td>
												<td align="left" width="50%">
													<xsl:value-of select="product_name"/>
												</td>
												<td align="center" width="12%">
													<xsl:value-of select="status"/>
												</td>
												<td align="center" width="10%">
													<xsl:choose>
													<xsl:when test="string-length(inventory_level) = 0">
													[<a href="#" onclick="doInventoryClick('{product_id}','NONE');">0</a>]
													</xsl:when>
													<xsl:otherwise>
													[<a href="#" onclick="doInventoryClick('{product_id}','{inventory_level}');"><xsl:value-of select="inventory_level"/></a>]													
													</xsl:otherwise>
													</xsl:choose>
												</td>
                                                                                                <td align="center" width="13%">
                                                                                                    <xsl:choose>
                                                                                                        <xsl:when test="zone_jump_eligible_flag = 'Y'">
                                                                                                            <xsl:value-of select="zone_jump_eligible_flag"/>
                                                                                                        </xsl:when>
                                                                                                    </xsl:choose>
												</td>
											</tr>
										</xsl:for-each>


									  </table>
								</div>

							</td>
							</tr>

						</table>
					</td>
				</tr>





		  	</table>
       </td>
     </tr>


	</table>

   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
      	 <td>
			 <table>
			 	<tr>
				 <td  align="left">
					 
				 </td>
				 </tr>
			 </table>
		  </td>
		  <td align="right">
			 <table>
			 	<tr>
				 	<td  align="right" valign="top">
					 <button class="BlueButton" style="width:80;" name="back" tabindex="10" onclick="displayVendor()">(B)ack </button>
				 	</td>
				</tr>
				<tr>
					<td align="right" valign="top">
				 	<button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10" onclick="clickMainMenu()">(M)ain Menu </button>
				 	</td>
				</tr>
			 </table>
		</td>
      </tr>
</table>



<!--Copyright bar-->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
      <td></td>
   </tr>
   <tr>
      <td class="disclaimer"></td><script>showCopyright();</script>
   </tr>
</table>

</form>
<iframe id="checkLock" width="0px" height="0px" border="0"/>
</body>
</html>

</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
		</xsl:call-template>
	</xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
	<xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
      <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>
	</xsl:template>
</xsl:stylesheet>