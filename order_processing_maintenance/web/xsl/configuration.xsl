<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>

<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<xsl:variable name="selectedContext" select="key('pageData','selected_context')/value"/>

<html>
<head>
    <title>FTD - System Configuration</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <link rel="stylesheet" type="text/css" href="css/tooltip.css"/>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" src="js/copyright.js"></script>
	  
    <script type="text/javascript" language="javascript"><![CDATA[


//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;
var checkcntr = 0;

//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
	setScrollingDivHeight();
	window.onresize = setScrollingDivHeight;
}


//****************************************************************************************************
//* setScrollingDivHeight
//****************************************************************************************************
function setScrollingDivHeight()
{
	var configDiv = document.getElementById("configListing");
	var newHeight = document.body.clientHeight - configDiv.getBoundingClientRect().top - 100;
	if (newHeight > 15)
		configDiv.style.height = newHeight;
}


//****************************************************************************************************
//*  Events
//****************************************************************************************************
function doValueBlur(input)
{
	if (input.defaultValue == input.value)
	{
		setButtonActionAccess(true);
		document.getElementById("id_" + input.key).checked = false;
	}
	else
	{
		somethingChange = true;
		setButtonActionAccess(false);
		document.getElementById("id_" + input.key).checked = true;
	}
}


//****************************************************************************************************
//*  Description input field control
//****************************************************************************************************
function toggleDescriptionInput(inputfield)
{
	if (document.getElementById(inputfield).style.display == "inline")
	{
		document.getElementById(inputfield).style.display = "none";
	}
	else
	{
		document.getElementById(inputfield).style.display = "inline";
	}
}


//****************************************************************************************************
//* Button control
//****************************************************************************************************
function checkActionAccess(input)
{
	if (input.checked == false)
	{
		setButtonActionAccess(true);
	}
	else
	{
		setButtonActionAccess(false);
	}
}


//****************************************************************************************************
//* setButtonActionAccess
//****************************************************************************************************
function setButtonActionAccess(access)
{
	var form = document.forms[0];
	if (access == false)
	{
		checkcntr++;
	}
	else
	{
		checkcntr--;
	}

	if (checkcntr > 0)
	{
		form.changesButton.disabled = false;
	}
	else
	{
		form.changesButton.disabled = true;
		checkcntr = 0;
	}
}


//****************************************************************************************************
//* doAction
//****************************************************************************************************
function doAction(action)
{

	//set the message
	var errorMessage = "Are you sure?";

	//get a true/false value from the modal dialog.
	var ret = displayConfirmError(errorMessage);

	if (ret)
	{
		var url = "ConfigurationAction.do" + getSecurityParams(true) + "&action_type=" + action;
		performAction(url);
	}
}


//****************************************************************************************************
//* doExitAction
//****************************************************************************************************
function doExitAction()
{
	if (somethingChanged)
	{
		var errorMessage = "Data changes were detected on this page. Are you sure want to exit with saving?";

		//get a true/false value from the modal dialog.
		var ret = displayConfirmError(errorMessage);

		if (ret)
		{
			var url = "ConfigurationAction.do" + getSecurityParams(true) + "&action_type=exit";
			performAction(url);
		}
	}
	else
	{
		var url = "ConfigurationAction.do" + getSecurityParams(true) + "&action_type=exit";
		performAction(url);
	}
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	document.forms[0].action = url;
	document.forms[0].submit();
}


    ]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Merchandising Parameters'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="SystemConfigurationForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('pageData', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('pageData', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('pageData', 'adminAction')/value}"/>
    <input type="hidden" name="selected_context" value="{key('pageData','selected_context')/value}"/>


    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- System Configuration -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="5%" class="colHeaderCenter"></td>
              <td width="30%" class="colHeaderCenter">Context:&nbsp;
								<xsl:value-of select="$selectedContext"/>
              </td>
              <td width="30%" class="colHeaderCenter">Name</td>
              <td width="30%" class="colHeaderCenter">Value</td>
            </tr>
            <tr>
              <td colspan="5" width="100%" align="center">

                <!-- Scrolling div contiains system messages -->
                <div id="configListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <xsl:for-each select="GLOBAL_PARMS/GLOBAL_PARM">
                      <xsl:sort select="@num" data-type="number"/>
                      <tr>
                        <td width="5%" align="center">
                          <input type="checkbox" name="id_{position()}" key="{position()}" onclick="javascript:checkActionAccess(this);"/>
                          <input type="hidden" name="context_{position()}" value="{context}"/>
                          <input type="hidden" name="name_{position()}" value="{name}"/>
                        </td>
                        <td width="30%" align="center"><xsl:value-of select="context"/></td>
                        <td width="30%" align="center">
                          <a href="#" class="tooltip" onclick="javascript:toggleDescriptionInput('descIn_{position()}');return false;">
                          <xsl:value-of select="name"/>
                          <span><xsl:choose>
                            <xsl:when test="string-length(description) > 0"><xsl:value-of select="description"/></xsl:when>
                            <xsl:otherwise>No description defined (click to add)</xsl:otherwise>
                            </xsl:choose>
                          </span></a>
                        </td>
                        <td width="30%">
                          <input type="text" name="value_{position()}" key="{position()}" value="{value}" size="60" maxlength="150" onblur="javascript:doValueBlur(this);fieldBlur();" onclick="javascript:fieldFocus();"/>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3" align="left"><div id="descIn_{position()}" style="display: none">
                        <input type="text" name="desc_{position()}" key="{position()}" value="{description}" size="180" maxlength="255" onblur="javascript:doValueBlur(this);fieldBlur();" onclick="javascript:fieldFocus();"/>
                        </div></td>
                      </tr>
                    </xsl:for-each>
                    <tr><td>&nbsp;</td></tr>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
				<td align="right">
					<button class="bigBlueButton" name="changesButton" id="changesButton" tabindex="1" disabled="true" accesskey="S" onclick="javascript:doAction('update');">(S)ave</button>
				</td>
        <td align="left">
          <button type="button" class="bigBlueButton" name="exitButton" id="exitButton" tabindex="2" accesskey="E" onclick="javascript:doExitAction();">(E)xit</button>
        </td>
      </tr>
    </table>

  </form>

	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="disclaimer"></td><script>showCopyright();</script>
			
		</tr>
	</table>

</body>
</html>

</xsl:template>
</xsl:stylesheet>