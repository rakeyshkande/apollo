<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="commonUtil.xsl"/>
  <!-- Keys -->
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:template match="/">
    <HTML>
      <TITLE>FTD - Vendor Carrier Maintenance</TITLE>
      <HEAD>
        <META http-equiv="Content-Type" content="text/html">
        </META>
        <title>Vendor Maintenance</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/clock.js"/>
        <script type="text/javascript" src="js/FormChek.js"/>
        <script type="text/javascript" src="js/cityLookup.js"/>
        <script type="text/javascript" src="js/popup.js"/>
        <script type="text/javascript" src="js/util.js"/>
        <script type="text/javascript" src="js/commonUtil.js"/>
        <script type="text/javascript" src="js/calendar.js"/>
        <script language="JavaScript">
          <![CDATA[    
            var PAGE_URL = '';
            var has_changed = false;
            
            /*
             * If any changes were made to this page this flag is set to true.
             * We make no data checks here the server will handle that. 
             * We simply check if a user modified any fields in anyway.
             */
            function doOnChangeAction()
            {	
              has_changed = true;
            }
            
            function doOnClickAction()
            {	
              has_changed = true;
            }
            
            function doBlurAction()
            {	
              has_changed = true;
            }
            
            function doCheckOnKeyDown()
            {
              has_changed = true;
            }
            
            function init() 
            {
              validateDates();
              
              
              if (!isEmptyDates())
              {
                updateTotalUsage("ExceptionUsage");
              }
              
              updateTotalUsage("Usage");
              
              var sav = document.getElementById("saved");

              if (sav.value == "true")
              {
                msg("ErrorMessage", "the record has been updated");
              }
            }
            
            function validateAll() 
            {
              if (validateUsages("Usage"))
              {
                updateTotalUsage("Usage");
                if (!isEmptyDates())
                {
                  if (validateDates() && validateUsages("ExceptionUsage"))
                  {
                    updateTotalUsage("ExceptionUsage");
                  }
                }
              }
            }
            
            function submitData() 
            {
              var isValidException = true;
              
              if(isEmptyDates())
              {
                clearRange();
              }
              else
              {
                isValidException = validateDates() && 
                  validateUsages("ExceptionUsage") && 
                  100 == getTotalUsage("ExceptionUsage");
              }
              
              var isValid = false;
              
              if (isValidException)
              {
                isValid = validateUsages("Usage") && 100 == getTotalUsage("Usage");
              }
              
              if(isValid && isValidException)
              {
                document.forms.editCarrier.action = "CarrierAction.do";
                document.forms.editCarrier.action_type.value = "edit_carrier";
                document.forms.editCarrier.submit();
              }
            }

            function isEmptyDates() 
            {
              var startDateText = document.getElementById("overrideStartDate");
              var endDateText = document.getElementById("overrideEndDate");
              
              var startDate = startDateText.value;
              var endDate = endDateText.value;
              
              return isEmptyString(startDate) && isEmptyString(endDate);
            }

            function validateDates()
            {
              var startDateText = document.getElementById("overrideStartDate");
              var endDateText = document.getElementById("overrideEndDate");
              
              var startDate = startDateText.value;
              var endDate = endDateText.value;
              
              if (isEmptyDates())
              {
                startDateText.style.backgroundColor = "White";
                endDateText.style.backgroundColor = "White";
                return true;
              } 
              else if (isEmptyString(startDate)) 
              {
                startDateText.style.backgroundColor = "Pink";
                endDateText.style.backgroundColor = "White";
                msg("ErrorMessage", "Start Date must exist");
                return false;
              }
              else if (isEmptyString(endDate))
              { 
                startDateText.style.backgroundColor = "White";
                endDateText.style.backgroundColor = "Pink";
                msg("ErrorMessage", "End Date must exist");
                return false;
              }
              
              var startDateObject = new Date();
              var startDay = startDate.substring(startDate.indexOf("/")+1, startDate.lastIndexOf("/"));
              var startMonth = startDate.substring(0, startDate.indexOf("/"));
              var startYear = startDate.substring(startDate.lastIndexOf("/") + 1);
              
              startDateObject.setFullYear(startYear,startMonth-1,startDay);

              var today = new Date();

              var endDateObject = new Date();
              var endDay = endDate.substring(endDate.indexOf("/")+1, endDate.lastIndexOf("/"));
              var endMonth = endDate.substring(0, endDate.indexOf("/"));
              var endYear = endDate.substring(endDate.lastIndexOf("/") + 1);
              
              endDateObject.setFullYear(endYear,endMonth-1,endDay);

              if (startDateObject < today)
              {
                startDateText.style.backgroundColor = "Pink";
                endDateText.style.backgroundColor = "White";
                msg("ErrorMessage", "Start Date cannot be before today");
                return false;
              }
              else if (startDateObject > endDateObject)
              {
                startDateText.style.backgroundColor = "Pink";
                endDateText.style.backgroundColor = "Pink";
                msg("ErrorMessage", "Start Date must be before End Date");
                return false;
              }
              else
              {
                startDateText.style.backgroundColor = "White";
                endDateText.style.backgroundColor = "White";
                msg("ErrorMessage", "");
                
                var counter = 1;
                while (true) 
                {
                  var currentBox = document.getElementById("ExceptionUsage" + counter);
              
                  if (null == currentBox)
                  {
                    break;
                  }
                  
                  if (isEmptyString(currentBox.value))
                  {
                    currentBox.value = 0;
                  }
                  
                  currentBox.disabled = false;
                
                  counter++;
                }
                
                updateTotalUsage("ExceptionUsage");
                
                return true;
              }
            }

            function updateTotalUsage(name)
            {
              var totalUsageText = document.getElementById("Total" + name);
              var totalUsage = getTotalUsage(name);
              totalUsageText.value = totalUsage;
                
              if (100 == totalUsage) 
              {
                totalUsageText.style.backgroundColor = "White";
                msg("ErrorMessage", "");
              }
              else
              {
                totalUsageText.style.backgroundColor = "Pink";
                msg("ErrorMessage", "Total Usage must be 100%");
              }
            }
              
            function isInteger(s)
            {
              var intRegExp = /^\d+$/;
              
              if (intRegExp.test(s))
              {
                return true;
              }

              return false;
            }
              
            function validateUsages(name) {
              var counter = 1;
              var isValid = true;
              
              while (true) 
              {
                var currentBox = document.getElementById(name + counter);
              
                if (null == currentBox)
                {
                  break;
                }
              
                isValid = validateUsageBox(currentBox);
                
                if (!isValid)
                {
                  isValid = false;
                  break;
                }
                
                counter++;
              }
            
              return isValid;
            }
              
            function validateUsageBox(textBox) 
            {
              var isValid = true;
              
              // Trim white space
              textBox.value = trim(textBox.value);
            
              // If text field is empty, replace with 0
              if (isEmptyString(textBox.value))
              {
                textBox.value = "0";
              }
            
              // Error is a non-numeric input
              if (!isInteger(textBox.value)) 
              {
                msg("ErrorMessage", textBox.value + " is not a valid integer");
                setfocus(textBox);
                textBox.style.backgroundColor = "Pink";
                isValid = false;
              }
              else
              {
                var num = parseInt(textBox.value, 10);

                var carrier = document.getElementById(textBox.name + "CarrierName");

                if (num <= 0 && "FedEx" == carrier.value)
                {
                  msg("ErrorMessage", "FedEx value must be greater than zero");
                  setfocus(textBox);
                  textBox.style.backgroundColor = "Pink";
                  isValid = false;
                }
                else if (100 < num || 0 > num)
                {
                  msg("ErrorMessage", textBox.value + " is not between 0 and 100");
                  setfocus(textBox);
                  textBox.style.backgroundColor = "Pink";
                  isValid = false;
                }
                else
                {  
                  // For valid input, update total usage
                  updateTotalUsage(textBox.id.substring(0,textBox.id.length-1));
                  textBox.style.backgroundColor = "White";
                  isValid = true;
                }
              }
              
              return isValid;
            }
              
            function isEmptyString(string)
            {
              var eString = /^\s*$/
              
              if (eString.test(string))
              {
                return true;
              }
              
              return false;
            }
            
            function getTotalUsage(name)
            {
              var totalUsage = 0;
              var position = 1;
                
              while (true) 
              {
                var keyIDName = "KeyID" + position;
                var keyID = document.getElementById(keyIDName);
                  
                if (null == keyID) 
                {
                  break;
                }

                var usageName = name + position;
                var usage = document.getElementById(usageName).value;
                totalUsage += parseInt(usage, 10);
                position++;
              }
                
              return totalUsage;
            }
            
            function checkAndForward(forwardAction)
            {
              doContinueProcess();
            }
            
            function goBack() 
            {
              if(!has_changed ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var origin = document.getElementById("origin").value;
              
                if (origin == "dashboard")
                {
                  var url = "VendorCarrierDashboardAction.do" + getSecurityParams(true); 
                  performActionWithUnlock(url);
                } 
                else
                {
                  var vendorId = document.getElementById("vendor_id").value;

                  var url = "VendorDetailAction.do" +
                    "?action_type=display_vendor" + "&vendor_id=" + vendorId + "&edit_mode=edit" + getSecurityParams(false);
                  performActionWithLock(url);
                }
                
                
              }
            }
            
            function performActionWithLock(url)
            {
              PAGE_URL = url;
              doRecordLock('VENDOR','check', null); 
  
              doContinueProcess();
            }

            function clickMainMenu()
            {
              if(!has_changed ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var url = "MainMenuAction.do" +  getSecurityParams(true);
                performActionWithUnlock(url);
              }
            }

            function performActionWithUnlock(url)
            {
              PAGE_URL = url;
              doRecordLock('VENDOR','unlock', null); 
              doContinueProcess();
          	}

            function editVendor()
            {
              if(!has_changed ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
              {
                var vendorId = document.getElementById("vendor_id").value;

                var url = "VendorDetailAction.do" +
                  "?action_type=display_vendor" + "&vendor_id=" + vendorId + "&edit_mode=edit" + getSecurityParams(false);
              
                performActionWithLock(url);
              }
            }
            
            function doContinueProcess()
            {
              document.forms[0].action = PAGE_URL;
              document.forms[0].target = window.name;
              document.forms[0].submit();
            }
            
            function numbersonly(e)
            {
              var unicode=e.charCode? e.charCode : e.keyCode
              if (unicode!=8)
              { 
                //if the key isn't the backspace key (which we should allow)
                if (unicode<48||unicode>57) //if not a number
                  return false //disable key press
              }
            }

            var nbsp = 160;    // non-breaking space char
            var node_text = 3; // DOM text node-type
            var emptyString = /^\s*$/
            var glb_vfld;      // retain vfld for timer thread

            // -----------------------------------------
            //                  trim
            // Trim leading/trailing whitespace off string
            // -----------------------------------------
            function trim(str)
            {
              return str.replace(/^\s+|\s+$/g, '');
            }

            // -----------------------------------------
            //                  setfocus
            // Delayed focus setting to get around IE bug
            // -----------------------------------------
            function setFocusDelayed()
            {
              glb_vfld.select()
            }

            function setfocus(vfld)
            {
              // save vfld in global variable so value retained when routine exits
              glb_vfld = vfld;
              setTimeout( 'setFocusDelayed()', 100 );
            }

            // -----------------------------------------
            //                  msg
            // Display warn/error message in HTML element
            // commonCheck routine must have previously been called
            // -----------------------------------------
            function msg(msgtype, // class to give element ("warn" or "error")
                         message) // string to display
            {
              // setting an empty string can give problems if later set to a 
              // non-empty string, so ensure a space present. (For Mozilla and Opera one could 
              // simply use a space, but IE demands something more, like a non-breaking space.)
              var dispmessage;
             
              if (emptyString.test(message)) 
              {
                dispmessage = String.fromCharCode(nbsp);    
              }
              else  
              {
                dispmessage = message;
              }
              var elem = document.getElementById("inf_total");
              elem.firstChild.nodeValue = dispmessage;  
  
              elem.className = msgtype;   // set the CSS class to adjust appearance of message
            }
            
            function clearRange() 
            {
              // Clear Start Date field
              var elem = document.getElementById("overrideStartDate");
              elem.value = "";
              elem.style.backgroundColor = "White";
              
              // Clear End Date field
              elem = document.getElementById("overrideEndDate");
              elem.value = "";
              elem.style.backgroundColor = "White";
            
              var name = "ExceptionUsage";
              var counter = 1;
              
              while (true) 
              {
                var currentBox = document.getElementById(name + counter);
              
                if (null == currentBox)
                {
                  break;
                }
              
                currentBox.value = "";
                currentBox.style.backgroundColor = "White";
                currentBox.disabled = true;
                
                counter++;
              }
              
              elem = document.getElementById("TotalExceptionUsage");
              elem.value = "";
              elem.style.backgroundColor = "White";
            }
          ]]>
        </script>
      </HEAD>
      <BODY onload="init()">
        <!-- Header-->
        <table width="98%" border="0" cellpadding="0" cellspacing="0"
               align="center">
          <TR>
              <TD align="left" width="20%">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131"
                     height="32"/>
              </TD>
              <TD class="header" align="center" width="60%">Vendor Carrier Maintenance </TD>
              <TD class="label" align="right" width="20%">
                <TABLE cellSpacing="0" cellPadding="0" width="100%">
                  <TBODY>
                    <TR>
                      <TD class="label" id="time" align="right">
                      </TD>
                      <SCRIPT type="text/javascript">
                        startClock();
                      </SCRIPT>
                    </TR>
                    <TR>
                      <TD align="right">&nbsp;</TD>
                    </TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>
        </table>
        <FORM name="editCarrier" method="post">
          <xsl:call-template name="securityanddata"/>
          <input type="hidden" name="action_type"/>
          <input type="hidden" name="lock_id" value="{//shipping_keys/shipping_key/vendor_id}"/>
          <input type="hidden" name="saved" value="{//shipping_keys/vendor_detail/saved}"/>
          <INPUT type="hidden" name="origin" id="origin" value="{//shipping_keys/vendor_detail/origin}"/>
          <input type="hidden" name="vendor_name" id="vendor_name" value="{//shipping_keys/vendors/vendor/vendor_name}"/>
          <input type="hidden" name="vendor_code" id="vendor_code" value="{//shipping_keys/vendors/vendor/member_number}"/>
          <TABLE width="98%" align="center" cellspacing="1" class="mainTable">
            <TBODY>
              <tr>
                <td>
                  <table width="100%" align="center" cellspacing="3">
                   <tr>
                      <td align="center" class="TotalLine">
                        <div align="center">
                          <strong>Vendor Information</strong>
                          <br/>
                        </div>
                        <div align="left">
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" align="center">
                          <tr>
                            <td class="label" align="right">Vendor ID Number:
                            </td>
                            <td><xsl:value-of select="shipping_keys/shipping_key/vendor_id"/>
                            </td>
                            <td class="label" align="right">Vendor Name:
                            </td>
                            <td>
                              <xsl:value-of select="shipping_keys/vendors/vendor/vendor_name"/>
                            </td>
                            <td class="label" align="right">Vendor Code:
                            </td>
                            <td>
                              <xsl:value-of select="shipping_keys/vendors/vendor/member_number"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td align="center"  class="TotalLine">
                        <div align="center">
                          <strong>Available Shipping Services</strong>
                          <br/>
                        </div>
                        <div align="left">
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table align="center">
                    <tr>
                      <td valign="top" align="center">
                        <table align="center" cellspacing="1" class="mainTable">
                          <TR>
                            <td style="text-align:center" class="label">Default</td>
                          </TR>
                          <tr>
                            <td align="center" class="TotalLine">Service</td>
                            <td align="center" class="TotalLine">Usage %</td>
                          </tr>
                          <xsl:for-each select="shipping_keys/shipping_key">
                            <tr>
                              <td>
                                <input type="hidden" name="KeyID{position()}" id="KeyID{position()}">
                                  <xsl:attribute name="value"><xsl:value-of select="carrier_id"/></xsl:attribute>
                                </input>
                                <input type="hidden" name="vendor_id" id="vendor_id">
                                  <xsl:attribute name="value"><xsl:value-of select="vendor_id"/></xsl:attribute>
                                </input>
                                <input type="hidden" name="Usage{position()}CarrierName" id="Usage{position()}CarrierName">
                                  <xsl:attribute name="value"><xsl:value-of select="carrier_name"/></xsl:attribute>
                                </input>
                                <xsl:value-of select="carrier_name"/>
                              </td>
                              <td>
                                <input type="text" name="Usage{position()}" id="Usage{position()}" maxlength="3">
                                  <xsl:attribute name="value"><xsl:value-of select="usage_percent"/></xsl:attribute>
                                  <xsl:attribute name="onchange">doOnChangeAction();validateAll();</xsl:attribute>
                                  <xsl:attribute name="onkeypress">doCheckOnKeyDown();return numbersonly(event)</xsl:attribute>
                                  <xsl:attribute name="tabindex"><xsl:value-of select="position()"/></xsl:attribute>
                                </input>
                              </td>
                              <TD id="inf_usage{position()}">
                                &nbsp;
                              </TD>
                            </tr>
                          </xsl:for-each>
                          <tr>
                            <td>
                              Total Usage (%)
                            </td>
                            <td>
                              <input type="text" name="TotalUsage" id="TotalUsage" tabindex="9">
                                <xsl:attribute name="readonly"/>
                              </input>
                            </td>
                          </tr>
                          <tr>
                            <!--<td colspan="2">
                              <input name="" type="checkbox"
                                     value="isFedExGround"/>FedEx Ground/Home</td>-->
                          </tr>
                        </table>
                      </td>
                      <td width="60" align="center">&nbsp;
                      </td>
                      <TD valign="top" align="center" width="50%">
                        <TABLE class="mainTable" cellSpacing="1" align="center">
                          <TBODY>
                            <TR>
                              <td style="text-align:center" class="label">Exception</td>
                              <td style="text-align:center"><a href="#" tabindex="10" onclick="clearRange();doOnClickAction();">clear range</a></td>
                            </TR>
                            <TR>
                              <TD class="TotalLine" align="center">Dates</TD>
                              <TD class="TotalLine" align="center" colspan="2">&nbsp;</TD>
                            </TR>
                            <TR>
                              <TD class="label">Start</TD>
                              <td>
                                <input name="overrideStartDate" type="text" onblur="validateDates();doBlurAction();" tabindex="11">
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="substring(//override_start,1,10)" />
                                  </xsl:attribute>
                                  <xsl:attribute name="readonly"/>
                                </input>
                              </td>
                              <!--td><img id="overrideStartDateImg" src="images/calendar.gif" width="24" height="22" align="middle" border="0" /></td-->
                            </TR>
                            <TR>
                              <TD class="label">End</TD>
                              <td>
                                <input name="overrideEndDate" type="text" onblur="validateDates();doBlurAction();" tabindex="12">
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="substring(//override_end,1,10)" />
                                  </xsl:attribute>
                                  <xsl:attribute name="readonly"/>
                                </input>
                              </td>
                              <!--td><img id="overrideEndDateImg" src="images/calendar.gif" width="24" height="22" align="middle" border="0"/></td-->
                            </TR>
                            <TR>
                              <TD class="TotalLine" align="center">Service</TD>
                              <TD class="TotalLine" align="center" colspan="2">Usage %</TD>
                            </TR>
                            <xsl:for-each select="shipping_keys/shipping_key">
                              <tr>
                                <td>
                                  <input type="hidden" name="ExceptionKeyID{position()}" id="ExceptionKeyID{position()}">
                                    <xsl:attribute name="value"><xsl:value-of select="carrier_id"/></xsl:attribute>
                                  </input>
                                  <input type="hidden" name="vendor_id" id="vendor_id">
                                    <xsl:attribute name="value"><xsl:value-of select="vendor_id"/></xsl:attribute>
                                  </input>
                                  <input type="hidden" name="ExceptionUsage{position()}CarrierName" id="ExceptionUsage{position()}CarrierName">
                                    <xsl:attribute name="value"><xsl:value-of select="carrier_name"/></xsl:attribute>
                                  </input>
                                  <xsl:value-of select="carrier_name"/>
                                </td>
                                <td>
                                  <input type="text" name="ExceptionUsage{position()}" id="ExceptionUsage{position()}" maxlength="3">
                                    <xsl:attribute name="value"><xsl:value-of select="override_percentage"/></xsl:attribute>
                                    <xsl:attribute name="onchange">doOnChangeAction();validateAll();</xsl:attribute>
                                    <xsl:attribute name="onkeypress">doCheckOnKeyDown();return numbersonly(event)</xsl:attribute>
                                    <xsl:attribute name="tabindex"><xsl:value-of select="position()+12"/></xsl:attribute>
                                    <xsl:attribute name="disabled"/>
                                  </input>
                                </td>
                                <TD id="inf_usage{position()}">
                                  &nbsp;
                                </TD>
                              </tr>
                            </xsl:for-each>
                            <TR>
                              <TD>Total Usage (%) </TD>
                              <TD>
                                <input tabindex="30" type="text" name="TotalExceptionUsage" id="TotalExceptionUsage">
                                  <xsl:attribute name="readonly"/>
                                </input> 
                              </TD>
                            </TR>
                          </TBODY>
                        </TABLE>
                      </TD>
                    </tr>
                    <!--<tr>
                      <td align="center" class="TotalLine">
                        <div align="center">
                          <strong>FedEx Ground/Home Meter Closings</strong>
                          <br/>
                        </div>
                        <div align="left">
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table align="center" border="0" cellpadding="3">
                          <tr>
                            <td align="center" valign="top">
                              <table align="center" cellspacing="1"
                                     class="mainTable">
                                <tr>
                                  <td colspan="2" class="TotalLine"
                                      align="center">Current Schedule</td>
                                </tr>
                                <tr>
                                  <td align="center">(central time zone)</td>
                                </tr>
                                <tr>
                                  <td align="center" valign="top">
                                    <select name="meterClosings" size="3">
                                      <option>2:00 pm</option>
                                      <option>10:00 pm</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2" align="center">
                                    <input name="deleteClosingButton"
                                           type="button" value="Delete Selected"/>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td valign="top">
                              <table align="center" cellspacing="1"
                                     class="mainTable">
                                <tr>
                                  <td colspan="2" class="TotalLine"
                                      align="center">Add New Closing </td>
                                </tr>
                                <tr>
                                  <td colspan="2" align="center">(central time zone)</td>
                                </tr>
                                <tr>
                                  <td class="Label">Hour:</td>
                                  <td>
                                    <select name="hourCombo" size="1">
                                      <option value="0">Midnight</option>
                                      <option value="1">1:00 am</option>
                                      <option value="2">2:00 am</option>
                                      <option value="3">3:00 am</option>
                                      <option value="4">4:00 am</option>
                                      <option value="5">5:00 am</option>
                                      <option value="6">6:00 am</option>
                                      <option value="7">7:00 am</option>
                                      <option value="8">8:00 am</option>
                                      <option value="9">9:00 am</option>
                                      <option value="10">10:00 am</option>
                                      <option value="11">11:00 am</option>
                                      <option value="12">Noon</option>
                                      <option value="13">1:00 pm</option>
                                      <option value="14">2:00 pm</option>
                                      <option value="15">3:00 pm</option>
                                      <option value="16">4:00 pm</option>
                                      <option value="17">5:00 pm</option>
                                      <option value="18">6:00 pm</option>
                                      <option value="19">7:00 pm</option>
                                      <option value="20">8:00 pm</option>
                                      <option value="21">9:00 pm</option>
                                      <option value="22">10:00 pm</option>
                                      <option value="23">11:00 pm</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="Label">Minute:</td>
                                  <td width="100%">
                                    <select name="minuteCombo" size="1">
                                      <option value="0">00</option>
                                      <option value="15">15</option>
                                      <option value="30">30</option>
                                      <option value="45">45</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="2 px">
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2" align="center">
                                    <input name="deleteClosingButton"
                                           type="button" value="Add Closing"/>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr> -->
                  </table>
                </td>
              </tr>
              <tr>
                <TD align="center" id="inf_total">
                  &nbsp;
                </TD>
              </tr>
              <tr>
                <td align="right" valign="top" colspan="4">
                  <button class="BlueButton" style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px" tabindex="37" accesskey="S" onclick="javascript:submitData()">(S)ave</button>
                </td>
              </tr>
            </TBODY>
          </TABLE>
          <TABLE cellSpacing="2" cellPadding="2" width="98%" align="center"
                 border="0">
            <TBODY>
              <TR>
                <TD width="10%">
                </TD>
              <td align="right">
                <table>
                  <tr>
                    <td align="right" valign="top">
                      <button class="BlueButton" style="width:80;" accesskey="B"
                              name="back" tabindex="38"
                              onclick="javascript:goBack()">(B)ack </button>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" valign="top">
                      <button class="BlueButton" style="width:80;" accesskey="M"
                              name="mainMenu" tabindex="39"
                              onclick="javascript:clickMainMenu()">(M)ain Menu </button>
                    </td>
                  </tr>
                </table>
                </td>
              </TR>
            </TBODY>
          </TABLE>
          <xsl:call-template name="footer"/>
        </FORM>
        <iframe id="checkLock" width="0px" height="0px" border="0"/>
      </BODY>
        <script type="text/javascript">
          Calendar.setup(
            {
              inputField  : "overrideStartDate",  // ID of the input field
              ifFormat    : "mm/dd/y",  // the date format
              button      : "overrideStartDate"  // ID of the button
            }
          );
          
          Calendar.setup(
            {
              inputField  : "overrideEndDate",  // ID of the input field
              ifFormat    : "mm/dd/y",  // the date format
              button      : "overrideEndDate"  // ID of the button
            }
          );
  </script>
    </HTML>
  </xsl:template>
  <xsl:template name="addHeader">
    <xsl:call-template name="header">
      <xsl:with-param name="showBackButton" select="true()"/>
      <xsl:with-param name="showPrinter" select="true()"/>
      <xsl:with-param name="showSearchBox" select="false()"/>
      <xsl:with-param name="searchLabel" select="'Order #'"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
    </xsl:call-template>
  </xsl:template>
  <!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
    <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/
    <xsl:value-of select="substring($dateTimeString, 9, 2)"/>/
    <xsl:value-of select="substring($dateTimeString, 1, 4)"/>
  </xsl:template>
</xsl:stylesheet>
