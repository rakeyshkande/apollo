<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>


<xsl:template match="/root">



<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <title>Add On Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/AddOnDashboard.js"></script>
    <script type="text/javascript" src="js/FormChek.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>
    <script type="text/javascript" src="js/copyright.js"></script>
  </head>
  <body onload="javascript: init()">
    <form name="fAddOnDashboard" method="post">

      <input type="hidden" name="securitytoken"           id="securitytoken"     value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"                 id="context"           value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"             id="adminAction"       value="{key('pageData', 'adminAction')/value}"/>
      <input type="hidden" name="action_type"             id="action_type"/>
      
      <table width="955" border="0" >
        <tr>
          <td width="20%" align="left" valign="center">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td width="60%" align="center" valign="center" class="Header" id="pageHeader">Add On Dashboard</td>
          <td width="20%">&nbsp;</td>
        </tr>
      </table>
      

      <!-- Main Div -->
      <div id="content" style="display:block">
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td align="left" valign="top">
            <table>
              <tr>
                <td>
                  <table width="1260" align="center" cellspacing="1" border="0">
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="right" valign="top">
                        <button name="bMainMenu" id="bMainMenu" class="BlueButton" style="width:80;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr><td>&nbsp;</td></tr>

        <!-- Header -->
        <!-- Note that the colors are defined at the TD level instead of Table level because we didnt want to display a different color table border -->
        <tr>
          <td align="left" valign="top">
            <table>
              <tr>
                <td>
                  <table width="955" align="left" border="0" style="background-color:rgb(209,238,252); table-layout:fixed">
                    <tr>
                      <td align="left" style="background-color:rgb(209,238,252); width:040;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:040;">          </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:040;">Select                   </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:065;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:065;">Add-on    </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:065;">ID                       </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:120;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:120;">Add-on    </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:120;">Type                     </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:165;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:165;">Add-on    </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:165;">Description              </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:315;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:315;">          </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:315;">Add-on Text              </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:070;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">          </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">Price $                  </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:070;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">Product   </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">Id                       </td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:060;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:060;">Add-on    </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:060;">Delivery Type            </td>    </tr>    </table>   </td>
                      <!-- DI-4: Add-on Dashboard Enhancements : Added two columns PQuad Accessory ID and FTD-West Addon-->
                      <td align="left" style="background-color:rgb(209,238,252); width:200;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">			</td>   </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">PQuad Accessory ID		</td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:070;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">FTD-West	</td>   </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;">Addon					</td>    </tr>    </table>   </td>
                      <td align="left" style="background-color:rgb(209,238,252); width:060;">    <table>   <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:060;">          </td>    </tr>    <tr>   <td align="left" style="background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:060;">Available                </td>    </tr>    </table>   </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>


        <!-- Details -->
        <tr>
          <td align="left" valign="top">
            <div id="detailDiv" style="width:1300; height:15em; overflow:auto;">
              <table>
                <tr>
                  <td>
                    <table width="955" align="center" cellspacing="1" class="mainTable" border="1" style='table-layout:fixed'>
                      <!-- Detail Lines -->
                      <xsl:for-each select="add_ons/add_on/AddOnVO">
                        <tr>
                          <xsl:if test="position() mod 2 = 0">
                            <xsl:attribute name="style">background-color:#D3D3D3;</xsl:attribute>
                          </xsl:if>
                          <td align="left" style="width:040;"><input type="radio" id="addOnIdRadio" name="addOnIdRadio" value="{addOnId}"/></td>
                          <td align="left" style="width:065;">
                            <xsl:value-of select="addOnId"/>
                            <xsl:if test="defaultPerTypeFlag = 'true'"><span style="color:red">*</span></xsl:if>
                          </td>
                          <td align="left" style="width:120;word-break:break-all;">
                            <xsl:value-of select="addOnTypeDescription"/>
                          </td>
                          <td align="left" style="width:165;word-break:break-all;">
                            <xsl:if test="string-length(addOnDescription/text()) = 0">&nbsp;</xsl:if>
                            <xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>
                          </td>
                          <td align="left" style="width:315;word-break:break-all;">
                            <xsl:if test="string-length(addOnText/text()) = 0">&nbsp;</xsl:if>
                            <xsl:value-of select="addOnText" disable-output-escaping="yes" />
                          </td>
                          <td align="left" style="width:070;">
                            <xsl:value-of select='format-number(addOnPrice, "#,###,##0.00")' />
                          </td>
                          <td align="left" style="width:070;">
                            <xsl:if test="string-length(productId/text()) = 0">&nbsp;</xsl:if>
                            <xsl:value-of select="productId"/>
                          </td>
                           <!-- #18729 - Flexible Fullfillment - Addon Dash board and Maintanance changes 
              added new column Add-on Delivery Type which will fetch the delivery type of the Addon(florist/dropship/both). -->
                           <td align="left" style="width:060;">
							<xsl:choose>
								<xsl:when test="addOnDeliveryType = 'florist'">&nbsp;Florist
								</xsl:when>
								<xsl:when test="addOnDeliveryType = 'dropship'">&nbsp;Dropship
								</xsl:when>
								<xsl:when test="addOnDeliveryType = 'both'">&nbsp;Both
								</xsl:when>
								<xsl:otherwise>
									N
								</xsl:otherwise>
							</xsl:choose>
                          </td>
                          <!-- DI-4: Add-on Dashboard Enhancements : Added two columns PQuad Accessory ID and FTD-West Addon-->
                          <td align="left" style="width:200;">
                            <xsl:if test="string-length(sPquadAccsryId/text()) = 0">&nbsp;</xsl:if>
                            <xsl:value-of select="sPquadAccsryId"/>
                          </td>
                          <td align="left" style="width:070;">
                            <xsl:if test="string-length(isFtdWestAddon/text()) = 0">&nbsp;</xsl:if>
                            <xsl:choose>
                            	<xsl:when test="isFtdWestAddon = 'true'">Yes</xsl:when>
                            	<xsl:otherwise>No</xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td align="left" style="width:060;">
                          <xsl:choose>
                          <xsl:when test="addOnAvailableFlag = 'true'">Y</xsl:when>
                          <xsl:otherwise>N</xsl:otherwise></xsl:choose>
                          
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>                                                                                                                          
                  </td>                                                                                                                                
                </tr>                                                                                                                                  
              </table>                                                                                                                                
            </div>                                                                                                                                    
          </td>                                                                                                                                        
        </tr>                                                                                                                                          
                                                                                                                                                      

        <tr>
          <td align="left" valign="top">
            <table>
              <tr>
                <td>
                  <table width="1260" align="center" cellspacing="1" border="0">

                    <tr><td colspan="2">&nbsp;</td></tr>                                                                                                                      
                    <tr><td><span style="color:red">*</span>&nbsp;Default&nbsp;Card</td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>                                                                                                                      

                    <tr>
                      <td align="left" valign="top">
                        <xsl:choose>
                          <xsl:when test="key('pageData', 'updateAllowed')/value = 'Y'">
                            <button name="bAdd"  id="bAdd"  class="BlueButton" style="width:80;" accesskey="A" onclick="performAdd();">(A)dd</button>
                            <button name="bEdit" id="bEdit" class="BlueButton" style="width:80;" accesskey="E" onclick="performEdit();">(E)dit</button>
                          </xsl:when>
                          <xsl:otherwise>                           
                            <button name="bAdd"  id="bAdd"  class="BlueButton" style="width:80;" disabled="true">(A)dd</button>
                            <button name="bEdit" id="bEdit" class="BlueButton" style="width:80;" disabled="true">(E)dit</button>
                          </xsl:otherwise>                            
                        </xsl:choose>
                      </td>
                      <td align="right" valign="top">
                        <button name="bMainMenu" id="bMainMenu" class="BlueButton" style="width:80;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
                                                                                                                                                      
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>



      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>          
          <td class="disclaimer"></td><script>showCopyright();</script>
        </tr>
      </table>

    </form>
    <script type="text/javascript" language="javascript">
      <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->
      <xsl:if test = "key('pageData', 'errorMessage')/value != ''">
         displayOkError('<xsl:value-of select = "key('pageData', 'errorMessage')/value"/>');
      </xsl:if>
    </script>
  </body>
</html>
</xsl:template>
</xsl:stylesheet>
