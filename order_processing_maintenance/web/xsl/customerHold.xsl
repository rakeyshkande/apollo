<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />
	
<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>
	
<xsl:template match="/">
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"></META>
      <title>Occasion Code Maintenance</title>
	  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonutil.js"></script>
	  <script type="text/javascript" src="js/calendar.js"></script>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" src="js/FormChek.js"></script>
	  <script type="text/javascript" src="js/copyright.js"></script>

	  <script>
	  
	  		function isChecked()
	  		{
	  			if(document.getElementById("cust_num_hold_chk").checked)
	  			{
	  				return true;
	  			}
	  			
	  			if(document.getElementById("cust_addr_hold_chk").checked)
	  			{
	  				return true;
	  			}
	  		
	  			if(document.getElementById("cust_name_hold_chk").checked)
	  			{
	  				return true;
	  			}
	  			
	  			var i = 0
	  			var obj = null;
	  			
	  			obj =document.getElementById("cust_card_hold_value" + i);
	  			i = 1;
	  			while(obj != null)
	  			{
	  				if(document.getElementById("cust_card_hold_chk" + i).checked)
	  				{
	  					return true;
	  				}
	  			}
		  		
	  			obj =document.getElementById("cust_phone_hold_value" + i);
	  			i = 1;
	  			while(obj != null)
	  			{
	  				if(document.getElementById("cust_phone_hold_chk" + i).checked)
	  				{
	  					return true;
	  				}
	
	  			
	  			obj =document.getElementById("cust_email_hold_value" + i);
	  			i = 1;
	  			while(obj != null)
	  			{
	  				if(document.getElementById("cust_email_hold_chk" + i).checked)
	  				{
	  					return true;
	  				}
	  			}
	  			
	  			obj =document.getElementById("cust_lp_hold_value" + i);
	  			i = 1;
	  			while(obj != null)
	  			{
	  				if(document.getElementById("cust_lp_hold_chk" + i).checked)
	  				{
	  					return true;
	  				}
	  			}	  				  			  			  				  			  			  			}
		  		
	  		}
	  		
	  
	  
			function doSave(){
			
				if(isChecked())
				{
					var holdReason = document.getElementById("holdreason").value;
					if(holdReason.length == 0)
					{
						alert("Please select a hold reason.");
						return;
					}
				}
				

				if(isLocked())
				{					
					return;
				}

			
				var url = "CustomerHoldAction.do" +
						   "?action_type=savecustomer"  + getSecurityParams(false);
				performAction(url);
			}
			
			function hasChanged()
			{
			  if(document.getElementById("changedflag").value == "Y")
			  {
				  return true;
		      }
		      else
		      {
		          return false;
		      }
			
			}
			
			function changed()
			{
			  document.getElementById("changedflag").value = "Y";
			}

			function doReleaseAction(){

				if(isLocked())
				{
					return;
				}

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
					var url = "CustomerHoldAction.do" +
							   "?action_type=releaseorders"  + getSecurityParams(false);
					performAction(url);
				}


			}
			
			function doCancelAction(){

				if(isLocked())
				{
					return;
				}

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{	
					var url = "CustomerHoldAction.do" +
							   "?action_type=cancelorders"  + getSecurityParams(false);
					performAction(url);
				}
				


			}

		/*
		Using the specified URL submit the form
		*/
		function performAction(url)
		{
			document.forms[0].action = url;
			document.forms[0].submit();
		}
		
		function isLocked(lockvalue)
		{

		   var lockvalue = document.getElementById("lockvalue").value;

		   if(lockvalue.length > 0){
		   	   alert("Another user is editing this record.  Please try again later.");
		   
		   	  return true;
		   	  }
		   	else
		   	{

		   	return false;
		   	}
		}
		
		
	</script>			
	
   </head>
	<body>
	<form name="customerHold" method="post" onkeypress="doSomething();" >
	<xsl:call-template name="securityanddata"/>
	<Input TYPE="HIDDEN" NAME="from_page" value='{//root/pagedata/from_page}'></Input>
	<Input TYPE="HIDDEN" NAME="customer_id" value='{//root/pagedata/customer_id}'></Input>
	<Input TYPE="HIDDEN" NAME="order_detail_id" value='{//root/pagedata/order_detail_id}'></Input>
	<Input TYPE="HIDDEN" NAME="order_guid" value='{//root/pagedata/order_guid}'></Input>
	<Input TYPE="HIDDEN" NAME="lockvalue" value='{//root/pagedata/lockvalue}'></Input>
    <Input TYPE="HIDDEN" NAME="changedflag" value='N'></Input>
   <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
   		<tr>
   			<td width="30%" height="33" valign="top">
   				<div class="floatleft">
   					<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
   				</div>
   			</td>
   			<td></td>
            <td  align="right" width="5%" valign="bottom">

            </td>
        </tr>
   		<tr>
   			<td></td>
   			<td  align="center" valign="top" class="Header" id="pageHeader">
           		Customer Hold Detail
         	</td>
			<td  id="time" align="right" valign="bottom" class="Label" width="20%" height="30">
			  <script type="text/javascript">startClock();</script>
			</td>
       </tr>
	</table>

<!-- Display table-->
   <table width="98%" align="center" cellspacing="1" >

   	<tr>
   	
	<xsl:choose>
		<xsl:when test="string-length(root/pagedata/lockvalue) = 0">
		   	<td align='left' width = '30%'><b>Currently being viewed by: <xsl:value-of select="root/pagedata/userid"/></b></td>
  	    </xsl:when>
  	    <xsl:otherwise>
		   	<td align='left' width = '30%' class="errorMessage"><b><xsl:value-of select="root/pagedata/lockvalue"/></b></td>
  	    </xsl:otherwise>
  </xsl:choose>
   	
   	
   	<td  align="center" valign="top" ><b>DNIS <xsl:value-of select="root/pagedata/dnis_id"/>-<xsl:value-of select="root/pagedata/dnis_desc"/></b></td>
   	<td align='right' width='20%'><b>1-800-227-2190 C/S</b></td></tr>

   </table>

   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td><table width="100%" align="center" class="innerTable" >
          <tr>
		<xsl:choose>
		<xsl:when test="string-length(root/pagedata/from_page) = 'customer_account'">
            <td align="center" class="TotalLine"><div align="center"><strong>Customer Hold Information</strong><br></br></div></td>
  	    </xsl:when>
		<xsl:when test="string-length(root/pagedata/from_page) = 'cart'">
            <td align="center" class="TotalLine"><div align="center"><strong>Buyer Hold Information</strong><br></br></div></td>
  	    </xsl:when>
		<xsl:when test="string-length(root/pagedata/from_page) = 'order_detail'">
            <td align="center" class="TotalLine"><div align="center"><strong>Recipient Hold Information</strong><br></br></div></td>
  	    </xsl:when>
		</xsl:choose>

							              
             							 
        					        </tr>
							  	    <tr>
										<td align="center" height="10"></td>
									</tr>
									<tr>
										<td align="center">

										<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
										<tr>
										<td valign="top" align="center" width="50%">
											<div style="overflow:auto; width:75%; height=400; border-left: 2px solid #000; border-right: 2px solid #000; border-bottom: 2px solid #000;border-top: 2px solid #000;">
											<table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
											<tr>
												<td width="35%" align="right"><b>Customer Record Number:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/customer_id"/></td>
												<td  align="left">
													<Input TYPE="HIDDEN" NAME="cust_num_hold_value" value='{root/customers/customer/id_reason}'></Input>
													<Input TYPE="HIDDEN" NAME="cust_num_value" value='{root/customers/customer/customer_id}'></Input>
													<Input TYPE="HIDDEN" NAME="cust_num_hold_id" value='{root/customers/customer/entity_id_customer}'></Input>													
													<xsl:choose>
													<xsl:when test="string-length(root/customers/customer/id_reason) = 0">
													<Input TYPE="CHECKBOX" NAME="cust_num_hold_chk" onclick="changed();">Hold</Input>
													</xsl:when>
													<xsl:otherwise>
													<Input TYPE="CHECKBOX" NAME="cust_num_hold_chk" checked="checked" onclick="changed();">Hold</Input>
													</xsl:otherwise>													
													</xsl:choose>
												</td>
											</tr>
											<tr>
												<td  align="right"><b>Customer Name:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/last_name"/>,<xsl:value-of select="root/customers/customer/first_name"/></td>
													<input TYPE="HIDDEN" NAME="cust_name_hold_value" value='{root/customers/customer/name_reason}'></input>
													<input TYPE="HIDDEN" NAME="cust_name_value" value='{root/customers/customer/first_name}|{root/customers/customer/last_name}'></input>
													<Input TYPE="HIDDEN" NAME="cust_name_hold_id" value='{root/customers/customer/entity_id_name}'></Input>
													<xsl:choose>
													<xsl:when test="string-length(root/customers/customer/name_reason) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_name_hold_chk" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_name_hold_chk" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>

											</tr>
											<tr>
												<td  align="right"><b>Address Line 1:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/address_1"/></td>
													<Input TYPE="HIDDEN" NAME="cust_addr_hold_value" value='{root/customers/customer/address_reason}'></Input>
													<Input TYPE="HIDDEN" NAME="cust_addr_value" value='{root/customers/customer/address_1}|{root/customers/customer/zip_code}'></Input>
													<Input TYPE="HIDDEN" NAME="cust_addr_hold_id" value='{root/customers/customer/entity_id_address}'></Input>
													<xsl:choose>
													<xsl:when test="string-length(root/customers/customer/address_reason) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_addr_hold_chk" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_addr_hold_chk" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>

											</tr>
											<tr>
												<td  align="right"><b>Address Line 2:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/address_2"/></td>
												<td  align="left"></td>
											</tr>
											<tr>
												<td  align="right"><b>City, State:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/city"/>, <xsl:value-of select="root/customers/customer/state"/></td>
												<td  align="left"></td>
											</tr>
											<tr>
												<td  align="right"><b>Zip/Postal Code:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/zip_code"/></td>
												<td  align="left"></td>
											</tr>
											<tr>
												<td  align="right"><b>Country:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/country"/></td>
												<td  align="left"></td>
											</tr>
											

											<xsl:for-each select="root/credit_cards/credit_card">
											<tr>
												<xsl:choose>
												<xsl:when test="count(//credit_card) > 1">
													<td width="35%" align="right"><b>Credit Card Number (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
													<td width="30%" align="right"><b>Credit Card Number:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="cc_number"/></td>
												    <Input TYPE="HIDDEN" NAME="cust_card_hold_value{position()}"  value="{hold_reason_code}"></Input>
												    <Input TYPE="HIDDEN" NAME="cust_card_value{position()}"  value="{cc_number}"></Input>
												    <Input TYPE="HIDDEN" NAME="cust_card_hold_id{position()}" value='{customer_hold_entity_id}'></Input>
													<xsl:choose>
													<xsl:when test="string-length(hold_reason_code) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_card_hold_chk{position()}" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_card_hold_chk{position()}" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>											
											</xsl:for-each>
											<xsl:for-each select="root/phonenumbers/phonenumber">
												<tr>
												<xsl:choose>
												<xsl:when test="count(//phonenumber) > 1">											
												<td  align="right"><b>Phone (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>Phone:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="phone_number"/></td>
													<Input TYPE="HIDDEN" NAME="cust_phone_hold_value{position()}"  value="{hold_reason_code}"></Input>
													<Input TYPE="HIDDEN" NAME="cust_phone_value{position()}"  value="{phone_number}"></Input>
													<Input TYPE="HIDDEN" NAME="cust_phone_hold_id{position()}" value='{customer_hold_entity_id}'></Input>
													<xsl:choose>
													<xsl:when test="string-length(hold_reason_code) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_phone_hold_chk{position()}" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_phone_hold_chk{position()}" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>
												</tr>
											</xsl:for-each>
											
											<xsl:for-each select="root/memberships/membership">
											<tr>
												<Input TYPE="HIDDEN" NAME="cust_member_hold_value{position()}"  value="{hold_reason_code}"></Input>
												<Input TYPE="HIDDEN" NAME="cust_member_value{position()}"  value="{membership_number}|{membership_type}"></Input>
												<Input TYPE="HIDDEN" NAME="cust_member_hold_id{position()}" value='{customer_hold_entity_id}'></Input>
												<xsl:choose>
												<xsl:when test="count(//membership) > 1">
												<td  align="right"><b>Membership ID (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>Membership ID:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="membership_type"/>:<xsl:value-of select="membership_number"/></td>
													<xsl:choose>
													<xsl:when test="string-length(hold_reason_code) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_member_hold_chk{position()}" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_member_hold_chk{position()}" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
											</xsl:for-each>
											<xsl:for-each select="root/emails/email">
											<tr>
												<Input TYPE="HIDDEN" NAME="cust_email_hold_value{position()}"  value="{hold_reason_code}"></Input>
												<Input TYPE="HIDDEN" NAME="cust_email_value{position()}"  value="{email_address}"></Input>
												<Input TYPE="HIDDEN" NAME="cust_email_hold_id{position()}" value='{customer_hold_entity_id}'></Input>
												<xsl:choose>
												<xsl:when test="count(//email) > 1">
												<td  align="right"><b>Email Address (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>Email Address:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="email_address"/></td>
													<xsl:choose>
													<xsl:when test="string-length(hold_reason_code) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_email_hold_chk{position()}" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_email_hold_chk{position()}" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
											</xsl:for-each>
											<xsl:for-each select="root/lps/lp">
											<tr>
												<Input TYPE="HIDDEN" NAME="cust_lp_hold_value{position()}"  value="{hold_reason_code}"></Input>
												<Input TYPE="HIDDEN" NAME="cust_lp_value{position()}"  value="{loss_prevention_indicator}"></Input>
												<Input TYPE="HIDDEN" NAME="cust_lp_hold_id{position()}" value='{customer_hold_entity_id}'></Input>
												<xsl:choose>
												<xsl:when test="count(//lp) > 1">
												<td  align="right"><b>LP Indicator (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>LP Indicator:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="loss_prevention_indicator"/></td>
													<xsl:choose>
													<xsl:when test="string-length(hold_reason_code) = 0">
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_lp_hold_chk{position()}" onclick="changed();">Hold</Input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><Input TYPE="CHECKBOX" NAME="cust_lp_hold_chk{position()}" checked="checked" onclick="changed();">Hold</Input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
											</xsl:for-each>
											</table>
										</div>
										</td>
										</tr>
										
										<tr>
										<td>
																				<table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
												<tr>
						                          <td>
						                          	<div align="center">
 						                          		<Input TYPE="HIDDEN" NAME="orig_hold_reason"  value="{//root/pagedata/holdreason}"></Input>
														<b>Hold Reason:</b><SELECT NAME="holdreason"  onclick="changed();">

														<xsl:choose>
														<xsl:when test="string-length(//root/pagedata/holdreason) = 0">
															<OPTION VALUE=""  selected="true"></OPTION>
														</xsl:when>
														<xsl:otherwise>
															<OPTION VALUE=""  ></OPTION>
														</xsl:otherwise>
														</xsl:choose>

														<xsl:for-each select="root/holdreasons/holdreason">																												
														<xsl:choose>
														<xsl:when test="hold_reason_code = //root/pagedata/holdreason">
															<OPTION VALUE="{hold_reason_code}" selected="true" ><xsl:value-of select="description"/></OPTION>
														</xsl:when>
														<xsl:otherwise>
															<OPTION VALUE="{hold_reason_code}" ><xsl:value-of select="description" /></OPTION>
														</xsl:otherwise>
														</xsl:choose>
														</xsl:for-each>
														</SELECT>
													</div>
						                          </td>
						                        </tr>
						                        <tr>
						                          <td>
						                            <div align="center">
						                              <button class="BlueButton" style="width:80;" name="save" tabindex="10" onclick="javascript:doSave();">(S)ave </button>
						                            </div>
						                          </td>
						                        </tr>
											</table>
										</td>
										</tr>
										
										</table>										
											</td>
												</tr>
										</table>


	</td>
	</tr>
</table>

   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
      	 <td>
			 <table>
			 	<tr>
				 <td  align="left">
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doAddNewAction();">(C)ustomer<br></br>Account </button>
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doAddNewAction();">(L)oss<br></br>Prevention </button>

					<xsl:choose>
					<xsl:when test="root/pagedata/from_page = 'customer_account'">
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
			  	    </xsl:when>
					<xsl:when test="root/pagedata/from_page = 'cart'">
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doReleaseAction();">(R)elease <br></br>Held Cart </button>
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doCancelAction();">Cancel <br></br>(H)eld Cart </button>
			  	    </xsl:when>
					<xsl:when test="root/pagedata/from_page = 'order_detail'">
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doReleaseAction();">(R)elease <br></br>Held Order </button>
					 <button class="bigBlueButton" style="width:80;" name="addNew" tabindex="10" onclick="javascript:doCancelAction();">Cancel <br></br>(H)eld Order </button>
			  	    </xsl:when>
					</xsl:choose>

				 </td>
				 </tr>
			 </table>
		  </td>
		  <td align="right">
			 <table>
			 	<tr>
				 	<td  align="right" valign="top">
					 <button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10" onclick="">(N)ew Search </button>
					 <button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10" onclick="">(B)ack </button>
				 	</td>
				</tr>
				<tr>
					<td align="right" valign="top">
				 	<button class="BlueButton" style="width:80;" name="mainMenu" tabindex="10" onclick="">(M)ain Menu </button>
				 	</td>
				</tr>
			 </table>
		</td>
      </tr>
</table>

<!--Copyright bar-->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
      <td></td>
   </tr>
   <tr>
     <td class="disclaimer"></td><script>showCopyright();</script>
   </tr>
</table>
	</form>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>