<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl" />
<xsl:import href="commonUtil.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pagedata" match="/root/pageData/data" use="name"/>

<xsl:param name="user_response_message"/>

<xsl:template match="/">

<HTML>

<TITLE>FTD - Load FedEx Shipping Tables</TITLE>

<HEAD>
<META http-equiv="Content-Type" content="text/html"></META>
<title>Vendor Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/clock.js"/>
<script type="text/javascript" src="js/util.js"/>
<script type="text/javascript" src="js/commonUtil.js"/>
<script type="text/javascript" src="js/calendar.js"/>
  	  
<script type="text/javascript" language="javaScript"><![CDATA[
//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;
var checkcntr = 0;


	var tokens;
	var idx=0;
	var READY_STATE_COMPLETE=4;
	var req;
	var method;
	var lastUpdateDate;
	var novatorServer;
	var securityText = "SECURITY_VIOLATION";
 
	function init() {
  	}

	function submitData() 
	{		
    	//document.forms[0].btnSendUpdates.disabled = true;
	    //document.forms[0].console.value="";
	    
	    if(validateForm()) 
	    {
			if(document.forms[0].loadShippingRadio[0].checked) 
			{
				document.forms[0].action="FileUploadAction.io";
				document.forms[0].submit();
			} 
			else if(document.forms[0].loadShippingRadio[1].checked)
			{
				alert("hey");
				document.forms[1].action="FedExShippingTableAction.do";
				document.forms[1].action_type.value="load_production";
				document.forms[1].production_time.value=document.forms[0].production_time.value;
				
				
				document.forms[1].submit();
			}
	    } 
	    else 
	    {
	      	//document.forms[0].btnSendUpdates.disabled = false;
	    }

    	return false; //never submit the form
	}
	
	
  
	function validateForm() 
  	{
    	var returnValid = true;
    	var errorString = "";
    
    	if( returnValid==false ) 
    	{
      		alert(errorString);
    	}
    
    	return returnValid;
  	}
  	
  	function retrieveURL(url, callback) {
    if (window.ActiveXObject) { // IE
      req = new ActiveXObject("Microsoft.XMLHTTP");
      if (req) {
        req.onreadystatechange = callback;
        req.open("POST", url, true);
        req.send();
      }
    } else if (window.XMLHttpRequest) { // Non-IE browsers
      req = new XMLHttpRequest();
      req.onreadystatechange = callback;
      try {
        req.open("GET", url, true);
      } catch (e) {
        alert(e);
      }
      req.send(null);
    }
  }
  
  
  function processGetProductsResponse() {
    if (req.readyState == READY_STATE_COMPLETE) { // Complete
      if (req.status == 200) { // OK response
        var returnedText = req.responseText;
        var testText = "NO_PRODUCTS_FOUND";
        
        alert(returnedText);
        
        //returnedText = specialCharsToSpaces(returnedText);
        //tokens = returnedText.tokenize(" ", true, true);
      }
        
    }
  }
  
  
//****************************************************************************************************
//* doExitAction
//****************************************************************************************************
function doExitAction()
{
	if (somethingChanged)
	{
		var errorMessage = "Data changes were detected on this page. Are you sure want to exit with saving?";

		//get a true/false value from the modal dialog.
		var ret = displayConfirmError(errorMessage);

		if (ret)
		{
			var url = "ConfigurationAction.do" + getSecurityParams(true) + "&action_type=exit";
			performAction(url);
		}
	}
	else
	{
		var url = "ConfigurationAction.do" + getSecurityParams(true) + "&action_type=exit";
		performAction(url);
	}
}

//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	document.forms[0].action = url;
	document.forms[0].submit();
}
  
  ]]>
</script>
</HEAD>

<BODY>

<!-- Header-->
   <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
   		<tr>
   			<td width="30%" height="33" valign="top">
   				<div class="floatleft">
   					<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
   				</div>
   			</td>
   			<td></td>
            <td  align="right" width="30%" valign="bottom">
				
            </td>
        </tr>
		<tr>
   			<td></td>
   			<td  align="center" valign="top" class="Header" id="pageHeader">
           		Load FedEx Shipping Tables
         	</td>
		<td  id="time" align="right" valign="bottom" class="Label" width="20%" height="30">
		  <script type="text/javascript">startClock();</script>
		</td>
       </tr>
	</table>
    
<FORM name="moveToStaging" method="post" enctype="multipart/form-data" onsubmit="return submitData();">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value=""/>

<TABLE width="98%" align="center" cellspacing="1" class="mainTable">
	<tr>
		<td>
			<table width="100%" align="center" cellspacing="3">
				<tr>
				<td colspan="3">
					<table>
						<xsl:choose>
							<xsl:when test="/root/pageData/data[name='errors_exist']/value = 'true'">
							<tr>
								<td class="ErrorMessage">
									File was unable to be processed due to the following errors:
								</td>
							</tr>
							<tr>
								<td>
									<div id="ScrollDiv"><xsl:for-each select="/root/FedExSchedule">&nbsp;Line #<xsl:value-of select="fileLine"/>&nbsp;has errors (Origin zip <xsl:value-of select="originZip"/>&nbsp;-&nbsp;Destination zip <xsl:value-of select="destZip"/>)<br/></xsl:for-each></div>
								</td>
							</tr>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="/root/pageData/data[name='system_error_message']/value != ''">
										<tr>
											<td class="ErrorMessage">
												Import unsuccessful: <xsl:value-of select="/root/pageData/data[name='system_error_message']/value"/>
											</td>
										</tr>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="/root/pageData/data[name='insert_count']/value != ''">
												<tr>
													<td class="ErrorMessage">
														Successfully imported&nbsp;<xsl:value-of select="/root/pageData/data[name='insert_count']/value"/>&nbsp;lines of data into staging table
													</td>
												</tr>
											</xsl:when>
											<xsl:otherwise>
												<xsl:if test="/root/pageData/data[name='production_success']/value != ''">
													<xsl:choose>
														<xsl:when test="/root/pageData/data[name='production_success']/value = 'true'">
															<tr>
																<td class="ErrorMessage">
																	Staging was successfully pushed to production
																</td>
															</tr>
														</xsl:when>
														<xsl:otherwise>
															<tr>
																<td class="ErrorMessage">
																	Push to production was unsuccessful
																</td>
															</tr>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</table>
				</td>
				</tr>
				<tr>
					<td><xsl:value-of select="$user_response_message"/> <input type="radio" name="loadShippingRadio" value="loadToStaging"/></td><td style="font-size: 12pt; color:teal;"><b>Load Files to Staging</b>&nbsp;&nbsp;&nbsp;<i><span style="font-size: 8pt; color:green;">Upload files received from FedEx to the staging tables</span></i></td>	
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<table>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;<b>Ground File</b></td><td><input name="GND" type="file" size="60"/></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;<b>Home File</b></td><td><input name="HOME" type="file" size="60"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr><td height="1" colspan="5" bgcolor="gray"></td></tr>
				<tr><td height="5"></td></tr>
				<tr>
					<td><input type="radio" name="loadShippingRadio" value="loadToProduction"/></td><td style="font-size: 12pt; color:teal;"><b>Move to Production</b>&nbsp;&nbsp;&nbsp;<i><span style="font-size: 8pt; color:green;">Replace production data with data in the staging tables</span></i></td>	
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<table>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;<b>Scheduled Time</b></td>
								<td>
									<select name="production_time">
										<option value="0">Midnight</option>
										<option value="1">1:00 am</option>
										<option value="2">2:00 am</option>
										<option value="3">3:00 am</option>
										<option value="4">4:00 am</option>
										<option value="5">5:00 am</option>
										<option value="6">6:00 am</option>
										<option value="7">7:00 am</option>
										<option value="8">8:00 am</option>
										<option value="9">9:00 am</option>
										<option value="10">10:00 am</option>
										<option value="11">11:00 am</option>
										<option value="12">Noon</option>
										<option value="13">1:00 pm</option>
										<option value="14">2:00 pm</option>
										<option value="15">3:00 pm</option>
										<option value="16">4:00 pm</option>
										<option value="17">5:00 pm</option>
										<option value="18">6:00 pm</option>
										<option value="19">7:00 pm</option>
										<option value="20">8:00 pm</option>
										<option value="21">9:00 pm</option>
										<option value="22">10:00 pm</option>
										<option value="23">11:00 pm</option>
										<option value="99">Now</option>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</TABLE>


<TABLE cellSpacing="2" cellPadding="2" width="98%" align="center" border="0">
  <TBODY>
  <TR>
    <TD width="10%"></TD>
    <TD align="center" width="80%"><button style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px" tabIndex='1' type="submit" value="Submit" name="submitButton" class="BlueButton">Submit</button></TD>
    <TD align="right" width="10%"><button onclick="javascript:doExitAction();" tabIndex='3' type="button" value="Exit" name="exitButton" class="BlueButton">Main Menu</button></TD>
  </TR>
  </TBODY>
</TABLE>

<xsl:call-template name="footer"/>

</FORM>
<form name="moveToProd">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value=""/>
<input type="hidden" name="production_time" value=""/>
</form>
</BODY>
</HTML>
       
    </xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
		</xsl:call-template>
	</xsl:template>
  	<!-- tranforms CCYY-MM-DDThh:mm:ss to MM/dd/yyyy -->
	<xsl:template name="formatDate">
    <xsl:param name="dateTimeString"/>
      <xsl:value-of select="substring($dateTimeString, 6, 2)"/>/<xsl:value-of select="substring($dateTimeString, 9, 2)"/>/<xsl:value-of select="substring($dateTimeString, 1, 4)"/>
	</xsl:template>
</xsl:stylesheet>