<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
  <head>
    <META http-equiv="Content-Type" content="text/html"/>
    <title>Price/Pay Vendor Update</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/clock.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>

    <script language="javascript"><![CDATA[

    function init(){
        if (document.forms[0].updateAllowed.value != "Y") {
            document.forms[0].loadButton.disabled = true;
            document.forms[0].userExcel.disabled = true;
        } else {
            document.getElementById("userExcel").focus();
        }
    }

    function getSecurityParams() {
        return  "?securitytoken=" + document.forms[0].securitytoken.value +
            "&context=" + document.forms[0].context.value +
            "&adminAction=" + document.forms[0].adminAction.value;
    }

    function submitForm(servlet, act){
        document.forms[0].action = servlet + getSecurityParams();
        document.forms[0].formAction.value = act;
        document.forms[0].submit();
    }

    function goMain() {
    	document.forms[0].reset();
        submitForm("MainMenuAction.do", "merchandising");
    }
    
    function loadFile() {
        var errorMessage = "Invalid file name. File name must begin with 'PPV_', followed by today's date in MMDDYY format.";
        var fileName = document.forms[0].userExcel.value;
        if (fileName == "") {
            alert("File Name is required");
            document.getElementById("userExcel").focus();
            return false;
        }
        var slash = '/' 
        if (fileName.match(/\\/)) { 
            slash = '\\' 
        }
        tempName = fileName.substring(fileName.lastIndexOf(slash) + 1);
        if (tempName.length == 0 ) {
            alert("Invalid file name");
            document.getElementById("userExcel").focus();
            return false;
        }
        var nameArray = new Array();
        nameArray = tempName.split(".");
        if (nameArray.length < 2) {
            alert(errorMessage);
            document.getElementById("userExcel").focus();
            return false;
        }
        if (nameArray[0].substr(0,4).toLowerCase() != "ppv_") {
            alert(errorMessage);
            document.getElementById("userExcel").focus();
            return false;
        } else if (nameArray[1].toLowerCase() != "xls") {
            alert("File name extension must be 'xls'");
            document.getElementById("userExcel").focus();
            return false;
        }
        if (document.forms[0].updateAllowed.value != "Y") {
            return false;
        }
        var now = new Date();
        var month = now.getMonth()+1;
        month = "0" + month;
        var day = now.getDate();
        day = "0" + day;
        var year = now.getFullYear();
        year = "0" + year;
        iLen = month.length;
        month = month.substr(iLen-2, 2);
        iLen = day.length;
        day = day.substr(iLen-2, 2);
        iLen = year.length;
        year = year.substr(iLen-2, 2);
        //alert(month + " " + day + " " + year);
        if (nameArray[0].substr(4,6) != month + day + year) {
            alert(errorMessage);
            document.getElementById("userExcel").focus();
            return false;
        } else if (nameArray[0].length != 10) {
            alert(errorMessage);
            document.getElementById("userExcel").focus();
            return false;
        }

        submitForm("PricePayVendorUpdate.do", "load");
    }
   ]]>
   </script>

  </head>
  <body onload="javascript:init();">
     
    <form name="uploadForm" method="post" enctype="multipart/form-data">
    <input type="hidden" name="securitytoken" value="{key('pageData', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('pageData', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('pageData', 'adminAction')/value}"/>
    <input type="hidden" name="formAction" value=""/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>

<table width="775" border="0" align="center">
    <tr>
        <td width="30%">
        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </td>
        <td width="40%" align="center" valign="center">
        <font class="Header">Price/Pay Vendor Update</font>
        </td>
        <td  id="time" align="right" valign="center" class="Label" width="30%">
        <script type="text/javascript">startClock();</script>
        </td>
    </tr>
</table>
    
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#006699">
				<tr>
					<td class="ErrorMessage" align="center"></td>
				</tr>
          		<tr>
            		<td>
						<b>Instructions:</b>
						<ol>
						<li>Update the Price/Pay Vendor spreadsheet with the new prices.</li>
						<li>Browse and select spreadsheet.</li>
						<li>Press Load to upload prices.</li>
						<li>If an error spreadsheet is generated (emailed), correct the errors and reload the error spreadsheet.</li>
						<li>If done, press Exit to return to the menu.</li>
						</ol>
      				</td>
          		</tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<a href="/orderprocessingmaint/html/ppv_example.xls">View Example File</a>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
          		<tr>
            		<td>
						File:<font color="red">&nbsp;***&nbsp;</font><input type="file" name="userExcel"/>
      				</td>
          		</tr>
            </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="RequiredFieldTxt">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="loadButton" id="loadButton" class="BlueButton" value="Load" onclick="javascript:loadFile()"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>