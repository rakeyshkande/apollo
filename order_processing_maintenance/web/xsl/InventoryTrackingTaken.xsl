<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- decimal format used when a value is not given -->
<xsl:decimal-format NaN="0.00" name="notANumber"/>
<xsl:decimal-format NaN="(0.00)"  name="noRefund"/>

<xsl:output method="html"/>
  <xsl:template match="/root">
  <html>
    <title>FTD - Weekly Orders Taken</title>
    <head>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <script type="text/javascript">
        <![CDATA[
          function doCloseAction()
          {
            top.close();
          }
        ]]>
      </script>
    </head>
    <body>
      <form name="fInventoryTrackingTaken" method="post">

        <input type="hidden" name="securitytoken"       id="securitytoken"       value="{key('pageData', 'securitytoken')/value}"/>
        <input type="hidden" name="context"             id="context"             value="{key('pageData', 'context')/value}"/>
        <input type="hidden" name="adminAction"         id="adminAction"         value="{key('pageData', 'adminAction')/value}"/>
        <table border="0" align="center" cellpadding="1" width="98%" cellspacing="1" class="mainTable">
          <tr>
            <td colspan="10" class="banner">
              Orders Taken
            </td>
          </tr>

          <tr>
            <td><b>Date</b></td>
            <xsl:for-each select="orders_taken/order_taken">
              <td><xsl:value-of select="date"/></td>
            </xsl:for-each>
            <td>&nbsp;</td>
            <td><b>Week Totals</b></td>
          </tr>

          <tr>
            <td><b>Day</b></td>
            <xsl:for-each select="orders_taken/order_taken">
              <td><xsl:value-of select="day"/></td>
            </xsl:for-each>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>

          <tr>
            <td><b>Orders Taken</b></td>
            <xsl:for-each select="orders_taken/order_taken">
              <td><xsl:value-of select="row_count"/></td>
            </xsl:for-each>
            <td>&nbsp;</td>
            <td><xsl:value-of select="key('pageData', 'weekTotal')/value"/></td>
          </tr>
        </table>

        <br/>
        <table border="0" width="98%" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
                <button class="BlueButton" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button>
            </td>
          </tr>
        </table>
      </form>
    </body>
  </html>


  </xsl:template>

</xsl:stylesheet>