<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
<head>
<META http-equiv="Content-Type" content="text/html"/>
<title>FTD - Component SKU Maintenance</title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/commonUtil.js"></script>
<script type="text/javascript" src="js/mercuryMessage.js"></script>

<script type="text/javascript"><![CDATA[

function init()
{
]]>
<xsl:if test="key('pageData','update_allowed')/value = 'N'">
document.getElementById('addCode').disabled = true;
document.getElementById('deleteCode').disabled = true;
</xsl:if>

<xsl:for-each select="root/component_skus/component_sku">
  document.maint.idList.value = document.maint.idList.value + '<xsl:value-of select="component_sku_id"/>' + ",";
</xsl:for-each>

<![CDATA[
}

function doMainMenuAction()
{
    document.maint.action = "ComponentSKUMaintAction.do";
    document.maint.action_type.value = "MainMenuAction";
    document.maint.submit();
}

function popEdit(component_sku_id, action)
{
    document.maint.action = "ComponentSKUMaintAction.do";
    document.maint.action_type.value = "loadComponentSku";
    document.maint.add_edit_flag.value = action
    document.maint.component_sku_id.value = component_sku_id;
    document.maint.submit();
}

]]>
</script>
</head>

<body onload="javascript:init();">

<table width="775" border="0" align="center">
    <tr>
        <td width="30%">
        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </td>
        <td width="40%" align="center" valign="center">
        <font class="Header">Component SKU List</font>
        </td>
        <td  id="time" align="right" valign="center" class="Label" width="30%">
        <script type="text/javascript">startClock();</script>
        </td>
    </tr>
</table>

<form name="maint" method="post">
<xsl:call-template name="securityanddata"/>
<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
<input type="hidden" name="add_edit_flag" value=""/>
<input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
<input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
<input type="hidden" name="component_sku_id" value=""/>
<input type="hidden" name="component_sku_name" value=""/>
<input type="hidden" name="base_cost" value=""/>
<input type="hidden" name="base_cost_eff_date" value=""/>
<input type="hidden" name="qa_cost" value=""/>
<input type="hidden" name="qa_cost_eff_date" value=""/>
<input type="hidden" name="freight_in_cost" value=""/>
<input type="hidden" name="freight_in_cost_eff_date" value=""/>
<input type="hidden" name="available_flag" value=""/>
<input type="hidden" name="comment_text" value=""/>
<input type="hidden" name="idList" value="" />

<table width="775" align="center" cellspacing="1">
    <tr>
        <td align="right">
        <button accesskey="M" class="BlueButton" style="width:75;" name="MainMenu" tabindex="1" onclick="javascript:doMainMenuAction();">(M)ain Menu</button>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<table width="775" align="center" cellspacing="1" class="mainTable">
    <tr>
        <td>
        <div id="componentSkuDiv">

            <table width="100%" align="center" class="innerTable">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
                            <tr>
                            <xsl:choose>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveSuccessful'">
                                    <td class="OperationSuccess">
                                    Component SKU&nbsp;<xsl:value-of select="key('pageData','component_sku_id')/value"/>&nbsp;was successfully saved.
                                    </td>
                                </xsl:when>
                                <xsl:when test="key('pageData','admin_action')/value = 'saveFailed'">
                                    <td class="errorMessage">
                                    Request to update Component SKU&nbsp;<xsl:value-of select="key('pageData','component_sku_id')/value"/>&nbsp;failed.
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td>&nbsp;</td>
                                </xsl:otherwise>
                            </xsl:choose>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div style="width: 750; overflow:auto; overflow-x:hidden; padding-right: 16px; height: 270px;">																													
                                    <table width="725" border="0" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <th class="TotalLine" width="75" align="center">Sku</th>
                                            <th class="TotalLine" width="150" align="center">Name</th>
                                            <th class="TotalLine" width="75" align="right">Base Cost</th>
                                            <th class="TotalLine" width="75" align="right">Base Cost<br/>Effective Date</th>
                                            <th class="TotalLine" width="75" align="right">QA Cost</th>
                                            <th class="TotalLine" width="75" align="right">QA Cost<br/>Effective Date</th>
                                            <th class="TotalLine" width="75" align="right">Freight-In Cost</th>
                                            <th class="TotalLine" width="75" align="right">Freight-In Cost<br/>Effective Date</th>
                                            <th class="TotalLine" width="75" align="center">Available</th>
                                        </tr>
                                        <xsl:for-each select="root/component_skus/component_sku">
                                            <tr>
                                                <td align="center">
                                                    <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                    <xsl:value-of select="component_sku_id" />
                                                    </a>
                                                </td>
                                                <td align="center">
                                                    <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                    <xsl:value-of select="component_sku_name" />
                                                    </a>
                                                </td>
                                                <td align="right">
                                                    <xsl:if test="base_cost_dollar_amt &gt; 0">
                                                        <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                        <xsl:value-of select='format-number(base_cost_dollar_amt, "#,###,###.00")' />
                                                        </a>
                                                    </xsl:if>
                                                </td>
                                                <td align="right">
                                                    <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                    <xsl:value-of select="base_cost_eff_date" />
                                                    </a>
                                                </td>
                                                <td align="right">
                                                    <xsl:if test="qa_cost_dollar_amt &gt; 0">
                                                        <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                        <xsl:value-of select='format-number(qa_cost_dollar_amt, "#,###,###.00")' />
                                                        </a>
                                                    </xsl:if>
                                                </td>
                                                <td align="right">
                                                    <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                    <xsl:value-of select="qa_cost_eff_date" />
                                                    </a>
                                                </td>
                                                <td align="right">
                                                    <xsl:if test="freight_in_cost_dollar_amt &gt; 0">
                                                        <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                        <xsl:value-of select='format-number(freight_in_cost_dollar_amt, "#,###,###.00")' />
                                                        </a>
                                                    </xsl:if>
                                                </td>
                                                <td align="right">
                                                    <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                    <xsl:value-of select="freight_in_cost_eff_date" />
                                                    </a>
                                                </td>
                                                <td align="center">
                                                    <a href="javascript:popEdit('{component_sku_id}','Edit')" tabindex="{position()}">
                                                    <xsl:value-of select="available_flag" />
                                                    </a>
                                                </td>
                                            </tr>                 		
                                        </xsl:for-each>
                                    </table>																
                                    </div>	
                                </td>
                            </tr>
                            <tr>
                            </tr>												
                        </table>
                    </td>
                </tr>	
            </table>					
            <table width="98%" align="center" cellpadding="5" cellspacing="1">
                <tr>
                    <td align="center">
                        <button accesskey="A" class="BlueButton" style="width:100;" name="AddCode" tabindex="98" onclick="javascript:popEdit('','Add');">(A)dd New</button>
                    </td>
                </tr>
            </table>

        </div>
        </td>
    </tr>
</table>
</form>

<div id="waitDiv" style="display:none">
    <table id="waitTable" width="750" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
            <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                        <td id="waitTD" width="50%" class="waitMessage"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<!-- call footer template-->
<xsl:call-template name="footer"/>
</body>
</html>
</xsl:template>  
</xsl:stylesheet>
