<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pageData" match="root/pageData/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Second Choice Maintenance</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript"><![CDATA[
          			function init()
				    {
				      ]]>
						<xsl:choose>
							<xsl:when test="key('pageData','lock_flag')/value = 'N'">
								document.getElementById('save').disabled = true;
								replaceText('invalidDiv', '<xsl:value-of select="key('pageData','lock_value')/value"/>');
							</xsl:when>
							<xsl:when test="key('pageData','edit_allowed')/value = 'N'">
								document.getElementById('save').disabled = true;
								document.getElementById('addNew').disabled = true;
							</xsl:when>
							<xsl:when test="key('pageData','add_allowed')/value = 'N'">
								document.getElementById('addNew').disabled = true;
							</xsl:when>
						</xsl:choose>
						<![CDATA[
				    }

      		function doSaveAction()
					{
						if(validFormSave())
						{
							enableFlds();
							document.maint.action = "SecondChoiceMaintAction.do";
							document.maint.action_type.value = "saveSecondChoiceAction";
		       		document.maint.submit();
		       	}
		       	return false;
          }

					function doAddNewAction()
					{
            if(validFormAdd()) {
              document.maint.action = "SecondChoiceMaintAction.do?action_type=addSecondChoiceAction&";
              document.maint.submit();
            }
					}

					function doMainMenuAction()
					{
            if(document.maint.global_update.value != 'Y') {
              document.maint.action = "SecondChoiceMaintAction.do?action_type=MainMenuAction&";
              document.maint.submit();
            } else {                
              if(doExitPageAction("Are you sure you want to leave without saving?")){
                document.maint.action = "SecondChoiceMaintAction.do?action_type=MainMenuAction&";
                document.maint.submit();
              }
            }
					}

          function setUpdateFlag(fld)
          {
            document.getElementById(fld).value='Y';
            document.maint.global_update.value='Y';
          }

          function validFormSave()
          {
            var elem = document.maint.elements;
						var elength = elem.length;
						for(var i=0; i<elength; i++)
						{
							var ename = elem[i].name;
							if(ename.substring(0,17)=='r_oe_description_')
							{
								if(isWhitespace(document.getElementById(elem[i].name).value))
								{
									replaceText('invalidDiv', 'STANDARD 2nd CHOICE DESCRIPTIONS CANNOT BE BLANK' + elem[i].name);
							    return false;
								}
							}
         		}

         		return true;
 				  }
          function codeExists(inCode)
          {
						var elem = document.maint.elements;
						var elength = elem.length;
						for(var i=0; i<elength; i++)
						{ 
							var ename = elem[i].name;
							if(ename.substring(0,24)=='r_product_second_choice_') {
                if(elem[i].value==inCode) {
                  return true;
                }
              }
						}
            return false;
          }
          function validFormAdd()
          { 
            if(codeExists(document.getElementById('r_product_second_choice').value)) {
              replaceText('invalidDivAdd', '2nd CHOICE CODE CANNOT BE BLANK AND MUST BE UNIQUE -- 1');
              return false;
            }            
						if(isWhitespace(document.getElementById('r_product_second_choice').value)) 
            {
              replaceText('invalidDivAdd', '2nd CHOICE CODE CANNOT BE BLANK AND MUST BE UNIQUE -- 2');
              return false;
            }
						if(isWhitespace(document.getElementById('r_oe_description').value))
            {
              replaceText('invalidDivAdd', 'STANDARD 2nd CHOICE DESCRIPTIONS CANNOT BE BLANK');
              return false;
            }
            if(!isInteger(document.getElementById('r_product_second_choice').value)){
              replaceText('invalidDivAdd', '2nd CHOICE CODE MUST BE NUMERIC');
              return false;
            }
            return true;
          }
 				  function replaceText( elementId, newString )
					{
					   	var node = document.getElementById( elementId );
					   	var newTextNode = document.createTextNode( newString );
					   	if(node.childNodes[0]!=null)
					   	{
					    	node.removeChild( node.childNodes[0] );
					   	}
					   	node.appendChild( newTextNode );
					}
          
          function printIt(){
            this.focus();
            self.print();
          }          

					function enableFlds()
					{
						var elem = document.maint.elements;
						var elength = elem.length;
						for(var i=0; i<elength; i++)
						{
							if(elem[i])
								if(elem[i].type=='text')
									document.getElementById(elem[i].name).disabled = false;
						}
					}
 				    ]]>
				</script>
			</head>
			<body onload="javascript:init();">
				<xsl:call-template name="header">
					<xsl:with-param name="headerName"  select="'Second Choice Maintenance'" />
					<xsl:with-param name="showBackButton" select="false()"/>
					<xsl:with-param name="showTime" select="true()"/>		
          <xsl:with-param name="showSearchBox" select="false()"/>
				</xsl:call-template>
				<form name="maint" method="post">
					<xsl:call-template name="securityanddata"/>
					<input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
					<input type="hidden" name="dnis_id" value="" />
          <input type="hidden" name="global_update" value="N"/>
          <input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>          
					<table width="98%" align="center" cellspacing="1">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td align="left" id="invalidDiv" class="ErrorMessage" height="10"></td>
									</tr>
									<tr>
										<td align="center" class="TotalLine"><strong>Second Choice List</strong></td>
									</tr>
									<tr>
										<td align="center" height="10"></td>
									</tr>
									<tr>
										<td align="center">
											<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
												<tr>
													<td align="center">
														<div style="width:95%;">																													
															<table width="100%" border="0" cellpadding="2" cellspacing="0">
																<tr>
																	<th class="label" width="20%" align="center">2nd Choice Code</th>
																	<th class="label" width="20%" align="center">2nd Choice Description</th>
																	<th class="label" width="20%" align="center">Mercury 2nd Choice Description</th>
																	<th class="label" width="20%" align="center">Holiday 2nd Choice Description</th>
																	<th class="label" width="20%" align="center">Holiday Mercury 2nd Choice Description</th>
																</tr>
															</table>
														</div>	
														<div style="overflow:auto; width:95%; height=200; border-left: 2px solid #000; border-right: 2px solid #000; border-top: 2px solid #000; border-bottom: 2px solid #000;">																													
															<table width="100%" border="0" cellpadding="2" cellspacing="0">	
							                                	<xsl:for-each select="root/second_choices/second_choice">							                                  		
							                                    	<tr>
																		<input type="hidden" name="r_updated_{position()}" value="N"/>
																		<td align="center" width="20%">
																			<input type="text" name="r_product_second_choice_{position()}" id="r_product_second_choice_{position()}" size="6" value="{product_second_choice_id}" tabindex="{position()}1" disabled="true" maxlength="2"/>
																		</td>
																		<td align="center" width="20%">
																			<input type="text" name="r_oe_description_{position()}" id="r_oe_description_{position()}" size="20" value="{oe_description}" tabindex="{position()}2" onchange="javascript:setUpdateFlag('r_updated_{position()}');" maxlength="180"/>
											        					</td>
																		<td align="center" width="20%">
																			<input type="text" name="r_mercury_description_{position()}" id="r_mercury_description_{position()}" size="20" value="{mercury_description}" tabindex="{position()}3" onchange="javascript:setUpdateFlag('r_updated_{position()}');" maxlength="180"/>
																		</td>
																		<td align="center" width="20%">
																			<input type="text" name="r_oe_holiday_description_{position()}" id="r_oe_holiday_description_{position()}" size="20" value="{oe_holiday_description}" tabindex="{position()}4" onchange="javascript:setUpdateFlag('r_updated_{position()}');" maxlength="180"/>
																		</td>
																		<td align="center" width="20%">
																			<input type="text" name="r_mercury_holiday_description_{position()}" id="r_mercury_holiday_description_{position()}" size="20" value="{oe_mercury_description}" tabindex="{position()}5" onchange="javascript:setUpdateFlag('r_updated_{position()}');" maxlength="180"/>
																		</td>
																	</tr>                 		
																</xsl:for-each>
															</table>																
														</div>	
													</td>
												</tr>
												<tr>
													<td>
														<div align="center">
															<table width="100%" border="0" cellpadding="2" cellspacing="0">
																<tr>
																	<td width="85%" align="right"></td>
																	<td width="10%" align="right"><a href="javascript:printIt();"><img src="images/printer.jpg" width="25" height="25" name="printer" id="printer" border="0" /></a></td>
																	<td width="5%" align="right"><button class="BlueButton" style="width:80;" name="save" id="save" onclick="javascript:doSaveAction();">(S)ave </button></td>
																</tr>															
															</table>
														</div>
													</td>
												</tr>												
						                    </table>
										</td>
									</tr>	
									<tr>
										<td align="center" class="TotalLine"><strong>Add Second Choice</strong></td>
									</tr>
									<tr>
										<td align="center" height="10"></td>
									</tr>
									<tr>
										<td align="center">
											<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">												
												<tr>
													<td align="center" colspan="2">
														<div style="width:95%;">
															<table width="100%" border="0" cellpadding="2" cellspacing="0">
																<tr>
																	<th class="label" width="20%" align="center">2nd Choice Code</th>
																	<th class="label" width="20%" align="center">2nd Choice Description</th>
																	<th class="label" width="20%" align="center">Mercury 2nd Choice Description</th>
																	<th class="label" width="20%" align="center">Holiday 2nd Choice Description</th>
																	<th class="label" width="20%" align="center">Holiday Mercury 2nd Choice Description</th>
																</tr>
															</table>
														</div>
														<div style="width:95%;">
															<table width="100%" border="0" cellpadding="2" cellspacing="0">
							                                	<tr>
																	<td align="center" width="20%">
																		<input type="text" name="r_product_second_choice" id="r_product_second_choice" size="6" value="{product_second_choice}" tabindex="1" maxlength="2"/>
																	</td>
																	<td align="center" width="20%">
																		<input type="text" name="r_oe_description" id="r_oe_description" size="20" value="{oe_description}" tabindex="2" maxlength="180"/>
										        					</td>
																	<td align="center" width="20%">
																		<input type="text" name="r_mercury_description" id="r_mercury_description" size="20" value="{mercury_description}" tabindex="3" maxlength="180"/>
																	</td>
																	<td align="center" width="20%">
																		<input type="text" name="r_oe_holiday_description" id="r_oe_holiday_description" size="20" value="{oe_holiday_description}" tabindex="4" maxlength="180"/>
																	</td>
																	<td align="center" width="20%">
																		<input type="text" name="r_mercury_holiday_description" id="r_mercury_holiday_description" size="20" value="{oe_mercury_description}" tabindex="5" maxlength="180"/>
																	</td>
																</tr>
															</table>
														</div>
													</td>
												</tr>
												<tr>
													<td align="left" id="invalidDivAdd" class="ErrorMessage" height="10"></td>
													<td align="right">
														<button class="BlueButton" style="width:80;" name="addNew" id="addNew" tabindex="6" onclick="javascript:doAddNewAction();">(A)dd </button>
													</td>
												</tr>
						                    </table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>					
					<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
				</form>
				<table width="98%" align="center" cellpadding="5" cellspacing="1">
					<tr>
						<td width="20%" align="right"></td>
						<td width="78%" align="right">
							<button class="BlueButton" style="width:80;" name="mainMenu" tabindex="7" onclick="javascript:doMainMenuAction();">(M)ain Menu </button>
						</td>
					</tr>
				</table>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
        <!-- call footer template-->
        <xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>  
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->