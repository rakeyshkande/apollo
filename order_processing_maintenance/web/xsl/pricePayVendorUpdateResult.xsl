<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pageData" match="root/pageData/data" use="name"/>
  <xsl:template match="/">

  <html>
  <head>
    <META http-equiv="Content-Type" content="text/html"/>
    <title>Price/Pay Vendor Update</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript" src="js/clock.js"></script>
    <script type="text/javascript" src="js/commonUtil.js"></script>

    <script language="javascript"><![CDATA[

    function init(){
        var errorMessage = document.getElementById("errorMessage").value;
        if (errorMessage == "") {
            document.getElementById("resultLabel").innerHTML = "Your spreadsheet has been successfully loaded. The results will be emailed shortly.";
            document.getElementById("resultLabel").className = "largeMsgTxt";
        } else {
            document.getElementById("resultLabel").innerHTML = errorMessage;
            document.getElementById("resultLabel").className = "largeErrorMsgTxt";
        }
    }

    function getSecurityParams() {
        return  "?securitytoken=" + document.forms[0].securitytoken.value +
            "&context=" + document.forms[0].context.value;
    }

    function submitForm(servlet, act){
        document.forms[0].action = servlet + getSecurityParams();
        document.forms[0].adminAction.value = act;
        document.forms[0].submit();
    }

    function goMain() {
        submitForm("MainMenuAction.do", "merchandising");
    }

   ]]>
   </script>

  </head>
  <body onload="javascript:init();">
     
    <form name="uploadForm" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="adminAction" value="load"/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="subject" value="user"/>
    <input type="hidden" name="updateAllowed" value="{key('pageData','update_allowed')/value}"/>
    <input type="hidden" name="errorMessage" value="{key('pageData','error_message')/value}"/>

    <table width="775" border="0" align="center">
        <tr>
            <td width="30%">
                <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
            </td>
            <td width="40%" align="center" valign="center">
                <font class="Header">Price/Pay Vendor Update Results</font>
            </td>
            <td  id="time" align="right" valign="center" class="Label" width="30%">
                <script type="text/javascript">startClock();</script>
            </td>
        </tr>
    </table>
    
    <table width="99%" border="0" cellpadding="1" cellspacing="1">
        <tr>
            <td align="right">
                <input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
            </td>
        </tr>
        <tr>
            <td align="center">
                <h2>Upload Results</h2>
            </td>
        </tr>
        <tr>
            <td align="center">
                <label id="resultLabel"></label>
            </td>
        </tr>
    </table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>