<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>

<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<xsl:variable name="selectedContext" select="key('pageData','selected_context')/value"/>

<html>
<head>
    <title>FTD - Phoenix Product Mapping Maintenance</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    <link rel="stylesheet" type="text/css" href="css/tooltip.css"/>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" src="js/copyright.js"></script>
	  
    <script type="text/javascript" language="javascript"><![CDATA[


//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;
var checkcntr = 0;

//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
	//setScrollingDivHeight();
	//window.onresize = setScrollingDivHeight;
}

//****************************************************************************************************
//* setScrollingDivHeight
//****************************************************************************************************
function setScrollingDivHeight()
{
	var configDiv = document.getElementById("configListing");
	var newHeight = document.body.clientHeight - configDiv.getBoundingClientRect().top - 100;
	if (newHeight > 15)
		configDiv.style.height = newHeight;
}

//****************************************************************************************************
//* doAction
//****************************************************************************************************
function doAction(action)
{	
		if(action == 'add')
		{
			var floral_product_sku = document.getElementById("florist_product_sku_add").value;
			var vendor_product_sku = document.getElementById("vendor_product_sku_add").value;
			var perfAction = true;
			if((floral_product_sku == null || floral_product_sku == "") && (vendor_product_sku == null || vendor_product_sku == ""))
			{
				perfAction = false;
				//var errorMessage = "Original Floral Product SKU and New Vendor Product SKU must not be blank.";
				var errorMessage = "Original Floral Product SKU must not be blank. <BR> New Vendor Product SKU must not be blank.";
				displayOkError(errorMessage);
			}
			else{
				  if (floral_product_sku == null || floral_product_sku == "")
				  {
					perfAction = false;
					var errorMessage = "Original Floral Product SKU must not be blank.";
					displayOkError(errorMessage);
				  }
				  if (vendor_product_sku == null || vendor_product_sku == "")
				  {
					perfAction = false;
					var errorMessage = "New Vendor Product SKU must not be blank.";
					displayOkError(errorMessage);
				  }
			}
			if(perfAction){
			var url = "ProductMappingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=" + action + "&floral_product_sku=" + floral_product_sku + "&vendor_product_sku=" + vendor_product_sku;
			performAction(url);
			}
		}
		
		if(action == 'remove'){
		
			//set the message
			var errorMessage = "Are you sure you want to remove? ";
			//get a true/false value from the modal dialog.
			var ret = displayConfirmError(errorMessage);
			
			if (ret){
			  var productMappingId = retrieveProductMappingId();
			  if (productMappingId == null)
			  {
				//set the message
				var errorMessage = "Please select a product mapping to remove.";

				//display the popup
				displayOkError(errorMessage);
			  }
			  else
			  {
				var url = "ProductMappingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=" + action + "&floral_product_sku=" + productMappingId;
				//alert(url);
				performAction(url);
			  }
			}
		}
}

//****************************************************************************************************
//* retrieveProductMappingId
//****************************************************************************************************
function retrieveProductMappingId()
{
  var radio = document.forms[0].productMappingIdRadio;
  var radioSelected;

  if (radio != null)
  {
    if (radio.length == null)
    {
      if (radio.checked)
        radioSelected = radio;
      else
        radioSelected = null;
    }
    else
    {
      for ( var i = 0; i < radio.length; i++){
        if ( radio[i].checked ){
          radioSelected = radio[i];
        }
      }
    }


    // if radio was not selected, return null
    if ( radioSelected == null )
    {
      return null;
    }
    // else return the radio value
    else
    {
      var productMappingId = radioSelected.value;
      return productMappingId;
    }
  }
}
//****************************************************************************************************
//* doExitAction
//****************************************************************************************************
function doExitAction()
{
	if (somethingChanged)
	{
		var errorMessage = "Data changes were detected on this page. Are you sure want to exit with saving?";

		//get a true/false value from the modal dialog.
		var ret = displayConfirmError(errorMessage);

		if (ret)
		{
			var url = "ProductMappingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=exit";
			performAction(url);
		}
	}
	else
	{
		var url = "ProductMappingMaintenanceAction.do" + getSecurityParams(true) + "&action_type=exit";
		performAction(url);
	}
}


//****************************************************************************************************
//* performAction
//****************************************************************************************************
function performAction(url)
{
	document.forms[0].action = url;
	document.forms[0].submit();
}


    ]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Phoenix Product Mapping Maintenance'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="ProductMappingForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('pageData', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('pageData', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('pageData', 'adminAction')/value}"/>
    <input type="hidden" name="selected_context" value="{key('pageData','selected_context')/value}"/>


    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
			<table width="100%" border="0" cellpadding="2" cellspacing="2">
			<tr>              
              <td width="80%" class="colHeaderCenter" style="font-size: 14px; font-style: bold">Product Mapping List</td>
            </tr>
			<tr><td>&nbsp;</td></tr>
			</table>
          <!-- System Configuration -->
          <table width="70%" border="0" cellpadding="2" cellspacing="2" align="center">
			<tr>
              <td width="5%" class="colHeaderCenter">Remove</td>
              <td width="30%" class="colHeaderCenter">Original Floral Product SKU</td>
              <td width="30%" class="colHeaderCenter">New Vendor Product SKU</td>
            </tr>
            <tr>
              <td colspan="5" width="100%" align="center">

                <!-- Scrolling div contiains system messages -->
                <div id="configListing" style="overflow:scroll; height:200px; width:100%; padding:0px; margin:0px; border:2px solid #006699;">
                  <table width="100%" cellpadding="1" cellspacing="1">
                    <xsl:for-each select="product_mappings/product_mapping">
                      <xsl:sort select="@num" data-type="number"/>
                      <tr>
                        <td width="5%" align="center">
						  <input type="radio" id="productMappingIdRadio" name="productMappingIdRadio" value="{original_product_id}"/>
                        </td>
                        <td width="30%" align="center"><xsl:value-of select="original_product_id"/></td>
						<td width="30%" align="center"><xsl:value-of select="new_product_id"/></td>                                               
                      </tr>
                    </xsl:for-each>
                    <tr><td>&nbsp;</td></tr>
                  </table>
                </div>

              </td>
            </tr>
          </table>
		  
		  <table width="70%" border="0" cellpadding="2" cellspacing="2" align="center">
			<tr>
				<td align="right">
					<button class="bigBlueButton" name="removeButton" id="removeButton" tabindex="1" accesskey="R" onclick="javascript:doAction('remove');">(R)emove</button>
				</td>        
			</tr>
          </table>
		  
			<table width="100%" border="0" cellpadding="2" cellspacing="2">
				<tr>              
					<td width="100%" class="colHeaderCenter" style="font-size: 14px; font-style: bold">Add Product Mapping</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
			
		  <table width="70%" border="0" cellpadding="2" cellspacing="2" align="center">
			<tr>              
              <td width="30%" class="colHeaderCenter">Original Floral Product SKU</td>
              <td width="30%" class="colHeaderCenter">New Vendor Product SKU</td>
            </tr>
            <tr>
              <td colspan="5" width="100%" align="center">
                <!-- Scrolling div contiains text boxes -->
                <div id="configListing" style="overflow:auto; width:100%; padding:0px; margin:0px; border:2px solid #006699;">
                  <table width="100%" cellpadding="1" cellspacing="1">                    
                      <tr>                       
                        <td width="30%" align="center">
							<input type="text" name="florist_product_sku_add" id="florist_product_sku_add" value="" size="60" maxlength="20" onclick="javascript:fieldFocus();">
							<xsl:attribute name="value">
								<xsl:value-of select="error_messages/florist_product_sku_add" />
							</xsl:attribute>
							</input>
						</td>
						<td width="30%" align="center">
							<input type="text" name="vendor_product_sku_add" id="vendor_product_sku_add" value="" size="60" maxlength="20" onclick="javascript:fieldFocus();">
							<xsl:attribute name="value">
								<xsl:value-of select="error_messages/vendor_product_sku_add" />
							</xsl:attribute>
							</input>
						</td>                                               
                      </tr>                    
                    <tr><td>&nbsp;</td></tr>
					<xsl:for-each select="error_messages/error_message">
                      <tr>
                        <td width="30%" align="center"><span style="color:red"><xsl:value-of select="message"/></span></td>                                               
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
          </table>		  
		  <table width="70%" border="0" cellpadding="2" cellspacing="2" align="center">
			<tr>
				<td align="right">
					<button class="bigBlueButton" name="addButton" id="addButton" tabindex="2" accesskey="A" onclick="javascript:doAction('add');">(A)dd</button>
				</td>        
			</tr>
          </table>
		  
        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td align="right">
          <button type="button" class="bigBlueButton" name="exitButton" id="exitButton" tabindex="3" accesskey="M" onclick="javascript:doExitAction();">(M)ain Menu</button>
        </td>
      </tr>
    </table>

  </form>

	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="disclaimer"></td><script>showCopyright();</script>
			
		</tr>
	</table>

</body>
</html>

</xsl:template>
</xsl:stylesheet>