<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="commonUtil.xsl"/>

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->


<xsl:template match="/root">



<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
    <meta http-equiv="Content-Type" content="text/html"></meta>
    <title>Inventory Tracking Error Page</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/InventoryTrackingDashboard.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script>
//****************************************************************************************************
//*  values
//****************************************************************************************************

function trim(str) {
  val = str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  return val;
}
    </script>


    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//*  Global variables
//****************************************************************************************************

    ]]>

    </script>


  </head>
  <body>
    <form name="fInventoryTrackingErrorPage" method="post">

      <input type="hidden" name="securitytoken"       id="securitytoken"       value="{key('pageData', 'securitytoken')/value}"/>
      <input type="hidden" name="context"             id="context"             value="{key('pageData', 'context')/value}"/>
      <input type="hidden" name="adminAction"         id="adminAction"         value="{key('pageData', 'adminAction')/value}"/>
       <input type="hidden" name="action_type"             id="action_type"/>
    
      <!-- Main Div -->
      <div id="content" style="display:block">

		 <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="30%" height="33" valign="top">
            <div class="floatleft">
              <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
            </div>
          </td>
          <td>&nbsp;</td>
          <td align="right" width="30%" valign="bottom">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" valign="top" class="Header" id="pageHeader">Inventory Tracking Error Page</td>
          <td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
        </tr>
      </table>

        <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">

          <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->
           <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>
            <tr>
              <td colspan="2" align="left" style="color:blue;font-size:12;"> The request failed due to the following error :</td>
              <td colspan="4" align="left">
              	 <textarea name="errorMessageTextArea" id="errorMessageTextArea" rows="3" cols="110" tabindex="5">
                          <xsl:value-of select = "key('pageData', 'errorMessage')/value"/>
                        </textarea>
              </td>
            </tr>
          
   


          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>

          <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="4" align="center" valign="top">
              <button name="bDashboard" id="bDashboard" class="BlueButton" style="width:120;" accesskey="D" onclick="performDashboard();">(D)ashboard</button>
              <button name="bMainMenu"  id="bMainMenu"  class="BlueButton" style="width:120;" accesskey="M" onclick="performMainMenu();">(M)ain Menu</button>
            </td>
          </tr>

          <tr><td colspan="6">&nbsp;</td></tr>
          <tr><td colspan="6">&nbsp;</td></tr>


        </table>
      </div>

      <!-- Used to dislay Processing... message to user -->
      <div id="waitDiv" style="display:none">
         <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
            <tr>
              <td width="100%">
                <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
                 <tr>
                    <td>
                       <table width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tr>
                             <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                             <td id="waitTD"  width="50%" class="waitMessage"></td>
                           </tr>
                       </table>
                     </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>

      



      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="disclaimer"></td><script>showCopyright();</script>
          
        </tr>
      </table>

     </form>
    <iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
  </body>
</html>


</xsl:template>
</xsl:stylesheet>
