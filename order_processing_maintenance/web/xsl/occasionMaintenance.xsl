<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pageData" match="root/pageData/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Occasion Code Maintenance</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript" src="js/FormChek.js"></script>
				<script type="text/javascript">
          <![CDATA[
            function init()
            {
                ]]>
                <xsl:choose>
                  <xsl:when test="key('pageData','lock_flag')/value = 'N'">
                    document.getElementById('save').disabled = true;
                    document.getElementById('addNew').disabled = true;
                    replaceText('invalidDiv', '<xsl:value-of select="key('pageData','lock_value')/value"/>');
                  </xsl:when>
                  <xsl:when test="key('pageData','edit_allowed')/value = 'N'">
                    document.getElementById('save').disabled = true;
                    document.getElementById('addNew').disabled = true;
                  </xsl:when>
                  <xsl:when test="key('pageData','add_allowed')/value = 'N'">
                    document.getElementById('addNew').disabled = true;
                  </xsl:when>
                </xsl:choose>
              <![CDATA[
            }

            function doSaveAction()
            {
              if(validForm()) {
                document.maint.action = "OccasionMaintAction.do?action_type=saveOccasionAction"
                                      +  "&securitytoken=" + document.forms[0].securitytoken.value
                                      +  "&context=" + document.forms[0].context.value;
                document.maint.submit();
              }
            }

            function doAddNewAction()
            {
              if(document.maint.global_update.value != 'Y') {
                document.maint.action = "OccasionMaintAction.do"
                                      +  "?securitytoken=" + document.forms[0].securitytoken.value
                                      +  "&context=" + document.forms[0].context.value;
                document.maint.action_type.value = "displayAddOccasionAction";
                document.maint.submit();
              } else {
                if(doExitPageAction("Are you sure you want to leave without saving?")){
                  document.maint.action = "OccasionMaintAction.do"
                                      +  "?securitytoken=" + document.forms[0].securitytoken.value
                                      +  "&context=" + document.forms[0].context.value;
                  document.maint.action_type.value = "displayAddOccasionAction";
                  document.maint.submit();
                }
              }
            }

            function doMainMenuAction()
            {
              if(document.maint.global_update.value != 'Y') {
                document.maint.action = "OccasionMaintAction.do?action_type=releaseLockAction"
                                      +  "&securitytoken=" + document.forms[0].securitytoken.value
                                      +  "&context=" + document.forms[0].context.value;
                document.maint.submit();
              } else {
                if(doExitPageAction("Are you sure you want to leave without saving?")){
                  document.maint.action = "OccasionMaintAction.do?action_type=releaseLockAction"
                                      +  "&securitytoken=" + document.forms[0].securitytoken.value
                                      +  "&context=" + document.forms[0].context.value;
                  document.maint.submit();
                }
              }
            }

            function setUpdateFlag(fld)
            {
              document.getElementById(fld).value='Y';
              document.maint.global_update.value='Y';
            }
            function printIt(){
            	this.focus();
            	self.print();
            }
            function validForm() {
              var elem = document.maint.elements;
              var elength = elem.length;
              var returnValue = true;
							var displayOrder;

              for(var i=0; i<elength; i++)
              {
                var ename = elem[i].name;
                if(ename.substring(0,13)=='r_description')
                {
                 	document.getElementById(elem[i].name).style.backgroundColor = 'white';
                  if(isWhitespace(document.getElementById(elem[i].name).value))
                  {
                  	document.getElementById(elem[i].name).style.backgroundColor = 'pink';
                    replaceText('invalidDiv', 'Please correct the highlighted field(s)');
                    returnValue = false;
                  }
                }
                if(ename.substring(0,15)=='r_display_order')
                {
									displayOrder = document.getElementById(elem[i].name).value;
									displayOrder = Trim(displayOrder);
                 	document.getElementById(elem[i].name).style.backgroundColor = 'white';
                  if(	isWhitespace(displayOrder) || !isInteger(displayOrder) )
                  {
                  	document.getElementById(elem[i].name).style.backgroundColor = 'pink';
                    replaceText('invalidDiv', 'Please correct the highlighted field(s)');
                    returnValue = false;
                  }
                }
              }

              return returnValue;
            }

            function replaceText( elementId, newString )
            {
                var node = document.getElementById( elementId );
                var newTextNode = document.createTextNode( newString );
                if(node.childNodes[0]!=null)
                {
                  node.removeChild( node.childNodes[0] );
                }
                node.appendChild( newTextNode );
            }

            function updatedFields(){
              if(document.maint.global_update == 'Y') {
                return true;
              } else {
                return false;
              }
            }
            function enableFlds()
            {
              var elem = document.maint.elements;
              var elength = elem.length;
              for(var i=0; i<=elength; i++)
              {
                if(elem[i])
                  if(elem[i].type=='text')
                    document.getElementById(elem[i].name).disabled = false;
              }
            }
   				]]>
				</script>
			</head>
			<body onload="javascript:init();">
				<xsl:call-template name="header">
					<xsl:with-param name="headerName"  select="'Occasion Code Maintenance'" />
					<xsl:with-param name="showTime" select="true()"/>
          <xsl:with-param name="showBackButton" select="false()"/>
          <xsl:with-param name="showSearchBox" select="false()"/>
				</xsl:call-template>
				<form name="maint" method="post">
          <input type="hidden" name="action_type" value="{key('pageData','action_type')/value}"/>
          <input type="hidden" name="dnis_id" value="" />
          <input type="hidden" name="next_occasion_seq" value="{key('pageData','next_occasion_seq')/value}"/>
          <input type="hidden" name="global_update" value="N"/>
          <input type="hidden" name="adminAction" value="{key('pageData','adminAction')/value}"/>
          <input type="hidden" name="context"                   value="{key('pageData', 'context')/value}"/>
          <input type="hidden" name="securitytoken"              value="{key('pageData', 'securitytoken')/value}"/>

					<table width="98%" align="center" cellspacing="1">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr>
										<td align="left" id="invalidDiv" class="ErrorMessage" height="10"></td>
									</tr>
									<tr>
										<td align="center" class="TotalLine"><strong>Occasion Code List</strong></td>
									</tr>
									<tr>
										<td align="center" height="10"></td>
									</tr>
									<tr>
										<td align="center">
											<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
												<tr>
													<td align="center">
														<div style="width:95%; border-left: 3px solid #000; border-right: 3px solid #000; border-top: 3px solid #000; border-bottom: 3px solid #000;">
															<table width="80%" border="0" cellpadding="2" cellspacing="0">
																<tr>
																	<th class="label" width="10%" align="center">Occasion<br/>Code</th>
																	<th class="label" width="50%" align="center">Description</th>
																	<th class="label" width="10%" align="center">Display<br/>Order</th>
																	<th class="label" width="10%" align="center">Active?</th>
																</tr>
															</table>
														</div>
														<div style="overflow:auto; width:95%; height=200; border-left: 3px solid #000; border-right: 3px solid #000; border-bottom: 3px solid #000;">
															<table width="80%" border="0" cellpadding="2" cellspacing="0">
                                <xsl:for-each select="root/occasions/occasion">
                                  <tr>
																		<input type="hidden" name="r_updated_{position()}" value="N"/>
																		<td align="center" width="10%">
																			<input type="text" name="r2_occasion_id_{position()}" id="r2_occasion_id_{position()}" size="6" maxlength="2" value="{occasionid}" tabindex="1" disabled="true"/>
                                      <input type="hidden" name="r_occasion_id_{position()}" value="{occasionid}"/>
																		</td>
																		<td align="center" width="50%">
																			<xsl:choose>
											                  <xsl:when test="usereditable='Y'">
                                          <input type="text" name="r_description_{position()}" id="r_description_{position()}" size="56" maxlength="55" value="{description}" tabindex="2" onchange="javascript:setUpdateFlag('r_updated_{position()}');"/>
											                  </xsl:when>
												                <xsl:otherwise>
                                          <input type="text" name="r2_description_{position()}" id="r2_description_{position()}" size="56" maxlength="55" value="{description}" tabindex="2" disabled="true" onchange="javascript:setUpdateFlag('r_updated_{position()}');"/>
																		      <input type="hidden" name="r_description_{position()}" id="r_description_{position()}"  value="{description}" />
																				</xsl:otherwise>
                                      </xsl:choose>
																		</td>
																		<td align="center" width="10%">
																			<input type="text" name="r_display_order_{position()}" id="r_display_order_{position()}" size="6" maxlength="2" value="{displayorder}" tabindex="3" onchange="javascript:setUpdateFlag('r_updated_{position()}');"/>
																		</td>
																		<td align="center" width="10%">
																			<xsl:choose>
                												<xsl:when test="active='Y'">
                                          <input type="checkbox" name="r_active_{position()}" id="r_active_{position()}" tabindex="4" checked="checked" onchange="javascript:setUpdateFlag('r_updated_{position()}');"/>
					                              </xsl:when>
										                    <xsl:otherwise>
																					<input type="checkbox" name="r_active_{position()}" id="r_active_{position()}" tabindex="4" onchange="javascript:setUpdateFlag('r_updated_{position()}');"/>
																				</xsl:otherwise>
                  										</xsl:choose>
																		</td>
																	</tr>
																</xsl:for-each>
															</table>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<div class="floatright">
															<a href="javascript:printIt();"><img src="images/printer.jpg" width="25" height="25" name="printer" id="printer" border="0" /></a>
														</div>
													</td>
												</tr>
                        <tr>
                          <td>
                            <div class="floatright">
                              <button class="BlueButton" name="save" tabindex="10" accesskey="S" onclick="javascript:doSaveAction();">(S)ave </button>
                            </div>
                          </td>
                        </tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
				</form>
				<table width="98%" align="center">
					<tr>
						<td width="78%" valign="top">
							<button class="BlueButton" name="addNew" tabindex="10" accesskey="A" onclick="javascript:doAddNewAction();">(A)dd New </button>
						</td>
						<td width="20%" align="right">
							<button class="BlueButton" name="mainMenu" tabindex="10" accesskey="M" onclick="javascript:doMainMenuAction();">(M)ain Menu </button>
						</td>
					</tr>
				</table>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
        <!-- call footer template-->
        <xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->