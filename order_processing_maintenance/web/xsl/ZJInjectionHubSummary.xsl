<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
		<meta http-equiv="Content-Type" content="text/html"></meta>
		<title>Injection Hub Summary</title>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/ZJInjectionHub.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/util.js"/>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//* Global variables
//****************************************************************************************************
var somethingChanged = false;

    ]]>

		</script>

	</head>
	<body onload="javascript: init()">
		<form name="fInjectionHubSummary" method="post">
			<input type="hidden" name="securitytoken" 	id="securitytoken" 	value="{key('pageData', 'securitytoken')/value}"/>
			<input type="hidden" name="context" 				id="context" 				value="{key('pageData', 'context')/value}"/>
			<input type="hidden" name="adminAction" 		id="adminAction" 		value="{key('pageData', 'adminAction')/value}"/>

			<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td width="30%" height="33" valign="top">
						<div class="floatleft">
							<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
						</div>
					</td>
					<td>&nbsp;</td>
					<td align="right" width="30%" valign="bottom">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center" valign="top" class="Header" id="pageHeader">Injection Hub Summary</td>
					<td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
				</tr>
			</table>
			<table width="98%" align="center" cellspacing="1" class="mainTable" border="0">
				<tr>
					<td>
						<table width="100%" align="center" class="innerTable" border="0">
							<tr>
								<td>
									<table width="100%" align="center" border="1">
										<tr>
											<td class="label" align="center" style="background-color:#ccccff;"></td>
											<td class="label" align="center" style="background-color:#ccccff;">Hub Location</td>
											<td class="label" align="center" style="background-color:#ccccff;">Carrier</td>
											<td class="label" align="center" style="background-color:#ccccff;">Scan Data Account Number</td>
											<td class="label" align="center" style="background-color:#ccccff;">Ship Point Id</td>
										</tr>
										<xsl:for-each select="INJECTION_HUBS/INJECTION_HUB">
											<tr>
												<td align="center"><input type="radio" id="injectionHubRadio" name="injectionHubRadio" value="{injection_hub_id}"/></td>
												<td align="center"><xsl:value-of select="city_name"/>,&nbsp;<xsl:value-of select="state_master_id"/></td>
												<td align="center"><xsl:value-of select="carrier_id"/></td>
												<td align="center"><xsl:value-of select="sds_account_num"/></td>
												<td align="center"><xsl:value-of select="ship_point_id"/></td>
											</tr>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
				<tr>
					<td>
						<table>
							<tr>
								<td align="left">
									<button class="BlueButton" style="width:80;" name="bAdd" accesskey="A" onclick="javascript:doAdd();">
										(A)dd
									</button>
								</td>
								<td align="left">
									<button class="BlueButton" style="width:80;" name="bEdit" accesskey="E" onclick="javascript:doEdit();">
										(E)dit
									</button>
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<table>
							<tr>
								<td align="right" valign="top">
									<button class="BlueButton" style="width:80;" accesskey="M" name="bMainMenu" onclick="javascript:doExit('main_menu');">(M)ain Menu </button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="disclaimer"></td><script>showCopyright();</script>
					
				</tr>
			</table>
		</form>
		<iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
	</body>
</html>


</xsl:template>
</xsl:stylesheet>
