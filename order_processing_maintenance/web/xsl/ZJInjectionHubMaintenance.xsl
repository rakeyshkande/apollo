<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->


<xsl:template match="/root">


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
		<meta http-equiv="Content-Type" content="text/html"></meta>
		<title>Injection Hub Maintenance</title>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"></link>

    <script type="text/javascript" src="js/ZJInjectionHub.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/util.js"/>
	<script type="text/javascript" src="js/copyright.js"></script>

    <script type="text/javascript" language="javascript"><![CDATA[

//****************************************************************************************************
//*  Global variables
//****************************************************************************************************
var somethingChanged = false;

    ]]>

		</script>


	</head>
	<body onload="javascript: init()">
		<form name="fTruckMaintenance" method="post">
			<input type="hidden" name="securitytoken" id="securitytoken"  value="{key('pageData', 'securitytoken')/value}"/>
			<input type="hidden" name="context"       id="context"				value="{key('pageData', 'context')/value}"/>
			<input type="hidden" name="adminAction" 	id="adminAction" 		value="{key('pageData', 'adminAction')/value}"/>
			<input type="hidden" name="ih_id" 				id="ih_id"	 				value="{INJECTION_HUBS/INJECTION_HUB/injection_hub_id}"/>

			<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td width="30%" height="33" valign="top">
						<div class="floatleft">
							<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
						</div>
					</td>
					<td>&nbsp;</td>
					<td align="right" width="30%" valign="bottom">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center" valign="top" class="Header" id="pageHeader">Injection Hub Maintenance</td>
					<td id="time" align="right" valign="bottom" class="Label" width="30%" height="30">&nbsp;</td>
				</tr>
			</table>
			<table width="98%" align="center" cellspacing="1" class="mainTable" border="0" >
				<tr>
					<td>
						<div id="dContent" style="display:block;">
							<table width="100%" align="center" class="innerTable" border="0">
								<tr>
									<td>
										<table width="100%" align="center" border="0">
											<tr>
												<td class="label" align="right" width="30%">Hub City</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<input type="text" id="ih_city" name="ih_city" maxlength="100" size="30" onchange="javascript:somethingChanged=true;">
																	<xsl:if test="INJECTION_HUBS/INJECTION_HUB/city_name">
																		<xsl:attribute name="value">
																			<xsl:value-of select="INJECTION_HUBS/INJECTION_HUB/city_name"/>
																		</xsl:attribute>
																	</xsl:if>
																</input>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="label" align="right" width="30%">Hub State</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<select name="ih_state" id="ih_state" onchange="javascript:somethingChanged=true;">
																	<xsl:for-each select="STATES/STATE">
																		<option value="{id}">
																			<xsl:if test="id = /root/INJECTION_HUBS/INJECTION_HUB/state_master_id">
																				<xsl:attribute name="selected"/>
																			</xsl:if>
																			<xsl:value-of select="name"/>
																		</option>
																	</xsl:for-each>
																</select>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="label" align="right" width="30%">Hub Address</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<input type="text" id="ih_address" name="ih_address" maxlength="250" size="50" onchange="javascript:somethingChanged=true;">
																	<xsl:if test="INJECTION_HUBS/INJECTION_HUB/address_desc">
																		<xsl:attribute name="value">
																			<xsl:value-of select="INJECTION_HUBS/INJECTION_HUB/address_desc"/>
																		</xsl:attribute>
																	</xsl:if>
																</input>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="label" align="right" width="30%">Hub Zip Code</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<input type="text" id="ih_zip" name="ih_zip" maxlength="20" size="5" onchange="javascript:somethingChanged=true;">
																	<xsl:if test="INJECTION_HUBS/INJECTION_HUB/zip_code">
																		<xsl:attribute name="value">
																			<xsl:value-of select="INJECTION_HUBS/INJECTION_HUB/zip_code"/>
																		</xsl:attribute>
																	</xsl:if>
																</input>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="label" align="right" width="30%">Carrier</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<select name="ih_carrier" id="ih_carrier" onchange="javascript:somethingChanged=true;">
																	<xsl:for-each select="carriers/carrier">
																		<option value="{carrier_id}">
																			<xsl:if test="carrier_id = /root/INJECTION_HUBS/INJECTION_HUB/carrier_id">
																				<xsl:attribute name="selected"/>
																			</xsl:if>
																			<xsl:value-of select="carrier_name"/>
																		</option>
																	</xsl:for-each>
																</select>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="label" align="right" width="30%">Scan Data Account Number</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<input type="text" id="ih_SDS_Account_Num" name="ih_SDS_Account_Num" maxlength="50" size="30" onchange="javascript:somethingChanged=true;">
																	<xsl:if test="INJECTION_HUBS/INJECTION_HUB/sds_account_num">
																		<xsl:attribute name="value">
																			<xsl:value-of select="INJECTION_HUBS/INJECTION_HUB/sds_account_num"/>
																		</xsl:attribute>
																	</xsl:if>
																</input>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="label" align="right" width="30%">Ship Point Id</td>
												<td align="left" width="70%">
													<table>
														<tr>
															<td>
																<input type="text" id="ih_ship_point_id" name="ih_ship_point_id" maxlength="50" size="30" onchange="javascript:somethingChanged=true;">
																	<xsl:if test="INJECTION_HUBS/INJECTION_HUB/ship_point_id">
																		<xsl:attribute name="value">
																			<xsl:value-of select="INJECTION_HUBS/INJECTION_HUB/ship_point_id"/>
																		</xsl:attribute>
																	</xsl:if>
																</input>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
				<tr>
					<td>
						<table>
							<tr>
								<td align="left">
									<button class="BlueButton" style="width:80;" name="bSave" accesskey="S" onclick="javascript:doSave();">
										(S)ave
									</button>
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<table>
							<tr>
								<td align="right" valign="top">
									<button class="BlueButton" style="width:80;" accesskey="B" name="bBack" onclick="javascript:doExit('load');">(B)ack </button>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">
									<button class="BlueButton" style="width:80;" accesskey="M" name="bMainMenu" onclick="javascript:doExit('main_menu');">(M)ain Menu </button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="disclaimer"></td><script>showCopyright();</script>
					
				</tr>
			</table>
		</form>
		<iframe id="checkLock" width="0px" height="0px" border="0"></iframe>
	</body>
</html>


</xsl:template>
</xsl:stylesheet>
