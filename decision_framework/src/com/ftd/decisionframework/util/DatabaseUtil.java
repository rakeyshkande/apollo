package com.ftd.decisionframework.util;

import com.ftd.decisionframework.constants.DecisionFrameworkConstants;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;


/**
 * Provides Database utility methods
 */
public class DatabaseUtil
{
  private static DatabaseUtil databaseUtil = new DatabaseUtil();
  
  public static DatabaseUtil getInstance()
  {
    return databaseUtil;
  }

    static public Connection getConnection() throws Exception
    {
      return getConnection("DATABASE_CONNECTION");
    }  
    
    static public Connection getConnection(String connectionName) throws Exception
    {
      
      ConfigurationUtil config = ConfigurationUtil.getInstance();            
      String dbConnection = config.getProperty(DecisionFrameworkConstants.CONFIG_FILE,connectionName);         
     
      //get DB connection
      DataSource dataSource = (DataSource)lookupResource(dbConnection);
      return dataSource.getConnection();  
    }
    
    
    
    static public void closeConnection(Connection conn)
    {
      if(conn != null)
      {
      try
      {
        conn.close();
      }
      catch (SQLException e)
      {
        
      }
    }
    }
    
    
    static public DataSource getDataSource() throws Exception 
    {
      ConfigurationUtil config = ConfigurationUtil.getInstance();            
      String dbConnection = config.getProperty(DecisionFrameworkConstants.CONFIG_FILE,"DATABASE_CONNECTION");         
     
      //get DB connection
      DataSource dataSource = (DataSource)lookupResource(dbConnection);
      return dataSource;
      
    }
    
    
        /**
     * Returns a transactional resource from the EJB container.
     * 
     * @param jndiName
     * @return 
     * @throws javax.naming.NamingException
     */
    public static Object lookupResource(String jndiName)
                throws NamingException
    {
      InitialContext initContext = null;
      try
      {
        initContext = new InitialContext();
           
        return initContext.lookup(jndiName);      
      }finally  {
        try  {
          initContext.close();
        } catch (Exception ex)  {

        } finally  {
        }
      }
    }  
}
