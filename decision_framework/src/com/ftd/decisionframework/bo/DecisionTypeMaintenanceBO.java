package com.ftd.decisionframework.bo;

import com.ftd.decisionframework.dao.DecisionTypeMaintenanceDAO;
import com.ftd.decisionframework.vo.DecisionConfigVO;
import com.ftd.decisionframework.vo.DecisionTypeVO;
import com.ftd.decisionframework.vo.RuleTypeVO;
import com.ftd.decisionframework.vo.ValueNodeVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.List;

/**
 * Provides the Business Methods for maintaining the Decision Type Information
 */
public class DecisionTypeMaintenanceBO
{
  Logger logger = new Logger(DecisionTypeMaintenanceBO.class.getName());
  
  public DecisionTypeMaintenanceBO()
  {
  }
  
  /**
   * Retrieves the Decision Type Information
   * @param typeCode
   * @param connection
   * @return
   */
  public DecisionTypeVO getDecisionTypeInfo(String typeCode, Connection connection) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    return decisionTypeMaintenanceDAO.getDecisionTypeInfo(typeCode, connection);
  }


  /**
   * Retrieves the Heirarchy for the Decision Type. 
   * This method returns the heirarchy for Admin functions. 
   * It will retrieve a Heirarchy in DRAFT status if there is one. Otherwise, it will return
   * the current Heirarchy that is Published.
   * @param decisionTypeCode
   * @param connection
   * @return
   */
  public DecisionConfigVO getConfigurationHeirarchy(String decisionTypeCode, Connection connection) throws Exception
  {
    DecisionConfigVO decisionConfigVO;
    
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    
    List<DecisionConfigVO> configList = decisionTypeMaintenanceDAO.getDecisionTypeConfigByStatus(decisionTypeCode, connection, DecisionConfigVO.STATUS.DRAFT.toString());
    if(configList == null || configList.size() == 0)
    {
      configList = decisionTypeMaintenanceDAO.getDecisionTypeConfigByStatus(decisionTypeCode, connection, DecisionConfigVO.STATUS.PUBLISH.toString());
    }
    
    if(configList == null || configList.size() == 0)
    {
      // Create a new Record, and return it if this is a valid Type Code
      decisionConfigVO = new DecisionConfigVO();
      decisionConfigVO.setStatus(DecisionConfigVO.STATUS.DRAFT);
      decisionConfigVO.setTypeCode(decisionTypeCode); 
      
      //decisionConfigVO.setRootHeirarchyNode(HeirarchyAdminBO.rootNode);
      
      //persist this record
      decisionTypeMaintenanceDAO.createDraftConfiguration(decisionTypeCode, connection);
      
    } else 
    {
      decisionConfigVO = configList.get(0);
    }  
  
    return decisionConfigVO;
  }
  
  
  public long saveValueNode(ValueNodeVO valueNodeVO, Connection connection) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    return decisionTypeMaintenanceDAO.saveValueNode(valueNodeVO, connection);    
  }
  
  public void updateValueNode(ValueNodeVO valueNodeVO, Connection connection) throws Exception
  {
     DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
     
    
  }
  
  public List<ValueNodeVO> getAllDraftNodesForConfig(String decisionTypeCode, Connection connection) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    List<ValueNodeVO> valueNodeList =  decisionTypeMaintenanceDAO.getValueNodesByStatusForDecisionType(decisionTypeCode, "DRAFT", connection);
    if(valueNodeList == null || valueNodeList.size() == 0)
    {
      DecisionConfigVO decisionConfigVO = new DecisionConfigVO();
      decisionConfigVO.setStatus(DecisionConfigVO.STATUS.DRAFT);
      decisionConfigVO.setTypeCode(decisionTypeCode); 
      decisionTypeMaintenanceDAO.createDraftConfiguration(decisionTypeCode, connection);
      valueNodeList =  decisionTypeMaintenanceDAO.getValueNodesByStatusForDecisionType(decisionTypeCode, "DRAFT", connection);
    }
     return valueNodeList;
  }
   
    
  public List<RuleTypeVO> getValueRules(long valueId, long decisionConfigId, Connection conn) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    return decisionTypeMaintenanceDAO.getValueRulesForConfig(valueId, decisionConfigId, conn);
  }
 
  public List<ValueNodeVO> getHierarchyNodes(long valueId, long decisionConfigId, Connection conn) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    return decisionTypeMaintenanceDAO.getHierarchyNodesForConfig(valueId, decisionConfigId, conn);        
  }
  
  public List<RuleTypeVO> getRulesForDecisionType(String decisionTypeCode, Connection conn) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    return decisionTypeMaintenanceDAO.getRulesForDecisionType(decisionTypeCode, conn);  
  }
  
  public void deleteValueNode(long valueId, long decisionConfigId, Connection conn) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    decisionTypeMaintenanceDAO.deleteValueNode(valueId, decisionConfigId, conn);
  }
  
   public void publishDecisionType(String decisionTypeCode, long decisionConfigId, String userId, Connection conn) throws Exception
  {
    DecisionTypeMaintenanceDAO decisionTypeMaintenanceDAO = new DecisionTypeMaintenanceDAO();
    decisionTypeMaintenanceDAO.publishDecisionType(decisionTypeCode, decisionConfigId, userId, conn);
  }
  
}
