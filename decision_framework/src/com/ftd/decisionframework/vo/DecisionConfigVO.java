package com.ftd.decisionframework.vo;

public class DecisionConfigVO extends BaseVO
{
  long configId;
  String typeCode;
  STATUS status;
  ValueNodeVO rootHeirarchyNode;

  public enum STATUS {DRAFT, PUBLISH, ARCHIVE};

  public DecisionConfigVO()
  {
  }

  public void setConfigId(long configId)
  {
    this.configId = configId;
  }

  public long getConfigId()
  {
    return configId;
  }

  public void setTypeCode(String typeCode)
  {
    this.typeCode = typeCode;
  }

  public String getTypeCode()
  {
    return typeCode;
  }

  public void setStatus(DecisionConfigVO.STATUS status)
  {
    this.status = status;
  }

  public DecisionConfigVO.STATUS getStatus()
  {
    return status;
  }

  public void setRootHeirarchyNode(ValueNodeVO rootHeirarchyNode)
  {
    this.rootHeirarchyNode = rootHeirarchyNode;
  }

  public ValueNodeVO getRootHeirarchyNode()
  {
    return rootHeirarchyNode;
  }

}
