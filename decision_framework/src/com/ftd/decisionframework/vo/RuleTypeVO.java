package com.ftd.decisionframework.vo;

public class RuleTypeVO extends BaseVO
{
  private String ruleCode;
  private String name;
  private boolean nextLevelFlag;
  private int displayOrder;
  private boolean isActiveFlag;


  public void setRuleCode(String ruleCode)
  {
    this.ruleCode = ruleCode;
  }

  public String getRuleCode()
  {
    return ruleCode;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setNextLevelFlag(boolean nextLevelFlag)
  {
    this.nextLevelFlag = nextLevelFlag;
  }

  public boolean isNextLevelFlag()
  {
    return nextLevelFlag;
  }

  public void setDisplayOrder(int displayOrder)
  {
    this.displayOrder = displayOrder;
  }

  public int getDisplayOrder()
  {
    return displayOrder;
  }

  public void setIsActiveFlag(boolean isActiveFlag)
  {
    this.isActiveFlag = isActiveFlag;
  }

  public boolean isIsActiveFlag()
  {
    return isActiveFlag;
  }
}
