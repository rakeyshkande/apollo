package com.ftd.decisionframework.vo;

import java.io.Serializable;

import java.util.Date;

public class BaseVO implements Serializable
{

  protected String createdBy;
  protected Date createdOn;
  protected String updateBy;
  protected Date updatedOn;


  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }

  public String getCreatedBy()
  {
    return createdBy;
  }

  public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }

  public void setUpdateBy(String updateBy)
  {
    this.updateBy = updateBy;
  }

  public String getUpdateBy()
  {
    return updateBy;
  }

  public void setUpdatedOn(Date updatedOn) throws Exception
  {
    this.updatedOn = updatedOn;
  }

  public Date getUpdatedOn()
  {
    return updatedOn;
  }
}
