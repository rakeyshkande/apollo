package com.ftd.decisionframework.vo;

public class ValueRuleVO extends BaseVO
{
  private long valueId;
  private long decisionConfigId;
  private String ruleCode;


  public void setValueId(long valueId)
  {
    this.valueId = valueId;
  }

  public long getValueId()
  {
    return valueId;
  }

  public void setDecisionConfigId(long decisionConfigId)
  {
    this.decisionConfigId = decisionConfigId;
  }

  public long getDecisionConfigId()
  {
    return decisionConfigId;
  }

  public void setRuleCode(String ruleCode)
  {
    this.ruleCode = ruleCode;
  }

  public String getRuleCode()
  {
    return ruleCode;
  }
}
