package com.ftd.decisionframework.vo;

import java.util.List;

public class TreeMainVO extends BaseVO
{
  
    private List valueNodes;
    private List allRules;


  public void setValueNodes(List valueNodes)
  {
    this.valueNodes = valueNodes;
  }

  public List getValueNodes()
  {
    return valueNodes;
  }

  public void setAllRules(List allRules)
  {
    this.allRules = allRules;
  }

  public List getAllRules()
  {
    return allRules;
  }
}
