package com.ftd.decisionframework.vo;

import java.util.ArrayList;
import java.util.List;


/**
 * This VO represents a Decision Type that is supported by the Framework
 */
public class DecisionTypeVO extends BaseVO
{
  private String decisionTypeCode;
  private String name;
  private String adminResourceId;
  private String widgetResourceId;
  private List<RuleTypeVO> ruleTypes = new ArrayList<RuleTypeVO>();

  public void setDecisionTypeCode(String decisionTypeCode)
  {
    this.decisionTypeCode = decisionTypeCode;
  }

  public String getDecisionTypeCode()
  {
    return decisionTypeCode;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setAdminResourceId(String adminResourceId)
  {
    this.adminResourceId = adminResourceId;
  }

  public String getAdminResourceId()
  {
    return adminResourceId;
  }

  public void setWidgetResourceId(String widgetResourceId)
  {
    this.widgetResourceId = widgetResourceId;
  }

  public String getWidgetResourceId()
  {
    return widgetResourceId;
  }


  public void setRuleTypes(List<RuleTypeVO> ruleTypes)
  {
    this.ruleTypes = ruleTypes;
  }

  public List<RuleTypeVO> getRuleTypes()
  {
    return ruleTypes;
  }
}
