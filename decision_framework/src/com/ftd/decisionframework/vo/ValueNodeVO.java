package com.ftd.decisionframework.vo;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ValueNodeVO extends BaseVO
{
  private Logger logger = new Logger(ValueNodeVO.class.getName());

  private long valueId;
  private long decisionConfigId;
  private String name;
  private boolean isActive = true;
  private String scriptText;
  private boolean allowComments = false;
  private long groupOrder;
  private int level;
  private String valueRuleIds;
  private String childValueIds;
  private List<ValueRuleVO> rules = new ArrayList<ValueRuleVO>();
  private String updatedOnStr;
  
  // This automatically tracks parentNode
  long parentNodeId = -1; 
  
  public ValueNodeVO() 
  {
  }
  
  public ValueNodeVO(long id, String name) 
  {
    this.valueId = id;
    this.name = name;
  }


  

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }
  


  public void setIsActive(boolean isActive)
  {
    this.isActive = isActive;
  }

  public boolean isIsActive()
  {
    return isActive;
  }

  public void setScriptText(String scriptText)
  {
    this.scriptText = scriptText;
  }

  public String getScriptText()
  {
    return scriptText;
  }

  public void setAllowComments(boolean allowComments)
  {
    this.allowComments = allowComments;
  }

  public boolean isAllowComments()
  {
    return allowComments;
  }

 
  public void setLevel(int level)
  {
    this.level = level;
  }

  public int getLevel()
  {
    return level;
  }


  public void setRules(List<ValueRuleVO> rules)
  {
    this.rules = rules;
  }

  public List<ValueRuleVO> getRules()
  {
    return rules;
  }

  public void setParentNodeId(long parentNodeId)
  {
    this.parentNodeId = parentNodeId;
  }

  public long getParentNodeId()
  {
    return parentNodeId;
  }

  public void setValueId(long valueId)
  {
    this.valueId = valueId;
  }

  public long getValueId()
  {
    return valueId;
  }


  public void setDecisionConfigId(long decisionConfigId)
  {
    this.decisionConfigId = decisionConfigId;
  }

  public long getDecisionConfigId()
  {
    return decisionConfigId;
  }
 
  public void setUpdatedOn(Date updatedOn)
    throws Exception
  {
    this.updatedOn = updatedOn;
    try
    {
      if(updatedOn != null){
        this.updatedOnStr = FieldUtils.formatUtilDateToString(updatedOn);
      }
    }
    catch (Exception e)
    {
      logger.error("Error occured while converting util updated date to String.");
      throw e;
    }
  }
 

  public void setUpdatedOnStr(String updatedOnStr)
  {
    this.updatedOnStr = updatedOnStr;
  }

  public String getUpdatedOnStr()
  {
    return updatedOnStr;
  }


  public void setValueRuleIds(String valueRuleIds)
  {
    this.valueRuleIds = valueRuleIds;
  }

  public String getValueRuleIds()
  {
    return valueRuleIds;
  }

  public void setChildValueIds(String childValueIds)
  {
    this.childValueIds = childValueIds;
  }

  public String getChildValueIds()
  {
    return childValueIds;
  }

  public void setGroupOrder(long groupOrder)
  {
    this.groupOrder = groupOrder;
  }

  public long getGroupOrder()
  {
    return groupOrder;
  }
}
