package com.ftd.decisionframework.actions;

import com.ftd.decisionframework.bo.DecisionTypeMaintenanceBO;
import com.ftd.decisionframework.constants.DecisionFrameworkConstants;
import com.ftd.decisionframework.util.DatabaseUtil;
import com.ftd.decisionframework.vo.DecisionTypeVO;
import com.ftd.decisionframework.vo.RuleTypeVO;
import com.ftd.decisionframework.vo.ValueNodeVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

import java.io.IOException;
import java.io.Writer;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class AdminMainAction
  extends BaseAction
{
  private Logger logger = new Logger(AdminMainAction.class.getName());
  private static final String CS_MENU = "CS_MENU_URL";
  private SecurityManager securityManager;
  
  

  
  public ActionForward adminMain(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
    logger.info("adminMain");
    
      
    
    Connection connection = null;
    
        
    ActionForward forward = mapping.findForward("AdminMain");
       
    DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();
    
    try
    {
      
      init(request);
      String decisionType = request.getParameter(REQUEST_PARAM_DECISION_TYPE);
      logger.info("decisionType: " + decisionType);
      connection = DatabaseUtil.getInstance().getConnection();
      
      DecisionTypeVO decisionTypeVO = decisionTypeMaintenanceBO.getDecisionTypeInfo(decisionType, connection);
      
      request.setAttribute("typeCode",decisionTypeVO.getDecisionTypeCode());
      request.setAttribute("name",decisionTypeVO.getName());
      request.setAttribute(DecisionFrameworkConstants.SEC_TOKEN, request.getParameter(DecisionFrameworkConstants.SEC_TOKEN));
      request.setAttribute(DecisionFrameworkConstants.CONTEXT, request.getParameter(DecisionFrameworkConstants.CONTEXT));            
      if(decisionTypeVO == null)
      {
        throw new Exception("Unable to Retrieve Decision Type Information for: " + decisionType);
      }
      

    }
    catch (Exception e)
    {
      logger.error("Error occured in adminMain method of AdminMainAction");
      logger.error(e);
      throw new RuntimeException(e);
    } finally
    {
      DatabaseUtil.getInstance().closeConnection(connection);
    }
    
    return forward;
  }



  public ActionForward valueRules(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
  
    DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();
    Connection conn = null;
    try{
      init(request);
      long valueId = Long.parseLong(request.getParameter("valueNodeId"));
      long configId = Long.parseLong(request.getParameter(DecisionFrameworkConstants.DECISION_CONFIG_ID));
      conn = DatabaseUtil.getInstance().getConnection();
      List<RuleTypeVO> valueRules = decisionTypeMaintenanceBO.getValueRules(valueId, configId, conn);
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
        public HierarchicalStreamWriter createWriter(Writer writer) {
            return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
        }
        });
      response.getOutputStream().write( xstream.toXML(valueRules).getBytes());
          
          
        
      
    }
    catch (Exception e)
    {
      logger.error("Error occured while getting value rules.");
      logger.error(e);
      throw new RuntimeException(e);
    } finally
    {
      DatabaseUtil.getInstance().closeConnection(conn);
    }
    return null;
  }
  
  
  public ActionForward hierarchyNodes(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
      DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();
      Connection conn = null;
    try{
      init(request);
      long valueId = Long.parseLong(request.getParameter("valueNodeId"));
      long configId = Long.parseLong(request.getParameter(DecisionFrameworkConstants.DECISION_CONFIG_ID));
      conn = DatabaseUtil.getInstance().getConnection();
      List<ValueNodeVO> hierarchyNodes = decisionTypeMaintenanceBO.getHierarchyNodes(valueId, configId, conn);
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
        public HierarchicalStreamWriter createWriter(Writer writer) {
            return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
        }
      });
        
        
        //System.out.println("Hierarchy Nodes : "+ xstream.toXML(hierarchyNodes));
        response.getOutputStream().write( xstream.toXML(hierarchyNodes).getBytes());
    }
    catch (Exception e)
    {
      logger.error("Error occured whille getting hierarchy nodes.");
      logger.error(e);
      throw new RuntimeException(e);
    } finally
    {
      DatabaseUtil.getInstance().closeConnection(conn);
    }
    return null;
  }


  
    
  public ActionForward saveValueNode(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
    DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();
    Connection conn = null;
    List<String> result = new ArrayList<String>();
    try
    {
      init(request);
      ValueNodeVO valueNodeVO = populateValueNode(request);
      conn = DatabaseUtil.getInstance().getConnection();
      long outValueId = decisionTypeMaintenanceBO.saveValueNode(valueNodeVO, conn);
      
      result.add(String.valueOf(outValueId));
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
          public HierarchicalStreamWriter createWriter(Writer writer) {
              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
          }
      });
      response.getOutputStream().write(xstream.toXML(result).getBytes());
    }catch(Exception e)
    {
      logger.error("Error occured while saving value node.");
      logger.error(e);
      throw new RuntimeException(e);
    }finally
    {
      DatabaseUtil.getInstance().closeConnection(conn);  
    }
    return null;
  }
  
  
   public ActionForward deleteValueNode(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
    long valueId;
    long decisionConfigId;
    DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();
    Connection conn = null;
    List<String> info = new ArrayList<String>();
    try{
      init(request);
      if(request.getParameter(DecisionFrameworkConstants.NODE_ID) == null){
        throw new Exception("Invalid Value ID.");
      }
      if(request.getParameter(DecisionFrameworkConstants.DECISION_CONFIG_ID) == null)
      {
        throw new Exception("Invalid Decision Config ID.");
      }
      valueId = Long.parseLong(request.getParameter(DecisionFrameworkConstants.NODE_ID));
      decisionConfigId = Long.parseLong(request.getParameter(DecisionFrameworkConstants.DECISION_CONFIG_ID));
      conn = DatabaseUtil.getInstance().getConnection();
      decisionTypeMaintenanceBO.deleteValueNode(valueId, decisionConfigId, conn);
      info = new ArrayList<String>();
      info.add("INFO:Value has been successfully deleted.");
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
          public HierarchicalStreamWriter createWriter(Writer writer) {
              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
          }
      });
      response.getOutputStream().write(xstream.toXML(info).getBytes());
    }catch(Exception e)
    {
      logger.error("Error occured while deleting value node.");
      logger.error(e);
      List<String> errors = null;
      if(e.getMessage().contains("FTD_APPS.DCSN_FX_CONFIG_VALUE_PARENT_FK"))
      {
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
          public HierarchicalStreamWriter createWriter(Writer writer) {
              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
          }
      });
      
          
      errors = new ArrayList<String>();
      errors.add("This value cannot be deleted. Please delete all child values first.");
      response.getOutputStream().write(xstream.toXML(errors).getBytes());
          
         
      
      }else{
        throw new RuntimeException(e);
      }
    }finally
    {
      DatabaseUtil.getInstance().closeConnection(conn);  
    }
    return null;
  }
  
  
  public ActionForward pulishDecisionType(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
    
    DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();
    Connection conn = null;
    long decisionConfigId;
    String decisionTypeCode;
    String userName;
     List<String> info = null;
    try
    {
      init(request);
      decisionConfigId = Long.parseLong(request.getParameter(DecisionFrameworkConstants.DECISION_CONFIG_ID));
      decisionTypeCode = request.getParameter(DecisionFrameworkConstants.DECISION_TYPE_CODE);
      conn = DatabaseUtil.getInstance().getConnection();
      userName = this.userId;
      decisionTypeMaintenanceBO.publishDecisionType(decisionTypeCode, decisionConfigId, userName, conn);
      info = new ArrayList<String>();
      info.add("All changes have been published.");
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
          public HierarchicalStreamWriter createWriter(Writer writer) {
              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
          }
      });
      response.getOutputStream().write(xstream.toXML(info).getBytes());
    }catch(Exception e)
    {
      logger.error("Error occured while publishing the decision type");
      logger.error(e);
      throw new RuntimeException(e);
    }finally
    {
      DatabaseUtil.getInstance().closeConnection(conn);  
    }
    return null;
  
  }
  
 
  public ActionForward mainMenu(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException, Exception
  {
    logger.debug("AdminMainAction.mainMenu");
    String  menuUrl     = "";
    HashMap requestParms    = new HashMap();
   
    try{
    //Get info passed in the request object
      requestParms = getRequestInfo(request);
      String serverName = request.getServerName();
      int portNumber = request.getServerPort();
      String baseUrl = "http://"+serverName+":"+portNumber;
      menuUrl = mapping.findForward(CS_MENU).getPath();     
      menuUrl = baseUrl + menuUrl + "&" + DecisionFrameworkConstants.SEC_TOKEN + "=" 
            + requestParms.get(DecisionFrameworkConstants.SEC_TOKEN) + "&" + DecisionFrameworkConstants.CONTEXT 
            + "=" + requestParms.get(DecisionFrameworkConstants.CONTEXT);
      //logger.info("exiting to IE8 " + menuUrl);
      //logger.info("context=" + context);
     } catch (Exception e) {
         logger.error("Error occured in mainMenu method of AdminMainAction");
         logger.error(e);
         return mapping.findForward("ERROR");
     }
     return new ActionForward(menuUrl, true);

  }
  
  
  private ValueNodeVO populateValueNode(HttpServletRequest req) throws Exception
  {
    HttpSession session = req.getSession();
    ValueNodeVO valueNodeVO = new ValueNodeVO();
    if(req.getParameter(DecisionFrameworkConstants.NODE_ID) != null){
      valueNodeVO.setValueId(Long.parseLong(req.getParameter(DecisionFrameworkConstants.NODE_ID)));
    }
    valueNodeVO.setName(req.getParameter(DecisionFrameworkConstants.NODE_NAME));
    
    if(req.getParameter(DecisionFrameworkConstants.PARENT_VALUE_ID) != null){
      valueNodeVO.setParentNodeId(Long.parseLong(req.getParameter(DecisionFrameworkConstants.PARENT_VALUE_ID)));
    }
    valueNodeVO.setScriptText(req.getParameter(DecisionFrameworkConstants.SCRIPTING_TEXT));
    if(req.getParameter(DecisionFrameworkConstants.NODE_STATUS) != null && req.getParameter(DecisionFrameworkConstants.NODE_STATUS).equalsIgnoreCase("Y")){
      valueNodeVO.setIsActive(true);
    }else if(req.getParameter(DecisionFrameworkConstants.NODE_STATUS) != null && req.getParameter(DecisionFrameworkConstants.NODE_STATUS).equalsIgnoreCase("N"))
    {
      valueNodeVO.setIsActive(false);
    }
    if(req.getParameter(DecisionFrameworkConstants.ALLOW_COMMENTS) != null && req.getParameter(DecisionFrameworkConstants.ALLOW_COMMENTS).equalsIgnoreCase("Y")){
      valueNodeVO.setAllowComments(true);
    }else
    {
      valueNodeVO.setAllowComments(false);
    }
    valueNodeVO.setUpdateBy(this.userId);
    valueNodeVO.setDecisionConfigId(Long.parseLong(req.getParameter(DecisionFrameworkConstants.DECISION_CONFIG_ID)));
    if(req.getParameter(DecisionFrameworkConstants.CHILD_NODES) != null){
      valueNodeVO.setChildValueIds(req.getParameter(DecisionFrameworkConstants.CHILD_NODES));
    }
    if(req.getParameter(DecisionFrameworkConstants.VALUE_RULES) != null){
      valueNodeVO.setValueRuleIds(req.getParameter(DecisionFrameworkConstants.VALUE_RULES));
    }
    return valueNodeVO;
  }
  
  
 /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none 
  * @throws none
  */
  private HashMap getRequestInfo(HttpServletRequest request)
  {
    String  adminAction                   = "";
    String  context                       = "";
    String  securityToken                 = ""; 
    HashMap requestParms                  = new HashMap();    
  

    /**********************retreive the security info**********************/
    //retrieve the context
    if(request.getParameter(DecisionFrameworkConstants.ADMIN_ACTION)!=null)
      adminAction = request.getParameter(DecisionFrameworkConstants.ADMIN_ACTION);

    //retrieve the context
    if(request.getParameter(DecisionFrameworkConstants.CONTEXT)!=null)
      context = request.getParameter(DecisionFrameworkConstants.CONTEXT);

    //retrieve the security token
    if(request.getParameter(DecisionFrameworkConstants.SEC_TOKEN)!=null)
      securityToken = request.getParameter(DecisionFrameworkConstants.SEC_TOKEN);

 
    requestParms.put(DecisionFrameworkConstants.ADMIN_ACTION, adminAction);
    requestParms.put(DecisionFrameworkConstants.CONTEXT, context);
    requestParms.put(DecisionFrameworkConstants.SEC_TOKEN, securityToken);
  
  
    return requestParms;  
  }  
  
}
