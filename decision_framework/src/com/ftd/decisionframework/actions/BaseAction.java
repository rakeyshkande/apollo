package com.ftd.decisionframework.actions;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.apache.struts.actions.DispatchAction;
import com.ftd.decisionframework.constants.DecisionFrameworkConstants;
import org.w3c.dom.Document;


/**
 * Common Functionality shared by all Actions
 */
public abstract class BaseAction extends DispatchAction{
    protected static Log logger = LogFactory.getLog(BaseAction.class.getName());
    protected static final String ERROR_PAGE = "errorPage";
    private static final String REQUEST_KEY_SECURITY_TOKEN = "securitytoken";
    private static final String REQUEST_KEY_CONTEXT = "context";
    private static final String REQUEST_KEY_APP_CONTEXT = "applicationcontext";

    public static final String REQUEST_PARAM_DECISION_TYPE = "decisionType";

    // These are field members that store FTD context metadata for each
    // servlet instance.
    // These members are initialized by initFtdContext().
    // FTD app security data.
    protected String context = null;
    protected String applicationContext = null;
    protected String userId;

    /**
    * Convenience method that initializes FTD-related context metadata
    * that is common to all servlets.
    * @param cleanFtdParams
    */
    protected void initFtdContext(Map cleanFtdParams) {
        // Initialize security context.
        context = (String) cleanFtdParams.get(REQUEST_KEY_CONTEXT);
        applicationContext = (String) cleanFtdParams.get(REQUEST_KEY_APP_CONTEXT);
    }
    
    
  protected void init( HttpServletRequest request)
  {   
    try{
      String securityToken = request.getParameter(DecisionFrameworkConstants.SEC_TOKEN);
            if(securityToken == null)
                throw new RuntimeException("Disposition - Security Token request parameter can not be null, try login again.");
      String context = request.getParameter(DecisionFrameworkConstants.CONTEXT);
            if(context == null)
                throw new RuntimeException("Disposition- context request parameter can not be null, try login again.");
       if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
      {
        userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
      }
    
    
    }catch (Exception e)
    {
      logger.error("Error occured in init method of Base action");
      logger.error(e);
      throw new RuntimeException(e);
    }
  }

    /*public abstract ActionForward execute(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException;*/


    public String lookupCurrentUserId(String securityToken)
        throws Exception {
        String userId = "";

        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
            userId = SecurityManager.getInstance().getUserInfo(securityToken)
                                    .getUserID();
        }

        return userId;
    }

    protected HashMap cleanHttpParameters(HttpServletRequest request) {
        HashMap result = new HashMap();

        for (Enumeration i = request.getParameterNames(); i.hasMoreElements();) {
            String key = (String) i.nextElement();
            String value = request.getParameter(key);

            if ((value != null) && (value.trim().length() > 0)) {
                result.put(key, value.trim());
            }
        }

        return result;
    }

    protected void appendPageDataToXml(Document responseDoc,
        HashMap metadataMap) {
        DOMUtil.addSection(responseDoc, "pageData", "data", metadataMap, false);
    }


    protected void printXml(Document responseDoc) throws Exception {
        StringWriter sw = new StringWriter(); //string representation of xml document
        DOMUtil.print(responseDoc, new PrintWriter(sw));
        logger.debug("printXml: request data DOM = \n" + sw.toString());
    }



    protected String extractSessionId(Map cleanFtdParams) {
        return (String) cleanFtdParams.get(REQUEST_KEY_SECURITY_TOKEN);
        //        return "FTD_GUID_10323358010155648063308421399370-12548460550-7407807470-2046635676013981173030205226370113624679920-348377806016605573041-1885770612415046532782061472589767186-1012330199244803411327212";
    }
    
    
  protected void addXMLSection(Document xmlDocument, String section)
  throws Exception
  {
    Document newDocument = DOMUtil.getDocument(section);    
    DOMUtil.addSection(xmlDocument, newDocument.getChildNodes());
  }
    

}
