package com.ftd.decisionframework.actions;

import com.ftd.decisionframework.bo.DecisionTypeMaintenanceBO;
import com.ftd.decisionframework.constants.DecisionFrameworkConstants;
import com.ftd.decisionframework.util.DatabaseUtil;
import com.ftd.decisionframework.vo.RuleTypeVO;
import com.ftd.decisionframework.vo.TreeMainVO;
import com.ftd.decisionframework.vo.ValueNodeVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.SecurityManager;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

import java.io.IOException;
import java.io.Writer;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action retrieves the Heirarchy data as XML and returns it.
 * It is called typically called by an AJAX call.
 */
public class GetHierarchyAction
  extends BaseAction
{
  private Logger logger = new Logger(GetHierarchyAction.class.getName());
  private String userId;
  
 
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
    String decisionType = request.getParameter(REQUEST_PARAM_DECISION_TYPE);
    logger.info("decisionType: " + decisionType);

    Connection connection = null;
    
    TreeMainVO treeMainVO = new TreeMainVO();
    
    DecisionTypeMaintenanceBO decisionTypeMaintenanceBO = new DecisionTypeMaintenanceBO();

    try
    {
      init(request);
      connection = DatabaseUtil.getInstance().getConnection();
      
      List<ValueNodeVO> valueNodes = new ArrayList<ValueNodeVO>();
      List<RuleTypeVO> allRules = new ArrayList<RuleTypeVO>();
      
      valueNodes = decisionTypeMaintenanceBO.getAllDraftNodesForConfig(decisionType, connection);
      
      treeMainVO.setValueNodes(valueNodes);

      treeMainVO.setAllRules(decisionTypeMaintenanceBO.getRulesForDecisionType(decisionType,connection));
      XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
      public HierarchicalStreamWriter createWriter(Writer writer) {
          return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
      }
      });
      response.getOutputStream().write( xstream.toXML(treeMainVO).getBytes());
     
    } catch (Exception e)
    {
      logger.error("Error occured while retrieving the Heirarchy data");
      logger.error(e);
      throw new RuntimeException(e);      
    }  finally
    {
      DatabaseUtil.getInstance().closeConnection(connection);
    }

    return null;
  }


 
  
  
  
}
