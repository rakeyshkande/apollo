package com.ftd.decisionframework.dao;

import com.ftd.decisionframework.constants.DecisionFrameworkConstants;
import com.ftd.decisionframework.vo.DecisionConfigVO;
import com.ftd.decisionframework.vo.DecisionTypeVO;
import com.ftd.decisionframework.vo.RuleTypeVO;
import com.ftd.decisionframework.vo.ValueNodeVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Provides DAO Methods for accessing the tables for the framework
 */
public class DecisionTypeMaintenanceDAO
{
  Logger logger = new Logger(DecisionTypeMaintenanceDAO.class.getName());
  
  public DecisionTypeMaintenanceDAO()
  {
  }
  
    /**
   * Retrieves the Decision Type Information
   * @param typeCode
   * @param connection
   * @return
   */
  public DecisionTypeVO getDecisionTypeInfo(String typeCode, Connection connection) throws Exception
  {    
    DecisionTypeVO decisionTypeVO = null;
    List<RuleTypeVO> ruleTypes = new ArrayList<RuleTypeVO>();
    Map inputs = new HashMap();
    
    inputs.put("IN_DECISION_TYPE", typeCode);
    
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_DECISION_TYPE);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request); 
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }    
    decisionTypeVO = new DecisionTypeVO();
    
    decisionTypeVO.setDecisionTypeCode(typeCode);
    decisionTypeVO.setName((String)results.get("OUT_NAME"));
    decisionTypeVO.setAdminResourceId((String)results.get("OUT_ADMIN_RESOURCE_ID"));
    decisionTypeVO.setWidgetResourceId((String)results.get("OUT_WIDGET_RESOURCE_ID"));
        
    decisionTypeVO.setRuleTypes(ruleTypes);
        
    return decisionTypeVO;    
  }

  /**
   * Returns all Decision Type Configurations with the given status
   * @param decisionTypeCode
   * @param connection
   * @param status
   * @return
   */
  public List<DecisionConfigVO> getDecisionTypeConfigByStatus(String decisionTypeCode, Connection connection, String status)
    throws Exception
  {
    List<DecisionConfigVO> decisionConfigs = new ArrayList<DecisionConfigVO>();
    
    Map inputs = new HashMap();
    
    inputs.put("IN_DECISION_TYPE", decisionTypeCode);
    inputs.put("IN_STATUS_CODE", status);
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_DECISION_TYPE_CONFIG_BY_STATUS);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String outStatus = (String) results.get("OUT_STATUS");
    if(outStatus != null && outStatus.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    CachedResultSet resultSet = (CachedResultSet)results.get("OUT_CURSOR");
    while(resultSet.next())
    {
      DecisionConfigVO decisionConfigVO = new DecisionConfigVO();
      decisionConfigVO.setConfigId(resultSet.getLong("decision_config_id"));
      if(decisionConfigVO.getStatus().DRAFT.equals(status))
      {
        decisionConfigVO.setStatus(decisionConfigVO.getStatus().DRAFT);
      }else if(decisionConfigVO.getStatus().PUBLISH.equals(status))
      {
        decisionConfigVO.setStatus(decisionConfigVO.getStatus().PUBLISH);
      }else
      {
        decisionConfigVO.setStatus(decisionConfigVO.getStatus().ARCHIVE);
      }
      decisionConfigVO.setTypeCode(resultSet.getString("decision_type_code"));
      
      decisionConfigs.add(decisionConfigVO);
      
    }
    
    return decisionConfigs;
  }
  
  
  public DecisionConfigVO getDecisionTypeConfigById(long decisionConfigId, Connection connection, String status) throws Exception
  {
    DecisionConfigVO decisionConfigVO = new DecisionConfigVO();
    Map inputs = new HashMap();
    
    inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_DECISION_TYPE_CONFIG_BY_ID);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String outStatus = (String) results.get("OUT_STATUS");
    if(outStatus != null && outStatus.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    CachedResultSet resultSet = (CachedResultSet)results.get("OUT_CURSOR");
    if(resultSet.next())
    {
      
      decisionConfigVO.setConfigId(resultSet.getLong("decision_config_id"));
      if(decisionConfigVO.getStatus().DRAFT.equals(status))
      {
        decisionConfigVO.setStatus(decisionConfigVO.getStatus().DRAFT);
      }else if(decisionConfigVO.getStatus().PUBLISH.equals(status))
      {
        decisionConfigVO.setStatus(decisionConfigVO.getStatus().PUBLISH);
      }else
      {
        decisionConfigVO.setStatus(decisionConfigVO.getStatus().ARCHIVE);
      }
      decisionConfigVO.setTypeCode(resultSet.getString("decision_type_code"));
      
      
      
    }
    return decisionConfigVO;
  }
  
  public DecisionConfigVO saveDecisionTypeConfig(DecisionConfigVO decisionConfigVO, Connection connection) throws Exception
  {
    Map inputs = new HashMap();
    
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_SAVE_DECISION_TYPE_CONFIG);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    return null;    
  }
  
  public DecisionConfigVO createDraftConfiguration(String decisionTypeCode, Connection connection) throws Exception
  {
    Map inputs = new HashMap();
    
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_CREATE_DRAFT_CONFIGURATION);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    return null;    
  }
  
  public void deleteValueNode(long valueNodeId, long decisionConfigId, Connection connection) throws Exception
  {
    Map inputs = new HashMap();
    inputs.put("IN_VALUE_ID", valueNodeId);
    inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
        
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_DELETE_VALUE_NODE);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
  }
  
  
  
  
  public ValueNodeVO getValueNode(long validNodeId, long decisionConfigId, Connection connection) throws Exception
  {
    Map inputs = new HashMap();
    inputs.put("", validNodeId);
    inputs.put("", decisionConfigId);
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_VALUE_NODE);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    return null;    
  }
  
  
  public List<ValueNodeVO> getValueNodesByStatusForDecisionType(String decisionTypeCode, String statusCode, Connection connection) throws Exception
  {
    List<ValueNodeVO> valueNodes = new ArrayList<ValueNodeVO>();
    Map inputs = new HashMap();
    inputs.put("IN_DECISION_TYPE_CODE", decisionTypeCode);
    inputs.put("IN_STATUS_CODE", statusCode);
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(connection);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_VALUE_NODES_BY_STATUS);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    CachedResultSet resultSet = (CachedResultSet)results.get("OUT_CURSOR");
    while(resultSet != null && resultSet.next()){
      ValueNodeVO valueNodeVO = new ValueNodeVO();
      
      valueNodeVO.setValueId(resultSet.getLong("value_id"));
      valueNodeVO.setDecisionConfigId(resultSet.getLong("decision_config_id"));
      valueNodeVO.setName(resultSet.getString("name"));
      if(resultSet.getObject("parent_value_id") == null)
        valueNodeVO.setParentNodeId(-1);
      else
        valueNodeVO.setParentNodeId(resultSet.getLong("parent_value_id"));
      valueNodeVO.setScriptText(resultSet.getString("script_text"));
      valueNodeVO.setGroupOrder(resultSet.getInt("group_order"));
      if(resultSet.getString("is_active_flag").equalsIgnoreCase("Y"))
        valueNodeVO.setIsActive(true);
      else
        valueNodeVO.setIsActive(false);
      if(resultSet.getString("allow_comments_flag").equalsIgnoreCase("Y"))
        valueNodeVO.setAllowComments(true);
      else
        valueNodeVO.setAllowComments(false);
      valueNodeVO.setUpdateBy(resultSet.getString("updated_by"));
      valueNodeVO.setUpdatedOn(resultSet.getDate("updated_on"));
      valueNodeVO.setLevel(resultSet.getInt("level"));
      valueNodes.add(valueNodeVO);
    }
    return valueNodes;    
  }
  
  
  public List<RuleTypeVO> getValueRulesForConfig(long valueId, long decisionConfigId, Connection conn) throws Exception
  {
    List<RuleTypeVO> valueRules = new ArrayList<RuleTypeVO>();
    Map inputs = new HashMap();
    
    inputs.put("IN_VALUE_ID", valueId);
    inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(conn);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_RULES_BY_VALUE);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map ruleResults = (Map)daUtil.execute(request);
    String status = (String) ruleResults.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) ruleResults.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    CachedResultSet ruleResultSet = (CachedResultSet)ruleResults.get("OUT_CURSOR");
    while(ruleResultSet != null && ruleResultSet.next()){
      RuleTypeVO ruleTypeVO = new RuleTypeVO();
      ruleTypeVO.setRuleCode(ruleResultSet.getString("rule_code"));
      ruleTypeVO.setName(ruleResultSet.getString("name"));
      if(ruleResultSet.getString("next_level_flag") != null && ruleResultSet.getString("next_level_flag").equalsIgnoreCase("Y")){
        ruleTypeVO.setNextLevelFlag(true);
      }else
      {
        ruleTypeVO.setNextLevelFlag(false);
      }
      ruleTypeVO.setDisplayOrder(ruleResultSet.getInt("display_order"));
      ruleTypeVO.setIsActiveFlag(true);
     
      valueRules.add(ruleTypeVO);
    }
    return valueRules;
  }
  
  public List<ValueNodeVO> getHierarchyNodesForConfig(long valueId, long decisionConfigId, Connection conn) throws Exception
  {
    List<ValueNodeVO> hierarchyNodes = new ArrayList<ValueNodeVO>();
    Map inputs = new HashMap();
    
    inputs.put("IN_VALUE_ID", valueId);
    inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(conn);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_HIERARCHY_BY_VALUE_ID);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map ruleResults = (Map)daUtil.execute(request);
    String status = (String) ruleResults.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) ruleResults.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    CachedResultSet resultSet = (CachedResultSet)ruleResults.get("OUT_CURSOR");
    while(resultSet != null && resultSet.next()){
      ValueNodeVO valueNodeVO = new ValueNodeVO();
      
      valueNodeVO.setValueId(resultSet.getLong("value_id"));
      valueNodeVO.setName(resultSet.getString("name"));
      if(resultSet.getObject("parent_value_id") == null)
        valueNodeVO.setParentNodeId(0);
      else
        valueNodeVO.setParentNodeId(resultSet.getLong("parent_value_id"));
      
      valueNodeVO.setGroupOrder(resultSet.getInt("group_order"));
//      if(resultSet.getString("is_active_flag").equalsIgnoreCase("Y"))
//        valueNodeVO.setIsActive(true);
//      else
//        valueNodeVO.setIsActive(false);
      valueNodeVO.setLevel(resultSet.getInt("level"));
     
      hierarchyNodes.add(valueNodeVO);
    }
    return hierarchyNodes;
  }
  
  public long saveValueNode(ValueNodeVO valueNodeVO, Connection conn) throws Exception
  {
    long outValueId = 0;
    Map inputs = new HashMap();
    inputs.put("IN_VALUE_ID", valueNodeVO.getValueId());
    inputs.put("IN_DECISION_CONFIG_ID", valueNodeVO.getDecisionConfigId());
    inputs.put("IN_NAME", valueNodeVO.getName());
    if(valueNodeVO.isIsActive())
      inputs.put("IN_IS_ACTIVE_FLAG", "Y");
    else
      inputs.put("IN_IS_ACTIVE_FLAG", "N");
    
    inputs.put("IN_PARENT_VALUE_ID", valueNodeVO.getParentNodeId());
      
    inputs.put("IN_SCRIPT_TEXT", valueNodeVO.getScriptText());
    if(valueNodeVO.isAllowComments())
      inputs.put("IN_ALLOW_COMMENTS_FLAG", "Y");
    else
      inputs.put("IN_ALLOW_COMMENTS_FLAG", "N");
    if(valueNodeVO.getGroupOrder() > 0){  
      inputs.put("IN_GROUP_NUMBER", valueNodeVO.getGroupOrder());
    }
      
    inputs.put("IN_UPDATED_BY", valueNodeVO.getUpdateBy());
    inputs.put("IN_CHILDNODE_IDS", valueNodeVO.getChildValueIds());
    inputs.put("IN_VALUERULE_IDS", valueNodeVO.getValueRuleIds());
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(conn);
    request.reset();
    request.setInputParams(inputs);
    
    if(valueNodeVO.getValueId() > 0){  
      request.setStatementID(DecisionFrameworkConstants.DF_UPDATE_VALUE_NODE);
    }else
    {
      request.setStatementID(DecisionFrameworkConstants.DF_SAVE_VALUE_NODE);
    }
    
    Iterator itr = inputs.keySet().iterator();
    String str = "";
    while(itr.hasNext())
    {
      str = (String)itr.next();
      logger.debug(str+ "===>>> "+inputs.get(str));
    }
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equals("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message);                    
    }else
    {
      if(results.containsKey("OUT_VALUE_ID"))
        outValueId = Long.valueOf(String.valueOf(results.get("OUT_VALUE_ID")));  
            
    }
    return outValueId;    
  }
  
  public List<RuleTypeVO> getRulesForDecisionType(String decisionTypeCode, Connection conn) throws Exception
  {
    Map inputs = new HashMap();
    List<RuleTypeVO> ruleTypes = new ArrayList<RuleTypeVO>();
    inputs.put("IN_DECISION_TYPE", decisionTypeCode);
    
    
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    request.setConnection(conn);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_GET_RULE_TYPES_FOR_DECISION_TYPE);
    Map ruleResults = (Map)daUtil.execute(request);
    String status = (String) ruleResults.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) ruleResults.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    CachedResultSet ruleResultSet = (CachedResultSet)ruleResults.get("OUT_CURSOR");
    while(ruleResultSet != null && ruleResultSet.next()){
      RuleTypeVO ruleTypeVO = new RuleTypeVO();
      ruleTypeVO.setRuleCode(ruleResultSet.getString("rule_code"));
      ruleTypeVO.setName(ruleResultSet.getString("name"));
      if(ruleResultSet.getString("next_level_flag") != null && ruleResultSet.getString("next_level_flag").equalsIgnoreCase("Y")){
        ruleTypeVO.setNextLevelFlag(true);
      }else
      {
        ruleTypeVO.setNextLevelFlag(false);
      }
      ruleTypeVO.setDisplayOrder(ruleResultSet.getInt("display_order"));
      ruleTypeVO.setIsActiveFlag(true);
     
      ruleTypes.add(ruleTypeVO);
    }
    return ruleTypes;
  }
  
  public void publishDecisionType(String decisionTypeCode, long decisionConfigId, String userId, Connection conn) throws Exception
  {
    Map inputs = new HashMap();
    inputs.put("IN_DECISION_TYPE_CODE", decisionTypeCode);
    inputs.put("IN_DECISION_CONFIG_ID", decisionConfigId);
    inputs.put("IN_UPDATED_BY", userId);
        
    //Craete a data request and set the connection, statement Id, input parama 
    DataRequest request = new DataRequest();
    request.setConnection(conn);
    request.reset();
    request.setInputParams(inputs);
    request.setStatementID(DecisionFrameworkConstants.DF_PUBLISH_DECISION_BY_TYPE);
    
    //Get the dataAccessUtil instance
    DataAccessUtil daUtil = DataAccessUtil.getInstance();
    Map results = (Map)daUtil.execute(request);
    String status = (String) results.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) results.get("OUT_MESSAGE");
      throw new Exception(message); 
    }
    
  }
  
}
