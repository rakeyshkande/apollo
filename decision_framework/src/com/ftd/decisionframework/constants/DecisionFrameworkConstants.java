package com.ftd.decisionframework.constants;

public abstract class DecisionFrameworkConstants
{
 
 //Constants for stored procs
 
 public static final String DF_GET_DECISION_TYPE = "DF_GET_DECISION_TYPE";
 
 public static final String DF_GET_RULE_TYPES_FOR_DECISION_TYPE = "DF_GET_RULE_TYPES_FOR_DECISION_TYPE";
 
 public static final String DF_GET_DECISION_TYPE_CONFIG_BY_STATUS = "DF_GET_DECISION_TYPE_CONFIG_BY_STATUS";
 
 public static final String DF_GET_DECISION_TYPE_CONFIG_BY_ID = "DF_GET_DECISION_TYPE_CONFIG_BY_ID";
 
 public static final String DF_SAVE_DECISION_TYPE_CONFIG = "DF_SAVE_DECISION_TYPE_CONFIG";
 
 public static final String DF_CREATE_DRAFT_CONFIGURATION = "DF_CREATE_DRAFT_CONFIGURATION";
 
 public static final String DF_SAVE_VALUE_NODE = "DF_SAVE_VALUE_NODE";
 
 public static final String DF_UPDATE_VALUE_NODE = "DF_UPDATE_VALUE_NODE";
 
 public static final String DF_DELETE_VALUE_NODE = "DF_DELETE_VALUE_NODE";
 
 public static final String DF_DELETE_ALL_RULES_BY_VALUE = "DF_DELETE_ALL_RULES_BY_VALUE";
 
 public static final String DF_GET_VALUE_NODE = "DF_GET_VALUE_NODE";
 
 public static final String DF_GET_RULES_BY_VALUE = "DF_GET_RULES_BY_VALUE";
 
 public static final String DF_GET_VALUE_NODES_FOR_CONFIG = "DF_GET_VALUE_NODES_FOR_CONFIG";
 
 public static final String DF_GET_VALUE_NODES_BY_STATUS = "DF_GET_VALUE_NODES_BY_STATUS";
 
 public static final String DF_GET_HIERARCHY_BY_VALUE_ID = "DF_GET_HIERARCHY_BY_VALUE_ID";
 
 public static final String DF_PUBLISH_DECISION_BY_TYPE = "DF_PUBLISH_DECISION_BY_TYPE";
 
 
 public static final String CONFIG_FILE = "decisionframework_config.xml";
 
 
 //Session variables
 
 public static final String SESSION_ALL_NODES           = "SESSION_ALL_NODES";
 public static final String SESSION_ALL_RULES           = "SESSION_ALL_RULES";
 public static final String SESSION_DECISION_CONFIG_ID  = "SESSION_DECISION_CONFIG_ID";
 
 //request param constants
 
    public static final String NODE_ID = "nodeId";
    public static final String NODE_NAME = "nodeName";
    public static final String PARENT_VALUE_ID = "parentValueId";
    public static final String NODE_STATUS = "nodeStatus";
    public static final String SCRIPTING_TEXT = "scripting";
    public static final String ALLOW_COMMENTS = "allowComments";
    public static final String VALUE_RULES = "valueRules";
    public static final String CHILD_NODES = "childNodes";
    public static final String DECISION_CONFIG_ID = "decisionConfigId";
    public static final String DECISION_TYPE_CODE = "decisionTypeCode";
    
    
    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT                                      = "context";
    public static final String SEC_TOKEN                                    = "securitytoken";
    public static final String SECURITY_IS_ON                               = "SECURITY_IS_ON";
    public static final String ADMIN_ACTION                                 = "adminAction";
    public static final String CONS_MAIN_MENU_URL                           = "security.main.menu";
    public static final String BASE_CONFIG                                  = "BASE_CONFIG";
    public static final String BASE_URL                                     = "BASE_URL";
 
  
  
 /*********************************************************************************************
    //config flie locations
    *********************************************************************************************/
    public static final String PROPERTY_FILE                      = "decisionframework_config.xml";
    public static final String SECURITY_FILE                      = "security-config.xml";
    public static final String DATASOURCE_NAME                    = "application.db.datasource";
    public static final String DECISIONFRAMEWORK_CONFIG_CONTEXT   = "decisionframework_config";
    public static final String FTD_APPS_CONTEXT                   = "FTDAPPS_PARMS";
    
     
}
