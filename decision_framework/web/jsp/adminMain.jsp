<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<jsp:include page="/includes/security.jsp" flush="true" />
<%
  String securitytoken = request.getParameter("securitytoken");
  if( securitytoken==null ) {
    securitytoken="null";
  }
  String applicationcontext = request.getParameter("applicationcontext");
  if( applicationcontext==null ) {
    applicationcontext="null";
  }
  String context = request.getParameter("context");
  if( context==null ) {
    context="null";
  }
%>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
   <title><%=request.getAttribute("name")%> : Maintenance Screen</title>
     <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
     <link rel="StyleSheet" type="text/css" href="css/dtree.css" />
	 <link rel="stylesheet" type="text/css" href="css/adminMain.css"/>
	<link rel="stylesheet" type="text/css" href="css/page.css"/>
      <script type="text/javascript" src="js/commonUtil.js"></script>
      <script type="text/javascript" src="js/util.js"></script>
      <script type="text/javascript" src="js/prototype.js"></script>     
      <script type="text/javascript" src="js/rico.js"></script>        
      <script type="text/javascript" src="js/ftdajax.js"></script>         
      <script type="text/javascript" src="js/clock.js"></script>
      <script type="text/javascript" src="js/dtree.js"></script>    
      <script type="text/javascript" src="js/admin/adminMain.js"></script>
      <script type="text/javascript" src="js/ModalPopups.js" ></script>			
	  
  </head>
  <body id="body" onload="javascript:init();" >
	<form method="POST">
    <jsp:include page="/includes/header.jsp"/>
    <div id="content">
     <script type="text/javascript">
      <!--
		var securitytoken = "<%=securitytoken%>";
		var applicationcontext = "<%=applicationcontext%>";
		var context = "<%=context%>";
		var v_decisionTypeCode = '<%=request.getAttribute("typeCode")%>';
		var v_decisionConfigId = '<%=request.getAttribute("decisionConfigId")%>';
       
       //-->
    </script>
      <table id="contentTable" width="90%" height="75%" >
        <tr>
          <td  width="45%" class="mainlefttd">
            <div class="PanelHeader"><%=request.getAttribute("name")%> Hierarchy</div>
            <div id="treePanel" class="ScrollPane" >
            
            </div>
          </td>
         
          <td width="55%" class="mainrighttd">
          
			
			<div id="valueHeader" class="PanelHeader"><%=request.getAttribute("name")%> Value</div>
			  
			  <div id="valuePanel">
			  
				<div id="previewDiv" style="display: block">
				<table width="95%" border="0" >
					<!--<tr>
						<td>&nbsp;</td>
					</tr>-->
					<tr>
						<td align="center">
							<input id = "previewBtn" type="button" value="Preview Draft" class="BlueButton overlay" onclick="previewDraft()" style="width:120px; font-weight:bold;">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input id = "previewPublishBtn" type="button" value="Preview Published" class="BlueButton overlay" onclick="previewPublish()" style="width:120px; font-weight:bold;">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input id = "publishBtn" type="button" value="Publish" class="BlueButton overlay" onclick="publish()" style="width:120px; font-weight:bold;">
						</td>
					</tr>
				</table>
			</div>
			
				<div id="rootTd" style="display:none;"><h5 style="background-color:#D1BC63; color:#000000; font-size: 12px;">Root Hierarchy Information</h5></div>
				<div id="nonRootTd" style="display:none;"><h5 style="background-color:#D1BC63; color:#000000; font-size: 12px;">Level <label id="sec1"></label> Value Information</h5></div>
				<div id="nodeInfo">
					<input type="hidden" id="nodeId" name="nodeId"/>
					<input type="hidden" id="parentValueId" name="parentValueId"/>
					<input type="hidden" id="valueRules" name="valueRules"/>
					<input type="hidden" id="childNodes" name="childNodes"/>
					<input type="hidden" id="decisionConfigId" name="decisionConfigId"/>
					<input type="hidden" id="decisionTypeCode" name="decisionTypeCode" value="<%=request.getAttribute("typeCode")%>" />
					<table >
						<tr >
							
							<td  class="Label">Name : </td>
							<td><input type="text" id="nodeName" name="nodeName" onchange="activateParam()" maxlength="30" size ="30"/></td>
						</tr>
						<tr>
							<td class="Label">Status : </td>
							<td>
								<select name="nodeStatus" id="nodeStatus" onchange="activateParam()">
									<options>
										<option value="Y">Active</option>
										<option value="N">InActive</option>
									</options>
								</select>
							</td>
						</tr>
					</table>
					
							
					
				</div>
				<h5 style="background-color:#D1BC63; color:#000000;">Hierarchy Assignment</h5>
				<div id="hierarchyInfo" class="Label">
					<table id="hierarchyTbl" border="0"> 
						<tr>
							<td></td>
							<td></td>
						</tr>
					
					</table>
				
				</div>
				<div>
					<h5 style="background-color:#D1BC63; color:#000000; font-size: 12px;">Business Rules</h5>
				</div>
				<div id="rulesInfo" class="Label">
				</div>
				<h5 style="background-color:#D1BC63; color:#000000; font-size: 12px;">Level <label id="nxt"></label> Information</h5>
				<div id="nextLevelInfo">
					<table border="0"> 
						<tr>
						<td class="Label">Scripting : </td>
						<td><textarea  rows='3' cols='35' id="scripting" name="scripting" onpaste="return checkMaxLength(this, 75);" onchange="activateParam()" 
						onkeypress="return imposeMaxLength(event, this, 75);"></textarea></td>
						</tr>
						<tr>
						<td class="Label" align="left"><input type="checkbox" id="allowComments" onchange="activateParam()">Allow Comments</input></td>
						<td >&nbsp;</td>
						</tr>
						
					</table>
					<div id="nextLevelRules" class="Label">
					
					</div>
					<div id='childItemsTbl' style="display: block">
						<table width="50%">
							<tr>
								<td width="100px" style= "vertical-align:top" class="Label">Attached Values :</td>
								

								<td><div id="childItems">
									<table id='childTbl' width="50%" class="ChildTable" >
										<tr></tr>
									</table>
								
								</div></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><input id = "addBtn" type="button" value="Add" onclick="checkAddValue()" class="BlueButton overlay" style="width:50px; font-weight:bold;"></td>
							</tr>
						</table>
					</div>
					
				</div>
					</br>
					<table width="25%" align="center">
						<tr>
							<td width="33%" align="center" id="saveTd"><input type="button" id = "saveBtn" value="Save" onclick="saveValueNode()" class="BlueButton overlay" style="width:50px; font-weight:bold;"></td>
							<td width="33%" align="center" id="deleteTd"><input id = "deleteBtn" type="button" value="Delete" class="BlueButton overlay" onclick="deleteValueNode()" style="width:50px; font-weight:bold;"></td>
							<td width="33%" align="center" id="cancelTd"><input id = "cancelBtn" type="button" value="Cancel" class="BlueButton overlay" onclick="cancelValueNode()" style="width:50px; font-weight:bold;"></td>
							
						</tr>
					</table>
					
			  </div>
          </td>
		 
        </tr>
      </table>
    </div>
    <jsp:include page="/includes/footer.jsp"/>  
	</form>
  </body>
</html>