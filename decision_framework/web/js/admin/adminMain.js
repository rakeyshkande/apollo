/*
 *  
 */

var treePanel_Dtree = new dTree('treePanel_Dtree');
var v_decisionTypeCode; // Initialized by Page on Load prior to Init call
var v_heirarchyData; // Loaded via AJAX
var v_unsavedChangesFlag = false;
var v_currentNode;
var v_newNode;
var treeHash;
var rulesArray;
var rootNode;
var valueIdArray;
var yesnocancelSel = false;
var cancelSelected = false;
var heirarchyChanged = false;
var maxLevel = 0;
var goToNode;
var selectFlag = true;
//
var backButtonFlag = false;
//
/*
 * Initializes the admin screen
 */
function init()
{
  setNavigationHandlers();
  addDefaultListeners();
  addAdminTree();
}

function activateParam(){
  v_unsavedChangesFlag = true;
}

function addAdminTree() 
{ 
  
  refreshHeirarchy();
  activateTree();
}

function activateTree() {
  
  treePanel_Dtree.config.selectValidator = validateTreeSelection;
}

function updateTree(newTree)
{
    var oldTree = treePanel_Dtree;
    treePanel_Dtree = newTree;
    treePanel_Dtree.copyNodeStates(oldTree);
      
    var treePanelDiv = document.getElementById('treePanel');        
    treePanelDiv.innerHTML = treePanel_Dtree.toString();
    
    if(v_newNode == null){
      processSelection(rootNode.id);
      treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(rootNode.id));
      treePanel_Dtree.closeAll();
    }else{
      processSelection(v_newNode.id);
      treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(v_newNode.id));
      if(!treePanel_Dtree.isOpen(v_newNode.id))
        treePanel_Dtree.o(treePanel_Dtree.getNodeIndexById(v_newNode.id));
      
    }
}

function validateTreeSelection(currentNode, newNode) 
{
  
  v_currentNode = currentNode;
  v_newNode = newNode;
  
  if(v_unsavedChangesFlag && !yesnocancelSel && !cancelSelected){ 
   showYesNoCancel();
   return false;
  }else{ 
    processSelection(newNode.id);  
    return true;
  }

  
} 



function processSelection(valueId)
{
  if(valueId != null && valueId != "" && valueId != undefined){
   
    var hierarchyInfoDiv = document.getElementById('hierarchyInfo');
    var tblElement = document.getElementById('hierarchyTbl');
    var rulesInfoDiv = document.getElementById('rulesInfo');
    var nextLevelDiv = document.getElementById('nextLevelInfo');
    var addButton = document.getElementById('addBtn');
    
    //mode = "save"; 
    v_unsavedChangesFlag = false;
    heirarchyChanged = false;
    
    if(treeHash == null || treeHash.keys().size() == 0){
      getGlobalData();
    }
    if(rootNode.id == valueId){
      rulesInfoDiv.style.display = "none";
      hierarchyInfoDiv.style.display = "none";
      document.getElementById('previewDiv').style.display = "block";
    }else{
      rulesInfoDiv.style.display = "block";
      hierarchyInfoDiv.style.display = "block";
      document.getElementById('previewDiv').style.display = "none";
    }
         
     document.getElementById('nodeName').value=treeHash.get(valueId).name;
     document.getElementById('nodeId').value=treeHash.get(valueId).valueId;
     document.getElementById('parentValueId').value=treeHash.get(valueId).parentNodeId;
    
    
    if(treeHash.get(valueId).isActive){
      document.getElementById('nodeStatus').options[0].selected=true;
    }else{
      document.getElementById('nodeStatus').options[1].selected=true;
    }
    
    
    var level =   treeHash.get(valueId).level
    
    valueIdArray = treeHash.keys();
    if(level == 1){
      document.getElementById('rootTd').style.display = 'block';  
      document.getElementById('nonRootTd').style.display = 'none';
    }else{
      document.getElementById('nonRootTd').style.display = 'block';  
      document.getElementById('rootTd').style.display = 'none';
    }
    replaceText(document.getElementById('sec1'), level+' ');
    replaceText(document.getElementById('nxt'),level+1+' ');
    //delete Hierarchy assignment section
    delRows(tblElement);
    //delete rules section
    deleteRules(rulesInfoDiv);
    
    
        
      var newReqHierarchy = new FTD_AJAX.Request('valueNodes.do?valueNodeId='+valueId+'&method=hierarchyNodes', FTD_AJAX_REQUEST_TYPE_POST, processHierarchyNodes, true, false);
      newReqHierarchy.addParam("decisionConfigId",document.getElementById('decisionConfigId').value);
      newReqHierarchy.addParam("securitytoken",document.getElementById("securitytoken").value);
      newReqHierarchy.addParam("context",document.getElementById("context").value);
      newReqHierarchy.addParam("adminAction",document.getElementById("adminAction").value);
      newReqHierarchy.send();
    
     
      
      if(rootNode.id != valueId){
      addRules(rulesInfoDiv);
      }
    
      generateNextLevel(valueId);
    
      var newReqRules = new FTD_AJAX.Request('valueNodes.do?valueNodeId='+valueId+'&method=valueRules', FTD_AJAX_REQUEST_TYPE_POST, processValueRules, true, false);
      newReqRules.addParam("decisionConfigId",document.getElementById('decisionConfigId').value);
      newReqRules.addParam("securitytoken",document.getElementById("securitytoken").value);
      newReqRules.addParam("context",document.getElementById("context").value);
      newReqRules.addParam("adminAction",document.getElementById("adminAction").value);
      newReqRules.send();
    
      if(!treeHash.get(valueId).isActive){
        //hierarchyInfoDiv.disabled = true; //commented by jbaba
        toggleChildEnableDisable(hierarchyInfoDiv, true); //line added by jbaba
        toggleChildEnableDisable(rulesInfoDiv, true);//rulesInfoDiv.disabled = true;
        toggleChildEnableDisable(nextLevelDiv, true);//nextLevelDiv.disabled = true;
        toggleChildEnableDisable(addButton, true);//addButton.disabled = true;        
      }else{
        toggleChildEnableDisable(hierarchyInfoDiv, false);//hierarchyInfoDiv.disabled = false;
        toggleChildEnableDisable(rulesInfoDiv, false);//rulesInfoDiv.disabled = false;
        toggleChildEnableDisable(nextLevelDiv, false);//nextLevelDiv.disabled = false;
        toggleChildEnableDisable(addButton, false);//addButton.disabled = false;
      }
    /*var newRequest = new FTD_AJAX.Request('getValue.do?valueNodeId =' + valueId, FTD_AJAX_REQUEST_TYPE_POST, showValueCallback, true, false);
    newRequest.send();*/
  }
  
}
  
  function processValueRules(data){
    var allInputs = document.getElementsByTagName('input');
    //var rules = eval("(" + data.responseText + ")");
    var rules = eval(data.responseText);
    if(rules == null || rules == undefined){
      window.location = "http://" + window.location.host + "/secadmin";
      return;
    }
    if(rules.length > 0){
      for(var a = 0; a < rules.length; a++){
        var nm = rules[a].ruleCode;
        for (var i=0; i<allInputs.length; i++) {
          if(allInputs[i].type == 'checkbox'){
            if(allInputs[i].value == rules[a].ruleCode){
              allInputs[i].checked = true;
            }
          }
        }
      }
    }
  
  }
  
  function processHierarchyNodes(data){
   
    var hierarchyDiv = document.getElementById("hierarchyInfo");
    var hierarchyNodes = eval(data.responseText);
    var levelSelects = hierarchyDiv.getElementsByTagName("select");
    var hierarchyHash = new Hash();
    var parentId = -1;
    if(hierarchyNodes == null || hierarchyNodes == undefined){
      window.location = "http://" + window.location.host + "/secadmin";
      return;
    }
    for(var x = 0; x < hierarchyNodes.length; x++){
      hierarchyHash.set(x, hierarchyNodes[x]);
    }
    //alert(levelSelects.length);
    var selectedNodeId;
    if(v_newNode == null)
      selectedNodeId = rootNode.id;
    else
      selectedNodeId = v_newNode.id;
      
    var selectedLevel = treeHash.get(selectedNodeId).level;
    for (var i=0; i<hierarchyNodes.length; i++) {
      var combo_box = document.createElement('select');      
      combo_box.name = 'level'+i;
      combo_box.id = 'level'+i;
      combo_box.setAttribute("class", "selBox");
      combo_box.style.width = "180px";
      var choice = document.createElement('option');
      if(parentId != -1){
        choice.value = "None";
        choice.text = "None";
        combo_box.options.add(choice);
        
      }
      
      for (var index = 0; index < valueIdArray.length; ++index) {
        choice = document.createElement('option');
        if(treeHash.get(valueIdArray[index]).level - 1 == i && treeHash.get(valueIdArray[index]).parentNodeId == parentId){
          choice.value = treeHash.get(valueIdArray[index]).valueId;
          choice.text = treeHash.get(valueIdArray[index]).name;
          combo_box.options.add(choice);
          
        }
      }
      for(var j = 0; j < combo_box.options.length; j++){
        if(combo_box.options[j].value ==  hierarchyHash.get(i).valueId) 
          //&& combo_box.options[j].value != selectedNodeId)
        {
          combo_box.options[j].selected='selected';
          parentId = hierarchyHash.get(i).valueId;
        }
      }
      if(i > 0){
        insRow('Level'+i, combo_box); 
      }else{
        insRow('Root Hierarchy', combo_box);
      }
    }
     addHierarchyHandlers();
     
  
  }
  
  function delRows(tblElement){
           
    for(var i = tblElement.rows.length; i > 0; i--)
    {
      tblElement.deleteRow(i -1);
    }
  }
  
  function insRow(label, comboBox)
  {
    //alert('document.getElementById('hierarchyTbl').rows.length'+document.getElementById('hierarchyTbl').rows.length);
    var i = document.getElementById('hierarchyTbl').rows.length;
    var x=document.getElementById('hierarchyTbl').insertRow(i);
    var y=x.insertCell(0);
    var z=x.insertCell(1);
    y.innerHTML=label;
    z.appendChild(comboBox);
  }

  function generateNextLevel(nodeId){
    var nextLevelDiv = document.getElementById('nextLevelInfo');
    var childTblDiv = document.getElementById('childItemsTbl');
    var deleteButton = document.getElementById('deleteTd');
    var cancelButton = document.getElementById('cancelTd');
    var addButton = document.getElementById('addBtn');
    if(nodeId != null){
      if(treeHash.get(nodeId).scriptText != null && treeHash.get(nodeId).scriptText != undefined){
        document.getElementById('scripting').value = treeHash.get(nodeId).scriptText;
      }else{
        document.getElementById('scripting').value = "";
      }
      if(treeHash.get(nodeId).allowComments){
        document.getElementById('allowComments').checked = true;
      }else{
        document.getElementById('allowComments').checked = false;
      }
    }else{
      document.getElementById('allowComments').checked = false;
      document.getElementById('scripting').value = "";
    }
    clearDivTag(document.getElementById('nextLevelRules'));
    addNextLevelRules();
    if(nodeId != null){
      addButton.disabled = false;
      deleteButton.style.display = "block";
      //cancelButton.style.display = "block"; 
      addChildItems(nodeId); 
    }else{
      delRows(document.getElementById('childTbl'));
      addButton.disabled = true;
      deleteButton.style.display = "none";
      //cancelButton.style.display = "none";
    }
  }
  
  function addScripText(nextLevelDiv, script){
    var txtNode = document.createTextNode('Scripting :    ');
    nextLevelDiv.appendChild(txtNode);
    var textArea = document.createElement('input');
    textArea.type = "textarea";
    textArea.name = "script";
    textArea.value = script;
    nextLevelDiv.appendChild(textArea);
  }
  
  function addChildItems(valueId){
   
    var childTbl = document.getElementById('childTbl');
    var childHash = new Hash();
    delRows(childTbl);
    
    var cnt = 0;
    for (var  index = 0; index < valueIdArray.length; ++index) {
     
      if(treeHash.get(valueIdArray[index]).parentNodeId  == valueId){
        childHash.set(treeHash.get(valueIdArray[index]).valueId, treeHash.get(valueIdArray[index]).name);
      }
    }
    var childKeys = childHash.keys();
    for(var i = 0; i < childKeys.length; i++){
      
      var hlink = document.createElement('a');
      hlink.setAttribute('name', childKeys[i]);
      hlink.setAttribute('id', childKeys[i]);
      if(!(treeHash.get(childKeys[i]).isActive)){
          hlink.style.cssText = 'font-style: italic; color:#666666; text-decoration:none;'; //added by jbaba
      }
      hlink.href="javascript:processChildSelection("+childKeys[i]+")";
      var txtNode = document.createTextNode(childHash.get(childKeys[i]));
      hlink.appendChild(txtNode);
      //childDiv.appendChild(hlink);
      //childDiv.appendChild(document.createElement('br'));
      var i = document.getElementById('childTbl').rows.length;
      var x=childTbl.insertRow(i);
      
      var y=x.insertCell(0);
      y.width = "1px";
      var z=x.insertCell(1);
      z.width = "100px";
      if(childKeys.length > 1){
        
        //var upTxtNode = document.createTextNode("^");
        if(i == 0){
          var downLink = document.createElement('a');
          downLink.setAttribute('name', 'down'+i);
          downLink.setAttribute('id', 'down'+i);
          downLink.href="javascript:moveDown("+i+");";
          //var downTxtNode = document.createTextNode("v");
          var downImg = document.createElement("img");
          downImg.src = "images/arrow_18.gif";
          downImg.style.cssText = 'border:1px solid #DECF98;';  //added by jbaba
          downLink.appendChild(downImg);
          y.appendChild(downLink);
          
        }else if(i == childKeys.length - 1){
          var upLink = document.createElement('a');
          upLink.setAttribute('name', 'up'+i);
          upLink.setAttribute('id', 'up'+i);
          upLink.href="javascript:moveUp("+i+");";
          var upImg = document.createElement("img");
          upImg.src = "images/arrow_17.gif";
          upImg.style.cssText = 'border:1px solid #DECF98;';
          upLink.appendChild(upImg);
          upLink.appendChild(document.createElement('br'));
          y.appendChild(upLink);
        }else{
          var upLink = document.createElement('a');
          upLink.setAttribute('name', 'up'+i);
          upLink.setAttribute('id', 'up'+i);
          upLink.href="javascript:moveUp("+i+");";
          var upImg = document.createElement("img");
          upImg.style.cssText = 'border:1px solid #DECF98;';
          upImg.src = "images/arrow_17.gif";
          upLink.appendChild(upImg);
          upLink.appendChild(document.createElement('br'));
          var downLink = document.createElement('a');
          downLink.setAttribute('name', 'down'+i);
          downLink.setAttribute('id', 'down'+i);
          downLink.href="javascript:moveDown("+i+");";
          //var downTxtNode = document.createTextNode("v");
          var downImg = document.createElement("img");
          downImg.style.cssText = 'border:1px solid #DECF98;';
          downImg.src = "images/arrow_18.gif";
          downLink.appendChild(downImg);
          upLink.appendChild(downLink);
          y.appendChild(upLink);
        }
        
        
      }else{
        y.innerHTML = ' ';
      }
      z.appendChild(hlink);
      
   } 
        
    
  
  }
  
  function addRules(rulesInfoDiv){

    for(var i = 0; i < rulesArray.size(); i++){
      if(!rulesArray[i].nextLevelFlag){
        var cb = document.createElement('input');
        cb.type = "checkbox";
        cb.id = rulesArray[i].ruleCode;
        cb.value = rulesArray[i].ruleCode;
        cb.checked = false;
        var txtNode = document.createTextNode(rulesArray[i].name);
        //alert(rulesArray[i].name);
        rulesInfoDiv.appendChild( cb );
        rulesInfoDiv.appendChild(txtNode);
        rulesInfoDiv.appendChild(document.createElement('br'));
        
      }
    
    }
    addRuleHandlers();
  }
  
  function addNextLevelRules(){

    var nextLevelRulesDiv = document.getElementById('nextLevelRules');
    
    for(var i = 0; i < rulesArray.size(); i++){
      
      if(rulesArray[i].nextLevelFlag){
        var cb = document.createElement('input');
        cb.type = "checkbox";
        cb.id = rulesArray[i].ruleCode;
        cb.value = rulesArray[i].ruleCode;
        cb.checked = false;
        cb.onchange = rulesChanged;
        var txtNode = document.createTextNode(rulesArray[i].name);
        //alert(rulesArray[i].name);
        nextLevelRulesDiv.appendChild( cb );
        nextLevelRulesDiv.appendChild(txtNode);
        
      }
    
    }
    
  }
  
  function deleteRules(rulesInfoDiv){
    
    for (var i=rulesInfoDiv.childNodes.length; i>0; i--) {
      
        var childNode = rulesInfoDiv.childNodes[i-1];
        rulesInfoDiv.removeChild(childNode);
      
    }
    
  }
  
  function clearDivTag(divElement){
    for (var i=divElement.childNodes.length; i>0; i--) {
      
        var childNode = divElement.childNodes[i-1];
        divElement.removeChild(childNode);
      
    }
  }
function refreshHeirarchy()
{
    //XMLHttpRequest
    var newRequest = new FTD_AJAX.Request('getHierarchy.do?decisionType=' + v_decisionTypeCode, FTD_AJAX_REQUEST_TYPE_POST, refreshHeirarchyJsonCallback, true, false);
    newRequest.addParam("securitytoken",document.getElementById("securitytoken").value);
    newRequest.addParam("context",document.getElementById("context").value);
    newRequest.addParam("adminAction",document.getElementById("adminAction").value);
    newRequest.send();
}



function refreshHeirarchyJsonCallback(data)
{
  var newTree = new dTree('treePanel_Dtree');
  var nodeStatus = false;
  treeHash = new Hash();
  v_heirarchyData =data.responseText.evalJSON(); 
  rulesArray = new Array(v_heirarchyData.allRules.length);
  for(var x in v_heirarchyData.allRules){
    rulesArray[x] = v_heirarchyData.allRules[x];
  }
  for (var y in v_heirarchyData.valueNodes) {
      
      treeHash.set(v_heirarchyData.valueNodes[y].valueId, v_heirarchyData.valueNodes[y]);
      var itemId =  v_heirarchyData.valueNodes[y].valueId;
      var parentId = v_heirarchyData.valueNodes[y].parentNodeId;
      var name = v_heirarchyData.valueNodes[y].name;
      var activeFlag = true;
      if(!v_heirarchyData.valueNodes[y].isActive){
        activeFlag = false;
      }
        
      
      
      if(maxLevel < v_heirarchyData.valueNodes[y].level){
        maxLevel = v_heirarchyData.valueNodes[y].level;
      }
          
      var nextNode = newTree.add(itemId, parentId, name, activeFlag, "javascript: ;");
      if(parentId == -1){
        rootNode = nextNode;
        document.getElementById('decisionConfigId').value = v_heirarchyData.valueNodes[y].decisionConfigId;
      }
      

      

  }
  
  updateTree(newTree);

}
   
function refreshHeirarchyCallback(data)
{
    var newTree = new dTree('treePanel_Dtree');    
    
    v_heirarchyData = data.responseXML;
       
    var items = v_heirarchyData.getElementsByTagName("valueNode");
         
    for (var i = 0 ; i < items.length ; i++) {
      var item = items[i];
      
      var itemId =  parseInt(item.getAttribute('id'));
      var parentId = parseInt(item.getAttribute('parentNodeId'));
      var name = item.getAttribute('name');
      var nextNode = newTree.add(itemId, parentId, name, "javascript: ;");
    }
    
    updateTree(newTree);            
}

function addTestNode() {        
      var newRequest = new FTD_AJAX.Request('newValue.do', FTD_AJAX_REQUEST_TYPE_POST, newValueCallback, true, false);
      newRequest.send();
}



function showValueCallback(data)
{
    var item = data.responseXML.documentElement;
    

}

function addHierarchyHandlers() {
  var hierarchyDiv = document.getElementById("hierarchyInfo");
  var levelSelects = hierarchyDiv.getElementsByTagName("select");
  for (var i=0; i<levelSelects.length; i++) {
    levelSelects[i].onchange = hierarchyChanged;
  }
}

  function addRuleHandlers(){
    var rulesDiv = document.getElementById("rulesInfo");
    var ruleChecks = rulesDiv.getElementsByTagName("input");
    for (var i=0; i<ruleChecks.length; i++) {
      if(ruleChecks[i].type = 'checkbox'){
        ruleChecks[i].onchange = rulesChanged;
      }
    }
  }
  
  
  function rulesChanged(){
    var chkBox = this;
    v_unsavedChangesFlag = true;
  }
  
  function hierarchyChanged(){
    v_unsavedChangesFlag = true;
    heirarchyChanged = true;
    var cmbBox = this;
    var cmbBoxName = cmbBox.name;
    var valueId = cmbBox.value;
    var cmbBoxIndex = cmbBoxName.substring(5,cmbBoxName.length);
    var hierarchyDiv = document.getElementById("hierarchyInfo");
    var levelSelects = hierarchyDiv.getElementsByTagName("select");
    var initVal = ++cmbBoxIndex;
    var cnt = 0; var chd = false; var cont = true;
    var counts = 0; for (var index = 0; index < valueIdArray.length; ++index) { if(treeHash.get(valueIdArray[index]).parentNodeId  == valueId){counts++; }}
    var cond = 0; cond = (initVal+1); if(valueId =='None' || counts==0){cond = levelSelects.length;}//added this line for 8937
    else if(valueId !='None' && (levelSelects.length > initVal) && counts>0) { initVal = initVal; cond = levelSelects.length; chd = true;} //added for 8937
    for (var i=initVal; i<cond; i++){ //i<maxLevel; i++) { //commented for 8629
      
      var nextCmbBox = 'level'+i;
      
      if(document.getElementById(nextCmbBox) != null && document.getElementById(nextCmbBox) != undefined){
        for(var j = document.getElementById(nextCmbBox).options.length; j >= 0; j--){
          document.getElementById(nextCmbBox).remove(j);
        }
        var choice = document.createElement('option');
        choice.value = 'None';
        choice.text = 'None';
        document.getElementById(nextCmbBox).options.add(choice);

        var count = 0;
        for (var index = 0; index < valueIdArray.length; ++index) {
            if(treeHash.get(valueIdArray[index]).parentNodeId  == valueId){
                count++;                
            }
        }
        
        if(count>0 && cont){ 
          document.getElementById(nextCmbBox).disabled = false;
          for (var index = 0; index < valueIdArray.length; ++index) {
            if(treeHash.get(valueIdArray[index]).parentNodeId  == valueId){            
              choice = document.createElement('option');
              
              choice.value = treeHash.get(valueIdArray[index]).valueId;
              choice.text = treeHash.get(valueIdArray[index]).name;
              //alert('adding in if '+ choice.value + choice.text);
              document.getElementById(nextCmbBox).options.add(choice);
            }
          }
          if(chd){ cont = false;}
        }
        else{
          document.getElementById(nextCmbBox).disabled = true;
        }
      
    }else{
     if( valueId !='None')
      {
      var nextCmbBox = document.createElement('select');      
      nextCmbBox.name = 'level'+i;
      nextCmbBox.id = 'level'+i;
      nextCmbBox.setAttribute("class", "selBox");
      nextCmbBox.style.width = "180px";
      var choice = document.createElement('option');
        choice.value = 'None';
        choice.text = 'None';
        nextCmbBox.options.add(choice);

        var counte = 0;
        for (var index = 0; index < valueIdArray.length; ++index) {
            if(treeHash.get(valueIdArray[index]).parentNodeId  == valueId){
                counte++;
                //alert('inside else cnt : '+ counte);
            }
        }
        
        if(counte>0){        
          nextCmbBox.disabled = false;
          for (var index = 0; index < valueIdArray.length; ++index) {
            if(treeHash.get(valueIdArray[index]).parentNodeId  == valueId){            
              choice = document.createElement('option');
              
              choice.value = treeHash.get(valueIdArray[index]).valueId;
              choice.text = treeHash.get(valueIdArray[index]).name;
              //alert('adding in else '+ choice.value + choice.text);
              nextCmbBox.options.add(choice);
            }
          }
        }
        else{
          nextCmbBox.disabled = true;
        }
        
        nextCmbBox.onchange = hierarchyChanged;
        insRow('Level'+i, nextCmbBox);
    
    }
    else{
      alert('Please select a value other than NONE');
    }
    
  }
  }  

  }
  
  function getGlobalData(){
    var newRequest = new FTD_AJAX.Request('getHierarchy.do?decisionType=' + v_decisionTypeCode, FTD_AJAX_REQUEST_TYPE_POST, populateGlobalVars, true, false);
    newRequest.addParam("securitytoken",document.getElementById("securitytoken").value);
    newRequest.addParam("context",document.getElementById("context").value);
    newRequest.addParam("adminAction",document.getElementById("adminAction").value);
    newRequest.send();
  }
  
  function populateGlobalVars(data){
    if(eval(data.responseText) == null || eval(data.responseText) == undefined){
      window.location = "http://" + window.location.host + "/secadmin";
      return;
    }
    treeHash = new Hash();
    var sessionData =data.responseText.evalJSON(); 
    rulesArray = new Array(sessionData.allRules.length);
    for(var x in sessionData.allRules){
      rulesArray[x] = sessionData.allRules[x];
    }
    for (var y in sessionData.valueNodes) {
      treeHash.set(sessionData.valueNodes[y].valueId, sessionData.valueNodes[y]);
      if(sessionData.valueNodes[y].parentNodeId == -1){
        var itemId =  sessionData.valueNodes[y].valueId;
        var parentId = sessionData.valueNodes[y].parentNodeId;
        var name = sessionData.valueNodes[y].name;
        var firstNode = newTree.add(itemId, parentId, name, "javascript: ;");
        rootNode = firstNode;
      }
    }
    valueIdArray = treeHash.keys();
  
  }
  

  
  function ModalPopupsYesNoCancelYes() {
      yesnocancelSel = true;
      //ModalPopups.Close("idYesNoCancel1");
      var newSelNodeId = "";
      var currentSelNodeId = "";
      if(v_newNode != null){
        newSelNodeId = v_newNode.id;
        selectFlag = false;
      }else if(v_currentNode != null){
        currentSelNodeId = v_currentNode.id;
        selectFlag = false;
      }else{
        selectFlag = true;
      }
        
      saveValueNode();
      v_unsavedChangesFlag = false;
      cancelSelected = false;
      yesnocancelSel = false;
      if(newSelNodeId != "" && !selectFlag){
        treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(newSelNodeId));
        processSelection(newSelNodeId);
        selectFlag = true;
      }else if(currentSelNodeId != "" && !selectFlag){
        processSelection(currentSelNodeId);
        treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(currentSelNodeId));
        treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(currentSelNodeId));
        handleAddValue();
        treePanel_Dtree.o(treePanel_Dtree.getNodeIndexById(treeHash.get(currentSelNodeId).parentNodeId));
        selectFlag = true;
      }            
    }
    function ModalPopupsYesNoCancelNo() {
      yesnocancelSel = true;
      //ModalPopups.Cancel("idYesNoCancel1");
      //alert('discard changes');
      v_unsavedChangesFlag = false;
      cancelSelected = false;
      yesnocancelSel = false;
      if(v_newNode != null){
        treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(v_newNode.id));
        processSelection(v_newNode.id);
      }else{
        handleAddValue();
      }      
    }
    function ModalPopupsYesNoCancelCancel() {
      yesnocancelSel = false;
      //ModalPopups.Cancel("idYesNoCancel1");
      //alert('do nothing');
      v_unsavedChangesFlag = true;
      yesnocancelSel = false;
      cancelSelected = false;
      backButtonFlag = false;   //jbaba 8865
      return false;
    }
  
  function checkAddValue(){
    if(v_unsavedChangesFlag && !yesnocancelSel && !cancelSelected){
      v_currentNode = treePanel_Dtree.getNodeById(document.getElementById('nodeId').value);
      v_newNode = null;
      showYesNoCancel();
    }else{
      handleAddValue();
    }
  }
  
  function handleAddValue(){
          
    v_unsavedChangesFlag = false;
    heirarchyChanged = false;
    var hierarchyInfoDiv = document.getElementById('hierarchyInfo');
    var tblElement = document.getElementById('hierarchyTbl');
    var rulesInfoDiv = document.getElementById('rulesInfo');
    var valueId = document.getElementById('nodeId').value;
    
    //alert(valueId);
    if(treeHash == null || treeHash.keys().size() == 0){
      getGlobalData();
    }
    
    //mode = "add";
    document.getElementById('previewDiv').style.display = "none";
    document.getElementById('nodeName').value="";
    document.getElementById('nodeId').value="";
    
    var level =   treeHash.get(valueId).level
    level++;
    valueIdArray = treeHash.keys();
    
    document.getElementById('nonRootTd').style.display = 'block';  
    document.getElementById('rootTd').style.display = 'none';
    replaceText(document.getElementById('sec1'), level+' ');
    replaceText(document.getElementById('nxt'),level+1+' ');
    //delete Hierarchy assignment section
    delRows(tblElement);
    
    rulesInfoDiv.style.display = "block";
    deleteRules(rulesInfoDiv);
    rulesInfoDiv.disabled = false;
    var newReqHierarchy = new FTD_AJAX.Request('valueNodes.do?valueNodeId='+valueId+'&method=hierarchyNodes', FTD_AJAX_REQUEST_TYPE_POST, processNewHierarchyNodes, true, false);
    newReqHierarchy.addParam("decisionConfigId",document.getElementById('decisionConfigId').value);
    newReqHierarchy.addParam("securitytoken",document.getElementById("securitytoken").value);
    newReqHierarchy.addParam("context",document.getElementById("context").value);
    newReqHierarchy.addParam("adminAction",document.getElementById("adminAction").value);
    newReqHierarchy.send();
    
    addRules(rulesInfoDiv);
    
    
    generateNextLevel(null);
    
    
    
    document.getElementById('parentValueId').value = valueId;
    addHierarchyHandlers();
    //hierarchyInfoDiv.disabled = true; //commented by jbaba
    toggleChildDisabled(hierarchyInfoDiv); //line added by jbaba
    document.getElementById('nodeName').focus();

  
  }
  
  
  function processChildSelection(valueId){
    
    v_currentNode = treePanel_Dtree.getNodeById(document.getElementById('nodeId').value);
    v_newNode = treePanel_Dtree.getNodeById(valueId);
    if(v_unsavedChangesFlag && !yesnocancelSel && !cancelSelected){
     showYesNoCancel();
     //v_unsavedChangesFlag = false; //commented by jbaba to fix display popup for attached values #8627
     heirarchyChanged = false;
     if(v_currentNode.pid != -1 && !treePanel_Dtree.isOpen(v_currentNode.id))
      treePanel_Dtree.o(treePanel_Dtree.getNodeIndexById(v_currentNode.id));
     //return false;
    }else{
      processSelection(valueId);  
      treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(valueId));
      if(v_currentNode.pid != -1 && !treePanel_Dtree.isOpen(v_currentNode.id))
        treePanel_Dtree.o(treePanel_Dtree.getNodeIndexById(v_currentNode.id));
    }
  
  }
  
  function validateSaveNode(){
    var hierarchyDiv = document.getElementById("hierarchyInfo");
    var nodeId = document.getElementById('nodeId').value;
    var nodeStatus = document.getElementById('nodeStatus').options[document.getElementById('nodeStatus').selectedIndex].value;
    var nodeName = document.getElementById('nodeName').value;
    var scriptText = document.getElementById('scripting').value;
    var parentId;
    
    if(nodeName == null || nodeName == "" || nodeName == undefined){
      alert("Please provide a name to continue.");
      return false;
    }else if (nodeName.capitalize() == "None"){
      alert("A value cannot ne named 'None'. Please update to continue.");
      return false;
    }else if (!nodeName.match(/^[a-zA-Z0-9- /?.()]+$/)) {
      alert("The Name field contains an invalid value. Please update to continue.");
      return false;
    }
    if(document.getElementById('nodeStatus').options[document.getElementById('nodeStatus').selectedIndex].value == 'N'){
        var cnt = 0;
        for (var  index = 0; index < valueIdArray.length; ++index) {         
          if(treeHash.get(valueIdArray[index]).parentNodeId  == nodeId){
            cnt++
          }
        }
        var question = "All child values will be updated to inactive. Would you like to continue?";
        if(cnt > 0){
          var ans = confirm(question);
          if(!ans){
            return false;
          }
       } 
    }
    var hierarchyNodes = hierarchyDiv.getElementsByTagName('select');
    if(nodeId.blank()){
      for(var i = hierarchyNodes.length-1; i >=0; i--){
      //alert(hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].text);
        if(hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].value != 'None'){
          parentId = hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].value;
          if(parentId == nodeId){
            document.getElementById('parentValueId').value = -1;
          }else{
            document.getElementById('parentValueId').value = parentId;
          }
          break;
        }
      }  
    }else{
      for(var i = hierarchyNodes.length-1; i >=0; i--){
      //alert(hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].text);
        if(hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].value != 'None' 
          && hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].value != nodeId){
          parentId = hierarchyNodes[i].options[hierarchyNodes[i].selectedIndex].value;
          if(parentId == nodeId){
            document.getElementById('parentValueId').value = -1;
          }else{
            document.getElementById('parentValueId').value = parentId;
          }
          break;
        }
      }
    }
    
    //validate duplicate name
    var valueIds = treeHash.keys();
    for(var i = 0; i <= valueIds.length; i++){
      
      if(valueIds[i] != undefined && treeHash.get(valueIds[i]).parentNodeId == parentId){
        if((nodeId.blank() || nodeId.empty()) && treeHash.get(valueIds[i]).name.toUpperCase() == nodeName.toUpperCase()){
          //Duplicate name found
          alert("A value with the same name exists. Please update to continue.");
          return false;
        }else{
          if(valueIds[i] != undefined && treeHash.get(valueIds[i]).valueId != nodeId && treeHash.get(valueIds[i]).name != null &&
          treeHash.get(valueIds[i]).name.toUpperCase() == nodeName.toUpperCase()){
            //Duplicate name found
            alert("A value with the same name exists. Please update to continue.");
            return false;
          }
        }
      }
    }
    if(heirarchyChanged){
      //validate for duplicate name
      /*var question = "All child values will be updated to the new hierarchy assignments. Would you like to continue?";
      var ans = confirm(question);
      if(!ans){
        return false;
      }*/
      var cnt = 0;
        for (var  index = 0; index < valueIdArray.length; ++index) {         
          if(treeHash.get(valueIdArray[index]).parentNodeId  == nodeId){
            cnt++
          }
        }
        var question = "All child values will be updated to the new hierarchy assignments. Would you like to continue?";
        if(cnt > 0){
          var ans = confirm(question);
          if(!ans){
            return false;
          }
       } 
              
    }
    
    if(!nodeId.blank() && !treeHash.get(nodeId).isActive && nodeStatus == 'Y'){
      //alert(treeHash.get(nodeId).parentNodeId);
      if(treeHash.get(nodeId).parentNodeId != null && treeHash.get(nodeId).parentNodeId != -1 
        && !treeHash.get(treeHash.get(nodeId).parentNodeId).isActive){
        alert("You can not activate child node while its parent is inactive. Please activate parent first.");
        return false;
      }
    }
    
    if(!scriptText.blank()){
      if (!scriptText.match(/^[a-zA-Z0-9- /?.()]+$/)) {
        alert("The Scripting field contains an invalid value. Please update to continue.");
        return false;  
      }
    }
    return true;
  }
  
  function saveValueNode(){
      if(!validateSaveNode()){
        return false;
      }
      var rules = "";
      var childNodes = "";
      var hierarchyDiv = document.getElementById("hierarchyInfo");
      var ruleChecks = document.getElementsByTagName('input');
      var childItemsDiv = document.getElementById('childItems');
      var childItems = childItemsDiv.getElementsByTagName('a');
      var childTbl = document.getElementById('childTbl');
      var childRows = childTbl.rows;
      for (var i=0; i<ruleChecks.length; i++) {
        if(ruleChecks[i].type == 'checkbox'){
          if(ruleChecks[i].checked == true && ruleChecks[i].value != null && ruleChecks[i].value.substring(0,2) == 'cd'){
            rules = rules + ruleChecks[i].value+",";
          }
        }
      }
      if(rules != null && rules.length > 0)
        rules = rules.substring(0,rules.length-1);
      
      
      for(var i = 0; i< childRows.length; i++){
        childNodes = childNodes + childRows[i].cells[1].firstChild.name+",";
      }
      childNodes = childNodes.substring(0,childNodes.length-1);
      //alert(childNodes);
      
      document.getElementById('valueRules').value = rules;
      document.getElementById('childNodes').value = childNodes;
      


      var url = "valueNodes.do?method=saveValueNode";
      
        

     var newRequest = new FTD_AJAX.Request(url, FTD_AJAX_REQUEST_TYPE_POST, handleSaveCallback, true, false);
     
     newRequest.addParam("nodeId",document.getElementById('nodeId').value);
     newRequest.addParam("nodeName",document.getElementById('nodeName').value);
     if(document.getElementById('parentValueId').value == -1)
      newRequest.addParam("parentValueId", null);
     else
      newRequest.addParam("parentValueId",document.getElementById('parentValueId').value);
     
     /*if(document.getElementById('parentValueId').value == null || 
      document.getElementById('parentValueId').value == ""  || document.getElementById('parentValueId').value == -1) */
      goToNode = document.getElementById('nodeId').value;
     /*else
      goToNode = document.getElementById('parentValueId').value;*/
      
     newRequest.addParam("nodeStatus",document.getElementById('nodeStatus').options[document.getElementById('nodeStatus').selectedIndex].value);
     if(document.getElementById('scripting').value != null && document.getElementById('scripting').value != undefined){
      newRequest.addParam("scripting",document.getElementById('scripting').value);
     }else{
      newRequest.addParam("scripting","");
     }
     if(document.getElementById('allowComments').checked == true)
     {     
     newRequest.addParam("allowComments","Y");
     //newRequest.addParam("allowComments",document.getElementById('allowComments').value);
     }
     if(document.getElementById('allowComments').checked == false)
     {     
     newRequest.addParam("allowComments","N");
     }
     newRequest.addParam("valueRules",rules);
     newRequest.addParam("childNodes",childNodes);
     newRequest.addParam("decisionConfigId",document.getElementById('decisionConfigId').value);
     newRequest.addParam("securitytoken",document.getElementById("securitytoken").value);
     newRequest.addParam("context",document.getElementById("context").value);
     newRequest.addParam("adminAction",document.getElementById("adminAction").value);
      //alert('Swamy : '+ url);
      
     newRequest.send();
  }
  
  function handleSaveCallback(data){
    if(eval(data.responseText) == null || eval(data.responseText) == undefined){
      window.location = "http://" + window.location.host + "/secadmin";
      return;
    }else{
      var outValueId = eval(data.responseText);
      
      if(outValueId != null && outValueId > 0)
        goToNode = outValueId;
    }
    
    toggleGlobalVars(false);
    init();
    if(selectFlag && goToNode != null & goToNode != ""){
      processSelection(goToNode);
      treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(goToNode));
      treePanel_Dtree.openTo(goToNode, true, false); //wasn't here
    }    
    //init(); //wasn't here
  }
    
  function cancelValueNode(){
    var valueId = document.getElementById('nodeId').value;
    var parentValId = document.getElementById('parentValueId').value;
    if(valueId != null && valueId != ""){
      processSelection(valueId);
    }else{
      processSelection(parentValId);
    }
  }
  
  function deleteValueNode(){
    var valId = document.getElementById('nodeId').value;
    var count = 0;
        for (var index = 0; index < valueIdArray.length; ++index) {
            if(treeHash.get(valueIdArray[index]).parentNodeId  == valId){
                count++;                
            }
        }
        
        if(count>0){
        }
        else{
              var question = "Are you sure, you want to delete selected value?";
              var ans = confirm(question);
              if(!ans){
              return false;
              }                 
        }
    var url = "valueNodes.do?method=deleteValueNode";
    goToNode = treeHash.get(document.getElementById('nodeId').value).parentNodeId;
    var deleteRequest = new FTD_AJAX.Request(url, FTD_AJAX_REQUEST_TYPE_POST, handleDeleteCallback, true, false);
    deleteRequest.addParam("nodeId",document.getElementById('nodeId').value);
    deleteRequest.addParam("decisionConfigId",document.getElementById('decisionConfigId').value);
    deleteRequest.addParam("securitytoken",document.getElementById("securitytoken").value);
    deleteRequest.addParam("context",document.getElementById("context").value);
    deleteRequest.addParam("adminAction",document.getElementById("adminAction").value);
    deleteRequest.send();
  }
  
  function handleDeleteCallback(data){
    toggleGlobalVars(false);
    if(eval(data.responseText) == null || eval(data.responseText) == undefined){
      window.location = "http://" + window.location.host + "/secadmin";
      return;
    }
    v_newNode = null;
    var str = data.responseText.evalJSON(true).toString();
    if(str.startsWith('INFO')){
      init();
      processSelection(goToNode);
      treePanel_Dtree.s(treePanel_Dtree.getNodeIndexById(goToNode));
      treePanel_Dtree.openTo(goToNode, true, false);
    }else{
      alert(str);
      
    }
   
   
  }
  
  function processNewHierarchyNodes(data){
    var hierarchyDiv = document.getElementById("hierarchyInfo");
    var hierarchyNodes = eval(data.responseText);
    var levelSelects = hierarchyDiv.getElementsByTagName("select");
    var hierarchyHash = new Hash();
    var parentId = -1;
    if(hierarchyNodes == null || hierarchyNodes == undefined){
      window.location = "http://" + window.location.host + "/secadmin";
      return;
    }
    for(var x = 0; x < hierarchyNodes.length; x++){
      hierarchyHash.set(x, hierarchyNodes[x]);
    }
    //alert(levelSelects.length);
    var selectedNodeId;
    if(v_newNode == null)
      selectedNodeId = rootNode.id;
    else
      selectedNodeId = v_newNode.id;
      
    var selectedLevel = treeHash.get(selectedNodeId).level;
    for (var i=0; i<hierarchyNodes.length; i++) {
      var combo_box = document.createElement('select');      
      combo_box.name = 'level'+i;
      combo_box.id = 'level'+i;
      combo_box.setAttribute("class", "selBox");
      combo_box.style.width = "180px";
      var choice = document.createElement('option');
      
      for (var index = 0; index < valueIdArray.length; ++index) {
        choice = document.createElement('option');
        if(treeHash.get(valueIdArray[index]).level - 1 == i && treeHash.get(valueIdArray[index]).parentNodeId == parentId){
          choice.value = treeHash.get(valueIdArray[index]).valueId;
          choice.text = treeHash.get(valueIdArray[index]).name;
          if(i == hierarchyNodes.length - 1){
            if(choice.value == selectedNodeId ){
              combo_box.options.add(choice);
            }
          }else{
            if(choice.value == selectedNodeId ){
              if(parentId == -1 ){
                combo_box.options.add(choice);
              }
            }else{
              combo_box.options.add(choice);
            }
          }
          
        }
      }
      for(var j = 0; j < combo_box.options.length; j++){
        if(combo_box.options[j].value ==  hierarchyHash.get(i).valueId && combo_box.options[j].value != selectedNodeId){
          combo_box.options[j].selected='selected';
          parentId = hierarchyHash.get(i).valueId;
        }
      }
      if(i > 0){
        insRow('Level'+i, combo_box); 
      }else{
        insRow('Root Hierarchy', combo_box);
      }
    }
     addHierarchyHandlers();
  
  }
  
  function applyInactiveCss(){
    var valueIds = treeHash.keys();
    for (var index = 0; index < valueIds.length; ++index) {
      //alert(treeHash.get(valueIds[index]).isActive);
      if(!treeHash.get(valueIds[index]).isActive){
        alert(treeHash.get(valueIds[index]).isActive + ' - '+ valueIds[index]);
        var node = treePanel_Dtree.getNodeById(valueIds[index]);
        addClass(node, "inactive");
      }
    }
  }
  

function moveUp(currentIdx){
  v_unsavedChangesFlag = true;
  var childTbl = document.getElementById('childTbl');
  var currentCell = childTbl.rows[currentIdx].cells[1];
  var previousCell = childTbl.rows[currentIdx-1].cells[1];
  var currentChild = currentCell.firstChild;
  var previousChild = previousCell.firstChild;
  currentCell.removeChild(currentChild);
  previousCell.removeChild(previousChild);
  currentCell.appendChild(previousChild);
  previousCell.appendChild(currentChild);
}
  
function moveDown(currentIdx){
  v_unsavedChangesFlag = true;
  var childTbl = document.getElementById('childTbl');
  var currentCell = childTbl.rows[currentIdx].cells[1];
  var nextCell = childTbl.rows[currentIdx+1].cells[1];
  var currentChild = currentCell.firstChild;
  var nextChild = nextCell.firstChild;
  currentCell.removeChild(currentChild);
  nextCell.removeChild(nextChild);
  currentCell.appendChild(nextChild);
  nextCell.appendChild(currentChild);
}


function publish(){
  if(v_unsavedChangesFlag && !yesnocancelSel && !cancelSelected){
     showYesNoCancel();
  }
    var question = "Are you sure you want to publish all changes?";
    var ans = confirm(question);
    if(!ans){
      return false;
    }
    var url = "valueNodes.do?method=pulishDecisionType";
    var newRequest = new FTD_AJAX.Request(url, FTD_AJAX_REQUEST_TYPE_POST, handlePublishCallback, true, false);
    newRequest.addParam("decisionConfigId",document.getElementById('decisionConfigId').value);
    newRequest.addParam("decisionTypeCode","calldisposition");
    newRequest.addParam("securitytoken",document.getElementById("securitytoken").value);
    newRequest.addParam("context",document.getElementById("context").value);
    newRequest.addParam("adminAction",document.getElementById("adminAction").value);
    newRequest.send();  

}

function handlePublishCallback(data){

  if(eval(data.responseText) == null || eval(data.responseText) == undefined){
    window.location = "http://" + window.location.host + "/secadmin";
    return;
  }
  toggleGlobalVars(false);
  if(eval(data.responseText)!=undefined){
    alert(eval(data.responseText));
    init();
  }else{
    init();
  }
}

function doMainMenuAction(){
  backButtonFlag = true;
  if(v_unsavedChangesFlag && !yesnocancelSel && !cancelSelected){
     showYesNoCancel();
  }
  if(backButtonFlag)
  {
  var url = ''; /*url = "valueNodes.do?method=mainMenu"+getSecurityParams(true);  */
  //url="MainMenuAction.do?securitytoken=" + escape(document.getElementById("securitytoken").value) + "&context=" + escape(document.getElementById("context").value) + "&adminAction=" + escape(document.getElementById("adminAction").value);
  url = "http://"+window.location.host+"/secadmin/security/Main.do?adminAction=" + escape(document.getElementById("adminAction").value)
  + "&securitytoken=" + escape(document.getElementById("securitytoken").value) 
  + "&context=" + escape(document.getElementById("context").value);
  performAction(url);   
  }
}

function performAction(url){
  document.forms[0].action = url;    
  document.forms[0].submit();  
}

function getSecurityParams(areOnlyParams){
   return   "&securitytoken=" + escape(document.getElementById("securitytoken").value) +
            "&context=" + escape(document.getElementById("context").value) +
            "&adminAction=" + escape(document.getElementById("adminAction").value);
}


function previewDraft(){
  var url = "http://"+window.location.host+"/decisionresult/resultPreview.do?method=previewDraft&dr_decisionConfigId=";
  url += escape(document.getElementById('decisionConfigId').value)+"&dr_decisionTypeCode="+escape(document.getElementById('decisionTypeCode').value)+ getSecurityParams(true);
  var sFeatures="dialogHeight:500px;dialogWidth:350px;";
  sFeatures +=  "center:yes;status:0;scroll:yes;resizable:0";
  window.showModalDialog(url, null, sFeatures);
}

function previewPublish(){
  var url = "http://"+window.location.host+"/decisionresult/resultPreview.do?method=previewPublish";
  url += "&dr_decisionTypeCode="+escape(document.getElementById('decisionTypeCode').value)+getSecurityParams(true);
  var sFeatures="dialogHeight:500px;dialogWidth:350px;";
  sFeatures +=  "center:yes;status:0;scroll:yes;resizable:0";
  window.showModalDialog(url, null, sFeatures);
}

function imposeMaxLength(Event, Object, maxLen)
{
  return (Object.value.length < maxLen)||(Event.keyCode == 8 ||Event.keyCode==46||(Event.keyCode>=35&&Event.keyCode<=45));
}

function checkMaxLength(object, maxLen){
  var str = object.value+window.clipboardData.getData("Text");
  if(str.length <= maxLen){
    return true;
  }else{
    event.returnValue = false;
    object.value = str.substring(0,maxLen);
    return;
  }
}

function toggleGlobalVars(flag){
  yesnocancelSel = flag;
  cancelSelected = flag;
  heirarchyChanged = flag;
  v_unsavedChangesFlag = flag;  
}

function showYesNoCancel(){
  var modalUrl = "yesnocancel.html";
  var modalArguments = new Object();
  
   modalArguments.modalText = 'Would you like to save changes?';

      
      var modalFeatures  =  'dialogWidth:300px; dialogHeight:150px; center:yes; status=no; help=no;';
      modalFeatures +=  'resizable:no; scroll:no';

      
      var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
      //implement cancel
      if(!ret){
        //alert('cancel');
        ModalPopupsYesNoCancelCancel();
      }
      else if(ret == 'yes')
      {
          ModalPopupsYesNoCancelYes();
          //alert('Yes');
      }
      else if (ret == 'no')
      {
        ModalPopupsYesNoCancelNo();
        //alert('No');
      }

}

function showModalWindow(){
  window.open("decisionresult.jsp", "ha_dialog","toolbar=no,menubar=no,personalbar=no," +
                                             "width=10,height=10,scrollbars=no,resizable=no," +
                                             "dependent=yes,z-lock=yes");
  
}