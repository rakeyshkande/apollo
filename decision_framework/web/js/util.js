/*
 *  Dependencies: js/FormChek.js
 */


/*
this variable is used in the Modify Order process in order
to make lock checking as generic as possible
also see the checkRecordLock() function in this file.
*/
var isRecordLocked = false;

/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Closes the parent window.
*/
function closeParentWindow()
{
 if(window.parent != undefined && window.parent!=null)
   {
       top.close();
   }
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8)
   {

			//when the control is on the page instead of an element like text box, we want to suppress the
			//back space.  This would prevent the users from hitting Alt+Backspace and would force them to
			//use the BACK button provided.
      var formElement = false;

			//get the form
			var testForm = document.forms[0];

			//if the page actually had a form, go in the loop.
			if (testForm != null)
			{
				for(i = 0; i < document.forms[0].elements.length; i++)
				{
					 if(document.forms[0].elements[i].name == document.activeElement.name)
					 {
							formElement = true;
							break;
					 }
				}
			}
			//else check if the activeElement is a Text or TextArea that is part of the body
			else if (document.activeElement.type == "text" || document.activeElement.type == "textarea")
			{
				formElement = true;
			}


      var searchBox = document.getElementById('numberEntry');
      if(searchBox != null){
        if(document.getElementById('numberEntry').id == document.activeElement.id){
          formElement = true;
        }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*
  Catch all numeric keypresses and send them to the search box.
*/
function evaluateKeyPress()
{
  var a;
  var keypress = window.event.keyCode;

  if (keypress == 48||keypress==96)
    a = 0;
  if (keypress == 49||keypress==97)
    a = 1;
  if (keypress == 50||keypress==98)
    a = 2;
  if (keypress == 51||keypress==99)
    a = 3;
  if (keypress == 52||keypress==100)
    a = 4;
  if (keypress == 53||keypress==101)
    a = 5;
  if (keypress == 54||keypress==102)
    a = 6;
  if (keypress == 55||keypress==103)
    a = 7;
  if (keypress == 56||keypress==104)
    a = 8;
  if (keypress == 57||keypress==105)
    a = 9;

  if (a >= 0 && a <= 9)
  {
    document.getElementById("numberEntry").focus();
  }
}

function doEnterKeyAction(functionToCall) {
  if (event.keyCode == 13)
    functionToCall();
}


/*
   Returns the style object for the given element''s field name.
*/
function getStyleObject(fieldName)
{
   if (document.getElementById && document.getElementById(fieldName))
      return document.getElementById(fieldName).style;
}

/*
   Limits input to digits only.
*/
function digitOnlyListener()
{
   var input = event.keyCode;
   if (input < 48 || input > 57){
       event.returnValue = false;
   }
}

/*
   The limitTextarea will squelch any characters that exceed the limit value included in the method call.
   textarea - the textarea object
   limit - the number of characters allowed
*/
function limitTextarea(textarea, limit)
{
   if (textarea.value.length > limit)
      textarea.value = textarea.value.substring(0, limit);
}

/*
   The following function changes the control's style sheet to 'errorField'.
   elements - an Array of control names on the form
*/
function setErrorFields(elements){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}
function setErrorField(element){
    var element = document.getElementById(element);
    if (element != null){
        element.className = "errorField";
    }
}

/*
    The following functions attached the 'onfocus' and 'onblur' events the the input
    elements.  When a control on the page gains focus, the control's border changes to red.
    When focus is lost, the control's border changes back to default.

    elements - an Array of control names on the form
    element - a control name on the form
*/
function addDefaultListeners(){
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < inputs.length; i++){
    if ( inputs[i].type == "text" ) {
      addDefaultListenersSingle(inputs[i]);
    }
  }
}
function addDefaultListenersArray(elements){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null)
            addDefaultListenersSingle(element);
    }
}
function addDefaultListenersSingle(element){
    element.attachEvent("onfocus", fieldFocus);
    element.attachEvent("onblur", fieldBlur);
}
function fieldFocus(){
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = "red";
}
function fieldBlur(){
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}

/*
    The following functions change the cursor UI when an image triggers a 'mouseover' event.

    elements - an Array of control names on the form
*/
function addImageCursorListener(elements){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.attachEvent("onmouseover", imageOver);
            element.attachEvent("onmouseout", imageOut);
        }
    }
}
function imageOver(){
    document.forms[0].style.cursor = "hand";
}
function imageOut(){
    document.forms[0].style.cursor = "default";
}

/*
    The following functions are used to dis/enable fields.

    elements - an Array of control names on the form
    access - boolean value used to set the disabled property
*/
function disableFields(elements){
    setFieldsAccess(elements, true)
}
function enableFields(elements){
    setFieldsAccess(elements, false)
}
function setFieldsAccess(elements, access){
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            setFieldAccess(element, access)
        }
    }
}
function setFieldAccess(element, access){
   element.disabled = access;
}

/*
   The following functions are used to show and hide any
   select boxes that would normally appear in front of a popup.
*/
function getAbsolutePos(element) {
   var r = { x: element.offsetLeft, y: element.offsetTop };
   if (element.offsetParent) {
      var tmp = getAbsolutePos(element.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
   }
   return r;
}
function hideShowCovered(popup){
   function contains(a, b) {
      while (b.parentNode)
         if ((b = b.parentNode) == a)
            return true;
      return false;
   };

   var p = getAbsolutePos(popup);
   var EX1 = p.x;
   var EX2 = popup.offsetWidth + EX1;
   var EY1 = p.y;
   var EY2 = popup.offsetHeight + EY1;

   var tagsToHide = new Array("select");
   for (var k = 0; k < tagsToHide.length; k++) {
      var tags = document.getElementsByTagName(tagsToHide[k]);
      var tag = null;

      for (var i = 0; i < tags.length; i++) {
         tag = tags[i];
         if (!contains(this.div, tag)){
            p = getAbsolutePos(tag);
            var CX1 = p.x;
            var CX2 = tag.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = tag.offsetHeight + CY1;

            if ( (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1) ) {
               if (!tag.saveVisibility) {
                  tag.saveVisibility = tag.currentStyle["visibility"];
               }
               tag.style.visibility = tag.saveVisibility;
            } else {
               if (!tag.saveVisibility) {
                  tag.saveVisibility =  tag.currentStyle["visibility"];
               }
               tag.style.visibility = "hidden";
            }
         }
      }
   }
}

/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  content - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
function showWaitMessage(content, wait, message){
  window.scrollTo(0,0);
  var content = document.getElementById(content);
  var height = content.offsetHeight;
  var waitDiv = document.getElementById(wait + "Div").style;
  var waitMessage = document.getElementById(wait + "Message");
  _waitTD = document.getElementById(wait + "TD");

  content.style.display = "none";
  waitDiv.display = "block";
  waitDiv.height = height;
  waitMessage.innerHTML = (message) ? message : "Processing";
  clearWaitMessage();
  updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage(){
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}
function updateWaitMessage(){
   setTimeout("updateWaitMessage()", 1000);
   setTimeout("showPeriod()", 1000);
}
function showPeriod(){
   _waitTD.innerHTML += "&nbsp;.";
}
function hideWaitMessage(content, wait){
   var content = document.getElementById(content);
   var waitDiv = document.getElementById(wait + "Div").style;
   content.style.display = "block";
   waitDiv.display = "none";
   clearTimeout("clearWaitMessage()");
   clearTimeout("updateWaitMessage()");
   clearTimeout("showPeriod()");
}

/*
   This is a helper function to return the two security parameters
   required by the security framework.

   areOnlyParams -   is used to change the url string (? vs. &) depending if these
                     are the only parameters in the url
*/
function getSecurityParams(areOnlyParams){
   return   ((areOnlyParams) ? "?" : "&") +
            "securitytoken=" + document.getElementById("securitytoken").value +
            "&context=" + document.getElementById("context").value +
            "&adminAction=" + document.getElementById("adminAction").value;
}
/*
  This function is used to clear all form fields on a page.
*/
function clearAllFields(form){

   for(var i = 0; i < form.elements.length; i++){
  if(form.elements[i].type == 'radio')
    form.elements[i].checked = false;
  if(form.elements[i].type == 'text')
    form.elements[i].value = "";
  if(form.elements[i].type == 'checkbox')
    form.elements[i].checked = false;
   }
}

function clearAllFieldsBetter(form) {
	
   for(var i = 0; i < form.elements.length; i++){
	if(form.elements[i].type == 'radio')
		form.elements[i].checked = false;
	else if(form.elements[i].type == 'text')
		form.elements[i].value = "";
	else if(form.elements[i].type == 'checkbox')
		form.elements[i].checked = false;	
  else if(form.elements[i].type == 'select-one')
		form.elements[i].selectedIndex = 0;	
  else if(form.elements[i].type == 'textarea')
		form.elements[i].value = "";
  else if(form.elements[i].type == 'file')
		form.elements[i].value = "";
   }
}

/***********************************************************************************
 doAccountSearch()
 This function is called from the orderShoppingCrat.xsl
 which is part of the customerAccount / customerAccountDetail.xsl files
************************************************************************************/
function doAccountSearch(orderNumber, buyerId){
  var guid = document.getElementById('guid_' + orderNumber).value;
  var url = "customerOrderSearch.do" +
      "?action=customer_account_search" + "&order_number=" + orderNumber
         + "&order_guid=" + guid + "&customer_id=" + buyerId;
  performAction(url);
}



/***********************************************************************************
  doReturnEmailAddresses()
  Called from the customerAccount page.Used to display additional email addresses
  tied to a customer
************************************************************************************/
function doReturnEmailAddresses(){
  var url_source = "customerOrderSearch.do" + "?action=more_customer_emails" +
      "&customer_id=" + cust_id + getSecurityParams(false);
    var modal_dim = "dialogWidth:775px; dialogHeight:355px; center:yes; status=0; help:no; scroll:no";
  showModalDialogIFrame(url_source, "", modal_dim);
  //sourceCodeWindow = window.open("customerEmailAddresses.html","testPage","width=700,height=200,location=no,status=no,toolbar=no,top=130,left=40,titlebar=no,title=no");
}

/*
  The following functions are triggered by buttons which are found on both the Customer Account
  page and the Customer Shopping Cart page.
*/
function doAccountHistoryAction(){
  var url = "loadHistory.do?page_position=1" +
    "&history_type=Customer" + "&customer_id=" + document.csci.customer_id.value +
    getSecurityParams(false);

  performAction(url);
}

function doLossPreventionAction()
{
  if(document.csci.loss_prevention_ind.value != '')
  {
    var url = "lossPrevention.do?customer_id=" + document.csci.customer_id.value +
            "&order_guid=" + document.csci.order_guid.value;
    performAction(url);
  }
  else
  {
    alert("There is no loss prevention indicator for this shopping cart");
  }
}



 function doHoldDetailAction(frompage,orderdetailid,externalOrderNumber){

  var url = "https://" + siteNameSsl + applicationContext + "/CustomerHoldAction.do" +
       "?action_type=loadcustomer&from_page=" + frompage  + "&order_detail_id=" + orderdetailid + "&external_order_number=" + externalOrderNumber + getSecurityParams(false);
  performAction(url);
 }


 function doHoldAction(frompage){

  var obj =document.getElementById("recipient_search");

  var flag = "";
  if(obj != null){
    flag = obj.value;
    }


  var url = "https://" + siteNameSsl + applicationContext + "/CustomerHoldAction.do" +
       "?action_type=loadcustomer&from_page=" + frompage  + "&recipient_search=" + flag + getSecurityParams(false);


  performAction(url);
}

/*
  This function receives as a parameter the name of the iframe which will
  be used when transforming the checkUpdateCustomerStatusIFrame.xsl page.
  We do this becuase this function can be executed on the Customer Account page
  (which can exist by itself) and on the Customer Shopping Cart page.
  When the CSR clicks the Update Customer button on the Customer Account page
  the value 'caUpdateCustomer' is passed. When the CSR clicks the Update Customer
  button on the Customer Shopping Cart the 'csUpdateCustomer' value is passed.
  These values refer to two different iframes on there respective pages.
*/
var displayPageName = '';
var updateCustomerAccountid = '';

function doUpdateCustomerAction(iframeName,count){
  if(iframeName != ''){
    if(iframeName == 'caUpdateCustomer'){
     if(count > 0){
      displayPageName = 'ACCTINFO';
     }else{
      displayPageName = 'CUSTOMER';
     }
    }
    if(iframeName == 'csUpdateCustomer'){
      displayPageName = 'ORDERS';
    }
    updateCustomerAccountid = document.getElementById('customer_id').value;
    var frame = document.getElementById(iframeName);

    var url = 'loadCustomerAccount.do?action=check_customer_account' +
        "&customer_id=" + updateCustomerAccountid + "&display_page=" + displayPageName +
          getSecurityParams(false);
    frame.src = url;
  }
}
/*
  This function is called by the page checkUpdateCustomerStatusIFrame.xsl
  If the message_display node is empty this function is called if not
  an alert message is displayed from within the checkUpdateCustomerStatusIFrame.xsl
*/
function doProcessUpdateCustomer(){
  var url = 'loadCustomerAccount.do?action=load_customer_account' +
      "&display_page=" + displayPageName +
      "&customer_id=" + updateCustomerAccountid +
      "&first_invoke=true" + getSecurityParams(false);
  performAction(url);
}



function doCustomerCommentsAction(){
  var url = "loadComments.do?comment_type=Customer" +
              "&page_position=1";
  performAction(url);
}

/*
      Shopping Cart specific function follow
*/
/*******************************************************************************
  Used to dynamically update the drop down list with the most current users
  viewing the page

    updateCurrentUsers(id,order_guid)
*******************************************************************************/
function updateCurrentUsers(id,order_guid){
  var url = "customerOrderSearch.do?action=refresh_ids";

  url += "&t_call_log_id=" + document.getElementById("t_call_log_id").value;
  url += "&t_entity_history_id=" + document.getElementById("t_entity_history_id").value;
  url += "&t_comment_origin_type=" + document.getElementById("t_comment_origin_type").value;
  url += "&customer_validation_required_flag=" + customerValidationRequiredFlag;
  url += "&order_validation_required_flag=" + orderValidationRequiredFlag;
  url += "&bypass_privacy_policy_verification=" + document.getElementById("bypass_privacy_policy_verification").value; 

  var src = event.srcElement;
  var iframe = document.getElementById('CSRFrame');

  if(src == null)
  {
    url += "&entity_id=" + document.csci.customer_id.value + "&entity_type=CUSTOMER" + "&end_timer=N";
  }
  else if (src.id == customerAccount )
  {
    url += "&entity_id=" + document.csci.customer_id.value + "&entity_type=CUSTOMER" + "&end_timer=Y";
  }
  else if(src.id == shoppingCart)
  {
    url += "&entity_type=ORDERS" + "&entity_id=" + orderGuid;
  }
  else
  {
    url += "&entity_id=" + id + "&entity_type=ORDER_DETAILS" + "&order_guid=" + order_guid;
  }

  iframe.src = url + "&master_order_number=" + cartMasterOrderNumber + "&skip_security=Y" + getSecurityParams(false);

}

/*******************************************************************************
* doMysteryBox()
* Handles the Search box on the header
*
*******************************************************************************/
function doMysteryBox(){
var isVisible = document.getElementById('searchBlock').style.visibility;
eventSrcId = (event.srcElement)?event.srcElement.id:'undefined';
/*If the enter button is used on a Tab, button or link do not run this function */

 if(eventSrcId != 'customerAccount' &&
     eventSrcId != 'shoppingCart' && eventSrcId.substr(0,4) != 'item' && event.srcElement.tagName != 'BUTTON'){

  if(isVisible != "hidden"){
    document.getElementById('numberEntry').focus();
  }
  if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
        return false;
  var m_box = document.getElementById('numberEntry');

  if(m_box.value == '' || m_box.value == null)
     return false;

  var url = "customerOrderSearch.do" +
      "?action=recipient_order_number_search" +
      "&order_number=" + m_box.value +
      "&order_guid=" + orderGuid +
      "&csci_current_page=" + document.csci.csci_current_page.value;

  performAction(url);
 }
}

/*******************************************************************************
  This function is activated when the user clicks the SCRUB, PENDING OR REMOVED
  link on the page. The link will appear when an order is in one of the above
  3 dispositions.
  A modal dialog window of scrub will be displayed

*******************************************************************************/
function doShowScrubAction(){
  var url = scrub_url + "?sc_mode=" + sc_mode;
  if(location != 'Scrub')
    url += "&servlet_action=result_list";
  if(location == 'Scrub')
    url += "&selected_master_order_number=" + cartMasterOrderNumber;
  url += "&sc_email_address=";
  url += "&sc_product_code=";
  url += "&sc_ship_to_type=";
  url += "&sc_origin=";
  url += "&sc_confirmation_number=" +  sc_confirmation_number;
  url += "&sc_search_type=" + sc_mode;
  url += getSecurityParams(false);

  var sURL = "incorrectOrderDisposition.html";
  var objFlorist = new Object();

  objFlorist.scrubURL = url;
  objFlorist.orderStatus = '';
  objFlorist.callingPage = 'customerShoppingCart';

   //Features
    var sFeatures  =  'dialogWidth:800px;dialogHeight:500px;dialogLeft:0px;dialogTop:90px;';
    sFeatures +=  'status:0;scroll:1;resizable:0';

    window.showModalDialog(sURL, objFlorist, sFeatures);

    var frame = document.getElementById('checkLock');
    var url = "lock.do?action_type=reset_scrub_lock&lock_id=" + document.getElementById("master_order_number").value + getSecurityParams(false);

    frame.src = url;

}

/*******************************************************************************

  Handles the Cart Level Refund button click action
  doRefundCartAction()

*******************************************************************************/
function doRefundCartAction()
{
  var refundStatus = document.getElementById('refund_status').value;
  if(refundStatus == 'Y' || refundStatus == 'y')
  {
     var refundURL = "customerOrderSearch.do?action=load_refund_codes" +
          "&master_order_number=" + document.csci.master_order_number.value +
          "&order_number=" + document.csci.master_order_number.value +
          "&order_guid=" + document.csci.order_guid.value +
          "&csci_current_page=" + document.csci.csci_current_page.value +
          "&cai_current_page=" + document.csci.cai_current_page.value +
          "&origin_id=" + document.csci.origin_id.value +
          "&customer_id=" + document.csci.customer_id.value + getSecurityParams(false);

      //Features
      var sFeatures="dialogHeight:225px;dialogWidth:360px;";
        sFeatures +=  "status:0;scroll:0;resizable:0";

      var ret = showModalDialogIFrame(refundURL, '',sFeatures);

      if(ret && ret != null && ret[0] != "EXIT")
      {
        var url = "customerOrderSearch.do?action=refund_cart&action_type=refund_cart";
            url += "&entity_id=" + document.csci.master_order_number.value;
        document.csci.refund_disp_code.value = ret[0];
        document.csci.refund_display_type.value = ret[1];
        document.csci.responsible_party.value = ret[2];
        document.csci.full_refund_indicator.value = ret[3];

        performAction(url);
      }
      else
      {
        var frame = document.getElementById('refundCartFrame');
        var surl =  "customerOrderSearch.do?action=refund_release_lock" +
            "&master_order_number=" + document.csci.master_order_number.value +
            "&order_number=" + document.csci.master_order_number.value +
            "&entity_type=PAYMENTS&entity_id=" + document.csci.order_guid.value;

      surl += getTimerParams();
        frame.src = surl += getSecurityParams(false);

      }
    }
}

/*******************************************************************************
 This function populates an iframe with refund inforamtion
 the inforamtion is used to either display error messages or
 call the doRefundCartAction()
 (see checkRefundsIFrame.xsl for further information)
*******************************************************************************/
function  doCheckRefundStatus(){
  var frame = document.getElementById('refundCartFrame');
if(doExitPageAction(WARNING_MSG_7)){
  frame.src = "customerOrderSearch.do?action=retrieve_refund_lock" +
              "&master_order_number=" + document.csci.master_order_number.value +
              "&order_number=" + document.csci.master_order_number.value +
              "&order_guid=" + document.csci.order_guid.value +
              "&csci_current_page=" + document.csci.csci_current_page.value +
              "&cai_current_page=" + document.csci.cai_current_page.value +
              "&customer_id=" + document.csci.customer_id.value +
              "&entity_type=PAYMENTS&entity_id=" + document.csci.order_guid.value +
              "&order_level=ORDERS" + getSecurityParams(false);

  }
}
/*
 Executed when the Update Email Status button on the
 customer shopping cart is clicked.
*/
function doUpdateEmailStatus(){
  var modalUrl = 'updateCustomerEmail.do?action=more_customer_emails' +
     getSecurityParams(false);

  var modalFeatures  =  'dialogWidth:800px;dialogHeight:445px;center:yes;status:no;help:no;';
      modalFeatures +=  'resizable:no;scroll:no';
  modalUrl += retrievePopupParms();
  showModalDialogIFrame(modalUrl, "", modalFeatures);
}

/*
Retrieves the fields based on the fieldParms
array defined at the top of the shopping cart page.
*/
function retrievePopupParms(){
  var url = '';
  var ele;
  for(var i = 0; i < fieldParms.length; i++){
    ele = document.getElementById(fieldParms[i]);
    url += '&uc_' + ele.name + '=' + ele.value;
  }
 return url;
}

/***********************************************************************
* Executed when the More..
* link is clicked.
*
*   showMoreAction(guid,oNumber)
*
***********************************************************************/
function showMoreAction(){
  var url = "customerOrderSearch.do?action=csc_more" +
      "&order_guid=" + orderGuid +
      "&csci_current_page=" + document.csci.csci_current_page.value +
      "&order_number=" + cartMasterOrderNumber +
      "&csci_order_position=" + document.csci.csci_order_position.value;

 performAction(url);
}

/**********************************************************************
* Executed when the Previous...
* link is clicked.
*
*   showPreviousAction()
*
***********************************************************************/
function showPreviousAction(){
  var url = "customerOrderSearch.do?action=csc_previous" +
    "&order_guid=" + orderGuid +
    "&csci_current_page=" + document.csci.csci_current_page.value +
    "&order_number=" + cartMasterOrderNumber +
    "&csci_order_position=" + document.csci.csci_order_position.value;

  performAction(url);
}

/*******************************************************************************
  Executes when the Payment Information
  link on the customerShoppingCart.xsl is clicked

    doCustomerPaymentInfo()

*******************************************************************************/
function doCustomerPaymentInfo(){
  var url = "customerOrderSearch.do" +
    "?action=shopping_cart_payment_info_lookup" +
    "&pay_info_number=" + orderGuid + getSecurityParams(false);
  var sFeatures="dialogHeight:275px;dialogWidth:740px;";
      sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";

  showModalDialogIFrame(url, "" ,sFeatures);
}

/*******************************************************************************
  Executes when the source code link is clicked
  This will be triggerd on the Customer Shopping Cart page and the itemDiv.xsl

    doSourceCodeAction(id)

*******************************************************************************/
function doSourceCodeAction(id){
  var url = "customerOrderSearch.do?action=source_code_lookup" +
      "&source_code=" + id + getSecurityParams(false);
  var sFeatures="dialogHeight:280px;dialogWidth:730px;";
    sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";

  showModalDialogIFrame(url,"",sFeatures);
}


/*******************************************************************************
* Executed when the Membership Information
* link is clicked on the itemDiv.xsl
*
* doMemberShipInfoAction()
*
*******************************************************************************/
function doMemberShipInfoAction(id){
 if(id != ''){
    var url = "customerOrderSearch.do?action=membership_info_lookup" +
      "&membership_id=" + membership_id + "&order_detail_id=" + id + getSecurityParams(false);
    var sFeatures="dialogHeight:240px;dialogWidth:700px;";
        sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";
     showModalDialogIFrame(url, "" ,sFeatures);
   }
}


/*******************************************************************************
  Executes when the Payment Information
  link on the itemDiv.xsl is clicked

    doRecipPaymentInfo(detail_id)

*******************************************************************************/
function doRecipPaymentInfo(detail_id){
  var url = "customerOrderSearch.do?" +
      "action=recipient_payment_info_lookup" +
      "&pay_info_number=" + detail_id + getSecurityParams(false);
  var sFeatures="dialogHeight:275px;dialogWidth:740px;";
    sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";


 showModalDialogIFrame(url, "" ,sFeatures);
}

/******************************************************************************
  This function is executed when the recipient link from the itemDiv page
  is clicked.
*******************************************************************************/
function doRecipientIdAction(recip_id){
  var url = "customerOrderSearch.do?action=recipient_search" +
      "&cai_start_position=1" + "&customer_id=" + recip_id + getSecurityParams(false);
  performAction(url);
}

/*******************************************************************************
  This function is executed when there is
  Alternate Contact Information avaible for a recipient on itemDiv.xsl

      doAlternateContactAction()

*******************************************************************************/
function doAlternateContactAction(){
  var sURL = "alternateContactPopup.html";
  var objFlorist = new Object();

  var securitytoken = document.forms[0].securitytoken.value;
  var context = document.forms[0].context.value;
  var applicationcontext = document.forms[0].applicationcontext.value;

  objFlorist.first_name = alt_first_name;
  objFlorist.last_name = alt_last_name;
  objFlorist.email = alt_email;

  //remove any elements that may appear in the phon number string
  var bag = '()- ';
  var formattedPhone = stripCharsInBag(alt_phone, bag);

  if( isUSPhoneNumber(formattedPhone,false) ){
      objFlorist.phone = reformatUSPhone(formattedPhone);  //format the US phone numbers
  }else{
      objFlorist.phone = formattedPhone; //if it is not a US phone simply display digits
  }
  objFlorist.ext = alt_ext;
  var sFeatures="dialogHeight:225px;dialogWidth:700px;";
      sFeatures +=  "status:0;scroll:0;resizable:0";
   showModalDialogIFrame(sURL, objFlorist, sFeatures);
 }

/******************************************************************************
  This function is executed when the Print Order button on the itemDiv.xsl
  is clicked.
    doPrintOrderAction()

*******************************************************************************/
function doPrintOrderAction(id){

  var url = '';
  var modalUrl = "confirm.html";
  var modalArguments = new Object();

  modalArguments.modalText = 'Would you like to include comments?';

  var modalFeatures  =  'dialogWidth:210px;dialogHeight:160px;center:yes;status:no;help:no;';
      modalFeatures +=  'resizable:no;scroll:no';

  var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  var url = "customerOrderSearch.do?action=print_order" +
      "&order_detail_id=" + id;
  if(ret){
    url += "&include_comments=Y";
  }else{
    url += "&include_comments=N";
  }
 callWinAPIPrint(url);
}
/*******************************************************************************
  Helper function for doPrintOrderAction.
*******************************************************************************/
function callWinAPIPrint(url){
  var frame = document.getElementById('refundCartFrame');
  frame.src = url += getSecurityParams(false) + getTimerParams();
}

/*
        Below are the functions which are executed from the buttons on
        the itemDiv.xsl page. (Recipient/Order Information)
*/

/*******************************************************************************
    doOrderCommentsAction(detail_id, bypass_com)
********************************************************************************/
function doOrderCommentsAction(detail_id, bypass_com){
  var url = "loadComments.do?comment_type=Order" +
      "&page_position=1" + "&order_detail_id=" + detail_id +
      "&bypass_com=" + bypass_com;
  performAction(url);
}

/*******************************************************************************

  doOrderEmailAction()

*******************************************************************************/
function doOrderEmailAction(id, elementId){
  var indicator = document.getElementById(elementId).email;
  if(indicator == '' || indicator == null){
    alert('There is no email address on file for this customer.');
    return;
  }
  var url = "loadSendMessage.do?action_type=load_titles&message_type=Email";
    url += "&order_detail_id=" + id;

  performAction(url);
}

/*******************************************************************************

    doOrderLettersAction()

*******************************************************************************/
function doOrderLettersAction(id){
  var url = "loadSendMessage.do?action_type=load_titles&message_type=Letter";
    url += "&order_detail_id=" + id;

  performAction(url);
}

/*******************************************************************************

    doOrderHistoryAction(detail_id)

*******************************************************************************/
function doOrderHistoryAction(detail_id){
  var url = "loadHistory.do?page_position=1" +
  "&history_type=Order" + "&order_detail_id=" + detail_id;

  performAction(url);
}

/*******************************************************************************

    doOriginalViewAction()

*******************************************************************************/
function doOriginalViewAction(display_order_number){
  //even though this is for original view we can reuse the
  //refund cart ifame
 document.csci.display_order_number.value = display_order_number;
 var frame = document.getElementById('refundCartFrame');
 var url = 'originalView.do?'
    + "&master_order_number=" + document.csci.master_order_number.value
    + "&display_order_number=" + document.csci.display_order_number.value
    + "&action=original_view_check" + getSecurityParams(false);
 frame.src = url;

}
/*******************************************************************************

  doProcessOriginalView()

*******************************************************************************/
function doProcessOriginalView(){
  var process = document.getElementById('process_original_view').value;
  if(process == ''){
      var url = 'originalView.do?'
      + "&master_order_number=" + document.csci.master_order_number.value
      + "&display_order_number=" +document.csci.display_order_number.value
      + "&action=original_view_load";
    performAction(url);
  }else{
    return;
  }
}
/*******************************************************************************
  Button Actions on customerShoppingCart page

    doFloristInquiry()

*******************************************************************************/
function doFloristInquiry(){
  var sURL = "incorrectOrderDisposition.html";
  var objFlorist = new Object();

  var   url = document.forms[0].load_member_data_url.value;
        url += "action=loadFloristSearch" + getSecurityParams(false);

  objFlorist.scrubURL = url;
  objFlorist.orderStatus = '';
  objFlorist.callingPage = 'customerShoppingCart';
  //Features
  var sFeatures="dialogHeight:500px;dialogWidth:800px;";
    sFeatures +=  "status:1;scroll:0;resizable:0";

  window.showModalDialog(sURL, objFlorist, sFeatures);
}

/*******************************************************************************
* Executed by button click on itemDiv.xsl
*
* doCommunicationAction(id, bypassCOM)
*
*******************************************************************************/
function doCommunicationAction(id, bypassCOM){
  var url = "displayCommunication.do?" + "&order_detail_id=" + id + 
            "&bypass_com=" + bypassCOM;

  performAction(url);
}


/*******************************************************************************

    doTagOrderAction()

*******************************************************************************/
function doTagOrderAction(tag,detailId){
  if(tag == '2Y' || tag == '2N'){
    alert('You cannot tag this order, there are two users already tagged');
    return;
  }
  if(tag == '1Y'){
   alert('You already have this order tagged');
   return;
  }
 var url = "tagOrder.do?order_detail_id=" + detailId   + getSecurityParams(false);
  var sFeatures="dialogHeight:525px;dialogWidth:800px;";
    sFeatures +=  "status:1;scroll:0;resizable:0";

  showModalDialogIFrame(url,"" , sFeatures);
}

function doLoadCartEmail(){
  var form = document.forms[0];
    var url = 'cartEmailAction.do?action_type=load&page_position=1&order_guid=' + form.order_guid.value + getSecurityParams(false);
    var sFeatures="dialogHeight:425px;dialogWidth:700px;";
    sFeatures +=  "status:1;scroll:0;resizable:0";
    var ret = showModalDialogIFrame(url,"" , sFeatures);

    document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=ORDERS&lock_id="+document.forms[0].order_guid.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;

    if(ret && ret != null && ret[0] == "REFRESH")
    {
      form.external_order_number.value = "";
        var loadUrl =  "customerOrderSearch.do?action=customer_search&order_number=" +form.master_order_number.value+ "customer_id=" + form.customer_id.value + "&last_order_date=Recipient";
        performAction(loadUrl);
    }
}


/*******************************************************************************

    doReconciliationAction()

*******************************************************************************/
function doReconciliationAction(url,id,eon){
    url += '?cbr_action=load&order_detail_id=' + id + '&external_order_number=' + eon;
  performAction(url);
}

/*
 *  doViewBillingTrans()
 */
function doViewBillingTrans(){
  if(view_billing_url != ''){
    performAction(view_billing_url += '?cbr_action=load');
  }

}
/*******************************************************************************

    doOrderRefundAction()

*******************************************************************************/
function doOrderRefundAction(id, origin_id, product_type, ship_method, vendor_flag, order_guid)
{

  var url = "loadRefunds.do?order_detail_id=" + id 	+
  					"&origin_id=" + origin_id 							+
  					"&product_type=" + product_type 				+
  					"&ship_method=" + ship_method						+
  					"&order_guid=" + order_guid							+
  					"&vendor_flag=" + vendor_flag;

  performAction(url);
}

/*******************************************************************************

    doLoadOrderReceipt()

*******************************************************************************/
function doLoadOrderReceipt(){

    if(document.getElementById("origin_id").value.indexOf('BULK') >= 0)
    {
      alert("Receipts unable to be generated for Bulk orders.");
    }
    else if(document.getElementById("origin_id").value.indexOf('AMZNI') >= 0)
    {
      alert("Receipts unable to be generated for Amazon orders.");
    }
    else if(document.getElementById("origin_id").value.indexOf('WLMTI') >= 0)
    {
      alert("Receipts unable to be generated for Walmart orders.");
    }
    else if(document.getElementById("receipt_avail_scrub_flag").value == "N")
    {
      alert("One or more of the orders associated with this shopping cart are in scrub or pending. A receipt cannot be printed until all orders are out of scrub or pending.");
    }
    else if(document.getElementById("receipt_avail_remove_flag").value == "N")
    {
      alert("All of the orders associated with this shopping cart are removed. A receipt cannot be printed.");
    }
    else
    {
      var url = "orderReceipt.do?origin_id=" + document.getElementById("origin_id").value + " &company_id=" +document.getElementById("company_id").value+ "&company_name=" + document.getElementById("company_name").value + "&order_guid=" + document.getElementById("order_guid").value + "&customer_id=" + document.getElementById("customer_id").value + "&start_origin=" + document.getElementById("start_origin").value + "&call_dnis=" + document.getElementById("call_dnis").value + getSecurityParams(false);

        var sURL = "letterPreview.html";
    var objFlorist = new Object();
    objFlorist.letterURL = url;

    var sFeatures="dialogHeight:500px;dialogWidth:800px;status:1;scroll:0;resizable:0";
    window.showModalDialog(sURL, objFlorist, sFeatures);

        //var sURL = "orderReceipt.do?origin_id=" + document.getElementById("origin_id").value + " &company_id=" +document.getElementById("company_id").value+ "&company_name=" + document.getElementById("company_name").value + "&order_guid=" + document.getElementById("order_guid").value + "&customer_id=" + document.getElementById("customer_id").value + "&start_origin=" + document.getElementById("start_origin").value + "&call_dnis=" + document.getElementById("call_dnis").value + getSecurityParams(false);
      //window.showModalDialog(url, objFlorist, sFeatures);

      //var sURL = "letterPreview.html";
    //var objFlorist = new Object();
    //objFlorist.letterURL = url;

    //var sFeatures="dialogHeight:500px;dialogWidth:800px;status:1;scroll:0;resizable:0";
    //window.showModalDialog(sURL, objFlorist, sFeatures);

      //performAction(url);
    }
}

/*******************************************************************************

    doOrderHoldAction()

*******************************************************************************/
function doOrderHoldAction(){
  alert('Button under construction');
}

/*******************************************************************************
This function opens the Update Order Info page.
doUpdateOrderInfoAction()

*******************************************************************************/
function doUpdateOrderInfoAction(){
  var url = 'loadDeliveryInfoAction.do?order_detail_id=' + updateOrderDetailID +
            '&customer_id=' + document.getElementById('customer_id').value + getSecurityParams(false);

  performAction(url);

}



/*******************************************************************************
  doFloristMaintenance(florist_id)

 Display Florist Maintentace screen
*******************************************************************************/
function doFloristMaintenance(florist_id){
  var sURL = "incorrectOrderDisposition.html";
  var objFlorist = new Object();

  /* build url used to access load member data */
   var  url = document.forms[0].load_member_data_url.value;
         url += "action=searchFlorist" + "&sc_floristnum=" + florist_id + getSecurityParams(false);

  objFlorist.scrubURL = url;
  objFlorist.orderStatus = '';
  objFlorist.callingPage = 'customerShoppingCart';

 //Features
           var sFeatures="dialogHeight:500px;dialogWidth:800px;";
          sFeatures +=  "status:0;scroll:1;resizable:0";

  window.showModalDialog(sURL, objFlorist, sFeatures);
}


/*
  Retrieve Timer parms
  Used to attach them to a URL
*/
function getTimerParams(){
  var timerParms = '';
    timerParms += "&t_call_log_id=" + document.getElementById("t_call_log_id").value;
    timerParms += "&t_entity_history_id=" + document.getElementById("t_entity_history_id").value;
    timerParms += "&t_comment_origin_type=" + document.getElementById("t_comment_origin_type").value;

 return timerParms;

}

/*
 * This function is used to display the membership info
 * on the customer account page
 * doMembershipDisplay(membershipObj)
 */
function doMembershipDisplay(array){
  var url = '';
  var modalUrl = "membershipInfoDisplayIFrame.html";
  var modalArguments = new Object();
  modalArguments = array;
  var modalFeatures  =  'dialogWidth:750px;dialogHeight:220px;center:yes;status:no;help:no;';
      modalFeatures +=  'resizable:no;scroll:no';

  var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

}




/*

function imgUnPressed(val){
    var img = event.srcElement;
    if(!img.disabled)
      img.src = imgArray[val].src;
}
function imgPressed(val){
    var img = event.srcElement;
    if(!img.disabled)
       img.src = imgPressedArray[val].src;
}
*/

function Trim(value)
{
  return value.replace(/^\s+/,'').replace(/\s+$/,'');
}





/*
The following functions are used on the multipleRecordsFound.xsl
and the lastRecordsAccessed.xsl pages. They are used to calculate the
row numbers in order for the search box on the hearder to function appropriatley
*/
/*
Find the row based on csr entry in the search box.
*/
function doRowCalculation(searchValue){
   var val = stripZeros(searchValue);
   var num = val;
   var page = cPage;
   if((cPage > 1 ) ){
       page--;
       var max_recs =
       document.forms[0].max_records_allowed.value;
       if(max_recs == '')
      max_recs = 50;
    var num = (val - (page * max_recs));
  }
   return num;
}
/*
Remove any leading zeros from the search number
*/
function stripZeros(num){
   var num,newTerm
   while (num.charAt(0) == "0") {
         newTerm = num.substring(1, num.length);
         num = newTerm;
   }
   if (num == "")
       num = "0";
       return num;
}

/*
 This function is executed when the csr uses the search box. If the csr hits the 'enter' key
 the associated td with the same id will be located and the customer id is grabbed from the
 hidden field and a search is performed.
*/
function doEntryAction(){
var eve = event.srcElement.value;
   if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
    return false;
   var eve = event.srcElement.value;
    /*
      I have added the below check in order to use this fuction
      with the Update Customer Account.
      When attempting to use this in conjunction with a button click
      eve had the value of html, which was causing problems
     */
   if(!isInteger(eve))
    eve = document.getElementById('numberEntry').value;
   if(isInteger(eve)){
       var searchNumber = doRowCalculation(eve);
       var td = document.getElementById('td_' + searchNumber);
       if(td == null){
     alert('Invalid line number entered.' + '\n' +
       'Please only enter a line number that is currently being displayed on the page.');
       }else{
      var id = td.childNodes(1).value;
            doSearchAction(id,searchNumber);
       }
    }
    else{
      alert('Please enter a valid line number');
    }
}

/*
This function implements the checkLockIFrame.xsl this will either display a message
to the user and set the isRecordLocked variable to false or isRecordLocked to true
*/
function checkRecordLock(appName){
  var frame = document.getElementById('checkLock');
  var url = "lock.do?action_type=retrieve&lock_id=" + document.getElementById("order_detail_id").value
          + "&lock_app="+appName + getSecurityParams(false);
  frame.src = url;
  return (isRecordLocked);
}
/*
   This function will obtain the lock on the record
*/
function doRecordLock(appName,action,forwardAction, formName){
  CANCEL_FORM_NAME = formName;
  var frame = document.getElementById('checkLock');
  var url = "lock.do?action_type=" + action +
            "&lock_id=" + document.getElementById("order_detail_id").value +
            "&lock_app="+appName +
            "&forward_action=" + forwardAction +
            getSecurityParams(false);

  if(forwardAction == 'updatePayment') {
    url += "&order_level=ORDER_DETAILS"; //used if from item pages
  }
  if(forwardAction == 'updateBilling') {
    url += "&order_level=ORDERS"; //used if from cart page
  }

  frame.src = url;
}


/*
 GUID based record lock for Cart level
*/
function doGUIDRecordLock(appName,action,forwardAction, formName){
  CANCEL_FORM_NAME = formName;
  var frame = document.getElementById('checkLock');
  var url = "lock.do?action_type=" + action +
            "&lock_id=" + document.getElementById("order_guid").value +
            "&lock_app="+ appName +
            "&forward_action=" + forwardAction +
            getSecurityParams(false);

  if(forwardAction == 'updatePayment') {
    url += "&order_level=ORDER_DETAILS"; //used if from item pages
  }
  if(forwardAction == 'updateBilling') {
    url += "&order_level=ORDERS"; //used if from cart page
  }

  frame.src = url;
}


/* Populates a select box containing states using the supplied parameters.  If 'selectedState'
 * whitespace only or undefined, the select box will default to the first option.
 *
 * @param countrySelectBoxName  The name of the select box containing a list of countries
 * @param stateSelectBoxName    The name of the select box containing a list of states
 * @param selectedState         The value to set on the stateSelectBoxName.  Example: 'IL'
 */
var ORIG_COUNTRY;
function populateStates(countrySelectBoxName, stateSelectBoxName, selectedState)
{
  var countryBox = document.getElementById(countrySelectBoxName);
  var selectedCountry = countryBox.options[countryBox.selectedIndex].value;
  var toPopulate = (selectedCountry == "US") ? usStates : (selectedCountry == "CA") ? caStates : (selectedCountry == "VI") ? viStates : (selectedCountry == "PR") ? prStates : naStates;

  ORIG_COUNTRY = selectedCountry;

  var states = document.getElementById(stateSelectBoxName);
  states.options.length = 0;
  for (var i = 0; i < toPopulate.length; i++){
    states.add(new Option(toPopulate[i][1], toPopulate[i][0], false, false));
  }
  setSelectedState(states, selectedState);
}

/* Sets a select box containing states to the 'state' supplied.  If 'selectedState'
 * whitespace only or undefined, the select box will default to the first option.
 *
 * @param selectBoxObj  The select box object containing a list of states
 * @param state         The value to set on the stateSelectBoxName.  Example: 'IL'
 */
function setSelectedState(selectBoxObj, state){
  if ( !isWhitespace(state) ) {
    for (var i = 0; i < selectBoxObj.length; i++) {
      if (selectBoxObj[i].value == state) {
        selectBoxObj.selectedIndex = i;
        selectBoxObj.options[i].defaultSelected = true;
        break;
      }
    }
  }
}



/* Validates the form using the supplied multi-dimensional array.
 * The array values should be as follows:
 *
 * 0 = name attribute of input
 * 1 = the error message to be displayed
 * 2 = the function name used to perform validation
 * 3 = any additional function params needed to perform validation
 *
 * @param matrix  The multi-dimensional array used to validate the form.
 */
var INPUT = 0;
var MESSAGE = 1;
var FUNCTION = 2;
var FUNCTION_PARAMS = 3;
var PREV_ERRORS = new Array();
function validateForm(matrix){
  validateFormResetPrevErrors();

  var errorInput, errorRow, errorCell, isValid = true;
  for (var i = 0; i < matrix.length; i++) {
    errorInput = document.getElementById(matrix[i][INPUT]);
    errorCell = document.getElementById(matrix[i][INPUT] + "_validation_message_cell");
    errorRow = document.getElementById(matrix[i][INPUT] + "_validation_message_row");

    // loop through array calling validation functions
    if ( !matrix[i][FUNCTION](matrix[i][INPUT], matrix[i][FUNCTION_PARAMS]) ) {
      PREV_ERRORS.push(matrix[i][INPUT]);
      errorInput.className = "errorField";
      if ( matrix[i][MESSAGE] != "" ) {
        errorCell.innerText = matrix[i][MESSAGE];
      }
      errorRow.style.display = "block";
      isValid = false;
    }
  }
  return isValid;
}

/* This method is to assist with Server side validation error messages.  The goal was to
 * reuse validateForm() so a generic pass-thru function was needed.
 *
 * @param input     the input name attribute
 * @param condition true or false
 */
function validateFormHelper(input, condition){
  return condition;
}

/* Resets any fields which were invalid when calling validateForm.  This is typically called
 * before calling validateForm() for the second time as to reset the error inputs.
 */
function validateFormResetPrevErrors(){
  for (var i = 0; i < PREV_ERRORS.length; i++) {
    // reset pink background
    errorInput = document.getElementById(PREV_ERRORS[i]);
    errorInput.className = "";

    // hide error message
    errorCell = document.getElementById(PREV_ERRORS[i] + "_validation_message_cell");
    errorRow = document.getElementById(PREV_ERRORS[i] + "_validation_message_row");
    errorRow.style.display = "none";
  }
  PREV_ERRORS = new Array();
}

/* Determines if the supplied input is a valid US or international
 * phone number.  The emptyOk default is false.  See FormChek.js.
 *
 * @param inputName The input name to validate.
 * @param emptyOk   true if empty values are valid, false if they are not
 * @return true if valid US or international phone number, otherwise false
 */
function validatePhone(inputName, emptyOk){
  emptyOk = (validatePhone.arguments.length == 1) ? false : emptyOk;
  var phone = document.getElementById(inputName);
  return checkUSPhone(phone, emptyOk) || checkInternationalPhone(phone, emptyOk);
}

/* Validates for valid US, Candian, or International zip codes.
 * Valid Formats:
 *    US - NNNNN{-NNNN}
 *    CA - ANA-NAN || ANA
 *    Inter - NNNNNNNNNNNN
 *
 * The emptyOk default is false.
 *
 * @param inputName The input name to validate.
 * @param emptyOk   true if empty values are valid, false if they are not
 * @return true if the zip code is a valid US, Candian, or international zip
 *         code, otherwise false
 */
function validateZipCode(inputName, countryList, emptyOk){
  emptyOk = (validateZipCode.arguments.length != 3) ? false : emptyOk;
  var zipCode = document.getElementById(inputName).value;
  var countryBox = document.getElementById(countryList);
  var selectedCountry = countryBox.options[countryBox.selectedIndex].value;
  var isDomestic = ( selectedCountry == 'US' || selectedCountry == 'CA' );

  var re = /^\d{5}([\-])?(\d{4})?$/;
  var can = /^\s*[a-ceghj-npr-tvxy]|[A-CEGHJ-NPR-TVXY]\d[a-z]|[A-Z](\s)?\d[a-z]|[A-Z]\d\s*$/i;
  var inter = /[a-z]|[A-Z]|[0-9](\s+)?/;

  if( isDomestic ) {
    return ( re.test(zipCode) || can.test(zipCode) );
  }
  return  inter.test(zipCode) || isWhitespace(zipCode);
}


/* Determines if any of the form's default values have changed.  Any tags that should
 * not be checked can use the attribute 'onchangeIgnore=true' to bypass the check.
 *
 * @return true if any of the form's default values have changed, otherwise false
 * @see hasInputChanged(obj)
 */
function hasFormChanged(){
  var hasFormBeenPreviouslyModified = document.getElementById("modify_order_updated");
  var hasChanged = ( hasFormBeenPreviouslyModified && hasFormBeenPreviouslyModified.value == "y" );

  if ( !hasChanged ) {
    // define tags to test
    var inputTypes = new Array("input", "select", "textarea");
    for (var k = 0; k < inputTypes.length && !hasChanged; k++){

      // loop through all tags of a specific type
      var inputs = document.getElementsByTagName(inputTypes[k]);
      for (var i = 0; i < inputs.length && !hasChanged; i++){
        // skip tags with attribute: 'onchangeIgnore=true'
        if ( !inputs[i].onchangeIgnore ){
 		      // unformat tags with attribute: 'onchangeUnformat=true'
       		if (inputs[i].onchangeUnformat)
       		{
						var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
						inputs[i].value = stripWhitespace(inputs[i].value);
						inputs[i].value = stripCharsInBag(inputs[i].value, bag);
       		}
      		hasChanged = hasInputChanged(inputs[i]);
        }
      }
    }
  }
  return hasChanged;
}

/* Determines if an input's default value has changed.
 *
 * @param input The object to test
 * @return true if the input's value is different from the default value, otherwise false
 */
function hasInputChanged(input){
  var inputType = input.type.toString();
  if ( inputType == "text" || inputType == "textarea" || inputType == "hidden" ) {
    if ( input.defaultValue != input.value ){
      return true;
    }
  }
  else if ( inputType.indexOf("select") > -1) {

    if (input.options.length > 1) {
      var defaultSelected, found = false;
      for ( var i = 0; i < input.options.length && !found; i++){
        if ( input.options[i].defaultSelected ) {
          defaultSelected = i;
          found = true;
        }
      }
      // no default was set so see if the first option is still selected
      if ( !found && !input[0].selected ) {
        return true;
      }
      // default was found, check to see if it matches the current selection
      else if ( found && defaultSelected != input.selectedIndex ) {
        return true;
      }
      else {
        return false;
      }
    }
    // select box contains no options, just return false
    else {
      return false;
    }
  }
  else if ( inputType == "checkbox" || inputType == "radio") {
    if (input.defaultChecked != input.checked) {
      return true;
    }
  }
  return false;
}


/**
 * Returns all the form elements in query string format.
 * ex.  ?input1=value1&input2=value2
 *
 * @param formObj The form object to gather name, value pairs for
 */
function getFormNameValueQueryString(formObj){
   var temp, toReturn = "";
   var inputs = formObj.elements;
   for (var i = 0; i < inputs.length; i++){
      temp = getNameValuePair(inputs[i]);
      if ( temp != "" ) {
         if ( i == 0 ) {
            toReturn += "?";
         }
         else {
            toReturn += "&";
         }
         toReturn += temp;
      }
   }
   return toReturn;
}

/**
 * Returns the input name and value attributes delimited by the
 * equals sign (=).
 *
 * @param input The input object to get the name and value attributes of
 */
function getNameValuePair(input){
   var inputType = input.type.toString();
   if ( inputType == "text" || inputType == "textarea" || inputType == "hidden" ) {
      return input.name + "=" + input.value;
   }
   else if ( inputType.indexOf("select") > -1) {
      if ( input.selectedIndex > -1 )  return input.name + "=" + input[input.selectedIndex].value;
      else                             return "";
   }
   else if ( inputType == "checkbox" || inputType == "radio") {
      if ( input.checked ) return input.name + "=" + input.value;
      else                 return "";
   }
   else {
      return "";
   }
}

/*
 * General function used to cancel orders.
 * @param formName The form name that should perform the final submit
 */
var CANCEL_FORM_NAME = "";
function baseDoCancelUpdateAction(formName){
  CANCEL_FORM_NAME = formName;
  var url = "cancelUpdateOrder.do"
            + "?external_order_number=" + document.getElementById("external_order_number").value
            + "&order_guid=" + document.getElementById("order_guid").value
            + "&customer_id=" + document.getElementById("customer_id").value
            + "&master_order_number=" + document.getElementById("master_order_number").value
            + "&order_detail_id=" + document.getElementById("order_detail_id").value
            + getSecurityParams(false);

  var frame = document.getElementById("cancelUpdateFrame");
  frame.src = url;
}

/*
 * Post action called by the iframe to perform the redirect.
 */
function baseDoCancelUpdatePostAction(eon, guid, cust_id){
  showWaitMessage("mainContent", "wait", "Canceling");
  var url = "customerOrderSearch.do?action=customer_account_search"
            + "&order_number=" + eon
            + "&order_guid=" + guid
            + "&customer_id=" + cust_id
            + getSecurityParams(false);

  var isOrderModified = document.getElementById("modify_order_updated");
  if ( isOrderModified != null ) {
    isOrderModified.value = "n";
  }
  var toSubmit = document.getElementById(CANCEL_FORM_NAME);
  toSubmit.action = url;
  if (typeof siteName != "undefined" && typeof applicationContext != "undefined") {
  toSubmit.action = "http://" + siteName + applicationContext + "/" + url;
  }
  toSubmit.submit();
}

/*
 * Post action called when a cancel action fails on the server.
 */
function baseDoCancelUpdatePostErrorAction(url) {
  if ( url.indexOf("/") == 0 ) {
    url = url.substring(1);
  }
  var toSubmit = document.getElementById(CANCEL_FORM_NAME);
  toSubmit.action = url + getSecurityParams(true);
  if (typeof siteName != "undefined" && typeof applicationContext != "undefined") {
  toSubmit.action = "http://" + siteName + applicationContext + "/" + url + getSecurityParams(true);
  }
  toSubmit.submit();
}

/*
 *  Show a modal dialog with an Iframe and put the url in the
 *  Iframe src.  This allows the url to go through an XSLT transform.
 */
function showModalDialogIFrame(url,vArguements,sFeatures)
{
  var iframeURL = "modalDialog.html";
  var imbeddedURL = new Object();
  imbeddedURL.dialogURL = url;

  return window.showModalDialog(iframeURL, imbeddedURL ,sFeatures);
}


/**************************************************************************************************
 *  Show a modal dialog for PPV
 **************************************************************************************************/
function displayPrivacyPolicy(customer_id, customerValidationRequiredFlag, order_detail_id, orderValidationRequiredFlag)
{
  var url_source = "customerOrderSearch.do" + "?action=load_privacy_policy" + 
                   "&customer_id=" + customer_id + 
                   "&order_detail_id=" + order_detail_id + 
                   "&t_call_log_id=" + document.getElementById("t_call_log_id").value + 
                   "&t_entity_history_id=" + document.getElementById("t_entity_history_id").value + 
                   "&t_comment_origin_type=" + document.getElementById("t_comment_origin_type").value + 
                   "&customer_validation_required_flag=" + customerValidationRequiredFlag + 
                   "&order_validation_required_flag=" + orderValidationRequiredFlag + 
                   "&bypass_privacy_policy_verification=" + document.getElementById("bypass_privacy_policy_verification").value + 
                   getSecurityParams(false);

  //Modal_URL
  var modalUrl = "modalDialog.html";

  //Modal_Arguments
  var modalArguments = new Object();
  modalArguments.dialogURL = url_source;
  modalArguments.title = "Privacy Policy Verification";


  //Modal_Features
  var modalFeatures = 'dialogWidth:800px; dialogHeight:600px; center:yes; status=no; help=no; resizable:no; scroll:yes';


  //show the modal dialog.
  var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  if(!ret || ret == null)
  {
    displayPrivacyPolicy(customer_id, customerValidationRequiredFlag, order_detail_id, orderValidationRequiredFlag);
  }
  else
  {
    if (ret[0] == 'cos')
    {
      var url = "customerOrderSearch.do" + "?action=load_page";
      performAction(url);
    }
    else if (ret[0] == 'cai')
    {
      showItemAt(document.getElementById('customerAccount')); 
    }
  }


}


/**************************************************************************************************
 * This is a generic method to call the alertDynamic.html
 * This html was created as part of PPV and will display a modal popup where the calling function 
 * can send in the access key, button text, text, text alignment option, and the title
 **************************************************************************************************/
function showAlertDynamicDialog(accessKeySent, buttonTextSent, modalTextSent, textAlignSent, titleSent)
{

  //Modal_URL
  var modalUrl = "alertDynamic.html";

  //Modal_Arguments
  var modalArguments = new Object();
  modalArguments.accessKeySent = accessKeySent;
  modalArguments.buttonTextSent = buttonTextSent;
  modalArguments.modalTextSent = modalTextSent;
  modalArguments.textAlignSent = textAlignSent;
  modalArguments.titleSent = titleSent;

  //Modal_Features
  var modalFeatures = 'dialogWidth:350px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';


  //show the modal dialog.
  var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

}



function convertHtmlEntity(input) {
    var div = document.createElement('div');
    div.innerHTML = input;
    return div.innerHTML;
}


function replaceText(el, text) {
  if (el != null) {
    clearText(el);
    var newNode = document.createTextNode(text);
    el.appendChild(newNode);
  }
}

function clearText(el) {
  if (el != null) {
    if (el.childNodes) {
      for (var i = 0; i < el.childNodes.length; i++) {
        var childNode = el.childNodes[i];
        el.removeChild(childNode);
      }
    }
  }
}