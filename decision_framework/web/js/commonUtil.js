/*
 * Common warning messages.
 */
var WARNING_MSG_4						= "Are you sure you want to exit without saving your changes?";
var WARNING_MSG_7						= "Are you sure you want to refund the entire shopping cart?";

var WARNING_INT_2_DMSTC						= "Changing from a Domestic address to an international address will require you to change the product and maybe the delivery date.  Do you wish to change the product?";
var WARNING_DMSTC_2_INT						= "Changing from an International Order to a Domestic order will require you to choose a different product.  Do you wish to change the product?";
var WARNING_WLMTI_PRODUCT					= "The product cannot be changed on Wal-Mart Orders.. If you wish to continue you will have to cancel this order and replace a new order with source code 9849.";
var WARNING_WLMTI_ORDER						= "Wal-Mart orders cannot be updated."
var WARNING_AMZNI_ORDER 					= "Amazon orders cannot be updated."
var WARNING_ARIBA_ORDER 					= "Ariba orders cannot be updated."
var WARNING_FRAUD 						= "Order currently in the LP queue and cannot be updated.";
var WARNING_NO_PAYMENTS 					= "Payments cannot be authorized at this time.\nPlease try again later.";
var WARNING_ORDER_IN_CREDIT_QUEUE                               = "Order must be removed from the Credit Queue before it can be updated.";
var WARNING_SFMB 						= "San Francisco Music Box orders cannot be updated.";
var WARNING_JCP 						= "JCPenney orders cannot be updated.";
var WARNING_ORDER_AUTHING 				        = "Payments cannot be authorized at this time.  Please try again later";
var WARNING_PERSONALIZATION_PRODUCT                             = "You are unable to perform this operation for a personalized order.  Please contact your manager for further instruction.";
var REP_CANNOT_ACCESS_USAA_ORDER                                = "Stop: This is a USAA order.  Please advise the customer:<br><br>''I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you.''<br><br>Transfer the call to XXXXX.";


/*
   Helper function to print the user screen
*/
function printIt(){
	window.print();
}


 /*
    This function handels the back button functionality
 function doBackAction(){
	var url = "BackButton.do" +
		    "?action=back_button" + getSecurityParams(false);
	performAction(url);
 }
 */

 /*
  This function handles the main menu functionality for all pages.
 */
 function doMainMenuAction(siteNameInput, applicationContextInput){
	var url = '';
  var urlPrefix = '';
  if (siteNameInput != null && applicationContextInput != null) {
      urlPrefix = "http://" + siteNameInput + applicationContextInput + "/";
  }

  //Modal_URL
  var modalUrl = "confirm.html";
  //Modal_Arguments
  var modalArguments = new Object();
  var dnis = document.getElementById('call_dnis').value;

   if((dnis != '') && (dnis != null)) {

      modalArguments.modalText = 'Have you completed your work on this order?';

      //Modal_Features
      var modalFeatures  =  'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
      modalFeatures +=  'resizable:no; scroll:no';

      //get a true/false value from the modal dialog.
      var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

      if(ret)
      {
          url = urlPrefix + "mainMenu.do" +	"?work_complete=y" + getSecurityParams(false);
      }
      else
      {
        url = urlPrefix + "mainMenu.do" + "?work_complete=n" + getSecurityParams(false);
      }

  }else{
     url = urlPrefix + "mainMenu.do" + "?work_complete=n" + getSecurityParams(false);
  }

  performAction(url);
}

/*
 Displays the Florist Maintenance screen for the user as
 a modal dialog window
*/
function doFloristMaintenanceAction(url){
		var modal_dim = 'dialogWidth=800px,dialogHeight=425px,dialogLeft=0px,dialogTop=90px';
	     	  modal_dim +='location=0,status=0,menubar=0,scrollbars=0,resizable=0';
    showModalDialogIFrame(url, "", modal_dim);

}

/*
	Displays the Load Memeber Data Search page
	in a modal dialog.
*/
 function doFloristInquiryAction(url){
 	var modal_dim = 'dialogWidth=800px,dialogHeight=425px,dialogLeft=0px,dialogTop=90px';
	     	  modal_dim +='location=0,status=0,menubar=0,scrollbars=1,resizable=0';
    showModalDialogIFrame(url, "", modal_dim);
}

function doLossPreventionSearchAction() {
    var url = "customerOrderSearch.do?action=load_lpsearch_page";
    performAction(url);
}

/***********************************************************************************
* doSearchAction()
************************************************************************************/
function doSearchAction()
{
  var url = "customerOrderSearch.do" +
    "?action=load_page";
	performAction(url);
}

/***********************************************************************************
*doMainMenuActionNoPopup() -- take to the main menu without asking if work is complete
************************************************************************************/
function doMainMenuActionNoPopup(){
	var url = "mainMenu.do" + "?work_complete=n" + getSecurityParams(false);
	performAction(url);
}


/***********************************************************************************
*showModalAlert() -- display a modal popup message
************************************************************************************/
function showModalAlert(message)
{
	//Modal_URL
	modalUrl = "alert.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = message;

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:no; scroll:no';

	window.showModalDialog(modalUrl, modalArguments, modalFeatures);
}


/***********************************************************************************
* doExitPageAction()
************************************************************************************/
function doExitPageAction(popupText){
    return doExitPageActionSetSize(popupText, '200px', '150px');
}

/***********************************************************************************
* doExitPageAction() and allows you to set the size of the popup
************************************************************************************/
function doExitPageActionSetSize(popupText, width, height){
  //Modal_URL
  	var modalUrl = "confirm.html";

  	//Modal_Arguments
  	var modalArguments = new Object();
  	modalArguments.modalText = popupText;

  	//Modal_Features
  	var modalFeatures  =  'dialogWidth:'+width+';';
  	modalFeatures +=  ' dialogHeight:'+height+';';
  	modalFeatures +=  'center:yes; status=no; help=no;';
  	modalFeatures +=  'resizable:no; scroll:no';

  	//get a true/false value from the modal dialog.
  	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  	if(ret)
  	{
  		return true;
	}
  	else
  	{
		return false;
	}

}



  function ModalPopupsYesNoCancel() {
			ModalPopups.YesNoCancel("idYesNoCancel1",
				" ",
				"<div style='padding: 25px;'><p>Would you like to save changes?<br/>" , 
				{
					onYes: "ModalPopupsYesNoCancelYes()",
					onNo: "ModalPopupsYesNoCancelNo()",
					onCancel: "ModalPopupsYesNoCancelCancel()"
				}
			);
		}

/*method toggleChildDisabled() added by jbaba*/
function toggleChildDisabled(el) {

    try {
      el.disabled = el.disabled ? false : true;
    }

    catch(E){    
    }

    if (el.childNodes && el.childNodes.length > 0) {
      for (var x = 0; x < el.childNodes.length; x++) {
      toggleChildDisabled(el.childNodes[x]);
      }
    }
}


function toggleChildEnableDisable(el, toWhat) {

    try {
      el.disabled = toWhat;
    }

    catch(E){    
    }

    if (el.childNodes && el.childNodes.length > 0) {
      for (var x = 0; x < el.childNodes.length; x++) {
      toggleChildEnableDisable(el.childNodes[x], toWhat);
      }
    }
}
/*end of toggleChildDisabled()*/
