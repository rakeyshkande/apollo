/**
 * 
 */
package com.ftd.mo.bo;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.RandomStringUtils;
import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.bo.RefundAPIBO;
import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.dao.ServicesProgramDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.CustomerPhoneVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.EmailVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.RefundVO;
import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.DomainObjectConverter;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.order.OrderIdGenerator;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.order.vo.TaxRequestVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.ps.webservice.AmountVO;
import com.ftd.ps.webservice.CardVO;
import com.ftd.ps.webservice.PaymentRequest;
import com.ftd.ps.webservice.PaymentResponse;
import com.ftd.ps.webservice.PaymentServiceClient;
import com.ftd.security.cache.vo.UserInfo;
import com.mockrunner.mock.jdbc.MockResultSet;

/**
 * @author cjohnson
 * 
 * This class holds the guts of some of unit tests of the CreateNewOrderBO workflow.
 * 
 * Usually when testing a class you might put all of the setup and guts for that class's test into one file,
 * but in this case it was just getting too big.
 *
 */
public class CreateNewOrderTestBase {
	
	private static Logger logger = new Logger(CreateNewOrderTestBase.class.getName());
	
	protected Connection connection;
	protected HttpServletRequest request;
	protected UpdateOrderDAO uoDAO;
	protected LockDAO lockDAO;
	protected RefundBO refundBO;
	protected CustomerDAO customerDAO;
	protected com.ftd.security.SecurityManager securityManager;
	protected InitialContext initialContext;
	protected OrderIdGenerator orderIdGenerator;
	protected DomainObjectConverter converter;
	protected DataMassager massager;
	protected ScrubMapperDAO scrubMapperDAO;
	protected CommentDAO commentDAO;
	protected Dispatcher dispatcher;
	protected GiftCardBO gbo;
	protected ConfigurationUtil configUtil;
	protected IdGeneratorUtil idUtil;
	protected PaymentDAO paymentDAO;
	protected RefundDAO refundDAO;
	protected OrderDAO orderDAO;
	protected RecalculateOrderBO recalcOrderBO;
	protected ServicesProgramDAO servicesProgramDAO;
	
	String origOrderDetailId = "1";
	String origExternalOrderNumber = "C001";
	String origOrderGuid = "FTD_GUID_123totallyunique";
	String securityToken = "some-token";
	UserInfo userInfo = new UserInfo();
	String origProductId = "BB53";  //build a bear
	
	String newOrderDetailId = "100";
	String newExternalOrderNumberSeq = "100";  //turns into C100
	String newMasterOrderNumberSeq = "100";  //turns into M100/1
	String newExternalOrderNumber;
	String newMasterOrderNumber;
	String newOrderGUID = "FTD_GUID_1234AlsoVeryUnique";
	
	Capture<OrderVO> scrubOrderCapture = new Capture<OrderVO>();
	OrderVO scrubOrder = null;

	//cached result set objects we need to mock out the database
	CachedResultSet originalDeliveryInfoCRS;
	HashMap<String,CachedResultSet> tempOrderInfos;

	
	protected void setUp() throws Exception {
		
		this.converter = new DomainObjectConverter(this.connection);
		
		userInfo.setUserID("cjohnson");
		
		connection = createMock(Connection.class);
		request = createMock(HttpServletRequest.class);
		expect(request.getParameter(COMConstants.ORDER_DETAIL_ID))
			.andReturn(origOrderDetailId).anyTimes();
		expect(request.getParameter(COMConstants.SEC_TOKEN)).andReturn(securityToken).anyTimes();
		expect(request.getParameter(COMConstants.CONTEXT)).andReturn("???").anyTimes();
		expect(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)).andReturn(origExternalOrderNumber).anyTimes();
		expect(request.getParameter(COMConstants.ORDER_GUID)).andReturn(origOrderGuid).anyTimes();
		expect(request.getParameter(COMConstants.START_ORIGIN)).andReturn("something-that-does-not-matter").anyTimes();
		Map<String,String> drParams = new HashMap<String, String>();
		drParams.put(DecisionResultConstants.DRF_AVAILABLE_ENTITIES, null);
		expect(request.getAttribute("parameters")).andReturn(drParams).anyTimes();
		request.setAttribute(isA(String.class), isA(HashMap.class));
		expectLastCall();
		
		securityManager = createMock(com.ftd.security.SecurityManager.class);
		expect(securityManager.getUserInfo(securityToken)).andReturn(userInfo).anyTimes();
		replay(securityManager);
		
		UserTransaction tx = createMock(UserTransaction.class);
		
		initialContext = createMock(InitialContext.class);
		expect(initialContext.lookup(isA(String.class))).andReturn(tx).anyTimes();
		initialContext.close();
		expectLastCall();
		replay(initialContext);
		
		orderIdGenerator = createMock(OrderIdGenerator.class);
		expect(orderIdGenerator.getOrderId(connection)).andReturn(newOrderDetailId).anyTimes();
		replay(orderIdGenerator);
		
		replay(request);
		
		lockDAO = createMock(LockDAO.class);
		lockDAO.releaseLock(isA(String.class), isA(String.class), isA(String.class), isA(String.class));
		EasyMock.expectLastCall();
		replay(lockDAO);
		
		customerDAO = createMock(CustomerDAO.class);
		expect(customerDAO.getCustomerById(isA(String.class))).andReturn(Arrays.asList(new CustomerVO())).anyTimes();
		expect(customerDAO.getCustomerPhones(1L))  //add a single customerPhoneVO to this return list with phoneType populated
			.andReturn(Arrays.asList(
					new CustomerPhoneVO() {{
						setPhoneType("Day");
					}}))
					.anyTimes();
		expect(customerDAO.getCustomerEmailInfo(1L, null)).andReturn(new EmailVO()).anyTimes();
		replay(customerDAO);
		this.converter.setCustomerDAO(customerDAO);
		this.converter.setDao(uoDAO);
		
		
		// we're overwriting the dataMassager massageOrder method to not do anything and just return the input order
		this.massager = new DataMassager() {
			public OrderVO massageOrderDataNotStatic(OrderVO scrubOrder, Connection conn) throws IOException, SAXException,
						    ParserConfigurationException, TransformerException, SQLException,
						    ParseException, Exception {
				return scrubOrder;
			};
		};
		
		scrubMapperDAO = createMock(ScrubMapperDAO.class);
		expect(scrubMapperDAO.mapOrderToDB(isA(OrderVO.class))).andReturn(true).anyTimes();
		replay(scrubMapperDAO);
		
		commentDAO = createMock(CommentDAO.class);
		commentDAO.insertComment(isA(CommentsVO.class));
		EasyMock.expectLastCall();
		replay(commentDAO);
		
		dispatcher = createMock(Dispatcher.class);
		dispatcher.dispatch(isA(InitialContext.class), isA(MessageToken.class));
		expectLastCall();
		replay(dispatcher);
		
//		mockStatic(DecisionResultDataFilter.class);
//		expect(DecisionResultDataFilter.updateEntity(isA(HttpServletRequest.class), isA(String.class), true));
		
		configUtil = createMock(ConfigurationUtil.class);
		expect(configUtil.getFrpGlobalParm("SERVICE", "PAYMENT_SERVICE_URL"))
			.andReturn("http://cobalt4.ftdi.com:8725/ps/services/PS?wsdl").anyTimes();
		expect(configUtil.getSecureProperty("SERVICE", "SVS_CLIENT")).andReturn("BEARS").anyTimes();
		expect(configUtil.getSecureProperty("SERVICE", "SVS_HASHCODE")).andReturn("RULES").anyTimes();
		expect(configUtil.getProperty(isA(String.class), isA(String.class))).andReturn("mocked out message").anyTimes();
		replay(configUtil);
		
		gbo = new GiftCardBO();
		gbo.setConnection(this.connection);
		gbo.setConfigurationUtil(configUtil);
		
		recalcOrderBO = createMock(RecalculateOrderBO.class);
		CalculateTaxUtil taxUtil = new CalculateTaxUtil();
		
		RecipientAddressesVO recipientAddress = new RecipientAddressesVO();
		recipientAddress.setCity("Downers Grove");
		recipientAddress.setCountry("US");
		recipientAddress.setStateProvince("IL");
		recipientAddress.setPostalCode("60515");
		TaxRequestVO taxRequestVO = new TaxRequestVO();
		taxRequestVO.setRecipientAddress(recipientAddress);
		taxRequestVO.setCompanyId("FTD");
		taxRequestVO.setShipMethod("SD");
		taxRequestVO.setTaxableAmount(BigDecimal.ZERO);
		
		
		ItemTaxVO itemTaxVO = new CalculateTaxUtil().calculateTaxRates(taxRequestVO);
		if(itemTaxVO != null) {
			expect(itemTaxVO.getTaxSplit()).andReturn(new ArrayList<TaxVO>()).anyTimes();
		}
		replay(recalcOrderBO);
		
		servicesProgramDAO = createMock(ServicesProgramDAO.class);
		expect(servicesProgramDAO.isProductFreeShipping(isA(Connection.class), isA(String.class))).andReturn(false).anyTimes();
		replay(servicesProgramDAO);
		


	}
	
	
	protected void translateInputDataForTest(
			com.ftd.customerordermanagement.vo.OrderVO order,
			List orderDetails, List orderBills, List payments, List refunds,
			List newOrderDetails, List newPayments) throws Exception {
		
		//original order data
		originalDeliveryInfoCRS = getOriginalDeliveryInfoCRS(orderDetails, orderBills);
		//new order data
		tempOrderInfos = createTempOrderInfo(orderDetails, orderBills, newOrderDetails, newPayments);
		HashMap paymentRefundMap = createPaymentRefundMap(orderDetails, orderBills, payments, refunds);
		
		//now mock out the updateOrderDAO with the data we've created
		
		uoDAO = createMock(UpdateOrderDAO.class);
		expect(uoDAO.hasOrderBeenModified(origExternalOrderNumber)).andReturn("N").anyTimes();
		expect(uoDAO.insertCommentsUpdate(EasyMock.isA(CommentsVO.class))).andReturn("true").anyTimes();
		expect(uoDAO.getOriginalDeliveryInfo(origOrderDetailId)).andReturn(originalDeliveryInfoCRS).anyTimes();
		expect(uoDAO.getOrderDetailsUpdate(origOrderDetailId, "DELIVERY_CONFIRMATION", false, userInfo.getUserID(), null))
			.andReturn(tempOrderInfos).anyTimes();
		expect(uoDAO.getExternalOrderSeqNum()).andReturn(newExternalOrderNumberSeq.replace("C", "")).anyTimes();
		expect(uoDAO.getMasterOrderSeqNum()).andReturn(newMasterOrderNumberSeq.replace("M", "")).anyTimes();
		expect(uoDAO.getPartnerName(null)).andReturn(null).anyTimes();
		expect(uoDAO.getScrubOrderDetailId(newExternalOrderNumber)).andReturn(newOrderDetailId).anyTimes();
		expect(uoDAO.getPaymentType("GD")).andReturn("Gift Card/Certificate").anyTimes();
		expect(uoDAO.getPaymentType("VI")).andReturn("Visa").anyTimes();
		
		uoDAO.insertChanges(isA(String.class), isA(String.class),isA(String.class), isA(String.class), isA(String.class), isA(Map.class));
		expectLastCall();
		uoDAO.deleteOrderHold(origOrderDetailId);
		expectLastCall();
		expect(uoDAO.getMercuryOrderMessageStatus(origOrderDetailId, null, "Y")).andReturn(new CachedResultSet()).anyTimes();
		replay(uoDAO);
		
//		refundBO = createMock(RefundBO.class);
//		expect(refundBO.postFullRefundMO(isA(List.class), isA(String.class), isA(String.class), isA(String.class), isA(String.class), isA(String.class)))
//			.andReturn(null).anyTimes();
//		replay(refundBO);
		
		paymentDAO = createMock(PaymentDAO.class);
		//expect(paymentDAO.getPaymentsForOrder(origOrderGuid));
		paymentDAO.updatePayments(isA(PaymentVO[].class));
		expectLastCall();
		replay(paymentDAO);
		
//		converter = createMock(DomainObjectConverter.class);
//		expect(converter.convertOrder(newExternalOrderNumber, newMasterOrderNumber, newOrderGUID, userInfo.getUserID()))
//			.andReturn(null).anyTimes();
//		replay(converter);
		
		//delete this crud
		refundDAO = createMock(RefundDAO.class);
		//updatePaymentRefundMapPaymentMethodTypes(paymentRefundMap);
		expect(refundDAO.loadPaymentRefund(origOrderDetailId)).andReturn(paymentRefundMap).anyTimes();
		expect(refundDAO.insertRefund(isA(RefundVO.class))).andReturn("").anyTimes();
		refundDAO.updateRefund(isA(RefundVO.class));
		expectLastCall().times(2);
		replay(refundDAO);
		
		orderDAO = createMock(OrderDAO.class);
		expect(orderDAO.getOrderDetail(origOrderDetailId)).andReturn((OrderDetailVO)orderDetails.get(0)).anyTimes();
		replay(orderDAO);
		
		this.refundBO = new RefundBO(this.connection);
		RefundAPIBO rApiBo = new RefundAPIBO();
		rApiBo.setoDAO(orderDAO);
		rApiBo.setRefundDAO(refundDAO);
		//TODO: implement me!
		rApiBo.setAccountBO(null);
		rApiBo.setRecalcOrderBO(recalcOrderBO);
		rApiBo.setServicesProgramDAO(servicesProgramDAO);
		this.refundBO.setRefundApiBO(rApiBo);
		
		
		//we're mocking out the generateId calls here.  It's kind of a weird place but it needs to happen per test
		idUtil = createMock(IdGeneratorUtil.class);
		//return different tx ids on subsequent calls
		//I think we should only need 3 of these, not 5???
		expect(idUtil.generateId(isA(IdRequestVO.class), isA(Connection.class)))
			.andReturn(generateTransactionId()).once();
		expect(idUtil.generateId(isA(IdRequestVO.class), isA(Connection.class)))
		.andReturn(generateTransactionId()).once();
		expect(idUtil.generateId(isA(IdRequestVO.class), isA(Connection.class)))
		.andReturn(generateTransactionId()).once();
		expect(idUtil.generateId(isA(IdRequestVO.class), isA(Connection.class)))
		.andReturn(generateTransactionId()).once();
		expect(idUtil.generateId(isA(IdRequestVO.class), isA(Connection.class)))
		.andReturn(generateTransactionId()).once();
		expect(idUtil.generateId(isA(IdRequestVO.class), isA(Connection.class)))
		.andReturn(generateTransactionId()).once();
		gbo.setIdGeneratorUtil(idUtil);
		
		replay(idUtil);		
		
	}

	
//	private Map updatePaymentRefundMapPaymentMethodTypes(HashMap paymentRefundMap) {
//		
//		Map newPayRefundMap = (HashMap)paymentRefundMap.clone();
//		
//		Map<String,List<PaymentVO>> paymap = (HashMap<String,List<PaymentVO>>)newPayRefundMap.get(RefundDAO.PAYMENT_LIST);
//		
//		Iterator<List<PaymentVO>> it = paymap.values().iterator();
//		
//		List<PaymentVO> paylist = null;
//		while (it.hasNext()) {
//			paylist = it.next();
//			for (PaymentVO payment : paylist) {
//				if (payment.getPaymentType() != null && payment.getPaymentType().equals("GD")) {
//					payment.setPaymentType("R");
//				}
//			}
//		}
//		
//		return newPayRefundMap;
//		
//		
//	}


	protected HashMap createPaymentRefundMap(List orderDetails, List orderBills,
			List payments, List refunds) {
		
		HashMap<String,Object> map = new HashMap<String, Object>();
		map.put("recipient_country", "US");
		map.put("recipient_state", "IL");
		map.put("company_id", "FTD");
		
		//PAYMENT_LIST is a map of orderBillIds to PaymentVOs
		//we need to organize the payments by their order bills. 
		// cart level payments will have OBID = -999
		// "add bill" payments will have a real order bill
		//This is such a PITA and wouldn't be a problem if we had a MODERN ORM!!!!
		
		HashMap<String, List<PaymentVO>> payMap = new HashMap<String, List<PaymentVO>>();
		for (PaymentVO payment : (List<PaymentVO>)payments) {
			
		}
		
		for (int i=0;i<orderBills.size();i++) {
			OrderBillVO ob = (OrderBillVO)orderBills.get(i);
			//set the order bill "fees"
			ob.setFeeAmount(ob.getServiceFee() + ob.getShippingFee());
			List<PaymentVO> billPaymentList = new ArrayList<PaymentVO>();
			for(int j=0;j<payments.size();j++) {
				//re-write the payment types based on the contents of the FTD_APPS.PAYMENT_METHOD table
				PaymentVO payment = (PaymentVO)payments.get(j);
				if (payment.getPaymentType() != null && (payment.getPaymentType().equals("GD"))) {
					payment.setPaymentType("R");
				} else if (payment.getPaymentType() != null && payment.getPaymentType().equals("GC")) {
					payment.setPaymentType("G");
				} else if (payment.getPaymentType() != null && 
							(payment.getPaymentType().equals("VI") || payment.getPaymentType().equals("MC")
								|| payment.getPaymentType().equals("NC"))) {
					payment.setPaymentType("C");
				}
				if (payment.getOrderBill() != null &&
						payment.getOrderBill().getOrderBillId() == ob.getOrderBillId()) { 
					billPaymentList.add(payment);
				}
				payMap.put(ob.getOrderBillId(), billPaymentList);
			}
			
		}
		
		map.put(RefundDAO.PAYMENT_LIST, payMap);
		
		//REFUND_LIST is an ArrayList of RefundBOs
		
		map.put(RefundDAO.REFUND_LIST, refunds);
		
		return map;
	}
	
	/**
	 * make up a phoney set of results for calls to get original delivery info
	 * @return
	 * @throws SQLException 
	 */
	protected CachedResultSet getOriginalDeliveryInfoCRS(List<OrderDetailVO> orderDetails,
			List<OrderBillVO> orderBills) throws SQLException {
		
		//we just care about the first record here
		OrderBillVO bill = orderBills.get(0);
		OrderDetailVO od = orderDetails.get(0);
		
		CachedResultSet crs = new CachedResultSet();
		MockResultSet rs = new MockResultSet("originalDeliveryInfo");
		rs.addColumn("add_on_amount", new Double[]{bill.getAddOnAmount()});
		rs.addColumn("discount_amount", new Double[]{bill.getDiscountAmount()});
		rs.addColumn("product_amount", new Double[]{bill.getProductAmount()});
		rs.addColumn("service_fee", new Double[]{bill.getServiceFee()});
		rs.addColumn("service_fee_tax", new Double[]{bill.getServiceFeeTax()});
		rs.addColumn("shipping_fee", new Double[]{bill.getShippingFee()});
		rs.addColumn("shipping_tax", new Double[]{bill.getShippingTax()});
		rs.addColumn("tax", new Double[]{bill.getTax()});
		rs.addColumn("vendor_flag", new String[]{"N"});
		rs.addColumn("delivery_date", new java.sql.Date[]{new java.sql.Date(od.getDeliveryDate().getTimeInMillis())});
		rs.addColumn("customer_id", new String[]{"1"});
		
		//delivery info vo
		//we probably need to populate more stuff here

		crs.populate(rs);
		
		return crs;
	}
	
	protected HashMap createTempOrderInfo(List<OrderDetailVO> orderDetails,
			List<OrderBillVO> orderBills, List<OrderDetailVO> newOrderDetails, 
			List<PaymentVO> newPayments) throws SQLException {
		
		HashMap<String,CachedResultSet> tmpOrderInfo = new HashMap<String, CachedResultSet>();
		//create the OUT_ORIG_ADD_ONS crs
		CachedResultSet origAddOnsCRS = new CachedResultSet();
		MockResultSet rs = new MockResultSet("out_orig_add_ons");
		origAddOnsCRS.populate(rs);
		tmpOrderInfo.put("OUT_ORIG_ADD_ONS", origAddOnsCRS);
		
		//OUT_UPD_ORDER_CUR
		CachedResultSet updOrderCRS = new CachedResultSet();
		MockResultSet uoRS = new MockResultSet("originalDeliveryInfo");
		
		
		uoRS.addColumn("add_on_amount");
		uoRS.addColumn("discount_amount");
		uoRS.addColumn("product_amount");
		uoRS.addColumn("service_fee");
		uoRS.addColumn("service_fee_tax");
		uoRS.addColumn("shipping_fee");
		uoRS.addColumn("shipping_tax");
		uoRS.addColumn("tax");
		uoRS.addColumn("actual_product_amount");
		uoRS.addColumn("delivery_date");
		uoRS.addColumn("order_detail_id");
		uoRS.addColumn("order_guid");
		uoRS.addColumn("product_id");
		uoRS.addColumn("quantity");
		uoRS.addColumn("vendor_flag");
		
		
		
		for(int i=0;i<newOrderDetails.size();i++) {
			OrderDetailVO od = orderDetails.get(i);
			//we're only testing single order bill scenarios
			OrderBillVO bill = (OrderBillVO)od.getOrderBillVOList().get(0);
			uoRS.addRow(new Object[]{
				bill.getAddOnAmount(),
				bill.getDiscountAmount(),
				bill.getProductAmount(),
				bill.getServiceFee(),
				bill.getServiceFeeTax(),
				bill.getShippingFee(),
				bill.getShippingTax(),
				bill.getTax(),
				bill.getProductAmount(),
				new java.sql.Date(od.getDeliveryDate().getTimeInMillis()),
				od.getOrderDetailId(),
				od.getOrderGuid(),
				od.getProductId(),
				od.getQuantity(),
				"N"  //vendor_flag
			});
		}
		
		updOrderCRS.populate(uoRS);
		tmpOrderInfo.put("OUT_UPD_ORDER_CUR", updOrderCRS);
		
		CachedResultSet updAddonCRS = new CachedResultSet();
		MockResultSet uaRS = new MockResultSet("out_upd_addon_cur");
		updAddonCRS.populate(uaRS);
		tmpOrderInfo.put("OUT_UPD_ADDON_CUR", updAddonCRS);
		
		//we need to be getting this data from somewhere including a credit card
		CachedResultSet updPaymentCRS = new CachedResultSet();
		MockResultSet payRS = new MockResultSet("out_payments_cur");
		payRS.addColumn("payment_type");
		payRS.addColumn("cc_id");
		payRS.addColumn("credit_amount");
		payRS.addColumn("auth_number");
		payRS.addColumn("auth_result");
		payRS.addColumn("gc_coupon_number");
		payRS.addColumn("nc_approval_identity_id");
		payRS.addColumn("cc_expiration");
		payRS.addColumn("cc_number");
		payRS.addColumn("cc_type");
		payRS.addColumn("gift_card_pin");
		
		for(PaymentVO payment : newPayments) {
			payRS.addRow(new Object[] {
					payment.getPaymentType(), //payment type
					payment.getCardNumber(), //cc_id.  need to fake the CC record ID so it will attach the CC data
					payment.getCreditAmount(), //credit amount
					payment.getAuthorization(),
					payment.getAuthResult(),
					payment.getGcCouponNumber(),
					payment.getNcApprovalId(),
					null, //cc expiration
					payment.getCardNumber(),
					payment.getPaymentType(), //cc type
					payment.getGiftCardPin()
			});
		}
		
		updPaymentCRS.populate(payRS);
		tmpOrderInfo.put("OUT_PAYMENTS_CUR", updPaymentCRS);
		
		

		return tmpOrderInfo;
	}

	public static String generateTransactionId() {
		/**
		 * first make a new transaction id.
		 */
		/* Step 1 : 
		 This value is supposed to come from global parameters �SVS_TRANSACTION_ID_PREFIX� of context SERVICE.
		 Please verify this value from one in the frp.global_parms table 
		 Use this as digit 1 through 6. 
		 */
		String txId = "603730";
		/* Step 2:
		 * This value is supposed to come from global parameters �ID_REQUEST_SVS_CLIENT_APOLLO" of context GLOBAL_CONFIG. 
		 * Use this as digit 7. */
		txId +="2";
		/*
		 * step 3 - add a random number component.  in regular environments this comes from a sequence
		 */
		return txId + RandomStringUtils.random(9,false, true);
	}
	
	protected void wireUpTheMocks(CreateNewOrderBO createNewOrderBO) {
		createNewOrderBO.uoDAO = this.uoDAO;
		createNewOrderBO.securityManager = this.securityManager;
		createNewOrderBO.initialContext = this.initialContext;
		createNewOrderBO.orderIdGenerator = this.orderIdGenerator;
		createNewOrderBO.rBO = this.refundBO;
		createNewOrderBO.lockDAO = this.lockDAO;
		this.converter.setDao(uoDAO);
		createNewOrderBO.converter = this.converter;
		createNewOrderBO.dm = this.massager;
		createNewOrderBO.scrubMapper = this.scrubMapperDAO;
		createNewOrderBO.commentDAO = this.commentDAO;
		createNewOrderBO.dispatcher = this.dispatcher;
		createNewOrderBO.giftCardBO = this.gbo;
		createNewOrderBO.paymentDAO = this.paymentDAO;
		createNewOrderBO.refundDAO = this.refundDAO;

		
	}


	/**
	 * set the state of the gift cards to something valid before the test runs.
	 * 
	 * There is not actually a good programatic way to put money back on a gift card without having the original TX id
	 * so if the gift card runs out of money you need to go to the web interface to put money on them or clear out the pre-auths.
	 * 
	 * origPayments - we will check the balance Then we will authorize the card with the new transaction ID.
	 *
	 * newPayments - we will check the balance and make sure there is enough to authorize the new payment amount.
	 * 
	 * @param origPayments
	 * @param newPayments
	 * @throws Exception
	 */
	protected void checkGiftCards(List<PaymentVO> origPayments, List<PaymentVO> newPayments) throws Exception {
		

		
		//now check the balance and settle/refund if necessary
		for(PaymentVO payment: origPayments) {
			if (payment.getPaymentType().equals("GD") || payment.getPaymentType().equals("R")) {
				logger.info("******Pre-Authing GIFT CARD "+payment.getCardNumber()+"************");
				Double balance = gbo.getCurrentBalance(DOMUtil.getDefaultDocument(), 
														payment.getCardNumber(), 
														payment.getGiftCardPin(), 
														String.valueOf(payment.getAmount()));
				logger.info("card has a balance of " + balance + " remaining");
				if (balance < payment.getCreditAmount()) {
					//refund to the amount we need
					/*
					 * "refund" might not be the right thing to do here.  
					 * We would need the transactionID from the original "settle" that we want to refund.
					 *It is also possible that there are a bunch of outstanding pre-auth's eating up the card balance.
					 *
					 * If you get an exception here it may be a better idea to just go to the SVS web interface and issue refunds and clear out the pre-auths
					 * 
					 *  https://osi-cert.storedvalue.com/osi
					 *  
					 *  SVS Group Location ID - FTDGC
					 *
					 *	Enter Group ID - FTDGCTEST
					 *	
					 *	Enter User ID - DEMO
					 *	
					 *	Password: ftdrock52
					 *
					 * 
					 */
					Assert.fail("Not enough balance on the gift card to run the test.  visit https://osi-cert.storedvalue.com/osi to add more balance. SVS Group Location ID: FTDGC, Group ID: FTDGCTEST, UserID/password: DEMO/ftdrock52 ");
				}
				gbo.authorizePayment(payment);
			}
		}

	}
	
	/**
	 * cancels any pre-auths on the gift cards based on tx ids on the payements
	 * @param origPayments
	 * @param newPayments
	 * @throws Exception 
	 */
	protected void resetGiftCards(List<PaymentVO> origPayments,List<PaymentsVO> newPayments) throws Exception {
		
		logger.info("*********Cleaning up after ourselves.  cancelling our GD pre-auths*********");
		
		for(PaymentVO payment : origPayments) {
			if (payment.getPaymentType() != null && payment.getPaymentType().equals("GD") && payment.getAuthorization() != null) {
				cancelPreAuth(payment);
			}
		}
		
		for(PaymentsVO payment : newPayments) {
			if (payment.getPaymentType() != null && payment.getPaymentType().equals("GD") && payment.getAuthNumber() != null) {
				PaymentVO p = new PaymentVO();
				p.setGiftCardPin(((CreditCardsVO)payment.getCreditCards().get(0)).getPin());
				p.setAuthorization(payment.getAuthNumber());
				p.setPaymentType("GD");
				p.setCardNumber(((CreditCardsVO)payment.getCreditCards().get(0)).getCCNumber());
				p.setCreditAmount(new Double(payment.getAmount()));
				cancelPreAuth(p);
			}
		}
	}
	
	/**
	 * logic to calculate the total bill for some item's orderBill.
	 * This logic was copied from CLEAN.ORDER_QUERY_PACKAGE.GET_ORDER_BILLS.  It looked something like this at the time of writing:
	 * 					
	 * sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee) + sum (ob.shipping_fee) 
		- sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax) bill_total,
	 * @param bill
	 * @return
	 */
	protected Double getBillTotal(OrderBillVO bill) {
		
		Double total = bill.getProductAmount()
					+ bill.getGrossProductAmount()
					+ bill.getAddOnAmount()
					+ bill.getServiceFee()
					+ bill.getShippingFee()
					- bill.getDiscountAmount()
					+ bill.getShippingTax()
					+ bill.getTax();
		

		return total;
	}
	
    
	public void cancelPreAuth(PaymentVO payment) throws Exception {

		PaymentServiceClient psc = gbo.getPaymentServiceClient();
		PaymentRequest req = new PaymentRequest();
		req.setClientUserName("BEARS");
		req.setClientPassword("RULES");
        req.setTransactionId(payment.getAuthorization());
        CardVO cardvo = new CardVO();
        cardvo.setCardCurrency("USD");
        cardvo.setCardNumber(payment.getCardNumber());
        cardvo.setPinNumber(payment.getGiftCardPin());
        req.setCardVO(cardvo);
        
        AmountVO amountvo = new AmountVO();
        amountvo.setAmount(new Double(0));
        amountvo.setCurrency("USD");
        req.setAmountVO(amountvo);
    	//set a transaction ID on the request. save it in the payment
    	PaymentResponse resp = psc.cancelAuthorization(req);
		if (resp != null) {
    		String respMsgs = "GiftCard Settle Service error code/message: " + resp.getErrorCode() + "/" + resp.getErrorMessage() +
            " --- SVS response code/message: " + resp.getResponseCode() + "/" + resp.getResponseMessage();
			logger.info(respMsgs);
    	}
	}
	
}

