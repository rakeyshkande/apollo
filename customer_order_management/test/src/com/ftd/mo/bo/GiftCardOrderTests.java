/**
 * 
 */
package com.ftd.mo.bo;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.RefundVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;

/**
 * This unit test is meant to exercise the logic behind doing an update order using a gift card.
 * 
 * 
 * @author cjohnson
 *
 */
public class GiftCardOrderTests extends CreateNewOrderTestBase {
	
	Logger logger = new Logger(GiftCardOrderTests.class.getName());

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		origOrderDetailId = "1";
		origExternalOrderNumber = "C001";
		origOrderGuid = "FTD_GUID_123totallyunique";
		securityToken = "some-token";
		userInfo = new UserInfo();
		origProductId = "BB53";  //build a bear
		
		newOrderDetailId = "100";
		newExternalOrderNumberSeq = "100";  //turns into C100
		newMasterOrderNumberSeq = "100";  //turns into M100/1
		//newExternalOrderNumber;
		//newMasterOrderNumber;
		newOrderGUID = "FTD_GUID_1234AlsoVeryUnique";
		
		scrubOrderCapture = new Capture<OrderVO>();
		scrubOrder = null;
		
		super.setUp();
	}

	
	/**
	 * This method will test what happens when you have a single item cart paid for with a GIFT CARD
	 * and you are updating the item to an item of less than or equal value.
	 * 
	 * We will instantiate CreateNewOrderBO and call processRequest on it as if it were called from the payments screen
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testUC22840ExistingGiftCardOrderSingleItemCart() throws Exception {
		
		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(20.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		final List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(20.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
//			new RefundVO(){{
//				setOrderBillId(1L);
//			}}
		);
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
			setPaymentVOList(payments);
		}};
		
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(10.00);  //less than the original of 20
				setServiceFee(15.0);
				setTax(5.0);
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(10.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 

		
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		
		checkGiftCards(payments, newPayments);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());	}
		
		
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		resetGiftCards(payments,newOrder.getPayments());
		//make sure refund values are what we expect
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("BB53"));
		assertTrue(od.getProductsAmount(), od.getProductsAmount().equals("20.0"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 1);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));
		
		assertTrue("tx id in both ap_auth_txt and auth_number", payment.getApAuth().equals(payment.getAuthNumber()));
		
		assertTrue("refund issued", refundsIssued != null && refundsIssued.size() >0);
		
		//make sure the original order total matches the refund amount
		assertTrue("existing order total ("+getBillTotal((OrderBillVO)orderBills.get(0))+") matches the refund ("+getBillTotal(refundsIssued.get(0).getOrderBill())+")", 
				getBillTotal((OrderBillVO)orderBills.get(0)).doubleValue() == getBillTotal(refundsIssued.get(0).getOrderBill()).doubleValue());
		
	}
	
	/**
	 * This method will test what happens when you have a single item cart paid for with a GIFT CARD and Credit card
	 * and you are updating the item to an item lesser value (less than the GD balance).
	 * 
	 * We should refund both the GD and CC and then authorize the GD for the new amount
	 * 
	 * We will instantiate CreateNewOrderBO and call processRequest on it as if it were called from the payments screen
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testUC22840ExistingGD_CCOrderSingleItemCart() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
		}};

		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(100.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		List payments = Arrays.asList(
			new PaymentVO(){{ //gift card
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(50.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}},
			new PaymentVO(){{  //credit card
				setPaymentId(2L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("VI");
				setCreditAmount(50.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("4111111111111111");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
//			new RefundVO(){{
//				setOrderBillId(1L);
//			}}
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(10.00);  //less than the original of 20
				setServiceFee(0);
				setTax(0);
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(10.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 

		
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		
		checkGiftCards(payments, newPayments);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());	}
		
		
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		resetGiftCards(payments,newOrder.getPayments());
		//make sure refund values are what we expect
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("BB53"));
		assertTrue(od.getProductsAmount(), od.getProductsAmount().equals("100.0"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 1);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));
		
		assertTrue("tx id in both ap_auth_txt and auth_number", payment.getApAuth().equals(payment.getAuthNumber()));
		
		assertTrue("2 refunds issued (" +refundsIssued.size() + ")", refundsIssued != null && refundsIssued.size() == 2);
		
		//make sure the original order total matches the total refund amount
		double originalItemTotal = 100.0;
		double refundTotal = refundsIssued.get(0).getPayment().getCreditAmount() + refundsIssued.get(1).getPayment().getCreditAmount();
		assertTrue("existing order total ("+originalItemTotal+") matches the refund ("+refundTotal+")", 
				originalItemTotal == refundTotal);
		
	}
	
	/**
	 * Like the above test except we have a multi-item cart.
	 * So we have one payment for all items and need to make sure that we properly handle the refund for the single item.
	 * and only charge the remainder of the GD for the new item.
	 * 
	 * We will instantiate CreateNewOrderBO and call processRequest on it as if it were called from the payments screen
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testUC22840ExistingGiftCardOrderMultiItemCart() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
		}};

		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("2");
				setOrderDetailId(2l);
				setProductAmount(20.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(50.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId("2");
				setExternalOrderNumber("C002");
				setProductId("4047");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(Arrays.asList(orderBills.get(0)));
			}}
		);
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
//			new RefundVO(){{
//				setOrderBillId(1L);
//			}}
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(10.00);  //less than the original of 20
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(10.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		checkGiftCards(payments, newPayments);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);

		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());	}

		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		resetGiftCards(payments,newOrder.getPayments());
		//make sure refund values are what we expect
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("4047"));
		assertTrue(od.getProductsAmount(), od.getProductsAmount().equals("20.0"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 1);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));
		
		assertTrue("tx id in both ap_auth_txt and auth_number", payment.getApAuth().equals(payment.getAuthNumber()));
		
		assertTrue("refund issued", refundsIssued != null && refundsIssued.size() >0);
		
		//make sure the original order total matches the refund amount
		double originalItemTotal = getBillTotal((OrderBillVO)orderBills.get(0));
		double refundTotal = getBillTotal(refundsIssued.get(0).getOrderBill()).doubleValue();
		assertTrue("existing order total ("+originalItemTotal+") matches the refund ("+refundTotal+")", originalItemTotal == refundTotal);
		assertTrue("we are only creating one refund", refundsIssued.size() == 1);
	}
	
	/**
	 * Like the above test except we have a multi-item cart.
	 * So we have one payment for all items and need to make sure that we properly handle the refund for the single item.
	 * and only charge the remainder of the GD for the new item.
	 * 
	 * Cart 1 - There were two items of $100 each.
	 * cart total was $200 paid for with credit card ($150) and Gift Card ($50)
	 * We are refunding a $100 item and purchasing a $10 item instead.
	 * 
	 * we should end up refunding on both the CC and GD and then creating new payments
	 * Cart 1:
	 *   GD Payment $50
	 *   GD payment (50)
	 *   CC payment 150
	 *   CC payment (50)
	 *   
 	 *  Cart 2:
 	 *  GD payment 10
	 * 
	 * 
	 * 
	 * We will instantiate CreateNewOrderBO and call processRequest on it as if it were called from the payments screen
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testUC22840GD_CCOrderMultiItemCart() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
		}};

		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("2");
				setOrderDetailId(2l);
				setProductAmount(200.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(50.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}},
			new PaymentVO(){{  //credit card
				setPaymentId(2L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("VI");
				setCreditAmount(150.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("4111111111111111");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId("2");
				setExternalOrderNumber("C002");
				setProductId("4047");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(Arrays.asList(orderBills.get(0)));
			}}
		);
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
//			new RefundVO(){{
//				setOrderBillId(1L);
//			}}
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(10.00);  //less than the original of 100
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(10.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		checkGiftCards(payments, newPayments);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);

		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());	}

		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		resetGiftCards(payments,newOrder.getPayments());
		//make sure refund values are what we expect
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("4047"));
		assertTrue(od.getProductsAmount(), od.getProductsAmount().equals("200.0"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 1);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));
		
		assertTrue("tx id in both ap_auth_txt and auth_number", payment.getApAuth().equals(payment.getAuthNumber()));
		
		assertTrue("2 refund issued", refundsIssued != null && refundsIssued.size() == 2);
		
		//make sure the original order total matches the refund amount
		//make sure the original order total matches the total refund amount
		double originalItemTotal = new Double(od.getProductsAmount());
		double refundTotal = refundsIssued.get(0).getPayment().getCreditAmount() + refundsIssued.get(1).getPayment().getCreditAmount();
		assertTrue("existing order total ("+originalItemTotal+") matches the refund ("+refundTotal+")", 
				originalItemTotal == refundTotal);
	}
	
	/**
	 * This method will test what happens when you have a single item cart paid for with a GIFT CARD
	 * and you are updating the item to an item of greater value.  
	 * Furthermore, the new item cost EXCEEDS what is available on the Gift Card so the customer is paying
	 * with a combination of the gift card and a credit card.
	 * 
	 * We will instantiate CreateNewOrderBO and call processRequest on it as if it were called from the payments screen
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testUC23223SingleItemCart_GDOrderUpdatedWithGD_and_CC() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
		}};

		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(20.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(20.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(300.00);  //The GD can have 250 max so the rest will go on the CC
				setBillStatus("Unbilled");
				//total should be 320
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{  //gift card
				setCreditAmount(250.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}},
			new PaymentVO() {{  //visa
				setCreditAmount(50.00);
				setCardNumber("4111111111111111");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("VI");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 

		
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		
		checkGiftCards(payments, newPayments);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());
		} finally {
			resetGiftCards(payments,createNewOrderBO.getNewScrubOrder().getPayments());
		}
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("BB53"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 2);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		//assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));
		
		assertTrue("tx id in both ap_auth_txt and auth_number", payment.getApAuth().equals(payment.getAuthNumber()));
		
		assertTrue("refund issued", refundsIssued != null && refundsIssued.size() >0);
		
		//make sure the original order total matches the refund amount
		assertTrue("existing order total ("+getBillTotal((OrderBillVO)orderBills.get(0))+") matches the refund ("+getBillTotal(refundsIssued.get(0).getOrderBill())+")", 
				getBillTotal((OrderBillVO)orderBills.get(0)) == getBillTotal(refundsIssued.get(0).getOrderBill()).doubleValue());
		
	}
	
	/**
	 * This method will test what happens when you have a single item cart paid for with a GIFT CARD AND a CREDIT CARD.
	 * and you are updating the item to an item of lesser value (but still more than the GD balance)
	 * 
	 * THe GD should be refunded first and the CC second.
	 * 
	 * We will instantiate CreateNewOrderBO and call processRequest on it as if it were called from the payments screen
	 * @throws Exception 
	 * 
	 */
	//@Test
	public void testUC23223SingleItemCart_GD_CCOrderUpdatedWithGD_and_CC() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
		}};

		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(20.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(20.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(300.00);  //The GD can have 250 max so the rest will go on the CC
				setBillStatus("Unbilled");
				//total should be 320
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{  //gift card
				setCreditAmount(250.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}},
			new PaymentVO() {{  //visa
				setCreditAmount(50.00);
				setCardNumber("4111111111111111");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("VI");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 

		
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		
		checkGiftCards(payments, newPayments);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());
		} finally {
			resetGiftCards(payments,createNewOrderBO.getNewScrubOrder().getPayments());
		}
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("BB53"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 2);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		//assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));
		
		assertTrue("tx id in both ap_auth_txt and auth_number", payment.getApAuth().equals(payment.getAuthNumber()));
		
		assertTrue("refund issued", refundsIssued != null && refundsIssued.size() >0);
		
		//make sure the original order total matches the refund amount
		assertTrue("existing order total ("+getBillTotal((OrderBillVO)orderBills.get(0))+") matches the refund ("+getBillTotal(refundsIssued.get(0).getOrderBill())+")", 
				getBillTotal((OrderBillVO)orderBills.get(0)) == getBillTotal(refundsIssued.get(0).getOrderBill()).doubleValue());
		
	}
	
	/**
	 * UC23227: Original order placed with a CC.  The new order is placed with a GD.
	 * The GD balance will cover the cost of of the new item.
	 * @throws Exception 
	 */
	@Test
	public void testUC23227ForSingleItemCart_CC_to_GD_balance_covered() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);


		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(20.00);
				//setServiceFee(15.00);
				setTax(5.0);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		final List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("VI");
				setCreditAmount(25.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("4111111111111111");
			}}
		);
		//order details
		final List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
			setPaymentVOList(payments);
			setOrderDetailVOList(orderDetails);
		}};
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(10.00);  //less than the original of 20
				//setServiceFee(15.00);
				setTax(5.00);
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(15.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 
		checkGiftCards(payments, newPayments);
		
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {  e.printStackTrace();		fail(e.getMessage());
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());	}
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		
		resetGiftCards(payments,newOrder.getPayments());
		
		//make sure refund values are what we expect
		
		//look for new order detail record
		assertTrue("new Order is not null",newOrder != null);
		assertTrue("new order has 1 order details",newOrder.getOrderDetail()!= null && newOrder.getOrderDetail().size() == 1);
		OrderDetailsVO od = (OrderDetailsVO)newOrder.getOrderDetail().get(0);
		assertTrue(od.getProductId().equals("BB53"));
		assertTrue(od.getProductsAmount(), od.getProductsAmount().equals("20.0"));
		
		//look for new payment record
		//payment type should be GD.
		assertTrue("new payments exist", newOrder.getPayments().size() == 1);
		PaymentsVO payment = (PaymentsVO)newOrder.getPayments().get(0);
		assertTrue("payment is a gift card",payment.getPaymentType().equals("GD"));
		//assertTrue("payment amount is 10", payment.getAmount().equals("10.0"));
		
		assertTrue("tx id is different on new payment", !payment.getApAuth()
				.equals(((PaymentVO)payments.get(0)).getTransactionId()));

		//refund should be for a credit card
		assertTrue("refund is for the credit card", refundsIssued.get(0).getPaymentType().equals("C"));
		double refundAmount = getBillTotal(refundsIssued.get(0).getOrderBill());
		double originalCharge = getBillTotal((OrderBillVO)orderBills.get(0));
		assertTrue("refund amount ("+refundAmount+") equals original charge("+originalCharge+")", 
				refundAmount == originalCharge);
		
	}
	
	/**
	 * UC23227: Original order placed with a CC.  The new order is placed with a GD.
	 * The GD balance will cover the cost of of the new item.
	 * @throws Exception 
	 */
	@Test
	public void testErrorResponse() throws Exception {
		
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);


		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(20.00);
				setTax(5.0);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		final List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("VI");
				setCreditAmount(25.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}}
		);
		//order details
		final List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
			setPaymentVOList(payments);
			setOrderDetailVOList(orderDetails);
		}};
		//refund
		List refunds = Arrays.asList(
				//no refunds for this test
		);
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(10.00);  //less than the original of 20
				setTax(5.00);
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(15.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("abcd"); //this is not valid
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 
		checkGiftCards(payments, newPayments);
		
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		boolean caughtException = false;
		try {
			createNewOrderBO.processRequest();
		} catch (Exception e) {
			
			assertTrue(e.getMessage().equals("mocked out message"));
			caughtException = true;
		} catch (Throwable e) {	 e.printStackTrace();		fail(e.getMessage());	}
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		
		assertTrue("caught exception", caughtException);

		
	}
	

	/**
	 * This will exercise what happens when we get an error while authorizing the new order
	 * 
	 * we'll use a simple case with existing GD order being updated with a GD.
	 * 
	 */
	@Test
	public void testErrorDuringNewAuth() throws Exception {
		
		//order bill
		final List orderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(20.00);
				setBillStatus("Unbilled");
			}}
		);
		//payments
		final List payments = Arrays.asList(
			new PaymentVO(){{
				setPaymentId(1L);
				setOrderGuid("FTD_GUID_1");
				setOrderBill((OrderBillVO)orderBills.get(0));
				setPaymentType("GD");
				setCreditAmount(20.0);
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
			}}
		);
		//order details
		List orderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_1");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId(origProductId);
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(orderBills);
			}}
		);
		//refund
		List refunds = Arrays.asList();
		//setup the input data representing the order in the database being modified
		//setupOrder(origOrderGuid);
		com.ftd.customerordermanagement.vo.OrderVO order = new com.ftd.customerordermanagement.vo.OrderVO() {{
			setOrderGuid("FTD_GUID_1");
			setMasterOrderNumber("M001");
			setPaymentVOList(payments);
		}};
		
		this.newExternalOrderNumber = "C100";
		this.newMasterOrderNumber = "M100/1";
		
		//here is the new product we are buying

		//new order bill
		final List newOrderBills = Arrays.asList(
			new OrderBillVO() {{
				setOrderBillId("1");
				setOrderDetailId(Long.valueOf(origOrderDetailId));
				setProductAmount(30.00);  //less than the original of 20
				setServiceFee(15.0);
				setTax(5.0);
				setBillStatus("Unbilled");
			}}
		);
		List newPayments = Arrays.asList(
			new PaymentVO() {{
				setCreditAmount(50.0);
				setCardNumber("6006492147013900464");
				setGiftCardPin("5530");
				setOrderBill((OrderBillVO)newOrderBills.get(0));
				setBillStatus("Unbilled");
				setPaymentInd("P");
				setPaymentType("GD");
			}}
		);
		//new order details
		List newOrderDetails = Arrays.asList(
			new OrderDetailVO() {{
				setOrderGuid("FTD_GUID_2");
				setOrderDetailId(origOrderDetailId);
				setExternalOrderNumber(origExternalOrderNumber);
				setProductId("BB55");
				setQuantity(1);
				setOrderDispCode("processed");
				setDeliveryDate(new GregorianCalendar(2012, 0, 15));
				setOrderBillVOList(newOrderBills);
			}}
		);
		
		//we need to make sure that the gift cards have enough balance to run these tests 
		translateInputDataForTest(order, orderDetails, orderBills,payments,refunds, newOrderDetails, newPayments);
		checkGiftCards(payments, newPayments);
		
		//here's the trick.  between the time we check balance and place the new order we will auth the gift card so it does not have enough
		// to cover the balance of the new order.
		//we previously authed for 20.  now we will auth for 210. By the time we refund the original 20 auth there will bet 40 left on the card, and the new costs $50
		PaymentVO someOtherPayment  = new PaymentVO(){{
			setCreditAmount(210.0);
			setCardNumber("6006492147013900464");
			setGiftCardPin("5530");
			setPaymentInd("P");
			setPaymentType("GD");
		}};
		this.gbo.authorizePayment(someOtherPayment);
		
		assertTrue("effective balance on the card is 20 250-20-210", gbo.getCurrentBalance(DOMUtil.getDefaultDocument(), someOtherPayment.getCardNumber(), someOtherPayment.getGiftCardPin(), "20") == 20.0);

		//instantiate the thing we are testing and wire in the mocks
		CreateNewOrderBO createNewOrderBO = new CreateNewOrderBO(this.request, this.connection);
		wireUpTheMocks(createNewOrderBO);
		
		// run it sucka!
		boolean caughtException = false;
		try {
			createNewOrderBO.processRequest();
		} catch (Throwable e) {	 
			//we expect to end up here
			caughtException = true;
		}
		
		assertTrue(caughtException);
		
		
		
		// get the results and test that they match the expected
		//OrderDetailVO originalOrderDetail = createNewOrderBO.getOriginalOrderDetail();
		List<RefundVO> refundsIssued = createNewOrderBO.getRefundsIssued();
		OrderVO newOrder = createNewOrderBO.getNewScrubOrder();
		
		//cancel the "refund reversal" a.k.a pre-auth on the original payment
		cancelPreAuth((PaymentVO)payments.get(0));
		
		//also reset the "other" payment
		cancelPreAuth(someOtherPayment);
		
		assertTrue("there is no new order", newOrder == null);
		
	}

	
}
