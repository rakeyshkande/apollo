package com.ftd.messaging.dao;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * @author rlazuk
 *
 */
public class MessageDAOTest {
	
	Connection connection;
	
	@Before
	public void setup() throws TransformerException, IOException, SAXException, ParserConfigurationException, Exception {
        Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
        Properties connProps = new Properties();
        connProps.put("user", "rlazuk");
        connProps.put("password", "");//add your password when testing, remove before checking it in
        this.connection = DriverManager
                .getConnection("jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie", connProps);
        
		
		this.connection = null;
	}

	/**
	 * Test method for {@link com.ftd.messaging.dao.updateFloristSelectionLogId(java.lang.String, java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testUpdateFloristSelectionLogId() throws Exception {
		boolean noExceptionThrown = true;
		try
		{
			MessageDAO messageDAO = new MessageDAO(connection);
			messageDAO.updateFloristSelectionLogId("FTD0000619431", "333");
		}
		catch(Exception e)
		{
			noExceptionThrown = false;
		}
		assertTrue(noExceptionThrown);
	}

}

