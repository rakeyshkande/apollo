package com.ftd.customerordermanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import oracle.jdbc.pool.OracleDataSource;

import org.junit.Before;

import junit.framework.TestCase;

public abstract class OutOfContainerTester extends TestCase
{
    protected static Connection connection;
    static String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie";

    protected static void setupInitialContext()
    {
        Properties env = System.getProperties();
        env.put("java.naming.factory.url", "org.jboss.naming:org.jnp.interfaces");
        env.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        env.put("line.separator", "\n");
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
    
        //env.put("UserTransaction", "CustomUserTransaction");
        System.setProperties(env);			
    }

    protected static Connection setupConnection()
    {
        if (connection == null)
        {
            try
            {
                String driver_ = "oracle.jdbc.driver.OracleDriver";
                String user_ = "osp";
                String password_ = "osp";
    
                Class.forName(driver_);                                                           
                connection = DriverManager.getConnection(database_, user_, password_);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    
        return connection;
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        setupConnection();
    }

    protected static void setupDataSources() throws Exception
    {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");            
            System.setProperty("line.separator", "\n");
            InitialContext ic = new InitialContext();
    
            ic.createSubcontext("jdbc");
            ic.createSubcontext("java:comp");
    
            // Construct DataSource
            OracleDataSource ds = new OracleDataSource();
            ds.setURL(database_);
            ds.setUser("osp");
            ds.setPassword("osp");
    
            ic.bind("jdbc/SCRUBDS", ds);
            ic.bind("jdbc/ORDER_SCRUBDS", ds);
    
    //        UserTransaction ut = (UserTransaction) new CustomUserTransaction();   
    //        ic.rebind("java:comp/UserTransaction",ut);
            
            /* test code
    			DataSource ds1  = (DataSource) ic.lookup("jdbc/SCRUBDS");
    			Connection conn = ds1.getConnection();
    			assertNotNull(conn);
             */
        }

    public OutOfContainerTester()
    {
        super();
    }

    public OutOfContainerTester(String name)
    {
        super(name);
    }

}