/**
 * 
 */
package com.ftd.customerordermanagement.bo;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ftd.customerordermanagement.GiftCardTester;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mo.bo.PaymentBO;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * Test class for various methods primarily of GiftCardBO but also paymentBO and a little of PaymentServiceClient.
 * 
 * SPECIAL NOTE: these tests are designed, mostly by the super class, to run outside the JBoss container. It does this
 * by setting up datasources and an initial context to provide most of what is needed. If you get an error
 * about the CacheConfig and a null object, you probably need to rebuild. These tests require that various statements
 * files from other projects exist in the bin directory. Build puts them there.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:**/cxf.xml"})
public class GiftCardBOPaymeSvcTest extends GiftCardTester {

	private static final String giftCardPin = "1396";
	private static final String giftCardNumber = "6006492147013900423";
	private static final String invalidGiftCardNumber = "6006491286999940184";
	private String orderGuid = "ORD113945";
	private String orderDetailId = "1753875";
	// order guid for order with unbilled gift card payment
	String orderGuidGiftCard = "ORD113941";
	private String orderDetailIdGiftCard = "1753871";
	// order guid for order with gift payment and refund, both unbilled
	String orderGuidRefund = "ORD114503";
	String orderDetailIdRefund = "1761043";
	// order guid for order with billed gift card payment and gift card refund
	String orderGuidPaymentRefundBilled = "ORD113738";
	String orderDetailIdPaymentRefundBilled = "1748894";
	// order guid for order with billed gift card payment (and no refund)
	String orderGuidPaymentBilled = "ORD113776";
    String orderDetailIdPaymentBilled = "1748940";
	private DecimalFormat moneyFormat = new DecimalFormat("#.##");

	
	private String error3 = "We're sorry, the Gift Card number or PIN you provided does not match our records. Please enter the numbers exactly as they appear on the Gift Card, with no spaces or punctuation.";

	/**
	 * Verify that giftCardBO returns the correct current balance. This test is hardcoded with card# and expected balance, which makes
	 * it questionably useful, other than to ensure the service is up.
	 * @throws Exception
	 */
	@Test
	public void testCheckBalance() throws Exception
	{
		//resetTheGiftCards(gbo, payments);
		
        Document errorDocument = DOMUtil.getDefaultDocument();
        
        double balance = gbo.getCurrentBalance(errorDocument, giftCardNumber, giftCardPin, "40");
		assertEquals(50.0, balance);
		assertEquals(errorDocument.getChildNodes().getLength(), 0);
	}
	
	/**
	 * Verify that passing a special card number will generate the expected invalid card number error - have to use this specific card #
	 * @throws Exception
	 */
	@Test
	public void testCheckBalanceInvalidCardNumber() throws Exception
	{		
        Document errorDocument = DOMUtil.getDefaultDocument();

        double balance = gbo.getCurrentBalance(errorDocument, invalidGiftCardNumber, giftCardPin, "40");
		assertEquals(-1.0, balance);
		
		// verify error document
		verifySVSError(errorDocument, error3);
	}

	/**
	 * Verify that validateGiftCard will return the expected values when the card is more than enough for the specified cost.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceExtra() throws Exception
	{		
		String owed = "40";
		String expectedNetOwed = "-10";
		
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuid, giftCardNumber, giftCardPin);
        owed = moneyFormat.format(expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)));
		
        Document returnDocument = gbo.validateGiftCard(orderGuid, orderDetailId, giftCardNumber, giftCardPin, owed);
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNumber);
	}

	/**
	 * Verify that validateGiftCard will return the expected values when the card is exactly enough for the specified cost.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceExact() throws Exception
	{		
		String owed ;
		String expectedNetOwed = "0";
		
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuid, giftCardNumber, giftCardPin);
        owed = moneyFormat.format(expectedEffectedBalance);
		
        Document returnDocument = gbo.validateGiftCard(orderGuid, orderDetailId, giftCardNumber, giftCardPin, owed);
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNumber);
	}

    /**
     * cart has 3 items, one removed, removed item less than GD payment amount, new price = gd payment/balance amount
     * @throws Exception
     */
    @Test
    public void testValidateGiftCardBalanceInsufficientRemovedOrder() throws Exception
    {       
        String owed ;
        String expectedNetOwed = "0"; // item is 32.98, new item cost is 50, but with item removed all of gift card should be available

        String orderGuid = "ORD115086";
        String orderDetailId = "1768978";
        PaymentVO foundPayment=null;
        foundPayment = findGiftCardPaymentForOrder(orderGuid);      
        assertNotNull(foundPayment);
        
        String giftCardNum=foundPayment.getCardNumber();
        String pinToUse = foundPayment.getGiftCardPin();

        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuid, giftCardNum, pinToUse);
        owed = moneyFormat.format(50);
        
        Document returnDocument = gbo.validateGiftCard(orderGuid, orderDetailId, giftCardNum, pinToUse, owed);
        
        // verify return document
        verifyNodeValue(returnDocument, "root/gcc_status", "valid");
        verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
        verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
        verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
        verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);
    }

    /**
     * @throws Exception
     */
    @Test
    public void testValidateGiftCardBalanceSufficientRemovedOrder3Orders() throws Exception
    {       
        String owed ;
        String expectedNetOwed = "0.00"; // item is 32.98, new item cost is 32.98, but with item removed all of gift card should be available
        
        String orderGuid = "ORD115094";
        String orderDetailId = "1768990";
        PaymentVO foundPayment=null;
        foundPayment = findGiftCardPaymentForOrder(orderGuid);      
        assertNotNull(foundPayment);
        
        String giftCardNum=foundPayment.getCardNumber();
        String pinToUse = foundPayment.getGiftCardPin();

        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuid, giftCardNum, pinToUse, 32.98, true);
        owed = moneyFormat.format(32.98);
        
        Document returnDocument = gbo.validateGiftCard(orderGuid, orderDetailId, giftCardNum, pinToUse, owed);
        
        // verify return document
        verifyNodeValue(returnDocument, "root/gcc_status", "valid");
        verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
        verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
        verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
        verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);
    }

    /**
     * @throws Exception
     */
    @Test
    public void testValidateGiftCardBalanceInsufficientRemovedOrder3Orders() throws Exception
    {       
        String owed ;
        String expectedNetOwed = "11.00"; // item is 32.98, new item cost is 43.98, should be 11 short
        
        String orderGuid = "ORD115094";
        String orderDetailId = "1768990";
        PaymentVO foundPayment=null;
        foundPayment = findGiftCardPaymentForOrder(orderGuid);      
        assertNotNull(foundPayment);
        
        String giftCardNum=foundPayment.getCardNumber();
        String pinToUse = foundPayment.getGiftCardPin();

        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuid, giftCardNum, pinToUse, 32.98, true);
        owed = moneyFormat.format(43.98);
        
        Document returnDocument = gbo.validateGiftCard(orderGuid, orderDetailId, giftCardNum, pinToUse, owed);
        
        // verify return document
        verifyNodeValue(returnDocument, "root/gcc_status", "valid");
        verifyNodeValue(returnDocument, "root/amount_owed", Boolean.TRUE.toString());
        verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
        verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
        verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);
    }

	/**
	 * Verify that validateGiftCard will return the expected values when the card is less than enough for the specified cost.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceInsufficient() throws Exception
	{		
		String owed;
		String expectedNetOwed = "10";
		
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuid, giftCardNumber, giftCardPin);

        owed = moneyFormat.format(expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)));
		
        Document returnDocument = gbo.validateGiftCard(orderGuid, orderDetailId, giftCardNumber, giftCardPin, owed);
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.TRUE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNumber);
	}
	
	@Test
	public void testValidateGiftCardItemLessThanGiftCardPayment() throws Exception
	{
	    String currentGDNum = "6006492147013900464";
	    String currentGDPin = "5530";
	    String owed;
	    String expectedNetOwed = "0.00";
        Document returnDocument = DOMUtil.getDefaultDocument();

	    BigDecimal amountOnOrder = new BigDecimal(32.98); // cost of item being updated
	    BigDecimal expectedEffectedBalance;
	    double currentBalance = gbo.getCurrentBalance(returnDocument, currentGDNum, currentGDPin, "");
	    expectedEffectedBalance = amountOnOrder.add(new BigDecimal(currentBalance));
	    owed = moneyFormat.format(expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)));

	    returnDocument = gbo.validateGiftCard("ORD114394", "1760922", currentGDNum, currentGDPin, owed);

        // verify return document
        verifyNodeValue(returnDocument, "root/gcc_status", "valid");
        verifyNodeValue(returnDocument, "root/amount", moneyFormat.format(expectedEffectedBalance));
        verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
        verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
        verifyNodeValue(returnDocument, "root/gift_number", currentGDNum);
	}

    @Test
    public void testValidateGiftCardItemLessThanGiftCardPaymentNewGiftCard() throws Exception
    {
        //String currentGDNum = "6006492147013900464";
        //String currentGDPin = "5530";
        String owed;
        String expectedNetOwed = "0";
        Document returnDocument = DOMUtil.getDefaultDocument();

        ///BigDecimal amountOnOrder = new BigDecimal(32.98); // cost of item being updated - not used in this test
        BigDecimal expectedEffectedBalance;
        double currentBalance = gbo.getCurrentBalance(returnDocument, giftCardNumber, giftCardPin, "");
        expectedEffectedBalance = new BigDecimal(currentBalance);
        owed = moneyFormat.format(expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)));

        returnDocument = gbo.validateGiftCard("ORD114394", "1760922", giftCardNumber, giftCardPin, owed);

        // verify return document
        verifyNodeValue(returnDocument, "root/gcc_status", "valid");
        verifyNodeValue(returnDocument, "root/amount", moneyFormat.format(expectedEffectedBalance));
        verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
        verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
        verifyNodeValue(returnDocument, "root/gift_number", giftCardNumber);
    }

	
	/**
	 * Verify that validateGiftCard will return the expected values when the card is more than enough for the specified cost and a different gift card
	 * has been used on this order. The order guid is hardcoded for this specific case, which while not ideal, is required for now.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceWithGiftCardPaymentSufficient() throws Exception
	{
		String owed = "0";
		String expectedNetOwed = "0";
        String giftCardNum;

        PaymentVO foundPayment=null;
        foundPayment = findGiftCardPaymentForOrder(orderGuidGiftCard);      
        assertNotNull(foundPayment);
        
        giftCardNum=foundPayment.getCardNumber();
        String pinToUse = foundPayment.getGiftCardPin();
								
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuidGiftCard, giftCardNum, pinToUse);
        owed = moneyFormat.format(expectedEffectedBalance);
        Document returnDocument = gbo.validateGiftCard(orderGuidGiftCard, orderDetailIdGiftCard, giftCardNum, pinToUse, owed);
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);		
	}

	
	/**
	 * Verify that validateGiftCard will return the expected values when the card is less than enough for the specified cost and a different gift card
	 * has been used on this order. The order guid is hardcoded for this specific case, which while not ideal, is required for now.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceWithGiftCardPaymentInsufficient() throws Exception
	{		       		
		String expectedNetOwed = "0.01";
		String giftCardNum;

		PaymentVO foundPayment=null;
		foundPayment = findGiftCardPaymentForOrder(orderGuidGiftCard);		
		assertNotNull(foundPayment);
		
		giftCardNum=foundPayment.getCardNumber();
		String pinToUse = foundPayment.getGiftCardPin();

        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuidGiftCard, giftCardNum, pinToUse);
		
		// setup expected and used values
		BigDecimal totalOwed = expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)); // ensure it is more than the effective balance
		
		// validate the gift card
        Document returnDocument = gbo.validateGiftCard(orderGuidGiftCard, orderDetailIdGiftCard, giftCardNum, pinToUse, String.valueOf(totalOwed));
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.TRUE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);		
	}

	/**
	 * Verify that validateGiftCard will return the expected values when:
	 * - the card is less than enough for the specified cost
	 * - the same gift card has been used on this order for an unbilled payment
	 * - the same gift card has an unbilled refund.
	 * The order guid is hardcoded for this specific case, which while not ideal, is required for now.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceWithGiftCardPaymentAndRefundUnbilled() throws Exception
	{		
		String expectedNetOwed = "0.01";
		String giftCardNum;
		
		PaymentVO foundPayment=null;
		foundPayment = findGiftCardPaymentForOrder(orderGuidRefund);		
		assertNotNull(foundPayment);
		giftCardNum=foundPayment.getCardNumber();
		String pinToUse = foundPayment.getGiftCardPin();
		
		BigDecimal expBal = getExpectedBalance(orderGuidRefund, giftCardNum, pinToUse);
        
        BigDecimal totalOwed = expBal.add(new BigDecimal(expectedNetOwed)); // ensure it is more than the effective balance
		
		// validate the gift card
        Document returnDocument = gbo.validateGiftCard(orderGuidRefund, orderDetailIdRefund, giftCardNum, pinToUse, String.valueOf(totalOwed));
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.TRUE.toString());
		verifyNodeValue(returnDocument, "root/amount", moneyFormat.format(expBal));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);		
	}

	/**
	 * Verify that validateGiftCard will return the expected values when:
	 * - the card is less than enough for the specified cost
	 * - the same gift card has been used on this order for a billed payment
	 * - the same gift card has no refund.
	 * The order guid is hardcoded for this specific case, which while not ideal, is required for now.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceWithGiftCardPaymentBilled() throws Exception
	{	
		String expectedNetOwed = "0.01";
		String giftCardNum;

		PaymentVO foundPayment=null;
		foundPayment = findGiftCardPaymentForOrder(orderGuidPaymentBilled);		
		assertNotNull(foundPayment);

		giftCardNum=foundPayment.getCardNumber();
		String pinToUse = foundPayment.getGiftCardPin();
		
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuidPaymentBilled, giftCardNum, pinToUse);
		BigDecimal totalOwed = expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)); // ensure it is more than the effective balance
		
		// validate the gift card
        Document returnDocument = gbo.validateGiftCard(orderGuidPaymentBilled, orderDetailIdPaymentBilled, giftCardNum, pinToUse, String.valueOf(totalOwed));
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.TRUE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);		
	}
	
	
	/**
	 * Verify that validateGiftCard will return the expected values when:
	 * - the card is less than enough for the specified cost
	 * - the same gift card has been used on this order for a billed payment
	 * - the same gift card has a billed refund.
	 * The order guid is hardcoded for this specific case, which while not ideal, is required for now.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceWithGiftCardPaymentRefundBilled() throws Exception
	{	
		String expectedNetOwed = "0.01";
		String giftCardNum;

		PaymentVO foundPayment=null;
		foundPayment = findGiftCardPaymentForOrder(orderGuidPaymentRefundBilled);		
		assertNotNull(foundPayment);

		giftCardNum=foundPayment.getCardNumber();
		String pinToUse = foundPayment.getGiftCardPin();
		
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuidPaymentRefundBilled, giftCardNum, pinToUse);
		BigDecimal totalOwed = expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)); // ensure it is more than the effective balance
		
		// validate the gift card
        Document returnDocument = gbo.validateGiftCard(orderGuidPaymentRefundBilled, orderDetailIdPaymentRefundBilled, 
                giftCardNum, pinToUse, String.valueOf(totalOwed));
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.TRUE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNum);	
	}
	
	
	/**
	 * Verify that validateGiftCard will return the expected values when:
	 * - the card is more than enough for the specified cost
	 * - a different gift card has been used on this order for a billed payment
	 * - a different gift card has a billed refund.
	 * The order guid is hardcoded for this specific case, which while not ideal, is required for now.
	 * Verification includes the amount still owed and the effective balance on the card (which accounts for billed charges/refunds
	 * this order). 
	 * @throws Exception
	 */
	@Test
	public void testValidateGiftCardBalanceWithGiftCardPaymentRefundBilledOtherCard() throws Exception
	{	
		String expectedNetOwed = "-0.01";
		String giftCardNum;

		PaymentVO foundPayment=null;
		foundPayment = findGiftCardPaymentForOrder(orderGuidPaymentRefundBilled);		
		assertNotNull(foundPayment);

		giftCardNum=foundPayment.getCardNumber();
		assertFalse(giftCardNum,equals(giftCardNumber));

		assertNotNull(foundPayment.getCreditAmount());
		
        BigDecimal expectedEffectedBalance = getExpectedBalance(orderGuidPaymentRefundBilled, giftCardNumber, giftCardPin);

		// setup expected and used values
		BigDecimal totalOwed = expectedEffectedBalance.add(new BigDecimal(expectedNetOwed)); // ensure it is less than the effective balance
		
		// validate the gift card
        Document returnDocument = gbo.validateGiftCard(orderGuidPaymentRefundBilled, orderDetailIdPaymentRefundBilled, giftCardNumber, giftCardPin, String.valueOf(totalOwed));
		
		// verify return document
		verifyNodeValue(returnDocument, "root/gcc_status", "valid");
		verifyNodeValue(returnDocument, "root/amount_owed", Boolean.FALSE.toString());
		verifyNodeValue(returnDocument, "root/amount", String.valueOf(expectedEffectedBalance));
		verifyNodeValue(returnDocument, "root/net_owed", expectedNetOwed);
		verifyNodeValue(returnDocument, "root/gift_number", giftCardNumber);	
	}

	// verify that giftcardbo can retrieve the correct gift card number when the specified number is masked
	@Test
	public void testGetUnmaskedGiftCardNumMasked() throws Exception
	{   
	    String giftCardNum;

	    PaymentVO foundPayment=null;
	    foundPayment = findGiftCardPaymentForOrder(orderGuidPaymentBilled);     
	    assertNotNull(foundPayment);

	    giftCardNum=foundPayment.getCardNumber();

	    String gccnum = GiftCardBO.getUnmaskedGiftCardNum("*****ABC", orderGuidPaymentBilled, connection);
	    assertEquals(giftCardNum, gccnum);
	}

	// verify that giftcardbo can retrieve the correct gift card number when the specified number is unmasked
    @Test
    public void testGetUnmaskedGiftCardNumUnmasked() throws Exception
    {   
        String giftCardNum;

        PaymentVO foundPayment=null;
        foundPayment = findGiftCardPaymentForOrder(orderGuidPaymentBilled);     
        assertNotNull(foundPayment);

        giftCardNum=foundPayment.getCardNumber();

        // using a different unmasked parm proves it is returning the parm passed in, not the actual gcc on the order
        String gccnum = GiftCardBO.getUnmaskedGiftCardNum(giftCardNum+"X", orderGuidPaymentBilled, connection);
        assertEquals(giftCardNum+"X", gccnum);
    }

    
    // return the expected balance for a gift card for a particular order
    private BigDecimal getExpectedBalance(String orderguid, String giftCardNum, String pinToUse) throws Exception
    {
        BigDecimal expectedEffectedBalance;
        double amtRefunded = 0.0;
        double amtBilled = 0.0;
        PaymentVO foundPayment;
        foundPayment = findGiftCardPaymentForOrder(orderguid);  
        if (foundPayment != null && foundPayment.getCardNumber().equals(giftCardNum))
        {
            if (foundPayment.getBillStatus().equals("Unbilled"))
                amtBilled = foundPayment.getAmount();
            else
                amtBilled = foundPayment.getCreditAmount();
        }
        double balance = getCardBalance(gbo, giftCardNum, pinToUse);
        expectedEffectedBalance = new BigDecimal(moneyFormat.format(balance));
        
        foundPayment = findGiftCardRefundForOrder(orderguid);
        if (foundPayment != null && foundPayment.getCardNumber().equals(giftCardNum))
        {
            amtRefunded = foundPayment.getDebitAmount();
        }
        
        // setup expected and used values
        BigDecimal expBal = expectedEffectedBalance.add(new BigDecimal(moneyFormat.format(amtBilled)));
        expBal = expBal.subtract(new BigDecimal(moneyFormat.format(amtRefunded)));
        
        ///// may need to factor in the cost of the item being updated - min(item, expbal)
        
        //// needs to account for removed orders
        return expBal;
    }

    private BigDecimal getExpectedBalance(String orderguid, String giftCardNum, String pinToUse, double itemcost, boolean cartHasRemovedItem) throws Exception
    {
        BigDecimal expectedEffectedBalance;
        double amtRefunded = 0.0;
        double amtBilled = 0.0;
        PaymentVO foundPayment;
        foundPayment = findGiftCardPaymentForOrder(orderguid);  
        if (foundPayment != null && foundPayment.getCardNumber().equals(giftCardNum))
        {
            if (foundPayment.getBillStatus().equals("Unbilled"))
                amtBilled = foundPayment.getAmount();
            else
                amtBilled = foundPayment.getCreditAmount();
        }
        double balance = getCardBalance(gbo, giftCardNum, pinToUse);
        expectedEffectedBalance = new BigDecimal(moneyFormat.format(balance));
        
        foundPayment = findGiftCardRefundForOrder(orderguid);
        if (foundPayment != null && foundPayment.getCardNumber().equals(giftCardNum))
        {
            amtRefunded = foundPayment.getDebitAmount();
        }
        
        // setup expected and used values
        BigDecimal expBal = expectedEffectedBalance.add(new BigDecimal(moneyFormat.format(amtBilled)));
        expBal = expBal.subtract(new BigDecimal(moneyFormat.format(amtRefunded)));

        if (cartHasRemovedItem)
        expBal = new BigDecimal(moneyFormat.format(Math.min(itemcost, expBal.doubleValue())));
        
        return expBal;
    }

    // find the giftcard  payment for an order, i.e. not a refund
	private PaymentVO findGiftCardPaymentForOrder(String orderGuid) throws Exception
	{
		PaymentVO foundPayment = null;
		List<PaymentVO> payments = findAllGiftCardPaymentsForOrder(orderGuid);
		if (payments == null)
		    return null;
		
		for (PaymentVO payment : payments)
		{
			if (payment.getPaymentInd().equals("P"))
			{
				assertNull(foundPayment);
				foundPayment = payment;
			}
		}
		return foundPayment;
	}

	// verify SVS errors coming out of giftcardBO.validateGiftCard
	private void verifySVSError(Document errorDocument, String expectedError) throws Exception
	{
		Node node;
		node = DOMUtil.selectSingleNode(errorDocument,"root/gcc_status/text()");
		assertNotNull(node);
		assertEquals("invalid", node.getNodeValue());

		node = DOMUtil.selectSingleNode(errorDocument,"root/total_owed/text()");
		assertNotNull(node);
		assertEquals("40", node.getNodeValue());
		
		node = DOMUtil.selectSingleNode(errorDocument,"root/message/text()");
		assertNotNull(node);
		assertEquals(expectedError, node.getNodeValue());
	}

}
