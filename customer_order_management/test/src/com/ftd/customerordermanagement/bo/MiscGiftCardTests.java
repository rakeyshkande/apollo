/**
 * 
 */
package com.ftd.customerordermanagement.bo;


import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.GiftCardTester;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mo.bo.PaymentBO;

/**
 * Test class for various methods related to gift card, payment service, etc.
 * 
 * SPECIAL NOTE: these tests are designed, mostly by the super class, to run outside the JBoss container. It does this
 * by setting up datasources and an initial context to provide most of what is needed. If you get an error
 * about the CacheConfig and a null object, you probably need to rebuild. These tests require that various statements
 * files from other projects exist in the bin directory. Build puts them there.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:**/cxf.xml"})
public class MiscGiftCardTests extends GiftCardTester {

	private static final String giftCardPin = "1396";
	private static final String giftCardNumber = "6006492147013900423";
	private String orderGuid = "ORD113945";
	private String orderDetailId = "1753875";
	// order guid for order with unbilled gift card payment
	String orderGuidGiftCard = "ORD113941";
	private String orderDetailIdGiftCard = "1753871";

	// simple test to ensure paymentBO.getGenPayInfo works - more verification could be done
    @Test
	public void testPaymentBONonGDOrder() throws Exception
	{
	    PaymentBO pbo = new PaymentBO(connection);
	    HashMap map = pbo.getGenPayInfo(orderGuid, orderDetailId, "2333");
	    assertNotNull(map);
	}
	
    // simple test to ensure paymentBO.getGenPayInfo works - more verification could be done
    @Test
    public void testPaymentBOGDOrder() throws Exception
    {
        PaymentBO pbo = new PaymentBO(connection);
        HashMap map = pbo.getGenPayInfo(orderGuidGiftCard, orderDetailIdGiftCard, "2333");
        assertNotNull(map);
    }
    
    // simple test to ensure paymentBO.getLastGiftCardUsedDoc finds the gift card used on an order
    // could do more tests, e.g. when global switch is off, etc.
    @Test
    public void testPaymentBOGetLastGD() throws Exception
    {
        PaymentBO pbo = new PaymentBO(connection);
        Document doc = pbo.getLastGiftCardUsedDoc(connection, orderGuidGiftCard);
        assertNotNull(doc);
        verifyNodeValue(doc, "LAST_GIFTCARDS/LAST_GIFTCARD/gift_card_number", "0464");
    }
    
    
    /*** PSC tests ***/
    
    // verify that paymentservicelcient checkBalanceAndAuthorizeExactAmount authorizes when
    // the amount exactly matches the balance
    @Test
    public void testPSCCheckBalanceAndAuth() throws Exception
    {        
        double amtAboveBalance;
        amtAboveBalance = .00;
        boolean expectToPass = true;
        String giftCardToUse = giftCardNumber;
        String pinToUse = giftCardPin;
        
        verifyCheckBalanceAndAuth(amtAboveBalance, expectToPass, giftCardToUse, pinToUse);
    }

    // verify that paymentservicelcient checkBalanceAndAuthorizeExactAmount authorizes when
    // the amount exactly matches the balance + 1 cent
    @Test
    public void testPSCCheckBalanceAndAuthPlusOne() throws Exception
    {        
        double amtAboveBalance;
        amtAboveBalance = -.01;
        boolean expectToPass = true;
        String giftCardToUse = giftCardNumber;
        String pinToUse = giftCardPin;
        
        verifyCheckBalanceAndAuth(amtAboveBalance, expectToPass, giftCardToUse, pinToUse);
    }

    // verify that paymentserviceclient checkBalanceAndAuthorizeExactAmount does not authorize when
    // the amount exactly matches the balance - 1 cent
    @Test
    public void testPSCCheckBalanceAndAuthFail() throws Exception
    {        
        double amtAboveBalance;
        amtAboveBalance = .01;
        boolean expectToPass = false;
        String giftCardToUse = giftCardNumber;
        String pinToUse = giftCardPin;
        
        verifyCheckBalanceAndAuth(amtAboveBalance, expectToPass, giftCardToUse, pinToUse);
    }

    // verifying change to dao
    @Test
    public void testFindPaymentRefundrecord() throws Exception
    {
        String orderguid = "ORD114825";
        PaymentVO payment = findGiftCardRefundForOrder(orderguid);
        assertNotNull(payment);
        assertEquals(1553713, payment.getPaymentId());
    }
    
    // verifying change to dao
    @Test
    public void testFindGiftCardPayments() throws Exception
    {
        String orderguid = "ORD115000"; // order with 2 payments, P and R, for GD. 
        double expectedamt = 50.0;
        
        List <PaymentVO> payments = findAllGiftCardPaymentsForOrder(orderguid);
        assertNotNull(payments);
        assertEquals(2, payments.size());
        for (PaymentVO payment : payments)
        {
            assertNotNull(payment.getAmount());
            if (payment.getPaymentInd().equals("P"))
                assertEquals(expectedamt, payment.getAmount());
        }
    }
    
    // verifying new dao/stored proc, mostly just verifying interface.
    @Test
    public void testUpdateRemovedOrderPayment() throws Exception
    {
        try
        {                   
            FTDCommonUtils.recalculateRemovedOrders(connection, "ORD114998");
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
    }

    /*removed dao method
    // verifying new dao/stored proc, mostly just verifying interface.
    @Test
    public void testCartHasRemovedOrder() throws Exception
    {
        String orderguid = "ORD115086";
        OrderDAO dao = new OrderDAO(connection);
        String flag = dao.cartHasRemovedOrder(orderguid);
        assertEquals("Y", flag);    
    }*/

}
