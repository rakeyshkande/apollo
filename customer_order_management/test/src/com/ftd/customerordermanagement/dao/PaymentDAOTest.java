/**
 * 
 */
package com.ftd.customerordermanagement.dao;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

/**
 * @author cjohnson
 *
 */
public class PaymentDAOTest {
	
	Connection connection;
	
	@Before
	public void setup() throws TransformerException, IOException, SAXException, ParserConfigurationException, Exception {
        Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
        Properties connProps = new Properties();
        connProps.put("user", "cjohnson");
        connProps.put("password", "grill");
        this.connection = DriverManager
                .getConnection("jdbc:oracle:thin:@adonis.ftdi.com:1521:ernie", connProps);
        
		
		this.connection = null;
	}

	/**
	 * Test method for {@link com.ftd.customerordermanagement.dao.PaymentDAO#getPaymentsForOrder(java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetPaymentsForOrder() throws Exception {
		
		PaymentDAO paymentDAO = new PaymentDAO(connection);
		List<PaymentVO> payments = paymentDAO.getPaymentsForOrder("FTD_GUID_1");
		
		assertTrue(payments.size() > 0);
		
	}

}
