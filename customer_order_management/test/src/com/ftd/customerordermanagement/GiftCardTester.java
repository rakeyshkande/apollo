package com.ftd.customerordermanagement;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.mock.web.MockHttpServletRequest;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.ps.common.constant.PaymentServiceConstants;
import com.ftd.ps.webservice.AmountVO;
import com.ftd.ps.webservice.CardVO;
import com.ftd.ps.webservice.PaymentRequest;
import com.ftd.ps.webservice.PaymentResponse;
import com.ftd.ps.webservice.PaymentServiceClient;


//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations={"classpath:**/cxf.xml"})
public abstract class GiftCardTester extends OutOfContainerTester {
    
    protected static GiftCardBO gbo;

    @BeforeClass
    public static void init() throws Exception
    {
    	setupConnection();
    	setupInitialContext();			
    	setupDataSources();
    	gbo = createGiftCardBO();		
    }

    private static GiftCardBO createGiftCardBO() throws Exception
    {
    	GiftCardBO gbo = new GiftCardBO(connection);
    	
    	return gbo;
    }

    public HttpServletRequest buildUpdateProductDetailRequest(String orderDetailID, String securityToken) throws Exception
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        OrderDetailVO detail = new OrderDetailVO();
        OrderVO order = new OrderVO();
        OrderDAO dao = new OrderDAO(connection);
        detail = dao.getOrderDetail(orderDetailID);
        order = dao.getOrder(detail.getOrderGuid());
        CachedResultSet rs = dao.getOrderRecipientInfo(orderDetailID);
        
        UpdateOrderDAO udao = new UpdateOrderDAO(connection);
        CachedResultSet rs2 = udao.getProductInfo(detail.getProductId());
        rs2.next();
  
        SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );

        /****************************************************************************************
         *  set specific parameters
         ***************************************************************************************/

        //retrieve the buyer_full_name
        request.setParameter(COMConstants.BUYER_FULL_NAME, "");

        //retrieve the category Index
        request.setParameter(COMConstants.CATEGORY_INDEX, "");

        //retrieve the company id
        request.setParameter(COMConstants.COMPANY_ID, order.getCompanyId());
        request.setParameter(COMConstants.CUSTOMER_ID, String.valueOf(order.getCustomerId()));
        request.setParameter(COMConstants.DELIVERY_DATE, dateFormat.format(detail.getDeliveryDate().getTime()));
        //request.setParameter(COMConstants.DELIVERY_DATE_RANGE_END, null);
        request.setParameter(COMConstants.EXCEPTION_START_DATE, "");
        request.setParameter(COMConstants.EXCEPTION_END_DATE, "");
        request.setParameter(COMConstants.EXTERNAL_ORDER_NUMBER, detail.getExternalOrderNumber());
        //request.setParameter(COMConstants.IGNORE_ERROR, null);
        request.setParameter(COMConstants.MASTER_ORDER_NUMBER, order.getMasterOrderNumber());
        request.setParameter(COMConstants.OCCASION, detail.getOccasion());
        request.setParameter(COMConstants.OCCASION_TEXT, "");
        request.setParameter(COMConstants.ORDER_DETAIL_ID, detail.getOrderDetailId());
        request.setParameter(COMConstants.ORDER_GUID, detail.getOrderGuid());
        request.setParameter(COMConstants.ORIG_PRODUCT_ID, detail.getProductId());
        request.setParameter(COMConstants.ORIGIN_ID, order.getOriginId());
        request.setParameter(COMConstants.PRODUCT_AMOUNT, Double.valueOf(order.getProductTotal()).toString());
        //request.setParameter(COMConstants.PRICE_POINT_ID, order);
        request.setParameter(COMConstants.PRODUCT_ID, detail.getProductId());
        request.setParameter(COMConstants.PRODUCT_SUB_TYPE, rs2.getString("productSubType"));
        request.setParameter(COMConstants.PRODUCT_TYPE, rs2.getString("productType"));
        rs.next();         
        String city = rs.getString("CITY");
        assertNotNull(city);
        request.setParameter(COMConstants.RECIPIENT_CITY, city);
        request.setParameter(COMConstants.RECIPIENT_COUNTRY, detail.getRecipientCountry());
        request.setParameter(COMConstants.RECIPIENT_STATE, rs.getString("STATE"));
        request.setParameter(COMConstants.RECIPIENT_ZIP_CODE, rs.getString("ZIP_CODE"));
        request.setParameter(COMConstants.SHIP_METHOD, detail.getShipMethod());
        //request.setParameter(COMConstants.SHIP_METHOD_CARRIER, vo);
        //request.setParameter(COMConstants.SHIP_METHOD_FLORIST, order);
        request.setParameter(COMConstants.SIZE_INDICATOR, detail.getSizeIndicator());
        request.setParameter(COMConstants.SOURCE_CODE, detail.getSourceCode());
        //request.setParameter(COMConstants.SUBCODE, order);
        //request.setParameter(COMConstants.SUBSTITUTION_INDICATOR, order);
        //request.setParameter("variable_price_selected", order);
        //request.setParameter(COMConstants.ZIP_SUNDAY_FLAG, order);
        
        request.setParameter(COMConstants.SEC_TOKEN, securityToken);
        
        HashMap parameters = new HashMap();
        request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);
        return request;
    }
    
    @Before
    public void setUp() throws Exception
    {
    	super.setUp();
    }

    public HttpServletRequest buildApplyGenPaymentRequestGD(String orderDetailID, String securityToken, String gdnum, String gdpin) 
    throws Exception
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        OrderDetailVO detail = new OrderDetailVO();
        OrderDAO dao = new OrderDAO(connection);
        detail = dao.getOrderDetail(orderDetailID);

        OrderVO order = new OrderVO();
        order = dao.getOrder(detail.getOrderGuid());

        String totalAmount = Double.valueOf(order.getOrderTotal()).toString();
        
        request.setParameter(COMConstants.ORDER_DETAIL_ID, orderDetailID);
        request.setParameter(COMConstants.ORDER_GUID, detail.getOrderGuid());
        request.setParameter("pay_method", COMConstants.GIFT_CERTIFICATE);
        request.setParameter("appr_gcc_num", gdnum);
        request.setParameter("gcc_pin", gdpin);
        request.setParameter("appr_gcc_amt", totalAmount);
        request.setParameter("charge_amt", totalAmount);
        request.setParameter(COMConstants.GD_OR_GC, COMConstants.GIFT_CARD_PAYMENT);
        
        request.setParameter(COMConstants.SEC_TOKEN, securityToken);
        
        return request;
    }

    protected void verifyCheckBalanceAndAuth(double amtAboveBalance, boolean expectToPass, String giftCardToUse, String pinToUse) throws Exception
    {
        PaymentRequest req = getAuthenticatedPaymentRequest();
        initSvc(req, giftCardToUse, pinToUse);
        double balance = getCardBalance(gbo, giftCardToUse, pinToUse);
        assertTrue(balance > 0);
        req.getAmountVO().setAmount(balance+amtAboveBalance);
    
        PaymentServiceClient psc = GiftCardBO.getPaymentServiceClient();
        PaymentResponse resp = psc.checkBalanceAndAuthorizeForExactAmount(req);
        try
        {
            assertNotNull(resp);
            if (!expectToPass)
            {
                if (balance == 0)
                {
                    // 0 balance give different error
                    assertEquals("05", resp.getResponseCode());
                    assertEquals(PaymentServiceConstants.PS_ERROR_SVS_FAILURE_CODE, resp.getErrorCode());          
                }
                else
                {
                    assertEquals("-1", resp.getResponseCode());
                    assertEquals(PaymentServiceConstants.PS_ERROR_INSUFFICIENT_FUNDS, resp.getErrorCode());
                }        
            }
            else
            {
                assertEquals("01", resp.getResponseCode());
            }
        }
        finally
        {
            AmountVO amountvo = new AmountVO();
            amountvo.setAmount(new Double(0));
            amountvo.setCurrency("USD");
            req.setAmountVO(amountvo);
    
            psc.cancelAuthorization(req);
            double balance2 = getCardBalance(gbo, giftCardToUse, pinToUse);
            assertEquals(balance, balance2);
    
        }
    }

    private PaymentRequest getAuthenticatedPaymentRequest() throws Exception
    {
        PaymentRequest req = new PaymentRequest();
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    
        req.setClientUserName(configUtil.getSecureProperty("SERVICE", "SVS_CLIENT"));
        req.setClientPassword(configUtil.getSecureProperty("SERVICE", "SVS_HASHCODE"));
          
        return req;
    }

    protected double getCardBalance(GiftCardBO gbo, String  giftCardNumber, String giftCardPin) throws Exception
    {
    	Document errorDocument = DOMUtil.getDefaultDocument();
        double balance = gbo.getCurrentBalance(errorDocument, giftCardNumber, giftCardPin, "0");
    	return balance;
    }

    protected List<PaymentVO> findAllGiftCardPaymentsForOrder(String orderGuid) throws Exception
    {
    	OrderDAO odao = new OrderDAO(connection);
    	List<PaymentVO> payments = odao.getGiftCardPayments(orderGuid);
    
    	return payments;
    }

    protected PaymentVO findGiftCardRefundForOrder(String orderGuid) throws Exception
    {
    	PaymentVO foundPayment = null;
    	List<PaymentVO> payments = findAllGiftCardPaymentsForOrder(orderGuid);
        if (payments == null)
            return null;
    
        for (PaymentVO payment : payments)
    	{
    		if (payment.getPaymentInd().equals("R"))
    		{
    			assertNull(foundPayment);
    			foundPayment = payment;
    		}
    	}
    	return foundPayment;
    }

    // some tests will fail when checking $$ values since doubles like to truncate their trailing zeroes, e.g.
    // 150.80 != 150.8. Further formatting and/or casting could help with this.
    protected void verifyNodeValue(Document returnDocument, String nodeName, Object expectedNodeValue) throws Exception
    {
    	Node node;
    	node = DOMUtil.selectSingleNode(returnDocument,nodeName+"/text()");
    	assertNotNull(node);
    	assertEquals(expectedNodeValue, node.getNodeValue());
    }

    public void initSvc(PaymentRequest reqvo, String cardNumber, String pin) throws Exception
    {
        IdGeneratorUtil idUtil = new IdGeneratorUtil();
        reqvo.setTransactionId(idUtil.generateId(new IdRequestVO("APOLLO", "SVS_TRANS_ID"), connection));
        
        CardVO cardvo = new CardVO();
        cardvo.setCardCurrency(GiftCardBO.PAYSVC_CURRENCY);
        cardvo.setCardNumber(cardNumber);
        cardvo.setPinNumber(pin);
        reqvo.setCardVO(cardvo);
        
        AmountVO amountvo = new AmountVO();
        amountvo.setAmount(new Double(0));
        amountvo.setCurrency(GiftCardBO.PAYSVC_CURRENCY);
        reqvo.setAmountVO(amountvo);
    }
}

