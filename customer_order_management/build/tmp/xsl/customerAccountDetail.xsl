<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="customerAccountDetail">
<div id="customerAccountDiv">
	<form name="caForm" method="post">
	 <xsl:if test="count(CSCI_ORDERS/CSCI_ORDER/child::*) = 0">
		    <input type="hidden" name="cai_number_of_carts" id="cai_number_of_carts" value="{key('pageData','cai_number_of_carts')/value}"/>
			<input type="hidden" name="cai_number_of_orders" id="cai_number_of_orders" value="{key('pageData','cai_number_of_orders')/value}"/>
			<input type="hidden" name="cai_locked_indicator" id="cai_locked_indicator" value="{key('pageData','cai_locked_indicator')/value}"/>
			<input type="hidden" name="cai_current_page" id="cai_current_page" value="{key('pageData','cai_current_page')/value}"/>
			<input type="hidden" name="cai_total_pages" id="cai_total_pages" value="{key('pageData','cai_total_pages')/value}"/>
			<input type="hidden" name="cai_vip_customer" id="cai_vip_customer" value="{key('pageData','cai_vip_customer')/value}"/>
			<input type="hidden" name="max_search_results" id="max_search_results" value="{key('pageData','max_search_results')/value}"/>
			<input type="hidden" name="cos_search_customer_count" id="cos_search_customer_count" value="{key('pageData','cos_search_customer_count')/value}"/>

			<input type="hidden" name="csr_id" id="csr_id" value="{key('pageData','csr_id')/value}"/>
			<input type="hidden" name="begin_index" id="begin_index" value="{key('pageData','begin_index')/value}"/>
			<input type="hidden" name="end_index" id="end_index" value="{key('pageData','end_index')/value}"/>
			<input type="hidden" name="max_records_allowed" id="max_records_allowed" value="{key('pageData','max_records_allowed')/value}"/>
			<input type="hidden" name="cai_more_email_indicator" id="cai_more_email_indicator" value="{key('pageData','cai_more_email_indicator')/value}"/>
			<input type="hidden" name="show_previous" id="show_previous" value="{key('pageData','cai_show_previous')/value}"/>
			<input type="hidden" name="cai_start_position" id="cai_start_position" value="{key('pageData','cai_start_position')/value}"/>
			<input type="hidden" name="scrub_url" id="scrub_url" value="{key('pageData','scrub_url')/value}"/>
			<input type="hidden" name="show_next" id="show_next" value="{key('pageData','cai_show_next')/value}"/>
			<input type="hidden" name="show_first" id="show_first" value="{key('pageData','cai_show_first')/value}"/>
			<input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
			<input type="hidden" name="recipient_search" id="recipient_search" value="{key('pageData','recipient_search')/value}"/>
			<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
 			<input type="hidden" name="lossPreventionPermission" id="lossPreventionPermission" value="{key('pageData','lossPreventionPermission')/value}"/>
 			<input type="hidden" name="customerHoldPermission" id="customerHoldPermission" value="{key('pageData','customerHoldPermission')/value}"/>
			<input type="hidden" name="vipCustUpdatePermission" id="vipCustUpdatePermission" value="{key('pageData','vipCustUpdatePermission')/value}"/> 
			<input type="hidden" name="vipCustomerMessage" id="vipCustomerMessage" value="{key('pageData','vipCustomerMessage')/value}"/>
			<input type="hidden" name="vipCustUpdateDeniedMessage" id="vipCustUpdateDeniedMessage" value="{key('pageData','vipCustUpdateDeniedMessage')/value}"/>
			<input type="hidden" name="skip_com"  id="skip_com" value="{$bypassCOM}" />
            <input type="hidden" name="bypass_com"  id="bypass_com" value="{$bypassCOM}" />

                        <xsl:call-template name="securityanddata"/>
                        <xsl:call-template name="decisionResultData"/>
			<xsl:call-template name="addHeader"/>
  </xsl:if>
   <iframe id="caUpdateCustomer" width="0px" height="0px" border="0" class="hidden"/>
	<table id="customerAccountDetail" align="center" class="mainTable" width="98%"  cellpadding="0" cellspacing="2">
		<tr>
			<td>
				<table border="0" class="innerTable" width="100%" cellpadding="0" cellspacing="2">
					<tr>
						<td>
							<table border="0" width="100%">
								<tr>
									<td>
                   <xsl:variable name="orderHeld" select="//CSCI_CARTS_HOLD/CSCI_CART_HOLD/customer_hold_description"/>
                   <xsl:if test="$orderHeld != $YES">
					  						&nbsp;
                    		<img src="images/IconLock.gif"/>
										</xsl:if>
						   &nbsp;
										<span class="Label">Customer #:</span>
						   &nbsp; &nbsp;
										<xsl:value-of select="$id"/>
									</td>
									<td>&nbsp;
										<span class="Label">Account Creation Date:</span>
						   &nbsp; &nbsp;
										<xsl:value-of select="$cai/customer_created_on"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="banner" width="100%">Customer Information</td>
					</tr>
					<tr>
						<td>
							<table border="0" id="ca" cellpadding="0" cellspacing="3" width="100%">
								<tr>
									<td class="LabelRight" width="10%" style="vertical-align:top">Name:
									</td>
									<td width="30%">
										<span style="WIDTH: 50px; VERTICAL-ALIGN: top; white-space: nowrap;">
										<xsl:value-of select="$cai/customer_first_name"/>
										 &nbsp;
										<xsl:value-of select="$cai/customer_last_name"/>
										</span>
									</td>
                           <td rowspan="5" align="left" valign="top" width="200" style="vertical-align:top">
                                 <!-- Absolutely position image container to work around an IE bug where
                                      stuff inside a TD with rowspan blows up the row heights.  This should work for up to 6 images -->     
                                 <span style="position:absolute;width:200px;display:block;">
	                                 <xsl:choose>
	                                 <xsl:when test="key('pageData','cai_vip_customer')/value='Y'">
	                                 <span class="ErrorMessage" style="WIDTH: 200px; font-weight:normal; DISPLAY: inline-block">   
	                                     <script type="text/javascript">
	                                       document.write(document.getElementById('vipCustomerMessage').value);
	                                       localStorage.setItem('isCustomerVip','Y');
	                                     </script>
	                                 </span>
	                                </xsl:when>
	                                <xsl:otherwise>
	                                    <script type="text/javascript">
	                                       localStorage.setItem('isCustomerVip','N');
	                                     </script>
	                                </xsl:otherwise>
	                                </xsl:choose>

                                   <xsl:for-each select="CUSTOMER_PREFERRED_PARTNER/PREFERRED_PARTNER">
												 <!-- <img src="images/{display_name/text()}_logo.gif"/>&nbsp; -->
                                                 <xsl:choose>
                                                  <xsl:when test="position()=3">
                                                    <img src="images/{display_name/text()}_logo.gif"/><br/>
                                                 </xsl:when>
 												<xsl:otherwise>
                                                 <img src="images/{display_name/text()}_logo.gif"/>&nbsp;
                                                 </xsl:otherwise>
												</xsl:choose>
                                              </xsl:for-each>
                                              <xsl:if test="key('pageData','IS_PC_CUSTOMER')/value='Y'">
                                              <img src="images/ftd_premier_circle_logo.gif"/>
                                              </xsl:if>
                                              
                                 </span>
									</td>
									<td class="LabelRight" width="15%" style="vertical-align:top">Phone 1:</td>
									<td width="10%" style="vertical-align:top">
										<xsl:if test="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Day']/../customer_phone_number != ''">
											<script type="text/javascript">
											if(isUSPhoneNumber('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Day']/../customer_phone_number"/>')){
												document.write(reformatUSPhone('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Day']/../customer_phone_number"/>'));
											}else{
												document.write('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Day']/../customer_phone_number"/>');
											}
											</script>
										 </xsl:if>
									</td>
									<td width="2%" style="font-weight: bold; text-align:left; vertical-align:top">Ext:</td>
									<td style="vertical-align:top">
										<xsl:value-of select="$cai_phone/customer_phone_type[. = 'Day']/../customer_extension"/>
									</td>
								</tr>
								<tr>
									<td class="LabelRight" style="vertical-align:top">Business:
									</td>
									<td style="vertical-align:top">
										<xsl:value-of select="$cai/business_name"/>
									</td>
									<td class="LabelRight" style="vertical-align:top">Phone 2:</td>
									<td style="vertical-align:top">
										<xsl:if test="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Evening']/../customer_phone_number != ''">
											<script type="text/javascript">
											if(isUSPhoneNumber('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Evening']/../customer_phone_number"/>')){
												document.write(reformatUSPhone('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Evening']/../customer_phone_number"/>'));
											}else{
												document.write('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Evening']/../customer_phone_number"/>');
											}
											</script>
										 </xsl:if>
									</td>
									<td style="font-weight: bold; text-align:left; vertical-align:top">Ext:</td>
									<td style="vertical-align:top">
										<xsl:value-of select="$cai_phone/customer_phone_type[. = 'Evening']/../customer_extension"/>
									</td>
								</tr>
								<tr>
									<td class="LabelRight" style="vertical-align:top">Address 1:</td>
									<td style="vertical-align:top">
										<xsl:value-of select="$cai/customer_address_1"/>
									</td>
                                                                        <td class="LabelRight">Hold Information:</td>
									<td colspan="3" style="vertical-align:top">
					               <xsl:choose>
					                 <xsl:when test="contains(CAI_HOLDS/CAI_HOLD/customer_hold_description,'Fraud') or contains(CAI_HOLDS/CAI_HOLD/customer_hold_description,'FRAUD')">
					                   LP Review
					                 </xsl:when>
					                 <xsl:otherwise>
					                   <xsl:value-of select="CAI_HOLDS/CAI_HOLD/customer_hold_description"/> 
					                 </xsl:otherwise>
					               </xsl:choose>
									</td>
								</tr>
								<tr>
									<td class="LabelRight" style="vertical-align:top">Address 2:</td>
									<td style="vertical-align:top">
										<xsl:value-of select="$cai/customer_address_2"/>
									</td>
                                    <td></td>
									<td colspan="3"></td>
								</tr>
								
								<tr>
									<td class="LabelRight" style="vertical-align:top">City, State, Zip:</td>
									<td style="vertical-align:top">
										<xsl:choose>
							              <xsl:when test="$cai/customer_state != ''">
							                  <xsl:value-of select="$cai/customer_city"/>,
							                  <xsl:value-of select="$cai/customer_state"/>
							                  &nbsp;&nbsp;
							                </xsl:when>
							                <xsl:otherwise>
							                   <xsl:value-of select="$cai/customer_city"/>
							                </xsl:otherwise>
							              </xsl:choose>
							                <xsl:value-of select="$cai/customer_zip_code"/>
							                  &nbsp;&nbsp;
							              <xsl:if test="$cai/customer_country != 'US'">
										  		<strong>
							                    	<xsl:value-of select="$cai/country_name"/>                   	 
							                    </strong>
										  </xsl:if>	
									</td>
                                                                       <td align="right">
										 <xsl:if test="count(CAI_MEMBERSHIPS/CAI_MEMBERSHIP) >= 1">
										 	<script type="text/javascript">
										 		var membershipObj = new Array();
												<xsl:for-each select="CAI_MEMBERSHIPS/CAI_MEMBERSHIP">
												   var membershipData = new Array();
													<!--membershipObj[<xsl:value-of select="position()"/>] = '<xsl:value-of select="membership_id"/>';-->
													membershipData[1] = "<xsl:value-of select="membership_number"/>";
													membershipData[2] = "<xsl:value-of select="membership_type"/>";
													membershipData[3] = "<xsl:value-of select="membership_first_name"/>";
													membershipData[4] = "<xsl:value-of select="membership_last_name"/>";
												    membershipObj[<xsl:value-of select="position()"/>] = membershipData;
												</xsl:for-each>
										 	</script>
											<a href="#" class="textlink" id="membershipInfoLink" onclick="doMembershipDisplay(membershipObj)" >Membership Info</a>
									  </xsl:if>
									</td>
									<td colspan="4" style="vertical-align:top">

									</td>
								</tr>
                                                                
								<tr>
									<td class="LabelRight" style="vertical-align:top">Email Address:
									</td>
									<td style="vertical-align:top">
									  <xsl:choose>
                                                                              <xsl:when test="count(//CAI_EMAILS/CAI_EMAIL) > 1">
                                                                                  <select id="email_address">
                                                                                      <xsl:for-each select="CAI_EMAILS/CAI_EMAIL">
                                                                                          <option>
                                                                                              <xsl:if test="email_address != ''">
							                                          <xsl:value-of select="email_address"/>&nbsp;
							                                          <xsl:if test="email_subscribe_status != ''">|&nbsp;</xsl:if>
							                                      </xsl:if>
							                                      <xsl:if test="email_subscribe_status != ''">
							                                          <xsl:value-of select="email_subscribe_status"/>&nbsp;
							                                      </xsl:if>
                                                                                          </option>
                                                                                      </xsl:for-each>
                                                                                  </select>
                                                                              </xsl:when>
						                              <xsl:when test="count(//CAI_EMAILS/CAI_EMAIL) = 0">
						                      	          <xsl:if test="boolean(key('pageData','cai_more_email_indicator')/value = $YES)">
						                                      <div style="padding-left:20%;display:block"> <a id="caMoreLink" href="javascript:doReturnEmailAddresses();" class="textlink" >More...</a></div>
						                                  </xsl:if>
						                              </xsl:when>
                                                                              <xsl:otherwise>
						                                  <xsl:if test="//CAI_EMAILS/CAI_EMAIL/email_address != ''">
						                        	      <xsl:value-of select="//CAI_EMAILS/CAI_EMAIL/email_address"/>&nbsp;
						                                      <xsl:if test="//CAI_EMAILS/CAI_EMAIL/email_subscribe_status != ''">|&nbsp;</xsl:if>
						                                  </xsl:if>
						                                  <xsl:if test="//CAI_EMAILS/CAI_EMAIL/email_subscribe_status != ''">
						                        	      <xsl:value-of select="//CAI_EMAILS/CAI_EMAIL/email_subscribe_status"/>&nbsp;
						                                  </xsl:if>
                                                                              </xsl:otherwise>
									  </xsl:choose>
                                                                          <xsl:if test="boolean(key('pageData','cai_more_email_indicator')/value = $YES) and count(//CAI_EMAILS/CAI_EMAIL) != 0">
                      	                                                      &nbsp;&nbsp;
                                                                              <a id="caMoreLink" href="javascript:doReturnEmailAddresses();" class="textlink">More...</a>
                                                                          </xsl:if>
									</td>
                                                                        
                                                                        <td align="right" style="vertical-align:top" colspan="2">
                                                                        <script type="text/javascript">
                                                                                var customerId = "<xsl:value-of select="$id"/>";
                                                                        </script>
                                                                           <!-- <xsl:if test="count(FSM_SUMMARY/SUMMARY_INFO/display_name) >0 and count(SERVICES/SERVICES_INFO/email_address) > 0 ">-->
                                                                                <a href="javascript:doServicesInfoAction(customerId);" class="textlink" id="servicesInfoLink">Services Info</a> 
                                                                           <!-- </xsl:if> -->
                                                                        </td>
                                                                        
                                                                        
                                                                        <td colspan="5"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="banner" colspan="6">Order/Shopping Cart</td>
					</tr>
					<!-- table to hold column headers for shopping cart-->
					<tr>
						<td colspan="6">
							<!--call template to dispaly shopping cart-->
							<xsl:call-template name="orderShoppingCart"/>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td width="33%" class="Label">
		   	     	             	  	 &nbsp;  Page &nbsp;
										<xsl:value-of select="key('pageData','cai_current_page')/value"/> &nbsp;of&nbsp;

										<xsl:choose>
											<xsl:when test="key('pageData','cai_total_pages')/value = '0'">
												<xsl:value-of select="'1'"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="key('pageData','cai_total_pages')/value"/>
											</xsl:otherwise>
										</xsl:choose>

									</td>
									<td align="center" width="33%" >
										<table border="0" cellpadding="1" cellspacing="0">
											<tr>
												<td >
                              <xsl:choose>
                                <xsl:when test="key('pageData','cai_show_first')/value ='y'">
                                  <button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="doPageAction(0);">
                                  7</button>
                                </xsl:when>
                                <xsl:when test="key('pageData','cai_show_first')/value ='n'">
                                  <button type="button" name="firstItem2" class="arrowButton" id="firstItem2" onclick="">
                                    	<xsl:attribute name="disabled">true</xsl:attribute>
                                  7</button>
                                </xsl:when>
                              </xsl:choose>
												</td>
												<td >
                              <xsl:choose>
                                <xsl:when test="key('pageData','cai_show_previous')/value ='y'">
                                  <button type="button" name="backItem" class="arrowButton"  id="backItem" onclick="doPageAction(1);">
                                  3</button>
                                </xsl:when>
                                <xsl:when test="key('pageData','cai_show_previous')/value ='n'">
                                  <button type="button" name="backItem2" class="arrowButton" id="backItem2" onclick="">
                                      	<xsl:attribute name="disabled">true</xsl:attribute>
                                  3</button>
                                </xsl:when>
                              </xsl:choose>
												</td>
												<td >

                            <xsl:choose>
                              <xsl:when test="key('pageData','cai_show_next')/value ='y'">
                                <button type="button" name="nextItem" class="arrowButton"  id="nextItem" onclick="doPageAction(2);">
                                4</button>
                              </xsl:when>
                              <xsl:when test="key('pageData','cai_show_next')/value ='n'">
                                <button type="button" name="nextItem2" class="arrowButton" id="nextItem2" onclick="">
                                  	<xsl:attribute name="disabled">true</xsl:attribute>
                                4</button>
                              </xsl:when>
                            </xsl:choose>
											</td>
												<td >
                              <xsl:choose>
                                <xsl:when test="key('pageData','cai_show_last')/value ='y'">
                                  <button type="button" name="lastItem" class="arrowButton"  id="lastItem" onclick="doPageAction(3);">
                                  8</button>
                                </xsl:when>
                                <xsl:when test="key('pageData','cai_show_last')/value ='n'">
                                  <button type="button" name="lastItem2" class="arrowButton"  id="lastItem2" onclick="">
                                    	<xsl:attribute name="disabled">true</xsl:attribute>
                                 8</button>
                                </xsl:when>
                              </xsl:choose>
												</td>
											</tr>
										</table>
									</td>
									<td align="right" class="Label">
										<xsl:value-of select="key('pageData','cai_number_of_orders')/value"/>&nbsp; &nbsp; Records Found
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table id="caButtonMenu" border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
		<tr style="vertical-align:text-top;">
			<td>
				<xsl:choose>
                                    <xsl:when test="$bypassCOM = 'Y' or $cai/customer_first_name = 'Target'">
                                        &nbsp;
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <button type="button" class="BigBlueButton" accesskey="C" id="customer_comments" onclick="javascript:doCustomerCommentsAction('caUpdateCustomer');">
                                            <xsl:if test="CAI_CUSTOMERS/CAI_CUSTOMER/customer_comment_indicator/text() = 'Y'">
                                                  <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                                            </xsl:if>
                                            (C)ustomer<br/>Comments
                                        </button>
                                    
                                    
                                        <button type="button" class="BigBlueButton" accesskey="U" id="update_customer" onclick="javascript:doUpdateCustomerAction('caUpdateCustomer',{count(CSCI_ORDERS/CSCI_ORDER/child::*)});">
                                        (U)pdate<br/>Customer</button>
                                        <xsl:if test="//data[name='customerHoldPermission']/value = $YES and //data[name='lossPreventionPermission']/value = $YES">
                                                <button type="button" style="vertical-align:top" class="BigBlueButton" accesskey="H" id="hold" onclick="javascript:doHoldAction('customer_account');">(H)old</button>
                                        </xsl:if>
                                     </xsl:otherwise>
                                     </xsl:choose>
			</td>
			<td align="right">
                                <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
                                    <button type="button" class="BigBlueButton" accesskey="V" id="lpsearch" onclick="javascript:doLossPreventionSearchAction();">
                                        Back to Loss<br />Pre(v)ention Search
                                    </button>
                                </xsl:if>
 				<button type="button" class="BigBlueButton" accesskey="B" id="search" onclick="javascript:doSearchAction();">(B)ack to Search</button>
				<br/>
				<button type="button" class="BigBlueButton" accesskey="M" style="width:55px;" id="main_menu" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu
				</button>
			</td>
		</tr>
	</table>
	<xsl:call-template name="footer"/>
	<script type="text/javascript">
	   if(document.getElementById('vipCustUpdatePermission').value == 'N'){
	      if(document.getElementById('cai_vip_customer').value == 'Y'){
	      if(localStorage.getItem('isVipPopupShown')=='N'){
       	        var modalArguments = new Object();
                modalArguments.modalText = document.getElementById('vipCustUpdateDeniedMessage').value;
                window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
       			localStorage.setItem('isVipPopupShown','Y');
	         } 
       		}
	      }
	 </script>
</form>
</div>
	</xsl:template>
</xsl:stylesheet>