<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var refundCanBeApplied = '<xsl:value-of select="key('pageData','refund_can_be_applied')/value"/>';

	function checkRefunds()
	{
    parent.document.getElementById("refund_status").value = refundCanBeApplied;
    if(refundCanBeApplied == 'y' || refundCanBeApplied == 'Y')
    {
      parent.doRefundCartAction();
    }
    else
    {
      var doc = document.forms[0];
		  var entireMessage = '';
			if(doc.message1.value != '')
				entireMessage = doc.message1.value + '\n\n';

			if(entireMessage != '')
				alert(entireMessage);
    }
	}

</script>
</head>
<body onload="javascript:checkRefunds();">
  <form>
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <input type="hidden" id="refundStatus" value="{key('pageData','refund_can_be_applied')/value}"/>
    <input type="hidden" id="message1" value="{key('pageData','refund_message_display1')/value}"/>
  </form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>