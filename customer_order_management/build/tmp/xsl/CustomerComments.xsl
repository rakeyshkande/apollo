
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  	<xsl:import href="securityanddata.xsl" />
  	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:template match="/">

  <!-- XSL Variables -->
  <xsl:variable name="userCanUpdate"  select="key('pagedata','user_can_update')/value"/>

		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Customer Comments</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript" src="js/updateComment.js"></script>
				<script type="text/javascript"><![CDATA[

var cos_show_previous = "true";
var cos_show_next = "true";
var cos_show_first = "true";
var cos_show_last = "true";

function checkKey()
{
  var tempKey = window.event.keyCode;

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cos_show_first == "true")
  {
    loadCommentsAction('Customer',--customerComments.page_position.value);
  }

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cos_show_last == "true")
  {
    loadCommentsAction('Customer',++customerComments.page_position.value);
  }

  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cos_show_next == "true")
  {
    loadCommentsAction('Customer',customerComments.page_position.value = customerComments.total_pages.value);
  }

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cos_show_previous == "true")
  {
    loadCommentsAction('Customer',customerComments.page_position.value = 1);
  }
}

// init code
function init()
{
  	var form = document.forms[0];
  	form.comment_text.disabled=true;
  	// save button is normally disabled
  	document.getElementById("saveAction").disabled=true;
	]]>
	<xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
		form.comment_text.disabled=true;
      	form.saveAction.disabled=true;
      	form.addComment.disabled=false;
      	replaceText("lockError", "Current record is locked by <xsl:value-of select='/root/out-parameters/OUT_LOCKED_CSR_ID'/>");
	</xsl:if><![CDATA[
}

// replace an html element value with new text
function replaceText( elementId, newString ){
   	var node = document.getElementById( elementId );
   	var newTextNode = document.createTextNode( newString );
   	if(node.childNodes[0]!=null){
    	node.removeChild( node.childNodes[0] );
   	}
   	node.appendChild( newTextNode );
}
  
//Disable buttons
function buttonDis(page){

	setNavigationHandlers();
	
   	if (customerComments.total_pages.value <= 1){
        cos_show_previous = "false";
      	cos_show_first = "false";
        cos_show_next = "false";
        cos_show_last = "false";
      	document.getElementById("firstItem").disabled= "true";
      	document.getElementById("backItem").disabled= "true";
      	document.getElementById("nextItem").disabled= "true";
      	document.getElementById("lastItem").disabled= "true";
      	return;
    }
   	if (page < 2){
        cos_show_previous = "false";
      	cos_show_first = "false";
      	document.getElementById("firstItem").disabled= "true";
      	document.getElementById("backItem").disabled= "true";
      	return;
    }
    if (page == customerComments.total_pages.value){
    	document.getElementById("nextItem").disabled= "true";
   		document.getElementById("lastItem").disabled= "true";
    	cos_show_next = "false";
      cos_show_last = "false";
    	return;
  	}
}
	
function addAction(value){
  document.getElementById('saveAction').disabled=true;
	var empty = isEmpty();
	if (empty == false)
	{
		var form = document.forms[0];
		if (form.comment_text.value.length > 4000)
		{
			alert("Comment must be less than 4000 characters.");
      document.getElementById('saveAction').disabled=false;
		}
		else
		{
			document.customerComments.action_type.value = value;
    	document.customerComments.action = "addComments.do";
     	document.customerComments.submit();
		}
  }
  else
  {
    document.getElementById('saveAction').disabled=false;
  }
}

function isEmpty(){
	var str = customerComments.comment_text.value;
	if (str.length == 0){
		alert("Please enter comment in order to save.");
		//loadCommentsAction();
		return true;
	}else{
		return false;
	}
}

function isExitSafe(){
  	var form = document.forms[0];
  	if(form.saveAction.disabled==false){
    	doaction = doExitPageAction("Your changes have not been saved. Do you wish to continue?");
  	}else{
    	doaction = true;
    }
  	return doaction;
}

function loadCommentsAction(commentType, newPagePosition)
{
  var form = document.forms[0];
  if(isExitSafe()==true)
  {
  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    form.page_position.value = newPagePosition;
    form.comment_type.value = commentType;
		form.action = "loadComments.do";
	 	form.submit();
  }
}

// main menu action
function mainMenuAction(){
  	if(isExitSafe()==true){
  		document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].customer_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    	doMainMenuAction(); // from commonUtil.js
  	}
}

function doOrderSearchAction(orderNumberType)
{
  var form = document.forms[0];
  var order_number = null;
  //if both external and master order numbers are present, use external order number
  if(form.external_order_number.value!=null)
  {
    order_number = form.external_order_number.value;
  }
  else
  {
    order_number = form.master_order_number.value;
  }

  if(isExitSafe()==true)
  {
  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    form.action = "customerOrderSearch.do" + 
                   "?action=customer_account_search" +
                   "&order_number="+order_number;
    form.submit();
  }
}

/*************************************************************************************
*	Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
	var form = document.forms[0];
	
	if(isExitSafe()==true)
	{
	  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].customer_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
	  	var url =  "customerOrderSearch.do?action=customer_search&customer_id=" + form.customer_id.value + "&last_order_date=Recipient" + getSecurityParams(false);
		performAction(url);
	}
}
		
]]>
		</script>
		<style>
			.messageContainer .lineNumber{
				width: 5%;
				vertical-align: top;
			}
			
			.messageContainer .userId{
				width: 10%;
				text-align: left;
				vertical-align: top;
			}
			
			.messageContainer .date{
				width: 20%;
				text-align: left;
				vertical-align: top;
			}
			
			.messageContainer .orderNum{
				width: 10%;
				text-align: left;
				vertical-align: top;
			}
			
			.messageContainer .origin{
				width: 10%;
				text-align: left;
				vertical-align: top;
			}
			
			.messageContainer .comment{
				width: 45%;
				text-align: left;
				vertical-align: top;
			}
		</style>
	</head>
	<body onload="init(); buttonDis(customerComments.page_position.value);" onkeydown="javascript:checkKey();">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName"  select="'Customer Comments'" />
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
		<form name="customerComments" method="post" action="">
          	<xsl:call-template name="securityanddata"/>
          	<xsl:call-template name="decisionResultData"/>
          	<iframe id="unlockFrame" name="unlockFrame" height="0" width="0" border="0"></iframe>
			<input type="hidden" name="action_type" id="action_type" value="{key('pagedata', 'action_type')/value}"/>
			<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>
			<input type="hidden" name="order_guid" id="order_guid" value="{key('pagedata', 'order_guid')/value}"/>
			<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata', 'order_detail_id')/value}"/>
			<input type="hidden" name="order_comment_origin" id="order_comment_origin" value="{key('pagedata', 'order_comment_origin')/value}"/>
			<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pagedata', 'external_order_number')/value}"/>
			<input type="hidden" name="comment_origin" id="comment_origin" value="{key('pagedata', 'comment_origin')/value}"/>
			<input type="hidden" name="comment_reason" id="comment_reason" value="{key('pagedata', 'comment_reason')/value}"/>
			<input type="hidden" name="comment_type" id="comment_type" value="Customer"/>
			<input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
			<input type="hidden" name="total_pages" id="total_pages" value="{key('pagedata', 'total_pages')/value}"/>
			<input type="hidden" name="start_position" id="start_position" value="{key('pagedata', 'start_position')/value}"/>
			<input type="hidden" name="comment_id" id="comment_id" value=""/>

			<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
				<tr>
					<td id="lockError" class="ErrorMessage"></td>
				</tr>
			</table>
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
				<tr>
					<td>
						<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
							<tr>
								<td width="25%" class="label"></td>
								<td width="25%" class="label"></td>
							</tr>
							<tr>
								<td colspan="3" align="left">
									<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
										<tr>
											<td>
												<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
													<tr>
														<td>
															<div class="messageContainer" style="padding-right: 16px; width: 98%;">
																<table width="100%" border="0" cellpadding="1" cellspacing="1">
																	<tr>
																		<th class="lineNumber"></th>
																		<th class="userId">User ID</th>
																		<th class="date">Date/Time</th>
																		<th class="orderNum">Order #</th>
																		<th class="origin">Origin</th>
																		<th colspan="2" class="comment">Comment</th>
																	</tr>
																</table>
															</div>
															<table width="100%" cellpadding="0" border="0" cellspacing="0">
																<tr>
																	<td colspan="7"><hr/></td>
																</tr>
															</table>
															<div class="messageContainer" style="width: 99%; overflow: auto; padding-right: 16px; height: 180px;">
																<table width="100%" cellpadding="1" border="0" cellspacing="1" style="margin-right: -16px;word-break:break-all;table-layout:fixed;">
																	<xsl:for-each select="root/comments/comment">
																		<tr>
																			<td class="lineNumber">
																				<xsl:choose>
																					<xsl:when test="$userCanUpdate = 'Y'">
																						<a href="javascript:updateComment('Customer Comments', '{comment_id}');" class="textlink">
																							<script type="text/javascript">document.write(customerComments.start_position.value); customerComments.start_position.value++</script>.
																						</a>
																					</xsl:when>
																					<xsl:otherwise>
																						<script type="text/javascript">document.write(customerComments.start_position.value); customerComments.start_position.value++</script>.
																					</xsl:otherwise>
																				</xsl:choose>
																			</td>
																			<xsl:choose>
																				<xsl:when test="manager_indicator = 'Y'">
																					<td class="userId">
																						<b><xsl:value-of select="created_by"/></b>
																					</td>
																				</xsl:when>
																				<xsl:otherwise>
																					<td class="userId">
																						<xsl:value-of select="created_by"/>
																					</td>
																				</xsl:otherwise>
																			</xsl:choose>
																			<xsl:choose>
																				<xsl:when test="manager_indicator = 'Y'">
																					<td class="date">
																						<b><xsl:value-of select="created_on"/></b>
																					</td>
																				</xsl:when>
																				<xsl:otherwise>
																					<td class="date">
																						<xsl:value-of select="created_on"/>
																					</td>
																				</xsl:otherwise>
																			</xsl:choose>
																			<xsl:choose>
																				<xsl:when test="external_order_number!=''and manager_indicator ='N'">
																					<td class="orderNum">
																						<xsl:value-of select="external_order_number"/>
																					</td>
																				</xsl:when>
																				<xsl:when test="external_order_number=''and manager_indicator ='N'">
																					<td class="orderNum">
																						N/A
																					</td>
																				</xsl:when>
																				<xsl:when test="external_order_number!=''and manager_indicator ='Y'">
																					<td class="orderNum">
																						<b><xsl:value-of select="external_order_number"/></b>
																					</td>
																				</xsl:when>
																				<xsl:when test="external_order_number=''and manager_indicator ='Y'">	
																					<td class="orderNum"><b>N/A</b></td>
																				</xsl:when>																	
																			</xsl:choose>
																			<xsl:choose>
																				<xsl:when test="manager_indicator = 'Y'">
																					<td class="origin">
																						<b><xsl:value-of select="comment_origin"/></b>
																					</td>
																				</xsl:when>
																				<xsl:otherwise>
																					<td class="origin">
																						<xsl:value-of select="comment_origin"/>
																					</td>
																				</xsl:otherwise>
																			</xsl:choose>
																			<xsl:choose>
																				<xsl:when test="manager_indicator = 'Y'">
                                           <td id="td_{comment_id}" class="comment">
																						<b><xsl:value-of disable-output-escaping="yes" select="comment_text"/></b>
																					</td>
																				</xsl:when>
																				<xsl:otherwise>
                                         <td id="td_{comment_id}" class="comment">
																						<xsl:value-of disable-output-escaping="yes" select="comment_text"/>
																					</td>
																				</xsl:otherwise>
																			</xsl:choose>
																		</tr>
																	</xsl:for-each>
																</table>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<hr/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="left"></td>
							</tr>
							<tr>
								<td class="label">Enter Comments:</td>
							</tr>
							<tr>
								<td width="80%">
									<textarea cols="100" rows="2" id="comment_text" name="comment_text" tabindex="1"></textarea>
								</td>
								<td align="right">
						        	<table  border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="25%" class="label" align="right">Page&nbsp;
												<xsl:for-each select="key('pagedata', 'page_position')">
													<xsl:value-of select="value" />
												</xsl:for-each>
												&nbsp;of&nbsp;
												<xsl:for-each select="key('pagedata', 'total_pages')">
         											<xsl:if test="value = '0'">
						                      			1
						                       		</xsl:if>
						                       		<xsl:if test="value != '0'">
								 						<xsl:value-of select="value" />
						                       		</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
										<xsl:for-each select="key('pagedata', 'page_position')">
										<tr>
											<td colspan="6" align="right">
										
								                  			<xsl:choose>
								                  				<xsl:when test="key('pagedata','total_pages')/value !='0'">
								                   					<button type="button" name="firstItem" tabindex="1" class="arrowButton" id="firstItem" onclick="loadCommentsAction('Customer',1);">
	                          											7
								                              		</button>
						                              			</xsl:when>
								                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
								                              		<button type="button" name="firstItem" tabindex="1" class="arrowButton" id="firstItem" onclick="">
								                              			7
								                              		</button>
								                              	</xsl:when>
							                              	</xsl:choose>
							                     		
							                       			<xsl:choose>
	                            								<xsl:when test="key('pagedata','total_pages')/value !='0'">
							                              			<button type="button" name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="loadCommentsAction('Customer',customerComments.page_position.value-1);">
							                              				3
							                              			</button>
							                              		</xsl:when>
							                              		<xsl:when test="key('pagedata','total_pages')/value ='0'">
							                              			<button type="button" name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="">
							                              				3
							                              			</button>
							                              		</xsl:when>
							                              	</xsl:choose>
                   										
							                           		<xsl:choose>
		                              							<xsl:when test="key('pagedata','total_pages')/value !='0'">
							                              			<button type="button" name="nextItem" tabindex="1" class="arrowButton" id="nextItem" onclick="x=customerComments.page_position.value;loadCommentsAction('Customer',++x);">
							                              				4
						                              				</button>
					                              				</xsl:when>
							                              		<xsl:when test="key('pagedata','total_pages')/value ='0'">
							                              			<button type="button" name="nextItem" tabindex="1" class="arrowButton" id="nextItem" onclick="">
							                              				4
							                              			</button>
						                              			</xsl:when>
                           									</xsl:choose>
						                           		
							                            	<xsl:choose>
							                             		<xsl:when test="key('pagedata','total_pages')/value !='0'">
								                            		<button type="button" name="lastItem" tabindex="1" class="arrowButton" id="lastItem" onclick="loadCommentsAction('Customer',customerComments.total_pages.value);">
								                            			8
								                            		</button>
							                             		</xsl:when>
							                             		<xsl:when test="key('pagedata','total_pages')/value ='0'">
							                             			<button type="button" name="lastItem" tabindex="1" class="arrowButton" id="lastItem" onclick="">
							                             				8
							                              			</button>
						                          				</xsl:when>
							                           		</xsl:choose>
							                             
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
			<tr>
				<td width="80%" valign="top">
					
					<xsl:if test="key('pagedata', 'order_comment_origin')/value='Y'">
						<button type="button" accesskey="O" class="bigBlueButton ButtonTextBox2" name="searchAction" tabindex="6" onClick="doOrderSearchAction('External Order');">Recipient/<br/>(O)rder</button>
						<button type="button" class="bigBlueButton ButtonTextBox2" accesskey="C" name="orderComments" tabindex="10" onclick="javascript:loadCommentsAction('Order',1);">Order<br/>(C)omments</button>
					</xsl:if>
					
					<button type="button" accesskey="A" class="bigBlueButton" style="width:102" id="addComment" name="addComment" tabindex="2" onClick="UPDATE_COMMENT.checkLock();" title="Click to enter comments">(A)dd<br/>Comments</button>
					<button type="button" style="width:90" accesskey="S" class="bigBlueButton" id="saveAction" name="saveAction" tabindex="4" onclick="addAction('save_comment')">(S)ave</button>
					<button type="button" accesskey="R" class="bigBlueButton" style="width:100" id="refreshAction" name="refreshAction" tabindex="6" onClick="loadCommentsAction('Customer',1);">(R)efresh</button>
					
				</td>
				<td width="20%" align="right">
					<button type="button" accesskey="B" class="blueButton" name="backButton" tabindex="9" onclick="doBackAction();">(B)ack to Cust Acct</button>
					<br/>					
					<button type="button" accesskey="M" class="bigBlueButton" name="mainMenu" tabindex="10" onclick="mainMenuAction();">(M)ain<br/>Menu</button>
				</td>
			</tr>
			<!--
			<tr>
				<td>
					<xsl:element name="input">
						<xsl:attribute name="onClick">loadCommentsAction('Customer',1);</xsl:attribute>
						<xsl:attribute name="type">checkbox</xsl:attribute>
						<xsl:attribute name="name">hide_system</xsl:attribute>
						<xsl:if test="key('pagedata', 'hide_system')/value='on'">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</xsl:element>
               		Hide System Generated Comments
				</td>
			</tr>
			-->
		</table>
		<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
	</form>
	<div id="waitDiv" style="display:none">
		<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
			<tr>
				<td width="100%">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
							<td id="waitTD" width="50%" class="waitMessage"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<xsl:call-template name="footer"></xsl:call-template>
</body>
</html>
</xsl:template>
</xsl:stylesheet>