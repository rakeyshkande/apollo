
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>

  <!-- XSL Variables -->
  <xsl:variable name="userCanUpdate"  select="key('pagedata','user_can_update')/value"/>

	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>View Email</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/tabIndexMaintenance.js"/>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript" src="js/FormChek.js"></script>
				<script type="text/javascript" language="javascript">// init code
function init(){
  setViewEmailIndexes();
	var form = document.viewEmail;
	if (form.IO.value == "I"){
		form.customer_email_address.value = "<xsl:value-of select="root/PointOfContact/poc/sender_email_address"/>";
	}else{
		form.customer_email_address.value = "<xsl:value-of select="root/PointOfContact/poc/recipient_email_address"/>";
	}
	canReply();
}

/*******************************************************************************
*  setViewEmailIndexes()
*  Sets tabs for View Email Page. 
*  See tabIndexMaintenance.js
*******************************************************************************/
function setViewEmailIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('searchAction', 'replyBtn', 'sendBtn', 'commBtn',
    'queueBtn', 'backButton', 'mainMenu', 'printer');
  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}

function canReply(){	
	if (document.viewEmail.customer_email_address.value == ""){
		//document.forms[0].replyBtn.disabled="true";
		//document.forms[0].sendBtn.disabled="true";
	}else{
		//replyBtn.disabled="false";
		//sendBtn.disabled="false";
	}

}<![CDATA[	

function doBackAction() {	
	doOrderSearchAction();
}

function doCommunication() {
	var form = document.forms[0];
	form.action = "displayCommunication.do?order_detail_id=" + form.order_detail_id.value;
	document.forms[0].action_type.value="display_communication";
	
	form.submit();
}

function doOrderSearchAction()
{
  var form = document.forms[0];
  form.action = "customerOrderSearch.do" + 
                   "?action=customer_account_search";
  document.forms[0].action_type.value="";
  document.forms[0].order_detail_id.value="";
  form.submit();
}

function sendAction(){
	document.forms[0].action="loadSendMessage.do";
	document.forms[0].action_type.value="load_titles";
	document.forms[0].replyFlag.value="N";
	document.forms[0].submit();
}

function replyAction(){
	document.forms[0].action="loadSendMessage.do";
	document.forms[0].action_type.value="load_titles"
	document.forms[0].replyFlag.value="Y";
	document.forms[0].submit();
}

function deleteQueueAction(){
	document.forms[0].action="removeEmailQueue.do";
	document.forms[0].action_type.value="";
	document.forms[0].submit();
}


function doDelete()
{
	var form = document.forms[0];
	form.action = "loadMessage.do";
	document.forms[0].action_type.value="delete_email";

	form.submit();
}



]]></script>
			</head>
			<body onload="init();">

				<xsl:choose>
					<xsl:when test="key('pagedata', 'attached')/value != 'false'">
						<xsl:call-template name="header">
							<xsl:with-param name="headerName" select="'View Email'"/>
							<xsl:with-param name="showTime" select="true()"/>
							<xsl:with-param name="showBackButton" select="true()"/>
							<xsl:with-param name="showPrinter" select="true()"/>
							<xsl:with-param name="showSearchBox" select="false()"/>
							<xsl:with-param name="dnisNumber" select="$call_dnis"/>
							<xsl:with-param name="cservNumber" select="$call_cs_number"/>
							<xsl:with-param name="indicator" select="$call_type_flag"/>
							<xsl:with-param name="brandName" select="$call_brand_name"/>
							<xsl:with-param name="showCSRIDs" select="true()"/>
							<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
							<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="header">
							<xsl:with-param name="headerName" select="'View Email'"/>
							<xsl:with-param name="showTime" select="true()"/>
							<xsl:with-param name="showBackButton" select="false()"/>
							<xsl:with-param name="showPrinter" select="true()"/>
							<xsl:with-param name="showSearchBox" select="false()"/>
							<xsl:with-param name="dnisNumber" select="$call_dnis"/>
							<xsl:with-param name="cservNumber" select="$call_cs_number"/>
							<xsl:with-param name="indicator" select="$call_type_flag"/>
							<xsl:with-param name="brandName" select="$call_brand_name"/>
							<xsl:with-param name="showCSRIDs" select="true()"/>
							<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>


				<form name="viewEmail" method="post" action="">
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="action_type" id="action_type" value="{key('pagedata','action_type')/value}"/>
					<input type="hidden" name="IO" id="IO" value="{root/PointOfContact/poc/sent_received_indicator}"/>
					<input type="hidden" name="replyFlag" id="replyFlag" value=""/>
					<input type="hidden" name="order_guid" id="order_guid" value="{root/PointOfContact/poc/order_guid}"/>
					<input type="hidden" name="order_number" id="order_number" value="{root/PointOfContact/poc/external_order_number}"/>
					<input type="hidden" name="external_order_number" id="external_order_number" value="{root/PointOfContact/poc/external_order_number}"/>
					<input type="hidden" name="message_subject" id="message_subject" value="{root/PointOfContact/poc/email_subject}"/>
					<input type="hidden" name="message_content" id="message_content" value="{root/PointOfContact/poc/body}"/>
					<input type="hidden" name="customer_email_address" id="customer_email_address" value=""/>
					<input type="hidden" name="message_type" id="message_type" value="{key('pagedata', 'message_type')/value}"/>
					<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata', 'order_detail_id')/value}"/>
					<!--		<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>-->
					<input type="hidden" name="customer_id" id="customer_id" value="{root/PointOfContact/poc/customer_id}"/>
					<input type="hidden" name="message_line_number" id="message_line_number" value="{key('pagedata', 'message_line_number')/value}"/>
					<input type="hidden" name="poc_id" id="poc_id" value="{key('pagedata', 'poc_id')/value}"/>
					<input type="hidden" name="status" id="status" value="{key('pagedata', 'status')/value}"/>
					<input type="hidden" name="tagged_csr_id" id="tagged_csr_id" value="{key('pagedata', 'tagged_csr_id')/value}"/>
					<input type="hidden" name="message_id" id="message_id" value="{key('pagedata', 'message_id')/value}"/>
					<input type="hidden" name="remove_email_queue_flag" id="remove_email_queue_flag" value="{key('pagedata', 'remove_email_queue_flag')/value}"/>
					<input type="hidden" name="attached" id="attached" value="{key('pagedata', 'attached')/value}"/>
					<input type="hidden" name="queue_delete_security_error" id="queue_delete_security_error" value="{key('pagedata', 'queue_delete_security_error')/value}"/>
					<input type="hidden" name="queue_delete_general_error" id="queue_delete_general_error" value="{key('pagedata', 'queue_delete_general_error')/value}"/>
					<xml id="laura">
						<xsl:copy-of select="node()"/>
					</xml>
					<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
							<xsl:if test="key('pagedata', 'queue_delete_security_error')/value != ''">
								<td class="ErrorMessage">Security role does not have access to delete this from queues</td>
							</xsl:if>
							<xsl:if test="key('pagedata', 'queue_delete_general_error')/value != ''">
								<td class="ErrorMessage">Email is attached to an order. &nbsp; The order must be removed before delete is allowed.</td>
							</xsl:if>
						</tr>
					</table>
					<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
									<tr>
										<td>
											<div style="overflow:auto; width:100%; height=270;">
                                                                                                <!-- Main content table -->
												<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
                                    <xsl:for-each select="root/PointOfContact/poc">
                                            <tr>
                                                    <th class="label" width="22%" align="right">Order&nbsp;Number</th>
                                                    <th class="label" width="20%" align="center">Line&nbsp;#</th>
                                                    <th class="label" width="10%" align="center">I/O</th>
                                                    <th class="label" width="8%" align="center">Date</th>
                                                    <th class="label" width="8%" align="center">Time</th>
                                                    <th class="label" width="24%" align="center">Subject</th>
                                                    <th class="label" width="8%" align="center">User&nbsp;ID</th>
                                            </tr>
                                            <tr>
                                                    <td align="right">

                                                            <xsl:choose>
                                                                    <xsl:when test="key('pagedata', 'attached')/value != 'false'">
                                                                            <a href="javascript:doOrderSearchAction();">
                                                                                    <xsl:value-of select="external_order_number"/>
                                                                            </a>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                            <xsl:value-of select="external_order_number"/>
                                                                    </xsl:otherwise>
                                                            </xsl:choose>
                                                    </td>
                                                    <td align="center">
                                                            <xsl:value-of select="key('pagedata', 'message_line_number')/value"/>
                                                    </td>
                                                    <td align="center">
                                                            <xsl:value-of select="sent_received_indicator"/>
                                                    </td>
                                                    <td align="center">
                                                            <xsl:value-of select="substring(created_on,0,11)"/>
                                                    </td>
                                                    <td align="center">
                                                            <xsl:value-of select="substring(created_on,11,9)"/>
                                                    </td>
                                                    <td align="center">
                                                            <input type="text" id="iEmailSubject" name="iEmailSubject" maxlength="100" size="40" >
                                                                    <xsl:attribute name="value">
                                                                            <xsl:value-of select="email_subject"/>
                                                                    </xsl:attribute>
                                                                    <xsl:if test="$userCanUpdate != 'Y'">
                                                                            <xsl:attribute name="disabled">
                                                                                    true
                                                                            </xsl:attribute>
                                                                    </xsl:if>
                                                            </input>
                                                    </td>
                                                    <td align="center">
                                                            <xsl:value-of select="created_by"/>
                                                    </td>
                                            </tr>
                                            <tr>
                                            <td colspan="7">
                                            <!-- Detail table -->
                                            <table border="0" align="center" cellpadding="2" cellspacing="0">
                                            <xsl:choose>
                                            <xsl:when test="is_modify_order = 'Y'">
                                            <tr>
                                            <td colspan="2" class="ColHeaderCenter" width="60%">&nbsp;Information from Modify Order Email Request</td>
                                            <td colspan="2" class="ColHeaderCenter" width="40%">&nbsp;Information from most recent FTD</td>
                                            </tr>
                                            </xsl:when>
                                            <xsl:otherwise>
                                            <tr>
                                            <td colspan="4">&nbsp;</td>
                                            </tr>
                                            </xsl:otherwise>
                                            </xsl:choose>
                                            <tr>
                                              <td valign="top" width="40%" colspan="2">
                                                <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
                                                <tr>
                                                    <td align="right" class="label">Bill-to First Name:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="first_name"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Bill-to Last Name:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="last_name"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Bill-to Email:</td>
                                                    <td align="left">
                                                            <xsl:choose>
                                                                    <xsl:when test="sent_received_indicator='I'">
                                                                            <xsl:value-of select="sender_email_address"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                            <xsl:value-of select="recipient_email_address"/>
                                                                    </xsl:otherwise>
                                                            </xsl:choose>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Bill-to Phone 1:</td>
                                                    <td align="left">
                                                            <script type="text/javascript">if(isUSPhoneNumber('<xsl:value-of select="daytime_phone_number"/>')){
                                                                    document.write(reformatUSPhone('<xsl:value-of select="daytime_phone_number"/>'));
                                                            }else{
                                                                    document.write('<xsl:value-of select="daytime_phone_number"/>');
                                                            }</script>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Delivery Date:</td>
                                                    <td align="left">
                                                            <xsl:choose>
                                                                    <xsl:when test="new_delivery_date != ''">
                                                                            <xsl:value-of select="substring(new_delivery_date,0,11)"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                            <xsl:value-of select="substring(delivery_date,0,11)"/>
                                                                    </xsl:otherwise>
                                                            </xsl:choose>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient First Name:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_recip_first_name"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient Last Name:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_recip_last_name"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient Phone:</td>
                                                    <td align="left">
                                                            <xsl:if test="new_phone != ''">
                                                                    <script type="text/javascript">if(isUSPhoneNumber('<xsl:value-of select="new_phone"/>')){
                                                                            document.write(reformatUSPhone('<xsl:value-of select="new_phone"/>'));
                                                                          }else{
                                                                            document.write('<xsl:value-of select="new_phone"/>');
                                                                          }</script>
                                                            </xsl:if>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient Address:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_address"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient City:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_city"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient State:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_state"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient ZIP:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_zip_code"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Recipient Country:</td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_country"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label"><xsl:if test="new_institution != ''">Recipient Institution:</xsl:if></td>
                                                    <td align="left">
                                                            <xsl:value-of select="new_institution"/>
                                                    </td>
                                                </tr>
                                                </table>

                                              </td>
                                              <td valign="top" width="60%" colspan="2">
                                                  <xsl:choose>
                                                      <xsl:when test="is_modify_order = 'Y'">
                                                          <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
                                                              <tr><td class="BackGrey" colspan="2">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey" colspan="2">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey" colspan="2">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey" colspan="2">&nbsp;</td></tr>
                                                              <tr>
                                                                  <td align="right" class="LabelGrey">Delivery Date:</td>
                                                                  <td align="left" class="BackGrey">
                                                                      <xsl:value-of select="mo_curr_delivery_date"/>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td align="right" class="LabelGrey" valign="top">Recipient Full Name:</td>
                                                                  <td align="left" class="BackGrey" valign="top" rowspan="2">
                                                                      <xsl:value-of select="mo_curr_recipient"/> 
                                                                  </td>
                                                              </tr>
                                                              <tr><td class="BackGrey">&nbsp;</td></tr>
                                                              <tr>
                                                                  <td align="right" class="LabelGrey">Recipient Phone:</td>
                                                                  <td align="left" class="BackGrey">
                                                                  <xsl:if test="mo_curr_phone_number != ''">
                                                                      <script type="text/javascript">if(isUSPhoneNumber('<xsl:value-of select="mo_curr_phone_number"/>')){
                                                                            document.write(reformatUSPhone('<xsl:value-of select="mo_curr_phone_number"/>'));
                                                                          }else{
                                                                            document.write('<xsl:value-of select="mo_curr_phone_number"/>');
                                                                          }</script>
                                                                  </xsl:if>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td align="right" class="LabelGrey" valign="top">Recipient Full Address:</td>
                                                                  <td align="left" class="BackGrey" valign="top" rowspan="6">
                                                                      <xsl:value-of select="mo_curr_address"/> <br/>
                                                                      <xsl:value-of select="mo_curr_city_state_zip"/>
                                                                  </td>
                                                              </tr>
                                                              <tr><td class="BackGrey">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey">&nbsp;</td></tr>
                                                              <tr><td class="BackGrey">&nbsp;</td></tr>
                                                          </table>
                                                      </xsl:when>
                                                      <xsl:when test="sent_received_indicator='I'">
                                                          <xsl:value-of select="body" disable-output-escaping="yes"/>
                                                      </xsl:when>
                                                      <xsl:otherwise>
                                                          <textarea rows="14" cols="90" readonly="true">
                                                              <xsl:value-of select="body"/>
                                                          </textarea>
                                                      </xsl:otherwise>
                                                  </xsl:choose>
                                              </td>
                                            </tr>

                                            <xsl:if test="new_product != ''">
                                                <tr>
                                                    <td align="right" class="label" width="20%" >Product Id:</td>
                                                    <td align="left" width="20%">
                                                            <xsl:value-of select="new_product"/>
                                                    </td>
                                                    <xsl:if test="is_modify_order = 'Y'">
                                                        <td align="right" class="LabelGrey">Product Id:</td>
                                                        <td align="left" class="BackGrey">
                                                            <xsl:value-of select="mo_curr_product_id"/>
                                                        </td>
                                                    </xsl:if>
                                                </tr>
                                            </xsl:if>
                                            <xsl:if test="new_card_message != '' or new_card_signature != ''">
                                                <tr>
                                                    <td align="right" class="label">Card Message:</td>
                                                    <td align="left">
                                                            <textarea readonly="readonly" rows="3" cols="20">
                                                                    <xsl:value-of select="new_card_message"/>
                                                            </textarea>
                                                    </td>
                                                    <xsl:if test="is_modify_order = 'Y'">
                                                        <td rowspan="2" align="right" class="LabelGrey">Card Message and<br/> Signature:</td>
                                                        <td rowspan="2" align="left" valign="top" class="BackGrey">
                                                            <textarea readonly="readonly" rows="6" cols="20">
                                                                    <xsl:value-of select="mo_curr_card_message"/>
                                                            </textarea>
                                                        </td>
                                                    </xsl:if>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="label">Card Signature:</td>
                                                    <td align="left">
                                                            <textarea readonly="readonly" rows="2" cols="20">
                                                                    <xsl:value-of select="new_card_signature"/>
                                                            </textarea>
                                                    </td>
                                                </tr>
                                            </xsl:if>

                                            <xsl:choose>
                                            <xsl:when test="is_modify_order = 'Y'">
                                                <tr>
                                                    <td align="right" class="label">Other info from customer email:</td>
                                                    <td></td>
                                                    <td align="left" class="LabelGrey" colspan="2">Outbound ASKs associated with this FTD:</td>
                                                </tr>
                                                <tr>
                                                <td align="center" colspan="2">
                                                <textarea name="dummybody" rows="15" cols="58" readonly="yes">
                                                    <xsl:if test="string(normalize-space(body))">
                                                            <xsl:value-of select="body"/>
                                                    </xsl:if>
                                                </textarea>
                                                </td>
                                                <td align="left" class="BackGrey" colspan="2">
                                                    <textarea name="dummycomments" rows="15" cols="58" readonly="yes">
                                                        <xsl:for-each select="/root/AskMessages/Ask">
                                                            <xsl:if test="string(normalize-space(comments))">
                                                                <xsl:value-of select="created_on"/>&nbsp;-&nbsp;<xsl:value-of select="comments"/><xsl:text>&#10;&#10;</xsl:text>
                                                            </xsl:if>
                                                        </xsl:for-each>
                                                    </textarea>
                                                </td>
                                                </tr>
                                            </xsl:when>
                                            </xsl:choose>


                                        <!-- End Detail table -->
                                        </table>


                                        </td>
                                        </tr>
                                            
                                    </xsl:for-each>
                                                                                                <!-- End Main content table -->
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" align="left"></td>
						</tr>
					</table>

					<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
				</form>
				<table width="98%" align="center">

					<xsl:choose>
						<xsl:when test="key('pagedata', 'attached')/value != 'false'">
							<tr>
								<td width="75%" valign="top">
									<button type="button" style="width:90" accesskey="" class="bigBlueButton" name="searchAction" onClick="javascript:doOrderSearchAction();">Recipient/<br/>Order</button>
									<input name="replyBtn" id="replyBtn" accesskey="R" style="width:90" class="bigBlueButton" type="button" value="(R)eply" onClick="replyAction()"/>
									<input name="sendBtn" id="sendBtn" accesskey="S" style="width:90" class="bigBlueButton" type="button" value="(S)end Email" onClick="sendAction()"/>
									<xsl:if test="key('pagedata', 'remove_email_queue_flag')/value = 'Y'">
										<input name="queueBtn" id="queueBtn" accesskey="D" style="width:110" class="bigBlueButton" type="button" value="Queue (D)elete" onClick="deleteQueueAction()"/>
									</xsl:if>
									<input name="commBtn" id="commBtn" style="width:90" accesskey="O" class="bigBlueButton" type="button" value="C(o)mmunication" onClick="doCommunication()"/>
									<xsl:if test="$userCanUpdate = 'Y'">
										<input name="deleteEmail" id="deleteEmail" style="width:110" accesskey="E" class="bigBlueButton" type="button" value="D(e)lete Email Text" onClick="doDelete()"/>
									</xsl:if>
								</td>
								<td width="20%" align="right">
									<button type="button" class="blueButton" accesskey="B" name="backButton" tabindex="9" onclick="javascript:doBackAction();">(B)ack to Order</button>
									<br/>
									<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" tabindex="10" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu</button>
								</td>
							</tr>
						</xsl:when>
						<xsl:otherwise>
							<tr>
								<td width="75%" valign="top">
									<input name="replyBtn" id="replyBtn" accesskey="R" style="width:90" class="bigBlueButton" type="button" value="(R)eply" onClick="replyAction()"/>
									<xsl:if test="key('pagedata', 'remove_email_queue_flag')/value = 'Y'">
										<input name="queueBtn" id="queueBtn" accesskey="D" style="width:110" class="bigBlueButton" type="button" value="Queue (D)elete" onClick="deleteQueueAction()"/>
									</xsl:if>
									<xsl:if test="$userCanUpdate = 'Y'">
										<input name="deleteEmail" id="deleteEmail" style="width:110" accesskey="E" class="bigBlueButton" type="button" value="D(e)lete Email Text" onClick="doDelete()"/>
									</xsl:if>
								</td>
								<td width="20%" align="right">

									<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" tabindex="10" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu</button>
								</td>
							</tr>
						</xsl:otherwise>
					</xsl:choose>
				</table>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<xsl:call-template name="footer"></xsl:call-template>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>