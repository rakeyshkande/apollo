<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <!-- Root template -->
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:key name="errmsg" match="root/errorMessage/error" use="name" />
   <xsl:variable name="message_type" select="key('pagedata','message_type')/value"/>
	<xsl:template match="/">
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html" />
      <title>FTD - Stock <xsl:value-of select="$message_type"/> &nbsp;Maintenance</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css" />
      <link rel="stylesheet" type="text/css" href="css/calendar.css" />
      <script type="text/javascript" src="js/util.js"></script>
      <script type="text/javascript" src="js/calendar.js"></script>
      <script type="text/javascript" src="js/clock.js"></script>
			<script type="text/javascript" src="js/commonUtil.js"></script>
			<script type="text/javascript" src="js/mercuryMessage.js"></script>
			<script type="text/javascript">

// init code
function init()
{
  if (document.getElementById("select_title") != null)
    document.getElementById("select_title").focus();

  // check for db lock results
    <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
      <xsl:if test="key('pagedata','action_type')/value='save_message'">
       <xsl:if test="key('errmsg', 'VALIDATION_SUCCESS')/value='Y'">
        replaceText("lockError", "Message saved successfully.");
        //alert("Save Operation Successful.");
       </xsl:if>
       <xsl:if test="key('errmsg', 'VALIDATION_SUCCESS')/value='N'">
        replaceText("lockError", "Data not saved.  Message title must be unique.");
        //alert("Save Operation Failed.");
       </xsl:if>
      </xsl:if>
    </xsl:if>
    <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
      replaceText("lockError", "Current record is locked by <xsl:value-of select='/root/out-parameters/OUT_LOCKED_CSR_ID'/>");
    //  document.getElementById("messageContent").disabled=true;
   //   document.getElementById("messageTitle").disabled=true;
      document.getElementById("save").disabled="true";
   //   if(document.getElementById("messageSubject")!=null)
     //   document.getElementById("messageSubject").disabled=true;
       
    </xsl:if>
}

<![CDATA[
function replaceText( elementId, newString )
{
   var node = document.getElementById( elementId );   
   var newTextNode = document.createTextNode( newString );
   if(node.childNodes[0]!=null)
   {
    node.removeChild( node.childNodes[0] );
   }
   node.appendChild( newTextNode );
}

function loadAction()
{

  var form = document.messageMaint;
  form.newMessageFlag.value = "N";
  if(form.message_type.value == "Email")
  {
    form.autoEmail.checked=false;
  }
  form.action_type.value = "load_content";
  form.action = "editMessage.do";
  form.message_id.value = form.select_title.options[form.select_title.selectedIndex].value;
  form.message_title.value = form.select_title.options[form.select_title.selectedIndex].text;
  form.submit();
}

function spellCheck()
{
  var form = document.messageMaint;
    var value = escape(form.message_content.value).replace(/\+/g,"%2B");
    var url = "spellCheck.do?content=" + value + "&callerName=message_content"+
              "&securitytoken="+form.securitytoken.value+
              "&context="+form.context.value;

    window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300,help=no");
   form.spellCheckFlag.value = "N";
}

function setFlag()
{
  document.forms[0].spellCheckFlag.value = "Y";
  document.forms[0].change_flag.value="Y";
}
/*
function setTitle()
{
  var indx = document.messageMaint.select_title.selectedIndex;
  var optText = document.messageMaint.select_title.options[indx].text;
  var optValue = document.messageMaint.select_title.options[indx].value;
  if(indx>0)
  {
    document.messageMaint.message_title.value = optText;
    document.messageMaint.message_id.value = optValue;
  }
  else
  {
    document.messageMaint.message_title.value = "";
    document.messageMaint.message_id.value = 0;
  }
}
*/
function saveAction()
{
  var form = document.messageMaint;

  // first, validate all the input parameters
  if(validateMessage())
  {
    // pop up a modal diaglog if the spell check flag is set to "Y"
    if(form.spellCheckFlag.value=="Y")
    {
      var ret = confirm("Do you want to save without spell-check?");
      if(!ret)
      {
        return; // allow the user to spell check
      }
    }
    var indx = document.messageMaint.origin_id.selectedIndex;
    var optValue = document.messageMaint.origin_id.options[indx].value;


    form.action = "editMessage.do";
    form.action_type.value = "save_message";
    form.message_id.value = form.select_title.options[form.select_title.selectedIndex].value;
    //form.message_title.value = form.select_title.options[form.select_title.selectedIndex].text;
    form.submit();
  }
  else
  {
    // user has to fix errors and resubmit the form
    alert("Enter any missing field(s) and resubmit.");
    return;
  }
}

function validateMessage()
{
  if(document.forms[0].message_title.value==null || document.forms[0].message_title.value=="")
    return false;
  if(document.forms[0].message_type == "Email")
   if(document.forms[0].message_subject.value==null || document.forms[0].message_subject.value=="")
     return false;
  if(document.forms[0].message_content.value==null || document.forms[0].message_content.value=="")
    return false;
  return true;
}

function copyAction()
{
  	document.forms[0].message_title.value="";
  	document.forms[0].select_title.value=0;
  	document.forms[0].newMessageFlag.value="Y";
  	
  	document.getElementById("save").disabled=false;
	replaceText("lockError", "");
	
	if(document.forms[0].message_type.value == "Email"){
		document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Email Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
	}else{
		document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Letter Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
	}
}

function newAction()
{
	if(document.forms[0].change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
		  	document.forms[0].message_title.value="";
		  	if(document.forms[0].message_type.value == "Email"){
		  	  document.forms[0].message_subject.value="";
		  	  document.forms[0].autoEmail.checked=false;
		  	}
		  	document.forms[0].select_title.value=0;
		  	document.forms[0].origin_id.value="FTD";
		  	document.forms[0].message_content.value="";
		  	document.forms[0].newMessageFlag.value="Y";
		 }
	}else{
		  	document.forms[0].message_title.value="";
		  	if(document.forms[0].message_type.value == "Email"){
		  	  document.forms[0].message_subject.value="";
		  	  document.forms[0].autoEmail.checked=false;
		  	}
		  	document.forms[0].select_title.value=0;
		  	document.forms[0].origin_id.value="FTD";
		  	document.forms[0].message_content.value="";
		  	document.forms[0].newMessageFlag.value="Y";
	}
	document.getElementById("save").disabled=false;
	replaceText("lockError", "");
	if(document.forms[0].message_type.value == "Email"){
		document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Email Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
	}else{
		document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Letter Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
	}
}

function mainMenuClick(){
	if(document.forms[0].change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
			if(document.forms[0].message_type.value == "Email"){
				document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Email Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
			}else{
				document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Letter Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
			}
			doMainMenuActionNoPopup();
		}
	}else{
		if(document.forms[0].message_type.value == "Email"){
			document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Email Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
		}else{
			document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=Stock Letter Maintenance&lock_id="+document.forms[0].message_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
		}
		doMainMenuActionNoPopup();
	}
}

function titleChange(){
	document.forms[0].change_flag.value="Y";
}

function changeOrigin()
{
	document.forms[0].originChangeFlag.value = "Y";
	document.forms[0].change_flag.value="Y";
}

]]>
</script>
  </head>

<body onload="init();">
            <xsl:variable name="stock_header">
            <xsl:choose>
              <xsl:when test="$message_type='Email'">
                Stock Email Maintenance
              </xsl:when>
              <xsl:otherwise>
                Stock Letter Maintenance
               </xsl:otherwise>
             </xsl:choose>
          </xsl:variable>
				<xsl:call-template name="header">
					<xsl:with-param name="headerName"  select="$stock_header" />
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="showBackButton" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>

  <form name="messageMaint" method="post" action="">
  		<iframe id="unlockFrame" name="unlockFrame" height="0" width="0" border="0"></iframe>
					<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
   <table width="100%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
     <td>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
        <xsl:call-template name="securityanddata"/>
        <xsl:call-template name="decisionResultData"/>
        <input type="hidden" name="message_type" id="message_type" value="{key('pagedata','message_type')/value}"/>
        <input type="hidden" name="newMessageFlag" id="newMessageFlag" value="{key('pagedata','newMessageFlag')/value}"/>
        <input type="hidden" name="spellCheckFlag" id="spellCheckFlag" value="N" />
        <input type="hidden" name="originChangeFlag" id="originChangeFlag" value="N"/>
        <xsl:variable name="msg_id" select="/root/StockMessageHeader/message_id"/>
        <xsl:variable name="msg_title" select="/root/StockMessageContent/Content/title"/>
        <input type="hidden" name="message_id" id="message_id" value="{$msg_id}" />
        <input type="hidden" name="action_type" id="action_type" value="load_titles" />
        <input type="hidden" name="previous_msg_id" id="previous_msg_id" value="{/root/StockMessageHeader/message_id}"/>
        <input type="hidden" name="change_flag" id="change_flag" value="N"/>

         <tr><td align="right" class="label" width="10%">Stock <xsl:value-of select="key('pagedata','message_type')/value"/>:</td>
				   <td align="left">
            <select name="select_title" id="select_title" size="1" tabindex="1">
             <option value="0">-select one-</option>
            <xsl:for-each select="/root/StockMessageTitles/Title">
             <xsl:choose>
             <xsl:when test="$message_type='Email'">
             <xsl:choose>
             <xsl:when test="$msg_id=stock_email_id">
            	<option value="{stock_email_id}" selected="selected"><xsl:value-of select="title"/></option>
             </xsl:when>
             <xsl:otherwise>
            	<option value="{stock_email_id}"><xsl:value-of select="title"/></option>
             </xsl:otherwise>
             </xsl:choose>
             </xsl:when>
             <xsl:otherwise>
             <xsl:choose>
             <xsl:when test="$msg_id=stock_letter_id">
            	<option value="{stock_letter_id}" selected="selected"><xsl:value-of select="title"/></option>
             </xsl:when>
             <xsl:otherwise>
            	<option value="{stock_letter_id}"><xsl:value-of select="title"/></option>
             </xsl:otherwise>
             </xsl:choose>
             </xsl:otherwise>
             </xsl:choose>
             </xsl:for-each>
            </select>&nbsp;&nbsp;&nbsp;&nbsp;<input tabindex="2" type="button" accesskey="G" name="refresh" id="refresh" class="blueButton" value="(G)o" onClick="loadAction()" />
     </td>
     </tr>
     <tr><td class="label" align="right">Company: </td>
            <td>
            <select tabindex="3" name="origin_id" id="origin_id" size="1" onchange="changeOrigin()">
             <xsl:choose>
             <xsl:when test="/root/StockMessageContent/Content/origin_id='FTD'">
                  <option value="FTD" selected="selected">All</option>
             </xsl:when>
             <xsl:otherwise>
                  <option value="FTD">All</option>
             </xsl:otherwise>
             </xsl:choose>
             <xsl:for-each select="/root/PreferredPartnerList/PreferredPartner">
                 <option>
                     <xsl:if test="/root/StockMessageContent/Content/partner_name=name">
                         <xsl:attribute name="selected">selected</xsl:attribute>
                     </xsl:if>
                     <xsl:if test="/root/StockMessageContent/Content/origin_id=name">
                     	<xsl:attribute name="selected">selected</xsl:attribute>
                     </xsl:if>
                     <xsl:attribute name="value"><xsl:value-of select="name"/></xsl:attribute>
                     <xsl:value-of select="value"/>
                 </option>
             </xsl:for-each>
            </select>
            <xsl:if test="$message_type='Email'">
            <label class="label">Type: </label>
            <select tabindex="4" name="stock_email_type_id" id="stock_email_type_id" size="1">
             <xsl:choose>
             <xsl:when test="/root/StockMessageContent/Content/stock_email_type_id='Normal'">
                  <option value="DCON" selected="selected">Normal</option>
             </xsl:when>
             <xsl:otherwise>
                  <option value="Normal">Normal</option>
             </xsl:otherwise>
             </xsl:choose>
             <xsl:choose>
             <xsl:when test="/root/StockMessageContent/Content/stock_email_type_id='DCON'">
                  <option value="DCON" selected="selected">Delivery Confirmation</option>
             </xsl:when>
             <xsl:otherwise>
                  <option value="DCON">Delivery Confirmation</option>
             </xsl:otherwise>
             </xsl:choose>
            </select>
            </xsl:if>
            
            <xsl:if test="$message_type='Email'">
              &nbsp;&nbsp;&nbsp;&nbsp;<input tabindex="5" type="checkbox" name="autoEmail" id="autoEmail"/><label class="label">Automated Email</label>
  <script>
  <xsl:if test="/root/StockMessageHeader/auto_email='true'">
    document.messageMaint.autoEmail.checked=true;
  </xsl:if>
  <xsl:if test="/root/StockMessageContent/Content/auto_response_indicator='Y'">
    document.messageMaint.autoEmail.checked=true;
  </xsl:if>
  </script>
            </xsl:if>
            &nbsp;&nbsp;&nbsp;&nbsp;
           </td>
				</tr>
				<tr>
         <td align="right" class="label">Title:</td>
				 <td align="left">
          <input tabindex="6" type="text" onchange="titleChange()" name="message_title" id="message_title" size="75" value="{$msg_title}"/>
         </td>
        </tr>
      <xsl:if test="$message_type='Email'">
				<tr>
         <td align="right" class="label">Subject:</td>
				 <td align="left">
   <xsl:variable name="msg_subject" select="/root/StockMessageContent/Content/subject"/>
          <input type="text" tabindex="7" name="message_subject" id="message_subject" size="100" value="{$msg_subject}"/>
         </td>
        </tr>
      </xsl:if>
      <xsl:if test="$message_type='Letter'">
       <input type="hidden" name="message_subject" id="message_subject" value=""/>
      </xsl:if>
				<tr>
         <td align="right" class="label">Body:</td>
				  <td align="left"><textarea tabindex="8" name="message_content" cols="150" rows="12" onChange="setFlag()"><xsl:value-of select='/root/StockMessageContent/Content/body'/></textarea></td>
				</tr>
            </table>
         </td>
      </tr>
   </table>
   <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
	  <tr>
	    <td align="left">
        <input name="spell" id="spell" style="width:90" accesskey="P" type="button" tabindex="9" class="BigBlueButton" value="S(p)ell Check" onClick="spellCheck()"/>
		<input id="save" name="save" style="width:90" tabindex="10" accesskey="S" type="button" class="BigBlueButton" value="(S)ubmit" onClick="saveAction()"/>
        
        <xsl:choose>
			<xsl:when test="key('pagedata','add_new_flag')/value='Y'">
				<input name="copy" id="copy" type="button" style="width:90" tabindex="11" accesskey="C" class="BigBlueButton" value="(C)opy" onClick="copyAction()"/>
			</xsl:when>
			<xsl:otherwise>
				<input name="copy" id="copy" type="button" style="width:90" class="BigBlueButton" value="(C)opy">
				<xsl:attribute name="disabled">true</xsl:attribute>
				</input>
			</xsl:otherwise>
		</xsl:choose>
        
        <xsl:choose>
			<xsl:when test="key('pagedata','add_new_flag')/value='Y'">
				<input name="new" id="new" type="button" style="width:90" tabindex="12" accesskey="A" class="BigBlueButton" value="(A)dd New" onClick="newAction()"/>
			</xsl:when>
			<xsl:otherwise>
				<input name="new" id="new" type="button" style="width:90" class="BigBlueButton" value="(A)dd New">
				<xsl:attribute name="disabled">true</xsl:attribute>
				</input>
			</xsl:otherwise>
		</xsl:choose>
        
        </td><td width="20%" align="right">
 						<button type="button" class="bigBlueButton" name="mainMenu" tabindex="14" accesskey="M" onclick="mainMenuClick()">(M)ain<br/>Menu</button>
         </td>
      </tr>
		 </table>
		 <iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
    </form>
		<xsl:call-template name="footer"></xsl:call-template>
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\editDnis.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->