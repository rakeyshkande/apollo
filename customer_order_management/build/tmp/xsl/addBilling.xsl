<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<!-- format xml numbers if not present -->
<xsl:decimal-format NaN="(0.00)"  name="noDiscount"/>
<xsl:decimal-format NaN="" name="notANumber"/>
<xsl:decimal-format NaN="0.00" name="zeroValue"/>

<!-- node paths -->
<xsl:variable name="CARTBILLING" select="root/CART_BILLINGS/CART_BILLING"/>
<xsl:variable name="ORIGINALCREDITCARD" select="root/ORIGINAL_CREDITCARDS/ORIGINAL_CREDITCARD"/>
<xsl:variable name="ORDERPAYMENTS" select="root/ORDER_PAYMENTS/ORDER_PAYMENT"/>
<!-- variables -->
<xsl:variable name="currentCCType" select="$ORIGINALCREDITCARD/cc_type"/>
<xsl:variable name="currentCCExp" select="$ORIGINALCREDITCARD/cc_expiration"/>
<xsl:variable name="currentCC" select="$ORIGINALCREDITCARD/cc_number"/>
<xsl:variable name="countOfOrderPayments" select="count($ORDERPAYMENTS)"/>
<xsl:variable name="corpPurch" select="key('pageData','corp_purch')/value"/>
<xsl:variable name="isRestricted" select="key('pageData','isRestricted_product')/value"/>
<xsl:variable name="YES" select="'true'"/>
	

<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html" />
      <title>Add Billing</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <style type="text/css">
		 /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 30px;
				width: 100%;
			}
		</style>
      <script type="text/javascript" src="js/FormChek.js"/>
      <script type="text/javascript" src="js/util.js"/>
      <script type="text/javascript" src="js/commonUtil.js" />
      <script type="text/javascript" src="js/addBilling.js"/>
	  <script type="text/javascript">
	  var selectedCCType = "<xsl:value-of select="$currentCCType"/>";
	  var selectedCCExp = "<xsl:value-of select="$currentCCExp"/>";
	  <!-- Used to determine if we displaythe scrolling top div or not -->
	  var countOfOrderPayments = "<xsl:value-of select="$countOfOrderPayments"/>";
    var orderLevel= ''; <!-- This is used when checking the lock -->
    var siteName = '<xsl:value-of select="$sitename"/>';
    var siteNameSsl = '<xsl:value-of select="$sitenamessl"/>';
    var applicationContext = '<xsl:value-of select="$applicationcontext"/>';
  
 	  function init(){
       setNavigationHandlers();
 	     addDefaultListeners();
 	     buildCCYearList('exp_date_year');
 	     populateCreditCardInfo('pay_type','exp_date_month','exp_date_year', selectedCCType, selectedCCExp);
       
 	  	 setScrollingDivHeight();
       orderLevel = (document.getElementById('billing_level').value == "cart") ? "updateBilling" : "updatePayment";	
 		   window.onresize = setScrollingDivHeight;
 	  }
 	  </script>
   </head>
<body onload="init();">

<xsl:call-template name="addHeader"/>

 <div id="mainContent" style="display:block">
 <!-- These iframes are initialized with a "dummy" src value to
 prevent the MS IE browser from detecting these as nonsecure resources. -->
 <iframe id="checkLock" width="0px" height="0px" border="0px" src="https://{$sitenamessl}{$applicationcontext}/html/blank.html"/>

 <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0px" src="https://{$sitenamessl}{$applicationcontext}/html/blank.html"/>

<form name="addBillingForm" id="addBillingForm" method="post" action="">
<xsl:call-template name="securityanddata"/>
<xsl:call-template name="decisionResultData"/>
   <!-- hidden fields -->
	<input type="hidden" name="action_type" id="action_type" value=""/>
  <!--
     return_action - if the user has typed in a merch, add_on or service shipping
     amount but the page has not been recalculated we set this hidden field to
     "submit" and use this to recall the doSubmitBilling() method once the page
     has been recalculated
  -->
  <input type="hidden" name="return_action" id="return_action" onchangeIgnore="true" value=""/>
  <input type="hidden" name="page_action" id="page_action" value=""/>
	<input type="hidden" name="aafes_code" id="aafes_code" value="" />
	<input type="hidden" name="auth_code" id="auth_code" value="" />
	<input type="hidden" name="manual_auth" id="manual_auth" value="" />
	<input type="hidden" name="total_amt" id="total_amt" value="" />
	<input type="hidden" name="change" id="change" value=""  />
	<input type="hidden" name="shipping_fee" id="shipping_fee" value="{$CARTBILLING/shipping_fee}" />
	<input type="hidden" name="service_fee" id="service_fee" value="{$CARTBILLING/service_fee}" />
  <input type="hidden" name="shipping_tax" id="shipping_tax" value="{$CARTBILLING/shipping_tax}"/>
  <input type="hidden" name="service_fee_tax" id="service_fee_tax" value="{$CARTBILLING/service_fee_tax}"/>
  <input type="hidden" name="product_tax" id="product_tax" value="{$CARTBILLING/product_tax}"/>
	<input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}" />
  <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}" />
	<input type="hidden" name="billing_level" id="billing_level" value="{key('pageData','billing_level')/value}" />
	<input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}" />
	<input type="hidden" name="gcc_status" id="gcc_status" value=""/>
	<input type="hidden" name="amount_owed" id="amount_owed" value=""/>
	<input type="hidden" name="gcc_num" id="gcc_num" value=""/>
	<input type="hidden" name="gcc_amt" id="gcc_amt" value=""/>
	<input type="hidden" name="csr_id" id="csr_id" value="{key('pageData','csr_id')/value}"/>
	<input type="hidden" name="entity_type" id="entity_type" value="{key('pageData','entity_type')/value}"/>
  <input type="hidden" name="order_level" id="order_level" value="{key('pageData','order_level')/value}"/>
  <input type="hidden" name="entity_id" id="entity_id" value="{key('pageData','entity_id')/value}"/>
  <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData','external_order_number')/value}"/>
  <input type="hidden" name="total" id="total" value="{$CARTBILLING/cart_total}"/>
  <input type="hidden" name="total_owed" id="total_owed" value=""/>
  <input type="hidden" name="tax" id="tax" value="{format-number($CARTBILLING/tax,'$#,###,##0.00;($#,###,##0.00)','zeroValue')}"/>
  <input type="hidden" name="total_charge" id="total_charge" value="{format-number($CARTBILLING/cart_total,'#,###,##0.00;#,###,##0.00','zeroValue')}"/>
  <input type="hidden" name="discount" id="discount" value="{format-number($CARTBILLING/discount_amount,'#,###,##0.00','zeroValue')}"/>
  <input type="hidden" name="ship_method" id="ship_method" value="{SHIPPING_METHODS/SHIPPING_METHOD/ship_method}"/>
  <input type="hidden" name="product_type" id="product_type" value="{SHIPPING_METHODS/SHIPPING_METHOD/product_type}"/>
  <input type="hidden" name="vendor_flag" id="vendor_flag" value="{SHIPPING_METHODS/SHIPPING_METHOD/vendor_flag}"/>
  <input type="hidden" name="no_tax_flag" id="no_tax_flag" value="{SHIPPING_METHODS/SHIPPING_METHOD/no_tax_flag}"/>
  <input type="hidden" name="is_orig_cc" id="is_orig_cc" value=""/>
  <input type="hidden" name="mgr_password" id="mgr_password" value=""/>
  <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
  <input type="hidden" name="cc_orig_number" id="cc_orig_number" value=""/>
  
  <input type="hidden" name="merchandise" id="merchandise" value=""/>
  <input type="hidden" name="add_on" id="add_on" value=""/>
  <input type="hidden" name="service_shipping" id="service_shipping" value=""/>

  <input type="hidden" name="product_id" id="product_id" value="{SHIPPING_METHODS/product_id}"/>
  <input type="hidden" name="recipient_state" id="recipient_state" value="{SHIPPING_METHODS/recipient_state}"/>

  <input type="hidden" name="tax1_name" id="tax1_name"/>
  <input type="hidden" name="tax1_description" id="tax1_description"/>
  <input type="hidden" name="tax1_rate" id="tax1_rate"/>
  <input type="hidden" name="tax1_amount" id="tax1_amount"/>

  <input type="hidden" name="tax2_name" id="tax2_name"/>
  <input type="hidden" name="tax2_description" id="tax2_description"/>
  <input type="hidden" name="tax2_rate" id="tax2_rate"/>
  <input type="hidden" name="tax2_amount" id="tax2_amount"/>

  <input type="hidden" name="tax3_name" id="tax3_name"/>
  <input type="hidden" name="tax3_description" id="tax3_description"/>
  <input type="hidden" name="tax3_rate" id="tax3_rate"/>
  <input type="hidden" name="tax3_amount" id="tax3_amount"/>

  <input type="hidden" name="tax4_name" id="tax4_name"/>
  <input type="hidden" name="tax4_description" id="tax4_description"/>
  <input type="hidden" name="tax4_rate" id="tax4_rate"/>
  <input type="hidden" name="tax4_amount" id="tax4_amount"/>

  <input type="hidden" name="tax5_name" id="tax5_name"/>
  <input type="hidden" name="tax5_description" id="tax5_description"/>
  <input type="hidden" name="tax5_rate" id="tax5_rate"/>
  <input type="hidden" name="tax5_amount" id="tax5_amount"/>

  <input type="hidden" name="wallet_indicator"  id="wallet_indicator" value="{$ORDERPAYMENTS/wallet_indicator}"/>
  <input type="hidden" name="cardinal_verified_flag"  id="cardinal_verified_flag" value="{$ORDERPAYMENTS/cardinal_verified_flag}"/>
  <input type="hidden" name="route"  id="route" value="{$ORDERPAYMENTS/route}"/>

 <!-- Display table-->
 <table width="98%" align="center" border="0" class="mainTable" cellpadding="1" cellspacing="1">
   <tr>
      <td colspan="4">
         <table border="0" width="100%" cellpadding="0" cellspacing="0" class="innerTable">
            <tr>
			         <td>
		              <!-- Scrolling div contiains orders -->
                   <div class="tableContainer" id="messageContainer"  >
                     <table class="scrollTable" cellpadding="0" cellspacing="0" border="0"  style="width:97%;">
				               <thead class="fixedHeader" id="fixedHeader">
                          <tr>
                             <th></th>
                             <th></th>
                             <th></th>
                             <th></th>
                          </tr>
				               </thead>
		                   <tbody class="scrollContent" id="scrollContent">
				                   <xsl:for-each select="ORDER_PAYMENTS/ORDER_PAYMENT">
					                    <tr>                      
						                     <td><div style="padding-left:1em;" align="left">Charge Amount:&nbsp;&nbsp;<xsl:value-of select="format-number(credit_amount,'#,###,##0.00','zeroValue')"/>  </div></td>
                                 <td><div style="padding-left:1em;" align="left">Payment Type:&nbsp;&nbsp; <xsl:value-of select="payment_type"/> </div></td>
                                 <xsl:choose>
                                     <xsl:when test="(string-length(card_number) = 4)">
                                        <td><div style="padding-left:1em;" align="left">Credit Card # :&nbsp;&nbsp; ************<xsl:value-of select="card_number"/> </div></td>
                                     </xsl:when>
                                     <xsl:otherwise>
                                        <td><div style="padding-left:1em;" align="left">Credit Card # :&nbsp;&nbsp; <xsl:value-of select="card_number"/> </div></td>
                                     </xsl:otherwise>
                                 </xsl:choose>
	                               <td><div style="padding-left:1em;" align="left">Exp: &nbsp;&nbsp; <xsl:value-of select="expiration_date"/></div></td>
					                   </tr>
				                  </xsl:for-each>
		                   </tbody>
			              </table>
	             	</div>
               </td>
            </tr>
            <tr>
            <tr>
	           <td><br /></td>
            </tr>
               <td>
                  <table border="0" cellpadding="1" cellspacing="0" width="100%" >
                     <tr>
                        <td class="TotalLine" colspan="4">
                           <div align="center" class="TotalLine">
                              <div align="left"><strong>Payment</strong></div>
                          </div>
                        </td>
                     </tr>
                     <tr id="giftCertRow" style="display:block">
                        <td id="giftCertCell" colspan="4" class="RequiredFieldTxt" style="padding-left:.5em;">
                        </td>
                     </tr>
                     <tr>
                        <td colspan="4" ><br /></td>
                     </tr>
                     <tr>
                        <td class="LabelRight" width="21%"><div align="right"><strong>Payment Type: </strong></div></td>
                        <td colspan="3" class="TextField">
                           <select tabindex="1" id="pay_type" name="pay_type" onchange="resetCreditCardLists('exp_date_month','exp_date_year','credit_card_num', null)">
                              <xsl:if test="$corpPurch = $YES"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
                  	          <option>-- Please Select --</option>
             	                 <xsl:for-each select="PAYMENT_METHODS/PaymentMethodVO">
             	                    <xsl:if test="paymentMethodId != 'MS'">
                                    <option value="{paymentMethodId}"><xsl:value-of select="description"/></option>
             	                    </xsl:if>
             	                 </xsl:for-each>
                           </select>
                           <span id="aafesField" style="padding-left:4em;display:none">
                             <span class="Label">AAFES Code: </span>
                             <input type="text" name="aafes_code_entered" id="aafes_code_entered" value=""/>
                          </span>
                          <span id="noChargeField" style="padding-left:1em;display:none">
                           	  <span class="Label">Manager Approval Code: </span>
                              <input type="text" name="mgr_appr_code" tabindex="2" id="mgr_appr_code" value=""/>&nbsp;&nbsp;&nbsp;
                              <span class="Label">Manager Password:</span>
                              <input type="password" name="password" tabindex="3" id="password" value="" onblur="forms[0].mgr_password.value = this.value;"/>
                          </span>
                        </td>
                     </tr>
                     <tr id="pay_type_validation_message_row" style="display:none" >
                         <td />
                         <td id="pay_type_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                     </tr>
                     <tr>
                        <td class="LabelRight">Credit Card Number:</td>
                        <td colspan="3" class="TextField">
                              <input type="text" size="35" tabindex="4" name="credit_card_num" id="credit_card_num" onkeypress="return doOnlyNumeric()" value="" >
								<xsl:if test="$corpPurch = $YES"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
							  </input>
								<script type="text/javascript">
									var paymentType;
									var ccNumber = '';
								<xsl:for-each  select="ORIGINAL_CREDITCARDS/ORIGINAL_CREDITCARD">
									paymentType = '<xsl:value-of select="cc_type"/>';																			
									if(paymentType!='GD') {
									  if(paymentType!='MS') {
									     ccNumber = '************'+'<xsl:value-of select="cc_number"/>';
									  }
									}
								</xsl:for-each>
									document.getElementById("cc_orig_number").value = ccNumber;
									document.getElementById("credit_card_num").value = ccNumber;
								</script>
                        </td>
                     </tr>
                      <tr id="credit_card_num_validation_message_row" style="display:none" >
                         <td />
                         <td id="credit_card_num_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                     </tr>
                     <tr>
                        <td class="LabelRight"><div align="right"><strong>Expiration Date: </strong></div></td>
                        <td colspan="3" class="TextField">
                           <select tabindex="5" id="exp_date_month" name="exp_date_month">
                           <xsl:if test="$corpPurch = $YES"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
                              <option> </option>
                              <option value="01">January</option>
                              <option value="02">February</option>
                              <option value="03">March</option>
                              <option value="04">April</option>
                              <option value="05">May</option>
                              <option value="06">June</option>
                              <option value="07">July</option>
                              <option value="08">August</option>
                              <option value="09">September</option>
                              <option value="10">October</option>
                              <option value="11">November</option>
                              <option value="12">December</option>
                           </select>
                          /
                           <select tabindex="6" id="exp_date_year" name="exp_date_year">
                              <xsl:if test="$corpPurch = $YES"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
		                     </select>
  		                </td>
                     </tr>
                      <tr id="exp_date_month_validation_message_row" style="display:none" >
                         <td />
                         <td id="exp_date_month_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                     </tr>

                     <tr id="exp_date_year_validation_message_row" style="display:none">
                        <td />
                         <td id="exp_date_year_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                     </tr>

                     <tr>
                        <td class="LabelRight">Merchandise Amount:</td>
                        <td colspan="3" class="TextField">
   		               	   <input type="text" tabindex="7" maxlength="9" size="35" name="merchandise_display" id="merchandise_display" onblur="changeRecalculateOrder()"  value="{format-number($CARTBILLING/product_amount,'#,##0.00;#,##0.00','notANumber')}" onkeypress="return doOnlyNumeric();">
	               		      <xsl:if test="$CARTBILLING/product_amount != ''"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
		               	   </input>
                        </td>
                     </tr>
                       <tr id="merchandise_display_validation_message_row" style="display:none">
                          <td />
                         <td id="merchandise_display_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                     </tr>
                     <tr>
                        <td class="LabelRight">Add-On Amount:</td>
		                   <td colspan="3" class="TextField">
		                      <input type="text" tabindex="8" size="35" maxlength="12" name="add_on_display"  id="add_on_display" onblur="changeRecalculateOrder()"  value="{format-number($CARTBILLING/add_on_amount,'#,###,##0.00;#,###,##0.00','notANumber')}" onkeypress="return doOnlyNumeric();">
		                         <xsl:if test="$CARTBILLING/add_on_amount != ''"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
                                 <xsl:if test="$isRestricted = $YES"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
		                      </input>
                           </td>
		                </tr>
		                <tr id="add_on_display_validation_message_row" style="display:none">
                          <td />
                         <td id="add_on_display_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                       </tr>
			            <tr>
			               <td class="LabelRight">Shipping / Service Amount:</td>
			               <td colspan="3" class="TextField">
			                     <input type="text" tabindex="9" size="35" maxlength="12" name="service_shipping_display" id="service_shipping_display" onblur="changeRecalculateOrder()" value="{format-number($CARTBILLING/serv_ship_fee,'#,###,##0.00;#,###,##0.00','notANumber')}" onkeypress="return doOnlyNumeric();" >
			                        <xsl:if test="$CARTBILLING/serv_ship_fee != ''"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
			                     </input>
			               </td>
			            </tr>
			            <tr id="service_shipping_display_validation_message_row" style="display:none">
                          <td />
                         <td id="service_shipping_display_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                       </tr>
			            <tr>
						   <td class="LabelRight">Discount Amount:</td>
						   <td colspan="3" class="indent">
						      ($<xsl:value-of select="format-number($CARTBILLING/discount_amount,'##,###,##0.00','zeroValue')"/>)
			      	 </td>
			            </tr>
			            <tr>
			               <td class="LabelRight">Tax Amount:</td>
			               <td colspan="3" id="tax_amount" class="indent">
			                  $<xsl:value-of select="format-number($CARTBILLING/tax,'##,###,##0.00;##,###,##0.00','zeroValue')"/>
                     </td>
			            </tr>
			            <tr>
			               <td class="LabelRight">Total Charge Amount: </td>
			               <td id="total_charge_amt" colspan="3" class="indent">
			                  $<xsl:value-of select="format-number($CARTBILLING/cart_total,'##,###,##0.00;##,###,##0.00','zeroValue')"/>
                      </td>
			            </tr>
			            <tr>
					       <td><br /></td>
                        </tr>
			            <tr>
							<td colspan="4" class="TotalLine"><br /></td>
			            </tr>
			            <tr>
							<td colspan="4"><br /></td>
			            </tr>
                     </table>
                  </td>
		       </tr>
           </table>
        </td>
     </tr>
</table>
<!-- action buttons -->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
   <tr>
      <td>
         <div align="center">
            <button type="button" class="BlueButton" name="submit_billing_but" tabindex="10" accesskey="S" onclick="doSubmitBilling()">
               (S)ubmit Billing
            </button>&nbsp;&nbsp;
            <button type="button" class="BlueButton" name="cancel_but" tabindex="11" accesskey="C" onclick="doCancelUpdateBilling()">(C)ancel</button>
         </div>

      </td>
   </tr>
</table>
</form>
</div>
<!-- Processing message div -->
 <div id="waitDiv" style="display:none">
    <table align="center" id="waitTable" height="100%" class="mainTable" width="98%"  border="0" cellpadding="0" cellspacing="2" >
       <tr>
          <td height="100%">
	         <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="innerTable">
		       <tr>
              <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
              <td id="waitTD"  width="50%" class="WaitMessage"></td>
		       </tr>
	         </table>
          </td>
       </tr>
    </table>
 </div>
<!--footer-->
<xsl:call-template name="footer"/>
</body>

</html>
</xsl:template>

<!-- call to header template -->
<xsl:template name="addHeader">
   <xsl:call-template name="header">
      <xsl:with-param name="showBackButton" select="false()"/>
      <xsl:with-param name="showPrinter" select="false()"/>
      <xsl:with-param name="headerName" select="'Add Billing'"/>

      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
   </xsl:call-template>
</xsl:template>


</xsl:stylesheet>