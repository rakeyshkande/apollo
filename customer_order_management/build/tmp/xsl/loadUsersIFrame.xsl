<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
 
  function loadUsers()
  {
    var tdElement = parent.document.getElementById("selectBoxTd");

    var newCustomersValidated = new Array( <xsl:for-each select="PPV_VALIDATED_CUSTOMERS/VALIDATED_CUSTOMER">
                                          ["<xsl:value-of select="customer_id"/>"]
                                          <xsl:choose>
                                            <xsl:when test="position()!=last()">,</xsl:when>
                                          </xsl:choose>
                                        </xsl:for-each>
                                      );

    var newOrdersValidated = new Array(  <xsl:for-each select="PPV_VALIDATED_ORDER_DETAILS/VALIDATED_ORDER_DETAIL">
                                        ["<xsl:value-of select="order_detail_id"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                   );

    var newBypassPrivacyPolicyVerification = '<xsl:value-of select="key('pageData','bypass_privacy_policy_verification')/value"/>';
    var customerValidationRequiredFlag = '<xsl:value-of select="key('pageData','customer_validation_required_flag')/value"/>';
    var orderValidationRequiredFlag= '<xsl:value-of select="key('pageData','order_validation_required_flag')/value"/>';
    if (parent.updatePrivacyPolicyVariables) 
      parent.updatePrivacyPolicyVariables(newCustomersValidated, newOrdersValidated, newBypassPrivacyPolicyVerification, customerValidationRequiredFlag, orderValidationRequiredFlag); 

    var userCount = <xsl:value-of select="count(CSR_VIEWINGS/CSR_VIEWING)"/>;

    // Erase the current user display
    while(tdElement.hasChildNodes())
    {
      tdElement.removeChild(tdElement.firstChild);
    }
    

    if(userCount > 0)
    {
      // Check to see if there was an error; if so display the error message without a label.
      var error = '<xsl:value-of select="CSR_VIEWINGS/CSR_VIEWING/error_mesg"/>';
      if(error != "")
      {
        var errorSpan = parent.document.createElement("span");
        errorSpan.className = "ErrorMessage";
  
        var errorText = parent.document.createTextNode(error);
  
        errorSpan.appendChild(errorText);
  
        tdElement.appendChild(errorSpan);
      }
      else
      {
        // Now, create a new user display element whose type depends upon the number of csrs:
        // a text field for one, a select drop-down for multiple.
        
        // First, create the label.
        var labelSpan = parent.document.createElement("span");
        labelSpan.className = "ErrorMessage";
        
        var labelText = parent.document.createTextNode("Currently being viewed by: ");
      
        labelSpan.appendChild(labelText);
        
        tdElement.appendChild(labelSpan);
        
        // Now, create the csr display.
        if(userCount == 1)
        {
          var csrSpan = parent.document.createElement("span");
          csrSpan.setAttribute("id", "csrLabel");
          
          var userName = parent.document.createTextNode('<xsl:value-of select="CSR_VIEWINGS/CSR_VIEWING"/>');
          
          csrSpan.appendChild(userName);
          
          tdElement.appendChild(csrSpan);
        }
        else
        {
          var users = new Array( 
            <xsl:for-each select="CSR_VIEWINGS/CSR_VIEWING">
              ['<xsl:value-of select="."/>']
              <xsl:if test="position() != last()">
                <xsl:text>,</xsl:text>
              </xsl:if>
            </xsl:for-each>
          );
    
          var selectBox = parent.document.createElement("select");
          selectBox.setAttribute("id", "currentCSRs");
          selectBox.setAttribute("name", "currentUsers");
    
          <![CDATA[
          var newOpt;
          for(var i = 0; i < users.length; i++)
          {    
            newOpt = parent.document.createElement("option");
            newOpt.text = users[i];
            selectBox.options.add(newOpt,i);
          }
          ]]>
          
          tdElement.appendChild(selectBox);
        }
      }
    }
  }
</script>
</head>
<body onload="javascript:loadUsers();">
  <form>
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
  </form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>