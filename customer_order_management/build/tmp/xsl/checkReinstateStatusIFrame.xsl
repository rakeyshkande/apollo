<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var statusValue = '<xsl:value-of select="key('pageData','reinstate_flag')/value"/>';
<![CDATA[
	function checkStatus()
	{
	    if(statusValue == 'Y')
	    {    
	      parent.processReinstate();
	    }
	    else
	    {
	      alert('Total redemption amount of gift certificate\nhas not been refunded - cannot reinstate');
	    }
	}
]]>
</script>
</head>
<body onload="javascript:checkStatus();">
    <xsl:call-template name="securityanddata"/>   
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>