<!DOCTYPE ACDemo [
 	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--External Templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!--variables-->
<xsl:variable name="in_csr_id" select="key('pageData','in_csr_id')/value"/>

 <xsl:template match="/">
	<html>
		<head>
			<title>FTD - Csr Locked Records</title>
			<link rel="stylesheet" href="css/ftd.css"/>
			<style type="text/css">
		    /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 225px;
				width: 100%;
			}
			</style>
			<script type="text/javascript" src="js/tabIndexMaintenance.js"/>
			<script type="text/javascript" src="js/util.js"/>
			<script type="text/javascript" src="js/commonUtil.js"/>
			<script type="text/javascript" src="js/FormChek.js"/>
			<script type="text/javascript" src="js/csrLockedRecords.js"/>
		</head>
		<body onload="javascript:init();">
			<xsl:call-template name="addHeader"/>
		  <form name="csrLockedRecord" method="post" onkeypress="doUnlockRecordAction('release_all');" >
        <input type="hidden" name="in_csr_id" id="in_csr_id" value="{$in_csr_id}" />
        <xsl:call-template name="securityanddata"/>
        <xsl:call-template name="decisionResultData"/>

      <div id="content" style="display:block">
			<!--  Main Table thick blue border -->
			   <table class="mainTable" align="center" width="98%" cellpadding="0" cellspacing="1">
				   <tr>
					   <td><!-- Inner table used to show thin blue border -->
						   <table class="innerTable" cellpadding="0" cellspacing="0" width="100%" border="0">
							   <tr>
								   <td colspan="2">
								     <div class="tableContainer" id="messageContainer" >
									   <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
										 <thead class="fixedHeader">
						                <tr style="text-align:left">
								              <td  colspan="5" class="ErrorMessage" width="100%">Csr Id:&nbsp;<xsl:value-of select="key('pageData', 'in_csr_id')/value"/></td>
								            </tr>
                            <tr style="text-align:left">
								              <td class="ColHeader" width="2%"></td>
								              <td class="ColHeader" width="33%">&nbsp;Customer Name</td>
								              <td class="ColHeader" width="25%">&nbsp;Customer Id</td>
								              <td class="ColHeader" width="25%">&nbsp;Order Number</td>
								              <td class="ColHeader" width="15%">&nbsp;Lock Date</td>
								            </tr>
								            </thead>
								             <tbody class="scrollContent" >
												<xsl:for-each select="root/csr_locked_entities/csr_locked_entity">
                          <input type="hidden" name="entitytype" id="entitytype" value=""/>
                          <input type="hidden" name="entityid" id="entityid" value=""/>
                        <xsl:choose>
                          <xsl:when test="entity_type = 'CUSTOMER'">
                            <tr style="vertical-align:text-top;" id="row">
                              <td width="23px" style="text-align:left;">
                               <input TYPE="checkbox" id="lock_checkbox" name="lock_checkbox" value="{entity_type}^{entity_id}" tabindex="{position()}"/>
                              </td>
                              <td id="td_{@num}" style="text-align:left;" valign="center"><xsl:value-of select="entity_description"/></td>
                              <td style="text-align:left;"><xsl:value-of select="entity_id"/></td>
                              <td style="text-align:left;">&nbsp;</td>
                              <td style="text-align:left;">
                                <xsl:value-of select="locked_on"/>
                              </td>
                            </tr>
                          </xsl:when>
                          <xsl:when test="entity_type = 'ORDER_DETAILS'">
                             <tr style="vertical-align:text-top;" id="row">
                              <td width="23px" style="text-align:left;">
                               <input TYPE="checkbox" id="lock_checkbox" name="lock_checkbox" value="{entity_type}^{entity_id}" tabindex="{position()}"/>
                              </td>
                              <td id="td_{@num}" style="text-align:left;">&nbsp;</td>
                              <td style="text-align:left;">&nbsp;</td>
                              <td style="text-align:left;"><xsl:value-of select="entity_description"/></td>
                              <td style="text-align:left;">
                                <xsl:value-of select="locked_on"/>
                              </td>
                            </tr>
                          </xsl:when>
                          <xsl:when test="entity_type = 'ORDERS'">
                             <tr style="vertical-align:text-top;" id="row">
                              <td width="23px" style="text-align:left;">
                               <input TYPE="checkbox" id="lock_checkbox" name="lock_checkbox" value="{entity_type}^{entity_id}" tabindex="{position()}"/>
                              </td>
                              <td id="td_{@num}" style="text-align:left;">&nbsp;</td>
                              <td style="text-align:left;">&nbsp;</td>
                              <td style="text-align:left;"><xsl:value-of select="entity_description"/></td>
                              <td style="text-align:left;">
                                <xsl:value-of select="locked_on"/>
                              </td>
                            </tr>
                          </xsl:when>
                         </xsl:choose>
											 </xsl:for-each>
											</tbody>
										</table>
									 </div>
								   </td>
							   </tr>
                 <tr>
							       <td colspan="5" class="Label" width="100%" >
							       		&nbsp;
							       </td>
							   </tr> 
						     <tr>
							       <td colspan="5" align="center" class="Label" width="50%" >
							       		<button type="button" id="releaseAllRecords" class="BlueButton" tabindex="100" accesskey="R" onclick="javascript:doReleaseAllAction();">(R)elease All Records</button>
                        &nbsp;
                        <button type="button" id="releaseSelectedRecords" class="BlueButton" tabindex="101" accesskey="S" onclick="javascript:doUnlockRecordAction('release_selected_records');">Release (S)elected Records</button>
                        &nbsp;
                        <button type="button" id="clearThemAll" class="BlueButton" tabindex="102" accesskey="C" onclick="javascript:uncheckAll();">(C)lear All</button>
                     </td>						       
    						 </tr> 
						   </table><!-- end Inner -->
					   </td>
				   </tr>
			   </table><!-- end Main-->
			   <table border="0" width="98%" align="center" cellpadding="0" cellspacing="1" >
					<tr valign="top">
						<td colspan="5" align="right">
							<button type="button" class="BlueButton" tabindex="103" accesskey="b" onclick="javascript:doBackAction();" id="backSearchButton">(B)ack</button>
						</td>
					</tr>
					<tr>
						<td colspan="5" align="right">
						   <button type="button" class="BigBlueButton" tabindex="104" onclick="javascript:doMainMenuAction();" style="width:55px;" accesskey="m"  id = "mainMenuButton" >(M)ain<br/>Menu</button>
						</td>
					</tr>
				</table>
        <!-- These buttons will provide the same functionality as the navigational links.  Buttons were
             implemented to use the acesskey functionality-->				
       </div>
       
       <!-- Used to dislay Processing... message to user -->
		<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>
       <xsl:call-template name="footer"/>
			</form>
		</body>
	</html>
 </xsl:template>
  <!-- Actual call to header w/ params-->
  <xsl:template name="addHeader">
	<xsl:call-template name="header">
		<xsl:with-param name="showPrinter" select="false()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
		<xsl:with-param name="showSearchBox" select="false()"/>
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="headerName" select="'CSR Locked Records'"/>
	 	<xsl:with-param name="entryAction" select="true()"/>
		<xsl:with-param name="backButtonLabel" select="'(B)ack'"/>
		<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
  </xsl:template>
</xsl:stylesheet>