
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
      <META http-equiv="Content-Type" content="text/html"/>
      <title>TAG Screen</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	<base target="_self" />
      <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" language="JavaScript">
           var count = 0;
           var shouldReload="N";
				
<![CDATA[
function init()
{
document.getElementById('printer').tabindex = 5;
}
function doSubmit()
{
var disposition = document.getElementById('tag_disposition').value
var priority = document.getElementById('tag_priority').value

if (disposition == 'none' || priority == 'none')
alert ('You must select a disposition and priority code')
else
tagOrder()
}

function tagOrder()
{
tag_frame.location="tagOrder.do?tag_disposition=" + document.forms[0].tag_disposition.value + "&tag_priority=" + document.forms[0].tag_priority.value + "&order_detail_id=" + document.forms[0].order_detail_id.value + "&action=tag_order" + "&securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
}

function doClose ()
{
	
  top.returnValue = shouldReload;

	top.close()
}

function performcheck()
{
var multag =  tag_frame.iframeform.multiple_tags.value;
var csrtag =  tag_frame.iframeform.order_already_tagged_to_csr.value;

 if(multag == 'Y')
        {
		alert('You cannot tag this order, there are two users already tagged');
		top.close()
        }
        if(csrtag == 'Y')
        {
		alert('You already have this order tagged');
		top.close()
        }
        else if(csrtag == 'N' && multag =='N' )
        {
         shouldReload="Y";
         doClose ();
        }

}

function countag()
{
count++;

        if (count > 1)
        {
        document.write(" , ");
        }

}

 ]]></script>
			</head>
			<body onload="javascript:init();">
				<form >
<div id="iframe_div">
  <iframe id="tag_frame" name="tag_frame" tabindex="-1" width="0px" height="0px" border="0" >
  </iframe>
</div>
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="order_detail_id" id="order_detail_id" value="{root/order_detail_id}"/>
					<!-- Header-->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName" select="'Tag Order'" />
						<xsl:with-param name="dnisNumber" select="$call_dnis"/>
						<xsl:with-param name="brandName" select="$call_brand_name" />
						<xsl:with-param name="indicator" select="$call_type_flag" />
						<xsl:with-param name="cservNumber" select="$call_cs_number" />
						<xsl:with-param name="showPrinter" select="true()"/>
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- Display table-->
<table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td>
				<table width="100%" align="center" class="innerTable">
               <tr class="script">
                 <td colspan="2" align="center" class="largeErrorMsgTxt"><p>&nbsp;</p>
                   <p>ORDER CURRENTLY TAGGED TO
                   <xsl:for-each select="root/order_tag/csr_id">
	                   <script>countag();</script>
	                   <xsl:value-of select="csr_id"/>
                   </xsl:for-each>
                   </p>

                   <p><br/>
                   </p></td>
                 </tr>
               <tr class="script">
                 <td width="44%" align="center"></td>
                 <td width="56%" align="center"></td>
          </tr>
               <tr valign="top" class="Label">
                 <td height="46"><div align="right">Disposition:

                 </div></td>
                 <td height="46">
                 <select id="tag_disposition" class="select" tabindex="1">
                 <option value="none"></option>
                 <xsl:for-each select="root/disposition/disposition_code">
                 		<option value="{tag_disposition}">
						<xsl:value-of select="description" />
						</option>
                   </xsl:for-each>
                 </select>
                 </td>
               </tr>
               <tr valign="top" class="Label">
                 <td height="46"><p align="right">Priority Code:

                   </p></td>
                 <td height="46"><select id="tag_priority" class="select" tabindex="2">
                  <option value="none"></option>
                 <xsl:for-each select="root/priority/priority_code">
                 		<option value="{tag_priority}">
						<xsl:value-of select="description" />
						</option>
                   </xsl:for-each>
                 </select></td>
               </tr>
               <tr class="Label">
                 <td colspan="3" align="center"><p>&nbsp;</p>
                 <p>&nbsp;</p></td>
          </tr>
		  </table>
  		</td>
     </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td align="center">
								<button type="button" class="BlueButton" accesskey="S" tabindex="3" onclick="doSubmit();">(S)ubmit</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							    <button type="button" class="BlueButton" accesskey="C" tabindex="4" onclick="doClose();">(C)lose</button>
						</td>
						</tr>
					</table>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>