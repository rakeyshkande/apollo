
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
        <xsl:import href="securityanddata.xsl" />
        <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>

	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
        <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:template match="/">

  <!-- XSL Variables -->
  <xsl:variable name="bypassCOM" select="/root/pageData/data[name='bypass_com']/value"/>
  <xsl:variable name="userCanUpdate"  select="key('pagedata','user_can_update')/value"/>
  <xsl:variable name="taborigin"  select="key('pagedata','origin_tab')/value"/>

		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Order Comments</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<link rel="stylesheet" href="css/ftdCOMTabs.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/tabIndexMaintenance.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript" src="js/updateComment.js"></script>
				<script type="text/javascript" src="js/jquery-1.12.3.js"></script>
				<script type="text/javascript"><![CDATA[
				
var cos_show_previous = "true";
var cos_show_next = "true";
var cos_show_first = "true";
var cos_show_last = "true";

var count = 0;
    
function countag()
{
  count++;

  if (count > 1)
  {
    document.write(", ");
  }
}

function checkKey()
{
  var tempKey = window.event.keyCode;

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cos_show_first == "true")
  {
    loadCommentsAction('Order', --orderComments.page_position.value);
  }

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cos_show_last == "true")
  {
    loadCommentsAction('Order', ++orderComments.page_position.value);
  }

  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cos_show_next == "true")
  {
    loadCommentsAction('Order', orderComments.page_position.value = orderComments.total_pages.value);
  }

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cos_show_previous == "true")
  {
    loadCommentsAction('Order', orderComments.page_position.value = 1);
  }
}

/*******************************************************************************
*  setOrderCommentIndexes()
*  Sets tab indexes for Order Comment Page.
*  See tabIndexMaintenance.js
*******************************************************************************/
function setOrderCommentIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('searchAction', 'addComment', 'customerComment', 
    'saveAction', 'shoppingCart', 'refreshAction', 'nextItem', 'backItem', 
    'firstItem', 'lastItem', 'hide_system', 'viewEmail', 'commButton', 
    'backButton', 'mainMenu', 'printer');

  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}


// init code
function init()
{
  setOrderCommentIndexes();
  var form = document.forms[0];
  form.comment_text.disabled=true;
  // save button is normally disabled
  document.getElementById("saveAction").disabled=true;
  document.getElementById("shoppingCart").disabled=true;


]]>
			<xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
      form.comment_text.disabled=true;
      form.saveAction.disabled=true;
      form.shoppingCart.disabled=true;
      form.addComment.disabled=false;
      replaceText("lockError", "Current record is locked by <xsl:value-of select='/root/out-parameters/OUT_LOCKED_CSR_ID'/>");
			</xsl:if>
			
			
		<xsl:if test="$taborigin = 'Comments'">
				$("#Comments").removeClass().addClass('selected');
				$("#Dtlcomments").removeClass().addClass('button');
		</xsl:if>
		<xsl:if test="$taborigin = 'DetailedComments'">
				$("#Comments").removeClass().addClass('button');
				$("#Dtlcomments").removeClass().addClass('selected');	
		</xsl:if> 	
		
		<![CDATA[

}

function doOrderSearchAction(orderNumberType)
{
  var form = document.forms[0];
  var order_number = null;
  //if both external and master order numbers are present, use external order number
  if(form.external_order_number.value!=null)
  {
    order_number = form.external_order_number.value;
  }
  else
  {
    order_number = form.master_order_number.value;
  }

  if(isExitSafe()==true)
  {
  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    form.action = "customerOrderSearch.do" + 
                   "?action=customer_account_search" +
                   "&order_number="+order_number;
    form.submit();
  }
}

function replaceText( elementId, newString )
  {
   var node = document.getElementById( elementId );   
   var newTextNode = document.createTextNode( newString );
   if(node.childNodes[0]!=null)
   {
    node.removeChild( node.childNodes[0] );
   }
   node.appendChild( newTextNode );
  }
  
//Disable buttons
function buttonDis(page)
{
	setNavigationHandlers();
 	if (orderComments.total_pages.value <= 1)
 	{
    cos_show_previous = "false";
  	cos_show_first = "false";
    cos_show_next = "false";
    cos_show_last = "false";
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
    document.getElementById("nextItem").disabled= "true";
    document.getElementById("lastItem").disabled= "true";
    return;
  }
 	if (page < 2)
 	{
    cos_show_previous = "false";
  	cos_show_first = "false";
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
    return;
  }
  if (page == orderComments.total_pages.value)
  {
    cos_show_next = "false";
    cos_show_last = "false";
    document.getElementById("nextItem").disabled= "true";
    document.getElementById("lastItem").disabled= "true";
    return;
  }
}
	
function addAction(value)
{
  document.getElementById('saveAction').disabled = true;
	var empty = isEmpty();
	if (empty == false)
	{
		var form = document.forms[0];
		if (form.comment_text.value.length > 4000)
		{
			alert("Comment must be less than 4000 characters.");
      document.getElementById('saveAction').disabled = false;
		}
		else
		{
			document.orderComments.action_type.value = value;
			document.orderComments.action = "addComments.do";
	 		document.orderComments.submit();
		}
	}
  else
  {
    document.getElementById('saveAction').disabled = false;
  }
}

function isEmpty(){
	var str = orderComments.comment_text.value;
	if (str.length == 0){
		alert("Please enter comment in order to save.");
		//loadCommentsAction();
		return true;
	}else{
		return false;
	}
}

function isExitSafe()
{
  var form = document.forms[0];
  if(form.saveAction.disabled==false)
  {
    doaction = doExitPageAction("Your changes have not been saved. Do you wish to continue?");
  }
  else
    doaction = true;
  return doaction;
}

function loadCommentsAction(commentType, newPagePosition)
{
  var form = document.forms[0];
  if(isExitSafe()==true)
  {
  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    form.page_position.value = newPagePosition;
    form.comment_type.value = commentType;
		form.action = "loadComments.do";
	 	form.submit();
  }
}

// perform email send button action
function emailAction()
{
  var form = document.forms[0];
  if(isExitSafe()==true)
  {
	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;  	
    form.action = "loadSendMessage.do?message_type=Email";
    form.action_type.value = "load_titles"; 
    form.submit();
  }
}

// perform communication button action
function commAction()
{
  if(isExitSafe()==true)
  {
  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    document.forms[0].action = "displayCommunication.do";
    document.forms[0].action_type.value = "display_communication";
    document.forms[0].submit();
  }
}


/*
Handler for the communicationScreen.xsl "(Q)Delete" button in the Mercury
message table. This button creates a popup window listing the messages in the queue, as
created by the qDelete.xsl.
*/
function openQDelete()
{
  
  var args = new Array();

  args.action = "displayQDelete.do";
  
  /*
  The action needs a list of all the messages on the communication screen. The list will
  consist of all the messages' mercury ids. All the message hidden input
  fields are named "n" where n is that message's line number. Their values are the
  concatenation of their mercury ids and their message types(mercury, venus, email or
  letter), separated by a "|" character. We use an appropriate regular expression to select
  the message hidden fields, split off just the message id portions of those fields'	values,
  and use them to make a list of ids we pass to the displayQDelete action.
  */
  //var formFields = document.getElementById("communicationScreenForm").elements;
  var messageNameRegex = /^\d+E*$/;
  
  var messageId;

  /*for(var i = 0; i < formFields.length; i++)
  {
    //alert("name="+formFields[i].name+" value="+formFields[i].value);
    if(messageNameRegex.test(formFields[i].name))
    {
      //alert("name="+formFields[i].name);
      messageId = formFields[i].value.split("|");
      
      args[args.length] = new Array(formFields[i].name, messageId[0]);
    }
  }*/

  args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
  args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
  args[args.length] = new Array("context", document.getElementById("context").value);

  var modal_dim = "dialogWidth:600px; dialogHeight:350px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes;";
  var shouldReload = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);

  if(shouldReload == "Y")
  {
    reloadComm();
    
  }
}

function mainMenuAction()
{
  if(isExitSafe()==true)
  {
  	document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMENTS&lock_id="+document.forms[0].order_detail_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
    doMainMenuAction();
  }
}

/*************************************************************************************
*	Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
	doOrderSearchAction('External Order');
}

function clickCmnts()
{
	if(document.forms[0].origin_tab.value != 'Comments')
	{
		document.forms[0].origin_tab.value = 'Comments';
		
		var jsPagePosition = parseInt(orderComments.page_position.value);
		var jsCmntsTotalPages = parseInt(document.forms[0].comments_total_pages.value);
		
		if(jsPagePosition > jsCmntsTotalPages)
			loadCommentsAction('Order',1);
		else
			loadCommentsAction('Order',orderComments.page_position.value);
	}
	else
	{
		return false;
	}		
}

function clickDtlCmnts()
{
	if(document.forms[0].origin_tab.value != 'DetailedComments')
	{
		document.forms[0].origin_tab.value = 'DetailedComments';
		loadCommentsAction('Order',orderComments.page_position.value);
	}
	else
	{
		return false;
	}	
}

function tabMouseOut(tab){
  if(tab.className != "selected")
  	tab.className = "button";
}

function tabMouseOver(tab){
  if(tab.className != "selected" )
      tab.className = "hover";
}

function callRefund(){
	var order_detail_id = document.getElementById("order_detail_id").value;
	var origin_id = document.getElementById("origin_id").value;
	var product_type = document.getElementById("product_type").value;
	var ship_method = document.getElementById("ship_method").value;
	var vendor_flag = document.getElementById("vendor_flag").value;
	var order_guid = document.getElementById("order_guid").value;
		
	doOrderRefundAction(order_detail_id, origin_id, product_type, ship_method, vendor_flag, order_guid);
}

]]>
	</script>
	<style>
			.messageContainer .lineNumber{
				width: 5%;
				vertical-align: top;
			}

			.messageContainer .userId{
				width: 10%;
				text-align: left;
				vertical-align: top;
			}

			.messageContainer .date{
				width: 20%;
				text-align: left;
				vertical-align: top;
			}

			.messageContainer .origin{
				width: 10%;
				text-align: left;
				vertical-align: top;
			}

			.messageContainer .comment{
				width: 45%;
				text-align: left;
				vertical-align: top;
			}
		</style>
</head>
<body onload="init(); buttonDis(orderComments.page_position.value);" onkeydown="javascript:checkKey();">
	<xsl:call-template name="header">
		<xsl:with-param name="headerName"  select="'Order Comments'" />
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
		<xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="showSearchBox" select="false()"/>
		<xsl:with-param name="dnisNumber" select="$call_dnis"/>
		<xsl:with-param name="cservNumber" select="$call_cs_number"/>
		<xsl:with-param name="indicator" select="$call_type_flag"/>
		<xsl:with-param name="brandName" select="$call_brand_name"/>
		<xsl:with-param name="showCSRIDs" select="true()"/>
		<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
		<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>

	<form name="orderComments" method="post" action="">
    	<xsl:call-template name="securityanddata"/>
    	<xsl:call-template name="decisionResultData"/>
   			<iframe id="unlockFrame" name="unlockFrame" height="0" width="0" border="0"></iframe>
			<input type="hidden" name="action_type" id="action_type" value="{key('pagedata', 'action_type')/value}"/>
			<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>
			<input type="hidden" name="origin_id" id="origin_id" value="{key('pagedata', 'origin_id')/value}"/>
			<input type="hidden" name="product_type" id="product_type" value="{key('pagedata', 'product_type')/value}"/>
			<input type="hidden" name="ship_method" id="ship_method" value="{key('pagedata', 'ship_method')/value}"/>
			<input type="hidden" name="vendor_flag" id="vendor_flag" value="{key('pagedata', 'vendor_flag')/value}"/>
			<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pagedata', 'master_order_number')/value}"/>
			<input type="hidden" name="order_comment_origin" id="order_comment_origin" value="{key('pagedata', 'order_comment_origin')/value}"/>
			<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pagedata', 'external_order_number')/value}"/>
			<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata', 'order_detail_id')/value}"/>
			<input type="hidden" name="comment_origin" id="comment_origin" value="{key('pagedata', 'comment_origin')/value}"/>
			<input type="hidden" name="comment_reason" id="comment_reason" value="{key('pagedata', 'comment_reason')/value}"/>
			<input type="hidden" name="comment_type" id="comment_type" value="Order"/>
			<input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
			<input type="hidden" name="total_pages" id="total_pages" value="{key('pagedata', 'total_pages')/value}"/>
			<input type="hidden" name="start_position" id="start_position" value="{key('pagedata', 'start_position')/value}"/>
			<input type="hidden" name="order_guid" id="order_guid" value="{key('pagedata', 'order_guid')/value}"/>
            <input type="hidden" name="vip_customer" id="vip_customer" value="{key('pagedata', 'vip_customer')/value}"/>
            <input type="hidden" name="vipCustUpdatePermission" id="vipCustUpdatePermission" value="{key('pagedata','vipCustUpdatePermission')/value}"/>
            <input type="hidden" name="vipCustomerMessage" id="vipCustomerMessage" value="{key('pagedata','vipCustomerMessage')/value}"/>
            <input type="hidden" name="vipCustUpdateDeniedMessage" id="vipCustUpdateDeniedMessage" value="{key('pagedata','vipCustUpdateDeniedMessage')/value}"/>
            <input type="hidden" name="bypass_com"  id="bypass_com" value="{$bypassCOM}" />
			<input type="hidden" name="comment_id" id="comment_id" value=""/>
			<input type="hidden" name="origin_tab" value="{key('pagedata', 'origin_tab')/value}"/>
			<input type="hidden" name="comments_total_pages" value="{key('pagedata', 'comments_total_pages')/value}"/>
			
			<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
				<tr>
					<td id="lockError" class="ErrorMessage"></td>
				</tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
				<td>
					<div id="tabs">
						<button type="button" id="Comments"  onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="clickCmnts();"><span style="font-size:8pt">COMMENTS</span></button>
						<button type="button" id="Dtlcomments" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="clickDtlCmnts();"><span style="font-size:8pt">DETAILED COMMENTS</span></button>
					</div>
				</td>
			</table> 
			
			<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
				<tr>
					<td>
						<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
							<tr>
								<td width="25%" class="label">Order Number:&nbsp;
									<a href="javascript:doOrderSearchAction('External Order');">
										<xsl:value-of select="key('pagedata', 'external_order_number')/value"/>
									</a>
										<span id="textMessage" style="WIDTH: 140px; FONT-WEIGHT: normal; COLOR: red; MARGIN-LEFT: 40px; DISPLAY: inline-block">
												<script type="text/javascript">
										            document.write(document.getElementById('vipCustomerMessage').value);
							 				    </script>
										</span>
										<script type="text/javascript">
											if(localStorage.getItem('isCustomerVip') != 'Y' ) {
												document.getElementById("textMessage").style.display = "none";
											}
										</script>
								</td>


                                                                <td width="25%" class="label">Tagged to:
                                                                  <span class="label" style="color:red">
                                                                    <xsl:for-each select="//order_tag/csr_id">
                                                                      <script>countag();</script>
                                                                      <xsl:value-of select="csr_id"/>
                                                                    </xsl:for-each>
                                                                  </span>
                                                                </td>
                                                                <td width="50%" align="right" class="label">Order Date:&nbsp;
									<xsl:value-of select="key('pagedata', 'order_date')/value"/>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="left">
									<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
										<tr>
											<td>
												
													<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
													<tr><td>
														<div class="messageContainer" style="padding-right: 16px; width: 98%;">
															<table width="100%" border="0" cellpadding="1" cellspacing="1">
																<tr>
																	<th class="lineNumber"></th>
																	<th class="userId">User ID</th>
																	<th class="date">Date/Time</th>
																	<th class="origin">Origin</th>
																	<th colspan="2" class="comment">Comment</th>
																</tr>
															</table>
														</div>
														<table width="100%" cellpadding="0" border="0" cellspacing="0">
															<tr>
																<td colspan="6"><hr/></td>
															</tr>
														</table>
														<div class="messageContainer" style="width: 99%; overflow: auto; padding-right: 16px; height: 150px;">
															<table width="100%" cellpadding="1" border="0" cellspacing="1" style="margin-right: -16px;word-break:break-all;table-layout:fixed;">
																<xsl:for-each select="root/comments/comment">
																	<tr>
																		<td class="lineNumber">
																			<xsl:choose>
																				<xsl:when test="$userCanUpdate = 'Y'">
																					<a href="javascript:updateComment('Order Comments', '{comment_id}');" class="textlink">
																						<script type="text/javascript">document.write(orderComments.start_position.value); orderComments.start_position.value++</script>.
																					</a>
																				</xsl:when>
																				<xsl:otherwise>
																					<script type="text/javascript">document.write(orderComments.start_position.value); orderComments.start_position.value++</script>.
																				</xsl:otherwise>
																			</xsl:choose>
																		</td>
																		<xsl:choose>
																			<xsl:when test="manager_indicator = 'Y'">
																				<td class="userId">
																					<b><xsl:value-of select="created_by"/></b>
																				</td>
																			</xsl:when>
																			<xsl:otherwise>
																				<td class="userId">
																					<xsl:value-of select="created_by"/>
																				</td>
																			</xsl:otherwise>
																		</xsl:choose>
																		<xsl:choose>
																			<xsl:when test="manager_indicator = 'Y'">
																				<td class="date">
																					<b><xsl:value-of select="created_on"/></b>
																				</td>
																			</xsl:when>
																			<xsl:otherwise>
																				<td class="date">
																					<xsl:value-of select="created_on"/>
																				</td>
																			</xsl:otherwise>
																		</xsl:choose>
																		<xsl:choose>
																			<xsl:when test="manager_indicator = 'Y'">
																				<td class="origin">
																					<b><xsl:value-of select="comment_origin"/></b>
																				</td>
																			</xsl:when>
																			<xsl:otherwise>
																				<td class="origin">
																					<xsl:value-of select="comment_origin"/>
																				</td>
																			</xsl:otherwise>
																		</xsl:choose>
																		<xsl:choose>
																			<xsl:when test="manager_indicator = 'Y'">
																				<td id="td_{comment_id}" class="comment">
																					<b><xsl:value-of disable-output-escaping="yes" select="comment_text"/></b>
																				</td>
																			</xsl:when>
																			<xsl:otherwise>
																				<td id="td_{comment_id}" class="comment">
																					<xsl:value-of disable-output-escaping="yes" select="comment_text"/>
																				</td>
																			</xsl:otherwise>
																		</xsl:choose>
																	</tr>
																</xsl:for-each>
															</table>
														</div>
														</td>
														</tr>
													</table>
												
											</td>
										</tr>
										<tr>
											<td colspan="6">
												<hr/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="left"></td>
							</tr>
							<tr>
								<td class="label">Enter Comments:</td>
										</tr>
							<tr>
								<td width="80%">
									<textarea cols="90" rows="2" id="comment_text" name="comment_text"></textarea>
								</td>
								<td>
									<table align="right">
										<xsl:for-each select="key('pagedata', 'page_position')">
											<tr>
												<td align="right">
													<table border="0">
														<tr>
															<td colspan="4" width="25%" class="label" align="right">Page&nbsp;
																<xsl:for-each select="key('pagedata', 'page_position')">
																<xsl:value-of select="value" />
																</xsl:for-each>
																&nbsp;of&nbsp;
																<xsl:for-each select="key('pagedata', 'total_pages')">
																<xsl:if test="value = '0'">
								                                     1
                												</xsl:if>
                            									<xsl:if test="value != '0'">
								                                	<xsl:value-of select="value" />
		                                    					</xsl:if>
																</xsl:for-each>
															</td>
														</tr>
														<tr>
             												<td colspan="4" align="right">
             													<xsl:choose>
									                              	<xsl:when test="key('pagedata','total_pages')/value !='0'">
									                              		<button type="button"  name="firstItem" class="arrowButton" id="firstItem" onclick="loadCommentsAction('Order',1);">
									                              			7
									                              		</button>
									                              	</xsl:when>
									                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
									                              		<button type="button"  name="firstItem" class="arrowButton" id="firstItem" onclick="">
									                              			7
									                              		</button>
									                              	</xsl:when>
                         										</xsl:choose>
						                             		
					                            				<xsl:choose>
									                              	<xsl:when test="key('pagedata','total_pages')/value !='0'">
									                              		<button type="button"  name="backItem" class="arrowButton" id="backItem" onclick="loadCommentsAction('Order',orderComments.page_position.value-1);">
									                              			3
									                              		</button>
									                              	</xsl:when>
									                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
									                              		<button type="button"  name="backItem" class="arrowButton" id="backItem" onclick="">
									                              			3
									                              		</button>
									                              	</xsl:when>
				                              					</xsl:choose>
						                             		
					                            				<xsl:choose>
									                              	<xsl:when test="key('pagedata','total_pages')/value !='0'">
									                              		<button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="x=orderComments.page_position.value;loadCommentsAction('Order',++x);">
									                              			4
									                              		</button>
									                              	</xsl:when>
									                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
									                              		<button type="button"   name="nextItem" class="arrowButton" id="nextItem" onclick="">
									                              			4
									                              		</button>
								                              		</xsl:when>
							                              		</xsl:choose>
							                             	
								                            	<xsl:choose>
									                              	<xsl:when test="key('pagedata','total_pages')/value !='0'">
									                              		<button type="button"   name="lastItem" class="arrowButton" id="lastItem" onclick="loadCommentsAction('Order',orderComments.total_pages.value);">
									                              			8
								                              			</button>
									                              	</xsl:when>
									                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
									                              		<button type="button"   name="lastItem" class="arrowButton" id="lastItem" onclick="">
									                              			8
									                              		</button>
									                              	</xsl:when>
						                              			</xsl:choose>
								                             </td>
														</tr>
													</table>
												</td>
											</tr>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
				<tr>
					<td width="80%" valign="top">
                                            <xsl:choose>
                                                <xsl:when test="$bypassCOM = 'Y'">
                                                    <button type="button"  class="BigBlueButton" style="WIDTH: 115px" id="searchAction" name="searchAction" onClick="doOrderSearchAction('External Order');">Recipient/<br/>(O)rder</button>
                                                    <button type="button"  accesskey="O" class="BigBlueButton ButtonTextBox2" id="commButton" name="commButton" ONCLICK="commAction();">C(o)mmunication </button>
                                                    <button type="button"  id="refund" class="BigBlueButton ButtonTextBox1" accesskey="R" onclick="javascript:callRefund();" >(R)efund </button><br/>
                                                    <button type="button"  accesskey="A" class="BigBlueButton" style="WIDTH: 115px" id="addComment" name="addComment" onClick="UPDATE_COMMENT.checkLock();" title="Click to enter comments">(A)dd<br/>Comments </button>
                                                    <button type="button"   accesskey="S" class="BigBlueButton ButtonTextBox2" id="saveAction" name="saveAction" onclick="addAction('save_comment')">(S)ave</button>
                                                    <button type="button"   accesskey="V" class="BigBlueButton ButtonTextBox1" id="shoppingCart" name="shoppingCart" onclick="addAction('cart_save_comment')">Sa(v)e in<br />Shopping Cart</button>
                                                    <button type="button" accesskey="R" class="BigBlueButton ButtonTextBox1" id="refreshAction" name="refreshAction" onClick="loadCommentsAction('Order',1);">(R)efresh </button>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <button type="button"  class="BigBlueButton" style="WIDTH: 115px" id="searchAction" name="searchAction" onClick="doOrderSearchAction('External Order');">Recipient/<br/>(O)rder </button>
                                                    <button type="button"  accesskey="O" class="BigBlueButton ButtonTextBox2" id="commButton" name="commButton" ONCLICK="commAction();">C(o)mmunication </button>
                                                    <button type="button"  id ="refund" class="BigBlueButton ButtonTextBox1" accesskey="R" onclick="javascript:callRefund();" >(R)efund </button>
                                                    <button type="button"  accesskey="E" class="BigBlueButton ButtonTextBox1" id="viewEmail" name="viewEmail" ONCLICK="emailAction();">(E)mail</button><br/>
                                                    <button type="button"  accesskey="A" class="BigBlueButton" style="WIDTH: 115px" id="addComment" name="addComment" onClick="UPDATE_COMMENT.checkLock();" title="Click to enter comments">(A)dd<br/>Comments</button>
                                                    <button type="button"  accesskey="S" class="BigBlueButton ButtonTextBox2" id="saveAction" name="saveAction" onclick="addAction('save_comment')">(S)ave</button>
                                                    <button type="button"  accesskey="V" class="BigBlueButton ButtonTextBox1" id="shoppingCart" name="shoppingCart" onclick="addAction('cart_save_comment')">Sa(v)e in<br />Shopping Cart</button>
                                                    <button type="button" accesskey="R" class="BigBlueButton ButtonTextBox1" id="refreshAction" name="refreshAction" onClick="loadCommentsAction('Order',1);">(R)efresh</button>
                                                    <button type="button"   style="width:100" accesskey="D" class="bigBlueButton ButtonTextBox1" id="queueButton" name="queueButton" ONCLICK="openQDelete();">Queue (D)elete</button>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
					<td width="20%" align="right">
						<button type="button"   class="blueButton" accesskey="B" name="backButton" style="width:80px" onclick="javascript:doBackAction();">(B)ack to Order</button>
						<br/>
						<button type="button"   class="bigBlueButton" accesskey="M" name="mainMenu" style="width:80px" onclick="javascript:mainMenuAction();">(M)ain<br/>Menu</button>
					</td>


				</tr>
				<!--
				<tr>
					<td>
						<xsl:element name="input">
							<xsl:attribute name="type">checkbox</xsl:attribute>
							<xsl:attribute name="onClick">loadCommentsAction('Order', 1);</xsl:attribute>
							<xsl:attribute name="name">hide_system</xsl:attribute>
							<xsl:if test="key('pagedata', 'hide_system')/value='on'">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:if>
						</xsl:element>
   						Hide System Generated Comments
					</td>
				</tr>
				-->
			</table>
			<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
		</form>
		<xsl:call-template name="footer"></xsl:call-template>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>