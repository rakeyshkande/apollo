<!DOCTYPE ACDemo[
 <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--external templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
<xsl:output method="html" indent="yes"/>

<!-- main keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<!--variables-->
<xsl:variable name="SCRUB" select="'S'"/>
<xsl:variable name="PENDING" select="'P'"/>
<xsl:variable name="REMOVED" select="'R'"/>
<xsl:variable name="cos_start_position" select="key('pageData','cos_start_position')/value"/>
<xsl:variable name="cos_current_page" select="key('pageData','cos_current_page')/value"/>
<xsl:variable name="YES" select="'Y'"/>
<xsl:variable name="NO" select="'N'"/>

<xsl:template match="/">
	<html>
		<head>
			<title>FTD - Last Records Accessed</title>
			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<style type="text/css">
		    /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
                        <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
			<script type="text/javascript" src="js/util.js"/>
			<script type="text/javascript" src="js/FormChek.js"/>
			<script type="text/javascript" src="js/commonUtil.js"/>
			<script type="text/javascript" language="javascript">
			var scrub_url = "<xsl:value-of select="key('pageData','scrub_url')/value"/>";
			var sc_confirmation_number = "<xsl:value-of select="key('searchCriteria', 'sc_order_number')/value"/>";
			var pos = "<xsl:value-of select="$cos_start_position"/>";
			var cPage = "<xsl:value-of select="$cos_current_page"/>";
                        var transfer_number = "<xsl:value-of select="key('pageData','transferNumber')/value"/>";
                        var mysteryBox = new Array('numberEntry');
			<![CDATA[
				/*
				Initalization
				*/
				function init(){
                                  setNavigationHandlers();
                                  addDefaultListenersArray(mysteryBox);
                                  document.getElementById('numberEntry').focus();
                                  setLastRecordAccessIndexes();
				}

/*******************************************************************************
*  setLastRecordAccessIndexes()
*  Sets tabs for Last Records Accessed Page
*  See tabIndexMaintenance.js
*******************************************************************************/
	function setLastRecordAccessIndexes()
        {
          untabAllElements();
          var tabOrder = new Array('numberEntry', 'backButton', 'mainMenu', 'printer');
          setTabIndexes(tabOrder, findBeginIndex());
        }

        /*
        Actions
        */

        /***********************************************************************************
        * canRepAccessOrder()
        ************************************************************************************/
        function canRepAccessOrder(id,num,preferredCustomer,ftdCustomer,preferredRecipient,ftdRecipient, preferredWarningMessage){
            var repHasPermission = document.getElementById('rep_has_access'+num).value;
            if ( preferredCustomer == 'Y' && ftdCustomer == 'N' && repHasPermission == 'N')
            {
                 var modalArguments = new Object();
                 modalArguments.modalText = preferredWarningMessage;
                 window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
            }
            else if ( preferredRecipient == 'Y' && ftdRecipient == 'N' && repHasPermission == 'N')
            {
                 var modalArguments = new Object();
                 modalArguments.modalText = preferredWarningMessage;
                 window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
            }
            else
            {
                doCustomerSearchAction(id,num);
            }
        }

            /*
            Actions
            */
            function doBuildScrubURL(scMode){
                     var url = scrub_url + "?sc_mode=" + scMode +
                                    "&sc_confirmation_number=" + sc_confirmation_number;
                     if(scMode.toUpperCase() != 'S')
                            url += "&action=result_list";

                    var modal_dim = 'dialogWidth=800px,dialogHeight=400px,dialogLeft=0px,dialogTop=90px';
                    modal_dim +='location=0,status=0,menubar=0,scrollbars=1,resizable=0';
            showModalDialogIFrame(url, "", modal_dim);
            }
            function doCustomerSearchAction(id,num){
                    var url = "customerOrderSearch.do" +
                                            "?action=customer_search" + "&customer_id=" + id;
                    var lastOrderDate = document.getElementById('lastOrderDate'+num).value;
                    if(lastOrderDate != '') url += "&last_order_date=" + lastOrderDate;
                     recipOrderNum = document.getElementById('recip_order_number'+num).value;
                    if(recipOrderNum != '') url += "&order_number=" + recipOrderNum;
              performAction(url);
            }
            function performAction(url){
                scroll(0,0);
                showWaitMessage("content", "wait", "Searching");
                document.forms[0].action = url;
                document.forms[0].submit();
	    }

function doEntryAction()
{
  if(window.event)
    if(window.event.keyCode)
      if(window.event.keyCode != 13)
        return false;

  var eve = event.srcElement.value;

  if(isInteger(eve))
  {
    var searchNumber = doRowCalculation(eve);
    if(document.getElementById('recip_order_number' + searchNumber)==null)
    {
      alert('Invalid line number entered.' + '\n' +
      'Please only enter a line number that is currently being displayed on the page.');
    }
    else
    {
      canRepAccessOrder(document.getElementById('customer_id'+searchNumber).value, searchNumber, document.getElementById('preferred_customer'+searchNumber).value, document.getElementById('ftd_customer'+searchNumber).value, document.getElementById('preferred_recipient'+searchNumber).value, document.getElementById('ftd_recipient'+searchNumber).value, document.getElementById('preferred_warning_message'+searchNumber).value);
    }
  }
}


		/*************************************************************************************
		*	Performs the Back button functionality
		**************************************************************************************/
		function doBackAction()
		{
		  	var url = "customerOrderSearch.do?action=load_page";
			performAction(url);
		}


			]]></script>
		</head>
    <body onload="javascript:init();" onkeypress="javascript: evaluateKeyPress();" >
			<xsl:call-template name="addHeader"/>
			<form name="lastAccessed" method="post">

				<!--page Data -->
				<input type="hidden" name="cos_current_page" id="cos_current_page" value="{key('pageData','cos_current_page')/value}"/>
				<input type="hidden" name="cos_total_pages" id="cos_total_pages" value="{key('pageData','cos_total_pages')/value}"/>
				<input type="hidden" name="total_records_found" id="total_records_found" value="{key('pageData','cos_total_records_found')/value}"/>
				<input type="hidden" name="begin_index" id="begin_index" value="{key('pageData','begin_index')/value}"/>
				<input type="hidden" name="end_index" id="end_index" value="{key('pageData','end_index')/value}"/>
				<input type="hidden" name="max_search_results" id="max_search_results" value="{key('pageData','max_search_results')/value}"/>
				<input type="hidden" name="max_records_allowed" id="max_records_allowed" value="{key('pageData','max_records_allowed')/value}"/>
                                <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
        <xsl:call-template name="securityanddata"/>
        <xsl:call-template name="decisionResultData"/>
	      <div id="content" style="display:block">      <!--  Main Table thick blue  -->
			   <table class="mainTable" align="center" width="98%" cellpadding="0" cellspacing="1">
				   <tr>
					   <td><!-- Inner table used to show thin blue border -->
						   <table class="innerTable" cellpadding="0" cellspacing="0" width="100%" border="0">
							   <tr>
								   <td >
								     <div class="tableContainer" id="messageContainer" >
  		  						  <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
      								 <thead class="fixedHeader">

<!--										 <table class="scrollTable" width="100%" border="0" cellpadding="1"  align="center" cellspacing="2">
										   <thead class="fixedHeader">-->
						                <tr style="text-align:left">
								              <td  class="ColHeader" width="2%"  ></td>
								              <td  class="ColHeader" width="26%" >&nbsp;Last Name</td>
								              <td  class="ColHeader" width="24%" >&nbsp;First Name</td>
								             <!-- <td  class="ColHeader" width="22%" >&nbsp;Order #</td>-->
								              <td  class="ColHeader" width="17%" >&nbsp;Last Order Date</td>
								             <!-- <td  class="ColHeader" >Status</td>-->
								            </tr>
								           </thead>
								           <tbody class="scrollContent">
								 				<xsl:for-each select="root/CSR_VIEWED/CSR">
													<xsl:variable name="INDICATOR" select="scrub_status"/>
													  <tr style="vertical-align:text-top;text-align:left;">
														  <td width="23px"><script type="text/javascript"><![CDATA[
																document.write(pos++ + ".");
														     ]]></script>
														  </td>
														  <td  width="141pt" id="td_{@num}" style="text-align:left;">
															   <xsl:choose>
																  <xsl:when test="$INDICATOR = $SCRUB or $INDICATOR = $REMOVED or $INDICATOR = $PENDING">
																	 <xsl:value-of select="last_name"/>
																  </xsl:when>
                                                                                                                                  <xsl:when test="preferred_customer = 'Y' and ftd_customer = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
																	 <a href="javascript:canRepAccessOrder('{customer_id}','{@num}','{preferred_customer}','{ftd_customer}','{preferred_recipient}','{ftd_recipient}', '{preferred_warning_message}');" class="textlink" style="color:blue;font-weight:bold"><xsl:value-of select="preferred_masked_name"/></a>
																  </xsl:when>
                                                                                                                                  <xsl:when test="preferred_recipient = 'Y' and ftd_recipient = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
																	 <a href="javascript:canRepAccessOrder('{customer_id}','{@num}','{preferred_customer}','{ftd_customer}','{preferred_recipient}','{ftd_recipient}', '{preferred_warning_message}');" class="textlink" style="color:blue;font-weight:bold"><xsl:value-of select="preferred_masked_name"/></a>
																  </xsl:when>
																  <xsl:otherwise>
																	<a href="javascript:doCustomerSearchAction('{customer_id}','{@num}');" class="textlink"><xsl:value-of select="last_name"/></a>
																  </xsl:otherwise>
															   </xsl:choose>
															   <input type="hidden" id="customer_id{@num}" value="{customer_id}"/>
															   <input type="hidden" id="recip_order_number{@num}" value="{order_number}"/>
                                                                                                                           <input type="hidden" id="preferred_customer{@num}" value="{preferred_customer}"/>
															   <input type="hidden" id="ftd_customer{@num}" value="{ftd_customer}"/>
                                                                                                                           <input type="hidden" id="preferred_recipient{@num}" value="{preferred_recipient}"/>
                                                                                                                           <input type="hidden" id="ftd_recipient{@num}" value="{ftd_recipient}"/>
                                                                                                                           <input type="hidden" id="preferred_warning_message{@num}" value="{preferred_warning_message}"/>
                                                                                                                           <input type="hidden" id="rep_has_access{@num}" value="{key('pageData', preferred_resource)/value}"/>
													  	  </td>
														  <xsl:choose>
                                                                                                                    <xsl:when test="preferred_customer = 'Y' and ftd_customer = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
                                                                                                                        <td width="160pt" style="text-align:left;color:blue;font-weight:bold"><xsl:value-of select="preferred_masked_name"/></td>
                                                                                                                    </xsl:when>
                                                                                                                    <xsl:when test="preferred_recipient = 'Y' and ftd_recipient = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
                                                                                                                        <td width="160pt" style="text-align:left;color:blue;font-weight:bold"><xsl:value-of select="preferred_masked_name"/></td>
                                                                                                                    </xsl:when>
                                                                                                                    <xsl:otherwise>
                                                                                                                      <td width="160pt" style="text-align:left;"><xsl:value-of select="first_name"/></td>
                                                                                                                      </xsl:otherwise>
                                                                                                                  </xsl:choose>
														<!--  <td  width="160pt" style="text-align:left;"><xsl:value-of select="order_number"/></td>-->
														  <td width="105pt" style="text-align:left;"><xsl:value-of select="last_order_date"/></td>
														<!--  <td width="60pt" style="text-align:left;">&nbsp;
														  	<xsl:choose>
														  		<xsl:when test="$INDICATOR = $SCRUB">
														  			<a href="javascript:doBuildScrubURL('{$SCRUB}');"><xsl:value-of select="'Scrub'"/></a>
														  		</xsl:when>
														  		<xsl:when test="$INDICATOR = $PENDING">
														  			<a href="javascript:doBuildScrubURL('{$PENDING}');"><xsl:value-of select="'Pending'"/></a>
														  		</xsl:when>
														  		<xsl:when test="$INDICATOR = $REMOVED">
														  			<a href="javascript:doBuildScrubURL('{$REMOVED}');"><xsl:value-of select="'Removed'"/></a>
														  		</xsl:when>
														  		<xsl:otherwise>
														  			<xsl:value-of select="''"/>
														  		</xsl:otherwise>
														  	</xsl:choose>
													  	 </td>-->
														  <input type="hidden" id="lastOrderDate{@num}" value="{last_order_date}"/>
													  </tr>
													 </xsl:for-each>
												  </tbody>
										      </table>
										</div>
								   </td>
							   </tr>
						   </table>
					   </td>
				   </tr>
			   </table>
			   <table border="0" width="98%" cellpadding="0" cellspacing="1" align="center">
			     <tr>
					<td align="right">
                                                <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
                                                    <button type="button" class="BigBlueButton" accesskey="V" id="lpsearch" onclick="javascript:doLossPreventionSearchAction();">
                                                        Back to Loss<br />Pre(v)ention Search
                                                    </button>
                                                </xsl:if>
						<button type="button" id="backButton" accesskey="b" class="BigBlueButton" onclick="doBackAction();">(B)ack to Search</button><br/>
					</td>
			     </tr>
			     <tr>
					<td align="right">
							<button type="button" id="mainMenu" accesskey="m" style="width:55px" class="BigBlueButton" onclick="doMainMenuAction();">(M)ain<br/>Menu</button>
					</td>
			     </tr>
			   </table>
		</div>

       <!-- Used to dislay Processing... message to user -->
		<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>
       	<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>
 </xsl:template>
<xsl:template name="addHeader">
	<xsl:call-template name="header">
	  <xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
               	<xsl:with-param name="showSearchBox" select="true()"/>
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="searchLabel" select="'Enter Line#'"/>
		<xsl:with-param name="headerName" select="'Last Records Accessed'"/>
                <xsl:with-param name="dnisNumber" select="$call_dnis"/>
                <xsl:with-param name="cservNumber" select="$call_cs_number"/>
                <xsl:with-param name="indicator" select="$call_type_flag"/>
                <xsl:with-param name="brandName" select="$call_brand_name"/>
		<xsl:with-param name="entryAction" select="true()"/>
		<xsl:with-param name="backButtonLabel" select="'(B)ack to Search'"/>
		<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
 </xsl:call-template>
</xsl:template>
</xsl:stylesheet>