<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY reg "&#xAE;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>


<xsl:template match="/">
<html>
  <head>
     <title>Queue Message Search</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>

    <script type="text/javascript" src="js/clock.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/util.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script type="text/javascript" src="js/queueSearch.js"/> 
    <script language="javascript" type="text/javascript">

    </script>
  </head>
  <body id="body" onload="javascript:init();">

  <xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Queue Message Search'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
        <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
      </xsl:call-template>
  <form name="theform" method="post" onkeypress="doSearch();" >
    <input type="hidden" name="action_type" id="action_type" value="{key('pageData','action_type')/value}"/>
    <input type="hidden" name="result" id="result" value="{key('pageData','result')/value}"/>
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- content to be hidden once the search begins -->
    <div id="content" style="display:block">
     <xsl:if test="key('pageData','message_display')/value != ''">
       <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
         <tr>
           <td class="largeErrorMsgTxt">
            <xsl:value-of select="key('pageData','message_display')/value"/>
           </td>
         </tr>
       </table>
       </xsl:if>
       <table class="mainTable" align="center" cellpadding="1" cellspacing="1" width="98%">
          <tr>
          <td><!--Content table-->
            <table border="0" class="innerTable" width="100%" align="center">
              <tr><td colspan="6">&nbsp;</td></tr>
              <tr>
                <td class="labelpad">
                   Message System<font color="red">&nbsp;*&nbsp;</font>:
                </td>
                <td align="left" valign="top"><select name="scMessageSystemId" id="scMessageSystemId" tabindex="1" onchange="changeCriteriaType()">
                                 <option value=""></option>
                                 <option value="Email">Email</option>
                                 <option value="Merc">Mercury</option>
                                 <option value="Argo">Venus - Argo</option>
                                 <option value="FTD WEST">FTD WEST</option>
                              </select>
                           </td>
                <td colspan="3" >
                </td>
              </tr>
              <tr>
                <td class="labelpad" width="24%" >
                  Queue Type<font color="red">&nbsp;*&nbsp;</font>:
                </td>
                <td align="left"><select name="scQueueTypeId" id="scQueueTypeId" tabindex="2"  onchange="changeCriteriaType()">
                      <option value=""></option>
                      <xsl:for-each select="/root/queueTypeList/queueType">
                        <option value="{@queue_type}">
                        <xsl:if test="@queue_type = key('pageData','scQueueTypeId')/@value">
                            <xsl:attribute name="selected">
                          true
                           </xsl:attribute>
                           </xsl:if>
                          <xsl:value-of select="@description"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                <td width="25%" align="right">
                </td>
                <td>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  Message Direction:
                </td>
                <td align="left" valign="top"><select name="scMessageDirectionId" id="scMessageDirectionId" tabindex="3" onchange="changeCriteriaType()">
                                 <option value=""></option>
                                 <option value="INBOUND">Inbound</option>
                                 <option value="OUTBOUND">Outbound</option>
                              </select>
                           </td>
                <td colspan="3">
                </td>
              </tr>
              <tr>
                    <td class="labelpad" >Include<font color="red">&nbsp;*&nbsp;</font>:</td>
                    
                    <td align="left" valign="top" halign="left">
                      <input name="scNonPartner" id="scNonPartner" type="checkbox" tabindex="4" checked="checked"/>General (non-partner)&nbsp;&nbsp;
                        <!--Product Properties checkboxes-->
                        <input name="prodCnt" id="prodCnt" type="hidden" value="{count(root/productProperties/productProperty)}"/>
                        <xsl:for-each select="/root/productProperties/productProperty">
                            <input name="sc{name}" id="prodProp_{position()}" type="checkbox" tabindex="5"/>
                            <xsl:value-of select="value"/>      
                             
                             
                        </xsl:for-each>
                      <!--   <input name="icnt" id="icnt" type="hidden" value="{$i}"/>-->
                        &nbsp;&nbsp;
                        <!--Preferred Partners checkboxes-->
                        <input name="partnerCnt" id="partnerCnt" type="hidden" value="{count(root/preferredPartners/preferredPartner)}"/>
                        <xsl:for-each select="/root/preferredPartners/preferredPartner">
                           <input name="sc{name}" id="prefPartner_{position()}" type="checkbox" tabindex="6"/>
                             <xsl:value-of select="value"/>    
                            
                             
                        </xsl:for-each>
                        <!-- <input name="jcnt" id="jcnt" type="hidden" value="{$j}"/>-->
                    </td>
                    <td colspan="3"></td>   
              </tr>
              <tr>
                <td class="labelpad" >Include Messages with No<br/>Customer Email Address:</td>
                <td><input type="checkbox" name="scCustomerEmail" id="scCustomerEmail" checked="checked" tabindex="7"/></td>
              </tr>
             
              <tr id="Tr_Askp_Amts" style="display:none">
                <td class="labelpad" >Search By ASKP Incremental Amounts Requested:</td>
                <td>$<input type="text" name="scAmountFrom" tabindex="8" onblur="validateAmountFields(this)"/>&nbsp;&nbsp;to&nbsp;&nbsp;$<input type="text" name="scAmountTo" id="scAmountTo" tabindex="9" onblur="validateAmountFields(this)"/></td>
              </tr>
              <input type="hidden" name="scSearchText1" id="scSearchText1"/>
              <input type="hidden" name="scSearchText2" id="scSearchText2"/>
              <input type="hidden" name="scSearchText3" id="scSearchText3"/>
             <tr id="Tr_Key_Word" style="display:none">
                <td class="labelpad" >Keyword Search: 
                <p style="font-family:arial;color:green;font-size:11px;font-style:italic;">Keyword search fields accept alphanumeric <br/>characters and the following special characters:  <br/>$ . - % , ’</p></td>
                <td>
                  <table>
                    
                    <tr><td><input type="text" name="scKeywordSearchText1" id="scKeywordSearchText1" tabindex="10" size="35" maxlength="30" /></td></tr>
                    <tr><td><select name="scKeywordSearchCondition1" id="scKeywordSearchCondition1" tabindex="11" onchange="changeKeywordCondition(this)" >
                               <option value="AND">AND</option>
                               <option value="OR">OR</option>
                            </select></td></tr>
                    <tr><td><input type="text" name="scKeywordSearchText2" id="scKeywordSearchText2" tabindex="12"  size="35" maxlength="30" /></td></tr>
                    <tr><td><select name="scKeywordSearchCondition2" id="scKeywordSearchCondition2" tabindex="13" onchange="changeKeywordCondition(this)">
                               <option value="AND">AND</option>
                               <option value="OR">OR</option>
                            </select></td></tr>
                    <tr><td><input type="text" name="scKeywordSearchText3" id="scKeywordSearchText3" tabindex="14"  size="35" maxlength="30" /></td></tr>
                  </table>
                
                </td>
              </tr>
              
            </table>
          </td><!--Content table-->
           </tr>
       </table>
      <table align="center" width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr><td colspan = "3">&nbsp;</td></tr>
        <tr>
          <td width="25%"></td>
          <td valign="top" align="center" width="49%">
            <button type="button" id="searchButton" class="bluebutton" accesskey="s" tabindex="20" onclick="javascript:doSearch();">(S)earch</button>
            <button type="button" id="clear" class="bluebutton" accesskey="c" tabindex="21" onclick="doClearFields();">(C)lear all Fields</button>
          </td>
          <td width="49%" align="right">
            <button type="button" id="mainMenu" class="bluebutton" accesskey="b" tabindex="22" onclick="javascript:doBackAction();">(B)ack</button>
          </td>
        </tr>
      </table>
    </div>

    <!-- Used to dislay Processing... message to user -->
    <div id="waitDiv" style="display:none">
       <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
          <tr>
            <td width="100%">
              <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                           <td id="waitTD"  width="50%" class="waitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
    </div>
    <!-- call footer template-->
    <xsl:call-template name="footer"/>
    </form>
  </body>
</html>
</xsl:template>
</xsl:stylesheet>