<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:decimal-format NaN="" name="notANumber"/>
    <xsl:template match="/root">
     <html>
		<title>FTD - Membership Information</title>
		<head>
			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<script type="text/javascript">
				<![CDATA[
					function doCloseAction(){
						top.close();
					}
				]]>
			</script>
		</head>
		<body>
     <xsl:call-template name="securityanddata"/>
     <xsl:call-template name="decisionResultData"/>
		<table class="mainTable" align="center" cellpadding="1" cellspacing="1" width="98%">
			<tr>
				<td class="banner">
					Membership Information
				</td>
			</tr>
			<tr>
				<td>
					<table align="center" width="100%" class="innerTable" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<table id="membershipInfoPopup"  style="word-break:break-all;"  cellpadding="0" cellspacing="3" width="100%">
									<tr>
										<td class="Label" width="25%">
											Membership ID:
										</td>
										<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_number"/></td>
									</tr>
									<tr>
										<td class="Label">Date Miles Posted:</td>
										<td>
                       <xsl:choose>
                       <xsl:when test="MEMBERSHIPS/MEMBERSHIP_INFO/miles_points_post_date != ''">
                          <xsl:call-template name="modDate">
                             <xsl:with-param name="dateValue" select="MEMBERSHIPS/MEMBERSHIP_INFO/miles_points_post_date"/>
                          </xsl:call-template>
                       </xsl:when>
                       <xsl:otherwise>
                          <xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/miles_points_post_date"/>
                       </xsl:otherwise>
                       </xsl:choose>
                                                            
                    </td>
									</tr>
									<tr>
										<td class="Label">Membership First Name:</td>
										<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_first_name"/></td>
									</tr>
									<tr>
										<td class="Label">Membership Last Name:</td>
										<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_last_name"/></td>
									</tr>
									<tr>
										<td class="Label"> Miles/Points Earned</td>
										<td>
										<xsl:if test="MEMBERSHIPS/MEMBERSHIP_INFO/miles_points != ''">
											<xsl:value-of select="format-number(MEMBERSHIPS/MEMBERSHIP_INFO/miles_points, '#,###,##0.00;(#,###,##0.00)', 'notANumber')"/>
										</xsl:if>
										</td>
									</tr>
									<tr>
										<td class="Label">Partner ID:</td>
										<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_type"/></td>
									</tr>
                  <xsl:if test="count(coBrands/coBrand) > 0">
                  <tr>
                    <!-- Temporary filter to display for Harry & David only -->
                    <xsl:for-each select="coBrands/coBrand">
                      <xsl:if test="source_code = '10682' or source_code = '10683'">
      										<td class="Label">
                          <xsl:choose>
                            <xsl:when test="boolean(info_display/text())">
                                <xsl:value-of select="info_display"/>:
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="info_name"/>:
                            </xsl:otherwise>
                          </xsl:choose>
                          </td><td>
                            <xsl:value-of select="info_data"/>
                          </td>
                      </xsl:if>
                    </xsl:for-each>
                    <!-- Code to be used when temporary filter removed 
										<td class="Label">Co-Brand Info:</td>
                    <td>
                      <xsl:for-each select="coBrands/coBrand">
                          <xsl:choose>
                            <xsl:when test="boolean(info_display/text())">
                              <span class="Label"><xsl:value-of select="info_display"/> -</span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span class="Label"><xsl:value-of select="info_name"/> -</span>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:value-of select="info_data"/>&nbsp;&nbsp;
                      </xsl:for-each>
                    </td>
                    -->
                  </tr>
                  </xsl:if>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
		</table>
		 <br/>
		<table border="0" width="98%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
				<button type="button" class="BlueButton" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button>
				</td>
			</tr>
		</table>
		
		</body>
     </html>
		  
		
		
	</xsl:template>
  
   <xsl:template name="modDate">
      <xsl:param name="dateValue"/>
      <xsl:param name="hackDate" select="substring($dateValue,1,10)"/>
   <xsl:value-of select="concat(substring($hackDate,6,2),'/',substring($hackDate,9,2),'/',substring($dateValue,1,4))"/>
   
</xsl:template>
  
  
</xsl:stylesheet>