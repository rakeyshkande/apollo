<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
  <xsl:output method="html"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pagedata" match="root/pageData/data" use="name" />
  <xsl:key name="remaining_totals" match="root/remainingTotals/amount" use="name"/>
  <xsl:key name="refund_amounts" match="root/refundAmounts/amount" use="name"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:variable name="c_type_flag" select="key('pagedata', 'modify_order_flag')/value"/>
  <xsl:variable name="c_disposition_id" select="key('pagedata', 'disposition_id')/value"/>
  <xsl:variable name="sRefundDisp" select="key('pagedata', 'refundDisp')/value"/>
  <xsl:variable name="gcCouponStatuss"/>
  <xsl:variable name="C30RefundFound" select="key('pagedata', 'C30RefundFound')/value"/>
  <xsl:variable name="milesPointsFlag" select="key('pagedata', 'miles_points_flag')/value"/>
  <xsl:variable name="msg_email_indicator" select="key('pagedata', 'msg_email_indicator')/value"/>


  <xsl:template match="/">
<html>
  <head>
    <META http-equiv="Content-Type" content="text/html"/>
        <title>FTD - Refunds</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/tabIndexMaintenance.js"></script>
        <script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script type="text/javascript" src="js/clock.js"></script>
        <script type="text/javascript" src="js/commonUtil.js"></script>
    <script type="text/javascript" src="js/mercuryMessage.js"></script>
    <script type="text/javascript">

//Map containing the full refund indicator (Y/N) for each refund disposition
var refundDispFullRefundIndMap = new Array();

var freeShippingMembershipOrder = "<xsl:value-of select="key('pagedata','freeShippingMembershipOrder')/value"/>";
var freeShippingProgramName = "<xsl:value-of select="key('pagedata','freeShippingProgramName')/value"/>";

//Map hash (required so the hash cannot be a number)
var mhash = '_';
var allowOnlyGCRefund = false;

function displayReturnToRefundReview()
{
  if ("refundReview" != document.RefundForm.prev_page.value)
  {
    document.getElementById("refundReview").style.visibility = "hidden";
  }
}

/*******************************************************************************
*  setRefundsIndexes()
*  Sets tabs for Refunds
*  See aMaintenance.js
*******************************************************************************/
function setRefundsIndexes()
{
  untabAllElements();
  var tabOrder = new Array('refundDisp', 'dispSelected', 'responsibleParty',
    'payType', 'fullRefund', 'refundPercent', 'product_amount', 'addon_amount',
    'fee_amount', 'tax_amount', 'viewMercury', 'searchAction',
    'postRefund', 'deleteRefund', 'refresh', 'update', 'orderComments',
    'backButton', 'mainMenu', 'printer');
  setTabIndexes(tabOrder, findBeginIndex());
}

function changeType(){
        if (document.getElementById('exceededRefundMaxDaysOut').value == 'Y'){
        	alert("Refunds cannot be processed on transactions this old. Please choose another method to resolve the issue.");
            return false;
        }

  var form=document.RefundForm;
        if (document.getElementById('exceededRefundMaxDaysOut').value == 'Y'){
        	alert("Refunds cannot be processed on transactions this old. Please choose another method to resolve the issue.");
            form.refundDisp.selectedIndex=form.tempSelect.value;
            return false;
        }

  form.change_flag.value="Y";
  var gcLabel = document.getElementById("gc_label");

  //if the type is A:
  if((form.refundDisp.options[form.refundDisp.selectedIndex].id)=='A'){

    aOK = "Y";
    if("<xsl:value-of select="key('pagedata', 'order_cancel_flag')/value"/>"=="N"
      &amp;&amp; "<xsl:value-of select="key('pagedata', 'order_live_flag')/value"/>"=="Y") {

      valueOfDisp = form.refundDisp.options[form.refundDisp.selectedIndex].value;
      autoCancelAFlag = "N";
      <xsl:for-each select="/root/RefundDispositions/Disposition">
        if("<xsl:value-of select="refund_disp_code"/>" == valueOfDisp) {
          autoCancelAFlag = "<xsl:value-of select="auto_cancel_a_flag"/>";
        }
      </xsl:for-each>

      if (autoCancelAFlag == "Y") {
        liveOrderCount = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/live_order_count"/>";
        ftdmFlag = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/has_ftdm"/>";
        printedFlag = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/order_printed"/>";
        var zjOrderInTransitFlag = "<xsl:value-of select="key('pagedata', 'zj_order_in_transit')/value"/>";

        if (zjOrderInTransitFlag == "Y") {
              alert("This order is being prepared for shipment and cannot be changed at this time.");
              aOK = "N";
        }
        else if(liveOrderCount != "1") {
          tempAlert = "The order has more than one live mercury message. Order must";
          tempAlert = tempAlert + "\nbe cancelled before A-type refunds may be selected.";
          alert(tempAlert);
          form.refundDisp.selectedIndex=form.tempSelect.value;
          aOK = "N";

        } else if (ftdmFlag == "Y") {
            tempAlert = "This is an FTDM order. CAN mercury message must";
            tempAlert = tempAlert + "\nbe generated on the communication screen and";
            tempAlert = tempAlert + "\ncalled out to the florist prior to refunding.";
            alert(tempAlert);
            form.refundDisp.selectedIndex=form.tempSelect.value;
            aOK = "N";
          } else if (printedFlag == "Y") {
        tempAlert = "Vendor order has printed. Automatic CAN";
              tempAlert = tempAlert + "\nVenus message cannot be created.";
              alert(tempAlert);
        form.refundDisp.selectedIndex=form.tempSelect.value;
              aOK = "N";
            }
      } else {
        alert("Order must be cancelled before A-type refunds may be selected");
        form.refundDisp.selectedIndex=form.tempSelect.value;
        aOK = "N";
      }
    }
    if (aOK == "Y") {
      if ("<xsl:value-of select="key('pagedata', 'delivery_passed_flag')/value"/>"=="Y") {
        alert("You cannot choose an A-type refund for orders that are past the delivery date");
        form.refundDisp.selectedIndex=form.tempSelect.value;
      } else {
        var x=document.getElementById('tblTopSection').rows;
        var y=x[0].cells;
        viewMercury.style.display="none";

        //*****responsible party*****
        form.responsibleParty.style.display="none";
        y[2].innerHTML="";

        //*****payment type*****
        form.payType.style.display="none";
        form.payTypeText.style.display="none";
        y[3].innerHTML="";

        //*****gift certificate/card*****
      showGCfield(false);

        //*****amount fields/buttons*****
        document.RefundForm.product_amount.readOnly=true;
        document.RefundForm.addon_amount.readOnly=true;
        document.RefundForm.fee_amount.readOnly=true;
        document.RefundForm.tax_amount.readOnly=true;
        document.RefundForm.postRefund.disabled=false;
        document.RefundForm.dispSelected.disabled=false;
        document.RefundForm.refresh.disabled=false;
        document.RefundForm.mercury.disabled=false;
        document.RefundForm.orderComments.disabled=false;
        document.RefundForm.refundPercent.disabled=false;
        document.RefundForm.fullRefund.disabled=false;
        setTempSelect();
      }
    }
  }

  //if the type is B:
  var count;
  if((form.refundDisp.options[form.refundDisp.selectedIndex].id)=='B'){

    viewMercury.style.display="inline";
    var x=document.getElementById('tblTopSection').rows;
    var y=x[0].cells;

    //*****responsible party*****
    form.responsibleParty.style.display="inline";
    form.responsibleParty.disabled=false;
    y[2].innerHTML="Responsible Party";

    //*****payment type*****
    form.payType.disabled=false;
    y[3].innerHTML="Payment Type";
    determinePayTypeDisplay();  //note that payTypeText will be determined by this function

    //*****gift certificate/card*****
    showGCfield(false);

    //*****amount fields/buttons*****
    document.RefundForm.product_amount.readOnly=false;
    document.RefundForm.addon_amount.readOnly=false;
    document.RefundForm.fee_amount.readOnly=false;
    document.RefundForm.tax_amount.readOnly=false;
    document.RefundForm.postRefund.disabled=false;
    document.RefundForm.dispSelected.disabled=false;
    document.RefundForm.refresh.disabled=false;
    document.RefundForm.mercury.disabled=false;
    document.RefundForm.orderComments.disabled=false;
    document.RefundForm.refundPercent.disabled=false;
    document.RefundForm.fullRefund.disabled=false;
    setTempSelect();
  }


  //if the type is C:
  if((form.refundDisp.options[form.refundDisp.selectedIndex].id)=='C')
  {
    var valueSelected = form.refundDisp.options[form.refundDisp.selectedIndex].value;
    var x=document.getElementById('tblTopSection').rows;
    var y=x[0].cells;
    viewMercury.style.display="inline";

    //*****payment type*****
    form.payType.style.display="inline";
    form.payType.disabled=false;
    y[3].innerHTML="Payment Type";
    determinePayTypeDisplay();  //note that payTypeText will be determined by this function

    //a C30 refund is not really a refund - it's a change of payment where a refund will be issued
    //depending on the amount of the Gift Certificate or gift card, and a payment record will be created
    if (valueSelected == 'C30')
    {
      //*****responsible party*****
      form.responsibleParty.style.display="inline";
      form.responsibleParty.disabled=true;
      y[2].innerHTML="Responsible Party";
      form.responsibleParty.value ="Other";

      //*****gift certificate/gift card*****
      showGCfield(true);

      //*****amount fields/buttons*****
      document.RefundForm.product_amount.readOnly=true;
      document.RefundForm.addon_amount.readOnly=true;
      document.RefundForm.fee_amount.readOnly=true;
      document.RefundForm.tax_amount.readOnly=true;
      document.RefundForm.postRefund.disabled=false;
        document.RefundForm.dispSelected.disabled=false;
      document.RefundForm.refresh.disabled=false;
      document.RefundForm.mercury.disabled=false;
      document.RefundForm.orderComments.disabled=false;
      document.RefundForm.refundPercent.disabled=false;
      document.RefundForm.fullRefund.disabled=false;
      setTempSelect();
    }
    else
    {
      //*****responsible party*****
      form.responsibleParty.style.display="inline";
      form.responsibleParty.disabled=false;
      y[2].innerHTML="Responsible Party";

      //*****gift certificate/card*****
      showGCfield(false);

      document.RefundForm.product_amount.readOnly=false;
      document.RefundForm.addon_amount.readOnly=false;
      document.RefundForm.fee_amount.readOnly=false;
      document.RefundForm.tax_amount.readOnly=false;
      document.RefundForm.postRefund.disabled=false;
      document.RefundForm.dispSelected.disabled=false;
      document.RefundForm.refresh.disabled=false;
      document.RefundForm.mercury.disabled=false;
      document.RefundForm.orderComments.disabled=false;
      document.RefundForm.refundPercent.disabled=false;
      document.RefundForm.fullRefund.disabled=false;
    }
  } //end of "if type = C"


  //regardless of type:
  if("<xsl:value-of select="key('pagedata', 'edit_tax_flag')/value"/>"=="Y"){
    document.RefundForm.tax_amount.style.display="inline";
  }

        showComplaintCommFields();


        if(refundDispFullRefundIndMap[mhash + form.refundDisp.options[form.refundDisp.selectedIndex].value]=='Y' || valueSelected=='C30')
  {
    fullRefund();
  }
  else
  {
    document.RefundForm.product_amount.value="0.00";
    document.RefundForm.addon_amount.value="0.00";
    document.RefundForm.fee_amount.value="0.00";
    document.RefundForm.tax_amount.value="0.00";
    //document.RefundForm.tax_amount.value=<xsl:value-of select="key('remaining_totals','taxTotal')/value"/>;
  }

  <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
    document.RefundForm.postRefund.disabled=true;
  </xsl:if>
}

function showGCfield(show)
{
  var gcLabel = document.getElementById("gc_label");
  var gcNumber = document.getElementById("gc_number");
  var gdPinLabel = document.getElementById("gd_pin_label");
  var gdPin = document.getElementById("gd_pin");

  if (show)
  {
    gcLabel.style.display = "inline";
    gcNumber.style.display = "inline";
    gdPinLabel.style.display = "inline";
    gdPin.style.display = "inline";
    gcNumber.focus();
  }
  else
  {
    gcLabel.style.display = "none";
    gcNumber.style.display = "none";
    gcNumber.style.backgroundColor = "white";
    gcNumber.value = "";
    gdPinLabel.style.display = "none";
    gdPin.style.display = "none";
    gdPin.style.backgroundColor = "white";
    gdPin.value = "";
  }
}


  // method for server call to check gcc number for gd or gc
  function checkGCType()
  {
    // mark gccnum field as dirty
    // document.getElementById("is_gd_dirty").value = "Y";
    var url = 'checkGCType.do';
    var querystring =  'gcc_num=' + document.getElementById("gc_number").value +
      '&amp;giftCardPaymentEnabled=' +  document.getElementById("giftCardPaymentEnabled").value +
      getSecurityParams(false);

    $.ajax({
      url:url,
        dataType:"xml",
        method:"GET",
        data: querystring,
        cache: false,
        success: function(data) {
          checkGCTypeCallBack(data);
        }

    });
  }

  function checkGCTypeCallBack(data)
  {
    var gcNumber = document.getElementById("gc_number");
    var gdPin = document.getElementById("gd_pin");

    var gdorgc = $(data).find("gd_or_gc").text();

    if (gdorgc == 'GD')
    {
      $('#gd_or_gc').val("GD")
      gdPin.disabled = false;
      gdPin.focus();
        if(allowOnlyGCRefund == true){
          fieldBlurByElement(gdPin);
        gdPin.disabled = true;
        }
    }
    else
    {
      $('#gd_or_gc').val("GC");
      fieldBlurByElement(gdPin);
      gdPin.disabled = true;
    }
    gdPin.value = "";
    if (gcNumber != undefined) {
      fieldBlurByElement(gcNumber);
    }
  }

function determinePayTypeDisplay(){
  var count;
  var form = document.RefundForm;
  //if more than one pay type, dropdown -- else a label.
  count=0;
  <xsl:for-each select="root/Payments/PaymentVO">
    count=count+1;
  </xsl:for-each>
  if(count>1){
    form.payType.style.display="inline";
    form.payTypeText.style.display="none";
    form.payDisplay="2";
  }else{
    form.payType.style.display="none";
    form.payTypeText.style.display="inline";
    form.payType.value="1";
    form.payDisplay="1";
  }
}

function fullRefund()
{

  document.getElementById("payType").selectedIndex = 0;
  var valueSelected = document.RefundForm.refundDisp.options[document.RefundForm.refundDisp.selectedIndex].value;

  //merchandise amount
  document.RefundForm.product_amount.readOnly=true;
  document.RefundForm.product_amount.value=<xsl:value-of select="key('remaining_totals','msdTotal')/value"/>;

  //addon amount
  document.RefundForm.addon_amount.readOnly=true;
  document.RefundForm.addon_amount.value=<xsl:value-of select="key('remaining_totals','addOnTotal')/value"/>;

  //service amount
  document.RefundForm.fee_amount.readOnly=true;
  document.RefundForm.fee_amount.value=<xsl:value-of select="key('remaining_totals','feeTotal')/value"/>;

  //tax amount
  document.RefundForm.tax_amount.readOnly=true;
  if (valueSelected == 'C30')
  {
    document.RefundForm.tax_amount.value=<xsl:value-of select="key('remaining_totals','taxTotal')/value"/>;
  }

  //buttons
  document.RefundForm.refundPercent.disabled="true";
  document.RefundForm.fullRefund.disabled="true";

  payTypeChange();
}

function setTempSelect(){
  var form = document.RefundForm;
  form.tempSelect.value=form.refundDisp.selectedIndex;
}

function checkForAutoCAN() {
  var retVal = "";

  liveOrderCount = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/live_order_count"/>";
  ftdmFlag = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/has_ftdm"/>";
  beforeDelivery = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/before_delivery"/>";
  fillingFlorist = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/filling_florist"/>";
  respSelected=(document.RefundForm.responsibleParty.options[document.RefundForm.responsibleParty.selectedIndex].value);
  orderLiveFlag = "<xsl:value-of select="key('pagedata', 'order_live_flag')/value"/>";

  if (liveOrderCount != "1") {
    retVal = "OrderCount";
  } else {
    if (ftdmFlag == "Y") {
      retVal = "FTDM";
    } else {
      if (beforeDelivery == "Y") {
        retVal = "BeforeDelivery";
      } else {
        if (fillingFlorist != respSelected) {
          retVal = "FillingFlorist";
        } else {
          if (orderLiveFlag == "N") {
            retVal = "OrderNotLive";
          }
        }
      }
    }
  }

  return retVal;
}

function checkForAutoACAN() {
  var retVal = "";

  orderLiveFlag = "<xsl:value-of select="key('pagedata', 'order_live_flag')/value"/>";

  if (orderLiveFlag == "N") {
    retVal = "OrderNotLive";
  }

  return retVal;
}

function checkForAutoAskP() {
  var retVal = "";

  liveOrderCount = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/live_order_count"/>";
  ftdmFlag = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/has_ftdm"/>";
  vendorFlag = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/vendor_flag"/>";
  beforeDelivery = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/before_delivery"/>";
  fillingFlorist = "<xsl:value-of select="/root/OrderFulfillment/Fulfill/filling_florist"/>";
  respSelected=(document.RefundForm.responsibleParty.options[document.RefundForm.responsibleParty.selectedIndex].value);

  if (vendorFlag == "Y") {
    retVal = "Vendor";
  } else {
    if (liveOrderCount != "1") {
      retVal = "OrderCount";
    } else {
      if (ftdmFlag == "Y") {
        retVal = "FTDM";
      } else {
        if (beforeDelivery == "Y") {
          retVal = "BeforeDelivery";
        } else {
          if (fillingFlorist != respSelected) {
            retVal = "FillingFlorist";
          }
        }
      }
    }
  }
  return retVal;
}

function displayAutoException(msg,msgType) {
  switch(msg)
  {
    case "OrderCount":
      tempAlert = "No " + msgType + " message created. Order must have only one live mercury message";
      break;
    case "FTDM":
      tempAlert = "This is an FTDM order. " + msgType + " mercury message must be generated";
      tempAlert = tempAlert + "\non the communication screen and called out to the florist.";
      break;
    case "BeforeDelivery":
      tempAlert = msgType + " automation cannot occur before the delivery date.";
      break;
    case "FillingFlorist":
      tempAlert = msgType + " automation cannot occur. Order florist does not match responsible florist.";
      break;
    case "Vendor":
      tempAlert = msgType + " automation cannot occur for Vendor Orders.";
      break;
    case "OrderNotLive":
      tempAlert = "No " + msgType + " message created. Order has no live mercury message.";
      break;
    default:
      tempAlert = "Automation failed";
  }
  alert(tempAlert);
  return;
}

function postRefundClick(){
  var form = document.RefundForm;
  var respSelected=(form.responsibleParty.options[form.responsibleParty.selectedIndex].value);
  var typeOfDisp=(form.refundDisp.options[form.refundDisp.selectedIndex].id);
  var valueOfDisp= form.refundDisp.options[form.refundDisp.selectedIndex].value;
        var errorDiv = document.getElementById('errorDiv');
        errorDiv.innerHTML = "";
        var complaintCommOrigin = form.complaint_comm_origin_type_id.options[form.complaint_comm_origin_type_id.selectedIndex].value;
        var complaintCommNotification = form.complaint_comm_notification_type_id.options[form.complaint_comm_notification_type_id.selectedIndex].value;

        //Validations first

        //Don't allow only one value set to Not a complaint
        if (!(form.complaint_comm_origin_type_id.disabled  || form.complaint_comm_notification_type_id.disabled) ) {
          if (complaintCommOrigin == "0" || complaintCommNotification == "0")
          {
               errorDiv.innerHTML = "Please select ‘Origin of Refund’ and ‘Origin of Refund Notification’ from the drop downs.";
               return true;
          }

          if (complaintCommOrigin == "NC" || complaintCommNotification == "NC")
          {
            if (complaintCommOrigin != "NC" || complaintCommNotification != "NC")
            {
                errorDiv.innerHTML = "When choosing not a complaint as an origin in one of the pull downs, not a complaint must also be selected in the second pull down.";
                return true;
            }
          }
        }


  if(respSelected=="0")
  {
    if( typeOfDisp=="B" ||
        (typeOfDisp=="C" &amp;&amp; valueOfDisp!="C30")
      )
    {
      alert("Please select a responsible party for the refund.");
      form.postRefund.disabled = false;
      form.dispSelected.disabled = false;
      return true;
    }
  }


        form.postRefund.disabled = true;
        form.dispSelected.disabled=true;

  //A C30 refund cannot be cancelled later.  Ensure that the CSR really wants to issue a C30
  if(valueOfDisp=="C30")
  {
    if (confirm("Note: A C30 refund cannot be removed later.  Are you sure you want to apply a C30 refund?"))
    {
      var giftCertNum = form.gc_number.value;
      if (giftCertNum.length == 0)
      {
        alert("Please enter a Gift Card/Gift Certificate Number");
        form.postRefund.disabled = false;
        form.dispSelected.disabled = false;
        form.gc_number.style.backgroundColor  = "pink";
        form.gc_number.focus();
        return true;
      }
    }
    else
    {
      form.postRefund.disabled = false;
      form.dispSelected.disabled = false;
      return true;
    }
  }


<![CDATA[
  if ((form.product_amount.value != '' && isNaN(parseFloat(form.product_amount.value)))
    || (form.addon_amount.value != '' && isNaN(parseFloat(form.addon_amount.value)))
    || (form.fee_amount.value != '' && isNaN(parseFloat(form.fee_amount.value)))
    || (form.tax_amount.value != '' && isNaN(parseFloat(form.tax_amount.value))))
  {
    alert('Refund amounts must be numeric values.');
    form.postRefund.disabled = false;
    form.dispSelected.disabled=false;
    return true;
  }

  if( parseFloat(form.product_amount.value) < 0
    || parseFloat(form.addon_amount.value) < 0
    || parseFloat(form.fee_amount.value) < 0
    || parseFloat(form.tax_amount.value) < 0)
  {
    alert('Must enter a positive amount for a refund.');
    form.postRefund.disabled = false;
    form.dispSelected.disabled=false;
    return true;
  }
]]>

  autoCancelFlag = "N";
  autoCancelAFlag = "N";
  autoAskPFlag = "N";

  <xsl:for-each select="/root/RefundDispositions/Disposition">
      if("<xsl:value-of select="refund_disp_code"/>" == valueOfDisp) {
          autoCancelFlag = "<xsl:value-of select="auto_cancel_flag"/>";
          autoCancelAFlag = "<xsl:value-of select="auto_cancel_a_flag"/>";
          autoAskPFlag = "<xsl:value-of select="auto_askp_flag"/>";
          temp = "autoCancelFlag: " + autoCancelFlag + "\n";
          temp = temp + "autoCancelAFlag: " + autoCancelAFlag + "\n";
          temp = temp + "autoAskPFlag: " + autoAskPFlag + "\n";
      }
  </xsl:for-each>

  productDiff = <xsl:value-of select="key('remaining_totals','msdTotal')/value"/> - form.product_amount.value;
  addonDiff = <xsl:value-of select="key('remaining_totals','addOnTotal')/value"/> - form.addon_amount.value;
  feeDiff = <xsl:value-of select="key('remaining_totals','feeTotal')/value"/> - form.fee_amount.value;
  totalDiff = productDiff + addonDiff + feeDiff;
  temp = temp + "productDiff: " + productDiff + "\n";
  temp = temp + "addonDiff: " + addonDiff + "\n";
  temp = temp + "feeDiff: " + feeDiff + "\n";
  temp = temp + "totalDiff: " + totalDiff + "\n";
  
  prodAddonDiff = productDiff + addonDiff;   	
  	
	
<![CDATA[
  // if order is in printed or shipped status and is on or after the ship date
  // but before the delivery date then it cannot be cancelled //
  if (document.getElementById('in_transit').value == 'Y' && (autoCancelFlag == 'Y' || autoCancelAFlag == 'Y') ){
    autoCancelFlag = 'N';
    autoCancelAFlag = 'N';
    alert('The automatic CAN message is not available.  This order is in printed or \nshipped status  and is on or after the ship date but before the delivery date \nand cannot be canceled at this time.');
  }
   	  if(document.getElementById('fullyRefundMechAmtFlag').value == "N")
      { 
	  	  if(prodAddonDiff == "0" && feeDiff != "0")
		  {	
			    alert('This refund amount is not allowed. You may issue partial refunds on any field or fully refund the order.');
		   		form.postRefund.disabled = false;
		    	form.dispSelected.disabled = false;
		    	return false;
		  }
 	  }
 	 
  if (totalDiff == "0") {
      if(freeShippingMembershipOrder == "Y") {
        if(!confirm('You are refunding ' + freeShippingProgramName + '.  Would you like to proceed with the refund?')) {
          form.postRefund.disabled = false;
          form.dispSelected.disabled = false;
          return false;
        }

        form.doAutoCan.value = "N";

      } else if (autoCancelFlag == "Y") {
          doAutoCan = checkForAutoCAN();
          temp = temp + "doAutoCan: " + doAutoCan;
          if (doAutoCan == "") {
              var modalArguments = new Object();
              var modal_dim = "dialogWidth:30; dialogHeight:20; center:yes; status:0; help:no; scroll:no";
              var popURL = "autoCancel.htm";
              var results;
              if(valueOfDisp=="B10" && form.full_refund_check.value=="F"){
              	
              	modalArguments.modalText = 'Please cancel this order due to product quality issue.';
        		results = window.showModalDialog(popURL, modalArguments, modal_dim);
      		  }
      		  else{
      		    modalArguments.modalText = 'Please cancel this order due to non-delivery of product.';
              	results = window.showModalDialog(popURL, modalArguments, modal_dim);
              }
              if (results && results != null) {
                  form.doAutoCan.value = results[0];
                  form.autoReasonText.value = results[1];
              } else {
                  form.doAutoCan.value = "N";
              }
             
          } else {
              displayAutoException(doAutoCan, "CAN");
              form.doAutoCan.value = doAutoCan;
          }
      } else {
          if (autoCancelAFlag == "Y") {
              doAutoACan = checkForAutoACAN();
              temp = temp + "doAutoACan: " + doAutoACan;
              if (doAutoACan == "") {
                  var modal_dim = "dialogWidth:30; dialogHeight:20; center:yes; status:0; help:no; scroll:no";
                  var popURL = "autoCancelA.htm";
                  var results=showModalDialogIFrame(popURL, "", modal_dim);
                  if (results && results != null) {
                      form.doAutoCan.value = results[0];
                      form.autoReasonText.value = results[1];
                  } else {
                      form.doAutoCan.value = "N";
                  }
              } else {
              	  displayAutoException(doAutoACan, "CAN");
                  form.doAutoCan.value = doAutoACan;
              }
          }
      }
  } else {
      if (freeShippingMembershipOrder == "Y") {
        form.doAutoAskP.value = "N";
      } else if ((form.product_amount.value + 0) > 0 || (form.addon_amount.value + 0) > 0) {
          doAutoAskP = checkForAutoAskP();
          temp = temp + "doAutoAskP: " + doAutoAskP;
          if (doAutoAskP == "") {
              var modal_dim = "dialogWidth:30; dialogHeight:20; center:yes; status:0; help:no; scroll:no";
              var popURL = "autoAskP.htm";
              var results=showModalDialogIFrame(popURL, "", modal_dim);
              if (results && results != null) {
                  form.doAutoAskP.value = results[0];
                  form.autoReasonText.value = results[1];
                  form.autoAskPPrice.value = results[2];
              } else {
                  form.doAutoAskP.value = "N";
              }
          } else {
              displayAutoException(doAutoAskP, "ASKP");
              form.doAutoAskP.value = doAutoAskP;
          }
      }
  }
]]>

  //For a C30 refund, we want to validate the Gift Certificate/Gift Card first and then apply the refund.
  if (valueOfDisp == "C30")
  {
    redeemGcCoupon();
    return true;
  }
  else
  {
    if(form.payDisplay=="2")
    {
      if (( form.payType.options[form.payType.selectedIndex].value)=="0"        &amp;&amp;
            typeOfDisp != 'A'                                                   &amp;&amp;
            (parseFloat(document.getElementById("product_total").value) !=
                parseFloat(document.getElementById("product_amount").value)     ||
            parseFloat(document.getElementById("addon_total").value) !=
                parseFloat(document.getElementById("addon_amount").value)       ||
            parseFloat(document.getElementById("fee_total").value) !=
              parseFloat(document.getElementById("fee_amount").value)))
      {
        alert("Please select a payment type to which you want to apply the refund.");
        form.postRefund.disabled = false;
        form.dispSelected.disabled=false;
      }
      else
      {
        var paySeqId=form.payType.options[form.payType.selectedIndex].value;
        form.payment_type_seq.value=paySeqId;
        form.disp_code.value=form.refundDisp.options[form.refundDisp.selectedIndex].value;
        form.responsible_party.value=form.responsibleParty.options[form.responsibleParty.selectedIndex].value;
        form.product_amount.value=form.product_amount.value;
        form.addon_amount.value=form.addon_amount.value;
        form.fee_amount.value=form.fee_amount.value;
        form.tax_amount.value=form.tax_amount.value;
        form.action_type.value="post";
        form.action="updateRefund.do";
        form.submit();
      }
    }
    else
    {
      form.payment_type_seq.value='<xsl:value-of select="/root/Payments/PaymentVO/paymentSeqId"/>';
      form.disp_code.value=form.refundDisp.options[form.refundDisp.selectedIndex].value;
      form.responsible_party.value=form.responsibleParty.options[form.responsibleParty.selectedIndex].value;
      form.product_amount.value=form.product_amount.value;
      form.addon_amount.value=form.addon_amount.value;
      form.fee_amount.value=form.fee_amount.value;
      form.tax_amount.value=form.tax_amount.value;
      form.action_type.value="post";
      form.action="updateRefund.do";
      form.submit();
    }
  }
}


/*****************************************************************************************************
*
* This function is called for a C30 refund type. It will call the GcCouponAction.java in an iFrame.
*
* Note that the parameter "method_to_invoke" will be passed back from the GcCouponAction.java to
* gcCoupon.xsl, which will then call that method within Refunds.xsl.
*
*****************************************************************************************************/
function redeemGcCoupon()
{
  var form = document.RefundForm;

  //Note that 0 is subtracted so that the variables are automatically considered as numeric
  var prodAmount    = form.product_amount.value - 0;
  var addOnAmount   = form.addon_amount.value - 0;
  var feeAmount     = form.fee_amount.value - 0;
  var taxAmount     = form.tax_amount.value - 0;

  var total = prodAmount + addOnAmount + feeAmount + taxAmount;

  var url  = "gcCoupon.do?page_action=redeem"  +
              getSecurityParams(false) +
              '&amp;method_to_invoke=postC30Refund' +
              '&amp;gc_coupon_number=' + form.gc_number.value +
              '&amp;gd_pin=' + form.gd_pin.value +
              '&amp;gd_or_gc=' + form.gd_or_gc.value +
              '&amp;total_owed=' + total +
              '&amp;uniqueifier=' + new Date().getTime();

  showWaitMessage("innerContent", "wait", "Please wait - validating gift card/gift certificate");

  var frame = document.getElementById('validateGiftCertIFrame');
  frame.src = url;


}//end method redeemGcCoupon()



/*****************************************************************************************************
*
* This function is called by gcCoupon.xsl for a C30 refund type.
*
* gcCoupon.xsl has evaluated the status of the GC/GD that was sent from the Action, and has set the
* necessary fields within Refunds.xsl.
*
* Based on the fields set by gcCoupon.xsl, this method will either display a message that the Gift
* Certificate/Gift Card could not be redeemed (and the reason why), or it will reset the amounts as per the
* GC/GD allowance and will call the UpdateRefundAction.java.
*
*****************************************************************************************************/
function postC30Refund()
{

  var form = document.RefundForm;
  //hideWaitMessage("innerContent", "wait");

  var typeOfDisp=(form.refundDisp.options[form.refundDisp.selectedIndex].id);
  var status = form.is_gc_valid.value;
  var message = form.invalid_gc_message.value;

  if (status == 'N')
  {
    hideWaitMessage("innerContent", "wait");
    // error messages need to be massaged for C30 usage but only for errors that apply to both
      // gift card and certificate
          message = message.replace(/^\s*|\s*$/,"");
        if (message.indexOf("Invalid Gift") != -1 || message.indexOf("has already been redeemed") != -1)
        {
          message = message.replace("Gift Certificate","Gift Card/Certificate") + "\n\n" +
            "If the customer is using a Gift Card, direct \nthem to the redemption instructions \nincluded " +
            "in their email or on the back of the \ncard in order to contact the card issuer."
        }
    alert(message);
    form.postRefund.disabled = false;
    form.dispSelected.disabled = false;
    form.responsibleParty.style.display="inline";
    form.responsibleParty.disabled=true;
    form.responsibleParty.value ="Other";
    form.gc_number.style.backgroundColor  = "pink";
    form.gc_number.focus();
    return true;
  }
  else
  {
    var gcAmount          = document.RefundForm.gc_amount.value - 0;
    var gcAmountRemainder = gcAmount;
    var prodAmount        = <xsl:value-of select="key('remaining_totals','msdTotal')/value"/> - 0;
    var addOnAmount       = <xsl:value-of select="key('remaining_totals','addOnTotal')/value"/> - 0;
    var feeAmount         = <xsl:value-of select="key('remaining_totals','feeTotal')/value"/> - 0;
    var taxAmount         = <xsl:value-of select="key('remaining_totals','taxTotal')/value"/> - 0;
    var C30FullRefund     = 'Y';
    var ccTotal           = 0;
    var GDorGC = document.getElementById("gd_or_gc").value;


    //if payment display shows a count > 1, that is, its a select box with multiple payment types
    if(form.payDisplay=="2")
    {
      var paymentSeq = document.getElementById("payType").value;

      //and payment type selected is not default "All Payments"
      if(paymentSeq != '0')
      {

        prodAmount = document.getElementById("payment_product_amount_" + paymentSeq).value - 0;
        addOnAmount = document.getElementById("payment_addon_amount_" + paymentSeq).value - 0;
        feeAmount = document.getElementById("payment_fee_amount_" + paymentSeq).value - 0;
        taxAmount = document.getElementById("payment_tax_amount_" + paymentSeq).value - 0;
      }
    }

    ccTotal = prodAmount + addOnAmount + feeAmount + taxAmount;

    //for issue 12646, business rule was implemented that the GC amount must be less than the total payment amount
    //of the payment type that's being swapped with C30.  This was done to prevent CSR from erroneously applying C30
    //to a payment type where amount is (significantly) less than GC - because C30 cannot be deleted/re-applied
    if (gcAmount > ccTotal &amp;&amp; GDorGC == 'GC')
    {
      var gcAmountDisplay = formatAmount2DecimalPlaces(Math.round(gcAmount * 100)/100);
      var ccTotalDisplay = formatAmount2DecimalPlaces(Math.round(ccTotal * 100)/100);

      hideWaitMessage("innerContent", "wait");
      alert("Gift Certificate amount ($" + gcAmountDisplay + ") is greater than the line total ($" + ccTotalDisplay + "). Please select a different payment type.");
      form.postRefund.disabled = false;
      form.dispSelected.disabled=false;
      form.responsibleParty.style.display="inline";
      form.responsibleParty.disabled=true;
      form.responsibleParty.value ="Other";
      form.payType.disabled=false;
      form.payType.focus();
      return true;
    }

    document.RefundForm.product_amount.value="0.00";
    document.RefundForm.addon_amount.value="0.00";
    document.RefundForm.fee_amount.value="0.00";
    document.RefundForm.tax_amount.value="0.00";


    //*****PRODUCT AMOUNT
    if (gcAmountRemainder > prodAmount)
    {
      document.RefundForm.product_amount.value = prodAmount;
      gcAmountRemainder -= prodAmount;
      gcAmountRemainder = Math.round(gcAmountRemainder * 100)/100;

      //*****ADD-ON AMOUNT
      if (gcAmountRemainder > addOnAmount)
      {
        document.RefundForm.addon_amount.value = addOnAmount;
        gcAmountRemainder -= addOnAmount;
        gcAmountRemainder = Math.round(gcAmountRemainder * 100)/100;

        //*****FEE AMOUNT
        if (gcAmountRemainder > feeAmount)
        {
          document.RefundForm.fee_amount.value = feeAmount  - 0;
          gcAmountRemainder -= feeAmount;
          gcAmountRemainder = Math.round(gcAmountRemainder * 100)/100;

          //*****TAX AMOUNT ---- note that even if gcAmountRemainder = taxAmount, its a full reund
          if (gcAmountRemainder >= taxAmount)
          {
            document.RefundForm.tax_amount.value = taxAmount    - 0;
            gcAmountRemainder -= taxAmount;
            gcAmountRemainder = Math.round(gcAmountRemainder * 100)/100;

          }
          else
          {
            document.RefundForm.tax_amount.value = gcAmountRemainder;
            gcAmountRemainder = 0;
            C30FullRefund = 'N';

          }
        }
        else
        {
          document.RefundForm.fee_amount.value = gcAmountRemainder;
          gcAmountRemainder = 0;
          C30FullRefund = 'N';

        }
      }
      else
      {
        document.RefundForm.addon_amount.value = gcAmountRemainder;
        gcAmountRemainder = 0;
        C30FullRefund = 'N';

      }
    }
    else
    {
      document.RefundForm.product_amount.value = gcAmountRemainder;
      gcAmountRemainder = 0;
      C30FullRefund = 'N';

    }

    document.RefundForm.C30_full_refund.value = C30FullRefund;
    document.RefundForm.gc_amount_remainder.value = gcAmountRemainder;

    if(form.payDisplay=="2")
    {
      if (( form.payType.options[form.payType.selectedIndex].value)=="0"        &amp;&amp;
            typeOfDisp != 'A'                                                   &amp;&amp;
            (parseFloat(document.getElementById("product_total").value) !=
                parseFloat(document.getElementById("product_amount").value)     ||
            parseFloat(document.getElementById("addon_total").value) !=
                parseFloat(document.getElementById("addon_amount").value)       ||
            parseFloat(document.getElementById("fee_total").value) !=
              parseFloat(document.getElementById("fee_amount").value)))
      {
        hideWaitMessage("innerContent", "wait");

        alert("Please select a payment type to which you want to apply the refund.");
        form.postRefund.disabled = false;
        form.dispSelected.disabled=false;
        form.responsibleParty.style.display="inline";
        form.responsibleParty.disabled=true;
        form.responsibleParty.value ="Other";
        form.payType.disabled=false;
        form.payType.focus();
        return true;
      }
      else
      {
        var paySeqId=form.payType.options[form.payType.selectedIndex].value;
        form.payment_type_seq.value=paySeqId;
        form.disp_code.value=form.refundDisp.options[form.refundDisp.selectedIndex].value;
        form.responsible_party.value=form.responsibleParty.options[form.responsibleParty.selectedIndex].value;
        form.product_amount.value=form.product_amount.value;
        form.addon_amount.value=form.addon_amount.value;
        form.fee_amount.value=form.fee_amount.value;
        form.tax_amount.value=form.tax_amount.value;
        form.action_type.value="postC30";
        form.action="updateRefund.do";
        form.submit();
      }
    }
    else
    {
      form.payment_type_seq.value='<xsl:value-of select="/root/Payments/PaymentVO/paymentSeqId"/>';
      form.disp_code.value=form.refundDisp.options[form.refundDisp.selectedIndex].value;
      form.responsible_party.value=form.responsibleParty.options[form.responsibleParty.selectedIndex].value;
      form.product_amount.value=form.product_amount.value;
      form.addon_amount.value=form.addon_amount.value;
      form.fee_amount.value=form.fee_amount.value;
      form.tax_amount.value=form.tax_amount.value;
      form.action_type.value="postC30";
      form.action="updateRefund.do";
      form.submit();
    }
  showWaitMessage("innerContent", "wait", "Please wait - validating gift card/gift certificate");
  }
}


function refreshClick(){
  var form=document.RefundForm;
  if(form.change_flag.value=="Y"){
    var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
      if (exitConfirmed==true){
        form.action="loadRefunds.do";
        form.submit();
      }
  }else{
    form.action="loadRefunds.do";
    form.submit();
  }
}

<![CDATA[
function deleteRefundClick(){
  var form=document.RefundForm;

  // Check if all refunds were checked when delete - a-type is all or none
  if(form.a_type_flag.value == "Y")
  {
    var refundCount=0;
    var deleteChecked=0;

    ]]>
    <xsl:for-each select="root/Payments/PaymentVO/RefundVO">
      refundCount = refundCount+1;
    </xsl:for-each>
    <![CDATA[

    for(i=refundCount; i>0; i--){
      if(document.getElementById("refund_delete_flag_"+i).checked==true){
        deleteChecked += 1;
      }
    }

    if (deleteChecked<refundCount){
      alert("You must delete all A-type refunds for this order if you want to remove the refund");
      return true;
    }
  }

  form.action_type.value="delete";
  form.action="updateRefund.do";
  form.submit();
}


function updateClick(){
  var form=document.RefundForm;
        var complaintErrorFlag = false;
  // Check if all refunds were checked when delete - a-type is all or none
        var refundCount=0;
        ]]>
        <xsl:for-each select="root/Payments/PaymentVO/RefundVO">
                refundCount = refundCount+1;
        </xsl:for-each>
        <![CDATA[

        for(i=refundCount; i>=1; i--)
        {
          if(document.getElementById("complaintCommDetail"+i).style.display == "inline")
          {
            var refundErrorDiv = document.getElementById("refundErrorDiv"+i);
            refundErrorDiv.innerHTML = "";
            if(document.getElementById("complaint_comm_origin_type_id_"+i).value == 'NC' || document.getElementById("complaint_comm_notification_type_id_"+i).value == 'NC')
            {
              if(document.getElementById("complaint_comm_origin_type_id_"+i).value != 'NC' || document.getElementById("complaint_comm_notification_type_id_"+i).value != 'NC')
              {
                refundErrorDiv.innerHTML = "When choosing not a complaint as an origin in one of the pull downs, not a complaint must also be selected in the second pull down.";
                complaintErrorFlag = true;
              }
            }

            if (document.getElementById("complaint_comm_origin_type_id_"+i).value == "0" || document.getElementById("complaint_comm_notification_type_id_"+i).value == "0")
            {
               refundErrorDiv.innerHTML = "Please select ‘Origin of Refund’ and ‘Origin of Refund Notification’ from the drop downs.";
               complaintErrorFlag = true;
            }
          }
          else  //if it is not displayed, don't disable the values
          {
            document.getElementById("complaint_comm_origin_type_id_"+i).disabled=true;
            document.getElementById("complaint_comm_notification_type_id_"+i).disabled=true;
          }
        }


  if(form.a_type_flag.value == "Y")
  {
    var mismatchFound=false;



    for(i=refundCount; i>1; i--){
      if(document.getElementById("refund_disp_"+i).value!=document.getElementById("refund_disp_1").value){
        mismatchFound = true;
      }
    }

    if (mismatchFound){
      alert("All dispositions for A-type refunds must be the same for an order");
      return true;
    }
  }

        if (complaintErrorFlag)
        {
          return true;
        }

  form.action_type.value="edit";
  form.action="updateRefund.do";
  form.submit();
}
]]>

function clickCheckBox(refundSeqID){
  var b = document.getElementById("refund_delete_flag_"+refundSeqID).checked;
  <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
    if(b == true){
        document.RefundForm.deleteRefund.disabled=false;
    }
    var count=0;
    <xsl:for-each select="root/Payments/PaymentVO/RefundVO">
      count=count+1;
    </xsl:for-each>
    var checked=0;
    for(i=count; i>0; i--){
      if(document.getElementById("refund_delete_flag_"+i).checked==true){
        checked+=1;
      }
    }
    if (checked==0){
      document.RefundForm.deleteRefund.disabled=true;
    }
    document.RefundForm.change_flag.value="Y";
  </xsl:if>
}

function dispCodeChange(refundSeqID){
  <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
    document.getElementById("refund_edit_flag_" + refundSeqID).value="Y";
    document.RefundForm.update.disabled=false;
    document.RefundForm.change_flag.value="Y";
  </xsl:if>

        if (document.RefundForm.preferred_partner.value == "Y"){
            refundErrorDiv = document.getElementById("refundErrorDiv"+refundSeqID);
            refundErrorDiv.style.display = "none";
            refundComplaintCommDetail = document.getElementById("complaintCommDetail"+refundSeqID);
            refundComplaintCommDetail.style.display="none";
            refundDisposition = document.getElementById("refund_disp_" + refundSeqID);

            valueOfDisp = refundDisposition.options[refundDisposition.selectedIndex].value;
            <xsl:for-each select="/root/RefundDispositions/Disposition">
              if("<xsl:value-of select="refund_disp_code"/>" == valueOfDisp)
              {
                  if("<xsl:value-of select="complaint_comm_type_req_flag"/>"=="Y")
                  {
                      refundComplaintCommDetail.style.display="inline";
                      refundErrorDiv.style.display = "inline";
                  }
              }
            </xsl:for-each>
        }
}

function respPartyChange(){
  document.RefundForm.change_flag.value="Y";
}

function resetAmounts()
{
  document.RefundForm.change_flag.value='Y';

  // Zero out values on payment change
  document.getElementById("product_amount").value = "0.00";
  document.getElementById("addon_amount").value = "0.00";
  document.getElementById("fee_amount").value = "0.00";
  document.getElementById("tax_amount").value = "0.00";
}

function payTypeChange(){
  var paymentSeq = document.getElementById("payType").value;
  var form=document.RefundForm;
  if(paymentSeq == '0')
  {
    document.getElementById("msdTotalDisplay").innerHTML    = formatNumber(document.getElementById("product_total").value);
    document.getElementById("addOnTotalDisplay").innerHTML  = formatNumber(document.getElementById("addon_total").value);
    document.getElementById("feeTotalDisplay").innerHTML    = formatNumber(document.getElementById("fee_total").value);
    document.getElementById("taxTotalDisplay").innerHTML    = formatNumber(document.getElementById("tax_total").value);

    document.getElementById("partial_product_total").value  = document.getElementById("product_total").value;
    document.getElementById("partial_addon_total").value    = document.getElementById("addon_total").value;
    document.getElementById("partial_fee_total").value      = document.getElementById("fee_total").value;
    document.getElementById("partial_tax_total").value      = document.getElementById("tax_total").value;

    if( (form.refundDisp.options[form.refundDisp.selectedIndex].id)=='B' ||  (form.refundDisp.options[form.refundDisp.selectedIndex].id)=='C'  )
    {
      var bValueSelected = form.refundDisp.options[form.refundDisp.selectedIndex].value;
      if (bValueSelected == 'B20' || bValueSelected == 'C30')
      {
        document.getElementById("product_amount").value = formatNumber(document.getElementById("product_total").value);
        document.getElementById("addon_amount").value = formatNumber(document.getElementById("addon_total").value);
        document.getElementById("fee_amount").value = formatNumber(document.getElementById("fee_total").value);
        document.getElementById("tax_amount").value = formatNumber("0.00");
      }
    }

  }
  else
  {
    document.getElementById("msdTotalDisplay").innerHTML    = formatNumber(document.getElementById("payment_product_amount_" + paymentSeq).value);
    document.getElementById("addOnTotalDisplay").innerHTML  = formatNumber(document.getElementById("payment_addon_amount_" + paymentSeq).value);
    document.getElementById("feeTotalDisplay").innerHTML    = formatNumber(document.getElementById("payment_fee_amount_" + paymentSeq).value);
    document.getElementById("taxTotalDisplay").innerHTML    = formatNumber(document.getElementById("payment_tax_amount_" + paymentSeq).value);

    document.getElementById("partial_product_total").value  = document.getElementById("payment_product_amount_" + paymentSeq).value;
    document.getElementById("partial_addon_total").value    = document.getElementById("payment_addon_amount_" + paymentSeq).value;
    document.getElementById("partial_fee_total").value      = document.getElementById("payment_fee_amount_" + paymentSeq).value;
    document.getElementById("partial_tax_total").value      = document.getElementById("payment_tax_amount_" + paymentSeq).value;

    if( (form.refundDisp.options[form.refundDisp.selectedIndex].id)=='B' ||  (form.refundDisp.options[form.refundDisp.selectedIndex].id)=='C'  )
    {
      var bValueSelected = form.refundDisp.options[form.refundDisp.selectedIndex].value;
      if (bValueSelected == 'B20' || bValueSelected == 'C30')
      {
        document.getElementById("product_amount").value = formatNumber(document.getElementById("payment_product_amount_" + paymentSeq).value);
        document.getElementById("addon_amount").value = formatNumber(document.getElementById("payment_addon_amount_" + paymentSeq).value);
        document.getElementById("fee_amount").value = formatNumber(document.getElementById("payment_fee_amount_" + paymentSeq).value);
        document.getElementById("tax_amount").value = formatNumber("0.00");
      }
    }
  }

}

function complaintOriginChange(){
  document.RefundForm.change_flag.value='Y';
  if(document.getElementById("complaint_comm_origin_type_id").value == 'NC')
  {
    document.getElementById("complaint_comm_notification_type_id").value = 'NC';
  }
}

function complaintNotificationChange(){
  document.RefundForm.change_flag.value='Y';
  if(document.getElementById("complaint_comm_notification_type_id").value == 'NC')
  {
    document.getElementById("complaint_comm_origin_type_id").value = 'NC';
  }
}

function complaintOriginChangeDetail(index)
{
  <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
    document.getElementById("refund_edit_flag_" + index).value="Y";
    document.RefundForm.update.disabled=false;
    document.RefundForm.change_flag.value="Y";
  </xsl:if>
  if(document.getElementById("complaint_comm_origin_type_id_"+index).value == 'NC')
  {
    document.getElementById("complaint_comm_notification_type_id_"+index).value = 'NC';
  }
}
function complaintNotificationChangeDetail(index)
{
  <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
    document.getElementById("refund_edit_flag_" + index).value="Y";
    document.RefundForm.update.disabled=false;
    document.RefundForm.change_flag.value="Y";
  </xsl:if>
  document.RefundForm.change_flag.value="Y";
  if(document.getElementById("complaint_comm_notification_type_id_"+index).value == 'NC')
  {
    document.getElementById("complaint_comm_origin_type_id_"+index).value = 'NC';
  }
}

function showComplaintCommFields() {
  if (document.RefundForm.preferred_partner.value == "Y")
  {
    document.RefundForm.complaint_comm_origin_type_id.disabled=true;
    document.RefundForm.complaint_comm_notification_type_id.disabled=true;
    originLabel = document.getElementById("origin_label");
    notificationLabel = document.getElementById("notification_label");
    originLabel.style.visibility="hidden";
    notificationLabel.style.visibility="hidden";
    document.RefundForm.complaint_comm_origin_type_id.style.visibility="hidden";
    document.RefundForm.complaint_comm_notification_type_id.style.visibility="hidden";

    valueOfDisp = document.RefundForm.refundDisp.options[document.RefundForm.refundDisp.selectedIndex].value;
    <xsl:for-each select="/root/RefundDispositions/Disposition">
      if("<xsl:value-of select="refund_disp_code"/>" == valueOfDisp)
      {
        if("<xsl:value-of select="complaint_comm_type_req_flag"/>"=="Y")
        {
          document.RefundForm.complaint_comm_origin_type_id.disabled=false;
          document.RefundForm.complaint_comm_notification_type_id.disabled=false;
          originLabel.style.visibility="visible";
          notificationLabel.style.visibility="visible";
          document.RefundForm.complaint_comm_origin_type_id.style.visibility="visible";
          document.RefundForm.complaint_comm_notification_type_id.style.visibility="visible";
        }
      }
    </xsl:for-each>
  }
}

function showRefundComplaintCommDetail()
{
  if (document.RefundForm.preferred_partner.value == "Y")
  {
    var refundCount=0;
    <xsl:for-each select="root/Payments/PaymentVO/RefundVO">
            refundCount = refundCount+1;
    </xsl:for-each>

    for(i=refundCount; i>=1; i--){

      refundErrorDiv = document.getElementById("refundErrorDiv"+i);
      refundErrorDiv.style.display = "none";
      refundComplaintCommDetail = document.getElementById("complaintCommDetail"+i);
      refundComplaintCommDetail.style.display="none";
      refundDisposition = document.getElementById("refund_disp_" + i);
      valueOfDisp = refundDisposition.options[refundDisposition.selectedIndex].value;

      <xsl:for-each select="/root/RefundDispositions/Disposition">
      if("<xsl:value-of select="refund_disp_code"/>" == valueOfDisp)
      {
        if("<xsl:value-of select="complaint_comm_type_req_flag"/>"=="Y")
        {
          refundComplaintCommDetail.style.display="inline";
          refundErrorDiv.style.display = "inline";
        }
      }
      </xsl:for-each>
    }
  }
}

function init(){
  setNavigationHandlers();
  setRefundsIndexes();
  displayReturnToRefundReview();
  var form = document.RefundForm;
  if("<xsl:value-of select="key('pagedata','refund_avail_flag')/value"/>"=="N"){
    document.RefundForm.refundDisp.disabled="true";
  }

  if("<xsl:value-of select="key('pagedata','exit_warning_flag')/value"/>"=="Y"){
    document.RefundForm.change_flag.value="Y";
  }

  var milesPointsFlag = "<xsl:value-of select="$milesPointsFlag"/>";
  if (milesPointsFlag == 'Y')
    document.getElementById("milesPoints").style.display = "block";


  document.RefundForm.postRefund.disabled="true";
  document.RefundForm.deleteRefund.disabled="true";
  document.RefundForm.update.disabled="true";
  document.RefundForm.refundPercent.disabled="true";
  document.RefundForm.fullRefund.disabled="true";
  document.RefundForm.product_amount.readOnly=true;
  document.RefundForm.addon_amount.readOnly=true;
  document.RefundForm.fee_amount.readOnly=true;
  document.RefundForm.tax_amount.readOnly=true;
  document.RefundForm.responsibleParty.disabled="true";
  document.RefundForm.payType.disabled="true";
  document.RefundForm.payTypeText.style.display="none";

  if( (form.refundDisp.options[form.refundDisp.selectedIndex].id)=='B' ||
      (form.refundDisp.options[form.refundDisp.selectedIndex].id)=='C'
    )
  {
    form.responsibleParty.disabled=false;
    form.payType.disabled=false;
    determinePayTypeDisplay();

    document.RefundForm.product_amount.readOnly=false;
    document.RefundForm.addon_amount.readOnly=false;
    document.RefundForm.fee_amount.readOnly=false;
    document.RefundForm.tax_amount.readOnly=false;
    document.RefundForm.postRefund.disabled=false;
    document.RefundForm.refresh.disabled=false;
    document.RefundForm.mercury.disabled=false;
    document.RefundForm.orderComments.disabled=false;
    document.RefundForm.refundPercent.disabled=false;
    document.RefundForm.fullRefund.disabled=false;

    var x=document.getElementById('tblTopSection').rows;
    var y=x[0].cells;
    y[2].innerHTML="Responsible Party";
    y[3].innerHTML="Payment Type";

    setTempSelect();
    <xsl:for-each select="/root/RefundDispositions/Disposition[refund_disp_code=$sRefundDisp]">
      if("<xsl:value-of select="full_refund_indicator"/>"=="Y"){
        fullRefund();
      }
    </xsl:for-each>
  }


  if((form.refundDisp.options[form.refundDisp.selectedIndex].id)=='A'){
    viewMercury.style.display="none";
    form.payType.style.display="none";
    form.responsibleParty.style.display="none";
    var x=document.getElementById('tblTopSection').rows;
    var y=x[0].cells;
    y[2].innerHTML="";
    y[3].innerHTML="";

    document.RefundForm.product_amount.readOnly=true;
    document.RefundForm.addon_amount.readOnly=true;
    document.RefundForm.fee_amount.readOnly=true;
    document.RefundForm.tax_amount.readOnly=true;
    document.RefundForm.postRefund.disabled=false;
    document.RefundForm.refresh.disabled=false;
    document.RefundForm.mercury.disabled=false;
    document.RefundForm.orderComments.disabled=false;
    document.RefundForm.refundPercent.disabled=false;
    document.RefundForm.fullRefund.disabled=false;
    setTempSelect();
    <xsl:for-each select="/root/RefundDispositions/Disposition[refund_disp_code=$sRefundDisp]">
      if("<xsl:value-of select="full_refund_indicator"/>"=="Y"){
        fullRefund();
      }
    </xsl:for-each>
  }

  if("<xsl:value-of select="key('pagedata','modify_order_flag')/value"/>"=="Y"){
    document.RefundForm.postRefund.disabled=false;
    document.RefundForm.refresh.disabled=false;
    document.RefundForm.mercury.disabled=false;
    document.RefundForm.orderComments.disabled=false;
    document.RefundForm.refundPercent.disabled=false;
    document.RefundForm.fullRefund.disabled=false;
    document.RefundForm.product_amount.readOnly=false;
    document.RefundForm.addon_amount.readOnly=false;
    document.RefundForm.fee_amount.readOnly=false;
    document.RefundForm.tax_amount.readOnly=false;
    document.RefundForm.responsibleParty.style.display="none";
    document.RefundForm.payType.disabled=false;
    determinePayTypeDisplay();
    viewMercury.style.display="none";
    var x=document.getElementById('tblTopSection').rows;
    var y=x[0].cells;
    y[2].innerHTML="";
    <xsl:for-each select="/root/RefundDispositions/Disposition[refund_disp_code=$c_disposition_id]">
      if("<xsl:value-of select="full_refund_indicator"/>"=="Y"){
        fullRefund();
      }
    </xsl:for-each>
  }

  if("<xsl:value-of select="key('pagedata','refund_amount_exceeded_error_flag')/value"/>"=="Y"){
    document.RefundForm.change_flag.value="Y";
    document.RefundForm.errorRefundAmt.style.display="inline";
  }

  if("<xsl:value-of select="key('pagedata','refund_amount_zero_error_flag')/value"/>"=="Y"){
    document.RefundForm.change_flag.value="Y";
    document.RefundForm.errorRefundAmtZero.style.display="inline";
  }
  
    if("<xsl:value-of select="key('pagedata','error_gift_certificate_call')/value"/>"=="Y"){
    document.RefundForm.change_flag.value="Y";
    document.RefundForm.errorGiftCerviceCall.style.display="inline";
  }

  if("<xsl:value-of select="key('pagedata','tax_only_error_flag')/value"/>"=="Y"){
    document.RefundForm.change_flag.value="Y";
    document.RefundForm.errorTaxOnly.style.display="inline";
  }

  if("<xsl:value-of select="key('pagedata', 'edit_tax_flag')/value"/>"=="Y"){
    document.RefundForm.tax_amount.style.display="inline";
  }

  setTempSelect();

  if("<xsl:value-of select="key('pagedata','refund_avail_flag')/value"/>"=="N"){
    document.RefundForm.postRefund.disabled="true";
    document.RefundForm.deleteRefund.disabled="true";
    document.RefundForm.update.disabled="true";
    document.RefundForm.refundPercent.disabled="true";
    document.RefundForm.fullRefund.disabled="true";
    document.RefundForm.product_amount.readOnly=true;
    document.RefundForm.addon_amount.readOnly=true;
    document.RefundForm.fee_amount.readOnly=true;
    document.RefundForm.tax_amount.readOnly=true;
    document.RefundForm.responsibleParty.disabled="true";
    document.RefundForm.payType.disabled="true";
    document.RefundForm.payTypeText.style.display="none";
  }

   <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
      document.RefundForm.lockError.style.display="inline";
      document.RefundForm.postRefund.disabled="true";
   </xsl:if>

    // Set dropdown to prev value
  <xsl:for-each select="/root/Payments/PaymentVO">
    <xsl:sort select="paymentSeqId"/>
    <xsl:variable name="sPayType" select="key('pagedata', 'payType')/value"/>
    <xsl:choose>
      <xsl:when test="$sPayType=paymentSeqId">
        form.payType.selectedIndex=<xsl:value-of select="paymentSeqId"/>;
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>

  // Update display value
  payTypeChange();
        showComplaintCommFields();
        showRefundComplaintCommDetail();


} //end init

function fullRefundClick(){
  document.RefundForm.full_refund_check.value="F";
  document.RefundForm.product_amount.value=document.RefundForm.partial_product_total.value;
  document.RefundForm.addon_amount.value=document.RefundForm.partial_addon_total.value;
  document.RefundForm.fee_amount.value=document.RefundForm.partial_fee_total.value;
  // document.RefundForm.tax_amount.value=document.RefundForm.partial_tax_total.value;

  document.RefundForm.change_flag.value="Y";
}

<![CDATA[

/*************************************************************************************
* Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
  var form = document.RefundForm;
  if(form.change_flag.value=="Y"){
    var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
      if (exitConfirmed==true){
        document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        var url = "customerOrderSearch.do?action=customer_account_search&order_number=" + form.external_order_number.value +"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        form.action = url;
          form.submit();
      }
  }else{
    document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    var url = "customerOrderSearch.do?action=customer_account_search&order_number=" + form.external_order_number.value +"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    form.action = url;
    form.submit();
  }
}


function openPopup(){
  var form = document.RefundForm;

  if(form.payType.value == '0' && form.payDisplay=='2')
  {
    alert("You must select a specific payment type before using the percentage calculator");
    return true;
  }

  var modal_dim = "dialogWidth:15; dialogHeight:20; center:yes; status:0; help:no; scroll:no";
//  var popURL = "loadRefunds.do?action_type=PercentPopup&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
  var popURL = "refundsPercentage.htm";
  var totals=showModalDialogIFrame(popURL, "", modal_dim);
  if (totals && totals != null){
    form.msd_incoming_amount.value=totals[0];
    form.addon_incoming_amount.value=totals[1];
    form.fee_incoming_amount.value=totals[2];
    form.refund_type.value="P";
    form.action="loadRefunds.do";
    form.action_type.value="";
    form.submit();
  }
}

/*************************************************************************************
* Perorms the Back to Refund Review button functionality
**************************************************************************************/
function doRefundReview()
{
  var form = document.RefundForm;
  //Added To retain Refund review Sort filters.
  var prev_sort_column =document.RefundForm.prev_sort_column.value;
  var prev_sort_status =document.RefundForm.prev_sort_status.value;
  var prev_current_sort_status =document.RefundForm.prev_current_sort_status.value;
  var prev_is_from_header =document.RefundForm.prev_is_from_header.value;
  var prev_userId_filter =document.RefundForm.prev_userId_filter.value;
  var prev_groupId_filter =document.RefundForm.prev_groupId_filter.value;
  var prev_codes_filter =document.RefundForm.prev_codes_filter.value;
  
  //Build Sort parameters.
  var refund_review_sort_params ="&sort_column="+prev_sort_column+"&sort_status="+prev_sort_status+"&current_sort_status="+prev_current_sort_status+"&is_from_header="+prev_is_from_header+"&userId_filter="+prev_userId_filter+"&groupId_filter="+prev_groupId_filter+"&codes_filter="+prev_codes_filter;
  
  if(form.change_flag.value=="Y"){
    var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
      if (exitConfirmed==true){
        document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        var url = "loadRefunds.do?action_type=RefundReview&page_position=" + document.RefundForm.prev_page_position.value + "&adminAction=customerService&securitytoken=" + form.securitytoken.value + "&context="+ form.context.value + "&start_origin=" + form.start_origin.value+refund_review_sort_params;
        form.action = url;
        form.submit();
      }
  }else{
    document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    var url = "loadRefunds.do?action_type=RefundReview&page_position=" + document.RefundForm.prev_page_position.value + "&adminAction=customerService&securitytoken=" + form.securitytoken.value + "&context="+ form.context.value + "&start_origin=" + form.start_origin.value+refund_review_sort_params;
    form.action = url;
    form.submit();
  }
}

function openEmail() {

  if(document.getElementById("msg_email_indicator").value == "true") {
    var form = document.RefundForm;
    var url = "loadSendMessage.do?message_type=Email&action_type=load_titles&external_order_number=" + document.getElementById("external_order_number").value;
      form.action = url;
    form.submit();
  } else {
    alert("There is no email address on file for this customer.");
  }
}

function mainMenuClick(){
  var form=document.RefundForm;
  if(form.change_flag.value=="Y"){
    var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
    if (exitConfirmed==true){
      document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
      doMainMenuAction();
    }
  }else{
    document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    doMainMenuAction();
  }
}

function mercuryLinkClick(){
  var form=document.RefundForm;
  var modal_dim = "dialogWidth:60; dialogHeight:40; center:yes; status:0; help:no; scroll:no";
  var popURL="displayCommunication.do?read_only_flag=Y&order_detail_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
  showModalDialogIFrame(popURL, "", modal_dim);
}

function mercuryClick(){
  var form = document.RefundForm;
  if(form.change_flag.value=="Y"){
    var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
      if (exitConfirmed==true){
        document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        var url="displayCommunication.do?action_type=display_communication&read_only_flag=N&order_detail_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        form.action = url;
        form.submit();
      }
  }else{
    document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    var url="displayCommunication.do?action_type=display_communication&read_only_flag=N&order_detail_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    form.action = url;
    form.submit();
  }
}

function orderCommentsClick(){
  var form = document.RefundForm;
  if(form.change_flag.value=="Y"){
    var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
      if (exitConfirmed==true){
        document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        var url="loadComments.do?comment_type=Order&page_position=1&order_detail_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
        form.action = url;
        form.submit();
      }
  }else{
    document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=PAYMENTS&lock_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    var url="loadComments.do?comment_type=Order&page_position=1&order_detail_id="+form.order_detail_id.value+"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
    form.action = url;
    form.submit();
  }
}

function processReinstate()
{
  var form = document.RefundForm;
  var url="updateRefund.do?gc_coupon_number=" + form.current_reinstate_gc_number.value;
  form.action_type.value="reinstate_gc";
  form.action = url;
  form.submit();
}

function validateReinstate(gcCouponNumber)
{
  var form = document.RefundForm;
  form.current_reinstate_gc_number.value = gcCouponNumber;
  document.getElementById("reinstateFrame").src="updateRefund.do?action_type=validate_reinstate&gc_coupon_number=" + gcCouponNumber + getSecurityParams(false);
}




function showFeesOnMouseOver()
{
txt = getFeesForMouseOver();

if(document.getElementById) // Netscape 6.0+ and Internet Explorer 5.0+
{
elm=document.getElementById("refundToolTip") ;
elm.innerHTML=txt;
if(txt == "hidden"){ hideFeesMouseOver(); }
else { elm.style.visibility = "visible" ; }
}

}

function hideFeesMouseOver(){

if(document.getElementById) // Netscape 6.0+ and Internet Explorer 5.0+
{
elm.style.visibility="hidden" ;
}

}





/* JS code for OnMouseOver function based on FuelSurchargeRepurpose Changes */
  function getFeesForMouseOver()
  {

  var serviceFee = document.getElementById('serviceFee').innerHTML;
  var sameDayUpcharge = document.getElementById('sameDayUpcharge').innerHTML;
  var shippingFee= document.getElementById('shippingFee').innerHTML;
  var morningDeliveryFee = document.getElementById('morningDeliveryFee').innerHTML;
  var satUpchargeFee = document.getElementById('satUpchargeFee').innerHTML;
  var alHISurchargeFee= document.getElementById('alHISurchargeFee').innerHTML;
  var fuelSurchargeFee = document.getElementById('fuelSurchargeFee').innerHTML;
  var fuelSurchargeDescription = document.getElementById('fuelSurchargeDescription').innerHTML;
  var applySurchargeCode = document.getElementById('applySurchargeCode').innerHTML;
  var refundApplied = document.getElementById('refundApplied').innerHTML;;
  
  // DI-27: Refund - Show new fees broken out in Mouseovers
  var sunUpchargeFee = document.getElementById('sunUpchargeFee').innerHTML;
  var monUpchargeFee = document.getElementById('monUpchargeFee').innerHTML;
  var lateCutoffFee = document.getElementById('lateCutoffFee').innerHTML;
  var internationalFee = document.getElementById('internationalFee').innerHTML;

  var serviceFeeString = "" ;
  var sameDayUpchargeString = "" ;
  var shippingFeeString = "" ;
  var morningDeliveryFeeString = "" ;
  var alHISurchargeFeeString = "";
  var satUpchargeFeeString = "" ;
  var fuelSurchargeFeeString = "" ;
  var mouseOverString  = "" ;
  // DI-27: Refund - Show new fees broken out in Mouseovers
  var sunUpchargeFeeString = "" ;
  var monUpchargeFeeString = "" ;
  var lateCutoffFeeString = "" ;
  var internationalFeeString ="";

 if (refundApplied == "Y")
 {
         mouseOverString  =   "Cannot display fee breakdown after refund has been applied." ;
 }
 else
 {

        if ( (!IsEmpty(fuelSurchargeFee)) && fuelSurchargeFee > 0 && applySurchargeCode == 'ON' )
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee- fuelSurchargeFee;
              fuelSurchargeFeeString = "\n" + fuelSurchargeDescription + ": " +  fuelSurchargeFee+"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - fuelSurchargeFee;
              fuelSurchargeFeeString = "\n" + fuelSurchargeDescription + ": " +  fuelSurchargeFee+"<BR>";
          }
        }

        if ((!IsEmpty(alHISurchargeFee)) && alHISurchargeFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee - alHISurchargeFee;
              alHISurchargeFeeString = "Alaska/Hawaii Upcharge: " +  alHISurchargeFee +"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - alHISurchargeFee;
              alHISurchargeFeeString = "Alaska/Hawaii Upcharge: " +  alHISurchargeFee +"<BR>";
          }
        }


        if ( (!IsEmpty(satUpchargeFee)) && satUpchargeFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee - satUpchargeFee;
              satUpchargeFeeString = "Saturday Upcharge: " +  satUpchargeFee +"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - satUpchargeFee;
              satUpchargeFeeString = "Saturday Upcharge: " +  satUpchargeFee +"<BR>";
          }
        }

        if ( (!IsEmpty(sameDayUpcharge)) && sameDayUpcharge > 0)
        {
          if (serviceFee > 0)
          {
              serviceFee = serviceFee - sameDayUpcharge;
              sameDayUpchargeString = "Same Day Fee: " + sameDayUpcharge +"<BR>";
          }
        }
		
		if ( (!IsEmpty(morningDeliveryFee)) && morningDeliveryFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee- morningDeliveryFee;
              morningDeliveryFeeString = "\n" + "Morning Delivery: " +  morningDeliveryFee+"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - morningDeliveryFee;
              morningDeliveryFeeString = "\n" + "Morning Delivery: " +  morningDeliveryFee+"<BR>";
          }
        }
        
        //DI-27: Refund - Show new fees broken out in Mouseovers
        
        if ( (!IsEmpty(sunUpchargeFee)) && sunUpchargeFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee - sunUpchargeFee;
              sunUpchargeFeeString = "Sunday Upcharge: " +  sunUpchargeFee +"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - sunUpchargeFee;
              sunUpchargeFeeString = "Sunday Upcharge: " +  sunUpchargeFee +"<BR>";
          }
        }
        
        if ( (!IsEmpty(monUpchargeFee)) && monUpchargeFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee - monUpchargeFee;
              monUpchargeFeeString = "Monday Upcharge: " +  monUpchargeFee +"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - monUpchargeFee;
              monUpchargeFeeString = "Monday Upcharge: " +  monUpchargeFee +"<BR>";
          }
        }
        
        if ( (!IsEmpty(lateCutoffFee)) && lateCutoffFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee - lateCutoffFee;
              lateCutoffFeeString = "Late Cutoff: " +  lateCutoffFee +"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - lateCutoffFee;
              lateCutoffFeeString = "Late Cutoff: " +  lateCutoffFee +"<BR>";
          }
        }
        
           if((!IsEmpty(internationalFee)) && internationalFee > 0)
        {
          if (shippingFee > 0)
          {
              shippingFee = shippingFee - internationalFee;
              internationalFeeString = "International Fee: " +  internationalFee +"<BR>";
          }
          else if (serviceFee > 0)
          {
              serviceFee = serviceFee - internationalFee;
              internationalFeeString = "International Fee: " +  internationalFee +"<BR>";
          }
        }
        
        serviceFee  = Math.round(serviceFee*100)/100;
        shippingFee = Math.round(shippingFee*100)/100;

        if ( (!IsEmpty(serviceFee)) &&  serviceFee > 0 )
         {
          serviceFeeString = "Service Fee: " + serviceFee +"<BR>";
         }

        if ( (!IsEmpty(shippingFee)) &&  shippingFee > 0 )
         {
          shippingFeeString = "Shipping Fee: " +  shippingFee +"<BR>";
         }

       mouseOverString  =   serviceFeeString + sameDayUpchargeString + shippingFeeString + satUpchargeFeeString + alHISurchargeFeeString + fuelSurchargeFeeString + morningDeliveryFeeString + lateCutoffFeeString + sunUpchargeFeeString + monUpchargeFeeString + internationalFeeString;
       if (mouseOverString == "")
       {
           //mouseOverString = "Does not include configurable fee" ;
           mouseOverString = "hidden";
       }

  }

  return mouseOverString ;

  }

/**************************************************************
* IsEmpty()
**************************************************************/
function IsEmpty(feeVal) {
var feeValString = String(feeVal);

   if (isNaN(feeValString) || (feeValString==null) || (trim(feeValString)=='') ) {
      return true;
   }
   else { return false; }
}


/**************************************************************
* trim()
**************************************************************/
function trim(str)
{
  str=str.replace(/^\s*(.*)/, "$1");
  str=str.replace(/(.*?)\s*$/, "$1");
  return str;
}


]]>


   </script>
   </head>

<body onload="init();">
  <div id="innerContent" >
    <form name="RefundForm" method="post" action="RefundServlet">
      <xsl:call-template name="securityanddata"/>
      <xsl:call-template name="decisionResultData"/>
      <input type="hidden" name="acceptPercentageDiscount" id="acceptPercentageDiscount"/>
      <input type="hidden" name="action_type" id="action_type" value=""/>
      <input type="hidden" name="refund_type" id="refund_type" value=""/>
      <input type="hidden" name="full_refund_check" id="full_refund_check" value=""/>
      <input type="hidden" name="tempSelect" id="tempSelect" value=""/>
      <input type="hidden" name="msd_incoming_amount" id="msd_incoming_amount" value=""/>
      <input type="hidden" name="addon_incoming_amount" id="addon_incoming_amount" value=""/>
      <input type="hidden" name="fee_incoming_amount" id="fee_incoming_amount" value=""/>
      <input type="hidden" name="giveFullRefund" id="giveFullRefund" value=""/>
      <input type="hidden" name="pay_display" id="pay_display" value=""/>
      <input type="hidden" name="disp_code" id="disp_code" value="{key('pagedata', 'disposition_id')/value}"/>
      <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pagedata', 'external_order_number')/value}"/>
      <input type="hidden" name="vendor_flag" id="vendor_flag" value="{key('pagedata', 'vendor_flag')/value}"/>
      <input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>
      <input type="hidden" name="responsible_party" id="responsible_party" value=""/>
      <input type="hidden" name="payment_type_seq" id="payment_type_seq" value=""/>

      <input type="hidden" name="product_total" id="product_total" value="{format-number(key('remaining_totals','msdTotal')/value, '#,###,##0.00')}"/>
      <input type="hidden" name="addon_total" id="addon_total" value="{format-number(key('remaining_totals','addOnTotal')/value, '#,###,##0.00')}"/>
      <input type="hidden" name="fee_total" id="fee_total" value="{format-number(key('remaining_totals','feeTotal')/value, '#,###,##0.00')}"/>
      <input type="hidden" name="tax_total" id="tax_total" value="{format-number(key('remaining_totals','taxTotal')/value, '#,###,##0.00')}"/>

      <input type="hidden" name="partial_product_total" id="partial_product_total" value="{key('remaining_totals','msdTotal')/value}"/>
      <input type="hidden" name="partial_addon_total" id="partial_addon_total" value="{key('remaining_totals','addOnTotal')/value}"/>
      <input type="hidden" name="partial_fee_total" id="partial_fee_total" value="{key('remaining_totals','feeTotal')/value}"/>
      <input type="hidden" name="partial_tax_total" id="partial_tax_total" value="{key('remaining_totals','taxTotal')/value}"/>

      <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata','order_detail_id')/value}"/>
      <input type="hidden" name="origin_id" id="origin_id" value="{key('pagedata','origin_id')/value}"/>
      <input type="hidden" name="refund_count" id="refund_count" value="{count(root/Payments/PaymentVO/RefundVO)}"/>
      <input type="hidden" name="gc_flag" id="gc_flag" value="{key('pagedata','gc_flag')/value}"/>
      <input type="hidden" name="a_type_flag" id="a_type_flag" value="{key('pagedata','a_type_flag')/value}"/>
      <input type="hidden" name="read_only_flag" id="read_only_flag" value="Y"/>
      <input type="hidden" name="change_flag" id="change_flag" value="N"/>
      <input type="hidden" name="current_reinstate_gc_number" id="current_reinstate_gc_number"/>
      <input type="hidden" name="is_gc_valid" id="is_gc_valid"/>
      <input type="hidden" name="invalid_gc_message" id="invalid_gc_message"/>
      <input type="hidden" name="gc_amount" id="gc_amount"/>
            <input type="hidden" name="gd_or_gc" id="gd_or_gc" value="GD"/>
            <input type="hidden" name="giftCardPaymentEnabled" id="giftCardPaymentEnabled" value="{/root/GIFTCARD_SETTINGS/gift_card_payment_enabled}"/>
      <input type="hidden" name="C30_full_refund" id="C30_full_refund"/>
      <input type="hidden" name="product_type" id="product_type" value="{key('pagedata', 'product_type')/value}"/>
      <input type="hidden" name="ship_method" id="ship_method" value="{key('pagedata', 'ship_method')/value}"/>
      <input type="hidden" name="vendor_flag" id="vendor_flag" value="{key('pagedata', 'vendor_flag')/value}"/>
      <input type="hidden" name="order_guid" id="order_guid" value="{key('pagedata', 'order_guid')/value}"/>
                  <input type="hidden" name="preferred_partner" id="preferred_partner" value="{key('pagedata', 'preferred_partner')/value}"/>
      <input type="hidden" name="gc_amount_remainder" id="gc_amount_remainder"/>
      <input type="hidden" name="aafes_ticket_number" id="aafes_ticket_number"/>
      <input type="hidden" name="doAutoCan" id="doAutoCan" value=""/>
      <input type="hidden" name="doAutoAskP" id="doAutoAskP" value=""/>
      <input type="hidden" name="autoReasonText" id="autoReasonText" value=""/>
      <input type="hidden" name="autoAskPPrice" id="autoAskPPrice" value=""/>
      <input type="hidden" name="venus_flag" id="venus_flag" value="{/root/OrderFulfillment/Fulfill/vendor_flag}"/>
      <input type="hidden" name="mercury_id" id="mercury_id" value="{/root/OrderFulfillment/Fulfill/mercury_id}"/>
      <input type="hidden" name="in_transit" id="in_transit" value="{key('pagedata', 'in_transit')/value}"/>
      <input type="hidden" name="exceededRefundMaxDaysOut" id="exceededRefundMaxDaysOut" value="{key('pagedata', 'exceededRefundMaxDaysOut')/value}"/>
      <input type="hidden" name="miles_points_flag"       id="miles_points_flag"      value="{$milesPointsFlag}"/>
      <input type="hidden" name="mp_redemption_rate_amt"  id="mp_redemption_rate_amt" value="{key('pagedata', 'mp_redemption_rate_amt')/value}"/>

      <input type="hidden" name="company_id"            id="company_id"              value="{key('pagedata', 'company_id')/value}"/>
      <input type="hidden" name="recipient_state"       id="recipient_state"         value="{key('pagedata', 'recipient_state')/value}"/>
      <input type="hidden" name="recipient_country"     id="recipient_country"       value="{key('pagedata', 'recipient_country')/value}"/>
      <input type="hidden" name="msg_email_indicator"   id="msg_email_indicator"     value="{$msg_email_indicator}"/>
	
	  <input type="hidden" name="fullyRefundMechAmtFlag"     id="fullyRefundMechAmtFlag"       value="{key('pagedata', 'fullyRefundMechAmtFlag')/value}"/>	

      <iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
      <iframe name="reinstateFrame" id="reinstateFrame" height="0" width="0" border="0"></iframe>

        <xsl:call-template name="header">
          <xsl:with-param name="headerName"  select="'Refunds'" />
          <xsl:with-param name="showTime" select="true()"/>
          <xsl:with-param name="showBackButton" select="true()"/>
          <xsl:with-param name="showPrinter" select="true()"/>
          <xsl:with-param name="showSearchBox" select="false()"/>
          <xsl:with-param name="dnisNumber" select="$call_dnis"/>
          <xsl:with-param name="cservNumber" select="$call_cs_number"/>
          <xsl:with-param name="indicator" select="$call_type_flag"/>
          <xsl:with-param name="brandName" select="$call_brand_name"/>
          <xsl:with-param name="showCSRIDs" select="true()"/>
          <xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
          <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
        </xsl:call-template>
	  <table width="96%" border="0" align="center" cellpadding="2" cellspacing="0">
		  <tr>
			 <td width="25%" class="label">Order Number:&nbsp;
				 <a href="javascript:doBackAction();">
					<xsl:value-of select="key('pagedata', 'external_order_number')/value"/>
				 </a>
			 </td>
		  </tr>
	  </table>
  
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <input name="lockError" id="lockError" type="text" value="   Current record is locked by {/root/out-parameters/OUT_LOCKED_CSR_ID}" style="border:0px solid #000000; color:red; width:500; display:none; font:bold"/>
          </td>
        </tr>
        <tr>
          <td colspan="4">
            <table width="98%" border="3" align="center" rules="none" cellpadding="1" cellspacing="1" bordercolor="#006699">
              <tr><td colspan="4"><input name="errorTaxOnly" id="errorTaxOnly" type="text" value="Tax is calculated automatically.  Please enter tax only if refund is just for tax." style="border:0px solid #000000; color:red; width:500; display:none; font:bold"/></td></tr>
              <tr><td colspan="4"><input name="errorRefundAmt" id="errorRefundAmt" type="text" value="Refunded amounts cannot exceed remaining balance." style="border:0px solid #000000; color:red; width:500; display:none; font:bold"/></td></tr>
              <tr><td colspan="4"><input name="errorRefundAmtZero" id="errorRefundAmtZero" type="text" value="Refunded amounts must be greater than zero." style="border:0px solid #000000; color:red; width:500; display:none; font:bold"/></td></tr>
              <tr><td colspan="4"><input name="errorGiftCerviceCall" id="errorRefundAmtZero" type="text" value="Error encountered when calling Gift Code Service.  Please try again." style="border:0px solid #000000; color:red; width:500; display:none; font:bold"/></td></tr>
              <tr>
                <td colspan="5">
                  <table id="tblTopSection" width="100%" border="0">
                    <tr>
                      <td width="1%">&nbsp;</td>
                      <td align="left" class="label" width="21%">Refund Disposition</td>
                      <td align="left" class="label" width="12%">Responsible Party</td>
                      <td align="left" class="label" width="12%">Payment Type</td>

                                            <td align="left" class="label" width="15%" id="origin_label" style="visibility:hidden">Origin of Refund</td>
                      <td align="left" class="label" width="15%" id="notification_label" style="visibility:hidden">Origin of Refund Notification</td>
                                            <td align="left" class="label" width="15%" id="gc_label" style="display:none">
                        <font color="red">***</font>
                        Gift Card/Certificate Number
                      </td>
                                           <td align="left" class="label" width="9%" id="gd_pin_label" style="display:none">
                        <font color="red">***</font>
                        Pin
                      </td>
                    </tr>
                                        <tr>
                                          <td align="center" colspan="8"><div id="errorDiv" style="color:red; font-weight:bold"></div></td>
                                        </tr>
                    <tr>
                      <td width="1%">&nbsp;</td>
                      <td align="left">
                        <xsl:choose>
                          <xsl:when test="$c_type_flag='N'">
                            <select name="refundDisp" id="refundDisp">
                              <option value="0">-select one-</option>
                              <xsl:for-each select="/root/RefundDispositions/Disposition">
                                  <xsl:choose>
                                      <xsl:when test="refund_disp_code='C30' and $C30RefundFound='Y'">
                                      </xsl:when>
                                      <xsl:otherwise>
                                          //Add full refund indicator (Y/N) to a Map for each refund disposition
                                          <script>refundDispFullRefundIndMap[mhash + '<xsl:value-of select="refund_disp_code"/>']='<xsl:value-of select="full_refund_indicator"/>';</script>
                                      </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:choose>
                                  <xsl:when test="//Payments/PaymentVO/RefundVO">
                                    <xsl:if test="refund_display_type!='A'">
                                      <xsl:choose>
                                        <!-- while looping, if disp=C30, check if a C30 already exist
                                             if so, do not include a C30 in the drop down. -->
                                        <xsl:when test="refund_disp_code='C30' and $C30RefundFound='Y'">
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <xsl:choose>
                                            <xsl:when test="$sRefundDisp=refund_disp_code">
                                              <option id="{refund_display_type}" value="{refund_disp_code}" selected="selected" xsl:use-attribute-sets=""><xsl:value-of select="refund_disp_code"/> - <xsl:value-of select="short_description"/></option>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <option id="{refund_display_type}" value="{refund_disp_code}" xsl:use-attribute-sets=""><xsl:value-of select="refund_disp_code"/> - <xsl:value-of select="short_description"/></option>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:if>
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <xsl:choose>
                                        <xsl:when test="$sRefundDisp=refund_disp_code">
                                          <option id="{refund_display_type}" value="{refund_disp_code}" selected="selected" xsl:use-attribute-sets=""><xsl:value-of select="refund_disp_code"/> - <xsl:value-of select="short_description"/></option>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <script type="text/javascript">
                                            var count;
                                            var c30ToggleFlag;
                                            var paymentMtdId;
                                            var refundC30disp;
                                            count=0;
                                            <xsl:for-each select="/root/Payments/PaymentVO">
                                              <xsl:sort select="paymentSeqId"/>
                                              count=count+1;
                                            </xsl:for-each>
                                            if(count>1){
                                              <xsl:for-each select="/root/Payments/PaymentVO">
                                                paymentMtdId='<xsl:value-of select="paymentMethodId"/>';
                                                if(paymentMtdId=='GD' || paymentMtdId=='GC')
                                                {
                                                c30ToggleFlag = false; //dont populate c30 disp
                                                }
                                              </xsl:for-each>
                                            }else{
                                              <xsl:for-each select="/root/Payments/PaymentVO">
                                                paymentMtdId='<xsl:value-of select="paymentMethodId"/>';
                                                if(paymentMtdId=='GC')
                                                {
                                                c30ToggleFlag = true;
                                                allowOnlyGCRefund = true;
                                                }
                                                if(paymentMtdId=='VI')
                                                {
                                                c30ToggleFlag = true;
                                                }
                                              </xsl:for-each>
                                            }
                                            if(c30ToggleFlag==false){
                                              refundC30disp ='<xsl:value-of select="refund_disp_code"/>';
                                              if(refundC30disp=='C30'){}
                                              else{
                                              document.write('<option id="{refund_display_type}" value="{refund_disp_code}" xsl:use-attribute-sets=""><xsl:value-of select="refund_disp_code"/> - <xsl:value-of select="short_description"/></option>');
                                              }
                                            }
                                            else{
                                              document.write('<option id="{refund_display_type}" value="{refund_disp_code}" xsl:use-attribute-sets=""><xsl:value-of select="refund_disp_code"/> - <xsl:value-of select="short_description"/></option>');
                                            }
                                            </script>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:for-each>
                            </select>
                            &nbsp;<input name="dispSelected" id="dispSelected" accesskey="G" style="width:30" type="button" onclick="changeType()" class="blueButton" value="(G)o"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:if test="$c_type_flag='Y'">
                              <xsl:for-each select="/root/RefundDispositions/Disposition[refund_disp_code=$c_disposition_id]">
                                <xsl:value-of select="refund_disp_code"/> - <xsl:value-of select="short_description"/>
                              </xsl:for-each>
                            </xsl:if>
                          </xsl:otherwise>
                        </xsl:choose>
                      </td>
                      <td align="left">
                        <select onchange="respPartyChange()" disabled="false" name="responsibleParty" id="responsibleParty">
                          <option value="0">-select one-</option>
                          <xsl:for-each select="/root/OrderFlorists/Florist">
                            <xsl:variable name="sRespParty" select="key('pagedata', 'responsibleParty')/value"/>
                            <xsl:choose>
                              <xsl:when test="$sRespParty=responsible_party">
                                <option value="{responsible_party}" selected="selected"><xsl:value-of select="responsible_party"/>&nbsp; &nbsp; <xsl:value-of select="florist_name"/></option>
                              </xsl:when>
                              <xsl:otherwise>
                                <option value="{responsible_party}"><xsl:value-of select="responsible_party"/>&nbsp; &nbsp; <xsl:value-of select="florist_name"/></option>
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:for-each>
                        </select>
                      </td>
                      <td align="left">
                        <select onchange="resetAmounts(); payTypeChange();" name="payType" id="payType">
                          <option value="0">All Payments</option>
                          <xsl:for-each select="/root/Payments/PaymentVO">
                            <xsl:sort select="paymentSeqId"/>
                            <option value="{paymentSeqId}"><xsl:value-of select="paymentSeqId"/>.&nbsp;<xsl:value-of select="paymentMethodDescription"/></option>
                          </xsl:for-each>
                        </select>
                        <input name="payTypeText" id="payTypeText" tabindex="-1" type="text" value="{/root/Payments/PaymentVO/paymentMethodDescription}" style="border:0px solid #000000;"/>



                      <td align="left">
                        <select disabled="true" onchange="complaintOriginChange()" name="complaint_comm_origin_type_id" id="complaint_comm_origin_type_id" style="visibility:hidden">
                                                                                                      <option value="0">-select one-</option>
                                                                                                      <xsl:variable name="complaintCommOriginTypeIdDetail" select="key('pagedata', 'complaint_comm_origin_type_id')/value"/>
                                                                                                        <xsl:for-each select="/root/complaint_comm_type/comm_type">
                                                                                                          <xsl:choose>
                                                                                                              <xsl:when test="complaint_comm_type_id=$complaintCommOriginTypeIdDetail">
                                                                                                                  <option value="{complaint_comm_type_id}" selected="selected"><xsl:value-of select="description"/></option>
                                                                                                             </xsl:when>
                                                                                                             <xsl:otherwise>
                                                                                                                  <option value="{complaint_comm_type_id}" ><xsl:value-of select="description"/></option>
                                                                                                             </xsl:otherwise>
                                                                                                          </xsl:choose>
                                                                                                        </xsl:for-each>
                        </select>
                      </td>
                      <td align="left">
                        <select disabled="true" onchange="complaintNotificationChange()"  name="complaint_comm_notification_type_id" id="complaint_comm_notification_type_id" style="visibility:hidden">
                          <option value="0">-select one-</option>
                                                                                                        <xsl:variable name="complaintCommNotificationTypeIdDetail" select="key('pagedata', 'complaint_comm_notification_type_id')/value"/>
                          <xsl:for-each select="/root/complaint_comm_type/comm_type">
                                                                                                          <xsl:choose>
                                                                                                             <xsl:when test="complaint_comm_type_id=$complaintCommNotificationTypeIdDetail">
                                                                                                                  <option value="{complaint_comm_type_id}" selected="selected"><xsl:value-of select="description"/></option>
                                                                                                             </xsl:when>
                                                                                                             <xsl:otherwise>
                                                                                                                  <option value="{complaint_comm_type_id}" ><xsl:value-of select="description"/></option>
                                                                                                             </xsl:otherwise>
                                                                                                          </xsl:choose>
                          </xsl:for-each>
                        </select>
                      </td>
                                            <td align="left">
                        <input name="gc_number" id="gc_number" tabindex="-1" type="text" size="19" maxlength="19" style="display:none"
                        onblur="javascript:checkGCType('gc_number')"/>
                      </td>
                                            <td align="left">
                        <input tabindex="-1" type="text" name="gd_pin" id="gd_pin" size="4" maxlength="4" onkeydown="" style="display:none"
                             disabled="true"/>
                      </td>
                                            </td>
                                        </tr>
                    <tr>
                      <td colspan="8">
                        <table width="100%" border="0">
                          <tr>
                            <td width="15%"></td>
                            <td width="15%" align="center" class="label">MSD:</td>
                            <td width="15%" align="center" class="label">Add-On:</td>
                            <td width="15%" align="center" class="label" >Service/Shipping:</td>
                            <td width="10%" align="center" class="label">Tax:</td>
                            <td width="15%"></td>
                            <td width="15%"></td>
                            <td width="15%"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td align="center"><div id="msdTotalDisplay"><xsl:value-of select="format-number(key('remaining_totals','msdTotal')/value, '#,###,##0.00')"/></div></td>
                            <td align="center"><div id="addOnTotalDisplay"><xsl:value-of select="format-number(key('remaining_totals','addOnTotal')/value, '#,###,##0.00')"/></div></td>
                            <div id = "serviceFee" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','serviceFee')/value, '#,###,##0.00')"/></div>
                            <div id = "sameDayUpcharge" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','sameDayUpcharge')/value, '#,###,##0.00')"/></div>
                            <div id = "shippingFee" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','shippingFee')/value, '#,###,##0.00')"/></div>
                            <div id = "morningDeliveryFee" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','morningDeliveryFee')/value, '#,###,##0.00')"/></div>
                            <div id = "satUpchargeFee" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','satUpchargeFee')/value, '#,###,##0.00')"/></div>
                            <div id = "alHISurchargeFee" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','alHISurchargeFee')/value, '#,###,##0.00')"/></div>
                            <div id = "fuelSurchargeFee" style="display: none"><xsl:value-of select="format-number(key('remaining_totals','fuelSurchargeFee')/value, '#,###,##0.00')"/></div>
                            <div id = "fuelSurchargeDescription" style="display: none"><xsl:value-of select="key('remaining_totals','fuelSurchargeDescription')/value"/></div>
                            <div id = "applySurchargeCode" style="display: none"><xsl:value-of select="key('remaining_totals','applySurchargeCode')/value"/></div>
                            <div id = "refundApplied" style="display: none"><xsl:value-of select="key('remaining_totals','refundApplied')/value"/></div>
                            <!-- DI-27: Refund - Show new fees broken out in Mouseovers -->
                            <div id = "sunUpchargeFee" style="display: none"><xsl:value-of select="key('remaining_totals','sunUpchargeFee')/value"/></div>
                            <div id = "monUpchargeFee" style="display: none"><xsl:value-of select="key('remaining_totals','monnUpchargeFee')/value"/></div>
                            <div id = "lateCutoffFee" style="display: none"><xsl:value-of select="key('remaining_totals','lateCutoffFee')/value"/></div>
                            <div id = "internationalFee" style="display: none"><xsl:value-of select="key('remaining_totals','internationalFee')/value"/></div>
                            <td align="center" >
                            <table>
                            <tr>
                            <td>
                              <div id="feeTotalDisplay" onmouseover = "javascript:showFeesOnMouseOver();" onmouseout = "javascript:hideFeesMouseOver();" >
                                <xsl:value-of select="format-number(key('remaining_totals','feeTotal')/value, '#,###,##0.00')"/>
                              </div>
                            </td>
                            <td>
                            <div id="refundToolTip" style="white-space:nowrap;position:absolute;visibility:hidden;border:1px solid #3669AC;background-color: #CCEFFF;color:#000000;text-align: left;padding-left:4px;padding-right:4px;font-family: arial,helvetica,sans-serif;font-size: 8pt;text-decoration:none; filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);" >
                            </div>
                            </td>
                            </tr>
                            </table>
                            </td>

                            <td align="center"><div id="taxTotalDisplay"><xsl:value-of select="format-number(key('remaining_totals','taxTotal')/value, '#,###,##0.00')"/></div></td>
                            <td></td>
                            <td class="label" align="right"></td>
                            <td width="15%" align="right"><input name="fullRefund" id="fullRefund" accesskey="F" style="width:90" type="button" onclick="fullRefundClick()" class="blueButton" value="(F)ull Refund"/></td>
                          </tr>
                          <tr>
                            <td align="center" class="label">Enter amounts to be Refunded:</td>
                            <xsl:variable name="msdAmt" select="key('refund_amounts', 'msdAmount')/value"/>
                            <xsl:variable name="addonAmt" select="key('refund_amounts', 'addOnAmount')/value"/>
                            <xsl:variable name="feeAmt" select="key('refund_amounts', 'feeAmount')/value"/>
                            <xsl:variable name="taxAmt" select="key('refund_amounts', 'taxAmount')/value"/>
                            <td align="center"><input name="product_amount" id="product_amount" type="text" size="4" value="{$msdAmt}" style="text-align: right"/></td>
                            <td align="center"><input name="addon_amount" id="addon_amount" type="text" size="4" value="{$addonAmt}" style="text-align: right"/></td>
                            <td align="center"><input name="fee_amount" id="fee_amount" type="text" size="4" value="{$feeAmt}" style="text-align: right"/></td>
                            <td align="center"><input name="tax_amount" id="tax_amount" type="text" size="4" value="{$taxAmt}" style="text-align: right; display:none"/></td>
                            <td align="center" class="link"><a href="javascript:mercuryLinkClick();" name="viewMercury" >VIEW COMMUNICATION</a></td>
                            <td colspan="8" align="right"><input name="refundPercent" id="refundPercent" style="width:90" accesskey="R" type="button" class="blueButton" onclick="openPopup()" value="(R)efund %"/></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td class="TblHeader" align="left" colspan="8">Calculated Totals</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <table width="98%" align="center" border="0">
                    <tr>
                      <td width="1%"></td>
                      <td width="1%"></td>
                      <td width="10%">&nbsp;</td>
                      <td class="label" align="center">Type</td>
                      <td class="label" align="center">
                        <div id="milesPoints" style="display:none">
                          Miles/Points
                        </div>
                      </td>
                      <td class="label" align="center">MSD</td>
                      <td class="label" align="center">Add-On</td>
                      <td class="label" align="center">Service/Shipping</td>
                      <td class="label" align="center">Tax</td>
                      <td class="label" align="center">User-ID</td>
                      <td class="label" align="center">ACT-Date</td>
                      <td class="label" align="center">Disp. Code</td>
                      <td class="label" align="center">Responsible Party</td>
                    </tr>
                    <xsl:for-each select="root/Payments/PaymentVO">
                      <xsl:sort select="paymentSeqId" data-type="number" />
                      <tr>
                        <td align="left" width="1%">
                          <xsl:if test="(paymentType='G') and (gcCouponMultiOrderFlag='Y')">
                            *
                          </xsl:if>
                        </td>
                        <td align="left" width="1%"><xsl:value-of select="paymentSeqId"/>.</td>
                        <xsl:choose>
                          <xsl:when test="(paymentType='C')">
                            <td align="left">************<xsl:value-of select="substring(cardNumber,13,4)"/></td>
                          </xsl:when>
                          <xsl:otherwise>
                            <!--show or get rid of other numbers?-->
                            <td align="left"><xsl:value-of select="cardNumber"/></td>
                          </xsl:otherwise>
                        </xsl:choose>
                        <td align="center"><xsl:value-of select="paymentMethodDescription"/></td>
                        <td align="center">
                          <xsl:if test="$milesPointsFlag = 'Y'">
                            <xsl:value-of select="OrderBillVO/milesPointsAmount"/>
                          </xsl:if>
                        </td>
                        <td align="center"><xsl:value-of select="format-number(OrderBillVO/productAmount, '#,###,##0.00')"/></td>
                        <td align="center"><xsl:value-of select="format-number(OrderBillVO/addOnAmount, '#,###,##0.00')"/></td>
                        <td align="center"  > <xsl:value-of select="format-number(OrderBillVO/feeAmount, '#,###,##0.00')"/></td>
                        <td align="center"><xsl:value-of select="format-number(OrderBillVO/tax, '#,###,##0.00')"/></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <!--for each record, pass in the values-->
                        <input type="hidden" name="payment_method_{paymentSeqId}" id="payment_method_{paymentSeqId}" value="{paymentMethodId}"/>
                        <input type="hidden" name="payment_seq_id_{paymentSeqId}" id="payment_seq_id_{paymentSeqId}" value="{paymentSeqId}"/>
                        <input type="hidden" name="payment_type_{paymentSeqId}" id="payment_type_{paymentSeqId}" value="{paymentType}"/>
                        <input type="hidden" name="payment_card_id_{paymentSeqId}" id="payment_card_id_{paymentSeqId}" value="{cardId}"/>
                        <input type="hidden" name="payment_card_number_{paymentSeqId}" id="payment_card_number_{paymentSeqId}" value="{cardNumber}"/>
                        <input type="hidden" name="payment_product_amount_{paymentSeqId}" id="payment_product_amount_{paymentSeqId}" value="{OrderBillVO/productAmount}"/>
                        <input type="hidden" name="payment_addon_amount_{paymentSeqId}" id="payment_addon_amount_{paymentSeqId}" value="{OrderBillVO/addOnAmount}"/>
                        <input type="hidden" name="payment_fee_amount_{paymentSeqId}" id="payment_fee_amount_{paymentSeqId}" value="{OrderBillVO/feeAmount}"/>
                        <input type="hidden" name="payment_tax_amount_{paymentSeqId}" id="payment_tax_amount_{paymentSeqId}" value="{OrderBillVO/tax}"/>
                        <input type="hidden" name="payment_merchandise_tax_{paymentSeqId}" id="payment_merchandise_tax_{paymentSeqId}" value="{OrderBillVO/merchandiseTax}"/>
                        <input type="hidden" name="payment_shipping_tax_{paymentSeqId}" id="payment_shipping_tax_{paymentSeqId}" value="{OrderBillVO/shippingTax}"/>
                        <input type="hidden" name="payment_service_fee_tax_{paymentSeqId}" id="payment_service_fee_tax_{paymentSeqId}" value="{OrderBillVO/serviceFeeTax}"/>
                        <input type="hidden" name="payment_commission_amount_{paymentSeqId}" id="payment_commission_amount_{paymentSeqId}" value="{OrderBillVO/commissionAmount}"/>
                        <input type="hidden" name="payment_wholesale_amount_{paymentSeqId}" id="payment_wholesale_amount_{paymentSeqId}" value="{OrderBillVO/wholesaleAmount}"/>
                        <input type="hidden" name="payment_discount_{paymentSeqId}" id="payment_discount_{paymentSeqId}" value="{OrderBillVO/discountAmount}"/>
                        <input type="hidden" name="payment_gross_{paymentSeqId}" id="payment_gross_{paymentSeqId}" value="{OrderBillVO/grossProductAmount}"/>
                        <input type="hidden" name="payment_addon_discount_{paymentSeqId}" id="payment_addon_discount_{paymentSeqId}" value="{OrderBillVO/addOnDiscountAmount}"/>
                        <input type="hidden" name="payment_type_{paymentSeqId}" id="payment_type_{paymentSeqId}" value="{paymentType}"/>
                        <input type="hidden" name="payment_wholesale_fee_{paymentSeqId}" id="payment_wholesale_fee_{paymentSeqId}" value="{OrderBillVO/wholesaleServiceFee}"/>
                        <input type="hidden" name="payment_coupon_status_{paymentSeqId}" id="payment_coupon_status_{paymentSeqId}" value="{gcCouponStatus}"/>
                        <input type="hidden" name="payment_service_fee_{paymentSeqId}" id="payment_service_fee_{paymentSeqId}" value="{OrderBillVO/serviceFee}"/>
                        <input type="hidden" name="payment_shipping_fee_{paymentSeqId}" id="payment_shipping_fee_{paymentSeqId}" value="{OrderBillVO/shippingFee}"/>

                        <input type="hidden" name="payment_miles_points_amount_{paymentSeqId}" id="payment_miles_points_amount_{paymentSeqId}" value="{OrderBillVO/milesPointsAmount}"/>
                        <input type="hidden" name="payment_miles_points_original_{paymentSeqId}" id="payment_miles_points_original_{paymentSeqId}" value="{OrderBillVO/milesPointsOriginal}"/>

                      </tr>

                      <xsl:if test="RefundVO">
                        <xsl:for-each select="RefundVO">
                          <tr>
                            <td width="1%"></td>
                            <td width="1%"></td>
                              <xsl:choose>
                              <xsl:when test="refundUpdateFlag='Y' and refundDispCode!='C30' and (paymentType!='G' or (paymentType='G' and ../gcCouponStatus!='Reinstate') )">
                                <td align="left"><input onclick="clickCheckBox({refundSeqId})" id="refund_delete_flag_{refundSeqId}" name="refund_delete_flag_{refundSeqId}" type="checkbox"></input></td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td align="left"><input disabled="true" id="refund_delete_flag_{refundSeqId}" name="refund_delete_flag_{refundSeqId}" type="checkbox"></input></td>
                              </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                              <xsl:when test="paymentType='G'">

                                <xsl:if test="../gcCouponStatus='Redeemed'">
                                  <xsl:choose>
                                    <xsl:when test="../gcReinstatedOnOriginalOrder='N'">
                                      <td align="center" class="TextLink"><a href="javascript:validateReinstate('{cardNumber}')">Reinstate</a></td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <td align="center"><b>Redeemed On Other Order</b></td>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:if>

                                <xsl:if test="../gcCouponStatus='Refund'">
                                  <td align="center"><b>Reinstate Pending</b></td>
                                </xsl:if>

                                <xsl:if test="../gcCouponStatus='Reinstate'">
                                  <td align="center"><b>Reinstated</b></td>
                                </xsl:if>
                              </xsl:when>
                              <xsl:otherwise>
                                <td></td>
                              </xsl:otherwise>
                            </xsl:choose>

                            <td align="center">
                              <xsl:if test="$milesPointsFlag = 'Y'">
                                <font color="red">
                                  (<xsl:value-of select="OrderBillVO/milesPointsAmount"/>)
                                </font>
                              </xsl:if>
                            </td>
                            <td align="center"><font color="red">(<xsl:value-of select="format-number(OrderBillVO/productAmount, '#,###,##0.00')"/>)</font></td>
                            <td align="center"><font color="red">(<xsl:value-of select="format-number(OrderBillVO/addOnAmount, '#,###,##0.00')"/>)</font></td>
                            <td align="center"><font color="red">(<xsl:value-of select="format-number(OrderBillVO/feeAmount, '#,###,##0.00')"/>)</font></td>
                            <td align="center"><font color="red">(<xsl:value-of select="format-number(OrderBillVO/tax, '#,###,##0.00')"/>)</font></td>
                            <td align="center"><font color="red"><xsl:value-of select="createdBy"/></font></td>
                            <td align="center"><font color="red"><xsl:value-of select="accountingDate"/></font></td>
                            <xsl:variable name="OutRefundID" select="refundId"/>
                            <xsl:variable name="OutDispCode" select="refundDispCode"/>
                            <xsl:variable name="dispType" select="//RefundDispositions/Disposition[refund_disp_code=$OutDispCode]/refund_display_type"/>
                            <td align="center">
                              <xsl:choose>
                                <xsl:when test="$dispType != ''">
                                  <xsl:choose>
                                    <xsl:when test="refundUpdateFlag='Y' and refundDispCode!='C30'">
                                      <select onchange="dispCodeChange({refundSeqId})" style="color:red" name="refund_disp_{refundSeqId}" id="refund_disp_{refundSeqId}">
                                        <xsl:for-each select="/root/RefundDispositions/Disposition[refund_display_type=$dispType]">
                                          <xsl:variable name="dispCode" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/refundDispCode"/>
                                          <xsl:choose>
                                            <xsl:when test="(refund_disp_code=$dispCode)">
                                              <option value="{refund_disp_code}" selected="selected"><xsl:value-of select="refund_disp_code"/></option>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <option value="{refund_disp_code}"><xsl:value-of select="refund_disp_code"/></option>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </xsl:for-each>
                                      </select>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <select disabled="true" style="color:red" name="refund_disp_{refundSeqId}" id="refund_disp_{refundSeqId}">
                                        <xsl:for-each select="/root/RefundDispositions/Disposition">
                                          <xsl:variable name="dispCode" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/refundDispCode"/>
                                          <xsl:choose>
                                            <xsl:when test="(refund_disp_code=$dispCode)">
                                              <option value="{refund_disp_code}" selected="selected"><xsl:value-of select="refund_disp_code"/></option>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <option value="{refund_disp_code}"><xsl:value-of select="refund_disp_code"/></option>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </xsl:for-each>
                                      </select>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                  <select disabled="true" style="color:red" name="refund_disp_{refundSeqId}" id="refund_disp_{refundSeqId}">
                                    <option value="{$OutDispCode}"><xsl:value-of select="$OutDispCode"/></option>
                                  </select>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td align="center">
                              <xsl:if test="$dispType != 'A'">
                                <xsl:choose>
                                  <xsl:when test="refundUpdateFlag='Y' and refundDispCode!='C30' or responsiblePartyUpdateFlag='Y' and refundDispCode!= 'C30'">
                                    <select onchange="dispCodeChange({refundSeqId})" style="color:red" name="refund_resp_party_{refundSeqId}" id="refund_resp_party_{refundSeqId}">

                                      <xsl:for-each select="/root/OrderFlorists/Florist">
                                        <xsl:variable name="respPartyCode" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/responsibleParty"/>
                                        <xsl:choose>
                                          <xsl:when test="(responsible_party=$respPartyCode)">
                                            <option value="{responsible_party}" selected="selected"><xsl:value-of select="responsible_party"/></option>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <option value="{responsible_party}"><xsl:value-of select="responsible_party"/></option>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    </select>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <select disabled="true" style="color:red" name="refund_resp_party_{refundSeqId}" id="refund_resp_party_{refundSeqId}">

                                      <xsl:for-each select="/root/OrderFlorists/Florist">
                                        <xsl:variable name="respPartyCode" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/responsibleParty"/>
                                        <xsl:choose>
                                          <xsl:when test="(responsible_party=$respPartyCode)">
                                            <option value="{responsible_party}" selected="selected"><xsl:value-of select="responsible_party"/></option>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <option value="{responsible_party}"><xsl:value-of select="responsible_party"/></option>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    </select>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:if>
                            </td>

                            <input type="hidden" name="refund_seq_id_{refundSeqId}" id="refund_seq_id_{refundSeqId}" value="{refundSeqId}"/>
                            <input type="hidden" name="refund_id_{refundSeqId}" id="refund_id_{refundSeqId}" value="{refundId}"/>
                            <input type="hidden" name="refund_coupon_number_{refundSeqId}" id="refund_coupon_number_{refundSeqId}" value="{cardNumber}"/>
                            <input type="hidden" name="refund_coupon_number2_{refundSeqId}" id="refund_coupon_number2_{refundSeqId}" value="{gcCouponStatus}"/>
                            <input type="hidden" name="refund_payment_type_{refundSeqId}" id="refund_payment_type_{refundSeqId}" value="{paymentType}"/>
                            <input type="hidden" name="refund_gc_multi_order_flag_{refundSeqId}" id="refund_gc_multi_order_flag_{refundSeqId}" value="{gcCouponMultiOrderFlag}"/>
                            <input type="hidden" name="refund_edit_flag_{refundSeqId}" id="refund_edit_flag_{refundSeqId}" value="N"/>
                          </tr>
                                                                                                        <tr>
                                                                                                               <td align="center" colspan="13"><div id="refundErrorDiv{refundSeqId}" style="color:red; display:none; font-weight:bold"></div></td>
                                                                                                        </tr>
                                                                                                        <tr id="complaintCommDetail{refundSeqId}" style="display:none">
                                                                                                             <td align="right" colspan="9" class="label">Origin of Refund:</td>
                                                                                                             <td align="left" colspan="1">
                                                                                                                  <xsl:choose>
                                                                                                                    <xsl:when test="refundUpdateFlag='Y' and refundDispCode!='C30'">
                                                                                                                      <select onchange="complaintOriginChangeDetail({refundSeqId})" name="complaint_comm_origin_type_id_{refundSeqId}" id="complaint_comm_origin_type_id_{refundSeqId}" style="color:red">
                                                                                                                           <option value="0">-select one-</option>
                                                                                                                           <xsl:for-each select="/root/complaint_comm_type/comm_type">
                                                                                                                            <xsl:variable name="complaintCommOriginTypeId" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/complaintCommOriginTypeId"/>
                                                                                                                            <xsl:choose>
                                                                                                                              <xsl:when test="(complaint_comm_type_id=$complaintCommOriginTypeId)">
                                   <option value="{complaint_comm_type_id}" selected="selected"><xsl:value-of select="description"/></option>
                                                                                                                                </xsl:when>
                                                                                                                                <xsl:otherwise>
                                                                                                                                   <option value="{complaint_comm_type_id}" ><xsl:value-of select="description"/></option>
                                                                                                                               </xsl:otherwise>
                                                                                                                              </xsl:choose>
                                                                                                                           </xsl:for-each>
                                                                                                                      </select>
                                                                                                                    </xsl:when>
                                                                                                                    <xsl:otherwise>
                                                                                                                        <select disabled="true" style="color:red" name="complaint_comm_origin_type_id_{refundSeqId}" id="complaint_comm_origin_type_id_{refundSeqId}">
                                                                                                                           <xsl:for-each select="/root/complaint_comm_type/comm_type">
                                                                                                                              <xsl:variable name="complaintCommOriginTypeId" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/complaintCommOriginTypeId"/>
                                                                                                                              <xsl:choose>
                                                                                                                                <xsl:when test="(complaint_comm_type_id=$complaintCommOriginTypeId)">
                                                                                                                                       <option value="{complaint_comm_type_id}" selected="selected"><xsl:value-of select="description"/></option>
                                                                                                                                </xsl:when>
                                                                                                                                <xsl:otherwise>
                                                                                                                                       <option value="{complaint_comm_type_id}" ><xsl:value-of select="description"/></option>
                                                                                                                                 </xsl:otherwise>
                                                                                                                              </xsl:choose>
                                                                                                                           </xsl:for-each>
                                                                                                                        </select>
                                                                                                                    </xsl:otherwise>
                                                                                                                  </xsl:choose>
                                                                                                              </td>
                                                                                                              <td align="right" colspan="2" class="label">Origin of Refund Notification:</td>
                                                                                                              <td align="left" colspan="1">
                                                                                                                 <xsl:choose>
                                                                                                                    <xsl:when test="refundUpdateFlag='Y' and refundDispCode!='C30'">
                                                                                                                      <select onchange="complaintNotificationChangeDetail({refundSeqId})" name="complaint_comm_notification_type_id_{refundSeqId}" id="complaint_comm_notification_type_id_{refundSeqId}" style="color:red">
                                                                                                                          <option value="0">-select one-</option>
                                                                                                                          <xsl:for-each select="/root/complaint_comm_type/comm_type">
                                                                                                                            <xsl:variable name="complaintCommNotificationTypeId" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/complaintCommNotificationTypeId"/>
                                                                                                                            <xsl:choose>
                                                                                                                              <xsl:when test="(complaint_comm_type_id=$complaintCommNotificationTypeId)">
                                   <option value="{complaint_comm_type_id}" selected="selected"><xsl:value-of select="description"/></option>
                                                                                                                                </xsl:when>
                                                                                                                                <xsl:otherwise>
                                                                                                                                   <option value="{complaint_comm_type_id}" ><xsl:value-of select="description"/></option>
                                                                                                                               </xsl:otherwise>
                                                                                                                             </xsl:choose>
                                                                                                                           </xsl:for-each>
                                                                                                                      </select>
                                                                                                                    </xsl:when>
                                                                                                                    <xsl:otherwise>
                                                                                                                        <select disabled="true" style="color:red" name="complaint_comm_notification_type_id_{refundSeqId}" id="complaint_comm_notification_type_id_{refundSeqId}">
                                                                                                                           <xsl:for-each select="/root/complaint_comm_type/comm_type">
                                                                                                                              <xsl:variable name="complaintCommNotificationTypeId" select="//Payments/PaymentVO/RefundVO[refundId=$OutRefundID]/complaintCommNotificationTypeId"/>
                                                                                                                              <xsl:choose>
                                                                                                                                <xsl:when test="(complaint_comm_type_id=$complaintCommNotificationTypeId)">
                                                                                                                                       <option value="{complaint_comm_type_id}" selected="selected"><xsl:value-of select="description"/></option>
                                                                                                                                </xsl:when>
                                                                                                                                <xsl:otherwise>
                                                                                                                                       <option value="{complaint_comm_type_id}" ><xsl:value-of select="description"/></option>
                                                                                                                                 </xsl:otherwise>
                                                                                                                              </xsl:choose>
                                                                                                                           </xsl:for-each>
                                                                                                                        </select>
                                                                                                                    </xsl:otherwise>
                                                                                                                  </xsl:choose>
                                                                                                              </td>
                                                                                                        </tr>
                        </xsl:for-each>
                      </xsl:if>

                    </xsl:for-each>
                    <tr><td colspan="13"><hr/></td></tr>
                    <tr>
                      <td colspan="3" align="left" class="label">Remaining Totals</td>
                      <td></td>
                      <td align="center">
                        <xsl:if test="$milesPointsFlag = 'Y'">
                          <xsl:value-of select="key('remaining_totals','milesPointsTotal')/value"/>
                        </xsl:if>
                      </td>
                      <td align="center"><xsl:value-of select="format-number(key('remaining_totals','msdTotal')/value, '#,###,##0.00')"/></td>
                      <td align="center"><xsl:value-of select="format-number(key('remaining_totals','addOnTotal')/value, '#,###,##0.00')"/></td>
                      <td align="center"><xsl:value-of select="format-number(key('remaining_totals','feeTotal')/value, '#,###,##0.00')"/></td>
                      <td align="center"><xsl:value-of select="format-number(key('remaining_totals','taxTotal')/value, '#,###,##0.00')"/></td>
                      <td colspan="5"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <xsl:if test="(paymentType='G') and (gcCouponMultiOrderFlag='Y')">
                      <tr>
                        <td colspan="4" align="left"><i>* Spans multiple orders</i></td>
                      </tr>
                    </xsl:if>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="96%" border="0" align="center">
        <tr>
          <td align="left" valign="top">
            <button type="button" accesskey="T" class="bigBlueButton" style="WIDTH: 115px" name="searchAction" onClick="doBackAction();">Recipien(t)/<br/>Order</button>
            <button type="button" onclick="mercuryClick()" accesskey="O" name="mercury" class="bigBlueButton ButtonTextBox2">
            C(o)mmunication
            </button>
            <button type="button" onclick="orderCommentsClick()" accesskey="C" name="orderComments" class="bigBlueButton ButtonTextBox2">
             Order<br/>(C)omments
            </button>
            <button type="button" onclick="openEmail();" accesskey="E" name="emailButton" id="emailButton" class="bigBlueButton ButtonTextBox1">
               <xsl:if test="$msg_email_indicator = 'true'">
                    <xsl:attribute name="style">font-weight:bold</xsl:attribute>
               </xsl:if>
              (E)mail
            </button>
            <button type="button" onclick="postRefundClick()" accesskey="P" name="postRefund" style="width:85" class="bigBlueButton">(P)ost Refund</button>
            <button type="button" onclick="deleteRefundClick()" accesskey="D" name="deleteRefund" style="width:90" class="bigBlueButton">(D)elete Refund</button>
            <br/>
            <button type="button" name="refresh" class="bigBlueButton" style="WIDTH: 115px" accesskey="R" onclick="refreshClick()">
            (R)efresh
            </button>
            <button type="button" onclick="updateClick()" accesskey="S" name="update" class="bigBlueButton" style="WIDTH: 115px">
            (S)ave<br/>Changes
            </button>
            <button type="button" onclick="doRefundReview()" accesskey="V" name="refundReview" id="refundReview" style="width:100" class="bigBlueButton" >Return to <br /> Refund Re(v)iew</button>

            <!-- <button type="button" onclick="reinstateClick()" accesskey="G" name="gcReinstate" style="width:90" class="bigBlueButton">Reinstate (G)ift<br/> Certificate</button> -->
          </td>
          <td align="right" width="60">
              <button type="button" class="blueButton" name="backButton" onclick="doBackAction();" style="width:80px">(B)ack to Order</button>
              <br />
              <button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" onclick="mainMenuClick();" style="width:80px">(M)ain<br />Menu</button>
          </td>
        </tr>
      </table>
    </form>
  </div>

  <xsl:call-template name="footer"></xsl:call-template>

  <div id="waitDiv" style="display:none">
    <table id="waitTable" class="mainTable" width="98%" height="195px" align="center" cellpadding="0" cellspacing="1" >
      <tr>
        <td width="100%">
          <table class="innerTable" style="width:100%;height:195px" cellpadding="30" cellspacing="1">
            <tr>
              <td>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td id="WaitMessage" align="right" width="60%" class="WaitMessage"></td>
                    <td id="waitTD"  width="40%" class="WaitMessage"></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <iframe name="validateGiftCertIFrame" id="validateGiftCertIFrame" width="0px" height="0px" border="0"/>

</body>


</html>
</xsl:template>
</xsl:stylesheet>
