
<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:variable name="replyFlag" select="key('pagedata','replyFlag')/value"/>
	<xsl:variable name="msg_id" select="key('pagedata', 'message_id')/value"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Send Email</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/tabIndexMaintenance.js"/>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript">// init code

        <![CDATA[
function init()
{
  var sentFlag = document.forms[0].status_flag.value;
  replaceText("lockError", sentFlag);
  setSendEmailIndexes();
  if(document.forms[0].customer_email_address.value=="" && document.forms[0].sender_email_address.value=="")
  {
    document.getElementById("sendButton").disabled="true";
  }
  if (document.getElementById('newBody').value != '' && sentFlag == '')
    textChange();
  
  var tempSubject = document.getElementById('newSubject').value;
  tempSubject = tempSubject.replace(/^\s+|\s+$/g, '');
  if (tempSubject != null && tempSubject.length > 3) {
      while ((tempSubject.substring(0,3).toLowerCase().lastIndexOf('re:', 0)) === 0) {
          tempSubject = tempSubject.substring(3).replace(/^\s+|\s+$/g, '');
      }
  }
  
  document.getElementById('newSubject').value = tempSubject;
}

/*******************************************************************************
*  setSendEmailIndexes()
*  Sets tab indexes for Send Email Page.
*  See tabIndexMaintenance.js
*******************************************************************************/
function setSendEmailIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('stockEmail','refreshButton', 'message_subject',
  'searchAction', 'spellCheck', 'sendButton', 'commButton', 'backButton', 
  'mainMenu', 'masterOrderNumberLink', 'printer');
  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}



function doOrderSearchAction(orderNumberType)
{
  	var form = document.sendEmail;
  	var order_number = null;
  	if(orderNumberType == "Master Order")
    	order_number = form.master_order_number.value;
  	else
    	order_number = form.external_order_number.value;

	if(document.sendEmail.change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
  			form.action = "customerOrderSearch.do" +
                 "?action=customer_account_search" +
                 "&order_number="+order_number;
  			form.submit();
  		}
  	}else{
		form.action = "customerOrderSearch.do" +
                 "?action=customer_account_search" +
                 "&order_number="+order_number;
		form.submit();
  	}

}

function textChange(){
	document.sendEmail.change_flag.value="Y";
	document.sendEmail.spellCheckFlag.value="Y";
}

function mainMenuClick(){
	if(document.sendEmail.change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
			doMainMenuAction();
		}
	}else{
		doMainMenuAction();
	}
}

// perform communication button action
function commAction()
{
	if(document.sendEmail.change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
      document.forms[0].action = "displayCommunication.do";
      document.forms[0].action_type.value = "display_communication";
      document.forms[0].submit();
		}
	}else{
    document.forms[0].action = "displayCommunication.do";
    document.forms[0].action_type.value = "display_communication";
    document.forms[0].submit();
	}

}

function spellCheck()
{
  var form = document.sendEmail;
    var value = escape(form.newBody.value).replace(/\+/g,"%2B");
    var url = "spellCheck.do?content=" + value + "&callerName=newBody"+
              "&securitytoken="+form.securitytoken.value+
              "&context="+form.context.value;

    window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300,help=no");
   form.spellCheckFlag.value = "N";
}

function doRefresh(){
  form = document.sendEmail;
  form.action_type.value = "load_content";
  form.action = "loadSendMessage.do";
  form.message_id.value = document.sendEmail.stockEmail.options[document.sendEmail.stockEmail.selectedIndex].value;
  form.message_title.value = document.sendEmail.stockEmail.options[document.sendEmail.stockEmail.selectedIndex].text;
  if(form.message_id.value==0)
  {
    alert("Select a Stock Email and then Refresh");
    return;
  }
  form.target = "_self";
  form.submit();
}

/*************************************************************************************
*	Performs the Back button functionality
**************************************************************************************/
function doBackAction()
{
 	doOrderSearchAction('External Order');
}

function sendAction(){

	var form=document.sendEmail;

	// START Validate subject and message body exists
  var subject = document.getElementById('newSubject');
  var newBody = document.getElementById('newBody');
	
	// Initialize as empty string to avoid any exceptions when testing the 
	// object
	var subjectVal = "";
	var newBodyVal = "";

	if(newBody != null) newBodyVal = newBody.value;	
	if(subject != null) subjectVal = subject.value;	

  if(Trim(subjectVal) == "")
	{
		alert("Please enter a subject");
	  return false;
	}
	else if(Trim(newBodyVal) == "")
	{
		alert("Please enter message body");
	  return false;
	}
	// END Validate subject and message body exists

	var sentFlag =  document.forms[0].status_flag.value;
 //took out the madatory spell check due to speed problems...will put it back when the process is sped up
  form.spellCheckFlag.value = "N";
  if(form.spellCheckFlag.value == "N")
	{
		
		if(sentFlag != '')
		{
			var modalUrl = "confirm.html";

			//Modal_Arguments
			var modalArguments = new Object();
			modalArguments.modalText = "This email has already been sent - are you sure you want to send it again?";

			//Modal_Features
			var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no;';
			modalFeatures += 'help=no; resizable:no; scroll:no';

			//get a true/false value from the modal dialog.
			var continueSubmit = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
			
			if(continueSubmit!=true)
			{
				return;
			}
	    }
	    
	    // Check if tokens have all been replaced
	    if(form.newBody.value.indexOf('~') > 0 || form.newSubject.value.indexOf('~') > 0)
	    {
	    	alert("Please replace all tokens before sending.");
	    	return;
	    }
	    
		form.action_type.value = "send_message";
		form.action = "sendMessage.do";
		form.message_id.value = form.stockEmail.options[form.stockEmail.selectedIndex].value;
		form.message_title.value = form.stockEmail.options[form.stockEmail.selectedIndex].text;
		form.message_subject.value = form.newSubject.value; 
		
	    if(form.replyFlag.value=="Y")
	    {
	      form.message_content.value = form.newBody.value + "\n\n" + form.replyBody.value;
	    }
	    else
	    {
	      form.message_content.value = form.newBody.value;
	      
	    }
	    
	    form.target = window.name;
	    form.submit();  
    }
    else
    {
    	alert("You must spell check the email before sending");
    }
}

function replaceText( elementId, newString )
  {
   var node = document.getElementById( elementId );   
   var newTextNode = document.createTextNode( newString );
   if(node.childNodes[0]!=null)
   {
    node.removeChild( node.childNodes[0] );
   }
   node.appendChild( newTextNode );
  }
  
  function openOrderComments(){
  var order_detail_id = document.getElementById("order_detail_id").value;
  var bypass_com = document.getElementById("bypass_com").value;
  var vip_customer = document.getElementById("vip_customer").value;
  var origin_id = document.getElementById("origin_id").value;
  var product_type = document.getElementById("product_type").value;
  var ship_method = document.getElementById("ship_method").value;
  var vendor_flag = document.getElementById("vendor_flag").value;
  var order_guid = document.getElementById("order_guid").value;
  
  doOrderCommentsAction(order_detail_id, bypass_com, vip_customer, origin_id, product_type, ship_method, vendor_flag, order_guid, vip_customer);
  }
  
  function openRefunds(){
  var order_detail_id = document.getElementById("order_detail_id").value;
  var origin_id = document.getElementById("origin_id").value;
  var product_type = document.getElementById("product_type").value;
  var ship_method = document.getElementById("ship_method").value;
  var vendor_flag = document.getElementById("vendor_flag").value;
  var order_guid = document.getElementById("order_guid").value;
 
  doOrderRefundAction(order_detail_id, origin_id, product_type, ship_method, vendor_flag, order_guid);
  }
  

]]></script>
			</head>
			<body onload="init();">

				<xsl:choose>
					<xsl:when test="key('pagedata','master_order_number')/value != ''">
						<xsl:call-template name="header">
							<xsl:with-param name="headerName" select="'Send Email'"/>
							<xsl:with-param name="showTime" select="true()"/>
							<xsl:with-param name="showBackButton" select="true()"/>
							<xsl:with-param name="showPrinter" select="true()"/>
							<xsl:with-param name="showSearchBox" select="false()"/>
							<xsl:with-param name="dnisNumber" select="$call_dnis"/>
							<xsl:with-param name="cservNumber" select="$call_cs_number"/>
							<xsl:with-param name="indicator" select="$call_type_flag"/>
							<xsl:with-param name="brandName" select="$call_brand_name"/>
							<xsl:with-param name="showCSRIDs" select="true()"/>
							<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
							<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="header">
							<xsl:with-param name="headerName" select="'Send Email'"/>
							<xsl:with-param name="showTime" select="true()"/>
							<xsl:with-param name="showBackButton" select="false()"/>
							<xsl:with-param name="showPrinter" select="true()"/>
							<xsl:with-param name="showSearchBox" select="false()"/>
							<xsl:with-param name="dnisNumber" select="$call_dnis"/>
							<xsl:with-param name="cservNumber" select="$call_cs_number"/>
							<xsl:with-param name="indicator" select="$call_type_flag"/>
							<xsl:with-param name="brandName" select="$call_brand_name"/>
							<xsl:with-param name="showCSRIDs" select="true()"/>
							<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>


				<form name="sendEmail" method="post" action="">
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="action_type" id="action_type" value="{key('pagedata','action_type')/value}"/>
					<input type="hidden" name="company_id" id="company_id" value="{key('pagedata','company_id')/value}"/>
					<input type="hidden" name="replyFlag" id="replyFlag" value="{key('pagedata', 'replyFlag')/value}"/>
					<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata','customer_id')/value}"/>
					<input type="hidden" name="customer_first_name" id="customer_first_name" value="{key('pagedata', 'customer_first_name')/value}"/>
					<input type="hidden" name="customer_last_name" id="customer_last_name" value="{key('pagedata', 'customer_last_name')/value}"/>
					<input type="hidden" name="order_guid" id="order_guid" value="{key('pagedata','order_guid')/value}"/>
					<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pagedata','master_order_number')/value}"/>
					<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pagedata', 'external_order_number')/value}"/>
					<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata', 'order_detail_id')/value}"/>
					<input type="hidden" name="bypass_com"  id="bypass_com" value="{key('pagedata', 'bypass_com')/value}" />
					<input type="hidden" name="vip_customer" id="vip_customer" value="{key('pagedata', 'vip_customer')/value}"/>
					<input type="hidden" name="product_type" id="product_type" value="{key('pagedata', 'product_type')/value}"/>
					<input type="hidden" name="ship_method" id="ship_method" value="{key('pagedata', 'ship_method')/value}"/>
					<input type="hidden" name="vendor_flag" id="vendor_flag" value="{key('pagedata', 'vendor_flag')/value}"/>
					<input type="hidden" name="customer_email_address" id="customer_email_address" value="{key('pagedata', 'customer_email_address')/value}"/>
					<input type="hidden" name="sender_email_address" id="sender_email_address" value="{key('pagedata', 'sender_email_address')/value}"/>
					<input type="hidden" name="message_type" id="message_type" value="{key('pagedata', 'message_type')/value}"/>
					<input type="hidden" name="spellCheckFlag" id="spellCheckFlag" value="N"/>
					<input type="hidden" name="message_id" id="message_id" value="{key('pagedata', 'message_id')/value}"/>
					<input type="hidden" name="message_title" id="message_title" value=""/>
					<input type="hidden" name="origin_id" id="origin_id" value="{key('pagedata','origin_id')/value}"/>
					<input type="hidden" name="change_flag" id="change_flag" value="N"/>
					<input type="hidden" name="poc_id" id="poc_id" value="{key('pagedata', 'poc_id')/value}"/>

          <input type="hidden" name="status_flag" id="status_flag" value="{key('pagedata', 'status_flag')/value}"/>
          
					<xsl:choose>
						<xsl:when test="$replyFlag='Y'">
							<input type="hidden" name="message_subject" id="message_subject" value="{key('pagedata', 'message_subject')/value}"/>
							<input type="hidden" name="message_content" id="message_content" value="{key('pagedata', 'message_content')/value}"/>
						</xsl:when>
						<xsl:otherwise>
							<input type="hidden" name="message_subject" id="message_subject" value="{root/PointOfContactVO/emailSubject}"/>
							<input type="hidden" name="message_content" id="message_content" value="{root/PointOfContactVO/body}"/>
						</xsl:otherwise>
					</xsl:choose>
					<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
					<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
									<tr>
										<td align="center">
											<div style="overflow:auto; width:98%; height=270;">
												<xsl:choose>
													<xsl:when test="$replyFlag='Y'">
														<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
															<tr>
																<td class="label" width="25%" align="right">Master Order Number:</td>
																<td align="left" width="25%">
																	<a id="masterOrderNumberLink" href="javascript:doOrderSearchAction('Master Order');">
																		<xsl:value-of select="key('pagedata','master_order_number')/value"/>
																	</a>
																</td>
																<td class="label" width="25%" align="right">Customer:</td>
																<td align="left" width="25%">
																	<xsl:value-of select="key('pagedata','customer_first_name')/value"/>&nbsp;<xsl:value-of select="key('pagedata','customer_last_name')/value"/></td>
															</tr>
															<tr>
																<td class="label" width="25%" align="right">Order Number:</td>
																<td align="left">
																	<a href="javascript:doOrderSearchAction('External Order');">
																		<xsl:value-of select="key('pagedata','external_order_number')/value"/>
																	</a>
																</td>
																<td class="label" width="25%" align="right">Email Address:</td>
																<td align="left" width="25%">
																	<select name="email_address" id="email_address">
																		<xsl:choose>
																			<xsl:when test="key('pagedata', 'customer_email_address')/value != ''">
																				<xsl:choose>
																					<xsl:when test="key('pagedata', 'customer_email_address')/value = key('pagedata', 'email_address')/value">
																						<option selected="selected" value="{key('pagedata', 'customer_email_address')/value}">
																							<xsl:value-of select="key('pagedata', 'customer_email_address')/value"/>
																						</option>
																					</xsl:when>
																					<xsl:otherwise>
																						<option value="{key('pagedata', 'customer_email_address')/value}">
																							<xsl:value-of select="key('pagedata', 'customer_email_address')/value"/>
																						</option>
																					</xsl:otherwise>
																				</xsl:choose>

																				<xsl:if test="key('pagedata', 'customer_alt_email_address')/value != ''">
																					<xsl:choose>
																						<xsl:when test="key('pagedata', 'customer_alt_email_address')/value = key('pagedata', 'email_address')/value">
																							<option selected="selected" value="{key('pagedata', 'customer_alt_email_address')/value}">
																								<xsl:value-of select="key('pagedata', 'customer_alt_email_address')/value"/>
																							</option>
																						</xsl:when>
																						<xsl:otherwise>
																							<option value="{key('pagedata', 'customer_alt_email_address')/value}">
																								<xsl:value-of select="key('pagedata', 'customer_alt_email_address')/value"/>
																							</option>
																						</xsl:otherwise>
																					</xsl:choose>
																				</xsl:if>
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:choose>
																					<xsl:when test="key('pagedata', 'sender_email_address')/value != ''">
																						<xsl:if test="key('pagedata', 'sender_email_address')/value != ''">
																							<xsl:choose>
																								<xsl:when test="key('pagedata', 'sender_email_address')/value = key('pagedata', 'email_address')/value">
																									<option selected="selected" value="{key('pagedata', 'sender_email_address')/value}">
																										<xsl:value-of select="key('pagedata', 'sender_email_address')/value"/>
																									</option>
																								</xsl:when>
																								<xsl:otherwise>
																									<option value="{key('pagedata', 'sender_email_address')/value}">
																										<xsl:value-of select="key('pagedata', 'sender_email_address')/value"/>
																									</option>
																								</xsl:otherwise>
																							</xsl:choose>
																						</xsl:if>
																					</xsl:when>
																					<xsl:otherwise>
																						<option value="">Not Available</option>
																					</xsl:otherwise>
																				</xsl:choose>
																			</xsl:otherwise>
																		</xsl:choose>
																	</select>
																</td>
															</tr>
															<tr>
																<td colspan="4">&nbsp;</td>
															</tr>
															<tr>
																<td colspan="2" class="label" align="right">Stock Email:</td>
																<td colspan="2" align="left">
																	<select name="stockEmail" id="stockEmail">
																		<option value="0">-select one-</option>
																		<xsl:for-each select="/root/StockMessageTitles/Title">
																			<xsl:choose>
																				<xsl:when test="$msg_id=stock_email_id">
																					<option value="{stock_email_id}" selected="selected">
																						<xsl:value-of select="title"/>- <xsl:value-of select="subject"/></option>
																				</xsl:when>
																				<xsl:otherwise>
																					<option value="{stock_email_id}">
																						<xsl:value-of select="title"/>- <xsl:value-of select="subject"/></option>
																				</xsl:otherwise>
																			</xsl:choose>
																		</xsl:for-each>
																	</select>
																	&nbsp;
																	<button type="button" class="blueButton" accesskey="R" name="refreshButton" onclick="doRefresh()" style="width:55px">(R)efresh</button>
																</td>
															</tr>
															<tr>
																<td class="label" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Original Email:</td>
																<td class="label" align="right">Subject:</td>
																<td colspan="2" align="left">
																	<input size="63" onchange="textChange()" name="newSubject" id="newSubject" type="text" value="{key('pagedata','message_subject')/value}"/>
																</td>
															</tr>
															<tr>
																<td align="center" rowspan="13" colspan="4">
																	<textarea name="replyBody" readonly="readonly" style="background-color:#DDDDDD" rows="10" cols="50">
																		<xsl:value-of select="key('pagedata', 'message_content')/value"/>
																	</textarea>&nbsp;
																	<textarea onchange="textChange()" name="newBody" rows="10" cols="80">
																		<xsl:value-of select="root/PointOfContactVO/body"/>
																	</textarea>
																</td>
															</tr>
														</table>
													</xsl:when>
													<xsl:otherwise>
														<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
															<tr>
																<td class="label" width="25%" align="right">Master Order Number:</td>
																<td align="left">
																	<a id="masterOrderNumberLink" href="javascript:doOrderSearchAction('Master Order');">
																		<xsl:value-of select="key('pagedata', 'master_order_number')/value"/>
																	</a>
																</td>
																<td class="label" width="25%" align="right">Customer:</td>
																<td align="left">
																	<xsl:value-of select="key('pagedata', 'customer_first_name')/value"/>&nbsp;<xsl:value-of select="key('pagedata', 'customer_last_name')/value"/></td>
															</tr>
															<tr>
																<td class="label" width="25%" align="right">Order Number:</td>
																<td align="left">
																	<a href="javascript:doOrderSearchAction('External Order');">
																		<xsl:value-of select="key('pagedata', 'external_order_number')/value"/>
																	</a>
																</td>
																<td class="label" width="25%" align="right">Email Address:</td>
																<td align="left" width="25%">
																	<select name="email_address" id="email_address">
																		<xsl:choose>
																			<xsl:when test="key('pagedata', 'customer_email_address')/value != ''">
																				<xsl:choose>
																					<xsl:when test="key('pagedata', 'customer_email_address')/value = key('pagedata', 'email_address')/value">
																						<option selected="selected" value="{key('pagedata', 'customer_email_address')/value}">
																							<xsl:value-of select="key('pagedata', 'customer_email_address')/value"/>
																						</option>
																					</xsl:when>
																					<xsl:otherwise>
																						<option value="{key('pagedata', 'customer_email_address')/value}">
																							<xsl:value-of select="key('pagedata', 'customer_email_address')/value"/>
																						</option>
																					</xsl:otherwise>
																				</xsl:choose>

																				<xsl:if test="key('pagedata', 'customer_alt_email_address')/value != ''">
																					<xsl:choose>
																						<xsl:when test="key('pagedata', 'customer_alt_email_address')/value = key('pagedata', 'email_address')/value">
																							<option selected="selected" value="{key('pagedata', 'customer_alt_email_address')/value}">
																								<xsl:value-of select="key('pagedata', 'customer_alt_email_address')/value"/>
																							</option>
																						</xsl:when>
																						<xsl:otherwise>
																							<option value="{key('pagedata', 'customer_alt_email_address')/value}">
																								<xsl:value-of select="key('pagedata', 'customer_alt_email_address')/value"/>
																							</option>
																						</xsl:otherwise>
																					</xsl:choose>
																				</xsl:if>
																			</xsl:when>
																			<xsl:otherwise>
																				<option value="">Not Available</option>
																			</xsl:otherwise>
																		</xsl:choose>
																	</select>
																</td>
															</tr>
															<tr>
																<td colspan="7">&nbsp;</td>
															</tr>
															<tr>
																<td class="label" align="right">Stock Email:</td>
																<td colspan="3" align="left">
																	<select name="stockEmail" id="stockEmail">
																		<option value="0">-select one-</option>
																		<xsl:for-each select="/root/StockMessageTitles/Title">
																			<xsl:choose>
																				<xsl:when test="$msg_id=stock_email_id">
																					<option value="{stock_email_id}" selected="selected">
																						<xsl:value-of select="title"/>- <xsl:value-of select="subject"/></option>
																				</xsl:when>
																				<xsl:otherwise>
																					<option value="{stock_email_id}">
																						<xsl:value-of select="title"/>- <xsl:value-of select="subject"/></option>
																				</xsl:otherwise>
																			</xsl:choose>
																		</xsl:for-each>
																	</select>
																	&nbsp;
																	<button type="button" class="blueButton" accesskey="R" name="refreshButton" onclick="doRefresh()" style="width:55px">(R)efresh</button>
																</td>
															</tr>
															<tr>
																<td class="label" align="right">Subject:</td>
																<td colspan="3" align="left">
																	<input size="85" onchange="textChange()" name="newSubject" id="newSubject" type="text" value="{root/PointOfContactVO/emailSubject}"/>
																</td>
															</tr>
															<tr>
																<td colspan="7">&nbsp;</td>
															</tr>
															<tr>
																<input type="hidden" name="replyBody" id="replyBody" value=""/>
																<td align="center" rowspan="13" colspan="6">
																	<textarea onchange="textChange()" name="newBody" rows="9" cols="100">
																		<xsl:value-of select="root/PointOfContactVO/body"/>
																	</textarea>
																</td>
															</tr>
														</table>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

					<iframe id="sendEmailFrame" src="" width="0" height="0" frameborder="0"/>
				</form>
				<table width="98%" align="center">
					<xsl:choose>
						<xsl:when test="key('pagedata','master_order_number')/value != ''">

							<tr>
								<td width="75%" valign="top">
									<!-- <button type="button" style="width:90" accesskey="" class="bigBlueButton" name="searchAction" onClick="doOrderSearchAction('External Order');">Recipient/<br/>Order</button> -->
									<button type="button" accesskey="" class="bigBlueButton" style="width: 120px" name="searchAction" onClick="doOrderSearchAction('External Order');">Recipient/<br/>Order</button>
									<!-- <input style="width:90" accesskey="O" class="bigBlueButton" type="button" value="C(o)mmunication" name="commButton" id="commButton" onClick="commAction();"/>-->
									<button type="button" accesskey="O" class="bigBlueButton ButtonTextBox2" name="commButton" id="commButton" onClick="commAction();">C(o)mmunication</button>
									<button type="button"   id="orderComments" class="BigBlueButton" style="width: 120px" accesskey="C" onclick="javascript:openOrderComments();">Order<br/>(C)omments
					                </button>
									<button type="button"   id="refund" class="BigBlueButton ButtonTextBox1" accesskey="R" onclick="javascript:openRefunds();" >
					                 (R)efund </button>
									<button type="button" style="width:90" accesskey="P" class="bigBlueButton" name="spellCheck" id="spellCheck" onClick="spellCheck();">
									S(p)ell<br/>Check </button>
									<button type="button" style="width:90" accesskey="S" class="bigBlueButton" name="sendButton" id="sendButton" onClick="sendAction();">(S)end</button>
								</td>
								<td width="20%" align="right">
									<button type="button" class="blueButton" accesskey="B" name="backButton" onclick="javascript:doBackAction();">(B)ack to Order</button>
									<br/>
									<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" onclick="javascript:mainMenuClick();">(M)ain<br/>Menu</button>
								</td>
							</tr>
						</xsl:when>
						<xsl:otherwise>
							<tr>
								<td width="75%" valign="top">
									<!-- <input style="width:90" accesskey="P" class="bigBlueButton" name="spellCheck" id="spellCheck" type="button" value="S(p)ell Check" onClick="spellCheck();"/> -->
									<button type="button" style="width:90" accesskey="P" class="bigBlueButton" name="spellCheck" id="spellCheck" onClick="spellCheck();">
									S(p)ell<br/>Check
									</button>
									<!-- <input style="width:90" accesskey="S" class="bigBlueButton" type="button" value="(S)end" name="sendButton" id="sendButton" onClick="sendAction();"/> -->
								    <input style="width:90" accesskey="S" class="bigBlueButton" type="button" value="(S)end" name="sendButton" id="sendButton" onClick="sendAction();"/>
								</td>
								<td width="20%" align="right">

									<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" onclick="javascript:mainMenuClick();">(M)ain<br/>Menu</button>
								</td>
							</tr>
						</xsl:otherwise>
					</xsl:choose>
				</table>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<xsl:call-template name="footer"></xsl:call-template>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>