<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="errorData" match="/root/errorData/data" use="name"/>
<xsl:variable name="errorUrl" select="key('errorData','errorUrl')/value"/>
<xsl:variable name="message" select="key('pageData','message_display')/value"/>
<xsl:variable name="external_order_number" select="key('pageData','external_order_number')/value"/>
<xsl:variable name="order_guid" select="key('pageData','order_guid')/value"/>
<xsl:variable name="customer_id" select="key('pageData','customer_id')/value"/>
<xsl:output indent="yes" method="html"/>
<xsl:template match="/root">
<html>
  <head>
  <script type="text/javascript">
  var message = "<xsl:value-of select='$message'/>";
  var containsErrors = <xsl:value-of select="$errorUrl != ''"/>;
  function init(){
    if ( containsErrors ) {
      parent.baseDoCancelUpdatePostErrorAction("<xsl:value-of select='$errorUrl'/>");
    }
    else {
      if ( message != '' ) {
        alert(message);
      }
      parent.baseDoCancelUpdatePostAction( "<xsl:value-of select='$external_order_number'/>", "<xsl:value-of select='$order_guid'/>", "<xsl:value-of select='$customer_id'/>" );
    }
  }
  </script>
  </head>
  <body onload="javascript:init();">
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>