<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- import securityanddata -->
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<!-- variables -->
<xsl:variable name="message" select="key('pageData','message_display')/value"/>

<xsl:output indent="yes" method="html"/>
<xsl:template match="/root">

<html>
  <head>
	<title></title>
<script type="text/javascript">
var message = "<xsl:value-of select="$message"/>";

<![CDATA[

function init(){
	if(message == ''){
		/* add an array here to pass values up to this function */
			parent.doCompleteOrder(document.forms[0]);
	}else{
		alert(message);
	}
}

]]>
	</script>
  </head>
  <body onload="javascript:init();">
     <xsl:call-template name="securityanddata"/>
     <xsl:call-template name="decisionResultData"/>
     <!-- collect fields -->
     <form name="updateDetailIForm" id="updateDetailIForm">
        <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>
        <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
    </form>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>