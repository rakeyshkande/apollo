<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var statusValue = '<xsl:value-of select="key('pageData','message_display')/value"/>';
var success = '<xsl:value-of select="key('pageData','got_lock')/value"/>';
<!--
  forward_action-
    users can pass in a value for this in order to determine
    which method needs to be processed after this xsl executes
-->
var forward_action = '<xsl:value-of select="key('pageData','forward_action')/value"/>';
<![CDATA[
	function checkLock() {
    if ( statusValue == '' && success.toUpperCase() == 'Y' ) {
      // This function must be implemented on any xsl that will use the checkLockIFrame.xsl
      parent.checkAndForward( forward_action );
    }
    else {
       alert( statusValue );
       //Defect 872 - this is not common functionality.  This was specific to modify order.  We should not
       //             be cancelling an order from any/every page just because the record was locked.
       //parent.baseDoCancelUpdateAction(parent.CANCEL_FORM_NAME);
    }
	}
]]>
</script>
</head>
<body onload="javascript:checkLock();">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>