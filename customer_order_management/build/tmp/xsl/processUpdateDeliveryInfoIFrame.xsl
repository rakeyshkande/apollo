<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
  <script type="text/javascript" language="javascript">
  var deliveryDates = new Array( <xsl:for-each select="deliveryDates/deliveryDate">["<xsl:value-of select="startDate"/>","<xsl:value-of select="endDate"/>","<xsl:value-of select="displayDate"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
  <![CDATA[
  function processRequest() {
    if ( parent.callingAction == parent.MAX_DATES_ACTION ) {
      parent.doGetMaxDeliveryDaysPostAction(deliveryDates);
    }
  }
  ]]>
  </script>
</head>
<body onload="javascript:processRequest();"/>
</html>
</xsl:template>
</xsl:stylesheet>