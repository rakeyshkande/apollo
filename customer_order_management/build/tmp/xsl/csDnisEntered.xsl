
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:key name="security" match="/root/security/data" use="name"/>
	    <xsl:output method="html" indent="yes"/>
	<xsl:template match="/">
		<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"/>
      <title>Customer-Order Inquiry</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <script type="text/javascript" src="js/clock.js"></script>
      <script type="text/javascript" src="js/commonUtil.js"></script>
      <script type="text/javascript" src="js/util.js"></script>
      <script type="text/javascript" language="JavaScript">
      <![CDATA[


function doCustomerOrderSearch()
{
 document.forms[0].action = "customerOrderSearch.do?action=load_page";
 document.forms[0].submit();
}

function doOrderEntry()
{
document.forms[0].action = "orderEntry.do";
document.forms[0].submit();
}

function performAction(url)
{
  document.forms[0].action = url;
  document.forms[0].submit();
}
      
       ]]></script>
   </head>
<body onload="setNavigationHandlers();">
<form method="post">
<xsl:call-template name="securityanddata"/>
<xsl:call-template name="decisionResultData"/>
<!-- Header-->
<xsl:call-template name="header">
<xsl:with-param name="headerName" select="''" />
<xsl:with-param name="showTime" select="true()"/>
<xsl:with-param name="dnisNumber" select="$call_dnis"/>
<xsl:with-param name="brandName" select="$call_brand_name" />
<xsl:with-param name="indicator" select="$call_type_flag" />
<xsl:with-param name="cservNumber" select="$call_cs_number" />
<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
</xsl:call-template>
<!-- Display table-->

   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td><table width="100%" align="center" class="innerTable">
               <tr class="Label">
                 <td width="100%" align="center"><p>&nbsp;</p>
                 <p class="blackHeading"><xsl:value-of select="root/script_data/cs_script"/></p>
</td>
          </tr>
		  </table>
  		</td>
     </tr>
</table>
   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
         <td width="91%"><div align="center">
         </div></td>
         <td width="9%" align="right"><button type="button" class="BlueButton" name="main_menu_but" tabindex="1" accesskey="M" onclick="doMainMenuAction();">(M)ain Menu</button>
		</td>
      </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
      <td><p>&nbsp;</p>

          <div align="center">
		  <button type="button" class="BigBlueButton" name="cust_ord_srch_but" tabindex="2" accesskey="S" onclick="doCustomerOrderSearch();">
		  <p>CUSTOMER/ ORDER<br/>
	      (S)EARCH</p>
		  </button>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <button type="button" class="BigBlueButton" name="ord_entry_but" tabindex="3" accesskey="O" onclick="doOrderEntry();">(O)RDER ENTRY</button>
          </div>
    <p>&nbsp;</p></td>
   </tr>
   </table>
<!--Copyright bar-->
<xsl:call-template name="footer"/>
</form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
