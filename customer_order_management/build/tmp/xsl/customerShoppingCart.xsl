<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- external templates -->
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="tabs.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
  <xsl:import href="customerAccountDetail.xsl"/>
  <xsl:import href="itemDiv.xsl"/>
  <xsl:import href="orderShoppingCart.xsl"/>

  <!-- decimal format used when a value is not given -->
  <xsl:decimal-format NaN="0" name="notANumberNoDecimal"/>
  <xsl:decimal-format NaN="0" name="noRefundNoDecimal"/>
  <xsl:decimal-format NaN="0.00" name="notANumber"/>
  <xsl:decimal-format NaN="0.00"  name="noRefund"/>

  <!-- Keys -->
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:key name="pageData" match="/root/pageData/data" use="name"/>
  <xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

  <!-- Constants -->
  <xsl:variable name="azOrderNumber" select="'az_order_number'"/>
  <xsl:variable name="FRAUD" select="'Fraud'"/>
  <xsl:variable name="FRAUD_FLAG" select="'FRAUD_FLAG'"/>
  <xsl:variable name="LP_INDICATOR" select="'LP_INDICATOR'"/>
  <xsl:variable name="masterOrderNumber" select="'master_order_number'"/>
  <xsl:variable name="RE_AUTH" select="'RE-AUTH'"/>
  <xsl:variable name="SFMB" select="'SFMB'"/>

  <!-- scrub indicators -->
  <xsl:variable name="SCRUB" select="'S'"/>
  <xsl:variable name="PENDING" select="'P'"/>
  <xsl:variable name="REMOVED" select="'R'"/>

  <!-- Case comparision values -->
  <xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
  <xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>
  <xsl:variable name="v-special-characters" select="'&quot;'"/>

  <!-- Origin Variables-->
  <xsl:variable name="AMAZON_ORDER" select="'AMZNI'"/>
  <xsl:variable name="ARIBA_ORDER_ARI" select="'ARI'"/>
  <xsl:variable name="ARIBA_ORDER_CAT" select="'CAT'"/>
  <xsl:variable name="FOX" select="'FOX'"/>
  <xsl:variable name="Interent" select="'Internet'"/>
  <xsl:variable name="Phone" select="'Phone'"/>
  <xsl:variable name="WALMART_ORDER" select="'WLMTI'"/>


  <!-- Values used for the customer account page-->
  <xsl:variable name="id"  select="key('pageData','customer_id')/value"/>
  <xsl:variable name="cai" select="root/CAI_CUSTOMERS/CAI_CUSTOMER"/>
  <xsl:variable name="cai_phone" select="root/CAI_PHONES/CAI_PHONE"/>
  <xsl:variable name="YES" select="'Y'"/>
  <xsl:variable name="NO" select="'N'"/>
  <xsl:variable name="isUS" select="boolean(root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_country = 'US')"/>
<!-- end values for the CUSTOMER ACCOUNT PAGE-->

<!-- locate which ORDERS node that this cart relates too -->

 <xsl:variable name="keyValue" select="//CSCI_ORDERS/CSCI_ORDER/external_order_number[1]"/>
 <xsl:variable name="cartPosition" select="//CAI_ORDERS/CAI_ORDER/external_order_number[. = $keyValue]/../@num"/>
 <xsl:variable name="cartMasterOrderNumber" select="//CAI_ORDERS/CAI_ORDER[$cartPosition]/master_order_number"/>


<!-- retrieve searched on external order number -->
<xsl:variable name="searchedOnOrder" select="key('pageData','searched_on_external_order_number')/value"/>
<xsl:variable name="isSearchedOrder" select="//CSCI_ORDERS/CSCI_ORDER/external_order_number[. = translate($searchedOnOrder,$lower_case, $upper_case)]/../@num"/>


<!--retrieve cart level order guid-->
<!--
 <xsl:variable name="cartGuid" select="//CAI_ORDERS/CAI_ORDER[$cartPosition]/order_guid"/>
-->

  <xsl:variable name="bypassCOM" select="/root/pageData/data[name='bypass_com']/value"/>

  <!-- orderIndicator : display shoppingCart page or Item pages-->
  <xsl:variable name="orderIndicator" select="key('pageData','csci_order_indicator')/value"/>
  <xsl:variable name="numberOfOrders" select="key('pageData','csci_number_of_orders')/value"/>
  <xsl:variable name="orderPosition" select="key('pageData','csci_order_position')/value"/>
  <xsl:variable name="isAuthorized"  select="key('pageData','isAuthorized')/value"/>
  <xsl:variable name="isRestricted"  select="key('pageData','isRestricted')/value"/>
  <xsl:variable name="isAccountProgramExpired"  select="key('pageData','isAccountProgramExpired')/value"/>
  <xsl:variable name="programName"  select="key('pageData','programName')/value"/>
  <xsl:variable name="premierCircleMembership"  select="key('pageData','premierCircleMembership')/value"/>
  <xsl:variable name="partnerName"  select="key('pageData','partnerName')/value"/>
  <xsl:variable name="partnerChannelIcon"  select="key('pageData','partnerChannelIcon')/value"/>
  <xsl:variable name="partnerChannelName"  select="key('pageData','partnerChannelName')/value"/>
  <xsl:variable name="imageServerUrl"  select="key('pageData','imageServerUrl')/value"/>
  <xsl:variable name="modifyOrderAllowed"  select="key('pageData','modifyOrderAllowed')/value"/>
  
  <!-- Flags -->
  <xsl:variable name="isMilesPoints" select="key('pageData','isMilesPoints')/value"/>

  <!-- If this value is true we will dispaly the shoppingCart page-->
  <xsl:variable name="shoppingCartInd" select="boolean($masterOrderNumber = $orderIndicator or $azOrderNumber = $orderIndicator)"/>
  <xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
  <xsl:output method="html" indent="yes" encoding="ISO-8859-1"/>
  <xsl:template match="/root">
    <html>
      <head>
        <title>FTD - Customer Shopping Cart</title>
        <link rel="stylesheet" href="css/ftd.css"/>
        <link rel="stylesheet" href="css/ftdCOMTabs.css"/>
        
		<style type="text/css">
		
   a.tooltiptextlink{
      position:relative;
      z-index:24
   }

	a.tooltiptextlink:hover{z-index:25; background-color:#fff}

	a.tooltiptextlink span{display: none}

   a.tooltiptextlink:hover span{ 
	    display:block;
	    position:absolute;
	    top:2em; left:2em; width:8em;
	    border:1px solid #3669AC;
	    background-color: #CCEFFF;
	    color:#000000;
	    text-align: left;
	    padding-left: 4px;
	    padding-right: 4px;
	    font-family: arial,helvetica,sans-serif;
	    font-size: 8pt;
	    text-decoration:none;
	    filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
    }

   <!-- For use with tooltip above, but left shifted (for hovers near right hand side of page) -->
   a.tooltiptextlinkLS:hover span{
       left:-8em; width: 12em;
   }
   <!-- For use with tooltip above, but wider and right shifted more (for use displaying personalization info) -->
   a.tooltiptextlinkWide:hover span{
       left: 10em; top: 1em; width: 30em; padding-top: 2px; padding-bottom: 2px; overflow: hidden; text-overflow: ellipsis;
       
   }

    
   .tooltip{
        display:none;
        position:fixed;
        top:2em; left:2em; width:12em;
        border:1px solid #3669AC;
        background-color: #CCEFFF;
        color:#000000;
        text-align: left;
        padding-left: 4px;
        padding-right: 4px;
        font-family: arial,helvetica,sans-serif;
        font-size: 8pt;
        text-decoration:none;
        filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
   }
		
	

		

        </style>
        <style type="text/css">
         /* Scrolling table overrides for the order/shopping cart table */
          div.tableContainer table {
            width: 97%;
          }
          div#messageContainer {
            height: 130px;
            width: 100%;
          }
        </style>
        <script type="text/javascript" src="js/commonUtil.js"/>
        <script type="text/javascript" src="js/util.js"/>
        <script type="text/javascript" src="js/FormChek.js"/>
        <script type="text/javascript" language="javascript">
      <!-- Customer alternate contact information -->
      var alt_first_name = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/alt_first_name"/>";
      var alt_last_name  = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/alt_last_name"/>";
      var alt_phone = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/alt_phone_number"/>";
      var alt_ext = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/alt_extension"/>";
      var cart_source_code = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/source_code"/>";
      var alt_email = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/alt_email_address"/>";
      
      var isJointCart = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/is_joint_cart"/>";
        
      <!-- end of alt contact info -->
      var cust_id = '<xsl:value-of select="$id"/>';
      <!--var cartIndicator = '<xsl:value-of select="$shoppingCartInd"/>';-->
      var isAuthorized = '<xsl:value-of select="$isAuthorized"/>';
      var isAccountProgramExpired = '<xsl:value-of select="$isAccountProgramExpired"/>';
      var programName = '<xsl:value-of select="$programName"/>';
      var isRestricted = '<xsl:value-of select="$isRestricted"/>';
      var numberOfOrders = '<xsl:value-of select="$numberOfOrders"/>';
      var orderPosition = '<xsl:value-of select="$orderPosition"/>';
      var scrub_url = '<xsl:value-of select="key('pageData','scrub_url')/value"/>';
      var view_billing_url = '<xsl:value-of select="key('pageData','view_billing_url')/value"/>';
      var view_recon_url = '<xsl:value-of select="key('pageData','view_recon_url')/value"/>';
      var orderGuid = '<xsl:value-of select="CSCI_CARTS/CSCI_CART/order_guid"/>';
      var cartMasterOrderNumber = '<xsl:value-of select="$cartMasterOrderNumber"/>';
      var actionName = '<xsl:value-of select="key('pageData','action')/value"/>';
      var isGlobalUpdateOrderActive = <xsl:value-of select="boolean(key('pageData', 'global_authorization_flag')/value = 'N')"/>; // no means yes
      var isSFMB = <xsl:value-of select="boolean(CSCI_CARTS/CSCI_CART/company_id = $SFMB)"/>;
      var isJCP = <xsl:value-of select="boolean(key('pageData', 'jcp_flag')/value = 'Y')"/>;
      var premierCircleMembership = '<xsl:value-of select="$premierCircleMembership"/>';
           
      var partnerName = '<xsl:value-of select="$partnerName"/>';
      var partnerChannelIcon = '<xsl:value-of select="$partnerChannelIcon"/>';
      var partnerChannelName = '<xsl:value-of select="$partnerChannelName"/>';
      var imageServerUrl = '<xsl:value-of select="$imageServerUrl"/>';
      var modifyOrderAllowed = '<xsl:value-of select="$modifyOrderAllowed"/>';
      
      var moJoeUrl = '<xsl:value-of select="key('pageData','modify_order_joe_url')/value"/>';
      


      var pageAction = new Array("ca_first","ca_previous","ca_next","ca_last");
      var cai_show_previous = "<xsl:value-of select="key('pageData','cai_show_previous')/value"/>";
      var cai_show_next     = "<xsl:value-of select="key('pageData','cai_show_next')/value"/>";
      var cai_show_first    = "<xsl:value-of select="key('pageData','cai_show_first')/value"/>";
      var cai_show_last     = "<xsl:value-of select="key('pageData','cai_show_last')/value"/>";

      <!-- Origin Checks -->
      var isAmazonOrder = <xsl:value-of select="boolean(CSCI_CARTS/CSCI_CART/origin_id = $AMAZON_ORDER)"/>;
      var isAribaOrderARI = <xsl:value-of select="boolean(CSCI_CARTS/CSCI_CART/origin_id = $ARIBA_ORDER_ARI)"/>;
      var isAribaOrderCAT = <xsl:value-of select="boolean(CSCI_CARTS/CSCI_CART/origin_id = $ARIBA_ORDER_CAT)"/>;
      var isWalmartOrder = <xsl:value-of select="boolean(CSCI_CARTS/CSCI_CART/origin_id = $WALMART_ORDER)"/>;
      var paymentTypeDesc = "<xsl:value-of select="CSCI_CARTS/CSCI_CART/payment_type_desc"/>";
      var isAltPay = <xsl:value-of select="boolean(key('pageData','isAltPay')/value = 'Y')"/>;


      <!-- Misc Checks -->
      var skipCOM = <xsl:value-of select="boolean(key('pageData','bypass_com')/value = 'Y')"/>;

      <!--
         This variable is used to ensure that the order the CSR initially searched on
         will be highlighted orange each time they see it on the page
         The highlighting of tabs is handled in tabs.xsl.
         Not only do we use the tabToHighLight variable, but we also check the
         type of action being processed in order to know if we should hightlight order
         tabs orange or not.
      -->
      var tabToHighLight = '<xsl:value-of select="$isSearchedOrder"/>';
      var recipient_flag = '<xsl:value-of select="key('pageData','recipient_flag')/value"/>';
      var tabToDisplay;
      var scrubStatus = '';
      var sc_mode = '';
      var show_more = '<xsl:value-of select="key('pageData','csci_show_next')/value"/>';
      var show_previous = '<xsl:value-of select="key('pageData','csci_show_previous')/value"/>';
      var mysteryBox = new Array('numberEntry');
      var queueId = "";
      var recipZipCode = "";
      var queOrderId = "";
       var statusArray = new Array(<xsl:for-each select="CSCI_ORDERS/CSCI_ORDER">
                ["<xsl:value-of select="scrub_status"/>"]
            <xsl:choose>
              <xsl:when test="position()!=last()">,</xsl:when>
            </xsl:choose>
          </xsl:for-each>);
      var confirmations = new Array(<xsl:for-each select="CSCI_ORDERS/CSCI_ORDER">
                ["<xsl:value-of select="external_order_number"/>"]
            <xsl:choose>
              <xsl:when test="position()!=last()">,</xsl:when>
            </xsl:choose>
          </xsl:for-each>);
  var fieldParms = new Array('customer_id','master_order_number','order_guid');

  var siteNameSsl = '<xsl:value-of select="$sitenamessl"/>';
  var applicationContext = '<xsl:value-of select="$applicationcontext"/>';

  <!-- Privacy Policy Verification -->
  var customerValidationRequiredFlag = 'Y';
  var orderValidationRequiredFlag = 'Y';
  var customersValidated = new Array( <xsl:for-each select="PPV_VALIDATED_CUSTOMERS/VALIDATED_CUSTOMER">
                                        ["<xsl:value-of select="customer_id"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );

  var ordersValidated = new Array(  <xsl:for-each select="PPV_VALIDATED_ORDER_DETAILS/VALIDATED_ORDER_DETAIL">
                                      ["<xsl:value-of select="order_detail_id"/>"]
                                      <xsl:choose>
                                        <xsl:when test="position()!=last()">,</xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>
                                 );

  var surchargeDescriptionForCart   = "<xsl:value-of select="CSCI_ORDER_BILLS/CSCI_ORDER_BILL/surcharge_description"/>";

  var cartTaxes = new Array( <xsl:for-each select="CSCI_CART_TAXES/CSCI_CART_TAX">
                                        ["<xsl:value-of select="tax_description"/>",
                                        "<xsl:value-of select="tax_amount"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );

  var orderTaxes = new Array( <xsl:for-each select="CSCI_ORDER_TAXES/CSCI_ORDER_TAX">
  									<xsl:if test="display_order!='0'"> 
                                        ["<xsl:value-of select="order_detail_id"/>",
                                        "<xsl:value-of select="tax_description"/>",
                                        "<xsl:value-of select="tax_amount"/>",
                                        "<xsl:value-of select="is_total"/>"] 
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                        </xsl:if>
                                      </xsl:for-each>
                                    );
var orderDetailBills = new Array( <xsl:for-each select="CSCI_ORDER_BILLS/CSCI_ORDER_BILL">
                                        ["<xsl:value-of select="order_detail_id"/>",
                                        "<xsl:value-of select="format-number(service_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(shipping_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(vend_sat_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(ak_hi_special_charge, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(fuel_surcharge_amt, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(same_day_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(morning_delivery_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(vendor_sun_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(vendor_mon_upcharge '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(late_cutoff_fee '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>",
                                        "<xsl:value-of select="format-number(international_fee '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );
                                    
var orderAddOns = new Array( <xsl:for-each select="CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON">
                                        ["<xsl:value-of select="order_detail_id"/>",
                                        "<xsl:value-of select="add_on_code"/>",
                                        "<xsl:value-of select="add_on_quantity"/>",
                                        "<xsl:value-of select="translate(description, $v-special-characters, '')"/>",
                                        "<xsl:value-of select="add_on_sale_price"/>",
                                        "<xsl:value-of select="add_on_discount_amount"/>",
                                        "<xsl:value-of select="price"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );
                                    
var orderDiscount = new Array( <xsl:for-each select="CSCI_ORDER_BILLS/CSCI_ORDER_BILL">
                                        ["<xsl:value-of select="order_detail_id"/>",
                                        "<xsl:value-of select="total_type"/>",
                                        "<xsl:value-of select="discount_amount"/>",
                                        "<xsl:value-of select="add_on_discount_amount"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );
                                    
var cartDiscount = new Array( <xsl:for-each select="CSCI_CART_BILLS/CSCI_CART_BILL">
                                        ["<xsl:value-of select="total_type"/>",
                                        "<xsl:value-of select="discount_amount"/>",
                                        "<xsl:value-of select="add_on_discount_amount"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );

           
  <!-- Privacy Policy -->
  var pdBypassPrivacyPolicyVerification = 'N';
  <xsl:if test="key('pageData', 'bypass_privacy_policy_verification')/value = 'Y'">
    pdBypassPrivacyPolicyVerification = 'Y';
  </xsl:if>

<![CDATA[

/*******************************************************************************
* Intilization
*
* init()
*
*******************************************************************************/
function init(){
  // TAB INDEX NOTICE
  // Tab indexes for page elements are set in the tab.xsl file using the
  // tabIndexMaintenance.js.  They are reset each time a tabbed section is clicked.
  setNavigationHandlers();
  addDefaultListenersArray(mysteryBox);

  //For the first invokation of customerOrderSearch, bypass_privacy_policy_verification in the dataFilter maybe empty.  Thus,
  //we have to rely on the bypass_privacy_policy_verification that is being passed back in the pageData.
  //Value from dataFilter should be ok for subsequent invokations.
  if (document.getElementById('bypass_privacy_policy_verification') == null ||
      document.getElementById('bypass_privacy_policy_verification').value == '' ||
      document.getElementById('bypass_privacy_policy_verification').value == 'N')
    document.getElementById('bypass_privacy_policy_verification').value = pdBypassPrivacyPolicyVerification;

  /* tabToDisplay is the order the CSR searched on*/
  if(tabToHighLight != '' && tabToHighLight != null){
    tabToDisplay = "item" + tabToHighLight;
  }
  initializeTabs(tabToDisplay);


  var caDetail = document.getElementById('customerAccountDetail').style;
  caDetail.width = "100%";
  var caButtonMenu = document.getElementById('caButtonMenu').style;

  caButtonMenu.width="100%";
  if(actionName.substr(0,6) != 'ca_csc' && document.getElementById('display_page').value != '' && document.getElementById('display_page').value != 'ACCTINFO')
    document.getElementById('numberEntry').focus();
  document.getElementById('numberEntry').onkeypress = '';

  // Disable receipt functionality if an order in scrub or pending or removed
  for(var i=0;i<statusArray.length;i++)
  {
    if(statusArray[i] == '')
    {
      document.getElementById("receipt_avail_remove_flag").value = "Y";
    }
    else if(statusArray[i] == 'S'
      || statusArray[i] == 'P')
    {
      document.getElementById("receipt_avail_remove_flag").value = "Y";
      document.getElementById("receipt_avail_scrub_flag").value = "N";
    }
  }

  /* determine if we display the alternate contact link or not */
  if((alt_first_name == "" || alt_first_name == undefined) && (alt_last_name == "" || alt_last_name == undefined) &&
     (document.getElementById("emailTD").altEmail == "" || document.getElementById("emailTD").altEmail == undefined) &&
     (alt_phone == "" || alt_phone == undefined) && (alt_ext == "" || alt_ext == undefined))
  {
    $('#altContactLink').hide();
  }
  else{
    $('#altContactLink').show();
  }

  // reset the flag used in modify order to denote that the order details info has changed
  document.getElementById("modify_order_updated").value = "";

  if( skipCOM )
  {
    alert("You are viewing a Target Order.  Modifications are not allowed. Direct the consumer/recipient to contact Target directly.");
  }

  // Pop-up Call Disposition widget if necessary
  cdisp_autoPopup();
}


/***********************************************************************************
* checkKey()
************************************************************************************/
function checkKey()
{
  var tempKey = window.event.keyCode;

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cai_show_first == 'y')
    doPageAction(1);

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cai_show_last == 'y')
    doPageAction(2);

  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cai_show_next == 'y')
    doPageAction(3);

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cai_show_previous == 'y')
    doPageAction(0);

}


/*
  The following functions are buttons that exist on the itemDiv.xsl
*/

/*******************************************************************************
Function makes a check to see if the lock associated with the customer_id
that is passed in is locked. (See updateOrderIFrame.xsl) If the records is locked
updateOrderIFrame.xsl displays a message stating who has it locked, if it is not
locked updateOrderIFrame.xsl will call the doUpdateOrderInfoAction()

    doUpdateOrderInfo()

*******************************************************************************/
var updateDelOrderId = '';
var updateDelExtOrderNum = '';
function doUpdateOrderInfo(	order_detail_id, ftp_vendor_product, order_held, hold_reason,
				vendorFlag, productId, order_date, mercuryFlag, floristId, external_order_number, personalization_template_id, isFreeShipProduct, orderHasMorningDelivery)
{
	var orderDate = new Date(order_date);
	var todayDate = new Date();
	var oneday=1000*60*60*24; // length of one day in millisecs
	var orderDay = Math.ceil(orderDate.getTime()/oneday);
	var todayDay = Math.ceil(todayDate.getTime()/oneday);
	var days = todayDay - orderDay;
	

  var modalArguments = new Object();
	//-----Validation Rule 2.1----- The system will not allow Wal-Mart orders to be updated through the Update order application.
  if ( isFreeShipProduct == 'Y')
  {
    modalArguments.modalText = WARNING_FREESHIP_PRODUCT;
  }
	//-----Validation Rule 2.2----- The system will not allow Amazon orders to be updated through the Update order application.
	else if ( isAmazonOrder )
  {
    modalArguments.modalText = WARNING_AMZNI_ORDER;
  }
  	else if ( partnerName!= '' && partnerName == 'Mercent' )
  {
    modalArguments.modalText = partnerChannelName +WARNING_MRCNT_ORDER;
  }
  	else if ( partnerName!= '' && partnerName == 'Partner'  &&
  	    modifyOrderAllowed != '' && modifyOrderAllowed == 'false')
  {
    modalArguments.modalText = partnerChannelName +WARNING_PARTNER_ORDER;
  }     
  else if ( isWalmartOrder )
  {
    modalArguments.modalText = WARNING_WLMTI_ORDER;
  }
	//-----Validation Rule 2.3----- The system will not allow Ariba orders to be updated through the Update order application.
	else if ( isAribaOrderARI || isAribaOrderCAT )
  {
    modalArguments.modalText = WARNING_ARIBA_ORDER;
  }
	//-----Validation Rule 2.9----- The system will not allow Non-Venus vendor orders to be updated through the Update order application.
  else if (ftp_vendor_product != 'N' )
  {
  		modalArguments.modalText = "This vendor order cannot be updated";
  }
  else if ( personalization_template_id != 'NONE' && personalization_template_id != '' )
  {
    modalArguments.modalText = WARNING_PERSONALIZATION_PRODUCT;
  }
	//-----Validation Rule 2.13----- The system will not allow orders that are currently in the LP queue to be updated through the Update order application.
  else if ( order_held == 'Y' && ( hold_reason == 'Fraud' || hold_reason == 'FRAUD_FLAG' || hold_reason == 'LP_INDICATOR' || hold_reason == 'Preferred' ))
  {
    modalArguments.modalText = WARNING_FRAUD;
  }
	//-----Validation Rule ???----- San Francisco Music Box orders cannot be updated.
  else if ( isSFMB )
  {
    modalArguments.modalText = WARNING_SFMB;
  }
	//-----Validation Rule ???----- JCPenney orders cannot be updated.
  else if ( isJCP )
  {
    modalArguments.modalText = WARNING_JCP;
  }
	//-----Validation Rule 2.18----- The system will not allow updates to orders that are more than 4 months past the order date
  else if ( days > 120 )
  {
    modalArguments.modalText = "Order is more than 4 months old and cannot be updated.";
  }
  //-----Validation Rule Blooms by Noon - Defect 11946 / Use Case 23998 - Apollo System does not allow BBN orders to be updated in Update Order in COM
  else if ( orderHasMorningDelivery == 'Y' )
  {
    modalArguments.modalText = "This order does not allow for any updates since it includes Morning Delivery.";
  }
  else
  {
    updateDelOrderId = order_detail_id;
    updateDelExtOrderNum = external_order_number;

    var frame = document.getElementById('modifyOrderValidation');
    var url = "modifyOrderValidation.do?order_detail_id=" + updateDelOrderId +
                                                "&vendor_flag=" + vendorFlag +
                                                "&product_id=" + productId +
                                                "&partner_name=" + partnerChannelName +
                                                "&mercury_flag=" + mercuryFlag +
                                                "&florist_id=" + floristId +
                                                "&external_order_number=" + external_order_number +
    											getSecurityParams(false) +
    											"&t=" + new Date().getTime(); //make the url unique so IE won't return a cached response.  stupid IE.
    frame.src = url;
  }

  if ( modalArguments.modalText != null )
  {
    window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
  }
}


/*
 continue processing to lock order
*/
function modifyOrderValidated()
{
    var cont = true;
    //If order payment type is PayPal, alert the user
    if(isAltPay)
    {
      var modalArguments = new Object();
      modalArguments.modalText = 'This order has a payment type of ' + paymentTypeDesc + ' .  Please contact the customer for their credit card number before proceeding.';
      cont = window.showModalDialog("continue.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
    }

    if(cont)
    {
    //alert('getting lock for order detail '+ $('#order_detail_id').val());
	doRecordLock('MODIFY_ORDER','retrieve','updateOrder', "csci", $('#order_detail_id').val());
    }
}



/*
 continue processing to udateDeliveryInfo
*/
function doProceedUpdateOrder(){
     var url = moJoeUrl + '?context=Order Proc&securitytoken=' + document.getElementById("securitytoken").value;
     // alert("Going to JOE with order: " + updateDelOrderId + " " + updateDelExtOrderNum);  
     
     document.forms["csci"].action = url;
     document.forms["csci"].method = "POST";
     document.forms["csci"].orig_order_detail_id.value = updateDelOrderId; 
     document.forms["csci"].orig_ext_order_num.value = updateDelExtOrderNum;
     document.forms["csci"].submit();
}

/*
 continue processing to lock order
*/
function updateDeliveryDateValidated()
{
        doRecordLock('MODIFY_ORDER','retrieve','updateDeliveryDate', "csci");
}


/*
 continue processing to updateDeliveryDate
 '{customer_id}','{master_order_number}','{order_guid}''{order_detail_id}',
 '{external_order_number}','{ship_method}','{vendor_flag}','{delivery_date}','{delivery_date_range_end}',
 '{state}','{zip_code}','{country}','{product_id}','{occasion}','{origin_id}'
*/
function doUpdateDeliverDate(order_detail_id,external_order_number,ship_method,vendorFlag,deliveryDate,deliveryDateRangeEnd,state,zip_code,country,product_id,occasion,origin_id){
     var url = "loadDeliveryDate.do"
                + "?customer_id=" + document.getElementById('customer_id').value
                + "&master_order_number=" + document.getElementById('master_order_number').value
                + "&order_guid=" + document.getElementById('order_guid').value
                + "&order_detail_id=" + order_detail_id
                + "&external_order_number=" + external_order_number
                + "&ship_method=" + ship_method
                + "&vendor_flag=" + vendorFlag
                + "&delivery_date=" + deliveryDate
                + "&delivery_date_range_end=" + deliveryDateRangeEnd
                + "&recipient_state=" + state
                + "&recipient_zip_code=" + zip_code
                + "&recipient_country=" + country
                + "&product_id=" + product_id
                + "&occasion=" + occasion
                + "&origin_id=" + origin_id
                + "&orig_product_id=" + product_id
                + getSecurityParams(false);
     performAction(url);
}



/*
  Executed from a button click on the itemDiv.xsl
         Update Florist Info
        doUpdateFloristAction()
*/
function doUpdateFloristAction(ordid,deliveryDate,recipZipCode){
    queOrderId = ordid;

    //we only want to compare the date parms, not the time parameters. Therefore, format the date.

    //format the delivery date in yyyymmdd format
    var delDate = new Date(deliveryDate);
    var delYear = delDate.getYear();
    if(delYear<1000)
      delYear+=1900;
    var delMonth = delDate.getMonth()+1;
    var delDay = delDate.getDate();
    var formattedDelDate = delYear.toString();
    if (delMonth < 10)
      formattedDelDate += '0';
    formattedDelDate += delMonth.toString();
    if (delDay < 10)
      formattedDelDate += '0';
    formattedDelDate += delDay.toString();


    //format today date in yyyymmdd format
    var todayDate = new Date();
    var todayYear = todayDate.getYear();
    if(todayYear<1000)
      todayYear+=1900;
    var todayMonth = todayDate.getMonth()+1;
    var todayDay = todayDate.getDate();
    var formattedTodayDate = todayYear.toString();
    if (todayMonth < 10)
      formattedTodayDate += '0';
    formattedTodayDate += todayMonth.toString();
    if (todayDay < 10)
      formattedTodayDate += '0';
    formattedTodayDate += todayDay.toString();


    //compare the formatted today and delivery dates
    if(formattedDelDate < formattedTodayDate)
    {
      alert('You are updating florist past the delivery date.');
//      alert('You cannot update florist past the delivery date.');
//      return;
    }


    var url = 'customerOrderSearch.do?action=check_update_florist&order_detail_id=' + ordid + '&recip_zip_code=' + recipZipCode + getSecurityParams(false);
    var frame = document.getElementById('updateOrderFrame');
    frame.src = url;
}

/**********************************************************************
Function to check if the order is eligible to Phoenix or not
***********************************************************************/
function doPhoenixOrderAction(ordid, externalOrderNumber, order_held, hold_reason)
{
	if ( order_held == 'Y' && ( hold_reason == 'Fraud' || hold_reason == 'FRAUD_FLAG' || hold_reason == 'LP_INDICATOR' || hold_reason == 'Preferred' ))
  	{
  		var modalArguments = new Object();
    	modalArguments.modalText = "This order is not Phoenix eligible";
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');		
		return;
  	}
    var tempAddonString = '';
    for (var i=0; i<orderAddOns.length; i++) {
        if (tempAddonString == '') {
            tempAddonString = orderAddOns[i][1];
        } else {
            tempAddonString = tempAddonString + ',' + orderAddOns[i][1];
        }
    }
    var url = 'phoenixOrderValidation.do?order_detail_id=' + ordid + '&external_order_number=' + externalOrderNumber + '&addons=' + tempAddonString + getSecurityParams(false);
    var frame = document.getElementById('phoenixOrderFrame');
    frame.src = url;
}

function doPhoenixOrder(ordid,phoenixProductId,deliveryDate,zipcode,shipMethod,shipDate,isOrigNewProdSame,externalOrderNumber,addons,sourceCode,companyId,originId)
{
  var url = 'phoenixOrder.do?action=load&order_detail_id='+ordid+'&phoenix_product_id=' + phoenixProductId 
  	+ '&recip_zip_code='+ zipcode +'&delivery_date='+ deliveryDate +'&ship_method='+ shipMethod +'&ship_date='+ shipDate
  	+'&isOrigNewProdSame='+isOrigNewProdSame +'&externalOrderNumber='+externalOrderNumber + '&addons='+addons + '&sourceCode='+sourceCode + '&companyId='+companyId + '&originId='+originId + getSecurityParams(false);
  performAction(url);
}


/*******************************************************************************
Function makes a check to see if the lock associated with the customer_id
that is passed in is locked. (See updateOrderDeliveryDateIFrame.xsl) If the records is locked
updateOrderDeliveryDateIFrame.xsl displays a message stating who has it locked, if it is not
locked updateOrderDeliveryDateIFrame.xsl will call the doUpdateOrderInfoAction()

    doUpdateDeliveryDate()

*******************************************************************************/
var updateDDOrderId  = '';
var updateDDVndrFlag = '';
var updateDDFloristId = '';
var updateDDShipMethod = '';
var updateDDSubCode = '';

function doUpdateDeliveryDate(	order_detail_id, ftp_vendor_product, vendorFlag, productId, mercuryFlag, floristId, shipMethod, subcode, personalization_template_id, isFreeShipProduct , product_type)
{
  var modalArguments = new Object();
  //-----Validation Rule ----- The system will not allow Non-Venus vendor orders to be updated through the Update order application.
  if ( isFreeShipProduct == 'Y')
  {
    modalArguments.modalText = WARNING_FREESHIP_PRODUCT;
  }
  else if ( ftp_vendor_product != 'N' )
  {
    modalArguments.modalText = "Orders for this Vendor cannot be updated.";
  }
  else if ( personalization_template_id != 'NONE' && personalization_template_id != '' )
  {
    modalArguments.modalText = WARNING_PERSONALIZATION_PRODUCT;
  }
  
  else if(product_type == "SDFC")
  {
    modalArguments.modalText = WARNING_SDFC_PRODUCT_SELECTED;
  }
  
  
  else
  {
    updateDDOrderId = order_detail_id;
    updateDDVndrFlag = vendorFlag;
    updateDDFloristId = floristId;
    updateDDShipMethod = shipMethod;
    updateDDSubCode = subcode;
    var frame = document.getElementById('deliveryDateValidation');
    var url = "deliveryDateValidation.do?order_detail_id=" + updateDDOrderId	+
  						"&vendor_flag=" + vendorFlag	+
  						"&product_id=" + productId 	+
  						"&mercury_flag=" + mercuryFlag 	+
  						"&florist_id=" + updateDDFloristId +
  						 getSecurityParams(false);

    frame.src = url;
  }

  if ( modalArguments.modalText != null )
  {
    window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
  }
}

/*
 continue processing to updateDeliveryDate
*/
function doProceedUpdateDeliveryDate(){
    var tempAddonString = '';
    for (var i=0; i<orderAddOns.length; i++) {
        if (tempAddonString == '') {
            tempAddonString = orderAddOns[i][1];
        } else {
            tempAddonString = tempAddonString + ',' + orderAddOns[i][1];
        }
    }
    var url = "loadDeliveryDate.do"
                + "?order_detail_id=" + updateDDOrderId +
                "&vendor_flag=" + updateDDVndrFlag	+
                "&florist_id=" + updateDDFloristId +
                "&ship_method=" + updateDDShipMethod +
                "&subcode=" + updateDDSubCode +
                "&origin_id=" + document.getElementById("origin_id").value +
                "&addons=" + tempAddonString +
                getSecurityParams(false);
     performAction(url);

}


/*
 If checkUpdateFloristIFrame.xsl is successful
 this fucntion is executed
*/
function doObtainLockAndProceedToQueue(queue, recipZip){
  queueId = queue; /*set global queue-id to retrieve after lock processing */
  recipZipCode = recipZip; /*set global recipZipCode to retrieve after lock processing */
  doRecordLock('MODIFY_ORDER','retrieve','updateFlorist', "csci");
}

/*
 Display queue page
*/
function doDisplayQueuePopUp(){
  var args = new Array();
  args.action = "updateFloristForm.do";
  args[args.length] = new Array("order_detail_id", queOrderId);
  args[args.length] = new Array("queue_id", queueId);
  args[args.length] = new Array("recip_zip_code", recipZipCode);
  args[args.length] = new Array("msg_message_type", "Mercury");
  args[args.length] = new Array("destination","recipient_order_page");
  args[args.length] = new Array("msg_guid",document.getElementById("order_guid").value);
  args[args.length] = new Array("msg_external_order_number",document.getElementById("searched_on_external_order_number").value);
  args[args.length] = new Array("msg_customer_id",document.getElementById("customer_id").value);
  args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
  args[args.length] = new Array("context", document.getElementById("context").value);
  args[args.length] = new Array("source_code", cart_source_code);


  var modal_dim = "dialogWidth:570px; dialogHeight:275px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes;";
  var forwardUrl = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);
  if(forwardUrl!=null&&forwardUrl!=''){
    document.forms[0].action=forwardUrl;
    document.forms[0].submit();
  }
}

/*******************************************************************************

    doUpdatePaymentInfo()

*******************************************************************************/
var payOrderId = '';
var payExtNum = '';
var paySrcCode = '';

function doUpdatePaymentInfo(ordid,extnum,srccode){

   payOrderId = ordid;
   payExtNum = extnum;
   paySrcCode = srccode;

   if(isAuthorized == 'false'){
     alert('Shopping Cart has not been authorized therefore billing cannot be updated.');
     return;
   }

   if(isRestricted == 'true')
   {
   	 var modalArguments = new Object();
   	 if ( partnerName!= '' && partnerName == 'Mercent' ){
   	 	modalArguments.modalText = partnerChannelName +WARNING_MRCNT_ORDER;
   	 }
   	 else if ( partnerName!= '' && partnerName == 'Partner' ){
   	 	modalArguments.modalText = partnerChannelName +WARNING_PARTNER_PAYMENT;
   	 }
   	 else {
     modalArguments.modalText = WARNING_AMZNI_ORDER;
     }
	 window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');     
     return;
   }
   if(isAccountProgramExpired == 'true')
   {
     alert('This auto-renewal order for '+programName+' cannot be processed because the customer\'s membership has expired.\nPlease cancel the order.');
     return;
   }
  doRecordLock('PAYMENTS','retrieve','updatePayment', "csci");
}

/*
 After obtaining the lock proceed
 to updateBilling page "order level"
*/
function doProceedUpdatePaymentInfo(){
   var url = 'updateBilling.do?order_detail_id=' + payOrderId + '&external_order_number=' + payExtNum + '&page_action=search' + '&source_code=' + paySrcCode;
  performAction(url);
}

/*
 Executed by the Update Billing Info
 button on the shopping cart page
*/
function doUpdateBillingInfo(){

	if (isAmazonOrder) {
		var modalArguments = new Object();
		modalArguments.modalText = WARNING_AMZNI_ORDER;
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');		
		return;
	}
	if ( partnerName!= '' && partnerName == 'Mercent' ) {
		var modalArguments = new Object();
		modalArguments.modalText = partnerChannelName +WARNING_MRCNT_ORDER;
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');		
		return;
	}
	if ( partnerName!= '' && partnerName == 'Partner' ) {
		var modalArguments = new Object();
		modalArguments.modalText = partnerChannelName +WARNING_PARTNER_ORDER;
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');		
		return;
	}
   if(isAuthorized == 'true')
   {
     alert('Payment is authorized and cannot be updated');
     return;
   }

   if(isRestricted == 'true')
   {
   	 var modalArguments = new Object();
     modalArguments.modalText = WARNING_AMZNI_ORDER;
	 window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
     return;
   }
   if(isAccountProgramExpired == 'true')
   {
     alert('This auto-renewal order for '+programName+' cannot be processed because the customer\'s membership has expired.\nPlease cancel the order.');
     return;
   }
   doGUIDRecordLock('PAYMENTS','retrieve','updateBilling', "csci");
}


/*
	Displays the alert for Amazon orders. 
	And calls the doLoadCarEmail for other orders (util.js).
*/
function doLoadCartEmailList() {
	if (isAmazonOrder) {
		var modalArguments = new Object();
		modalArguments.modalText = "Order is an Amazon order and cannot be updated.";
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');		
		return;
	}
	doLoadCartEmail();
}

/*
	Displays the alert for Amazon orders. 
	And calls the doUpdateCustomerAction(iframeName, count) in utils.js. 
*/
function doUpdateCustomerActionShoppingCart(iframeName,count) {
	if (isAmazonOrder) {
		var modalArguments = new Object();
		modalArguments.modalText = "Order is an Amazon order and cannot be updated.";
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');		
		return;
	}	
	doUpdateCustomerAction(iframeName,count);
}

/*
 Executed after the doUpdateBillingInfo()
 method completes retrieving the lock
 takes the user to updateBilling page
*/
function doProceedUpdateBillingInfo(){
   var url = 'updateBilling.do?order_guid=' + document.getElementById("order_guid").value +
             '&external_order_number=' + '&master_order_number=' +  document.getElementById('master_order_number').value + '&page_action=search' +
             '&billing_level=cart&source_code=' + cart_source_code;
  performAction(url);
}


/*
 This function is called from checkLockIFrame.xsl
 If there were no problems with locking this function is called
*/

function checkAndForward(forwardAction){
   if(forwardAction == 'updateOrder'){
      doProceedUpdateOrder();
   }
   if(forwardAction == 'updateFlorist'){
      doDisplayQueuePopUp();
   }
   if(forwardAction == 'updatePayment'){
      doProceedUpdatePaymentInfo();
   }
   if(forwardAction == 'updateBilling'){
      doProceedUpdateBillingInfo();
   }
   if(forwardAction == 'updateDeliveryDate'){
      doProceedUpdateDeliveryDate();
   }
}


/*******************************************************************************
*
*    performAction()
*
*******************************************************************************/
function performAction(url){
    /*
      Display a message to the user called Processing...
      this allows the user to know that the system is working
      and not stalled out.
      scroll(0,0) returns the window to the top position since
      all(most) buttons are located at the bottom of a scrolled window
    */
    scroll(0,0);
    showWaitMessage("mainContent", "wait", "Processing");

     document.forms[0].action = url;
     document.forms[0].submit();
}


/***********************************************************************************
* doPageAction()
************************************************************************************/
      function doPageAction(val){
        var pageAction = new Array("ca_csc_first","ca_csc_previous","ca_csc_next","ca_csc_last");
        var url = "customerOrderSearch.do" + "?action=" +
            pageAction[val];

        performAction(url);
      }

/***********************************************************************************
* doUpdateCustomerEmail()
************************************************************************************/
function doUpdateCustomerEmail(){

  var modalFeatures  =  'dialogWidth:800px;dialogHeight:420px;center:yes;status:no;help:no;';
      modalFeatures +=  'resizable:no;scroll:no';

  var modalUrl =  'updateCustomerEmail.do?action=get_email_info' + getSecurityParams(false);
      modalUrl += '&uc_customer_id=' + document.getElementById('customer_id').value;
      modalUrl += '&uce_email_address=' + document.getElementById("emailTD").email;;
      modalUrl += '&page_action=update';

  ret = showModalDialogIFrame(modalUrl, "", modalFeatures);

}

/*************************************************************************************
* Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
    if (!cdisp_showWidgetIfRequired()) {   // Display Call Disposition widget if required, otherwise perform action
        var url = "customerOrderSearch.do?action=load_page";
        performAction(url);
    }
}

/**************************************************************
* updatePrivacyPolicyVariables()
**************************************************************/
function updatePrivacyPolicyVariables(newCustomersValidated, newOrdersValidated, newBypassPrivacyPolicyVerification, customerValidationRequiredFlag, orderValidationRequiredFlag)
{
  if (customerValidationRequiredFlag == 'Y' || orderValidationRequiredFlag == 'Y')
  {
    this.customersValidated = newCustomersValidated;
    this.ordersValidated = newOrdersValidated;
    document.getElementById('bypass_privacy_policy_verification').value = newBypassPrivacyPolicyVerification;
  }
}

/**************************************************************
* displayCartFeesOnMouseOver()
**************************************************************/

function displayCartFeesOnMouseOver(xpos,ypos)
{

  var serviceFeeForCart       = document.getElementById('serviceFeeForCart').innerHTML;
  var shippingFeeForCart      = document.getElementById('shippingFeeForCart').innerHTML;
  var satUpchargeFeeForCart   = document.getElementById('satUpchargeFeeForCart').innerHTML;
  var alHISurchargeFeeForCart = document.getElementById('alHISurchargeFeeForCart').innerHTML;
  var fuelSurchargeFeeForCart = document.getElementById('fuelSurchargeFeeForCart').innerHTML;
  var applySurchargeCodeForCart = document.getElementById('applySurchargeCodeForCart').innerHTML;
  var sameDayUpchargeForCart = document.getElementById('sameDayUpchargeForCart').innerHTML;
  var morningDeliveryFeeForCart = document.getElementById('morningDeliveryFeeForCart').innerHTML;
  var sunUpchargeFeeForCart = document.getElementById('sunUpchargeFeeForCart').innerHTML;
  var monUpchargeFeeForCart = document.getElementById('monUpchargeFeeForCart').innerHTML;
  var lateCutoffFeeForCart = document.getElementById('lateCutoffFeeForCart').innerHTML;
  var internationalFeeForCart = document.getElementById('internationalFeeForCart').innerHTML;

  var serviceFeeString = "" ;
  var shippingFeeString = "" ;
  var alHISurchargeFeeString = "";
  var satUpchargeFeeString = "" ;
  var fuelSurchargeFeeString = "" ;
  var sameDayUpchargeString = "";
  var morningDeliveryFeeString = "";
  var sunUpchargeFeeString = "";
  var monUpchargeFeeString = "";
  var lateCutoffFeeString = "";
  var internationalFeeString="";

  for (var i=0; i<orderDetailBills.length; i++) {		
				        
		var itemDetailId = orderDetailBills[i][0];
		var itemServiceFee = orderDetailBills[i][1];
		var itemShippingFee = orderDetailBills[i][2];
		var itemVendSatUpcharge = orderDetailBills[i][3];
		var itemAkHiSpecialCharge = orderDetailBills[i][4];
		var itemFuelSurchargeAmt = orderDetailBills[i][5];
		var itemSameDayUpcharge = orderDetailBills[i][6];
		var itemMorningDeliveryFee = orderDetailBills[i][7];		        
		var itemVendSunUpcharge = orderDetailBills[i][8];
		var itemVendMonUpcharge = orderDetailBills[i][9];
		var itemLateCutoffFee = orderDetailBills[i][10];
		var itemInternationalFee = orderDetailBills[i][11];
		
		if ( (!IsEmpty(morningDeliveryFeeForCart)) && morningDeliveryFeeForCart > 0)
		  {
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemMorningDeliveryFee;
				  morningDeliveryFeeString = "Morning Delivery: " +  formatAmount2DecimalPlaces(morningDeliveryFeeForCart) + "<BR>";
			}
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart- itemMorningDeliveryFee;
				morningDeliveryFeeString =  "Morning Delivery: " +  formatAmount2DecimalPlaces(morningDeliveryFeeForCart) + "<BR>" ;
			}
		  }		
		
		if ( (! IsEmpty(fuelSurchargeFeeForCart)) && fuelSurchargeFeeForCart > 0 && applySurchargeCodeForCart == 'ON' )
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart- itemFuelSurchargeAmt;
				fuelSurchargeFeeString =  surchargeDescriptionForCart + ": " +  formatAmount2DecimalPlaces(fuelSurchargeFeeForCart) + "<BR>" ;
			}
			else if (itemServiceFee > 0)
			{
				serviceFeeForCart = serviceFeeForCart - itemFuelSurchargeAmt;
				fuelSurchargeFeeString =  surchargeDescriptionForCart + ": " +  formatAmount2DecimalPlaces(fuelSurchargeFeeForCart) + "<BR>" ;
			}
		  }
		 
		if ((! IsEmpty(alHISurchargeFeeForCart)) && alHISurchargeFeeForCart > 0)
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart - itemAkHiSpecialCharge;
				alHISurchargeFeeString = "Alaska/Hawaii Upcharge: " +  formatAmount2DecimalPlaces(alHISurchargeFeeForCart) + "<BR>";
			}
			if (itemServiceFee > 0)
			{
				serviceFeeForCart = serviceFeeForCart - itemAkHiSpecialCharge;
				alHISurchargeFeeString = "Alaska/Hawaii Upcharge: " +  formatAmount2DecimalPlaces(alHISurchargeFeeForCart) + "<BR>";
			}
		  }
  
		if ( (!IsEmpty(satUpchargeFeeForCart)) && satUpchargeFeeForCart > 0)
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart - itemVendSatUpcharge;
				satUpchargeFeeString = "Saturday Upcharge: " +  formatAmount2DecimalPlaces(satUpchargeFeeForCart) +"<BR>";
			}
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemVendSatUpcharge;
				  satUpchargeFeeString = "Saturday Upcharge: " +  formatAmount2DecimalPlaces(satUpchargeFeeForCart) +"<BR>";
			}
		  }

		if ( (!IsEmpty(sameDayUpchargeForCart)) && sameDayUpchargeForCart > 0)
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart- itemSameDayUpcharge;
				sameDayUpchargeString = "Same Day Fee: " +  formatAmount2DecimalPlaces(sameDayUpchargeForCart) + "<BR>";
			}
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemSameDayUpcharge;
				  sameDayUpchargeString = "Same Day Fee: " +  formatAmount2DecimalPlaces(sameDayUpchargeForCart) + "<BR>";
			}
		  }			
		  
		  //DI-24
		  if ( (!IsEmpty(lateCutoffFeeForCart)) && lateCutoffFeeForCart > 0)
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart - itemLateCutoffFee;
				lateCutoffFeeString = "Late Cutoff: " +  formatAmount2DecimalPlaces(lateCutoffFeeForCart) +"<BR>";
			}
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemLateCutoffFee;
				  lateCutoffFeeString = "Late Cutoff: " +  formatAmount2DecimalPlaces(lateCutoffFeeForCart) +"<BR>";
			}
		  }
		  
		  if ( (!IsEmpty(sunUpchargeFeeForCart)) && sunUpchargeFeeForCart > 0)
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart - itemVendSunUpcharge;
				sunUpchargeFeeString = "Sunday Upcharge: " +  formatAmount2DecimalPlaces(sunUpchargeFeeForCart) +"<BR>";
			}
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemVendSunUpcharge;
				  sunUpchargeFeeString = "Sunday Upcharge: " +  formatAmount2DecimalPlaces(sunUpchargeFeeForCart) +"<BR>";
			}
		  }
		  
		  if ( (!IsEmpty(monUpchargeFeeForCart)) && monUpchargeFeeForCart > 0)
		  {
			if (itemShippingFee > 0)
			{
				shippingFeeForCart = shippingFeeForCart - itemVendMonUpcharge;
				monUpchargeFeeString = "Monday Upcharge: " +  formatAmount2DecimalPlaces(monUpchargeFeeForCart) +"<BR>";
			}
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemVendMonUpcharge;
				  monUpchargeFeeString = "Monday Upcharge: " +  formatAmount2DecimalPlaces(monUpchargeFeeForCart) +"<BR>";
			}
		  }
		  
		    if ( (!IsEmpty(internationalFeeForCart)) && internationalFeeForCart > 0)
		  {
			if (itemServiceFee > 0)
			{
				  serviceFeeForCart = serviceFeeForCart - itemInternationalFee;
				  internationalFeeString = "International Fee: " +  formatAmount2DecimalPlaces(internationalFeeForCart) +"<BR>";
			}
		  }
		  
		  
	}
  
  serviceFeeForCart  = Math.round(serviceFeeForCart*100)/100;
  shippingFeeForCart = Math.round(shippingFeeForCart*100)/100;

  if ( (!IsEmpty(serviceFeeForCart)) && (serviceFeeForCart > 0))
  {
    serviceFeeString = "Service Fee: " + formatAmount2DecimalPlaces(serviceFeeForCart) +"<BR>";
  }

  if ( (!IsEmpty(shippingFeeForCart)) && (shippingFeeForCart > 0) )
  {
    shippingFeeString = "Shipping Fee: " +  formatAmount2DecimalPlaces(shippingFeeForCart) +"<BR>";
  }

  var mouseOverString  =  serviceFeeString + shippingFeeString + satUpchargeFeeString + alHISurchargeFeeString + sameDayUpchargeString + fuelSurchargeFeeString + morningDeliveryFeeString + lateCutoffFeeString + sunUpchargeFeeString + monUpchargeFeeString + internationalFeeString;
  if (mouseOverString != "" )
  {
    $("#spanMouseOver").html(mouseOverString);
    $("#spanMouseOver")[0].style.top=ypos+10;
    $("#spanMouseOver")[0].style.left=xpos+10;
    $("#spanMouseOver").show();
    // hack for IE7 issue where space appears under the tabs when you mouse over
    $("iframe.hidden").attr("style", "display:none");
    $("div.visible").attr("style", "margin:0px");
  }

}

function displayCartTaxesOnMouseOver(xpos,ypos) {
    var mouseOverString = "";
    for (var i=0; i<cartTaxes.length; i++) {
        if (cartTaxes[i][1] != '' && cartTaxes[i][1] > 0) {
            mouseOverString = mouseOverString + cartTaxes[i][0] + ': ';
         	mouseOverString = mouseOverString + formatDollarAmt(cartTaxes[i][1]) + '<br>';
     	}
	}
    if (mouseOverString != "" ) {
        $("#spanTaxMouseOver").html(mouseOverString);
        $("#spanTaxMouseOver")[0].style.top=ypos+15;
        $("#spanTaxMouseOver")[0].style.left=xpos+10;
        $("#spanTaxMouseOver").show();
    }
   // hack for IE7 issue where space appears under the tabs when you mouse over
   $("iframe.hidden").attr("style", "display:none");
   $("div.visible").attr("style", "margin:0px");

}

function displayOrderTaxesOnMouseOver(orderDetailId, object, e) {
    var xpos = e.clientX;
    var ypos = e.clientY;
    var mouseOverString = "";
    var mouseOverStingTotal = "";
    for (var i=0; i<orderTaxes.length; i++) {
        if (orderTaxes[i][0] == orderDetailId) { // order_detail_id
            if (orderTaxes[i][2] != '' && orderTaxes[i][2] > 0) { // tax_amount            	
            	if(orderTaxes[i][3] == 'Y') { // is_total 
            		mouseOverStingTotal = mouseOverStingTotal + orderTaxes[i][1] + ': ';
    				mouseOverStingTotal = mouseOverStingTotal + formatDollarAmt(orderTaxes[i][2]) + '<br>';
            	} else {	            	
	                mouseOverString = mouseOverString + orderTaxes[i][1] + ': ';
	    			mouseOverString = mouseOverString + formatDollarAmt(orderTaxes[i][2]) + '<br>';
	    		}
			}
	    }
	}
	
	 if (mouseOverString == "" && mouseOverStingTotal != "") {
	 	mouseOverString = mouseOverStingTotal;
	 }
	
    if (mouseOverString != "" ) {
        $("#spanOrderTaxMouseOver_" + orderDetailId).html(mouseOverString);
        $("#spanOrderTaxMouseOver_" + orderDetailId)[0].style.top=ypos+15;
        $("#spanOrderTaxMouseOver_" + orderDetailId)[0].style.left=xpos-50;
        $("#spanOrderTaxMouseOver_" + orderDetailId).show();
    }
   // hack for IE7 issue where space appears under the tabs when you mouse over
   $("iframe.hidden").attr("style", "display:none");
   $("div.visible").attr("style", "margin:0px");
}


function displayOrderAddOnsOnMouseOver(orderDetailId, object, e) {
    var xpos = e.clientX;
    var ypos = e.clientY;
    var mouseOverString = "";
    var nbsp = " ";
    var discountAmount = 0;
    var salePrice = 0;
    
    for (var i=0; i<orderAddOns.length; i++) {
        if (orderAddOns[i][0] == orderDetailId) { // order_detail_id
                mouseOverString = mouseOverString + orderAddOns[i][2] + ' ' + orderAddOns[i][3] + ': ' ;
                
                if (orderAddOns[i][4] != null && orderAddOns[i][4] != "" && orderAddOns[i][4] != 0) {
                	salePrice = (orderAddOns[i][2] * orderAddOns[i][4]);
                }
                else if (orderAddOns[i][5] != null && orderAddOns[i][5] != "") {
         			// dont do anything		
         		}
                else {
                	salePrice = (orderAddOns[i][2] * orderAddOns[i][6]);
                }
                if (orderAddOns[i][5] != null && orderAddOns[i][5] != "" ) {
                	discountAmount = (orderAddOns[i][2] * orderAddOns[i][5]);
                }
                mouseOverString = mouseOverString + formatAmount2DecimalPlaces(parseFloat(salePrice.toPrecision(4)) + parseFloat(discountAmount.toPrecision(4))) +  '<br>';
	    }
	}
	
	
    if (mouseOverString != "" ) {
        $("#spanOrderAddOnsMouseOver_" + orderDetailId).html(mouseOverString);
        $("#spanOrderAddOnsMouseOver_" + orderDetailId)[0].style.top=ypos+15;
        $("#spanOrderAddOnsMouseOver_" + orderDetailId)[0].style.left=xpos-50;
        $("#spanOrderAddOnsMouseOver_" + orderDetailId).show();
		// hack for IE7 issue where space appears under the tabs when you mouse over
		$("iframe.hidden").attr("style", "display:none");
		$("div.visible").attr("style", "margin:0px");
    }

}


function displayOrderDiscountOnMouseOver(orderDetailId, object, e) {
    var xpos = e.clientX;
    var ypos = e.clientY;
    var mouseOverString = "";
    var nbsp = " ";
    var discountAmount = 0;
    var salePrice = 0;
    
    for (var i=0; i<orderDiscount.length; i++) {
        if (orderDiscount[i][0] == orderDetailId) { // order_detail_id
                if (orderDiscount[i][1] == 'Original' && orderDiscount[i][2] != null && orderDiscount[i][2] != '' && orderDiscount[i][2] > 0) {
                	mouseOverString = mouseOverString + "Merchandise" + ': ' ;
                	mouseOverString = mouseOverString + "(" + formatAmount2DecimalPlaces(orderDiscount[i][2]) + ")" +  '<br>';	
                }    			
	    }
	}
    
    for (var i=0; i<orderAddOns.length; i++) {
        if (orderAddOns[i][0] == orderDetailId) { // order_detail_id
                if (orderAddOns[i][5] != null && orderAddOns[i][5] != "" && orderAddOns[i][5] > 0) {
                	mouseOverString = mouseOverString + orderAddOns[i][2] + ' ' + orderAddOns[i][3] + ': ' ;
                	discountAmount = (parseFloat(orderAddOns[i][2]) * parseFloat(orderAddOns[i][5]));
                	mouseOverString = mouseOverString + "(" + formatAmount2DecimalPlaces(discountAmount) + ")" +  '<br>';
                }
	    }
	}
	
	
    if (mouseOverString != "" ) {
        $("#spanOrderDiscountMouseOver_" + orderDetailId).html(mouseOverString);
        $("#spanOrderDiscountMouseOver_" + orderDetailId)[0].style.top=ypos+15;
        $("#spanOrderDiscountMouseOver_" + orderDetailId)[0].style.left=xpos-50;
        $("#spanOrderDiscountMouseOver_" + orderDetailId).show();
		// hack for IE7 issue where space appears under the tabs when you mouse over
		$("iframe.hidden").attr("style", "display:none");
		$("div.visible").attr("style", "margin:0px");
    }

}

function displayCartAddOnsOnMouseOver(object, e) {
    var xpos = e.clientX;
    var ypos = e.clientY;
    var mouseOverString = "";
    var nbsp = " ";
    var discountAmount = 0;
    var salePrice = 0;
    
    for (var i=0; i<orderAddOns.length; i++) {
         mouseOverString = mouseOverString + orderAddOns[i][2] + ' ' + orderAddOns[i][3] + ': ' ;
         
         if (orderAddOns[i][4] != null && orderAddOns[i][4] != "" && orderAddOns[i][4] != 0) {
           	salePrice = (orderAddOns[i][2] * orderAddOns[i][4]);
         }
         else if (orderAddOns[i][5] != null && orderAddOns[i][5] != "") {
         	// dont do anything		
         }
         else {
         		salePrice = (orderAddOns[i][2] * orderAddOns[i][6]);
         }
         if (orderAddOns[i][5] != null && orderAddOns[i][5] != "") {
         	discountAmount = (parseFloat(orderAddOns[i][2]) * parseFloat(orderAddOns[i][5]));
         }
         mouseOverString = mouseOverString + formatAmount2DecimalPlaces(parseFloat(salePrice.toPrecision(4)) + parseFloat(discountAmount.toPrecision(4))) +  '<br>';
	}
	
	
    if (mouseOverString != "" ) {
        $("#spanCartAddOnsMouseOver").html(mouseOverString);
        $("#spanCartAddOnsMouseOver")[0].style.top=ypos+15;
        $("#spanCartAddOnsMouseOver")[0].style.left=xpos-50;
        $("#spanCartAddOnsMouseOver").show();
		// hack for IE7 issue where space appears under the tabs when you mouse over
		$("iframe.hidden").attr("style", "display:none");
		$("div.visible").attr("style", "margin:0px");
    }

}

function displayCartDiscountOnMouseOver(object, e) {
    var xpos = e.clientX;
    var ypos = e.clientY;
    var mouseOverString = "";
    var nbsp = " ";
    var discountAmount = 0;
    
    for (var i=0; i<cartDiscount.length; i++) {
         if (cartDiscount[i][0] == 'Original' && cartDiscount[i][1] > 0) {
         	mouseOverString = mouseOverString + "Merchandise" + ': ' ;
         	mouseOverString = mouseOverString + "(" + formatAmount2DecimalPlaces(cartDiscount[i][1]) + ")" +  '<br>';	
         }
	}
    
    for (var i=0; i<orderAddOns.length; i++) {
         
         if (orderAddOns[i][5] != null && orderAddOns[i][5] != "" && orderAddOns[i][5] > 0) {
         	mouseOverString = mouseOverString + orderAddOns[i][2] + ' ' + orderAddOns[i][3] + ': ' ;
         	discountAmount = (parseFloat(orderAddOns[i][2]) * parseFloat(orderAddOns[i][5]));
		 	mouseOverString = mouseOverString + "(" + formatAmount2DecimalPlaces(discountAmount) + ")" +  '<br>';
         }
	}
	
	
    if (mouseOverString != "" ) {
        $("#spanCartDiscountMouseOver").html(mouseOverString);
        $("#spanCartDiscountMouseOver")[0].style.top=ypos+15;
        $("#spanCartDiscountMouseOver")[0].style.left=xpos-50;
        $("#spanCartDiscountMouseOver").show();
		// hack for IE7 issue where space appears under the tabs when you mouse over
		$("iframe.hidden").attr("style", "display:none");
		$("div.visible").attr("style", "margin:0px");
    }

}

/**************************************************************
* displayOrderFeesOnMouseOver()
**************************************************************/
function displayOrderFeesOnMouseOver(serviceFee, shippingFee, satUpchargeFee, alHISurchargeFee, fuelSurchargeFee, fuelSurchargeDescription, applySurchargeCode, sameDayUpcharge, morningDeliveryFee, sunUpchargeFee, monUpchargeFee, lateCutoffFee, internationalFee, orderDetailId, object,e)
{
  var xpos = e.clientX;
  var ypos = e.clientY;
  var serviceFeeString = "" ;
  var shippingFeeString = "" ;
  var alHISurchargeFeeString = "";
  var satUpchargeFeeString = "" ;
  var fuelSurchargeFeeString = "" ;
  var sameDayUpchargeString = "";
  var morningDeliveryFeeString = "";
  var sunUpchargeFeeString = "";
  var monUpchargeFeeString = "";
  var lateCutoffFeeString = "";
  var internationalFeeString ="";
  

  if ( (!IsEmpty(morningDeliveryFee)) && morningDeliveryFee > 0)
  {
      if (serviceFee > 0){
        serviceFee = serviceFee - morningDeliveryFee;
        morningDeliveryFeeString = "Morning Delivery: " +  formatAmount2DecimalPlaces(morningDeliveryFee) +"<BR>";
      }
      else if (shippingFee > 0)
      {
      shippingFee = shippingFee - morningDeliveryFee;
      morningDeliveryFeeString =  "Morning Delivery: " +  formatAmount2DecimalPlaces(morningDeliveryFee) +"<BR>";
      }
  }
  
  if ( applySurchargeCode == 'ON' && (!IsEmpty(fuelSurchargeFee)) && fuelSurchargeFee > 0)
  {
    if (shippingFee > 0)
    {
      shippingFee = shippingFee - fuelSurchargeFee;
      fuelSurchargeFeeString =  fuelSurchargeDescription + ": " +  formatAmount2DecimalPlaces(fuelSurchargeFee) +"<BR>";
    }
    else if (serviceFee > 0)
    {
      serviceFee = serviceFee - fuelSurchargeFee;
      fuelSurchargeFeeString =  fuelSurchargeDescription + ": " +  formatAmount2DecimalPlaces(fuelSurchargeFee) +"<BR>";
    }
  }

  if ( (!IsEmpty(alHISurchargeFee)) && alHISurchargeFee > 0)
  {
    if (shippingFee > 0)
    {
      shippingFee = shippingFee - alHISurchargeFee;
      alHISurchargeFeeString = "Alaska/Hawaii Upcharge: " +  formatAmount2DecimalPlaces(alHISurchargeFee) +"<BR>";
    }
    else if (serviceFee > 0)
    {
      serviceFee = serviceFee - alHISurchargeFee;
      alHISurchargeFeeString = "Alaska/Hawaii Upcharge: " +  formatAmount2DecimalPlaces(alHISurchargeFee) +"<BR>";
    }
  }


  if ( (!IsEmpty(satUpchargeFee)) && satUpchargeFee > 0)
  {
    if (shippingFee > 0){
      shippingFee = shippingFee - satUpchargeFee;
      satUpchargeFeeString = "Saturday Upcharge: " +  formatAmount2DecimalPlaces(satUpchargeFee) +"<BR>";
    }
    else if (serviceFee > 0)
    {
      serviceFee = serviceFee - satUpchargeFee;
      satUpchargeFeeString = "Saturday Upcharge: " +  formatAmount2DecimalPlaces(satUpchargeFee) +"<BR>";
    }
  }

  if ( (!IsEmpty(sameDayUpcharge)) && sameDayUpcharge > 0)
  {
      if (serviceFee > 0){
        serviceFee = serviceFee - sameDayUpcharge;
        sameDayUpchargeString = "Same Day Fee: " +  formatAmount2DecimalPlaces(sameDayUpcharge) +"<BR>";
      }
      else if (shippingFee > 0){
      shippingFee = shippingFee - sameDayUpcharge;
      sameDayUpchargeString = "Same Day Fee: " +  formatAmount2DecimalPlaces(sameDayUpcharge) +"<BR>";
      }
  }
  
  //DI-24
  if ( (!IsEmpty(lateCutoffFee)) && lateCutoffFee > 0)
  {
    if (shippingFee > 0){
      shippingFee = shippingFee - lateCutoffFee;
      lateCutoffFeeString = "Late Cutoff: " +  formatAmount2DecimalPlaces(lateCutoffFee) +"<BR>";
      
    }
    else if (serviceFee > 0)
    {
      serviceFee = serviceFee - lateCutoffFee;
      lateCutoffFeeString = "Late Cutoff: " +  formatAmount2DecimalPlaces(lateCutoffFee) +"<BR>";
    }
  }
  
  if ( (!IsEmpty(sunUpchargeFee)) && sunUpchargeFee > 0)
  {
    if (shippingFee > 0){
      shippingFee = shippingFee - sunUpchargeFee;
      sunUpchargeFeeString = "Sunday Upcharge: " +  formatAmount2DecimalPlaces(sunUpchargeFee) +"<BR>";
    }
    else if (serviceFee > 0)
    {
      serviceFee = serviceFee - sunUpchargeFee;
      sunUpchargeFeeString = "Sunday Upcharge: " +  formatAmount2DecimalPlaces(sunUpchargeFee) +"<BR>";
    }
  }
  
  if ( (!IsEmpty(monUpchargeFee)) && monUpchargeFee > 0)
  {
    if (shippingFee > 0){
      shippingFee = shippingFee - monUpchargeFee;
      monUpchargeFeeString = "Monday Upcharge: " +  formatAmount2DecimalPlaces(monUpchargeFee) +"<BR>";
    }
    else if (serviceFee > 0)
    {
      serviceFee = serviceFee - monUpchargeFee;
      monUpchargeFeeString = "Monday Upcharge: " +  formatAmount2DecimalPlaces(monUpchargeFee) +"<BR>";
    }
  }
  
   if ( (!IsEmpty(internationalFee)) && internationalFee > 0)
  {
    
    if (serviceFee > 0)
    {
      serviceFee = serviceFee - internationalFee;
      internationalFeeString = "International Fee: " +  formatAmount2DecimalPlaces(internationalFee) +"<BR>";
    }
   else  if (shippingFee > 0){
      shippingFee = shippingFee - internationalFee;
      internationalFeeString = "International Fee: " +  formatAmount2DecimalPlaces(internationalFee) +"<BR>";
    }
    
  }
  
  

  serviceFee=Math.round(serviceFee*100)/100;
  shippingFee=Math.round(shippingFee*100)/100;

  if (serviceFee > 0 )
  {
    serviceFeeString = "Service Fee: " + formatAmount2DecimalPlaces(serviceFee) +"<BR>";
  }

  if (shippingFee > 0)
  {
    shippingFeeString = "Shipping Fee: " +  formatAmount2DecimalPlaces(shippingFee) +"<BR>";
  }

  var mouseOverString = serviceFeeString + shippingFeeString + satUpchargeFeeString + alHISurchargeFeeString + sameDayUpchargeString + fuelSurchargeFeeString + morningDeliveryFeeString +  lateCutoffFeeString + sunUpchargeFeeString + monUpchargeFeeString + internationalFeeString;
  
  if (mouseOverString != "" )
  {
    $("#spanOrderMouseOver_" + orderDetailId).html(mouseOverString);
    $("#spanOrderMouseOver_" + orderDetailId)[0].style.top=ypos+10;
    $("#spanOrderMouseOver_" + orderDetailId)[0].style.left=xpos+10;
    $("#spanOrderMouseOver_" + orderDetailId).show();
    // hack for IE7 issue where space appears under the tabs when you mouse over
    $("iframe.hidden").attr("style", "display:none");
    $("div.visible").attr("style", "margin:0px");
  }
}


/**************************************************************
* IsEmpty()
**************************************************************/
function IsEmpty(feeVal) {
var feeValString = String(feeVal);

   if (isNaN(feeValString) || (feeValString==null) || (trim(feeValString)=='') ) {
      return true;
   }
   else { return false; }
}


/**************************************************************
* trim()
**************************************************************/
function trim(str)
{

	str=str.replace(/^\s*(.*)/, "$1");
	str=str.replace(/(.*?)\s*$/, "$1");


	return str;
}

/**************************************************************
* cdisp_autoPopup()
**************************************************************/
function cdisp_autoPopup()
{
    // Opens Call Disposition Widget if this is first time user views cart
    // (and Call Dispo is enabled and DNIS was specified).
    //
    var autoOpened = document.getElementById('cdisp_autoOpenedFlag');
    var isEnabled  = document.getElementById('cdisp_enabledFlag');
    var isReminder = document.getElementById('cdisp_reminderFlag');
    var callDnis   = document.getElementById('call_dnis');

    // Call Dispo is displayed only if enabled (based on CSR permission and DNIS)
    //
    if ((isEnabled != null) && (isEnabled.value == 'Y') &&
        ((autoOpened.value == null) || (autoOpened.value != 'Y')))
    {
        autoOpened.value = 'Y';
        loadCallDisp();
        // alert('Initial popup'); // ???
    }
}

/**************************************************************
* imgOnError and imgOnLoad
**************************************************************/

// Used for hiding broken image icon 
function imgOnError(img) {
	img.style.display = 'none';
	//delete img.onerror;
}
// Used to hide alternate text 
function imgOnLoad(elem) {
   // Use JQuery to hide since it deals with IE madness
	$(elem).hide();
}


]]>
  </script>
</head>
<body onload="init()"  onkeypress="doMysteryBox();" onkeydown="javascript:checkKey();">
<xsl:call-template name="addHeader"/>
<form name="csci" method="post">
 <!-- Call template to import security params and searchCriteria -->
  <xsl:call-template name="securityanddata"/>
  <xsl:call-template name="decisionResultData"/>

  <!-- updated when the user clicks a tab -->
  <input type="hidden" name="order_detail_id" id="order_detail_id" value=""/>

 <!--Page Data Fields-->

  <input type="hidden" name="cai_current_page" id="cai_current_page" value="{key('pageData','cai_current_page')/value}"/>
  <input type="hidden" name="cai_locked_indicator" id="cai_locked_indicator" value="{key('pageData','cai_locked_indicator')/value}"/>
  <input type="hidden" name="cai_more_email_indicator" id="cai_more_email_indicator" value="{key('pageData','cai_more_email_indicator')/value}"/>
  <input type="hidden" name="cai_number_of_carts" id="cai_number_of_carts" value="{key('pageData','cai_number_of_carts')/value}"/>
  <input type="hidden" name="cai_number_of_orders" id="cai_number_of_orders" value="{key('pageData','cai_number_of_orders')/value}"/>
  <input type="hidden" name="cai_show_first" id="cai_show_first" value="{key('pageData','cai_show_first')/value}"/>
  <input type="hidden" name="cai_show_last" id="cai_show_last" value="{key('pageData','cai_show_last')/value}"/>
  <input type="hidden" name="cai_show_next" id="cai_show_next" value="{key('pageData','cai_show_next')/value}"/>
  <input type="hidden" name="cai_show_previous" id="cai_show_previous" value="{key('pageData','cai_show_previous')/value}"/>
  <input type="hidden" name="cai_start_position" id="cai_start_position" value="{key('pageData','cai_start_position')/value}"/>
  <input type="hidden" name="cai_total_pages" id="cai_total_pages" value="{key('pageData','cai_total_pages')/value}"/>
  <input type="hidden" name="receipt_avail_scrub_flag" id="receipt_avail_scrub_flag" value="Y"/>
  <input type="hidden" name="receipt_avail_remove_flag" id="receipt_avail_remove_flag" value="N"/>
  <input type="hidden" name="vipCustUpdatePermission" id="vipCustUpdatePermission" value="{key('pageData','vipCustUpdatePermission')/value}"/> 
  <input type="hidden" name="vipCustomerMessage" id="vipCustomerMessage" value="{key('pageData','vipCustomerMessage')/value}"/>
  <input type="hidden" name="vipCustUpdateDeniedMessage" id="vipCustUpdateDeniedMessage" value="{key('pageData','vipCustUpdateDeniedMessage')/value}"/>
  <input type="hidden" name="orig_order_detail_id" id="orig_order_detail_id" value=""/>
  <input type="hidden" name="orig_ext_order_num" id="orig_ext_order_num" value=""/>
  <input type="hidden" name="modify_order_joe_url" id="modify_order_joe_url" value="{key('pageData','modify_order_joe_url')/value}"/>
  <input type="hidden" name="modify_order_dnis" id="modify_order_dnis" value="{CSCI_CARTS/CSCI_CART/default_dnis}"/>

  <input type="hidden" name="cos_search_customer_count" id="cos_search_customer_count" value="{key('pageData','cos_search_customer_count')/value}"/>

  <input type="hidden" name="csci_current_page" id="csci_current_page" value="{key('pageData','csci_current_page')/value}"/>
  <input type="hidden" name="csci_florist_url" id="csci_florist_url" value="{key('pageData','csci_florist_url')/value}"/>
  <input type="hidden" name="csci_number_of_orders" id="csci_number_of_orders" value="{key('pageData','csci_number_of_orders')/value}"/>
  <input type="hidden" name="csci_order_found" id="csci_order_found" value="{key('pageData','csci_order_found')/value}"/>
  <input type="hidden" name="csci_order_indicator" id="csci_order_indicator" value="{key('pageData','csci_order_indicator')/value}"/>
  <input type="hidden" name="csci_order_position" id="csci_order_position" value="{key('pageData','csci_order_position')/value}"/>
  <input type="hidden" name="csci_show_first" id="csci_show_first" value="{key('pageData','csci_show_first')/value}"/>
  <input type="hidden" name="csci_show_last" id="csci_show_last" value="{key('pageData','csci_show_last')/value}"/>
  <input type="hidden" name="csci_show_next" id="csci_show_next" value="{key('pageData','csci_show_next')/value}"/>
  <input type="hidden" name="csci_show_previous" id="csci_show_previous" value="{key('pageData','csci_show_previous')/value}"/>
  <input type="hidden" name="csci_total_pages" id="csci_total_pages" value="{key('pageData','csci_total_pages')/value}"/>
  <input type="hidden" name="origin_id" id="origin_id" value="{CSCI_CARTS/CSCI_CART/origin_id}"/>

  <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
  <input type="hidden" name="load_member_data_url" id="load_member_data_url" value="{key('pageData','load_member_data_url')/value}"/>
  <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
  <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>
  <input type="hidden" name="scrub_url" id="scrub_url" value="{key('pageData','scrub_url')/value}"/>
  <input type="hidden" name="searched_on_external_order_number" id="searched_on_external_order_number" value="{key('pageData','searched_on_external_order_number')/value}"/>
  <input type="hidden" id="display_page" name="display_page" value="{key('pageData','display_page')/value}"/>
  <!-- outter div will be hidden when/if an action is performed-->
  <input type="hidden" name="refund_disp_code" id="refund_disp_code"/>
  <input type="hidden" name="refund_display_type" id="refund_display_type"/>
  <input type="hidden" name="responsible_party" id="responsible_party"/>
  <input type="hidden" name="full_refund_indicator" id="full_refund_indicator"/>
  <input type="hidden" name="refund_status" id="refund_status"/>
  <input type="hidden" name="process_original_view" id="process_original_view"/>
  <input type="hidden" name="loss_prevention_ind" id="loss_prevention_ind" value="{CSCI_CARTS/CSCI_CART/loss_prevention_indicator}"/>
  <input type="hidden" name="company_name" id="company_name" value="{CSCI_CARTS/CSCI_CART/company_name}"/>
  <input type="hidden" name="company_id" id="company_id" value="{CSCI_CARTS/CSCI_CART/company_id}"/>

  <input type="hidden" name="lossPreventionPermission" id="lossPreventionPermission" value="{key('pageData','lossPreventionPermission')/value}"/>
  <input type="hidden" name="customerHoldPermission" id="customerHoldPermission" value="{key('pageData','customerHoldPermission')/value}"/>
  <input type="hidden" name="bypass_com"  id="bypass_com" value="{$bypassCOM}" />
  <!-- needed for original view on itemDiv.xsl-->
  <input type="hidden" name="display_order_number" id="display_order_number"/>
  <!-- premier circle membership -->
  <input type="hidden" id="premier_circle_membership" name="premier_circle_membership" value="{key('pageData','premierCircleMembership')/value}"/>
  <input type="hidden" id="pcMembershipErrorMsg" name="pcMembershipErrorMsg" value="{key('pageData','pcMembershipChkErrorMsg')/value}"/>

<div id="mainContent" style="display:block;" >



  <xsl:call-template name="tabs"/>
      <!--call the tabs xsl-->
   <div id="content" style="top:0px;left:0px;margin:0px;padding:0px;">
     <div id="shoppingCartDiv">
     <table border="0" class="mainTable" width="100%" cellpadding="0" cellspacing="2">
    	<tr>
        <td>
        	<table border="0" class="innerTable" width="100%" cellpadding="0" cellspacing="2">
          <tr>
            <td>
              <table id="csciHeader" border="0" width="100%"  cellpadding="0" cellspacing="2" style="word-break:break-all">
                <tr>
                  <td width="16%">&nbsp;

                    <xsl:variable name="orderHeld" select="//CSCI_CARTS_HOLD/CSCI_CART_HOLD/customer_hold_description"/>
                    <xsl:if test="$orderHeld != $YES">
                      <img src="images/IconLock.gif"/>&nbsp;&nbsp;
                    </xsl:if>
                    <span class="Label">Customer #:</span>&nbsp;
                    <xsl:value-of select="key('pageData','customer_id')/value"/>
                  </td>
                  <td width="22%">
                    <span class="Label">Order&nbsp;Date:
                    </span>&nbsp;&nbsp;
                    <xsl:value-of select="CSCI_CARTS/CSCI_CART/order_date_formatted"/>
		            &nbsp;
		            <xsl:value-of select="CSCI_CARTS/CSCI_CART/order_time_formatted"/>
                  </td>
                  <td width="18%">
                    <span class="Label">Origin:</span>&nbsp;&nbsp;
                    <xsl:choose>
                      <xsl:when test="CSCI_CARTS/CSCI_CART/origin_id = $FOX">
                        <xsl:value-of select="'Internet'"/>
                      </xsl:when>
                      <xsl:when test="CSCI_CARTS/CSCI_CART/origin_id = ''">
                        <xsl:value-of select="$Phone"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="CSCI_CARTS/CSCI_CART/origin_id"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                  <td width="25%">
                    <span class="Label">Company:</span>&nbsp;&nbsp;
                    <xsl:value-of select="CSCI_CARTS/CSCI_CART/company_name"/>
                  </td>
                  <td >
                    <span class="Label">Source&nbsp;Code:
                    </span>&nbsp;
                    <a id="cartSourceCodeLink" href="javascript:doSourceCodeAction('{CSCI_CARTS/CSCI_CART/source_code}');" class="textlink">
                      <xsl:value-of select="CSCI_CARTS/CSCI_CART/source_code"/>
                    </a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="banner">
          Customer Information
            </td>
          </tr>
          <tr>
            <td>
              <!--Customer Information table-->
              <table id="csciInfo" border="0" width="100%" cellpadding="0" cellspacing="3">
                <tr >
                  <td width="10%" class="alignright">
                    <span class="Label">Name:</span>&nbsp;
                  </td>
                  <td width="25%" style="white-space: nowrap;">
                    <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_first_name"/>&nbsp;<xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_last_name"/>
                  </td>
                  <td rowspan="4" align="left" valign="top" width="120">
                    <xsl:if test="CSCI_CARTS/CSCI_CART/partner_name/text() != ''">
                        <img width="50" src="images/{/root/CUSTOMER_PREFERRED_PARTNER/PREFERRED_PARTNER[preferred_processing_resource = /root/CSCI_CARTS/CSCI_CART/partner_name/text()]/display_name}_logo.gif"/>
						<xsl:if test="CSCI_CARTS/CSCI_CART/is_joint_cart/text() = 'Y'">
							<br/><img src="images/partner_jointcart.gif"/>
						</xsl:if>
                    </xsl:if>
					<script type="text/javascript" language="javascript">
						if (isAmazonOrder) {
							document.write('<img width="50" src="images/amazon_logo_large.gif" />');
							}					
						if (partnerName != ''){
							document.write('<img src="{$imageServerUrl}/{$partnerChannelIcon}" />');
						}
						if (premierCircleMembership == 'true') {
							document.write('<img width="50" src="images/ftd_premier_circle_logo.gif" />');
						} 						                   
					</script>                                       
                  </td>
                  <td class="alignright" width="15%">
                    <span class="Label">Phone 1:</span>
                  </td>
                  <td width="10%">
                    <xsl:if test="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Day']/../customer_phone_number != ''">
                      <script type="text/javascript">
                      if(isUSPhoneNumber('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Day']/../customer_phone_number"/>')){
                        document.write(reformatUSPhone('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Day']/../customer_phone_number"/>'));
                      }else{
                        document.write('<xsl:value-of select="CAI_PHONES/CAI_PHONE//customer_phone_type[. = 'Day']/../customer_phone_number"/>');
                      }
                      </script>
                     </xsl:if>
                  </td>
                  <td class="alignright" width="2%">
                    <span class="Label">Ext:</span>
                  </td>
                  <td>
                    <xsl:value-of select="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Day']/../customer_extension"/>
                  </td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td class="alignright">
                    <span class="Label">Business:</span>
                  </td>
                  <td>
                    <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/business_name"/>
                  </td>
                  <td class="alignright">
                    <span class="Label">Phone 2:</span>
                  </td>
                  <td>
                    <xsl:if test="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Evening']/../customer_phone_number != ''">
                      <script type="text/javascript">
                      if(isUSPhoneNumber('<xsl:value-of select="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Evening']/../customer_phone_number"/>')){
                        document.write(reformatUSPhone('<xsl:value-of select="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Evening']/../customer_phone_number"/>'));
                      }else{
                        document.write('<xsl:value-of select="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Evening']/../customer_phone_number"/>');
                      }
                      </script>
                     </xsl:if>

                  </td>
                  <td class="alignright">
                    <span class="Label">Ext:</span>
                  </td>
                  <td>
                    <xsl:value-of select="CAI_PHONES/CAI_PHONE/customer_phone_type[. = 'Evening']/../customer_extension"/>
                  </td>
                </tr>
                <tr>
                  <td class="alignright">
                    <span class="Label" >Address 1:
                    </span>
                  </td>
                  <td>
                    <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_address_1"/>
                  </td>
                  <td class="alignright" id="emailTD" email="{CSCI_CARTS/CSCI_CART/email_address}" altEmail="{CSCI_CARTS/CSCI_CART/alt_email_address}">
                    <span class="Label">Email Address:</span>
                  </td>
                  <td colspan="3">
                    <xsl:value-of select="CSCI_CARTS/CSCI_CART/email_address"/>
                  </td>
                </tr>
                <tr>
                  <td class="alignright">
                    <span class="Label">Address 2:</span>
                  </td>
                  <td>
                    <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_address_2"/>
                  </td>
                  <td class="alignright">
                    <span class="Label">Marketing Emails:</span>
                  </td>
                  <td>
                    <xsl:value-of select="CSCI_CARTS/CSCI_CART/direct_mail"/>
                  </td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td class="alignright">
                    <span class="Label">City, State, Zip:</span>
                  </td>
                  <td style="vertical-align:top">
										<xsl:choose>
							              <xsl:when test="CAI_CUSTOMERS/CAI_CUSTOMER/customer_state != ''">
							                  <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_city"/>,
							                  <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_state"/>
							                  &nbsp;&nbsp;
							                </xsl:when>
							                <xsl:otherwise>
							                   <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_city"/>
							                </xsl:otherwise>
							              </xsl:choose>
							                <xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/customer_zip_code"/>
							                  &nbsp;&nbsp;
							              <xsl:if test="CAI_CUSTOMERS/CAI_CUSTOMER/customer_country != 'US'">
										  		<strong>
						                    		<xsl:value-of select="CAI_CUSTOMERS/CAI_CUSTOMER/country_name"/>                    	 
						                    	</strong>
										  </xsl:if>	
 				  </td>
                  <td align="right">
                       <a id="altContactLink" href="javascript:doAlternateContactAction();" class="textlink" style="display:none;">Alternate Contact</a>
                  </td>
                  <td class="alignright">
                    <span class="Label">Hold Information:</span>
                  </td>
                  <td class="alignleft" style="vertical-align:top">
                    <xsl:choose>
                      <xsl:when test="contains(CSCI_CARTS_HOLD/CSCI_CART_HOLD/customer_hold_description,'Fraud') or contains(CSCI_CARTS_HOLD/CSCI_CART_HOLD/customer_hold_description,'FRAUD')">
                        LP Review
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="CSCI_CARTS_HOLD/CSCI_CART_HOLD/customer_hold_description"/> 
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                  <td></td>
                </tr>
               </table>
              <!-- end customer information table-->
            </td>
          </tr>
          <tr>
            <td class="banner">Shopping Cart Totals</td>
          </tr>
          <tr>
            <td>
              <!-- Shopping Cart totals -->
              <table border="0" align="center" width="100%" cellpadding="0" cellspacing="3">
                <tr style="text-align:right">
                  <td width="15%"></td>
                  <td class="Label">Merchandise</td>
                  <td class="Label">Add-Ons</td>
                  <td class="Label">Service/Shipping
                  </td>
                  <td class="Label">Discount</td>
                  <td class="Label">Tax</td>
                  <td class="Label">Shopping Cart
                    <br/>Total
                  </td>
                  <xsl:choose>
                    <xsl:when test="$isMilesPoints = 'Y'"><td class="Label">Miles/<br/>Points</td></xsl:when>
                    <xsl:otherwise><td></td></xsl:otherwise>
                  </xsl:choose>
                </tr>
                <tr style="text-align:right">
                  <td class="Label">Original:</td>
                  <td>
                    <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../product_amount, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                  </td>
                  <td>
                  	<span class="cartAddOnValue" onmouseover="displayCartAddOnsOnMouseOver(this, event);" style="text-decoration: none; cursor:default; color: black;">
		           	<xsl:choose>
			           	<xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_discount_amount) != 0 and CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_discount_amount != ''">
							<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
						</xsl:otherwise>
					</xsl:choose>
					</span>
					<span id="spanCartAddOnsMouseOver" class="tooltip" ></span>
			         <script>
			         	$(".cartAddOnValue").mouseout(function() {
			         		$("#spanCartAddOnsMouseOver").hide();
			         		$("iframe.hidden").attr("style", "display:none");
			         		$("div.visible").attr("style", "margin:0px");
			       		});
			       	 </script>
                  </td>

              <div id = "serviceFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../service_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
              <div id = "shippingFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../shipping_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
              <div id = "satUpchargeFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../vend_sat_upcharge_amount, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
              <div id = "alHISurchargeFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../ak_hi_special_surcharge, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
              <div id = "fuelSurchargeFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../fuel_surcharge_amount, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
              <div id = "applySurchargeCodeForCart" style="display: none"><xsl:value-of select="CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../apply_surcharge_code" /></div>
              <div id = "sameDayUpchargeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../same_day_upcharge, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
			  <div id = "morningDeliveryFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../morning_delivery_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
			  <div id = "sunUpchargeFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../vendor_sun_upcharge, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
			  <div id = "monUpchargeFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../vendor_mon_upcharge, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
			  <div id = "lateCutoffFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../late_cutoff_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
			  <div id = "internationalFeeForCart" style="display: none"><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../international_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/></div>
			  
                <td align="right" >
                <table><tr>
                <td></td>
                <td align="right" >
                  <span class="feeValue" style="text-decoration: none; cursor:default; color: black;">
                     <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../serv_ship_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                  </span>
                  <span id="spanMouseOver" class="tooltip" ></span>
                  <script>
                  $(".feeValue").mouseover(function(e) {displayCartFeesOnMouseOver(e.pageX, e.pageY);	});
                  $(".feeValue").mouseout(function() {
               			$("#spanMouseOver").hide();
               			//IE7 hack to hide whitespace that appears under tabs
                  		$("iframe.hidden").attr("style", "display:none");
                  		$("div.visible").attr("style", "margin:0px");
              	  });
                  </script>
                </td>
                </tr></table>
                </td>

                  <td style="text-align:right;color:black">
                  <span class="cartDiscountValue" onmouseover="displayCartDiscountOnMouseOver(this, event);" style="text-decoration: none; cursor:default; color:black;">
                  	<xsl:choose>
		            	<xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_discount_amount) != 0 and CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_discount_amount != ''">
							<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../discount_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
						</xsl:otherwise>
					</xsl:choose>
					</span>
					<span id="spanCartDiscountMouseOver" class="tooltip" ></span>
			         <script>
			         	$(".cartDiscountValue").mouseout(function() {
			         		$("#spanCartDiscountMouseOver").hide();
			         		$("iframe.hidden").attr("style", "display:none");
			         		$("div.visible").attr("style", "margin:0px");
			       		});
			       	 </script>
                  </td>
                  <td>
				    <span class="taxValue" style="text-decoration: none; cursor:default; color: black;">
                      <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../tax, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                    </span>
                    <span id="spanTaxMouseOver" class="tooltip" ></span>
                    <script>
                    $(".taxValue").mouseover(function(e) {displayCartTaxesOnMouseOver(e.pageX, e.pageY);	});
                    $(".taxValue").mouseout(function() {
                    	$("#spanTaxMouseOver").hide();
                    	//IE7 hack to hide whitespace that appears under tabs
                    	$("iframe.hidden").attr("style", "display:none");
                    	$("div.visible").attr("style", "margin:0px");
                   	});
                    </script>
                  </td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../bill_total, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                  </td>
                  <xsl:choose>
                    <xsl:when test="$isMilesPoints = 'Y'"><td><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Original']/../miles_points_amount, '#,###,##0;-#,###,##0','notANumberNoDecimal')"/></td></xsl:when>
                    <xsl:otherwise><td></td></xsl:otherwise>
                  </xsl:choose>
                </tr>
                <tr style="text-align:right">
                  <td class="Label">Add Bill(s):</td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../product_amount, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                  </td>
                  <td>
                  	<xsl:choose>
			           	<xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount) != 0 and CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount != ''">
							<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
						</xsl:otherwise>
					</xsl:choose>
                  </td>
                  <td>
                    <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../serv_ship_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/>

                  </td>



		    <xsl:choose>
		      <xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount) = 0">
			  <td style="text-align:right;color:black">
			  	<xsl:choose>
	            	<xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount) = 0 or CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount = ''">
	            		<xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:when test="CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount >= 0">
						<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
					</xsl:otherwise>
				</xsl:choose>
			  </td>
		      </xsl:when>
		      <xsl:when test="CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount >= 0">
			  <td style="text-align:right;color:black">
			    <xsl:choose>
	            	<xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount) = 0 or CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount = ''">
	            		<xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:when test="CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount >= 0">
						<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
					</xsl:otherwise>
				</xsl:choose>
			  </td>
		      </xsl:when>
		      <xsl:otherwise>
			  <td style="text-align:right;color:black">
				<xsl:choose>
	            	<xsl:when test="string-length(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount) = 0 or CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount = ''">
	            		<xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount * -1, '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:when test="CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount >= 0">
						<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount * -1) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number((CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../discount_amount * -1) + (CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../add_on_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
					</xsl:otherwise>
				</xsl:choose>
			  </td>
		      </xsl:otherwise>
		    </xsl:choose>

                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../tax, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                  </td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../bill_total, '#,###,##0.00;-#,###,##0.00','notANumber')"/>
                  </td>
                  <xsl:choose>
                    <xsl:when test="$isMilesPoints = 'Y'"><td><xsl:value-of select="format-number(CSCI_CART_BILLS/CSCI_CART_BILL/total_type[. = 'Additional Bill']/../miles_points_amount, '#,###,##0;-#,###,##0','notANumberNoDecimal')"/></td></xsl:when>
                    <xsl:otherwise><td></td></xsl:otherwise>
                  </xsl:choose>
                  <td align="center" rowspan="3">
                    <xsl:choose>
                        <xsl:when test="$bypassCOM = 'Y'">
                            <a id="cartPaymentInfoLink" href="javascript:doCustomerPaymentInfo();" class="textLink" style="font-size:9pt;">Payment Information</a>
                            <br /><br />
                        </xsl:when>
                        <xsl:otherwise>
                            <a id="cartPaymentInfoLink" href="javascript:doCustomerPaymentInfo();" class="textLink" style="font-size:9pt;">Payment Information</a>
                            <br /><br />
                            <button type="button"  class="BlueButton" id="updateBillingInfo" accesskey="I" onclick="javascript:doUpdateBillingInfo()">Update Billing (I)nfo</button>
                        </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>
                <tr style="text-align:right;color:black">
                  <td class="Label" style="color:#000000">Refund(s):</td>
                  <td>
                    <xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_product_amount, '-#,###,##0.00','noRefund')"/>
                  </td>
                  <td>
		            <xsl:choose>
			           	<xsl:when test="string-length(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount) != 0 and CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount != ''">
							<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount), '-#,###,##0.00','noRefund')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_amount, '-#,###,##0.00','noRefund')"/>
						</xsl:otherwise>
					</xsl:choose>
                  </td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_serv_ship_fee, '-#,###,##0.00','noRefund')"/>

                  </td>
		    <xsl:choose>
		      <xsl:when test="string-length(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) = 0">
			  <td>
			  <span style="color:black">
			  	<xsl:choose>
	            	<xsl:when test="string-length(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount) = 0 or CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount = ''">
	            		<xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
					</xsl:when>
					<xsl:when test="CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount >= 0">
						<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
					</xsl:otherwise>
				</xsl:choose>
			  </span>
			  </td>
		      </xsl:when>
		      <xsl:when test="CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount >= 0">
			  <td>
			  <span style="color:black">
			  	<xsl:choose>
	            	<xsl:when test="string-length(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount) = 0 or CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount = ''">
	            		<xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
					</xsl:when>
					<xsl:when test="CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount >= 0">
						<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) >= (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount * -1)">
								<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="format-number(((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount)) * -1, '-#,###,##0.00', 'noRefund')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			  </span>
			  </td>
		      </xsl:when>
		      <xsl:otherwise>
			  <td>
			  <span style="color:black">
			  	<xsl:choose>
	            	<xsl:when test="string-length(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount) = 0 or CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount = ''">
	            		<xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount * -1, '-#,###,##0.00', 'noRefund')"/>
					</xsl:when>
					<xsl:when test="CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount >= 0">
						<xsl:choose>
							<xsl:when test="(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount) >= (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount * -1)">
								<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="format-number(((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount)) * -1, '-#,###,##0.00', 'noRefund')"/>
							</xsl:otherwise>
						</xsl:choose>
						
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number((CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_discount_amount * -1) + (CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_addon_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
					</xsl:otherwise>
				</xsl:choose>
			  </span>
			  </td>
		      </xsl:otherwise>
		    </xsl:choose>



                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_tax, '-#,###,##0.00','noRefund')"/>
                  </td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../refund_total, '-#,###,##0.00','noRefund')"/>
                  </td>
                  <xsl:choose>
                    <xsl:when test="$isMilesPoints = 'Y'"><td><xsl:value-of select="format-number(CSCI_CART_REFUNDS/CSCI_CART_REFUND/refund_type[. = 'Refund']/../miles_points_amount, '-#,###,##0','noRefundNoDecimal')"/></td></xsl:when>
                    <xsl:otherwise><td></td></xsl:otherwise>
                  </xsl:choose>
                </tr>
                <tr style="text-align:right">
                  <td class="Label">Current Totals:</td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../product_amount, '#,###,##0.00;-#,###,##0.00','notANumber')"/>

                  </td>
                  <td>
			        <xsl:choose>
			          	<xsl:when test="string-length(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_discount_amount) != 0 and CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_discount_amount != ''">
							<xsl:value-of select="format-number((CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_amount) + (CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
						</xsl:otherwise>
					</xsl:choose>
                  </td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../serv_ship_fee, '#,###,##0.00;-#,###,##0.00','notANumber')"/>

                  </td>
                  <td style="text-align:right;color:black">
			        <xsl:choose>
			           	<xsl:when test="string-length(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_discount_amount) != 0 and CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_discount_amount != ''">
							<xsl:value-of select="format-number((CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../discount_amount) + (CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
						</xsl:otherwise>
					</xsl:choose>

                  </td>
                  <td>
                  <xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../tax, '#,###,##0.00;-#,###,##0.00','notANumber')"/>

                  </td>
                  <td>
                     <table style="border: 1px solid black;" cellpadding="1">
						<tr>
							<td>
                 			 <xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../cart_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                 			 </td>
                 		</tr>
                 	</table>		 
                  </td>
                  <xsl:choose>
                    <xsl:when test="$isMilesPoints = 'Y'"><td><xsl:value-of select="format-number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/total_type[. = 'Modified Total']/../miles_points_total, '#,###,##0;-#,###,##0','notANumberNoDecimal')"/></td></xsl:when>
                    <xsl:otherwise><td></td></xsl:otherwise>
                  </xsl:choose>
                </tr>
                <xsl:if test="key('pageData','cart_fees_saved_message')/value != ''">
                  <td colspan="8" class="InformationalMsg">
                    <xsl:value-of select="key('pageData','cart_fees_saved_message')/value"/>
                  </td>
                </xsl:if>
              </table>
              <!-- End Shopping Cart totals -->
              </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table id="cmdButtons" border="0" width="100%" align="center" cellpadding="0" cellspacing="0">
          <tr >
            <td style="vertical-align:top">
              <xsl:choose>
                    <xsl:when test="$bypassCOM = 'Y'">
                        &nbsp;
                    </xsl:when>
                    <xsl:otherwise>

                      <button type="button"  id="cartCommentsAction" class="BigBlueButton ButtonTextBox1" accesskey="C" onclick="javascript:doCustomerCommentsAction();">
                        <xsl:if test="CSCI_CARTS/CSCI_CART/customer_comment_indicator/text() = 'Y'">
                          <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                        </xsl:if>
                      (C)ustomer<br/>Comments
                      </button>
                      <button type="button"  id="cartUpdateCustomer" class="BigBlueButton ButtonTextBox2" accesskey="U" onclick="javascript:doUpdateCustomerActionShoppingCart('csUpdateCustomer',0);">
                     	 (U)pdate <br/>Customer
                      </button>
                      <button type="button"  id="loadCartEmail" class="BigBlueButton ButtonTextBox2" accesskey="E" onclick="javascript:doLoadCartEmailList();">
                       Update Cart<br/>(E)mail Address
                      </button>
                      <xsl:if test="//data[name='customerHoldPermission']/value = $YES and //data[name='lossPreventionPermission']/value = $YES">
                        <button type="button" style="vertical-align:top" id="cartHold" class="BigBlueButton ButtonTextBox1" accesskey="H" onclick="javascript:doHoldAction('cart');">(H)old</button>
                      </xsl:if>
                      <br/>
                      <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
                        <button type="button"  id="cartLossPrevention" class="BigBlueButton ButtonTextBox1" accesskey="L" onclick="javascript:doLossPreventionAction();">(L)oss<br/>Prevention</button>
                      </xsl:if>
                      <button type="button"  id="cartRefund" class="BigBlueButton ButtonTextBox2" accesskey="R" onclick="javascript:doCheckRefundStatus();">
                                     <xsl:if test="number(CSCI_CART_TOTALS/CSCI_CART_TOTAL/cart_total) = 0">
                                  <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                                     </xsl:if>
                               (R)efund<br/>Shopping Cart
                      </button>
                      <button type="button"  id="viewBillingTrans" class="BigBlueButton ButtonTextBox2" accesskey="T" onclick="javascript:doViewBillingTrans();">View Billing<br />(T)ransaction</button>
                      <button type="button"  id="loadOrderReceipt" class="BigBlueButton ButtonTextBox1" accesskey="O" onclick="javascript:doLoadOrderReceipt();">(O)rder<br/>Receipt</button>
	                    <xsl:if test="key('pageData','surchargeExplanation')/value != ''">
							 <br/><xsl:value-of select="key('pageData','surchargeExplanation')/value"
						              	disable-output-escaping="yes"/>
					     </xsl:if>

                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td style="text-align:right;">
                <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
                    <button type="button"  class="BigBlueButton" accesskey="V" id="lpsearch" onclick="javascript:doLossPreventionSearchAction();">
                        Back to Loss<br />Pre(v)ention Search
                    </button>
                </xsl:if>
              <button type="button"  id="cartSearch" class="BigBlueButton" accesskey="S" style="width:61px;" onclick="javascript:doSearchAction();">(S)earch</button>
              <br/>
              <button type="button"  id="cartMainMenu" class="BigBlueButton" accesskey="M" style="width:61px;" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu
              </button>
            </td>
          </tr>
        </table>
        <!-- Used to dislay Processing... message to user -->
        <xsl:if test="key('pageData','vip_customer')/value='true'">
			<span class="ErrorMessage" style="WIDTH: 120px; font-weight:normal; MARGIN-LEFT: 135px; DISPLAY: inline-block">	
				 <script type="text/javascript">
				 	document.write(document.getElementById('vipCustomerMessage').value);
				 	localStorage.setItem('isCustomerVip','Y');
				 </script>
			</span>
		</xsl:if>
        <xsl:call-template name="footer"/>
      </div>
      <xsl:call-template name="itemDiv"/>
      <xsl:call-template name="customerAccountDetail"/>
    </div>
    <!--
       The Heights on these tables are set using javascript when an order is in
     Scrub, Pending or Removed. You can find the javascript that does this
     in the tabs.xsl. The message that is displayed here is also set using
     javascript which, can be found in the same tabs.xsl file
    -->
  </div>

  <div id="waitDiv" style="padding-top:.2em;display:none;">
    <table id="waitTable" width="100%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table cellspacing="2" cellpadding="2" style="width:100%;height:300px">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
              <td id="waitTD" width="50%" class="WaitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

   <div id="hiddenPageNavigation" style="display:none">
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
       <tr>
          <td></td>
          <td style="text-align:right">
                <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
                    <button type="button"  class="BigBlueButton" accesskey="V" id="lpsearch" onclick="javascript:doLossPreventionSearchAction();">
                        Back to Loss<br />Pre(v)ention Search
                    </button>
                </xsl:if>
               <button type="button"  id="hidesearch" class="BigBlueButton" accesskey="B" onclick="javascript:doSearchAction();">(B)ack to Search</button><br/>
			   <button type="button"  id="hidemainMenu" class="BigBlueButton" style="width:55px;" accesskey="M" onclick="javascript:doMainMenuAction();">(M)ain<br />Menu</button>
          </td>
       </tr>
    </table>
  </div>

    <xsl:call-template name="footer"/>
    <script type="text/javascript">
	   if(document.getElementById('vipCustUpdatePermission').value == 'N'){
	      if(document.getElementById('vip_customer').value == 'true'){
       	       if(localStorage.getItem('isVipPopupShown') == 'N')
	       	 {
       	        var modalArguments = new Object();
                modalArguments.modalText = document.getElementById('vipCustUpdateDeniedMessage').value;
                window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
       		 	localStorage.setItem('isVipPopupShown','Y');
       		 }
       		}
	      }
	 </script>
  </div>
  <iframe id="refundCartFrame" width="0px" height="0px"  border="0" class="hidden" src="blank.html" />
  <iframe id="updateEmailFrame" name="updateEmailFrame" height="0" width="0" border="0" class="hidden" src="blank.html"></iframe>
  <iframe id="csUpdateCustomer" width="0px" height="0px"  border="0" class="hidden" src="blank.html" />
  <iframe id="unlockFrame" width="0px" height="0px"  border="0" class="hidden" src="blank.html" />
  <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0" class="hidden" src="blank.html" />

</form>
</body>
</html>
  </xsl:template>
  <xsl:template name="addHeader">
    <xsl:call-template name="header">
      <xsl:with-param name="showBackButton" select="true()"/>
      <xsl:with-param name="showPrinter" select="true()"/>
      <xsl:with-param name="showSearchBox" select="true()"/>
      <xsl:with-param name="searchLabel" select="'Order #'"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="headerName" select="'Customer Shopping Cart'"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="backButtonLabel" select="'(B)ack to Search'"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>
  </xsl:template>
</xsl:stylesheet>
