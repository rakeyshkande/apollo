<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
  <!-- xsl imports -->
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

  <!-- keys -->
  <xsl:key name="pageData" match="/root/pageData/data" use="name"/>
  <xsl:key name="security" match="/root/security/data" use="name"/>

  <!-- variables -->


   <xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
  <xsl:template match="/root">
    <html>
      <head>
        <base target="_self"/>
        <title>FTD - Privacy Policy Verification</title>

        <link rel="stylesheet" href="css/ftd.css"/>
        <script type="text/javascript" src="js/commonUtil.js"/>
        <script type="text/javascript" src="js/util.js"/>
        <script type="text/javascript" src="js/FormChek.js"></script>
        <script type="text/javascript" src="js/privacyPolicyVerification.js"></script>

        <script type="text/javascript">


var customerValidationRequiredFlag = '<xsl:value-of select="key('pageData', 'customer_validation_required_flag')/value"/>';
var orderValidationRequiredFlag = '<xsl:value-of select="key('pageData', 'order_validation_required_flag')/value"/>';

var cvCheckboxes = new Array( <xsl:for-each select="/root/PPV_OPTIONS_CV/PPV_OPTION_CV">
                                "<xsl:value-of select="privacy_policy_option_code"/>"
                                <xsl:choose>
                                  <xsl:when test="position()!=last()">,</xsl:when>
                                </xsl:choose>
                              </xsl:for-each>
                            );

var ovCheckboxes = new Array( <xsl:for-each select="/root/PPV_OPTIONS_OV/PPV_OPTION_OV">
                                "<xsl:value-of select="privacy_policy_option_code"/>"
                                <xsl:choose>
                                  <xsl:when test="position()!=last()">,</xsl:when>
                                </xsl:choose>
                              </xsl:for-each>
                            );

var ldCheckboxes = new Array( <xsl:for-each select="/root/PPV_OPTIONS_LD/PPV_OPTION_LD">
                                "<xsl:value-of select="privacy_policy_option_code"/>"
                                <xsl:choose>
                                  <xsl:when test="position()!=last()">,</xsl:when>
                                </xsl:choose>
                              </xsl:for-each>
                            );

var veCheckboxes = new Array( <xsl:for-each select="/root/PPV_OPTIONS_VE/PPV_OPTION_VE">
                                "<xsl:value-of select="privacy_policy_option_code"/>"
                                <xsl:choose>
                                  <xsl:when test="position()!=last()">,</xsl:when>
                                </xsl:choose>
                              </xsl:for-each>
                            );

//NOTE: an empty returnArray means that the user did not hit the submit button.  We HAVE to populate this array on submit otherwise the code will go in an infinite loop
var returnArray = new Array();

        </script>


      </head>
      <body onload="javascript: init()" onkeypress="javascript: checkKeyPressed();">
      <br/>
        <form name="disp" method="post" action="">
          <input type="hidden" name="order_detail_id" id="order_detail_id" value="{/root/PPV_ORDER_INFO/ORDER_INFO/order_detail_id}"/>
          <input type="hidden" name="customer_id" id="customer_id" value="{/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/customer_id}"/>
          <input type="hidden" name="errorNoInfoMessage1" id="errorNoInfoMessage1" value="{key('pageData', 'error_no_info_message_1')/value}"/>
          <input type="hidden" name="errorNoInfoMessage2" id="errorNoInfoMessage2" value="{key('pageData', 'error_no_info_message_2')/value}"/>
          <input type="hidden" name="errorNotEnoughMessage1" id="errorNotEnoughMessage1" value="{key('pageData', 'error_not_enough_message_1')/value}"/>
          <input type="hidden" name="errorNotEnoughMessage2" id="errorNotEnoughMessage2" value="{key('pageData', 'error_not_enough_message_2')/value}"/>
          <input type="hidden" name="rememberMessage1" id="rememberMessage1" value="{key('pageData', 'remember_message_1')/value}"/>
          <input type="hidden" name="rememberMessage2" id="rememberMessage2" value="{key('pageData', 'remember_message_2')/value}"/>

          <!-- security -->
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>

          <table class="mainTable" border="1" frame="border">
            <tr>
              <td>
<!-- ******************************** -->
<!-- ************HEADER************** -->
<!-- ******************************** -->
                <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
                  <tr>
                    <td width="20%" valign="top">
                        <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"></img>
                    </td>
                    <td width="5%">&nbsp;</td>
                    <td  width="45%" valign="center" align="left" class="Header" id="pageHeader">Privacy Policy Verification</td>
                    <td width="30%">&nbsp;</td>
                  </tr>
                  <tr><td colspan="4">&nbsp;</td></tr>
                  <tr><td colspan="4">&nbsp;</td></tr>
               </table>


<!-- ******************************** -->
<!-- ************DETAIL************** -->
<!-- ******************************** -->
                <table width="98%" border="0" cellpadding="0" cellspacing="0">
                  <tr>

                    <!-- ******************************** -->
                    <!-- *****CUSTOMER VALIDATION******** -->
                    <!-- ******************************** -->
                    <td valign="top" width="28%">
                      <table border="0" width="98%" cellpadding="0" cellspacing="0">

                        <tr>
                          <td align="center" style="font-size:14;font-weight:bold;"><u>Customer Validation</u></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                          <td style="font-weight:bold;"><xsl:value-of select="key('pageData','customer_validation_message_1')/value"/></td>
                        </tr>

                        <xsl:choose>
                          <xsl:when test = "key('pageData', 'customer_validation_required_flag')/value = 'Y'">
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                              <td style="font-weight:bold;"><xsl:value-of select="key('pageData','customer_validation_message_2')/value"/></td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                              <td>
                                <table>
                                  <xsl:for-each select="PPV_OPTIONS_CV/PPV_OPTION_CV">
                                    <tr>
                                      <td valign="top">
                                        <input type="checkbox" name="{privacy_policy_option_code}" id="{privacy_policy_option_code}"/>
                                      </td>
                                      <td style="font-weight:bold;">
                                      <!-- privacy_policy_opt_detail_desc can contain <u>.  Thus, did a "document.write".  Note that
                                           special characters like Apostrophes must be saved in DB with escape character (\') or &quot;-->
                                        <script>
                                          document.write('<xsl:value-of select="privacy_policy_opt_detail_desc"/>');
                                        </script>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td style="color:#000099;font-weight:bold;">
                                        <xsl:choose>
                                          <xsl:when test="privacy_policy_option_code = 'CVBtNameAdr'">
                                            <xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/first_name"/>&nbsp;<xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/last_name"/>
                                            <br/>
                                            <xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/address_1"/>
                                            <br/>
                                            <xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/city"/>,&nbsp;<xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/state"/>&nbsp;<xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/zip_code"/>
                                          </xsl:when>
                                          <xsl:when test="privacy_policy_option_code = 'CVBtNameOrd'">
                                            <xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/first_name"/>&nbsp;<xsl:value-of select="/root/PPV_CUSTOMER_INFO/CUSTOMER_INFO/last_name"/>
                                            <br/>
                                            <xsl:for-each select="/root/PPV_CUSTOMER_ORDERS_INFO/CUSTOMER_ORDERS_INFO">
                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<xsl:value-of select="external_order_number"/><br/>
                                            </xsl:for-each>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            &nbsp;
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </td>
                                    </tr>

                                    <xsl:choose>
                                      <xsl:when test="position()!=last()">
                                        <tr><td style="font-size:1;" colspan="2">&nbsp;</td></tr>
                                        <tr><td colspan="2" align="center" style="font-size:14;font-weight:bold;" >--OR--</td></tr>
                                        <tr><td style="font-size:1;" colspan="2">&nbsp;</td></tr>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <tr><td colspan="2">&nbsp;</td></tr>
                                      </xsl:otherwise>
                                    </xsl:choose>

                                  </xsl:for-each>
                                </table>
                              </td>
                            </tr>
                          </xsl:when>
                          <xsl:when test = "key('pageData', 'customer_validation_required_flag')/value = 'V'">
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                              <td style="font-size:16;font-weight:bold;"><i><xsl:value-of select="key('pageData','customer_validation_message_3')/value"/></i></td>
                            </tr>
                          </xsl:when>
                          <xsl:otherwise>
                            <tr><td>&nbsp;</td></tr>
                          </xsl:otherwise>
                        </xsl:choose>
                      </table>
                    </td>

                    <!-- ******************************** -->
                    <!--*************LINE****************-->
                    <!-- ******************************** -->
                    <td valign="top" width="9%" >
                      <table border="0" width="98%" cellpadding="0" cellspacing="0">

                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <xsl:if test = "key('pageData', 'order_validation_required_flag')/value = 'Y'">
                          <tr><td style="font-size:14;font-weight:bold;" align="center">--AND--</td></tr>
                        </xsl:if>

                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>

                        <tr>
                          <td align="center">
                            <img border="0" src="images/black_gradient.gif" width="3" height="300"></img>
                          </td>
                        </tr>
                      </table>
                    </td>

                    <!-- ******************************** -->
                    <!-- ********ORDER VALIDATION******** -->
                    <!-- ******************************** -->
                    <td valign="top" width="45%" >
                      <table border="0" width="98%" cellpadding="0" cellspacing="0">

                        <xsl:if test = "key('pageData', 'order_validation_required_flag')/value = 'Y'">
                          <tr>
                            <td align="center" style="font-size:14;font-weight:bold;"><u>Order Validation</u></td>
                          </tr>
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                            <td style="font-weight:bold;"><xsl:value-of select="key('pageData','order_validation_message_1')/value"/></td>
                          </tr>
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                            <td>
                              <table >
                                <xsl:for-each select="PPV_OPTIONS_OV/PPV_OPTION_OV">
                                  <tr>
                                    <td valign="top">
                                      <input type="checkbox" name="{privacy_policy_option_code}" id="{privacy_policy_option_code}"/>
                                    </td>
                                    <td style="font-weight:bold;">
                                      <!-- privacy_policy_opt_detail_desc can contain <u>.  Thus, did a "document.write".  Note that
                                           special characters like Apostrophes must be saved in DB with escape character (\') or &quot;-->
                                      <script>
                                        document.write('<xsl:value-of select="privacy_policy_opt_detail_desc"/>');
                                      </script>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td style="color:#000099;font-weight:bold;">
                                      <xsl:choose>
                                        <xsl:when test="privacy_policy_option_code = 'OVRName'">
                                          <xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/first_name"/>&nbsp;<xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/last_name"/>
                                        </xsl:when>
                                        <xsl:when test="privacy_policy_option_code = 'OVRAdr'">
                                          <xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/address_1"/>
                                          <br/>
                                          <xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/city"/>,&nbsp;<xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/state"/>&nbsp;<xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/zip_code"/>
                                        </xsl:when>
                                        <xsl:when test="privacy_policy_option_code = 'OVRPhone'">
                                          <xsl:if test="/root/PPV_PHONE_INFO/PHONE_INFO/recipient_day_number != ''">
                                            <script>
                                              document.write(reformatUSPhone('<xsl:value-of select="/root/PPV_PHONE_INFO/PHONE_INFO/recipient_day_number"/>'));
                                            </script>
                                            <br/>
                                          </xsl:if>
                                          <xsl:if test="/root/PPV_PHONE_INFO/PHONE_INFO/recipient_evening_number != ''">
                                            <script>
                                              document.write(reformatUSPhone('<xsl:value-of select="/root/PPV_PHONE_INFO/PHONE_INFO/recipient_evening_number"/>'));
                                            </script>
                                          </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="privacy_policy_option_code = 'OVCard'">
                                          <xsl:value-of select="/root/PPV_ORDER_INFO/ORDER_INFO/card_message"/>
                                        </xsl:when>
                                        <xsl:when test="privacy_policy_option_code = 'OVCC4'">
                                          <xsl:choose>
                                            <xsl:when test="/root/PPV_CC_INFO/CC_INFO/cc_number_masked != ''">
                                              <xsl:value-of select="/root/PPV_CC_INFO/CC_INFO/cc_number_masked"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              N/A
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                          &nbsp;
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </td>
                                  </tr>
                                  <tr><td style="font-size:4;" colspan="2">&nbsp;</td></tr>
                                </xsl:for-each>
                              </table>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                    </td>

                    <!-- ******************************** -->
                    <!--*************EMPTY***************-->
                    <!-- ******************************** -->
                    <td valign="top" width="1%" >
                      <table border="0" width="98%" cellpadding="0" cellspacing="0">
                        <tr><td>&nbsp;</td></tr>
                      </table>
                    </td>

                    <td valign="top" width="17%" >
                      <table border="0" width="98%" cellpadding="0" cellspacing="0">

                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>

                        <!-- ******************************** -->
                        <!-- *******LIMITED DISCLOSURE******* -->
                        <!-- ******************************** -->
                        <tr>
                          <td align="left" style="font-size:12;font-weight:bold;"><u>Limited Disclosure:</u></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                          <td>
                            <table >
                              <xsl:for-each select="PPV_OPTIONS_LD/PPV_OPTION_LD">
                                <tr>
                                  <td valign="top" >
                                    <input type="checkbox" tabindex="-1" name="{privacy_policy_option_code}" id="{privacy_policy_option_code}" onclick="toggleClick('{privacy_policy_option_code}')"/>
                                  </td>
                                  <td style="font-size:10;font-weight:bold;" >
                                    <!-- privacy_policy_opt_detail_desc can contain <u>.  Thus, did a "document.write".  Note that
                                         special characters like Apostrophes must be saved in DB with escape character (\') or &quot;-->
                                    <script>
                                      document.write('<xsl:value-of select="privacy_policy_opt_detail_desc"/>');
                                    </script>
                                  </td>
                                </tr>
                              </xsl:for-each>
                            </table>
                          </td>
                        </tr>

                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>

                        <!-- ******************************** -->
                        <!-- ******VALIDATION EXCEPTION****** -->
                        <!-- ******************************** -->
                        <tr>
                          <td align="left" style="font-size:12;font-weight:bold;"><u>Validation Exception:</u></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                          <td>
                            <table >
                              <xsl:for-each select="PPV_OPTIONS_VE/PPV_OPTION_VE">
                                <tr>
                                  <td id="td_code_{privacy_policy_option_code}" name="td_code_{privacy_policy_option_code}" valign="top">
                                    <input type="checkbox" tabindex="-1" name="{privacy_policy_option_code}" id="{privacy_policy_option_code}" onclick="toggleClick('{privacy_policy_option_code}')"/>
                                  </td>
                                  <td id="td_desc_{privacy_policy_option_code}" name="td_desc_{privacy_policy_option_code}" style="font-size:10;font-weight:bold;">
                                    <!-- privacy_policy_opt_detail_desc can contain <u>.  Thus, did a "document.write".  Note that
                                         special characters like Apostrophes must be saved in DB with escape character (\') or &quot;-->
                                    <script>
                                      document.write('<xsl:value-of select="privacy_policy_opt_detail_desc"/>');
                                    </script>
                                  </td>
                                </tr>
                              </xsl:for-each>
                            </table>
                          </td>
                        </tr>

                      </table>
                    </td>
                    <!-- ******************************** -->
                    <!-- **************END*************** -->
                    <!-- ******************************** -->
                  </tr>
                </table>
                <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                    <td align="center">
                      <input type="button" name="bSubmit" id="bSubmit" class="BlueButton" style="width:80;" accesskey="S" onclick="processSubmit();" value="(S)ubmit"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </form>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>