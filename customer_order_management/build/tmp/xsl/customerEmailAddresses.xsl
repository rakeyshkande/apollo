<!DOCTYPE ACDemo [
 	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<!-- Page/Navigation variables -->
<xsl:variable name="total_records"      select="key('pageData','cea_customer_email_count')/value"/>
<xsl:variable name="cea_current_page"   select="key('pageData','cea_current_page')/value"/>
<xsl:variable name="cea_total_pages"    select="key('pageData','cea_total_pages')/value"/>
<xsl:variable name="show_next"          select="key('pageData','cea_show_next')/value"/>
<xsl:variable name="show_previous"      select="key('pageData','cea_show_previous')/value"/>
<xsl:variable name="show_first"         select="key('pageData','cea_show_first')/value"/>
<xsl:variable name="show_last"          select="key('pageData','cea_show_last')/value"/>

	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Customer Account E-mail Addresses</title>
				<base target="_self" />
				<link rel="stylesheet" href="css/ftd.css"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript" language="javascript">
				var pageAction = new Array("cea_first","cea_previous","cea_next","cea_last");
    		var cea_show_previous = "<xsl:value-of select="key('pageData','cea_show_previous')/value"/>";
        var cea_show_next     = "<xsl:value-of select="key('pageData','cea_show_next')/value"/>";
        var cea_show_first    = "<xsl:value-of select="key('pageData','cea_show_first')/value"/>";
        var cea_show_last     = "<xsl:value-of select="key('pageData','cea_show_last')/value"/>";

				<![CDATA[

/***********************************************************************************
* doExitAction()
************************************************************************************/
function doExitAction()
{
  top.close();
}

/***********************************************************************************
* doPageAction()
************************************************************************************/
function doPageAction(val)
{
  var url = "customerOrderSearch.do" + "?action=" + pageAction[val];
  performAction(url);
}

/***********************************************************************************
* performAction()
************************************************************************************/
function performAction(url)
{
  document.forms[0].action = url;
	document.forms[0].submit();
}

/***********************************************************************************
* checkKey()
************************************************************************************/
function checkKey()
{
  var tempKey = window.event.keyCode;
 
  //left arrow pressed
  if (window.event.altKey && tempKey == 37 && cea_show_first == 'y')
    doPageAction(0);

  //right arrow pressed
  if (window.event.altKey && tempKey == 39 && cea_show_last == 'y')
    doPageAction(3);

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cea_show_next == 'y')
    doPageAction(2);

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cea_show_previous == 'y')
    doPageAction(1);

}



			 	]]>
			 </script>
			</head>
		<body onkeydown="javascript:checkKey();"><br />
		 <form name="emails" method="post">
		 <!-- pageData -->
		  <input type="hidden" name="cea_current_page" id="cea_current_page" value="{key('pageData','cea_current_page')/value}"/>
		  <input type="hidden" name="cea_total_pages" id="cea_total_pages" value="{key('pageData','cea_total_pages')/value}"/>
		  <input type="hidden" name="cea_customer_email_count" id="cea_customer_email_count" value="{key('pageData','cea_customer_email_count')/value}"/>
		  <input type="hidden" name="start_position" id="start_position" value="{key('pageData','cea_start_position')/value}"/>
		  <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
	     <!-- Misc Variables -->
		  <input type="hidden" name="begin_index" id="begin_index" value="{key('pageData','begin_index')/value}"/>
		  <input type="hidden" name="end_index" id="end_index" value="{key('pageData','end_index')/value}"/>
	      <input type="hidden" name="max_records_allowed" id="max_records_allowed" value="{key('pageData','max_records_allowed')/value}"/>
          <input type="hidden" name="max_search_results" id="max_search_results" value="{key('pageData','max_search_results')/value}"/>
		  <input type="hidden" name="show_previous" id="show_previous" value="{key('pageData','cea_show_previous')/value}"/>
		  <input type="hidden" name="show_next" id="show_next" value="{key('pageData','cea_show_next')/value}"/>
		  <input type="hidden" name="scrub_url" id="scrub_url" value="{key('pageData','scrub_url')/value}"/>
		  <input type="hidden" name="show_first" id="show_first" value="{key('pageData','cea_show_first')/value}"/>
		  <input type="hidden" name="show_last" id="show_last" value="{key('pageData','cea_show_last')/value}"/>
		 <!-- security
		  <input type="hidden" name="securitytoken" id="securitytoken" value="{key('security','securitytoken')/value}"/>
		  <input type="hidden" name="context" id="context" value="{key('security','context')/value}"/>
		  -->
		 <!-- search Criteria
		  <input type="hidden" name="sc_cust_number" id="sc_cust_number" value="{key('searchCriteria','sc_cust_number')/value}" />
		  <input type="hidden" name="sc_cc_number" id="sc_cc_number" value="{key('searchCriteria','sc_cc_number')/value}" />
		  <input type="hidden" name="sc_order_number" id="sc_order_number" value="{key('searchCriteria','sc_order_number')/value}" />
		  <input type="hidden" name="sc_email_address" id="sc_email_address" value="{key('searchCriteria','sc_email_address')/value}" />
		  <input type="hidden" name="sc_recip_ind" id="sc_recip_ind" value="{key('searchCriteria','sc_recip_ind')/value}" />
		  <input type="hidden" name="sc_cust_ind" id="sc_cust_ind" value="{key('searchCriteria','sc_cust_ind')/value}" />
		  <input type="hidden" name="sc_zip_code" id="sc_zip_code" value="{key('searchCriteria','sc_zip_code')/value}" />
		  <input type="hidden" name="sc_tracking_number" id="sc_tracking_number" value="{key('searchCriteria','sc_tracking_number')/value}" />
		  <input type="hidden" name="sc_phone_number" id="sc_phone_number" value="{key('searchCriteria','sc_phone_number')/value}" />
		  <input type="hidden" name="sc_rewards_number" id="sc_rewards_number" value="{key('searchCriteria','sc_rewards_number')/value}" />
		  <input type="hidden" name="sc_last_name" id="sc_last_name" value="{key('searchCriteria','sc_last_name')/value}" />
		  -->
		  <xsl:call-template name="securityanddata"/>
		  <xsl:call-template name="decisionResultData"/>
 
<table border="0" align="center" width="98%" class="mainTable" cellpadding="1" cellspacing="1">
<tr>
	<td class="banner">Customer Email Addresses</td>
</tr>
  <tr>
     <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2" class="innerTable">
            <tr style="text-align:center;">
              <td align="center" width="7%" class="Label">Line Number</td>
              <td align="center" width="21%" class="Label">Email Address</td>
              <td align="center" width="11%" class="Label">Membership<br/>Type</td>
              <td align="center" width="6%" class="Label">Status</td>
              <td align="center" width="11%" class="Label">Membership<br/>ID</td>
              <td align="center" width="11%" class="Label">Membership<br/>Start</td>
              <td align="center" width="11%" class="Label">Membership<br/>End</td>
              <td align="center" width="11%" class="Label">Subscribe/<br/>Unsubscribe</td>
              <td align="center" width="11%" class="Label">Modified Date</td>
            </tr>
            <tr>
              <td colspan="9" width="100%" align="center">

                <!-- Scrolling div contiains orders -->
                <div id="orderEmailDiv"  style="overflow:auto;height:120px; padding:0px; margin:0px;">                
                  <table width="100%" border="0" cellpadding="1" cellspacing="1" style="word-break:break-all;">  
                                  
                    <xsl:for-each select="customerMembershipDetail/CEA_CUSTOMER_EMAILS/CEA_CUSTOMER_EMAIL"> 
                                       
                      <tr style="vertical-align:bottom;text-align:center;height:30px;">  
                                          	  
                      	  <td class="lineNumber" width="7%">
						    <script type="text/javascript">document.write(emails.start_position.value); emails.start_position.value++</script>.						    
					      </td>	
					      				      
                          <td align="center" width="21%"><xsl:value-of select="emailAddress"/></td>
						  		<script type="text/javascript">						  			
						  			var mshipType = '--';					  								  								  				
						  			var mshipStatus = '--';
						  			var mshipId = '--';
						  			var mshipStart = '--';
						  			var mshipEnd = '--';
						  					  						  	
						  			<xsl:if test="membershipList">			
							  			<xsl:for-each select="membershipList">							  			
								  			<xsl:if test="membershipType">
								  				mshipType = '<xsl:value-of select="membershipType"/>';									  				
								  				//Data should be displayed only for PC
								  				if(mshipType == "Premier Circle") {								  											  			
									  				<xsl:choose>
										  				<xsl:when test="isMembershipBenefitsApplicable = 'true'">
										  					mshipStatus = 'Active';
										  				</xsl:when>								  				
										  				<xsl:otherwise>	
										  					mshipStatus = 'Inactive';
										  				</xsl:otherwise>
									  				</xsl:choose>
										  			mshipEnd = '<xsl:value-of select="expireDate"/>';									  					
									  				mshipId = '<xsl:value-of select="identifier1"/>';								  				
								  					mshipStart = '<xsl:value-of select="startDate"/>';	
								  				}								  									  				
								  			</xsl:if>									  									  				
							  			</xsl:for-each>
							  		</xsl:if>
							  	</script>
						  <td align="center" width="11%"><script type="text/javascript">document.write(mshipType);</script></td>
						  <td align="center" width="6%"><script type="text/javascript">document.write(mshipStatus);</script></td>
						  <td align="center" width="11%"><script type="text/javascript">document.write(mshipId);</script></td>
						  <td align="center" width="11%"><script type="text/javascript">document.write(mshipStart);</script></td>
						  <td align="center" width="11%"><script type="text/javascript">document.write(mshipEnd);</script></td>                                                             
                          <td align="center" width="11%"><xsl:value-of select="subscribeStatus"/></td>                          
                          <td align="center" width="11%"><xsl:value-of select="updatedOn"/></td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>
              </td>
            </tr>
         </table>
      </td>
    </tr>
   </table>
   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
     <tr>
		<td width="33%" class="Label">
   	              	  	 Page
			<xsl:value-of select="key('pageData','cea_current_page')/value"/>&nbsp;of

      <xsl:choose>
        <xsl:when test="key('pageData','cea_total_pages')/value = '0'">
          <xsl:value-of select="'1'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="key('pageData','cea_total_pages')/value"/>
        </xsl:otherwise>
      </xsl:choose>
		</td>
		<td align="center" width="33%" >
			<table border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td >
						 <!--<a href="#" tabindex="3"><img src="images/firstItem.gif" border="0" name="firstItem" id="firstItem" onmousedown="imgPressed(0);" onmouseup="imgUnPressed(0);" onclick="doPageAction(0);"/></a>-->
            <xsl:choose>
              <xsl:when test="key('pageData','cea_show_first')/value ='y'">
                <button type="button" name="firstItem" tabindex="3" class="arrowButton"  id="firstItem" onclick="doPageAction(0);">                 
                7</button>
              </xsl:when>
              <xsl:when test="key('pageData','cea_show_first')/value ='n'">
                <button type="button" name="firstItem2" tabindex="3" class="arrowButton"  id="firstItem2" onclick="">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                7</button>
              </xsl:when>
            </xsl:choose>
					</td>
					<td >
						<!--<a href="#" tabindex="1"><img src="images/backItem.gif"  border="0" name="backItem" id="backItem" onmousedown="imgPressed(1);" onmouseup="imgUnPressed(1);" onclick="doPageAction(1);"/></a>-->
            <xsl:choose>
              <xsl:when test="key('pageData','cea_show_previous')/value ='y'">
                <button type="button" name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="doPageAction(1);">                 
                3</button>
              </xsl:when>
              <xsl:when test="key('pageData','cea_show_previous')/value ='n'">
                <button type="button" name="backItem2" tabindex="1" class="arrowButton" id="backItem2" onclick="">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                3</button>
              </xsl:when>
            </xsl:choose>
					</td>
					<td >
						<!--<a href="#" tabindex="2"><img src="images/nextItem.gif"  border="0" name="nextItem" id="nextItem" onmousedown="imgPressed(2);" onmouseup="imgUnPressed(2);" onclick="doPageAction(2);"/></a>-->
            <xsl:choose>
              <xsl:when test="key('pageData','cea_show_next')/value ='y'">
                <button type="button" name="nextItem" tabindex="2" class="arrowButton" id="nextItem" onclick="doPageAction(2);">                 
                4</button>
              </xsl:when>
              <xsl:when test="key('pageData','cea_show_next')/value ='n'">
                <button type="button" name="nextItem2" tabindex="2" class="arrowButton" id="nextItem2" onclick="">
                    <xsl:attribute name="disabled">true</xsl:attribute>
                4</button>
              </xsl:when>
            </xsl:choose>
					</td>
					<td >
						<!--<a href="#" tabindex="4"><img src="images/lastItem.gif"  border="0" name="lastItem" id="lastItem" onmousedown="imgPressed(3);" onmouseup="imgUnPressed(3);" onclick="doPageAction(3);"/></a>-->
            <xsl:choose>
              <xsl:when test="key('pageData','cea_show_last')/value ='y'">
                <button type="button" name="lastItem" tabindex="4" class="arrowButton" id="lastItem" onclick="doPageAction(3);">                
                8</button>
              </xsl:when>
              <xsl:when test="key('pageData','cea_show_last')/value ='n'">
                <button type="button" name="lastItem2" tabindex="4" class="arrowButton" id="lastItem2" onclick="">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                8</button>
              </xsl:when>
            </xsl:choose>
					</td>
				</tr>
			</table>
		</td>
		<td class="Label" style="text-align:right">
  		    <xsl:value-of select="key('pageData','cea_customer_email_count')/value"/>&nbsp;Records Found
		</td>
     </tr>
     <tr>
		<td colspan="3" style="padding-right:1em;text-align:center;"><br/>
				<button type="button" class="BlueButton" accesskey="C" onclick="doExitAction();">(C)lose</button>
		</td>
     </tr>
  </table>
  </form>
 </body>
</html>
   </xsl:template>
</xsl:stylesheet>