<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- format xml numbers if not present -->
<xsl:decimal-format NaN="(0.00)"  name="noDiscount"/>
<xsl:decimal-format NaN="" name="notANumber"/>
<xsl:decimal-format NaN="0.00" name="zeroValue"/>

<xsl:template name="refundAndAddBill">
   <xsl:param name="add_refund_node"/>
    <!-- Displays the Net Add Bill or Net Refund amounts  -->
   <table border="0" cellpadding="0" cellspacing="3" width="100%">
      <tr class="label">
           <td width="25%"></td>
 	         <td class="merchTD"><xsl:value-of select="$merch"/></td>
           <td class="addOnTD"><xsl:value-of select="$addOn"/></td>
 	         <td class="servShipTD"><xsl:value-of select="$servShip"/></td>
 	         <td class="taxTD"><xsl:value-of select="$tax"/></td>
           <td class="discountTD"><xsl:value-of select="$discount"/></td>
           <td class="totalTD"><xsl:value-of select="$total"/></td>
      </tr>
      <tr>
         <xsl:if test="$add_refund_node/billing_type != 'add_bill'">
		        <td class="label">Net Refund Amount</td>
         </xsl:if>
          <xsl:if test="$add_refund_node/billing_type = 'add_bill'">
		        <td class="label">Net Add Bill Amount </td>
         </xsl:if>
         <td id="netMerchTD"><xsl:value-of select="format-number($add_refund_node/product_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
         <td id="netAddOnTD"><xsl:value-of select="format-number($add_refund_node/add_on_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
	       <td id="netServShipTD"><xsl:value-of select="format-number($add_refund_node/serv_ship_fee,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
         <td id="netTaxTD"><xsl:value-of select="format-number($add_refund_node/tax,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
         <td id="netDiscountAmtTD"><xsl:value-of select="format-number($add_refund_node/discount_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
 	     <td id="netTotalTD"><xsl:value-of select="format-number($add_refund_node/order_total,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
      </tr>
   </table>

   <!-- display the stylish blue banner -->
  <div style="padding:.3em;">
	   <div style="width:100%;" class="banner"/>
  </div>


  <!-- display the Net Add Bill Charge Amounts or the Net Refund Amounts -->
  <table border="0" cellpadding="0" cellspacing="3" width="100%">
      <tr class="label">
         <td width="25%"></td>
         <td class="merchTD"><xsl:value-of select="$merch"/></td>
         <td class="addOnTD"><xsl:value-of select="$addOn"/></td>
         <td class="servShipTD"><xsl:value-of select="$servShip"/></td>
         <td class="taxTD"><xsl:value-of select="$tax"/></td>
         <td class="discountTD"><xsl:value-of select="$discount"/></td>
         <td class="totalTD"><xsl:value-of select="$total"/></td>
      </tr>

      <!-- call the appropriate template below based on the type of bill being processed -->
         <xsl:call-template name="addBillingInfo">
            <xsl:with-param name="node" select="$add_refund_node"/>
         </xsl:call-template>
	  <tr>
	  <!-- do we display INVALID FORMAT here if values are incorrect? -->
	    <td></td>
	    <td id="entered_merch_error" class="RequiredFieldTxt"></td>
	    <td id="entered_add_on_error" class="RequiredFieldTxt" ></td>
	    <td id="entered_serv_ship_error" class="RequiredFieldTxt" ></td>
	    <td></td>
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	     <td colspan="7" style="text-align:center">
	        <button type="button" class="blueButton" name="refresh" id="refresh" accesskey="R" onclick="refreshDisplay()">
			   <xsl:if test="$add_refund_node/billing_type != 'add_bill'">
			      <xsl:attribute name="disabled">true</xsl:attribute>
			   </xsl:if>
	        (R)efresh</button>
	     </td>
	  </tr>
   </table>

  <!-- create stylish blue seperating banner -->
   <div style="padding:.3em;">
      <div style="width:100%;" class="banner"/>
   </div>


 </xsl:template>




<!-- used if type is Add Bill -->
 <xsl:template name="addBillingInfo">
    <xsl:param name="node"/>
    <tr>
       <xsl:if test="$node/billing_type = 'add_bill'">
          <td class="label">Charged Add Bill Amount</td>
       </xsl:if>
       <xsl:if test="$node/billing_type != 'add_bill'">
           <td class="label">Posted Refund Amount</td>
       </xsl:if>
       <td id="chrgPostMerchTD">
          <xsl:choose>
			<xsl:when test="$node/product_amount > 0 and $node/billing_type = 'add_bill'">
			   <input type="text" name="entered_merch" id="entered_merch" size="4" onblur="isEnteredMerch()" value="{format-number($node/product_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')}"/>
			</xsl:when>
			<xsl:otherwise>
			    <xsl:value-of select="format-number($node/product_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/>
			</xsl:otherwise>
	     </xsl:choose>
      </td>
       <td id="chrgPostAddOnTD">
          <xsl:choose>
 		     <xsl:when test="$node/add_on_amount > 0 and $node/billing_type = 'add_bill'" >
 		           <input type="text" name="entered_add_on" id="entered_add_on" size="4" onblur="isEnteredAddOn();" value="{format-number($node/add_on_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')}"/>
 		     </xsl:when>
 		     <xsl:otherwise>
 		         <xsl:value-of select="format-number($node/add_on_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/>
 		     </xsl:otherwise>
          </xsl:choose>
       </td>
       <td id="chrgPostServShipTD">
          <xsl:choose>
			 <xsl:when test="$node/serv_ship_fee > 0 and $node/billing_type = 'add_bill'">
			    <input type="text" name="entered_serv_ship" id="entered_serv_ship" size="4" onblur="isEnteredServShip()" value="{format-number($node/serv_ship_fee,'#,###,##0.00;(#,###,##0.00)','zeroValue')}"/>
			 </xsl:when>
			 <xsl:otherwise>
			    <xsl:value-of select="format-number($node/serv_ship_fee,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/>
			 </xsl:otherwise>
          </xsl:choose>
       </td>
       <td id="chrgPostTaxTD"><xsl:value-of select="format-number($node/tax,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
       <td id="chrgPostDiscountAmtTD"><xsl:value-of select="format-number($node/discount_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
       <td id="chrgPostTotalTD"><xsl:value-of select="format-number($node/order_total,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
    </tr>
 </xsl:template>





</xsl:stylesheet>