<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template name="itemDiv">
    <xsl:for-each select="CSCI_ORDERS/CSCI_ORDER">
      <xsl:call-template name="addItemDiv">
         <xsl:with-param name="node" select="."/>
      </xsl:call-template>
    </xsl:for-each>
</xsl:template>
<!-- Build Recipient/Order Information-->
<xsl:template name="addItemDiv">
<xsl:variable name="isPhoenixOrder" select="boolean(IS_PHOENIX_ORDER = 'Y')"/>
<xsl:variable name="isVendor" select="boolean(vendor_flag = 'Y')"/>
<xsl:variable name="bypassCOM" select="/root/pageData/data[name='bypass_com']/value"/>
<xsl:variable name="isVipCustomer" select="boolean(/root/CAI_CUSTOMERS/CAI_CUSTOMER/vip_customer ='Y')"/>
<xsl:variable name="isCountryNotUS" select="boolean(country != 'US')"/>
<xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>


<!-- Page level param for isFreeShipProduct-->
<xsl:variable name="isFreeShipProduct">
    <xsl:choose>
        <xsl:when test="./product_type/text() = 'SERVICES' and ./product_sub_type/text() ='FREESHIP'">
    <xsl:value-of select="'Y'"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:value-of select="'N'"/>
    </xsl:otherwise>
    </xsl:choose>
</xsl:variable>
<xsl:variable name="AMAZON_ORDER" select="'AMZNI'"/>

<xsl:variable name="premierCircleMembership"  select="key('pageData','premierCircleMembership')/value"/>
<xsl:variable name="cardMessage"  select="card_message"/>
<xsl:variable name="specialInstructions"  select="special_instructions"/>
<xsl:variable name="custFname"  select="/root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_first_name"/>
<xsl:variable name="custLname"  select="/root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_last_name"/>

  <script type="text/javascript" language="javascript">
  var premierCircleMembership = '<xsl:value-of select="$premierCircleMembership"/>';
  var membership_id = '<xsl:value-of select="//CSCI_CARTS/CSCI_CART/membership_id"/>';
   <![CDATA[
    document.write('<div id="item]]><xsl:value-of select="@num"/><![CDATA[Div"   class="content">');]]>

    var count = 0;

    function countag()
    {
      count++;

      if (count > 1)
      {
        document.write(", ");
      }
    }

  </script>
 
 <xsl:variable name="partnerName"  select="key('pageData','partnerName')/value"/>
 <xsl:variable name="partnerChannelIcon"  select="key('pageData','partnerChannelIcon')/value"/>
 <xsl:variable name="imageServerUrl"  select="key('pageData','imageServerUrl')/value"/>
 
  <script type="text/javascript" language="javascript"> 
  var partnerName = '<xsl:value-of select="$partnerName"/>';
  var partnerChannelIcon = '<xsl:value-of select="$partnerChannelIcon"/>';
  var imageServerUrl = '<xsl:value-of select="$imageServerUrl"/>';
  </script>
  
<input type="hidden" name="order_detail_id{position()}" id="order_detail_id{position()}" value="{order_detail_id}"/>
<input type="hidden" name="external_order_number" id="external_order_number" value="{external_order_number}"/>
<input type="hidden" name="bypass_com"  id="bypass_com" value="{$bypassCOM}" />
<input type="hidden" name="vip_customer" id="vip_customer" value="{$isVipCustomer}"/>
<input type="hidden" name="vipCustUpdatePermission" id="vipCustUpdatePermission" value="{key('pageData','vipCustUpdatePermission')/value}"/>
<input type="hidden" name="vipCustomerMessage" id="vipCustomerMessage" value="{key('pageData','vipCustomerMessage')/value}"/>
<input type="hidden" name="origin_id" id="origin_id" value="{//CSCI_CARTS/CSCI_CART/origin_id}"/>
<input type="hidden" name="product_type" id="product_type" value="{product_type}"/>
<input type="hidden" name="ship_method" id="ship_method" value="{ship_method}"/>
<input type="hidden" name="vendor_flag" id="vendor_flag" value="{vendor_flag}"/>
<input type="hidden" name="roi_order_guid" id="roi_order_guid" value="{order_guid}"/>
<table id="itemContainer" border="0" class="mainTable" width="100%" cellpadding="0" cellspacing="2">


  <tr>

    <td>
      <table border="0" class="innerTable" cellpadding="0" cellspacing="2" width="100%">
       <tr>
        <td>
        <table border="0" align="left" id="recipInfoCustHeader" width="100%" cellpadding="0" cellspacing="2">
          <tr>
            <td class="alignright" width="8%" style="vertical-align:top">
                <span class="Label" >Customer:</span>
            </td>
            <td width="18%" class="alignleft">
              	<xsl:choose>
					<xsl:when test="string-length(concat($custFname, ' ' , $custLname)) > 40">
						<span style="WIDTH: 50px; VERTICAL-ALIGN: top; white-space: nowrap;">
		                	<xsl:value-of select="/root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_first_name"/><br /><xsl:value-of select="/root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_last_name"/>
		                </span>
					</xsl:when>
					<xsl:otherwise>
						<span style="WIDTH: 50px; VERTICAL-ALIGN: top; white-space: nowrap;">
		                	<xsl:value-of select="/root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_first_name"/>&nbsp;<xsl:value-of select="/root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_last_name"/>
		                </span>
					</xsl:otherwise>
				</xsl:choose>
            	<xsl:if test = "$isVipCustomer">
					<span class="ErrorMessage" style=" font-weight:normal; WIDTH:120px; MARGIN-LEFT: 125px; DISPLAY: inline-block">
					    <script type="text/javascript">
				            document.write(document.getElementById('vipCustomerMessage').value);
	 				    </script> 
					</span>
			 	</xsl:if>
			</td>
            <td rowspan="3" align="left" valign="top" width="120">
                    <xsl:if test="/root/CSCI_ORDERS/CSCI_ORDER/partner_name/text() != ''">
                        <img width="50" src="images/{/root/CUSTOMER_PREFERRED_PARTNER/PREFERRED_PARTNER[preferred_processing_resource = /root/CSCI_ORDERS/CSCI_ORDER/partner_name/text()]/display_name}_logo.gif"/>
						<xsl:if test="/root/CSCI_CARTS/CSCI_CART/is_joint_cart/text() = 'Y'">
							<br/><img src="images/partner_jointcart.gif"/>
						</xsl:if>
                    </xsl:if>
					<xsl:variable name="isAmazonOrder" select="boolean(/root/CSCI_CARTS/CSCI_CART/origin_id = $AMAZON_ORDER)"/>
					<xsl:if test="$isAmazonOrder">						
						<img width="50" src="images/amazon_logo_large.gif" /> 
					</xsl:if>
					<script type="text/javascript" language="javascript">
						if (partnerName != ''){
							document.write('<img src="{$imageServerUrl}/{$partnerChannelIcon}" />');
						} 					                   
					</script> 
					<script type="text/javascript" language="javascript">
						if (premierCircleMembership == 'true') {
							document.write('<img width="50" src="images/ftd_premier_circle_logo.gif" />');
						} 						                   
					</script>  
					<script type="text/javascript" language="javascript">
					<xsl:if test="$isPhoenixOrder">
							document.write('<img width="50" src="images/phoenix_logo.gif" />');
					</xsl:if>					                   
					</script>                  
            </td>
            <td  class="alignright" width="11%" style="vertical-align:top">
                <span class="Label">Scrub Date:</span>
            </td>
	    <td width="15%" class="alignleft" style="vertical-align:top">
                <xsl:if test="scrubbed_on_date != ''">
                  <xsl:call-template name="modDate">
                    <xsl:with-param name="dateValue" select="scrubbed_on_date"/>
                  </xsl:call-template>
                </xsl:if>
            </td>
            <td class="alignright" width="8%" style="vertical-align:top">
                <span class="Label">User:</span>
            </td>
            <td width="10%" class="alignleft" style="vertical-align:top">
                <xsl:value-of select="user_id"/>
            </td>
            <td class="alignright" style="vertical-align:top">
               <span class="Label">Company:</span>
            </td>
            <td colspan="1" class="alignleft" style="vertical-align:top">
              <xsl:value-of select="/root/CSCI_CARTS/CSCI_CART/company_name"/>
            </td>
          </tr>
          <tr>
            
            <td class="alignright" width="8%" style="vertical-align:top">
                    <span class="Label">Cust. Phone:</span>
                  </td>
                  <td width="18%" class="alignleft" style="vertical-align:top">
                    <xsl:if test="/root/CAI_PHONES/CAI_PHONE/customer_phone_number !=''">
                      <script type="text/javascript">
                      if(isUSPhoneNumber('<xsl:value-of select="/root/CAI_PHONES/CAI_PHONE/customer_phone_number"/>')){
                        document.write(reformatUSPhone('<xsl:value-of select="/root/CAI_PHONES/CAI_PHONE/customer_phone_number"/>'));
                      }else{
                        document.write('<xsl:value-of select="/root/CAI_PHONES/CAI_PHONE/customer_phone_number"/>');
                      }
                      </script>
                     </xsl:if>
                  </td>
            <td class="alignright" style="vertical-align:top">
                <span class="Label">Order Date:</span>
            </td>
            <td class="alignleft" style="vertical-align:top">
            <xsl:value-of select="/root/CSCI_CARTS/CSCI_CART/order_date_formatted"/>
            &nbsp;
            <xsl:value-of select="/root/CSCI_CARTS/CSCI_CART/order_time_formatted"/>
            </td>
            <td class="alignright" style="vertical-align:top">
               <span class="Label">Origin:</span>
            </td>
            <td  class="alignleft" style="vertical-align:top">
              <xsl:choose>
                <xsl:when test="/root/CSCI_CARTS/CSCI_CART/origin_id = $FOX">
                  <xsl:value-of select="'Internet'"/>
                </xsl:when>
                <xsl:when test="/root/CSCI_CARTS/CSCI_CART/origin_id = ''">
                  <xsl:value-of select="$Phone"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="/root/CSCI_CARTS/CSCI_CART/origin_id"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td class="alignright" style="vertical-align:top">
               <span class="Label">Source&nbsp;Code:</span>
            </td>
            <td  class="alignleft" style="vertical-align:top">
              <a id="sourceCodeLink{@num}" href="javascript:doSourceCodeAction('{source_code}');" class="tooltiptextlink tooltiptextlinkLS"><xsl:value-of select="source_code"/><span><xsl:value-of select="source_code_description"/></span></a>
            </td>
          </tr>
          <tr>
          <td class="alignright" style="vertical-align:top">
              <span class="Label">Order #:</span>
            </td>
            <td class="alignleft" style="vertical-align:top">
              <xsl:value-of select="external_order_number"/>
              <xsl:if test="lifecycle_delivered_status = 'Y'">
                <xsl:choose>
                  <xsl:when test="status_url != ''">
                    <xsl:choose>
                      <xsl:when test="status_url_active = 'Y'">
                        <a href="javascript:showPopup('{status_url}');">
                          <img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                        </a>
                        &nbsp;
                      </xsl:when>
                      <xsl:otherwise>
                        <img src="images/green-check-mark.png" style="width:14px;height:14px;">
                          <xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                        </img>
                        &nbsp;
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;&nbsp;
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </td>
            <td class="alignright" style="vertical-align:top">
              <span class="Label">Hold Status: </span>
            </td>
            <td class="alignleft" style="vertical-align:top">
               <xsl:choose>
                 <xsl:when test="contains(hold_reason,'Fraud') or contains(hold_reason,'FRAUD')">
                   LP Review
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:value-of select="hold_reason"/> 
                 </xsl:otherwise>
               </xsl:choose>
            </td>
            <td class="alignright" style="vertical-align:top">
              <span class="Label">Tagged to: </span>
            </td>
            <td class="alignleft" style="color:red; vertical-align:top">
              <xsl:for-each select="//order_tag/csr_id">
	              <script>countag();</script>
	              <xsl:value-of select="csr_id"/>
              </xsl:for-each>
            </td>
            <td class="alignright" style="vertical-align:top">
              <span class="Label">Offer: </span>
            </td>
            <td class="alignleft">
            <xsl:value-of select="price_description"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="banner" style="text-align:left;">&nbsp;
        Recipient Information
      </td>
    </tr>
    <tr>
      <td>
        <table border="0" id="recipInfo" cellpadding="0" cellspacing="3" width="100%" align="left">
           <tr>
           <td class="alignleft" style="vertical-align:top;" colspan="2" width="40%">
             <span class="Label">Location:</span>
             <span>&nbsp;<xsl:value-of select="address_type_description"/></span>
            </td>
             <td class="alignleft" width="25%">
                    <span class="Label">Occasion:</span>
					&nbsp;<xsl:value-of select="occasion_description"/>
             </td>
             <!-- <td class="alignleft" width="10%">
                     	<xsl:value-of select="occasion_description"/>
             </td> -->
            
            <!-- <td width="10%"></td> -->
            <td colspan="2" style="text-align:right;padding-right:3%;" width="35%">
                <span class="Label">Recipient Id:</span>&nbsp;
                <a id="recipNumberLink{@num}" href="javascript:doRecipientIdAction('{recipient_id}');" class="textlink">
                    <xsl:value-of select="recipient_id"/>
                </a>
            </td>
            </tr>
			<tr>
				<td class="Label" style="vertical-align:top;" align="right" width="15%">
					<xsl:if test="hold_reason != ''">
						<span style="text-align:left;">
							<img src="images/IconLock.gif" />
						</span>&nbsp;
					</xsl:if>
					Name:
				</td>
				<td style="vertical-align:top;" width="25%">
					<xsl:value-of select="first_name" />&nbsp;
					<xsl:value-of select="last_name" />
				</td>
		
		
				<td class="alignleft" style="vertical-align:top;" width="25%">
					<span class="Label">Card Message:</span>
				</td>
				
				<td class="alignleft" width="25%" style="vertical-align:top;">
				<span class="Label">Special Instructions:</span>
			</td>
			<td rowspan="5" style="vertical-align:center text-align:center;" width="15%">
            <xsl:choose>
               <xsl:when test="not($isVendor) and florist_id != ''">
                     <img src="images/FloristIcon.jpg" />   
               </xsl:when>
               <xsl:when test="$isVendor">
                     <img src="images/DropshipIcon.jpg" />   
               </xsl:when>
            </xsl:choose>
		   </td>
			<!-- 	<td width="10%"></td> -->
				
			                   
            </tr>
            <tr>
               <td class="alignright" style="vertical-align:top;">
              <span class="Label">Business:</span>
            </td>
            <td class="alignleft" style="vertical-align:top;">
              <xsl:value-of select="business_name"/>
            </td>
            
            
				<td class="alignleft" rowspan="5">
				<textarea id="cardMessage{@num}"  rows="8" cols="50" wrap="soft" readonly="true" style="overflow:hidden">
						<xsl:choose>
							<xsl:when test="string-length($cardMessage) > 300">
								<xsl:value-of select="substring($cardMessage, 1, 300)"
									disable-output-escaping="yes" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$cardMessage" />
							</xsl:otherwise>
						</xsl:choose>
				</textarea>
				</td>
            	
            	<td class="alignleft" rowspan="5">
					<textarea id="specialIns{@num}"  rows="8" cols="50" wrap="soft"
						readonly="true" style="overflow:hidden">
						<xsl:choose>
							<xsl:when test="string-length($specialInstructions) > 300">
								<xsl:value-of select="substring($specialInstructions, 1, 300)"
									disable-output-escaping="yes" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$specialInstructions" />
							</xsl:otherwise>
						</xsl:choose>
					</textarea>
				</td>
            	<td></td>
            </tr>
            <tr>
            <td class="alignright" style="vertical-align:top;">
              <span class="Label">Address 1:</span>
            </td>
            <td  class="alignleft" style="vertical-align:top;">
              <xsl:value-of select="address_1"/>
            </td>
            <td></td>
          </tr>
          <tr>
            <td class="alignright" style="vertical-align:top;">
              <span class="Label">Address 2:</span>
            </td>
            <td class="alignleft" style="vertical-align:top;">
              <xsl:value-of select="address_2"/>
            </td>
			<td></td>
          </tr>
          <tr>
            <td class="alignright" style="vertical-align:top;">
              <span class="Label">City, State, Zip:</span>
            </td>
            <td class="alignleft" style="vertical-align:top;">
              <xsl:choose>
                <xsl:when test="state != ''">
                  <xsl:value-of select="city"/>,
                  <xsl:value-of select="state"/>
                  &nbsp;&nbsp;
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="city"/>
                </xsl:otherwise>
              </xsl:choose>
                <xsl:value-of select="zip_code"/>
                  &nbsp;&nbsp;
              <xsl:if test="$isCountryNotUS">
			  		<strong>
                    	<xsl:value-of select="country_name"/>                    	 
                    </strong>
			  </xsl:if>	
            </td>
            <td></td>
          </tr>
          <tr>
            <td align="right" style="vertical-align:top;">
              <span class="Label">Phone:</span>
            </td>
            <td style="vertical-align:top;">
              <xsl:if test="recipient_phone_number != ''">
                <script type="text/javascript">
                  if(isUSPhoneNumber('<xsl:value-of select="recipient_phone_number"/>')){
                    document.write(reformatUSPhone('<xsl:value-of select="recipient_phone_number"/>'));
                  }else{
                    document.write('<xsl:value-of select="recipient_phone_number"/>');
                  }
                </script>
              </xsl:if>
            </td>
            
            <td></td>
            
          </tr>
          <tr>
            <td style="text-align:right;vertical-align:top;">
              <span class="Label">Ext:</span>
            </td>
            <td  class="alignleft" style="vertical-align:top;">
              <xsl:value-of select="recipient_extension"/>
            </td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
              <td class="banner" style="text-align:left;">&nbsp;
                Fulfillment Information
              </td>
    </tr>
    <tr>
      <td>
      <xsl:choose>
        <xsl:when test="vendor_flag = 'N' and $isFreeShipProduct = 'Y'">
          <xsl:call-template name="addServicesInformation"/>
        </xsl:when>
        <xsl:when test="vendor_flag = 'N' and $isFreeShipProduct = 'N'">
          <xsl:call-template name="addFloristInformation"/>
        </xsl:when>
        <xsl:when test="vendor_flag = 'Y'">
          <xsl:call-template name="addVendorInformation"/>
        </xsl:when>
      </xsl:choose>
      </td>
    </tr>

    <xsl:if test="personalization_template_id != ''">
      <xsl:if test="personalization_template_id != 'NONE'">
        <tr>
          <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;
          <a class="tooltiptextlink tooltiptextlinkWide"><strong><font color="red">***Personalized Item***</font></strong>
          <span><script>document.write('<xsl:value-of select="pdp_personalization_data"/>');</script></span></a></td>
        </tr>
      </xsl:if>
    </xsl:if>

    <tr>
      <td class="banner" style="text-align:left;">&nbsp;
        Product Summary
      </td>
    </tr>

    <tr >
      <td >
      <!--call recipient/vendor specific xsl summary-->
        <xsl:if test="vendor_flag = 'N'">
          <xsl:call-template name="addRecipSummary">
            <xsl:with-param name="detailId" select="order_detail_id"/>
            <xsl:with-param name="isVendor" select="$isVendor"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="vendor_flag = 'Y'">
          <xsl:call-template name="addVendorSummary">
            <xsl:with-param name="detailId" select="order_detail_id"/>
            <xsl:with-param name="isVendor" select="$isVendor"/>
            <xsl:with-param name="isFreeShipProduct" select="$isFreeShipProduct"/>
          </xsl:call-template>
        </xsl:if>
      </td>
    </tr>

    <tr>
      <td style="padding-left:5em;text-align:left;"><span style="float:left">
        <xsl:choose>
            <xsl:when test="$bypassCOM = 'Y'">
                &nbsp;
            </xsl:when>
            <xsl:otherwise>
                <button type="button"   id="updateOrderInfo{@num}" class="BlueButton" accesskey="U" onclick="javascript:doUpdateOrderInfo('{order_detail_id}', '{ftp_vendor_product}', '{order_held}', '{hold_reason}', '{vendor_flag}', '{product_id}', '{order_date}', '{mercury_flag}' , '{florist_id}' , '{external_order_number}' , '{personalization_template_id}', '{$isFreeShipProduct}', '{order_has_morning_delivery}');">(U)pdate Order Info</button>&nbsp;
                <button type="button"   id="updatePaymentInfo{@num}" class="BlueButton" accesskey="A" onclick="javascript:doUpdatePaymentInfo('{order_detail_id}','{external_order_number}','{source_code}');">Upd(a)te Payment Info</button>&nbsp;
                
                <xsl:if test="not($isVendor) and $isFreeShipProduct='N'"><button type="button"   id="updateFlorist{@num}" class="BlueButton" accesskey="I" onclick="javascript:doUpdateFloristAction('{order_detail_id}','{delivery_date}','{zip_code}');">Update Flor(i)st</button>&nbsp;</xsl:if>
                
                <button type="button"   id="updateDeliveryDateInfo{@num}" class="BlueButton" accesskey="Y" onclick="javascript:doUpdateDeliveryDate('{order_detail_id}', '{ftp_vendor_product}', '{vendor_flag}', '{product_id}', '{mercury_flag}' , '{florist_id}', '{ship_method}', '{subcode}' , '{personalization_template_id}', '{$isFreeShipProduct}' , '{product_type}' );">Update Deliver(y) Date</button>&nbsp;
                
                <xsl:if test="not($isVendor)"><button type="button"   id="updatePhoenixOrder{@num}" class="BlueButton" accesskey="P" onclick="javascript:doPhoenixOrderAction('{order_detail_id}', '{external_order_number}', '{order_held}', '{hold_reason}');">(P)hoenix Order</button>&nbsp;</xsl:if>
                
            </xsl:otherwise>
        </xsl:choose>
        </span>
        <span style="float:right;"><a id="memberInfoLink{@num}" href="javascript:doMemberShipInfoAction('{order_detail_id}');" class="textlink">Member Info</a>&nbsp;</span>
        <br /><br />
      </td>
    </tr>
    
      </table>
      </td>
    </tr>
  </table>

  <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td style="vertical-align:top;text-align:left;">
	 <xsl:choose>
            <xsl:when test="$bypassCOM = 'Y'">
                <button type="button"   id="communiction{@num}" class="BigBlueButton ButtonTextBox2" accesskey="O" onclick="javascript:doCommunicationAction('{order_detail_id}', '{$bypassCOM}');">C(o)mmunication </button>
                <button type="button" id="orderComments{@num}" class="BigBlueButton ButtonTextBox2" accesskey="C" onclick="javascript:doOrderCommentsAction('{order_detail_id}', '{$bypassCOM}','{$isVipCustomer}', '{//CSCI_CARTS/CSCI_CART/origin_id}', '{product_type}', '{ship_method}', '{vendor_flag}', '{roi_order_guid}');">
                     <xsl:if test="order_comment_indicator = 'Y'">
                      <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                     </xsl:if>
                Order<br />(C)omments</button>
                <button type="button"   id="orderHistory{@num}" class="BigBlueButton ButtonTextBox2" accesskey="H" onclick="javascript:doOrderHistoryAction('{order_detail_id}');">Order<br/>(H)istory</button>
                <button type="button"   id="origView{@num}" class="BigBlueButton ButtonTextBox2" accesskey="V" onclick="javascript:doOriginalViewAction('{external_order_number}');">Original<br/>(V)iew</button>
            </xsl:when>
            <xsl:otherwise>
         
                 <button type="button"   id="communiction{@num}" class="BigBlueButton ButtonTextBox2" accesskey="O" onclick="javascript:doCommunicationAction('{order_detail_id}', '{$bypassCOM}');">C(o)mmunication </button>
                 <button type="button"   id="orderComments{@num}" class="BigBlueButton ButtonTextBox2" accesskey="C" onclick="javascript:doOrderCommentsAction('{order_detail_id}', '{$bypassCOM}', '{$isVipCustomer}', '{//CSCI_CARTS/CSCI_CART/origin_id}', '{product_type}', '{ship_method}', '{vendor_flag}', '{roi_order_guid}');">
                 <xsl:if test="order_comment_indicator = 'Y'">
                  <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                 </xsl:if>
                Order<br />(C)omments</button>
                 <button type="button"   id="refund{@num}" class="BigBlueButton ButtonTextBox1" accesskey="R" onclick="javascript:doOrderRefundAction('{order_detail_id}', '{//CSCI_CARTS/CSCI_CART/origin_id}', '{product_type}', '{ship_method}', '{vendor_flag}', '{roi_order_guid}');" >
	                <xsl:if test="order_refund_indicator = 'Y'">
	                  <xsl:attribute name="style">font-weight:bold</xsl:attribute>
	                </xsl:if>
                      (R)efund</button>
                 <button type="button"  id="email{@num}" class="BigBlueButton ButtonTextBox1" accesskey="E" email="{//CSCI_CARTS/CSCI_CART/email_address}" onclick="javascript:doOrderEmailAction('{order_detail_id}','email{@num}', '{$bypassCOM}', '{$isVipCustomer}', '{//CSCI_CARTS/CSCI_CART/origin_id}', '{product_type}', '{ship_method}', '{vendor_flag}', '{roi_order_guid}');">
                  <xsl:if test="order_emails_indicator = 'Y'">
                  <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                 </xsl:if>
                (E)mail</button>
                <button type="button"   id="floristInquiry{@num}" class="BigBlueButton ButtonTextBox1" accesskey="F" onclick="javascript:doFloristInquiry();">(F)lorist<br />Inquiry</button>
        
                <button type="button"   id="tagOrder{@num}" class="BigBlueButton ButtonTextBox1" accesskey="T" onclick="javascript:doTagOrderAction('{order_tagged_indicator}','{order_detail_id}');">
                  <xsl:choose>
                    <xsl:when test="order_tagged_indicator = '1Y' or  order_tagged_indicator = '1N' or order_tagged_indicator = '2Y' or order_tagged_indicator = '2N'">
                    <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                    </xsl:when>
                  </xsl:choose>
                (T)ag<br />Order</button><br/>
                <button type="button"   id="orderHistory{@num}" class="BigBlueButton ButtonTextBox2" accesskey="H" onclick="javascript:doOrderHistoryAction('{order_detail_id}');">Order<br/>(H)istory</button>
                <button type="button"   id="origView{@num}" class="BigBlueButton ButtonTextBox2" accesskey="V" onclick="javascript:doOriginalViewAction('{external_order_number}');">Original<br />(V)iew</button>
                <button type="button"   id="reconciliation{@num}" class="BigBlueButton ButtonTextBox1" accesskey="N" onclick="javascript:doReconciliationAction(view_recon_url,'{order_detail_id}','{external_order_number}');">Reconciliation<br />I(n)quiry</button> 
                <xsl:if test="//data[name='customerHoldPermission']/value = $YES and //data[name='lossPreventionPermission']/value = $YES">
                  <button type="button"   id="hold{@num}" class="BigBlueButton ButtonTextBox1" accesskey="D" onclick="javascript:doHoldDetailAction('order_detail',{order_detail_id},'{external_order_number}');">Hol(d)</button>
                </xsl:if>                	
               		<xsl:if test="country = 'CA' and key('pageData','surchargeExplanation')/value != ''">                		
					<br/>
						<xsl:value-of select="key('pageData','surchargeExplanation')/value"
				    			disable-output-escaping="yes"/>
			    	 
			    	 </xsl:if>
				    	 
            </xsl:otherwise>
        </xsl:choose>
      </td>
      <td style="text-align:right;vertical-align:top;">
        <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
            <button type="button"   class="BigBlueButton" accesskey="K" id="lpsearch" onclick="javascript:doLossPreventionSearchAction();">
                Bac(k) to Loss<br />Prevention Search
            </button>
        </xsl:if>
        <button type="button"   id="search{@num}" class="BigBlueButton" accesskey="B" onclick="javascript:doSearchAction();">(B)ack to Search</button><br/>
        <button type="button"   id="printOrder{@num}" class="BigBlueButton" style="width:55px;" accesskey="P" onclick="doPrintOrderAction('{order_detail_id}');">(P)rint<br />Order</button>
        <button type="button"    id="mainMenu{@num}" class="BigBlueButton" style="width:55px;" accesskey="M" onclick="javascript:doMainMenuAction();">(M)ain<br />Menu</button>
      </td>
    </tr>
  </table>
  <xsl:call-template name="footer"/>
  <script type="text/javascript" language="javascript"><![CDATA[
    document.write('</div>');]]>
  </script>
  <iframe id="updateOrderFrame" width="0px" height="0px" border="0" class="hidden" src="blank.html" />
   <iframe id="checkLock" width="0px" height="0px" border="0" class="hidden" src="blank.html"/>
   <iframe id="modifyOrderValidation" width="0px" height="0px" border="0" class="hidden" src="blank.html"/>
   <iframe id="deliveryDateValidation" width="0px" height="0px" border="0" class="hidden" src="blank.html"/>
   <iframe id="phoenixOrderFrame" width="0px" height="0px" border="0" class="hidden" src="blank.html"/>
</xsl:template>


<!--
  Template to be called when the recipient inforamtion
  contains vendor specific information
-->
<xsl:template name="addVendorSummary">
  <xsl:param name="detailId"/>
  <xsl:param name="isVendor"/>
  <xsl:param name="isFreeShipProduct"/>
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td width="40%" style="VERTICAL-ALIGN: middle">
        <table border="0" id="vendorSummary"  width="100%" cellpadding="0" cellspacing="3">
          <tr>
            <td align="right" class="Label" width="8%">
              Delivery&nbsp;Date:
            </td>
            <td width="33%" class="alignleft">
                      <xsl:value-of select="delivery_date_dow"/>,&nbsp;<xsl:value-of select="delivery_date"/>
            </td>
          </tr>
          <xsl:if test="$isFreeShipProduct = 'N'">
              <tr>
                <td class="Label" align="right">
                  Ship&nbsp;Method:
                </td>
                <td class="alignleft">
                  <xsl:value-of select="ship_method_desc"/>
                </td>
              </tr> 
              <tr>
                <td class="Label" align="right">
                  Ship&nbsp;Date:
                </td>
                <td class="alignleft">
                  <xsl:choose>
                    <xsl:when test="shipping_system = 'FTD WEST'">
                      <xsl:if test="venus_ship_date !=''">
                        <xsl:call-template name="modDate">
                          <xsl:with-param name="dateValue" select="ship_date"/>                      
                        </xsl:call-template>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test="ship_date != ''">
                      <xsl:call-template name="modDate">
                        <xsl:with-param name="dateValue" select="ship_date"/>                      
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="ship_date"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
               </tr>

          </xsl:if>
          <tr>
            <td align="right" class="Label">
              Item:
            </td>
            <td class="alignleft">
              <xsl:choose>
                <xsl:when test="subcode != ''">
                  <xsl:value-of select="subcode"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="product_id"/>
                </xsl:otherwise>
               </xsl:choose>
               <xsl:if test="premier_collection_flag = 'Y'">
                 &nbsp;&nbsp;&nbsp;<img border="0" src="images/luxury.jpg" align="absmiddle"/>
               </xsl:if>
            </td>
          </tr>
          <tr>
            <td align="right" class="Label">
              Price:
            </td>
            <td class="alignleft">
              <xsl:variable name="orderDetailId" select="order_detail_id"/>
					    <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../product_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
            </td>
          </tr>
          <tr>
            <td class="Label" align="right" >
                Description:
            </td>
            <td class="alignleft">
              <xsl:choose>
                <xsl:when test="subcode != ''">
                  <xsl:value-of select="subcode_description"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="product_name"/>
                </xsl:otherwise>
               </xsl:choose>
            </td>
          </tr>
          <tr>
            <td align="right" class="Label">
              Add-On(s):
            </td>
            <td class="alignleft">
                <xsl:choose>
                  <xsl:when test="count(//CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON/order_detail_id[. = $detailId]) > 1">
                    <select id="add-on-details">
                    <xsl:apply-templates select="/root/CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON">
                      <xsl:with-param name="id" select="$detailId"/>
                      <xsl:with-param name="useLabel" select="false()"/>
                    </xsl:apply-templates>
                    </select>
                  </xsl:when>
                  <xsl:when test="count(//CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON/order_detail_id[. = $detailId]) = 1 ">
                  	<span id="add-on-details">
	                    <xsl:apply-templates select="/root/CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON">
	                      <xsl:with-param name="id" select="$detailId"/>
	                      <xsl:with-param name="useLabel" select="true()"/>
	                    </xsl:apply-templates>
                    </span>
                  </xsl:when>

                </xsl:choose>
            </td>
          </tr>
        </table>
      </td>
      <!-- Product Image -->
      <td id="prodImageCell" style="VERTICAL-ALIGN: middle" width="5%">
         <img src="{small_image_url}" width="75" height="75" onload="imgOnLoad(document.getElementById('imgAltText{$detailId}'))" onerror="imgOnError(this)"></img>
         <span id="imgAltText{$detailId}" style="font-size: 8pt; color: #959595; display: hidden">No Product Image</span>         
      </td>      
      <!-- Billing Info -->
      <td width="55%" style="vertical-align:top;">
        <xsl:call-template name="addBillingInfo">
          <xsl:with-param name="detailId" select="$detailId"/>
          <xsl:with-param name="isVendor" select="$isVendor"/>
        </xsl:call-template>
      </td>
    </tr>
  </table>
</xsl:template>
<!-- addVendorInformation-->
<xsl:template name="addVendorInformation">
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td id="vendorName" align="center" style="vertical-align:top;width:30%">
        <span class="Label">Vendor Name</span><br />
        <xsl:value-of select="florist_name"/>
      </td>
      <td id="shippingStatus" align="center" style="vertical-align:top;width:15%">
        <span class="Label">Shipping Status</span><br />
        <xsl:choose>
               <xsl:when test="rejected_status != '' ">
                      <xsl:value-of select="rejected_status"/>
               </xsl:when>
               <xsl:when test="delivered_status !='' ">
                      <xsl:value-of select="delivered_status"/>
               </xsl:when>
               <xsl:when test="shipped_status != '' ">
                      <xsl:value-of select="shipped_status"/>
               </xsl:when>
               <xsl:when test="printed_status != '' ">
                      <xsl:value-of select="printed_status"/>
               </xsl:when>
        </xsl:choose>
      </td>
      <td id="shippingPartner" align="center" style="vertical-align:top;width:15%">
        <span class="Label">Shipping Partner</span><br />
        <xsl:value-of select="shipping_partner"/>
      </td>
      <td id="trackingNumber" align="center" style="vertical-align:top;width:25%">
        <span class="Label">Tracking Number</span><br />
        <a href="javascript:doCarrierTrackingPopup('{tracking_url}{tracking_number}')"><xsl:value-of select="tracking_number"/></a>
      </td>
      <td><br /><br /></td>
    </tr>
  </table>
</xsl:template>
<xsl:template match="CSCI_ORDER_ADDON">
<xsl:param name="id"/>
<xsl:param name="useLabel"/>
  <xsl:choose>
  <xsl:when test="$useLabel">
    <xsl:if test="order_detail_id[. = $id]">
      <xsl:value-of select="add_on_quantity"/>&nbsp;
      <xsl:value-of select="description" disable-output-escaping="yes"/>
    </xsl:if>
  </xsl:when>
  <xsl:otherwise>
    <xsl:if test="order_detail_id[. = $id]">
    <option>
      <xsl:value-of select="add_on_quantity"/>&nbsp;
      <xsl:value-of select="description" disable-output-escaping="yes"/>
    </option>
  </xsl:if>
  </xsl:otherwise>
  </xsl:choose>

</xsl:template>


 <!--
  Tempalte to be called when the recipient information
  contains florist specific information
-->
 <xsl:template name="addRecipSummary">
   <xsl:param name="detailId"/>
   <xsl:param name="isVendor"/>
  <table  border="0" width="100%" cellpadding="0" cellspacing="0" >
    <tr>
      <td width="40%" style="VERTICAL-ALIGN: middle">
        <table border="0" id="recipientSummary" width="100%" cellpadding="0" cellspacing="3" align="left">
          <tr>
            <td align="right" class="Label" width="8%">
              Delivery&nbsp;Date:
            </td>
            <td width="33%" class="alignleft">
                       <xsl:value-of select="delivery_date_dow"/>,&nbsp;<xsl:value-of select="delivery_date"/>
                       <xsl:if test="delivery_date_range_end != ''">
                         &nbsp;-&nbsp;<xsl:value-of select="delivery_date_range_end_dow"/>,&nbsp;<xsl:value-of select="delivery_date_range_end"/>
                     </xsl:if>
            </td>
          </tr>
          <tr>
            <td class="Label" align="right">
              Item:
            </td>
            <td class="alignleft">
              <xsl:choose>
                <xsl:when test="subcode != ''">
                  <xsl:value-of select="subcode"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="product_id"/>
                </xsl:otherwise>
               </xsl:choose>
               <xsl:if test="premier_collection_flag = 'Y'">
                 &nbsp;&nbsp;&nbsp;<img border="0" src="images/luxury.jpg" align="absmiddle"/>
               </xsl:if>
            </td>
          </tr>
          <tr>
            <td class="Label" align="right">
              Price:
            </td>
            <td class="alignleft">
              <xsl:variable name="orderDetailId" select="order_detail_id"/>
					    <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../product_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
            </td>

          </tr>
          <tr>
            <td class="Label" align="right" style="vertical-align:top;">
              Description:
            </td>
            <td class="alignleft">
              <xsl:choose>
                <xsl:when test="subcode != ''">
                  <xsl:value-of select="subcode_description"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="product_name"/>
                </xsl:otherwise>
               </xsl:choose>
            </td>
          </tr>
          <tr>
            <td align="right" class="Label">
              Add-On(s):
            </td>
            <td class="alignleft">
                <xsl:choose>
                  <xsl:when test="count(//CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON/order_detail_id[. = $detailId]) > 1">
                    <select id="add-on-details">
	                    <xsl:apply-templates select="/root/CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON">
	                      <xsl:with-param name="id" select="$detailId"/>
	                      <xsl:with-param name="useLabel" select="false()"/>
	                    </xsl:apply-templates>
                    </select>
                  </xsl:when>
                  <xsl:when test="count(//CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON/order_detail_id[. = $detailId]) = 1 ">
                  	<span id="add-on-details">
	                    <xsl:apply-templates select="/root/CSCI_ORDER_ADDONS/CSCI_ORDER_ADDON">
	                      <xsl:with-param name="id" select="$detailId"/>
	                      <xsl:with-param name="useLabel" select="true()"/>
	                    </xsl:apply-templates>
                    </span>
                  </xsl:when>
                </xsl:choose>
            </td>
          </tr>
        </table>
      </td>
      <!-- Product Image -->
      <td id="prodImageCell" style="VERTICAL-ALIGN: middle" width="5%">
         <img src="{small_image_url}" width="75" height="75" onload="imgOnLoad(document.getElementById('imgAltText{$detailId}'))" onerror="imgOnError(this)"></img>
         <span id="imgAltText{$detailId}" style="font-size: 8pt; color: #959595; display: hidden">No Product Image</span>         
      </td>      
      <!-- Billing Info -->
      <td width="55%" style="vertical-align:top;">
        <xsl:call-template name="addBillingInfo">
          <xsl:with-param name="detailId" select="$detailId"/>
          <xsl:with-param name="isVendor" select="$isVendor"/>
        </xsl:call-template>
      </td>
    </tr>
  </table>
 </xsl:template>
  <!-- Services Information section -->
 <xsl:template name="addServicesInformation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" style="vertical-align:top;width:20%">
        <span class="Label">Program Name</span><br />
        <xsl:value-of select="key('pageData', 'service_displayname_freeship')/value"/>
        </td>
        
        <td align="center" style="vertical-align:top;width:15%">
        <span class="Label">Program Description</span><br />
        <xsl:value-of select="key('pageData', 'service_description_freeship')/value"/>
        </td>
        
        <td align="center" style="vertical-align:top;width:20%">
        <span class="Label">Program Information</span><br />
        <a><xsl:attribute name="target">_blank</xsl:attribute><xsl:attribute name="href"><xsl:value-of select="key('pageData', 'service_url_freeship')/value"/></xsl:attribute><xsl:value-of select="key('pageData', 'service_url_freeship')/value"/></a>
        </td>
    </tr>
    </table>
 </xsl:template>
 <!-- Florist Information section -->
 <xsl:template name="addFloristInformation">
 <xsl:variable name="isVendor" select="boolean(vendor_flag = 'Y')"/>
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr align="center">
      <td style="vertical-align:top;width:25%">
        <span class="Label">Florist Name</span><br />
         <xsl:choose>
          <xsl:when test="florist_id = ''">
              <xsl:value-of select="'UNASSIGNED'"/>
          </xsl:when>
          <xsl:otherwise>
            <a class="tooltiptextlink" tabindex="-1" id="floristNameLink{@num}" href="javascript:doFloristMaintenance('{florist_id}');">
            	<xsl:value-of select="florist_name"/>
            	<span>
            		<xsl:value-of select="florist_id"/>
            	</span>
            </a>
          </xsl:otherwise>
         </xsl:choose>
      </td>
      <td style="vertical-align:top;width:16%">
        <span class="Label">Florist Phone Number</span><br />
         <xsl:if test="florist_phone_number != ''">
          <script type="text/javascript">
            if(isUSPhoneNumber('<xsl:value-of select="florist_phone_number"/>')){
              document.write(reformatUSPhone('<xsl:value-of select="florist_phone_number"/>'));
            }else{
              document.write('<xsl:value-of select="florist_phone_number"/>');
            }
           </script>
         </xsl:if>
      </td>
      <td style="vertical-align:top;width:12%">
        <span class="Label">Status</span><br />
        <xsl:value-of select="translate(florist_status, $lower_case, $upper_case)"/>
        <br /><br />
      </td>
      <td style="vertical-align:top;width:10%">
        <span class="Label">Zip Cutoff</span><br />
        <xsl:value-of select="zipcode_cutoff"/>
      </td>
      <td style="vertical-align:top;width:15%">
        <span class="Label">Minimum Order Amount</span><br />
        <xsl:if test="florist_id != ''">
             $<xsl:value-of select="format-number(minimum_order_amount,'#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
        </xsl:if>
      </td>
      <td style="vertical-align:top;width:12%">
        <span class="Label">SKU Codification</span><br />
      		<xsl:choose>
                <xsl:when test="codified_product = 'Y'">
                       <xsl:choose>
                              <xsl:when test="florist_codified_blocked != '' and florist_codified_blocked = 'Y' ">
                                     <xsl:value-of select="'BLOCKED'" />
                              </xsl:when>
                              <xsl:when test="florist_codified != '' and florist_codified = 'Y' ">
                                     <xsl:value-of select="'YES'" />
                              </xsl:when>
                              <xsl:otherwise>
                                     <xsl:value-of select="'NO'" />
                              </xsl:otherwise>
                       </xsl:choose>
                </xsl:when>
        	</xsl:choose>
      </td>
      <td style="vertical-align:top;width:10%">
        <span class="Label">Mercury</span><br />
        
        <xsl:choose>
				<xsl:when test="florist_id != ''">
					<xsl:choose>
						<xsl:when test="mercury_flag = 'Y' or mercury_flag = 'M'">
							<xsl:value-of select="'YES'" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'FTDM'" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
		</xsl:choose>
      </td>
    </tr>
  </table>
 </xsl:template>

 <!-- Billing Information-->
	<xsl:template name="addBillingInfo">
	<xsl:param name="detailId"/>
	<xsl:param name="isVendor"/>
  <table border="0" id="billingInfo" cellpadding="0" cellspacing="3" width="100%">
			<tr align="right">
				<td>
					<td class="Label">
						Merchandise
					</td>
					
                                        <td class="Label">
                                                Add-On(s)
                                        </td>
					
					<td class="Label">
						Service/Shipping
					</td>
					<td class="Label">
						Discount
					</td>
					<td class="Label">
						Tax
					</td>
					<td class="Label">
						Total
					</td>
                                        <xsl:choose>
                                          <xsl:when test="$isMilesPoints = 'Y'"><td class="Label">Miles/<br/>Points</td></xsl:when>
                                          <xsl:otherwise><td></td></xsl:otherwise>
                                        </xsl:choose>
				</td>
			</tr>

			<!--
			<xsl:apply-templates select="/root/CSCI_ORDER_BILLS/CSCI_ORDER_BILL">
				<xsl:with-param name="id" select="$detailId"/>
				<xsl:with-param name="isVendor" select="$isVendor"/>
			</xsl:apply-templates>
			-->
			<!-- Origianl Order Bill -->
			<tr>
				<td width="15%" class="Label" align="right">Original:</td>
				<td align="right">  
					<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../product_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</td>
        <td align="right">
         	<span class="orderAddOnValue" onmouseover="displayOrderAddOnsOnMouseOver({$detailId}, this, event);" style="text-decoration: none; cursor:default; color: black;">
         	<xsl:choose>
	           	<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../add_on_discount_amount) != 0 and //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../add_on_discount_amount != ''">
					<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original' ]/../add_on_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../add_on_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original' ]/../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</xsl:otherwise>
			</xsl:choose>
			</span>
			<span id="spanOrderAddOnsMouseOver_{$detailId}" class="tooltip" ></span>
	         <script>
	         	$(".orderAddOnValue").mouseout(function() {
	         		$("#spanOrderAddOnsMouseOver_<xsl:value-of select="$detailId" />").hide();
	         		$("iframe.hidden").attr("style", "display:none");
	         		$("div.visible").attr("style", "margin:0px");
	       		});
	       	 </script>
        </td>

        <td align="right" >
		       <span class="feeValue" onmouseover="displayOrderFeesOnMouseOver(
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../service_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')}' ,
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../shipping_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')}' ,
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../vend_sat_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')}' ,
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../ak_hi_special_charge, '#,###,##0.00;-#,###,##0.00', 'notANumber')}' ,
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../fuel_surcharge_amt, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',
				        '{//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../surcharge_description/text()}',
				        '{//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../apply_surcharge_code/text()}',
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../same_day_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../morning_delivery_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',				         
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../vendor_sun_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../vendor_mon_upcharge, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../late_cutoff_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',
				        '{format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../international_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')}',
				         {$detailId},this, event);"
		       		style="text-decoration: none; cursor:default; color: black;">
		         <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../serv_ship_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
		            
		        </span>
				<span id="spanOrderMouseOver_{$detailId}" class="tooltip" ></span>
                  <script>
                  $(".feeValue").mouseout(function() {
                  		$("#spanOrderMouseOver_<xsl:value-of select="$detailId" />").hide();
                  		$("iframe.hidden").attr("style", "display:none");
                  		$("div.visible").attr("style", "margin:0px");
                  });
                  </script>
				</td>
				<td style="text-align:right;color:black">
					<span class="orderDiscountValue" onmouseover="displayOrderDiscountOnMouseOver({$detailId}, this, event);" style="text-decoration: none; cursor:default; color: black;">
					<xsl:choose>
		            	<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../add_on_discount_amount) != 0 and //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../add_on_discount_amount != ''">
							<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../discount_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../discount_amount, '- #,###,##0.00', 'noRefund')"/>
						</xsl:otherwise>
					</xsl:choose>
					</span>
					<span id="spanOrderDiscountMouseOver_{$detailId}" class="tooltip" ></span>
			         <script>
			         	$(".orderDiscountValue").mouseout(function() {
			         		$("#spanOrderDiscountMouseOver_<xsl:value-of select="$detailId" />").hide();
			         		$("iframe.hidden").attr("style", "display:none");
			         		$("div.visible").attr("style", "margin:0px");
			       		});
			       	 </script>
				</td>
				<td align="right">
				    <span class="taxValue" onmouseover="displayOrderTaxesOnMouseOver({$detailId}, this, event);" style="text-decoration: none; cursor:default; color: black;">
					<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../tax, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
					</span>
    				<span id="spanOrderTaxMouseOver_{$detailId}" class="tooltip" ></span>
                    <script>
                    $(".taxValue").mouseout(function() {
                    	$("#spanOrderTaxMouseOver_<xsl:value-of select="$detailId" />").hide();
                    	$("iframe.hidden").attr("style", "display:none");
                    	$("div.visible").attr("style", "margin:0px");
                  	});
                    </script>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../bill_total, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</td>
                                <xsl:choose>
                                    <xsl:when test="$isMilesPoints = 'Y'">				
                                        <td align="right">
                                            <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Original']/../miles_points_amount, '#,###,##0;-#,###,##0', 'notANumberNoDecimal')"/>
                                        </td>
                                    </xsl:when>
                                  <xsl:otherwise><td align="right"></td></xsl:otherwise>
                                </xsl:choose>
			</tr>
       <!-- Additional Bills -->
			<tr>
				<td width="15%" class="Label" align="right">Add Bill(s):</td>
          <td align="right">
             <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../product_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
          </td>
          
          <td align="right">
            <xsl:choose>
	           	<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount) != 0 and //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount != ''">
					<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill' ]/../add_on_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill' ]/../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</xsl:otherwise>
			</xsl:choose>
          </td>
          
         <td align="right">
            <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../serv_ship_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
         </td>

				<xsl:choose>
					<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount) = 0 or //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount = '0' ">
						<td style="text-align:right;color:black">
							<xsl:choose>
				            	<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount) = 0 or //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount = ''">
				            		<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:when test="//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount >= 0">
									<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:when>
					<xsl:when test="//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/total_type[. = 'Additional Bill']/../discount_amount >= 0">
						<td style="text-align:right;color:black">
							<xsl:choose>
				            	<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount) = 0 or //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount = ''">
				            		<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:when test="//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount >= 0">
									<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:when>
					<xsl:otherwise>
						<td style="text-align:right;color:black">
							<xsl:choose>
				            	<xsl:when test="string-length(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount) = 0 or //CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount = ''">
				            		<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount * -1, '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:when test="//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount >= 0">
									<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount * -1) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number((//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../discount_amount * -1) + (//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../add_on_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:otherwise>
				</xsl:choose>

				<td align="right">
					<xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../tax, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</td>
				<td align="right">
					 <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../bill_total, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
				</td>
                                <xsl:choose>
                                    <xsl:when test="$isMilesPoints = 'Y'">				
                                        <td align="right">
                                            <xsl:value-of select="format-number(//CSCI_ORDER_BILLS/CSCI_ORDER_BILL/order_detail_id[. = $detailId and ../total_type = 'Additional Bill']/../miles_points_amount, '#,###,##0;-#,###,##0', 'notANumberNoDecimal')"/>
                                        </td>
                                    </xsl:when>
                                  <xsl:otherwise><td align="right"></td></xsl:otherwise>
                                </xsl:choose>
			</tr>
      <!-- Refunds -->
      <tr style="text-align:right;color:black">
         <td class="Label" style="color:#000000">Refund(s):</td>
         <td >
            <xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../product_amount, '-#,###,##0.00','noRefund')"/>
         </td>
        
         <td>
            <xsl:choose>
	           	<xsl:when test="string-length(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount) != 0 and //CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount != ''">
					<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount), '-#,###,##0.00','noRefund')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_amount, '-#,###,##0.00','noRefund')"/>
				</xsl:otherwise>
			</xsl:choose>
         </td>
         
        <td>
           <xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../serv_ship_fee, '-#,###,##0.00','noRefund')"/>
        </td>

		    <xsl:choose>
		      <xsl:when test="string-length(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) = 0">
					  <td>
						 <span style="color:black">
							 <xsl:choose>
							 	<xsl:when test="string-length(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount) = 0 or //CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount = ''">
				            		<xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
								</xsl:when>
								<xsl:when test="//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount >= 0">
									<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
								</xsl:otherwise>
							 </xsl:choose>
						 </span>
					  </td>
		      </xsl:when>
		      <xsl:when test="//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount >= 0">
					  <td>
						 <span style="color:black">
							 <xsl:choose>
							 	<xsl:when test="string-length(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount) = 0 or //CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount = ''">
				            		<xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
								</xsl:when>
								<xsl:when test="//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount >= 0">
									<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount * -1) >= (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount)">
											<xsl:value-of select="format-number(((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount)) * -1, '-#,###,##0.00', 'noRefund')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
										</xsl:otherwise>
									</xsl:choose>
									
								</xsl:otherwise>
							 </xsl:choose>
						 </span>
					  </td>
		      </xsl:when>
		      <xsl:otherwise>
					  <td>
							<span style="color:black">
							<xsl:choose>
								<xsl:when test="string-length(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount) = 0 or //CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount = ''">
				            		<xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount * -1, '-#,###,##0.00', 'noRefund')"/>
								</xsl:when>
								<xsl:when test="//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount >= 0">
									<xsl:choose>
										<xsl:when test="(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount) >= (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount * -1)">
											<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="format-number(((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount)) * -1, '-#,###,##0.00', 'noRefund')"/>
										</xsl:otherwise>
									</xsl:choose>
									
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number((//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../discount_amount * -1) + (//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../addon_discount_amount * -1), '-#,###,##0.00', 'noRefund')"/>
								</xsl:otherwise>
							</xsl:choose>
							</span>
						</td>
		      </xsl:otherwise>
		    </xsl:choose>

        <td >
           <xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../tax, '-#,###,##0.00','noRefund')"/>
        </td>
        <td >
           <xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../refund_total, '-#,###,##0.00','noRefund')"/>
        </td>
        <xsl:choose>
            <xsl:when test="$isMilesPoints = 'Y'">				
                <td >
                   <xsl:value-of select="format-number(//CSCI_ORDER_REFUNDS/CSCI_ORDER_REFUND/order_detail_id[. = $detailId]/../miles_points_amount, '-#,###,##0','noRefundNoDecimal')"/>
                </td>
            </xsl:when>
          <xsl:otherwise><td></td></xsl:otherwise>
        </xsl:choose>
     </tr>
      <tr align="right">
         <td class="Label" >Current Totals:</td>
      <td>
         <xsl:value-of select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../product_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
     </td>
        
     <td>
	    <xsl:choose>
          	<xsl:when test="string-length(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_discount_amount) != 0 and /root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_discount_amount != ''">
				<xsl:value-of select="format-number((/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_amount) + (/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_discount_amount), '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
			</xsl:otherwise>
		</xsl:choose>
     </td>
       
     <td>
        <xsl:value-of select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../serv_ship_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
     </td>
     <td style="text-align:right;color:black">
		<xsl:choose>
           	<xsl:when test="string-length(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_discount_amount) != 0 and /root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_discount_amount != ''">
				<xsl:value-of select="format-number((/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../discount_amount) + (/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../add_on_discount_amount), '-#,###,##0.00', 'noRefund')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../discount_amount, '-#,###,##0.00', 'noRefund')"/>
			</xsl:otherwise>
		</xsl:choose>
     </td>
     <td>
        <xsl:value-of select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../tax, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
     </td>
     <td>
    <table style="border: 1px solid black;" cellpadding="1">
	<tr>
		<td >
			<xsl:value-of
				select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../order_total, '#,###,##0.00;-#,###,##0.00', 'notANumber')" />
		</td>
	</tr>
	</table>
     </td>
    <xsl:choose>
        <xsl:when test="$isMilesPoints = 'Y'">				
            <td align="right">
                <xsl:value-of select="format-number(/root/CSCI_ORDER_TOTALS/CSCI_ORDER_TOTAL/order_detail_id[. = $detailId]/../miles_points_total, '#,###,##0;-#,###,##0', 'notANumberNoDecimal')"/>
            </td>
        </xsl:when>
      <xsl:otherwise><td align="right"></td></xsl:otherwise>
    </xsl:choose>
  </tr>
  <tr>
    <td colspan="8" style="text-align:right;font-size:9pt">
    <a id="orderPaymentInfoLink{@num}" href="javascript:doRecipPaymentInfo('{order_detail_id}');" class="textlink">Payment Information</a>&nbsp;
    </td>

    <xsl:if test="/root/CSCI_ORDER_FEES_SAVED/CSCI_ORDER_FEE_SAVED/order_detail_id[. = $detailId]/../order_fees_saved > 0">
        <tr>
         <td colspan="4" align="left" class="InformationalMsg"><xsl:value-of select="/root/CSCI_ORDER_FEES_SAVED/CSCI_ORDER_FEE_SAVED/order_detail_id[. = $detailId]/../order_fees_saved_message/text()"/></td>
        </tr>
        <tr/><tr/>
    </xsl:if>
  </tr>
</table>

 </xsl:template>

 <!-- Template to build Orginal and Additional Bill -->
 <xsl:template match="CSCI_ORDER_BILL">
   <!-- holds the order_detail_id -->
   <xsl:param name="id"/>
   <xsl:param name="isVendor"/>
 <tr>
  <xsl:choose>
    <xsl:when test="total_type = 'Original' and order_detail_id = $id">
      <td width="15%" class="Label" align="right">Original:</td>
    </xsl:when>
    <xsl:when test="total_type = 'Additional Bill' and order_detail_id = $id">
      <td width="15%" class="Label" align="right">Addtl<br />Bill(s):</td>
    </xsl:when>
  </xsl:choose>
  <xsl:apply-templates select="order_detail_id[. = $id]">
    <xsl:with-param name="isVendor" select="$isVendor"/>
  </xsl:apply-templates>
 </tr>
 </xsl:template>

 <xsl:template match="order_detail_id" >
 <xsl:param name="isVendor"/>
    <td align="right">
    <xsl:value-of select="format-number(../product_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
    </td>
  <xsl:if test="not($isVendor)">
    <td align="right">
    <xsl:value-of select="format-number(../add_on_amount, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
    </td>
  </xsl:if>
    <td align="right">
    <xsl:value-of select="format-number(../serv_ship_fee, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>

    </td>
    <td style="text-align:right;color:black">
      <xsl:value-of select="format-number(../discount_amount, '-#,###,##0.00', 'noRefund')"/>
    </td>
    <td align="right">
      <xsl:value-of select="format-number(../tax, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>
    </td>
    <td align="right">
    <xsl:value-of select="format-number(../bill_total, '#,###,##0.00;-#,###,##0.00', 'notANumber')"/>

    </td>
 </xsl:template>
 <xsl:template name="modDate">
  <xsl:param name="dateValue"/>
  <xsl:param name="hackDate" select="substring($dateValue,1,10)"/>
   <xsl:value-of select="concat(substring($hackDate,6,2),'/',substring($hackDate,9,2),'/',substring($dateValue,1,4))"/>
</xsl:template>
</xsl:stylesheet>
