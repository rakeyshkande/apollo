<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- xsl imports -->

<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
	<html>
		<head>
			<base target="_self"/>
			<title>FTD - Refund Disposition</title>
			<link rel="stylesheet" href="css/ftd.css"/>
			<script type="text/javascript" src="js/commonUtil.js"/>
			<script type="text/javascript" src="js/util.js"/>
			<script type="text/javascript">
												
			var refund_disp = '<xsl:value-of select="key('pageData','refund_disp_code')/value"/>';
			var refund_display = '<xsl:value-of select="key('pageData','refund_disp_code')/value"/>';
			var resp_party = '<xsl:value-of select="key('pageData','responsible_party')/value"/>';
			var refund_ind = '<xsl:value-of select="key('pageData','full_refund_ind')/value"/>';
      var refundCanBeApplied = '<xsl:value-of select="key('pageData','refund_can_be_applied')/value"/>';
	<![CDATA[
var refundType = '';
function init(){
  setNavigationHandlers();
  var ret = new Array();
  ret[0] = "EXIT";

	/*
   *	If we have values for these values return to cart
	 */
	if(refundCanBeApplied == 'Y' || refundCanBeApplied == 'y'){
		ret[0] = refund_disp;
		ret[1] = refund_display;
		ret[2] = resp_party;
		ret[3] = refund_ind;

	 submitToCart(ret);
	}
  /* Initially disable responsible party drop down */
  //var respPart = document.getElementById('respParty');
  //respPart.disabled = 'true';

}
function doCloseAction()
{
  var ret = new Array();
  ret[0] = "EXIT";
	submitToCart(ret);

}
function doRefundChange(){
  var dispSelect = document.getElementById('refundDisp');
  //var ddl = document.getElementById('respParty');

  /* retrieve value from drop down */
  //var value = dispSelect.options[dispSelect.selectedIndex].text;

  //doEnableDisableAction(dispSelect,ddl,value);
}

/* Enable-Disable the Responsible Party drop down */
function doEnableDisableAction(dispSelect,ddl,value){
  var cha = value.charAt(0);
  if(cha == 'B'){
    refundType = cha;
    ddl.disabled = false;
  }
  if(cha == 'A'){
    refundType = cha;
	ddl.selectedIndex = "0";
    ddl.disabled = "true";

  }
}

function doReturnToCartAction(){
  document.getElementById("submitButton").disabled = true;
  var dispCode = document.getElementById('refundDisp');
  //var respPart = document.getElementById('respParty');
  var index = dispCode.selectedIndex;
  //var partIndex = respPart.selectedIndex;

	if(index == 0){
		document.getElementById("submitButton").disabled = false;
		alert('Please make a selection.');
    return;
	}

  /* refund dispostion */
  var refund_type = document.getElementById('refundType'+index).value;
  var fullInd = document.getElementById('fullRefundInd'+index).value;

  var url = "customerOrderSearch.do?action=check_refund_status" +
	    "&refund_disp_code=" + dispCode.value +
	    "&refund_display_type=" + refund_type +
	    "&full_refund_ind=" + fullInd +
      "&entity_type=PAYMENTS&order_level=ORDERS&entity_id=" + document.getElementById('order_guid').value;

  /* responsible party */
  //if((partIndex == 0) && refundType != 'A'){
  //  alert('Please select a responsible party for the refund.');
  //  return;
  //}
  //if(partIndex != 0 ){
    //var respParty = document.getElementById('respParty'+partIndex).value;
  //}

  //if(respParty != null && respParty != '')
	//url +=   "&responsible_party=" + respParty;

  performAction(url);
}

function HandleOnClose() {
    //check to see if user tried to close the browser by clicking on the X
   if (event.clientX > 400) {
      event.returnValue = '';
      var ret = new Array();
      ret[0] = "EXIT";
      submitToCart(ret);
   }

}

function performAction(url){
	document.forms[0].action = url;
	document.forms[0].submit();

}
function submitToCart(ret){
	top.returnValue = ret;
	top.close();
}
]]>
			</script>
		</head>
		<body onload="javascript:init();" onbeforeunload="HandleOnClose()">
		<br />
		 <form name="disp" method="post" action="">
      	<!-- security -->
			<xsl:call-template name="securityanddata"/>
			<xsl:call-template name="decisionResultData"/>
      <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
      <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>

      <xsl:for-each select="RefundDispositions/Disposition">
        <input type="hidden" id="refundType{@num}" value="{refund_display_type}"/>
        <input type="hidden" id="fullRefundInd{@num}" value="{full_refund_indicator}"/>
		  </xsl:for-each>

		 <xsl:for-each select="OrderFlorists/Florist">
      	<input type="hidden" id="respParty{@num}" value="{responsible_party}"/>
		 </xsl:for-each>
		  <xsl:if test="key('pageData','message_display')/value != ''">
		   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
				   <td class="largeErrorMsgTxt">
						<xsl:value-of select="key('pageData','message_display')/value"/>
				   </td>
			   </tr>
		   </table>
		   </xsl:if>
			<table align="center" class="mainTable" width="98%" height="25%" cellpadding="1" cellspacing="0">
				<tr>
					<td>
						<table class="innerTable"  width="100%" height="20px"  cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td style="vertical-align:top">
									<table border="0" align="center"  width="80%" cellpadding="3" cellspacing="0" >
										<tr>
											<td align="center" class="Label" width="100%">Refund Disposition</td>
											<!--<td class="Label">:Responsible Party</td>-->
											<td width="2"></td>
											<td>
												<select id="refundDisp" onchange="javascript:doRefundChange()">
													<option  value="0" selected="selected">- select one -</option>
													<xsl:for-each select="RefundDispositions/Disposition[refund_display_type='A']">
														<option value="{refund_disp_code}">
															<xsl:value-of select="refund_disp_code"/>&nbsp;&nbsp;
															<xsl:value-of select="short_description"/>
														</option>
													</xsl:for-each>
												</select>
											</td>
											<!--<td>
												<select id="respParty">
													<option value="0" selected="selected">- select one -</option>
													<xsl:for-each select="OrderFlorists/Florist">
														<option value="{responsible_party}">
															<xsl:value-of select="responsible_party"/>
														</option>
													</xsl:for-each>
												</select>
											</td>-->
										</tr>
                    <tr>
                      <td colspan="3">
                        <div style="overflow:auto; width:100%;height:50px; padding:0px; margin:0px;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <xsl:for-each select="pageData/data/name[contains(.,'refund_message')]">
                             <ul>
                              <tr class="largeErrorMsgTxt">
                                <td width="100%"><li><xsl:value-of select="../value"/></li></td>
                              </tr>
                              </ul>
                            </xsl:for-each>                                                    
                          </table>
                        </div>
                      </td>
                    </tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br />
			<table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
				<tr style="text-align:center;">
					<td><button type="button" class="BlueButton" id="submitButton" onclick="javascript:doReturnToCartAction()">(S)ubmit</button>&nbsp;
					<button type="button" class="BlueButton" onclick="javascript:doCloseAction();" >(C)lose</button>
					</td>
				</tr>
			</table>
		   </form>
		</body>
	</html>
   </xsl:template>
</xsl:stylesheet>