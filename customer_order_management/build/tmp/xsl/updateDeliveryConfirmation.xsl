<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>
<xsl:import href="breadcrumbs.xsl"/>
<xsl:output indent="yes" method="html"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:variable name="originalData" select="/root/ORIG_PRODUCTS/ORIG_PRODUCT"/>
<xsl:variable name="updatedData" select="/root/UPDATED_PRODUCTS/UPDATED_PRODUCT"/>
<xsl:variable name="OK" select="'O'"/>
<xsl:variable name="CONFIRM" select="'C'"/>
<xsl:template match="/root">
<html>
<head>
  <title>FTD - Update Delivery Confirmation</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script type="text/javascript" src="js/FormChek.js"/>
  <script type="text/javascript" src="js/util.js"/>
  <script type="text/javascript" src="js/commonUtil.js" />
  <script type="text/javascript" src="js/updateDeliveryConfirmation.js"/>
  <script type="text/javascript" language="javascript">
    var containsErrors = <xsl:value-of select="boolean(ERROR_MESSAGES/ERROR_MESSAGE)"/>;
    var fieldErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[FIELDNAME!='']">["<xsl:value-of select="FIELDNAME"/>","<xsl:value-of select="TEXT"/>",parent.validateFormHelper,false]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var okErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[POPUP_INDICATOR=$OK]">["<xsl:value-of select="@id"/>", "<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE_YES"/>" , "<xsl:value-of select="POPUP_GOTO_PAGE_NO"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var confirmErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[POPUP_INDICATOR=$CONFIRM]">["<xsl:value-of select="@id"/>", "<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE_YES"/>" , "<xsl:value-of select="POPUP_GOTO_PAGE_NO"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );

		var shipCharges = '<xsl:value-of select="UPDATED_PRODUCTS/UPDATED_PRODUCT/shipping_fee"/>';
		var serviceCharges = '<xsl:value-of select="UPDATED_PRODUCTS/UPDATED_PRODUCT/service_fee"/>';
		var shipPlusService;
    var siteNameSsl = '<xsl:value-of select="$sitenamessl"/>';
    var applicationContext = '<xsl:value-of select="$applicationcontext"/>';
<![CDATA[
	//convert the shipCharges into a number
	var temp1 = shipCharges - 0;

	//convert the serviceCharges into a number
	var temp2 = serviceCharges - 0;

	//add the service and shipping charges
	shipPlusService = temp1 + temp2;

	//round it to 2 decimal places.
	shipPlusService = shipPlusService.toFixed(2);

]]>



  </script>
</head>
<body onload="javascript:init();">
    <xsl:call-template name="addHeader"/>

    <!-- Locking IFrame -->
    <iframe id="checkLock" width="0px" height="0px" border="0"/>

    <!-- Cancel Update IFrame -->
    <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>

    <!-- Main Content Div used to hide content when submitting requests -->
    <div id="mainContent" style="display:block">

    <!-- Customer Product data -->
    <xsl:call-template name="cusProduct">
      <xsl:with-param name="subheader" select="subheader"/>
      <xsl:with-param name="displayContinue" select="false()"/>
      <xsl:with-param name="displayComplete" select="true()"/>
      <xsl:with-param name="displayCancel" select="true()"/>
    </xsl:call-template>

    <form name="udcForm" action="updateDeliveryConfirmation.do" method="post">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
    <input type="hidden" name="vendor_flag" id="vendor_flag" value="{$updatedData/vendor_flag}"/>
    <input type="hidden" name="order_detail_id" id="order_detail_id" value="{$updatedData/order_detail_id}"/>
    <input type="hidden" name="order_guid" id="order_guid" value="{$updatedData/order_guid}"/>
    <input type="hidden" name="customer_id" id="customer_id" value="{$originalData/customer_id}"/>
    <input type="hidden" name="master_order_number" id="master_order_number" value="{$updatedData/master_order_number}"/>
    <input type="hidden" name="message_order_number" id="message_order_number" value="{key('pageData', 'message_id')/value}"/>
    <input type="hidden" name="external_order_number" id="external_order_number" value="{$originalData/external_order_number}"/>
    <input type="hidden" name="message_type" id="message_type" value="venus"/>
    <input type="hidden" name="recipient_city" id="recipient_city" value="{$updatedData/recipient_city}"/>
    <input type="hidden" name="recipient_state" id="recipient_state" value="{$updatedData/recipient_state}"/>
    <input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{$updatedData/recipient_zip_code}"/>
    <input type="hidden" name="recipient_country" id="recipient_country" value="{$updatedData/recipient_country}"/>
    <input type="hidden" name="source_code" id="source_code" value="{$updatedData/source_code}"/>
    <input type="hidden" name="occasion" id="occasion" value="{$updatedData/occasion}"/>
    <input type="hidden" name="florist_type" id="florist_type" value=""/>
    <input type="hidden" name="delivery_date" id="delivery_date" value="{$updatedData/delivery_date}"/>
    <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{$updatedData/delivery_date_range_end}"/>
    <input type="hidden" name="florist_id" id="florist_id" value="{$updatedData/florist_id}"/>
    <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="category_index" id="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="company_id" id="company_id" value="{key('pageData', 'company_id')/value}"/>
    <input type="hidden" name="occasion_description" id="occasion_description" value="{key('pageData', 'occasion_description')/value}"/>
    <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData', 'orig_product_amount')/value}"/>
    <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData', 'orig_product_id')/value}"/>
    <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData', 'origin_id')/value}"/>
    <input type="hidden" name="page_number" id="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="product_id" id="product_id" value="{$updatedData/product_id}"/>
    <input type="hidden" name="reward_type" id="reward_type" value="{$updatedData/discount_reward_type}"/>
    <input type="hidden" name="ship_method" id="ship_method" value="{key('pageData', 'ship_method')/value}"/>
    <input type="hidden" name="is_custom_order" id="is_custom_order" value="{key('pageData', 'is_custom_order')/value}"/>

    <!-- Customer Product params -->
    <xsl:call-template name="cusProduct-params">
      <xsl:with-param name="subheader" select="subheader"/>
    </xsl:call-template>

    <table class="mainTable" align="center" border="0" cellpadding="0" cellspacing="1" width="98%">
      <tr>
        <td>
          <table class="innerTable" align="center" border="0" cellpadding="0" cellspacing="1" width="100%">

            <!-- Original details -->
            <tr>
              <td width="100%">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
                  <tr>
                    <td width="50%" valign="top">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <xsl:if test="$updatedData/vendor_flag = 'N'">
                          <xsl:if test="$originalData/vendor_flag = 'N'">
                            <tr>
                              <td width="20%"><strong>Current Florist:</strong></td>
                              <td><xsl:value-of select="$updatedData/florist_name"/>&nbsp;-&nbsp;<xsl:value-of select="$updatedData/florist_id"/></td>
                            </tr>
                          </xsl:if>
                        </xsl:if>
                      </table>
                    </td>
                    <td valign="bottom">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td colspan="2" valign="top"><strong>Source Code</strong>:&nbsp;<xsl:value-of select="$updatedData/source_code"/>&nbsp;-&nbsp;<xsl:value-of select="$updatedData/source_description"/></td>
                        </tr>
                        <tr>
                          <td><strong>Subtotal is US Dollars</strong><br/>(Including shipping/service fees&nbsp;&amp;&nbsp;taxes )</td>
                          <td class="innerTable" bgcolor="#C6D7EF" align="center" valign="center"><big><strong>$&nbsp;<xsl:value-of select="key('pageData', 'order_total')/value"/></strong></big></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                </table>
              </td>
            </tr>

            <!-- Updated details -->
            <tr>
              <td width="100%">
                <table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099" width="98%">
                  <tr>
                    <td>
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>

                          <!-- Picture -->
                          <td width="25%" valign="center">
                            <table width="100%" border="0" cellspacing="4">
                              <tr>
                                <td valign="top">
                                  <div align="center">
                                    <img src="{key('pageData', 'product_images')/value}{$updatedData/product_id}.jpg" width="127" height="113"/>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>

                          <!-- Item -->
                          <td width="25%" valign="top">
                            <table width="100%" border="0" cellspacing="4">
                              <tr>
                                <td colspan="2" bgcolor="#C0D7ED">Item</td>
                              </tr>
                              <tr>
                                <td colspan="2"><strong><xsl:value-of select="$updatedData/product_name"/></strong></td>
                              </tr>
                              <tr>
                                <td colspan="2"><xsl:value-of select="$updatedData/product_id"/></td>
                              </tr>
                              <xsl:if test="$updatedData/color1_description != ''">
                                <tr>
                                  <td width="75%" class="indent">1st Color Choice</td>
                                  <td><xsl:value-of select="$updatedData/color1_description"/></td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="$updatedData/color2_description != ''">
                                <tr>
                                  <td width="75%" class="indent">2nd Color Choice</td>
                                  <td><xsl:value-of select="$updatedData/color2_description"/></td>
                                </tr>
                              </xsl:if>
                              <tr>
                                <td colspan="2"><a href="javascript:doLinkAction('loadDeliveryInfo.do');">Editx Delivery Information</a></td>
                              </tr>
                              <tr>
                                <td colspan="2"><a href="javascript:doLinkAction('loadProductCategory.do');">Change Product</a></td>
                              </tr>
                              <tr>
                              <xsl:choose>
                              	<xsl:when test="//is_customer_order = 'Y'">
                              		<td colspan="2"><a href="javascript:doLinkAction('loadCustomProductDetail.do');">Edit Item Information</a></td>
                              	</xsl:when>
                              	<xsl:otherwise>
                              		<td colspan="2"><a href="javascript:doLinkAction('loadProductDetail.do');">Edit Item Information</a></td>
                              	</xsl:otherwise>
                              </xsl:choose>
                              </tr>
                            </table>
                          </td>
<!-- Start -->
                          <!-- Price -->
                          <td width="25%" valign="top">
                            <table width="100%" border="0" cellspacing="1">
                              <tr>
                                <td colspan="2" bgcolor="#C0D7ED">Price</td>
                              </tr>

                              <tr>
                                <td width="75%">
                                	Price
                                </td>
                                <td width="25%" align="right">
                                  <xsl:choose>
                                    <xsl:when test="$updatedData/discount_amount &gt; 0">
                                      $<strike style="color:red"><xsl:value-of select="format-number($updatedData/product_amount, '#,###,##0.00')"/></strike>&nbsp;<xsl:value-of select="format-number($updatedData/product_amount - $updatedData/discount_amount, '#,###,##0.00')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="format-number($updatedData/product_amount, '#,###,##0.00')"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </td>
															</tr>
															<xsl:for-each select="UPDATED_ADDONS/UPDATED_ADDON">
																<xsl:if test="description != ''">
	                              	<tr>
	                                	<td width="75%">
	                                		<xsl:value-of select="description"/>(<xsl:value-of select="add_on_quantity"/>)
	                                	</td>
		                                <td width="25%" align="right">
	                                    $<xsl:value-of select="format-number(price * add_on_quantity, '#,###,##0.00')"/>
		                                </td>
																	</tr>
																</xsl:if>
															</xsl:for-each>

                              <xsl:choose>
	                            	<xsl:when test="$updatedData/ship_method = 'SD'">
		                              <tr>
		                                <td width="75%">
		                                	Service Charge:
		                                </td>
		                                <td width="25%" align="right">
	                                  	$<script>document.write(shipPlusService)</script>
		                                </td>
																	</tr>
	                            	</xsl:when>
	                            	<xsl:when test="$updatedData/ship_method_desc != ''">
		                              <tr>
		                                <td width="75%">
																			<xsl:value-of select="$updatedData/ship_method_desc"/>
		                                </td>
		                                <td width="25%" align="right">
	                                  	$<xsl:value-of select="format-number($updatedData/shipping_fee, '#,###,##0.00')"/>
		                                </td>
																	</tr>
	                            	</xsl:when>
	                            	<xsl:otherwise>
																	<xsl:if test="$updatedData/service_fee != 0">
																		<tr>
																			<td width="75%">
																				Service Charge:
																			</td>
																			<td width="25%" align="right">
																				$<xsl:value-of select="format-number($updatedData/service_fee, '#,###,##0.00')"/>
																			</td>
																		</tr>
																	</xsl:if>
	                            	</xsl:otherwise>
                            	</xsl:choose>

	                            <xsl:if test="$updatedData/tax != 0">
		                            <tr>
		                              <td width="75%">
																		Taxes:
		                              </td>
		                              <td width="25%" align="right">
	                                	$<xsl:value-of select="format-number($updatedData/tax, '#,###,##0.00')"/>
		                              </td>
																</tr>
	                            </xsl:if>
	                            <xsl:if test="$updatedData/shipping_tax != 0">
		                            <tr>
		                              <td width="75%">
																		Shipping Tax:
		                              </td>
		                              <td width="25%" align="right">
	                                	$<xsl:value-of select="format-number($updatedData/shipping_tax, '#,###,##0.00')"/>
		                              </td>
																</tr>
	                            </xsl:if>
<!-- End -->

                              <tr>
                                <td colspan="2"><hr/></td>
                              </tr>
                              <tr>
                                <td width="75%">
                                  <strong>Item Total</strong>
                                </td>
                                <td width="25%" align="right">
                                  <input type="hidden" name="florist_prd_price" id="florist_prd_price" value="{format-number(key('pageData', 'order_total')/value, '#.00')}"/>
                                  <strong>$<xsl:value-of select="format-number(key('pageData', 'order_total')/value, '#,###,##0.00')"/></strong>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <xsl:if test="$updatedData/miles_points > 0">
                                    <xsl:choose>
                                      <xsl:when test="$updatedData/discount_reward_type = 'Dollars'">
                                        <strong>Dollars</strong>&nbsp;$<xsl:value-of select="format-number($updatedData/discount_amount, '#,###,##0.00')"/>
                                      </xsl:when>
                                      <xsl:when test="$updatedData/discount_reward_type = 'Percent'">
                                        <strong>Percent</strong>&nbsp;$<xsl:value-of select="format-number($updatedData/discount_amount, '#,###,##0.00')"/>
                                      </xsl:when>
                                      <xsl:when test="($updatedData/discount_reward_type = 'Dollars' or $updatedData/discount_reward_type = 'TotSavings') and $updatedData/miles_points > 0">
                                        <strong><xsl:value-of select="$updatedData/discount_reward_type"/></strong>&nbsp;$<xsl:value-of select="format-number($updatedData/miles_points, '#,###,##0.00')"/>
                                      </xsl:when>
                                      <xsl:when test="($updatedData/discount_reward_type = 'Miles' or $updatedData/discount_reward_type = 'Points') and $updatedData/miles_points > 0">
                                        <strong><xsl:value-of select="$updatedData/discount_reward_type"/></strong>&nbsp;<xsl:value-of select="$updatedData/miles_points"/>
                                      </xsl:when>
                                    </xsl:choose>
                                  </xsl:if>
                                </td>
                              </tr>
                            </table>
                          </td>

                          <!-- Delivery Info -->
                          <td width="25%" valign="top">
                            <table width="100%" border="0" cellspacing="4">
                              <tr>
                                <td bgcolor="#C0D7ED">Delivery Address</td>
                              </tr>
                              <tr>
                                <td colspan="2" valign="top">
                                  <table width="100%" border="0" cellspacing="0">
                                    <tr>
                                      <td colspan="2">
                                        <strong><xsl:value-of select="$updatedData/recipient_address_type"/></strong><br/>
                                        <xsl:if test="$updatedData/recipient_business_name != ''"><xsl:value-of select="$updatedData/recipient_business_name"/><br/></xsl:if>
                                        <xsl:value-of select="$updatedData/recipient_first_name"/>&nbsp;<xsl:value-of select="$updatedData/recipient_last_name"/><br/>
                                        <xsl:value-of select="$updatedData/recipient_address_1"/><br/>
                                        <xsl:if test="$updatedData/recipient_address_2 != ''"><xsl:value-of select="$updatedData/recipient_address_2"/><br/></xsl:if>
                                        <xsl:value-of select="$updatedData/recipient_city"/>,&nbsp;<xsl:value-of select="$updatedData/recipient_state"/>&nbsp;<xsl:value-of select="$updatedData/recipient_zip_code"/><br/>
                                        <xsl:value-of select="$updatedData/recipient_country"/><br/>
                                        <xsl:value-of select="$updatedData/recipient_phone_number"/><xsl:if test="$updatedData/recipient_extension != ''">&nbsp;ext.&nbsp;<xsl:value-of select="$updatedData/recipient_extension"/></xsl:if><br/><br/>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="40%"><strong>Delivery Date:</strong></td>
                                      <td><xsl:value-of select="$updatedData/delivery_date"/><xsl:if test="$updatedData[1]/delivery_date_range_end != $updatedData[1]/delivery_date">&nbsp;-</xsl:if></td>
                                    </tr>
                                    <xsl:if test="$updatedData[1]/delivery_date != $updatedData[1]/delivery_date_range_end">
                                    <tr>
                                      <td/>
                                      <td><xsl:value-of select="$updatedData/delivery_date_range_end"/></td>
                                    </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </form>

    <!-- Bottom buttons -->
    <div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
      <div style="text-align:right;padding-top:.5em">
         <span style="padding-right:2em;"><button type="button" style="width:110px;" class="BigBlueButton" accesskey="p" onclick="doLinkAction('paymentPage.do');">Com(p)lete Update</button></span>
         <button type="button" class="BigBlueButton" style="width:110px;" accesskey="C" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
      </div>
    </div>

    <!-- Footer -->
    <xsl:call-template name="footer"/>

    <!-- end mainContent Div -->
    </div>

    <!-- Processing message div -->
    <div id="waitDiv" style="display:none">
      <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
        <tr>
          <td width="100%">
            <table width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                <td id="waitTD" width="50%" class="waitMessage"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <xsl:call-template name="footer"/>
    </div>
  </body>
</html>
</xsl:template>

<xsl:template name="addHeader">
   <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Update Delivery Confirmation'"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
   </xsl:call-template>
</xsl:template>
</xsl:stylesheet>