<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:template match="/">
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"/>
      <title>FTD - Refund Percentage</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
      <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
         <script type="text/javascript" src="js/util.js"></script>
         <script type="text/javascript" src="js/calendar.js"></script>
         <script type="text/javascript" src="js/clock.js"></script>
	 <script type="text/javascript">
	  <![CDATA[


function closeWindow(){
 top.close();
}

function validatePercent()
{

  var form = document.refPercent;

//  if(form.percent.value>'100' || !isPositiveInteger(form.percent.value))
  if((form.percent.value>100)||(form.percent.value<0)||(form.percent.value==""))
  {
    alert("Percent amount is missing or bad. Please make sure percent is between 1 and 100 before submitting.");
    return false;
  }
  if((form.chkMsd.checked==false)&&(form.chkAddon.checked==false)&&(form.chkFee.checked==false)){
  	alert("Please check one or more items to which you wish to apply the percent.");
  	return false; 	
  }
 
  return true;
}

function sendData()
{

  if(validatePercent())
  {
   	var form = document.forms[0];
	var totals = new Array();
	if(form.chkMsd.checked==true){
		totals[0]=form.percent.value;
	}else{
		totals[0]=0;
	}
	if(form.chkAddon.checked==true){
		totals[1]=form.percent.value;
	}else{
		totals[1]=0;
	}
	if(form.chkFee.checked==true){
		totals[2]=form.percent.value;
	}else{
		totals[2]=0;
	}
	window.returnValue = totals;
 	window.close();
  }
  else
  {
    // user has to fix errors and resubmit the form
    return;
  }
}
]]>
	</script>
</head>

<body>
	<table border="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
   		<tr>
			<td>
				<table align="center" border="0" width="100%" cellpadding="1" cellspacing="1">
   					<form name="refPercent">
    					<tr>
    	  					<td class="label" align="right">Enter Percentage</td>
							<td align="left"><input type="textbox" name="percent" id="percent" size="3"></input>%</td>
						</tr>
	         			<tr>
			   				<td colspan="2">Take Percentage off of:</td>
						</tr>
						<tr><td colspan="2"><hr/></td></tr>
						<tr>
							<td colspan="2"><input type="checkbox" name="chkMsd" id="chkMsd"/> Merchandise</td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" name="chkAddon" id="chkAddon"/> Add-On</td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" name="chkFee" id="chkFee"/> Service Fee</td>
						</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
		<tr>
			<td align="center">
				<button type="button" style="width:80" onclick="sendData()" class="bigBlueButton">(A)ccept</button>
				<button type="button" style="width:80" onclick="closeWindow()" class="bigBlueButton">(C)lose</button>
			</td>
 		</tr>
	</table>

	<div id="waitDiv" style="display:none">
   	<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      	<tr>
         	<td width="100%">
            	<table width="100%" cellspacing="0" cellpadding="0">
               		<tr>
                  		<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                  		<td id="waitTD" width="50%" class="waitMessage"></td>
               		</tr>
            	</table>
         	</td>
      	</tr>
   	</table>
	</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>