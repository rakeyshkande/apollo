<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="floristDeliveryDates" >
  <xsl:param name="requestedDeliveryDate"/>
  <xsl:param name="requestedDeliveryDateRangeEnd"/>
  <xsl:param name="deliveryDates"/>
  <xsl:param name="displayGetMax"/>
  <xsl:param name="defaultDates"/>

  <script type="text/javascript" language="javascript">
  <![CDATA[
  /*
   * The prepareDeliveryDate function is dynamic because delivery date submission preperation
   * is different for florist and vendor items.  This version of prepareDeliveryDate checks to
   * see if a range was selected by comparing start and end dates for equality, if they are 
   * different, a range is assumed.
   */
  function prepareDeliveryDate(formName) {
    var deliveryDate = document.getElementById("delivery_date");
    var deliveryDateRangeEnd = document.getElementById("delivery_date_range_end");
    var dd = document.getElementById("page_delivery_date");
    var option = dd[dd.selectedIndex];
    var startDate = option.value;
    var endDate = option.endDate;
    var outEndDate = ( endDate != startDate ) ?  endDate : "";

    deliveryDate.value = startDate;
    deliveryDateRangeEnd.value = outEndDate;
  }
  ]]>
  </script>
  <tr>
     <td class="Label" align="right" width="25%">Delivery Date:</td>
     <td style="padding-left:.5em;">
        <select name="page_delivery_date" id="page_delivery_date">
          <xsl:for-each select="$deliveryDates/deliveryDate">
          <option value="{startDate}" endDate="{endDate}">
            <xsl:if test="$defaultDates">
              <xsl:if test="startDate = $requestedDeliveryDate and ($requestedDeliveryDateRangeEnd = '' or endDate = $requestedDeliveryDateRangeEnd)"><xsl:attribute name="selected"/></xsl:if>
            </xsl:if>
            <xsl:value-of select="displayDate"/>
          </option>
          </xsl:for-each>
        </select>
        <span style="color:#FF0000">***</span>
        <xsl:if test="boolean($displayGetMax)">
          <span style="padding-left:4em;">&nbsp;<button type="button" id="getMaxButton" class="bluebutton" onclick="javascript:doGetMaxDeliveryDateAction();">Get Max</button></span>
        </xsl:if>
     </td>
  </tr>

</xsl:template>
</xsl:stylesheet>