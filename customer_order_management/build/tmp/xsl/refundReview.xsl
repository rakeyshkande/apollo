
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
    <!-- decimal format used when a value is not given -->
  <xsl:decimal-format NaN="0.00" name="notANumber"/>
  <xsl:decimal-format NaN="(0.00)"  name="noRefund"/>
  
  
  <xsl:output method="html" indent="yes"/>
  <!--xsl:output indent="yes"/-->
  <xsl:key name="pagedata" match="root/pageData/data" use="name" />
  <xsl:key name="security" match="/root/security/data" use="name"/>
  
  <xsl:template match="/">
    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html"/>
        <title>FTD - Refund Review</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/tabIndexMaintenance.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script type="text/javascript" src="js/clock.js"></script>
        <script type="text/javascript" src="js/commonUtil.js"></script>
        <script type="text/javascript" src="js/mercuryMessage.js"></script>
        <script type="text/javascript"><![CDATA[
        
var cos_show_previous = "true";
var cos_show_next = "true";
var cos_show_first = "true";
var cos_show_last = "true";
var page_count=0;
// init code
function init()
{
  // set focus //
  if (document.getElementById('numberEntry') != null)
    	document.getElementById('numberEntry').focus();
  else if (document.getElementById('nextItem') != null)
    	document.getElementById('nextItem').focus();
  untabAllElements();
  localStorage.setItem('isVipPopupShown','N');    
  setRefundReviewIndexes();
  updateSortHeaders()
}

function ReplaceAll(Source,stringToFind,stringToReplace){
  var temp = Source;
  var index = temp.indexOf(stringToFind);

  while(index != -1){
    temp = temp.replace(stringToFind,stringToReplace);
    index = temp.indexOf(stringToFind);
  }

  return temp;
}



/*******************************************************************************
*  setRefundReviewIndexes()
*  Sets tabs for Refund Review
*  See tabIndexMaintenance.js
*******************************************************************************/
function setRefundReviewIndexes()
{
  untabAllElements();
  var tabOrder = new Array('numberEntry', 'backItem', 'firstItem', 'nextItem', 'lastItem', 'backButton', 'mainMenu', 'printer');
  setTabIndexes(tabOrder, findBeginIndex());
}

//Disable buttons
function buttonDis(page)
{
  setNavigationHandlers();

  if (page < 2)
  {
    cos_show_previous = "false";
  	cos_show_first = "false";
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
  }
  if (page > refundReview.total_pages.value-1)
  {
  	cos_show_next = "false";
	cos_show_last = "false";
    document.getElementById("nextItem").disabled= "true";
    document.getElementById("lastItem").disabled= "true";
    return;
  }
}

function checkKeyDown()
{
  var tempKey = window.event.keyCode;

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cos_show_first == "true")
  {
    loadRefundAction(--refundReview.page_position.value);
  }

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cos_show_last == "true")
  {
    loadRefundAction(++refundReview.page_position.value);
  }

  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cos_show_next == "true")
  {
    loadRefundAction(refundReview.page_position.value = refundReview.total_pages.value);
  }

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cos_show_previous == "true")
  {
    loadRefundAction(refundReview.page_position.value = 1);
  }
}

function checkAuthorization(preferredPartnerName, csrCanViewPrefPartner)
{
  if(preferredPartnerName != null && preferredPartnerName != "" && csrCanViewPrefPartner != "" && csrCanViewPrefPartner != "Y")
  {
    var warningMessage = "Stop: This is a XXXX order. This order will need to be handled by a XXXX CSR";
    warningMessage = ReplaceAll(warningMessage,'XXXX', preferredPartnerName);
    alert(warningMessage);
    return false;
  }

  return true;

}
 
]]> 

function showRefundScreen(extOrderNum, refundId, refundGuid, preferredPartnerName, csrCanViewPrefPartner)
{

  if (checkAuthorization(preferredPartnerName, csrCanViewPrefPartner))
  {
    document.refundReview.prev_page.value = "refundReview";
    document.refundReview.prev_page_position.value = refundReview.page_position.value;
   
    //Added to retain sorting filters values accross the pages.
    document.refundReview.prev_sort_column.value = refundReview.sort_column.value;
    document.refundReview.prev_sort_status.value = refundReview.sort_status.value;
    document.refundReview.prev_current_sort_status.value = refundReview.current_sort_status.value;
    document.refundReview.prev_is_from_header.value = refundReview.is_from_header.value;
    document.refundReview.prev_userId_filter.value = refundReview.userId_filter.value;
    document.refundReview.prev_groupId_filter.value = refundReview.groupId_filter.value;
    document.refundReview.prev_codes_filter.value = refundReview.codes_filter.value;
    
    var form = document.forms[0];
    form.action_type.value="reviewed";
    form.t_entity_history_id.value = <xsl:value-of select="key('pagedata', 't_entity_history_id')/value"/>;
    form.order_detail_id.value=extOrderNum;
    form.refund_guid.value=refundGuid;
    form.refund_id.value=refundId;
    form.action="updateRefund.do";
    form.submit();
  }
}



function doRowCalculation(searchValue){
  var val = stripZeros(searchValue);
    var num = val;
    var cPage = <xsl:value-of select="key('pagedata','page_position')/value"/>;
    var page=cPage;
    if((cPage > 1 ) ){
      page--;
        var max_recs = <xsl:value-of select="key('pagedata','MAX_REFUNDS_PER_PAGE')/value"/>;
        if(max_recs == '')
          max_recs = 50;
      var num = (val - (page * max_recs));
  }
    return num;
}

function loadRefundAction(newPagePosition){
  var form = document.forms[0];
  form.page_position.value = newPagePosition;
  form.t_entity_history_id.value = <xsl:value-of select="key('pagedata', 't_entity_history_id')/value"/>;
  form.action = "loadRefunds.do";
  form.submit();
}

function sortColumnData(colHeaderObj)
{
  if(colHeaderObj != null){
         var colStatus = document.getElementById(colHeaderObj).sortStatus; 
         var form = document.forms[0];
         form.is_from_header.value='true';
         form.sort_column.value=colHeaderObj;
         form.sort_status.value=colStatus;
        <!--  filters data-->
         fetchFiltersDataAndSetToForm();
         <!-- form submission -->
         submitFormData();
  }
}
function goToSpecificPage()
{
	var jumpToPage = document.getElementById('jumpToPage');
	var form = document.forms[0];
	form.page_position.value = jumpToPage.options[jumpToPage.selectedIndex].value;
    <!-- here form submission -->
    submitFormData();
}
function performFiltersRequest(){
  fetchFiltersDataAndSetToForm();
  var form = document.forms[0];
  form.page_position.value="1";
  <!-- here form submission -->
  submitFormData();
}
function performRequestRefresh(){
  var form = document.forms[0];
  form.sort_column.value="";
  form.sort_status.value="";
  form.userId_filter.value="";
  form.groupId_filter.value="";
  form.codes_filter.value="";
  form.page_position.value="1";
  <!-- here form submission -->
  submitFormData();
}
function updateSortHeaders(){
 var curStatus =document.getElementById('current_sort_status').value;
 var sortCol =document.getElementById('sort_column').value;
 if(sortCol != null)
   document.getElementById(sortCol).sortStatus=curStatus;
}

function submitFormData(){
  var form = document.forms[0];
  form.action_type.value="RefundReview";
  form.action = "loadRefunds.do";
  form.submit();
}

<![CDATA[
function fetchFiltersDataAndSetToForm(){
  var userId = document.getElementById('user_id_filter').value;
  var groupId = document.getElementById('group_id_filter').value;
  var codes = document.getElementById('codes');
  var codesString ="";
  if(codes != null && codes.options.length >0){
	  for(var i=0;i < codes.options.length;i++){
	    if(codes.options[i].selected)
	       codesString =codesString.length>0?codesString+","+codes.options[i].value:codes.options[i].value;
	  }
  }
  var form = document.forms[0];
  form.userId_filter.value=userId;
  form.groupId_filter.value=groupId;
  form.codes_filter.value=codesString;
  //form.page_position.value="1";
}
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripZeros(num){
   var num,newTerm
   while (num.charAt(0) == "0") {
         newTerm = num.substring(1, num.length);
         num = newTerm;
   }
   if (num == "")
       num = "0";
       return num;
}

function doEntryAction(){
  if(window.event)
    if(window.event.keyCode)
      if(window.event.keyCode != 13)
        return false;
  var eve = event.srcElement.value;
  if(isInteger(eve)){
    var searchNumber = doRowCalculation(eve);
    if(document.getElementById('order_detail_id_'+searchNumber)==null){
      alert('Invalid line number entered.' + '\n' +
      'Please only enter a line number that is currently being displayed on the page.');
    }else{
      //alert(document.getElementById('order_detail_id_'+searchNumber).value);
      showRefundScreen(document.getElementById('order_detail_id_'+searchNumber).value, document.getElementById('refund_id'+searchNumber).value, document.getElementById('refund_guid_'+searchNumber).value, document.getElementById('preferred_partner_name_'+searchNumber).value, document.getElementById('csr_can_view_pref_partner_'+searchNumber).value);
    }
  }
}

function mainMenuClick(){
   doMainMenuAction();
}

function doBackAction()
{
   doMainMenuAction();
}

]]></script>
  <style>
    .messageContainer .lineNumber{
      width: 5%;
      vertical-align: top;
    }

    .messageContainer .orderNumber{
      width: 15%;
      text-align: left;
      word-break: break-all;
      vertical-align: top;
    }

    .messageContainer .userId{
      width: 8%;
      text-align: left;
      word-break: break-all;
      vertical-align: top;
    }
    
    .messageContainer .group{
      width: 6%;
      text-align: left;
      word-break: break-all;
      vertical-align: top;
    }
    .messageContainer .status{
      width: 6%;
      text-align: left;
      word-break: break-all;
      vertical-align: top;
    }

    .messageContainer .mercValue{
      width: 10%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }

    .messageContainer .refundAmt{
      width: 8%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }

    .messageContainer .merchandiseValue{
      width: 11%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }
    
    .messageContainer .addOn{
      width: 6%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }
  
    .messageContainer .tax{
      width: 6%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }
    
    .messageContainer .serviceFee{
      width: 7%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }
  
    .messageContainer .date{
      width: 8%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }

    .messageContainer .code{
      width: 5%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }
    
    .messageContainer .reviewedBy{
      width: 10%;
      text-align: center;
      word-break: break-all;
      vertical-align: top;
    }
  </style>
      </head>
      <body onload="init();buttonDis(refundReview.page_position.value);"  onkeydown="checkKeyDown()">
        <xsl:call-template name="header">
          <xsl:with-param name="headerName"  select="'Refund Review'" />
          <xsl:with-param name="showTime" select="true()"/>
          <xsl:with-param name="showBackButton" select="true()"/>
          <xsl:with-param name="showPrinter" select="true()"/>
          <xsl:with-param name="showSearchBox" select="true()"/>
          <xsl:with-param name="searchLabel" select="'Enter Line#'" />
       	  <xsl:with-param name="backButtonLabel" select="'(B)ack'"/>
       	  <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
        </xsl:call-template>
        <form name="refundReview" method="post" action="">
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>
          <input type="hidden" name="securitytoken" id="securitytoken" value="{$securitytoken}"/>
          <input type="hidden" name="context" id="context" value="{$context}"/>
          <input type="hidden" name="action_type" id="action_type" value="RefundReview"/>
          <input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
          <input type="hidden" name="total_pages" id="total_pages" value="{key('pagedata', 'total_pages')/value}"/>
          <input type="hidden" name="start_position" id="start_position" value="{key('pagedata', 'start_position')/value}"/>          
          <input type="hidden" name="order_detail_id" id="order_detail_id" value=""/>
          <input type="hidden" name="refund_guid" id="refund_guid" value=""/>
          <input type="hidden" name="refund_id" id="refund_id" value=""/>
          <input type="hidden" name="sort_column" id="sort_column" value="{key('pagedata', 'sort_column')/value}"/>
          <input type="hidden" name="sort_status" id="sort_status" value="{key('pagedata', 'sort_status')/value}"/> 
          <input type="hidden" name="current_sort_status" id="current_sort_status" value="{key('pagedata', 'current_sort_status')/value}"/> 
          <input type="hidden" name="is_from_header" id="is_from_header" value="false"/>
          <input type="hidden" name="userId_filter" id="userId_filter" value="{key('pagedata', 'userId_filter')/value}"/>
          <input type="hidden" name="groupId_filter" id="groupId_filter" value="{key('pagedata', 'groupId_filter')/value}"/>
          <input type="hidden" name="codes_filter" id="codes_filter" value="{key('pagedata', 'codes_filter')/value}"/>
          <!--<input type="hidden" name="t_entity_history_id" id="t_entity_history_id" value=""/>-->
          <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
            <tr>
              <td id="lockError" class="ErrorMessage"></td>
            </tr>
          </table>
          <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
            <tr>
              <td>
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                  <tr>
                    <td colspan="3" align="left">
                      <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
                        <tr>
                          <td colspan = "9">
                            <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">                              
                              <tr>
                                <td>
                                  <div class="messageContainer" style="width: 100%; overflow: auto; padding-right: 32px;">
                                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                                      <tr>
                                        <th class="lineNumber"></th>
                                        <th class="orderNumber">Order#</th>
                                        <th class="userId" onclick="sortColumnData('created_by');"><span class="cursor" id="created_by" sortStatus="ASC"><u>User ID</u></span></th>
                                        <th class="group" onclick="sortColumnData('cs_group_id');"><span class="cursor" id="cs_group_id" sortStatus="DESC"><u>Group</u></span></th>
                                        <th class="mercValue">Merc<br/>Value</th>
                                        <th class="refundAmt" onclick="sortColumnData('refund_total');"><span class="cursor" id="refund_total" sortStatus="DESC"><u>Refund<br/>Amount</u></span></th>
                                        <th class="merchandiseValue" onclick="sortColumnData('refund_product_amount');"><span class="cursor" id="refund_product_amount" sortStatus="DESC"><u>Merchandise<br/>Value</u></span></th>
                                        <th class="addOn">Add<br/>On</th>
                                        <th class="tax">Tax</th>                                  
                                        <th class="serviceFee">Service<br/>Fee</th>                                 
                                        <th class="date">Date</th>
                                        <th class="code" onclick="sortColumnData('refund_disp_code');"><span class="cursor" id="refund_disp_code" sortStatus="DESC"><u>Code</u></span></th>
                                        <th class="reviewedBy">Reviewed<br/>By</th>                               
                                      </tr>
                                       <tr>
                                        <td colspan="2"> </td>
                                        <td class="userId">
                                         <input type="text" id="user_id_filter" size="7">
	                                         <xsl:attribute name="value">
	                                         <xsl:value-of select="key('pagedata', 'userId_filter')/value" />
	                                         </xsl:attribute>
                                         </input>
                                        </td>
									    <td class="group">
									     <input type="text" id="group_id_filter" size="7">
	                                         <xsl:attribute name="value">
	                                         <xsl:value-of select="key('pagedata', 'groupId_filter')/value" />
	                                         </xsl:attribute>
                                         </input>
                                        </td>
                                        <td class="mercValue"></td>
                                        <td class="refundAmt"></td>
                                        <td class="merchandiseValue"></td>
                                        <td class="addOn"></td>
                                        <td class="tax"></td>                                  
                                        <td class="serviceFee"></td>                                 
                                        <td class="date"></td>
                                        <td class="code">
	                                        <select name="codes" id="codes" multiple="true" size="2" >
												<xsl:for-each select="root/CODES/CODE">
												 <xsl:choose>
													<xsl:when test="value='true'">
														<option value="{name}" selected="true"><xsl:value-of select="name"/></option>
													</xsl:when>
													<xsl:otherwise>
														<option value="{name}"><xsl:value-of select="name"/></option>
													</xsl:otherwise>
												</xsl:choose> 
												</xsl:for-each>
										    </select>
							           </td>
                                        <td class="reviewedBy"></td>                               
                                      </tr>
                                    </table>
                                  </div>
                                  <table width="100%" cellpadding="0" border="0" cellspacing="0">
                                    <tr>
                                      <td colspan="13">
                                        <hr/>
                                      </td>
                                    </tr>
                                  </table>
                                  <div class="messageContainer" style="width: 100%; overflow: auto; padding-right: 32px; height: 175px;">
                                    <table width="100%" cellpadding="1" border="0" cellspacing="1" style="margin-right: -16px;">
                                      <xsl:for-each select="root/RefundReview/Refund">
                                        <tr>
                                          <td class="lineNumber">
                                            
                                            <input type="hidden" id="order_detail_id_{@num}" value="{order_detail_id}"/>
                                            <input type="hidden" id="refund_guid_{@num}" value="{order_guid}"/>
                                            <input type="hidden" id="refund_id{@num}" value="{refund_id}"/>
                                            <input type="hidden" id="preferred_partner_name_{@num}" value="{preferred_partner_name}"/>
                                            <input type="hidden" id="csr_can_view_pref_partner_{@num}" value="{csr_can_view_pref_partner}"/>
                                            
                                            <script type="text/javascript">document.write(refundReview.start_position.value); refundReview.start_position.value++</script>.
                                          </td>
                                          <td class="orderNumber">
                                            <a class="tableheaderlink" href="javascript:showRefundScreen('{order_detail_id}', '{refund_id}', '{order_guid}', '{preferred_partner_name}', '{csr_can_view_pref_partner}');"><xsl:value-of select="external_order_number"/></a>
                                            <xsl:if test="preferred_partner_name != '' and string-length(preferred_partner_name) > 0">
                                              <a style="font:bold;color:blue;">
                                                &nbsp;&nbsp;<xsl:value-of select="partner_display_name"/>
                                              </a>
                                            </xsl:if>
                                          </td>
                                          <td class="userId">
                                            <xsl:value-of select="created_by"/>
                                          </td>
                                          <td class="group">
                                            <xsl:value-of select="cs_group_id"/>
                                          </td>
                                          <td class="mercValue">
                                            <xsl:value-of select="format-number(message_value, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                          </td>
                                          <td class="refundAmt">
                                            <xsl:value-of select="format-number(refund_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                          </td>
                                          <td class="merchandiseValue">
                                            <xsl:value-of select="format-number(refund_product_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                          </td>
                                          <td class="addOn">
                                            <xsl:value-of select="format-number(refund_addon_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                          </td>
                                          <td class="tax">
                                            <xsl:value-of select="format-number(refund_tax, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                          </td>
                                          <td class="serviceFee">
                                            <xsl:value-of select="format-number(refund_serv_ship_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                          </td>
                                          <td class="date">
                                            <xsl:value-of select="order_date"/>
                                          </td>
                                          <td class="code">
                                            <xsl:value-of select="refund_disp_code"/>
                                          </td>
                                          <td class="reviewedBy">
                                            <xsl:value-of select="reviewed_by"/>
                                          </td>
                                        </tr>
                                      </xsl:for-each>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3"><hr/></td>
                  </tr>
                  <tr>
                    <td align="right">
                      <table>
                        <tr>
                                <td width="25%" class="label" align="right">Page&nbsp;
                                  <xsl:for-each select="key('pagedata', 'page_position')">
                                    <xsl:value-of select="value" />
                                  </xsl:for-each>
                                  &nbsp;of&nbsp;
                                  <xsl:for-each select="key('pagedata', 'total_pages')">
                                                    <xsl:if test="value = '0'">
                                                      1
                                                    </xsl:if>
                                                    <xsl:if test="value != '0'">
                                      <xsl:value-of select="value" />
                                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>
                              <xsl:for-each select="key('pagedata', 'page_position')">
                          <tr>
                            <td colspan="6" align="right">
                              <xsl:choose>
                                                    <xsl:when test="key('pagedata','total_pages')/value !='0'">
                                                      <button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="loadRefundAction(1);">
                                                      7
                                                      </button>
                                                    </xsl:when>
                                                    <xsl:when test="key('pagedata','total_pages')/value ='0'">
                                                      <button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="">
                                                      7
                                                      </button>
                                                    </xsl:when>
                                                    </xsl:choose>
                                                <xsl:choose>
                                                    <xsl:when test="key('pagedata','total_pages')/value !='0'">
                                                      <button type="button" name="backItem" class="arrowButton" id="backItem" onclick="loadRefundAction(refundReview.page_position.value-1);">
                                                      3
                                                      </button>
                                                    </xsl:when>
                                                    <xsl:when test="key('pagedata','total_pages')/value ='0'">
                                                      <button type="button" name="backItem" class="arrowButton" id="backItem" onclick="">
                                                      3
                                                      </button>
                                                    </xsl:when>
                                                    </xsl:choose>
                                                  <xsl:choose>
                                                    <xsl:when test="key('pagedata','total_pages')/value !='0'">
                                                      <button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="x=refundReview.page_position.value;loadRefundAction(++x);">
                                                      4
                                                      </button>
                                                    </xsl:when>
                                                      <xsl:when test="key('pagedata','total_pages')/value ='0'">
                                                        <button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="">
                                                        4
                                                        </button>
                                                      </xsl:when>
                                                    </xsl:choose>
                                                <xsl:choose>
                                                    <xsl:when test="key('pagedata','total_pages')/value !='0'">
                                                      <button type="button" name="lastItem" class="arrowButton" id="lastItem" onclick="loadRefundAction(refundReview.total_pages.value);">
                                                      8
                                                      </button>
                                                    </xsl:when>
                                                    <xsl:when test="key('pagedata','total_pages')/value ='0'">
                                                      <button type="button" name="lastItem" class="arrowButton" id="lastItem" onclick="">
                                                      8
                                                      </button>
                                                  </xsl:when>
                                                  </xsl:choose>
                                                
                            </td>
                          </tr>
                        </xsl:for-each>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </form>
        <table width="98%" border="0" align="center">
        <tr>
		    <td rowspan="5" width="56%" align="right">
				<button class="BlueButton"  accesskey="R" onClick="javascript:performRequestRefresh()">(R)efresh</button><br/><br/>
			</td>
			<td>
				<button class="BlueButton"  accesskey="F" onClick="javascript:performFiltersRequest()">(F)ilter</button><br/><br/>
		    </td>
           <td align="right" width="30%">
				<select id="jumpToPage" name="jumpToPage">
					<option>1</option>
					<script>
					    page_count=<xsl:value-of select="key('pagedata', 'total_pages')/value"/>
						for (var i = 2; i &lt;= page_count; i++)
						{
						  document.write("<option value=" + i + ">" + i + "</option>");
						}
					</script>
				</select>
				<button style="width: 125px;" class="BlueButton"  accesskey="G" onclick="javascript:goToSpecificPage();">(G)o To Page</button>
		 </td>
          <td width="20%" align="right">
              <button type="button" class="blueButton" style="width:60" name="backButton" onclick="javascript:doBackAction();">(B)ack</button>
              <br />
            <button type="button" class="bigBlueButton" style="width:60" name="mainMenu" onclick="mainMenuClick()">(M)ain<br/>Menu</button>
            </td>
        </tr>
      </table>
        <div id="waitDiv" style="display:none">
          <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
            <tr>
              <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                    <td id="waitTD" width="50%" class="waitMessage"></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
        <xsl:call-template name="footer"></xsl:call-template>
        <script type="text/javascript"><![CDATA[
         window.onload=init(); 
         localStorage.setItem('isVipPopupShown','N');
        ]]></script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
