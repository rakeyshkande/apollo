<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<!--
Notes:

This xsl is used on the billingReview.xsl page.
When the user clicks the Submit Billing button after the lock is checked
we call billingReview.do and this xsl is then transformed.
If the status = successful we finish submitting our page otherwise
we display the appropriate error message and/or pop-ups.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/">
<html>
<head>
<script type="text/javascript" language="javascript">
var isError = "<xsl:value-of select="net_amounts/is_error"/>";

<![CDATA[
	function init()
	{
     if(isError == 'Y'){
        parent.doCallErrorPage();
       return;
     }
     if(isError == 'N' || isError == "")
     {
	      parent.displayRecalculationTotals(document.getElementById('billingRecalc'));
     }
       
	}
]]>
</script>
</head>
<body onload="javascript:init();">
   <form name="billingRecalc" id="billingRecalc" >
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
    
      <input type="hidden" name="product_amount" id="product_amount" value="{net_amounts/net_add_bill_refund_amounts/product_amount}"/>
      <input type="hidden" name="add_on_amount" id="add_on_amount" value="{net_amounts/net_add_bill_refund_amounts/add_on_amount}"/>
      <input type="hidden" name="serv_ship_fee" id="serv_ship_fee" value="{net_amounts/net_add_bill_refund_amounts/serv_ship_fee}"/>
      <input type="hidden" name="service_fee" id="service_fee" value="{net_amounts/net_add_bill_refund_amounts/service_fee}"/>
      <input type="hidden" name="shipping_fee" id="shipping_fee" value="{net_amounts/net_add_bill_refund_amounts/shipping_fee}"/>
      <input type="hidden" name="discount_amount" id="discount_amount" value="{net_amounts/net_add_bill_refund_amounts/discount_amount}"/>
      <input type="hidden" name="shipping_tax" id="shipping_tax" value="{net_amounts/net_add_bill_refund_amounts/shipping_tax}"/>
      <input type="hidden" name="service_fee_tax" id="service_fee_tax" value="{net_amounts/net_add_bill_refund_amounts/sercie_fee_tax}"/>
      <input type="hidden" name="product_tax" id="product_tax" value="net_amounts/{net_add_bill_refund_amounts/product_tax}"/>
      <input type="hidden" name="tax" id="tax" value="{net_amounts/net_add_bill_refund_amounts/tax}"/>
      <input type="hidden" name="total" id="total" value="{net_amounts/net_add_bill_refund_amounts/order_total}"/>
      <input type="hidden" name="order_total" id="order_total" value="{net_amounts/net_add_bill_refund_amounts/order_total}"/>
     </form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>