
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<!--xsl:output indent="yes"/-->
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Order History</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/tabIndexMaintenance.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript"><![CDATA[
// init code
function init()
{
  setOrderHistoryIndexes();
}

/*******************************************************************************
*  setOrderHistoryIndexes()
*  Sets tabs for Order History Page. 
*  See tabIndexMaintenance.js
*******************************************************************************/
function setOrderHistoryIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('searchAction', 'nextItem', 'backItem', 'firstItem', 
  'lastItem', 'backButton', 'mainMenu', 'printer');
  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}

//Disable buttons
function buttonDis(page)
{
  if (page < 2)
  {
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
  }
	if (page > orderHistory.total_pages.value-1)
	{
    document.getElementById("nextItem").disabled= "true";
   	document.getElementById("lastItem").disabled= "true";
    return;
  }
}

/*************************************************************************************
*	Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
	var form = document.forms[0];
	var url = "customerOrderSearch.do?action=customer_account_search&order_number=" + form.external_order_number.value +"&securitytoken="+form.securitytoken.value+getSecurityParams(false);
	form.action = url;
	form.submit();
}
	
function loadHistoryAction()
{
		document.orderHistory.action = "loadHistory.do";
	 	document.orderHistory.submit();
}

]]></script>
	<style>
		.messageContainer .lineNumber{
			width: 5%;
			vertical-align: top;
		}

		.messageContainer .date{
			width: 10%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}

		.messageContainer .start{
			width: 10%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}
		
		.messageContainer .end{
			width: 10%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}
		
		.messageContainer .length{
			width: 10%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}

		.messageContainer .userId{
			width: 10%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}

		.messageContainer .origin{
			width: 35%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}
	</style>
			</head>
			<body onload="init();buttonDis(orderHistory.page_position.value);">				
				<xsl:call-template name="header">
					<xsl:with-param name="headerName"  select="'Order History'" />
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="dnisNumber" select="$call_dnis"/>
					<xsl:with-param name="cservNumber" select="$call_cs_number"/>
					<xsl:with-param name="indicator" select="$call_type_flag"/>
					<xsl:with-param name="brandName" select="$call_brand_name"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>
				
				<form name="orderHistory" method="post" action="">
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="action_type" id="action_type" value="{key('pagedata', 'action_type')/value}"/>
					<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>
					<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata', 'order_detail_id')/value}"/>
					<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pagedata', 'external_order_number')/value}"/>
					<input type="hidden" name="history_type" id="history_type" value="Order"/>
					<input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
					<input type="hidden" name="total_pages" id="total_pages" value="{key('pagedata', 'total_pages')/value}"/>
					<input type="hidden" name="start_position" id="start_position" value="{key('pagedata', 'start_position')/value}"/>
					<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
					<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
									<tr>
										<td colspan="3" align="left">
											<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
												<tr>
													<td colspan = "9">
														<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
															<tr>
																<td>
																	<div class="messageContainer" style="padding-right: 16px; width: 98%;">
																		<table width="100%" border="0" cellpadding="1" cellspacing="1">
																			<tr>
																				<th class="lineNumber"></th>
																				<th class="date">DATE</th>
																				<th class="start">START</th>
																				<th class="end">END</th>
																				<th class="length">LENGTH</th>
																				<th class="userId">UserID</th>
																				<th colspan="2" class="origin">ORIGIN</th>
																			</tr>
																		</table>
																	</div>
																	<table width="100%" cellpadding="0" border="0" cellspacing="0">
																		<tr>
																			<td colspan="9">
																				<hr/>
																			</td>
																		</tr>
																	</table>
																	<div class="messageContainer" style="width: 99%; overflow: auto; padding-right: 16px; height: 150px;">
																		<table width="100%" cellpadding="1" border="0" cellspacing="1" style="margin-right: -16px;">
																			<xsl:for-each select="root/OrderHistory/History">
																				<tr>
																					<td class="lineNumber">
																						<script type="text/javascript">document.write(orderHistory.start_position.value); orderHistory.start_position.value++</script>.
																					</td>
																					<td class="date">
																						<xsl:value-of select="history_date"/>
																					</td>
																					<td class="start">
																						<xsl:value-of select="history_start_time"/>
																						<!--td></td-->
																					</td>
																					<td class="end">
																						<xsl:choose>
																							<xsl:when test="history_end_time!=''">
																								<xsl:value-of select="history_end_time"/>
																							</xsl:when>
																							<xsl:otherwise>
																							</xsl:otherwise>
																						</xsl:choose>
																					</td>
																					<td class="length">
																						<xsl:choose>
																							<xsl:when test="history_end_time!=''">
																								<xsl:value-of select="length"/>
																							</xsl:when>
																							<xsl:otherwise>
																							</xsl:otherwise>
																						</xsl:choose>
																					</td>
																					<td class="userId">
																						<xsl:value-of select="csr_id"/>
																					</td>
																					<td class="origin">
																						<xsl:value-of select="comment_origin"/>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</table>
																	</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="3"><hr/></td>
									</tr>
									<tr>
										<td align="right">
											<table border="0">
												<tr>
													<td width="15%" class="label" align="right">Page&nbsp;
														<xsl:for-each select="key('pagedata', 'page_position')">
														<xsl:value-of select="value" />
														</xsl:for-each>
														&nbsp;of&nbsp;
														<xsl:for-each select="key('pagedata', 'total_pages')">
             						                    <xsl:if test="value = '0'">
                  							                1
						                                  </xsl:if>
						                                  <xsl:if test="value != '0'">
						                                   <xsl:value-of select="value" />
						                                  </xsl:if>
														</xsl:for-each>
													</td>
												</tr>
												<xsl:for-each select="key('pagedata', 'page_position')">
													<tr>
                            <td width="50%" align="right"><xsl:choose>
                              <xsl:when test="key('pagedata','total_pages')/value !='0'">
                              <button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="loadHistoryAction(orderHistory.page_position.value = 1);">
                              7
                              </button>
                              </xsl:when>
                              <xsl:when test="key('pagedata','total_pages')/value ='0'">
                              <button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="">
                              7
                              </button>
                              </xsl:when>
                              </xsl:choose>
                            <!-- </td>-->
                            <!--<td align="right">--><xsl:choose>
                              <xsl:when test="key('pagedata','total_pages')/value !='0'">
                              <button type="button" name="backItem" class="arrowButton" id="backItem" onclick="loadHistoryAction(--orderHistory.page_position.value);">
                              3
                              </button>
                              </xsl:when>
                              <xsl:when test="key('pagedata','total_pages')/value ='0'">
                              <button type="button" name="backItem" class="arrowButton" id="backItem" onclick="">
                              3
                              </button>
                              </xsl:when>
                              </xsl:choose>
                            <!-- </td>-->
                           <!-- <td align="right">--><xsl:choose>
                              <xsl:when test="key('pagedata','total_pages')/value !='0'">
                              <button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="loadHistoryAction(++orderHistory.page_position.value);">
                              4
                              </button>
                              </xsl:when>
                              <xsl:when test="key('pagedata','total_pages')/value ='0'">
                              <button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="">
                              4
                              </button>
                              </xsl:when>
                              </xsl:choose>
                             <!--</td>-->
                         <!--   <td align="right">--><xsl:choose>
                              <xsl:when test="key('pagedata','total_pages')/value !='0'">
                              <button type="button" name="lastItem" class="arrowButton" id="lastItem" onclick="loadHistoryAction(orderHistory.page_position.value = orderHistory.total_pages.value);">
                              8
                              </button>
                              </xsl:when>
                              <xsl:when test="key('pagedata','total_pages')/value ='0'">
                              <button type="button" name="lastItem" class="arrowButton" id="lastItem" onclick="">
                              8
                              </button>
                              </xsl:when>
                              </xsl:choose>
                             </td>
													</tr>
												</xsl:for-each>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
				<table width="98%" border="0" align="center">
					
					<tr align="right">
						<td align="left" valign="top">
							<button type="button" style="width:120" accesskey="O" class="BigBlueButton" name="searchAction" onClick="doBackAction();">Recipien(t)/<br/>Order</button>
						</td>
						<td width="20%" align="right">
  							<button type="button" class="blueButton" name="backButton" style="width:85px" onclick="javascript:doBackAction();">(B)ack to Order</button>
  							<br />
 							<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" style="width:85px" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu</button>
						</td>
					</tr>
				</table>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<xsl:call-template name="footer"></xsl:call-template>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>