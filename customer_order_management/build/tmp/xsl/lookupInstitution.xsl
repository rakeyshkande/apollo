<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:variable name="institutionInput" select="key('pageData', 'institutionInput')/value"/>
<xsl:variable name="institutionTypeInput" select="key('pageData', 'institutionTypeInput')/value"/>
<xsl:variable name="cityInput" select="key('pageData', 'cityInput')/value"/>
<xsl:variable name="stateInput" select="key('pageData', 'stateInput')/value"/>
<xsl:variable name="countryInput" select="key('pageData', 'countryInput')/value"/>

<xsl:template match="/root">
<html>
<head>
<title>Business Lookup</title>
<link rel="stylesheet" TYPE="text/css" href="css/ftd.css"></link>
<script language="Javascript" src="js/FormChek.js"/>
<script language="Javascript" src="js/util.js"/>
<script language="Javascript">
  <![CDATA[
  function init() {
    addDefaultListeners();
    window.name = "VIEW_RECIPIENT_LOOKUP";
    window.focus();
    document.forms[0].institutionInput.focus();
  }

  function onKeyDown() {
    if (window.event.keyCode == 13)
      reopenPopup();
  }

  function closeInstitutionLookup(businessid, address1, address2, city, state, zip, phone, bfh) {
    if (window.event.keyCode == 13)
      populatePage(businessid, address1, address2, city, state, zip, phone, bfh);
  }

  //This function passes parameters from this page to a function on the calling page
  function populatePage(businessid, address1, address2, city, state, zip, phone, bfh) {
    top.returnValue = new Array(businessid, address1, address2, city, state, zip, phone, bfh);
    top.close();
  }

  function closeInstitutionLookup(businessid, address1, address2, city, state, zip, phone, bfh) {
    if ( window.event.keyCode == 13 ) {
      populatePage(businessid, address1, address2, city, state, zip, phone, bfh);
    }
  }

  //This function submits the page and populates it with new search results.
  function reopenPopup() {
    var toSubmit = document.getElementById("institutionLookupForm");
    var city = document.getElementById("cityInput");
    city.style.backgroundColor='white';
    var cityVal = stripWhitespace(city.value);

    check = true;
    if ( cityVal.length == 0 ) {
      if (check == true) {
        city.focus();
        city.style.backgroundColor='pink';
        zip.style.backgroundColor='pink';
        check = false;
      }
    }

    //Everything is valid, so the popup can be opened
    if ( check ) {
      toSubmit.target = window.name;
      toSubmit.submit();
    }
    else {
      alert("One of City or Zip/Postal Code are required.");
      return false;
    }
  }

  function populatePage(businessid, address1, address2, city, state, zip, phone, bfh) {
    var ret = new Array();
    ret[0] = bfh;
    ret[1] = address1;
    ret[2] = address2;
    ret[3] = city;
    ret[4] = state;
    ret[5] = zip;
    ret[6] = phone;
    window.returnValue = ret;
    window.close();
  }
  ]]>
</script>
</head>
<body onLoad="javascript:init();">
<form name="institutionLookupForm" method="post" action="lookupInstitution.do">
<xsl:call-template name="securityanddata"/>
<xsl:call-template name="decisionResultData"/>
<input type="hidden" name="institutionTypeInput" id="institutionTypeInput" value="{$institutionTypeInput}"/>
<input type="hidden" name="countryInput" id="countryInput" value="{$countryInput}"/>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td width="100%" valign="top">
      <BR></BR>
      <h1 align="center">Business Lookup</h1>
      <center>
      <table width="100%" border="0" cellpadding="2" cellspacing="2">
        <tr>
          <td>
            <table width="95%">
              <tr>
                <td width="50%" class="label" align="right">Institution Name:</td>
                <td width="50%"><input name="institutionInput" id="institutionInput" class="TblText" maxlength="50" size="20" type="text" value="{$institutionInput}"></input></td>
              </tr>
              <tr>
                <td width="50%" class="label" align="right">City:</td>
                <td width="50%">
                  <input type="text" name="cityInput" id="cityInput" class="TblText" tabindex="1" size="20" maxlength="50" value="{$cityInput}"/>
                </td>
              </tr>
              <tr>
                <td width="50%" class="label" align="right">State:</td>
                <xsl:choose>
                <xsl:when test="$countryInput = 'US'">
                  <td width="50%" class="TblText">
                    <select class="TblText" name="stateInput" id="stateInput">
                      <option value=""></option>
                      <xsl:for-each select="STATES/STATE[countrycode='']">
                      <option value="{statemasterid}">
                        <xsl:if test="statemasterid=$stateInput"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="statename"/>
                      </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </xsl:when>
                <xsl:when test="$countryInput = 'CA'">
                  <td width="50%" class="TblText">
                    <select class="TblText" name="stateInput" id="stateInput">
                      <option value=""></option>
                      <xsl:for-each select="STATES/STATE[countrycode='CAN']">
                      <option value="{statemasterid}">
                        <xsl:if test="statemasterid=$stateInput"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="statename"/>
                      </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td width="50%"><input name="stateInput" id="stateInput" class="TblText" maxlength="30" size="6" type="text" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"></input></td>
                </xsl:otherwise>
                </xsl:choose>
              </tr>
              <tr>
                <td width="50%">&nbsp;</td>
                <td width="50%"><button type="button" tabindex="1" class="BlueButton" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()">Search</button></td>
              </tr>
              <tr><td colspan="10"><hr/></td></tr>
              <tr>
                <td class="instruction">&nbsp;Please click on an arrow to select a customer.</td>
                <td align="right"><button type="button" tabindex="2" class="BlueButton" border="0" onclick="javascript:populatePage('', '', '', '', '', '', '', '')" onkeydown="javascript:closeInstitutionLookup('', '', '', '', '', '', '', '')">Close screen</button></td>
              </tr>
            </table>
            <table width="98%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td>
                  <table class="LookupTable" width="100%" border="1" cellpadding="2" cellspacing="2">
                    <tr>
                      <td width="5%" class="label"> &nbsp; </td>
                      <td class="label" valign="bottom">Name </td>
                      <td class="label" valign="bottom">Address </td>
                      <td class="label" valign="bottom">City </td>
                      <td class="label" valign="bottom">State </td>
                      <td class="label" valign="bottom">Zip/Postal Code</td>
                      <td class="label" valign="bottom">Phone</td>
                    </tr>
                    <xsl:for-each select="institutions/institution">
                    <tr>
                      <td width="5%">
                        <img src="images/selectButtonRight.gif" onclick='javascript:populatePage("{businessid}", "{address1}", "{address2}", "{city}", "{state}", "{zipcode}", "{homephone}", "{bfhname}");' onkeydown='javascript:closeCityLookup("{businessid}", "{address1}", "{address2}", "{city}", "{state}", "{zipcode}", "{homephone}", "{bfhname}");'/>
                      </td>
                      <td align="left">
                        <xsl:value-of select="bfhname"/>
                      </td>
                      <td align="left">
                        <xsl:value-of select="address1"/> &nbsp; <xsl:value-of select="address2"/>
                      </td>
                      <td align="left">
                        <xsl:value-of select="city"/>&nbsp;
                      </td>
                      <td align="left">
                        <xsl:value-of select="state"/>&nbsp;
                      </td>
                      <td align="left">
                        <xsl:value-of select="zipcode"/>&nbsp;
                      </td>
                      <td align="left">
                        <xsl:value-of select="homephone"/>&nbsp;
                      </td>
                    </tr>
                    </xsl:for-each>
                    <tr>
                      <td>
                        <img tabindex="4" src="images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('', '', '', '', '', '', '', '')" onkeydown="javascript:closeInstitutionLookup('', '', '', '', '', '', '', '')"></img>
                      </td>
                      <td colspan="8"> None of the above </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="95%">
              <tr>
                <td align="right">
                  <button type="button" class="BlueButton" tabindex="5" border="0" onclick="javascript:populatePage('', '', '', '', '', '', '', '')" onkeydown="javascript:closeInstitutionLookup('', '', '', '', '', '', '', '')">Close screen</button>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      </center>
    </td>
  </tr>
</table>
</form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>