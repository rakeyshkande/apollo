<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
  <xsl:key name="pagedata" match="/root/pageData/data" use="name"/>
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:decimal-format NaN="0.00" name="zeroValue"/>
  <xsl:variable name="bill_level" select="key('pagedata','billing_level')/value"/>
  <xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/">
    <html>
      <head>
        <!-- <META http-equiv="Content-Type" content="text/html"/>-->
        <title>Update Billing</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <script type="text/javascript" src="js/clock.js">
        </script>
        <script type="text/javascript" src="js/util.js">
        </script>
        <script type="text/javascript" src="js/commonUtil.js">
        </script>
        <style type="text/css">
		 /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 160px;
				width: 100%;
			}
      </style>
        <script type="text/javascript" language="JavaScript">
      var totalNumber = 0;
      var orderLevel = "";
      var forward = "";
      var siteName = '<xsl:value-of select="$sitename"/>';
      var siteNameSsl = '<xsl:value-of select="$sitenamessl"/>';
      var applicationContext = '<xsl:value-of select="$applicationcontext"/>'; 

          <![CDATA[
          function init()
          {
              original = new Array(document.forms[0].length);
              original = saveFormValue(original);
              document.getElementById('update').value = 'false';
              orderLevel = (document.getElementById('billing_level').value == "cart") ? "updateBilling" : "updatePayment";

          }

          function saveFormValue(formValueArray) {

              for (var i=0;i<document.forms[0].length;i++)
              {
                  current = document.forms[0].elements[i];
                  if (current.type == "text")
                  {
            formValueArray[i] = current.value;
                  }
              }
               return formValueArray;
          }

          /*******************************************************
          Call this when Exit button is clicked.
          Returns true if any value on the form has changed.
          Returns false otherwise.
          *********************************************************/
          function testValueChange() {

              valueChanged = false;

              current = new Array(document.forms[0].length);
              if(original.length != current.length)
              {
                  valueChanged = true;
              }

               current = saveFormValue(current);
               for (var i=0;i<current.length;i++)

              {
                  if(original[i] != current[i])
                  {
            valueChanged = true;
                  }
              }
              return valueChanged;
          }

          function setUpdateFlag()
          {
          if (testValueChange() == 'true')
          document.getElementById('update').value = 'true';
          }

          function doBackAction()
          {
             setUpdateFlag();

             if (testValueChange() == true)
          	 {
                if( doExitPageAction(WARNING_MSG_4 ) ){
                   //Before send the user back to the shopping cart we need to release the lock on the record
                   if( orderLevel == 'updateBilling' ){
                    doGUIDRecordLock("PAYMENTS","unlock","unlocking");
                   }
                   else
                   {
                    doRecordLock("PAYMENTS","unlock","unlocking");
                   }
                   doCompleteCancelProcess();
                }
             }else{
                if( orderLevel == 'updateBilling' ){
									doGUIDRecordLock("PAYMENTS","unlock","unlocking");
                   }
                   else
                   {
                    doRecordLock("PAYMENTS","unlock","unlocking");
                   }
                doCompleteCancelProcess();
             }
          }

          /*
           Returns user to the shopping cart or the recipient order page when
           the cancel button on the credit card decline popup is executed
          */
          function doCompleteCancelProcess(){
              /*
               use orderLevel variable to determine which page we cam from shopping cart or recipient order
             */
             var url = "";
             //go to shopping cart
             if( orderLevel == 'updateBilling' ){
                url = "http://" + siteName + applicationContext + "/customerOrderSearch.do?action=search" + "&in_order_number=" + document.getElementById('sc_order_number').value;
             }
             //go to recipient order page
             if( orderLevel == 'updatePayment'){
                url = "http://" + siteName + applicationContext + "/customerOrderSearch.do?action=search&recipient_flag=Y&in_order_number=" + document.getElementById('external_order_number').value;
             }
             document.forms[0].action = url;
             document.forms[0].submit();
          }

          /*
           continue to the addBilling screen
          */
          function doProceedAddBilling(){
             setUpdateFlag();
             if (testValueChange() == true)
          	 {
                  var modalUrl = "confirmstatus.html";
                  var modalArguments = new Object();
                  modalArguments.modalText = 'Are you sure you want to exit without saving your changes?';
                  var modalFeatures  =  'dialogWidth:250px; dialogHeight:175px; center:yes; status=no; help=no;';
                  modalFeatures +=  'resizable:no; scroll:no';
                  var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
                  if(ret)
                  {
          			     document.forms[0].action = "https://" + siteNameSsl + applicationContext + "/addBilling.do?page_action=search";
           document.forms[0].submit();
          			  }
                  else
                  {
           return false;
                  }
             }
          	 else
          	 {
          	    document.forms[0].action = "https://" + siteNameSsl + applicationContext + "/addBilling.do?page_action=search";
          		document.forms[0].submit();
          	 }
          }
          /*
           check & refresh the lock
          */
          function doAddBillingAction()
          {
          	if(document.getElementById('glbl_auth_message_flag').value == 'Y')
          	{
          		var modalArguments = new Object();
          		modalArguments.modalText = WARNING_NO_PAYMENTS;
          	    window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
          	}
          	else
          	{
          		forward = 'addBilling';
          		doRecordLock('PAYMENTS','retrieve',orderLevel);
          	}
          }

          /*
           if the lock is still obtained
           continue to updateAuth
          */
          function  doProceedUpdateAuth(){
             var flag1 = 'true';
             var flag = 'true';

             flag = checkAafesCode();
             if (flag == 'false' && flag1 == 'true')
                flag1 = 'false';

             if (flag1 == 'true')
             {
                document.getElementById('page_action').value = 'update';
                document.forms[0].action = "http://" + siteName + applicationContext + "/updateBilling.do";
                document.forms[0].submit();
             }

          }

          /*
           check & refresh the lock
          */
          function doUpdateAuthorizationAction()
          {
              forward = 'updateAuth';

              if( orderLevel == 'updateBilling' )
              {
                  doGUIDRecordLock('PAYMENTS','retrieve',orderLevel,"Updatebilling");
              }
              else
              {
                  doRecordLock('PAYMENTS','retrieve',orderLevel,"Updatebilling");
              }

          }

          /*
           called by the checkLockIFrame.xsl
          */
          function checkAndForward(forwardAction){
             if(forward == 'updateAuth' ){
                doProceedUpdateAuth();
             }
             if( forward == 'addBilling'){
                doProceedAddBilling();
             }
          }



          function setPaymentsnum()
          {
             document.getElementById('num_of_payments').value = totalNumber;
             return totalNumber;
          }

          function checkAafesCode()
          {
          var flag='true';
          var num = document.getElementById('num_of_payments').value
            for (var x = 1 ; x <= num ; x++)
            {
             var pay = document.getElementById('payment_type'+x).value;
             if (pay == 'MS')
             {
              if(document.getElementById('aafes_code'+x).value == "")
              {
              alert('Military Star ticket number is required for credit card processing');
              flag = 'false';
              }
                 }
             }
             return flag;
          }


                 ]]>
        </script>
      </head>
      <body onload="init();">
        <form name="Updatebilling" method="post" action="">
          <input type="hidden" name="update" id="update" value=""/>
          <input type="hidden" name="page_action" id="page_action" value=""/>
          <input type="hidden" name="num_of_payments" value="" id="num_of_payments"/>
          <input type="hidden" name="external_order_number" id="external_order_number"
                 value="{key('pagedata','external_order_number')/value}"/>
          <!-- <input type="hidden" name="num_of_payments" id="num_of_payments" value=""/> -->
          <input type="hidden" name="order_detail_id" id="order_detail_id"
                 value="{key('pagedata','order_detail_id')/value}"/>
          <input type="hidden" name="order_guid" id="order_guid"
                 value="{key('pagedata','order_guid')/value}"/>
          <input type="hidden" name="order_level" id="order_level"
                 value="{key('pagedata','order_level')/value}"/>
          <input type="hidden" name="source_code" id="source_code"
                 value="{key('pagedata','source_code')/value}"/>
          <input type="hidden" name="glbl_auth_message_flag" id="glbl_auth_message_flag"
                 value="{key('pagedata','glbl_auth_message_flag')/value}"/>
          <input type="hidden" name="master_order_number" id="master_order_number"
                 value="{key('pagedata','master_order_number')/value}"/>
          <input type="hidden" name="billing_level" id="billing_level"
                 value="{key('pagedata','billing_level')/value}"/>
          <input type="hidden" name="entity_type" id="entity_type"
                 value="{key('pagedata','entity_type')/value}"/>
          <input type="hidden" name="entity_id" id="entity_id"
                 value="{key('pagedata','entity_id')/value}"/>
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>
          <xsl:choose>
            <xsl:when test="$bill_level = 'CART'">
              <xsl:call-template name="addHeader">
                <xsl:with-param name="buttonText" select="'Back to Cust Acct'"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="addHeader">
                <xsl:with-param name="buttonText" select="'Back to Order'"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
          <!-- Display table-->
          <table width="98%" align="center" class="mainTable">
            <tr>
              <td colspan="7" class="TotalLine">
                <div align="center" class="TotalLine">
                  <div align="center">
                    <strong>Update Billing Information </strong>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <table border="0" width="100%" cellpadding="1" cellspacing="1"
                       class="innerTable">
                  <tr>
                    <td>
                      <div class="tableContainer" id="messageContainer">
                        <table width="100%" border="0" align="center"
                               cellpadding="" cellspacing="">
                          <thead class="fixedHeader" id="fixedHeader">
                            <tr class="Label">
                              <th align="center">
                              </th>
                              <th align="center">Payment Type</th>
                              <th align="center">Card Number</th>
                              <th align="center">Exp. Date</th>
                              <th align="center">Authorization#</th>
                              <th align="center">Charged</th>
                              <th align="center">
                              </th>
                            </tr>
                          </thead>
                          <tr>
                            <td colspan="7">
                            </td>
                          </tr>
                          <xsl:for-each select="root/ORDER_PAYMENTS/ORDER_PAYMENT">
                            <xsl:variable name="authstatus"
                                          select="auth_status/@auth_status"/>
                            <script>
                              totalNumber ++;
			               </script>
                            <tr>
                              <xsl:choose>
                                <xsl:when test=" payment_indicator[(. = preceding::payment_indicator)]">
                                  <td align="center" class="Label">
                                  </td>
                                </xsl:when>
                                <xsl:otherwise>
                                  <td align="center" class="Label">
                                    <xsl:value-of select="payment_indicator"/>
                                  </td>
                                </xsl:otherwise>
                              </xsl:choose>
                              <td align="center">
                                <xsl:value-of select="payment_type_description"/>
                              </td>
                              <xsl:choose>
                                <xsl:when test="(string-length(card_number)> 4)">
                                  <td align="center">
                                    <xsl:value-of select="card_number"/>
                                  </td>
                                </xsl:when>
                                <xsl:when test="(string-length(card_number)= 0)">
                                  <td align="center">
                                    <xsl:value-of select="card_number"/>
                                  </td>
                                </xsl:when>
                                <xsl:otherwise>
                                  <td align="center">xxxxxxxxxxxx
                                    <xsl:value-of select="card_number"/>
                                  </td>
                                </xsl:otherwise>
                              </xsl:choose>
                              <td align="center">
                                <xsl:value-of select="expiration_date"/>
                              </td>
                              <xsl:choose>
                                <xsl:when test="$authstatus = 'editable' and payment_type != 'MS'">
                                  <td align="center">
                                    <input type="text" size="15" maxlength="6"
                                           id="auth_code" name="auth_code{@num}"
                                           value="{auth_number}"
                                            tabindex = "{position()*2}"/>
                                  </td>
                                </xsl:when>
                                <xsl:otherwise>
                                  <td align="center">
                                    <xsl:value-of select="auth_number"/>
                                  </td>
                                </xsl:otherwise>
                              </xsl:choose>
                              <td align="center">$
                                <xsl:value-of select="format-number(credit_amount, '#,###,##0.00;(#,###,##0.00)','zeroValue')"/>
                              </td>
                                  <td align="center">
                                  </td>
                              <input type="hidden" name="status{@num}" id="status{@num}"
                                     value="{$authstatus}"/>
                              <input type="hidden" name="payment_id{@num}" id="payment_id{@num}"
                                     value="{payment_id}"/>
                              <input type="hidden" id="payment_type{@num}"
                                     name="payment_type{@num}"
                                     value="{payment_type}"/>
                              <input type="hidden" id="cardinal_verified_flag{@num}"
                                     name="cardinal_verified_flag{@num}"
                                     value="{cardinal_verified_flag}"/>
                            </tr>
                            <tr>
                              <td colspan="7">&nbsp;</td>
                            </tr>
                          </xsl:for-each>
                          <script>
	                        setPaymentsnum();
                        </script>
                          <tr>
                            <td colspan="7" align="center">
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!--Copyright bar-->
          <table width="98%" border="0" align="center" cellpadding="0"
                 cellspacing="1">
            <tr>
              <td align="center" width="100%">
                <button type="button" class="BlueButton" name="update_auth_but" accesskey="U"
                        onclick="doUpdateAuthorizationAction()" tabindex="700">(U)pdate Authorization</button>

		                    &nbsp;&nbsp;&nbsp;

                <button type="button" class="BlueButton" name="add_billing_but" accesskey="A" 
                        onclick="doAddBillingAction()" tabindex="710">(A)dd Billing</button>
              </td>
              <xsl:choose>
                <xsl:when test="$bill_level = 'CART'">
                  <td align="right">
                    <button type="button" class="BlueButton" name="back_but" accesskey="B" style="width:100px"
                            onclick="doBackAction();" tabindex="720">(B)ack to Cust Acct</button>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align="right">
                    <button type="button" class="BlueButton" name="back_but" accesskey="B" style="width:100px"
                            onclick="doBackAction();" tabindex="730">(B)ack to Order</button>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
          </table>
        </form>
        <!--Copyright bar-->
        <xsl:call-template name="footer"/>
        <iframe id="checkLock" width="0px" height="0px" border="0">
            <!-- This iframe is initialized with a "dummy" src value to
            prevent the MS IE browser from detecting these as nonsecure resources. -->
            <xsl:if test="$isclickthroughsecure = 'true'">
                <xsl:attribute name="src">https://<xsl:value-of select="$sitenamessl"/><xsl:value-of select="$applicationcontext"/>/html/blank.html</xsl:attribute>
            </xsl:if>
        </iframe>
      </body>
    </html>
  </xsl:template>
  <!-- display header -->
  <xsl:template name="addHeader">
    <xsl:param name="buttonText"/>
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Update Billing'"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showBackButton" select="true()"/>
      <xsl:with-param name="backButtonLabel" select="$buttonText"/>
      <xsl:with-param name="showSearchBox" select="false()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>
  </xsl:template>
</xsl:stylesheet>
