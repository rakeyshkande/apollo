<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="breadcrumbs">
  <xsl:param name="breadcrumbs"/>
  <xsl:param name="subheader"/>
  <xsl:param name="includeForm"/>
  <xsl:param name="bc_total" select="count($breadcrumbs/BREADCRUMB)"/>
  <table border="0" align="center" cellpadding="0" cellspacing="1" width="98%">
    <tr>
      <td style="background-color:#006699;text-decoration:none;color:white;font-size:9pt">
      <xsl:for-each select="$breadcrumbs/BREADCRUMB">
        <!-- The breadcrumb links may appear on the page multiple times.  'includeForm' is a way to decided when to include the form for the links -->
        <xsl:if test="boolean($includeForm) and position() = 1">
           <xsl:call-template name="breadcrumb-form">
             <xsl:with-param name="breadcrumbs" select="$breadcrumbs"/>
             <xsl:with-param name="subheader" select="$subheader"/>
           </xsl:call-template>
        </xsl:if>
        <xsl:if test="position() != 1">&nbsp;&gt;</xsl:if>
        &nbsp;<a tabindex="-1" href="javascript:doBreadcrumbsAction({position()-1});" style="text-decoration:none;color:white;font-size:9pt"><xsl:value-of select="breadcrumb_title"/></a>
      </xsl:for-each>
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template name="breadcrumb-form">
  <xsl:param name="breadcrumbs"/>
  <xsl:param name="subheader"/>
  <script type="text/javascript" src="js/breadcrumbs.js"/>  
  <form name="breadcrumbsForm" action="" method="post" style="display:inline;">
  <xsl:call-template name="securityanddata"/>
  <xsl:call-template name="decisionResultData"/>
  <xsl:call-template name="breadcrumb-params">
    <xsl:with-param name="breadcrumbs" select="$breadcrumbs"/>
    <xsl:with-param name="limit" select="last()"/>
  </xsl:call-template>
  <xsl:call-template name="cusProduct-params">
    <xsl:with-param name="subheader" select="$subheader"/>
  </xsl:call-template>
  </form>
</xsl:template>

<xsl:template name="breadcrumb-params">
  <xsl:param name="breadcrumbs"/>
  <xsl:param name="limit"/>
  <input type="hidden" name="breadcrumb_total" id="breadcrumb_total" value="{$limit}"/>
  <xsl:for-each select="$breadcrumbs/BREADCRUMB">
    <xsl:if test="position() &lt;= $limit">
      <input type="hidden" name="breadcrumb_title_{breadcrumb_num}" id="breadcrumb_title_{breadcrumb_num}" value="{breadcrumb_title}"/>
      <input type="hidden" name="breadcrumb_url_{breadcrumb_num}" id="breadcrumb_url_{breadcrumb_num}" value="{breadcrumb_url}"/>
      <input type="hidden" name="breadcrumb_action_{breadcrumb_num}" id="breadcrumb_action_{breadcrumb_num}" value="{breadcrumb_action}"/>
      <input type="hidden" name="breadcrumb_error_page_{breadcrumb_num}" id="breadcrumb_error_page_{breadcrumb_num}" value="{breadcrumb_error_page}"/>
      <input type="hidden" name="breadcrumb_enable_link_{breadcrumb_num}" id="breadcrumb_enable_link_{breadcrumb_num}" value="{breadcrumb_enable_link}"/>
      <input type="hidden" name="breadcrumb_param_count_{breadcrumb_num}" id="breadcrumb_param_count_{breadcrumb_num}" value="{PARAMS/breadcrumb_param_count}"/>
      <input type="hidden" name="breadcrumb_param_count_{breadcrumb_num}" id="breadcrumb_param_count_{breadcrumb_num}" value="{PARAMS/breadcrumb_param_count}"/>
      <xsl:variable name="bc_num" select="breadcrumb_num"/>
      <xsl:for-each select="PARAMS/PARAM">
        <input type="hidden" name="breadcrumb_param_name_{$bc_num}_{position()-1}" id="breadcrumb_param_name_{$bc_num}_{position()-1}" value="{breadcrumb_param_name}"/>
        <input type="hidden" name="breadcrumb_param_value_{$bc_num}_{position()-1}" id="breadcrumb_param_value_{$bc_num}_{position()-1}" value="{breadcrumb_param_value}"/>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template name="breadcrumb-url-string">
  <xsl:param name="breadcrumb"/>
  <xsl:value-of select="breadcrumb_url"/>
  <xsl:for-each select="PARAMS/PARAM">
    <xsl:choose><xsl:when test="position()=1">?</xsl:when><xsl:otherwise>&amp;</xsl:otherwise></xsl:choose>
    <xsl:value-of select="breadcrumb_param_name"/>=<xsl:value-of select="breadcrumb_param_value"/>
  </xsl:for-each>
</xsl:template>
</xsl:stylesheet>