
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>

  <xsl:decimal-format NaN="0.00" name="zeroValue"/>


	<xsl:output method="html" indent="yes"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>Credit Card</title>
				<base target="_self" />
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" language="JavaScript">

<![CDATA[

/***********************************************************************************
* back
* go back to credit card declined page
************************************************************************************/
function back() {

   //build array to return
   var returnArray= new Array()
   returnArray["page_action"]="close";

   top.returnValue = returnArray;
   top.close();
}
/***********************************************************************************
* examineStatus
* if a valid status was returned then go to add billing page
************************************************************************************/
function examineStatus()
{

	var status =document.forms[0].status.value;
	var methodToInvoke = document.forms[0].method_to_invoke.value;
	var message = document.forms[0].message.value;
	var netOwed = document.forms[0].net_owed.value - 0; //convert to numeric

	if (methodToInvoke.length == 0)
	{
		if(status == "valid")
		{
		 //build array to return
			var returnArray= new Array()
			returnArray["page_action"]="save";
			returnArray["gift_number"]=document.forms[0].gc_coupon.value;
			returnArray["amount_owed"]=document.forms[0].amount_owed.value;
			returnArray["status"]=document.forms[0].status.value;
			returnArray["amount"]=document.forms[0].amount.value;
			returnArray["net_owed_frmtd"]=document.forms[0].net_owed_frmtd.value;
			returnArray["net_owed"]=document.forms[0].net_owed.value;

		 top.returnValue = returnArray;
		 top.close();
		}
	}
	else
	{
		if (methodToInvoke == 'postC30Refund')
		{
			if (status == "valid")
			{
				parent.document.getElementById('is_gc_valid').value = 'Y';
				parent.document.getElementById('gc_amount').value = document.forms[0].amount.value;
			}
			else
			{
				parent.document.getElementById('is_gc_valid').value = 'N';
				parent.document.getElementById('invalid_gc_message').value = message;
			}
			parent.postC30Refund();
		}
	}

}
/***********************************************************************************
* submit
* Validate the entered data and close page
************************************************************************************/
function submitPage(){

   if(validate())
   {
      document.getElementById('page_action').value = 'redeem';
      var url = "gcCoupon.do?";

     // var frame = document.getElementById('gcCouponIFrame');
     // frame.src = "gcCoupon.do" + getFormNameValueQueryString(document.getElementById('gcCouponFrm') ) ;

    performAction(url);
   }
}

/*
 executed when the user hits a key
*/
function checkKey(){
    if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
        return false;
    submitPage();

}


/***********************************************************************************
* validate
* contains page validation
************************************************************************************/
function validate()
{

   var giftField = document.forms[0].gc_coupon_number;
   var giftValue = document.forms[0].gc_coupon_number.value;

   var valid = true;

   //field required
   if (giftValue == null || giftValue.length ==0  )
   {
	  alert("Gift Certificate Number Required.");
      giftField.focus();
      giftField.style.backgroundColor='pink';
      valid = false;
   }

  return valid;
}

/***********************************************************************************
* performAction
* forward to a page
************************************************************************************/
function performAction(url)
{
  document.forms[0].action = url;
  document.forms[0].submit();
}


       ]]></script>
			</head>
			<body onload="examineStatus();" onkeypress="checkKey();">
				<form method="post" action="" name="gcCouponFrm">
        <!-- these fields get returned to the addBilling.xsl page upon successful verification-->
          <input type="hidden" name="status" id="status" value="{root/gcc_status}"></input>
          <input type="hidden" name="method_to_invoke" id="method_to_invoke" value="{root/method_to_invoke}"></input>
          <input type="hidden" name="amount_owed" id="amount_owed" value="{root/amount_owed}"></input>
          <input type="hidden" name="amount" id="amount" value="{format-number(root/amount,'#,###,##0.00;#,###,##0.00','zeroValue')}"></input>
          <input type="hidden" name="net_owed_frmtd" id="total_owed_to_return" value="{format-number(root/net_owed,'#,###,##0.00;#,###,##0.00','zeroValue')}"></input>
          <input type="hidden" name="net_owed" id="total_owed_to_return" value="{root/net_owed}"></input>
          <input type="hidden" name="gc_coupon" id="gc_coupon" value="{root/gift_number}"></input>

          <input type="hidden" name="message" id="message" value="{root/message}"/>
          <input type="hidden" name="page_action" id="page_action" value=""/>
          <!-- sent to the gcCoupon Action w/ the gc_coupon_number for verification -->
          <input type="hidden" name="total_owed" id="total_owed" value="{root/total_owed}"></input>

					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<!-- Header-->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName" select="''" />
						<xsl:with-param name="dnisNumber" select="$call_dnis"/>
						<xsl:with-param name="brandName" select="$call_brand_name" />
						<xsl:with-param name="indicator" select="$call_type_flag" />
						<xsl:with-param name="cservNumber" select="$call_cs_number" />
						<xsl:with-param name="showBackButton" select="false()"/>
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- Display table-->
					<span class="ErrorMessage">
					<xsl:value-of select="root/message"/>
					</span>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr><td class="TopHeading" align="center">
									Enter Your Gift Certificate Number:
									<input type="text" size="25" name="gc_coupon_number" id="gc_coupon_number" maxlength="25"></input>
									</td></tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<div align="center">
								     <button type="button" class="BlueButton" accesskey="S" onclick="submitPage();">(S)ubmit Billing</button>
								</div>
							</td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" >
						<tr>
							<td>
								<div align="right">
								     <button type="button" class="BlueButton" accesskey="B" onclick="back();">(B)ack</button>
								</div>
							</td>
						</tr>
					</table>
<!-- Processing message div -->
 <div id="waitDiv" style="display:none">
    <table align="center" id="waitTable" class="mainTable" width="98%" border="0" cellpadding="0" cellspacing="2" height="100%">
       <tr>
          <td>
	         <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="innerTable">
		       <tr>
                  <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                  <td id="waitTD"  width="50%" class="waitMessage"></td>
		       </tr>
	         </table>
          </td>
       </tr>
    </table>
 </div>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
        <iframe id="gcCouponIFrame" width="0px" height="0px" border="0"/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>