
<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="cusProduct.xsl"/>
	<xsl:output indent="yes" method="html"/>
	<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<!-- node paths -->
	<xsl:variable name="LASTCREDITCARD" select="root/LAST_CREDITCARDS/LAST_CREDITCARD"/>
	<xsl:variable name="LASTGIFTCARD" select="root/LAST_GIFTCARDS/LAST_GIFTCARD"/>
	<!-- variables -->
	<xsl:variable name="corpPurch" select="key('pageData','corp_purch')/value"/>
	<xsl:variable name="chargeAmt" select="key('pageData','order_total')/value"/>
	<xsl:variable name="currentCCType" select="$LASTCREDITCARD/cc_type"/>
	<xsl:variable name="currentCCExp" select="$LASTCREDITCARD/cc_expiration"/>
	<xsl:variable name="currentCC" select="$LASTCREDITCARD/cc_number"/>
	<xsl:variable name="currentGD" select="$LASTGIFTCARD/gift_card_number"/>
	<xsl:variable name="currentGDPin" select="$LASTGIFTCARD/gift_card_pin"/>
	<xsl:variable name="walletIndicator" select="$LASTCREDITCARD/wallet_indicator"/>
	<xsl:variable name="gdPaymentEnabled" select="root/GIFTCARD_SETTINGS/gift_card_payment_enabled"/>
	
	<xsl:variable name="YES" select="'true'"/>
  <xsl:variable name="billingPrompt" select="root/BILLING_INFO/BILLING_INFO/billinginfoprompt"/>

	<!-- format xml numbers if not present -->
	<xsl:decimal-format NaN="" name="notANumber"/>
	<xsl:decimal-format NaN="0.00" name="zeroValue"/>
	
	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Payment </title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript" src="js/commonUtil.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript" src="js/payment.js"/>
        <SCRIPT language="JavaScript">
        var siteName = '<xsl:value-of select="$sitename"/>';
        var applicationContext = '<xsl:value-of select="$applicationcontext"/>';
    
  		<![CDATA[
      /* This is a hack.  I can't find a way to turn the string infoFormat
        from a string into a regular expression.  If you can fix it, please do*/
      function matchDynamicField(element, rowNum)
      {
        ]]>
        <xsl:for-each select="/root/BILLING_INFO/BILLING_INFO">
          var  billingSeq = "<xsl:value-of select="billinginfosequence"/>";
          <![CDATA[
            if (billingSeq == rowNum)
            {
                ]]>
              var formatString = <xsl:value-of select="infoformat"/>;
                  <![CDATA[
            }
                ]]>
        </xsl:for-each>
          <![CDATA[
        return ((element.value).match(formatString));
      }
    ]]>
    </SCRIPT>
			</head>
			<body onload="init();">
				<xsl:call-template name="addHeader"/>
        <!-- These iframes are initialized with a "dummy" src value to
        prevent the MS IE browser from detecting these as nonsecure resources. -->
				<iframe id="checkLock" width="0px" height="0px" border="0px" src="https://{$sitenamessl}{$applicationcontext}/html/blank.html"/>
				<iframe id="placeOrderFrame" width="0px" height="0px" border="0px" src="https://{$sitenamessl}{$applicationcontext}/html/blank.html"/>
        <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0" src="https://{$sitenamessl}{$applicationcontext}/html/blank.html"/>
  		<div id="mainContent" style="display:block">
					<form name="paymentForm" id="paymentForm" action="" method="post">


						<xsl:call-template name="securityanddata"/>
						<xsl:call-template name="decisionResultData"/>
						<!-- The next two fields help us send data to the customerOrderSearch search screen when we are finished with this page -->

						<input type="hidden" name="recipient_flag" id="recipient_flag" value="" />
						<input type="hidden" name="in_order_number" id="in_order_number" value="" />
						<!--  end customerOrderSearch params -->
						<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}" />
          	<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData','external_order_number')/value}" />
           	<input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}" />
           	<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}" />
						<input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}" />
						<input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}" />
						<input type="hidden" name="partner_id" id="partner_id" value="{key('pageData','partner_id')/value}" />
    	      <input type="hidden" name="pay_type" id="pay_type" value="{key('pageData','payment_type')/value}"/>
            <input type="hidden" name="is_orig_cc" id="is_orig_cc" value="N"/>
            <input type="hidden" name="is_gd_dirty" id="is_gd_dirty" value="N"/>
            <input type="hidden" name="lastCCType" id="lastCCType" value="{$LASTCREDITCARD/cc_type}"/>
            <input type="hidden" name="lastCCExp" id="lastCCExp" value="{$LASTCREDITCARD/cc_expiration}"/>
            <input type="hidden" name="lastCCExpMonth" id="lastCCExpMonth" value=""/>
            <input type="hidden" name="lastCCExpYear" id="lastCCExpYear" value=""/>
            <input type="hidden" name="lastCC" id="lastCC" value="{$LASTCREDITCARD/cc_number}"/>
            
			<input type="hidden" name="wallet_indicator"  id="wallet_indicator" value="{$LASTCREDITCARD/wallet_indicator}"/>
			
            <input type="hidden" name="gd_or_gc" id="gd_or_gc" value="GD"/>
            <input type="hidden" name="giftCardPaymentEnabled" id="giftCardPaymentEnabled" value="{$gdPaymentEnabled}"/>            
            
            <input type="hidden" name="billingPrompt" id="billingPrompt" value="{$billingPrompt}"/>
            <input type="hidden" name="appr_gcc_num" id="appr_gcc_num" value=""/>
            <input type="hidden" name="appr_gcc_amt" id="appr_gcc_amt" value=""/>
            <input type="hidden" name="gd_pin"  id="gd_pin" value="{$LASTGIFTCARD/gift_card_pin}" />
            <input type="hidden" name="cc_skip_auth" id="cc_skip_auth" value=""/>
						<input type="hidden" name="approval_password" id="approval_password" value=""/>


						 <!-- Customer Product data -->
						 <xsl:call-template name="cusProduct">
							 <xsl:with-param name="subheader" select="subheader"/>
							 <xsl:with-param name="displayContinue" select="false()"/>
							 <xsl:with-param name="displayComplete" select="false()"/>
							 <xsl:with-param name="displayCancel" select="false()"/>
						 </xsl:call-template>

						<table align="center" border="0" cellpadding="0" cellspacing="1" width="98%">
							<tr>
								<td style="background-color:#006699;text-decoration:none;color:white;font-size:9pt">
				Billing
								</td>
							</tr>
						</table>

						<table class="mainTable" align="center" border="0" cellpadding="0" cellspacing="1" width="98%">
							<tr>
								<td>
									<table class="innerTable" align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
										<tr>
                      <xsl:if test="$billingPrompt">
                        <xsl:attribute name="style">display:none</xsl:attribute>
                      </xsl:if>
											<td>
												<span class="error" id="error" style="display:none"></span>
												<table width="100%" border="0" cellpadding="1" cellspacing="10">
													<tr>
														<td colspan="3" align="left" class="SectionHeader"> Credit Card
														</td>
													</tr>
													<tr>
														<td colspan="3" class="ScreenPrompt">
									What credit card will you be using today?  May I have your...?
														</td>
													</tr>
													<tr id="giftCertMessageRow" style="display:none">
														<td class="Label" width="100%" colspan="3" >
                              Gift card/certificate <span id = "giftCertId"></span>&nbsp;was applied in the amount of $<span id="gcTotal"></span>.  The remaining total is $<span id="remainingTotal"></span>.
                          								</td>
                          							</tr>
													<tr>
														<td class="Label" width="25%">
															<div align="right">Payment Type</div>
														</td>
														<td colspan="2" width="75%" class="TextField">
                              <xsl:choose>
                              <xsl:when test="$billingPrompt">
                                <input type="hidden" id="pay_method" name="pay_method" value="{key('pageData','payment_method')/value}"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <select tabindex="1" id="pay_method" name="pay_method" onchange="javascript:onPayMethodChange();">
                                  <xsl:if test="$corpPurch = $YES">
                                    <xsl:attribute name="disabled">true</xsl:attribute>
                                  </xsl:if>
                                  <option>-- Please Select --</option>
                                  <xsl:for-each select="PAYMENT_METHODS/PaymentMethodVO">
                                    <option value="{paymentMethodId}">
										<xsl:choose>
	                                      <xsl:when test="paymentMethodId = $LASTCREDITCARD/cc_type">
	                                        <xsl:attribute name="selected">true</xsl:attribute>
	                                      </xsl:when>
	                                      <xsl:when test="paymentMethodId='GC' and $LASTGIFTCARD/gift_card_number!=''">
	                                        <xsl:attribute name="selected">true</xsl:attribute>
	                                      </xsl:when>
	                                  	</xsl:choose>
                                      <xsl:value-of select="description"/>
                                    </option>
                                  </xsl:for-each>
                                </select>
                     &nbsp;
                                <SPAN style="color:red"> *** </SPAN>
                            </xsl:otherwise>
                          </xsl:choose>
														</td>
													</tr>
													<tr>
														<td class="LabelRight">Credit Card Number </td>
														<td colspan="2" class="TextField">
															<xsl:choose>
																<xsl:when test="$LASTCREDITCARD/cc_number">
																	<input type="text" size="30" tabindex="4" name="card_num" id="card_num" onFocus="javascript:fieldFocus('card_num')" onblur="javascript:fieldBlur('card_num')" value="************{$LASTCREDITCARD/cc_number}" >
																		<xsl:if test="$corpPurch = $YES">
																			<xsl:attribute name="disabled">true</xsl:attribute>
																		</xsl:if>
																	</input>
																</xsl:when>
																<xsl:otherwise>
																	<input type="text" size="30" tabindex="4" name="card_num" id="card_num" onFocus="javascript:fieldFocus('card_num')" onblur="javascript:fieldBlur('card_num')" value="" >
																		<xsl:if test="$corpPurch = $YES">
																			<xsl:attribute name="disabled">true</xsl:attribute>
																		</xsl:if>
																	</input>
																</xsl:otherwise>
															</xsl:choose>
									&nbsp;
															<SPAN style="color:red"> *** </SPAN>
														</td>
													</tr>
													<tr>
														<td class="LabelRight">
															<div align="right">Expiration Date </div>
														</td>
														<td colspan="2" class="TextField">
															<select tabindex="5" id="exp_month" name="exp_month">
																<xsl:if test="$corpPurch = $YES">
																	<xsl:attribute name="disabled">true</xsl:attribute>
																</xsl:if>
																<option> </option>
																<option value="01">January</option>
																<option value="02">February</option>
																<option value="03">March</option>
																<option value="04">April</option>
																<option value="05">May</option>
																<option value="06">June</option>
																<option value="07">July</option>
																<option value="08">August</option>
																<option value="09">September</option>
																<option value="10">October</option>
																<option value="11">November</option>
																<option value="12">December</option>
															</select>
									/
															<select tabindex="6" id="exp_year" name="exp_year">
																<xsl:if test="$corpPurch = $YES">
																	<xsl:attribute name="disabled">true</xsl:attribute>
																</xsl:if>
															</select>
								 &nbsp;
															<SPAN style="color:red"> *** </SPAN>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="giftCertificateRow" style="display:none">
											<td>
												<table width="100%" border="0" cellpadding="1" cellspacing="10">
													<TR>
														<TD colspan="3" class="SectionHeader">Gift Card/Certificate
														</TD>
													</TR>
													<TR>
														<TD colspan="3" class="ScreenPrompt">May I have the number that appears on your gift card or certificate?</TD>
													</TR>
													<TR>
														<TD valign="top" width="25%" class="labelright"> Gift Card/Certificate Number </TD>
														<TD valign="top" width="50%" >
															<xsl:choose>
																<xsl:when test="$LASTGIFTCARD/gift_card_number!='' and $gdPaymentEnabled!='N'">
																	<input tabindex="30" type="text" name="gcc_num" id="gcc_num" size="19" maxlength="19" onkeydown="return clearPin(event)" 
																		onFocus="javascript:fieldFocus('gcc_num')" onblur="javascript:checkGCType('gcc_num')" 
																		value="************{$LASTGIFTCARD/gift_card_number}"/>&nbsp; &nbsp;
																</xsl:when>
																<xsl:otherwise>
																	<input tabindex="30" type="text" name="gcc_num" id="gcc_num" size="19" maxlength="19" onkeydown="return clearPin(event)"  
																		onFocus="javascript:fieldFocus('gcc_num')" onblur="javascript:checkGCType('gcc_num')"/>&nbsp; &nbsp;
																</xsl:otherwise>
															</xsl:choose>
															<SPAN style="color:red"> *** </SPAN>
														</TD>
														<TD width="25%" class="Instruction" valign="top">
														</TD>
													</TR>
													<TR id="giftCardPinRow">													
                      									<xsl:if test="$gdPaymentEnabled='N'">
                        									<xsl:attribute name="style">display:none</xsl:attribute>
                      									</xsl:if>
														<TD valign="top" width="25%" class="labelright"> Gift Card PIN </TD>
														<TD valign="top" width="50%" >
															<xsl:choose>
																<xsl:when test="$LASTGIFTCARD/gift_card_pin!=''">
																	<input tabindex="30" type="text" name="gcc_pin" id="gcc_pin" size="4" maxlength="4" onkeydown="" 
																		onFocus="javascript:fieldFocus('gcc_pin')" onblur="javascript:fieldBlur('gcc_pin')" disabled="true"
																		value="{$LASTGIFTCARD/gift_card_pin}"/>&nbsp; &nbsp;
																</xsl:when>
																<xsl:otherwise>
																	<input tabindex="30" type="text" name="gcc_pin" id="gcc_pin" size="4" maxlength="4" onkeydown="" 
																		onFocus="javascript:fieldFocus('gcc_pin')" onblur="javascript:fieldBlur('gcc_pin')" disabled="true"/>&nbsp; &nbsp;
																</xsl:otherwise>
															</xsl:choose>
														</TD>
														<TD width="25%" class="Instruction" valign="top">
														</TD>
													</TR>
												</table>
											</td>
										</tr>
										<tr id="invoiceRow" style="display:block">
                    <xsl:if test="$billingPrompt">
											<td>
												<table width="100%" border="0" cellpadding="1" cellspacing="10">
													<TR>
														<TD colspan="3" class="SectionHeader">Payment Information
														</TD>
													</TR>
													<TR>
														<TD colspan="3" class="ScreenPrompt">May I have your...?</TD>
													</TR>
													<xsl:for-each select="/root/BILLING_INFO/BILLING_INFO">
														<TR id="promptListRow" billingInfoPrompt="{billinginfoprompt}">
															<TD valign="top" class="labelright" width="25%">
																<xsl:value-of select="billinginfodisplay"/>
															</TD>
															<TD valign="top" width="50%">
																<xsl:variable name="billingInfoPromptVar" select="billinginfoprompt"/>
																<xsl:variable name="promptNumber" select="position()-1"/>
																<xsl:variable name="promptName" select="concat('promptName',$promptNumber)"/>
																<xsl:variable name="promptValue" select="concat('promptValue',$promptNumber)"/>
																<xsl:choose>
																	<xsl:when	test="prompttype = '' or prompttype = 'TEXTBOX'">
																		<input type="hidden" name="{$promptName}" id="{$promptName}" value="{billinginfoprompt}"/>
																		<input tabindex="5" type="text" name="{$promptValue}" id="{$promptValue}"  infoFormat = "{infoformat}" size="16" maxlength="30">
																			<xsl:attribute name="onfocus">javascript:fieldFocus('<xsl:value-of select="$promptValue"/>')</xsl:attribute>
																			<xsl:attribute name="onblur">javascript:fieldBlur('<xsl:value-of select="$promptValue"/>')</xsl:attribute>
																		</input>
																	</xsl:when>
																	<xsl:when	test="prompttype = 'DROPDOWN'">
																		<input type="hidden" name="{$promptName}" id="{$promptName}" value="{billinginfoprompt}"/>
																		<select tabindex="5" name="{$promptValue}" id="{$promptValue}"  infoFormat = "{infoformat}">
																			<xsl:for-each select="/root/BILLING_INFO_OPTIONS/BILLING_INFO_OPTIONS[billinginfodesc = $billingInfoPromptVar]">
																				<option value="{optionvalue}">
																					<xsl:value-of select="optiondisplay"/>
																				</option>
																			</xsl:for-each>
																		</select>
																	</xsl:when>
																	<xsl:when	test="prompttype = 'STATIC'">
																		<xsl:variable name="hiddenValue" select="/root/BILLING_INFO_OPTIONS/BILLING_INFO_OPTIONS[billinginfodesc = $billingInfoPromptVar]/optionvalue"/>
																		<input type="hidden" name="{$promptName}" id="{$promptName}" value="{billinginfoprompt}"/>
																		<input tabindex="5" type="text" name="{$promptValue}" id="{$promptValue}" infoFormat = "{infoformat}" size="16" maxlength="30" value="{$hiddenValue}" disabled="true" />
																	</xsl:when>
																</xsl:choose>
															</TD>
															<TD class="Instruction" valign="top" width="25%"></TD>
														</TR>
													</xsl:for-each>
												</table>
											</td>
                      </xsl:if>
										</tr>
										<tr>
											<td>
												<table width="100%" border="0" cellpadding="1" cellspacing="10">
													<tr>
														<td colspan="3" class="ScreenPrompt">
									The total amount charged to your card will be...
														</td>
													</tr>
													<tr>
														<td class="LabelRight" width="25%">
															<div align="right">Charge Amount </div>
														</td>
														<td colspan="2" width="75%" class="TextField">
															<span id="charge_amt_span">
																<xsl:value-of select="format-number($chargeAmt,'#,###,##0.00;#,###,##0.00','zeroValue')" />
															</span>
															<input type="hidden" size="9" tabindex="4" name="charge_amt" id="charge_amt" value="{format-number($chargeAmt,'#,###,##0.00;#,###,##0.00','zeroValue')}"/>
															
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<DIV id="authDIV" style="display:none">
													<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
														<TR>
															<TD colspan="3">
																<HR/>
															</TD>
														</TR>
														<TR>
															<TD valign="top" width="25%" class="labelright"> Verbal Authorization:</TD>
															<TD valign="top" width="50%" >
																<input tabindex="5" name="auth_code" id="auth_code" size="6" maxlength="6" value="" onFocus="javascript:fieldFocus('auth_code')" onblur="javascript:fieldBlur('auth_code')"></input>
										&nbsp;
																<SPAN style="color:red"> *** </SPAN>
															</TD>
															<TD class="Instruction" width="25%" valign="top"></TD>
														</TR>
														<TR id="aafesRow" style="display:none">
															<TD valign="top" width="25%" class="labelright"> AAFES Codes:</TD>
															<TD valign="top" width="50%" >
																<input tabindex="5" name="aafes_code" id="aafes_code" size="6" maxlength="6" value="" onFocus="javascript:fieldFocus('aafes_code')" onblur="javascript:fieldBlur('aafes_code')"></input>
										&nbsp;
																<SPAN style="color:red"> *** </SPAN>
															</TD>
															<TD class="Instruction" width="25%" valign="top"></TD>
														</TR>
													</TABLE>
												</DIV>
												<DIV id="noChargeDIV" style="display:none">
													<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
														<TR>
															<TD colspan="3">
																<HR/>
															</TD>
														</TR>
														<TR>
															<TD width="25%" valign="top" class="labelright"> Approval ID </TD>
															<TD width="40%" valign="top" >
																<input tabindex="6" type="text" name="approval_id" id="approval_id" size="10" maxlength="30" onFocus="javascript:fieldFocus('approval_id')" onblur="javascript:fieldBlur('approval_id')"/>
										&nbsp;
																<SPAN style="color:red"> *** </SPAN>
															</TD>
															<TD width="35%" class="Instruction" valign="top">User ID of manager approving No Charge payment.</TD>
														</TR>
														<TR>
															<TD valign="top" width="25%" class="labelright"> Approval Password </TD>
															<TD valign="top" width="40%" >
																<input tabindex="7" type="password" name="approval_passwd" id="approval_passwd" size="10" maxlength="10" onFocus="javascript:fieldFocus('approval_passwd')" onblur="javascript:fieldBlur('approval_passwd');forms[0].approval_password.value = this.value;"/>
										&nbsp;
																<SPAN style="color:red"> *** </SPAN>
															</TD>
															<TD class="Instruction" width="35%" valign="top">Password of manager approving No Charge payment.</TD>
														</TR>
													</TABLE>
												</DIV>
											</td>
										</tr>
										<tr>
											<td>
												<table width="100%" border="0" cellpadding="1" cellspacing="10">
													<tr>
														<td colspan="4" align="right">
															 <div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
																	<div style="text-align:right;padding-top:.5em;">
																		 <span style="padding-right:2em;">
																			<button type="button" name="place_order_b" id="place_order_b" tabindex="10" accesskey="P" class="BigBlueButton" style="width:110px;" onclick="javascript:doPlaceOrder();">(P)lace Order</button>
																		 </span>
								                     <button type="button" tabindex="11" id="updCancelButton" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
																	</div>
															 </div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="1" width="98%">
							<tr>
								<td style="background-color:#006699;text-decoration:none;color:white;font-size:9pt">
				Billing
								</td>
							</tr>
						</table>

					</form>
					<xsl:call-template name="footer"/>
				</div>
        <!-- Processing message div -->
       <div id="waitDiv" style="display:none">
          <table align="center" id="waitTable" height="100%" class="mainTable" width="98%"  border="0" cellpadding="0" cellspacing="2" >
             <tr>
                <td height="100%">
                 <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="innerTable">
                 <tr>
                    <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                    <td id="waitTD"  width="50%" class="WaitMessage"></td>
                 </tr>
                 </table>
                </td>
             </tr>
          </table>
       </div>


			</body>
		</html>
	</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="''"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="overrideRTQ" select="Y"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>