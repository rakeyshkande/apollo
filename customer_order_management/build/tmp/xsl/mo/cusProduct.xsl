<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="cusProduct">
	<xsl:param name="subheader"/>
	<xsl:param name="displayContinue"/>
	<xsl:param name="displayComplete"/>
	<xsl:param name="displayCancel"/>

	<style type="text/css">
        
        .tooltip{ 
            display:none;
            position:absolute;
            top:2em; left:2em; width:12em;
            border:1px solid #3669AC;
            background-color: #CCEFFF;
            color:#000000;
            text-align: left;
            padding-left: 4px;
            padding-right: 4px;
            font-family: arial,helvetica,sans-serif;
            font-size: 8pt;
            text-decoration:none;
            filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
        }

    </style>

    <script type="text/javascript" language="javascript">

 	var taxArray = new Array( <xsl:for-each select="$subheader/subheader_taxes/subheader_tax">
                                        ["<xsl:value-of select="sh_tax_description"/>",
                                        "<xsl:value-of select="sh_tax_amount"/>",
                                        "<xsl:value-of select="sh_display_order"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );

<![CDATA[

	function sortByDisplayOrder(a,b) {
		return a[2] - b[2];
	}                              

    taxArray.sort(sortByDisplayOrder);

    function displayTaxesOnMouseOver(xpos,ypos) {
        var mouseOverString = "";
        for (var i=0; i<taxArray.length; i++) {
            if (taxArray[i][1] != '' && taxArray[i][1] > 0) {
                mouseOverString = mouseOverString + taxArray[i][0] + ': ';
         	    mouseOverString = mouseOverString + formatDollarAmt(taxArray[i][1]) + '<br>';
     	    }
	    }
        if (mouseOverString != "" ) {
            $("#spanTaxMouseOver").html(mouseOverString);
            $("#spanTaxMouseOver")[0].style.top=ypos+5;
            $("#spanTaxMouseOver")[0].style.left=xpos+10;
            $("#spanTaxMouseOver").show();
        }
        // hack for IE7 issue where space appears under the tabs when you mouse over
        //$("iframe.hidden").attr("style", "display:none");
        //$("div.visible").attr("style", "margin:0px");

    }

]]>
    </script>

		<table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div style="float:left;">
						<span class="Label">Current Customer is: </span><xsl:value-of select="$subheader/subheader_info/sh_customer_first_name"/>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_customer_last_name"/><br/>
						<span class="Label">Order #: </span><xsl:value-of select="$subheader/subheader_info/sh_external_order_number"/><br/>


						<xsl:choose>
							<xsl:when test="$subheader/subheader_info/sh_delivery_date != $subheader/subheader_info/sh_delivery_date_range_end
															and
													 		$subheader/subheader_info/sh_delivery_date_range_end != ''">
								<span class="Label">Requested Delivery Date Range: </span>
								<xsl:value-of select="$subheader/subheader_info/sh_delivery_date"/>
								&nbsp;-&nbsp;
								<xsl:value-of select="$subheader/subheader_info/sh_delivery_date_range_end"/>
								<br/>
							</xsl:when>
							<xsl:otherwise>
								<span class="Label">Requested Delivery Date: </span>
								<xsl:value-of select="$subheader/subheader_info/sh_delivery_date"/>
								<br/>
							</xsl:otherwise>
						</xsl:choose>


						<span class="Label">Item #: </span>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_product_id"/> - <xsl:value-of select="$subheader/subheader_info/sh_product_name"/>

						<xsl:if test="string-length($subheader/subheader_info/sh_color1_description) &gt; 0">
							<span class="Label"><br/>1st Color Choice:</span>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_color1_description"/>&nbsp;&nbsp;
						</xsl:if>

						<xsl:if test="string-length($subheader/subheader_info/sh_color2_description) &gt; 0">
							<span class="Label">2nd Color Choice:</span>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_color2_description"/>
						</xsl:if>

						<br/>
						<span class="Label">Merch Price:</span>&nbsp;$&nbsp;
						<xsl:choose>
							<xsl:when test="$subheader/subheader_info/sh_discount_amount &gt; 0">
								<strike style="color:red"><xsl:value-of select="format-number($subheader/subheader_info/sh_product_amount, '#,###,##0.00')"/></strike>&nbsp;<xsl:value-of select="format-number($subheader/subheader_info/sh_product_amount - $subheader/subheader_info/sh_discount_amount, '#,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="format-number($subheader/subheader_info/sh_product_amount, '#,###,##0.00')"/>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:if test="$subheader/subheader_info/sh_miles_points > 0">
							&nbsp;(<xsl:value-of select="$subheader/subheader_info/sh_miles_points"/>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_discount_reward_type"/>)
						</xsl:if>

						<xsl:if test="$subheader/subheader_info/sh_ship_method_desc != ''">
							<br/>
							<span class="Label">Ship Method: </span>&nbsp;&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_ship_method_desc"/>
						</xsl:if>
            
                        <xsl:if test="$subheader/subheader_info/sh_tax != '' and number($subheader/subheader_info/sh_tax) != 0">
							<br/>
							<span class="Label">Taxes: </span>
        				    <span class="taxValue" style="text-decoration: none; cursor:default; color: black;">
    							&nbsp;$&nbsp;<xsl:value-of select="format-number($subheader/subheader_info/sh_tax, '#,###,##0.00')"/>
	    					</span>
    						<span id="spanTaxMouseOver" class="tooltip" ></span>
		    				<script>
			    			$(".taxValue").mouseover(function(e) {displayTaxesOnMouseOver(e.pageX, e.pageY); });
                            $(".taxValue").mouseout(function() {
                            	$("#spanTaxMouseOver").hide(); 
                            	//IE7 hack to hide whitespace that appears under tabs
                    	        //$("iframe.hidden").attr("style", "display:none");
                    	        //$("div.visible").attr("style", "margin:0px");
                   	        });
					    	</script>
						</xsl:if>

						<xsl:if test="count($subheader/subheader_addons/subheader_addon) != 0">
							<br/>
							<span class="Label">Add-Ons:</span> $ <xsl:value-of select="format-number($subheader/subheader_info/sh_add_on_amount, '#,###,##0.00')"/>
						</xsl:if>

						<xsl:for-each select="$subheader/subheader_addons/subheader_addon">
							<xsl:if test="position() mod 2 > 0">
								<br/>
							</xsl:if>
							<xsl:value-of select="sh_add_on_description"/>(<xsl:value-of select="sh_add_on_quantity"/>) &nbsp;&nbsp;&nbsp;
						</xsl:for-each>
					</div>

					<!-- Top Buttons -->
					<div style="margin-left:0px;margin-top:0px;align:center">
						<div style="width:35%;float:right;">
							<!--
							<div style="text-align:right;padding-bottom:.5em;">
							<xsl:if test="$displayContinue">
							<button type="button" id="custUpdateProduct" class="BigBlueButton" style="width:110px;" accesskey="t" onclick="javascript:doContinueUpdateAction();">Con(t)inue Update<br/>Product</button>
							</xsl:if>
							</div>
							<xsl:if test="$displayComplete">
							<button type="button" id="custComplete" style="width:110px;" class="BigBlueButton" accesskey="p" onclick="javascript:doCompleteUpdateAction();">Com(p)lete Update</button>
							</xsl:if>
							-->
							<!--
							&nbsp;display_complete&nbsp;=&nbsp;			<xsl:value-of select="$displayComplete"				/>
							&nbsp;display_continue&nbsp;=&nbsp;			<xsl:value-of select="$displayContinue"				/>
							-->

							<div style="text-align:right;">
								<span style="padding-right:2em;">
									<xsl:choose>
										<xsl:when test="$displayComplete and $displayContinue">
											<button type="button" id="custUpdateProduct" class="BigBlueButton" style="width:110px;" accesskey="t" onclick="javascript:doContinueUpdateAction();">Con(t)inue Update<br/>Product</button>
										</xsl:when>
										<xsl:when test="$displayComplete and $displayContinue!='true'">
											<button type="button" id="custComplete" style="width:110px;" class="BigBlueButton" accesskey="p" onclick="javascript:doCompleteUpdateAction();">Com(p)lete Update</button>
										</xsl:when>
										<xsl:when test="$displayComplete!='true' and $displayContinue">
											<button type="button" id="custUpdateProduct" class="BigBlueButton" style="width:110px;" accesskey="t" onclick="javascript:doContinueUpdateAction();">Con(t)inue Update<br/>Product</button>
										</xsl:when>
									</xsl:choose>
								</span>
								<xsl:if test="$displayCancel">
									<button type="button" id="custCancel" class="BigBlueButton" style="width:110px;" accesskey="C" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
								</xsl:if>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="cusProduct-params">
		<xsl:param name="subheader"/>
		<input type="hidden" name="sh_customer_first_name" id="sh_customer_first_name" value="{$subheader/subheader_info/sh_customer_first_name}"/>
		<input type="hidden" name="sh_customer_last_name" id="sh_customer_last_name" value="{$subheader/subheader_info/sh_customer_last_name}"/>
		<input type="hidden" name="sh_external_order_number" id="sh_external_order_number" value="{$subheader/subheader_info/sh_external_order_number}"/>
		<input type="hidden" name="sh_product_id" id="sh_product_id" value="{$subheader/subheader_info/sh_product_id}"/>
		<input type="hidden" name="sh_product_name" id="sh_product_name" value="{$subheader/subheader_info/sh_product_name}"/>
		<input type="hidden" name="sh_short_description" id="sh_short_description" value="{$subheader/subheader_info/sh_short_description}"/>
		<input type="hidden" name="sh_color1_description" id="sh_color1_description" value="{$subheader/subheader_info/sh_color1_description}"/>
		<input type="hidden" name="sh_color2_description" id="sh_color2_description" value="{$subheader/subheader_info/sh_color2_description}"/>
		<input type="hidden" name="sh_product_amount" id="sh_product_amount" value="{$subheader/subheader_info/sh_product_amount}"/>
		<input type="hidden" name="sh_discount_amount" id="sh_discount_amount" value="{$subheader/subheader_info/sh_discount_amount}"/>
		<input type="hidden" name="sh_tax" id="sh_tax" value="{$subheader/subheader_info/sh_tax}"/>
		<input type="hidden" name="sh_addon_count" id="sh_addon_count" value="{count($subheader/subheader_addons/subheader_addon)}"/>
		<input type="hidden" name="sh_miles_points" id="sh_miles_points" value="{$subheader/subheader_info/sh_miles_points}"/>
		<input type="hidden" name="sh_miles_points_posted" id="sh_miles_points_posted" value="{$subheader/subheader_info/sh_miles_points_posted}"/>
		<input type="hidden" name="sh_discount_reward_type" id="sh_discount_reward_type" value="{$subheader/subheader_info/sh_discount_reward_type}"/>
		<input type="hidden" name="sh_add_on_amount" id="sh_add_on_amount" value="{$subheader/subheader_info/sh_add_on_amount}"/>
		<input type="hidden" name="sh_delivery_date" id="sh_delivery_date" value="{$subheader/subheader_info/sh_delivery_date}"/>
		<input type="hidden" name="sh_delivery_date_range_end" id="sh_delivery_date_range_end" value="{$subheader/subheader_info/sh_delivery_date_range_end}"/>
    <input type="hidden" name="sh_ship_method_desc" id="sh_ship_method_desc" value="{$subheader/subheader_info/sh_ship_method_desc}"/>
    
		<xsl:for-each select="$subheader/subheader_addons/subheader_addon">
			<input type="hidden" name="sh_add_on_description_{position()}" id="sh_add_on_description_{position()}" value="{sh_add_on_description}"/>
			<input type="hidden" name="sh_add_on_quantity_{position()}" id="sh_add_on_quantity_{position()}" value="{sh_add_on_quantity}"/>
			<input type="hidden" name="sh_add_on_price_{position()}" id="sh_add_on_price_{position()}" value="{sh_add_on_price}"/>
		</xsl:for-each>

		<input type="hidden" name="sh_taxes_count" id="sh_taxes_count" value="{count($subheader/subheader_taxes/subheader_tax)}"/>
		<xsl:for-each select="$subheader/subheader_taxes/subheader_tax">
			<input type="hidden" name="sh_tax_description_{position()}" id="sh_tax_description_{position()}" value="{sh_tax_description}"/>
			<input type="hidden" name="sh_tax_amount_{position()}" id="sh_tax_amount_{position()}" value="{sh_tax_amount}"/>
			<input type="hidden" name="sh_display_order_{position()}" id="sh_display_order_{position()}" value="{sh_display_order}"/>
		</xsl:for-each>

	</xsl:template>
</xsl:stylesheet>
