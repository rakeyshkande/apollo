<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>

<xsl:decimal-format NaN="0.00" name="notANumber"/>

<xsl:variable name="itemComments" select="normalize-space(/root/item/COMMENT/@item)" />
<xsl:variable name="floristComments" select="normalize-space(key('pageData','florist_comments')/value)" />
<xsl:variable name="OK" select="'O'"/>
<xsl:variable name="CONFIRM" select="'C'"/>
<xsl:variable name="originalProductPrice" select="/root/PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT/product_amount"/>
<xsl:variable name="originalProductId" select="/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCTS/product_id"/>
<xsl:variable name="customProductPrice" select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice"/>
<xsl:variable name="customProductId" select="key('pageData','product_id')/value"/>

<!-- Personal Greeting -->
<xsl:variable name="personalGreetingFlag"   select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@personalgreetingflag"/>
<xsl:variable name="personalGreetingId"	    select="/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT/personal_greeting_id"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="no"/>
<xsl:template match="/root">
<html>
<head>
  <title>FTD - Custom Order </title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script language="javascript" src="js/tabIndexMaintenance.js"/>
  <script language="javascript" src="js/FormChek.js"/>
  <script language="javascript" src="js/util.js"/>
  <script language="javascript" src="js/customOrder.js"/>
  <script language="javascript" src="js/commonUtil.js"/>
  <script language="javascript" >
    var containsErrors = <xsl:value-of select="boolean(ERROR_MESSAGES/ERROR_MESSAGE)"/>;
    var fieldErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[FIELDNAME!='']">["<xsl:value-of select="FIELDNAME"/>","<xsl:value-of select="TEXT"/>",parent.validateFormHelper,false]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var okErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[POPUP_INDICATOR=$OK]">["<xsl:value-of select="@id"/>", "<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE"/>" , "<xsl:value-of select="POPUP_CANCEL_PAGE"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var confirmErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[POPUP_INDICATOR=$CONFIRM]">["<xsl:value-of select="@id"/>", "<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE"/>" , "<xsl:value-of select="POPUP_CANCEL_PAGE"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var itemComments = "<xsl:value-of select="$itemComments"/>";
    var floristComments = "<xsl:value-of select="$floristComments"/>";
    var variablePrice = "<xsl:value-of select="/root/item/DETAIL/@variablePrice"/>";
    var variable = "<xsl:value-of select="/root/item/DETAIL/@variable"/>";
    var variablePriceMax = <xsl:value-of select="format-number(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@variablepricemax, '#.00')"/>;
    var variablePriceMin = <xsl:value-of select="format-number(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice, '#.00')"/>;

    var customProductPrice = <xsl:value-of select="format-number(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice, '#.00')"/>;
    var customProductId = "<xsl:value-of select="$customProductId"/>";

    var originalProductPrice = <xsl:value-of select="format-number($originalProductPrice,  	'#0.00','notANumber')"/>;
    var originalProductId = "<xsl:value-of select="$originalProductId"/>";

    var origin = "<xsl:value-of select="/root/PRODUCTS/PRODUCT/marketing_group"/>";
  <!-- Personal Greeting -->
<xsl:if test="$personalGreetingFlag = 'N' and $personalGreetingId != '' ">
  okErrors.push([null, "You are changing a Personal Greeting order to a non-Personal Greeting order.  Any personalized greeting created by customer will not be associated with this product.", null, null]);
</xsl:if>

  </script>
  


</head>
<body marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onLoad="init()">

  <iframe id="checkLock" width="0px" height="0px" border="0"/>
  <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>

  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Custom Order'"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="overrideRTQ" select="Y"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
  </xsl:call-template>
  <!-- Customer Product data -->
  <xsl:call-template name="cusProduct">
     <xsl:with-param name="subheader" select="subheader"/>
    <xsl:with-param name="displayContinue" select="false()"/>
    <xsl:with-param name="displayComplete" select="false()"/>
    <xsl:with-param name="displayCancel" select="true()"/>
  </xsl:call-template>

  <form name="coForm" method="post" action="updateCustomOrder.do" onsubmit="return submitForm();">
    <input type="hidden" name="productId" id="productId" >
      <xsl:attribute name="value"><xsl:value-of select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@productid"/></xsl:attribute>
    </input>
    <input type="hidden" name="custom" id="custom" value="Y" />
    <input type="hidden" name="command" id="command" value="updateItem" />

    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- Customer Product params -->
    <xsl:call-template name="cusProduct-params">
      <xsl:with-param name="subheader" select="subheader"/>
    </xsl:call-template>

      

    <!-- New pageData fields  -->
    <input type="hidden" name="action_type" id="action_type" value=""/>
    <input type="hidden" name="product_images" id="product_images" value="{key('pageData','product_images')/value}"/>
    <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
    <input type="hidden" name="comment_id" id="comment_id" value="{key('pageData','comment_id')/value}"/>
    <input type="hidden" name="category_index" id="category_index" value="{key('pageData','category_index')/value}"/>
    <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>
    <input type="hidden" name="recipient_city" id="recipient_city" value="{key('pageData','recipient_city')/value}"/>
    <input type="hidden" name="company_id" id="company_id" value="{key('pageData','company_id')/value}"/>
    <input type="hidden" name="recipient_country" id="recipient_country" value="{key('pageData', 'recipient_country')/value}"/>
    <input type="hidden" name="item_order_number" id="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData', 'external_order_number')/value}"/>
    <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="occasion" id="occasion" value="{key('pageData', 'occasion')/value}"/>
    <input type="hidden" name="occasion_description" id="occasion_description" value="{key('pageData', 'occasion_description')/value}"/>
    <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="page_name" id="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="page_number" id="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{key('pageData', 'recipient_zip_code')/value}"/>
    <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}"/>
    <input type="hidden" name="recipient_state" id="recipient_state" value="{key('pageData', 'recipient_state')/value}"/>
    <input type="hidden" name="domestic_international_flag" id="domestic_international_flag" value="{key('pageData', 'domestic_international_flag')/value}"/>
    <input type="hidden" name="domestic_srvc_fee" id="domestic_srvc_fee" value="{key('pageData', 'domestic_srvc_fee')/value}"/>
    <input type="hidden" name="international_srvc_fee" id="international_srvc_fee" value="{key('pageData', 'international_srvc_fee')/value}"/>
    <input type="hidden" name="line_number" id="line_number" value="{key('pageData', 'line_number')/value}"/>
    <input type="hidden" name="partner_id" id="partner_id" value="{key('pageData','partner_id')/value}"/>
    <input type="hidden" name="pricing_code" id="pricing_code" value="{key('pageData','pricing_code')/value}"/>
    <input type="hidden" name="snh_id" id="snh_id" value="{key('pageData','snh_id')/value}"/>
    <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
    <input type="hidden" name="product_id" id="product_id" value="{key('pageData','product_id')/value}"/>
    <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData','orig_product_id')/value}"/>
    <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData','orig_product_amount')/value}"/>
    <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
    <input type="hidden" name="product_amount" id="product_amount" value=""/>
    <input type="hidden" name="delivery_date" id="delivery_date" value="{key('pageData','delivery_date')/value}"/>
    <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
    <input type="hidden" name="reward_type" id="reward_type" value="{key('pageData','reward_type')/value}"/>
    <input type="hidden" name="ship_method" id="ship_method" value="{key('pageData', 'ship_method')/value}"/>
    <input type="hidden" name="personal_greeting_id" id="personal_greeting_id" value="{/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT/personal_greeting_id}"/>
    <input type="hidden" name="personal_greeting_flag" id="personal_greeting_flag" value="{/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@personalgreetingflag}"/>

    <center>
      <table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
        <tr>
          <td>
            <button type="button" class="BlueButton" onclick="javascript:doUpdateDeliveryInfo()">Update Delivery Information</button>
            &nbsp;&nbsp;
            <button type="button" class="BlueButton" onclick="javascript:doUpdateProductCategory()">Update Product Category</button>
          </td>
        </tr>
      </table>
        <div id="mainContent">
        <!-- Main table -->
        <table width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
          <tr>
            <td>
              <div id="customOrder" style="overflow:auto; width:100%; height:300; padding:0px; margin:0px">        
                <xsl:choose>
                  <xsl:when test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@status = 'U'">
                    <table align="center" width="75%" height="25%" border="0" cellspacing="2" cellpadding="2">
                      <tr>
                        <td align="center" class="waitMessage">
                          The product is currently unavailable. Please select another product.
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" align="right">
                          <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                        </td>
                      </tr>
                    </table>
                  </xsl:when>
                  <xsl:otherwise>
                    <table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="labelright"> Price: &nbsp; </td>
                        <td align="left">
                          <input type="radio" checked="true" onclick="checkVariablePrice(value);" name="PRICE" id="radPrice" value="standard" />
                            $<xsl:value-of select="format-number($customProductPrice, '#,###,##0.00')"/>
                        </td>
                        <td/>
                      </tr>
      
                      <!-- variable price -->
                      <tr>
                        <td/>
                        <td align="left">
                          <input type="radio" onclick="checkVariablePrice(value);" name="PRICE" id="radVariable" value="variable"/>Variable Price
                          &nbsp;&nbsp;
                            $<input type="text" onselect="resetRadio(value,'variable');"  onkeypress="resetRadio(value,'variable');" onclick="resetRadio(value,'variable');" name="VARIABLE_PRICE_AMT" id="variablePriceAmt" size="8" maxlength="8" class="tblText">
                              <xsl:choose>
                                <xsl:when test="number($originalProductPrice > 0)" >
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="format-number($originalProductPrice, '#.00')"/>
                                  </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="format-number(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice, '#.00')"/>
                                  </xsl:attribute>
                                </xsl:otherwise>
                              </xsl:choose>
                            </input>
                        </td>
                        <td/>
                      </tr>
                      <tr>
                        <td width="20%" class="labelright" valign="top"> Custom Order Description: &nbsp; </td>
                        <td width="30%">
                          <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="95%" valign="top">
                                <TEXTAREA name="florist_comments" id="florist_comments" rows="3" cols="80" onkeyup="javascript:checkFloristCommentsSize();">
                                  <xsl:choose>
                                    <xsl:when test="$floristComments != ''">
                                      <xsl:value-of select="$floristComments"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@productname"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </TEXTAREA>
                              </td>
                              <td width="5%" valign="top">
                                &nbsp; <SPAN style="color:red"> *** </SPAN>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="50%">&nbsp;  </td>
                      </tr>
                      <tr>
                        <td> &nbsp; <span style="color:red"> *** - required field</span> </td>
                        <td colspan="2" align="right">
                          <button type="button" id="continueButton" class="BlueButton" onkeydown="javascript:onKeyDown()" onclick="javascript:submitForm(); ">Continue</button>
                        </td>
                      </tr>
                    </table>
                  </xsl:otherwise>
                </xsl:choose>  
              </div>
            </td>
          </tr>
        </table>

        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>

          </tr>
        </table>

        <xsl:call-template name="footer"/>
      </div>

      <div id="waitDiv" style="display:none">
        <table id="waitTable" class="mainTable" width="98%" border="0" cellpadding="0" cellspacing="2" height="100%">
          <tr>
            <td>
              <table border="0" class="innerTable" cellpadding="0" width="100%" height="100%" cellspacing='0'>
                <tr>
                  <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                  <td id="waitTD"  width="50%" class="waitMessage"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <xsl:call-template name="footer"/>
      </div>

    </center>
  </form>

  <div id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <br/><br/><p align="center"> <font face="Verdana">Please wait ... Continue!</font> </p>
  </div>
  <iframe id="checkLock" width="0px" height="0px" border="0"/>
</body>
</html>

</xsl:template>
</xsl:stylesheet>
