<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>
<!-- Keys -->
<xsl:key name="addOn" match="/root/Addons/Addon" use="addon_id"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<!-- Variables -->
<xsl:variable name="searchType" select="key('pageData','search_type')/value"/>
<xsl:variable name="requesteddeliverydate" select="key('pageData','delivery_date')/value"/>
<xsl:variable name="rewardType" select="key('pageData','reward_type')/value"/>
<xsl:variable name="origProductId" select="key('pageData','orig_product_id')/value"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
  <title>FTD - Upsell Product</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script language="javascript" src="js/tabIndexMaintenance.js"/>
  <script language="javascript" src="js/FormChek.js"/>
  <script language="javascript" src="js/util.js"/>
  <script language="javascript" src="js/commonUtil.js"/>
  <script type="text/javascript" src="js/upsell.js"/>
  <style type="text/css">
	img{cursor:hand;}
  </style>
  <script type="text/javascript" language="javascript">
  /*
   *  Global variables
   */
    var novator_id = "<xsl:value-of select="PRODUCTRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT/@novatorid"/>";
    var product_images = "<xsl:value-of select="key('pageData', 'product_images')/value"/>";
<![CDATA[

]]>
  </script>


  <!-- O L D  S T U F F -->
  <script language="javascript"><![CDATA[
    var disableFlag = false;
    var backupFlag = false;]]>

    var searchType = "<xsl:value-of select="$searchType"/>";

  /*
   *  Initialization
   */
    function init(){
   	  setNavigationHandlers();
      disableFlag = false;
      setScrollingDivHeight();
      setTabIndexes(tabsArray, 10);

      var item = document.getElementById("product_id");
      if ( item != null ) {
        item.focus();
      }

      <xsl:choose>
        <xsl:when test="key('pageData', 'jcp_flag')/value = 'JP' and key('pageData', 'countryType')/value = 'I'">
          openJCPPopup();
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="key('pageData', 'jcp_flag')/value = 'JP' and PRODUCTSEARCHRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT/@jcpcategory != 'A'">
                openJcpFoodItem();
            </xsl:when>
            <xsl:otherwise>
              <xsl:if test="key('pageData', 'displaySpecGiftPopup')/value = 'Y'">
                //display the Yes/No Floral DIV Tag
                openNoFloral();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayNoProductPopup')/value = 'Y'">
                //display the No Products DIV Tag
                openNoProduct();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayFloristPopup')/value = 'Y'">
                //display the No Products DIV Tag
                openOnlyFloral();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayCodifiedSpecial')/value = 'Y'">
                // display the Codified Special DIV Tag
                openCodifiedSpecial();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayProductUnavailable')/value = 'Y'">
                // display the Product Unavailable DIV Tag
                openProductUnavailable();
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>

      //Display an alert when the page refreshes or reloads if the recipient is in Alaska or Hawaii
      <xsl:if test="key('pageData', 'displaySpecialFee')/value = 'Y'">
        alertHIorAK();
      </xsl:if><![CDATA[
    }


    function validateForm(){
      size = document.all.price.length;
      checked = true;]]>

      <xsl:if test="extraInfo/subtypes/product"><![CDATA[
        document.forms[0].subCodeId.className="TblText";
        if (document.forms[0].subCodeId.value == ""){
          document.forms[0].subCodeId.className="Error";
          document.forms[0].subCodeId.focus();
          checked = false;
        }]]>
      </xsl:if>

      <xsl:if test="PRODUCTRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT/@color1 !=''"><![CDATA[
        document.forms[0].color1.className="TblText";
        if (document.forms[0].color1.value == ""){
          document.forms[0].color1.className="Error";
          if (checked != false){
            document.forms[0].color1.focus();
            checked = false;
          }
        }]]>
      </xsl:if>

      <xsl:if test="PRODUCTRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT/@color2 !=''"><![CDATA[
        document.forms[0].color2.className="TblText";
        if (document.forms[0].color2.value == ""){
          document.forms[0].color2.className="Error";
          if (checked != false){
            document.forms[0].color2.focus();
            checked = false;
          }
        }]]>
      </xsl:if><![CDATA[

      return checked;
     }]]>
  </script>
</head>

<body onload="javascript:init();">

 <!-- Header Template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Upsell Product'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="overrideRTQ" select="Y"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>

  <!-- Content div -->
  <div id="mainContent" style="display:block">

  <!-- Customer Product data -->
   <xsl:call-template name="cusProduct">
      <xsl:with-param name="subheader" select="subheader"/>
     <xsl:with-param name="displayContinue" select="true()"/>
     <xsl:with-param name="displayComplete" select="false()"/>
     <xsl:with-param name="displayCancel" select="true()"/>
   </xsl:call-template>
 
  <form name="upsellForm" id="form" method="post" action="loadProductDetail.do">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- Customer Product params -->
    <xsl:call-template name="cusProduct-params">
      <xsl:with-param name="subheader" select="subheader"/>
    </xsl:call-template>

    <input type="hidden" name="displayCodifiedSpecial" id="displayCodifiedSpecial" value="{key('pageData', 'displayCodifiedSpecial')/value}"/>
    <input type="hidden" name="list_flag" id="list_flag" value="{key('pageData', 'list_flag')/value}"/>
    <input type="hidden" name="displaySpecialFee" id="displaySpecialFee" value="{key('pageData', 'displaySpecialFee')/value}"/>
    <input type="hidden" name="upsell_id" id="upsell_id" value="{key('pageData', 'upsell_id')/value}"/>
    <input type="hidden" name="domestic_flag" id="domestic_flag" value="{key('pageData', 'domestic_flag')/value}"/>
    <input type="hidden" name="script_code" id="script_code" value="{key('pageData', 'script_code')/value}"/>
    <input type="hidden" name="search_type" id="search_type" value="{key('pageData', 'search_type')/value}"/>
    <input type="hidden" name="total_pages" id="total_pages" value="{key('pageData', 'total_pages')/value}"/>
    <input type="hidden" name="upsell_id" id="upsell_id" value="{key('pageData', 'product_id')/value}"/>
    <input type="hidden" name="jcp_flag" id="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
    <input type="hidden" name="orig_bear" id="orig_bear" value="{key('addOn', 'B')/addon_id}"/>
    <input type="hidden" name="orig_bear_quantity" id="orig_bear_quantity" value="{key('addOn', 'B')/addon_qty}"/>
    <input type="hidden" name="orig_baloon" id="orig_baloon" value="{key('addOn', 'A')/addon_id}"/>
    <input type="hidden" name="orig_baloon_quantity" id="orig_baloon_quantity" value="{key('addOn', 'A')/addon_qty}"/>
    <input type="hidden" name="orig_chocolate" id="orig_chocolate" value="{key('addOn', 'C')/addon_id}"/>
    <input type="hidden" name="orig_chocolate_quantity" id="orig_chocolate_quantity" value="{key('addOn', 'C')/addon_qty}"/>
    <input type="hidden" name="orig_funeral_banner" id="orig_funeral_banner" value="{key('addOn', 'F')/addon_id}"/>
    <input type="hidden" name="orig_funeral_banner_quantity" id="orig_funeral_banner_quantity" value="{key('addOn', 'F')/addon_qty}"/>
    <input type="hidden" name="orig_greeting_card" id="orig_greeting_card" value="{Addons/Addon[starts-with(addon_id, 'RC')]/addon_id}"/>
    <input type="hidden" name="upsell_flag" id="upsell_flag" value="Y"/>
    <input type="hidden" name="source" id="source" value=""/>

    <!-- new fields for modify order -->
    <input type="hidden" name="action_type" id="action_type" value=""/>
    <input type="hidden" name="product_images" id="product_images" value="{key('pageData','product_images')/value}"/>
    <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
    <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData', 'orig_product_id')/value}"/>
    <input type="hidden" name="category_index" id="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>
    <input type="hidden" name="recipient_city" id="recipient_city" value="{key('pageData','recipient_city')/value}"/>
   	<input type="hidden" name="company_id" id="company_id" value="{key('pageData','company_id')/value}"/>
    <input type="hidden" name="recipient_country" id="recipient_country" value="{key('pageData', 'recipient_country')/value}"/>
    <input type="hidden" name="item_order_number" id="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData', 'external_order_number')/value}"/>
    <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="occasion" id="occasion" value="{key('pageData', 'occasion')/value}"/>
    <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="page_name" id="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="page_number" id="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{key('pageData', 'recipient_zip_code')/value}"/>
    <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}"/>
    <input type="hidden" name="recipient_state" id="recipient_state" value="{key('pageData', 'recipient_state')/value}"/>
    <input type="hidden" name="domestic_international_flag" id="domestic_international_flag" value="{key('pageData', 'domestic_international_flag')/value}"/>
    <input type="hidden" name="domestic_srvc_fee" id="domestic_srvc_fee" value="{key('pageData', 'domestic_srvc_fee')/value}"/>
   	<input type="hidden" name="international_srvc_fee" id="international_srvc_fee" value="{key('pageData', 'international_srvc_fee')/value}"/>
   	<input type="hidden" name="line_number" id="line_number" value="{key('pageData', 'line_number')/value}"/>
    <input type="hidden" name="occasion_description" id="occasion_description" value="{key('pageData', 'occasion_description')/value}"/>
    <input type="hidden" name="partner_id" id="partner_id" value="{key('pageData','partner_id')/value}"/>
    <input type="hidden" name="pricing_code" id="pricing_code" value="{key('pageData','pricing_code')/value}"/>
   	<input type="hidden" name="snh_id" id="snh_id" value="{key('pageData','snh_id')/value}"/>
    <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
    <input type="hidden" name="delivery_date" id="delivery_date" value="{key('pageData','delivery_date')/value}"/>
    <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
    <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
    <input type="hidden" name="product_amount" id="product_amount" value="{key('pageData','product_amount')/value}"/>
    <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData','orig_product_amount')/value}"/>
    <input type="hidden" name="reward_type" id="reward_type" value="{key('pageData','reward_type')/value}"/>
    <input type="hidden" name="ship_method" id="ship_method" value="{key('pageData', 'ship_method')/value}"/>

        <table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
      <tr>
        <td>
          <button type="button" class="BlueButton" onclick="javascript:doUpdateDeliveryInfo()">Update Delivery Information</button>
          &nbsp;&nbsp;
          <button type="button" class="BlueButton" onclick="javascript:doUpdateProductCategory()">Update Product Category</button>
        </td>
      </tr>
    </table>
    <!-- Main table-->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <!-- Content div
          <div id="mainContent" style="display:block"> -->
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td>
                  <!-- Scrolling div contiains product detail -->
                  <div id="upsellDetail" style="overflow:auto; width:100%; padding:0px; margin:0px;">
                    <!-- Product Information or Products unavialable message -->
                    <xsl:choose>
                      <xsl:when test="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name = 'masterStatus']/@value = 'Y'">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <script language="JavaScript">
                                // Exception display trigger variables
                                var exceptionMessageTrigger = false;
                                var selectedDate = new Date("<xsl:value-of select="$requesteddeliverydate"/>");
                                var exceptionStart = new Date("<xsl:value-of select="PRODUCTRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/>");
                                var exceptionEnd = new Date("<xsl:value-of select="PRODUCTRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/>");<![CDATA[
                                document.write("&nbsp; &nbsp; <img  border=\"0\" ");
                                document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
                                document.write("if (imag != 'npi_1.gif'){ ");
                                document.write("this.src='" + product_images + "npi_1.gif'}\" ");
                                document.write("src='" + product_images + novator_id + "_1.gif'  " + "onclick=viewLargeImage();" + "  /> ");]]>
                              </script>
                            </td>

                            <td colspan="3" valign="top">
															<span class="label">
																<xsl:value-of select="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='masterName']/@value"/>
																&nbsp; &nbsp; #<xsl:value-of select="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='masterId']/@value"/>
															</span>
															<br/>
															<span class="description">
																<xsl:value-of select="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='masterdescription']/@value"  disable-output-escaping="yes"/>
                              </span>

															<xsl:if test="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name = 'noneAvailable']/@value = 'Y' or PRODUCTSEARCHRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='baseAvailable']/@value = 'N'">
																<br/><br/>
																<span class="tblText"><font color="red">
																	None of the products are currently available for this delivery location.</font>
																</span>
															</xsl:if>
														</td>
                          </tr>
                          <tr>
                            <td colspan="3">
                            &nbsp; &nbsp;
                            <a tabindex="-1" href="#" onclick="javascript:viewLargeImage();">View Larger Image</a>
                            </td>
                          </tr>

                          <tr>
                            <td height="10" colspan="6"></td>
                          </tr>

                          <xsl:variable name="rewardType" select="$rewardType"/>
                          <xsl:variable name="finalSequence" select="count(PRODUCTRESULTS/UPSELLS/UPSELL)"/>
                          <xsl:variable name="finalProductName" select="PRODUCTSEARCHRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT[@upsellsequence = $finalSequence]/@upsellproductname"/>
                          <!-- List of products -->
                          <xsl:for-each select="PRODUCTRESULTS/UPSELLS/UPSELL/PRODUCTLIST/PRODUCTS/PRODUCT">
                            <tr>
                              <xsl:variable name="upsellSequence" select="@upsellsequence"/>
                              <td id="productRow_{$upsellSequence}" valign="top" colspan="4">

                              <xsl:if test="@status = 'U' or @specialunavailable = 'Y' or PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='baseAvailable']/@value = 'N'">
                                <script language="JavaScript">
                                  var productSeq = "<xsl:value-of select="@upsellsequence"/>";<![CDATA[
                                  document.getElementById("productRow_" + productSeq).style.backgroundColor = '#CCCCCC';]]>
                                </script>
                              </xsl:if>

                              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                                <tr>
                                  <td height="1" colspan="6" class="tblheader"></td>
                                </tr>

                                <xsl:if test="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='baseAvailable']/@value = 'Y' and @upsellsequence != $finalSequence and PRODUCTSEARCHRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='showScripting']/@value = 'Y'">
                                  <xsl:if test="@status = 'U' or @specialunavailable = 'Y'">
                                    <tr>
                                      <td colspan="6" class="ScreenPrompt">
                                        <xsl:value-of select="upsellExtraList/scripting/script[@fieldname='BASE_NOT_AVAIL_1']/@scripttext"/>
                                        <xsl:value-of select="@upsellproductname"/>
                                        <xsl:value-of select="upsellExtraList/scripting/script[@fieldname='BASE_NOT_AVAIL_2']/@scripttext"/>
                                        <xsl:value-of select="$finalProductName"/>
                                        <xsl:value-of select="upsellExtraList/scripting/script[@fieldname='BASE_NOT_AVAIL_3']/@scripttext"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </xsl:if>

                                <tr>
                                  <td width="15%" class="tblText" align="left" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td>
                                          <xsl:choose>
                                            <xsl:when test="@status='U' or @specialunavailable='Y' or PRODUCTSEARCHRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='baseAvailable']/@value='N'">
                                              <input type="radio" class="tblText" name="product_id" id="product_id" disabled="true" tabindex="{position()}"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <input type="radio" class="tblText" name="product_id" id="product_id" value="{@productid}" tabindex="1">
																								<xsl:if test="position()=1 or @productid = $origProductId">
																									<xsl:attribute name="CHECKED"/>
																								</xsl:if>
                                              </input>
                                           </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                        <td>
                                          &nbsp; &nbsp;
                                          <xsl:choose>
                                            <xsl:when test="@standarddiscountprice > 0 and @standarddiscountprice != @standardprice">
                                              <span style="color='red'">
                                                <strike>$<xsl:value-of select="@standardprice"/></strike>&nbsp;
                                              </span>
                                              $<xsl:value-of select="@standarddiscountprice"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              $<xsl:value-of select="@standardprice"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                          <xsl:if test="@standardrewardvalue > 0">
                                            <xsl:if test="$rewardType != 'P' and $rewardType != 'D'">
                                               &nbsp; (<xsl:value-of select="@standardrewardvalue"/>)
                                            </xsl:if>
                                          </xsl:if>
                                           &nbsp; &nbsp;
                                        </td>
                                      </tr>
                                    </table>
                                  </td>

                                  <td width="85%" colspan="2" class="tblText">
                                    <span class="label"> <xsl:value-of select="@upsellproductname" />&nbsp;&nbsp;&nbsp;&nbsp; #<xsl:value-of select="@productid"/> </span> <br/>
                                      <xsl:value-of select="@longdescription"  disable-output-escaping="yes"/>
                                      <br/>
                                      <xsl:value-of select="@arrangementsize"/>
                                  </td>
                                </tr>

                                <tr>
                                  <td></td>
                                  <td width="15%" class="label" valign="top">Delivery Method:</td>
                                  <td>We can deliver this product as early as
                                    <xsl:value-of select="@deliverydate"/>
                                    <xsl:if test="@shipmethodflorist = 'Y'">
                                      &nbsp;using an FTD Florist.
                                      <xsl:if test="@shipmethodcarrier = 'Y'">
                                        Or it can be delivered
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="@shipmethodcarrier = 'Y'">
                                      &nbsp;using <xsl:value-of select="@carrier"/>.
                                    </xsl:if>
                                  </td>
                                </tr>

                                <xsl:if test="@discountpercent > 0">
                                  <tr>
                                    <td class="TblTextBold" align="left">
                                      Discount: </td>
                                    <td class="tblText" align="left"> <xsl:value-of select="@discountpercent"/>% </td>
                                  </tr>
                                </xsl:if>

                                <xsl:if test="@status = 'U' or @specialunavailable = 'Y'">
                                  <xsl:if test="PRODUCTRESULTS/UPSELLMASTER/UPSELLDETAIL[@name='baseAvailable']/@value = 'Y'">
                                    <script language="JavaScript"><![CDATA[
                                      document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for the selected delivery location.<![CDATA[</font></td></tr>");]]>
                                    </script>
                                  </xsl:if>
                                </xsl:if>

                                <xsl:choose>
                                  <xsl:when test="$requesteddeliverydate">
                                    <script language="JavaScript">
                                      exceptionStart = new Date("<xsl:value-of select="@exceptionstartdate"/>");
                                      exceptionEnd = new Date("<xsl:value-of select="@exceptionenddate"/>");
                                    </script>
                                    <xsl:if test="@exceptioncode = 'U'">
                                      <script language="JavaScript"><![CDATA[
                                        if (selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
                                          exceptionMessageTrigger = true;
                                          document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionenddate"/><![CDATA[</font></td></tr>");
                                        }]]>
                                      </script>
                                    </xsl:if>

                                    <xsl:if test="@exceptioncode = 'A'">
                                      <script language="JavaScript"><![CDATA[
                                        if (selectedDate < exceptionStart || selectedDate > exceptionEnd) {
                                          exceptionMessageTrigger = true;
                                          document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionenddate"/><![CDATA[</font></td></tr>");
                                        }]]>
                                      </script>
                                    </xsl:if>

                                    <script language="JavaScript"><![CDATA[
                                      if (exceptionMessageTrigger == true)
                                        document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="@exceptionmessage"/><![CDATA[</font></td></tr>");
                                      else
                                        document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="@exceptionmessage"/><![CDATA[</font></td></tr>");]]>
                                    </script>
                                  </xsl:when>

                                  <xsl:otherwise>
                                    <xsl:if test="@exceptioncode = 'A'">
                                      <tr>
                                        <td></td>
                                        <td colspan="3" class="tblText" align="left"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionenddate"/></font> </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="@exceptioncode = 'U'">
                                      <tr>
                                        <td></td>
                                        <td colspan="3" class="tblText" align="left"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionenddate"/></font> </td>
                                      </tr>
                                    </xsl:if>

                                    <tr>
                                      <td></td>
                                      <td colspan="3" class="tblText" align="left"><font color="red"><xsl:value-of select="@exceptionmessage"/></font></td>
                                    </tr>
                                  </xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="@displaycablefee = 'Y'">
                                  <tr>
                                    <td colspan="2" class="tblText" align="left"> <font color="red">A $12.99 cable fee will  be added to deliver this item to an International country</font> </td>
                                  </tr>
                                </xsl:if>

                              </table>
                            </td>
                          </tr>
                          </xsl:for-each>
                        </table>
                      </xsl:when>

                      <!-- Product not avaialable -->
                      <xsl:otherwise>
                        <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="waitMessage"><center>This product is unavailable.</center></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                      </xsl:otherwise>
                    </xsl:choose>
                  </div>
                  <!-- End scrolling div -->

                </td>
              </tr>
            </table>

        </td>
      </tr>
    </table>
    <!-- End Main Table -->

      <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="90%" align="center">
          </td>
          <td width="10%" >
            <button type="button" tabindex="-1" accesskey="t" class="BigBlueButton" style="width:110px;" onclick="javascript:doContinueUpdateAction();">Con(t)inue Update<br/>Product</button>
          </td>
        </tr>
        <tr>
          <td width="90%" align="center">
          </td>
          <td width="10%" >
            <button type="button" tabindex="-1" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
          </td>
        </tr>
      </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

  </form>

  </div>
  <!-- End Content div -->

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <xsl:call-template name="footer"/>
  </div>


  <!-- Large image popup -->
  <div id="showLargeImage" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <center>
    <br/><br/>
    <script language="JavaScript"><![CDATA[
      document.write("<img ")
      document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
      document.write("if (imag!='npi_2.jpg'){ ")
      document.write("this.src='" + product_images + "npi_2.jpg'}\" ")
      document.write("src='" + product_images + novator_id + ".jpg'/> ");]]>
    </script>
    <br/><br/>
    <a href="#" onclick="javascript:closeLargeImage();">close</a>
    </center>
  </div>

  <!--No Floral DIV -->
  <div id="noFloral" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table class="innerTable">
            <tr>
              <td align="center">Floral Items can no longer be delivered to this zip/postal code area.
                <br/><br/>
                  Would you be interested in our specialty items?
              </td>
            </tr>
            <tr>
              <td align="center">
                <!--<img src="../images/button_no.gif" onclick="javascript:closeNoFloral()"/>-->
                <button type="button" class="BlueButton" onclick="javascript:cancelItem()">Yes</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                 <button type="button" class="BlueButton" onclick="javascript:closeNoFloral()">&nbsp;No&nbsp;</button>
               <!-- <img src="../images/button_yes.gif" onclick="javascript:cancelItem()"/>-->
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--No Product DIV -->
  <div id="noProduct" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                No Items can be delivered to this Zip/Postal Code at this time.
                <br/><br/>
                Please enter a new Zip/Postal Code or select <U>Occasion</U>&nbsp; to start over.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <!--<img  src="../images/button_ok.gif" onclick="javascript:noProduct()"/>-->
                <button type="button" class="BlueButton" onclick="javascript:noProduct()">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Product Unavailable DIV -->
  <div id="productUnavailable" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                This item cannot be delivered to this Zip/Postal Code at this time.
                <br/><br/>
                Please enter a new Zip/Postal Code or select another item to order.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
              <!--  <img src="../images/button_ok.gif" onclick="javascript:closeProductUnavailable();"/>-->
              <button type="button" class="BlueButton" onclick="javascript:closeProductUnavailable();">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--JCPenney Food DIV -->
  <div id="jcpFood" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                We're sorry, food products are unavailable for purchase using a JCPenney credit card.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
             <!--   <img  src="../images/button_ok.gif" onclick="javascript:closeJcpFoodItem()"/>-->
             	<button type="button" class="BlueButton" onclick="javascript:closeJcpFoodItem()">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Floral only DIV -->
  <div id="onlyFloral" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                Only Floral items can be delivered to this Zip/Postal Code.
                <br/><br/>
                Would you be interested in selecting a floral item?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
               <!-- <img src="../images/button_no.gif" onclick="javascript:closeOnlyFloral()"/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <img src="../images/button_yes.gif" onclick="javascript:cancelItem()"/>-->
                <button type="button" class="BlueButton" onclick="javascript:cancelItem()" >Yes</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="BlueButton" onclick="javascript:closeOnlyFloral()" >&nbsp;No&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Codified Special DIV -->
  <div id="codifiedSpecial" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF;">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center" >
                We're sorry the product you've selected is currently not available in the
                zip code you entered.  However, the majority of our flowers can be delivered
                to this zip code.
                <br/><br/>
                If this is an incorrect zip code, please re-enter the zip code
                below and "Click to Accept".  Otherwise, please click the "Back" button to
                return to shopping.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
               <!-- <img src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"/>-->
               <button type="button" class="BlueButton" onclick="javascript:codifiedSpecial()">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- JCP Popup DIV -->
  <div id="JCPPop" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF;">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                At this time, JC Penney accepts orders going to or coming from the 50 United States
                only.  We can process your Order using any major credit card through FTD.com.
                <br/><br/>
                Would you like to proceed?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
               <!-- <img src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>-->

                &nbsp;&nbsp;&nbsp;&nbsp;
               <!-- <img src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>-->
               <button type="button" class="BlueButton" onclick="javascript:JCPPopChoice('Y')" >Yes</button>

               <button type="button" class="BlueButton" onclick="javascript:JCPPopChoice('N')">&nbsp;No&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
 <iframe id="checkLock" width="0px" height="0px" border="0"/>
 <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>
</body>
</html>

</xsl:template>
</xsl:stylesheet>
