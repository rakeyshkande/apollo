
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:output method="html" indent="yes"/>

<xsl:template match="/">
<html>
	<head>
		<META http-equiv="Content-Type" content="text/html"/>
		<base target="_self"/>
		<title>Credit Card</title>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
		<script type="text/javascript" src="js/FormChek.js"></script>
		<script type="text/javascript" src="js/clock.js"></script>
		<script type="text/javascript" src="js/util.js"></script>
		<script type="text/javascript" language="JavaScript">
<![CDATA[

/***********************************************************************************
* back
* go back to credit card declined page
************************************************************************************/
function back(){
   var url = "ccDecline.do";
   performAction(url);
}

/***********************************************************************************
* save
* Validate the entered date and close page
************************************************************************************/
function save(){

   if(validate()){
      var authValue = "";
      var aafesTicket = "";
      var type = "";
      if(document.forms[0].pay_type.value != "MS" ){
         type = "auth";
         authValue = document.forms[0].auth_code.value;
      }else{
         type = "aafes";
         authValue = document.forms[0].auth_code.value;
         aafesTicket = document.forms[0].aafes_ticket.value;

      }

   //build array to return
   var returnArray= new Array()
   returnArray["page_action"]="save";
   returnArray["auth_code"]=authValue;
   returnArray["aafes_code"]=aafesTicket;
   returnArray["type"] = type;
   top.returnValue = returnArray;

    top.close();

   }
}

/***********************************************************************************
* validate
* contains page validation
************************************************************************************/
function validate()
{
   var authField = document.forms[0].auth_code;
   var paymentType = document.forms[0].pay_type.value;
   var aafesField = document.forms[0].aafes_ticket;

   var valid = true;

   //An authorization number must be entered
    if (authField == null || authField.value.length == 0  )
      {
         alert("Authorization Code Required.");

         authField.focus();
         authField.style.backgroundColor='pink';
         valid = false;
      }
   if(paymentType == "MS")
   {
      //An aafes number must be entered for military star cards
      if ((paymentType != null || paymentType == 'MS') && (aafesField == null || aafesField.value.length == 0))
      {
         alert("AAFES Ticket Required.");
         aafesField.focus();
         aafesField.style.backgroundColor='pink';
         valid = false;
      }
      else
      {
   	     if(!isInteger(aafesField.value))
       	 {
            alert("AAFES Ticket Must be Numeric.");
            aafesField.focus();
            aafesField.style.backgroundColor='pink';
            valid = false;
   	     }
      }
   }
  return valid;
}

/***********************************************************************************
* performAction
* forward to a page
************************************************************************************/
function performAction(url)
{
  document.forms[0].action = url;
  document.forms[0].submit();
}

/***********************************************************************************
* checkKey
* Check the keypress
************************************************************************************/
function checkKey()
{
	if(window.event)
		if(window.event.keyCode)
			if(window.event.keyCode != 13)
				return false;
	save();	
}

       ]]></script>
			</head>
			<body>
				<form onkeypress='checkKey()'>
					<input type="hidden" name="pay_type" id="pay_type" value="{root/payment_type}" ></input>
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<!-- Header-->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName" select="''" />
						<xsl:with-param name="dnisNumber" select="$call_dnis"/>
						<xsl:with-param name="brandName" select="$call_brand_name" />
						<xsl:with-param name="indicator" select="$call_type_flag" />
						<xsl:with-param name="cservNumber" select="$call_cs_number" />
						<xsl:with-param name="showBackButton" select="false()"/>
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- Display table-->
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
								   <xsl:choose>
								      <xsl:when test="root/payment_type != 'MS'">
                        <tr>
                           <td class="TopHeading" align="center">
                               Enter the Authorization Code:
                               <input type="text" size="10" name="auth_code" id="auth_code" maxlength="6"></input>
                            </td>
                        </tr>
									    </xsl:when>
                      <xsl:otherwise>
                        <tr>
                           <td class="TopHeading" align="center">
                               Enter the Authorization Code:
                               <input type="text" size="10" name="auth_code" id="auth_code" maxlength="6"></input>
                            </td>
                        </tr>
									       <tr>
                             <td class="TopHeading" align="center">
                                 AAFES Ticket Number:
                                 <input type="text" size="14" name="aafes_ticket" id="aafes_ticket" maxlength="14"></input>
                             </td>
						              </tr>
									    </xsl:otherwise>
						   	   </xsl:choose>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<div align="center">
								     <button type="button" class="BlueButton" accesskey="S" onclick="save()">(S)ave</button>
								</div>
							</td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" >
						<tr>
							<td>
								<div align="right">
								     <button type="button" class="BlueButton" accesskey="B" onclick="back()">(B)ack</button>
								</div>
							</td>
						</tr>
					</table>

					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>