<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="cusProduct">
   <xsl:param name="subheader"/>
   <xsl:param name="displayContinue"/>
   <xsl:param name="displayComplete"/>
   <xsl:param name="displayCancel"/>
   <table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
      <tr>
         <td>
            <div style="float:left;">
               <span class="Label">Current Customer is: </span><xsl:value-of select="$subheader/subheader_info/sh_customer_first_name"/>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_customer_last_name"/><br/>
               <span class="Label">Order #: </span><xsl:value-of select="$subheader/subheader_info/sh_external_order_number"/><br/>
               <span class="Label">Item #: </span>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_product_id"/> - <xsl:value-of select="$subheader/subheader_info/sh_product_name"/>
               <xsl:if test="string-length($subheader/subheader_info/sh_color1_description) &gt; 0"><span class="Label"><br/>1st Color Choice:</span>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_color1_description"/>&nbsp;&nbsp;</xsl:if>
               <xsl:if test="string-length($subheader/subheader_info/sh_color2_description) &gt; 0"><span class="Label">2nd Color Choice:</span>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_color2_description"/></xsl:if>

               <br/><span class="Label">Merch Price:</span>&nbsp;$&nbsp;
               <xsl:choose>
                 <xsl:when test="$subheader/subheader_info/sh_discount_amount &gt; 0">
                   <strike style="color:red"><xsl:value-of select="format-number($subheader/subheader_info/sh_product_amount, '#,###,##0.00')"/></strike>&nbsp;<xsl:value-of select="format-number($subheader/subheader_info/sh_product_amount - $subheader/subheader_info/sh_discount_amount, '#,###,##0.00')"/>
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:value-of select="format-number($subheader/subheader_info/sh_product_amount, '#,###,##0.00')"/>
                 </xsl:otherwise>
               </xsl:choose>

               <xsl:if test="$subheader/subheader_info/sh_miles_points > 0">&nbsp;(<xsl:value-of select="$subheader/subheader_info/sh_miles_points"/>&nbsp;<xsl:value-of select="$subheader/subheader_info/sh_discount_reward_type"/>)</xsl:if>

               <xsl:if test="$subheader/subheader_info/sh_tax != '' and number($subheader/subheader_info/sh_tax) != 0"><br/><span class="Label">Taxes</span>&nbsp;$&nbsp;<xsl:value-of select="format-number($subheader/subheader_info/sh_tax, '#,###,##0.00')"/></xsl:if>

               <xsl:for-each select="$subheader/subheader_addons/subheader_addon">
                 <xsl:if test="position() mod 2 > 0"><br/></xsl:if>
                 <xsl:value-of select="sh_add_on_description"/>(<xsl:value-of select="sh_add_on_quantity"/>) $<xsl:value-of select="format-number(sh_add_on_price * sh_add_on_quantity, '#,###,##0.00')"/>&nbsp;&nbsp;&nbsp;
               </xsl:for-each>
            </div>
            <!-- Top Buttons -->
            <div style="margin-left:0px;margin-top:0px;align:center">
               <div style="width:35%;float:right;">
                  <div style="text-align:right;padding-bottom:.5em;">
                    <xsl:if test="$displayContinue">
                       <button type="button" id="custContinue" class="BigBlueButton" style="width:110px;" accesskey="t" onclick="javascript:doContinueUpdateAction();">Con(t)inue Update<br/>Product</button>
                    </xsl:if>
                  </div>
                  <div style="text-align:right;">
                     <span style="padding-right:2em;"><xsl:if test="$displayComplete"><button type="button" id="custComplete" style="width:110px;" class="BigBlueButton" accesskey="p" onclick="javascript:doCompleteUpdateAction();">Com(p)lete Update</button></xsl:if></span>
                    <xsl:if test="$displayCancel"> <button type="button" id="custCancel" class="BigBlueButton" style="width:110px;" accesskey="C" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button></xsl:if>
                  </div>
               </div>
            </div>
         </td>
      </tr>
   </table>
</xsl:template>

<xsl:template name="cusProduct-params">
   <xsl:param name="subheader"/>
   <input type="hidden" name="sh_customer_first_name" id="sh_customer_first_name" value="{$subheader/subheader_info/sh_customer_first_name}"/>
   <input type="hidden" name="sh_customer_last_name" id="sh_customer_last_name" value="{$subheader/subheader_info/sh_customer_last_name}"/>
   <input type="hidden" name="sh_external_order_number" id="sh_external_order_number" value="{$subheader/subheader_info/sh_external_order_number}"/>
   <input type="hidden" name="sh_product_id" id="sh_product_id" value="{$subheader/subheader_info/sh_product_id}"/>
   <input type="hidden" name="sh_product_name" id="sh_product_name" value="{$subheader/subheader_info/sh_product_name}"/>
   <input type="hidden" name="sh_short_description" id="sh_short_description" value="{$subheader/subheader_info/sh_short_description}"/>
   <input type="hidden" name="sh_color1_description" id="sh_color1_description" value="{$subheader/subheader_info/sh_color1_description}"/>
   <input type="hidden" name="sh_color2_description" id="sh_color2_description" value="{$subheader/subheader_info/sh_color2_description}"/>
   <input type="hidden" name="sh_product_amount" id="sh_product_amount" value="{$subheader/subheader_info/sh_product_amount}"/>
   <input type="hidden" name="sh_discount_amount" id="sh_discount_amount" value="{$subheader/subheader_info/sh_discount_amount}"/>
   <input type="hidden" name="sh_tax" id="sh_tax" value="{$subheader/subheader_info/sh_tax}"/>
   <input type="hidden" name="sh_addon_count" id="sh_addon_count" value="{count($subheader/subheader_addons/subheader_addon)}"/>
   <input type="hidden" name="sh_miles_points" id="sh_miles_points" value="{$subheader/subheader_info/sh_miles_points}"/>
   <input type="hidden" name="sh_miles_points_posted" id="sh_miles_points_posted" value="{$subheader/subheader_info/sh_miles_points_posted}"/>
   <input type="hidden" name="sh_discount_reward_type" id="sh_discount_reward_type" value="{$subheader/subheader_info/sh_discount_reward_type}"/>
   <xsl:for-each select="$subheader/subheader_addons/subheader_addon">
     <input type="hidden" name="sh_add_on_description_{position()}" id="sh_add_on_description_{position()}" value="{sh_add_on_description}"/>
     <input type="hidden" name="sh_add_on_quantity_{position()}" id="sh_add_on_quantity_{position()}" value="{sh_add_on_quantity}"/>
     <input type="hidden" name="sh_add_on_price_{position()}" id="sh_add_on_price_{position()}" value="{sh_add_on_price}"/>
   </xsl:for-each>
</xsl:template>
</xsl:stylesheet>