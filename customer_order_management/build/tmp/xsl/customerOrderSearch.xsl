<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY reg "&#xAE;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:variable name="SCRUB" select="'S'"/>
<xsl:variable name="PENDING" select="'P'"/>
<xsl:variable name="REMOVED" select="'R'"/>
<xsl:variable name="YES" select="'Y'"/>
<xsl:variable name="INDICATOR" select="/root/SC_SEARCH_CUSTOMERS/SC_SEARCH_CUSTOMER[position() = 1]/scrub_status"/>

<!-- Bollean modes -->
<xsl:variable name="SCRUB_MODE" select="boolean($SCRUB = $INDICATOR)"/>
<xsl:variable name="PENDING_MODE" select="boolean($PENDING = $INDICATOR)"/>
<xsl:variable name="REMOVED_MODE" select="boolean($REMOVED = $INDICATOR)"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>


<xsl:template match="/">
<html>
  <head>
     <title>FTD - Customer / Order Search</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>

    <script type="text/javascript" src="js/clock.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/util.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script language="javascript" type="text/javascript">
    var orderStatus = '';
    var sc_mode = '';
    var action = '';
    var sc_confirmation_number = '<xsl:value-of select="$sc_order_number"/>';
    var formattedOrderNumber = '<xsl:value-of select="$sc_formatted_ord_number"/>';
    var scrub_url = '<xsl:value-of select="key('pageData','scrub_url')/value"/>';
    var order_access_denied_message = '<xsl:value-of select="key('pageData','orderAccessDeniedMessage')/value"/>';
    

    <xsl:variable name="popUp">
      <xsl:choose>
      <xsl:when test="$SCRUB_MODE">
        orderStatus = 'SCRUB';
        sc_mode = 'S';
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:when test="$PENDING_MODE">
        orderStatus = 'PENDING';
        sc_mode = 'P';
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:when test="$REMOVED_MODE">
        orderStatus = 'REMOVED';
        sc_mode = 'R';
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    var showPopup = <xsl:value-of select="$popUp"/>;
    
    <xsl:variable name="preferredPartnerPopUp">
      <xsl:choose>
      <xsl:when test="key('pageData', 'repCanAccessOrder')/value = 'N'">
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    var showPreferredPartnerPopUp = <xsl:value-of select="$preferredPartnerPopUp"/>;

    <!-- Privacy Policy -->
    var pdBypassPrivacyPolicyVerification = 'N';
    <xsl:if test="key('pageData', 'bypass_privacy_policy_verification')/value = 'Y'">
      pdBypassPrivacyPolicyVerification = 'Y';
    </xsl:if>

/*************************************************************************************
* Initilization()
**************************************************************************************/
      function init(){
          clearReturnToRefundReview();
          clearBypassComValues();
          setNavigationHandlers();
          addDefaultListeners();
          resetErrorFields();
          document.getElementById('in_order_number').focus();
          document.getElementById('printer').tabindex = 17;
          document.getElementById('backButtonHeader').tabindex = 18;
          localStorage.setItem('isVipPopupShown','N');
          localStorage.setItem('isCustomerVip','N');
          if(showPopup){
            doPopUpAction();
          }
          if(showPreferredPartnerPopUp){
            doPreferredPartnerPopUpAction();
          }          

          //For the first invokation of customerOrderSearch, bypass_privacy_policy_verification in the dataFilter maybe empty.  Thus,
          //we have to rely on the bypass_privacy_policy_verification that is being passed back in the pageData. 
          //Value from dataFilter should be ok for subsequent invokations. 
          if (document.getElementById('bypass_privacy_policy_verification') == null ||
              document.getElementById('bypass_privacy_policy_verification').value == '' ||
              document.getElementById('bypass_privacy_policy_verification').value == 'N')
            document.getElementById('bypass_privacy_policy_verification').value = pdBypassPrivacyPolicyVerification;
          	if ( location.href.indexOf('autosubmit=1') !== -1 ) {
               document.getElementById("searchButton").click();
		  	}
      }

    </script>
    <script type="text/javascript" src="js/comSearch.js"/>
  </head>
  <body id="body" onload="javascript:init();" onkeydown="javascript:checkKey();">

  <xsl:call-template name="addHeader"/>
  <form name="orderSearch" method="post" onkeypress="doCustomerSearchAction(event);" >
  	<iframe id="checkLock" name="checkLock" height="0" width="0" border="0"></iframe>
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- content to be hidden once the search begins -->
    <div id="content" style="display:block">
     <xsl:if test="key('pageData','message_display')/value != ''">
       <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
         <tr>
           <td class="largeErrorMsgTxt">
            <xsl:value-of select="key('pageData','message_display')/value"/>
           </td>
         </tr>
       </table>
       </xsl:if>
       <table class="mainTable" align="center" cellpadding="1" cellspacing="1" width="98%">
          <tr>
          <td><!--Content table-->
            <table border="0" class="innerTable" width="100%" align="center">
              <tr><td colspan="6">&nbsp;</td></tr>
              <tr>
                <td class="labelpad" width="24%" >
                  ** Order Number:
                </td>
                <td >
                  <input type="text" value="{$sc_order_number}" tabindex="1" maxlength="25" id="in_order_number" name="in_order_number"/>
                </td>
                <td width="25%" align="right">
                  <label for="in_cust_ind">
                    <input type="checkbox" tabindex="12" name="in_cust_ind" id="in_cust_ind">
                      <xsl:if test="$sc_cust_ind = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                    </input>
                    Customer
                  </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label for="in_recip_ind">
                    <input type="checkbox" tabindex="13" name="in_recip_ind" id="in_recip_ind">
                      <xsl:if test="$sc_recip_ind = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                    </input>
                    Recipient
                  </label>
                </td>
                <td>
                  <span id="errorSearchType" class="RequiredFieldTxt" style="display:none;text-align:left">
                    &nbsp;&nbsp;Invalid search type.
                  </span>
                </td>

              </tr>
              <tr>
                <td class="labelpad">
                   Last Name:
                </td>
                <td>
                  <input type="text" value="{$sc_last_name}" tabindex="2" maxlength="20" id="in_last_name" name="in_last_name"/>
                </td>
                <td colspan="3" >
                <span id="errorName" class="RequiredFieldTxt" style="display:none;text-align:left">
                    &nbsp;&nbsp;Invalid Format Last Name, First Name
                  </span>
                </td>
              </tr>

              <tr>
                <td class="labelpad">
                  Phone Number:
                </td>
                <td >
                  <input type="text" value="{$sc_phone_number}" tabindex="3" maxlength="20" id="in_phone_number" name="in_phone_number"/>
                </td>
                <td colspan="3">
                  <span id="errorPhone" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  Email Address:
                </td>
                <td >
                  <input type="text" tabindex="4" maxlength="55" id="in_email_address" name="in_email_address" value="{$sc_email_address}"/>
                </td>
                <td colspan="3">
                  <span id="errorEmail" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format xxxxx@xxxx.com
                  </span>
                  <span id="errorEmail2" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid search criteria: Recipient Email
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  Zip Code:
                </td>
                <td>
                  <input type="text" tabindex="5" maxlength="12" id="in_zip_code" name="in_zip_code" value="{$sc_zip_code}"/>
                </td>
                <td colspan="3">
                  <span id="errorZip" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                  <span id="errorZipInter" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format A9AA9A
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Customer Number:
                </td>
                <td >
                  <input type="text" value="{$sc_cust_number}" tabindex="7" maxlength="20" id="in_cust_number" name="in_cust_number"/>
                </td>
                <td colspan="3">
                  <span id="errorCustNumber" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>

                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Tracking Number:
                </td>
                <td>
                  <input type="text" value="{$sc_tracking_number}" tabindex="8" maxlength="100" id="in_tracking_number" name="in_tracking_number"/>
                </td>
                <td colspan="3">
                  <span id="errorTrackingNum" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Rewards Number:
                </td>
                <td>
                  <input type="text" value="{$sc_rewards_number}" tabindex="9" maxlength="55" id="in_rewards_number" name="in_rewards_number"/>
                </td>
                <td colspan="3">
                  <span id="errorRewardNum" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>

              </tr>
              <tr>
                <td class="labelpad">
                  ** Premier Circle Membership:
                </td>
                <td>
                  <input type="text" value="{$sc_pc_membership}" tabindex="10" maxlength="55" id="in_pc_membership" name="in_pc_membership"/>
                </td>
                <td colspan="3">
                  <span id="errorPCNum" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>

              </tr>

				<tr>
					<td class="labelpad"> ** ProFlowers Order Number: </td>
					<td>
						<input type="text" value="{$sc_pro_order_number}" tabindex="11" maxlength="25" id="in_pro_order_number" name="in_pro_order_number" />
					</td>
				</tr>
              
              <tr>
                <td class="labelpad" colspan="3">
                  ** Designates Unique Search Criteria
                </td>
                <td  class="label" align="center">
                  QUICK SEARCH <br/>
                  <button type="button" id="lastRecords" class="bluebutton" tabindex="14" accesskey="L" onclick="javascript:doLastRecordAccessed();">(L)ast Record Accessed</button><br/>
                  OR <br/>
                  <button type="button" id="last50" class="bluebutton"  tabindex="15" onclick="javascript:doLast50RecordsAccessed();">Last (5)0 Records Accessed</button>
                </td>
              </tr>
            </table>
          </td><!--Content table-->
           </tr>
       </table>
      <table align="center" width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr><td colspan = "3">&nbsp;</td></tr>
        <tr>
          <td width="25%"></td>
          <td valign="top" align="center" width="49%">
            <button type="button" id="searchButton" class="bluebutton" accesskey="S" tabindex="16" onclick="javascript:doCustomerSearchAction(event);">(S)earch</button>
            <button type="button" id="clear" class="bluebutton" accesskey="C" tabindex="17" onclick="doClearFields();">(C)lear all Fields</button>
          </td>
          <td width="49%" align="right">
            <button type="button" id="mainMenu" class="bluebutton" accesskey="B" tabindex="18" onclick="javascript:doMainMenuAction();">(B)ack to Main Menu</button>
          </td>
        </tr>
      </table>
    </div>

    <!-- Used to dislay Processing... message to user -->
    <div id="waitDiv" style="display:none">
       <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
          <tr>
            <td width="100%">
              <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                           <td id="waitTD"  width="50%" class="waitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
    </div>
    <!-- call footer template-->
    <xsl:call-template name="footer"/>
    </form>
  </body>
</html>
</xsl:template>
   <!--Actual Call to the Header.xsl page -->
     <xsl:template name="addHeader">
      <xsl:call-template name="header">
        <xsl:with-param name="showPrinter" select="true()"/>
        <xsl:with-param name="showBackButton" select="true()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="headerName" select="'Customer / Order Search'"/>
        <xsl:with-param name="dnisNumber" select="$call_dnis"/>
        <xsl:with-param name="cservNumber" select="$call_cs_number"/>
        <xsl:with-param name="indicator" select="$call_type_flag"/>
        <xsl:with-param name="brandName" select="$call_brand_name"/>
        <xsl:with-param name="backButtonLabel" select="'(B)ack to Main Menu'"/>
        <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
      </xsl:call-template>
    </xsl:template>
</xsl:stylesheet>