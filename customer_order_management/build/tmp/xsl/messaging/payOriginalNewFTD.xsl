
<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
	<xsl:import href="mercMessButtons.xsl"/>
	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessFormatPhone.xsl"/>
	<xsl:import href="mercMessSendButton.xsl"/>
	<xsl:import href="compInfoTemplate.xsl"/>
  <xsl:import href="newFTDInfoTemplate.xsl"/>
  <xsl:key name="security" match="/message/security/data" use="name"/>
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>
<xsl:variable name="xvVendorCost" select="format-number(normalize-space(message/vendorCost), '#,###,##0.00')"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>
					<xsl:value-of select="$xvHeaderName"/>
				</title>

				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
				<meta http-equiv="language" content="en-us"/>

				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
				<script type="text/javascript" src="js/commonUtil.js">;</script>
				<script type="text/javascript" src="js/clock.js">;</script>

				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
				<script><![CDATA[
					var actions = new Array();
                                        var monthArray = new Array("JAN", "FEB", "MAR", "APR", "MAY", "JUN",
                                                                   "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");

					//********************************************************************************
					//Issue 3625 - start
					//********************************************************************************

					//today
					var today = new Date();

					//tomorrow
					var tomorrow = new Date();
					var oneDayInMilliSec = 24*60*60*1000; // 24 hr * 60 min per hr * 60 sec per min * 1000 milli per sec
					tomorrow.setTime(today.getTime() + oneDayInMilliSec);

					//determine timezone and set cutoff
					var todayString = today.toString();
					var todayStringLength = todayString.length;
					var csrTimezone = todayString.substring(todayStringLength-8, todayStringLength-5);
					var csrCutoffHour;
					if (csrTimezone == "EDT" || csrTimezone == "EST")
					{
						csrCutoffHour = 17;
					}
					else
					{
						if (csrTimezone == "CDT" || csrTimezone == "CST")
						{
							csrCutoffHour = 16;
						}
						else
						{
							if (csrTimezone == "MDT" || csrTimezone == "MST")
							{
								csrCutoffHour = 15;
							}
							//for Pacific and ALL other timezones, the cutoff is going to be 1400 hours
							else
							{
								csrCutoffHour = 14;
							}
						}
					}
					//********************************************************************************
					//Issue 3625 - end
					//********************************************************************************

					function init()
					{

						document.oncontextmenu = contextMenuHandler;
						document.onkeydown = backKeyHandler;
						var buttons = document.getElementsByTagName("BUTTON");

						for(var i = 0; i < buttons.length; i++)
						{
							if(buttons[i].id != "mainMenuButton" && buttons[i].id != "sendButton"&& buttons[i].id != "lookupButton"&& buttons[i].id != "autoSelectButton")
							{
								actions[buttons[i].id] = buttons[i].onclick;
								buttons[i].onclick = checkButtons;
							}else if(buttons[i].id == "mainMenuButton"){
								buttons[i].onclick=mainMenuAction;
							}
						}

						if(document.getElementById("filling_florist_code"))
						{
							if(document.getElementById("filling_florist_code").type=="text"){
								document.getElementById("filling_florist_code").focus();
							}
						}
						untabHeader();
]]>

						registerDefaultsForNewDeliveryDate();

	//********************************************************************************
	//Issue 3625 - start
	//********************************************************************************

	//retrieve the orderDeliveryDateEnd parm.  This will show if the date came in as a range
	var orderDeliveryDateEnd = document.getElementById("order_delivery_date_end").value;


	//if the delivery date had a range
	if (orderDeliveryDateEnd != "")
	{
		/***** Start Date *********************************/
		var canMakeCutoffStartDate = checkValidCutoff("delivery_month", "delivery_date", csrCutoffHour, "00", "invalidOrderDeliveryDate", false);

		//if we can't make the cutoff, change the delivery start date
		//note that the server side code is setting delivery date end in PrepareMessageBO.retrieveMessageForCurrentOrder()
		//however, its not accounting for the cutoff time
		if (!canMakeCutoffStartDate)
		{
			//During testing, it was found that if the CSR's machine was set to a date ahead of the server's date,
			//the delivery date that the PrepareMessageBO sends is according to the server's date/time.  Thus, we
			//first have to set the delivery month/date to today's value as per the CSR's clock, check for the cutoff
			//again, and if the problem persists, set it to tomorrow's date
			document.getElementById("delivery_month").value = today.getMonth();
			document.getElementById("delivery_date").value = today.getDate();
			canMakeCutoffStartDate = true;

			canMakeCutoffStartDate = checkValidCutoff("delivery_month", "delivery_date", csrCutoffHour, "00", "invalidOrderDeliveryDate", false);
			if (!canMakeCutoffStartDate)
			{
				document.getElementById("delivery_month").value = tomorrow.getMonth();
				document.getElementById("delivery_date").value = tomorrow.getDate();
			}

		}

		/***** End Date *********************************/
		var canMakeCutoffEndDate = checkValidCutoff("delivery_month_end", "delivery_date_end", csrCutoffHour, "00", "invalidOrderDeliveryDateEnd", false);

		//if we can't make the cutoff, change the delivery end date
		//note that the server side code is setting delivery date end in PrepareMessageBO.retrieveMessageForCurrentOrder()
		//however, its not accounting for the cutoff time
		if (!canMakeCutoffEndDate)
		{
			//During testing, it was found that if the CSR's machine was set to a date ahead of the server's date,
			//the delivery date that the PrepareMessageBO sends is according to the server's date/time.  Thus, we
			//first have to set the delivery month/date to today's value as per the CSR's clock, check for the cutoff
			//again, and if the problem persists, set it to tomorrow's date
			document.getElementById("delivery_month_end").value = today.getMonth();
			document.getElementById("delivery_date_end").value = today.getDate();
			canMakeCutoffEndDate = true;

			canMakeCutoffEndDate = checkValidCutoff("delivery_month_end", "delivery_date_end", csrCutoffHour, "00", "invalidOrderDeliveryDateEnd", false);
			if (!canMakeCutoffEndDate)
			{
				document.getElementById("delivery_month_end").value = tomorrow.getMonth();
				document.getElementById("delivery_date_end").value = tomorrow.getDate();
			}

		}

	}
	//********************************************************************************
	//Issue 3625 - end
	//********************************************************************************


					}

//******************************************************************************************
// registerDefaultsForNewDeliveryDate
//    - chooses the default selections for the delivery date; we use the original
//        delivery date unless it has already passed, in which case, we use today's
//        date.
//
//        Since the "today" element in the XML message does not include the year,
//        as per the business analysts, we will assume the delivery date
//        is later as long as it is either a later date in the same month as today,
//        or a month 1 - 6 months after today (wrapping from Dec to Jan if needed)
//******************************************************************************************
					function registerDefaultsForNewDeliveryDate()
					{
						// extract delivery date information from message
						var origDeliveryMonthString = "<xsl:value-of select="substring(normalize-space(message/deliveryText), 1, 3)"/>";
						var origDeliveryMonthNum = getMonthNumber(origDeliveryMonthString);

						// If CSR notifies us of the below error message, it is because the
						// month string of the delivery date was not one of JAN through DEC
						// in capital letters.
						if (origDeliveryMonthNum == -1)
							 alert("Delivery month may be corrupted; \n" +
									 "please copy order number and this error message\n" +
									 "and alert development team.");

						var origDeliveryDateNum = "<xsl:value-of select="number(substring(normalize-space(message/deliveryText), 5, 2))"/>";

						// extract current date information from message
						var todayMonthString ="<xsl:value-of select="substring(normalize-space(message/today), 1, 3)"/>";
						var todayMonthNum = getMonthNumber(todayMonthString);


						// If CSR notifies us of the below error message, it is because the
						// month string of the "today" field was not one of JAN through DEC
						// in capital letters.
						if (todayMonthNum == -1)
							 alert("Month of today's date may be corrupted; \n" +
									"please copy order number and this error message\n" +
									"and alert development team.");

						var todayDateNum = "<xsl:value-of select="number(substring(normalize-space(message/today), 5, 2))"/>";

						<![CDATA[

						// choose the month and date of the later of the two dates above
						var selectedMonthNum = getMonthNumOfLaterMonth(origDeliveryMonthNum, todayMonthNum);


						var selectedDateNum = getDateNumOfLaterDate(origDeliveryDateNum, todayDateNum,
																				origDeliveryMonthNum, todayMonthNum, selectedMonthNum);


						// DOM code to obtain the month and date selection items and assign
						// them default values
						var monthSelection = document.getElementById("delivery_month");
						monthSelection.selectedIndex = selectedMonthNum;
						var dayInput = document.getElementById("delivery_date");
						dayInput.value = selectedDateNum;
					}


//******************************************************************************************
// getMonthNumber
//    - parameters : monthString - expected input is a string consisting of
//                      a three-letter prefix of one of the twelve months;
//                      all letters should be capital letters. (ex.: AUG)
//    - return value : an integer from -1 through 11, with -1 being an
//       error code
//    - If the parameter string is a legal month string, returns an integer
//        from 0 - 11 representing the parameter month, with 0 corresponding
//        to "JAN" and 11 corresponding to "DEC". If the parameter string is
//        not a legal month string, returns -1.
//******************************************************************************************
					function getMonthNumber(monthString)
					{
						// performs linear search through 12-element array declared
						//  earlier above; for now, simple seems best, but if there
						//  are performance issues later, might need a faster setup
						//  than linear search through an array
						var monthNum = -1;
						for (var i = 0; i <= 11; i++)
						{
							 if (monthArray[i] == monthString)
							 {
									monthNum = i;
									break;
							 }
						}
						return monthNum;
					}




//******************************************************************************************
// getMonthNumOfLaterMonth
//    - parameters : deliveryMonthNum - number from 0-11 corresponding
//                      to the delivery date month
//                 : todayMonthNum - number from 0-11 corresponding
//                      to today''s month
//    - return value : the month number (0 - 11) corresponding to one of
//        the two parameter strings
//    - the original delivery month is considered to be later than
//        today''s month, if it is between 1 and 6 months after today''s
//        month. Returns original delivery month''s number if it is later
//        than today''s month or if the two months are the same; otherwise,
//        returns today''s month''s number.
//******************************************************************************************
					function getMonthNumOfLaterMonth(sDeliveryMonthNum, sTodayMonthNum)
					{

						//convert the variables into a numeric value.
						var deliveryMonthNum	= sDeliveryMonthNum - 0;
						var todayMonthNum			= sTodayMonthNum - 0;

					 var juneMonthNum = getMonthNumber("JUN");
					 var laterMonthNum = deliveryMonthNum;  // default assumption, pending if-stmt below

					 if (deliveryMonthNum != todayMonthNum)  // different months
					 {
							if (todayMonthNum <= juneMonthNum) // a later delivery month is still this year
							{
								 // delivery month NOT between 1 and 6 months after today''s month
								 if ((deliveryMonthNum < todayMonthNum + 1) || (deliveryMonthNum > todayMonthNum + 6))
								 {
										laterMonthNum = todayMonthNum;
								 }
							}
							else  // an earlier delivery month was earlier this year
							{
								 // delivery month between 1 and 5 months before today's month
								 if ((deliveryMonthNum >= todayMonthNum - 5) && (deliveryMonthNum <= todayMonthNum - 1))
								 {
										laterMonthNum = todayMonthNum;
								 }
							}
					 }

					 return laterMonthNum;
					}




//******************************************************************************************
// getDateNumOfOfLaterDate
//    - parameters : deliveryDateNum - integer indicating date of
//                      original delivery
//                 : todayDateNum - integer indicating today's date
//                 : deliveryMonthNum - number from 0-11 corresponding
//                      to the delivery date month
//                 : todayMonthNum - number from 0-11 corresponding
//                      to today's month
//                 : laterMonthNum - integer indicating "later" of two
//                      parameter months, as chosen by getMonthNumOfLaterMonth(...)
//    - return value : the value of either deliveryDateNum or todayDateNum
//    - if the two months are the same, returns the later date; otherwise,
//         returns the date associated with the later month
//******************************************************************************************
					function getDateNumOfLaterDate(sDeliveryDateNum, sTodayDateNum, sDeliveryMonthNum,
																 sTodayMonthNum, sLaterMonthNum)
					{

						//convert the variables into a numeric value.
						var deliveryDateNum		= sDeliveryDateNum	- 0;
						var todayDateNum			=	sTodayDateNum			- 0;
						var deliveryMonthNum	=	sDeliveryMonthNum - 0;
						var todayMonthNum			=	sTodayMonthNum    - 0;
						var laterMonthNum			=	sLaterMonthNum    - 0;


					 var laterDateNum;

					 if (deliveryMonthNum == todayMonthNum)   // dates are in same month
					 {
							if (deliveryDateNum >= todayDateNum)
							{
								 laterDateNum = deliveryDateNum;
							}
							else
							{
								 laterDateNum = todayDateNum;
							}
					 }
					 else  // we want the date for the month we already decided was "later"
					 {
							if (deliveryMonthNum == laterMonthNum)
							{
								 laterDateNum = deliveryDateNum;
							}
							else
							{
								 laterDateNum = todayDateNum;
							}
					 }

					 return laterDateNum;
					}







				function doFloristLookupAction()
				{
					var args = new Array();

					args.action = "floristLookup.do";

					var cty=document.forms[0].recipient_country.value;
					var zip=document.forms[0].recipient_zip.value;
					  if(cty =='US'||cty=='USA'){
					  	if(zip.length>5){
					  	    zip=zip.substring(0,5);
					  	}

					  }else if(cty=='CA'){

					  	if(zip.length>3){
					  	    zip=zip.substring(0,3);
					  	}
					  }

					args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
					args[args.length] = new Array("msg_message_order_number", document.forms[0].msg_message_order_number.value);
					args[args.length] = new Array("msg_message_type", document.forms[0].msg_message_type.value);

					args[args.length] = new Array("recipient_city", document.forms[0].recipient_city.value);
					args[args.length] = new Array("recipient_state", document.forms[0].recipient_state.value);
					args[args.length] = new Array("recipient_zip", zip);

					args[args.length] = new Array("delivery_month", document.forms[0].delivery_month.value);
					args[args.length] = new Array("delivery_date", document.forms[0].delivery_date.value);
					if(document.forms[0].delivery_month_end != null &&  document.forms[0].delivery_month_end != undefined) {
				      args[args.length] = new Array("delivery_month_end", document.forms[0].delivery_month_end.value);
					  args[args.length] = new Array("delivery_date_end", document.forms[0].delivery_date_end.value);
				    }
                   
					args[args.length] = new Array("product_id", document.forms[0].product_id.value);
          args[args.length] = new Array("source_code", document.forms[0].message_source_code.value);
					args[args.length] = new Array("florist_prd_price", document.forms[0].florist_prd_price.value);
					args[args.length] = new Array("comp_order", document.forms[0].comp_order.value);
					args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
					args[args.length] = new Array("context", document.getElementById("context").value);

          var modal_dim = "dialogWidth:750px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";
					var floristInfo = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);

					if(floristInfo != null)
					{
						document.forms[0].filling_florist_code.value = floristInfo[0];
            document.forms[0].florist_status.value = floristInfo[1];
						document.forms[0].mercury_status.value = floristInfo[2];
						doSendNewFTDMessageAction("florist_lookup_send");
					}

				}
				]]>





                                        function validateMessageForm(actionType)
					{
                                                var isValidSecondChoice=true;
						var isValidFirstChoice=true;

                                                if(actionType != "auto_select_send")
                                                {
                                                    var isValidFillingFloristCode=checkFloristCode("filling_florist_code", "invalidFloristCode");
                                                }

						var isValidRecipientName = checkRecipientName("recipient_name", "invalidRecipientName");

						var isValidRecipientStreet = checkRecipientStreet("recipient_street", "invalidRecipientStreet");

						var isValidRecipientCity = checkRecipientCity("recipient_city", "invalidRecipientCityStateZip");

						var isValidRecipientState = checkRecipientState("recipient_state", "invalidRecipientCityStateZip");

						var isValidRecipientZip = checkRecipientZip("recipient_zip", "invalidRecipientCityStateZip");

						var isValidRecipientPhone = checkRecipientPhone("recipient_phone", "invalidRecipientPhone");

	                                        var monthSelect = document.getElementById("delivery_month");

                                                var dateBox = document.getElementById("delivery_date");

                        			var dateField = document.getElementById("order_delivery_date");

                                                dateField.value = monthSelect.options[monthSelect.selectedIndex].value
									+ " "
									+ dateBox.value;


                                                var isValidOrderDeliveryDate = checkOrderDeliveryDate("delivery_month", "delivery_date","invalidOrderDeliveryDate");


	//********************************************************************************
	//Issue 3625 - start
	//********************************************************************************

	//retrieve the orderDeliveryDateEnd parm.  This will show if the date came in as a range
	var orderDeliveryDateEnd = document.getElementById("order_delivery_date_end").value;

	//set the default for these flags.
	var allOk = true;
	var isValidOrderDeliveryDateEnd = true;
	var isDateRangeValid = true;
	var isDateRangeDifValid = true;
	var canMakeCutoff = true;
	var isStartAndEndSame = false;


	//if the delivery date had a range
	if (orderDeliveryDateEnd != "")
	{
		/***validate delivery date end that a CSR may have updated***/
		if (allOk)
			isValidOrderDeliveryDateEnd = checkOrderDeliveryDate("delivery_month_end", "delivery_date_end","invalidOrderDeliveryDateEnd");

		//set the allOk flag that will show if we need to further validate
		if (!isValidOrderDeliveryDateEnd)
			allOk = false;

		/***ensure that the end date is after the start date for the given range***/
		if (allOk)
			isDateRangeValid = checkDateRange("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");

		//set the allOk flag that will show if we need to further validate
		if (!isDateRangeValid)
			allOk = false;

		/***if the end date is after the start date, check the difference in days.  We are only allowing a max of 3 days difference***/
		if (allOk)
			isDateRangeDifValid = checkDateRangeDifference("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end", 3, "invalidOrderDeliveryDateEnd", true);

		//set the allOk flag that will show if we need to further validate
		if (!isDateRangeDifValid)
			allOk = false;

		/***check the cuttoff. if passed the cutoff, set canMakeCutoff to false***/
		if (allOk)
			canMakeCutoff = checkValidCutoff("delivery_month", "delivery_date", csrCutoffHour, "00", "invalidOrderDeliveryDate", true);

		//set the allOk flag that will show if we need to further validate
		if (!canMakeCutoff)
			allOk = false;

		/***check if the start and end date are the same***/
		if (allOk)
			isStartAndEndSame = checkSameStartEndDate("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end");

	}

	//********************************************************************************
	//Issue 3625 - end
	//********************************************************************************



                                                if(actionType != "auto_select_send")
                                                {
						<![CDATA[

	//Issue 3625 - start
		if (	 isValidRecipientName
				&& isValidFillingFloristCode
				&& isValidRecipientStreet
				&& isValidRecipientCity
				&& isValidRecipientZip
				&& isValidRecipientPhone
				&& isValidOrderDeliveryDate
				&& isValidOrderDeliveryDateEnd
				&& isDateRangeValid
				&& isDateRangeDifValid
				&& canMakeCutoff
				&& isStartAndEndSame
			 )
		{
			document.getElementById("delivery_month_end").value = '';
			document.getElementById("delivery_date_end").value = '';
		}
	//Issue 3625 - end

                                                    return (
                                                            isValidRecipientName
                                                            && isValidFillingFloristCode
                                                            && isValidRecipientStreet
                                                            && isValidRecipientCity
                                                            && isValidRecipientZip
                                                            && isValidRecipientPhone
                                                            && isValidOrderDeliveryDate
                                                            && isValidOrderDeliveryDateEnd //issue 3625
                                                            && isDateRangeValid //issue 3625
                                                            && isDateRangeDifValid //issue 3625
                                                            && canMakeCutoff //issue 3625
                                                            );
                                                ]]>
                                                }
                                                else
                                                 {
                                                <![CDATA[
	//Issue 3625 - start
		if (	 isValidRecipientName
				&& isValidRecipientStreet
				&& isValidRecipientCity
				&& isValidRecipientZip
				&& isValidRecipientPhone
				&& isValidOrderDeliveryDate
				&& isValidOrderDeliveryDateEnd
				&& isDateRangeValid
				&& isDateRangeDifValid
				&& canMakeCutoff
				&& isStartAndEndSame
			 )
		{
			document.getElementById("delivery_month_end").value = '';
			document.getElementById("delivery_date_end").value = '';
		}
	//Issue 3625 - end


                                                		return (
                                                                isValidRecipientName
                                                                && isValidRecipientStreet
                                                                && isValidRecipientCity
                                                                && isValidRecipientZip
                                                                && isValidRecipientPhone
                                                                && isValidOrderDeliveryDate
                                                                && isValidOrderDeliveryDateEnd //issue 3625
                                                                && isDateRangeValid //issue 3625
                                                                && isDateRangeDifValid //issue 3625
                                                                && canMakeCutoff //issue 3625
                                                              );
                                                ]]>
                                                 }



					}



					function doNewFTDFormAction(actionType){
                                        <![CDATA[
						if(actionType == "auto_select_send")
                                                {
                                                        var cty=document.forms[0].recipient_country.value;
                                                        var zip=document.forms[0].recipient_zip.value;
                                                          if(cty =='US'||cty=='USA'){
                                                                if(zip.length>5){
                                                                    zip=zip.substring(0,5);
                                                                }

                                                          }else if(cty=='CA'){

                                                                if(zip.length>3){
                                                                    zip=zip.substring(0,3);
                                                                }
                                                          }

                                                        var url = "autoSelectFlorist.do?action="
                                                                                        + actionType
                                                                                        + "&order_detail_id="
                                                                                        + document.forms[0].order_detail_id.value
                                                                                        + "&msg_message_type="
                                                                                        + document.forms[0].msg_message_type.value
                                                                                        + "&call_out="+document.forms[0].call_out.checked
                                                                                        + "&recipient_street="+document.forms[0].recipient_street.value
                                                                                        + "&recipient_city="+document.forms[0].recipient_city.value
                                                                                        + "&recipient_state="+document.forms[0].recipient_state.value
                                                                                        + "&recipient_zip="+ zip
                                                                                        + "&delivery_month="+document.forms[0].delivery_month.value
                                                                                        + "&delivery_date="+document.forms[0].delivery_date.value
                                                                                        + "&product_id="+document.forms[0].product_id.value
                                                                                        + "&source_code="+document.forms[0].message_source_code.value
                                                                                        + "&florist_prd_price="+document.forms[0].florist_prd_price.value
                                                                                        + getDeliveryDateParams()
                                                                                        + getSecurityParams(false);
                                                        //alert(url);
                                                        FloristStatusIFrame.location.href = url;
                                                }
                                                else if(actionType == "florist_lookup_send")
						{
							document.getElementById("invalidFloristCode").style.display = "none";
                                                        var callOut=document.forms[0].call_out.checked;

							if(callOut){
								document.forms[0].action="prepareCallout.do?comp_order=true";
								document.forms[0].submit();
							}else{
								checkStatusIframe();
							}
						}
						else
						{
                                                        document.getElementById("invalidFloristCode").style.display = "none";
                                                        checkStatusIframe();

						}

                                        ]]>
					}



                                function doSendNewFTDMessageAction(actionType)
				{
					    showWaitMessage("content", "wait");
					    var valid=true;
						<xsl:if test="$destination ='' ">
                                                    valid=validateMessageForm(actionType);
                                                </xsl:if>
						if(valid){

							doNewFTDFormAction(actionType);
						}else{

							hideWaitMessage("content", "wait");
						}


				}
				<![CDATA[

					function checkStatusIframe(){

						var url = "checkFloristStatus.do?comp_order=true&filling_florist_code="
								   + document.forms[0].filling_florist_code.value
								   + "&action=send_new_ftd"
									+ "&msg_message_type="
									+ document.forms[0].msg_message_type.value
									+ "&new_msg_message_type="
									+ document.forms[0].new_msg_message_type.value
									+"&call_out="+document.forms[0].call_out.checked
                                                                        + "&order_detail_id="
									+ document.forms[0].order_detail_id.value
									+ getDeliveryDateParams()
									+ getSecurityParams(false);
						//alert(url);
						FloristStatusIFrame.location.href = url;

					}

                                        //***********************************************************************************************
                                        // function getDeliveryDateParams()
                                        //***********************************************************************************************
                                                function getDeliveryDateParams()
                                                {
                                                        var dateUrl	="&delivery_month="+document.forms[0].delivery_month.value
                                                                                                        +"&delivery_date="+document.forms[0].delivery_date.value;

                                        ]]>


                                                        <xsl:if test="$xvOrderDeliveryDateEnd != ''"><![CDATA[
                                                                dateUrl +="&delivery_month_end="+document.forms[0].delivery_month_end.value
                                                                                                +"&delivery_date_end="+document.forms[0].delivery_date_end.value;
                                        ]]>
                                                        </xsl:if>
                                                        return dateUrl;
                                                }



				</script>

				<style>td.label {
        			width: 225px;
        			vertical-align: middle;
        		}</style>
			</head>

			<body onload="init();">
				<form id="payOriginalNewFTDForm" method="post" action="sendFTDMessage.do">
 
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<xsl:call-template name="messagingSecurityAndData"/>

					<input type="hidden" name="order_delivery_date" id="order_delivery_date" value="{$xvOrderDeliveryDate}"/>
					<input type="hidden" name="order_delivery_timestamp" id="order_delivery_timestamp" value="{$xvOrderDeliveryDateTimestamp}"/>
					<input type="hidden" name="order_delivery_date_end" id="order_delivery_date_end" value="{$xvOrderDeliveryDateEnd}"/>

					<input type="hidden" name="order_date_text" id="order_date_text" value="{$xvOrderDate}"/>
					<input type="hidden" name="order_date" id="order_date" value="{$xvOrderDateTimestamp}"/>
					<input type="hidden" name="florist_status" id="florist_status"/>
					<input type="hidden" name="mercury_status" id="mercury_status"/>
					<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
					<input type="hidden" name="occasion_desc" id="occasion_desc" value="{$xvOccasionDescription}"/>
					<input type="hidden" name="product_id" id="product_id" value="{normalize-space(message/productId)}"/>
					<input type="hidden" name="message_source_code" id="message_source_code" value="{normalize-space(message/sourceCode)}"/>

					<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
					<iframe id="checkLock" width="0px" height="0px" border="0"/>
					<iframe id="FloristStatusIFrame" name="FloristStatusIFrame" width="0" height="0" border="0" style="display: none"/>

					<xsl:choose>
						<xsl:when test="$destination ='' ">
							<xsl:call-template name="header">
								<xsl:with-param name="showSearchBox" select="false()"/>
								<xsl:with-param name="showPrinter" select="true()"/>
								<xsl:with-param name="showTime" select="true()"/>
								<xsl:with-param name="headerName" select="$xvHeaderName"/>

								<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
								<xsl:with-param name="indicator" select="$xvIndicator"/>
								<xsl:with-param name="brandName" select="$xvBrandName"/>
								<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
								<xsl:with-param name="showCSRIDs" select="true()"/>
								<xsl:with-param name="showBackButton" select="true()"/>
								<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
								<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="header">
								<xsl:with-param name="showSearchBox" select="false()"/>
								<xsl:with-param name="showPrinter" select="true()"/>
								<xsl:with-param name="showTime" select="true()"/>
								<xsl:with-param name="headerName" select="$xvHeaderName"/>

								<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
								<xsl:with-param name="indicator" select="$xvIndicator"/>
								<xsl:with-param name="brandName" select="$xvBrandName"/>
								<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
								<xsl:with-param name="showCSRIDs" select="true()"/>
								<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>

					<script>updateCurrentUsers();</script>

					<div id="content">
						<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td>
									<table class="PaneSection">
										<tr>
											<td class="banner">Message Detail
												<xsl:if test="$xvMercuryMessageType">
													&nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/></xsl:if>
											</td>
										</tr>
									</table>

									<div style="overflow: auto; height: 329px; padding-right: 16px;">
										<table width="100%" cellspacing="0" cellpadding="0" style="margin-right: -16px;">


                      <xsl:choose>
												<xsl:when test="$comp_order = 'true'">
                          <xsl:call-template name="compInfoTemplate">
                          <xsl:with-param name="destination" select="$destination"/>
                          <xsl:with-param name="new_msg_message_type" select="$new_msg_message_type"/>
                          <xsl:with-param name="msg_message_type" select="$msg_message_type"/>
                          </xsl:call-template>
												</xsl:when>
                        <xsl:otherwise>
                          <xsl:call-template name="newFTDInfoTemplate">
                          <xsl:with-param name="destination" select="$destination"/>
                          <xsl:with-param name="new_msg_message_type" select="$new_msg_message_type"/>
                          <xsl:with-param name="msg_message_type" select="$msg_message_type"/>
                          </xsl:call-template>
                        </xsl:otherwise>
											</xsl:choose>

											<tr>
												<td class="label" style="vertical-align: top;">OPERATOR:</td>

												<td class="datum">
													<xsl:value-of select="$xvOperatorCode"/>
												</td>
												<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
											</tr>
										</table>
										<!-- End of order -->
									</div>
									<!-- End of scrolling div -->
									<table width="100%">
										<tr align="center">
											<td>

												<xsl:choose>
													<xsl:when test="$destination ='' ">
                                                                                                                <button type="button" class="BlueButton" accesskey="U" id="lookupButton" onclick="doFloristLookupAction();" style="margin-right: 10px;">Florist Look(U)p</button>
														<xsl:if test="$comp_order != 'true'">
                                                                                                                    <button type="button" class="BlueButton" accesskey="A" id="autoSelectButton" onclick="checkLockProcessRequest('auto_select_send');" style="margin-right: 10px;">(A)uto Select &amp; Send</button>
                                                                                                                </xsl:if>
                                                                                                                <button type="button" class="BlueButton" accesskey="N" id="sendButton" onclick="checkLockProcessRequest('send_new_ftd');">Se(n)d</button>
													</xsl:when>

													<xsl:otherwise>
														<button type="button" class="BlueButton" accesskey="N" id="sendButton" onclick="checkLockProcessRequest('send_new_ftd');" style="margin-right: 10px;">Se(n)d</button>
														<button type="button" class="BlueButton" id="notSendButton" accesskey="D" onclick="openRecipientOrder();" style="margin-right: 10px;">Not Sen(d)</button>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End of mainTable -->
					</div>

					<div id="waitDiv" style="display: none;">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
											<td id="waitTD" align="left" width="50%" class="WaitMessage"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>


					<xsl:call-template name="mercMessButtons"/>
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->