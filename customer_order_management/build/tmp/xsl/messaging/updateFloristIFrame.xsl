<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="msg_order_type"/>
<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        	forwardAction();

		function forwardAction()
		{
			
			//get error message
			var error = "<xsl:value-of select='error_message'/>";
			
			if((error == null) || (error.length &lt; 1))
			{
				var forwardAction = "<xsl:value-of select='forwarding_action'/>";
				var actionType = "<xsl:value-of select='action_type'/>";
				var floristSelectionLogId = "<xsl:value-of select='/root/florist_selection_log_id'/>";
				var zipCityFlag = "<xsl:value-of select='/root/zip_city_flag'/>";
                                //send new ftd message for auto selected florist
                                if(actionType == 'send_ftd')
                                {
                                    parent.document.forms[0].filling_florist_code.value = "<xsl:value-of select='filling_florist_code'/>";
                                    parent.submitFormAction(floristSelectionLogId, zipCityFlag);
                                }
				//create parent page redirect url
                                else if(!forwardAction.match(/^\w/))
                                {
				    forwardAction=forwardAction.substr(1);
                                    parent.redirectForm(forwardAction);
				}
                                else
                                {
                                    parent.redirectForm(forwardAction);
                                }
			}
			else
			{
				parent.hideWaitMessage("content", "wait");
				alert(error);
			} 
        }
      </script>
    </head>
    <body></body>
  </html>
</xsl:template>

</xsl:stylesheet>