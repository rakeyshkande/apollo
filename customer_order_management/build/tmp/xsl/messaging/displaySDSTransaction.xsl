<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:variable name="message" select="key('pageData','message_display')/value"/>
<xsl:variable name="external_order_number" select="key('pageData','external_order_number')/value"/>
<xsl:variable name="order_guid" select="key('pageData','order_guid')/value"/>
<xsl:variable name="customer_id" select="key('pageData','customer_id')/value"/>
<xsl:output indent="yes" method="html"/>
<xsl:template match="/root">

<html>
	<title>FTD - Cart Payment Information</title>
	<head>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
		<script type="text/javascript">
<![CDATA[

]]>
		</script>
	</head>

	<body>
		<table border="1" align="center" cellpadding="1" cellspacing="1" class="mainTable">
			<tr>
				<td width="200" align="center" colspan="2" class="banner">SDS Transaction Information</td>
			</tr>
		</table>

		<table border="1" align="center" cellpadding="1" cellspacing="1" class="mainTable">
			<tr>
				<td>Id</td>
				<td><xsl:value-of select="SDS_Transaction/id" /></td>
			</tr>

			<tr>
				<td>Venus id</td>
				<td><xsl:value-of select="SDS_Transaction/venus_id" /></td>
			</tr>


			<tr>
				<td width="200" colspan="2" class="banner">Request Information</td>
			</tr>

			<tr>
				<td>Request Sent</td>
				<td><xsl:value-of select="SDS_Transaction/request_string" /></td>
			</tr>

			<tr>
				<td width="200" colspan="2" class="banner">Response Information</td>
			</tr>

			<tr>
				<td>Response Received</td>
				<td><xsl:value-of select="SDS_Transaction/response_string" /></td>
			</tr>

		</table>

		<br/>
	</body>
</html>

</xsl:template>


</xsl:stylesheet>
