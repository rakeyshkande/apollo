<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="xvMercuryOrderNumber" select="normalize-space($msg_message_order_number)"/>
<xsl:variable name="xvOrderDetailId" select="normalize-space($order_detail_id)"/>
<xsl:variable name="xvMessageCategory" select="normalize-space($msg_message_type)"/>
<xsl:variable name="xvMercuryMessageType" select="normalize-space($msg_message_detail_type)"/>
<xsl:variable name="xvOutboundMercuryMessageType" select="normalize-space(message/msg_message_detail_type)"/>

<xsl:variable name="xvLoadMemberDataUrl" select="normalize-space(//load_member_data_url)"/>
<xsl:variable name="xvFtdMessageID" select="normalize-space(message/ftdMessageID)"/>
<xsl:variable name="xvOperatorCode" select="normalize-space(message/operator)"/>


<xsl:variable name="xvStatus" select="normalize-space(message/status)"/>
<xsl:variable name="xvStatusTimestamp" select="normalize-space(message/statusDateText)"/>
<xsl:variable name="xvCombinedReportNumber" select="normalize-space(message/combineRptNum)"/>
<xsl:variable name="xvIsSubsequentMessageAllowed" select="normalize-space(message/isSubsequentMessageAllowed)"/>
<xsl:variable name="xvAdjustmentReasonCode" select="normalize-space(message/adjustmentReasonCode)"/>
<xsl:variable name="xvAdjustmentReasonDescription" select="normalize-space(message/adjustmentReasonDesc)"/>
<xsl:variable name="xvCancelReasonCode" select="normalize-space(message/cancelReasonCode)"/>
<xsl:variable name="xvCancelReasonDescription" select="normalize-space(message/cancelReasonDesc)"/>

<!-- Variables for the order information -->
<xsl:variable name="xvFillingFloristPhone" select="normalize-space(message/floristPhoneNumber)"/>
<xsl:variable name="xvFillingFloristName" select="normalize-space(message/floristName)"/>
<xsl:variable name="xvFillingFloristCode" select="normalize-space(message/fillingFloristCode)"/>
<xsl:variable name="xvSendingFloristCode" select="normalize-space(message/sendingFloristCode)"/>
<xsl:variable name="xvSendingFloristCompanyName" select="normalize-space(message/sendingFloristCompanyName)"/>
<xsl:variable name="xvCalloutPhone" select="normalize-space(message/calloutPhone)"/>
<xsl:variable name="xvIsEfaFTDMFlorist" select="normalize-space(message/isEfaFTDMFlorist)"/>
<xsl:variable name="xvZipCityFlag" select="normalize-space(message/zipCityFlag)"/>

<xsl:variable name="xvRecipientName" select="normalize-space(message/recipientName)"/>
<xsl:variable name="xvRecipientStreet" select="normalize-space(message/recipientStreet)"/>
<xsl:variable name="xvRecipientCityStateZip" select="normalize-space(message/recipientCityStateZip)"/>
<xsl:variable name="xvRecipientCity" select="normalize-space(message/recipientCity)"/>
<xsl:variable name="xvRecipientState" select="normalize-space(message/recipientState)"/>
<xsl:variable name="xvRecipientZip" select="normalize-space(message/recipientZip)"/>
<xsl:variable name="xvRecipientPhone" select="normalize-space(message/recipientPhone)"/>
<xsl:variable name="xvOrderDate" select="normalize-space(message/orderDateText)"/>
<xsl:variable name="xvOrderDateTimestamp" select="normalize-space(message/orderDate)"/>
<xsl:variable name="xvOrderDeliveryDate" select="normalize-space(message/deliveryText)"/>
<xsl:variable name="xvOrderDeliveryDateTimestamp" select="normalize-space(message/deliveryDate)"/>
<xsl:variable name="xvOrderDeliveryDateEnd" select="normalize-space(message/endDeliveryDate)"/>
<xsl:variable name="xvOccasionCode" select="normalize-space(message/occasionCode)"/>
<xsl:variable name="xvOccasionDescription" select="normalize-space(message/occasionDesc)"/>
<xsl:variable name="xvCardMessage" select="normalize-space(message/cardMessage)"/>
<xsl:variable name="xvFirstChoice" select="message/firstChoice"/>
<xsl:variable name="xvSecondChoice" select="message/substitution"/>
<xsl:variable name="xvInstructions" select="normalize-space(message/specialInstruction)"/>
<xsl:variable name="xvPriorityCode" select="normalize-space(message/priority)"/>
<xsl:variable name="xvOldPrice" select="format-number(normalize-space(message/oldPrice), '#,###,##0.00')"/>
<xsl:variable name="xvNewPrice" select="format-number(normalize-space(message/currentPrice), '#,###,##0.00')"/>
<xsl:variable name="xvCostOfGoods" select="format-number(normalize-space(message/cogs), '#0.00')"/>
<xsl:variable name="xvOverUnderCharge" select="format-number(normalize-space(message/overUnderCharge), '#,###,##0.00')"/>
<xsl:variable name="xvMessageComment" select="normalize-space(message/init_reason_text)"/>
<xsl:variable name="xvViewMessageComment" select="normalize-space(message/comments)"/>
<xsl:variable name="xvSubsequentAction" select="normalize-space(message/subsequent_action)"/>
<xsl:variable name="xvShipDate" select="normalize-space(message/shipDate)"/>
<xsl:variable name="xvPreferredPartner" select="normalize-space(message/preferred_partner)"/>
<xsl:variable name="xvOrderAddOns" select="normalize-space(message/orderAddOns)"/>

<!-- Variables for the header -->
<xsl:variable name="xvHeaderName" select="normalize-space(message/page_title)"/>
<xsl:variable name="xvCservNumber" select="normalize-space($call_cs_number)"/>
<xsl:variable name="xvIndicator" select="normalize-space($call_type_flag)"/>
<xsl:variable name="xvDnisNumber" select="normalize-space($call_dnis)"/>
<xsl:variable name="xvBrandName">
	<xsl:variable name="temp" select="normalize-space($call_brand_name)"/>
	<xsl:value-of select="substring($temp, 1, 50)"/>
</xsl:variable>
</xsl:stylesheet>