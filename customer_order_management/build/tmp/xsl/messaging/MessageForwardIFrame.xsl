<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:param name="msg_order_type"><xsl:value-of select="key('security','msg_order_type')/value" /></xsl:param>
<xsl:param name="msg_order_status"><xsl:value-of select="key('security','msg_order_status')/value" /></xsl:param>
<xsl:param name="msg_ftd_message_cancelled"><xsl:value-of select="key('security','msg_ftd_message_cancelled')/value" /></xsl:param>
<xsl:param name="cdisp_cancelledOrder"><xsl:value-of select="key('security','cdisp_cancelledOrder')/value" /></xsl:param>
<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        forwardAction();
	
		function forwardAction()
		{
			//get error message
			var error = "<xsl:value-of select='/root/error_message'/>";
      
      //get comp order error message
      var compError = "<xsl:value-of select='/root/comp_error_message'/>";
			      	
			if (compError.length > 0)
      {
        parent.hideWaitMessage("content", "wait");
        parent.document.getElementById("approval_id").style.backgroundColor='pink';
        parent.document.getElementById("approval_passwd").style.backgroundColor='pink';
        alert(compError);
        parent.document.getElementById("approval_id").focus();
      }
      else if((error == null) || (error.length &lt; 1))
			{
				var forwardAction = "<xsl:value-of select='/root/forwarding_action'/>";
				var subsequentAction = "<xsl:value-of select='/root/subsequent_action'/>";
				var orderType = "<xsl:value-of select='$msg_order_type'/>";
				var orderStatus = "<xsl:value-of select='$msg_order_status'/>";
				var cancelled = "<xsl:value-of select='$msg_ftd_message_cancelled'/>";
				var cdispCancelled = "<xsl:value-of select='$cdisp_cancelledOrder'/>";
				//alert(forwardAction);
				if(!forwardAction.match(/^\w/)){
				   
				    forwardAction=forwardAction.substr(1);
				}
				//alert(forwardAction);
				parent.resetFormActionAndSubmit(forwardAction, subsequentAction, orderType, orderStatus, cancelled, cdispCancelled);
			}
      else
			{
				parent.hideWaitMessage("content", "wait");
				
				alert(error);
			} 
        }
      </script>
    </head>
    <body></body>
  </html>
</xsl:template>

</xsl:stylesheet>