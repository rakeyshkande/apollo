<!DOCTYPE stylesheet [
	<!ENTITY  nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="mercMessFormatDate.xsl"/>
<xsl:key name="security" match="/queues/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:variable name="xvErrorMessage" select="normalize-space(queues/ErrorMessage)"/>
<xsl:variable name="xvDeleteAttempted" select="normalize-space(queues/DeleteAttempted)"/>
<xsl:variable name="xvUnableDeletedQueues" select="normalize-space(queues/unableDeleteQueues)"/>
<xsl:template match="/">

	<html>
		<head>
			<title>Queue Delete</title>

			<base target="_self"/>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<link rel="stylesheet" type="text/css" href="css/qDelete.css"/>
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script>
				var shouldReload = "<xsl:value-of select='$xvDeleteAttempted'/>";

				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;
					
					window.name = "qDelete"
				}
				
				function exit()
				{
					window.returnValue = shouldReload;
				}
			</script>
		</head>

		<body onload="init();" onunload="exit();" style="margin: 0; padding: 0;">
			<form name="qDeleteForm" id="qDeleteForm" method="post">

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<xsl:if test="count(queues/queue) != 0">
					<input type="hidden" name="order_detail_id" id="order_detail_id" value="{queues/queue[1]/order_detail_id}"/>
				</xsl:if> 

				<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1" style="border: 0;">
					<tr>
						<td>
							<div class="banner" style="margin-bottom: 5px; margin-top: 1px;">
								ITEMS IN QUEUE
							</div>

							<div class="floatleft">
								<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
							</div>

							<div class="RequiredFieldTxt" id="qDeleteErrorField">
								<xsl:value-of select="$xvErrorMessage"/>
							</div>

							<br/>

							<div class="floatright" style="margin-bottom: 5px;">
								<button type="button" class="BlueButton" tabindex="2400" accesskey="C" onclick="closeQDelete();">
									(C)lose
								</button>
							</div>
							
							<div style="clear: both; padding-right: 16px; background: #006699;">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr class="ColHeader">
										<th class="selected">
											&nbsp;
										</th>

										<th class="number">
											&nbsp;
										</th>

										<th class="date">
											Date
										</th>

										<th class="time">
											Time
										</th>

										<th class="type">
											Type
										</th>

										<th class="tagged">
											Tagged
										</th>

										<th class="taggedDate">
											Tagged Date
										</th>

										<th class="taggedTime">
											Tagged Time
										</th>

										<th class="priority">
											Priority
										</th>
									</tr>
								</table> <!-- End of table headings. -->
							</div>

							<div style="overflow: auto; height: 113px; border: 1px solid #006699; padding-right: 16px;">
								<table cellpadding="0" cellspacing="2" style="width: 100%; margin-right: -16px;">
									<xsl:for-each select="queues/queue">
                     <xsl:variable name="isTagged" select="is_tag_expired"/>
									        <xsl:variable name="xvqueueId" select="concat(normalize-space(message_id),'|',normalize-space(csr_id))"/>
											<tr>
												<xsl:if test="contains($xvUnableDeletedQueues,$xvqueueId)">
												 <xsl:attribute name="bgcolor">yellow</xsl:attribute>
												</xsl:if>
																					
										
											<td class="selected">
												<input type="checkbox" name="messageCheckboxes" id="messageCheckboxes" value="{$xvqueueId}" tabIndex="{position()}"/>
											</td>

											<td class="number">
												<xsl:if test="normalize-space(line_item_number) != ''">
													<xsl:value-of select="normalize-space(line_item_number)"/>.
												</xsl:if>
											</td>

											<xsl:variable name="createdOnTimestamp" select="normalize-space(created_on)"/>

											<td class="date">
												<xsl:call-template name="extractDate">
													<xsl:with-param name="timestamp" select="$createdOnTimestamp"/>
												</xsl:call-template>
											</td>

											<td class="time">
												<xsl:call-template name="extractTime">
													<xsl:with-param name="timestamp" select="$createdOnTimestamp"/>
												</xsl:call-template>
											</td>

											<xsl:variable name="messageType" select="normalize-space(message_type)"/>
											<td class="type">
												<xsl:value-of select="$messageType"/>
											</td>

											<xsl:variable name="taggedUser" select="normalize-space(csr_id)"/>
											<td class="tagged">
												<xsl:value-of select="substring($taggedUser, 1, 12)"/>
											</td>

											<xsl:variable name="taggedOnTimestamp" select="normalize-space(tagged_on)"/>

											<td class="taggedDate">
												<xsl:call-template name="extractDate">
													<xsl:with-param name="timestamp" select="$taggedOnTimestamp"/>
												</xsl:call-template>
											</td>

											<td class="taggedTime">
												<xsl:call-template name="extractTime">
													<xsl:with-param name="timestamp" select="$taggedOnTimestamp"/>
												</xsl:call-template>
											</td>
                      <xsl:choose>
                         <xsl:when test="$isTagged = 'Y'">
                             <td class="priority" style="background-color:#FFFF00;">
											   	      <xsl:value-of select="normalize-space(priority)"/>
											       </td>
                         </xsl:when>
                         <xsl:otherwise>
                             <td class="priority">
											   	      <xsl:value-of select="normalize-space(priority)"/>
											       </td>
                         </xsl:otherwise>
                      </xsl:choose>
											  
										</tr>
									</xsl:for-each>
								</table>
							</div> <!-- End of scrolling div for table -->
       				<xsl:if test="count(queues/queue) != 0">
                <table class="PaneSection" cellspacing="0" cellpadding="0" style="margin-top: 5px;">
                  <tr>
                    <td style="width: 33%;">&nbsp;</td>
                    <td style="width: 33%;">
                      <button type="button" id="qDeleteButton" class="BlueButton" tabIndex="2000" accesskey="Q" style="margin-right: 5px;" onclick="doQDeleteAction();">
                        (Q)Delete
                      </button>
  
                      <button type="button" class="BlueButton" accesskey="A" style="margin-left: 5px;" tabIndex="2100" onclick="checkAll();">
                        QDelete(A)ll
                      </button>
                    </td>
  
                    <td style="width: 33%; text-align: right;">
                      <button type="button" class="BlueButton" accesskey="C" onclick="closeQDelete();" tabIndex="2200">
                        (C)lose
                      </button>
                    </td>
                  </tr>
                </table>
              </xsl:if>
						</td>
					</tr>
				</table> <!-- End of mainTable -->
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet>