<!DOCTYPE stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="formattingTemplates.xsl"/>
    <xsl:key name="security" match="/message/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>

<xsl:template match="/">

	<html>
		<head>
			<title><xsl:value-of select="$xvHeaderName"/></title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/clock.js">;</script>
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script>
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;

					untabHeader();
				}
			</script>

			<style>
        		td.label {
        			width: 225px;
        		}
			</style>
		</head>

		<body onload="init();">
			<form id="viewSystemAdjMessageForm" name="viewSystemAdjMessageForm" method="post">

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>
				
				<script>updateCurrentUsers();</script>

				<div id="content">
					<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td>
								<table class="PaneSection">
									<tr>
										<td class="banner">
											Message Detail
											<xsl:if test="$xvMercuryMessageType">
												&nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/>
											</xsl:if>
										</td>
									</tr>
								</table>

								<div style="overflow: auto; height: 355px; padding-right: 16px;">
									<table width="100%" cellpadding="0" cellspacing="0" style="margin-right: -16px;">
										<tr>
											<td class="label">
												MERCURY ID:
											</td>
					
											<td class="datum">
												<span style="float: left;">
													<xsl:value-of select="$xvMercuryOrderNumber"/>
												</span>
					
												<span style="float: right; padding-right: 3px;">
													<xsl:value-of select="$xvStatus"/>
														&nbsp;
													<xsl:value-of select="$xvStatusTimestamp"/>
												</span>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												COMBINED REPORT NO:
											</td>
						
											<td class="datum">
												<xsl:value-of select="$xvCombinedReportNumber"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												FILLER CODE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvFillingFloristCode"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												SENDER CODE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvSendingFloristCode"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												ORDER DATE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvOrderDate"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												RECIPIENT:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvRecipientName"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												STREET ADDRESS APT:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvRecipientStreet"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												CITY, STATE, ZIP:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvRecipientCityStateZip"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												DELIVERY DATE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvOrderDeliveryDate"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												TEXT OR REASON:
											</td>
					
											<td class="datum">
												<textarea rows="7" cols="50" readOnly="true">
                          <xsl:call-template name="replace">
                            <xsl:with-param name="string" select="$xvViewMessageComment"/>
                          </xsl:call-template>
                      </textarea>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												OLD MERC VALUE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvNewPrice"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												NEW MERC VALUE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvOverUnderCharge"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												REASON CODE:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvAdjustmentReasonDescription"/>
											</td>
										</tr>
					
										<tr>
											<td class="label">
												OPERATOR:
											</td>
					
											<td class="datum">
												<xsl:value-of select="$xvOperatorCode"/>
											</td>
										</tr>
									</table> <!-- End of order -->
								</div> <!-- End of scrolling div -->
							</td>
						</tr>
					</table> <!-- End of mainTable -->
				</div>

				<div id="waitDiv" style="display: none;">
					<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">                      
									<tr>
										<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
										<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>

				<xsl:call-template name="mercMessButtons"/>
				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet>
