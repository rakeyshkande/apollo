<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="msg_order_type"/>
<xsl:param name="msg_order_status"/>
<xsl:param name="msg_ftd_message_cancelled"/>
<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        	forwardAction();
	
		function forwardAction()
		{
			//get error message
			//alert("in check lock ");
			var error = "<xsl:value-of select='/root/error_message'/>";
				
			if((error == null) || (error.length &lt; 1))
			{
								
				parent.processRequest('<xsl:value-of select="actionType"/>','<xsl:value-of select="florist_selection_log_id"/>','<xsl:value-of select="zip_city_flag"/>');
			}
			else
			{
				parent.hideWaitMessage("content", "wait");
				
				alert(error);
			} 
        }
      </script>
    </head>
    <body></body>
  </html>
</xsl:template>

</xsl:stylesheet>