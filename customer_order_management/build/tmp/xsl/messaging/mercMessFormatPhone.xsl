<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
						 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
						 indent="yes"/>


<xsl:template name="mercMessFormatPhone">
<xsl:param name="phoneNumber"/>

	<xsl:value-of select="substring($phoneNumber, 1, 3)"/>
	<xsl:text>/</xsl:text>
	<xsl:value-of select="substring($phoneNumber, 4, 3)"/>
	<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($phoneNumber, 7)"/>

</xsl:template>

</xsl:stylesheet>