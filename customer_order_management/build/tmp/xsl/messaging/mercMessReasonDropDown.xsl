<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
						 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
						 indent="yes"/>


<xsl:template name="mercMessReasonDropDown">

	<select id="reason_codes" name="reason_code">
		<option value="0" selected="selected">Select A Reason Code</option>

		<xsl:for-each select="message/AdjustmentCodes/AdjustmentCode | message/CancelCodes/CancelCode">
			<option value="{normalize-space(@name)}"><xsl:value-of select="normalize-space(@value)"/></option>
		</xsl:for-each>

	</select>

</xsl:template>

</xsl:stylesheet>