
<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessFormatPhone.xsl"/>
        <xsl:key name="security" match="/root/security/data" use="name"/>

	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>

	<xsl:template match="/">

		<html>
		    <script>
function showPopup(mylink) {
    //Modal_Arguments
    var modalArguments = new Object();

    //Modal_Features
    var modalFeatures = 'dialogWidth:600px; dialogHeight:400px; resizable:yes; scroll:yes';

    window.showModalDialog(mylink, modalArguments, modalFeatures);
    //return false;
}
		    
		    </script>
			<head>
				<title>Order Lifecycle Display</title>

				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
				<meta http-equiv="language" content="en-us"/>

				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
    			<link rel="stylesheet" type="text/css" href="css/tooltip.css"/>
				<script type="text/javascript" src="js/commonUtil.js">;</script>
				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			</head>

			<body style="margin: 0; padding: 0;">

                        <table class="mainTable" width="98%" align="center" cellpadding="3" cellspacing="0" style="border: 0;">
                            <tr>
                                <td>
                                    <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
                                </td>
                                <td colspan="2" valign="center" class="Header" align="center">
                                    Order Lifecycle Display
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="33%" align="left" class="bannerLeft">Timestamp</td>
                                <td width="33%" align="left" class="bannerLeft">Status</td>
                                <td width="33%" align="left" class="bannerLeft">Notes</td>
                            </tr>
                            <xsl:for-each select="lifecycleList/lifecycle">
                                <tr>
                                    <td align="left">
                                        <xsl:value-of select="normalize-space(status_timestamp)"/>
                                    </td>
                                    <td align="left">
                                        <xsl:choose>
                                            <xsl:when test="status_url != ''">
                                                <xsl:choose>
                                                    <xsl:when test="status_url_active = 'Y'">
                                                        <a class="tooltipLS" id="lifecycleLink" href="javascript:showPopup('{status_url}');">
                                                            <xsl:value-of select="normalize-space(order_status)"/>
                                                        </a>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <div class="tooltipLS">
                                                            <xsl:value-of select="normalize-space(order_status)"/>
                                                            <span><xsl:value-of select="normalize-space(status_url_expired_msg)"/></span>
                                                        </div>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="normalize-space(order_status)"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </td>
                                    <td align="left">
                                        <xsl:value-of select="normalize-space(status_txt)"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </table>
			</body>
		</html>
	</xsl:template>


</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->