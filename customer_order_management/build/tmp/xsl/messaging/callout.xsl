<!DOCTYPE stylesheet [
	<!ENTITY  nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="mercMessFormatPhone.xsl"/>

<xsl:key name="security" match="/message/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>

<xsl:variable name="xvOrderNumber" select="normalize-space(message/externalOrderNumber)"/>
<xsl:variable name="xvCsrName" select="normalize-space(message/operatorFirstName)"/>
<xsl:param name="ftd_message_id"><xsl:value-of select="key('security','ftd_message_id')/value" /></xsl:param>

<xsl:template match="/">

	<html>
		<head>
			<title>Call Out Script</title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>

			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script>
			<![CDATA[
				var actions = new Array();
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;

					// On this page, all the footer buttons need to show a confirmation dialog
					// before they take the user away from this page. Here we re-wire the onclick
					// event for all those buttons, except the Main Menu button, which already
					// presents a confirmation dialog. We also exclude the "Save" button.
					var buttons = document.getElementsByTagName("BUTTON");
					for(var i = 0; i < buttons.length; i++)
					{
						if(buttons[i].id != "mainMenuButton" && buttons[i].id != "saveButton")
						{
							actions[buttons[i].id] = buttons[i].onclick;
							buttons[i].onclick = checkCalloutButtons;
						}else if(buttons[i].id == "mainMenuButton"){
							buttons[i].onclick=mainMenuCalloutAction;
						}
					}

				}
				
							

				function checkCalloutButtons()
				{
					

					if(isExitSafe("Are you sure you want to leave without saving?")==true)
					{
						document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMUNICATION&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);
				          
						actions[elementClicked.id]();
					}
				}

				function validateMessageForm()
				{
					var isValid=true;
					var isEfa = document.forms[0].is_efa_ftdm_florist.value;
					
					if(!document.forms[0].call_out_disposition[2].checked){
						if(!checkPersonSpokeTo("person_spoke_to", "invalidPersonSpokeTo")){

							isValid=false;
						}
						
						if(isEfa == 'Y')
						{
							if(!checkPersonSpokeTo("nonftd_florist_shop_name", "invalidShopName")){
	
								isValid=false;
							}
							
							if(!checkPersonSpokeTo("nonftd_florist_phone", "invalidShopPhone")){
	
								isValid=false;
							}
						}
										
						if(document.forms[0].call_out_disposition[0].checked){//successfully called out.

							if(isEfa == 'N' && !checkCalloutPrice()){
								isValid=false;
							}

							if(isEfa == 'Y' && !checkCallOutAdditionalFee()){
								isValid = false;
							}
						}
			
				    }
				    
					
				    return isValid;
				        
				        
				}


				function checkCalloutPrice(){
					
					if(checkPrice("florist_prd_price", "invalidCalloutPrice")){
						var originPrice=parseFloat(document.forms[0].ftd_origin_price.value);
						var calloutPrice=parseFloat(document.forms[0].florist_prd_price.value);
						var diff=Math.round((calloutPrice-originPrice)*100)/100;
						//alert("old="+originPrice+" new="+calloutPrice+" diff="+diff) ;
					    if(diff>20){
							document.getElementById("invalidCalloutPrice").firstChild.nodeValue="The price can only be increased by $20.00.";
							document.getElementById("invalidCalloutPrice").style.display = "block";
							return false;
					    }else if(calloutPrice<originPrice){
					    
					    		document.getElementById("invalidCalloutPrice").firstChild.nodeValue="The price can not be less than the original price: "+ originPrice;
							document.getElementById("invalidCalloutPrice").style.display = "block";
							return false;
					    
					    }else{
							return true;
					    }

				
					}else{
						return false;

					}

				}

								
				function checkCallOutAdditionalFee(){
					var calloutAddFee = document.getElementById("additional_delivery_fee").value;
					
					if(isWhitespace(calloutAddFee)){
						document.getElementById("invalidAdditionalFee").style.display = "none";
						return true;
					}
					else if(!checkAdditionalDeliveryFee(calloutAddFee, "invalidAdditionalFee")){
						return false;
					} 
					else {
					    document.getElementById("invalidAdditionalFee").style.display = "none";
						return true;
					}
				}


				function doCalloutAction()
				{
					showWaitMessage("content", "wait");

					if(validateMessageForm())
					{
						document.forms[0].submit();
					}
					else
					{
						hideWaitMessage("content", "wait");
					}
				}
				
				function mainMenuCalloutAction()
				{
				  if(isExitSafe("Are you sure you want to leave without saving?")==true)
				  {
				  	
				           document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMUNICATION&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);
				          
				           doMainMenuAction();
					
				  }
				}
				
			]]>
			</script>
			<style>
				p{
					margin: 15px;
				}
			</style>
		</head>

		<body onload="init();">
			<form id="calloutForm" method="post" action="sendCallout.do">

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>
				<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
				<iframe id="checkLock" width="0px" height="0px" border="0"/>
				<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
				<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
				<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
				<input type="hidden" name="filling_florist_name" id="filling_florist_name" value="{$xvFillingFloristName}"/>
				<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$ftd_message_id}"/>
				<input type="hidden" name="external_order_number" id="external_order_number" value="{$xvOrderNumber}"/>
				<xsl:variable name="florist_phone">
					<xsl:call-template name="mercMessFormatPhone">
						<xsl:with-param name="phoneNumber" select="$xvFillingFloristPhone"/>
					</xsl:call-template>
				</xsl:variable>				
				<input type="hidden" name="florist_phone_number" id="florist_phone_number" value="{$florist_phone}"/>

				<input type="hidden" name="order_date_text" id="order_date_text" value="{$xvOrderDate}"/>
				<input type="hidden" name="order_date" id="order_date" value="{$xvOrderDateTimestamp}"/>

				<input type="hidden" name="order_delivery_date" id="order_delivery_date" value="{$xvOrderDeliveryDate}"/>
				<input type="hidden" name="order_delivery_timestamp" id="order_delivery_timestamp" value="{$xvOrderDeliveryDateTimestamp}"/>
				<input type="hidden" name="order_delivery_date_end" id="order_delivery_date_end" value="{$xvOrderDeliveryDateEnd}"/>
				
				<input type="hidden" name="ftd_origin_price" id="ftd_origin_price" value="{$xvNewPrice}"/>
				
				<input type="hidden" name="recipient_name" id="recipient_name" value="{$xvRecipientName}"/>
				<input type="hidden" name="recipient_street" id="recipient_street" value="{$xvRecipientStreet}"/>
				<input type="hidden" name="recipient_city" id="recipient_city" value="{$xvRecipientCity}"/>
				<input type="hidden" name="recipient_state" id="recipient_state" value="{$xvRecipientState}"/>
				<input type="hidden" name="recipient_zip" id="recipient_zip" value="{$xvRecipientZip}"/>
				<xsl:variable name="recipient_phone">
					<xsl:call-template name="mercMessFormatPhone">
						<xsl:with-param name="phoneNumber" select="$xvRecipientPhone"/>
					</xsl:call-template>
				</xsl:variable>
				<input type="hidden" name="recipient_phone" id="recipient_phone" value="{$recipient_phone}"/>
				
				<input type="hidden" name="first_choice" id="first_choice" value="{$xvFirstChoice}"/>
				<input type="hidden" name="allowed_substitution" id="allowed_substitution" value="{$xvSecondChoice}"/>
				<input type="hidden" name="comments" id="comments" value="{$xvMessageComment}"/>
				<input type="hidden" name="card_message" id="card_message" value="{$xvCardMessage}"/>
				<input type="hidden" name="occasion_code" id="occasion_code" value="{$xvOccasionCode}"/>
				<input type="hidden" name="occasion_desc" id="occasion_desc" value="{$xvOccasionDescription}"/>
				<input type="hidden" name="spec_instruction" id="spec_instruction" value="{$xvInstructions}"/>
				<input type="hidden" name="is_efa_ftdm_florist" id="is_efa_ftdm_florist" value="{$xvIsEfaFTDMFlorist}"/>

				<input type="hidden" name="position" id="position" value="{message/position}"/>
				<input type="hidden" name="queue_type" id="queue_type" value="{message/queue_type}"/>
				<input type="hidden" name="queue_ind" id="queue_ind" value="{message/queue_ind}"/>
				<input type="hidden" name="tagged" id="tagged" value="{message/tagged}"/>
				<input type="hidden" name="attached" id="attached" value="{message/attached}"/>
				<input type="hidden" name="group" id="group" value="{message/group}"/>
				<input type="hidden" name="user" id="user" value="{message/user}"/>
				
				<input type="hidden" name="q_message_id" id="q_message_id" value="{message/q_message_id}"/>

				<div id="content">
					<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1" style="border: 0;">
						<tr>
							<td>
								<div class="floatleft">
									<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
								</div>

								<div class="Header">
									<xsl:value-of select="$xvFillingFloristCode"/>
									&nbsp;&nbsp;
									<xsl:value-of select="$xvFillingFloristName"/>
									&nbsp;&nbsp;
									<xsl:value-of select="$florist_phone"/>
								</div>
								<xsl:choose>
									<xsl:when test="$xvIsEfaFTDMFlorist = 'Y'">
										<div style="overflow: auto; text-align: left; height: 410px; clear: both; position: relative;">
											<p>
												Hello, My name is <xsl:value-of select="$xvCsrName"/>&nbsp; and I know that you are not FTD but was wondering if you would take an order by credit card? If yes, then proceed with the following information....
											</p>
											
											<p>
												Ask if they deliver to the city on the order? 
											</p>
											
											<p> 
												Ask if they can fill the requested item and for the price that is in RED (which includes delivery/taxes).
											</p>  
												
											<p>
												If all is okay, go ahead and give them the information on the order.
											</p>
												
											<p>
												His/Her Address is <xsl:value-of select="$xvRecipientStreet"/>&nbsp;in <xsl:value-of select="$xvRecipientCity"/>,
												<xsl:value-of select="$xvRecipientState"/>&nbsp;<xsl:value-of select="$xvRecipientZip"/>.
												His/Her Phone # is <xsl:value-of select="$recipient_phone"/>.
											</p>
		
											<p>
												The Delivery Date is <xsl:value-of select="$xvOrderDeliveryDate"/>.
											</p>
											
											<p>
												(If there is a time request, check to make sure they can make the delivery by that time.  If they can't ask what time approximately it would be there and then note that on the order.) If they do not deliver to this area, try the next shop, otherwise if there are no more shops to try the order will need to be cancelled.  If they cannot fill for the item or order value, inquire on what would be needed and take down their name (we will need to ASK for this.)
											</p>
											
											<p>
												Tell them the credit card is a Mastercard and it is made out to FTD Headquarters,
											</p>
											
											<p>
												Expiration date is XX/XX
											</p>
											
											<p>
												V-code or security code on the back of the card is 
											</p>
											
											<p>
												Address for credit card billing is&nbsp;:
											</p>
											
											<p>
												&nbsp;&nbsp;&nbsp;&nbsp;3113 Woodcreek Drive
											</p>
											<p>
												&nbsp;&nbsp;&nbsp;&nbsp;Downers Grove, IL 60515
											</p>
											
											<p>
												The order calls for <xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>.
											</p>
		
											<p>
												<xsl:choose>
													<xsl:when test="$xvSecondChoice != ''">
														The second choice is <xsl:value-of select="$xvSecondChoice" disable-output-escaping="yes"/>
													</xsl:when>
		
													<xsl:otherwise>
														There is no second choice.
													</xsl:otherwise>
												</xsl:choose>
											</p>
		
											<p>
												The price including delivery is: 
												<span id= "priceDisplay" style="BORDER-TOP: #ccc 1px solid; BORDER-RIGHT: #ccc 1px solid; BORDER-BOTTOM: #ccc 1px solid; POSITION: relative; PADDING-BOTTOM: 3px; PADDING-TOP: 3px; PADDING-LEFT: 3px; BORDER-LEFT: #ccc 1px solid; DISPLAY: inline-block; TOP: 5px; PADDING-RIGHT: 3px">
												 	<xsl:value-of select="$xvNewPrice" />
												</span>
												<input type="text" name="florist_prd_price" id="florist_prd_price" style="display: none;" value="{$xvNewPrice}" size="8" maxlength="8"/>
											</p>
											<p>
												<span style="background: #D3D3D3; color: red;">
													**The price above is 73% of the Merc value and cannot be edited, if asked for any additonal fee please enter amount below
												</span>
											</p>
																					
											<p>
												Additional Delivery Fee (if applicable): <input type="text" name="additional_delivery_fee" id="additional_delivery_fee" size="8" maxlength="8" onKeyPress="return noEnter();"/>
												<span id="invalidAdditionalFee" class="RequiredFieldTxt" style="display: none;"> &nbsp;&nbsp;
												</span>
											</p>

											<p>
												<xsl:choose>
													<xsl:when test="$xvCardMessage != ''">
														The card message signature reads as: <xsl:value-of select="$xvCardMessage"/>
													</xsl:when>
		
													<xsl:otherwise>
														There is no card message signature.
													</xsl:otherwise>
												</xsl:choose>
											</p>
		
											<p>
												<xsl:choose>
													<xsl:when test="$xvInstructions != ''">
														Special Instructions: <xsl:value-of select="$xvInstructions"/>
													</xsl:when>
		
													<xsl:otherwise>
														There are no special instructions.
													</xsl:otherwise>
												</xsl:choose>
											</p>
		
											<p>
												Capture the following details about the call you made:
											</p>
											
											<p>
												Be sure to give the florist your name!
											</p>
											<p>
												See sample for how to write information on the front of the order-
											</p>
					
											<p>
												1. Filling Shop Name &nbsp;:&nbsp;
												<input type="text" size="35" name="nonftd_florist_shop_name" id="nonftd_florist_shop_name" onKeyPress="return noEnter();"/>
												
												<span id="invalidShopName" class="RequiredFieldTxt" style="display: none;">
												</span>
											</p>
											<p>
												2. Filling Shop Phone number &nbsp;:&nbsp;
												<input type="text" size="35" name="nonftd_florist_phone" id="nonftd_florist_phone" onKeyPress="return noEnter();"/>
												
												<span id="invalidShopPhone" class="RequiredFieldTxt" style="display: none;">
												</span>
											</p>
											
											<p>
												3. Name of the person you gave the order to &nbsp;:&nbsp;&nbsp;
												<input type="text" size="35" name="spoke_to" id="person_spoke_to" onKeyPress="return noEnter();"/>
												
												<span id="invalidPersonSpokeTo" class="RequiredFieldTxt" style="display: none;">
												</span>
											</p>
											
											<input type="radio" name="call_out_disposition" id="call_out_disposition_success" value="call_out_disposition_success" checked="checked"/>
											<label for="call_out_disposition_success">
												Successfully Called Out
											</label>
		
											<br/>
		
											<input type="radio" name="call_out_disposition" id="call_out_disposition_failure" value="call_out_disposition_failure"/>
											<label for="call_out_disposition_failure">
												Florist Could Not Fill Order
											</label>
											
											<br/>
		
											<input type="radio" name="call_out_disposition" id="call_out_disposition_unreachable" value="call_out_disposition_unreachable"/>
											<label for="call_out_disposition_unreachable">
												Florist Could Not Be Reached
											</label>
										</div>
									</xsl:when>
									<xsl:otherwise>	
										<div style="overflow: auto; text-align: left; height: 410px; clear: both;">
											<p>
														Hello, This is <xsl:value-of select="$xvCsrName"/>&nbsp;from <xsl:value-of select="$xvSendingFloristCompanyName"/>. I have an <xsl:value-of select="$xvSendingFloristCompanyName"/> order for you.
														You will not need to report this order.
														Our shop code is <xsl:value-of select="$xvSendingFloristCode"/>AA.
														We are located in Downer's Grove, IL.
														Our phone # is <xsl:value-of select="$xvCalloutPhone"/>.
														This is order # <xsl:value-of select="$xvOrderNumber"/>.
														The Recipient's name is <xsl:value-of select="$xvRecipientName"/>.
											</p>
											
											<p>
												His/Her Address is <xsl:value-of select="$xvRecipientStreet"/>&nbsp;in <xsl:value-of select="$xvRecipientCity"/>,
												<xsl:value-of select="$xvRecipientState"/>&nbsp;<xsl:value-of select="$xvRecipientZip"/>.
												His/Her Phone # is <xsl:value-of select="$recipient_phone"/>.
											</p>
		
											<p>
												The Delivery Date is <xsl:value-of select="$xvOrderDeliveryDate"/>.
											</p>
		
											<p>
												The order calls for <xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>.
											</p>
		
											<p>
												<xsl:choose>
													<xsl:when test="$xvSecondChoice != ''">
														The second choice is <xsl:value-of select="$xvSecondChoice" disable-output-escaping="yes"/>
													</xsl:when>
		
													<xsl:otherwise>
														There is no second choice.
													</xsl:otherwise>
												</xsl:choose>
											</p>
		
											<p>
												The price including delivery is: <input type="text" name="florist_prd_price" id="florist_prd_price" value="{$xvNewPrice}" size="8" maxlength="8" onKeyPress="return noEnter();"/>
												<span id="invalidCalloutPrice" class="RequiredFieldTxt" style="display: none;">Invalid Price.
												</span>
											</p>
											<p>
												<span style="background: #D3D3D3; color: red;">
													**If asked, $5.00 is included for delivery. You may pay up to $5 additional but you MUST change amount to agree.
												</span>
											</p>
		
											<p>
												<xsl:choose>
													<xsl:when test="$xvCardMessage != ''">
														The card message signature reads as: <xsl:value-of select="$xvCardMessage"/>
													</xsl:when>
		
													<xsl:otherwise>
														There is no card message signature.
													</xsl:otherwise>
												</xsl:choose>
											</p>
		
											<p>
												<xsl:choose>
													<xsl:when test="$xvInstructions != ''">
														Special Instructions: <xsl:value-of select="$xvInstructions"/>
													</xsl:when>
		
													<xsl:otherwise>
														There are no special instructions.
													</xsl:otherwise>
												</xsl:choose>
											</p>
		
											<p>
												Thank you for taking care of our order. Please contact us if there are any problems.
											</p>
		
											<p>
												May I have your name for our records please?&nbsp;:&nbsp;
												<input type="text" size="35" name="spoke_to" id="person_spoke_to" onKeyPress="return noEnter();"/>
												<span id="invalidPersonSpokeTo" class="RequiredFieldTxt" style="display: none;">
												</span>
											</p>
		
											<input type="radio" name="call_out_disposition" id="call_out_disposition_success" value="call_out_disposition_success" checked="checked"/>
											<label for="call_out_disposition_success">
												Successfully Called Out
											</label>
		
											<br/>
		
											<input type="radio" name="call_out_disposition" id="call_out_disposition_failure" value="call_out_disposition_failure"/>
											<label for="call_out_disposition_failure">
												Florist Could Not Fill Order
											</label>
											
											<br/>
		
											<input type="radio" name="call_out_disposition" id="call_out_disposition_unreachable" value="call_out_disposition_unreachable"/>
											<label for="call_out_disposition_unreachable">
												Florist Could Not Be Reached
											</label>
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>

						<tr align="center">
							<td>
								<button type="button" id="saveButton" class="BlueButton" accesskey="V" onclick="checkLockProcessRequest('call_out');">
									Sa(v)e
								</button>
							</td>
						</tr>
					</table> <!-- End of mainTable -->
				</div>

				<div id="waitDiv" style="display: none;">
					<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
										<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>

				<xsl:call-template name="mercMessButtons"/>
				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->