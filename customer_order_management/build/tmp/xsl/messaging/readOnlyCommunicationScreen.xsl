<!DOCTYPE stylesheet [
	<!ENTITY  nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="mercMessFormatDate.xsl"/>

<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>

<!-- Variables for the messages and emails tables -->
<xsl:param name="read_only_flag"><xsl:value-of select="key('security','read_only_flag')/value" /></xsl:param>
<xsl:variable name="xvMessages" select="root/messages"/>
<xsl:variable name="xvCurrentPage" select="normalize-space(root/CurrentPage)"/>
<xsl:variable name="xvTotalPages" select="normalize-space(root/NumberOfPages)"/>
<xsl:variable name="xvEmailLetters" select="root/email-letters"/>
<!-- Variables for queue information -->
<xsl:variable name="xvOrderNumber" select="normalize-space($msg_external_order_number)"/>
<xsl:variable name="xvCreditQueue" select="normalize-space(root/queues/queue[@num = '1']/queue_item)"/>
<xsl:variable name="xvCreditQueueId">
	<xsl:variable name="temp1" select="normalize-space(root/queues/queue[@num = '1']/tag_userid)"/>
	<xsl:value-of select="substring($temp1, 1, 11)"/>
</xsl:variable>

<xsl:variable name="xvZipQueue" select="normalize-space(root/queues/queue[@num = '2']/queue_item)"/>
<xsl:variable name="xvZipQueueId">
	<xsl:variable name="temp2" select="normalize-space(root/queues/queue[@num = '2']/tag_userid)"/>
	<xsl:value-of select="substring($temp2, 1, 11)"/>
</xsl:variable>

<xsl:variable name="xvOrder1" select="normalize-space(root/queues/queue[@num = '3']/queue_item)"/>
<xsl:variable name="xvOrder1Id">
	<xsl:variable name="temp3" select="normalize-space(root/queues/queue[@num = '3']/tag_userid)"/>
	<xsl:value-of select="substring($temp3, 1, 11)"/>
</xsl:variable>

<xsl:variable name="xvOrder2" select="normalize-space(root/queues/queue[@num = '4']/queue_item)"/>
<xsl:variable name="xvOrder2Id">
	<xsl:variable name="temp4" select="normalize-space(root/queues/queue[@num = '4']/tag_userid)"/>
	<xsl:value-of select="substring($temp4, 1, 11)"/>
</xsl:variable>

<xsl:template match="/">

	<html>
		<head>
			<base target="_self" />
			<title>Communication</title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<link rel="stylesheet" type="text/css" href="css/communication.css"/>

			<script type="text/javascript" src="js/clock.js">;</script>
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script>
				function init()
				{
					document.oncontextmenu = contextMenuHandler;

					<xsl:if test="$xvTotalPages &gt; 1">
						document.attachEvent("onkeydown", arrowKeyHandler);
					</xsl:if>

					document.attachEvent("onkeydown", backKeyHandler);
					document.attachEvent("onkeydown", evaluateKeyPress);

					untabHeader();
				}

				<xsl:if test="$xvTotalPages &gt; 1">
					function arrowKeyHandler()
					{
						var keypress = window.event.keyCode;

						<xsl:if test="$xvCurrentPage &gt; 1">
							//go to 1st page(down arrow)
							if(window.event.altKey &amp;&amp; keypress == 38)
								newPage(1);

							//go to previous page(left arrow)
							if(window.event.altKey &amp;&amp; keypress == 37)
								newPage(<xsl:value-of select="$xvCurrentPage - 1"/>);
						</xsl:if>

						<xsl:if test="number($xvCurrentPage) != number($xvTotalPages)">
							//go to the last page(up arrow)
							if(window.event.altKey &amp;&amp; keypress == 40)
								newPage(<xsl:value-of select="$xvTotalPages"/>);

							//go to the next page(right arrow)
							if(window.event.altKey &amp;&amp; keypress == 39)
								newPage(<xsl:value-of select="$xvCurrentPage + 1"/>);
						</xsl:if>
					}
				</xsl:if>
			</script>
		</head>

		<body onload="init();">
			<form id="communicationScreenForm" name="communicationScreenForm" action="viewMessageSwitch.do" method="post">

				<input type="hidden" name="message_info" id="message_info"/>
				<input type="hidden" name="message_line_number" id="message_line_number"/>
				<input type="hidden" name="read_only_flag" id="read_only_flag" value="{$read_only_flag}"/>
				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<xsl:for-each select="$xvMessages/message">
					<xsl:variable name="line" select="normalize-space(row_number)"/>
					<xsl:variable name="id" select="normalize-space(message_id)"/>
					<xsl:variable name="type">
						<xsl:choose>
							<xsl:when test="normalize-space(group_indicator) = 'Email/Letter'">
								<xsl:value-of select="normalize-space(msg_type)"/>
							</xsl:when>

							<xsl:otherwise>
								<xsl:value-of select="normalize-space(group_indicator)"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<input type="hidden" name="{$line}" id="{$line}" value="{concat($id, '|', $type)}"/>
				</xsl:for-each>

				<xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="'Communication'"/>
					<xsl:with-param name="showBackButton" select="false()"/>
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="showCSRIDs" select="false()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>

				<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
					<tr>
						<td>
							<!--Order information-->
							<table class="PaneSection">
								<tr style="font-weight: bold;">
									<td id="orderNumber">
										Order Number:&nbsp;
										<xsl:value-of select="$xvOrderNumber"/>
									</td>

									<td id="creditQueue">
										<xsl:if test="$xvCreditQueue != ''">
											<xsl:value-of select="$xvCreditQueue"/>

											<xsl:if test="$xvCreditQueueId != ''">
												<xsl:text> - </xsl:text>
												<xsl:value-of select="$xvCreditQueueId"/>
											</xsl:if>
										</xsl:if>
									</td>

									<td id="zipQueue">
										<xsl:if test="$xvZipQueue != ''">
											<xsl:value-of select="$xvZipQueue"/>

											<xsl:if test="$xvZipQueueId != ''">
												<xsl:text> - </xsl:text>
												<xsl:value-of select="$xvZipQueueId"/>
											</xsl:if>
										</xsl:if>
									</td>

									<td id="order1">
										<xsl:if test="$xvOrder1 != ''">
											<xsl:value-of select="$xvOrder1"/>

											<xsl:if test="$xvOrder1Id != ''">
												<xsl:text> - </xsl:text>
												<xsl:value-of select="$xvOrder1Id"/>
											</xsl:if>
										</xsl:if>
									</td>

									<td id="order2">
										<xsl:if test="$xvOrder2 != ''">
											<xsl:value-of select="$xvOrder2"/>

											<xsl:if test="$xvOrder2Id != ''">
												<xsl:text> - </xsl:text>
												<xsl:value-of select="$xvOrder2Id"/>
											</xsl:if>
										</xsl:if>
									</td>
								</tr>
							</table>

							<!--Messages banner-->
							<table class="PaneSection">
								<tr>
									<td align="center" class="banner">
										Messages
									</td>
								</tr>
							</table>

							<!-- Mercury messages -->
							<div class="messageContainer" style="padding-right: 16px;">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<th class="number">
										</th>

										<th class="date">
											Date
										</th>

										<th class="time">
											Time
										</th>

										<th class="type">
											Type
										</th>

										<th class="florist">
											Filler Code
										</th>

										<th class="amount">
											Merc Value
										</th>

										<th class="status">
											Status
										</th>

										<th class="id">
											Mercury ID
										</th>
									</tr>
								</table>
							</div>

							<div class="messageContainer" style="overflow: auto; padding-right: 16px; height: 126px;">
								<table cellpadding="0" cellspacing="0" style="width: 100%; margin-right: -16px;">
									<xsl:variable name="mercuryMessages" select="$xvMessages/message[normalize-space(group_indicator) = 'Mercury']"/>
									<xsl:variable name="venusMessages" select="$xvMessages/message[normalize-space(group_indicator) = 'Venus']"/>

									<xsl:for-each select="$mercuryMessages | $venusMessages">
										<tr>
											<td class="number">
												<xsl:value-of select="normalize-space(row_number)"/>.
											</td>

											<td class="date">
												<xsl:call-template name="mercMessFormatDate">
													<xsl:with-param name="dateString" select="normalize-space(message_date)"/>
												</xsl:call-template>
											</td>

											<td class="time">
												<xsl:value-of select="normalize-space(message_time)"/>
											</td>

											<td class="type">
												<xsl:variable name="messageType" select="normalize-space(msg_type)"/>

												<xsl:if test="not(starts-with($messageType, 'FTD'))">
													&nbsp;&nbsp;
												</xsl:if>

												<xsl:value-of select="substring($messageType, 1, 28)"/>
											</td>

											<td class="florist">
												<xsl:variable name="florist">
													<xsl:variable name="temp5" select="normalize-space(filling_florist)"/>
													<xsl:value-of select="substring($temp5, 1, 9)"/>
												</xsl:variable>

												<xsl:choose>
													<xsl:when test="group_indicator = 'venus'">
														<xsl:text>VENDOR</xsl:text>
													</xsl:when>

													<xsl:when test="$florist = ''">
														<xsl:text>UNASSIGNED</xsl:text>
													</xsl:when>

													<xsl:otherwise>
															<xsl:value-of select="$florist"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>

											<td class="amount">
												<xsl:variable name="ftdPrice" select="normalize-space(price)"/>
												<xsl:if test="$ftdPrice &gt; 0">
													<xsl:value-of select="$ftdPrice"/>
												</xsl:if>
											</td>

											<td class="status">
												<xsl:value-of select="normalize-space(message_status)"/>
											</td>

											<td class="id">
												<xsl:value-of select="normalize-space(message_id)"/>
											</td>
										</tr>
									</xsl:for-each>

								</table> <!-- End of messageTable -->
							</div> <!-- End of messageContainer -->

							<!-- Email banner-->
							<table class="PaneSection">
								<tr>
									<td align="center" class="banner">
										Email / Letters
									</td>
								</tr>
							</table>

							<!-- Emails and letters -->
							<div class="emailContainer" style="padding-right: 16px;">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<th class="number">
										</th>

										<th class="date">
											Date
										</th>

										<th class="time">
											Time
										</th>

										<th class="type">
											Type
										</th>

										<th class="email">
											Email Address / Letter Type
										</th>

										<th class="subject">
											Subject
										</th>
									</tr>
								</table>
							</div>

							<div class="emailContainer" style="overflow: auto; padding-right: 16px; height: 105px;">
								<table cellpadding="0" cellspacing="0" style="width: 100%; margin-right: -16px;">
									<xsl:for-each select="$xvEmailLetters/email-letter">
										<tr>
											<td class="number">
												<xsl:value-of select="concat(normalize-space(row_number),'E')"/>.
											</td>

											<td class="date">
												<xsl:call-template name="mercMessFormatDate">
													<xsl:with-param name="dateString" select="normalize-space(message_date)"/>
												</xsl:call-template>
											</td>

											<td class="time">
												<xsl:value-of select="normalize-space(message_time)"/>
											</td>

											<td class="type">
												<xsl:variable name="temp6" select="normalize-space(msg_type)"/>
												<xsl:value-of select="substring($temp6, 1, 12)"/>
											</td>

											<td class="email">
													<xsl:variable name="temp7" select="normalize-space(sender_email_address)"/>
													<xsl:value-of select="substring($temp7, 1, 55)"/>
											</td>

											<td class="subject">
													<xsl:variable name="temp8" select="normalize-space(subject)"/>
													<xsl:value-of select="substring($temp8, 1, 26)"/>
											</td>
										</tr>
									</xsl:for-each>
								</table> <!-- End of emailTable -->
							</div> <!-- End of emailContainer -->

							<table class="PaneSection">
								<tr>
									<td width="33%"/>

									<td width="33%">
									</td>

									<td align="right" width="33%">
										<xsl:if test="$xvTotalPages &gt; 1">
											<xsl:choose>
												<xsl:when test="$xvCurrentPage = 1">
													<button type="button" class="arrowButton" disabled="disabled">
														7
													</button>

													<button type="button" class="arrowButton" disabled="disabled">
														3
													</button>
												</xsl:when>

												<xsl:otherwise>
													<button type="button" class="arrowButton" onclick="newPage(1);">
														7
													</button>

													<button type="button" class="arrowButton" onclick="newPage({$xvCurrentPage - 1})">
														3
													</button>
												</xsl:otherwise>
											</xsl:choose>

											<xsl:choose>
												<xsl:when test="number($xvCurrentPage) = number($xvTotalPages)">
													<button type="button" class="arrowButton" disabled="disabled">
														4
													</button>

													<button type="button" class="arrowButton" disabled="disabled">
														8
													</button>
												</xsl:when>

												<xsl:otherwise>
													<button type="button" class="arrowButton" onclick="newPage({$xvCurrentPage + 1});">
														4
													</button>

													<button type="button" class="arrowButton" onclick="newPage({$xvTotalPages});">
														8
													</button>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <!-- End of mainTable -->

				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="communicationScreen.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->