<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
	<xsl:import href="mercMessButtons.xsl"/>
	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessFormatPhone.xsl"/>
	<xsl:import href="dateRangeSelect.xsl"/>
	<xsl:import href="mercFTDFillerCode.xsl"/>
  <xsl:import href="formattingTemplates.xsl"/>
	<xsl:import href="compVendorDeliveryDates.xsl"/>

<xsl:key name="security" match="/message/security/data" use="name"/>
	<!--The temporary solution for modify order. Will be replaced by newFTD-PRE-MO.xsl after Modify Order goes live.-->
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>


	<xsl:template match="/">


		<html>
			<head>
				<title>
					<xsl:value-of select="$xvHeaderName"/>
				</title>

				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
				<meta http-equiv="language" content="en-us"/>

				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
				<script type="text/javascript" src="js/commonUtil.js">;</script>
				<script type="text/javascript" src="js/clock.js">;</script>

				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
				<script>var actions = new Array();

//***********************************************************************************************
// function init()
//***********************************************************************************************
	function init()
	{
		document.oncontextmenu = contextMenuHandler;
		document.onkeydown = backKeyHandler;
		var buttons = document.getElementsByTagName("BUTTON");<![CDATA[
		for(var i = 0; i < buttons.length; i++)
		{
			if(buttons[i].id != "mainMenuButton" && buttons[i].id != "sendButton"&& buttons[i].id != "lookupButton"&& buttons[i].id != "autoSelectButton")
			{
				actions[buttons[i].id] = buttons[i].onclick;
				buttons[i].onclick = checkButtonsNewFTD;
			}
			else if(buttons[i].id == "mainMenuButton")
			{
				buttons[i].onclick=mainMenuActionNewFTD;
			}
		}

		untabHeader();
		document.forms[0].filling_florist_code.focus();

	}


//***********************************************************************************************
// function checkButtonsNewFTD()
//***********************************************************************************************
	function checkButtonsNewFTD()
	{
		if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
		{
			document.getElementById("unlockFrame").src=getUnlockUrl();
			actions[elementClicked.id]();
		}
	}


//***********************************************************************************************
// function mainMenuActionNewFTD()
//***********************************************************************************************
	function mainMenuActionNewFTD()
	{
		if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
		{
			document.getElementById("unlockFrame").src=getUnlockUrl();
			doMainMenuAction();
		}
	}


//***********************************************************************************************
// function getUnlockUrl()
//***********************************************************************************************
	function getUnlockUrl()
	{
		var url="messagingCancelUpdateUnLock.do?lock_app=COMMUNICATION&order_detail_id="+document.getElementById("order_detail_id").value
		+"&order_delivery_date="+document.getElementById("origin_delivery_date").value
		+getSecurityParams(false);

		if(document.forms[0].origin_delivery_date_end.value!='')
		{
			url +="&order_delivery_date_end="+document.forms[0].origin_delivery_date_end.value;
		}
		return url;
	}


]]>


//***********************************************************************************************
// function validateMessageForm()
//***********************************************************************************************
	function validateMessageForm()
	{
		if(document.getElementById("filling_florist_code"))
		{
			var isValidFloristCode = checkFloristCode("filling_florist_code", "invalidFloristCode");
			return isValidFloristCode;
		}
		else
		{
			return true;
		}
	}



//***********************************************************************************************
// function doFloristLookupAction()
//***********************************************************************************************
	function doFloristLookupAction()
	{
		if(isValidDeliveryDate())
		{

<![CDATA[

			//update delivery date.

			var args = new Array();

			args.action = "floristLookup.do";

			args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
			args[args.length] = new Array("msg_message_order_number", document.forms[0].msg_message_order_number.value);
			args[args.length] = new Array("msg_message_type", document.forms[0].msg_message_type.value);
]]>

			args[args.length] = new Array("zip_code", '<xsl:value-of select="$xvRecipientZip"/>');<![CDATA[
			args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
			args[args.length] = new Array("context", document.getElementById("context").value);
			args[args.length] = new Array("lookup_by_order", "true");
			args[args.length] = new Array("context", document.getElementById("context").value);

			args[args.length] = new Array("delivery_month", document.getElementById("delivery_month").value);
			args[args.length] = new Array("delivery_date", document.getElementById("delivery_date").value);
			if(document.forms[0].delivery_month_end != null &&  document.forms[0].delivery_month_end != undefined) {
				args[args.length] = new Array("delivery_month_end", document.forms[0].delivery_month_end.value);
				args[args.length] = new Array("delivery_date_end", document.forms[0].delivery_date_end.value);
			}

]]>

			<xsl:if test="$xvOrderDeliveryDateEnd != ''">
				args[args.length] = new Array("delivery_month_end", document.getElementById("delivery_month_end").value);
				args[args.length] = new Array("delivery_date_end", document.getElementById("delivery_date_end").value);
			</xsl:if><![CDATA[
			args[args.length] = new Array("lookup_by_order", "true");
			if(document.getElementById("modify_order"))
			{
				args[7] = new Array("modify_order", document.getElementById("modify_order").value);
			}
			var modal_dim = "dialogWidth:750px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";

			var floristInfo = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);

			if(floristInfo != null)
			{
				document.forms[0].filling_florist_code.value = floristInfo[0];
				document.forms[0].florist_status.value = floristInfo[1];
				document.forms[0].mercury_status.value = floristInfo[2];
				doSendNewFTDMessageAction("florist_lookup_send");
			}
]]>
		}
	}


//***********************************************************************************************
// function isValidDeliveryDate()
//***********************************************************************************************
	function isValidDeliveryDate()
	{
		var isValidDateRange=true;
		var isValidOrderDeliveryDate = checkOrderDeliveryDate("delivery_month", "delivery_date","invalidOrderDeliveryDate");
		<xsl:if test="$xvOrderDeliveryDateEnd != ''">
			var isValidOrderDeliveryDateEnd = checkOrderDeliveryDate("delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");

			isValidDateRange = false;
<![CDATA[
			if( isValidOrderDeliveryDate && isValidOrderDeliveryDateEnd)
			{
				isValidDateRange = checkDateRange("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");
			}
]]>
		</xsl:if>

<![CDATA[
		return (isValidOrderDeliveryDate && isValidDateRange);
]]>
	}


//***********************************************************************************************
// function doSendNewFTDMessageAction()
//***********************************************************************************************
	function doSendNewFTDMessageAction(actionType)
	{
		showWaitMessage("content", "wait");
		<xsl:choose>
			<xsl:when test="$xvMessageCategory = 'Venus' or $new_florist_id !=''"><![CDATA[
				//alert("submit form directly");
				if (validateVendorShipMethodAndDeliveryDate())
				{
					document.forms[0].submit();
				}
				else
				{
					hideWaitMessage("content", "wait");
					alert("Please select a valid ship method and date" );
				}
]]>
			</xsl:when>
			<xsl:otherwise>
				var isValidDateRange=true;
				var isValidOrderDeliveryDate = checkOrderDeliveryDate("delivery_month", "delivery_date","invalidOrderDeliveryDate");

				//alert("isValidOrderDeliveryDate="+isValidOrderDeliveryDate);
				<xsl:if test="$xvOrderDeliveryDateEnd != ''">
					var isValidOrderDeliveryDateEnd = checkOrderDeliveryDate("delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");

					isValidDateRange = false;
<![CDATA[
					if( isValidOrderDeliveryDate && isValidOrderDeliveryDateEnd)
					{
						isValidDateRange = checkDateRange("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");
					}
]]>
				</xsl:if>

<![CDATA[
				if(isValidOrderDeliveryDate && isValidDateRange)
				{
					doNewFTDFormAction(actionType);
				}
				else
				{
					hideWaitMessage("content", "wait");
				}
]]>
			</xsl:otherwise>
		</xsl:choose>
	}


//***********************************************************************************************
// function doNewFTDFormAction()
//***********************************************************************************************
	function doNewFTDFormAction(actionType)
	{
        <![CDATA[
		document.getElementById("invalidFloristCode").style.display = "none";

		if(actionType == "auto_select_send")
		{
			var url = "autoSelectFlorist.do?action="
							+ actionType
							+ "&order_detail_id="
							+ document.forms[0].order_detail_id.value
							+ "&msg_message_type="
							+ document.forms[0].msg_message_type.value
							+ "&call_out="+document.forms[0].call_out.checked
							+ getDeliveryDateParams()
							+ getSecurityParams(false);
			//alert(url);
			FloristStatusIFrame.location.href = url;
		}
		else if(actionType == "florist_lookup_send")
		{
			var callOut=document.forms[0].call_out.checked;

			if(callOut)
			{
				document.forms[0].action="prepareCallout.do";
				document.forms[0].submit();
			}
			else
			{
				checkStatusIframe();
			}
		}
		else
		{
			if(validateMessageForm())
			{
				checkStatusIframe();
			}
			else
			{
				hideWaitMessage("content", "wait");
			}
		}
]]>
	}


//***********************************************************************************************
// function checkStatusIframe()
//***********************************************************************************************
	function checkStatusIframe()
	{
		var url = "checkFloristStatus.do?filling_florist_code="+document.forms[0].filling_florist_code.value
<![CDATA[
						+ "&florist_status="
						+ document.forms[0].florist_status.value
						+ "&mercury_status="
						+ document.forms[0].mercury_status.value
						+ "&msg_message_type="
						+ document.forms[0].msg_message_type.value
						+ "&call_out="+document.forms[0].call_out.checked
						+ "&order_detail_id="
						+ document.forms[0].order_detail_id.value
						+ getDeliveryDateParams()
						+ getSecurityParams(false);
		//alert(url);

		FloristStatusIFrame.location.href = url;
	}


//***********************************************************************************************
// function getDeliveryDateParams()
//***********************************************************************************************
	function getDeliveryDateParams()
	{
		var dateUrl	="&delivery_month="+document.forms[0].delivery_month.value
								+"&delivery_date="+document.forms[0].delivery_date.value;

]]>


		<xsl:if test="$xvOrderDeliveryDateEnd != ''"><![CDATA[
			dateUrl +="&delivery_month_end="+document.forms[0].delivery_month_end.value
							+"&delivery_date_end="+document.forms[0].delivery_date_end.value;
]]>
		</xsl:if>
		return dateUrl;
	}


<![CDATA[

//***********************************************************************************************
// function validateVendorShipMethodAndDeliveryDate()
//***********************************************************************************************
function validateVendorShipMethodAndDeliveryDate()
{

  var radio = document.forms["newFTDForm"].page_ship_method;
	var radioSelected;

	//incase the product is not available for the delivery date/address selected, we will not
	//get the delivery dates/shipping methods at all.
	//In this case, use the default delivery date, delivery date range, and the shipping method.
	if (radio != null)
	{
		if (radio.length == null)
		{
			if (radio.checked)
			{
				radioSelected = radio;
			}
		}
		else
		{
			for ( var i = 0; i < radio.length; i++)
			{
				radio[i].style.backgroundColor='white';
				if ( radio[i].checked )
				{
					radioSelected = radio[i];
				}
			}
		}


		// continue only if a ship method was selected
		if ( radioSelected != null )
		{
			var shipMethod = radioSelected.value;

			var dd = document.getElementById(shipMethod + "_delivery_date");
			var ddSelected = dd[dd.selectedIndex];
			var startDate = ddSelected.value;
			var endDate = ddSelected.endDate;
			var outEndDate = ( endDate != startDate ) ?  endDate : "";

			document.forms["newFTDForm"].vendor_ship_method.value = shipMethod;
			document.forms["newFTDForm"].vendor_delivery_date.value = startDate;
			document.forms["newFTDForm"].vendor_delivery_date_end.value = outEndDate;

			return true;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++)
			{
				radio[i].style.backgroundColor='pink';
			}
			return false;
		}
	}
	else
	{
		return false;
	}

}
]]>


				</script>

				<style>td.label {
        			width: 225px;
        		}</style>
			</head>

			<body onload="init();">
				<form id="newFTDForm" method="post" action="sendFTDMessage.do">

					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<xsl:call-template name="messagingSecurityAndData"/>

					<iframe id="FloristStatusIFrame" name="FloristStatusIFrame" width="0" height="0" border="0" style="display: none"/>
					<iframe id="SendFTDIFrame" name="SendFTDIFrame" width="0" height="0" border="0" style="display: none;"/>
					<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
					<iframe id="checkLock" width="0px" height="0px" border="0"/>
					<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
					<input type="hidden" name="florist_status" id="florist_status"/>
					<input type="hidden" name="mercury_status" id="mercury_status"/>

					<input type="hidden" name="origin_delivery_date" id="origin_delivery_date" value="{message/deliveryDate}"/>
					<input type="hidden" name="origin_delivery_date_end" id="origin_delivery_date_end" value="{$xvOrderDeliveryDateEnd}"/>

					<input type="hidden" name="vendor_delivery_date"			id="vendor_delivery_date"/>
					<input type="hidden" name="vendor_delivery_date_end" 	id="vendor_delivery_date_end"/>
					<input type="hidden" name="vendor_ship_method" 				id="vendor_ship_method"/>
					<input type="hidden" name="recipient_city" 						id="recipient_city"			value="{$xvRecipientCity}"/>
					<input type="hidden" name="recipient_state" 					id="recipient_state" 		value="{$xvRecipientState}"/>
					<input type="hidden" name="recipient_zip" 						id="recipient_zip" 			value="{$xvRecipientZip}"/>
					<input type="hidden" name="recipient_country" 				id="recipient_country" 	value="{message/recipientCountry}"/>
					<input type="hidden" name="product_id" 								id="product_id" 				value="{message/productId}"/>


					<xsl:choose>
						<xsl:when test="$destination ='' ">
							<xsl:call-template name="header">
								<xsl:with-param name="showSearchBox" select="false()"/>
								<xsl:with-param name="showPrinter" select="true()"/>
								<xsl:with-param name="showTime" select="true()"/>
								<xsl:with-param name="headerName" select="$xvHeaderName"/>

								<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
								<xsl:with-param name="indicator" select="$xvIndicator"/>
								<xsl:with-param name="brandName" select="$xvBrandName"/>
								<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
								<xsl:with-param name="showCSRIDs" select="true()"/>
								<xsl:with-param name="showBackButton" select="true()"/>
								<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
								<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="header">
								<xsl:with-param name="showSearchBox" select="false()"/>
								<xsl:with-param name="showPrinter" select="true()"/>
								<xsl:with-param name="showTime" select="true()"/>
								<xsl:with-param name="headerName" select="$xvHeaderName"/>

								<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
								<xsl:with-param name="indicator" select="$xvIndicator"/>
								<xsl:with-param name="brandName" select="$xvBrandName"/>
								<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
								<xsl:with-param name="showCSRIDs" select="true()"/>
								<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>

					<script>updateCurrentUsers();</script>

					<div id="content">
						<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td>
									<table class="PaneSection">
										<tr>
											<td class="banner">Message Detail

												&nbsp;-&nbsp;FTD</td>
										</tr>
									</table>

									<div style="overflow: auto; height: 329px; padding-right: 16px;">
										<table width="100%" cellpadding="0" cellspacing="0" style="margin-right: -16px;">
											<xsl:choose>
												<xsl:when test="$xvMessageCategory = 'Venus' or $new_florist_id !=''">
													<input type="hidden" name="call_out" id="call_out" value="false"/>
													<tr>
														<td class="label" style="vertical-align: middle;">FILLER CODE:</td>

														<td class="datum">

															<xsl:value-of select="$xvFillingFloristCode"/>
															<input type="hidden" name="filling_florist_code" maxlength="9" id="filling_florist_code" value="{$xvFillingFloristCode}"/>

															<span id="invalidFloristCode" class="RequiredFieldTxt" style="display: none;">
															</span>
														</td>
													</tr>
												</xsl:when>

												<xsl:otherwise>
													<tr>
														<td class="label" style="vertical-align: middle;">MANUAL CALL OUT ORDER:</td>

														<td class="datum">

															<input type="checkbox" name="call_out" id="call_out" value="true">
																<xsl:if test="$xvMercuryMessageType='FTDM'">
																	<xsl:attribute name="checked">true</xsl:attribute>
																</xsl:if>
															</input>
														</td>
													</tr>
													<tr>
														<td class="label" style="vertical-align: middle;">FILLER CODE:</td>

														<td class="datum">


															<input type="text" size="9" name="filling_florist_code" id="filling_florist_code" onKeyPress="return noEnter();"/>
															<span id="invalidFloristCode" class="RequiredFieldTxt" style="display: none;">
															</span>
														</td>
													</tr>
												</xsl:otherwise>
											</xsl:choose>


											<tr>
												<td class="label">SENDER CODE:</td>

												<td class="datum">
													<xsl:value-of select="$xvSendingFloristCode"/>
												</td>
											</tr>

											<tr>
												<td class="label">DELIVER TO:</td>

												<td class="datum">
													<xsl:value-of select="$xvRecipientName"/>
												</td>
											</tr>

											<tr>
												<td class="label">STREET ADDRESS APT:</td>

												<td class="datum">
													<xsl:value-of select="$xvRecipientStreet"/>
												</td>
											</tr>

											<tr>
												<td class="label">CITY, STATE, ZIP:</td>

												<td class="datum">
													<xsl:value-of select="concat($xvRecipientCity, ', ', $xvRecipientState, ' ', $xvRecipientZip)"/>
												</td>
											</tr>

											<tr>
												<td class="label">PHONE:</td>

												<td class="datum">
													<xsl:call-template name="mercMessFormatPhone">
														<xsl:with-param name="phoneNumber" select="$xvRecipientPhone"/>
													</xsl:call-template>
												</td>
											</tr>
											<xsl:choose>
												<xsl:when test="$xvMessageCategory = 'Venus' or $new_florist_id !=''">
													<tr>
														<td colspan="3">
															<xsl:call-template name="vendorDeliveryDates">
																<xsl:with-param name="deliveryDates" select="deliveryDates"/>
																<xsl:with-param name="origShipMethod" select="shipMethod"/>
															</xsl:call-template>
														</td>
													</tr>
												</xsl:when>
												<xsl:otherwise>
													<xsl:call-template name="dateRangeSelect"/>
												</xsl:otherwise>
											</xsl:choose>
											<tr>
												<td class="label">FIRST CHOICE:</td>

												<td class="datum">

													<xsl:call-template name="replace">
														<xsl:with-param name="string" select="$xvFirstChoice" disable-output-escaping="yes"/>
													</xsl:call-template>
												</td>
											</tr>

											<tr>
												<td class="label">ALLOWED SUBSTITUTION:</td>

												<td class="datum">

													<xsl:call-template name="replace">
														<xsl:with-param name="string" select="$xvSecondChoice" disable-output-escaping="yes"/>
													</xsl:call-template>
												</td>
											</tr>

											<tr>
												<td class="label">PRICE INCLUDING DELIVERY:</td>

												<td class="datum">
													<xsl:value-of select="$xvNewPrice"/>
												</td>
											</tr>

											<tr>
												<td class="label">CARD:</td>

												<td class="datum">
													<xsl:choose>
														<xsl:when test="$xvCardMessage = ''">NO MESSAGE ENTERED</xsl:when>

														<xsl:otherwise>
															<xsl:call-template name="replace">
																<xsl:with-param name="string" select="$xvCardMessage"/>
															</xsl:call-template>

														</xsl:otherwise>
													</xsl:choose>
												</td>
											</tr>

											<tr>
												<td class="label">OCCASION CODE:</td>

												<td class="datum">
													<xsl:value-of select="$xvOccasionDescription"/>
												</td>
											</tr>

											<tr>
												<td class="label">SPECIAL INSTRUCTIONS:</td>

												<td class="datum">
													<xsl:choose>
														<xsl:when test="$xvInstructions = ''">NONE</xsl:when>

														<xsl:otherwise>
															<xsl:call-template name="replace">
																<xsl:with-param name="string" select="$xvInstructions"/>
															</xsl:call-template>

														</xsl:otherwise>
													</xsl:choose>
												</td>
											</tr>

											<tr>
												<td class="label">OPERATOR:</td>

												<td class="datum">
													<xsl:value-of select="$xvOperatorCode"/>
												</td>
												<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
											</tr>
										</table>
										<!-- End of order -->
									</div>
									<!-- End of scrolling div -->

									<table width="100%">
										<tr align="center">
											<td>
												<xsl:choose>
													<xsl:when test="$destination ='' ">
														<xsl:if test="$xvMessageCategory != 'Venus'">
															<button type="button" class="BlueButton" accesskey="U" id="lookupButton" onclick="doFloristLookupAction();" style="margin-right: 10px;">Florist Look(U)p</button>

															<button type="button" class="BlueButton" accesskey="A" id="autoSelectButton" onclick="checkLockProcessRequest('auto_select_send');" style="margin-right: 10px;">(A)uto Select &amp; Send</button>
														</xsl:if>

														<button type="button" class="BlueButton" accesskey="N" id="sendButton" onclick="checkLockProcessRequest('send_new_ftd');">Se(n)d</button>
													</xsl:when>
													<xsl:otherwise>
														<button type="button" class="BlueButton" accesskey="N" id="sendButton" onclick="checkLockProcessRequest('send_new_ftd');" style="margin-right: 10px;">Se(n)d</button>
														<button type="button" class="BlueButton" id="notSendButton" accesskey="D" onclick="openRecipientOrder();" style="margin-right: 10px;">Not Sen(d)</button>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End of mainTable -->
					</div>

					<div id="waitDiv" style="display: none;">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
											<td id="waitTD" align="left" width="50%" class="WaitMessage"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>

					<xsl:call-template name="mercMessButtons"/>
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
  <!--
	<xsl:template name="replace">
		<xsl:param name="string"/>
		<xsl:choose>
			<xsl:when test="contains($string,'&#10;')">
				<xsl:value-of select="substring-before($string,'&#10;')"/>
				<br/>
				<xsl:call-template name="replace">
					<xsl:with-param name="string" select="substring-after($string,'&#10;')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
  -->
</xsl:stylesheet>