<!DOCTYPE stylesheet [
	<!ENTITY  nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<!-- Variables for florist information -->
<xsl:variable name="xvTotal" select="normalize-space(dashboard/item/total_covering)"/>
<xsl:variable name="xvReceived" select="normalize-space(dashboard/item/total_used)"/>
<xsl:variable name="xvBlocked" select="normalize-space(dashboard/item/total_blocked)"/>
<xsl:variable name="xvCodified" select="normalize-space(dashboard/item/total_active_codified)"/>
<xsl:variable name="xvNotCodified" select="normalize-space(dashboard/item/total_active_not_codified)"/>
<xsl:variable name="xvCurrentlyClosed" select="normalize-space(dashboard/item/total_currently_closed)"/>
<xsl:variable name="xvDeliveryDateClosed" select="normalize-space(dashboard/item/total_delivery_date_closed)"/>

<xsl:template match="/">

	<html>
		<head>
			<title>Florist Dashboard</title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script>
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;
				}
			</script>
		</head>

		<body onload="init();" style="margin: 0; padding: 0;">
			<form id="floristDashboardForm" name="floristDashboardForm" method="post">
				
				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1" style="border: 0;">
					<tr>
						<td>
							<div class="banner" style="margin-bottom: 5px; margin-top: 1px;">
								FLORIST DASHBOARD
							</div>

							<div class="floatleft">
								<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
							</div>

							<br/>

							<div class="floatright">
								<button type="button" class="BlueButton" tabindex="-1" accesskey="C" onclick="top.close();">
									(C)lose
								</button>
							</div>

							<div style="overflow: auto; height: 138px; clear: both; width: 93%;">
								<table class="PaneSection" style="margin-top: 30px;">
									<tr>
										<td class="LabelRight" >
											Total Florist to Fill Order:
										</td>

										<td>
											<xsl:value-of select="$xvTotal"/>
										</td>

										<td class="LabelRight">
											Florist Available and Codified:
										</td>

										<td>
											<xsl:value-of select="$xvCodified"/>
										</td>
									</tr>

									<tr>
										<td class="LabelRight">
											Florist That Have Already Received:
										</td>

										<td>
											<xsl:value-of select="$xvReceived"/>
										</td>

										<td class="LabelRight">
											Florist Available /not Codified:
										</td>

										<td>
											<xsl:value-of select="$xvNotCodified"/>
										</td>
									</tr>

									<tr>
										<td class="LabelRight">
											Florist Currently Blocked:
										</td>

										<td>
											<xsl:value-of select="$xvBlocked"/>
										</td>

										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									
									<tr>
										<td class="LabelRight">
											Florist Currently Closed:
										</td>

										<td>
											<xsl:value-of select="$xvCurrentlyClosed"/>
										</td>

										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									
									
									<tr>
										<td class="LabelRight">
											Florist Delivery Date Closed:
										</td>

										<td>
											<xsl:value-of select="$xvDeliveryDateClosed"/>
										</td>

										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									
								</table>
							</div>

							<div class="floatright">
								<button type="button" class="BlueButton" accesskey="C" onclick="top.close();">
									(C)lose
								</button>
	        		</div>
						</td>
					</tr>
				</table> <!-- End of mainTable -->
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet>