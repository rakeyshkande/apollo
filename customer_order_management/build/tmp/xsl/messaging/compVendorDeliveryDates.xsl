<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="vendorDeliveryDates" >
  <xsl:param name="origShipMethod"/>
  <xsl:param name="deliveryDates"/>
 
  


  <script type="text/javascript" language="javascript">
  <![CDATA[

  var hasShippingMethods = "N";
  
/*
   * The prepareDeliveryDate function is dynamic because delivery date submission preperation
   * is different for florist and vendor items.  This version of prepareDeliveryDate gets the selected
   * ship method and the corresponding selected delivery date.
   */
  function prepareDeliveryDate(formName)
  {
    var radio = document.forms["payOriginalNewFTDForm"].page_ship_method;
    var radioSelected;

		//in case the product is not available for the delivery date/address selected, we will not
		//get the delivery dates/shipping methods at all.
		//In this case, use the default delivery date, delivery date range, and the shipping method.
		if (radio != null)
		{
			if (radio.length == null)
			{
				if (radio.checked)
					radioSelected = radio;
				else
					radioSelected = null;
			}
			else
			{
				for ( var i = 0; i < radio.length; i++){
					if ( radio[i].checked ){
						radioSelected = radio[i];
					}
				}
			}

			// continue only if a ship method was selected
			if ( radioSelected != null )
			{
				var shipMethod = radioSelected.value;
                
				var dd = document.getElementById(shipMethod + "_delivery_date");
				var ddSelected = dd[dd.selectedIndex];
		    var startDate = ddSelected.value;
       
				document.forms[formName].ship_method.value = shipMethod;
				document.forms[formName].delivery_date.value = startDate;
			}
			
		}
  }

function validateShipMethodAndDeliveryDate()
{
  var radio = document.forms["payOriginalNewFTDForm"].page_ship_method;
	var radioSelected;

	//in case the product is not available for the delivery date/address selected, we will not
	//get the delivery dates/shipping methods at all.
	//In this case, use the default delivery date, delivery date range, and the shipping method.
	if (radio != null)
	{
		if (radio.length == null)
		{
			if (radio.checked)
			{
				radioSelected = radio;
			}
		}
		else
		{
			for ( var i = 0; i < radio.length; i++)
			{
				radio[i].style.backgroundColor='white';
				if ( radio[i].checked )
				{
					radioSelected = radio[i];
				}
			}
		}

		if ( radioSelected != null )
		{
			return true;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++)
			{
				radio[i].style.backgroundColor='pink';
			}
      
			return false;
		}
	}
	else
	{
		return false;
	}
}


  ]]>
  </script>
  <tr>
    <input type="hidden" name="origShipMethod" id="origShipMethod" value="{$origShipMethod}"/>
    <td class="label" >
				DELIVERY DATE:
		</td>
    <td colspan="3">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <!-- There are four shipping methods for the vendor, plus the ship method of Florist
					<shippingMethod>
						<method cost="0" description="Saturday Delivery" code="SA"/>
						<method cost="0" description="Standard Delivery" code="GR"/>
						<method cost="0" description="Next Day Delivery" code="ND"/>
						<method cost="0" description="Two Day Delivery"  code="2F"/>
					</shippingMethod>
					Note that the florist delivery method can be '' or 'FL'.
			-->
        <xsl:for-each select="/message/shippingData/shippingMethods/shippingMethod/method">

          <script type="text/javascript" language="javascript">
          <![CDATA[
            hasShippingMethods = "Y";
          ]]>
          </script>

          <tr>

	          <!-- ******************************************************************************* -->
	          <!-- Radio Button -->
	          <!-- ******************************************************************************* -->
            <td class="Label" align="center" colspan="30%">
              <xsl:choose>
                <xsl:when test="@code = $origShipMethod">
                  <input type="radio" name="page_ship_method" id="page_ship_method" value="{@code}"/>
								</xsl:when>

                <xsl:when test="@code != 'FL'">
                 <input type="radio" name="page_ship_method" id="page_ship_method" value="{@code}"/>
               </xsl:when>
              </xsl:choose>
            </td>


	          <!-- ******************************************************************************* -->
            <!-- Description -->
	          <!-- ******************************************************************************* -->
            <td colspan="10%">
             <xsl:choose>  
              <xsl:when test="@code != 'FL'">
                <xsl:value-of select="@description"/>
              </xsl:when>
             </xsl:choose>
            </td>

            <!-- ******************************************************************************* -->
	          <!-- Day/Date Drop down -->
	          <!-- ******************************************************************************* -->
            <td>
              <xsl:choose>
                <xsl:when test="@code = 'ND'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/message/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'ND')">
                        <option value="{startDate}" endDate="{endDate}">

                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = '2F'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/message/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, '2F')">
                        <option value="{startDate}" endDate="{endDate}">
                          
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'GR'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/message/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'GR')">
                        <option value="{startDate}" endDate="{endDate}">
                          
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'SA'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/message/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'SA')">
                        <option value="{startDate}" endDate="{endDate}">
                          
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>
                
                
                <xsl:when test="@code = 'SU'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/message/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'SU')">
                        <option value="{startDate}" endDate="{endDate}">
                          
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>
                
              </xsl:choose>
            </td>
          </tr>
        </xsl:for-each>

      </table>

      <div id="noDeliveryDatesDiv" style="display:none">
        <table width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr>
          <td>&nbsp;</td>
          <td>
            <font color="red">
            No Delivery Dates are available
            </font>
          </td>
        </tr>
        </table>
      </div>

      <script type="text/javascript" language="javascript">
      <![CDATA[
      if (hasShippingMethods == 'N') {
          document.getElementById('noDeliveryDatesDiv').style.display='block';
      } else {
          document.getElementById('noDeliveryDatesDiv').style.display='none';
      }
      ]]>
      </script>

    </td>
  </tr>
  <tr id="page_ship_method_validation_message_row" style="display:none">
    <td/>
    <td id="page_ship_method_validation_message_cell" colspan="10" class="RequiredFieldTxt" style="padding-left:4.5em;"/>
  </tr>


</xsl:template>
</xsl:stylesheet>