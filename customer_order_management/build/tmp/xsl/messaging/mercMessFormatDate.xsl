<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
						 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
						 indent="yes"/>


<xsl:template name="mercMessFormatDate">
<xsl:param name="dateString"/>

	<xsl:variable name="length" select="string-length($dateString)"/>
	<xsl:value-of select="substring($dateString, 1, $length - 4)"/>
	<xsl:value-of select="substring($dateString, $length - 1)"/>

</xsl:template>


<xsl:template name="extractDate">
	<xsl:param name="timestamp"/>

	<xsl:if test="$timestamp != ''">
		<xsl:variable name="month" select="substring($timestamp, 6, 2)"/>
		<xsl:variable name="day" select="substring($timestamp, 9, 2)"/>
		<xsl:variable name="year" select="substring($timestamp, 3, 2)"/>
		<xsl:value-of select="concat($month, '/', $day, '/', $year)"/>
	</xsl:if>
</xsl:template>


<xsl:template name="extractTime">
	<xsl:param name="timestamp"/>

	<xsl:if test="$timestamp != ''">
		<xsl:variable name="militaryHours" select="substring($timestamp, 12, 2)"/>

		<xsl:variable name="hours">
			<xsl:choose>
				<xsl:when test="$militaryHours = 0">
					<xsl:text>12</xsl:text>
				</xsl:when>

				<xsl:when test="$militaryHours &gt; 12">
					<xsl:value-of select="$militaryHours - 12"/>
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="$militaryHours"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="meridian">
			<xsl:choose>
				<xsl:when test="$militaryHours &lt; 12">
					<xsl:text>AM</xsl:text>
				</xsl:when>

				<xsl:otherwise>
					<xsl:text>PM</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="minutes" select="substring($timestamp, 15, 2)"/>

		<xsl:value-of select="concat($hours, ':', $minutes, $meridian)"/>
	</xsl:if>
</xsl:template>


<xsl:template name="extractViewDate">
  <xsl:param name="timestamp"/>
  
  <xsl:variable name="date" select="substring($timestamp, 5, 6)"/>
  <xsl:variable name="day" select="substring($timestamp, 1, 3)"/>
  
  <xsl:value-of select="concat(date, ' ', day)"/>  
</xsl:template>

</xsl:stylesheet>