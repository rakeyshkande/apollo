
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>


	<xsl:template name="mercMessSendButton">
		<xsl:param name="action_type"/>
		
			
			
				<table class="PaneSection">
					<tr>
						<td align="center">
							<button type="button" class="BlueButton" id="sendButton" accesskey="N" onclick="checkLockProcessRequest('{$action_type}');" style="margin-right: 10px;">Se(n)d</button>						
						</td>
					</tr>
				</table>
			
		
	</xsl:template>
</xsl:stylesheet>