
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>

	<xsl:template name="mercMessReasonText">
		<xsl:choose>
			<xsl:when test="$destination ='' ">
				<tr>
					<td class="label">Message:</td>

					<td class="datum">
						<textarea name="message_comment" id="message_comment" rows="7" cols="50">
              <xsl:call-template name="replace">
                <xsl:with-param name="string" select="$xvMessageComment"/>
              </xsl:call-template>
						</textarea>
					</td>
				</tr>
			</xsl:when>
			<xsl:otherwise>
				<tr>
					<td class="label">Message:</td>

					<td class="datum">
						<textarea name="message_system_reason" id="message_system_reason" rows="7" cols="50" readOnly="true" style="background-color:#C0C0C0">
              <xsl:call-template name="replace">
                <xsl:with-param name="string" select="message/message_system_reason"/>
              </xsl:call-template>
						</textarea>
					</td>
				</tr>
				<tr>
					<td class="label">Additional Message:</td>

					<td class="datum">
						<textarea name="message_comment" id="message_comment" rows="7" cols="50">
              <xsl:call-template name="replace">
                <xsl:with-param name="string" select="$xvMessageComment"/>
              </xsl:call-template>
						</textarea>
					</td>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>