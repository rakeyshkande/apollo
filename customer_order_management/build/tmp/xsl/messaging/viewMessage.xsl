<!DOCTYPE stylesheet [
	<!ENTITY nbsp "&#160;">
]>



<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="mercMessDropDown.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="formattingTemplates.xsl"/>
    <xsl:key name="security" match="/message/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>
<xsl:template match="/">

	<html>
		<head>
			<title><xsl:value-of select="$xvHeaderName"/></title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/clock.js">;</script>
			<script type="text/javascript" src="js/tabIndexMaintenance.js">;</script>
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script type="text/javascript" src="js/FormChek.js">;</script>
			<script>
				function init()
				{
          setViewMessagesIndexes();
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;

					untabHeader();
				}

        /*******************************************************************************
        *  setViewMessagesIndexes()
        *  Sets tabs for View Messages Page.
        *  See tabIndexMaintenance.js
        *******************************************************************************/
        function setViewMessagesIndexes()
        {
          untabAllElements();
          var begIndex = findBeginIndex();
          var tabArray = new Array('reasonTextArea', 'recipientOrderButton',
            'historyButton', 'orderCommentsButton', 'emailButton',
            'lettersButton', 'refundButton', 'floristInquiryButton',
            'searchButton', 'backButton', 'mainMenuButton');
          begIndex = setTabIndexes(tabArray, begIndex);
          return begIndex;
        }
			</script>

			<style>
        		td.label {
        			width: 225px;
        		}
			</style>
		</head>

		<body onload="init();">
			<form id="viewMessageForm" name="viewMessageForm" method="post">

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<input type="hidden" name="subsequent_action" id="subsequent_action"/>
				<input type="hidden" name="ftd_allowed" id="ftd_allowed" value="{$ftd_allowed}"/>
                                <input type="hidden" name="personalized" id="personalized" value="{$personalized}"/>
				<input type="hidden" name="in_transit" id="in_transit" value="{$in_transit}"/>
				<input type="hidden" name="ship_date" id="ship_date" value="{$xvShipDate}"/>
				<xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>

				<script>updateCurrentUsers();</script>
			</form>

			<div id="content">
				<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
					<tr>
						<td>
							<table class="PaneSection">
								<tr>
									<td class="banner">
										Message Detail
										<xsl:if test="$xvMercuryMessageType">
											&nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/>
										</xsl:if>
									</td>
								</tr>
							</table>

							<div style="overflow: auto; padding-right: 16px;">
								<br></br>
								<div style = "FONT-SIZE: 13pt; MARGIN-LEFT: 25px; FONT-WEIGHT: bold;" id = "vm_delivery_info">Delivery Information:</div>
								<br></br>
								<table width="50%" cellpadding="0" cellspacing="0" style="DISPLAY: inline-block; MARGIN-RIGHT: -16px; float: left">
									<tr>
										<td class="label" id = "vm_recipient_name">
											Name:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientName"/>
										</td>
									</tr>

									<tr>
										<td class="label" id = "vm_recipient_address">
											Address:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientStreet"/>											
										</td>
									</tr>

									<tr>
										<td class="label" id = "vm_recipient_city_state_zip">
											City, State, Zip:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientCityStateZip"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_recipient_phone">
											Phone:
										</td>

										<td class="datum">
											<script type="text/javascript"> 
											  document.write(reformatUSPhoneWithSlash('<xsl:value-of select="$xvRecipientPhone"/>'));
										    </script>
											
										</td>
									</tr>

									<tr>
										<td class="label" id = "vm_delivery_date">
											Delivery Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDeliveryDate"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_order_date">
											Order Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDate"/>
										</td>
									</tr>

									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_message">
											Message:
										</td>

										<td class="datum">
											<textarea rows="7" cols="50" readOnly="true" name="reasonTextArea" >
                        <xsl:call-template name="replace">
                          <xsl:with-param name="string" select="$xvViewMessageComment"/>
                        </xsl:call-template>
                      </textarea>
										</td>
									</tr>

									<xsl:if test="$xvIsSubsequentMessageAllowed = 'true'">
										<tr>
											<td class="label">
												&nbsp;
											</td>
					
											<td class="datum">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td class="label">
												&nbsp;
											</td>
					
											<td class="datum">
												&nbsp;
											</td>
										</tr>
										<tr>
											<td class="label" style = "FONT-SIZE: 12pt">
												I WOULD LIKE TO:
											</td>

											<td class="datum">
												<xsl:call-template name="mercMessDropDown"/>
											</td>
										</tr>
										<tr>
											<td class="label">
												&nbsp;
											</td>
						
											<td class="datum">
												&nbsp;
											</td>
									  </tr>
									</xsl:if>
								</table> <!-- End of order -->
					              <table width="400px" cellpadding="0" cellspacing="0" style="FLOAT: right; MARGIN-TOP: -40px; MARGIN-RIGHT: -16px" >
									<tr>
										<td class="label" id = "vm_status">
											Status:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvStatus"/>
										</td>
									</tr>
									<tr>
										<td class="label" id = "vm_message_time">
											Message Time:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvStatusTimestamp"/>
										</td>
									</tr>
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td class="label" id = "vm_mercury_order">
											Mercury Order:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvMercuryOrderNumber"/>
										</td>
									</tr>
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td class="label" id = "vm_filler_code">
											Filler Code:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvFillingFloristCode"/>
										</td>
									</tr>
				
									<tr>
										<td class="label" id = "vm_sender_code">
											Sender Code:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvSendingFloristCode"/>
										</td>
									</tr>
									<tr>
										<td class="label" id = "vm_operator">
											Operator:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOperatorCode"/>
										</td>
									</tr>
								  </table>
				                
								<DIV id="compOrderDIV" style="display:none">
					                <TABLE width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-right: -16px;">
					                  <TR>
					                    <TD colspan="2">
					                      <HR/>
					                    </TD>
					                  </TR>
					                  <TR>
					                    <TD class="label"> Approval ID: </TD>
					                    <TD class="datum">
					                      <input tabindex="6" type="text" name="approval_id" id="approval_id" size="10" maxlength="30"/>
					          &nbsp;
					                      <SPAN style="color:red"> *** </SPAN>&nbsp;
					                      <SPAN style="color:green"> User ID of manager approving Comp Order. </SPAN>&nbsp;
					                    </TD>
					                  </TR>
					                  <TR>
					                    <TD class="label"> Approval Password: </TD>
					                    <TD class="datum">
					                      <input tabindex="7" type="password" name="approval_passwd" id="approval_passwd" size="10" maxlength="10"/>
					          &nbsp;
					                      <SPAN style="color:red"> *** </SPAN>&nbsp;
					                      <SPAN style="color:green"> Password of manager approving Comp Order. </SPAN>
					                    </TD>
					                  </TR>
					                  <tr>
											<td class="label">
												&nbsp;
											</td>
					
											<td class="datum">
												&nbsp;
											</td>
										</tr>
					                </TABLE>
              					</DIV>
				                
							</div> <!-- End of scrolling div -->
						</td>
					</tr>
				</table> <!-- End of mainTable -->
			</div>

			<div id="waitDiv" style="display: none;">
				<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
					<tr>
						<td width="100%">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
									<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

			<xsl:call-template name="mercMessButtons"/>
			<xsl:call-template name="footer"/>
		</body>
	</html>

</xsl:template>
<!--
<xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:choose>
        <xsl:when test="contains($string,'&#10;')">
            <xsl:value-of select="substring-before($string,'&#10;')"/>
            <br/>
            <xsl:call-template name="replace">
                <xsl:with-param name="string" select="substring-after($string,'&#10;')"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$string"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>
-->

</xsl:stylesheet>