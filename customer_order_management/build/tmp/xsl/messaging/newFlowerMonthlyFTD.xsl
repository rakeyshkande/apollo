
<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
	<xsl:import href="mercMessButtons.xsl"/>
	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessFormatPhone.xsl"/>
	<xsl:import href="dateRangeSelect.xsl"/>
	<xsl:import href="mercFTDFillerCode.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>
	<!--The temporary solution for modify order. Will be replaced by newFTD-PRE-MO.xsl after Modify Order goes live.-->
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>


	<xsl:template match="/">
	<xsl:variable name="xvRecipientCty" select="message/recipientCountry"/>
		<html>
			<head>
				<title>
					<xsl:value-of select="$xvHeaderName"/>
				</title>

				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
				<meta http-equiv="language" content="en-us"/>

				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
				<script type="text/javascript" src="js/commonUtil.js">;</script>
				<script type="text/javascript" src="js/clock.js">;</script>

				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
				<script>var actions = new Array();
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;
					var buttons = document.getElementsByTagName("BUTTON");<![CDATA[
					for(var i = 0; i < buttons.length; i++)
					{
						if(buttons[i].id != "mainMenuButton" && buttons[i].id != "sendButton"&& buttons[i].id != "lookupButton"&& buttons[i].id != "autoSelectButton")
						{
							actions[buttons[i].id] = buttons[i].onclick;
							buttons[i].onclick = checkButtonsNewFTD;
						}else if(buttons[i].id == "mainMenuButton"){
							buttons[i].onclick=mainMenuActionNewFTD;
						}
					}
					
					untabHeader();
					document.forms[0].filling_florist_code.focus();
					
				}
				
				
				
				function checkButtonsNewFTD()
{
	

					if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
					{
						document.getElementById("unlockFrame").src=getUnlockUrl();
				           
						actions[elementClicked.id]();
					}
				}
				
				
				function mainMenuActionNewFTD()
				{
				  if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
				  {
				  	  				           
				           document.getElementById("unlockFrame").src=getUnlockUrl();
				           doMainMenuAction();
					
				  }
				}
				
				
				function getUnlockUrl(){
				
					var url="messagingCancelUpdateUnLock.do?lock_app=COMMUNICATION&order_detail_id="+document.getElementById("order_detail_id").value
				           						+"&order_delivery_date="+document.getElementById("origin_delivery_date").value
				           						+getSecurityParams(false);
				           						
				           if(document.forms[0].origin_delivery_date_end.value!=''){
				           	url +="&order_delivery_date_end="+document.forms[0].origin_delivery_date_end.value;
				           }
				        return url;
				
				}

				
				
					

				function validateMessageForm()
				{
					var isValidFillingFloristCode=checkFloristCode("filling_florist_code", "invalidFloristCode");
																	
						
						//var isValidFirstChoice = checkFirstChoice("first_choice", "invalidFirstChoice");
					
						
						var isValidRecipientName = checkRecipientName("recipient_name", "invalidRecipientName");
						
						var isValidRecipientStreet = checkRecipientStreet("recipient_street", "invalidRecipientStreet");
						
						var isValidRecipientCity = checkRecipientCity("recipient_city", "invalidRecipientCityStateZip");
						
						var isValidRecipientState = checkRecipientState("recipient_state", "invalidRecipientCityStateZip");
						
						var isValidRecipientZip = checkRecipientZip("recipient_zip", "invalidRecipientCityStateZip");
						
						var isValidRecipientPhone = checkRecipientPhone("recipient_phone", "invalidRecipientPhone");
						
	
						var monthSelect = document.getElementById("delivery_month");
					
						var dateBox = document.getElementById("delivery_date");
						
						var dateField = document.getElementById("order_delivery_date");
						dateField.value = monthSelect.options[monthSelect.selectedIndex].value
									+ " "
									+ dateBox.value;
						
						var isValidOrderDeliveryDate = checkOrderDeliveryDate("delivery_month", "delivery_date","invalidOrderDeliveryDate");
				
												
	
						return (
				
				
							isValidFillingFloristCode
							//&& isValidFirstChoice
							&&isValidRecipientName
							&& isValidRecipientStreet
							&& isValidRecipientCity
							&& isValidRecipientZip
							&& isValidRecipientPhone
							&& isValidOrderDeliveryDate
							
							
						);
				}
											
				
				]]>
				

				function isValidDeliveryDate(){
				
						var isValidDateRange=true;
						var isValidOrderDeliveryDate = checkOrderDeliveryDate("delivery_month", "delivery_date","invalidOrderDeliveryDate");
					<xsl:if test="$xvOrderDeliveryDateEnd != ''">var isValidOrderDeliveryDateEnd = checkOrderDeliveryDate("delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");
				
								isValidDateRange = false;<![CDATA[
								if( isValidOrderDeliveryDate && isValidOrderDeliveryDateEnd)
								{
									isValidDateRange = checkDateRange("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");
								}
								]]></xsl:if><![CDATA[ 
						return (isValidOrderDeliveryDate && isValidDateRange);
						]]>}
				

				function doSendNewFTDMessageAction(actionType)
				{       //alert("in doSendNewFTDMessageAction");
					showWaitMessage("content", "wait");
					var isValidDateRange=true;
					var isValidOrderDeliveryDate = checkOrderDeliveryDate("delivery_month", "delivery_date","invalidOrderDeliveryDate");
						
						//alert("isValidOrderDeliveryDate="+isValidOrderDeliveryDate);
							<xsl:if test="$xvOrderDeliveryDateEnd != ''">var isValidOrderDeliveryDateEnd = checkOrderDeliveryDate("delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");
				
								isValidDateRange = false;<![CDATA[
								if( isValidOrderDeliveryDate && isValidOrderDeliveryDateEnd)
								{
									isValidDateRange = checkDateRange("delivery_month", "delivery_date", "delivery_month_end", "delivery_date_end", "invalidOrderDeliveryDateEnd");
								}
								]]></xsl:if><![CDATA[
						if(isValidOrderDeliveryDate && isValidDateRange){
							
							doNewFTDFormAction(actionType);
						}else{
						
							hideWaitMessage("content", "wait");
						}
						
						]]>
				}
				
				
				function doNewFTDFormAction(actionType){<![CDATA[
					document.getElementById("invalidFloristCode").style.display = "none";
					
					
						if(validateMessageForm())
						{
					
					
							document.forms[0].submit();
						
						}
						else
						{
							hideWaitMessage("content", "wait");
						}
					
					]]>
				
					
				
				}
				
				

				function checkStatusIframe(){
										
					var url = "checkFloristStatus.do?filling_florist_code="+document.forms[0].filling_florist_code.value<![CDATA[
									+ "&florist_status="
									+ document.forms[0].florist_status.value
									+ "&mercury_status="
									+ document.forms[0].mercury_status.value
									+ "&msg_message_type="
									+ document.forms[0].msg_message_type.value
									+"&call_out="+document.forms[0].call_out.checked
									+ "&order_detail_id="
									+ document.forms[0].order_detail_id.value
									+getDeliveryDateParams()
									+ getSecurityParams(false);
						//alert(url);
						FloristStatusIFrame.location.href = url;

				}
				
				function getDeliveryDateParams(){
				
					var dateUrl="&delivery_month="+document.forms[0].delivery_month.value
						    +"&delivery_date="+document.forms[0].delivery_date.value;
					
					
				]]>


					<xsl:if test="$xvOrderDeliveryDateEnd != ''"><![CDATA[
						dateUrl +="&delivery_month_end="+document.forms[0].delivery_month_end.value
							  +"&delivery_date_end="+document.forms[0].delivery_date_end.value;
						]]>
					</xsl:if>return dateUrl;
					}</script>

				<style>td.label {
        			width: 225px;
        		}</style>
			</head>

			<body onload="init();">
				<form id="newFTDForm" method="post" action="sendFlowerMonthlyFTDMessage.do">

					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<xsl:call-template name="messagingSecurityAndData"/>

					<iframe id="FloristStatusIFrame" name="FloristStatusIFrame" width="0" height="0" border="0" style="display: none"/>
					<iframe id="SendFTDIFrame" name="SendFTDIFrame" width="0" height="0" border="0" style="display: none;"/>
					<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
					<iframe id="checkLock" width="0px" height="0px" border="0"/>
					<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
					<input type="hidden" name="florist_status" id="florist_status"/>
					<input type="hidden" name="mercury_status" id="mercury_status"/>
					<input type="hidden" name="flower_monthly_prd_flag" id="flower_monthly_prd_flag" value="{normalize-space(message/flowerMonthlyProduct)}"/>
					<input type="hidden" name="origin_delivery_date" id="order_delivery_date" value="{message/deliveryDate}"/>
					<input type="hidden" name="origin_delivery_date_end" id="order_delivery_date_end" value="{$xvOrderDeliveryDateEnd}"/>
					<input type="hidden" name="order_date" id="order_date" value="{$xvOrderDateTimestamp}"/>
					<input type="hidden" name="product_id" id="product_id" value="{normalize-space(message/productId)}"/>
          <input type="hidden" name="message_source_code" id="message_source_code" value="{normalize-space(message/sourceCode)}"/>
					
					<xsl:choose>
						<xsl:when test="$destination ='' ">
							<xsl:call-template name="header">
								<xsl:with-param name="showSearchBox" select="false()"/>
								<xsl:with-param name="showPrinter" select="true()"/>
								<xsl:with-param name="showTime" select="true()"/>
								<xsl:with-param name="headerName" select="$xvHeaderName"/>

								<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
								<xsl:with-param name="indicator" select="$xvIndicator"/>
								<xsl:with-param name="brandName" select="$xvBrandName"/>
								<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
								<xsl:with-param name="showCSRIDs" select="true()"/>
								<xsl:with-param name="showBackButton" select="true()"/>
								<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
								<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="header">
								<xsl:with-param name="showSearchBox" select="false()"/>
								<xsl:with-param name="showPrinter" select="true()"/>
								<xsl:with-param name="showTime" select="true()"/>
								<xsl:with-param name="headerName" select="$xvHeaderName"/>

								<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
								<xsl:with-param name="indicator" select="$xvIndicator"/>
								<xsl:with-param name="brandName" select="$xvBrandName"/>
								<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
								<xsl:with-param name="showCSRIDs" select="true()"/>
								<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>

					<script>updateCurrentUsers();</script>

					<div id="content">
						<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td>
									<table class="PaneSection">
										<tr>
											<td class="banner">Message Detail
											
												&nbsp;-&nbsp;FTD</td>
										</tr>
									</table>

									<div style="overflow: auto; height: 329px; padding-right: 16px;">
										<table width="100%" cellpadding="0" cellspacing="0" style="margin-right: -16px;">
											<input type="hidden" name="call_out" id="call_out" value="false"/>

											<tr>
												<td class="label" style="vertical-align: middle;">FILLER CODE:</td>

												<td class="datum">


													<xsl:value-of select="$xvFillingFloristCode"/>
													<input type="hidden" name="filling_florist_code" maxlength="9" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
													<span id="invalidFloristCode" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>



											<tr>
												<td class="label">SENDER CODE:</td>

												<td class="datum">
													<xsl:value-of select="$xvSendingFloristCode"/>
													<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
												</td>
											</tr>

											<tr>
												<td class="label">DELIVER TO:</td>

												<td class="datum">
													<input type="text" size="50" maxlength="60" name="recipient_name" id="recipient_name" value="{$xvRecipientName}" onKeyPress="return noEnter();"/>
													<span id="invalidRecipientName" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>

											<tr>
												<td class="label">BUSINESS NAME:</td>

												<td class="datum">
													<input type="text" size="50" name="business_name" id="business_name" value="{normalize-space(message/businessName)}" onKeyPress="return noEnter();"/>
													<span id="invalidBusiness_name" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>

											<tr>
												<td class="label">STREET ADDRESS APT:</td>

												<td class="datum">
													<input type="text" size="50" maxlength="100" name="recipient_street" id="recipient_street" value="{$xvRecipientStreet}" onKeyPress="return noEnter();"/>
													<span id="invalidRecipientStreet" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>

											<tr>
												<td class="label">CITY, STATE, ZIP:</td>

												<td class="datum">
													<input type="text" size="30" maxlength="50" name="recipient_city" id="recipient_city" value="{$xvRecipientCity}" onKeyPress="return noEnter();"/>

													<select name="recipient_state" id="recipient_state">
														<xsl:for-each select="message/STATES/STATE">
															<option value="{id}">
																<xsl:if test="id =$xvRecipientState">
																	<xsl:attribute name="selected">true</xsl:attribute>
																</xsl:if>
																<xsl:value-of select="name"/>
															</option>
														</xsl:for-each>
													</select>

													<input type="text" size="9" maxlength="6" name="recipient_zip" id="recipient_zip" value="{$xvRecipientZip}" onKeyPress="return noEnter();"/>
													<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>

											<tr>
												<td class="label">COUNTRY:</td>

												<td class="datum">

													<xsl:choose>
														<xsl:when test="$xvRecipientCty='US' or $xvRecipientCty='CA'">
															<input type="text" size="2" maxlength="2" name="recipient_country" id="recipient_country" value="{$xvRecipientCty}" onKeyPress="return noEnter();"/>
														</xsl:when>
														<xsl:otherwise>
															<input type="hidden" name="recipient_country" id="recipient_country" value="{$xvRecipientCty}"/>
															<xsl:value-of select="$xvRecipientCty"/>
														</xsl:otherwise>
													</xsl:choose>
													<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>

											<tr>
												<td class="label">PHONE:</td>

												<td class="datum">

													<input type="text" maxlength="3" size="3" name="recipient_phone_area" id="recipient_phone_area" value="{substring($xvRecipientPhone, 1, 3)}" onKeyPress="return noEnter();"/>/<input type="text" maxlength="3" size="3" name="recipient_phone_prefix" id="recipient_phone_prefix" value="{substring($xvRecipientPhone, 4, 3)}" onKeyPress="return noEnter();"/>
												-<input type="text" maxlength="4" size="4" name="recipient_phone_number" id="recipient_phone_number" value="{substring($xvRecipientPhone, 7)}" onKeyPress="return noEnter();"/>
													<span id="invalidRecipientPhone" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>



											<xsl:call-template name="dateRangeSelect"/>

											<tr>
												<td class="label">SHIP METHOD:</td>

												<td class="datum">
												        <select name="ship_method" id="ship_method">
														<xsl:for-each select="message/ShipMethods/ShipMethod">
															<option value="{@name}">
																<xsl:if test="@name =/message/shipMethod">
																	<xsl:attribute name="selected">true</xsl:attribute>
																</xsl:if>
																<xsl:value-of select="@value"/>
															</option>
														</xsl:for-each>
													</select>
													
													
												</td>
											</tr>
											
											<tr>
												<td class="label">FIRST CHOICE:</td>
					
												<td class="datum">
													<xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>
													
												</td>
											</tr>					

											<tr>
												<td class="label">Vendor Cost:</td>

												<td class="datum">
													<input type="hidden" name="florist_prd_price" id="florist_prd_price" value="{$xvNewPrice}"/>
													<xsl:value-of select="$xvNewPrice"/>
												</td>
											</tr>

											<tr>
												<td class="label">CARD:</td>

												<td class="datum">
													<input type="text" name="card_message" id="card_message" size="70" maxlength="240" onKeyPress="return noEnter();">
														<xsl:attribute name="value">
															<xsl:choose>
																<xsl:when test="$xvCardMessage = ''">
																	<xsl:text>NO MESSAGE ENTERED</xsl:text>
																</xsl:when>

																<xsl:otherwise>
																	<xsl:value-of select="$xvCardMessage"/>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:attribute>
													</input>
													<span id="invalidCardMessage" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>


											<tr>
												<td class="label">SPECIAL INSTRUCTIONS:</td>

												<td class="datum">
													<input type="hidden" name="spec_instruction" id="spec_instruction" >
														<xsl:attribute name="value">
															<xsl:choose>
																<xsl:when test="$xvInstructions = ''">
																	<xsl:text>NONE</xsl:text>
																</xsl:when>

																<xsl:otherwise>
																	<xsl:value-of select="$xvInstructions"/>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:attribute>
													</input>
                          <xsl:choose>
                            <xsl:when test="$xvInstructions = ''">
                              <xsl:text>NONE</xsl:text>
                            </xsl:when>

                            <xsl:otherwise>
                              <xsl:value-of select="$xvInstructions"/>
                            </xsl:otherwise>
                          </xsl:choose>
                          
												</td>
											</tr>

											<tr>
												<td class="label">OPERATOR:</td>

												<td class="datum">
													<xsl:value-of select="$xvOperatorCode"/>
												</td>
												<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
											</tr>
										</table>
										<!-- End of order -->
									</div>
									<!-- End of scrolling div -->

									<table width="100%">
										<tr align="center">
											<td>


												<button type="button" class="BlueButton" accesskey="N" id="sendButton" onclick="checkLockProcessRequest('send_new_ftd');">Se(n)d</button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End of mainTable -->
					</div>

					<div id="waitDiv" style="display: none;">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
											<td id="waitTD" align="left" width="50%" class="WaitMessage"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>

					<xsl:call-template name="mercMessButtons"/>
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="replace">
		<xsl:param name="string"/>
		<xsl:choose>
			<xsl:when test="contains($string,' ')">
				<xsl:value-of select="substring-before($string,' ')" disable-output-escaping="yes"/>
				<br/>
				<xsl:call-template name="replace">
					<xsl:with-param name="string" select="substring-after($string,' ')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string" disable-output-escaping="yes"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>