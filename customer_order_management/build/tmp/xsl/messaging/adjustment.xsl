
<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
<!ENTITY amp "&#233;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
	<xsl:import href="mercMessButtons.xsl"/>
	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessCommentTable.xsl"/>
	<xsl:import href="mercMessReasonDropDown.xsl"/>
  <xsl:import href="formattingTemplates.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>

	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>
	<xsl:variable name="xvMessageSubType" select="normalize-space(message/subType)"/>
        <xsl:variable name="ADJCancelReasonCode" select="normalize-space(message/ADJCancelReasonCode)"/>
	
	<xsl:template match="/">

		<html>
			<head>
				<title>
					<xsl:value-of select="$xvHeaderName"/>
				</title>

				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
				<meta http-equiv="language" content="en-us"/>

				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>

				<script type="text/javascript" src="js/clock.js">;</script>

				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
				<script type="text/javascript" src="js/commonUtil.js">;</script>
				<script><![CDATA[
					var actions = new Array();
					function init()
					{
                                                document.oncontextmenu = contextMenuHandler;
						document.onkeydown = backKeyHandler;
						
						
							var buttons = document.getElementsByTagName("BUTTON");
							
							for(var i = 0; i < buttons.length; i++)
							{
								if(buttons[i].id != "sendButton")
								{
									actions[buttons[i].id] = buttons[i].onclick;
									buttons[i].onclick = forceComplete;
								}
							}
							

						untabHeader();
					}
                                        
                                        function forceComplete()
                                        {
                                            	showModalAlert("Please complete the adjustment.");
                                        }
			
									
						]]>function validateMessageForm()
					{
						var isValidPrice;
						var isValidReasonText;
					<xsl:choose>
						<xsl:when test="$xvMessageSubType = 'S'"><![CDATA[
							isValidPrice=true;
							]]>
						</xsl:when>
						<xsl:otherwise><![CDATA[
							isValidPrice = checkADJPrice("message_price", "invalidPrice");
							]]>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="$xvMessageCategory = 'Mercury'"><![CDATA[
							isValidReasonText=true;
							]]>
						</xsl:when>
						<xsl:otherwise><![CDATA[
							isValidReasonText = isRequired("message_comment", "invalidReasonText");
							]]>
						</xsl:otherwise>
					</xsl:choose>
					
					        var isValidReasonCode = checkReasonCode("reason_codes", "invalidReasonCode");

						return (isValidPrice &amp;&amp; isValidReasonCode &amp;&amp; isValidReasonText );
					}

					function doSendAdjustmentAction()
					{
						showWaitMessage("content", "wait");
						if(validateMessageForm())
						{
							document.forms[0].submit();
						}
						else
						{
							hideWaitMessage("content", "wait");
						}		
						
						
					}
					function onClose()
					{
					<xsl:if test="$xvMessageSubType = 'S'"><![CDATA[
							if((event.screenX > 782) && (event.screenY < 13))
							{
								event.returnValue = "You must send send this message before closing."
													+ "\nChoose 'Cancel' if you haven't yet sent this message.";
							}
							]]>
					</xsl:if>}</script>

				<style>td.label {
					width: 128px;
				}</style>
			</head>

			<body onload="init();" onbeforeunload="onClose();">
				<form id="adjustmentForm" method="post" action="sendAdjustmentMessage.do">

					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<xsl:call-template name="messagingSecurityAndData"/>

					<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
					<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
					<input type="hidden" name="adj_cancel_reason_code" id="adj_cancel_reason_code" value="{$ADJCancelReasonCode}"/>
					<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
					<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
					<iframe id="checkLock" width="0px" height="0px" border="0"/>
					<xsl:choose>				
				<xsl:when test="$destination ='' ">
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:otherwise>
				</xsl:choose>

					<script>updateCurrentUsers();</script>

					<div id="content">
						<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td>
									<table class="PaneSection">
										<tr>
											<td class="banner">Message Detail
												<xsl:if test="$xvMercuryMessageType">
													&nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/></xsl:if>
											</td>
										</tr>
									</table>

									<table>
										<tr>
											<td width="60%">
												<div style="overflow: auto; height: 321px;">
													<table cellspacing="0" cellpadding="0">
														<tr>
															<td class="label">MERCURY ID:</td>

															<td class="datum">
																<xsl:value-of select="$xvMercuryOrderNumber"/>
															</td>
														</tr>

														<tr>
															<td class="label">COMBINED REPORT NO:</td>

															<td class="datum">
																<xsl:value-of select="$xvCombinedReportNumber"/>
															</td>
															<input type="hidden" name="combined_report_number" id="combined_report_number" value="{$xvCombinedReportNumber}"/>
														</tr>

														<tr>
															<td class="label">FILLER CODE:</td>

															<td class="datum">
																<xsl:value-of select="$xvFillingFloristCode"/>
															</td>
														</tr>

														<tr>
															<td class="label">SENDER CODE:</td>

															<td class="datum">
																<xsl:value-of select="$xvSendingFloristCode"/>
															</td>
														</tr>

														<tr>
															<td class="label">ORDER DATE:</td>

															<td class="datum">
																<xsl:value-of select="$xvOrderDate"/>
															</td>
														</tr>

														<tr>
															<td class="label">RECIPIENT:</td>

															<td class="datum">
																<xsl:value-of select="$xvRecipientName"/>
															</td>
														</tr>

														<tr>
															<td class="label">STREET ADDRESS APT:</td>

															<td class="datum">
																<xsl:value-of select="$xvRecipientStreet"/>
															</td>
														</tr>

														<tr>
															<td class="label">CITY, STATE, ZIP:</td>

															<td class="datum">
																<xsl:value-of select="$xvRecipientCityStateZip"/>
															</td>
														</tr>

														<tr>
															<td class="label">DELIVERY DATE:</td>

															<td class="datum">
																<xsl:value-of select="$xvOrderDeliveryDate"/>
															</td>
														</tr>

														<tr>
															<td class="label">TEXT OR REASON:</td>

															<td class="datum">
																<textarea name="message_comment" id="message_comment" rows="7" cols="50">
                                  <xsl:call-template name="replace">
                                    <xsl:with-param name="string" select="$xvMessageComment"/>
                                  </xsl:call-template>
																</textarea>
															</td>
														</tr>
														
														<tr>
															<td/>

															<td class="datum">
																<span id="invalidReasonText" class="RequiredFieldTxt" style="display: none;">
																</span>
															</td>
														</tr>
														
														<tr>
															<td class="label">OLD MERC VALUE:</td>

															<td class="datum">
																<xsl:choose>
																	<xsl:when test="$xvMessageSubType = 'S'">
																		<xsl:value-of select="$xvOldPrice"/>
																		<input type="hidden" name="message_old_price" id="message_old_price" value="{$xvOldPrice}"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="$xvNewPrice"/>
																		<input type="hidden" name="message_old_price" id="message_old_price" value="{$xvNewPrice}"/>
																	</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>

														<tr>
															<td class="label" style="vertical-align: middle;">NEW MERC VALUE:</td>

															<td class="datum">
																<xsl:choose>
																	<xsl:when test="$xvMessageSubType = 'S'">
																		<xsl:value-of select="$xvNewPrice"/>
																		<input type="hidden" name="message_price" id="message_price" value="{$xvNewPrice}"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<input type="text" name="message_price" id="message_price" size="6" maxlength="8" onKeyPress="return noEnter();"/>
																		<span id="invalidPrice" class="RequiredFieldTxt" style="display: none;">
																		</span>
																	</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>

														<tr>
															<td class="label" style="vertical-align: middle;">REASON CODE:</td>

															<td class="datum">
																<xsl:call-template name="mercMessReasonDropDown"/>
															</td>
														</tr>

														<tr>
															<td/>

															<td class="datum">
																<span id="invalidReasonCode" class="RequiredFieldTxt" style="display: none;">
																</span>
															</td>
														</tr>

														<tr>
															<td class="label">OPERATOR:</td>

															<td class="datum">
																<xsl:value-of select="$xvOperatorCode"/>
																<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
															</td>
														</tr>
														
														
													</table>
													<!-- End of order -->
												</div>
												<!-- End of scrolling div -->
											</td>

											<td width="40%">
												<xsl:call-template name="mercMessCommentTable"/>
											</td>
										</tr>
									</table>

									<table class="PaneSection">
										<tr>
											<td align="center">
												<button type="button" class="BlueButton" id="sendButton" accesskey="N" onclick="checkLockProcessRequest('send_adjustment');">Se(n)d</button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End of mainTable -->
					</div>

					<div id="waitDiv" style="display: none;">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
											<td id="waitTD" align="left" width="50%" class="WaitMessage"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>

					<xsl:call-template name="mercMessButtons"/>
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->
