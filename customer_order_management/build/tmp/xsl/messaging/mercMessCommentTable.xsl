<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
						 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
						 indent="yes"/>


<xsl:template name="mercMessCommentTable">

	<div style="overflow: auto; height: 321px; border: 1px solid #006699;">
		<!-- Message Comment list -->
		<table>
			<xsl:for-each select="message/HotKeys/HotKey">
				<tr>
					<td>
						<a class="reason" tabindex="-1" onclick="addReason();" href="#">
							&lt;&lt;<xsl:value-of select="@value"/>
						</a>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</div> <!-- End of Message Comment scrolling div -->

</xsl:template>

</xsl:stylesheet>