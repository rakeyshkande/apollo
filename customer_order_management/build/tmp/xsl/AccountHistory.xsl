
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - Account History</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript"><![CDATA[
// init code
function init()
{
// nothing
}

//Disable buttons
function buttonDis(page)
{
  if (page < 2)
  {
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
  }
	if (page > CustomerHistory.total_pages.value-1)
	{
    document.getElementById("nextItem").disabled= "true";
   	document.getElementById("lastItem").disabled= "true";
    return;
  }
}
	
function loadHistoryAction()
{
		document.CustomerHistory.action = "loadHistory.do";
	 	document.CustomerHistory.submit();
}

]]></script>
	<style>
		.messageContainer .lineNumber{
			width: 5%;
			vertical-align: top;
		}
		
		.messageContainer .date{
			width: 15%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}
		
		.messageContainer .userId{
			width: 15%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}
		
		.messageContainer .activity{
			width: 55%;
			text-align: left;
			word-break: break-all;
			vertical-align: top;
		}
	</style>
</head>
<body onload="init();buttonDis(CustomerHistory.page_position.value);">
	<xsl:call-template name="header">
	<xsl:with-param name="headerName"  select="'Account History'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="true()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
	<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
	<form name="CustomerHistory" method="post" action="">
        <xsl:call-template name="securityanddata"/>
        <xsl:call-template name="decisionResultData"/>
		<input type="hidden" name="action_type" id="action_type" value="{key('pagedata', 'action_type')/value}"/>
		<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>
		<input type="hidden" name="order_guid" id="order_guid" value="{key('pagedata', 'order_guid')/value}"/>
		<input type="hidden" name="history_type" id="history_type" value="Customer"/>
		<input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
		<input type="hidden" name="total_pages" id="total_pages" value="{key('pagedata', 'total_pages')/value}"/>
		<input type="hidden" name="start_position" id="start_position" value="{key('pagedata', 'start_position')/value}"/>
		<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			<tr>
				<td id="lockError" class="ErrorMessage"></td>
			</tr>
		</table>
		<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td>
					<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
						<tr>
							<td colspan="3" align="left">
								<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
									<tr>
										<td>
											<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
												<tr>
													<td>
														<div class="messageContainer" style="padding-right: 16px; width: 98%;">
															<table width="100%" border="0" cellpadding="1" cellspacing="1">
																<tr>
																	<th class="lineNumber"></th>
																	<th class="date">Date</th>
																	<th class="userId">UserID</th>
																	<th colspan="2" class="activity">ACTIVITY</th>
																</tr>
															</table>
														</div>
														<table width="100%" cellpadding="0" border="0" cellspacing="0">
															<tr>
																<td colspan="6">
																	<hr/>
																</td>
															</tr>
														</table>
														<div class="messageContainer" style="width: 99%; overflow: auto; padding-right: 16px; height: 150px;">
															<table width="100%" cellpadding="1" border="0" cellspacing="1" style="margin-right: -16px;">
																<xsl:for-each select="root/CustomerHistory/History">
																	<tr>
																		<td class="lineNumber">
																			<script type="text/javascript">document.write(CustomerHistory.start_position.value); CustomerHistory.start_position.value++</script>.
																		</td>
																		<td class="date">
																			<xsl:value-of select="created_on"/>
																		</td>
																		<td class="userId">
																			<xsl:value-of select="csr_id"/>
																		</td>
																		<td class="activity">
																			<xsl:value-of select="activity"/>
																		</td>
																	</tr>
																</xsl:for-each>
															</table>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="4"><hr/></td>
									</tr>
									<tr>
										<td align="right">
											<table>				
												<tr>
													<td width="15%" class="label" align="right">Page&nbsp;
														<xsl:for-each select="key('pagedata', 'page_position')">
														<xsl:value-of select="value" />
														</xsl:for-each>
														&nbsp;of&nbsp;
														<xsl:for-each select="key('pagedata', 'total_pages')">
              											<xsl:if test="value = '0'">
   															1
                                  						</xsl:if>
                                  						<xsl:if test="value != '0'">
           													<xsl:value-of select="value" />
                                  						</xsl:if>
														</xsl:for-each>
													</td>
												</tr>
												<tr>
                            						<td width="50%" align="right">
                            							<xsl:choose>
	                              							<xsl:when test="key('pagedata','total_pages')/value !='0'">
	                              								<button type="button" name="firstItem" tabindex="1" class="arrowButton"   id="firstItem" onclick="loadHistoryAction(CustomerHistory.page_position.value = 1);">
		                              								7
	                         									</button>
	                              							</xsl:when>
	                          								<xsl:when test="key('pagedata','total_pages')/value ='0'">
	                              								<button type="button" name="firstItem" tabindex="1" class="arrowButton"  id="firstItem" onclick="">
	                        										7	
	                              								</button>
	                              							</xsl:when>
                              							</xsl:choose>
                             							<xsl:choose>
						                              		<xsl:when test="key('pagedata','total_pages')/value !='0'">
								                              	<button type="button" name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="loadHistoryAction(--CustomerHistory.page_position.value);">
									                              	3
								                              	</button>
						                              		</xsl:when>
						                              		<xsl:when test="key('pagedata','total_pages')/value ='0'">
								                              	<button type="button" name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="">
									                              	3
								                              	</button>
						                              		</xsl:when>
						                              		</xsl:choose>
                            								<xsl:choose>
								                              	<xsl:when test="key('pagedata','total_pages')/value !='0'">
								                              		<button type="button" name="nextItem" tabindex="1" class="arrowButton" id="nextItem" onclick="loadHistoryAction(++CustomerHistory.page_position.value);">
									                              		4
								                              		</button>
							                              		</xsl:when>
								                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
								                              		<button type="button" name="nextItem" tabindex="1" class="arrowButton" id="nextItem" onclick="">
									                              		4
								                              		</button>
								                              	</xsl:when>
                              								</xsl:choose>
                             								<xsl:choose>
								                              	<xsl:when test="key('pagedata','total_pages')/value !='0'">
								                              		<button type="button" name="lastItem" tabindex="1" class="arrowButton" id="lastItem" onclick="loadHistoryAction(CustomerHistory.page_position.value = CustomerHistory.total_pages.value);">
									                              		8
								                              		</button>
						                              			</xsl:when>
								                              	<xsl:when test="key('pagedata','total_pages')/value ='0'">
								                              		<button type="button" name="lastItem" tabindex="1" class="arrowButton" id="lastItem" onclick="">
									                              		8
								                              		</button>
								                              	</xsl:when>
								                              	</xsl:choose>
							                             	</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3" align="left"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<table width="98%" border="0" align="center">				
				<tr align="right">
					<td width="20%" align="right">
						<button type="button" class="blueButton" name="backButton" tabindex="9" onclick="javascript:doBackAction();">(B)ack</button>
  							<br />
						<button type="button" class="bigBlueButton" name="mainMenu" tabindex="10" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu</button>
					</td>
				</tr>
			</table>
			<div id="waitDiv" style="display:none">
				<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
					<tr>
						<td width="100%">
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
									<td id="waitTD" width="50%" class="waitMessage"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<xsl:call-template name="footer"></xsl:call-template>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\CustomerComments.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->