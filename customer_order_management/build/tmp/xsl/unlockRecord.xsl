<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY reg "&#xAE;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!--variables-->
<xsl:variable name="in_csr_id" select="key('pageData','in_csr_id')/value"/>
<xsl:variable name="message_display" select="key('pageData','message_display')/value"/>

<xsl:template match="/">
<html>
  <head>
     <title>FTD - Unlock Record</title>
      <link rel="stylesheet" href="css/ftd.css"/>
      <script type="text/javascript" src="js/commonUtil.js"/>
      <script type="text/javascript" src="js/util.js"/>
      <script type="text/javascript" src="js/FormChek.js"/>
      <script type="text/javascript" src="js/unlockRecord.js"/>
  </head>
  <body id="body" onload="javascript:init();">

  <xsl:call-template name="addHeader"/>
  <form name="unlockRecord" method="post" onkeypress="doGetCsrLockedRecords();" >
  	<input type="hidden" name="error_message" id="error_message" value="{$message_display}" />
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- content to be hidden once the search begins -->
    <div id="content" style="display:block">
            
       <table class="mainTable" align="center" cellpadding="1" cellspacing="1" width="98%">
          <tr>
          <td><!--Content table-->
            <table border="0" class="innerTable" width="100%" align="center">
              <tr><td colspan="6">&nbsp;</td></tr>
              <tr>
                <td class="labelpad" width="24%" >
                  Csr Id:
                </td>
                <td >
                  <input type="text" value="{$in_csr_id}" tabindex="1" maxlength="20" id="in_csr_id" name="in_csr_id"/>
                </td>
                <td width="25%" align="right">
                  &nbsp;
                </td>
                <td>
                  <span id="errorCsrId" class="RequiredFieldTxt" style="display:none;text-align:left">
                    &nbsp;&nbsp;Invalid csr id.
                  </span>
                </td>

              </tr>
              
              <tr>
                <td class="label" colspan="3">
                  &nbsp;
                </td>
                <td  class="label" align="center">
                  <button type="button" id="clear" class="bluebutton" accesskey="C" tabindex="2" onclick="doClearFields();">(C)lear all Fields</button>
                  &nbsp;
                  <button type="button" id="getCsrLockedRecords" class="bluebutton" tabindex="3" accesskey="G" onclick="javascript:doGetCsrLockedRecords();">(G)et Csr Locked Records</button><br/>
                </td>
              </tr>
            </table>
          </td><!--Content table-->
           </tr>
       </table>
       
      <table align="center" width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr><td colspan = "3">&nbsp;</td></tr>
        <tr>
          <td width="25%"></td>
          <td valign="top" align="center" width="49%">
            &nbsp;
          </td>
          <td width="49%" align="right">
            <button type="button" id="mainMenu" class="bluebutton" accesskey="B" tabindex="4" onclick="javascript:doMainMenuAction();">(B)ack to Main Menu</button>
          </td>
        </tr>
      </table>
    </div>

    <!-- Used to dislay Processing... message to user -->
    <div id="waitDiv" style="display:none">
       <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="2" cellspacing="1" >
          <tr>
            <td width="100%">
              <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                           <td id="waitTD"  width="50%" class="waitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
    </div>
    <!-- call footer template-->
    <xsl:call-template name="footer"/>
    </form>
  </body>
</html>
</xsl:template>

<!--Actual Call to the Header.xsl page -->
<xsl:template name="addHeader">
  <xsl:call-template name="header">
    <xsl:with-param name="showPrinter" select="false()"/>
    <xsl:with-param name="showBackButton" select="true()"/>
    <xsl:with-param name="showSearchBox" select="false()"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="headerName" select="'Unlock Record'"/>
    <xsl:with-param name="backButtonLabel" select="'(B)ack to Main Menu'"/>
    <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
  </xsl:call-template>
</xsl:template>
</xsl:stylesheet>