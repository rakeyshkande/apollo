<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="refundAndAddBill.xsl"/>
<xsl:import href="cusProduct.xsl"/>

<!-- keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:variable name="ORIGINALCREDITCARD" select="root/LAST_CREDITCARDS/LAST_CREDITCARD"/>

<!-- format xml numbers if not present -->
<xsl:decimal-format NaN="(0.00)"  name="noDiscount"/>
<xsl:decimal-format NaN="" name="notANumber"/>
<xsl:decimal-format NaN="0.00" name="zeroValue"/>

<!-- set of variables for column headers since many are the same -->
<xsl:variable name="merch" select="'Merch'"/>
<xsl:variable name="addOn" select="'Add-On'"/>
<xsl:variable name="servShip" select="'Service/Shipping'"/>
<xsl:variable name="tax" select="'Tax'"/>
<xsl:variable name="discount" select="'Discount'"/>
<xsl:variable name="total" select="'Total'"/>

<!-- variables -->
<xsl:variable name="currentCCType" select="$ORIGINALCREDITCARD/cc_type"/>
<xsl:variable name="currentCCExp" select="$ORIGINALCREDITCARD/cc_expiration"/>
<xsl:variable name="currentCC" select="$ORIGINALCREDITCARD/cc_number"/>



<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
  <html>
    <head>
       <title>FTD - Billing Review</title>
       <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
       <script type="text/javascript" src="js/FormChek.js"/>
       <script type="text/javascript" src="js/util.js"/>
       <script type="text/javascript" src="js/commonUtil.js" />
       <!-- New JS file for billingReview specific functionality -->
       <script type="text/javascript" src="js/billingReview.js"/>
       <style type="text/css">
		.merchTD{
		   width: 12%;
		}
		.addOnTD{
		   width:12%;
		}
		.servShipTD{
		 width:19%;
		}
		.taxTD{
		   width:10%;
		}
		.discountTD{
		   width:12%;
		}
		.totalTD{
		   width:10%;
		}
		#actionButtons{
		   padding-top:.3em;
       margin:auto;
       text-align:center;
		}
       </style>

       <script type="text/javascript">
         var selectedCCType = "<xsl:value-of select="$currentCCType"/>";
         var selectedCCExp = "<xsl:value-of select="$currentCCExp"/>";	     
<![CDATA[

       function init(){
 	        addDefaultListeners();
 	        buildCCYearList('exp_date_year');
          populateCreditCardInfo('pay_type','exp_date_month','exp_date_year', selectedCCType, selectedCCExp);

 	     }
       ]]>
       </script>
    </head>
    <body onload="init();">
 <form name="billingReviewForm" id="billingReviewForm" method="post" action="">
         <iframe id="checkLock" width="0px" height="0px" border="0px"/>

        <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0px"/>

 	      <iframe id="billingReviewActions" width="0px" height="0px" border="0px"/>

       <!-- Customer Product params -->
      <xsl:call-template name="cusProduct-params">
        <xsl:with-param name="subheader" select="subheader"/>
      </xsl:call-template>


  <div id="mainContent" style="display:block">
	 	    

 	   <!-- include security -->
 	   <xsl:call-template name="securityanddata"/>
 	   <xsl:call-template name="decisionResultData"/>

    

 	      <!-- hidden fields -->
 	    <input type="hidden" name="page_action" id="page_action" value=""/>
 	    <input type="hidden" name="aafes_code" id="aafes_code" value=""/>
 	    <input type="hidden" name="auth_code" id="auth_code" value=""/>
 	    <input type="hidden" name="manual_auth" id="manual_auth" value=""/>
 	    <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>
 	    <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
 	    <input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}"/>
      <input type="hidden" name="total" id="total" value="{net_amounts/net_add_bill_refund_amounts/order_total}"/>
 	    <input type="hidden" name="billing_type" id="billing_type" value="{net_amounts/net_add_bill_refund_amounts/billing_type}"/>
    
      <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData','external_order_number')/value}"/>
      <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
      <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
   
      <input type="hidden" name="msg_type" id="msg_type" value="{key('pageData','msg_type')/value}"/>
      <input type="hidden" name="page_florist_id" id="page_florist_id" value="{key('pageData','page_florist_id')/value}"/>
      <input type="hidden" name="florist_type" id="florist_type" value="{key('pageData','florist_type')/value}"/>
      <input type="hidden" name="class_change" id="class_change" value="{key('pageData','class_change')/value}"/>
      <input type="hidden" name="start_origin" id="start_origin" value="{key('pageData','start_origin')/value}"/>
      
      <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>
      <input type="hidden" name="category_index" id="category_index" value="{key('pageData','category_index')/value}"/>
      <input type="hidden" name="company_id" id="company_id" value="{key('pageData','company_id')/value}"/>
      <input type="hidden" name="occasion_text" id="occasion_text" value="{key('pageData','occasion_text')/value}"/>
      <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData','orig_product_amount')/value}"/>
      <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData','orig_product_id')/value}"/>
      <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
      <input type="hidden" name="page_number" id="page_number" value="{key('pageData','page_number')/value}"/>
      <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData','price_point_id')/value}"/>
      
   
 	      <!-- Gift certificate fields -->
 	      <input type="hidden" name="gcc_num" id="gcc_num" value=""/>
 	      <input type="hidden" name="amount_owed" id="amount_owed" value=""/>
 	      <input type="hidden" name="gcc_status" id="gcc_status" value=""/>
 	      <input type="hidden" name="gcc_amt" id="gcc_amt" value=""/>
 	      <input type="hidden" name="total_owed" id="total_owed" value="{net_amounts/net_add_bill_refund_amounts/order_total}"/>

 	      <!-- Original Order-->
 	      <input type="hidden" name="orig_ord_merch" id="orig_ord_merch" value="{original_orders/original_order/product_amount}"/>
 	      <input type="hidden" name="orig_ord_add_on" id="orig_ord_add_on" value="{original_orders/original_order/add_on_amount}"/>
 	      <input type="hidden" name="orig_ord_serv_ship" id="orig_ord_serv_ship" value="{original_orders/original_order/serv_ship_fee}"/>
 	      <input type="hidden" name="orig_ord_tax" id="orig_ord_tax" value="{original_orders/original_order/tax}"/>
 	      <input type="hidden" name="orig_ord_disc" id="orig_ord_disc" value="{original_orders/original_order/discount_amount}"/>
 	      <input type="hidden" name="orig_ord_total" id="orig_ord_total" value="{original_orders/original_order/order_total}"/>
 		    <input type="hidden" name="orig_ord_shipping_fee" id="orig_ord_shipping_fee" value="{original_orders/original_order/shipping_fee}"/>
 	      <input type="hidden" name="orig_ord_service_fee" id="orig_ord_service_fee" value="{original_orders/original_order/service_fee}"/>
 	      <input type="hidden" name="orig_ord_service_fee_tax" id="" value="{original_orders/original_order/service_fee_tax}"/>
        <input type="hidden" name="orig_ord_product_tax" id="orig_ord_product_tax" value="{original_orders/original_order/product_tax}"/>
        <input type="hidden" name="orig_ord_shipping_tax" id="orig_ord_shipping_tax" value="{original_orders/original_order/shipping_tax}"/>
 
 	      <!-- Modified Order amounts -->
 	      <input type="hidden" name="mod_ord_merch" id="mod_ord_merch" value="{modified_orders/modified_order/product_amount}"/>
 	      <input type="hidden" name="mod_ord_add_on" id="mod_ord_add_on" value="{modified_orders/modified_order/add_on_amount}"/>
 	      <input type="hidden" name="mod_ord_serv_ship" id="mod_ord_serv_ship" value="{modified_orders/modified_order/serv_ship_fee}"/>
 	      <input type="hidden" name="mod_ord_tax" id="mod_ord_tax" value="{modified_orders/modified_order/tax}"/>
 	      <input type="hidden" name="mod_ord_disc" id="mod_ord_disc" value="{modified_orders/modified_order/discount_amount}"/>
 	      <input type="hidden" name="mod_ord_total" id="mod_ord_total" value="{modified_orders/modified_order/order_total}"/>
 		    <input type="hidden" name="mod_ord_shipping_fee" id="orig_ord_shipping_fee" value="{modified_orders/modified_order/shipping_fee}"/>
 	      <input type="hidden" name="mod_ord_service_fee" id="orig_ord_service_fee" value="{modified_orders/modified_order/service_fee}"/>
 	      <input type="hidden" name="mod_ord_service_fee_tax" id="" value="{modified_orders/modified_order/service_fee_tax}"/>
        <input type="hidden" name="mod_ord_product_tax" id="orig_ord_product_tax" value="{modified_orders/modified_order/product_tax}"/>
        <input type="hidden" name="mod_ord_shipping_tax" id="orig_ord_shipping_tax" value="{modified_orders/modified_order/shipping_tax}"/>


 	      <!-- Original Net order amount -->
 	      <input type="hidden" name="orig_net_product_amount" id="orig_net_product_amount" value="{net_amounts/net_add_bill_refund_amounts/product_amount}"/>
 	      <input type="hidden" name="orig_net_add_on_amount" id="orig_net_add_on_amount" value="{net_amounts/net_add_bill_refund_amounts/add_on_amount}"/>
 	      <input type="hidden" name="orig_net_serv_ship_fee" id="orig_net_serv_ship_fee" value="{net_amounts/net_add_bill_refund_amounts/serv_ship_fee}"/>
 	      <input type="hidden" name="orig_net_service_fee" id="orig_net_service_fee" value="{net_amounts/net_add_bill_refund_amounts/service_fee}"/>
 	      <input type="hidden" name="orig_net_shipping_fee" id="orig_net_shipping_fee" value="{net_amounts/net_add_bill_refund_amounts/shipping_fee}"/>
 	      <input type="hidden" name="orig_net_discount_amount" id="orig_net_discount_amount" value="{net_amounts/net_add_bill_refund_amounts/discount_amount}"/>
   	    <input type="hidden" name="orig_net_shipping_tax" id="orig_net_shipping_tax" value="{net_amounts/net_add_bill_refund_amounts/shipping_tax}"/>
   	    <input type="hidden" name="orig_net_service_fee_tax" id="orig_net_service_fee_tax" value="{net_amounts/net_add_bill_refund_amounts/service_fee_tax}"/>
   	    <input type="hidden" name="orig_net_product_tax" id="orig_net_product_tax" value="{net_amounts/net_add_bill_refund_amounts/product_tax}" />
   	    <input type="hidden" name="orig_net_tax" id="orig_net_tax" value="{net_amounts/net_add_bill_refund_amounts/tax}"/>
   	    <input type="hidden" name="orig_net_total" id="orig_net_total" value="{net_amounts/net_add_bill_refund_amounts/order_total}"/>

 	      <!-- Update Order Amounts (this changes after a recalculation is performed -->
        <input type="hidden" name="net_product_amount" id="net_product_amount" value="{net_amounts/net_add_bill_refund_amounts/product_amount}"/>
 	      <input type="hidden" name="net_add_on_amount" id="net_add_on_amount" value="{net_amounts/net_add_bill_refund_amounts/add_on_amount}"/>
 	      <input type="hidden" name="net_serv_ship_fee" id="net_serv_ship_fee" value="{net_amounts/net_add_bill_refund_amounts/serv_ship_fee}"/>
 	      <input type="hidden" name="net_service_fee" id="net_service_fee" value="{net_amounts/net_add_bill_refund_amounts/service_fee}"/>
 	      <input type="hidden" name="net_shipping_fee" id="net_shipping_fee" value="{net_amounts/net_add_bill_refund_amounts/shipping_fee}"/>
 	      <input type="hidden" name="net_discount_amount" id="net_discount_amount" value="{net_amounts/net_add_bill_refund_amounts/discount_amount}"/>
   	    <input type="hidden" name="net_shipping_tax" id="net_shipping_tax" value="{net_amounts/net_add_bill_refund_amounts/shipping_tax}"/>
   	    <input type="hidden" name="net_service_fee_tax" id="net_service_fee_tax" value="{net_amounts/net_add_bill_refund_amounts/service_fee_tax}"/>
   	    <input type="hidden" name="net_product_tax" id="net_product_tax" value="{net_amounts/net_add_bill_refund_amounts/product_tax}" />
   	    <input type="hidden" name="net_tax" id="net_tax" value="{net_amounts/net_add_bill_refund_amounts/tax}"/>
   	    <input type="hidden" name="net_total" id="net_total" value="{net_amounts/net_add_bill_refund_amounts/order_total}"/>

 	      <!-- Add Bill -->
 	      <input type="hidden" name="add_bill_product_amount" id="add_bill_product_amount" value="{net_amounts/add_bill/product_amount}"/>
 	      <input type="hidden" name="add_bill_add_on_amount" id="add_bill_add_on_amount" value="{net_amounts/add_bill/add_on_amount}"/>
 	      <input type="hidden" name="add_bill_serv_ship_fee" id="add_bill_serv_ship_fee" value="{net_amounts/add_bill/serv_ship_fee}"/>
 	      <input type="hidden" name="add_bill_discount_amount" id="add_bill_discount_amount" value="{net_amounts/add_bill/discount_amount}"/>
 	      <input type="hidden" name="add_bill_tax" id="add_bill_tax" value="{net_amounts/add_bill/tax}"/>
 	      <input type="hidden" name="add_bill_order_total" id="add_bill_order_total" value="{net_amounts/add_bill/order_total}"/>
 	      <input type="hidden" name="add_bill_service_fee" id="add_bill_service_fee" value="{net_amounts/add_bill/service_fee}"/>
 	      <input type="hidden" name="add_bill_shipping_fee" id="add_bill_shipping_fee" value="{net_amounts/add_bill/shipping_fee}"/>
 	      <input type="hidden" name="add_bill_shipping_tax" id="add_bill_shipping_tax" value="{net_amounts/add_bill/shipping_tax}"/>
 	      <input type="hidden" name="add_bill_service_fee_tax" id="add_bill_service_fee_tax" value="{net_amounts/add_bill/service_fee_tax}"/>
 	      <input type="hidden" name="add_bill_product_tax" id="add_bill_product_tax" value="{net_amounts/add_bill/product_tax}"/>

 	      <!-- Refund -->
 	      <input type="hidden" name="refund_product_amount" id="refund_product_amount" value="{net_amounts/refund/product_amount}"/>
 	      <input type="hidden" name="refund_add_on_amount" id="refund_add_on_amount" value="{net_amounts/refund/add_on_amount}"/>
 	      <input type="hidden" name="refund_serv_ship_fee" id="refund_serv_ship_fee" value="{net_amounts/refund/serv_ship_fee}"/>
 	      <input type="hidden" name="refund_discount_amount" id="refund_discount_amount" value="{net_amounts/refund/discount_amount}"/>
 	      <input type="hidden" name="refund_tax" id="refund_tax" value="{net_amounts/refund/tax}"/>
 	      <input type="hidden" name="refund_order_total" id="refund_order_total" value="{net_amounts/refund/order_total}"/>
 	      <input type="hidden" name="refund_service_fee" id="refund_service_fee" value="{net_amounts/refund/service_fee}"/>
 	      <input type="hidden" name="refund_shipping_fee" id="refund_shipping_fee" value="{net_amounts/refund/shipping_fee}"/>
 	      <input type="hidden" name="refund_shipping_tax" id="refund_shipping_tax" value="{net_amounts/refund/shipping_tax}"/>
 	      <input type="hidden" name="refund_service_fee_tax" id="refund_service_fee_tax" value="{net_amounts/refund/service_fee_tax}"/>
 	      <input type="hidden" name="refund_product_tax" id="refund_product_tax" value="{net_amounts/refund/product_tax}"/>

 	      <input type="hidden" name="mgr_password" id="mgr_password" value=""/>
 	      <input type="hidden" name="is_orig_cc" id="is_orig_cc" value=""/>

 	      <!-- required in order to return to delivery confirmation -->
 	      <input type="hidden" name="DC_START_ORIGIN" id="DC_START_ORIGIN" value="{key('pageData','start_origin')/value}"/>
       	<input type="hidden" name="DC_CUSTOMER_ID" id="DC_CUSTOMER_ID" value="{key('pageData','customer_id')/value}"/>
        <input type="hidden" name="DC_EXTERNAL_ORDER_NUMBER" id="DC_EXTERNAL_ORDER_NUMBER" value="{key('pageData','external_order_number')/value}"/>
        <input type="hidden" name="DC_MSG_TYPE" id="DC_MSG_TYPE" value="{key('pageData','msg_type')/value}"/>
        <input type="hidden" name="DC_PAGE_FLORIST_ID" id="DC_PAGE_FLORIST_ID" value="{key('pageData','page_florist_id')/value}"/>
        <input type="hidden" name="DC_FLORIST_TYPE" id="DC_FLORIST_TYPE" value="{key('pageData','florist_type')/value}"/>
        <input type="hidden" name="DC_MASTER_ORDER_NUMBER" id="DC_MASTER_ORDER_NUMBER" value="{key('pageData','master_order_number')/value}"/>

  		 <input type="hidden" name="max_entered_merch" id="max_entered_merch" value="{format-number(net_amounts/net_add_bill_refund_amounts/product_amount,'#,###,##0.00;#,###,##0.00','zeroValue')}"/>
		   <input type="hidden" name="max_entered_add_on" id="max_entered_add_on" value="{format-number(net_amounts/net_add_bill_refund_amounts/add_on_amount,'#,###,##0.00;#,###,##0.00','zeroValue')}"/>
	     <input type="hidden" name="max_entered_serv_ship" id="max_entered_serv_ship" value="{format-number(net_amounts/net_add_bill_refund_amounts/serv_ship_fee,'#,###,##0.00;#,###,##0.00','zeroValue')}"/>


 	      <xsl:call-template name="addHeader"/>
 	      <!-- begin main table -->
          <table class="mainTable" width="98%" align="center" cellpadding="2" cellspacing="0">
             <tr>
                <td>

                   <!-- begin inner table -->
	               <table class="innerTable" width="100%" cellpadding="2" cellspacing="0">
				      <tr>
				         <td>

				            <!-- Original order  -->
				            <table border="0" cellpadding="0" cellspacing="3" width="100%">
				 			   <tr class="label">
				 			      <td width="25%"></td>
				 			      <td class="merchTD"><xsl:value-of select="$merch"/></td>
				 			      <td class="addOnTD"><xsl:value-of select="$addOn"/></td>
				 			      <td class="servShipTD"><xsl:value-of select="$servShip"/></td>
				 			      <td class="taxTD"><xsl:value-of select="$tax"/></td>
				 			      <td class="discountTD"><xsl:value-of select="$discount"/></td>
				 			      <td class="totalTD"><xsl:value-of select="$total"/></td>
				 			   </tr>
				 			    <tr>
				 			      <td class="label">Original Order</td>
				 			      <td><xsl:value-of select="format-number(original_orders/original_order/product_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(original_orders/original_order/add_on_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(original_orders/original_order/serv_ship_fee,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(original_orders/original_order/tax,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(original_orders/original_order/discount_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(original_orders/original_order/order_total, '#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			   </tr>
				            </table>
				             <!-- end original order table -->


				             <!-- modified order -->
				            <table border="0" cellpadding="0" cellspacing="3" width="100%">
				 			   <tr class="label">
				 			      <td width="25%"></td>
				 			      <td class="merchTD"><xsl:value-of select="$merch"/></td>
				 			      <td class="addOnTD"><xsl:value-of select="$addOn"/></td>
				 			      <td class="servShipTD"><xsl:value-of select="$servShip"/></td>
				 			      <td class="taxTD"><xsl:value-of select="$tax"/></td>
				 			      <td class="discountTD"><xsl:value-of select="$discount"/></td>
				 			      <td class="totalTD"><xsl:value-of select="$total"/></td>
				 			   </tr>
				 			    <tr>
				 			      <td class="label">Modified Order</td>
				 			      <td><xsl:value-of select="format-number(modified_orders/modified_order/product_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(modified_orders/modified_order/add_on_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(modified_orders/modified_order/serv_ship_fee,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(modified_orders/modified_order/tax,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(modified_orders/modified_order/discount_amount,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			      <td><xsl:value-of select="format-number(modified_orders/modified_order/order_total,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
				 			   </tr>
				            </table>
				              <!-- end of orignal order and modified order tables -->

				   			   <!-- create stylish blue seperating banner -->
				 			<div style="padding:.3em;">
							   <div style="width:100%;" class="banner"/>
							</div>
							<!--
								call tempalte based on wether we are processing a refund
							     or an add bill payment
						    -->
							<xsl:call-template name="refundAndAddBill">
							   <xsl:with-param name="add_refund_node" select="net_amounts/net_add_bill_refund_amounts/."/>
							</xsl:call-template>



							<!-- credit card text boxes -->
							<table border="0" cellpadding="0" cellspacing="2" width="100%">
							<tr id="giftCertRow" style="display:block">
                               <td id="giftCertCell" colspan="4" class="RequiredFieldTxt" style="padding-left:.5em;">
                                   <!-- If the decides to use a Gift Certificate display the GC number and Amount here -->
                               </td>
                             </tr>
							   <tr>
							      <td class="LabelRight" width="15%">Payment Type:</td>
							      <td class="TextField">
							          <select tabindex="1" id="pay_type" name="pay_type"  onchange="resetCreditCardLists('exp_date_month','exp_date_year','credit_card_num', null)">
							             <xsl:if test="net_amounts/net_add_bill_refund_amounts/billing_type != 'add_bill'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
      	                                 <option>-- Please Select --</option>
                                         <xsl:for-each select="PAYMENT_METHODS/PAYMENT_METHOD">
    	                                    <option value="{payment_method_id}"><xsl:value-of select="description"/></option>
                                         </xsl:for-each>
                                     </select>
                                     <span id="aafesField" style="padding-left:4em;display:none">
                                         <span class="Label">AAFES Code: </span>
                                         <input type="text" name="aafes_code_entered" id="aafes_code_entered" value=""/>
                                    </span>
                                    <span id="noChargeField" style="padding-left:1em;display:none">
                           	           <span class="Label">Approval Code: </span>
                                       <input type="text" name="mgr_appr_code" id="mgr_appr_code" value=""/>&nbsp;&nbsp;&nbsp;
                                     <span class="Label">Password:</span>
                                        <input type="password" name="password" id="password" value="" onblur="forms['billingReviewForm'].mgr_password.value = this.value;"/>
                                     </span>
							      </td>
							   </tr>
							   <!-- Payment type validation -->
						       <tr id="pay_type_validation_message_row" style="display:none" >
                                  <td />
                                  <td id="pay_type_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                               </tr>

                               <!-- credit card number -->
							  <tr>
                                 <td class="LabelRight">Credit Card Number:</td>
                                 <td class="TextField">
                                    <input type="text" size="35" tabindex="2" name="credit_card_num" id="credit_card_num" onkeypress="return doOnlyNumeric()" >
                                        <xsl:if test="net_amounts/net_add_bill_refund_amounts/billing_type != '' and $ORIGINALCREDITCARD/cc_number != ''"><xsl:attribute name="value">************<xsl:value-of select="$ORIGINALCREDITCARD/cc_number"/></xsl:attribute></xsl:if>
									                      <xsl:if test="net_amounts/net_add_bill_refund_amounts/billing_type != 'add_bill'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
                                    </input>
                                </td>
                              </tr>
							  <!-- credit card number validation cell -->
						     <tr id="credit_card_num_validation_message_row" style="display:none" >
                                <td />
                                <td id="credit_card_num_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                             </tr>

							<!-- expiration date list boxes -->
							  <tr>
                                 <td class="LabelRight"><div align="right"><strong>Expiration Date: </strong></div></td>
                                 <td colspan="3" class="TextField">
                                    <select tabindex="3" id="exp_date_month" name="exp_date_month">
                                      <xsl:if test="net_amounts/net_add_bill_refund_amounts/billing_type != 'add_bill'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
		                              <option selected="selected"> </option>
		                              <option value="01">January</option>
		                              <option value="02">February</option> 
		                              <option value="03">March</option>
		                              <option value="04">April</option>
		                              <option value="05">May</option>
		                              <option value="06">June</option>
		                              <option value="07">July</option>
		                              <option value="08">August</option>
		                              <option value="09">September</option>
		                              <option value="10">October</option>
		                              <option value="11">November</option>
		                              <option value="12">December</option>
                           			</select>
                         		 /
		                           <select tabindex="4" id="exp_date_year" name="exp_date_year">
		                           <xsl:if test="net_amounts/net_add_bill_refund_amounts/billing_type != 'add_bill'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
		                              <option selected="selected"></option>
				                   </select>
  		                		</td>
                     		</tr>

							<!-- expiration date validation cells -->
							 <tr id="exp_date_month_validation_message_row" style="display:none" >
                                <td />
                                <td id="exp_date_month_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                             </tr>

                             <tr id="exp_date_year_validation_message_row" style="display:none">
                                <td />
                                <td id="exp_date_year_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                             </tr>

                             <!-- charge amount to be displayed -->
                             <tr>
 							    <td class="labelRight">Charge Amount:</td>
 							    <td  id="total_charge_amt" style="padding-left:.5em;"><xsl:value-of select="format-number(net_amounts/net_add_bill_refund_amounts/order_total,'#,###,##0.00;(#,###,##0.00)','zeroValue')"/></td>
                             </tr>

							</table>
							<!-- end credit card information -->
				         </td>
				      </tr>
                   </table>
                   <!-- end inner table -->
                </td>
             </tr>
          </table>
          <!-- end main table -->

          <!-- action buttons -->
          <div id="actionButtons">
		         <button type="button" class="BlueButton" accesskey="S" id="submitBtn" name="submitBtn" onclick="doSubmitBillingReview()">(S)ubmit Billing</button>
		         <span style="padding-left:.3em;">
		            <button type="button"  class="BlueButton" accesskey="C" id="cancel" name="cancel" onclick="javascript:doCancelBillingReview()">(C)ancel</button>&nbsp;&nbsp;
                 <button type="button"  class="BlueButton" accesskey="T" id="test" name="test" onclick="javascript:doTestReview()">(T)emp Submit</button>
		         </span>
          </div>

   
  </div>
</form>
      <!-- Processing message div -->
 <div id="waitDiv" style="display:none">
    <table align="center" id="waitTable" height="100%" class="mainTable" width="98%"  border="0" cellpadding="0" cellspacing="2" >
       <tr>
          <td height="100%">
	         <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="innerTable">
		       <tr>
              <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
              <td id="waitTD"  width="50%" class="WaitMessage"></td>
		       </tr>
	         </table>
          </td>
       </tr>
    </table>
 </div>
<!--footer-->
<xsl:call-template name="footer"/>




    </body>
   </html>
 </xsl:template>

 <xsl:template name="addHeader">
    <xsl:call-template name="header">
	   <xsl:with-param name="showBackButton" select="false()"/>
	   <xsl:with-param name="showTime" select="true()"/>
	   <xsl:with-param name="headerName" select="'Billing Review Screen'"/>
	   <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>
 </xsl:template>


</xsl:stylesheet>