<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="vendorDeliveryDates" >
  <xsl:param name="origShipMethod"/>
  <xsl:param name="shipCosts"/>
  <xsl:param name="requestedDeliveryDate"/>

  <script type="text/javascript" language="javascript">
  <![CDATA[
  /*
   * The prepareDeliveryDate function is dynamic because delivery date submission preperation
   * is different for florist and vendor items.  This version of prepareDeliveryDate gets the selected
   * ship method and the corresponding selected delivery date.
   */
  function prepareDeliveryDate(formName) {
    var radio = document.forms[formName].page_ship_method;
    var radioSelected;

		if (radio.length == null)
		{
			radioSelected = radio;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++){
				if ( radio[i].checked ) {
					radioSelected = radio[i];
				}
			}
		}

    // continue only if a ship method was selected
    if ( radioSelected != null ){
      var shipMethod = radioSelected.value;
      var dd = document.getElementById(shipMethod + "_delivery_date");
      var ddSelected = dd[dd.selectedIndex];
      var date = ddSelected.value;

      document.forms[formName].ship_method.value = shipMethod;
      document.forms[formName].delivery_date.value = date;
      document.forms[formName].delivery_date_range_end.value = "";
    }
  }
  ]]>
  </script>
  <tr>
    <td colspan="3">
      <table border="0" cellpadding="0" cellspacing="2" width="100%">
        <xsl:for-each select="/root/shippingData/shippingMethods/shippingMethod/method">
          <tr>
            <td class="Label" align="right" colspan="30%">
              <xsl:choose>
                <xsl:when test="@code = $origShipMethod">
                  <input type="radio" name="page_ship_method" value="{@code}" checked="true"/>
                </xsl:when>
                <xsl:when test="@code = 'FL' and $origShipMethod = 'SD'">
                  <input type="radio" name="page_ship_method" value="SD" checked="true"/>
                </xsl:when>
                <xsl:when test="@code = 'FL'">
                  <input type="radio" name="page_ship_method" value="SD"/>
                </xsl:when>
                <xsl:otherwise>
                  <input type="radio" name="page_ship_method" value="{@code}"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>

            <!-- Description -->
            <td colspan="10%">
              <xsl:value-of select="@description"/>
            </td>

            <xsl:variable name="desc" select="@description"/>
            <xsl:variable name="outCost">
              <xsl:for-each select="$shipCosts/PD_SHIP_COSTS">
                <xsl:if test="$desc = description">
                  <xsl:value-of select="cost"/>
                </xsl:if>
              </xsl:for-each>
            </xsl:variable>

            <!-- Cost -->
            <td colspan="10%">
              <xsl:if test="$outCost != ''">
                $<xsl:value-of select="format-number($outCost, '#,###,##0.00')"/>
              </xsl:if>
            </td>

            <td colspan="40%"/>

            <td >
            <xsl:choose>
              <xsl:when test="@code = 'ND' or @code = 'SA' or @code = 'SU'">
                Will be delivered on &nbsp; &nbsp;
              </xsl:when>
              <xsl:otherwise>
                Arrives no later than &nbsp;
              </xsl:otherwise>
            </xsl:choose>
            </td>

            <td>
              <xsl:choose>
                <xsl:when test="@code = 'ND'">
                  <select name="{@code}_delivery_date" id="{@code}_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(@types, 'ND')">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:if test="concat(@dayOfWeek, ' ' , @displayDate) = $requestedDeliveryDate">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = '2D'">
                  <select name="{@code}_delivery_date" id="{@code}_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(@types, '2D')">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:if test="concat(@dayOfWeek, ' ' , @displayDate) = $requestedDeliveryDate">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'GR'">
                  <select name="{@code}_delivery_date" id="{@code}_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(@types, 'GR')">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'SA'">
                  <select name="{@code}_delivery_date" id="{@code}_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(@types, 'SA')">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:if test="concat(@dayOfWeek, ' ' , @displayDate) = $requestedDeliveryDate">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'SU'">
                  <select name="{@code}_delivery_date" id="{@code}_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(@types, 'SU')">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:if test="concat(@dayOfWeek, ' ' , @displayDate) = $requestedDeliveryDate">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'FL'">
                  <select name="SD_delivery_date" id="SD_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(@types, 'FL')">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:if test="concat(@dayOfWeek, ' ' , @displayDate) = $requestedDeliveryDate">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:otherwise>
                  <select name="{@code}_delivery_date" id="{@code}_delivery_date">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                        <option value="{@dayOfWeek} {@date}">
                          <xsl:if test="concat(@dayOfWeek, ' ' , @displayDate) = $requestedDeliveryDate">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>
                        </option>
                    </xsl:for-each>
                  </select>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </td>
  </tr>
  <tr id="page_ship_method_validation_message_row" style="display:none">
    <td/>
    <td id="page_ship_method_validation_message_cell" colspan="10" class="RequiredFieldTxt" style="padding-left:4.5em;"/>
  </tr>


</xsl:template>
</xsl:stylesheet>