<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- import templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:variable name="email_found" select="boolean(key('pageData','uc_email_found')/value = 'y')"/>
<xsl:variable name="directMailSum" select="count(/root/CustomerVO/CustomerVO/DirectMailVO)"/>
 <xsl:variable name="cai" select="root/CAI_CUSTOMERS/CAI_CUSTOMER"/>
<xsl:variable name="buyerIndicator" select="CustomerVO/CustomerVO/buyerIndicator"/>
<xsl:variable name="recipientIndicator" select="CustomerVO/CustomerVO/recipientIndicator"/>

<xsl:output indent="yes" method="html"/>
<xsl:template match="/root">
<html>
<head>
  <title>Update Customer Account</title>
  <script type="text/javascript" src="js/util.js" />
  <script type="text/javascript" src="js/tabIndexMaintenance.js" />
  <script type="text/javascript" src="js/commonUtil.js" />
  <script type="text/javascript" src="js/FormChek.js"/>
  <script type="text/javascript" src="js/cityLookup.js"/>
  <script type="text/javascript" src="js/popup.js"/>
  <script type="text/javascript" src="js/updateCustomerAccount.js"/>
  <script type="text/javascript">

   var usStates = new Array( <xsl:for-each select="STATES/STATE[countryCode='']">
                                ["<xsl:value-of select="id"/>", "<xsl:value-of select="name"/>"]
                                <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                              </xsl:for-each>);
    var caStates = new Array( <xsl:for-each select="STATES/STATE[countryCode='CAN']">
                                ["<xsl:value-of select="id"/>", "<xsl:value-of select="name"/>"]
                                <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                              </xsl:for-each>);
  var naStates = new Array(["NA", "N/A"]);
  var refresh_flag = '<xsl:value-of select="key('pageData','data_exists')/value"/>';
  var email_hist_url = "<xsl:value-of select="key('pageData','email_history_url')/value"/>";
  var uc_change_flag = false;
  var uc_submit_flag;
  var field = '';
  var errorOverride;
  var refreshOverride;

	var requiredParms = new Array ('uc_customer_id','display_page','uc_master_order_number',
							'uc_order_guid','uc_direct_mail_subscribe_status_count','uc_address_type','uc_buyer_indicator','uc_recipient_indicator',
							'uc_customer_county','uc_customer_birthday','uc_customer_first_name','uc_customer_last_name','uc_business_name',
							'uc_customer_address_1','uc_customer_address_2',
							'uc_customer_city','uc_customer_state','uc_customer_zip_code','uc_customer_country','uc_daytime_customer_phone_number',
							'uc_daytime_customer_phone_id','uc_daytime_customer_extension','uc_evening_customer_phone_number','uc_evening_customer_phone_id',
              'uc_evening_customer_extension','uc_vip_customer');
    var requiredFields = new Array("uc_customer_first_name","uc_customer_last_name","uc_customer_address_1", "uc_customer_city","uc_customer_zip_code");


  	var errorFields = new Array();
  	var errorDivs = new Array();
    var errorMsg = '<xsl:value-of select="key('pageData','update_cust_acct_error_msg')/value"/>';
    var ret = new Array();
   <!--
   	We use this variable to check whcih action we need to finish processing.
   	For example, when a CSR clicks the comments button. If the csr made changes
   	to any of the fields on this page we issue the doExitPage action which asks them
   	if the wish to continue without saving their chagnes.
   	If the answer is YES we send a request to release the lock on the record and
   	this variable will be set to comments. This way from the IFrame we are able to
   	decide whcih action to continue on to.

   	possible values:
   	comments - email_history - search - back - main_menu - complete
   -->
   var pageAction = '';
   
   var isBuyer = '<xsl:value-of select="CustomerVO/CustomerVO/buyerIndicator"/>';
   var isRecipient = '<xsl:value-of select="CustomerVO/CustomerVO/recipientIndicator"/>';
   
   
function init(){
   setNavigationHandlers();
   setUpdateCustomerAccountIndexes();
   setScrollingDivHeight();
   addDefaultListeners();
   setCountry('<xsl:value-of select="CustomerVO/CustomerVO/country"/>');
   populateStates('uc_customer_country', 'uc_customer_state', '<xsl:value-of select="CustomerVO/CustomerVO/state"/>');

   

 if( isRecipient == 'Y'  ){
    disableCustomerInformation();
 }
 else {
    var re = doRequiredFields();
   var v = doValidate();
   if(re || !re){
    if(v)
    {}else{
  		 setErrorFields(errorFields);
		   setErrorDivs(errorDivs);
		   displayErrorAlert();
    }
   }
   if(errorMsg != ''){
   	  alert(errorMsg);
   }
  }
  
  if(document.getElementById('vipCustUpdatePermission').value == 'Y')  
	 {
	  document.getElementById('uc_vip_customer').disabled = false;
	  if(document.getElementById('uc_vip_customer').value == 'true')
	  	 document.getElementById('uc_vip_customer').checked = true;
     }
  else
    {
      document.getElementById('uc_vip_customer').disabled = true;
    }
   
   if(document.getElementById('uc_vip_customer').value == 'true')
		  localStorage.setItem('isCustomerVip','Y');
   else
          localStorage.setItem('isCustomerVip','N');  
     
}

/*
 This function is executed when a customer is both a buyer and a recipient
 or just a recipient.
*/
function disableCustomerInformation(){

   document.getElementById('uc_customer_first_name').disabled = true;
   document.getElementById('uc_customer_last_name').disabled = true;
   document.getElementById('uc_business_name').disabled = true;
   document.getElementById('uc_customer_address_1').disabled = true;
   document.getElementById('uc_customer_address_2').disabled = true;
   document.getElementById('uc_customer_city').disabled = true;
   document.getElementById('uc_customer_state').disabled = true;
   document.getElementById('uc_customer_zip_code').disabled = true;
   document.getElementById('uc_customer_country').disabled = true;
   document.getElementById('uc_daytime_customer_phone_number').disabled = true;
   document.getElementById('uc_daytime_customer_extension').disabled = true;
   document.getElementById('uc_evening_customer_phone_number').disabled = true;
   document.getElementById('uc_evening_customer_extension').disabled = true;
}
</script>
<link rel="stylesheet" href="css/ftd.css"/>
<style type="text/css">

		span.Label{
			padding-left:.5em;
		}
		.banner{
			text-align:left;
			padding-left:.5em;
		}
		td.Label{
			text-align:right;
		}
		 /* Scrolling table overrides for the order/shopping cart table */
		div.tableContainer table {
			width: 95%;
		}
		div#messageContainer {
			height: 130px;
			width:99%;
		}
		td span{
			padding-left:.5em;
		}
	</style>
</head>

	<body onload="javascript:init();" onkeypress="javascript:enterCheckCustomerAccount()">
		<xsl:call-template name="addHeader" />
		<form method="post" action="" >
			<xsl:call-template name="securityanddata"/>
			<xsl:call-template name="decisionResultData"/>
		    <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','uc_customer_id')/value}"/>
			<input type="hidden" name="uc_customer_id" id="uc_customer_id" value="{key('pageData','uc_customer_id')/value}"/>
			<input type="hidden" id="display_page" name="display_page" value="{key('pageData','display_page')/value}"/>
			<input type="hidden" name="uc_master_order_number" id="uc_master_order_number" value="{key('pageData','uc_master_order_number')/value}"/>
			<input type="hidden" name="uc_order_guid" id="uc_order_guid" value="{key('pageData','uc_order_guid')/value}"/>
			<input type="hidden" id="uc_direct_mail_subscribe_status_count" name="uc_direct_mail_subscribe_status_count" value="{$directMailSum}" />
			<input type="hidden" name="uc_address_type" id="uc_address_type" value="{CustomerVO/CustomerVO/addressType}"/>
			<input type="hidden" name="uc_buyer_indicator" id="uc_buyer_indicator" value="{CustomerVO/CustomerVO/buyerIndicator}"/>
			<input type="hidden" name="uc_recipient_indicator" id="uc_recipient_indicator" value="{CustomerVO/CustomerVO/recipientIndicator}"/>
			<input type="hidden" name="uc_customer_county" id="uc_customer_county" value="{CustomerVO/CustomerVO/country}"/>
			<input type="hidden" name="uc_customer_birthday" id="uc_customer_birthday" value="{CustomerVO/CustomerVO/birthday}"/>
			<input type="hidden" name="vipCustUpdatePermission" id="vipCustUpdatePermission" value="{key('pageData','vipCustUpdatePermission')/value}"/>
			<input type="hidden" name="submitSwitch" id="submitSwitch" value="yes"/>
			<!--
					There are other hidden fields in the Marketing section of this page.
					The hidden fields are in a for-each loop below instead of repeating the process here.
				-->
			<iframe id="ucActions" width="0px" height="0px" border="0" />
			<div id="outterContent">
				<table width="98%"  border="0" cellpadding="0" cellspacing="1" class="mainTable" align="center">
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="1" class="innerTable">
								<tr>
									<td>
										<span class="Label">Customer Record Number:</span>&nbsp;
										<a name = "customerRecordLink" href = "#" onclick ="javascript:doUCABackAction();"><xsl:value-of select="key('pageData','uc_customer_id')/value"/></a>
										<span id="accountPad" class="Label">Account Creation Date:</span>&nbsp;
					                      <xsl:value-of select="substring(/root/CustomerVO/CustomerVO/createdOn,6,2)"/>
					                      <xsl:text>/</xsl:text>
					                      <xsl:value-of select="substring(/root/CustomerVO/CustomerVO/createdOn,9,2)"/>
					                      <xsl:text>/</xsl:text>
					                      <xsl:value-of select="substring(/root/CustomerVO/CustomerVO/createdOn,1,4)"/>
									</td>
								</tr>
								<tr>
									<td class="banner">
										<span id="customerBanner">Customer Information</span>
										<span id="marketingBanner">Marketing</span>
									</td>
								</tr>
								<tr>
									<td>
										<table border="0" width="100%" height="100%" cellpadding="1">
											<tr>
												<td width="48%" style="vertical-align:top">
													<table border="0" cellpadding="0" cellspacing="0" width="100%" >
														<tr>
															<td class="Label" id="customerField" >
																					 First Name:
															</td>
															<td class="textfield">
																<input type="text" id="uc_customer_first_name" name="uc_customer_first_name" size="35" maxlength="25" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/firstName}" />
															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaFirstNameError"  style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label" >
																											 Last Name:
															</td>
															<td class="textfield" >
																<input type="text" id="uc_customer_last_name" name="uc_customer_last_name" size="35" maxlength="25" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);"  value="{CustomerVO/CustomerVO/lastName}" />
															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaLastNameError"  style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label" >
														             Bus. Name:
															</td>
															<td class="textfield">
																<input type="text" id="uc_business_name" name="uc_business_name" size="35" maxlength="50" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/businessName}" />
															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaBusinessNameError"  style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label">
																			Address 1:
															</td>
															<td class="textfield">
																<input type="text" id="uc_customer_address_1" name="uc_customer_address_1" size="35" maxlength="45" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/address1}" />
															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaAddress1Error"  style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label">
																			Address 2:
															</td>
															<td class="textfield">
																<input type="text" id="uc_customer_address_2" name="uc_customer_address_2" size="35" maxlength="45" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/address2}" />
															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaAddress2Error"  style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label">City:</td>
															<td class="textfield">
																<input type="text" id="uc_customer_city" name="uc_customer_city" size="10" maxlength="30" onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/city}" />
																<span class="Label">State:</span>&nbsp;&nbsp;
																<select id="uc_customer_state" name="uc_customer_state" onchange="javascript:doOnChangeAction(this);">
																	<xsl:for-each select="STATES/STATE">
																		<option value="{id}"><xsl:value-of select="name"/></option>
																	</xsl:for-each>
																</select>
															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaCityError"  style="display:none;">Invalid Format</span>
																<span id="ucaStateError"  style="display:none;">Invalid state for zip code</span>
															</td>
														</tr>
														<tr>
															<td class="Label">
																		Zip/Postal:
															</td>
															<td class="textfield">
																<input type="text" id="uc_customer_zip_code" name="uc_customer_zip_code" maxlength="12" onfocus="doOnChangeAction(this)" onblur="doOnChangeAction(this)" onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/zipCode}" />&nbsp;&nbsp;<xsl:if test="$buyerIndicator = 'Y' and $recipientIndicator = 'N'">
                                                                                                                                  <a id="cityLookupLink" onkeypress="doSubmitCityLookup();" href="#" class="textlink" onclick="javascript:doCityLookup();" >Lookup by City</a>
                                                                                                                                </xsl:if>
                                                                                                                        </td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaZipCodeError"  style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label">
																	Country:
															</td>
															<td class="textfield">
																<select id="uc_customer_country" name="uc_customer_country" onchange="javascript:populateStates('uc_customer_country', 'uc_customer_state', ''),doOnChangeAction(this);">
																	<xsl:for-each select="COUNTRIES/COUNTRY">
																		<option value="{id}"><xsl:value-of select="description"/></option>
																	</xsl:for-each>
																</select>
															</td>
														</tr>
														<tr>
															<td class="Label">
																			Phone 1:
															</td>
															<td class="textfield" >
																<input type="text" id="uc_daytime_customer_phone_number" name="uc_daytime_customer_phone_number" maxlength="20" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" />
																<input type="hidden" id="uc_daytime_customer_phone_id" name="uc_daytime_customer_phone_id" value="{CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Day']/../phoneId}"/>
																<script type="text/javascript">
                                        if(isUSPhoneNumber('<xsl:value-of select="CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Day']/../phoneNumber"/>')){
                                          document.getElementById('uc_daytime_customer_phone_number').value = reformatUSPhone('<xsl:value-of select="CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Day']/../phoneNumber"/>');

                                        }else{
                                          document.getElementById('uc_daytime_customer_phone_number').value = '<xsl:value-of select="CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Day']/../phoneNumber"/>';
                                        }
																</script>
																<span class="Label">Ext:</span>
																<input type="text" id="uc_daytime_customer_extension" name="uc_daytime_customer_extension" size="6" maxlength="10" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/CustomerPhoneVO/phoneType[. = 'Day']/../extension}"/>

															</td>
														</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaDayPhoneError" style="display:none;">Invalid Format</span>
															</td>
														</tr>
														<tr>
															<td class="Label">
                                     							 Phone 2:
															</td>
															<td class="textfield" >
																<input type="text" id="uc_evening_customer_phone_number" name="uc_evening_customer_phone_number" maxlength="20" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" />
																<input type="hidden" id="uc_evening_customer_phone_id" name="uc_evening_customer_phone_id" value="{CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Evening']/../phoneId}"/>
																<script type="text/javascript">
                                        if(isUSPhoneNumber('<xsl:value-of select="CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Evening']/../phoneNumber"/>')){
                                          document.getElementById('uc_evening_customer_phone_number').value = reformatUSPhone('<xsl:value-of select="CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Evening']/../phoneNumber"/>');

                                        }else{
                                          document.getElementById('uc_evening_customer_phone_number').value = '<xsl:value-of select="CustomerVO/CustomerVO/CustomerPhoneVO//phoneType[. = 'Evening']/../phoneNumber"/>';
                                        }
																</script>
																<span class="Label">Ext:</span>
																<input type="text" id="uc_evening_customer_extension" name="uc_evening_customer_extension" size="6" maxlength="10" onkeydown="doCheckOnKeyDown(this)"  onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/CustomerPhoneVO/phoneType[. = 'Evening']/../extension}"/>
															</td>
														</tr>
															<tr> 
															<td class="Label">
																<input type="checkbox" name="uc_vip_customer" id="uc_vip_customer" onfocus="doOnChangeAction(this)" onblur="doOnChangeAction(this)" onchange="javascript:doOnChangeAction(this);" value="{CustomerVO/CustomerVO/vipCustomer}"/>
															</td>
															<td class="textfield">
																<xsl:attribute name="style">font-weight:bold</xsl:attribute>VIP
															</td>
															</tr>
														<tr>
															<td></td>
															<td class="errorMessage">
																<span id="ucaEveningPhoneError" style="display:none;">Invalid Format</span>
															</td>
														</tr>
													</table>
												</td>
												<td style="text-align:bottom" width="52%">
													<div style="width:100%;height:100%" >
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr id="brandLabels">
																<td class="Label" style="text-align:left;" width="20%" >Catalog Status</td>
																<td class="Label" style="text-align:left;" width="40%" >Email Address</td>
																<td class="Label" style="text-align:left;" width="20%">Status</td>
																<td class="Label" style="text-align:left;" width="20%">Updated</td>
															</tr>
															<tr>
																<td colspan="5">
														    <div id="orderListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
																	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="marketingTable" style="table-layout:fixed;">
																		<xsl:for-each select="CustomerVO/CustomerVO/DirectMailVO">
																			<tr>
																				<td width="20%">
																					<!--<input type="hidden" id="uc_company_name_{position()}" name="uc_company_name_{position()}" value="{company_name}"/>-->
																					<input type="hidden" id="uc_direct_mail_company_id_{position()}" name="uc_direct_mail_company_id_{position()}" value="{companyId}"/>
																					<xsl:variable name="status" select="status"/>
                                           <select name="uc_direct_mail_subscribe_status_{position()}" id="uc_direct_mail_subscribe_status" onchange="javascript:doOnChangeAction(this);">
                                              <xsl:choose>
																						<xsl:when test="$status = 'Unsubscribe'">
                                              <option value="Subscribe">S</option>
                                              <option value="Unsubscribe" selected="selected">U</option>
																						</xsl:when>
																						<xsl:when test="$status = 'Subscribe'">
                                              <option value="Subscribe" selected="selected">S</option>
                                              <option value="Unsubscribe" >U</option>
																						</xsl:when>
                                            <!-- you can't unsubscribe someone who has never opted in-->
																						<xsl:otherwise>
                                              <option value="" selected="selected">&nbsp;</option>
                                              <option value="Subscribe">S</option>
																						</xsl:otherwise>
																						</xsl:choose>
																					</select>
																				</td>
																				<td width="40%" >
																					<xsl:call-template name="applyEmailVO">
                                            <xsl:with-param name="pos" select="position()"/>
																					</xsl:call-template>
																				</td>
																				<td width="20%">
																					<xsl:call-template name="emailStatus">
                                          	<xsl:with-param name="pos" select="position()"/>
																					</xsl:call-template>
																				</td>
																				<td width="20%">
																					<xsl:call-template name="emailUpdated">
                                            <xsl:with-param name="pos" select="position()"/>
																					</xsl:call-template>
																				</td>
																			</tr>
																		</xsl:for-each>
																	</table>
																	</div>
																</td>
															</tr>
														</table><br />
														<div style="width:100%;" >
															<table border="0" cellpadding="0" cellspacing="0" width="100%" >
																<tr>
																	<td align="left" colspan="2">
                       							<button type="button"   id="ucaSubmitChanges" class="BlueButton" accesskey="G" onclick="javascript:doCheckCustomerAccount();" >Submit Chan(g)es</button>
                                    &nbsp;
																		<button type="button"   class="BlueButton" id="updateCustEmail" accesskey="U" onclick="javascript:doUpdateCustomerEmail();">
																			<xsl:if test="$email_found = true">
																				<xsl:attribute name="disabled">true</xsl:attribute>
																			</xsl:if>
																			(U)pdate Email Status
																		</button>&nbsp;
																		<button type="button"   class="BlueButton" id="addCustEmail"  accesskey="D" onclick="javascript:doAddEmailAddress();">A(d)d Email Address</button>
																	</td>
																</tr>
																<tr style="text-align:left;">
																	<td class="Label"  width="25%">
																																Hold Information:
																	</td>
																	<td class="textfield">
																		<xsl:choose>
																			<xsl:when test="count(CAI_HOLDS/child::*) > 1">
																				<textarea wrap="soft" rows="1" cols="50">
																					<xsl:value-of select="CAI_HOLDS/CAI_HOLD/customer_hold_description"/>
																				</textarea>
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="CAI_HOLDS/CAI_HOLD/customer_hold_description"/>
																			</xsl:otherwise>
																		</xsl:choose>
																	</td>
																</tr>
																<tr>
																	<td  >
									                                     <xsl:if test="count(CustomerVO/CustomerVO/MembershipVO) >= 1">
									                                        <script type="text/javascript">
									                                          var membershipObj = new Array();
									                                          <xsl:for-each select="CustomerVO/CustomerVO/MembershipVO">
									                                             var membershipData = new Array();
									                                            membershipData[1] = "<xsl:value-of select="membershipNumber"/>";
									                                            membershipData[2] = "<xsl:value-of select="membershipType"/>";
									                                            membershipData[3] = "<xsl:value-of select="firstName"/>";
									                                            membershipData[4] = "<xsl:value-of select="lastName"/>";
									                                              membershipObj[<xsl:value-of select="position()"/>] = membershipData;
									                                          </xsl:for-each>
									                                        </script>
									                                        <a href="#" class="textlink" onclick="doMembershipDisplay(membershipObj)" >Membership Info</a>
									                                       </xsl:if>
																	</td>
																	<td class="textfield">

																	</td>
																</tr>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                          <script type="text/javascript">
                                                                                                                                          var customerId = "<xsl:value-of select="key('pageData','uc_customer_id')/value"/>";
                                                                                                                                          </script>
                                                                                                                                          <!--  <xsl:if test="count(SERVICES/SERVICES_INFO) > 0"> -->
                                                                                                                                            <a href="javascript:doServicesInfoAction(customerId);" class="textlink" id="servicesInfoLink">Services Info</a> 
                                                                                                                                          <!-- </xsl:if> -->
                                                                                                                                    </td>
                                                                                                                                </tr>
															</table>
														</div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table border="0" align="center" width="98%" cellpadding="0" cellspacing="0">
					<tr>
						<td style="vertical-align:top;">
							<button type="button"   id="ucaCustomerComments" class="BigBlueButton" accesskey="C" onclick="javascript:doUCACustomerCommentsAction();">
								<xsl:if test="key('pageData','customer_comment_indicator')/value = 'Y'">
									<xsl:attribute name="style">font-weight:bold</xsl:attribute>
								</xsl:if>
								(C)ustomer<br />Comments
							</button>
							<button type="button"    id="ucaEmailHistory" class="BigBlueButton" accesskey="E" onclick="javascript:doEmailHistoryAction(email_hist_url);" >(E)mail<br />History</button>
						</td>
						<td align="right">
							<button type="button"   id="ucaSearch" class="BlueButton" style="width:55px;" accesskey="S" onclick="javascript:doUCASearchAction();">(S)earch</button>
							<button type="button"   id="ucaBack" class="BlueButton" style="width:105px;" accesskey="B" onclick="javascript:doUCABackAction();">(B)ack to Cust Acct</button><br />
							<button type="button"   id="ucaMainMenu" class="BigBlueButton" style="width:55px;" accesskey="M" onclick="javascript:doUCAMainMenuAction()" >(M)ain<br />Menu</button>
						</td>
					</tr>
				</table>
				<xsl:call-template name="footer"/>
			</div>
			<div id="waitDiv" style="display:none">
				<table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="1" >
					<tr>
						<td width="100%">
							<table class="innerTable" style="width:100%;height:300px" cellpadding="30" cellspacing="1">
								<tr>
									<td>
										<table width="100%" cellspacing="0" cellpadding="0" border="0">
											<tr>
												<td id="WaitMessage" align="right" width="60%" class="WaitMessage"></td>
												<td id="waitTD"  width="40%" class="WaitMessage"></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<xsl:call-template name="footer"/>
			</div><!-- TO remove put and end-comment marker here -->
		</form>
	</body>


</html>
</xsl:template>
<!--
	Template to display the header
-->
<xsl:template name="addHeader">
	<xsl:call-template name="header">
		<xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="headerName" select="'Update Customer Account'"/>
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
		<xsl:with-param name="showCSRIDs" select="true()"/>
    <xsl:with-param name="dnisNumber" select="$call_dnis"/>
    <xsl:with-param name="cservNumber" select="$call_cs_number"/>
    <xsl:with-param name="indicator" select="$call_type_flag"/>
    <xsl:with-param name="brandName" select="$call_brand_name"/>
    <xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
    <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
</xsl:template>

<!--
	Pulls the email address from the EmailVO nodes
	using the associated position of the DirectMailVO nodes above
-->
<xsl:template name="applyEmailVO">
	<xsl:param name="pos"/>
	<input type="hidden" name="uc_email_address_{$pos}" id="uc_email_address_{$pos}" value="{/root/CustomerVO/CustomerVO/EmailVO[$pos]/emailAddress/text()}"/>
	<a  style="text-decoration:none" title="{/root/CustomerVO/CustomerVO/EmailVO[$pos]/emailAddress/text()}">
		<xsl:choose>
			<xsl:when test="string-length(/root/CustomerVO/CustomerVO/EmailVO[$pos]/emailAddress/text()) > 20">
					<xsl:value-of select="substring(/root/CustomerVO/CustomerVO/EmailVO[$pos]/emailAddress/text(),1,12)"/>...
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/root/CustomerVO/CustomerVO/EmailVO[$pos]/emailAddress/text()"/>
			</xsl:otherwise>
		</xsl:choose>
	</a>
</xsl:template>

<!--
	This template is used to display the email subscribe/unsubscribe status
-->
<xsl:template name="emailStatus">
	<xsl:param name="pos"/>
	<input type="hidden" name="uc_email_subscribe_status_{$pos}" id="uc_email_subscribe_status_{$pos}" value="{/root/CustomerVO/CustomerVO/EmailVO[$pos]/subscribeStatus/text()}"/>
	<xsl:value-of select="/root/CustomerVO/CustomerVO/EmailVO[$pos]/subscribeStatus/text()"/>
</xsl:template>
<!--
	Pulls the date the email sub/unsub status had been last changed
-->
<xsl:template name="emailUpdated">
	<xsl:param name="pos"/>
	<input type="hidden" name="uc_email_updated_on_{$pos}" id="uc_email_updated_on_{$pos}" value="{/root/CustomerVO/CustomerVO/EmailVO[$pos]/updatedOn}"/>

  <xsl:if test="substring(/root/CustomerVO/CustomerVO/EmailVO[$pos]/updatedOn,1,4) != ''">
    <xsl:value-of select="substring(/root/CustomerVO/CustomerVO/EmailVO[$pos]/updatedOn,6,2)"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="substring(/root/CustomerVO/CustomerVO/EmailVO[$pos]/updatedOn,9,2)"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="substring(/root/CustomerVO/CustomerVO/EmailVO[$pos]/updatedOn,1,4)"/>
  </xsl:if>
  <xsl:if test="substring(/root/CustomerVO/CustomerVO/EmailVO[$pos]/updatedOn,1,4) = ''">
		&nbsp;
  </xsl:if>
</xsl:template>
</xsl:stylesheet>