<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="letterDetail" match="/root/letter/detail" use="name" />
	
	<xsl:template match="/">
	
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		  <fo:layout-master-set>
		    <fo:simple-page-master master-name="simpleA4" page-height="29.7cm" page-width="21cm" margin-top="2cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
		      <fo:region-body/>
		    </fo:simple-page-master>
		  </fo:layout-master-set>
		  
		  <fo:page-sequence master-reference="simpleA4">
		    <fo:flow flow-name="xsl-region-body">
		    
		    <fo:block text-align="center" space-after.optimum='25pt'>
           		<fo:external-graphic src="{key('letterDetail','logo')/value}"/>
			</fo:block>

		    <fo:block>
		    	<xsl:value-of select="key('letterDetail','body')/value" disable-output-escaping="yes"/>
		    </fo:block>
		    
		    </fo:flow>
		  </fo:page-sequence>
		</fo:root>
		
	</xsl:template>
</xsl:stylesheet>