/***********************************************************************************
* init()
************************************************************************************/
function init(){
  
}


/***********************************************************************************
* doPageAction()
************************************************************************************/
function doPageAction(val){
  if(window.event)
   if(window.event.keyCode)
    if(window.event.keyCode != 13)
      return false;
  var url = "unlockRecord.do" + "?action=" + pageAction[val];
  performAction(url);
}


/***********************************************************************************
* doUnlockRecordAction()
************************************************************************************/
function doUnlockRecordAction(val){
  if(doValidateForm()) {
    var checkBoxes = document.getElementsByName("lock_checkbox");
    
    for (i=0; i< checkBoxes.length; i++) {
      if (checkBoxes[i].checked == true) {
        var temp = checkBoxes[i].value;
        
        if (checkBoxes.length == 1) {
          document.forms[0].entitytype.value = temp.substring(0, temp.indexOf('^'));
          document.forms[0].entityid.value = temp.substring(temp.lastIndexOf('^')+1, temp.length);
        }
        else {
          document.forms[0].entitytype[i].value = temp.substring(0, temp.indexOf('^'));
          document.forms[0].entityid[i].value = temp.substring(temp.lastIndexOf('^')+1, temp.length);
        }
      }
    }
    
  //Going into modify order creates two locks.  An order_details lock and a customer lock.  Since
  //there is really no way to associate those two locks together we need to pop open a warning
  //message to the user to alert them in this instance they need to release the customer record 
  //too.  Otherwise, they will still be locked out of modify order.
  
      //Modal_URL
      var modalUrl = "confirm.html";
      //Modal_Arguments
      var modalArguments = new Object();
            
      modalArguments.modalText = 'When unlocking a customer or order record, you may also need to unlock other associated records.  Would you like to select other records?';
          
      //Modal_Features
      var modalFeatures  =  'dialogWidth:250px; dialogHeight:200px; center:yes; status=no; help=no;';
      modalFeatures +=  'resizable:no; scroll:no';
        
      //get a true/false value from the modal dialog.
      var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);
    
      if(!ret) {
        var url = "unlockRecord.do" + "?action=" + val;
        performAction(url);
      } 

 }
  else {
    alert("You must select at least one record to unlock.");
    var checkBoxes = document.getElementsByName("lock_checkbox");
    for (i=0; i< checkBoxes.length; i++) {
      checkBoxes[i].style.background = "pink";
    }
  }
}

/***********************************************************************************
* performAction()
************************************************************************************/
function performAction(url){
  scroll(0,0);
  showWaitMessage("content", "wait", "Updating");
  document.forms[0].action = url;
  document.forms[0].submit();
}

/***********************************************************************************
* checkAll()
************************************************************************************/
function checkAll() {
  var checkBoxes = document.getElementsByName("lock_checkbox");
  for (i=0; i< checkBoxes.length; i++) {
    if (checkBoxes[i].type == 'checkbox') {
      checkBoxes[i].checked = true;
    }
  }
}

/***********************************************************************************
* uncheckAll()
************************************************************************************/ 
function uncheckAll() {
  var checkBoxes = document.getElementsByName("lock_checkbox");
  for (i=0; i< checkBoxes.length; i++) {
    if (checkBoxes[i].type == 'checkbox') {
      checkBoxes[i].checked = false;
    }
  }
}


/*************************************************************************************
*	Performs the Back button functionality
**************************************************************************************/
function doBackAction() {
  var url = "unlockRecord.do?action=load";
  performAction(url);
}

/***********************************************************************************
* doReleaseAllAction()
************************************************************************************/ 
function doReleaseAllAction()
{
  var url = '';

  checkAll();
  
  //Modal_URL
  var modalUrl = "confirm.html";
  //Modal_Arguments
  var modalArguments = new Object();
        
  modalArguments.modalText = 'Are you sure you want to release all records for this csr?';
      
  //Modal_Features
  var modalFeatures  =  'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
  modalFeatures +=  'resizable:no; scroll:no';
    
  //get a true/false value from the modal dialog.
  var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  if(ret) {
      var url = "unlockRecord.do" + "?action=" + 'release_all';   
      performAction(url);
  } 
}

/*************************************************************************************
* Validate the form before the submit is allowed.
**************************************************************************************/
function doValidateForm() {
  var isValid =  checkForEmptyFields();
  return isValid;
}

/*************************************************************************************
* Validate the form before the submit is allowed.
**************************************************************************************/
function checkForEmptyFields() {
  
  var count = 0;
  
  var checkBoxes = document.getElementsByName("lock_checkbox");
  for (i=0; i< checkBoxes.length; i++)
  {
    if (checkBoxes[i].type == 'checkbox')
    {
      if (checkBoxes[i].checked == true)
        count++;
    }
  }
          
  if(count == 0) 
  {
    return false;
  }
  else
  {
    return true;
  }
}