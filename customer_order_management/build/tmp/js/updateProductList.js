/*
 * Used for the C.O.M updateProductList.xsl file
 * (This javascript along with the updateProductList.xsl file were copied
 * over from the order_scrub project and chagned to work with C.O.M.
 *
 *  Initialization
 */
var PAGE_URL = '';
var CANCEL_ACTION       = "cancel_update";
var CANCEL_UPDATE_PAGE  = 'cancelUpdateOrder.do?';
var tabsArray = new Array("custCancel", "sortLink", "pricepointfilter", "PrevPageSection", "NextPageSection");


function init() {
  setNavigationHandlers();
  var form = document.getElementById('ProductListForm');
  if (hasProducts){
     setScrollingDivHeight();
     document.getElementById("PrevPageSection").style.display = (form.page_number.value == "1") ? "none" : "";
     document.getElementById("NextPageSection").style.display = (form.page_number.value ==  form.total_pages.value) ? "none" : "";
  }

 window.onresize = setScrollingDivHeight;
 setTabIndexes(tabsArray, 20);
}


function setScrollingDivHeight(){
  var productListingDiv = document.getElementById("productListing");
  productListingDiv.style.height = document.body.clientHeight - productListingDiv.getBoundingClientRect().top - 75;
}


/*
 *  Show large image of product
 */
function viewLargeImage(prodId){
   var closeTag= "<a href=\"javascript:self.close();\" tabindex='-1'>close the window</a>";
   imageWindow = window.open("","enlarged_view","toolbar=no,resizable=no,scrollbars=no,width=340,height=470");
   imageWindow.document.write("<html>");
   imageWindow.document.write("<head>");
   imageWindow.document.write("<title>Large Image</title></head><body><form><center>");
   imageWindow.document.write("<br/>");
   imageWindow.document.write(closeTag);
   imageWindow.document.write("<br/>");
   imageWindow.document.write("<img ");
   imageWindow.document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
   imageWindow.document.write("if(imag != 'npi_2.jpg'){ ");
   imageWindow.document.write("this.src='" + product_images + "npi_2.jpg'}\" ");
   imageWindow.document.write("src='" + product_images + prodId + "_2.jpg'> ");
   imageWindow.document.write("</img>");
   imageWindow.document.write("<br/>");
   imageWindow.document.write(closeTag);
   imageWindow.document.write("</center></form></body></html>");
}


function doOrderNowAction(inProdId){
   var url = "search.do?product_id=" + inProdId;
 PAGE_URL = url;
 performAction(url);
}

function doSortByPrice(){
 document.getElementById('ProductListForm').sort_type.value = "priceSortAsc";
  var url = "loadProductList.do" +
        "?page_number=" + document.getElementById('ProductListForm').page_number.value;
 PAGE_URL = url;
 performAction(url);
}


function doPriceFilterAction(select){
   if (select[select.selectedIndex].value > 0) {
     var url = "loadProductList.do" +
          "?page_number=1" +
          "&price_point_id=" + select[select.selectedIndex].value;
    PAGE_URL = url;
    performAction(url);
  }
}

function doPrevPageAction(){
   var page = parseInt(document.getElementById('ProductListForm').page_number.value);
   doViewPageAction(page - 1);
}

function doNextPageAction(){
   var page = parseInt(document.getElementById('ProductListForm').page_number.value);
   doViewPageAction(page + 1);
}

function doViewPageAction(pageNum){
   document.getElementById('ProductListForm').page_number.value = pageNum;
   var url = "loadProductList.do" +
        "?page_number" + pageNum;
   PAGE_URL = url;
   performAction(url);
}

function doCategoriesAction(){
   var url = "CategoryServlet" +
        "?page_number=1";

  document.getElementById('ProductListForm').method = "get";
  PAGE_URL = url;
  performAction(url);
}

function doCloseAction(){
  window.close();
}


function doExitAction(){
   window.returnValue = new Array("_EXIT_");
   doCloseAction();
}


function performAction(url){
   PAGE_URL = url;
   showWaitMessage("mainContent", "wait");
   doRecordLock('MODIFY_ORDER','check',"", "ProductListForm");


}

function doContinueProcess(){
   document.getElementById('ProductListForm').action = PAGE_URL;
  document.getElementById('ProductListForm').submit();
}
/*

*/
function checkAndForward(forwardAction){
   doContinueProcess();
}

/*
 * Used to go back to the Update Product Category page.
 */
function doUpdateProductCategoryAction() {
  var toSubmit = document.getElementById("ProductListForm");
  toSubmit.action = "loadProductCategory.do";
  toSubmit.submit();
}

function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("ProductListForm");
    }
  }
  else {
    baseDoCancelUpdateAction("ProductListForm");
  }
}

/*
 * Handle the Update Delivery Information button event
 */
function doUpdateDeliveryInfo(){
  var url = 'loadDeliveryInfo.do';
  performAction(url);
}

/*
 * Handle the Update Product Category button event
 */
function doUpdateProductCategory(){
  var url = 'loadProductCategory.do';
  performAction(url);
}

/*******************************************************************************
*
*    performAction()
*
*******************************************************************************/
function performAction(url){
    /*
      Display a message to the user called Processing...
      this allows the user to know that the system is working
      and not stalled out.
      scroll(0,0) returns the window to the top position since
      all(most) buttons are located at the bottom of a scrolled window
    */
    scroll(0,0);
    showWaitMessage("mainContent", "wait", "Processing");

     document.forms[0].action = url;
     document.forms[0].submit();
}


