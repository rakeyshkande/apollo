/*******************************************************************************
*  setTabIndexes(var array)
*  This function takes a array of ids for elements on a page and sets the tab
*    indexes incrementally.  To change the order of what appears on the page,
*    you change the order that the ids appear in the array.
*******************************************************************************/
function setTabIndexes(arr, beginIndex)
{
	for(var x = 0; x < arr.length; x++)
  {
	  var element = document.getElementById(arr[x]);
    if (element != null)
    {
      element.tabIndex = beginIndex;
      beginIndex++;
    }
	}
  return beginIndex;
}
/*******************************************************************************
*  findBeginIndex()
*  Gives a starting index for the page.  Gives 100 index tab spaces for header
*   and tabs.
*******************************************************************************/
function findBeginIndex()
{
	var begIndex = 100;
	return begIndex;
}

/*******************************************************************************
*  untabAllElements()
*  Sets the tab index to -1 for all indexes on the page. To be used before
*  setting the tab indexes on a page.
*******************************************************************************/
function untabAllElements()
{
  var tabArray = document.getElementsByTagName('A', 'AREA', 'BUTTON', 'INPUT', 'OBJECT', 'SELECT', 'TEXTAREA');
 	for(var x = 0; x < tabArray.length; x++)
  {
    tabArray[x].tabIndex = -1;
  }
}

/*******************************************************************************
*  setFocusForArray(var array)
*  This function takes a array of ids for elements on a page and sets the focus
*   on the first element that exists in the array.
*******************************************************************************/
function setFocusForArray(arr)
{
	for(var x = 0; x < arr.length; x++)
  {
	  var element = document.getElementById(arr[x]);
    if (element != null)
    {
      element.focus();
      return true;
    }
	}
}
